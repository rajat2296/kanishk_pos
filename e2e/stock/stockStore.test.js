var should = require('should');
var supertest = require('supertest');

var server = supertest("http://localhost:9000");

describe("Stock Store OPEN API",function(){

	it("should create stock store",function(done){
	    server
	    .post("/api/stock/stores/partner/stockStore?customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .send({
	  		"store_id":"abcd1123",
	  		"storeName":"qwerty"
	    })
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.store_id.should.equal("abcd1123");
	      res.body.storeName.should.equal("qwerty");
	      done();
	    });
  	});

	it("should create stock store item",function(done){
	    server
	    .post("/api/stockItems/partner/stockItem?customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .send({
		  "store_id":"abcd1123",
		  "itemName":"Haldi",
		  "code": "asdf"
		})
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.store_id.should.equal("abcd1123");
	      res.body.itemName.should.equal("Haldi");
	      res.body.code.should.equal("asdf");
	      done();
	    });
  	});  	

	it("should delete stock store item",function(done){
	    server
	    .delete("/api/stockItems/partner/stockItem?code=asdf&store_id=abcd1123&customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.status.should.equal("success");
	      done();
	    });
  	});

  	it("should delete stock store",function(done){
	    server
	    .delete("/api/stock/stores/partner/stockStore?store_id=abcd1123&customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.status.should.equal("success");
	      done();
	    });
  	});

});