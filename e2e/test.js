describe('Test Suite', function(){
	
	before(function(){
		console.log('Starting All Tests.\n');
	})
	
	require('./onlineOrders/onlineOrder.test');
	require('./stock/stockStore.test');
	
	after(function(){
		console.log('\nAll tests completed.');
	})
})