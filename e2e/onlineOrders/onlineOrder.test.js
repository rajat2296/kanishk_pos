var should = require('should');
var supertest = require('supertest');

var server = supertest("http://localhost:9000");

describe("Online Order OPEN API",function(){

	it("GET /getCustomersByTenant",function(done){
	    server
	    .get("/api/onlineOrders/partner/getCustomersByTenant?mobile=7838088743&customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.should.be.instanceof(Array);
	      done();
	    });
  	});

  	it("GET /getDeploymentTabs",function(done){
	    server
	    .get("/api/onlineOrders/partner/getDeploymentTabs?customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.should.be.instanceof(Array);
	      done();
	    });
  	});

  	it("GET /getDeployment",function(done){
	    server
	    .get("/api/onlineOrders/partner/getDeployment?customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.should.be.instanceof(Array);
	      done();
	    });
  	});

  	it("GET /getFavourite",function(done){
	    server
	    .get("/api/onlineOrders/partner/getFavourite?mobile=7838088743&customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.should.be.instanceof(Array);
	      done();
	    });
  	});

  	it("GET /getItemByTab",function(done){
	    server
	    .get("/api/onlineOrders/partner/getItemByTab?tabtype=delivery&customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(200)
	    .end(function(err,res){
	      res.status.should.equal(200);
	      res.body.should.be.instanceof(Array);
	      done();
	    });
  	});
  	
  	it("GET /getOnlineOrderPageWise - customer not registered",function(done){
	    server
	    .get("/api/onlineOrders/partner/getOnlineOrderPageWise?mobile=7838088743&password=abcd&page=1&customer_key=23b9c78eb46e8adf5c1abb11cef2d05eb0ea15da0b8a784abc152c846faea668ad43adf1ea77b7b6d36db5b07197ee56")
	    .expect(401)
	    .end(function(err,res){
	      res.status.should.equal(401);
	      res.body.status.should.equal("error");
	      res.body.message.should.equal("Customer not registered.");
	      done();
	    });
  	});

});