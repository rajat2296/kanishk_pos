var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var fs = require('fs');
mongoose.connect("127.0.0.1", "mongoose_dbref", 27017);

/*var PersonSchema = new Schema({
    name    : String
  , age     : Number
  , stories : [{ type: Schema.ObjectId, ref: 'Story' }]
});
var StorySchema = new Schema({
    _creator : { type: Schema.ObjectId, ref: 'Person' }
  , title    : String
  , fans     : [{ type: Schema.ObjectId, ref: 'Person' }]
});*/

var ItemSchema = new Schema({
	name: String,
	rate: Number,
	category: {type: Schema.ObjectId, ref: 'Category'}
});

var CategorySchema = new Schema({
	name: String,
	superCategory: { type: Schema.ObjectId, ref: 'SuperCategory'}
});

var SuperCategorySchema = new Schema({
	name: String,
	station: {type: Schema.ObjectId, ref: 'Station'}
});

var StationSchema = new Schema({
	name: String,
	printName: String
});

var Item = mongoose.model('Item', ItemSchema);
var Category = mongoose.model('Category', CategorySchema);
var SuperCategory = mongoose.model('SuperCategory', SuperCategorySchema);
var Station = mongoose.model('Station', StationSchema);

/*var station1 = new Station({name: "Station 1", printName: "HP Deskjet 105"});
station1.save(function (err) {
	if (err) throw err;
	
	var sCat1 = new SuperCategory({name:"North India", station: station1._id});
	sCat1.save(function (err) {
		if (err) throw err;
		
		var cat1 = new Category({name:"Dals", superCategory: sCat1._id});
		cat1.save(function (err) {
			if (err) throw err;
			
			var item1 = new Item({name: "Dal Makhani", rate: 100, category: cat1._id});
			item1.save(function (err) {
				if (err) throw err;
				console.log("Item Saved");
			})
			
		})
	})
	
})*/


Item.find({name:/Dal Makhani/i}).populate('category').exec(function (err, items) {
	var opts = {
		path: 'category.superCategory',
		model: 'SuperCategory',
	};
	//console.log(item);
	Item.populate(items, opts, function (err, items){
		var opts2 = {
			path: 'category.superCategory.station',
			model: 'Station',
			select: 'printName'
		};
		Item.populate(items, opts2, function (err, items) {
			fs.writeFile('item', JSON.stringify(items));
			//console.log(items);
		});
		
	});
});

/*Item.findOne({name: /Dal Makhani/i}).populate('category').exec(function (err, item) {
	//console.log(item);
	Category.findOne({name: item.category.name}).populate('superCategory').exec(function (err, cat) {
		//console.log(cat);
		//item.category = cat;
		//console.log(item);
		Station.findOne({_id:cat.superCategory.station}).exec(function (err, sc) {
			//console.log(sc);
			//cat.superCategory.station = sc;
			delete cat.superCategory['station'];
			console.log(cat);
			//cat.superCategory.station = sc;
			//console.log(cat);
			
			//item.category  = cat;
			//item.category.superCategory.station = sc;
			//console.log(item);
			//item.category.superCategory = sc;
			//item.category.superCategory = sc;
			//console.log(item);
			//fs.writeFile('item', item);
		});
		item.category = sc;
		SuperCategory.findOne({name:sc.name})
		fs.writeFile('item',item);
	});
});*/


/*var Story  = mongoose.model('Story', StorySchema);
var Person = mongoose.model('Person', PersonSchema);

var aaron = new Person({name: 'Aaron', age: 100});
aaron.save(function (err) {
  if (err) throw err;

  var story1 = new Story({
      title: "A man who cooked Nintendo"
    , _creator: aaron._id
  });
	
  story1.fans.push(aaron._id);
  
  story1.save(function (err) {
    if (err) throw err;
	aaron.stories.push(story1._id);
	aaron.save(function (err) {
		
		Person.findOne({name: "Aaron"}).populate('stories').exec(function (err, person) {
		  if (err) throw err;

			fs.writeFile("person", person, function(err) {
				if(err) {
					return console.log(err);
				}

				console.log("The file was saved!");
			}); 
		})

		Story.findOne({title: /Nintendo/i}).populate('_creator')
				.exec(function (err, story) {
		  if (err) throw err;
			fs.writeFile("story", story, function(err) {
				if(err) {
					return console.log(err);
				}

				console.log("The file was saved!");
			}); 
		})
	})
  });
});*/

// person = { stories: [ ],
//   _id: 4e566b7131ca6f2825000001,
//   age: 100,
//   name: 'Aaron' }
// person.stories = undefined
// story = { fans: [ ],
//   _id: 4e566b7131ca6f2825000002,
//   _creator:
//    { stories: [ ],
//      _id: 4e566b7131ca6f2825000001,
//      age: 100,
//      name: 'Aaron' },
//   title: 'A man who cooked Nintendo' }