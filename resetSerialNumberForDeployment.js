var arg_serialNumberStart = NumberInt(start);
var arg_deployment_id = depId;

print("==:: Reset Bill Serial Number ::==");
print("== Start SerialNumber: " + arg_serialNumberStart + " ==");
print("== Deployment ID: " + arg_deployment_id + " ==");

var conn = new Mongo();
var db = conn.getDB("posist-dev");
var bills = db.bills.find(
{ 
	deployment_id: ObjectId("554d093c50f463dc22e5eaa2") 
}).sort(
{
	serialNumber: 1
});

bills.forEach(function(bill) {
  //print(bill.serialNumber);
  print(bill._id);
  var _sN = NumberInt(bill.serialNumber);
  _sN = start;
  db.bills.update({ _id: bill._id } , {
	  $set: {
		  "serialNumber": NumberInt(_sN)
	  }
  });
  start++;
});

print("==Showing Updated SerialNumbers==");
var bills = db.bills.find({
	deployment_id: ObjectId("554d093c50f463dc22e5eaa2")
}).sort({
	serialNumber: 1
});
bills.forEach(function (bill) {
	print(bill.serialNumber);
	print(typeof bill.serialNumber);
});