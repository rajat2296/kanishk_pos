/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');
var multer = require('multer');
var bodyParser = require('body-parser');
var cors=require('cors');
var cluster = require('cluster');
var os = require('os');
// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);

function serve(port) {
    var app = express();
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    app.use(cors());
    app.disable('etag');

    var server = require('http').createServer(app);
    require('./config/express')(app);
    require('./routes')(app);
    server.listen(parseInt( port), config.ip, function () {
        console.log('Express server listening on %d, in %s mode', parseInt( port), app.get('env'));
    });
    exports = module.exports = app;

}
serve(9000);
