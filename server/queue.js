'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var mongoose = require('mongoose');
var config = require('./config/environment');
var multer = require('multer');
var Item = require('./api/item/item.model');
var Tab = require('./api/tab/tab.model');
var _=require('lodash')
var onlineOrder=require('./api/onlineOrder/KueOnlineOrder');
var kueBill=require('./api/bill/KueBill');
var kueReport=require('./api/report/kueReport');
var posToMIQ = require('./api/PosToMI/PosToMI.queue');
mongoose.connect(config.mongo.uri, config.mongo.options);


var kue = require('kue')
  , cluster = require('cluster')
 , queue = kue.createQueue(config.redisConn);

var clusterWorkerSize = require('os').cpus().length;

if (cluster.isMaster) {
  kue.app.listen(3000);
  for (var i = 0; i < clusterWorkerSize; i++) {
    cluster.fork();
  }
  cluster.on('exit', function (worker, code, signal) {
      console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
      console.log('Starting a new worker');
      cluster.fork();
  });
} else {
 //  onlineOrder.doQueue(queue);
 // kueBill.doQueue(queue);
  posToMIQ.doQueue(queue);
  kueReport.doQueue(queue);

}