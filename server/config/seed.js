/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');

Thing.find({}).remove(function() {
  Thing.create({
    name : 'No Installation Required',
    info : 'Works in a browser online.'
  }, {
    name : 'Works Online and Offline',
    info : 'Works even when internet connection goes off.'
  }, {
    name : 'Never Lose Data',
    info : 'All data securely saved on a Cloud.'
  },  {
    name : 'Anywhere Reports Access',
    info : 'Access the data on mobile while travelling.'
  },  {
    name : 'Supports all Hardwares',
    info : 'No special hardware required. Even a PC or a Tablet will work.'
  },{
    name : 'Cost Effective',
    info : 'No licence cost. No long term Contracts.'
  });
});

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function() {
      console.log('finished populating users');
    }
  );
});