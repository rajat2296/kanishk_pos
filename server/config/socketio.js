/**
 * Socket.io configuration
 */

'use strict';

var config = require('./environment');
var _ = require('lodash');
var lockBill = require('../api/syncKot/syncLockBill.model');
var mongoose = require('mongoose');
var count = 0;
//var cacheData=[];
//var cacheOb= {dep:};
// When the user disconnects.. perform this
function onDisconnect(socket) {
  console.log(socket.disconnected);
}

// When the user connects.. perform this
function onConnect(socket) {

  console.log(socket.connected);
  // When the client emits 'info', this listens and executes
  /*socket.on('instance_start', function (data) {
   var lockData={tables:[],nonTables:[]};
   var _f= _.filter(cacheData,{'deployment_id':data.deployment_id});
   if(_f.length>0){
   // lockData.tables=_f[0].tables;
   // lockData.nonTables=_f[0].nonTables;
   socket.broadcast.emit('instance_start_'+data.deployment_id, _f[0]);

   }else{
   socket.broadcast.emit('instance_start_'+data.deployment_id, lockData);
   }
   console.log('instance'+data.deployment_id);
   // return cacheData;
   });*/

  socket.on('info', function (data) {
    console.info('Hello [%s] %s', socket.address, JSON.stringify(data, null, 2));
    socket.emit('info', JSON.stringify({isConnected: true}));
  });

  /* Table Start Sync */
  socket.on('table_start', function (data) {
    console.log("Event: Table Start");
    console.log(data);
    socket.broadcast.emit('table_started' + data.deployment_id, data);
  });

  socket.on('table_event', function (data) {
    console.log("Event: Table ");
    // console.log(data);
    socket.broadcast.emit('table_event_' + data.deployment_id, data);
    saveLockedBill(data);

    //---added by rajat for syncing payments b/w instances--//
    socket.on('added_payment',function(data){
      console.log('added payment event');
      socket.broadcast.emit('added_payment'+data.deployment_id,data);
    })

    socket.on('cancel_payment',function(data){
      console.log('cancel_payment');
      socket.broadcast.emit('cancel_payment'+data.deployment_id,data);
    })


    socket.on('table_split_remove_event',function(data){
    console.log('split_remove',data);
    socket.broadcast.emit('table_split_remove_event' + data.deployment_id,data);
  })
    //---added by rajat for syncing payments b/w instances end--//
    /* */
    /*     syncLock.findOne({deployment_id:req.query.deployment_id},function(err,syncLockData) {

     });*/
    /*
     if(data.type=='table'){

     saveLockedBill({deployment_id:data.deployment_id,tables:tables,nonTables:[]});
     // lockBill.create(cacheData, function(err, syncKot){});
     }else {
     if (data.event == 'add') {
     {_f[0].tables.push(data.number);}
     }
     else
     {
     var tempTables=[];
     _.forEach(_f[0].tables,function(table){
     if(table!=data.number){
     tempTables.push(data.number);
     }
     })
     //_f[0].tables.splice(_f[0].tables.indexOf(data.number), 1);
     _f[0].tables=tempTables;
     }
     saveLockedBill(_f[0]);
     }

     }else{
     var _f= _.filter(cacheData,{'deployment_id':data.deployment_id})
     if(_f.length==0){
     var nonTables=[];
     nonTables.push(data.number);
     cacheData.push({deployment_id:data.deployment_id,tables:[],nonTables:[nonTables]})
     saveLockedBill({deployment_id:data.deployment_id,tables:[],nonTables:[nonTables]});
     // lockBill.create(cacheData, function(err, syncKot){});
     }else {
     if (data.event == 'add') {
     {_f[0].nonTables.push(data.number);}
     }
     else
     {
     //    _f[0].nonTables.splice(_f[0].tables.indexOf(data.number), 1);
     var tempTables=[];
     _.forEach(_f[0].nonTables,function(table){
     if(table!=data.number){
     tempTables.push(data.number);
     }
     })
     //_f[0].tables.splice(_f[0].tables.indexOf(data.number), 1);
     _f[0].nonTables=tempTables;
     }
     saveLockedBill(_f[0]);
     }
     }*/
    //console.log(cacheData);

  });
  socket.on('table_split_event', function (data) {
    console.log("Event: Table ");
    console.log(data);
    socket.broadcast.emit('table_split_event_' + data.deployment_id, data);
  });

  socket.on('table_toBeReleased', function (data) {
    socket.broadcast.emit('table_freed' + data.deployment_id, data);
  });

  function saveLockedBill(bill) {
    lockBill.findOne({deployment_id: new mongoose.Types.ObjectId(bill.deployment_id)}, function (err, bills) {
      // console.log(bills);
      var tempbill = {deployment_id: bill.deployment_id, tables: [], nonTables: []};

      if (!bills) {
        //  var tempbill={deployment_id:bill.deployment_id,tables:[],nonTables:[]};
        if (bill.type == 'table') {
          if (bill.event = 'add') {
            tempbill.tables.push(bill.number);
          }
        } else {
          if (bill.event = 'add') {
            tempbill.nonTables.push(bill.number);
          }
        }
        console.log('create');
        lockBill.create(tempbill, function (err, syncKot) {
          console.log(err)
        });
        return;
      }
      // var tempbill={deployment_id:bill.deployment_id,tables:[],nonTables:[]};
      tempbill.tables = bills.tables;
      tempbill.nonTables = bills.nonTables;
      if (bill.type == 'table') {
        if (bill.event == 'add') {
          tempbill.tables.push(bill.number);
        }
        else {
          var tempts = [];
          _.forEach(tempbill.tables, function (table) {
            //if(table!=bill.number){
            if (_.has(bill, 'number')) {
              if (bill.number != null) {
                if (table.toString() != bill.number.toString()) {
                  tempts.push(table);
                }
              }
            }
          })
          tempbill.tables = tempts;
        }
      } else {
        if (bill.event == 'add') {
          tempbill.nonTables.push(bill.number);
        }
        else {
          var tempts = [];
          _.forEach(tempbill.nonTables, function (table) {
            console.log(table);
            console.log(bill.number);
            if (_.has(bill, 'number')) {
              if (bill.number != null) {
                if (table.toString() != bill.number.toString()) {
                  tempts.push(table);
                }
              }
            }
          })
          tempbill.nonTables = tempts;
        }
      }
      // var updated = _.merge(bills, tempbill);
      // updated.save(function (err) {console.log(err); });
      lockBill.update({deployment_id: new mongoose.Types.ObjectId(bill.deployment_id)}, {
        $set: {
          tables: tempbill.tables,
          nonTables: tempbill.nonTables
        }
      }, {multi: false}, function (err) {
        //  console.log(err);
      });
    });
  }

  // Insert sockets below
  require('../api/stockLogs/stockLogs.socket').register(socket);
  require('../api/stockMasterUser/stockMasterUser.socket').register(socket);
  require('../api/stockComment/stockComment.socket').register(socket);
  //require('../api/stockAggregate/stockAggregate.socket').register(socket);

 // require('../api/delivery/delivery.socket').register(socket);
  //require('../api/dayBreakup/dayBreakup.socket').register(socket);
  //require('../api/section/section.socket').register(socket);
  //require('../api/demand/demand.socket').register(socket);
  //require('../api/outletIndent/outletIndent.socket').register(socket);
  //require('../api/purchaseOrder/purchaseOrder.socket').register(socket);
  //require('../api/nomnom/nomnom.socket').register(socket);
  //require('../api/billCardTransaction/billCardTransaction.socket').register(socket);
  //require('../api/cardTransaction/cardTransaction.socket').register(socket);
  /* require('../api/notification/notification.socket').register(socket);*/
  /*require('../api/accounts/accounts.socket').register(socket);
   require('../api/PosToDLF/PosToDLF.socket').register(socket);
   require('../api/PosToMI/PosToMI.socket').register(socket);*/
//    require('../api/leadForm/leadForm.socket').register(socket);
//    require('../api/feedback/feedback.socket').register(socket);
  //   require('../api/advancebooking/advancebooking.socket').register(socket);
  /*require('../api/settle/settle.socket').register(socket);*/
  // require('../api/baseKitchenItem/baseKitchenItem.socket').register(socket);
  // require('../api/baseKitchenUnit/baseKitchenUnit.socket').register(socket);
  // require('../api/BaseKitchenItems/BaseKitchenItems.socket').register(socket);
  // require('../api/BaseKitchenUnits/BaseKitchenUnits.socket').register(socket);
  // require('../api/BasekitchenUnit/BasekitchenUnit.socket').register(socket);
  // require('../api/BasekitchenItem/BasekitchenItem.socket').register(socket);
  /*require('../api/emailer/emailer.socket').register(socket);*/
  //  require('../api/favouriteOrder/favouriteOrder.socket').register(socket);
//    require('../api/stockReportNew/stockReportNew.socket').register(socket);
  //require('../api/mobileauth/mobileauth.socket').register(socket);
  // require('../api/smstransaction/smstransaction.socket').register(socket);
  /* require('../api/smstemplate/smstemplate.socket').register(socket);
   */
  /* require('../api/mobileApp/mobileApp.socket').register(socket);*/
  /*
   require('../api/onlineOrder/onlineOrder.socket').register(socket);

   require('../api/dashboard/dashboard.socket').register(socket);

   require('../api/codeApplied/codeApplied.socket').register(socket);
   require('../api/stockReport/stockReport.socket').register(socket);
   require('../api/editTransactionHistory/editTransactionHistory.socket').register(socket);
   require('../api/vendorPaymentHistory/vendorPaymentHistory.socket').register(socket);
   require('../api/stockRequirement/stockRequirement.socket').register(socket);
   require('../api/stockTransaction/stockTransaction.socket').register(socket);
   require('../api/offerCode/offerCode.socket').register(socket);
   require('../api/dateList/dateList.socket').register(socket);
   require('../api/report/report.socket').register(socket);
   require('../api/deploymentclone/deploymentclone.socket').register(socket);
   require('../api/upload/upload.socket').register(socket);
   require('../api/station/station.socket').register(socket);
   require('../api/email/email.socket').register(socket);
   require('../api/customer/customer.socket').register(socket);
   require('../api/stockProperty/stockProperty.socket').register(socket);
   require('../api/vendor/vendor.socket').register(socket);
   require('../api/receiver/receiver.socket').register(socket);
   require('../api/stockItem/stockItem.socket').register(socket);
   require('../api/stockUnit/stockUnit.socket').register(socket);
   require('../api/stockCategory/stockCategory.socket').register(socket);
   require('../api/store/store.socket').register(socket);
   require('../api/bill/bill.socket').register(socket);
   require('../api/role/role.socket').register(socket);
   require('../api/item/item.socket').register(socket);
   require('../api/superCategory/superCategory.socket').register(socket);
   require('../api/deployment/deployment.socket').register(socket);
   require('../api/offer/offer.socket').register(socket);
   require('../api/tax/tax.socket').register(socket);
   require('../api/category/category.socket').register(socket);
   require('../api/tab/tab.socket').register(socket);
   require('../api/tenant/tenant.socket').register(socket);
   require('../api/thing/thing.socket').register(socket);
   */

  //if(!socket.connected)
  if (count == 0) {
    require('../api/onlineOrder/onlineOrder.socket').register(socket);
    require('../api/syncKot/syncKot.socket').register(socket);
    require('../api/onlineOrder/onlineOrder.controller').registerOnline(socket);
    require('../api/syncKot/syncKot.controller').set(socket);
    require('../api/payments/payments.controller').set(socket);
    require('../api/technopurple/technopurple.controller').set(socket);
    require('../api/zomatoTrace/zomatoTrace.controller').set(socket);
    require('../api/delivery/delivery.controller').set(socket);
    require('../api/syncKot/seatcustomer.socket').register(socket);
    //require('../api/socket/socket.controller').set(socket);
  }
  count += 1;
  //console.log(count);
  /*  require('../api/bill/bill.socket').register(socket);*/

}

module.exports = function (socketio) {
  // socket.io (v1.x.x) is powered by debug.
  // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
  //
  // ex: DEBUG: "http*,socket.io:socket"

  // We can authenticate socket.io users and access their token through socket.handshake.decoded_token
  //
  // 1. You will need to send the token in `client/components/socket/socket.service.js`
  //
  // 2. Require authentication here:
  // socketio.use(require('socketio-jwt').authorize({
  //   secret: config.secrets.session,
  //   handshake: true
  // }));

  socketio.on('connection', function (socket) {
    // socket.emit('auth');
    socket.address = socket.handshake.address !== null ?
    socket.handshake.address.address + ':' + socket.handshake.address.port :
      process.env.DOMAIN;
    socket.connectedAt = new Date();

    // Call onDisconnect.
    socket.on('disconnect', function () {
      onDisconnect(socket);
      //  console.log(socket);
      console.info('[%s] DISCONNECTED Shit', socket.address);
    });

    // Call onConnect.
    onConnect(socket);
    // console.log(socket.handshake);
    console.info('[%s] CONNECTED YO :-)', socket.address);
  });
};
