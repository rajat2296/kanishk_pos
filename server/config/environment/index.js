'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT || 9000,

  // Should we populate the DB with sample data?
  seedDB: false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'posist-secret'
  },

  // List of user roles
  userRoles: ['guest', 'user', 'admin'],
  max_cap:70000,
  //White Listed IP addresses for OPEN API
  white_ips: ['127.0.0.1','::1','::ffff:127.0.0.1', '139.162.43.175', 'http://139.162.43.175'],
  redisConn:{
  prefix: 'q',
  redis: {
    port: 6379,
    //host:'178.79.131.220'
      host:'127.0.0.1'
   // host: '162.13.82.114'
   // ,auth: 'techno123pos567&^%'
  }
},
isQueue:false,
max_cap:70000,
reportServer:{url: 'http://178.79.131.220:9000'},

  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true
      }
    }
  },

  facebook: {
    clientID:     process.env.FACEBOOK_ID || 'id',
    clientSecret: process.env.FACEBOOK_SECRET || 'secret',
    callbackURL:  process.env.DOMAIN || '' + '/auth/facebook/callback'
  },

  twitter: {
    clientID:     process.env.TWITTER_ID || 'id',
    clientSecret: process.env.TWITTER_SECRET || 'secret',
    callbackURL:  process.env.DOMAIN || '' + '/auth/twitter/callback'
  },

  google: {
    clientID:     process.env.GOOGLE_ID || 'id',
    clientSecret: process.env.GOOGLE_SECRET || 'secret',
    callbackURL:  process.env.DOMAIN || '' + '/auth/google/callback'
  }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
