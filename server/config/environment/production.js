'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip:       process.env.OPENSHIFT_NODEJS_IP ||
  process.env.IP ||
  undefined,

  // Server port
  port:     process.env.OPENSHIFT_NODEJS_PORT ||
  process.env.PORT ||
  9300,

  // MongoDB connection options
  mongo: {
    uri:    process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    process.env.OPENSHIFT_MONGODB_DB_URL+process.env.OPENSHIFT_APP_NAME ||
    //'mongodb://106.186.16.166:27017/posist-dev'
    'mongodb://127.0.0.1:6666/posist-dev'
    //'mongodb://127.0.0.1/posist-org'
  }
};
