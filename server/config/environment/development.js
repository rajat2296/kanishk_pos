'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    
    //uri:'mongodb://127.0.0.1/posist-dev',
//     uri:'mongodb://127.0.0.1/posist-org',
    uri:'mongodb://192.168.0.192/posist-org',

    //uri:'mongodb://192.168.0.106/posist-dev',
    //uri:'mongodb://localhost/posist-dev',
    //uri: 'mongodb://106.1816.166:27017/posist-dev',
    //uri: 'mongodb://test.po//sist.net/posist-dev',
    options:{db:{native_parser:true}}
  },
  seedDB: false
};
