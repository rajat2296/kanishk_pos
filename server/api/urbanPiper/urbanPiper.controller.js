/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var UrbanPiper = require('./urbanPiper.model');
var request = require('request');
var Item = require('../item/item.model');
var async = require('async');

// Get list of things
//var url = 'https://staging.urbanpiper.com';
var url = 'https://api.urbanpiper.com';
exports.index = function(req, res) {
  console.log(req.query)
  UrbanPiper.find({tenantId: req.query.tenantId, deploymentId: req.query.deploymentId}, function (err, things) {
    if(err) { return handleError(res, err); }
    return res.json(200, things);
  });
};

// Get a single thing
exports.show = function(req, res) {
  UrbanPiper.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    return res.json(thing);
  });
};

// Creates a new thing in the DB.
exports.create = function(req, res) {
  console.log("create")
  UrbanPiper.create(req.body, function(err, thing) {
    if(err) { return handleError(res, err); }
    return res.json(201, thing);
  });
};

// Updates an existing thing in the DB.
exports.update = function(req, res) {
  UrbanPiper.findById(req.body._id, function (err, thing) {
    if (err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    var updated = _.merge(thing, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, thing);
    });
  });
};

// Deletes a thing from the DB.
exports.destroy = function(req, res) {
  console.log("req.params", req.body)
  UrbanPiper.findById(req.body.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    thing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.postItems = function(req,res){
  var options = { method: 'POST',
    uri: url+'/api/v1/order/items/locations/',
    followAllRedirects: true,
    maxRedirects: 10,
    headers:
    {
      'authorization': 'APIKey '+ req.body.urbanPiperKeys.username+':'+req.body.urbanPiperKeys.apiKey,
      'content-type': 'application/json' },
    body:req.body.items,
    json: true };

  request(options, function (error, response, body) {
    console.log(error)
    //if(!error){
    if(response && response.statusCode && response.statusCode == 200){

      async.forEach(body, function (item, callback1) {
        Item.findById(item.item_ref_id, function (err, thing) {
          console.log(thing)
          if (err) { console.log(err); }
          if(thing) {
            var updated = _.merge(thing, {urbanPiperId : 1});
            updated.save(function (err, saved) {
              if (err) { console.log(err); }
              console.log(saved)
            });
          }
          callback1();
        });
      },function(error){
        return res.send({ status: response.statusCode });
      });
    }
    //}
    else{
      //console.log("error")
      return res.send({ status: 500});
    }

  });
}

exports.updateItems = function(req,res){
  var options = { method: 'PUT',
    uri: url+'/api/v1/order/items/locations/',
    followAllRedirects: true,
    maxRedirects: 10,
    headers:
    {
      'authorization': 'APIKey '+ req.body.urbanPiperKeys.username+':'+req.body.urbanPiperKeys.apiKey,
      'content-type': 'application/json' },
    body:req.body.items,
    json: true };
    //console.log(req.body.items)
  request(options, function (error, response, body) {
    console.log(body)
    if(response && response.statusCode && response.statusCode == 200){
      return res.send({ status: response.statusCode });
    }
    else{
      return res.send({ status: 500 });
    }

  });
}

exports.deleteItems = function(req,res){
  var options = { method: 'PUT',
    uri: url+'/api/v1/order/items/locations/',
    followAllRedirects: true,
    maxRedirects: 10,
    headers:
    {
      'authorization': 'APIKey '+ req.body.urbanPiperKeys.username+':'+req.body.urbanPiperKeys.apiKey,
      'content-type': 'application/json' },
    body:req.body.items,
    json: true };
  //console.log(req.body.items)
  request(options, function (error, response, body) {
    //console.log(error,response)
    if(response && response.statusCode && response.statusCode == 200){
      async.forEach(body, function (item, callback1) {
        Item.findById(item.item_ref_id, function (err, thing) {
          if (err) { console.log(err); }
          if(thing) {
            var updated = _.merge(thing, {urbanPiperId : -1});
            updated.save(function (err, saved) {
              if (err) { console.log(err); }
            });
          }
          callback1();
        });
      },function(error){
        return res.send(200);
      });

    }
    else{
      return res.send(500);
    }

  });
}

exports.postItem = function(req,res){
  var options = { method: 'POST',
    uri: url+'/api/v1/order/item/'+req.body.itemId+'/location/'+req.body.depId+'/',
    followAllRedirects: true,
    maxRedirects: 10,
    headers:
    {
      'authorization': 'APIKey '+ req.body.urbanPiperKeys.username+':'+req.body.urbanPiperKeys.apiKey,
      'content-type': 'application/json' },
    body:req.body.item,
    json: true };

  request(options, function (error, response, body) {
    if(response && response.statusCode && response.statusCode == 200){
      Item.findById(body.item_ref_id, function (err, thing) {
        if (err) { console.log(err); }
        if(thing) {
          var updated = _.merge(thing, {urbanPiperId : 1});
          updated.save(function (err) {
            if (err) { console.log(err); }
          });
        }
      });
      return res.send({ status: 200 });
    }
    else
      return res.send({ status: 500 });

  });
}

exports.updateItem = function(req,res){
  var options = { method: 'PUT',
    uri: url+'/api/v1/order/item/'+req.body.itemId+'/location/'+req.body.depId+'/',
    followAllRedirects: true,
    maxRedirects: 10,
    headers:
    {
      'authorization': 'APIKey '+ req.body.urbanPiperKeys.username+':'+req.body.urbanPiperKeys.apiKey,
      'content-type': 'application/json' },
    body:req.body.item,
    json: true };
  request(options, function (error, response, body) {
    if(response && response.statusCode && response.statusCode == 200){
      return res.send({ status: response.statusCode });
    }
    else{
      return res.send({ status: 500 });
    }

  });
}
exports.updateItemLocationAssociation=function(req,res){
  var options = { method: 'POST',
    uri: url+'/api/v1/order/items/locations/',
    followAllRedirects: true,
    maxRedirects: 10,
    headers:
    {
      'authorization': 'APIKey '+ req.body.urbanPiperKeys.username+':'+req.body.urbanPiperKeys.apiKey,
      'content-type': 'application/json' },
    body:req.body.item,
    json: true };
  request(options, function (error, response, body) {
     console.log(options,options.body.association.taxes,body);  
    if(response && response.statusCode && response.statusCode == 200){
      return res.send({ status: response.statusCode });
    }
    else{
      return res.send({ status: 500 });
    }
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
