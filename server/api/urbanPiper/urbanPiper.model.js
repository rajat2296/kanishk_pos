'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UrbanPiperSchema = new Schema({
    username: String,
    apiKey: String,
    tenantId: String,
	deploymentId: String,
	active: { type: Boolean, default: false }
});

module.exports = mongoose.model('UrbanPiper', UrbanPiperSchema);