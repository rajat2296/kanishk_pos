'use strict'

var _ = require('lodash');
var email   = require("emailjs/email");
var Email = require('./email.model');
var moment = require( 'moment' );
var http=require('http');
var request=require('request');
var utils=require('../Utils/utils');
/*

 var nodemailer = require('nodemailer');
 var sgTransport = require('nodemailer-sendgrid-transport');
 var http = require('http')
 var options = {
 auth: {
 api_user: 'posist',
 api_key: 'pos123ist'
 }
 }
 var mailer = nodemailer.createTransport(sgTransport(options));


 var server  = email.server.connect({
 user:    "ranjeet.sinha@posist.com",
 password:"ranjeet@123",
 host:    "smtp.gmail.com",
 ssl:     true

 });
 */

// Get list of emails
exports.index = function(req, res) {
  Email.find(function (err, emails) {
    if(err) { return handleError(res, err); }
    return res.json(200, emails);
  });
};

// Get a single email
exports.show = function(req, res) {
  Email.findById(req.params.id, function (err, email) {
    if(err) { return handleError(res, err); }
    if(!email) { return res.send(404); }
    return res.json(email);
  });
};

// Creates a new email in the DB.
exports.create = function (req, res) {

  console.log(req.body);
  server.send({
    text: req.body.messageText,
    from: "Posist Support <support@posist.com>",
    to: req.body.to,
    subject: "Welcome to Posist",
    attachment: [
      {data: req.body.messageHtml, alternative: true}
    ]
  }, function (err, message) {
    /*console.log(message);*/
    console.log(err);
    res.json(201, err || message);
  });


};

// Updates an existing email in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Email.findById(req.params.id, function (err, email) {
    if (err) { return handleError(res, err); }
    if(!email) { return res.send(404); }
    var updated = _.merge(email, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, email);
    });
  });
};

// Deletes a email from the DB.
exports.destroy = function(req, res) {
  Email.findById(req.params.id, function (err, email) {
    if(err) { return handleError(res, err); }
    if(!email) { return res.send(404); }
    email.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.sendBillToCustomer = function(req, res) {
  // console.log(req.body)
  var subTotal = 0;
  var totalQuantity = 0;
  var roundOff = 0;
  var discount = (req.body.billObject.DAmt=='')?0:parseFloat(req.body.billObject.DAmt);
  var headers = req.body.billObject.Hd.split("\n")
  var footers = req.body.billObject.Ft.split("\n")
  for (var i = 0; i < req.body.items.length; i++) {
    subTotal += req.body.items[i].R*req.body.items[i].Q;
    totalQuantity += req.body.items[i].Q;
  }
  var grandTotal = 0.00;
  grandTotal = parseFloat(subTotal);
  for (var i = 0; i < req.body.taxes.length; i++) {
    grandTotal += parseFloat(req.body.taxes[i].A);
  }

  grandTotal-=parseFloat(discount);
  roundOff =utils.roundNumber(  utils.roundNumber(grandTotal,0)- grandTotal,2);
  grandTotal=utils.roundNumber(grandTotal,0);
  var obj = {
    header : '<div style="padding:10px 0;text-align:center"><table style="width:100%"> <tr><td style="font-family:arial; color:white;font-size:30px;font-weight:bold">'+headers[0]+'</td><tr></table></div>',
    subHeader : '<div style="background-color:#ebeef0;"><table style="width:100%"><tr><td style="font-family:arial; color:red; padding-left:10px;width:75%;text-align:center">',
    subHeader1 : '<td style="background-color:#22a753;color:white;padding:5px"><span style="font-family:arial; color:white;font-size:23px;font-weight:bold">Bill No. : '+req.body.billObject.BN+'</span><br /><span style="font-family:arial; color:white;font-size:13px;">'+moment(req.body.billObject.BD).format("dddd, MMMM Do YYYY, h:mm:ss a")+'</span></td></tr></table></div>',
    subHeader2 : '<tr><td style="font-family:arial; color:white;font-size:13px;padding-left:10px;">Customer Information:</td><tr></table></div>',
    footer : '<div style="padding:20px 5px"><table style="width:100%">',
    customerHeader : '<div>Customer Details:</div>',
    customerNameMobile : '<table style="width:100%;padding:10px;"><tr><td style="width:70%"><table><tr><td style="font-family:arial; color:#13414c; font-weight:bold;padding-left:10px;">Name: '+req.body.billObject.Name+'</td></tr><tr><td style="font-family:arial; color:#13414c; font-weight:bold;padding-left:10px;">Mobile: '+req.body.billObject.MNm+'</td></tr></table></td>',
    address1 : req.body.billObject.Add+', ',
    address2 : req.body.billObject.St+', ',
    city : req.body.billObject.Cy+', ',
    state : req.body.billObject.Sta+', ',
    pin : req.body.billObject.Pin,
    billDeatils : '<div>Bill Number: '+req.body.billObject.BN+'</div>'+'<div>Steward: '+req.body.billObject.Waiter+'</div>'+'<div>Date: '+moment(req.body.billObject.BD).format("dddd, MMMM Do YYYY, h:mm:ss a")+'</div>',
    Type : '<div>Type:'+req.body.billObject.Tp+'&nbsp;&nbsp;TableNo.: '+req.body.billObject.TN,
    itemHeader :'<div><table cellspacing="0" cellpadding="0" style="border:none;width:100%;font-weight:bold;color:white;"><tr><td style="padding:5px;width:40%;background:#008bd2;padding-left:10px;">Item</td><td style="width:30%;background:#008bd2;">Qty.</td><td style="width:10%;background:#008bd2;">Amount</td><td style="width:20%;background:#008bd2;"></td></tr></table></div>',
    qtySubTotal:'<div style="background:#ebeef0;padding:10px 0;"><table style="width:100%;color:#13414c"><tr><td style="width:38%;padding-left:10px;">Total Qty</td><td style="width:70%;padding-left:10px;">'+totalQuantity+'</td></tr></table><table style="width:100%;color:#13414c"><tr><td style="width:70%;padding-left:10px;font-weight:bold;">Sub Total</td><td style="font-weight:bold;">'+subTotal+'</td></tr>',
    itemFooter:'<div style="background:#445f7d;padding:10px 0;"><table style="width:100%;color:white"><tr><td style="width:70%;padding-left:10px;"><table style="color:white"><tr><td style="font-weight:bold;">Round Off</td></tr><tr><td style="font-weight:bold;">Grand Total</td></tr></table></td><td><table style="color:white"><tr><td style="font-weight:bold;">'+roundOff+'</td></tr><tr><td style="font-weight:bold;">'+grandTotal+'</td></tr></table></td></tr> </table></div>',
    discount :'<tr>'+'<td style="width:70%;padding-left:10px;font-weight:bold;">Discount</td>'+'<td style="font-weight:bold;">'+req.body.billObject.DAmt+'</td>'+'</tr>'
  }

  var body = {
    from: 'Posist',
    to: req.body.email,
    messageText: 'Posist Bill #'+req.body.billObject.BN,

    messageHtml:(((req.body.tenant_id=='56c0313be4788e4666461813'))?makeHtmlTemplateNukkad(req,obj):makeHtmlTemplate(req,obj))
  };
  console.log(req.body);
 // res.send(200,{body:body.messageHtml});
  //emailSend(body);
  server.send({
    text: body.messageText,
    from: "Posist Info <no-reply@posist.info>",
    to: body.to,
    subject: body.messageText,
    attachment: [
      {data: body.messageHtml, alternative: true}
    ]
  }, function (err, message) {
    res.send(200,{body:body.messageHtml});
    /*console.log(message);*/
    console.log(err);
    //res.json(201, err || message);
  });

}
var server = email.server.connect({
  user: "posist",
  password: "pos123ist",
  host: "smtp.sendgrid.net",
  ssl: true
});
function emailSend( body) {
  //console.log(req.body);
  server.send({
    text: body.messageText,
    from: "Posist Info <no-reply@posist.info>",
    to: body.to,
    subject: body.messageText,
    attachment: [
      {data: body.messageHtml, alternative: true}
    ]
  }, function (err, message) {
    res.send(200,{body:body.messageHtml});
    /*console.log(message);*/
    console.log(err);
    //res.json(201, err || message);
  });
}

function makeItemsHtml(req) {
  var subTotal = 0;
  var totalQuantity = 0;
  var roundOff = 0;
  var discount = (req.body.billObject.DAmt=='')?0:parseFloat(req.body.billObject.DAmt);
  for (var i = 0; i < req.body.items.length; i++) {
    subTotal += req.body.items[i].R*req.body.items[i].Q;
    totalQuantity += req.body.items[i].Q;
  }
  var grandTotal = 0.00;
  grandTotal = parseFloat(subTotal);
  for (var i = 0; i < req.body.taxes.length; i++) {
    grandTotal += parseFloat(req.body.taxes[i].A);
  }

  grandTotal-=parseFloat(discount);
  roundOff =utils.roundNumber(  utils.roundNumber(grandTotal,0)- grandTotal,2);
  grandTotal=utils.roundNumber(grandTotal,0);
  var itemsHtml = '<div style="padding:10px 0;text-align:center"><table style="width:100%;font-weight:bold;border-collapse: collapse; border:1px solid grey; padding:10px;" cellpadding="6">';
  itemsHtml+='<tr><th style="text-align:left;border:1px solid grey">Item</th><th style="border:1px solid grey;background-color: #b3b3b3;">Qty.</th><th style="border:1px solid grey;background-color: #e6e6e6;">Amount</th></tr>'
  for (var i = 0; i < req.body.items.length; i++) {
    itemsHtml += (i % 2 == 0) ? '<tr style="background:#f6f6f6;">' : '<tr style="background:#ffffff;">';
    itemsHtml += '<td style="border:1px solid grey;text-align:left;">' + req.body.items[i].N + '</td>';
    itemsHtml += '<td style="text-align:center;border:1px solid grey;background-color: #b3b3b3;">' + req.body.items[i].Q + '</td>';
    itemsHtml += '<td style="text-align:center;border:1px solid grey;background-color: #e6e6e6;">' + req.body.items[i].R + '</td>';
    itemsHtml += '</tr>';
  }
  itemsHtml+='<tr style="background:#ffffff;"><td style="border:1px solid grey;text-align:left;">Total Qty</td>';
  itemsHtml+='<td style="text-align:center;border:1px solid grey;background-color: #b3b3b3;">'+totalQuantity+'</td><td style="text-align:center;border:1px solid grey;background-color: #e6e6e6;"></td></tr>';

  itemsHtml+='<tr><td style="border:1px solid grey;background-color: #e6e6e6;text-align:left;"><span>Sub Total</span><br /><span>Discount</span><br />'
  for (var i = 0; i < req.body.taxes.length; i++) {
    itemsHtml += '<span>' + req.body.taxes[i].N + '</span><br />';
  }
  itemsHtml+='</td><td style="border:1px solid grey;background-color: #b3b3b3;"></td><td style="text-align:center;border:1px solid grey;background-color: #e6e6e6;"><span>'+subTotal+'</span><br /><span>'+discount+'</span><br />'
  for (var i = 0; i < req.body.taxes.length; i++) {
    itemsHtml += '<span>' + req.body.taxes[i].A + '</span><br />';
  }
  itemsHtml+='</td></tr>';

  itemsHtml += '</table>';
  itemsHtml += '<table style="width:100%;font-weight:bold;border-bottom:1px solid grey;" cellpadding="6">';
  itemsHtml+='<tr><td style="text-align:left;"><span>Round Off</span><br /><span>Grand Total</span></td><td></td>';
  itemsHtml+='<td style="text-align:center;"><span>'+roundOff+'</span><br /><span>'+grandTotal+'</span></td></tr></table></div>';
  return itemsHtml;
}

function makeFooter(req){
  console.log(req.body.billObject.Ft.split("\n"))
  var footer = '<div style="text-align:center; padding:5px"><table style="margin:auto;"><tr>';
  for (var i = 0; i < req.body.billObject.Ft.split("\n").length-2; i++) {
    footer+='<td style="text-align:center;font-weight:bold; padding:5px;word-wrap: break-word;">'+req.body.billObject.Ft.split("\n")[i]+'</td>';
  }
  footer+='</tr></table>';
  footer+='<div style="text-align:center;font-weight:bold; padding:5px;word-wrap: break-word;">'+req.body.billObject.Ft.split("\n")[req.body.billObject.Ft.split("\n").length-1]+'</div></div>'
  return footer;
}
function makeHeader(req){
  var header = '<div style="text-align:center"><table style="width:100%; border-bottom:1px solid #404040;"> <tr>';
  header+='<td><img src="http://admad.mobi/flash/nukkadLogo.jpg" style="width:120px;height:120px;"></td>';
  header+='<td style="font-weight:bold;font-size:20px;font-weight:bold;text-align:left;">';
  for(var i=0;i<req.body.billObject.Hd.split("\n").length;i++){
    header+='<span>'+req.body.billObject.Hd.split("\n")[i]+'</span><br />';
  }
  header+='</td>';
  header+='<td style="border-left:1px solid #404040;padding:5px;font-weight:bold;background-color: #e6e6e6; text-align:left;"><span style="font-family:arial;font-size:23px;">Bill No. : '+req.body.billObject.BN+'</span><br />';
  header+='<span>Day: '+moment(req.body.billObject.BD).format('dddd')+'</span><br />';
  header+='<span>Date: '+moment(req.body.billObject.BD).format('DD-MM-YYYY')+'</span><br />';
  header+='<span>Time: '+moment(req.body.billObject.BD).format("hh:mm:ss a")+'</span><br />';
  if(req.body.billObject.RPT)
    header+='<span>Reprint Time: '+moment(req.body.billObject.RPT).format("hh:mm:ss a")+'</span><br />';
  header+='</td>';
  header+='<tr></table></div>'
  return header;
}
function makeSubheader(req){
  var header = '<div style="padding:10px 0;text-align:center"><table style="width:100%;color:#404040"><tr>';
  header+='<td style="text-align:left"><span>Name: '+req.body.billObject.Name+'</span><br /><span>Mobile: '+req.body.billObject.MNm+'</span></td>';
  header+='<td style="text-align:left"><span>Address: </span><br /><span>'+req.body.billObject.Add+', '+'</span><br /><span>'+req.body.billObject.St+', '+'</span><br /><span>'+req.body.billObject.Cy+', '+'</span><br /><span>'+req.body.billObject.Sta+', '+'</span><br /><span>'+req.body.billObject.Pin+'</span></td>';
  header+='</tr></table></div>'
  return header;
}
function makeHtmlTemplate(req,obj) {

  var htmlContent = '<div style="margin:2%;border:1px solid black;">';
  htmlContent+=makeHeader(req);
  htmlContent+=makeSubheader(req);
  htmlContent+=makeItemsHtml(req);
  htmlContent+=makeFooter(req);
  console.log(htmlContent)

  htmlContent+='</div>';
  return htmlContent;
}

// Send Bill to Customer
// exports.sendBillToCustomer = function(req, res) {
//   // console.log(req.body)
//   var subTotal = 0;
//   var totalQuantity = 0;
//   var roundOff = 0;
//   var discount = (req.body.billObject.DAmt=='')?0:parseFloat(req.body.billObject.DAmt);
//   var headers = req.body.billObject.Hd.split("\n")
//   var footers = req.body.billObject.Ft.split("\n")
//   for (var i = 0; i < req.body.items.length; i++) {
//     subTotal += req.body.items[i].R*req.body.items[i].Q;
//     totalQuantity += req.body.items[i].Q;
//   }
//   var grandTotal = 0.00;
//   grandTotal = parseFloat(subTotal);
//   for (var i = 0; i < req.body.taxes.length; i++) {
//     grandTotal += parseFloat(req.body.taxes[i].A);
//   }

//   grandTotal-=parseFloat(discount);
//   roundOff =utils.roundNumber(  utils.roundNumber(grandTotal,0)- grandTotal,2);
//   grandTotal=utils.roundNumber(grandTotal,0);
//   var obj = {
//     header : '<div style="padding:10px 0;text-align:center"><table style="width:100%"> <tr><td style="font-family:arial; color:white;font-size:30px;font-weight:bold">'+headers[0]+'</td><tr></table></div>',
//     subHeader : '<div style="background-color:#ebeef0;"><table style="width:100%"><tr><td style="font-family:arial; color:red; padding-left:10px;width:75%;text-align:center">',
//     subHeader1 : '<td style="background-color:#22a753;color:white;padding:5px"><span style="font-family:arial; color:white;font-size:23px;font-weight:bold">Bill No. : '+req.body.billObject.BN+'</span><br /><span style="font-family:arial; color:white;font-size:13px;">'+moment(req.body.billObject.BD).format("dddd, MMMM Do YYYY, h:mm:ss a")+'</span></td></tr></table></div>',
//     subHeader2 : '<tr><td style="font-family:arial; color:white;font-size:13px;padding-left:10px;">Customer Information:</td><tr></table></div>',
//     footer : '<div style="padding:20px 5px"><table style="width:100%">',
//     customerHeader : '<div>Customer Details:</div>',
//     customerNameMobile : '<table style="width:100%;padding:10px;"><tr><td style="width:70%"><table><tr><td style="font-family:arial; color:#13414c; font-weight:bold;padding-left:10px;">Name: '+req.body.billObject.Name+'</td></tr><tr><td style="font-family:arial; color:#13414c; font-weight:bold;padding-left:10px;">Mobile: '+req.body.billObject.MNm+'</td></tr></table></td>',
//     address1 : req.body.billObject.Add+', ',
//     address2 : req.body.billObject.St+', ',
//     city : req.body.billObject.Cy+', ',
//     state : req.body.billObject.Sta+', ',
//     pin : req.body.billObject.Pin,
//     billDeatils : '<div>Bill Number: '+req.body.billObject.BN+'</div>'+'<div>Steward: '+req.body.billObject.Waiter+'</div>'+'<div>Date: '+moment(req.body.billObject.BD).format("dddd, MMMM Do YYYY, h:mm:ss a")+'</div>',
//     Type : '<div>Type:'+req.body.billObject.Tp+'&nbsp;&nbsp;TableNo.: '+req.body.billObject.TN,
//     itemHeader :'<div><table cellspacing="0" cellpadding="0" style="border:none;width:100%;font-weight:bold;color:white;"><tr><td style="padding:5px;width:40%;background:#008bd2;padding-left:10px;">Item</td><td style="width:30%;background:#008bd2;">Qty.</td><td style="width:10%;background:#008bd2;">Amount</td><td style="width:20%;background:#008bd2;"></td></tr></table></div>',
//     qtySubTotal:'<div style="background:#ebeef0;padding:10px 0;"><table style="width:100%;color:#13414c"><tr><td style="width:38%;padding-left:10px;">Total Qty</td><td style="width:70%;padding-left:10px;">'+totalQuantity+'</td></tr></table><table style="width:100%;color:#13414c"><tr><td style="width:70%;padding-left:10px;font-weight:bold;">Sub Total</td><td style="font-weight:bold;">'+subTotal+'</td></tr>',
//     itemFooter:'<div style="background:#445f7d;padding:10px 0;"><table style="width:100%;color:white"><tr><td style="width:70%;padding-left:10px;"><table style="color:white"><tr><td style="font-weight:bold;">Round Off</td></tr><tr><td style="font-weight:bold;">Grand Total</td></tr></table></td><td><table style="color:white"><tr><td style="font-weight:bold;">'+roundOff+'</td></tr><tr><td style="font-weight:bold;">'+grandTotal+'</td></tr></table></td></tr> </table></div>',
//     discount :'<tr>'+'<td style="width:70%;padding-left:10px;font-weight:bold;">Discount</td>'+'<td style="font-weight:bold;">'+req.body.billObject.DAmt+'</td>'+'</tr>'

//   }



//   /* var email = {
//    from: 'Posist',
//    to: req.body.billObject.EML,
//    subject: 'Posist Bill #'+req.body.billObject.BN,
//    html:makeHtmlTemplate()

//    };
//    var temp = makeHtmlTemplate()
//    console.log(temp)
//    mailer.sendMail(email, function(err, info) {
//    if (err) {
//    console.log(err)
//    }
//    console.log(info);
//    res.send(info);
//    });
//    }*/
//   console.log( req.body.email);
//   var body = {
//     from: 'Posist',
//     to: req.body.email,
//     messageText: 'Posist Bill #'+req.body.billObject.BN,
//     messageHtml:makeHtmlTemplate(req,obj)

//   };
//   emailSend(body);
//   res.send(200,{status:'send'})
// }
// var server = email.server.connect({
//   user: "posist",
//   password: "pos123ist",
//   host: "smtp.sendgrid.net",
//   ssl: true
// });
// function emailSend( body) {
//   //console.log(req.body);
//   server.send({
//     text: body.messageText,
//     from: "Posist Info <no-reply@posist.info>",
//     to: body.to,
//     subject: body.messageText,
//     attachment: [
//       {data: body.messageHtml, alternative: true}
//     ]
//   }, function (err, message) {
//     /*console.log(message);*/
//     console.log(err);
//     //res.json(201, err || message);
//   });
// }

// function makeItemsHtml(req) {
//   var itemsHtml = '<table cellspacing="0" cellpadding="0" style="border:none;width:100%;color:#13414c">';
//   for (var i = 0; i < req.body.items.length; i++) {
//     itemsHtml += (i % 2 == 0) ? '<tr style="background:#f6f6f6;">' : '<tr style="background:#ffffff;">';
//     itemsHtml += '<td style="padding:5px;width:40%;padding-left:10px;">' + req.body.items[i].N + '</td>';
//     itemsHtml += '<td style="width:30%">' + req.body.items[i].Q + '</td>';
//     itemsHtml += '<td style="width:10%">' + req.body.items[i].R + '</td>';
//     itemsHtml += '<td style="width:20%"></td>';
//     itemsHtml += '</tr>';
//   }
//   itemsHtml += '</table>';
//   return itemsHtml;
// }
// function makeTaxHtml(req) {
//   var itemsHtml = '';
//   for (var i = 0; i < req.body.taxes.length; i++) {
//     itemsHtml += '<tr>';
//     itemsHtml += '<td style="width:70%;padding-left:10px;font-weight:bold;">' + req.body.taxes[i].N + '</td>';
//     itemsHtml += '<td style="font-weight:bold;">' + req.body.taxes[i].A + '</td>';
//     itemsHtml += '</tr>';
//   }
//   itemsHtml += '</table>';
//   return itemsHtml;
// }

// function makeHtmlTemplate(req,obj) {
//   var headers = req.body.billObject.Hd.split("\n")
//   var footers = req.body.billObject.Ft.split("\n")
//   var htmlContent = '<div style="margin:2%;padding:2%;background-image:url(http://admad.mobi/flash/pattern-img.jpg);">';
//   htmlContent+=obj.header;
//   htmlContent+='<div style="background-color:white;padding:0px">';
//   htmlContent+=obj.subHeader;
//   //htmlContent+=obj.subHeader1;
//   for(var i=1;i<headers.length;i++){
//     htmlContent+='<span style="font-size:18px;font-weight:bold;">'+headers[i]+'</span><br />'
//   }
//   htmlContent+='</td>';
//   htmlContent+=obj.subHeader1;
//   htmlContent+=obj.customerNameMobile;
//   htmlContent+='<td style="width:50%"><table><tr><td style="font-family:arial; color:#13414c; font-weight:bold;">Address:</td></tr>';
//   htmlContent+='<tr><td style="font-family:arial; color:#13414c;font-size:13px">'+req.body.billObject.Add+', '+ req.body.billObject.St+',</td></tr>';
//   htmlContent+='<tr><td style="font-family:arial; color:#13414c;font-size:13px">'+req.body.billObject.Cy+',</td></tr>';
//   htmlContent+='<tr><td style="font-family:arial; color:#13414c;font-size:13px">'+req.body.billObject.Sta +' - '+ req.body.billObject.Pin+'</td></tr></table></td></tr></table></div>';
//   htmlContent+=obj.itemHeader;
//   htmlContent+=makeItemsHtml(req);
//   htmlContent+=obj.qtySubTotal;
//   if(req.body.billObject.DAmt!="")
//     htmlContent+=obj.discount;
//   htmlContent+=makeTaxHtml(req);
//   htmlContent+=obj.itemFooter;
//   if(req.body.billObject.Ft != "" && footers.length>0){
//     htmlContent+=obj.footer;
//     for(var i=0;i<footers.length;i++){
//       if(i%3==0)
//         htmlContent+='<tr>'
//       htmlContent+='<th style="width:33%;color:#ff7800">'+footers[i]+'</th>'
//       if((i+1)%3==0)
//         htmlContent+='</tr>'
//     }
//     htmlContent+='</table></div>';
//   }
//   htmlContent+='</div></div>';
//   return htmlContent;
// }

exports.sendSms = function(req, res) {
  //return
  /*  console.log("hey", req.body)
   var username = 'posistapi';
   var password = 'del12345';
   var Senderid = 'POSIST';
   var options = {
   host: '203.212.70.200',
   method: 'GET',
   path: '/smpp/sendsms?username='+username+'&password='+password+'&from='+Senderid+'&to='+req.body.mobile+'&text='+req.body.message
   };

   http.get('http://203.212.70.200/smpp/sendsms?username=posistapi&password=del12345&from=POSIST&to='+req.body.mobile+'&text=Thankyou! for your valuable feedback. :)', function(resp){
   resp.on('data', function(chunk){
   //console.log('chunk')
   // console.log(chunk)//do something with chunk
   });
   }).on("error", function(e){
   console.log("Got error: " + e.message);
   });*/
  // var template='Dear '++' Thanks for choosing us. Your bill amount is Rs %s.Bill Amount - %sDiscount - %sTax - %sAmount to pay : %d';
  var url = "http://sms.technoapex.in/sendurlcomma.aspx?user=20063804&pwd=xzi2zu&senderid=POSIST&mobileno=%mobile%&msgtext=%msg%";
  // var url="http://203.212.70.200/smpp/sendsms?username=posistapi&password=del12345&to=%mobile%&from=POSIST&text=%msg%";
  //var template = smsTemplate.content;
  /*   template = replaceAll('%name%', ' ' + name + ',\r\n', template);
   template = replaceAll('%date%', ':' + getFormattedDate(moment(resetDate).toDate()) + ' ', template);
   template = replaceAll('%amount%', ' ' + '' + amountContent + '\r\n Thank', template);*/
  /*url = replaceAll('%msg%', template, url);
   console.log(url);*/
  url = replaceAll('%mobile%', req.body.mobile, url);
  url = replaceAll('%msg%', req.body.message, url);
  /*request(url, function (error, response, body) {
   //utils.sendSMSExotel(mobile,template).then(function(body){
   console.log(response);
   return  res.send(200,{status:response});
   });*/
  request.post("https://posist:ad3024355ae9bdc0024bf34bbe7e35c393bb3c58@twilix.exotel.in/v1/Accounts/posist/Sms/send",
    // {form:   req.query.body}
    {form:{"From":"08010133399","To":req.body.mobile,"Body":req.body.message}
    }, function(error, response, body){
      // console.log(body);
    });
  return  res.send(200,{status:"done"});
}
function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}

function makeItemsHtmlNukkad(req) {

  var subTotal = 0;
  var totalQuantity = 0;
  var roundOff = 0;
  var discount = (req.body.billObject.DAmt=='')?0:parseFloat(req.body.billObject.DAmt);
  for (var i = 0; i < req.body.items.length; i++) {
    subTotal += req.body.items[i].R*req.body.items[i].Q;
    totalQuantity += req.body.items[i].Q;
  }
  var grandTotal = 0.00;
  grandTotal = parseFloat(subTotal);
  for (var i = 0; i < req.body.taxes.length; i++) {
    grandTotal += parseFloat(req.body.taxes[i].A);
  }

  grandTotal-=parseFloat(discount);
  roundOff =utils.roundNumber(  utils.roundNumber(grandTotal,0)- grandTotal,2);
  grandTotal=utils.roundNumber(grandTotal,0);
  var itemsHtml = '<div style="padding:10px 0;text-align:center"><table style="width:100%;font-weight:bold;border-collapse: collapse; border:1px solid grey; padding:10px;" cellpadding="6">';
  itemsHtml+='<tr><th style="text-align:left;border:1px solid grey">Item</th><th style="border:1px solid grey;background-color: #b3b3b3;">Qty.</th><th style="border:1px solid grey;background-color: #e6e6e6;">Amount</th></tr>'
  for (var i = 0; i < req.body.items.length; i++) {
    itemsHtml += (i % 2 == 0) ? '<tr style="background:#f6f6f6;">' : '<tr style="background:#ffffff;">';
    itemsHtml += '<td style="border:1px solid grey;text-align:left;">' + req.body.items[i].N + '</td>';
    itemsHtml += '<td style="text-align:center;border:1px solid grey;background-color: #b3b3b3;">' + req.body.items[i].Q + '</td>';
    itemsHtml += '<td style="text-align:center;border:1px solid grey;background-color: #e6e6e6;">' + req.body.items[i].R + '</td>';
    itemsHtml += '</tr>';
  }
  itemsHtml+='<tr style="background:#ffffff;"><td style="border:1px solid grey;text-align:left;">Total Qty</td>';
  itemsHtml+='<td style="text-align:center;border:1px solid grey;background-color: #b3b3b3;">'+totalQuantity+'</td><td style="text-align:center;border:1px solid grey;background-color: #e6e6e6;"></td></tr>';

  itemsHtml+='<tr><td style="border:1px solid grey;background-color: #e6e6e6;text-align:left;"><span>Sub Total</span><br /><span>Discount</span><br />'
  for (var i = 0; i < req.body.taxes.length; i++) {
    itemsHtml += '<span>' + req.body.taxes[i].N + '</span><br />';
  }
  itemsHtml+='</td><td style="border:1px solid grey;background-color: #b3b3b3;"></td><td style="text-align:center;border:1px solid grey;background-color: #e6e6e6;"><span>'+subTotal+'</span><br /><span>'+discount+'</span><br />'
  for (var i = 0; i < req.body.taxes.length; i++) {
    itemsHtml += '<span>' + req.body.taxes[i].A + '</span><br />';
  }
  itemsHtml+='</td></tr>';

  itemsHtml += '</table>';
  itemsHtml += '<table style="width:100%;font-weight:bold;border-bottom:1px solid grey;" cellpadding="6">';
  itemsHtml+='<tr><td style="text-align:left;"><span>Round Off</span><br /><span>Grand Total</span></td><td></td>';
  itemsHtml+='<td style="text-align:center;"><span>'+roundOff+'</span><br /><span>'+grandTotal+'</span></td></tr></table></div>';
  return itemsHtml;
}

function makeFooterNukkad(req){
  console.log(req.body.billObject.Ft.split("\n"))
  var footer = '<div style="text-align:center; padding:5px"><table style="margin:auto;"><tr>';
  for (var i = 0; i < req.body.billObject.Ft.split("\n").length-2; i++) {
    footer+='<td style="text-align:center;font-weight:bold; padding:5px;word-wrap: break-word;">'+req.body.billObject.Ft.split("\n")[i]+'</td>';
  }
  footer+='</tr></table>';
  footer+='<div style="text-align:center;font-weight:bold; padding:5px;word-wrap: break-word;">'+req.body.billObject.Ft.split("\n")[req.body.billObject.Ft.split("\n").length-1]+'</div></div>'

  return footer;
}
function makeHeaderNukkad(req){
  var header = '<div style="text-align:center"><table style="width:100%; border-bottom:1px solid #404040;"> <tr>';
  header+='<td><img src="http://admad.mobi/flash/nukkadLogo.jpg" style="width:120px;height:120px;"></td>';
  header+='<td style="font-weight:bold;font-size:20px;font-weight:bold;text-align:left;">';
  for(var i=0;i<req.body.billObject.Hd.split("\n").length;i++){
    header+='<span>'+req.body.billObject.Hd.split("\n")[i]+'</span><br />';
  }
  header+='</td>';
  header+='<td style="border-left:1px solid #404040;padding:5px;font-weight:bold;background-color: #e6e6e6; text-align:left;"><span style="font-family:arial;font-size:23px;">Bill No. : '+req.body.billObject.BN+'</span><br />';
  header+='<span>Day: '+moment(req.body.billObject.BD).format('dddd')+'</span><br />';
  header+='<span>Date: '+moment(req.body.billObject.BD).format('DD-MM-YYYY')+'</span><br />';
  header+='<span>Time: '+moment(req.body.billObject.BD).format("hh:mm:ss a")+'</span><br />';
  if(req.body.billObject.RPT)

    header+='<span>Reprint Time: '+moment(req.body.billObject.RPT).format("hh:mm:ss a")+'</span><br />';

  header+='</td>';
  header+='<tr></table></div>'
  return header;
}
function makeSubheaderNukkad(req){
  var header = '<div style="padding:10px 0;text-align:center"><table style="width:100%;color:#404040"><tr>';
  header+='<td style="text-align:left"><span>Name: '+req.body.billObject.Name+'</span><br /><span>Mobile: '+req.body.billObject.MNm+'</span></td>';
  header+='<td style="text-align:left"><span>Address: </span><br /><span>'+req.body.billObject.Add+', '+'</span><br /><span>'+req.body.billObject.St+', '+'</span><br /><span>'+req.body.billObject.Cy+', '+'</span><br /><span>'+req.body.billObject.Sta+', '+'</span><br /><span>'+req.body.billObject.Pin+'</span></td>';
  header+='</tr></table></div>'
  return header;
}
function makeHtmlTemplateNukkad(req,obj) {

  var htmlContent = '<div style="margin:2%;border:1px solid black;">';
  htmlContent+=makeHeaderNukkad(req);
  htmlContent+=makeSubheaderNukkad(req);
  htmlContent+=makeItemsHtmlNukkad(req);
  htmlContent+=makeFooterNukkad(req);
  console.log(htmlContent)

  htmlContent+='</div>';
  return htmlContent;
}
function handleError(res, err) {
  return res.send(500, err);
}
