/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Charges = require('./charges.model');

exports.register = function(socket) {
  Charges.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Charges.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('charges:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('charges:remove', doc);
}