'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ChargesSchema = new Schema({
  deployment_id:String,
  tenant_id:String,
  name: String,
  value:Number,
  isDeleted:Boolean,
  tabs:[],
  type:String
});

module.exports = mongoose.model('Charges', ChargesSchema);
