'use strict';

var _ = require('lodash');
var Charges = require('./charges.model');
var Tab=require('../tab/tab.model');

// Get list of chargess
exports.index = function(req, res) {
  Charges.find({deployment_id:req.query.deployment_id,isDeleted:{$ne:true}},function (err, chargess) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(chargess);
  });
};

// Get a single charges
exports.show = function(req, res) {
  Charges.findById(req.params.id, function (err, charges) {
    if(err) { return handleError(res, err); }
    if(!charges) { return res.status(404).send('Not Found'); }
    return res.json(charges);
  });
};

// Creates a new charges in the DB.
exports.create = function(req, res) {
  Charges.create(req.body, function(err, charges) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(charges);
  });
};

// Updates an existing charges in the DB.
exports.update = function(req, res) {
  console.log(req.body);
  Charges.findOneAndUpdate({deployment_id:req.body.deployment_id,_id:req.body._id},req.body,{new:true},function(err,doc){
    if(doc)  res.status(200).send({status:'success'});
    else return res.status(500).send(err);
  })
};

// Deletes a charges from the DB.
exports.destroy = function(req, res) {
 Charges.findOneAndUpdate({deployment_id:req.body.deployment_id,_id:req.body._id},req.body,{new:true},function(err,doc){
    if(doc) res.status(200).send({status:'success'});
    else return res.status(500).send(err);
  })
};

/*function updateChargeInTabs(req,res){
  Tab.update({deployment_id:req.body.deployment_id, "charges._id":req.body._id},{$set:{'charges.$.amount':req.body.amount}}, {multi:true},function(err,numAffected){
      console.log(err,numAffected)
      if(numAffected>=0)  return res.status(200).send({status:'success'});
      else return handleError(res,{status:'error'});
    });
}

function deleteChargeInTabs(req,res){
  Tab.update({deployment_id:req.body.deployment_id},{$pull: {charges: {_id: req.body._id}}}, {multi: true},function(err,numAffected){
    console.log(err,numAffected);
    if(numAffected>=0) res.status(200).send({status:'success'});
    else return res.status(500).send({status:'error'});
  });
}*/

exports.assignChargesToTab=function(req,res){
   Charges.update({deployment_id:req.body.deployment_id,isDeleted:{$ne:true}},{$pull: {tabs: {_id: req.body.tab._id}}}, {multi: true},function(err,numAffected){
    console.log(err,numAffected);
    if(numAffected>=0){
      Charges.update({deployment_id:req.body.deployment_id,_id:{$in:req.body.list},isDeleted:{$ne:true}},{$push: {tabs: req.body.tab}}, {multi: true},function(err,num){
       console.log(err,num);
       if(num>=0) return res.status(200).send({status:'success'});
       else return handleError(res,{status:'error'});
     });
    }
    else return res.status(500).send({status:'error'});
  });
}

exports.applyCharges=function(req,res){
  Charges.findOneAndUpdate({deployment_id:req.body.deployment_id,_id:req.body._id},{$set:{tabs:req.body.tabs}},{new:true},function(err,doc){
    if(doc) res.status(200).send({status:'success'});
    else return handleError(res,{status:'error'});
  })
}
exports.orderingAppCharges = function(req, res) {
  Charges.find({deployment_id:req.query.deployment_id,isDeleted:{$ne:true},tabs:{$elemMatch:{tabType:req.query.tabtype}}},{name:1,type:1,value:1},function (err, chargess) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(chargess);
  });
};


exports.orderingAppCharges = function(req, res) {
  console.log(req.query,'query');
  Charges.find({deployment_id:req.query.deployment_id,isDeleted:{$ne:true},tabs:{$elemMatch:{tabType:req.query.tabtype}}},{name:1,type:1,value:1},function (err, chargess) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(chargess);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
