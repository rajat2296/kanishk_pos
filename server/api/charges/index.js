'use strict';

var express = require('express');
var controller = require('./charges.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/orderingAppCharges',controller.orderingAppCharges);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/update', controller.update);
router.post('/delete',controller.destroy);
router.post('/assignCharges',controller.assignChargesToTab);
router.post('/applyCharges',controller.applyCharges);
module.exports = router;
