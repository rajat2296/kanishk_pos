/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockRecipe = require('./stockRecipe.model');

exports.register = function(socket) {
  StockRecipe.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockRecipe.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockRecipe:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockRecipe:remove', doc);
}