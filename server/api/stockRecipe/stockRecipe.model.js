'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockRecipeSchema = new Schema({
  recipeName: String,
  itemId:String,
  quantity:Number,
  categoryId:String,
  selectedUnitId:{},
  receipeDetails:[],
  rawItems:[],
  kitchenDetails:{},
  isSemiProcessed:{type:Boolean,default:false},
  menuQty:Number,
  recipeQty:Number,
  unit:{},//menu selected unit
  units:[],
  tenant_id: Schema.Types.ObjectId,
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  active: Boolean,
  lastUpdated: {
    date: Date,
    username: String
  },
},{versionKey:false});

StockRecipeSchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});

module.exports = mongoose.model('StockRecipe', StockRecipeSchema);