'use strict';

var _ = require('lodash');
var StockRecipe = require('./stockRecipe.model');
var ObjectId = require('mongoose').Types.ObjectId;
var Deployment = require('../deployment/deployment.model');
// Get list of stockRecipes
exports.index = function(req, res) {
  var projection = {};
  if(req.query.projection){
    projection = JSON.parse(req.query.projection);
  }
  var tenant_idString=req.query.tenant_id;
  var tenant_idObj=new ObjectId(req.query.tenant_id)
  var deployment_idString=req.query.deployment_id;
  var deployment_idObj=new ObjectId(req.query.deployment_id)
  //console.log('projection', projection);
  StockRecipe.find( { $and:[{ $or:[{"tenant_id":tenant_idString} , {"tenant_id":tenant_idObj} ] },{$or:[{"deployment_id":deployment_idString} , {"deployment_id":deployment_idObj} ]} ]}, projection, function (err, stockRecipes) {
    if(err) {
      console.log(err)
      return handleError(res, err); }
    // console.log("returning", stockRecipes)
    return res.json(200, stockRecipes);
  });
};

// Get a single stockRecipe
exports.show = function(req, res) {
  StockRecipe.findById(req.params.id, function (err, stockRecipe) {
    if(err) { return handleError(res, err); }
    if(!stockRecipe) { return res.send(404); }
    return res.json(stockRecipe);
  });
};

// Creates a new stockRecipe in the DB.
exports.create = function(req, res) {
  StockRecipe.create(req.body, function(err, stockRecipe) {
    if(err) { return handleError(res, err); }
    console.log(stockRecipe._id);
    console.log(typeof stockRecipe._id);
    console.log(stockRecipe.tenant_id);
    console.log(typeof stockRecipe.tenant_id);
    return res.json(201, stockRecipe);
  });
};

// Updates an existing stockRecipe in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  StockRecipe.findById(req.params.id, function (err, stockRecipe) {
    if (err) { return handleError(res, err); }
    if(!stockRecipe) { return res.send(404); }
    //var updated = _.merge(stockRecipe, req.body);
    var updated=_.extend(stockRecipe, req.body);
    updated.markModified("receipeDetails");
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, stockRecipe);
    });
  });
};

// Deletes a stockRecipe from the DB.
exports.destroy = function(req, res) {
  StockRecipe.findById(req.params.id, function (err, stockRecipe) {
    if(err) { return handleError(res, err); }
    if(!stockRecipe) { return res.send(404); }
    stockRecipe.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

// Get list of stockRecipes Semi-Processed
exports.getSemiProcessedRecipes = function(req, res) {
  StockRecipe.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id,isSemiProcessed:true}, function (err, stockRecipes) {
    if(err) { return handleError(res, err); }
    return res.json(200, stockRecipes);
  });
};
// Get list of stockRecipes Processed
exports.getProcessedRecipes = function(req, res) {
  StockRecipe.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id,isSemiProcessed:false}, function (err, stockRecipes) {
    if(err) { return handleError(res, err); }
    return res.json(200, stockRecipes);
  });
};

exports.findByItemId = function (req, res) {
  var itemId = req.query.itemId;
  StockRecipe.findOne({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id, itemId: itemId}, function (err, stockRecipe) {
    if(err) { return handleError(res, err); }
    if(!stockRecipe)
      return res.status(200).send({err: "NotFound"});
    return res.json(200, stockRecipe);
  });
}

exports.findByItemIdNew = function (req, res) {
  var itemId = req.query.itemId;
  StockRecipe.findOne({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id, itemId: itemId}, {"selectedUnit.baseUnit": 1}, function (err, stockRecipe) {
    if(err) { return handleError(res, err); }
    if(!stockRecipe)
      return res.status(200).send({err: "NotFound"});
    return res.json(200, stockRecipe);
  });
}

exports.findByRecipeItemId = function (req, res) {
  var itemId = req.query.itemId;
  StockRecipe.find({tenant_id: req.query.tenant_id, "receipeDetails.itemId": itemId}, {deployment_id: 1}, function (err, stockRecipes) {
    if(err) { return handleError(res, err); }
    if(stockRecipes.length == 0)
      return res.status(200).json([]);
    var deployments = [];
    _.forEach(stockRecipes, function(recipe){
      deployments.push(recipe.deployment_id);
    });
    Deployment.find({_id: {$in: deployments}}, {name: 1}, function(err, deployments) {
      if(err)
        return handleError(res, err);
      return res.status(200).json(deployments);
    });
  });
}

exports.checkIfBaseKitchenItemInRecipe = function (req, res) {
  var itemId = req.query.itemId;
  var tenant_id = req.query.itemId;
  var deployment_id = req.query.deployment_id;

  StockRecipe.findOne({tenant_id: tenant_id, "recipeDetails.itemId": itemId}, {_id: 1}, function (err, recipe) {
    if(err)
      return handleError(res, err);
    if(!recipe)
      return res.status(200).json({recipeFound: false});
    return res.status(200).json({recipeFound: true});
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
