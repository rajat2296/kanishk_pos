'use strict';

var express = require('express');
var controller = require('./stockRecipe.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/getSemiProcessedRecipes', controller.getSemiProcessedRecipes);
router.get('/getProcessedRecipes', controller.getProcessedRecipes);
router.get('/findByItemId', controller.findByItemId);
router.get('/findByRecipeItemId', controller.findByRecipeItemId);
router.get('/checkIfBaseKitchenItemInRecipe', controller.checkIfBaseKitchenItemInRecipe);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
