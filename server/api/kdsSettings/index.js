'use strict';

var express = require('express');
var controller = require('./kdsSettings.controller');

var router = express.Router();

router.get('/colorCode', controller.index);
router.post('/colorCode', controller.create);
router.put('/colorCode', controller.update);
router.delete('/colorCode', controller.destroy);

module.exports = router;