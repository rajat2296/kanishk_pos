'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var KdsSettingsSchema = new Schema({
  deployment_id:String,
  tenant_id: String,
  lapseTime: Number,
  colorName:String,
  colorCode:String
});

module.exports = mongoose.model('KdsSettings', KdsSettingsSchema);