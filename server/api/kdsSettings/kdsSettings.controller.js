'use strict';

var _ = require('lodash');
var KdsSettings = require('./kdsSettings.model');

// Get list of kdsSettingss
exports.index = function(req, res) {
  KdsSettings.find({deployment_id:req.query.deployment_id},function (err, kdsSettingss) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(kdsSettingss);
  });
};



// Creates a new kdsSettings in the DB.
exports.create = function(req, res) {
  KdsSettings.create(req.body, function(err, kdsSettings) {
    if(err) { return handleError(res, err); }
    return res.status(200).send(kdsSettings);
  });
};

// Updates an existing kdsSettings in the DB.
exports.update = function(req, res) {
 KdsSettings.update({deployment_id:req.body.deployment_id,_id:req.body._id},req.body,function(err,num){
  if(num)
    res.status(200).send({status:'updated'});
  else
    return handleError(res,{status:'error'})
 })
};

// Deletes a kdsSettings from the DB.
exports.destroy = function(req, res) {
  KdsSettings.findOne({_id:req.query._id,deployment_id:req.query.deployment_id}, function (err, kdsSettings) {
    if(err) { return handleError(res, err); }
    if(!kdsSettings) { return res.status(404).send('Not Found'); }
    kdsSettings.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(200).send({status:'deleted'});
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}