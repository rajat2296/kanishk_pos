/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var KdsSettings = require('./kdsSettings.model');

exports.register = function(socket) {
  KdsSettings.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  KdsSettings.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('kdsSettings:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('kdsSettings:remove', doc);
}