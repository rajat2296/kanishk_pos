'use strict';

var _ = require('lodash');
var Deploymentclone = require('./deploymentclone.model');

var Item = require('../item/item.model');
var Category = require('../category/category.model');
var Supercategory = require('../superCategory/superCategory.model');
var Tab = require('../tab/tab.model');
var Station = require('../station/station.model');
var Tax = require('../tax/tax.model');
var async = require('async');

// Get list of deploymentclones
exports.index = function (req, res) {

    var _masterData = {
        stations: [],
        superCategories:[],
        categories:[],
        tabs:[],
        taxes:[],
        items:[],
        toDeploymentId: req.query.toDeploymentId,
        fromMasterId: req.query.fromMasterId
    };

/*    var _cleanMasterData = {
        stations: [],
        superCategories:[],
        categories:[],
        tabs:[],
        taxes:[],
        items:[],
        toDeploymentId: req.query.toDeploymentId,
        fromMasterId: req.query.fromMasterId
    };*/

    Station.find({deployment_id: _masterData.fromMasterId}, function (err, stations) {
        _masterData.stations = stations;
        Supercategory.find({deployment_id: _masterData.fromMasterId}, function (err, scs) {
           _masterData.superCategories = scs;
            Tab.find({deployment_id: _masterData.fromMasterId}, function (err, tabs) {
                _masterData.tabs = tabs;
                Category.find({deployment_id: _masterData.fromMasterId}, function (err, cats) {
                    _masterData.categories = cats;
                    Tax.find({deployment_id: _masterData.fromMasterId}, function (err, taxes) {
                        _masterData.taxes = taxes;
                        Item.find({deployment_id: _masterData.fromMasterId}, function (err, items) {
                            _masterData.items = items;

                            /* Fetched all Data from Master */
                            async.eachSeries(_masterData.stations, function (station, cb) {
                                // Clean Stations
                                _masterData.stations[_masterData.stations.indexOf(station)].deployment_id = _masterData.toDeploymentId;
                                cb();

                            }, function (err) {

                                // Clean Super Categories
                                async.eachSeries(_masterData.superCategories, function (sc, cb) {
                                    _masterData.superCategories[_masterData.superCategories.indexOf(sc)].deployment_id = _masterData.toDeploymentId;
                                    cb();

                                }, function () {

                                    // Clean Tabs
                                    async.eachSeries(_masterData.tabs, function (tab, cb) {
                                        _masterData.tabs[_masterData.tabs.indexOf(tab)].deployment_id = _masterData.toDeploymentId;
                                        cb();

                                    }, function () {

                                        // Clean Categories
                                        async.eachSeries(_masterData.categories, function (cat, cb) {
                                            _masterData.categories[_masterData.categories.indexOf(cat)].deployment_id = _masterData.toDeploymentId;
                                            cb();

                                        }, function () {

                                            // Clean Tax
                                            async.eachSeries(_masterData.taxes, function (tax, cb) {
                                                _masterData.taxes[_masterData.taxes.indexOf(tax)].deployment_id = _masterData.toDeploymentId;
                                                cb();

                                            }, function () {
                                                // Clean Items
                                                async.eachSeries(_masterData.items, function (item, cb) {
                                                    _masterData.items[_masterData.items.indexOf(item)].deployment_id = _masterData.toDeploymentId;
                                                    cb();

                                                }, function () {

                                                    /* All Done Save and Send Response */
                                                    res.json(200, _masterData);

                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

};

// Get a single deploymentclone
exports.show = function (req, res) {
    Deploymentclone.findById(req.params.id, function (err, deploymentclone) {
        if (err) {
            return handleError(res, err);
        }
        if (!deploymentclone) {
            return res.send(404);
        }
        return res.json(deploymentclone);
    });
};

// Creates a new deploymentclone in the DB.
exports.create = function (req, res) {
    Deploymentclone.create(req.body, function (err, deploymentclone) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, deploymentclone);
    });
};

// Updates an existing deploymentclone in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Deploymentclone.findById(req.params.id, function (err, deploymentclone) {
        if (err) {
            return handleError(res, err);
        }
        if (!deploymentclone) {
            return res.send(404);
        }
        var updated = _.merge(deploymentclone, req.body);
        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, deploymentclone);
        });
    });
};

// Deletes a deploymentclone from the DB.
exports.destroy = function (req, res) {
    Deploymentclone.findById(req.params.id, function (err, deploymentclone) {
        if (err) {
            return handleError(res, err);
        }
        if (!deploymentclone) {
            return res.send(404);
        }
        deploymentclone.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    return res.send(500, err);
}