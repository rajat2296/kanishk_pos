/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Deploymentclone = require('./deploymentclone.model');

exports.register = function(socket) {
  Deploymentclone.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Deploymentclone.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('deploymentclone:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('deploymentclone:remove', doc);
}