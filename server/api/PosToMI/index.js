'use strict';

var express = require('express');
var controller = require('./PosToMI.controller');

var router = express.Router();
router.post('/UploadTextFiles', controller.UploadTextFiles);
router.get('/scheduler', controller.scheduler);
// router.get('/', controller.index);
// router.get('/:id', controller.show);
// router.post('/', controller.create);
// router.put('/:id', controller.update);
// router.patch('/:id', controller.update);
// router.delete('/:id', controller.destroy);

module.exports = router;