'use strict';

var _ = require('lodash');
var config = require('../../config/environment');
var PosToMI = require('./PosToMI.model');
var PosToDLF = require('../PosToDLF/PosToDLF.model');
var moment = require('moment');
var path = require('path');
var fs = require('fs');
var Client = require('ftp');
var async = require('async');
var kue = require('kue');
var queue = kue.createQueue(config.redisConn);

//var connectionProperties_Local = {host: "127.0.0.1",user: "admin",password: "admin"};//localhost
var connectionProperties_Local = {host: "14.141.54.43",user: "dlfposuser",password: "dlfposuser"};//server
// Get list of PosToMIs
exports.index = function(req, res) {
  PosToMI.find(function (err, PosToMIs) {
    if(err) { return handleError(res, err); }
    return res.json(200, PosToMIs);
  });
};

// Get a single PosToMI
exports.show = function(req, res) {
  PosToMI.findById(req.params.id, function (err, PosToMI) {
    if(err) { return handleError(res, err); }
    if(!PosToMI) { return res.send(404); }
    return res.json(PosToMI);
  });
};

// Creates a new PosToMI in the DB.
exports.create = function(req, res) {
  PosToMI.create(req.body, function(err, PosToMI) {
    if(err) { return handleError(res, err); }
    return res.json(201, PosToMI);
  });
};

// Updates an existing PosToMI in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  PosToMI.findById(req.params.id, function (err, PosToMI) {
    if (err) { return handleError(res, err); }
    if(!PosToMI) { return res.send(404); }
    var updated = _.merge(PosToMI, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, PosToMI);
    });
  });
};

// Deletes a PosToMI from the DB.
exports.destroy = function(req, res) {
  PosToMI.findById(req.params.id, function (err, PosToMI) {
    if(err) { return handleError(res, err); }
    if(!PosToMI) { return res.send(404); }
    PosToMI.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

// Upload File to Server with file Name
exports.UploadTextFiles = function(req, res, next) {
  var fileName=req.body.fileName;
  var fileData=req.body.fileData;
  var c = new Client();
  var ret="";
  if(req.body.ftpSettings && req.body.ftpSettings.hostAddress){
    connectionProperties_Local = {host: req.body.ftpSettings.hostAddress,user: req.body.ftpSettings.username,password:req.body.ftpSettings.password};
  }
  try {
    c.on('ready', function () {
      c.put(fileData, fileName, function(err) {
        if (err){
          c.end();
          ret="error";
          throw new Error(err);
        }
        else{
          c.end();
          ret="Done";
        }
        console.log(connectionProperties_Local, ret);
        return !req.body.forwarded_from_Q?res.json(200, {result:ret}): next();
      });
    });

    c.connect(connectionProperties_Local);
  }catch(ex){
    console.log(ex);
    return !req.body.forwarded_from_Q?res.json(402, ex): next();
  }
};

exports.scheduler = function(req, res, next){
  PosToDLF.find({autoSync:true},function (err, PosToDLFs) {
    if(!err){
      async.eachSeries(PosToDLFs, function(recipient, callback){
        queue.create('sync_ADSR', recipient).attempts(3).removeOnComplete(false).save(function(err){
          if(err)
            callback(err, {deployment_id: recipient.deployment_id, brand: recipient.brandName});
          else
            callback(null);
        });
      }, function(err, data){
        if(err){
          console.log(data);
          res.status(500).send('error');
        }else
          res.send('done');
      });
    }else{
      res.status(500).send();
    }
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
