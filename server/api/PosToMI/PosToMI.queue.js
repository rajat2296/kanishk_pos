var posToMI = require('./PosToMI.controller');
var Bill = require('../bill/bill.model');
var Deployment = require('../deployment/deployment.model');
var moment = require('moment');
var mongoose = require('mongoose');

exports.doQueue = function(queue){
	queue.process('sync_ADSR', 4, function(job, done) {
      getTodaysBillsForADSR(job.data.deployment_id, function(err, bills){
      	if(err) done(err, []);
      	else{
      		if(bills.length>0){
		      	formatBillsForADSR(job.data, bills, function(err,file){
		      		if(err){
		      			done(err, null);
		      		}else{
			      		var req = {
			      			body: {
			      				ftpSettings: job.data.ftpSettings,
			      				fileName: file.name,
			      				fileData: file.data,
			      				forwarded_from_Q: true
			      			}
			      		};
			      		posToMI.UploadTextFiles(req, {}, function(){
			      			done(null);
			      		});
			      	}
		      	});
		    }else{
		    	console.log("no bills");
		    	done(null);
		    }
	    }
      });
    });
}

function getTodaysBillsForADSR(deployment_id, callback){
	getDateRange(deployment_id, function(err, range){
		if(err)
			callback(err);
		else{
			Bill.aggregate(

				// Pipeline
				[
					// Stage 1
					{
						$match: {
							"deployment_id": new mongoose.Types.ObjectId(deployment_id),
							"isVoid":false,
							"isDeleted": {$ne:true},
							"_created": {$gte: new Date(range.from), $lt: new Date(range.till)}
						}
					},

					// Stage 2
					{
						$unwind: "$_kots"
					},

					// Stage 3
					{
						$group: {
							"_id": "$_id",
							"serialNumber": {$first: "$serialNumber"},
							"billNumber": {$first: "$billNumber"},
							"billDiscountAmount":{$first: "$billDiscountAmount"},
							"kots": {$push: "$_kots.items"},
							"username": {$first: "$_currentUser.username"},
							"charges": {$first: "$charges"},
							"totalDiscount":{$sum: "$_kots.totalDiscount"},
							"totalAmount":{$sum: "$_kots.totalAmount"},
							"taxAmount":{$sum: "$_kots.taxAmount"},
							"payments": {$first:"$payments"},
							"_created": {$first:"$_created"}
						}
					},

					// Stage 4
					{
						$project: {
							"serialNumber": 1,
							"billNumber": 1,
							"kots": 1,
							"username": 1,
							"charges": {$ifNull:["$charges.amount",0]},
							"totalDiscount": {$add:[{$ifNull:["$billDiscountAmount", 0]}, "$totalDiscount"]},
							"totalAmount":1,
							"taxAmount":1,
							"payments": {$concatArrays: [[{
											"cardType": {$literal:"Cash"},
											"totalAmount": {$sum: "$payments.cash"}
										}], "$payments.cards"]},
							"_created":1
						}
					},

					// Stage 5
					{
						$sort: {
							"billNumber":1
						}
					}

				]

				// Created with 3T MongoChef, the GUI for MongoDB - https://3t.io/mongochef

			).exec(function(err, bills){
				if(err)
					callback(err);
				else
					callback(null, bills);
			});
		}
	});
}

function getDateRange(deployment_id, callback){
	Deployment.findOne({"_id": deployment_id,"settings.name": "day_sale_cutoff"},{"settings.$":1}).lean().exec(function(err, deployment){
		if(err)
			callback(err);
		else{
			var today = new Date();
			var yesterday = new Date((new Date(today)).setDate(today.getDate()-1));
			var hours = (new Date(deployment.settings[0].value)).getUTCHours();
			var mins = (new Date(deployment.settings[0].value)).getUTCMinutes();
			var from = (new Date(Date.UTC(yesterday.getUTCFullYear(), yesterday.getUTCMonth(), yesterday.getUTCDate(), hours, mins))).toISOString();
			var till = (new Date(Date.UTC(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate(), hours, mins))).toISOString();
			callback(null, {from: from, till: till});
		}
	}); 
}

function formatBillsForADSR(brand, bills, callback){
	var tfn = 1;
	var pos = 1001;
	var d = bills[0]._created;
	var recNo = bills[0].billNumber;
	var dateForFileName=moment(d).format('YYMMDDHHmm');
	var tfn_str="0000".substr(0,4-(String(tfn)).length)+String(tfn);
	var fileName = "t" + brand.dlfTenantId + "_" + pos + "_" + tfn_str + "_" + dateForFileName+".txt";
	var dd=moment(d).format('YYYYMMDD');
	var dd1=moment(d).format('HH:mm:ss');
	var fileData = "1|OPENED|"+brand.dlfTenantId+"|"+pos+"|"+recNo+"|"+tfn_str+"|"+dd+"|"+dd1+"|"+bills[0].username.substr(0, 8)+"|"+dd+"\r\n";
	bills.forEach(function(b){
		recNo=b.billNumber;
		dd=moment(b._created).format('YYYYMMDD');
		dd1=moment(b._created).format('HH:mm:ss');
		fileData += "101|"+b.billNumber+"|1|"+dd+"|"+dd1+"|"+b.username.substr(0,8)+"|||||||N|SALE\r\n";
		b.kots.forEach(function(kot){
			kot.forEach(function(itm){
				var rate = parseFloat(itm.rate);
				var taxcode = "N";
				var itemTax=getTaxAmount(itm);
				if (itm.taxes != undefined)
				{
					taxcode = 0;//itm.taxes.substr(0, 4);
				}
				var itemCode = itm.number;
				fileData += "111|" + itemCode + "|" + parseFloat(itm.quantity).toFixed(3) + "|" + parseFloat(rate).toFixed(2) + "|" + parseFloat(rate).toFixed(2) + "||" + taxcode + "|||SALE||";
				fileData += "|N|" + parseFloat(itm.subtotal).toFixed(2) + "||%|" + parseFloat(itemTax).toFixed(2) + "|\r\n";
			});
		});
		var grossSale=parseFloat(b.totalAmount).toFixed(2);
		var discount = parseFloat(b.totalDiscount).toFixed(2);
		var charges = parseFloat(b.charges).toFixed(2);
		//fileData += "121|" + decimal.Round(billAmount, 2) + "|" + decimal.Round(decimal.Parse(bill.Discount), 2) + "|0.00|" + decimal.Round(decimal.Parse(bill.ServiceCh), 2) + "|" + decimal.Round(decimal.Parse(bill.VatAmt), 2) + "|E|N||||#";
		fileData += "121|" + parseFloat(grossSale).toFixed(2) + "|" + discount + "|0.00|" + charges + "|" + parseFloat(b.taxAmount).toFixed(2) + "|E|N||||\r\n";
		//code 131(Footer Payment Medium)  if payment in N currency N records are created
		b.payments.forEach(function(payment){
			if(payment.totalAmount!=0)
				fileData += "131|T|" + payment.cardType + "|INR|1.0000000|" + parseFloat(payment.totalAmount).toFixed(2) + "||||" + parseFloat(payment.totalAmount).toFixed(2) + "\r\n";
		});
	});

	fileData += "1|CLOSED|"+brand.dlfTenantId+"|"+pos+"|"+recNo+"|"+tfn_str+"|"+dd+"|"+dd1+"|"+bills[0].username.substr(0, 8)+"|"+dd+"\r\n";
	callback(null, {name: fileName, data: fileData});
	
	function getTaxAmount(item){
		var total_tax=0;
		item.taxes.forEach(function(tax){total_tax+=tax.tax_amount;});
		return total_tax;
	}
}