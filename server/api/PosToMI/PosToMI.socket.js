/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var PosToMI = require('./PosToMI.model');

exports.register = function(socket) {
  PosToMI.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  PosToMI.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('PosToMI:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('PosToMI:remove', doc);
}