'use strict';

var _ = require('lodash');
var Dashboard = require('./dashboard.model');
var moment = require('moment');
var ObjectId = require('mongoose').Types.ObjectId;
// Get list of dashboards
exports.index = function(req, res) {
  Dashboard.Dashboard.find(function (err, dashboards) {
    if(err) { return handleError(res, err); }
    return res.json(200, dashboards);
  });
};

// Get a single dashboard
exports.show = function(req, res) {
  Dashboard.Dashboard.findById(req.params.id, function (err, dashboard) {
    if(err) { return handleError(res, err); }
    if(!dashboard) { return res.send(404); }
    return res.json(dashboard);
  });
};

// Creates a new dashboard in the DB.
exports.create = function(req, res) {
  Dashboard.Dashboard.create(req.body, function(err, dashboard) {
    if(err) { return handleError(res, err); }
    return res.json(201, dashboard);
  });
};

// Updates an existing dashboard in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Dashboard.Dashboard.findById(req.params.id, function (err, dashboard) {
    if (err) { return handleError(res, err); }
    if(!dashboard) { return res.send(404); }
    var updated = _.merge(dashboard, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, dashboard);
    });
  });
};

// Deletes a dashboard from the DB.
exports.destroy = function(req, res) {
  Dashboard.Dashboard.findById(req.params.id, function (err, dashboard) {
    if(err) { return handleError(res, err); }
    if(!dashboard) { return res.send(404); }
    dashboard.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.getDashboardDataByTime=function(req,res){

  //req.query={openTimeResetTime:new Date("2015-08-01T20:30:00.000Z"),startDate:new Date("2015-07-01T20:30:00.000Z"),endDate:new Date("2015-08-01T20:30:00.000Z"),deployment_id:new ObjectId("556eadf102fad90807111eac"),tenant_id:new ObjectId("556e9b0302fad90807111e34")};
  //return res.json(req.query.startDate);
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(req.query.tenant_id),
      deployment_id: new ObjectId(req.query.deployment_id),
      //isVoid: { $ne: true },
      _created: {
          $gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)
        }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      //kot:"$_kots",
      kot:{$cond:{if:'$_kots.isVoid',then:{},else:"$_kots"}},
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount",
      isVoid:"$isVoid"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{billId:"$_id",closeTime:"$closeTime",isVoid:"$isVoid",billNo:"$billNo",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"}
    }
  },
  {
    $project:{
      _id:0,
      totalAmount:{$cond:{if:'$_id.isVoid',then:0,else:"$totalAmount"}},
      totalDiscount:{$cond:{if:'$_id.isVoid',then:0,else:{ $add: [ "$totalDiscount", "$_id.billDiscountAmount"]}}} ,
      taxAmount:{$cond:{if:'$_id.isVoid',then:0,else:"$taxAmount"}},//"$taxAmount",
      closeTime:"$_id.closeTime",
      //closeTime:{ $dateToString: { format: "%Y-%m-%d", date: "$_id.closeTime" } },
      billNo:"$_id.billNo",
      billId:"$_id.billId"
    }
  }
  ], function (err, result_entry) {
    if (err) {
      handleError(res, err);
    }
    else
    {
      return res.json(result_entry);
    } 
  });
};

exports.testScript=function(req,res){
  req.query={openTimeResetTime:new Date("2015-08-01T20:30:00.000Z"),startDate:new Date("2015-06-30T20:30:00.000Z"),endDate:new Date("2015-07-31T20:30:00.000Z"),deployment_id:new ObjectId("556eadf102fad90807111eac"),tenant_id:new ObjectId("556e9b0302fad90807111e34")};
  // getDailyReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
  // getMonthlyReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
  // getItemWiseReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
  // getCategoryWiseReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
  // getTabWiseReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
  // getStaffWaiterWiseReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
  // getTableOccupacyWiseReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
  getCustomerWiseReport(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
    return res.send(200, result);
  });
  // getCustomerCountBeforeDate(req.query.deployment_id,req.query.tenant_id,'a','b',function(result){
  //   return res.send(200, result);
  // });
};

exports.getDailyReport=function(req,res){
    getDailyReport(req.query.deployment_id,req.query.tenant_id,req.query.startDate,req.query.endDate,function(result){
      return res.send(200, result);
    });
};
function getBillCounts (deployment_id,tenant_id,startDate,endDate,callback) {
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $group :{
      _id:null,
      BillCounts:{$sum:1}
    }
  }
  ], function (err, result) {
    if (err) {
      callback(err);
    }
    else
    {
      callback(result);
    } 
  });
}
function getDailyReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      kot:"$_kots",
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{billId:"$_id",created:"$closeTime",billNo:"$billNo",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"}
    }
  },
  {
    $project:{
      _id:0,
      billId:"$_id.billId",
      created:"$_id.created",
      billNo:"$_id.billNo",
      taxAmount:"$taxAmount",
      totalAmount:"$totalAmount",
      totalDiscount:{$add:["$totalDiscount","$_id.billDiscountAmount"]}
    }
  }
  ], function (err, result) {
    if (err) {
      callback(err);
    }
    else
    {
      getBillCounts(deployment_id,tenant_id,startDate,endDate,function(BillCounts){
        var daySale={totalAmount:0,totalDiscount:0,taxAmount:0,totalBills:0};
        _.forEach(result,function(r,i){
            daySale.totalAmount+=r.totalAmount;
            daySale.totalDiscount+=r.totalDiscount;
            daySale.taxAmount+=r.taxAmount;
            daySale.totalBills=BillCounts[0].BillCounts;
        });
      callback(daySale);
      });
    } 
  });
};

exports.getMonthlyReport=function(req,res){
  getMonthlyReport(req.query.deployment_id,req.query.tenant_id,req.query.startDate,req.query.endDate,function(result){
    return res.send(200, result);
  });
};

function getMonthlyReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      kot:"$_kots",
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{billId:"$_id",created:"$closeTime",billNo:"$billNo",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"}
    }
  },
  {
    $project:{
      _id:0,
      billId:"$_id.billId",
      created:"$_id.created",
      billNo:"$_id.billNo",
      taxAmount:"$taxAmount",
      totalAmount:"$totalAmount",
      totalDiscount:{$add:["$totalDiscount","$_id.billDiscountAmount"]}
    }
  }
  ], function (err, result) {
    if (err) {
      callback(err);
    }
    else
    {
      getBillCounts(deployment_id,tenant_id,startDate,endDate,function(BillCounts){
        var monthSale={totalAmount:0,totalDiscount:0,taxAmount:0,totalBills:0};
        _.forEach(result,function(r,i){
          monthSale.totalAmount+=r.totalAmount;
          monthSale.totalDiscount+=r.totalDiscount;
          monthSale.taxAmount+=r.taxAmount;
          monthSale.totalBills=BillCounts[0].BillCounts;
        });
        callback(monthSale);
      });
    } 
  });
};

exports.getItemWiseReport=function(req,res){
  getItemWiseReport(req.query.deployment_id,req.query.tenant_id,req.query.startDate,req.query.endDate,function(result){
    return res.send(200, result);
  });
};

function getItemWiseReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $unwind :"$_kots.items"
  },
  {
    $project:{
      kot:"$_kots",
      isVoid:"$isVoid",
      items:"$_kots.items",
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{itemId:"$items._id",itemName:"$items.name",categoryName:"$items.category.categoryName",categoryId:"$items.category._id"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$items.subtotal"}
    }
  },
  {
    $project:{
      _id:0,
      itemId:"$_id.itemId",
      itemName:"$_id.itemName",
      totalDiscount:"$totalDiscount",
      totalAmount:"$totalAmount",
      categoryName:"$_id.categoryName",
      categoryId:"$_id.categoryId"
    }
  }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else
    {
      callback(result_entry);
    }
  });
};

function getCategoryWiseReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: tenant_id,
      deployment_id: deployment_id,
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $unwind :"$_kots.items"
  },
  {
    $project:{
      kot:"$_kots",
      items:"$_kots.items",
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{itemId:"$items.category._id",itemName:"$items.category.categoryName"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$items.subtotal"}
    }
  },
  {
    $project:{
      _id:0,
      itemId:"$_id.itemId",
      itemName:"$_id.itemName",
      totalDiscount:"$totalDiscount",
      totalAmount:"$totalAmount"
    }
  }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else
    {
      callback(result_entry);
    }
  });
};

exports.getTabWiseReport=function(req,res){
  getTabWiseReport(req.query.deployment_id,req.query.tenant_id,req.query.startDate,req.query.endDate,function(result){
    return res.send(200, result);
  });
};
function getTabWiseReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      kot:"$_kots",
      tabType:"$tab",
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{tabType:"$tabType",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"}
    }
  },
  {
    $project:{
      _id:0,
      tabType:"$_id.tabType",
      taxAmount:"$taxAmount",
      //totalDiscount:"$totalDiscount",
      totalDiscount:{$add:["$totalDiscount","$_id.billDiscountAmount"]},
      totalAmount:"$totalAmount",
      netTotal:{$subtract:[{$add:["$taxAmount","$totalAmount"]},{$add:["$totalDiscount","$_id.billDiscountAmount"]}]}
      //billDiscountAmount:"$_id.billDiscountAmount"
    }
  }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else
    {
      var data=[];
      _.forEachRight(result_entry,function(r,i){
        var index=_.findIndex(data,{tabType:r.tabType});
        if(index<0){
          data.push(r);
        }
        else{
          data[index].taxAmount+=r.taxAmount;
          data[index].totalDiscount+=r.totalDiscount;
          data[index].totalAmount+=r.totalAmount;
          data[index].netTotal+=r.netTotal;
        }
      });
      callback(data);
    }
  });
};

exports.getStaffWaiterWiseReport=function(req,res){
  getStaffWaiterWiseReport(req.query.deployment_id,req.query.tenant_id,req.query.startDate,req.query.endDate,function(result){
    return res.send(200, result);
  });
};
function getStaffWaiterWiseReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      kot:"$_kots",
      waiter:"$_waiter",
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{waiterName:"$waiter.username",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"}
    }
  },
  {
    $project:{
      _id:0,
      waiterName:"$_id.waiterName",
      taxAmount:"$taxAmount",
      //totalDiscount:"$totalDiscount",
      totalDiscount:{$add:["$totalDiscount","$_id.billDiscountAmount"]},
      totalAmount:"$totalAmount",
      netTotal:{$subtract:[{$add:["$taxAmount","$totalAmount"]},{$add:["$totalDiscount","$_id.billDiscountAmount"]}]}
    }
  }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else
    {
      _.forEach(result_entry,function(r,i){
        if(r.waiterName==null){
          r.waiterName="No Waiter Assigned";
        }
      });
      var data=[];
      _.forEachRight(result_entry,function(r,i){
        var index=_.findIndex(data,{waiterName:r.waiterName});
        if(index<0){
          data.push(r);
        }
        else{
          data[index].taxAmount+=r.taxAmount;
          data[index].totalDiscount+=r.totalDiscount;
          data[index].totalAmount+=r.totalAmount;
          data[index].netTotal+=r.netTotal;
        }
      });
      callback(data);
    }
  });
};

exports.getTableOccupacyWiseReport=function(req,res){
  getTableOccupacyWiseReport(req.query.deployment_id,req.query.tenant_id,req.query.startDate,req.query.endDate,function(result){
    return res.send(200, result);
  });
};
function getTableOccupacyWiseReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      kot:"$_kots",
      tableId:"$_tableId",
      closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{tableId:"$tableId",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"}
    }
  },
  {
    $project:{
      _id:0,
      tableId:"$_id.tableId",
      taxAmount:"$taxAmount",
      //totalDiscount:"$totalDiscount",
      totalDiscount:{$add:["$totalDiscount","$_id.billDiscountAmount"]},
      totalAmount:"$totalAmount"
    }
  }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else
    {
      _.forEachRight(result_entry,function(r,i){
        if(r.tableId==null){
          result_entry.splice(i,1);
        }
      });
      callback(result_entry);
    }
  });
};

exports.getCustomerWiseReport=function(req,res){
  getCustomerWiseReport(req.query.deployment_id,req.query.tenant_id,req.query.startDate,req.query.endDate,function(result){
    return res.send(200, result);
  });
};
function getCustomerWiseReport(deployment_id,tenant_id,startDate,endDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $gte: new Date(startDate), $lte: new Date(endDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      kot:"$_kots",
      customer:"$_customer",
      closeTime:{$dateToString: { format: "%Y-%m-%d", date: "$_created" }},
      //closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{customerId:"$customer.customerId",closeTime:"$closeTime",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"},
      //closeTime:{$dateToString: { format: "%Y-%m-%d", date: "$closeTime" }},
      count: { $sum: 1 }
    }
  },
  {
    $project:{
      _id:0,
      customerId:"$_id.customerId",
      taxAmount:"$taxAmount",
      //totalDiscount:"$totalDiscount",
      totalDiscount:{$add:["$totalDiscount","$_id.billDiscountAmount"]},
      totalAmount:"$totalAmount",
      closeTime:"$_id.closeTime",
      count:"$count",
      netSale:{$subtract:[{$add:["$totalAmount","$taxAmount"]},{$add:["$totalDiscount","$_id.billDiscountAmount"]}]}
    }
  }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else
    {
      getCustomerCountBeforeDate(deployment_id,tenant_id,startDate,function(result){
           _.forEachRight(result_entry,function(r,i){
              r.repeatCustomer=0;r.newCustomer=0;r.noCustomer=0;
              r.netSale_No=0;r.netSale_New=0;r.netSale_Repeat=0;
              if(r.customerId!=null){
                _.forEach(result,function(re,ii){
                  if(re.customerId!=null){
                    if(r.customerId==re.customerId){
                      if(re.count>0){
                        r.repeatCustomer=re.count;
                        r.netSale_Repeat=re.netSale;
                      }
                      else
                      {
                        r.newCustomer=r.count;
                        r.netSale_New=re.netSale;
                      }
                    }
                  }
                  else
                  {
                    r.newCustomer=1;
                    r.netSale_New=r.netSale;
                  }
                });
              }
              else
              {
                r.noCustomer=r.count;
                r.netSale_No=r.netSale;
              }
              delete r.count;
            });
            callback(result_entry);
      });
      
    } 
  });
};
function getCustomerCountBeforeDate(deployment_id,tenant_id,startDate,callback){
  Dashboard.Bill.aggregate([
  {
    $match: {
      tenant_id: new ObjectId(tenant_id),
      deployment_id: new ObjectId(deployment_id),
      isVoid:{$ne:true},
      _created: {
        $lte: new Date(startDate)
      }
    }
  },
  {
    $unwind :"$_kots"
  },
  {
    $project:{
      kot:"$_kots",
      customer:"$_customer",
      closeTime:{$dateToString: { format: "%Y-%m-%d", date: "$_created" }},
      //closeTime:"$_created",
      billNo:"$billNumber",
      billDiscountAmount:"$billDiscountAmount"
    }
  },
  {
    $match:{
      "kot.isVoid":{$ne:true}
    }
  },
  {
    $group :{
      _id:{customerId:"$customer.customerId",closeTime:"$closeTime",billDiscountAmount:"$billDiscountAmount"},
      taxAmount:{$sum:"$kot.taxAmount"},
      totalDiscount:{$sum:"$kot.totalDiscount"},
      totalAmount:{$sum:"$kot.totalAmount"},
      //closeTime:{$dateToString: { format: "%Y-%m-%d", date: "$closeTime" }},
      count: { $sum: 1 }
    }
  },
  {
    $project:{
      _id:0,
      customerId:"$_id.customerId",
      taxAmount:"$taxAmount",
      //totalDiscount:"$totalDiscount",
      totalDiscount:{$add:["$totalDiscount","$_id.billDiscountAmount"]},
      totalAmount:"$totalAmount",
      closeTime:"$_id.closeTime",
      count:"$count"
    }
  }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else
    {
      callback(result_entry);
    }
  });
};
function handleError(res, err) {
  return res.send(500, err);
}