'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    BillSchema=require('mongoose').model('Bill').schema;

var DashboardSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

 var Dashboard = mongoose.model('Dashboard', DashboardSchema);
 var Bill =mongoose.model('Bill', BillSchema);
//module.exports = mongoose.model('Dashboard', DashboardSchema);
module.exports={
	Dashboard:Dashboard,
  	Bill:Bill
}
//module.exports = mongoose.model('StockReport', StockReportSchema);