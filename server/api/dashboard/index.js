'use strict';

var express = require('express');
var controller = require('./dashboard.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/getDashboardDataByTime', controller.getDashboardDataByTime);
router.get('/getDailyReport', controller.getDailyReport);
router.get('/getMonthlyReport', controller.getMonthlyReport);
router.get('/getItemWiseReport', controller.getItemWiseReport);
router.get('/getTabWiseReport', controller.getTabWiseReport);
router.get('/getStaffWaiterWiseReport', controller.getStaffWaiterWiseReport);
router.get('/getTableOccupacyWiseReport', controller.getTableOccupacyWiseReport);
router.get('/getCustomerWiseReport', controller.getCustomerWiseReport);
router.get('/testScript', controller.testScript);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;