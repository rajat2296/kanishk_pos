'use_strict'
var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var Utils=require('../Utils/utils');
var Q = require('q');
var DayBreakup = require('../dayBreakup/dayBreakup.model');
var Category = require('../category/category.model');

exports.fetchSessions = function (req) {
  var query = {
    deployment_id: req.deployment_id,
    tenant_id: req.tenant_id
  }
  DayBreakup.find(query, {sessionName: 1, fromTime: 1, toTime: 1}, function (err, dayBreakups) {
    return JSON.parse(dayBreakups);
  });
}
exports.fetchCategories = function (req) {
  Category.find(req.query, function (err, categories) {
    return JSON.parse(categories)
  });
}
exports.getTax = function (taxId, collection) {
  //console.log(taxId, collection);
  var _f = _.filter(collection.taxes, {
    id: taxId
  });
  // console.log(_f);
  if (_f.length > 0) {
    var tempTax = 0 
    _.forEach(_f,function(f){
      tempTax += f.tax_amount
    })
    return (!_.isNaN(tempTax)) ? parseFloat(tempTax) : 0;
  } else {
    return 0;
  }
}
exports.getBillTax = function (toPrintTaxId, bill) {
  var _bTax = 0;
  var _f = _.filter(bill._taxes, {
    id: toPrintTaxId
  });
  //console.log(toPrintTaxId, _f);
  if (_f.length > 0) {
    _.forEach(_f, function (t) {
      if (!_.isNaN(t.tax_amount)) {
        _bTax += parseFloat(t.tax_amount);
      } else {
        _bTax += 0;
      }
    });

  }
  return parseFloat(_bTax);
}
exports.getTotalTax = function (taxId, bills) {
  var _totalTax = 0;
  _.forEach(bills, function (bill) {
    var _fTax = _.filter(bill._taxes, {
      id: taxId
    });
    if (_fTax.length > 0) {
      _.forEach(_fTax, function (t) {
        if (!_.isNaN(t.tax_amount)) {
          _totalTax += t.tax_amount;
        } else {
          _totalTax += 0;
        }
      });
    }
  });
  return _totalTax;
}
exports.getTotalTaxRows = function (taxId, rows) {
  var _totalTax = 0;

  _.forEach(rows, function (row) {
    var _f = [];
    // console.log(row)
    if (row.taxes) {
      if (_.has(row.taxes[0], '_id')) {
        var _f = _.filter(row.taxes, {
          _id: taxId
        });
      } else {
        var _f = _.filter(row.taxes, {
          id: taxId
        });
      }
    }
    if (_f.length > 0) {
      _.forEach(_f, function (t) {
        _totalTax += (!_.isNaN(t.tax_amount)) ? parseFloat(t.tax_amount) : 0;
      });
    } else {
      _totalTax += 0;
    }
  });
  return _totalTax;
}
exports.msToTime = function(duration) {
  //console.log(duration);
  var seconds = parseInt((duration % (1000 * 60)) / 1000)
    , minutes = parseInt((duration % (60 * 60 * 1000)) / (1000 * 60))
    , hours = parseInt((duration / (1000 * 60 * 60)));

  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours + ":" + minutes + ":" + seconds;
}
exports.msToTimes = function(duration) {
  //console.log(duration);
  //seconds = parseInt((duration % (1000 * 60)) / 1000)
  var minutes = parseInt((duration % (60 * 60 * 1000)) / (1000 * 60))
    , hours = parseInt((duration / (1000 * 60 * 60)));

  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  //seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours + ":" + minutes;
}
exports.getRoundOffs = function (netAmount) {
  var _r = parseFloat(parseFloat(netAmount) % 1);
  if (_r >= .5) {
    return parseFloat(1 - _r);
  } else {
    if (_r < .5) {
      return -(_r);
    }
  }
}
exports.getAfterRoundOff = function (netAmount) {
  //return Math.round(netAmount);
  return parseFloat(Utils.roundNumber(netAmount));
}
exports.getRoundOff = function (netAmount) {
  return Utils.roundNumber((Utils.roundNumber(netAmount) - netAmount), 2);
}
exports.getObjectKeysSorted = function (object) {
  var keysSorted = Object.keys(object).sort(function (a, b) {
    return new Date(b).getTime() - new Date(a).getTime()
  });
  var newObj = {};
  for (var k in keysSorted) {
    newObj[keysSorted[k]] = object[keysSorted[k]];
  }
  return newObj;
}
exports.getFormattedDateByCutoffTime = function (date,cutOffTimeSetting) {
  var dt = new Date(date);
  var cutOff = new Date(cutOffTimeSetting);
  var cOHr = cutOff.getHours();
  var cOMin = cutOff.getMinutes();

  var hr = date.getHours();
  var min = date.getMinutes();

  var nDt = new Date(date);
  if (hr < cOHr) {
    nDt = nDt.setDate(nDt.getDate() - 1);
  } else if (hr === cOHr) {
    if (min < cOMin) {
      nDt = nDt.setDate(nDt.getDate() - 1);
    }
  }

  return moment(new Date(nDt)).format('YYYY-MM-DD');
}
exports.getBillTaxTotal = function (id, taxes) {
  var tax = _.find(taxes, function (tax) {
    return tax.id == id;
  });
  if (tax)
    return tax.tax_amount;
  else
    return 0;
}
exports.getChargesTotal = function(name,charges){
  var charge = _.find(charges,function(ch){
    return ch.name == name;
  })
  if(charge)
    return charge.amount;
  else
    return 0;
}
exports.getTotalTaxPaymentDetails = function (taxId, bills) {
  var taxAmount = 0;
  _.forEach(bills, function (bill) {
    _.forEach(bill.aggregate.taxes, function (tax) {
      if (tax.id == taxId) {
        // console.log(Utils.roundNumber(tax.tax_amount), 2);
        taxAmount += Utils.roundNumber(tax.tax_amount, 2);
      }
    });
  });
  return taxAmount;
}
exports.getTotalChargesPaymentDetails = function (name, bills) {
  var chargeAmount = 0;
  _.forEach(bills, function (bill) {
    _.forEach(bill.aggregate.charges, function (charge) {
      if (charge.name == name) {
        // console.log(Utils.roundNumber(charge.amount), 2);
        chargeAmount += Utils.roundNumber(charge.amount, 2);
      }
    });
  });
  return chargeAmount;
}
exports.getTaxForTaxSummary = function (id, taxes) {
  var taxAmount = 0;
  var tax = _.find(taxes, function (tx) {
    return tx.id == id;
  });
  if (tax)
    return tax.tax_amount;
  return 0;
}
exports.getChargeForTaxSummary = function (name,row) {
  var chargeAmount = 0;
  _.forEach(row.charges, function (charge) {
    if (charge.name == name) {
      // console.log(Utils.roundNumber(charge.amount), 2);
      chargeAmount += Utils.roundNumber(charge.amount, 2);
    }
  });
  return chargeAmount;
}
exports.getTotalChargesTaxSummary = function (name, bills) {
  var chargeAmount = 0;
  _.forEach(bills, function (bill) {
    _.forEach(bill.charges, function (charge) {
      if (charge.name == name) {
        // console.log(Utils.roundNumber(charge.amount), 2);
        chargeAmount += Utils.roundNumber(charge.amount, 2);
      }
    });
  });
  return chargeAmount;
}
exports.getTotalTaxInvoiceReport = function (id, bills) {
  var taxAmount = 0;
  _.forEach(bills, function (bill) {
    _.forEach(bill.aggregate.taxes, function (tax) {
      if (tax.id == id)
        taxAmount += tax.tax_amount;
    })
  })
  return taxAmount;
}
exports.getTotalChargeInvoiceReport = function (name, bills) {
  var taxAmount = 0;
  _.forEach(bills, function (bill) {
    _.forEach(bill.aggregate.charges, function (charge) {
      if (charge.name == name)
        taxAmount += charge.amount;
    })
  })
  return taxAmount;
}
exports.getItemTaxInvoiceReport = function (taxId, item) {
  //console.log(item.taxes);
  var taxAmount = 0;
  _.forEach(item.taxes, function (tax) {
    if (tax.id == taxId) {
      taxAmount += tax.tax_amount;
    }
  })
  return taxAmount;
}
exports.getDayTaxInvoiceReport = function(taxId, bills) {
  var taxAmount = 0;
  _.forEach(bills, function (bill) {
    _.forEach(bill.aggregate.taxes, function (tax) {
      if (tax.id == taxId)
        taxAmount += tax.tax_amount;
    });
  });
  return taxAmount;
}
exports.getDayChargeInvoiceReport = function(chargeName, bills) {
  var chargeAmount = 0;
  _.forEach(bills, function (bill) {
    _.forEach(bill.aggregate.charges, function (charge) {
      if (charge.name == chargeName)
        chargeAmount += charge.amount;
    });
  });
  return chargeAmount;
}
exports.getDates = function(startDate, stopDate) {
  var dateArray = new Array();
  var currentDate = startDate;
  // console.log(currentDate.getDate());
  while (currentDate <= stopDate) {
    dateArray.push(moment(currentDate).format('YYYY-MM-DD'));
    currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));
  }
  return dateArray;
}