'use strict';

var _ = require('lodash');
var config = require('../../config/environment');
var moment = require('moment');
var Bill = require('../bill/bill.model');
var billapi = require('../bill/bill.controller')
var async = require('async');
var crypto = require('crypto');
var mongoose = require('mongoose');
var request = require('request');
var SmsTemplate = require('../smstemplate/smstemplate.model');
var Deployment = require('../deployment/deployment.model');
var SmsTransaction = require('../smstransaction/smstransaction.model');
var Q = require('q');
var utils = require('../Utils/utils');
var request = require('request');
var Client = require('node-rest-client').Client;
var client = new Client();
var advanceBooking = require('../advancebooking/advancebooking.model')
var Report = require('../report/report.controller');
var kue = require('kue');
var queue = kue.createQueue(config.redisConn);

exports.hourlyAggregation = function(req,res){
  var d = Q.defer();
  var startDate = new Date(new Date(req.body.arg.startDate));
  var endDate = new Date(new Date(req.body.arg.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  console.log(startDate);
  console.log(endDate);
  var dep = req.body.arg.deployment_id;
  console.log("dep" + dep);
  console.log(req.body.arg.tabType);
  if (req.body.arg.tabType ) {
    var itemCondition = {
      _created: {
         $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
      },
      deployment_id: new mongoose.Types.ObjectId(dep),
      isDeleted: {$ne: true},
      isVoid: {$ne: true},
      tab: req.body.arg.tabType
    };
  } else {
    var itemCondition = {
      _created: {
         $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
      },
      deployment_id: new mongoose.Types.ObjectId(dep),
      isDeleted: {$ne: true},
      isVoid: {$ne: true},
    }; 
  }

  Bill.aggregate([
    {
      $match: itemCondition
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: "$_id",
        totalNet: {$cond: ['$_kots.isVoid', 0, {$subtract:["$_kots.totalAmount","$_kots.totalDiscount"]}]},
        deployment_id: "$deployment_id",
        _created: "$_created",
        _closeTime: "$_closeTime",
        tabType: "$tabType",
        _covers: "$_covers",
        prefix: "$prefix",
        billNumber: "$billNumber",
        serialNumber: "$serialNumber",
        daySerialNumber: "$daySerialNumber",
        billDiscountAmount: "$billDiscountAmount",
        chargesAmount:  { $ifNull: [ "$charges.amount", 0 ] }
      }
    },
    {
      $group: {
        _id: {
          _id: "$_id", deployment_id: "$deployment_id", _created: "$_created",
          _closeTime: "$_closeTime",
          tabType: "$tabType",
          _covers: "$_covers",
          prefix: "$prefix",
          billNumber: "$billNumber",
          serialNumber: "$serialNumber",
          billDiscountAmount: "$billDiscountAmount",
          daySerialNumber: "$daySerialNumber"
        },
        chargesAmount:{$first:'$chargesAmount'},
        totalNet: {$sum: "$totalNet"}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        totalNet:{$subtract:["$totalNet","$_id.billDiscountAmount"]},
        _created: "$_id._created",
        _closeTime: "$_id._closeTime",
        tabType: "$_id.tabType",
        _covers: "$_id._covers",
        prefix: "$_id.prefix",
        billNumber: "$_id.billNumber",
        serialNumber: "$_id.serialNumber",
        daySerialNumber: "$_id.daySerialNumber"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    console.log(err);
    if (err) {
      d.reject(err)
    }
    d.resolve(bills)
  });
  return d.promise;
}

exports.customerDataReport = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id: deployment_id,
        isDeleted: {$ne: true},
        isVoid: {$ne: true},
        _customer: {$exists: true}

      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: "$_id",
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        deployment_id: "$deployment_id",

        billPrintTime: "$billPrintTime",
        _created: "$_created",
        _closeTime: "$_closeTime",
        tabType: "$tabType",
        prefix: "$prefix",
        billNumber: "$billNumber",
        serialNumber: "$serialNumber",
        daySerialNumber: "$daySerialNumber",
        _customer: {
          customerId: "$_customer.customerId",
          address1: "$_customer.address1",
          address2: "$_customer.address2",
          email: "$_customer.email",
          state: "$_customer.state",
          city: "$_customer.city",
          mobile: "$_customer.mobile",
          phone: "$_customer.phone",
          firstname: "$_customer.firstname",

        },
        onlineBill: {$cond: ['$onlineBill._id', '$onlineBill.source.name', 'POS']},

      }
    },
    {$unwind: "$kot.items"},
    {
      $project: {
        name: "$kot.items.name",
        qty: "$kot.items.quantity",
        deployment_id: "$deployment_id",
        complimentary: "$complimentary",
        _customer: "$_customer",
        subtotal: "$kot.items.subtotal",
        onlineBill: "$onlineBill",
        billPrintTime: "$billPrintTime",
        _created: "$_created",
        _closeTime: "$_closeTime",
        tabType: "$tabType",
        prefix: "$prefix",
        billNumber: "$billNumber",
        serialNumber: "$serialNumber",
        daySerialNumber: "$daySerialNumber",

      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if (err) {
      d.reject(err)
      //return handleError(res, err);
    }
    d.resolve(bills)
  });
  return d.promise;
}
    
exports.deliveryReport = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);

  Bill.aggregate([
    {
      $match: {
       _created: {
            $gte: moment(startDate).toDate(),
            $lte: moment(endDate).toDate()
          },
          deployment_id: deployment_id,
          isDeleted: {$ne: true},
          
          tabType: "delivery",
          _isDelivery: {$eq: true},
         
          isVoid:{$ne:true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: "$_id",
        totalNet: {$cond: ['$_kots.isVoid', 0, {$subtract: [{$add: ["$_kots.totalAmount", "$_kots.taxAmount"]}, "$_kots.totalDiscount"]}]},
        deployment_id: "$deployment_id",
        _delivery: {
          firstname: "$_delivery.firstname",
          lastname: "$_delivery.lastname",
          username: "$_delivery.username",
          _id: "$_delivery._id",
          deliveryTime: "$_delivery.deliveryTime"
        },
        billPrintTime: "$billPrintTime",
        _created: "$_created",
        _closeTime: "$_closeTime",
        tabType: "$tabType",
        prefix: "$prefix",
        billNumber: "$billNumber",
        serialNumber: "$serialNumber",
        daySerialNumber: "$daySerialNumber",
        billDiscountAmount: "$billDiscountAmount",
        chargesAmount:{ $ifNull: [ "$charges.amount", 0 ] }
      }
    },
    {
      $group: {
        _id: {
          _id: "$_id", deployment_id: "$deployment_id", _delivery: "$_delivery", _created: "$_created",
          _closeTime: "$_closeTime",
          billPrintTime: "$billPrintTime",
          prefix: "$prefix",
          billNumber: "$billNumber",
          serialNumber: "$serialNumber",
          billDiscountAmount: "$billDiscountAmount",
          daySerialNumber: "$daySerialNumber"
        },
        chargesAmount:{$first:'$chargesAmount'},
        totalNet: {$sum: "$totalNet"}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        _delivery: "$_id._delivery",
         totalNet:{$subtract:[{$add:["$totalNet","$chargesAmount"]},"$_id.billDiscountAmount"]},
        _created: "$_id._created",
        _closeTime: "$_id._closeTime",
        tabType: "$_id.tabType",
        //_covers:"$_id._covers",
        prefix: "$_id.prefix",
        billPrintTime: "$_id.billPrintTime",
        billNumber: "$_id.billNumber",
        serialNumber: "$_id.serialNumber",
        daySerialNumber: "$_id.daySerialNumber"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if (err) {
      // console.log(err)
      d.reject(err);
      // return handleError(res, err);
    }
   d.resolve(bills);
  });
  return d.promise
}
  
exports.dailySalesAggregation = function(req,res) {
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
      endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
  Bill.find({
    _created: {$gte: startDate, $lt: endDate},
    deployment_id: deployment_id,
    isDeleted: {$ne: true}
  },{
      _created: 1,
      _covers: 1,
      isVoid: 1,
      complimentary:1,
      billDiscountAmount: 1,
      "_kots.isVoid": 1,
      "_kots.items.subtotal": 1,
      "_kots.items.isDiscounted": 1,
      "_kots.items.billDiscountAmount": 1,
      "_kots.items.discounts.discountAmount": 1,
      "_kots.items.category.name": 1,
      "_kots.items.category._id": 1,
      "_kots.items.category.categoryName": 1,
      "_kots.items.category.section": 1,
      "_kots.items.addOns.subtotal": 1,
      "_kots.items.addOns.category": 1,
      '_kots.items.taxes._id': 1,
       '_kots.items.taxes.name': 1,
      '_kots.items.taxes.tax_amount': 1,
      '_kots.items.taxes.baseRate_preTax': 1,
      '_kots.items.taxes.cascadingTaxes._id': 1,
      '_kots.items.taxes.cascadingTaxes.name': 1,
      '_kots.items.taxes.cascadingTaxes.tax_amount': 1,
      "_kots.items.addOns.discount.discountAmount": 1,
      '_kots.deleteHistory.subtotal':1,
      '_kots.deleteHistory.category._id':1,
      '_kots.deleteHistory.category.section':1,
      '_kots.deleteHistory.category.categoryName':1,
      '_kots.deleteHistory.addOns.category._id':1,
      '_kots.deleteHistory.addOns.category.section':1,
      '_kots.deleteHistory.addOns.category.categoryName':1,
      '_kots.deleteHistory.addOns.subtotal':1,
      '_kots.items.addOns.taxes._id': 1,
      '_kots.items.addOns.taxes.name': 1,
      '_kots.items.addOns.taxes.tax_amount': 1,
      '_kots.items.addOns.taxes.baseRate_preTax': 1,
      '_kots.items.addOns.taxes.cascadingTaxes.baseRate_preTax': 1,
      '_kots.items.addOns.taxes.cascadingTaxes._id': 1,
      '_kots.items.addOns.taxes.cascadingTaxes.name': 1,
      '_kots.items.addOns.taxes.cascadingTaxes.selected': 1,
      '_kots.items.addOns.taxes.cascadingTaxes.tax_amount': 1,
      "_kots.items.addOns.category._id": 1,
      "_kots.items.addOns.category.categoryName": 1,
      "_kots.items.addOns.category.section": 1,
  }, function (err, bills) {
    if (err)
      d.reject(err)
    //return handleError(res, err);
    d.resolve(bills);
  });
  return d.promise
}

exports.kotDelete = function(req,res) {
  var d = Q.defer();
  var startDate=new Date( new Date(req.body.arg.startDate));
  var endDate=new Date( new Date(req.body.arg.endDate));
      endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  var dep = req.body.arg.deployment_id ;

  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id:new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
        $or: [ {"isVoid": true}, {"_kots.isVoid":true},
          {"_kots.deleteHistory.0": {$exists: true}}]
      }
    },
    {
      $project:{
        billNumber:"$billNumber",
        serialNumber:"$serialNumber",
        daySerialNumber:"$daySerialNumber",
        _closeTime:"$_closeTime",
        _created:"$_created",
        prefix:"$prefix",
        isVoid:"$isVoid",
        voidBillTime:1,
        voidComment:1,
        '_waiter':{
          $cond: {
            if: "$_waiter.username",
            then: "$_waiter",
            else: { 
              $cond: {
                if:"$_delivery.username",
                then: "$_delivery",
                else:{username:{$literal:"NA"}}
              }
            }
          }
        },
        'voidDetail.userName':1,
        'voidDetail.time':1,
        '_kots.isVoid':1,
        '_kots.waiter.username':1,
        '_currentUser.username':1,
        '_kots.created':1,
        '_kots.kotNumber':1,
        '_kots.comment':1,
        '_kots.voidKotTime':1,
        '_kots.voidUser':1,
        '_kots.deleteHistory.voidItemTime':1,
        '_kots.deleteHistory.name':1,
        '_kots.deleteHistory.deleteUser':1,
        '_kots.deleteHistory.rate':1,
        '_kots.deleteHistory.subtotal':1,
        '_kots.deleteHistory.comment':1,
        '_kots.deleteHistory.quantity':1,
        '_kots.deleteHistory.addOns.subtotal':1,
        '_kots.deleteHistory.addOns.name':1,
        '_kots.deleteHistory.addOns.comment':1,
        '_kots.deleteHistory.addOns.rate':1,
        '_kots.deleteHistory.addOns.quantity':1,
        '_kots.items.name':1,
        '_kots.items.rate':1,
        '_kots.items.subtotal':1,
        '_kots.items.quantity':1,
        '_kots.items.addOns.subtotal':1,
        '_kots.items.addOns.name':1,
        '_kots.items.addOns.rate':1,
        '_kots.items.addOns.quantity':1,
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if (err) {
      d.reject(err)
    }
    // console.log("result is "+ JSON.stringify(result[0]));
    d.resolve(bills)
  });
  return d.promise
}

exports.kotTracking = function(req,res){
  var d =Q.defer();
  var startDate=new Date( new Date(req.body.arg.startDate));
  var endDate=new Date( new Date(req.body.arg.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  console.log(startDate);
  console.log(endDate);
  var dep = req.body.arg.deployment_id ;


  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id:new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
      }
    },

    {
      $project:{
        billNumber:"$billNumber",
        serialNumber:"$serialNumber",
        daySerialNumber:"$daySerialNumber",
        _closeTime:"$_closeTime",
        _created:"$_created",
        instance:"$instance",
        isVoid:"$isVoid",
        '_kots.isVoid':1,
        '_kots.waiter.firstname':1,
        '_kots.waiter.lastname':1,
        '_currentUser.username':1,
        '_waiter.firstname':1,
        '_waiter.lastname':1,
        '_kots.created':1,
        '_kots.kotNumber':1,
        '_kots.comment':1,
        '_kots.voidKotTime':1,
        '_kots.deleteHistory.voidItemTime':1,
        '_kots.deleteHistory.name':1,
        '_kots.deleteHistory.rate':1,
        '_kots.deleteHistory.subtotal':1,
        '_kots.deleteHistory.quantity':1,
        '_kots.deleteHistory.addOns.subtotal':1,
        '_kots.deleteHistory.addOns.name':1,
        '_kots.deleteHistory.addOns.rate':1,
        '_kots.deleteHistory.addOns.quantity':1,
        '_kots.items.name':1,
        '_kots.items.rate':1,
        '_kots.items.subtotal':1,
        '_kots.items.quantity':1,
        '_kots.items.addOns.subtotal':1,
        '_kots.items.addOns.name':1,
        '_kots.items.addOns.rate':1,
        '_kots.items.addOns.quantity':1,

      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    console.log(err);
    if (err) {
     d.reject(err)
    }
    d.resolve(bills)
  });
  return d.promise
}

exports.aggregateOtherPaymentDetail = function(req,res){
  var d = Q.defer();
  var startDate=new Date( new Date(req.query.startDate));
  var endDate = new Date( new Date(req.query.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  var dep = req.query.deployment_id ;

  Bill.aggregate([
    {
      $match : {
        _created: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        },
        deployment_id: new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$payments.cards"
    },
    {
      $project: {
        _created: 1,
        daySerialNumber: 1,
        serialNumber: 1,
        prefix: 1,
        otherPayments: {$cond: [{$eq: ["$payments.cards.cardType", "Online"]}, "$payments.cards", {}]}
      }
    },
    {
      $unwind: "$otherPayments.detail"
    },
    {
      $group: {
        _id: {
          otherName: "$otherPayments.detail.onlineName",
          otherType: "$otherPayments.detail.onlineType",
          _created: "$_created",
          billId: "$_id",
          prefix: "$prefix",
          daySerialNumber: "$daySerialNumber",
          serialNumber: "$serialNumber"
        },
        amount: {$sum: "$otherPayments.totalAmount"}
      }
    },
    {
      $project: {
        _id: "$_id.billId",
        _created: "$_id._created",
        otherName: "$_id.otherName",
        prefix: "$_id.prefix",
        daySerialNumber: "$_id.daySerialNumber",
        serialNumber: "$_id.serialNumber",
        onlinePayment: {$cond: [{$eq: ["$_id.otherType", "Online-Payment"]}, "$amount", 0]},
        codPayment: {$cond: [{$eq: ["$_id.otherType", "COD"]}, "$amount", 0]}
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
      d.reject(err)
    d.resolve(bills);
  });
  return d.promise
}

exports.aggregateConsolidate = function(req,res){
  var d = Q.defer();
  var startDate=new Date( new Date(req.query.startDate));
  var endDate=new Date( new Date(req.query.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  var dep = req.query.deployment_id ;

  Bill.aggregate([
    {
      $match : {
        _created: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        },
        deployment_id: new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$payments.cards"
    },
    {
      $project: {
        daySerialNumber: 1,
        _created: 1,
        otherPayments: {$cond: [{$eq: ["$payments.cards.cardType", "Online"]}, "$payments.cards", {}]}
      }
    },
    {
      $unwind: "$otherPayments.detail"
    },
    {
      $group: {
        _id: {
          otherType: "$otherPayments.detail.onlineType",
          otherName: "$otherPayments.detail.onlineName"
        },
        amount: {$sum: "$otherPayments.totalAmount"},
        numBills: {$sum: 1}
      }
    },
    {
      $project: {
        otherType: "$_id.otherType",
        otherName: "$_id.otherName",
        amount: "$amount",
        numBills: "$numBills"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
      d.reject(err)
    d.resolve(bills)
  });
 return d.promise
}

exports.removeTaxes = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
  Bill.find({
    _created: {
      $gte: moment(startDate).toDate(),
      $lte: moment(endDate).toDate()
    },
    deployment_id: deployment_id,
    isVoid:{$ne:true},
    isDeleted: {$ne: true},
    removedTaxes:{$exists:true},
    "removedTaxes.0":{$exists:true}
  },{
    _created: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    serialNumber: 1,
    'removedTaxes.name':1,
    'removedTaxes.userName':1,
    '_currentUser.username':1
  }, function(err, bills){
    if (err)
      d.reject(err);
    d.resolve(bills)
  });
  return d.promise
}

exports.complimentaryHeadwise = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
  Bill.find({
    _created: {
      $gte: moment(startDate).toDate(),
      $lte: moment(endDate).toDate()
    },  
    deployment_id: deployment_id,
    isVoid:{$ne:true},
    isDeleted:{$ne:true},
    $or:[{complimentary:true},
         {"_offers.0.offer.complimentary": true}]
  },{
    serialNumber:1,
    daySerialNumber:1,
    _created:1,
    complimentaryHead:1,
    tabType:1,
    complimentaryComment:1,
    prefix:1,
    '_customer.firstname':1,
    '_currentUser.username':1,
    '_waiter.username':1,
    '_offers':1
  }, function(err, bills){
    if (err)
      d.reject(err);
    d.resolve(bills);
  })
  return d.promise
}

exports.settlementReport = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep_id = req.body.arg.deployment_id;
  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
         $and: [{ 'payments.cards.totalAmount':{ $gt: 0 }},
        {"payments.cards.0": {$exists: true}}],
        deployment_id: new mongoose.Types.ObjectId(dep_id),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    }, {
      $unwind: "$payments.cards"
    },
    {
      $project: {
        _created: 1,
        daySerialNumber: 1,
        serialNumber: 1,
        prefix: 1,
        _closeTime:1,
        paymentsByCard:{  
          $cond: {
            if: {$or: [{ $eq: [ "$payments.cards.cardType", "CreditCard" ] }, { $eq: [ "$payments.cards.cardType", "DebitCard" ]}
            ]},
            then: "$payments.cards",
            else: []
          }
        }
      }
    },
    {
      $unwind: {path: "$paymentsByCard.detail", includeArrayIndex: "itemIndex"}
    },
    {
      $group: {
        _id: {
          bankType: "$paymentsByCard.detail.bankType",
          bankName: "$paymentsByCard.detail.bankName",
          _created: "$_created",
          billId: "$_id",
          prefix: "$prefix",
          daySerialNumber: "$daySerialNumber",
          serialNumber: "$serialNumber",
            name:'$paymentsByCard.cardType',
            itemIndex:"$itemIndex",
            _closeTime:'$_closeTime'
        },
        amount: {$first: "$paymentsByCard.detail.amount"}
      }
    },
    {
      $project: {
        _id: "$_id.billId",
        _created: "$_id._created",
        otherName: "$_id.otherName",
        prefix: "$_id.prefix",
        daySerialNumber: "$_id.daySerialNumber",
        serialNumber: "$_id.serialNumber",
         bankType: "$_id.bankType",
          bankName: "$_id.bankName",
           name:'$_id.name',
          amount:'$amount',
          _closeTime:'$_closeTime'
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if (err)
     d.reject(err);
     d.resolve(bills);
  });
  return d.promise;
}

exports.couponDetail = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep = req.body.arg.deployment_id ;

  Bill.aggregate([
    {
      $match : {
        _created: {
          $gte: moment(startDate).toDate(),
      $lte: moment(endDate).toDate()
        },
        deployment_id: new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
         $and: [{ 'payments.cards.totalAmount':{ $gt: 0 }},
          {"payments.cards.0": {$exists: true}}],
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$payments.cards"
    },
    {
      $project: {
        _created: 1,
        daySerialNumber: 1,
        serialNumber: 1,
        prefix: 1,
        otherPayments: {$cond: [{$eq: ["$payments.cards.cardType", "Coupon"]}, "$payments.cards", {}]}
      }
    },
    {
      $unwind: "$otherPayments.detail"
    },
    {
      $group: {
        _id: {
          otherName: "$otherPayments.detail.couponName",
         // otherType: "$otherPayments.detail.otherType",
          _created: "$_created",

          billId: "$_id",
          prefix: "$prefix",
          daySerialNumber: "$daySerialNumber",
          serialNumber: "$serialNumber"
        },
        amount: {$first: "$otherPayments.detail.amount"}
      }
    },
    {
      $project: {
        _id: "$_id.billId",
        _created: "$_id._created",
        otherName: "$_id.otherName",
        prefix: "$_id.prefix",
        daySerialNumber: "$_id.daySerialNumber",
        serialNumber: "$_id.serialNumber",
        amount:"$amount"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
     d.reject(err);
    d.resolve(bills);
  });
  return d.promise;
}

exports.newOtherPayment = function(req,res){
  var d = Q.defer();
  var startDate=new Date( new Date(req.query.startDate));
  var endDate=new Date( new Date(req.query.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  var dep = req.query.deployment_id ;
  console.log(req.query)
  Bill.aggregate([
    {
      $match : {
        _created: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        },
        deployment_id: new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$payments.cards"
    },
    {
      $project: {
        _created: 1,
        daySerialNumber: 1,
        serialNumber: 1,
        prefix: 1,
        otherPayments: {$cond: [{$eq: ["$payments.cards.cardType", "Other"]}, "$payments.cards", {}]}
      }
    },
    {
      $unwind: "$otherPayments.detail"
    },
    /*{
      $group: {
        _id: {
          otherName: "$otherPayments.detail.otherName",
         // otherType: "$otherPayments.detail.onlineType",
          _created: "$_created",
          billId: "$_id",
          prefix: "$prefix",
          daySerialNumber: "$daySerialNumber",
          serialNumber: "$serialNumber"
        },
        amount: {$sum: "$otherPayments.totalAmount"}
      }
    },*/
    {
      $project: {
         otherName: "$otherPayments.detail.otherName",
         // otherType: "$otherPayments.detail.onlineType",
          _created: "$_created",
          _id: "$_id",
          prefix: "$prefix",
          daySerialNumber: "$daySerialNumber",
          serialNumber: "$serialNumber",
          amount:"$otherPayments.detail.amount"
       
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
     d.reject(err);
   d.resolve(bills)
  });
  return d.promise;
}

exports.complimentary = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep_id = req.body.arg.deployment_id;

  Bill.find({
    "_created": {$gte: moment(startDate).toDate(), $lte: moment(endDate).toDate()},
    "deployment_id": dep_id,
    $or: [{"_kots.items.isDiscounted": true}, {"_offers.0": {$exists: true}}],
    isDeleted: {$ne: true},
    isVoid: {$ne: true}
  },{
    isVoid:1,
    _created: 1,
    tabType: 1,
    tab: 1,
    _closeTime:1,
    '_offers.offer.complimentary':1,
    '_offers.offerAmount':1,
    '_offers.user':1,
    prefix:1,
    billNumber:1,
    daySerialNumber:1,
    serialNumber:1,
    _covers:1,
    '_kots.isVoid':1,
    '_kots.items.name': 1,
    '_kots.items._id': 1,
    '_kots.items.quantity': 1,
    '_kots.items.subtotal': 1,
    '_kots.items.rate': 1,
    '_kots.items.category.categoryName':1,
    '_kots.items.category.superCategory.superCategoryName':1,
    '_kots.items.taxes._id':1,
    '_kots.items.taxes.name':1,
    '_kots.items.taxes.tax_amount':1,
    '_kots.items.taxes.baseRate_preTax':1,
    '_kots.items.taxes.cascadingTaxes._id':1,
    '_kots.items.taxes.cascadingTaxes.name':1,
    '_kots.items.taxes.cascadingTaxes.tax_amount':1,
    '_kots.items.taxes.cascadingTaxes.selected':1,
    '_kots.items.taxes.cascadingTaxes.baseRate_preTax':1,
    '_kots.items.addOns._id':1,
    '_kots.items.addOns.name':1,
    '_kots.items.addOns.rate':1,
    '_kots.items.addOns.subtotal':1,
    '_kots.items.addOns.quantity':1,
    '_kots.items.addOns.category.categoryName':1,
    '_kots.items.addOns.category.superCategory.superCategoryName':1,
    '_kots.items.addOns.taxes._id':1,
    '_kots.items.addOns.taxes.name':1,
    '_kots.items.addOns.taxes.tax_amount':1,
    '_kots.items.addOns.taxes.baseRate_preTax':1,
    '_kots.items.addOns.taxes.cascadingTaxes.baseRate_preTax':1,
    '_kots.items.addOns.taxes.cascadingTaxes._id':1,
    '_kots.items.addOns.taxes.cascadingTaxes.name':1,
    '_kots.items.addOns.taxes.cascadingTaxes.selected':1,
    '_kots.items.addOns.taxes.cascadingTaxes.tax_amount':1,
    '_kots.items.discounts.discountAmount':1,
    '_kots.items.discounts.type':1,
    '_kots.items.discounts.value':1,
    '_kots.items.discounts.comment':1,
    '_kots.items.discounts.user': 1,
    '_kots.items.isDiscounted':1,
    'instance.name':1,
    'instance.prefix':1,
    billDiscountAmount:1,
    '_customer.firstname':1,
    '_customer.lastname':1,
    '_currentUser.firstname':1,
    '_currentUser.lastname':1,
    '_currentUser.username':1,
    '_waiter.username':1,
    '_kots.items.comment':1,
    '_kots.waiter.username':1,
    'complimentaryHead':1,
    'complimentary': 1,
    'complimentaryComment': 1,
  },  function (err, bills) {
    if (err)
     d.reject(err);
   d.resolve(bills);
  });
  return d.promise;
}

exports.discountReport = function(req,res){
  var d = Q.defer()
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep_id = req.body.arg.deployment_id;

  Bill.find({
    "_created": {$gte: moment(startDate).toDate(), $lte: moment(endDate).toDate()},
    "deployment_id": dep_id,
    complimentary:{$ne: true},
    $or: [{"_kots.items.isDiscounted": true}, {"_offers.0": {$exists: true}}],
    isDeleted: {$ne: true},
    isVoid: {$ne: true}
  }, {
    _created: 1,
    tabType: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    serialNumber: 1,
    _offers: 1,
    '_customer.firstname': 1,
    '_customer._id': 1,
    '_currentUser.username': 1,
    '_kots.isVoid': 1,
    '_kots.items._id': 1,
    '_kots.items.quantity': 1,
    '_kots.items.subtotal': 1,
    '_kots.items.discounts.offerName': 1,
    '_kots.items.discounts._id': 1,
    '_kots.items.discounts.comment': 1,
    '_kots.waiter.username': 1,
    '_kots.items.addOns.subtotal': 1,
    '_kots.items.discounts.discountAmount': 1,
    '_kots.items.discounts.user': 1,
    '_kots.items.isDiscounted': 1,
    '_waiter.firstname': 1,
    '_delivery.firstname': 1,
  }, function (err, bills) {
    if (err)
      d.reject(err)
    //console.log(bills);
    d.resolve(bills);
  });
  return d.promise
}

exports.itemwiseDiscountReport = function(req,res){
  var d = Q.defer()
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);

  Bill.find(
  {
    _created: {$gte: moment(startDate).toDate(), $lte: moment(endDate).toDate()},
    $or: [{"_kots.items.isDiscounted": true}, {"_offers.0": {$exists: true}}],
    complimentary: {$ne: true},
    deployment_id: dep_id,
    isDeleted: {$ne: true},
    isVoid: {$ne: true}
  }, {
    _created: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    serialNumber: 1,
    _offers: 1,
    '_customer.firstname': 1,
    '_customer._id': 1,
    '_currentUser.username': 1,
    '_kots.isVoid': 1,
    '_kots.items._id': 1,
    '_kots.items.quantity': 1,
    '_kots.items.billDiscountAmount': 1,
    '_kots.items.subtotal': 1,
    '_kots.items.name': 1,
    '_kots.items.category': 1,
    '_kots.items.rate': 1,
    '_kots.items.discounts.offerName': 1,
    '_kots.items.discounts._id': 1,
    '_kots.items.discounts.comment': 1,
    '_kots.items.discounts.user': 1,
    '_kots.waiter.username': 1,
    '_kots.items.addOns._id': 1,
    '_kots.items.addOns.name': 1,
    '_kots.items.addOns.quantity': 1,
    '_kots.items.addOns.subtotal': 1,
    '_kots.items.discounts.discountAmount': 1,
    '_kots.items.isDiscounted': 1,
    '_waiter.username':1,
    '_delivery.username':1
  }, function (err, bills) {
    if (err)
      d.reject(err)
    d.resolve(bills);
  });
  return d.promise
}

exports.forInvoiceReport = function(req,res){
  var d = Q.defer();
  var fdate = new Date(req.query.fromDate);
  var tdate = new Date(req.query.toDate);
  tdate = tdate.setDate(tdate.getDate() + 1);
  //console.log(tdate);
  var _bills = [];
  var query = {
    //complimentary:req.query.complimentary,
    deployment_id: req.query.deployment_id,
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true}
  };
  //async.eachSeries(_dates, function (date, callback) {
  if (req.query.complimentary != undefined) {
    // console.log(req.query.complimentary);
    if (req.query.complimentary == 'true') {

      query = {
        complimentary: req.query.complimentary,
        deployment_id: req.query.deployment_id,
        _created: {
          $gte: new Date(fdate), $lte: new Date(tdate)
        },
        isDeleted: {$ne: true}
      }
    }
  }

  if(req.query.tabName != null && req.query.tabName != undefined && req.query.tabName != 'all')
    query.tab = req.query.tabName

  Bill.find(query,
  {
    isVoid: 1,
    _created: 1,
    tabType: 1,
    tab: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    complimentary: 1,
    serialNumber: 1,

    splitNumber: 1,
    payments: 1,
    _tableId: 1,
    _covers: 1,
    '_kots.isVoid': 1,
    '_kots.items.name': 1,
    '_kots.items._id': 1,
    '_kots.items.quantity': 1,
    '_kots.items.subtotal': 1,
    '_kots.items.rate': 1,
    '_kots.items.billDiscountAmount': 1,
    '_kots.items.category.categoryName': 1,
    '_kots.items.category.superCategory.superCategoryName': 1,
    '_kots.items.taxes._id': 1,
    '_kots.items.taxes.name': 1,
    '_kots.items.taxes.value': 1,
    '_kots.items.taxes.tax_amount': 1,
    '_kots.items.taxes.baseRate_preTax': 1,
    '_kots.items.taxes.cascadingTaxes._id': 1,
    '_kots.items.taxes.cascadingTaxes.name': 1,
    '_kots.items.taxes.cascadingTaxes.value': 1,
    '_kots.items.taxes.cascadingTaxes.tax_amount': 1,
    '_kots.items.taxes.cascadingTaxes.selected': 1,
    '_kots.items.taxes.cascadingTaxes.baseRate_preTax': 1,
    '_kots.items.addOns._id': 1,
    '_kots.items.addOns.name': 1,
    '_kots.items.addOns.rate': 1,
    '_kots.items.addOns.subtotal': 1,
    '_kots.items.addOns.quantity': 1,
    '_kots.items.addOns.category.categoryName': 1,
    '_kots.items.addOns.category.superCategory.superCategoryName': 1,
    '_kots.items.addOns.taxes._id': 1,
    '_kots.items.addOns.taxes.name': 1,
    '_kots.items.addOns.taxes.value': 1,
    '_kots.items.addOns.taxes.tax_amount': 1,
    '_kots.items.addOns.taxes.baseRate_preTax': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.baseRate_preTax': 1,
    '_kots.items.addOns.taxes.cascadingTaxes._id': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.name': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.value': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.selected': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.tax_amount': 1,
    '_kots.items.discounts.discountAmount': 1,
    '_kots.items.discounts.type': 1,
    '_kots.items.discount.value': 1,
    '_kots.items.isDiscounted': 1,
    "_offers.offer.complimentary": 1,
    "_offers.offerAmount": 1,
    'instance.name': 1,
    'instance.prefix': 1,
    charges:1,
    billDiscountAmount: 1
  }, function (err, bills) {
    if (err) {
      d.reject(err)
    }
    d.resolve(bills);
  });
  return d.promise
}

exports.instanceWiseReport = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep_id = req.body.arg.deployment_id;
  //console.log(req.body.arg.deployment_id);

  Bill.find({
    "_created": {$gte: moment(startDate).toDate(), $lte: moment(endDate).toDate()},
    "deployment_id": dep_id
  },{
    isVoid:1,
    _created: 1,
    _closeTime:1,
    prefix:1,
    billNumber:1,
    daySerialNumber:1,
    serialNumber:1,
    '_kots.isVoid':1,
    '_kots.items.name': 1,
    '_kots.items._id': 1,
    '_kots.items.quantity': 1,
    '_kots.items.subtotal': 1,
    '_kots.items.rate': 1,
    '_kots.items.category.categoryName':1,
    '_kots.items.category.superCategory.superCategoryName':1,
    '_kots.items.taxes._id':1,
    '_kots.items.taxes.name':1,
    '_kots.items.taxes.tax_amount':1,
    '_kots.items.taxes.baseRate_preTax':1,
    '_kots.items.taxes.cascadingTaxes._id':1,
    '_kots.items.taxes.cascadingTaxes.name':1,
    '_kots.items.taxes.cascadingTaxes.tax_amount':1,
    '_kots.items.taxes.cascadingTaxes.selected':1,
    '_kots.items.taxes.cascadingTaxes.baseRate_preTax':1,
    '_kots.items.addOns._id':1,
    '_kots.items.addOns.name':1,
    '_kots.items.addOns.rate':1,
    '_kots.items.addOns.subtotal':1,
    '_kots.items.addOns.quantity':1,
    '_kots.items.addOns.category.categoryName':1,
    '_kots.items.addOns.category.superCategory.superCategoryName':1,
    '_kots.items.addOns.taxes._id':1,
    '_kots.items.addOns.taxes.name':1,
    '_kots.items.addOns.taxes.tax_amount':1,
    '_kots.items.addOns.taxes.baseRate_preTax':1,
    '_kots.items.addOns.taxes.cascadingTaxes.baseRate_preTax':1,
    '_kots.items.addOns.taxes.cascadingTaxes._id':1,
    '_kots.items.addOns.taxes.cascadingTaxes.name':1,
    '_kots.items.addOns.taxes.cascadingTaxes.selected':1,
    '_kots.items.addOns.taxes.cascadingTaxes.tax_amount':1,
    '_kots.items.discounts.discountAmount':1,
    '_kots.items.discounts.type':1,
    '_kots.items.discount.value':1,
    '_kots.items.isDiscounted':1,
    'instance.name':1,
    'instance.prefix':1,
    billDiscountAmount:1,
    charges:1
  }, function (err, bills) {
    if (err)
      d.reject(err)
    d.resolve(bills);
  });
  return d.promise
}

exports.reprintReport = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep_id = req.body.arg.deployment_id;
  //console.log(req.body.arg.deployment_id);
  Bill.find({
    "_created": {$gte: moment(startDate).toDate(), $lte: moment(endDate).toDate()},
    "deployment_id": dep_id, "rePrint.1": {$exists: true}, isDeleted: {$ne: true},
    isVoid: {$ne: true}
  }, {
    _created: 1,
    tabType: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    serialNumber: 1,
    rePrint: 1,
    '_currentUser.firstname': 1,
    '_currentUser.lastname': 1
  }, function (err, bills) {
    if (err)
     d.reject(err)
   d.resolve(bills);
  });
  return d.promise
}

exports.billsInDateRange = function(req , res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
  var query = {
    deployment_id: new mongoose.Types.ObjectId(req.body.arg.deployment_id),
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true},
    isVoid: {$ne:true}
  }
  
  Bill.find(query, function (err, bills) {
    if (err) {
      d.reject(err);
    }
    d.resolve(bills);
  });
  return d.promise;
}

exports.ItemWiseConsolidateForProfitSharing = function(req,res){
  var d = Q.defer();
  var startDate = new Date(new Date(req.body.arg.startDate));
  var endDate = new Date(new Date(req.body.arg.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  console.log(startDate);
  console.log(endDate);
  var dep = req.body.arg.deployment_id;
  console.log("dep" + dep);
  //var deployemnts=[];
  /* _.forEach(req.body.arg.deps,function(dep){
   console.log(dep);
   // deployemnts.push(new mongoose.Types.ObjectId(dep._id));
   });*/
  //  console.log(deployemnts);
  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id: new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    // Stage 3
    {
      $project: {
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _id: "$_id",
        deployment_id: "$deployment_id",

      }
    },
    // Stage 4
    {
      $unwind: {path: "$kot.items", includeArrayIndex: "itemIndexKot"}
    },
    {
      $project: {
        "kot.items._id": 1,
        "kot.items.name": 1,
        "kot.kotNumber": 1,
        "kot.items.rate": 1,
        deployment_id: "$deployment_id",
        "kot.items.addOns.name": 1,
        "kot.items.addOns._id": 1,
        "kot.items.addOns.quantity": 1,
        "kot.items.addOns.subtotal": 1,
        "kot.items.addOns.category._id": 1,
        "kot.items.addOns.rate": 1,
        "kot.items.addOns.category.categoryName": 1,
        "kot.items.addOns.category.superCategory._id": 1,
        "kot.items.addOns.category.superCategory.superCategoryName": 1,
        "kot.items.discounts.discountAmount": 1,
        "kot.items.subtotal": 1,
        "kot.items.quantity": 1,
        "kot.items.category._id": 1,
        "kot.items.category.categoryName": 1,
        "kot.items.category.superCategory._id": 1,
        "kot.items.category.superCategory.superCategoryName": 1,
        "kot.items.kotIndex": "$itemIndexKot",
        "_id": "$_id"
      }
    },
    {
      $project: {
        items: ["$kot.items"],
        kotNumber: "$kot.kotNumber",
        _id: "$_id",
        "addOns": {
          $cond: {
            if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]},
            then: "$kot.items.addOns",
            else: []
          }
        },

      }
    },
    {$project: {items: {$concatArrays: ["$items", "$addOns"]}, kotNumber: "$kotNumber", _id: "$_id"}},
    {$unwind: {path: "$items", preserveNullAndEmptyArrays: true, includeArrayIndex: "itemIndex"}},
    {
      $project: {
        "items._id": "$items._id",
        "items.name": "$items.name",
        "items.index": "$itemIndex",
        "items.subtotal": "$items.subtotal",
        "items.quantity": "$items.quantity",
        "items.kotIndex": "$items.kotIndex",
        "items.rate": "$items.rate",
        kotNumber: "$kotNumber",
        _id: "$_id",
        "items.category": "$items.category",
        "items.discounts": {
          $cond: {
            if: {$and: ["$items.discounts", {$gt: [{$size: "$items.discounts"}, 0]}]},
            then: "$items.discounts",
            else: []
          }
        },
      }
    },
    {$unwind: {path: "$items.discounts", preserveNullAndEmptyArrays: true}},
    {
      $group: {
        _id: {
          itemId: "$items._id",
          kotNumber: "$kotNumber",
          _id: "$_id",
          kotIndex: "$items.kotIndex",
          itemName: "$items.name",
          itemQuantity: "$items.quantity",
          itemIndex: "$items.index",
          itemSubtotal: "$items.subtotal",
          itemRate: "$items.rate",
          category: "$items.category"
        },
        discountAmount: {$sum: "$items.discounts.discountAmount"}
      }
    },
    {
      $project: {
        "items._id": "$_id.itemId",
        "items.name": "$_id.itemName",
        "items.index": "$_id.itemIndex",
        "items.subtotal": "$_id.itemSubtotal",
        "items.quantity": "$_id.itemQuantity",
        "items.kotIndex": "$_id.kotIndex",
        "items.rate": "$_id.itemRate",
        kotNumber: "$_id.kotNumber",
        _id: "$_id._id",
        "items.category": "$_id.category",
        discountAmount: "$discountAmount"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", itemName: "$items.name", category: "$items.category", itemRate: "$items.rate"
        },
        subtotal: {$sum: "$items.subtotal"},
        discountAmount: {$sum: "$discountAmount"},
        quantity: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        "name": "$_id.itemName",
        "quantity": "$quantity",
        "subtotal": "$subtotal",
        "discountAmount": "$discountAmount",
        "category": "$_id.category",
        "rate": "$_id.itemRate"

      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    console.log(err);
    if (err) {
      d.reject(err);
    }
    d.resolve(bills);
  });
  return d.promise;
}

exports.deliveryBoy = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);

  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id: deployment_id,
        isDeleted: {$ne: true},
        tabType: "delivery",
        _isDelivery: {$eq: true},
        isVoid:{$ne:true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: "$_id",
        //kot:{$cond: ['$_kots.isVoid', {},"$_kots"]},
        totalNet: {$cond: ['$_kots.isVoid', 0, {$subtract: [{$add: ["$_kots.totalAmount", "$_kots.taxAmount"]}, "$_kots.totalDiscount"]}]},
        deployment_id: "$deployment_id",
        _delivery: {
          firstname: "$_delivery.firstname",
          lastname: "$_delivery.lastname",
          username:"$_delivery.username",
          _id: "$_delivery._id",
          deliveryTime: "$_delivery.deliveryTime"
        },
        customer:{
          firstname:"$_customer.firstname",
          phone:"$_customer.phone",
          mobile:"$_customer.mobile",
          address1:"$_customer.address1",
          address2:"$_customer.address2",
          city:"$_customer.city",
          state:"$_customer.state",
          _id:"$_customer._id",
        },
        billPrintTime: "$billPrintTime",
        _created: "$_created",
        _closeTime: "$_closeTime",
        tabType: "$tabType",
        prefix: "$prefix",
        billNumber: "$billNumber",
        serialNumber: "$serialNumber",
        daySerialNumber: "$daySerialNumber",
        billDiscountAmount: "$billDiscountAmount",
        '_kots.items.name':1,
        '_kots.items.quantity':1,
        '_kots.items.category.categoryName':1,
        '_kots.items.addOns.name':1,
        '_kots.items.addOns.quantity':1,
        '_kots.items.addOns.category.categoryName':1,
        'onlineBill.source.name':1,
        chargesAmount:  { $ifNull: [ "$charges.amount", 0 ] }
      }
    },
    {
      $group: {
        _id: {
          _id: "$_id", deployment_id: "$deployment_id", _delivery: "$_delivery", _created: "$_created",
          _closeTime: "$_closeTime",
          _customer:"$customer",
          billPrintTime: "$billPrintTime",
          prefix: "$prefix",
          billNumber: "$billNumber",
          serialNumber: "$serialNumber",
          billDiscountAmount: "$billDiscountAmount",
          daySerialNumber: "$daySerialNumber",
          onlineBill:"$onlineBill"

        },
        chargesAmount:{$first:'$chargesAmount'},
        kot:{$push:"$_kots"},
        totalNet: {$sum: "$totalNet"}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        _delivery: "$_id._delivery",
        totalNet:{$subtract:[{$add:["$totalNet","$chargesAmount"]},"$_id.billDiscountAmount"]},
        _created: "$_id._created",
        _customer:"$_id._customer",
        _closeTime: "$_id._closeTime",
        tabType: "$_id.tabType",
        //_covers:"$_id._covers",
        prefix: "$_id.prefix",
        billPrintTime: "$_id.billPrintTime",
        billNumber: "$_id.billNumber",
        serialNumber: "$_id.serialNumber",
        daySerialNumber: "$_id.daySerialNumber",
        _kots:"$kot",
        onlineBill:"$_id.onlineBill"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if (err) {
      // console.log(err)
      d.reject(err);
    }
    // console.log(bills)
    d.resolve(bills);
  });
  return d.promise
}

exports.billConslidateAggregation = function(req,res){
  var d =Q.defer();
  var startDate=new Date( new Date(req.body.arg.startDate));
  var endDate=new Date( new Date(req.body.arg.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  console.log(startDate);
  console.log(endDate);
  var dep = req.body.arg.deployment_id ;
  console.log("dep"+dep);

  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id:new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
        isVoid:{$ne:true}
      }
    },
    {
      $unwind:"$_kots"
    },
    {
      $project: {
        _id: "$_id",
        totalNet:{$cond: ['$_kots.isVoid', 0, {$subtract:[{$add:["$_kots.totalAmount","$_kots.taxAmount"]},"$_kots.totalDiscount"]}]},
        deployment_id: "$deployment_id",
        customer:{
          email:"$_customer.email",
          firstname:"$_customer.firstname",
          phone:"$_customer.phone",
          mobile:"$_customer.mobile",
          address1:"$_customer.address1",
          address2:"$_customer.address2",
          city:"$_customer.city",
          state:"$_customer.state",
          _id:"$_customer._id",
        },
        _created:"$_created",
        _closeTime:"$_closeTime",
        tabType:"$tabType",
        _covers:"$_covers",
        prefix:"$prefix",
        billNumber:"$billNumber",
        serialNumber:"$serialNumber",
        daySerialNumber:"$daySerialNumber",
        billDiscountAmount:"$billDiscountAmount",
        chargesAmount:  { $ifNull: [ "$charges.amount", 0 ] }
      }
    },
    {
      $group:{
        _id:{ _id: "$_id", deployment_id: "$deployment_id",customer:"$customer",_created:"$_created",
          _closeTime:"$_closeTime",
          tabType:"$tabType",
          _covers:"$_covers",
          prefix:"$prefix",
          billNumber:"$billNumber",
          serialNumber:"$serialNumber",
          billDiscountAmount: "$billDiscountAmount",
          daySerialNumber:"$daySerialNumber"},
          chargesAmount:{$first:"$chargesAmount"},
        totalNet:{$sum : "$totalNet"}
      }
    },
    {
      $project:{
        _id:"$_id._id",
        deployment_id:"$_id.deployment_id",
        _customer:"$_id.customer",
         chargesAmount:'$chargesAmount',
        totalNet:{$subtract:[{$add:["$totalNet","$chargesAmount"]},"$_id.billDiscountAmount"]},
        _created:"$_id._created",
        _closeTime:"$_id._closeTime",
        tabType:"$_id.tabType",
        _covers:"$_id._covers",
        prefix:"$_id.prefix",
        billNumber:"$_id.billNumber",
        serialNumber:"$_id.serialNumber",
        daySerialNumber:"$_id.daySerialNumber"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    console.log(err);
    if (err) {
      d.reject(err);
    }
   d.resolve(bills);
  });
  return d.promise
}

exports.forKotReport = function(req,res){
  var d = Q.defer();
  var fdate = new Date(req.query.fromDate);
  var tdate = new Date(req.query.toDate);
  tdate = tdate.setDate(tdate.getDate() + 1);
  console.log(tdate);

  var _bills = [];
  var query = {
    //complimentary:req.query.complimentary,
    tenant_id: req.query.tenant_id,
    deployment_id: req.query.deployment_id,
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true}
  };
  //async.eachSeries(_dates, function (date, callback) {
  if (req.query.complimentary != undefined) {
    // console.log(req.query.complimentary);
    if (req.query.complimentary == 'true') {

      query = {
        complimentary: req.query.complimentary,
        tenant_id: req.query.tenant_id,
        deployment_id: req.query.deployment_id,
        _created: {
          $gte: new Date(fdate), $lte: new Date(tdate)
        },
        isDeleted: {$ne: true}
      }
    }
  }

  Bill.find(query,
  {
    isVoid: 1,
    _created: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    billDiscountAmount: 1,
    serialNumber: 1,
    '_kots.isVoid': 1,
    '_kots.kotNumber': 1,
    '_kots.items.name': 1,
    '_kots.items._id': 1,
    '_kots.items.quantity': 1,
    '_kots.items.subtotal': 1,
    '_kots.items.rate': 1,

    '_kots.items.addOns._id': 1,
    '_kots.items.addOns.name': 1,
    '_kots.items.addOns.rate': 1,
    '_kots.items.addOns.subtotal': 1,
    '_kots.items.addOns.quantity': 1
  }, function (err, bills) {
    if (err) {
      d.reject(err);
    }
    d.resolve(bills);
  })
  return d.promise
}

exports.itemWiseConsolidateAggregation = function(req,res){
  var d = Q.defer();
  var startDate = new Date(new Date(req.body.arg.startDate));
  var endDate = new Date(new Date(req.body.arg.endDate));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  console.log(startDate);
  console.log(endDate);
  var dep = req.body.arg.deployment_id;
  console.log("dep" + dep);
  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id: new mongoose.Types.ObjectId(dep),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: "$_id",
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        deployment_id: "$deployment_id",
        complimentary: "$complimentary"
      }
    },
    {
      $unwind: {path: "$kot.items", includeArrayIndex: "itemIndex"}
    },
    {
      $project: {
        _id: "$_id",
        deployment_id: "$deployment_id",
        complimentary: "$complimentary",
        "kot.items._id": "$kot.items._id",
        "kot.items.name": "$kot.items.name",
        "kot.items.index": "$itemIndex",
        "kot.kotNumber": "$kot.kotNumber",
        "kot.items.rate": "$kot.items.rate",
        "kot.items.billDiscountAmount": { $ifNull: [ "$kot.items.billDiscountAmount", 0 ] },
        "kot.items.taxes": {
          $cond: {
            if: {$and: ["$kot.items.taxes", {$gt: [{$size: "$kot.items.taxes"}, 0]}]},
            then: "$kot.items.taxes",
            else: []
          }
        },
        "kot.items.addOns": {
          $cond: {
            if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]},
            then: "$kot.items.addOns",
            else: []
          }
        },
        "kot.items.discounts": {
          $cond: {
            if: {$and: ["$kot.items.discounts", {$gt: [{$size: "$kot.items.discounts"}, 0]}]},
            then: "$kot.items.discounts",
            else: []
          }
        },
        "kot.items.subtotal": "$kot.items.subtotal",
        "kot.items.noDiscountAmount": "$kot.items.subtotal",
        "kot.items.quantity": "$kot.items.quantity",
        "kot.items.category._id": "$kot.items.category._id",
        "kot.items.category.categoryName": "$kot.items.category.categoryName",
        "kot.items.category.superCategory._id": "$kot.items.category.superCategory._id",
        "kot.items.category.superCategory.superCategoryName": "$kot.items.category.superCategory.superCategoryName",

      }
    },
    {
      $unwind: {path: "$kot.items.taxes", preserveNullAndEmptyArrays: true}
    },
    {
      $project: {
        _id: "$_id",
        deployment_id: "$deployment_id",
        complimentary: "$complimentary",
        "kot.items._id": "$kot.items._id",
        "kot.items.index": "$kot.items.index",
        "kot.items.name": "$kot.items.name",
        "kot.kotNumber": "$kot.kotNumber",
        "kot.items.rate": "$kot.items.rate",
        "kot.items.addOns": "$kot.items.addOns",
        "kot.items.discounts": "$kot.items.discounts",
        "kot.items.subtotal":"$kot.items.subtotal",
        "kot.items.noDiscountAmount":"$kot.items.noDiscountAmount",
        "kot.items.quantity": "$kot.items.quantity",
        "kot.items.billDiscountAmount":"$kot.items.billDiscountAmount",
        "kot.items.category": "$kot.items.category",
        "kot.items.taxes._id": "$kot.items.taxes._id",
        "kot.items.taxes.tax_amount": "$kot.items.taxes.tax_amount",
        "kot.items.taxes.cascadingTaxes": {
          $cond: {
            if: {$and: ["$kot.items.taxes.cascadingTaxes", {$gt: [{$size: "$kot.items.taxes.cascadingTaxes"}, 0]}]},
            then: "$kot.items.taxes.cascadingTaxes",
            else: []
          }
        },
      }
    },
    {
      $unwind: {path: "$kot.items.taxes.cascadingTaxes", preserveNullAndEmptyArrays: true}
    },
    {
      $group: {
        _id: {
          tax_id: "$kot.items.taxes._id",
          deployment_id: "$deployment_id",
          kotNumber: "$kot.kotNumber",
          item_id: "$kot.items._id",
          _id: "$_id",
          itemName: "$kot.items.name",
          itemRate: "$kot.items.rate",
          addOns: "$kot.items.addOns",
          discounts: "$kot.items.discounts",
          itemSubtotal: "$kot.items.subtotal",
          itemQuantity: "$kot.items.quantity",
          itemBillDiscount:"$kot.items.billDiscountAmount",
          category: "$kot.items.category",
          tax_amount: "$kot.items.taxes.tax_amount",
          complimentary: "$complimentary",
          itemIndex: "$kot.items.index",
          noDiscountAmount:"$kot.items.noDiscountAmount"
        },
        cascadingTaxes: {$sum: "$kot.items.taxes.cascadingTaxes.tax_amount"},
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        complimentary: "$_id.complimentary",
        "kot.items._id": "$_id.item_id",
        "kot.items.index": "$_id.itemIndex",
        "kot.items.name": "$_id.itemName",
        "kot.kotNumber": "$_id.kotNumber",
        "kot.items.rate": "$_id.itemRate",
        "kot.items.addOns": "$_id.addOns",
        "kot.items.discounts": "$_id.discounts",
        "kot.items.subtotal": "$_id.itemSubtotal",
        "kot.items.quantity": "$_id.itemQuantity",
        "kot.items.category": "$_id.category",
        "kot.items.taxes._id": "$_id.tax_id",
        "kot.items.noDiscountAmount":"$_id.noDiscountAmount",
        "kot.items.billDiscountAmount":"$_id.itemBillDiscount",
        "kot.items.taxes.tax_amount": {$add: ["$_id.tax_amount", "$cascadingTaxes"]}
      }
    },
    {
      $group: {
        _id: {
          item_id: "$kot.items._id",
          _id: "$_id",
          deployment_id: "$deployment_id",
          kotNumber: "$kot.kotNumber",
          itemName: "$kot.items.name",
          addOns: "$kot.items.addOns",
          discounts: "$kot.items.discounts",
          itemSubtotal: "$kot.items.subtotal",
          itemQuantity: "$kot.items.quantity",
          category: "$kot.items.category",
          itemRate: "$kot.items.rate",
          complimentary: "$complimentary",
          itemIndex: "$kot.items.index",
          itemBillDiscount:"$kot.items.billDiscountAmount",
          noDiscountAmount:"$kot.items.noDiscountAmount"
        },
        taxAmount: {$sum: "$kot.items.taxes.tax_amount"}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        complimentary: "$_id.complimentary",
        "kot.items._id": "$_id.item_id",
        "kot.items.index": "$_id.itemIndex",
        "kot.items.name": "$_id.itemName",
        "kot.kotNumber": "$_id.kotNumber",
        "kot.items.rate": "$_id.itemRate",
        "kot.items.addOns": "$_id.addOns",
        "kot.items.discounts": "$_id.discounts",
        "kot.items.subtotal": "$_id.itemSubtotal",
        "kot.items.taxAmount": "$taxAmount",
        "kot.items.quantity": "$_id.itemQuantity",
        "kot.items.category": "$_id.category",
        "kot.items.billDiscountAmount":"$_id.itemBillDiscount",
        "kot.items.noDiscountAmount":"$_id.noDiscountAmount"
      }
    },
    {
      $unwind: {path: "$kot.items.discounts", preserveNullAndEmptyArrays: true}
    },
    {
      $group: {
        _id: {
          item_id: "$kot.items._id",
          _id: "$_id",
          deployment_id: "$deployment_id",
          itemName: "$kot.items.name",
          kotNumber: "$kot.kotNumber",
          addOns: "$kot.items.addOns",
          itemSubtotal: "$kot.items.subtotal",
          itemQuantity: "$kot.items.quantity",
          category: "$kot.items.category",
          itemRate: "$kot.items.rate",
          complimentary: "$complimentary",
          tax_amount: "$kot.items.taxAmount",
          itemIndex: "$kot.items.index",
          itemBillDiscount:"$kot.items.billDiscountAmount",
          noDiscountAmount:"$kot.items.noDiscountAmount"
        },
        discountAmount: {$sum: "$kot.items.discounts.discountAmount"}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        complimentary: "$_id.complimentary",
        "kot.items._id": "$_id.item_id",
        "kot.items.index": "$_id.itemIndex",
        "kot.items.name": "$_id.itemName",
        "kot.kotNumber": "$_id.kotNumber",
        "kot.items.rate": "$_id.itemRate",
        "kot.items.addOns": "$_id.addOns",
        "kot.items.itemWiseDiscount":"$discountAmount",
        "kot.items.subtotal": "$_id.itemSubtotal",
        "kot.items.discountAmount": {$add:["$discountAmount","$_id.itemBillDiscount"]},

        "kot.items.billDiscountAmount":"$_id.itemBillDiscount",
        "kot.items.noDiscountAmount":"$_id.noDiscountAmount",
      
        "kot.items.taxAmount": "$_id.tax_amount",
        "kot.items.quantity": "$_id.itemQuantity",
        "kot.items.complimentary": {
          $cond: {
            if: {$or: [{$eq: ["$_id.complimentary", true]}, {$eq: ["$_id.itemSubtotal", "$discountAmount"]}]},
            then: "$_id.itemQuantity",
            else: 0
          }
        },
        "kot.items.category": "$_id.category",
      }
    },
    {
      $unwind: {path: "$kot.items.addOns", preserveNullAndEmptyArrays: true}
    },
    {
      $project: {
        _id: "$_id",
        deployment_id: "$deployment_id",
        complimentary: "$complimentary",
        "kot.items._id": "$kot.items._id",
        "kot.items.index": "$kot.items.index",
        "kot.items.name": "$kot.items.name",
        "kot.kotNumber": "$kot.kotNumber",
        "kot.items.rate": "$kot.items.rate",
        "kot.items.subtotal": "$kot.items.subtotal",
        "kot.items.discountAmount": "$kot.items.discountAmount",
        "kot.items.taxAmount": "$kot.items.taxAmount",
        "kot.items.complimentary": "$kot.items.complimentary",
        "kot.items.quantity": "$kot.items.quantity",
        "kot.items.category": "$kot.items.category",
        "kot.items.addOns._id": "$kot.items.addOns._id",
        "kot.items.addOns.name": "$kot.items.addOns.name",
        "kot.items.addOns.subtotal": "$kot.items.addOns.subtotal",
        "kot.items.addOns.rate": "$kot.items.addOns.rate",
        "kot.items.addOns.quantity": "$kot.items.addOns.quantity",
        "kot.items.addOns.category._id": "$kot.items.addOns.category._id",
        "kot.items.addOns.category.categoryName": "$kot.items.addOns.category.categoryName",
        "kot.items.addOns.category.superCategory._id": "$kot.items.addOns.category.superCategory._id",
        "kot.items.addOns.category.superCategory.superCategoryName": "$kot.items.addOns.category.superCategory.superCategoryName",
        "kot.items.billDiscountAmount":"$kot.items.billDiscountAmount",
        "kot.items.addOns.taxes": {
          $cond: {
            if: {$and: ["$kot.items.addOns.taxes", {$gt: [{$size: "$kot.items.addOns.taxes"}, 0]}]},
            then: "$kot.items.addOns.taxes",
            else: []
          }
        },
      }
    },
    {
      $unwind: {path: "$kot.items.addOns.taxes", preserveNullAndEmptyArrays: true}
    },
    {
      $project: {
        _id: "$_id",
        deployment_id: "$deployment_id",
        complimentary: "$complimentary",
        "kot.items._id": "$kot.items._id",
        "kot.items.index": "$kot.items.index",
        "kot.items.name": "$kot.items.name",
        "kot.kotNumber": "$kot.kotNumber",
        "kot.items.rate": "$kot.items.rate",
        "kot.items.subtotal": "$kot.items.subtotal",
        "kot.items.quantity": "$kot.items.quantity",
        "kot.items.discountAmount": "$kot.items.discountAmount",
        "kot.items.taxAmount": "$kot.items.taxAmount",
        "kot.items.complimentary": "$kot.items.complimentary",
        "kot.items.category": "$kot.items.category",
        "kot.items.addOns._id": "$kot.items.addOns._id",
        "kot.items.addOns.name": "$kot.items.addOns.name",
        "kot.items.addOns.subtotal": "$kot.items.addOns.subtotal",
        "kot.items.billDiscountAmount":"$kot.items.billDiscountAmount",
        "kot.items.addOns.rate": "$kot.items.addOns.rate",
        "kot.items.addOns.quantity": "$kot.items.addOns.quantity",
        "kot.items.addOns.category": "$kot.items.addOns.category",
        "kot.items.addOns.taxes._id": "$kot.items.addOns.taxes._id",
        "kot.items.addOns.taxes.tax_amount": "$kot.items.addOns.taxes.tax_amount",
        "kot.items.addOns.taxes.cascadingTaxes": {
          $cond: {
            if: {$and: ["$kot.items.addOns.taxes.cascadingTaxes", {$gt: [{$size: "$kot.items.addOns.taxes.cascadingTaxes"}, 0]}]},
            then: "$kot.items.addOns.taxes.cascadingTaxes",
            else: []
          }
        },
      }
    },
    {
      $unwind: {path: "$kot.items.addOns.taxes.cascadingTaxes", preserveNullAndEmptyArrays: true}
    },
    {
      $group: {
        _id: {
          item_id: "$kot.items._id",
          _id: "$_id",
          deployment_id: "$deployment_id",
          itemName: "$kot.items.name",
          itemSubtotal: "$kot.items.subtotal",
          kotNumber: "$kot.kotNumber",
          itemRate: "$kot.items.rate",
          addOnRate: "$kot.items.addOns.rate",
          itemQuantity: "$kot.items.quantity",
          "discountAmount": "$kot.items.discountAmount",
          taxAmount: "$kot.items.taxAmount",
          billComplimentary: "$complimentary",
          complimentary: "$kot.items.complimentary",
          "addOnId": "$kot.items.addOns._id",
          addOnName: "$kot.items.addOns.name",
          addOnCategory: "$kot.items.addOns.category",
          addOnTaxId: "$kot.items.addOns.taxes._id",
          addOnSubtotal: "$kot.items.addOns.subtotal",
          addOnQuantity: "$kot.items.addOns.quantity",
          addOnTaxAmount: "$kot.items.addOns.taxes.tax_amount",
          category: "$kot.items.category",
          itemIndex: "$kot.items.index",
          itemBillDiscount:"$kot.items.billDiscountAmount",
        },
        cascadingTaxes: {$sum: "$kot.items.addOns.taxes.cascadingTaxes.tax_amount"}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        complimentary: "$_id.billComplimentary",
        "kot.items._id": "$_id.item_id",
        "kot.items.index": "$_id.itemIndex",
        "kot.items.name": "$_id.itemName",
        "kot.kotNumber": "$_id.kotNumber",
        "kot.items.rate": "$_id.itemRate",
        "kot.items.subtotal": "$_id.itemSubtotal",
        "kot.items.quantity": "$_id.itemQuantity",
        "kot.items.discountAmount": "$_id.discountAmount",
        "kot.items.taxAmount": "$_id.taxAmount",
        "kot.items.complimentary": "$_id.complimentary",
        "kot.items.category": "$_id.category",
        "kot.items.addOns._id": "$_id.addOnId",
        "kot.items.addOns.name": "$_id.addOnName",
        "kot.items.addOns.rate": "$_id.addOnRate",
        "kot.items.addOns.subtotal": "$_id.addOnSubtotal",
        "kot.items.addOns.quantity": "$_id.addOnQuantity",
        "kot.items.addOns.category": "$_id.addOnCategory",
        "kot.items.addOns.taxes._id": "$_id.addOnTaxId",
        "kot.items.billDiscountAmount":"$_id.itemBillDiscount",
        "kot.items.addOns.taxes.tax_amount": {$add: ["$_id.addOnTaxAmount", "$cascadingTaxes"]},
      }
    },
    {
      $group: {
        _id: {
          item_id: "$kot.items._id",
          _id: "$_id",
          deployment_id: "$deployment_id",
          itemName: "$kot.items.name",
          itemSubtotal: "$kot.items.subtotal",
          itemRate: "$kot.items.rate",
          kotNumber: "$kot.kotNumber",
          itemQuantity: "$kot.items.quantity",
          "discountAmount": "$kot.items.discountAmount",
          taxAmount: "$kot.items.taxAmount",
          billComplimentary: "$complimentary",
          complimentary: "$kot.items.complimentary",
          "addOnId": "$kot.items.addOns._id",
          addOnName: "$kot.items.addOns.name",
          addOnCategory: "$kot.items.addOns.category",
          category: "$kot.items.category",
          addOnSubtotal: "$kot.items.addOns.subtotal",
          addOnRate: "$kot.items.addOns.rate",
          addOnQuantity: "$kot.items.addOns.quantity",
          itemIndex: "$kot.items.index",
          itemBillDiscount:"$kot.items.billDiscountAmount"
        },
        addOnTaxAmount: {$sum: "$kot.items.addOns.taxes.tax_amount"}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        complimentary: "$_id.billComplimentary",
        "kot.items._id": "$_id.item_id",
        "kot.items.index": "$_id.itemIndex",
        "kot.items.name": "$_id.itemName",
        "kot.kotNumber": "$_id.kotNumber",
        "kot.items.rate": "$_id.itemRate",
        "kot.items.subtotal": "$_id.itemSubtotal",
        "kot.items.quantity": "$_id.itemQuantity",
        "kot.items.discountAmount": "$_id.discountAmount",
        "kot.items.taxAmount": "$_id.taxAmount",
        "kot.items.complimentary": "$_id.complimentary",
        "kot.items.category": "$_id.category",
        "kot.items.addOns._id": "$_id.addOnId",
        "kot.items.addOns.name": "$_id.addOnName",
        "kot.items.addOns.rate": "$_id.addOnRate",
        "kot.items.addOns.subtotal": "$_id.addOnSubtotal",
        "kot.items.addOns.quantity": "$_id.addOnQuantity",
        "kot.items.addOns.category": "$_id.addOnCategory",
        "kot.items.billDiscountAmount":"$_id.itemBillDiscount",
        "kot.items.addOns.complimentary": {$cond: ["$_id.billComplimentary", "_id.addOnQuantity", 0]},
        "kot.items.addOns.taxAmount": "$addOnTaxAmount"
      }
    },
    {
      $group: {
        _id: {
          item_id: "$kot.items._id",
          _id: "$_id",
          deployment_id: "$deployment_id",
          itemName: "$kot.items.name",
          itemSubtotal: "$kot.items.subtotal",
          itemRate: "$kot.items.rate",
          kotNumber: "$kot.kotNumber",
          itemQuantity: "$kot.items.quantity",
          "discountAmount": "$kot.items.discountAmount",
          taxAmount: "$kot.items.taxAmount",
          billComplimentary: "$complimentary",
          complimentary: "$kot.items.complimentary",
          category: "$kot.items.category",
          itemIndex: "$kot.items.index",
          itemBillDiscount:"$kot.items.billDiscountAmount"
        },
        addOns: {$push: {$cond: ["$kot.items.addOns.name", "$kot.items.addOns", {}]}}
      }
    },
    {
      $project: {
        _id: "$_id._id",
        deployment_id: "$_id.deployment_id",
        complimentary: "$_id.billComplimentary",
        "kot.items._id": "$_id.item_id",
        "kot.items.index": "$_id.itemIndex",
        "kot.items.name": "$_id.itemName",
        "kot.kotNumber": "$_id.kotNumber",
        "kot.items.rate": "$_id.itemRate",
        "kot.items.subtotal": {$cond: [{$eq: ["$_id.billComplimentary", true]}, 0, "$_id.itemSubtotal"]},
         "kot.items.billDiscountAmount": {$cond: [{$eq: ["$_id.billComplimentary", true]}, 0, "$_id.itemBillDiscount"]},
        "kot.items.quantity": "$_id.itemQuantity",
        "kot.items.discountAmount": {$cond: [{$eq: ["$_id.billComplimentary", true]}, 0, "$_id.discountAmount"]},
        "kot.items.taxAmount": "$_id.taxAmount",
        "kot.items.complimentary": "$_id.complimentary",
        //"kot.items.billDiscountAmount":"$_id.itemBillDiscount",
        "kot.items.category": "$_id.category",
        "kot.items.addOns": "$addOns"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$kot.items._id",
          itemName: "$kot.items.name",
          deployment_id: "$deployment_id",
          //rate: "$kot.items.rate",
          categoryName: "$kot.items.category.categoryName",
          categoryId: "$kot.items.category._id",
          superCategoryName: "$kot.items.category.superCategory.superCategoryName",
          superCategoryId: "$kot.items.category.superCategory._id",
          //itemBillDiscount:"$kot.items.billDiscountAmount"
        },
        rate:{$first:"$kot.items.rate"},
        itemBillDiscount:{$sum: "$kot.items.billDiscountAmount"},
        subtotal: {$sum: "$kot.items.subtotal"},
        quantity: {$sum: "$kot.items.quantity"},
        discountAmount: {$sum: "$kot.items.discountAmount"},
        taxAmount: {$sum: "$kot.items.taxAmount"},
        complimentary: {$sum: "$kot.items.complimentary"},
        addOns: {$push: "$kot.items.addOns"}
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id.itemId",
        name: "$_id.itemName",
        rate: "$rate",
        deployment_id: "$_id.deployment_id",
        category: {
          _id: "$_id.categoryId",
          categoryName: "$_id.categoryName",
          superCategory: {
            _id: "$_id.superCstegoryId",
            superCategoryName: "$_id.superCategoryName"
          }
        },
        subtotal: "$subtotal",
        quantity: "$quantity",
        taxAmount: "$taxAmount",
        itemBillDiscount:"$itemBillDiscount",
        discountAmount: "$discountAmount",
        complimentary: "$complimentary",
        addOns: "$addOns"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if (err) {
      d.reject(err)
    }
    d.resolve(bills);
  });
  return d.promise
}

exports.itemWiseEnterpriseAggregation = function(req,res){
  var d = Q.defer();
  var startDate = new Date(new Date(req.body.arg.startDate).setHours(0, 0, 0, 0));
  var endDate = new Date(new Date(req.body.arg.endDate).setHours(0, 0, 0, 0));
  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
  console.log(startDate);
  console.log(endDate);
  var deployemnts = [];
  _.forEach(req.body.arg.deps, function (dep) {
    console.log(dep);
    deployemnts.push(new mongoose.Types.ObjectId(dep._id));
  });
  //  console.log(deployemnts);
  Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: moment(startDate).toDate(),
          $lte: moment(endDate).toDate()
        },
        deployment_id: {$in: deployemnts},
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: {depid: "$deployment_id"},
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _created: "$_created"//,
      }
    },
    {
      $unwind: "$kot.items"
    },
    {
      $project: {
        items: "$kot.items",
        created: {
          "$concat": [
            {"$substr": [{"$year": "$_created"}, 0, 4]},
            '-',
            {
              "$cond": [
                {"$lte": [{"$month": "$_created"}, 9]},
                {
                  "$concat": [
                    "0",
                    {"$substr": [{"$month": "$_created"}, 0, 2]},
                  ]
                },
                {"$substr": [{"$month": "$_created"}, 0, 2]}
              ]
            },
            '-',
            {
              "$cond": [
                {"$lte": [{"$dayOfMonth": "$_created"}, 9]},
                {
                  "$concat": [
                    "0",
                    {"$substr": [{"$dayOfMonth": "$_created"}, 0, 2]},
                  ]
                },
                {"$substr": [{"$dayOfMonth": "$_created"}, 0, 2]}
              ]
            }
          ]
        }
      }
    },
    {
      $group: {
        _id: {
          created: "$created", deployment: "$_id.depid",
          itemName: "$items.name", category: '$items.category.categoryName'
        },
        qty: {$sum: "$items.quantity"},
        amount: {$sum: "$items.subtotal"},
        addOns: {$push: "$items.addOns"}
        //addOnAmt: {$sum: "$items.addOns.subtota}"}//,
      }
    },
    {
      $project: {
        _id: 0,
        created: "$_id.created",
        deployment: "$_id.deployment",
        itemName: "$_id.itemName",
        itemId: "$_id.itemId",
        category: "$_id.category",
        qty: "$qty",
        amount: "$amount",
        addOns: "$addOns"
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if (err) {
      console.log(err);
      d.reject(err);
    }
    _.forEach(bills, function (res) {
      //console.log('amt', res.amount);
      // res.deployment = res.deployment.toString()
      if (_.has(res, 'addOns')) {
        _.forEach(res.addOns, function (adds) {
          _.forEach(adds, function (addon) {
            //console.log(res.amount);
            //console.log(addon.subtotal);
            res.amount += Number(addon.subtotal);
          });
        });
      }
    });
    //console.log(result);
    d.resolve(bills);
  });
  return d.promise
}

/* NOT IN QUEUE*/
exports.bohReport = function(req,res){
  // console.log("HEllo")
  var d = Q.defer();
  var startDate = new Date(req.body.arg.fromDate);
  var endDate = new Date(req.body.arg.toDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
  Bill.find({
    _created: {
      $gte: moment(startDate).toDate(),
      $lte: moment(endDate).toDate()
    },
    deployment_id: deployment_id,
    isDeleted: {$ne: true},
    isVoid: {$ne:true}
  },{
    billPrintTime: 1,
    _created: 1,
    _closeTime: 1,
    tabType: 1,
    _tableId: 1,
    prefix: 1,
    billNumber: 1,
    serialNumber: 1,
    daySerialNumber: 1,
    payments:1,
    '_customer.firstname': 1,
    '_customer.lastname': 1,
    '_currentUser.firstname': 1,
    '_currentUser.lastname': 1
  }, function (err, bills) {
    console.log(bills[0])
    if (err) {
      d.reject(err)
    }
    d.resolve(bills)
  });
  return d.promise;
}

exports.forDailySalesInvoice = function (req, res) {
  var d = Q.defer();
  var fdate = new Date(req.body.arg.fromDate);
  var tdate = new Date(req.body.arg.toDate);
  tdate = tdate.setDate(tdate.getDate() + 1);
  var query = {
    deployment_id: req.body.arg.deployment_id,
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true}
  };
  if (req.body.arg.complimentary != undefined) {
    if (req.body.arg.complimentary == 'true') {
      query = {
        complimentary: req.body.arg.complimentary,
        deployment_id: req.body.arg.deployment_id,
        _created: {
          $gte: new Date(fdate), $lte: new Date(tdate)
        },
        isDeleted: {$ne: true}
      }
    }
  }

  Bill.find(query,
  {
    isVoid: 1,
    _created: 1,
    tabType: 1,
    tab: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    complimentary: 1,
    serialNumber: 1,
    splitNumber: 1,
    payments: 1,
    _tableId: 1,
    _covers: 1,
    '_kots.isVoid': 1,
    '_kots.items.name': 1,
    '_kots.items._id': 1,
    '_kots.items.quantity': 1,
    '_kots.items.subtotal': 1,
    '_kots.items.rate': 1,
    '_kots.items.billDiscountAmount': 1,
    '_kots.items.category.categoryName': 1,
    '_kots.items.category.superCategory.superCategoryName': 1,
    '_kots.items.taxes._id': 1,
    '_kots.items.taxes.name': 1,
    '_kots.items.taxes.value': 1,
    '_kots.items.taxes.tax_amount': 1,
    '_kots.items.taxes.baseRate_preTax': 1,
    '_kots.items.taxes.cascadingTaxes._id': 1,
    '_kots.items.taxes.cascadingTaxes.name': 1,
    '_kots.items.taxes.cascadingTaxes.value': 1,
    '_kots.items.taxes.cascadingTaxes.tax_amount': 1,
    '_kots.items.taxes.cascadingTaxes.selected': 1,
    '_kots.items.taxes.cascadingTaxes.baseRate_preTax': 1,
    '_kots.items.addOns._id': 1,
    '_kots.items.addOns.name': 1,
    '_kots.items.addOns.rate': 1,
    '_kots.items.addOns.subtotal': 1,
    '_kots.items.addOns.quantity': 1,
    '_kots.items.addOns.category.categoryName': 1,
    '_kots.items.addOns.category.superCategory.superCategoryName': 1,
    '_kots.items.addOns.taxes._id': 1,
    '_kots.items.addOns.taxes.name': 1,
    '_kots.items.addOns.taxes.value': 1,
    '_kots.items.addOns.taxes.tax_amount': 1,
    '_kots.items.addOns.taxes.baseRate_preTax': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.baseRate_preTax': 1,
    '_kots.items.addOns.taxes.cascadingTaxes._id': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.name': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.value': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.selected': 1,
    '_kots.items.addOns.taxes.cascadingTaxes.tax_amount': 1,
    '_kots.items.discounts.discountAmount': 1,
    '_kots.items.discounts.type': 1,
    '_kots.items.discount.value': 1,
    '_kots.items.isDiscounted': 1,
    '_offers': 1,
    'instance.name': 1,
    'instance.prefix': 1,
    '_waiter.firstname':1,
    '_waiter.lastname':1,
    '_waiter.username':1,
    '_delivery.username':1,
    charges:1,
    billDiscountAmount: 1,
    '_currentUser.selectedRoles.name':1,
    '_kots.deleteHistory.subtotal':1,
    '_kots.deleteHistory.addOns.subtotal':1
  }, function (err, bills) {
    if (err) {
      d.reject(err)
    }
    d.resolve(bills);
  });
  return d.promise
}

exports.forItemMenuMix = function(req,res){
  var d = Q.defer();
  var fdate = new Date(req.body.arg.startDate);
  var tdate = new Date(req.body.arg.endDate);
    tdate = tdate.setDate(tdate.getDate() + 1);
  var query = {
    deployment_id: new mongoose.Types.ObjectId(req.body.arg.deployment_id),
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true},
    isVoid: {$ne:true}
  }
  if(req.body.arg.tabType != null && req.body.arg.tabType != undefined && req.body.arg.tabType != 'all')
    query.tabType = req.body.arg.tabType
  
  Bill.aggregate([
    {
      $match: query
    },
    {
      $unwind: "$_kots"
    },
    // Stage 3
    {
      $project: {
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _id: "$_id",
        deployment_id: "$deployment_id",
        _created:1
      }
    },
    {
      $sort:{_created:-1}
    },
    // Stage 4
    {
      $unwind: {path: "$kot.items", includeArrayIndex: "itemIndexKot"}
    },
    {
      $project: {
        "kot.items._id": 1,
        "kot.items.name": 1,
        "kot.kotNumber": 1,
        "kot.items.rate": 1,
        "kot.items.number": 1,
        deployment_id: "$deployment_id",
        "kot.items.addOns.name": 1,
        "kot.items.addOns._id": 1,
        "kot.items.addOns.quantity": 1,
        "kot.items.addOns.subtotal": 1,
        "kot.items.addOns.category._id": 1,
        "kot.items.addOns.rate": 1,
        "kot.items.addOns.number": 1,
        "kot.items.addOns.unit":1,
        "kot.items.addOns.stockQuantity":1,
        "kot.items.addOns.category.categoryName": 1,
        "kot.items.addOns.category.superCategory._id": 1,
        "kot.items.addOns.category.superCategory.superCategoryName": 1,
        "kot.items.discounts.discountAmount": 1,
        "kot.items.subtotal": 1,
        "kot.items.unit":1,
        "kot.items.billDiscountAmount":1,
        "kot.items.stockQuantity":1,
        "kot.items.mapComboItems":1,
        "kot.items.category.isCombos":1,
        "kot.items.quantity": 1,
        "kot.items.category._id": 1,
        "kot.items.category.categoryName": 1,
        "kot.items.category.superCategory._id": 1,
        "kot.items.category.superCategory.superCategoryName": 1,
        "kot.items.kotIndex": "$itemIndexKot",
        "_id": "$_id"
      }
    },
    {
      $project: {
        items: ["$kot.items"],
        kotNumber: "$kot.kotNumber",
        _id: "$_id",
        "addOns": {
          $cond: {
            if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]},
            then: "$kot.items.addOns",
            else: []
          }
        },
      }
    },
    {$project: {items: {$concatArrays: ["$items", "$addOns"]}, kotNumber: "$kotNumber", _id: "$_id"}},
    {$unwind: {path: "$items", preserveNullAndEmptyArrays: true, includeArrayIndex: "itemIndex"}},
    {
      $project: {
        "items._id": "$items._id",
        "items.name": "$items.name",
        "items.number": "$items.number",
        "items.index": "$itemIndex",
        "items.subtotal": {$cond:[
            "$items.billDiscountAmount",
            {$subtract:["$items.subtotal","$items.billDiscountAmount"]},
            "$items.subtotal"
          ]
        },
        "items.quantity": "$items.quantity",
        "items.kotIndex": "$items.kotIndex",
        "items.mapComboItems": "$items.mapComboItems",
        "items.unit": {
          conversionFactor: "$items.unit.conversionFactor",
          updated:"$items.unit.updated",
          stockQuantity: "$items.stockQuantity"
        },
        "items.rate": "$items.rate",
        kotNumber: "$kotNumber",
        _id: "$_id",
        "items.category": "$items.category.superCategory.superCategoryName",
        "items.discounts": {
          $cond: {
            if: {$and: ["$items.discounts", {$gt: [{$size: "$items.discounts"}, 0]}]},
            then: "$items.discounts",
            else: []
          }
        }
      }
    },
    {$unwind: {path: "$items.discounts", preserveNullAndEmptyArrays: true}},
    {
      $group: {
        _id: {
          itemId: "$items._id",
          kotNumber: "$kotNumber",
          _id: "$_id",
          kotIndex: "$items.kotIndex",
          itemName: "$items.name",
          itemQuantity: "$items.quantity",
          itemIndex: "$items.index",
          itemSubtotal: "$items.subtotal",
          itemRate: "$items.rate",
          itemNumber: "$items.number",
          category: "$items.category",
          mapComboItems: "$items.mapComboItems",
          unit: "$items.unit"
        },
        discountAmount: {$sum: "$items.discounts.discountAmount"}
      }
    },
    {
      $project: {
        "items._id": "$_id.itemId",
        "items.name": "$_id.itemName",
        "items.index": "$_id.itemIndex",
        "items.number" : "$_id.itemNumber",
        "items.subtotal": "$_id.itemSubtotal",
        "items.quantity": "$_id.itemQuantity",
        "items.kotIndex": "$_id.kotIndex",
        "items.rate": "$_id.itemRate",
        "items.mapComboItems": "$_id.mapComboItems",
        "items.unit" : "$_id.unit",
        kotNumber: "$_id.kotNumber",
        _id: "$_id._id",
        "items.category": "$_id.category",
        discountAmount: "$discountAmount"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          itemName: "$items.name",
          category: "$items.category",
          itemRate: "$items.rate",
          itemNumber: "$items.number",
          mapComboItems: "$items.mapComboItems",
        },
        unit: {$push:"$items.unit"},
        subtotal: {$sum: "$items.subtotal"},
        discountAmount: {$sum: "$discountAmount"},
        quantity: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        "itemId": "$_id.itemId",
        "name": "$_id.itemName",
        "quantity": "$quantity",
        "subtotal": {$subtract:["$subtotal","$discountAmount"]},
        "number": { $cond: [
            "$_id.itemNumber",
            "$_id.itemNumber",
            {$literal: "-"}
          ]
        },
        "category": "$_id.category",
        "price": "$_id.itemRate"
        // "mapComboItems": "$_id.mapComboItems"
        // "unit": "$unit",
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
      d.reject(err)
    else
      d.resolve(bills)
  })
  return d.promise
}

exports.offersData = function(req,res) {
  var d = Q.defer()
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var dep_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);

  Bill.aggregate([
    {
      $match:{
        "_created": {$gte: moment(startDate).toDate(), $lte: moment(endDate).toDate()},
        "deployment_id": dep_id,
        "_offers.0": {$exists: true},
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    // Stage 2
    {
      $unwind: "$_offers"
    },
    // Stage 3
    {
      $project: {
        offerAmount:"$_offers.offerAmount",
        offerName:"$_offers.offer.name",
        counter:{
          $cond:[{$gt:["$_offers.offerAmount",0]},1,0]
        }
      }
    },
    // Stage 4
    {
      $group: {
        _id:"$offerName",
        offerAmount:{$sum:"$offerAmount"},
        offerCount:{$sum:"$counter"}
      }
    },
    {
      $project:{
        _id:0,
        name:"$_id",
        amount:"$offerAmount",
        count:"$offerCount"
      }
    }
  ], function (err, bills) {
    if (err){
      d.reject(err)
    } else {
      Bill.aggregate([ 
        {
          $match:{
            "_created": {$gte: moment(startDate).toDate(), $lte: moment(endDate).toDate()},
            "deployment_id": dep_id,
            "_kots.items.offer": {$exists: true},
            isDeleted: {$ne: true},
            isVoid: {$ne: true}
          }
        },
        {
          $unwind: "$_kots"
        },
        {
          $project: {
            _id:"$_id",
            "kots":{$cond:["$_kots.isVoid",{},"$_kots"]}
          }
        },
        {
          $group:{
            _id:{
              _id:"$_id",
              kotNumber: "$kots.kotNumber",
              kots:"$kots.items"
            }
          }
        },
        {
          $project:{
            _id:0,
            billId:"$_id._id",
            kotNumber: "$_id.kotNumber",
            kots:"$_id.kots"
          }
        }
      ], function (err, billsOffers) {
        if (err){
          d.reject(err)
        } else {
          var billsR = []
          billsR.push(bills)
          billsR.push(billsOffers)
          d.resolve(billsR)
        }
      })
    }
  });
  return d.promise
}

exports.forEmployeePerformance = function(req,res){
  var d = Q.defer();
  var fdate = new Date(req.body.arg.startDate);
  var tdate = new Date(req.body.arg.endDate);
    tdate = tdate.setDate(tdate.getDate() + 1);
  var query = {
    deployment_id: new mongoose.Types.ObjectId(req.body.arg.deployment_id),
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true},
    isVoid: {$ne:true}
  }

  Bill.aggregate([
    {
      $match: query
    },
    {
      $unwind: "$_kots"
    },
    // Stage 3
    {
      $project: {
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _id: "$_id",
        deployment_id: "$deployment_id",
        _created:1,
        username : {
          $cond: {
            if: "$_waiter.username",
            then: "$_waiter.username",
            else: { 
              $cond: {
                if:"$_delivery.username",
                then: "$_delivery.username",
                else:{$literal:"NA"}
              }
            }
          }
        }
      }
    },
    {
      $sort:{_created:-1}
    },
    // Stage 4
    {
      $unwind: {path: "$kot.items", includeArrayIndex: "itemIndexKot"}
    },
    {
      $project: {
        "kot.items._id": 1,
        "kot.items.name": 1,
        "kot.kotNumber": 1,
        "kot.items.rate": 1,
        "kot.items.number": 1,
        deployment_id: "$deployment_id",
        "kot.items.addOns.name": 1,
        "kot.items.addOns._id": 1,
        "kot.items.addOns.quantity": 1,
        "kot.items.addOns.subtotal": 1,
        "kot.items.addOns.category._id": 1,
        "kot.items.addOns.rate": 1,
        "kot.items.addOns.number": 1,
        "kot.items.addOns.unit":1,
        "kot.items.addOns.stockQuantity":1,
        "kot.items.addOns.category.categoryName": 1,
        "kot.items.addOns.category.superCategory._id": 1,
        "kot.items.addOns.category.superCategory.superCategoryName": 1,
        "kot.items.discounts.discountAmount": 1,
        "kot.items.subtotal": 1,
        "kot.items.unit":1,
        "kot.items.billDiscountAmount":1,
        "kot.items.stockQuantity":1,
        "kot.items.mapComboItems":1,
        "kot.items.category.isCombos":1,
        "kot.items.quantity": 1,
        "kot.items.category._id": 1,
        "kot.items.category.categoryName": 1,
        "kot.items.category.superCategory._id": 1,
        "kot.items.category.superCategory.superCategoryName": 1,
        "kot.items.kotIndex": "$itemIndexKot",
        "_id": "$_id",
        username:"$username"
      }
    },
    {
      $project: {
        username:"$username",
        items: ["$kot.items"],
        kotNumber: "$kot.kotNumber",
        _id: "$_id",
        "addOns": {
          $cond: {
            if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]},
            then: "$kot.items.addOns",
            else: []
          }
        },
      }
    },
    {$project: {items: {$concatArrays: ["$items", "$addOns"]}, kotNumber: "$kotNumber", _id: "$_id",username:"$username"}},
    {$unwind: {path: "$items", preserveNullAndEmptyArrays: true, includeArrayIndex: "itemIndex"}},
    {
      $project: {
        username:"$username",
        "items._id": "$items._id",
        "items.name": "$items.name",
        "items.number": "$items.number",
        "items.index": "$itemIndex",
        "items.subtotal": {$cond:[
            "$items.billDiscountAmount",
            {$subtract:["$items.subtotal","$items.billDiscountAmount"]},
            "$items.subtotal"
          ]
        },
        "items.quantity": "$items.quantity",
        "items.kotIndex": "$items.kotIndex",
        "items.mapComboItems": "$items.mapComboItems",
        "items.unit": {
          conversionFactor: "$items.unit.conversionFactor",
          updated:"$items.unit.updated",
          stockQuantity: "$items.stockQuantity"
        },
        "items.rate": "$items.rate",
        kotNumber: "$kotNumber",
        _id: "$_id",
        "items.category": "$items.category.superCategory.superCategoryName",
        "items.discounts": {
          $cond: {
            if: {$and: ["$items.discounts", {$gt: [{$size: "$items.discounts"}, 0]}]},
            then: "$items.discounts",
            else: []
          }
        }
      }
    },
    {$unwind: {path: "$items.discounts", preserveNullAndEmptyArrays: true}},
    {
      $group: {
        _id: {
          username:"$username",
          itemId: "$items._id",
          kotNumber: "$kotNumber",
          _id: "$_id",
          kotIndex: "$items.kotIndex",
          itemName: "$items.name",
          itemQuantity: "$items.quantity",
          itemIndex: "$items.index",
          itemSubtotal: "$items.subtotal",
          itemRate: "$items.rate",
          itemNumber: "$items.number",
          category: "$items.category",
          mapComboItems: "$items.mapComboItems",
          unit: "$items.unit"
        },
        discountAmount: {$sum: "$items.discounts.discountAmount"}
      }
    },
    {
      $project: {
        username: "$_id.username",
        "items._id": "$_id.itemId",
        "items.name": "$_id.itemName",
        "items.index": "$_id.itemIndex",
        "items.number" : "$_id.itemNumber",
        "items.subtotal": "$_id.itemSubtotal",
        "items.quantity": "$_id.itemQuantity",
        "items.kotIndex": "$_id.kotIndex",
        "items.rate": "$_id.itemRate",
        "items.mapComboItems": "$_id.mapComboItems",
        "items.unit" : "$_id.unit",
        kotNumber: "$_id.kotNumber",
        _id: "$_id._id",
        "items.category": "$_id.category",
        discountAmount: "$discountAmount"
      }
    },
    {
      $group: {
        _id: {
          username: "$username",
          itemId: "$items._id",
          itemName: "$items.name",
          category: "$items.category",
          itemRate: "$items.rate",
          itemNumber: "$items.number",
          mapComboItems: "$items.mapComboItems"
        },
        unit: {$push:"$items.unit"},
        subtotal: {$sum: "$items.subtotal"},
        discountAmount: {$sum: "$discountAmount"},
        quantity: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        username:"$_id.username",
        "itemId": "$_id.itemId",
        "name": "$_id.itemName",
        "quantity": "$quantity",
        "subtotal": {$subtract:["$subtotal","$discountAmount"]},
        "number": { $cond: [
            "$_id.itemNumber",
            "$_id.itemNumber",
            {$literal: "-"}
          ]
        },
        "category": "$_id.category",
        "price": "$_id.itemRate"
        // "mapComboItems": "$_id.mapComboItems",
        // "unit": "$unit",
      }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
      d.reject(err)
    else {
      Bill.aggregate([
      {
        $match:query
      },
      {
        $project: {
          username : {
                 $cond: {
                  if: "$_waiter.username",
                  then: "$_waiter.username",
                  else: { $cond: {
                    if:"$_delivery.username",
                    then: "$_delivery.username",
                    else:{$literal:"NA"}
                  }
                }
              }
            },
            billDiscountAmount:"$billDiscountAmount",
            _kots:1,
          _covers:{$cond:["$_covers","$_covers",0]},
        }
      },
      {
        $group: {
          _id:"$username",
          bills:{$push:"$_kots"},
          billDiscountAmount:{$sum:"$billDiscountAmount"},
          covers:{$sum:"$_covers"},
          NoOfBills:{$sum:1}
        }
      },
      {$unwind: "$bills"},
      {$unwind: "$bills"},
      {
        $project: {
          covers:{$cond:["$covers","$covers",0]},
            bill:{$cond:["$bills.isVoid",{},"$bills"]},
            No_of_bills: "$NoOfBills",
            billDiscountAmount:{$cond:["$bills.isVoid",0,"$billDiscountAmount"]},
        }
      },
      {
        $group: {
          _id:"$_id",
          netSale:{$sum:{$subtract:["$bill.totalAmount","$bill.totalDiscount"]}},
          billDiscountAmount:{$first:"$billDiscountAmount"},
          covers:{$first:"$covers"},
          bills:{$first:"$No_of_bills"}
        }
      },
      {
        $project: {
          _id:"$_id",
          netSale:{$subtract:["$netSale","$billDiscountAmount"]},
          covers:"$covers",
          bills:"$bills"
        }
      }]).allowDiskUse(true).exec(function (err, server) {
        console.log(server)
        if(err)  
          d.reject(err) 
        else{
          var result = []
          result.push(bills)  
          result.push(server)
          d.resolve(result)
        }
      })
    }
  })
  return d.promise
}

exports.forEnterpriseSettlement = function(req,res){
  var d = Q.defer();
  var fdate = new Date(req.body.arg.startDate);
  var tdate = new Date(req.body.arg.endDate);
    tdate = tdate.setDate(tdate.getDate() + 1);
  var deployments = []
  _.forEach(req.body.arg.deployment_id, function(dep){
    deployments.push(new mongoose.Types.ObjectId(dep._id));
  })
  var query = {
    tenant_id: new mongoose.Types.ObjectId(req.body.arg.tenant_id),
    deployment_id: {$in: deployments},
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true},
    isVoid: {$ne:true}
  }

  Bill.aggregate([
    {
      $match: query
    },
    {
        $project:{
            deployment_id:1,
            payments: 1
        }
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
      d.reject(err)
    else 
      d.resolve(bills)
  })
  return d.promise
}

exports.forEnterpriseTaxes = function(req,res){
 var d = Q.defer();
  var fdate = new Date(req.body.arg.startDate);
  var tdate = new Date(req.body.arg.endDate);
    tdate = tdate.setDate(tdate.getDate() + 1);
  var query = {
    tenant_id: new mongoose.Types.ObjectId(req.body.arg.tenant_id),
    _created: {
      $gte: new Date(fdate), $lte: new Date(tdate)
    },
    isDeleted: {$ne: true},
    isVoid: {$ne:true}
  }

  Bill.aggregate([
    {
      $match: query
    },
    {
      $unwind: "$_kots"
    },
    {
      $match: { 
          "_kots.isVoid" : false
      }
    },
    {
      $unwind: "$_kots.items"
    },
    {
      $project: { 
          "_id" : "$_id", 
          "deployment_id" : "$deployment_id", 
          "tenant_id" : "$tenant_id", 
          "items" : {
              "$concatArrays" : [
                  [
                      {
                          "_id" : "$_kots.items._id", 
                          "taxes" : "$_kots.items.taxes", 
                          "isAddOn" : {
                              "$literal" : false
                          }
                      }
                  ], 
                  {
                      "$ifNull" : [
                          "$_kots.items.addOns", 
                          [
      
                          ]
                      ]
                  }
              ]
          }
      }
    },
    {
      $unwind: "$items"
    },
    {
      $unwind: "$items.taxes"
    },
    {
      $project: { 
          "_id" : "$_id", 
          "deployment_id" : "$deployment_id", 
          "isAddOn" : {
              "$ifNull" : [
                  "$items.isAddOn", 
                  true
              ]
          }, 
          "taxes" : {
              "$concatArrays" : [
                  [
                      {
                          "name" : "$items.taxes.name", 
                          "type" : "$items.taxes.type", 
                          "value" : "$items.taxes.value", 
                          "tax_amount" : "$items.taxes.tax_amount", 
                          "_id" : "$items.taxes._id", 
                          "isCascaded" : {
                              "$literal" : false
                          }
                      }
                  ], 
                  "$items.taxes.cascadingTaxes"
              ]
          }
      }
    },
    {
      $unwind: { 
          "path" : "$taxes", 
          "preserveNullAndEmptyArrays" : true
      }
    },
    {
      $project: { 
          "_id" : 0, 
          "bill_id" : "$_id", 
          "deployment_id" : "$deployment_id", 
          "tenant_id" : "$tenant_id", 
          "item_id" : "$item_id", 
          "isAddOn" : "$isAddOn", 
          "name" : "$taxes.name", 
          "type" : "$taxes.type", 
          "value" : "$taxes.value", 
          "tax_amount" : "$taxes.tax_amount", 
          "tax_id" : "$taxes._id", 
          "isCascaded" : {
              "$ifNull" : [
                  "$taxes.isCascaded", 
                  true
              ]
          }
      }
    },
    {
     $group:{
      _id:{
        "deployment_id" : "$deployment_id",
        "tax_id" : "$tax_id",
        "name" : "$name", 
      },
      "tax_amount" : {$sum:"$tax_amount"}, 
     } 
    },
    {
      $group:{
        _id: "$_id.deployment_id",
        tax: {$push:{tax_id: "$_id.tax_id", "name" : "$_id.name", "tax_amount" : "$tax_amount"}}
      },
    }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
      d.reject(err)
    else 
      d.resolve(bills)
  })
  return d.promise 
}

exports.forAdvanceBooking = function(req,res){
  var d = Q.defer();
  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
  advanceBooking.aggregate([
  {
    $match: {
      _created: {
        $gte: moment(startDate).toDate(),
        $lte: moment(endDate).toDate()
      },
      deployment_id: deployment_id,
      isDeleted: {$ne: true},
      isVoid: {$ne: true},
    }
  },
  {
    $project: {
      _id: "$_id",
      deployment_id: "$deployment_id",
      tab:"$tab",
      "_items.billDiscountAmount":1,
      "_items.subtotal":1,
      "_items.taxes.tax_amount":1,
      "_items.taxes.cascadingTaxes.tax_amount":1,
      "_items.isDiscounted":1,
      "_items.discounts":1,
      customer:{
        firstname:"$_customer.firstname",
        phone:"$_customer.phone",
        mobile:"$_customer.mobile",
        address1:"$_customer.address1",
        address2:"$_customer.address2",
        city:"$_customer.city",
        state:"$_customer.state",
        _id:"$_customer._id",
      },
      // billPrintTime: "$billPrintTime",
      _created: "$_created",
      _closeTime: "$_closeTime",
      tabType: "$tabType",
      prefix: "$prefix",
      billNumber: "$billNumber",
      serialNumber: "$serialNumber",
      daySerialNumber: "$daySerialNumber",
      // billDiscountAmount: "$billDiscountAmount",
      chargesAmount:  { $ifNull: [ "$charges.amount", 0 ] },
      advance:1
    }
  }
  ]).allowDiskUse(true).exec(function (err, bills) {
    if(err)
      d.reject(err)
    else 
      d.resolve(bills)
  })
  return d.promise 
}