/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var DayBreakup = require('./dayBreakup.model');

exports.register = function(socket) {
  DayBreakup.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  DayBreakup.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('dayBreakup:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('dayBreakup:remove', doc);
}