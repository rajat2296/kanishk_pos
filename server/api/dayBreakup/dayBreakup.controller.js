'use strict';

var _ = require('lodash');
var DayBreakup = require('./dayBreakup.model');
// Get list of dayBreakups
exports.index = function(req, res) {
  var query = {
    deployment_id: req.query.deployment_id,
    tenant_id: req.query.tenant_id
  }
  var projection;
  try{
    projection = JSON.parse(req.query.projection);
  } catch (err) {
    projection = {};
  }
  DayBreakup.find(query, projection, function (err, dayBreakups) {
    if(err) { return handleError(res, err); }
    return res.json(200, dayBreakups);
  });
};

// Get a single dayBreakup
exports.show = function(req, res) {
  DayBreakup.findById(req.params.id, function (err, dayBreakup) {
    if(err) { return handleError(res, err); }
    if(!dayBreakup) { return res.send(404); }
    return res.json(dayBreakup);
  });
};

// Creates a new dayBreakup in the DB.
exports.create = function(req, res) {
  DayBreakup.create(req.body, function(err, dayBreakup) {
    if(err) { return handleError(res, err); }
    return res.json(201, dayBreakup);
  });
};

// Updates an existing dayBreakup in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  DayBreakup.findById(req.params.id, function (err, dayBreakup) {
    if (err) { return handleError(res, err); }
    if(!dayBreakup) { return res.send(404); }
    var updated = _.merge(dayBreakup, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, dayBreakup);
    });
  });
};

// Deletes a dayBreakup from the DB.
exports.destroy = function(req, res) {
  DayBreakup.findById(req.params.id, function (err, dayBreakup) {
    if(err) { return handleError(res, err); }
    if(!dayBreakup) { return res.send(404); }
    dayBreakup.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
