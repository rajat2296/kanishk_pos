'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DayBreakupSchema = new Schema({
  sessionName: {
    type: String,
    required: true
  },
  deployment_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  tenant_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  fromTime: {
    type: Date,
    required: true
  },
  toTime: {
    type: Date,
    required: true
  }
});

DayBreakupSchema
  .path('sessionName')
  .validate(function (sessionName) {
    return sessionName.length;
  }, 'Name cannot be blank');

DayBreakupSchema
  .path('sessionName')
  .validate(function (value, respond) {
    var self = this;
    this.constructor.findOne({sessionName: value, deployment_id: this.deployment_id}, function (err, user) {
      if (err) throw err;
      if (user) {
        if (self.id === user.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
  }, 'The specified session is already in present.');

module.exports = mongoose.model('DayBreakup', DayBreakupSchema);
