'use strict';

var _ = require('lodash');
var Comment = require('./comment.model');
var Item=require('../item/item.model');
var async=require('async');

// Get list of comments
exports.index = function(req, res) {
  Comment.find({deployment_id:req.query.deployment_id,isDeleted:{$ne:true}},function (err, comments) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(comments);
  });
};

// Get a single comment
exports.show = function(req, res) {
  Comment.findById(req.params.id, function (err, comment) {
    if(err) { return handleError(res, err); }
    if(!comment) { return res.status(404).send('Not Found'); }
    return res.json(comment);
  });
};

// Creates a new comment in the DB.
exports.create = function(req, res) {
  Comment.create(req.body, function(err, comment) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(comment);
  });
};

// Updates an existing comment in the DB.
/*exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Comment.findById(req.params.id, function (err, comment) {
    if (err) { return handleError(res, err); }
    if(!comment) { return res.status(404).send('Not Found'); }
    var updated = _.merge(comment, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(comment);
    });
  });
};

// Deletes a comment from the DB.
exports.destroy = function(req, res) {
  Comment.findById(req.params.id, function (err, comment) {
    if(err) { return handleError(res, err); }
    if(!comment) { return res.status(404).send('Not Found'); }
    comment.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};*/

exports.editComment=function(req,res){
   console.log(req.body);
  Comment.findOneAndUpdate({deployment_id:req.body.deployment_id,_id:req.body._id},req.body,{new:true},function(err,doc){
    if(doc) updateCommentsInItems(req,res);
    else return res.status(500).send(err);
  });
}

exports.deleteComment = function(req, res) {
  console.log(req.body);
 Comment.findOneAndUpdate({deployment_id:req.body.deployment_id,_id:req.body._id},req.body,{new:true},function(err,doc){
    console.log(err,doc);
    if(doc) deleteCommentsInItems(req,res);
    else return res.status(500).send(err);
  })
};

function updateCommentsInItems(req,res){
  Item.update({deployment_id:req.body.deployment_id, "comments._id":req.body._id},{$set:{'comments.$.name':req.body.name},updated:new Date()}, {multi:true},function(err,numAffected){
      console.log(err,numAffected)
      if(numAffected>=0)  return res.status(200).send({status:'success'});
      else return handleError(res,{status:'error'});
    });
}

function deleteCommentsInItems(req,res){
  Item.update({deployment_id:req.body.deployment_id},{$pull: {comments: {_id: req.body._id}},updated:new Date()}, {multi: true},function(err,numAffected){
    console.log(err,numAffected);
    if(numAffected>=0) res.status(200).send({status:'success'});
    else return res.status(500).send({status:'error'});
  });
}

exports.assignComment=function(req,res){
  var list=req.body.list;
  console.log(list);
  async.each(list,function(comment,next) {
      Item.update({
        deployment_id: req.body.deployment_id,
        _id: comment._id
      }, {$set: {comments: comment.comments},updated:new Date()}, function (err, num) {
        if (err)
          next(err);
        else
          next();
      })
    },
    function(err){
      if(err)
       return handleError(res,{status:'error'});
      console.log("all done");
      res.status(200).send({status:'ok'});
    });
}


function handleError(res, err) {
  return res.status(500).send(err);
}
