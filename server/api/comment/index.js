'use strict';

var express = require('express');
var controller = require('./comment.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/assignComment',controller.assignComment);
router.post('/editComment',controller.editComment);
router.post('/deleteComment',controller.deleteComment);

module.exports = router;