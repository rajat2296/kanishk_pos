'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CommentSchema = new Schema({
  name: String,
  deployment_id:String,
  tenant_id:String,
  isDeleted:Boolean
});

module.exports = mongoose.model('Comment', CommentSchema);
