'use strict';

var _ = require('lodash');
var Ledgers = require('./ledgers.model');
var Section=require('../section/section.model');
var Charges=require('../charges/charges.model');
var Q=require('q');

exports.index = function(req, res) {
  var result={};
  getSections(req).then(function(sections) {
    result.sections=sections;
    getCharges(req).then(function(charges) {
      result.charges=charges;
      Ledgers.findOne({deployment_id: req.query.deployment_id}, function (err, ledger) {
        result.ledgerMap=ledger?ledger.ledgerMap:[];
        if(!err)
         return res.status(200).send(result);
        else return res.status(500).send({status:'error'});
      });
    },function(err){
      return res.status(500).send({status:'error'});
    });
  },function(err){
    return res.status(500).send({status:'error'});
  })
};

function getSections(req,res){
  var d= Q.defer();
  Section.find({deployment_id:req.query.deployment_id},function(err,sections){
    if(sections) d.resolve(sections);
    else d.reject(false);
  })
  return d.promise;
}

function getCharges(req,res){
  var d= Q.defer();
  Charges.find({deployment_id:req.query.deployment_id},function(err,charges){
    if(charges) d.resolve(charges);
    else d.reject(false);
  })
  return d.promise;
}

// Get a single ledgers
exports.show = function(req, res) {
  Ledgers.findById(req.params.id, function (err, ledgers) {
    if(err) { return handleError(res, err); }
    if(!ledgers) { return res.status(404).send('Not Found'); }
    return res.json(ledgers);
  });
};

// Creates a new ledgers in the DB.
exports.create = function(req, res) {
  Ledgers.findOneAndUpdate({deployment_id:req.query.deployment_id},req.body,{upsert:true},function(err, ledger) {
    if(ledger)
      res.status(200).send({status:'success'});
    else
      res.status(500).send({status:'error'});
  });
};

// Updates an existing ledgers in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Ledgers.findById(req.params.id, function (err, ledgers) {
    if (err) { return handleError(res, err); }
    if(!ledgers) { return res.status(404).send('Not Found'); }
    var updated = _.merge(ledgers, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(ledgers);
    });
  });
};

// Deletes a ledgers from the DB.
exports.destroy = function(req, res) {
  Ledgers.findById(req.params.id, function (err, ledgers) {
    if(err) { return handleError(res, err); }
    if(!ledgers) { return res.status(404).send('Not Found'); }
    ledgers.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
