'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LedgersSchema = new Schema({
  deployment_id:Schema.Types.ObjectId,
  tenant_id:Schema.Types.ObjectId,
  ledgerMap:[]
});

module.exports = mongoose.model('Ledgers', LedgersSchema);
