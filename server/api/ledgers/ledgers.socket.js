/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Ledgers = require('./ledgers.model');

exports.register = function(socket) {
  Ledgers.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Ledgers.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('ledgers:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('ledgers:remove', doc);
}