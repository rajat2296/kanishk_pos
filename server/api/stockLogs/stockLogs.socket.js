/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockLogs = require('./stockLogs.model');

exports.register = function(socket) {
  StockLogs.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockLogs.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockLogs:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockLogs:remove', doc);
}