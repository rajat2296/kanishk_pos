'use strict';

var _ = require('lodash');
var StockLogs = require('./stockLogs.model');

// Get list of stockLogss
exports.index = function(req, res) {
  StockLogs.find(function (err, stockLogss) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(stockLogss);
  });
};

// Get a single stockLogs
exports.show = function(req, res) {
  StockLogs.findById(req.params.id, function (err, stockLogs) {
    if(err) { return handleError(res, err); }
    if(!stockLogs) { return res.status(404).send('Not Found'); }
    return res.json(stockLogs);
  });
};

// Creates a new stockLogs in the DB.
exports.create = function(req, res) {
  StockLogs.create(req.body, function(err, stockLogs) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(stockLogs);
  });
};

// Updates an existing stockLogs in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  StockLogs.findById(req.params.id, function (err, stockLogs) {
    if (err) { return handleError(res, err); }
    if(!stockLogs) { return res.status(404).send('Not Found'); }
    var updated = _.merge(stockLogs, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(stockLogs);
    });
  });
};

// Deletes a stockLogs from the DB.
exports.destroy = function(req, res) {
  StockLogs.findById(req.params.id, function (err, stockLogs) {
    if(err) { return handleError(res, err); }
    if(!stockLogs) { return res.status(404).send('Not Found'); }
    stockLogs.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}