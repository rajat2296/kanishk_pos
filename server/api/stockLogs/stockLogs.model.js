'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockLogsSchema = new Schema({
  transaction: {},
  deletionDate: {type: Date, default : new Date()},
  deletionType: String,
  comment : String
});

StockLogsSchema.set('versionKey', false);


module.exports = mongoose.model('StockLogs', StockLogsSchema);