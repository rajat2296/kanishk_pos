'use strict';

var express = require('express');
var controller = require('./advancebooking.controller');

var router = express.Router();

router.post('/updateOrderModify', controller.updateOrderModify);
router.get('/', controller.index);
router.get('/getAdvanceByDeployment', controller.getAdvanceByDeployment);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;