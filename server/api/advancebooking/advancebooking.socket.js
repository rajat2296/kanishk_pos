/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Advancebooking = require('./advancebooking.model');

exports.register = function(socket) {
  Advancebooking.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Advancebooking.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('advancebooking:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('advancebooking:remove', doc);
}