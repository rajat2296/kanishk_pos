'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AdvancebookingSchema = new Schema({
    tab: String,
    tabId: String,
    tabType: String,
    _waiter: {},
    _delivery: {},
    _currentUser: {},
    selectedPermissions: [],
    _isTakeOut: Boolean,
    _isDelivery: Boolean,
    _covers: Number,
    _tableId: Number,
    _tabDetails: Boolean,
    _customer: {},
    _items: [],
    _taxes: [],
    _kots: [],
    _cash: [],
    _debitCards: [],
    _creditCards: [],
    _manualKotNumber: String,
    instance_id: String,
    deployment_id: Schema.Types.ObjectId,
    tenant_id: Schema.Types.ObjectId,
    ng_id: { type: String},
    printItems: [],
    printTaxes: [],
    isPrinted: Boolean,
    isSettled: Boolean,
    isVoid: Boolean,
    voidComment: String,
    isSynced: Boolean,
    creditCardForm: {},
    debitCardForm: {},
    _created: {type: Date}, // Offline Creation
    created: {type: Date, default: Date.now}, // Online Creation
    billNumber: Number,
    daySerialNumber: Number,
    serialNumber: Number,
    splitNumber: Number,
    kotNumber: Number,
    _closeTime: {type: Date},
    payments: {},
    voidBillTime: {type: Date},
    billPrintTime: {type: Date},
    billDiscountAmount:Number,
    copy_id:{},
    isDeleted:Boolean,
    complimentary:Boolean,
    _offers:[],
    advance:{id:Schema.Types.ObjectId, datetime:{type: Date},amount:Number},
    aggregation:{}

});

module.exports = mongoose.model('Advancebooking', AdvancebookingSchema);