'use strict';

var _ = require('lodash');
var Advancebooking = require('./advancebooking.model');
var mongoose=require('mongoose');

// Get list of advancebookings
exports.index = function(req, res) {
return res.json(200,[]);

 /* Advancebooking.find(function (err, advancebookings) {
    if(err) { return handleError(res, err); }
    return res.json(200, advancebookings);
  });*/
};
// Get list of advancebookings
exports.getAdvanceByDeployment = function(req, res) {
  Advancebooking.find({deployment_id:req.query.deployment_id,_closeTime:null},function (err, advancebookings) {
    if(err) { return handleError(res, err); }
    return res.json(200, advancebookings);
  });
};

// Get a single advancebooking
exports.show = function(req, res) {
  Advancebooking.findById(req.params.id, function (err, advancebooking) {
    if(err) { return handleError(res, err); }
    if(!advancebooking) { return res.send(404); }
    return res.json(advancebooking);
  });
};

// Creates a new advancebooking in the DB.
exports.create = function(req, res) {
  console.log(req.body);
  Advancebooking.create(req.body, function(err, advancebooking) {
    if(err) { return handleError(res, err); }
    return res.json(201, advancebooking);
  });
};

exports.updateOrderModify = function(req, res) {
  console.log(req.body.id);

  Advancebooking.update({_id: new mongoose.Types.ObjectId(req.body.id)}, {_closeTime: new Date()}, function (err, affected, resp) {
      if (err) {
        console.log(err);
        return handleError(res, err);
      }
      return res.json(200, req.body);
    });
};

// Updates an existing advancebooking in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Advancebooking.findById(req.params.id, function (err, advancebooking) {
    if (err) { return handleError(res, err); }
    if(!advancebooking) { return res.send(404); }
    var updated = _.extend(advancebooking, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, advancebooking);
    });
  });
};

// Deletes a advancebooking from the DB.
exports.destroy = function(req, res) {
  console.log(req.body)
  Advancebooking.findById(req.params.id, function (err, advancebooking) {
    if(err) { return handleError(res, err); }
    if(!advancebooking) { return res.send(404); }
    advancebooking.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}