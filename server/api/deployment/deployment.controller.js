'use strict';

var _ = require('lodash');
var Deployment = require('./deployment.model');
var Instance = require('./instance.model');
var InstanceStations = require('./instanceStations.model');
var Tenant = require('../tenant/tenant.model');
var async = require('async');
var mongoose=require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
var SmsTransaction=require('../smstransaction/smstransaction.model');
var Settle=require('../settle/settle.model');
var Bank=require('../settle/bank.model');
var utils=require('../Utils/utils');
var request = require('request');
var config = require('../../config/environment');


// Get list of deployments
exports.index = function (req, res) {

    // console.log(req.query);
    if(req.query.tenant_id==null){   return handleError(res, []);}
     if(config.isQueue){

      request(config.reportServer.url+'/api/deployments?'+utils.createGetUrl(req.query),function(error,response,body){
        if(error){
          console.log(error);
          res.json(404,{error:'eeror in data'})
        }else{
         // console.log(body);
          return res.json(200, JSON.parse(body));
         }
      })
    }else{

    Deployment.find(req.query, function (err, deployments) {
        if (err) {
            return handleError(res, err);
        }
        //  return res.json(200, deployments);
        return res.json(200, deployments);
    });
    }
};

// Get list of deployments
exports.getAuthDeployment=function(req,res){
    console.log(req.body);
    if(req.body.username==undefined){return res.json(401,{error:'username required.'});}
    if(req.body.password==undefined){return res.json(401,{error:'password required.'});}
    utils.authenticateUser({username:req.body.username,password:req.body.password}).then(function(user){
        //console.log(user);
        if(user) {
            if(user.role=='user') {
                if (user.deployment_id == undefined || user.deployment_id == null) {
                    return res.json(401, {error: 'Deployment not assinged or may be admin user.'});
                }
                else {

                    Deployment.find({_id:new mongoose.Types.ObjectId( user.deployment_id)},{name:1},function(errdn,deployments) {
                        // deps.push(deployments);
                        return res.json(200, deployments);
                    });
                }
            }else{
                //console.log('tenent:'+user.tenant_id);
                Deployment.find({tenant_id:new mongoose.Types.ObjectId( user.tenant_id),isMaster:false},{name:1},function(errd,deployments){
                    //console.log(deployments);
                    return res.json(200,deployments);
                })
            }
        }else{
            return res.json(401,{error:'user not found.'});
        }
    },function(err){
        return res.json(401,{error:err});
    })
};
exports.getTenantByUser=function(req,res){
     /*if(config.isQueue){

  request(config.reportServer.url+'/api/deployments/getTenantByUser?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{
*/
    //console.log(req.body);
    if(req.body.username==undefined){return res.json(401,{error:'username required.'});}
    if(req.body.password==undefined){return res.json(401,{error:'password required.'});}
    utils.authenticateUser({username:req.body.username,password:req.body.password}).then(function(user){
        //console.log(user);
        if(user) {
            if(user.role=='user') {
                if (user.deployment_id == undefined || user.deployment_id == null) {
                    return res.json(401, {error: 'Deployment not assinged or may be admin user.'});
                }
                else {

                    Deployment.find({_id:new mongoose.Types.ObjectId( user.deployment_id)},{name:1},function(errdn,deployments) {
                        // deps.push(deployments);
                        return res.json(200, {deployments:deployments,tenant_id:user.tenant_id,isCallcenter:user.isCallcenterUser});
                    });
                }
            }else{
                //console.log('tenent:'+user.tenant_id);
                Deployment.find({tenant_id:new mongoose.Types.ObjectId( user.tenant_id),isMaster:false},{name:1},function(errd,deployments){
                    //console.log(deployments);
                    return res.json(200,{deployments:deployments,tenant_id:user.tenant_id,isCallcenter:user.isCallcenterUser});
                })
            }
        }else{
            return res.json(401,{error:'user not found.'});
        }
    },function(err){
        return res.json(401,{error:err});
    })
    //}
}


exports.getAllDeploymentsByName = function (req, res) {
    if(req.body.username==undefined){return res.json(401,{error:'username required.'});}
    if(req.body.password==undefined){return res.json(401,{error:'password required.'});}
    utils.authenticateUser({username:req.body.username,password:req.body.password}).then(function(user){
        if(user){
            Deployment.find({tenant_id:req.body.tenant_id,isMaster:false},{name:1}, function (err, deployments) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(200, deployments);
            });
        }else{
            return res.json(404, {error:"User not authenticated."});
        }
    });
};

exports.getAllDeployments = function (req, res) {
    //console.log(req.query);
    //if(req.query.tenant_id==null){   return handleError(res, []);}
     if(config.isQueue){

        request(config.reportServer.url+'/api/deployments/getAllDeployments?'+utils.createGetUrl(req.query),function(error,response,body){
        if(error){
          console.log(error);
          res.json(404,{error:'eeror in data'})
        }else{
         // console.log(body);
          return res.json(200, JSON.parse(body));
         }
      })
    }else{

    Deployment.find({isMaster:false},{name:1,smsSettings:1,tenant_id:1,created:1,balanceSMS:1,emails:1,instances:1,mapDeployment:1}, function (err, deployments) {
        if (err) {
            return handleError(res, err);
        }
        //console.log(deployments);
        var depWithTenant=[];
        async.eachSeries(deployments,function(dep, callback){
            console.log("DEPLOY", dep)
            Tenant.findOne({_id:dep.tenant_id},function(err,tenant){

                var temp={_id: dep._id,name:dep.name,smsSettings:dep.smsSettings,created:dep.created,balanceSMS: dep.balanceSMS,tenant:tenant,emails:dep.emails, isBaseKitchen: dep.isBaseKitchen,instances:dep.instances,mapDeployment:dep.mapDeployment};
                //temp.tenant=tenant;
               // console.log("DEPLOYMENT", temp)
                depWithTenant.push(temp);
                callback();
            })
        },function(){
            return res.json(200, depWithTenant);
        });

        //  return res.json(200, deployments);


    });
 }
};

exports.getDeploymentsByTenantName=function(req,res){
    Tenant.findOne({subdomain:req.body.name},{_id:1},function(err,tenant){
        if(err){
            return handleError(res,err);
        }
        if(tenant){
            Deployment.find({tenant_id:tenant._id,isMaster:false},{name:1},function(errd,deployments){
                if(errd){
                    return handleError(res,errd);
                }
                if(deployments.length>0){
                    return res.json(200,deployments) ;
                }else{
                    return res.json(404,{error:'deployments not found'});
                }
            })
        }else{
             return res.json(404,{error:'tenant not found'});
        }
    });
}
// Get Master Deployment
exports.getMasterDeployment = function (req, res) {
    //console.log(req.query);
     if(config.isQueue){

        request(config.reportServer.url+'/api/deployments/getMasterDeployment?'+utils.createGetUrl(req.query),function(error,response,body){
        if(error){
          console.log(error);
          res.json(404,{error:'eeror in data'})
        }else{
         // console.log(body);
          return res.json(200, JSON.parse(body));
         }
      })
    }else{

    Deployment.findOne({tenant_id:req.query.tenant_id, isMaster: req.query.isMaster}, function (err, masterDeployment) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, masterDeployment);
    });
  }
};
exports.getEmailMappedDeployment = function (req, res) {
 if(config.isQueue){

        request(config.reportServer.url+'/api/deployments/getMasterDeployment?'+utils.createGetUrl(req.query),function(error,response,body){
        if(error){
          console.log(error);
          res.json(404,{error:'eeror in data'})
        }else{
         // console.log(body);
          return res.json(200, JSON.parse(body));
         }
      })
    }else{
    //console.log(req.query);
    if(req.query.mapName == 'swiggy') {
        Deployment.findOne({'mapDeployment.swiggy': req.query.brand}, {name: 1}, function (err, deployment) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, deployment);
        });
    }
    if(req.query.mapName == 'foodpanda') {
        Deployment.findOne({'mapDeployment.foodpanda': req.query.brand}, {name: 1}, function (err, deployment) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, deployment);
        });
    }
    if(req.query.mapName == 'zomato') {
        Deployment.findOne({'mapDeployment.zomato': req.query.brand}, {name: 1}, function (err, deployment) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, deployment);
        });
    }
    if(req.query.mapName == 'limetray') {
        Deployment.findOne({'mapDeployment.limetray': req.query.brand}, {name: 1}, function (err, deployment) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, deployment);
        });
    }
 }
};

// Get a single deployment
exports.show = function (req, res) {
    
    //console.log(req.params);
    Deployment.findById(req.params.id, function (err, deployment) {
        if (err) {
            return handleError(res, err);
        }
        if (!deployment) {
            return res.send(404);
        }
        var tempDep={};
        tempDep=deployment;
        Settle.find(function(err,settleSettings){
            //console.log(settleSettings);
            var tempset=[];
            settleSettings.forEach(function(set){
                //console.log(set);
                var settleObj={name:set.name,isOnlineOrder:false};
                //if(_.has(set,'isOnlineOrder')){
                    if(set.isOnlineOrder==true){
                        settleObj.isOnlineOrder=true;
                    }
                //}
                tempset.push(settleObj);
            })
            Bank.find(function(errBank,settleBankSettings){
                var bankSets=[];
                settleBankSettings.forEach(function(bk){
                    bankSets.push({name:bk.name});
                })
                tempDep.settings.push({'name':'settleSettings','value' :tempset});
                tempDep.settings.push({'name':'bankSettings','value' :bankSets});
                // console.log(tempDep);
                return res.json(tempDep);    
            })
            
        })

    });
};

// Creates a new deployment in the DB.
exports.create = function (req, res) {

    /*Deployment.collection.insert(req.body, null, function (err, docs) {
     if (err) {
     return handleError(res, err);
     }
     return res.json(201, docs);
     });*/

    Deployment.create(req.body, function (err, deployment) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, deployment);
    });

};
// Creates a new instance print in the DB.
exports.createInstancePrint = function (req, res) {
    console.log(req.body);
    req.body.updated = new Date();
    Instance.find({deployment_id: req.body.deployment_id}, function (err,instance) {
        if (err) {
            return handleError(res, err);
        }
       // console.log(instance);
        if(instance.length>0){
            var instanceOb=instance[0];
            var updated = _.merge(instanceOb, req.body);
            updated.markModified("instances");
            updated.save(function (err) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(200, instance);
            });
        }else {
            Instance.create(req.body, function (err, instance) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(201, instance);
            });
        }
    })
};

// Creates a new instance print in the DB.
exports.getInstancePrint = function (req, res) {
    Instance.findOne({deployment_id: req.query.deployment_id}, function (err, instance) {
        if (err) {
            return handleError(res, err);
        }
        if(instance) {
            return res.json(200, instance);
        }else{
            return res.json(404, {error:"instance not found"});
        }
    });
};

// Creates a new instance print in the DB.
exports.createInstanceStationsPrint = function (req, res) {
    req.body.updated = new Date();
    InstanceStations.find({deployment_id: req.body.deployment_id}, function (err,instance) {
        if (err) {
            return handleError(res, err);
        }
        // console.log(instance);
        if(instance.length>0){
            var instanceOb=instance[0];
            var updated = _.merge(instanceOb, req.body);
            updated.markModified("instancesStations");
            updated.save(function (err) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(200, instance);
            });
        }else {
            InstanceStations.create(req.body, function (err, instance) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(201, instance);
            });
        }
    })
};

// Creates a new instance print in the DB.
exports.getInstanceStationsPrint = function (req, res) {
    console.log(req.query);
    InstanceStations.findOne({deployment_id: req.query.deployment_id}, function (err, instance) {
        if (err) {
            return handleError(res, err);
        }
        if(instance) {
            return res.json(200, instance);
        }else{
            return res.json(404, {error:"instance not found"});
        }
    });
};

exports.updateField = function (req, res) {
    console.log(req.body);
    Deployment.update({_id:( req.body.dep_id)}, {$inc:{balanceSMS:req.body.balanceSMS}}, {multi:false},function (err,result) {
        if (err) {
            return handleError(res, err);
        }
        SmsTransaction.create(req.body.smsTransaction,function(err1,smsTrans){
            if (err1) {
                return handleError(res, err1);
            }
            return res.json(200, {rowsAffected:result});
        });

    });
};
exports.updateFieldEmailer = function (req, res) {
    console.log(req.body);
    Deployment.update({_id:( req.body.dep_id)}, {$set:{emails:req.body.emails}}, {multi:false},function (err,result) {
        if (err) {
            return handleError(res, err);
        }
        res.json(200,{status:"Updated successfully"})
    });
};

exports.updateFieldInstances = function (req, res) {
    //console.log(req.body);
    Deployment.update({_id:( req.body.dep_id)}, {$set:{instances:req.body.instances}}, {multi:false},function (err,result) {
        if (err) {
            return handleError(res, err);
        }
        res.json(200,{status:"Updated successfully"})
    });
};

exports.updateFieldMapOrder = function (req, res) {
    //console.log(req.body);
    Deployment.update({_id:( req.body.dep_id)}, {$set:{mapDeployment:req.body.mapDeployment}}, {multi:false},function (err,result) {
        if (err) {
            return handleError(res, err);
        }
        res.json(200,{status:"Updated successfully"})
    });
};


// Updates an existing deployment in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Deployment.findById(req.params.id, function (err, deployment) {
        if (err) {
            return handleError(res, err);
        }
        if (!deployment) {
            return res.send(404);
        }
        var updated = _.merge(deployment, req.body);
        updated.markModified("settings");
        updated.markModified("smsSettings");
        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, deployment);
        });
    });
};

// Deletes a deployment from the DB.
exports.destroy = function (req, res) {
    Deployment.findById(req.params.id, function (err, deployment) {
        if (err) {
            return handleError(res, err);
        }
        if (!deployment) {
            return res.send(404);
        }
        deployment.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

exports.getCurrentDeploymentDetails = function (req, res) {
  console.log(req.query.deployment_id);
  Deployment.findOne({_id: new ObjectId(req.query.deployment_id)}, {tenant_id: 1, name: 1}, function (err, deployment) {
    if (err) {
      return handleError(res, err);
    }
    if (!deployment) {
      return res.send(404);
    }
    return res.status(200).json(deployment);
  });
};

exports.getSingleDeployment = function (req, res) {
    
    console.log(req.query);
    Deployment.findOne({_id:new ObjectId(req.query.deployment_id)}, function (err, deployment) {
        if (err) {
            return handleError(res, err);
        }
        if (!deployment) {
            return res.send(404);
        }
        console.log('deployment',deployment._id)
        return res.json(200,deployment);    
       }); 
};

function handleError(res, err) {
    return res.send(500, err);
}
