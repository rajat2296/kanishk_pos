/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Deployment = require('./deployment.model');

exports.register = function(socket) {
  Deployment.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Deployment.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('deployment:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('deployment:remove', doc);
}