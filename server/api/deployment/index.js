'use strict';

var express = require('express');
var controller = require('./deployment.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();

router.get('/getMasterDeployment', controller.getMasterDeployment);
router.get('/getEmailMappedDeployment', controller.getEmailMappedDeployment);
router.get('/getAllDeployments', controller.getAllDeployments);
router.get('/getInstancePrint', controller.getInstancePrint);
router.get('/getInstanceStationsPrint', controller.getInstanceStationsPrint);
router.get('/getSingleDeployment', controller.getSingleDeployment);
router.put('/updateField',auth.isAuthenticated(), controller.updateField);
router.get('/getCurrentDeploymentDetails', controller.getCurrentDeploymentDetails);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/updateFieldEmailer',auth.isAuthenticated(),controller.updateFieldEmailer)
router.post('/updateFieldInstances',auth.isAuthenticated(),controller.updateFieldInstances)
router.post('/updateFieldMapOrder',auth.isAuthenticated(),controller.updateFieldMapOrder)
router.post('/getAuthDeployment', controller.getAuthDeployment);
router.post('/createInstancePrint', controller.createInstancePrint);
router.post('/createInstanceStationsPrint', controller.createInstanceStationsPrint);
router.post('/getAllDeploymentsByName', controller.getAllDeploymentsByName);
router.post('/getTenantByUser', controller.getTenantByUser);
router.post('/getDeploymentsByTenantName',auth.isAuthenticated(), controller.getDeploymentsByTenantName);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
