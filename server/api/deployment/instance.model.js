'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var InstanceSchema = new Schema({
    deployment_id:Schema.Types.ObjectId,
    created: { type: Date, default: Date.now },
    updated: {type: Date},
    instances:[]
});
//InstanceSchema.set('versionKey', false);

module.exports = mongoose.model('Instance', InstanceSchema);
