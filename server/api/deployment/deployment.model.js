'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DeploymentSchema = new Schema({
    name:String,
    address1: String,
    address2: String,
    city:String,
    state:String,
    country:String,
    mobile:String,
    telephone:String,
    isBaseKitchen: Boolean,
    tenant_id: Schema.Types.ObjectId,
    created: { type: Date, default: Date.now },
    settings:[],
    smsSettings:[],
    isMaster: {type: Boolean, default: false},
    updated: {type: Date},
    isCompleted: {type: Boolean, default: false},
    balanceSMS:Number,
    emails:String,
    mapDeployment:{},
    instances:[],
    instanceStations:[]
});
DeploymentSchema.set('versionKey', false);
DeploymentSchema
  .pre('save', function (next) {
    this.updated = new Date;
    next();
  });

module.exports = mongoose.model('Deployment', DeploymentSchema);
