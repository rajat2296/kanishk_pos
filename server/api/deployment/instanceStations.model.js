/**
 * Created by Ranjeet Sinha on 7/28/2016.
 */
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var InstanceStationsSchema = new Schema({
    deployment_id:Schema.Types.ObjectId,
    created: { type: Date, default: Date.now },
    updated: {type: Date},
    instancesStations:[]
});
//InstanceSchema.set('versionKey', false);

module.exports = mongoose.model('InstanceStations', InstanceStationsSchema);
