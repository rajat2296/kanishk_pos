'use strict';

var _ = require('lodash');
var moment = require('moment');
var BillCardTransaction = require('./billCardTransaction.model');
var mongoose=require('mongoose');
var Q=require('q');

// Get list of billCardTransactions
exports.index = function(req, res) {
  BillCardTransaction.find(function (err, billCardTransactions) {
    if(err) { return handleError(res, err); }
    return res.json(200, billCardTransactions);
  });
};

// Get a single billCardTransaction
exports.show = function(req, res) {
  BillCardTransaction.findById(req.params.id, function (err, billCardTransaction) {
    if(err) { return handleError(res, err); }
    if(!billCardTransaction) { return res.send(404); }
    return res.json(billCardTransaction);
  });
};
exports.getTransactionsForReport=function(req,res) {
  BillCardTransaction.aggregate([
    {
      $match: {
        'deployment_id': new mongoose.Types.ObjectId(req.body.deployment_id),
        created: {
          $gt: moment(new Date(new Date(req.body.startDate))).add(-1, 'days').toDate()
        }/*,
         closeTime: {
         $lte: moment( new Date(new Date(req.body.endDate))).add(1,'days').toDate()
         }*/
      }
    },
    {$unwind: '$transactions'},
    {
      $match: {
        'transactions.serverTime': {
         // $gt: moment(new Date(req.body.startDate)).toISOString(),
         // $lte: moment(new Date(req.body.endDate)).toISOString()
          $gt: (new Date(req.body.startDate)).toISOString(),
          $lte: (new Date(req.body.endDate)).toISOString()
        }
      }
    },
    {
      $project: {
        _id: 0,
        trans: '$transactions',
        cardId: '$cardId',
        created: '$created',
        name: '$name',
        mobile: '$mobile',
        formNumber: '$formNumber',
        user: '$user.name'
      }
    }
  ], function (err, results) {
    console.log(err);
     getUnsettledAmountForReportFun(req.body.startDate,req.body.endDate,req.body.deployment_id).then(function(unsettleAmountTrans){
          //  console.log(unsettleAmountTrans);
            return res.json(200,{results: results,unsettled:unsettleAmountTrans});    
           // return res.json(200,results);    
        
        })
   // return res.json(200, results);
  })
}

function getUnsettledAmountForReportFun(startDate,endDate,deployment_id){
    var d = Q.defer();
    BillCardTransaction.aggregate(

  // Pipeline
  [
    // Stage 1
    {
      $match: {
        created: {
                $gt: moment(new Date(startDate)).toDate(),
                $lte: moment(new Date(endDate)).toDate()
            }
            ,deployment_id: new mongoose.Types.ObjectId(deployment_id)
      }
    },

    // Stage 2
    {
      $unwind: "$transactions"
    },

    // Stage 3
    {
      $project: {
        _id:"$_id",
        amount:"$transactions.amount",
        cardId:"$cardId",
        created:"$created",
        securityAmount:"$securityAmount",
        userName:"$user.name"
      }
    },

    // Stage 4
    {
      $group: {
      _id:{_id:"$_id",cardId:"$cardId",created:"$created",userName:"$userName",securityAmount:"$securityAmount"},
      tAmount:{$sum:"$amount"}
      }
    },

    // Stage 5
    {
      $project: {
        _id:0,
        cardId:"$_id.cardId",
        userName:"$_id.userName",
        created:"$_id.created",
        //balance:{$add:["$tAmount","$_id.securityAmount"]}
        balance:"$tAmount"
      }
    }

  ], function (err, results) {
        console.log(err);
        if(err){d.reject(err)}
        //return res.json(200, results);
    d.resolve(results);
    })
    return d.promise;
}

// Creates a new billCardTransaction in the DB.
exports.create = function(req, res) {
  req.body.cardTransaction_id = req.body._id;
  delete req.body._id;
  BillCardTransaction.count({cardTransaction_id: req.body.cardTransaction_id}, function (err1, count) {
    //console.log(count);
    if(err1)
    return handleError(res, err1);
    if(count>0)
    return res.json(200, {status:'found'});
    BillCardTransaction.create(req.body, function (err, billCardTransaction) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(201, billCardTransaction);
    });
  })
};

// Updates an existing billCardTransaction in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  BillCardTransaction.findById(req.params.id, function (err, billCardTransaction) {
    if (err) { return handleError(res, err); }
    if(!billCardTransaction) { return res.send(404); }
    var updated = _.merge(billCardTransaction, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, billCardTransaction);
    });
  });
};

// Deletes a billCardTransaction from the DB.
exports.destroy = function(req, res) {
  BillCardTransaction.findById(req.params.id, function (err, billCardTransaction) {
    if(err) { return handleError(res, err); }
    if(!billCardTransaction) { return res.send(404); }
    billCardTransaction.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}