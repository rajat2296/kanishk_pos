/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var BillCardTransaction = require('./billCardTransaction.model');

exports.register = function(socket) {
  BillCardTransaction.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  BillCardTransaction.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('billCardTransaction:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('billCardTransaction:remove', doc);
}