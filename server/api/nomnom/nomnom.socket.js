/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Nomnom = require('./nomnom.model');

exports.register = function(socket) {
  Nomnom.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Nomnom.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('nomnom:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('nomnom:remove', doc);
}