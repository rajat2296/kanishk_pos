/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Nomnom = require('./nomnom.model');
var request = require('request');
var Item = require('../item/item.model');
var async = require('async');

// Get list of things
exports.index = function(req, res) {
  console.log(req.query)
  Nomnom.find({tenantId: req.query.tenantId, deploymentId: req.query.deploymentId}, function (err, things) {
    if(err) { return handleError(res, err); }
    return res.json(200, things);
  });
};

// Get a single thing
exports.show = function(req, res) {
  Nomnom.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    return res.json(thing);
  });
};

// Creates a new thing in the DB.
exports.create = function(req, res) {
  console.log(req.body)
  var options = { method: 'POST',
    uri: 'http://pos-test.gonomnom.in/nomnom/pos/login/',
    headers: {'Cache-Control': 'no-cache',
      'Postman-Token':'f7dc34ba-d2ca-0658-1084-50f692266ee3'},
    body:req.body,
    json: true };

  request(options, function (error, response, body) {
    console.log(body,error)
    if(error){
      return res.send(500);
    }
    else{
      var data = req.body;
      data.accessToken = body.data.access_token;
      console.log(data)
      Nomnom.create(data, function(err, thing) {
        if(err) { return handleError(res, err); }
        return res.json(201, thing);
      });
    }

  });

};

// Updates an existing thing in the DB.
exports.update = function(req, res) {
  Nomnom.findById(req.body._id, function (err, thing) {
    if (err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    var updated = _.merge(thing, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, thing);
    });
  });
};

// Deletes a thing from the DB.
exports.destroy = function(req, res) {
  console.log("req.params", req.body)
  Nomnom.findById(req.body.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    thing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};


exports.postItems = function(req,res){
  async.forEach(req.body.items, function (item, callback1) {
    var options = { method: 'POST',
      uri: 'http://pos-test.gonomnom.in/nomnom/pos/posist/dish/add/',
      headers: {'Cache-Control': 'no-cache',
        'Postman-Token':'f7dc34ba-d2ca-0658-1084-50f692266ee3',
        'access-token': req.body.nomnomKeys.accessToken},
      followAllRedirects: true,
      maxRedirects: 10,
      body:item,
      json: true };

    request(options, function (error, response, body) {
      console.log(error)
      if(!error){
        console.log(body)
        Item.findById(item.id, function (err, thing) {
          if (err) { console.log(err); }
          if(thing) {
            var updated = _.merge(thing, {nomnomId : 1});
            updated.save(function (err) {
              if (err) { console.log(err); }
            });
          }
        });
      }
      callback1();

    });
  },function(error){
    return res.send({status:200});
  });
}

exports.updateItems = function(req,res){
  async.forEach(req.body.items, function (item, callback1) {
    var options = { method: 'PUT',
      uri: 'http://pos-test.gonomnom.in/nomnom/pos/posist/dish/update/'+item.id+'/',
      headers: {'Cache-Control': 'no-cache',
        'Postman-Token':'f7dc34ba-d2ca-0658-1084-50f692266ee3',
        'access-token': req.body.nomnomKeys.accessToken},
      followAllRedirects: true,
      maxRedirects: 10,
      body:item,
      json: true };

    request(options, function (error, response, body) {
      callback1();
    });
  },function(error){
    return res.send({status:200});
  });
}

exports.deleteItems = function(req,res){
  async.forEach(req.body.items, function (item, callback1) {
    var options = { method: 'PUT',
      uri: 'http://pos-test.gonomnom.in/nomnom/pos/posist/dish/update/'+item.id+'/',
      headers: {'Cache-Control': 'no-cache',
        'Postman-Token':'f7dc34ba-d2ca-0658-1084-50f692266ee3',
        'access-token': req.body.nomnomKeys.accessToken},
      followAllRedirects: true,
      maxRedirects: 10,
      body:item,
      json: true };

    request(options, function (error, response, body) {
      console.log(error)
      if(!error){
        console.log(body)
        Item.findById(item.id, function (err, thing) {
          if (err) { console.log(err); }
          if(thing) {
            var updated = _.merge(thing, {nomnomId : -1});
            updated.save(function (err) {
              if (err) { console.log(err); }
            });
          }
        });
      }
      callback1();

    });
  },function(error){
    return res.send({status:200});
  });
}

exports.postItem = function(req,res){
  var options = { method: 'PUT',
    uri: 'http://pos-test.gonomnom.in/nomnom/pos/posist/dish/add/',
    headers: {'Cache-Control': 'no-cache',
      'Postman-Token':'f7dc34ba-d2ca-0658-1084-50f692266ee3',
      'access-token': req.body.nomnomKeys.accessToken},
    followAllRedirects: true,
    maxRedirects: 10,
    body:req.body.item,
    json: true };

  request(options, function (error, response, body) {
    console.log(error)
    if(!error){
      console.log(body)
      Item.findById(req.body.item.id, function (err, thing) {
        console.log(thing)
        if (err) { console.log(err); }
        if(thing) {
          var updated = _.merge(thing, {nomnomId : 1});
          updated.save(function (err) {
            if (err) { console.log(err); }
          });
        }
      });
    }
    return res.send({status:200});
  });
}

exports.updateItem = function(req,res){
  var options = { method: 'PUT',
    uri: 'http://pos-test.gonomnom.in/nomnom/pos/posist/dish/update/'+req.body.item.id+'/',
    headers: {'Cache-Control': 'no-cache',
      'Postman-Token':'f7dc34ba-d2ca-0658-1084-50f692266ee3',
      'access-token': req.body.nomnomKeys.accessToken},
    followAllRedirects: true,
    maxRedirects: 10,
    body:req.body.item,
    json: true };

  request(options, function (error, response, body) {
    //console.log(response)
    console.log(body)
    return res.send({status:200});
  });
}

exports.deleteItem = function(req,res){
  var options = { method: 'PUT',
    uri: 'http://pos-test.gonomnom.in/nomnom/pos/posist/dish/update/'+req.body.item.id+'/',
    headers: {'Cache-Control': 'no-cache',
      'Postman-Token':'f7dc34ba-d2ca-0658-1084-50f692266ee3',
      'access-token': req.body.nomnomKeys.accessToken},
    followAllRedirects: true,
    maxRedirects: 10,
    body:req.body.item,
    json: true };

  request(options, function (error, response, body) {
    console.log(error)
    if(!error){
      console.log(body)
      Item.findById(req.body.item.id, function (err, thing) {
        if (err) { console.log(err); }
        if(thing) {
          var updated = _.merge(thing, {nomnomId : -1});
          updated.save(function (err) {
            if (err) { console.log(err); }
          });
        }
      });
    }
    return res.send({status:200});
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
