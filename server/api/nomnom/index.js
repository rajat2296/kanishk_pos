'use strict';

var express = require('express');
var controller = require('./nomnom.controller');

var router = express.Router();
router.post('/delete', controller.destroy);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/update', controller.update);
router.patch('/:id', controller.update);
router.post('/postItems', controller.postItems);
router.post('/updateItems', controller.updateItems);
router.post('/deleteItems', controller.deleteItems);
router.post('/postItem', controller.postItem);
router.post('/updateItem', controller.updateItem);
router.post('/deleteItem', controller.deleteItem);


module.exports = router;
