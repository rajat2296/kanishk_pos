'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var NomnomSchema = new Schema({
  email: String,
  password: String,
  tenantId: String,
  deploymentId: String,
  active: { type: Boolean, default: false },
  accessToken: String
});

module.exports = mongoose.model('Nomnom', NomnomSchema);
