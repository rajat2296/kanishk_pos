/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Receiver = require('./receiver.model');

exports.register = function(socket) {
  Receiver.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Receiver.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('receiver:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('receiver:remove', doc);
}