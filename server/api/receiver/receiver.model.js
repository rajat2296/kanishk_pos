'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ReceiverSchema = new Schema({
  receiverName:String,
  contactPerson:String,
  contactNumber:String,
  address:String,
  pricing:{},
  tenant_id: Schema.Types.ObjectId,  
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  active: { type:Boolean , default:true }
},{versionKey:false});

ReceiverSchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});
ReceiverSchema.path('receiverName').validate(function(v, respond) {
  var self=this;
    this.constructor.findOne({'receiverName':  new RegExp('^'+v+'$', "i"),deployment_id:this.deployment_id}, function (err, receivers) {
    
    var flag=true;
    if(err){throw err;}
    
    if(receivers)
    {
      if(receivers.receiverName.toLowerCase()===self.receiverName.toLowerCase())
      {
        if(receivers.id!=self.id){
          flag=false;
        }
      }
    }
    respond(flag);
    });
  
}, 'This Receiver Name is already used please try  with another name !');

module.exports = mongoose.model('Receiver', ReceiverSchema);