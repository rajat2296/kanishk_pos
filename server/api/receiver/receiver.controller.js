'use strict';

var _ = require('lodash');
var Receiver = require('./receiver.model');

// Get list of receivers
exports.index = function(req, res) {
  var paramsquery;
  if(req.query.lastStoreSynced === undefined) {
    paramsquery = {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
  }
  else {
    paramsquery = {
      tenant_id: req.query.tenant_id,
      deployment_id: req.query.deployment_id,
      updated: {$gte: new Date(req.query.lastStoreSynced), $lt: new Date(req.query.currentSyncTime)}
    }
  }
  // Receiver.find(paramsquery,function (err, receivers) {
  //   if(err) { return handleError(res, err); }
  //   return res.json(200, receivers);
  // });
  Receiver.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id},function (err, receivers) {
    if(err) { return handleError(res, err); }
    return res.json(200, receivers);
  });
};

// Get a single receiver
exports.show = function(req, res) {
  Receiver.findById(req.params.id, function (err, receiver) {
    if(err) { return handleError(res, err); }
    if(!receiver) { return res.send(404); }
    return res.json(receiver);
  });
};

// Creates a new receiver in the DB.
exports.create = function(req, res) {
  Receiver.create(req.body, function(err, receiver) {
    if(err) {
      return res.json({error:err});
      //return handleError(res, err);
    }
    return res.json(201, receiver);
  });
};

// Updates an existing receiver in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Receiver.findById(req.params.id, function (err, receiver) {
    if (err) { return handleError(res, err); }
    if(!receiver) { return res.send(404); }
    var updated = _.extend(receiver, req.body);
    updated.markModified("receiverName");
    updated.markModified("contactPerson");
    updated.markModified("contactNumber");
    updated.markModified("address");
    updated.markModified("pricing");

    updated.save(function (err) {
      if (err) {
        return res.json({error:err});
        //return handleError(res, err);
      }
      return res.json(200, receiver);
    });
  });
};

// Deletes a receiver from the DB.
exports.destroy = function(req, res) {
  Receiver.findById(req.params.id, function (err, receiver) {
    if(err) { return handleError(res, err); }
    if(!receiver) { return res.send(404); }
    receiver.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.getNumReceiversHavingCategory = function (req, res) {
  var cat_id = req.query.id;
  Receiver.find({}, function (err, receivers) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(receivers);
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
