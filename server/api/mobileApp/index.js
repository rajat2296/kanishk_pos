'use strict';

var express = require('express');
var controller = require('./mobileApp.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/getBillByDeployment', controller.getBillByDeployment);
router.post('/', controller.create);
router.get('/topFiveOffers',controller.topFiveOffers);
router.get('/MonthlySalesTrend',controller.MonthlySalesTrend);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
module.exports = router;