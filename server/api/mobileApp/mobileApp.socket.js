/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var MobileApp = require('./mobileApp.model');

exports.register = function(socket) {
  MobileApp.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  MobileApp.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('mobileApp:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('mobileApp:remove', doc);
}