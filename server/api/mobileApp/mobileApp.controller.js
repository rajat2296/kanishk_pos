'use strict';

var _ = require('lodash');
var MobileApp =require('./mobileApp.model');
var User=require('../user/user.model');
var Bill=require('../bill/bill.model');
var Tenant=require('../tenant/tenant.model');
var Deployment=require('../deployment/deployment.model');
var async = require('async');
var utils=require('../Utils/utils');
var moment=require('moment');
var Q = require('q');
var ObjectId = require('mongoose').Types.ObjectId;
// Get list of mobileApps
exports.index = function(req, res) {
    User.findOne({
        username: req.query.username.toLowerCase()
    }, function (err, user) {
        if (err) return handleError(res,err);

        if (!user) {
            return handleError(res, { error: 'This username is not registered.' });
        }
        if (!user.authenticate(req.query.password)) {
            return handleError(res, { error: 'This password is not correct.' });
        }

        if (!user.isActivated()) {
            return handleError(res, {error:"You are not activated."});
        }
        var userSplit=req.query.username.split('_');
        var subDomain='';
        if(userSplit.length>1){
            subDomain=userSplit[0];
        }
        if(!(user.role=='superadmin'||user.role=='admin'))
        {
            var deps=[];
            var depData=[];
            Deployment.find({_id:user.deployment_id,isMaster:false},{name:1,settings:1},function(errd,deployment){
                if(!_.has(user.deployment_id)) {
                    deployment.forEach(function(dep){
                        deps.push(dep);
                    });

                    async.eachSeries(deps, function (dep, callback) {
                        var _dayCutOff= _.filter(dep.settings,{name:'day_sale_cutoff'});
                        var resetDate=new Date(req.query.startDate);
                        var hr=new Date(_dayCutOff[0].value).getHours();
                        /*   resetDate.setHours(hr,0,0,0);
                         console.log(new Date().toISOString());
                         console.log(new Date(resetDate.setDate(resetDate.getDate()+1)).toISOString());*/
                        Bill.aggregate([
                            {
                                $match: {
                                    _created: {
                                        //$gte:new Date(resetDate).toISOString(), $lte: new Date( resetDate.setDate(resetDate.getDate()+1)).toISOString()
                                        $gte:new Date( new Date(req.query.startDate).setHours(hr,0,0,0)), $lte: new Date( new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).setDate(new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).getDate()+1))
                                    },
                                    /*   deployment_id: {$in:dep_ids},*/
                                    deployment_id: dep._id,
                                    isDeleted: {$ne: true},
                                    //isVoid: {$ne: true}
                                }
                            },
                            {
                                $unwind: "$_kots"
                            },
                            {
                                $project: {
                                    isVoid: "$isVoid",
                                    kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
                                    items:"$_kots.items",
                                    billDiscountAmountcomp:{$cond:["$complimentary","$billDiscountAmount",0]},
                                    billDiscountAmount: "$billDiscountAmount",
                                    deploymentId: "$deployment_id",
                                    customer: "$_customer",
                                    chargesAmount:{ $ifNull: [ "$charges.amount", 0 ] }
                                }
                            },
                            {
                                $group: {
                                    _id: {billId: "$_id",billDiscountAmountcomp:"$billDiscountAmountcomp", billDiscountAmount: "$billDiscountAmount",charges:"$chargesAmount", deploymentId: '$deploymentId',customerCount:{$cond:[ "$customer.firstname",1,0]}, isVoid: "$isVoid"},
                                    taxAmount: {$sum: "$kot.taxAmount"},
                                    totalDiscount: {$sum: "$kot.totalDiscount"},
                                    totalAmount: {$sum: "$kot.totalAmount"},
                                    totalItems: {$sum: "$kot.totalItems"},
                                    kot:{$push:"$kot"}
                                }
                            },
                            {
                                $unwind:"$kot"
                            },
                            {
                                $unwind:"$kot.items"
                            },
                            {
                                $unwind:{path:"$kot.items.discounts",preserveNullAndEmptyArrays:true}
                            },
                            {
                                $group: {
                                    _id: {billId: "$_id.billId",billDiscountAmountcomp:"$_id.billDiscountAmountcomp", billDiscountAmount: "$_id.billDiscountAmount",charges:"$_id.charges", deploymentId: '$_id.deploymentId',customerCount:'$_id.customerCount', isVoid: "$_id.isVoid",taxAmount:'$taxAmount',totalDiscount:'$totalDiscount',totalAmount:'$totalAmount',totalItems:'$totalItems'},

                                    complimentaryitemdiscount:{$sum: {$cond:[{$eq:['$kot.items.discounts.discountAmount',"$kot.items.subtotal"]},'$kot.items.subtotal',0]}},

                                }
                            },
                            {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$_id.deploymentId',
                                    billDiscountAmountcomp:"$_id.billDiscountAmountcomp",
                                    taxAmount: '$_id.taxAmount',
                                    totalAmount: {$add:['$_id.totalAmount','$_id.charges']},
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$_id.totalDiscount',
                                    billDiscountAmount:'$_id.billDiscountAmount',
                                    totalItems: '$_id.totalItems',
                                    isVoid: "$_id.isVoid",
                                    //gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]},
                                    // gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, "$totalDiscount"]},
                                    billId: "$_id.billId",
                                    customerCount:"$_id.customerCount"
                                }
                            },
                            {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$deploymentId',
                                    taxAmount: '$taxAmount',
                                    totalAmount: '$totalAmount',
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$totalDiscount',
                                    totalItems: '$totalItems',
                                    isVoid: "$isVoid",
                                    billDiscountAmountcomp:"$billDiscountAmountcomp",
                                    billDiscountAmount:"$billDiscountAmount",
                                    gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]},
                                    billId: "$billId",
                                    customerCount:"$customerCount"
                                }
                            },
                            {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$deploymentId',
                                    taxAmount: '$taxAmount',
                                    totalAmount: '$totalAmount',
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$totalDiscount',
                                    totalItems: '$totalItems',
                                    isVoid: "$isVoid",
                                    billDiscountAmountcomp:"$billDiscountAmountcomp",
                                    billDiscountAmount:"$billDiscountAmount",
                                    gTotal:{$cond: [ { $gte: [ {$subtract:['$gTotal',{$floor:"$gTotal"}]}, .5 ] }, {$ceil:"$gTotal"}, {$floor:'$gTotal'} ]},
                                    billId: "$billId",
                                    customerCount:"$customerCount"
                                }
                            },

                            {
                                $group: {
                                    _id: {deploymentId: '$deploymentId'},
                                    taxAmount: {$sum: '$taxAmount'},
                                    totalAmount: {$sum: '$totalAmount'},
                                    complimentaryitemdiscount:{$sum:{$cond: ["$isVoid", 0, "$complimentaryitemdiscount"]}},
                                    billDiscountAmountcomp:{$sum: {$cond: ["$isVoid", 0, "$billDiscountAmountcomp"]}},
                                    billDiscountAmount: {$sum: {$cond: ["$isVoid", 0, '$billDiscountAmount']}},
                                    totalDiscount: {$sum: {$cond: ["$isVoid", 0, '$totalDiscount']}},
                                    totalItems: {$sum:{$cond: ["$isVoid", 0, '$totalItems']} },
                                    gTotal: {$sum: {$cond: ["$isVoid", 0, '$gTotal']}},
                                    count: {$sum: 1},
                                    customerCount:{$sum:"$customerCount"}
                                }
                            }

                        ], function (err, result) {
                            if (err) {
                                return handleError(res, err);
                            }

                            if(result.length>0) {
                                var depdataOb = result[0];
                                delete depdataOb._id;
                                delete dep.settings;
                                depdataOb.deployment ={_id:dep._id,name:dep.name} ;
                                depData.push(depdataOb);
                                //  console.log(depData);
                            }else{
                                var depdataOb={} ;
                                depdataOb.deployment ={_id:dep._id,name:dep.name} ;
                                depData.push(depdataOb);
                            }
                            callback();
                        });
                    },function(){
                        //  console.log(depData);
                        return res.json(200, depData);
                    })

                }else{
                    deps.push(user.deployment_id);

                    //  console.log(dep_ids);
                    var _dayCutOff= _.filter(dep.settings,{name:'day_sale_cutoff'});
                    var hr=new Date(_dayCutOff[0].value).getHours();

                    Bill.aggregate([
                        {
                            $match: {
                                _created: {
                                    //$gte:new Date(resetDate).toISOString(), $lte: new Date( resetDate.setDate(resetDate.getDate()+1)).toISOString()
                                    $gte:new Date( new Date(req.query.startDate).setHours(hr,0,0,0)), $lte: new Date( new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).setDate(new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).getDate()+1))
                                },
                                deployment_id: user.deployment_id,
                                isDeleted: {$ne: true}
                            }
                        },
                        {
                            $unwind: "$_kots"
                        },
                        {
                            $project: {
                                isVoid: "$isVoid",
                                kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
                                items:"$_kots.items",
                                billDiscountAmountcomp:{$cond:["$complimentary","$billDiscountAmount",0]},
                                billDiscountAmount: "$billDiscountAmount",
                                deploymentId: "$deployment_id",
                                customer: "$_customer",
                                chargesAmount:{ $ifNull: [ "$charges.amount", 0 ] }
                            }
                        },
                        {
                            $group: {
                                _id: {billId: "$_id",billDiscountAmountcomp:"$billDiscountAmountcomp", billDiscountAmount: "$billDiscountAmount",charges:"$chargesAmount", deploymentId: '$deploymentId',customerCount:{$cond:[ "$customer.firstname",1,0]}, isVoid: "$isVoid"},
                                taxAmount: {$sum: "$kot.taxAmount"},
                                totalDiscount: {$sum: "$kot.totalDiscount"},
                                totalAmount: {$sum: "$kot.totalAmount"},
                                totalItems: {$sum: "$kot.totalItems"},
                                kot:{$push:"$kot"}
                            }
                        },
                        {
                            $unwind:"$kot"
                        },
                        {
                            $unwind:"$kot.items"
                        },
                        {
                            $unwind:{path:"$kot.items.discounts",preserveNullAndEmptyArrays:true}
                        },
                        {
                            $group: {
                                _id: {billId: "$_id.billId",billDiscountAmountcomp:"$_id.billDiscountAmountcomp", billDiscountAmount: "$_id.billDiscountAmount",charges:"$_id.charges", deploymentId: '$_id.deploymentId',customerCount:'$_id.customerCount', isVoid: "$_id.isVoid",taxAmount:'$taxAmount',totalDiscount:'$totalDiscount',totalAmount:'$totalAmount',totalItems:'$totalItems'},

                                complimentaryitemdiscount:{$sum: {$cond:[{$eq:['$kot.items.discounts.discountAmount',"$kot.items.subtotal"]},'$kot.items.subtotal',0]}},

                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                deploymentId: '$_id.deploymentId',
                                billDiscountAmountcomp:"$_id.billDiscountAmountcomp",
                                taxAmount: '$_id.taxAmount',
                                totalAmount: {$add:['$_id.totalAmount','$_id.charges']},
                                complimentaryitemdiscount:"$complimentaryitemdiscount",
                                totalDiscount: '$_id.totalDiscount',
                                billDiscountAmount:'$_id.billDiscountAmount',
                                totalItems: '$_id.totalItems',
                                isVoid: "$_id.isVoid",
                                //gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]},
                                // gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, "$totalDiscount"]},
                                billId: "$_id.billId",
                                customerCount:"$_id.customerCount"
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                deploymentId: '$deploymentId',
                                taxAmount: '$taxAmount',
                                totalAmount: '$totalAmount',
                                complimentaryitemdiscount:"$complimentaryitemdiscount",
                                totalDiscount: '$totalDiscount',
                                totalItems: '$totalItems',
                                isVoid: "$isVoid",
                                billDiscountAmountcomp:"$billDiscountAmountcomp",
                                billDiscountAmount:"$billDiscountAmount",
                                gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]},
                                billId: "$billId",
                                customerCount:"$customerCount"
                            }
                        },
                        {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$deploymentId',
                                    taxAmount: '$taxAmount',
                                    totalAmount: '$totalAmount',
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$totalDiscount',
                                    totalItems: '$totalItems',
                                    isVoid: "$isVoid",
                                    billDiscountAmountcomp:"$billDiscountAmountcomp",
                                    billDiscountAmount:"$billDiscountAmount",
                                    gTotal:{$cond: [ { $gte: [ {$subtract:['$gTotal',{$floor:"$gTotal"}]}, .5 ] }, {$ceil:"$gTotal"}, {$floor:'$gTotal'} ]},
                                    billId: "$billId",
                                    customerCount:"$customerCount"
                                }
                        },

                        {
                            $group: {
                                _id: {deploymentId: '$deploymentId'},
                                taxAmount: {$sum: '$taxAmount'},
                                totalAmount: {$sum: '$totalAmount'},
                                complimentaryitemdiscount:{$sum:{$cond: ["$isVoid", 0, "$complimentaryitemdiscount"]}},
                                billDiscountAmountcomp:{$sum: {$cond: ["$isVoid", 0, "$billDiscountAmountcomp"]}},
                                billDiscountAmount: {$sum: {$cond: ["$isVoid", 0, '$billDiscountAmount']}},
                                totalDiscount: {$sum: {$cond: ["$isVoid", 0, '$totalDiscount']}},
                                totalItems: {$sum:{$cond: ["$isVoid", 0, '$totalItems']} },
                                gTotal: {$sum: {$cond: ["$isVoid", 0, '$gTotal']}},
                                count: {$sum: 1},
                                customerCount:{$sum:"$customerCount"}
                            }
                        }

                    ], function (err, result) {
                        if (err) {
                            return handleError(res, err);
                        }
                        return res.json(200, result);
                    });
                }
            });
        }
        else
        {
            Tenant.findOne({subdomain: subDomain}, function (err, tenant) {
                if (err) return handleError(res,err);
                var deps=[];
                var depData=[];
                if(tenant==null){ return res.json(403, {error:'tenant not found'}); }
                Deployment.find({tenant_id:tenant._id,isMaster:false},{name:1,settings:1},function(errd,deployment){
                    if(!_.has(user.deployment_id)) {
                        deployment.forEach(function(dep){
                            deps.push(dep);
                        });

                        async.eachSeries(deps, function (dep, callback) {
                            var _dayCutOff= _.filter(dep.settings,{name:'day_sale_cutoff'});
                            var resetDate=new Date(req.query.startDate);
                            var hr=new Date(_dayCutOff[0].value).getHours();
                            /*   resetDate.setHours(hr,0,0,0);
                             console.log(new Date().toISOString());
                             console.log(new Date(resetDate.setDate(resetDate.getDate()+1)).toISOString());*/
                            Bill.aggregate([
                                {
                                    $match: {
                                        _created: {
                                            //$gte:new Date(resetDate).toISOString(), $lte: new Date( resetDate.setDate(resetDate.getDate()+1)).toISOString()
                                            $gte:new Date( new Date(req.query.startDate).setHours(hr,0,0,0)), $lte: new Date( new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).setDate(new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).getDate()+1))
                                        },
                                        /*   deployment_id: {$in:dep_ids},*/
                                        deployment_id: dep._id,
                                        isDeleted: {$ne: true},
                                        //isVoid: {$ne: true}
                                    }
                                },
                                {
                                    $unwind: "$_kots"
                                },
                                {
                                    $project: {
                                        isVoid: "$isVoid",
                                        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
                                        items:"$_kots.items",
                                        billDiscountAmountcomp:{$cond:["$complimentary","$billDiscountAmount",0]},
                                        billDiscountAmount: "$billDiscountAmount",
                                        deploymentId: "$deployment_id",
                                        customer: "$_customer",
                                        chargesAmount:{ $ifNull: [ "$charges.amount", 0 ] }
                                    }
                                },
                                {
                                    $group: {
                                        _id: {billId: "$_id",billDiscountAmountcomp:"$billDiscountAmountcomp", billDiscountAmount: "$billDiscountAmount",charges:"$chargesAmount", deploymentId: '$deploymentId',customerCount:{$cond:[ "$customer.firstname",1,0]}, isVoid: "$isVoid"},
                                        taxAmount: {$sum: "$kot.taxAmount"},
                                        totalDiscount: {$sum: "$kot.totalDiscount"},
                                        totalAmount: {$sum: "$kot.totalAmount"},
                                        totalItems: {$sum: "$kot.totalItems"},
                                        kot:{$push:"$kot"}
                                    }
                                },
                                {
                                    $unwind:"$kot"
                                },
                                {
                                    $unwind:"$kot.items"
                                },
                                {
                                    $unwind:{path:"$kot.items.discounts",preserveNullAndEmptyArrays:true}
                                },
                                {
                                    $group: {
                                        _id: {billId: "$_id.billId",billDiscountAmountcomp:"$_id.billDiscountAmountcomp", billDiscountAmount: "$_id.billDiscountAmount",charges:"$_id.charges", deploymentId: '$_id.deploymentId',customerCount:'$_id.customerCount', isVoid: "$_id.isVoid",taxAmount:'$taxAmount',totalDiscount:'$totalDiscount',totalAmount:'$totalAmount',totalItems:'$totalItems'},

                                        complimentaryitemdiscount:{$sum: {$cond:[{$eq:['$kot.items.discounts.discountAmount',"$kot.items.subtotal"]},'$kot.items.subtotal',0]}},

                                    }
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        deploymentId: '$_id.deploymentId',
                                        billDiscountAmountcomp:"$_id.billDiscountAmountcomp",
                                        taxAmount: '$_id.taxAmount',
                                        totalAmount: {$add:['$_id.totalAmount','$_id.charges']},
                                        complimentaryitemdiscount:"$complimentaryitemdiscount",
                                        totalDiscount: '$_id.totalDiscount',
                                        billDiscountAmount:'$_id.billDiscountAmount',
                                        totalItems: '$_id.totalItems',
                                        isVoid: "$_id.isVoid",
                                        //gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]},
                                        // gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, "$totalDiscount"]},
                                        billId: "$_id.billId",
                                        customerCount:"$_id.customerCount"
                                    }
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        deploymentId: '$deploymentId',
                                        taxAmount: '$taxAmount',
                                        totalAmount: '$totalAmount',
                                        complimentaryitemdiscount:"$complimentaryitemdiscount",
                                        totalDiscount: '$totalDiscount',
                                        totalItems: '$totalItems',
                                        isVoid: "$isVoid",
                                        billDiscountAmountcomp:"$billDiscountAmountcomp",
                                        billDiscountAmount:"$billDiscountAmount",
                                        gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]},
                                        billId: "$billId",
                                        customerCount:"$customerCount"
                                    }
                                },
                                {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$deploymentId',
                                    taxAmount: '$taxAmount',
                                    totalAmount: '$totalAmount',
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$totalDiscount',
                                    totalItems: '$totalItems',
                                    isVoid: "$isVoid",
                                    billDiscountAmountcomp:"$billDiscountAmountcomp",
                                    billDiscountAmount:"$billDiscountAmount",
                                    gTotal:{$cond: [ { $gte: [ {$subtract:['$gTotal',{$floor:"$gTotal"}]}, .5 ] }, {$ceil:"$gTotal"}, {$floor:'$gTotal'} ]},
                                    billId: "$billId",
                                    customerCount:"$customerCount"
                                }
                                },

                                {
                                    $group: {
                                        _id: {deploymentId: '$deploymentId'},
                                        taxAmount: {$sum: '$taxAmount'},
                                        totalAmount: {$sum: '$totalAmount'},
                                        complimentaryitemdiscount:{$sum:{$cond: ["$isVoid", 0, "$complimentaryitemdiscount"]}},
                                        billDiscountAmountcomp:{$sum: {$cond: ["$isVoid", 0, "$billDiscountAmountcomp"]}},
                                        billDiscountAmount: {$sum: {$cond: ["$isVoid", 0, '$billDiscountAmount']}},
                                        totalDiscount: {$sum: {$cond: ["$isVoid", 0, '$totalDiscount']}},
                                        totalItems: {$sum:{$cond: ["$isVoid", 0, '$totalItems']} },
                                        gTotal: {$sum: {$cond: ["$isVoid", 0, '$gTotal']}},
                                        count: {$sum: 1},
                                        customerCount:{$sum:"$customerCount"}
                                    }
                                }


                            ], function (err, result) {
                                if (err) {
                                    return handleError(res, err);
                                }

                                if(result.length>0) {
                                    var depdataOb = result[0];
                                    delete depdataOb._id;
                                    delete dep.settings;
                                    depdataOb.deployment ={_id:dep._id,name:dep.name} ;
                                    depData.push(depdataOb);
                                    //  console.log(depData);
                                }else{
                                    var depdataOb={} ;
                                    depdataOb.deployment ={_id:dep._id,name:dep.name} ;
                                    depData.push(depdataOb);
                                }
                                callback();
                            });
                        },function(){
                            //  console.log(depData);
                            return res.json(200, depData);
                        })

                    }else{
                        deps.push(user.deployment_id);

                        //  console.log(dep_ids);
                        var _dayCutOff= _.filter(dep.settings,{name:'day_sale_cutoff'});
                        var hr=new Date(_dayCutOff[0].value).getHours();

                        Bill.aggregate([
                            {
                                $match: {
                                    _created: {
                                        //$gte:new Date(resetDate).toISOString(), $lte: new Date( resetDate.setDate(resetDate.getDate()+1)).toISOString()
                                        $gte:new Date( new Date(req.query.startDate).setHours(hr,0,0,0)), $lte: new Date( new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).setDate(new Date( new Date(req.query.startDate).setHours(hr,0,0,0)).getDate()+1))
                                    },
                                    deployment_id: user.deployment_id,
                                    isDeleted: {$ne: true}
                                }
                            },
                            ,
                            {
                                $unwind: "$_kots"
                            },
                            {
                                $project: {
                                    isVoid: "$isVoid",
                                    kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
                                    items:"$_kots.items",
                                    billDiscountAmountcomp:{$cond:["$complimentary","$billDiscountAmount",0]},
                                    billDiscountAmount: "$billDiscountAmount",
                                    deploymentId: "$deployment_id",
                                    customer: "$_customer",
                                    chargesAmount:{ $ifNull: [ "$charges.amount", 0 ] }
                                }
                            },
                            {
                                $group: {
                                    _id: {billId: "$_id",billDiscountAmountcomp:"$billDiscountAmountcomp", billDiscountAmount: "$billDiscountAmount",charges:"$chargesAmount", deploymentId: '$deploymentId',customerCount:{$cond:[ "$customer.firstname",1,0]}, isVoid: "$isVoid"},
                                    taxAmount: {$sum: "$kot.taxAmount"},
                                    totalDiscount: {$sum: "$kot.totalDiscount"},
                                    totalAmount: {$sum: "$kot.totalAmount"},
                                    totalItems: {$sum: "$kot.totalItems"},
                                    kot:{$push:"$kot"}
                                }
                            },
                            {
                                $unwind:"$kot"
                            },
                            {
                                $unwind:"$kot.items"
                            },
                            {
                                $unwind:{path:"$kot.items.discounts",preserveNullAndEmptyArrays:true}
                            },
                            {
                                $group: {
                                    _id: {billId: "$_id.billId",billDiscountAmountcomp:"$_id.billDiscountAmountcomp", billDiscountAmount: "$_id.billDiscountAmount",charges:"$_id.charges", deploymentId: '$_id.deploymentId',customerCount:'$_id.customerCount', isVoid: "$_id.isVoid",taxAmount:'$taxAmount',totalDiscount:'$totalDiscount',totalAmount:'$totalAmount',totalItems:'$totalItems'},

                                    complimentaryitemdiscount:{$sum: {$cond:[{$eq:['$kot.items.discounts.discountAmount',"$kot.items.subtotal"]},'$kot.items.subtotal',0]}},

                                }
                            },
                            {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$_id.deploymentId',
                                    billDiscountAmountcomp:"$_id.billDiscountAmountcomp",
                                    taxAmount: '$_id.taxAmount',
                                    totalAmount: {$add:['$_id.totalAmount','$_id.charges']},
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$_id.totalDiscount',
                                    billDiscountAmount:'$_id.billDiscountAmount',
                                    totalItems: '$_id.totalItems',
                                    isVoid: "$_id.isVoid",
                                    //gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]},
                                    // gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, "$totalDiscount"]},
                                    billId: "$_id.billId",
                                    customerCount:"$_id.customerCount"
                                }
                            },
                            {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$deploymentId',
                                    taxAmount: '$taxAmount',
                                    totalAmount: '$totalAmount',
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$totalDiscount',
                                    totalItems: '$totalItems',
                                    isVoid: "$isVoid",
                                    billDiscountAmountcomp:"$billDiscountAmountcomp",
                                    billDiscountAmount:"$billDiscountAmount",
                                    gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]},
                                    billId: "$billId",
                                    customerCount:"$customerCount"
                                }
                            },
                            {
                                $project: {
                                    _id: 0,
                                    deploymentId: '$deploymentId',
                                    taxAmount: '$taxAmount',
                                    totalAmount: '$totalAmount',
                                    complimentaryitemdiscount:"$complimentaryitemdiscount",
                                    totalDiscount: '$totalDiscount',
                                    totalItems: '$totalItems',
                                    isVoid: "$isVoid",
                                    billDiscountAmountcomp:"$billDiscountAmountcomp",
                                    billDiscountAmount:"$billDiscountAmount",
                                    gTotal:{$cond: [ { $gte: [ {$subtract:['$gTotal',{$floor:"$gTotal"}]}, .5 ] }, {$ceil:"$gTotal"}, {$floor:'$gTotal'} ]},
                                    billId: "$billId",
                                    customerCount:"$customerCount"
                                }
                            },
                            {
                                $group: {
                                    _id: {deploymentId: '$deploymentId'},
                                    taxAmount: {$sum: '$taxAmount'},
                                    totalAmount: {$sum: '$totalAmount'},
                                    complimentaryitemdiscount:{$sum:{$cond: ["$isVoid", 0, "$complimentaryitemdiscount"]}},
                                    billDiscountAmountcomp:{$sum: {$cond: ["$isVoid", 0, "$billDiscountAmountcomp"]}},
                                    billDiscountAmount: {$sum: {$cond: ["$isVoid", 0, '$billDiscountAmount']}},
                                    totalDiscount: {$sum: {$cond: ["$isVoid", 0, '$totalDiscount']}},
                                    totalItems: {$sum:{$cond: ["$isVoid", 0, '$totalItems']} },
                                    gTotal: {$sum: {$cond: ["$isVoid", 0, '$gTotal']}},
                                    count: {$sum: 1},
                                    customerCount:{$sum:"$customerCount"}
                                }
                            }


                        ], function (err, result) {
                            if (err) {
                                return handleError(res, err);
                            }
                            return res.json(200, result);
                        });
                    }
                });
            });
        }

    });
};

// Get a single mobileApp
exports.getBillByDeployment = function(req, res) {
    if(req.query.username==undefined){
        return handleError(res, {error:"username not found."})
    }

    User.findOne({
        username: req.query.username.toLowerCase()
    }, function (err, user) {
        if (err) return handleError(res, err);

        if (!user) {
            return handleError(res, {error: 'This username is not registered.'});
        }
        if (!user.authenticate(req.query.password)) {
            return handleError(res, {error: 'This password is not correct.'});
        }

        if (!user.isActivated()) {
            return handleError(res, {error: "You are not activated."});
        }
        Deployment.findOne({_id: req.query.deploymentId}, {name: 1, settings: 1}, function (err, deployment) {
            if (deployment) {
                //  var _dayCutOff = _.filter(deployment.settings, {name: 'day_sale_cutoff'});
                //  var hr = new Date(_dayCutOff[0].value).getHours();
                var strtDate=new Date( utils.getResetDate(deployment.settings,req.query.startDate));
                console.log(strtDate);
                var endDate = new Date(strtDate);
                endDate = new Date(endDate.setDate(endDate.getDate() + 1));

                Bill.find({
                        _created: {
                            //$gte:new Date('1 sep 2015'), $lte: new Date('2 sep 2015')
                            //$gte: new Date(new Date(req.query.startDate).setHours(hr, 0, 0, 0)),
                            // $gte: new Date(new Date(new Date(req.query.startDate).setHours(hr, 0, 0, 0)).setDate(new Date(new Date(req.query.startDate).setHours(hr, 0, 0, 0)).getDate() - 1)),
                            //$lte: new Date(new Date(new Date(req.query.startDate).setHours(hr, 0, 0, 0)).setDate(new Date(new Date(req.query.startDate).setHours(hr, 0, 0, 0)).getDate() ))
                            // $lte:new Date(new Date(req.query.startDate).setHours(hr, 0, 0, 0))
                            $gte:strtDate,
                            $lt:endDate
                        }
                        ,
                        /*   deployment_id: {$in:dep_ids},*/
                        deployment_id: deployment._id,
                        isDeleted: {$ne: true}
                    }
                    , {_kots: 1, _created: 1,billDiscountAmount:1,isVoid:1,tabType:1,serialNumber:1,
                        _closeTime: 1,
                        billNumber: 1,
                        daySerialNumber: 1,
                        rePrint: 1,
                        aggregation:1,
                        prefix:1,
                        '_currentUser.firstname': 1,
                        '_currentUser.lastname': 1,
                        _offers:1,
                        complimentary:1,
                        charges : 1,tab:1}, function (err, bills) {

                        if (err) {
                            return handleError(res, err);
                        }
                        return res.json(bills);
                    });
            } else {
                res.send(402, {error: 'Deployment not found.'})
            }
        });
    });
};

// Creates a new mobileApp in the DB.
exports.create = function(req, res) {
    MobileApp.create(req.body, function(err, mobileApp) {
        if(err) { return handleError(res, err); }
        return res.json(201, mobileApp);
    });
};

// Updates an existing mobileApp in the DB.
exports.update = function(req, res) {
    if(req.body._id) { delete req.body._id; }
    MobileApp.findById(req.params.id, function (err, mobileApp) {
        if (err) { return handleError(res, err); }
        if(!mobileApp) { return res.send(404); }
        var updated = _.merge(mobileApp, req.body);
        updated.save(function (err) {
            if (err) { return handleError(res, err); }
            return res.json(200, mobileApp);
        });
    });
};

// Deletes a mobileApp from the DB.
exports.destroy = function(req, res) {
    MobileApp.findById(req.params.id, function (err, mobileApp) {
        if(err) { return handleError(res, err); }
        if(!mobileApp) { return res.send(404); }
        mobileApp.remove(function(err) {
            if(err) { return handleError(res, err); }
            return res.send(204);
        });
    });
};
// Getting Monthly sales trend
exports.MonthlySalesTrend=function(req,res)
{
    var endDate=new Date(req.query.date);
    var start=new Date(req.query.date);
    var startDate = new Date(start.setFullYear(start.getFullYear() -1))
    console.log(endDate,'endDate',startDate,'startDate')
     Deployment.findOne({_id: req.query.deployment_id}, {name: 1, settings: 1}, function (err, deployment) {
            if (deployment) {
                //  var _dayCutOff = _.filter(deployment.settings, {name: 'day_sale_cutoff'});
                //  var hr = new Date(_dayCutOff[0].value).getHours();
                var strtDate=new Date( utils.getResetDate(deployment.settings,startDate));
                var enDate=new Date( utils.getResetDate(deployment.settings,endDate));
                enDate = new Date(enDate.setDate(enDate.getDate() + 1));
                // console.log(strtDate);
                console.log(enDate,'endDate',strtDate,'strtDate')
                // var endDate = new Date(strtDate);
                // endDate = new Date(endDate.setDate(endDate.getDate() + 1));
                    Bill.aggregate([
        {
            $match:{
                _created: {
                    $gte: new Date(strtDate),
                    $lte: new Date(enDate)
                },
                isDeleted: {$ne: true},
                isVoid:{$ne:true},
                deployment_id:new ObjectId(req.query.deployment_id)
            }},
        {
          $unwind: "$_kots"
        },
        {
          $project: {
            totalNet: {$cond: ['$_kots.isVoid', 0, {$subtract: ["$_kots.totalAmount", "$_kots.totalDiscount"]}]},
            _created: "$_created",
            deployment_id: "$deployment_id",
            tenant_id: "$tenant_id",
            _id: "$_id",
            billDiscountAmount: {$ifNull: ["$billDiscountAmount", 0]},
            createdAtYear: {$year: "$_created"},
            createdAtMonth: {$month: "$_created"},
            //chargesAmount:  { $ifNull: [ "$charges.amount", 0 ] }
          },

        },
        {
          $group: {
            _id: {
              _id: "$_id",
              createdAtYear: "$createdAtYear",
              deployment_id: "$deployment_id",
              billDiscountAmount: "$billDiscountAmount",
              createdAtMonth: "$createdAtMonth"
            },

            //chargesAmount:{$first:'$chargesAmount'},
            totalNet: {$sum: "$totalNet"}

          }

        },
        {
          $project: {
            _id: 0,
            deployment_id: "$_id.deployment_id",
            createdAtMonth: "$_id.createdAtMonth",
            createdAtYear: "$_id.createdAtYear",
             totalNet:{$subtract:["$totalNet","$_id.billDiscountAmount"]},

          }
        },
        {
          $group: {
            _id: {
              deployment_id: "$deployment_id", createdAtMonth: "$createdAtMonth", createdAtYear: "$createdAtYear"
            }
            ,

            totalNet: {$sum: "$totalNet"}
          }
        },
        {
          $project: {
            _id: 0,
            deployment_id: "$_id.deployment_id",
            createdAtMonth: "$_id.createdAtMonth",
            createdAtYear: "$_id.createdAtYear",
            totalNet: "$totalNet"

          }
      },
          {
            $sort:{"createdAtMonth": 1,"createdAtYear":1}
        }
        
    ],function(err,result){
        if(err)
        {
            console.log(err,'err')
            return handleError(res, err);
        }


        res.json(200,result)

    })
            }
        })
    //   var startDate=new Date(req.query.date);
    //   var endDate=new Date(req.query.date)
    // console.log(moment(startDate).subtract(1, 'years').toDate(),moment(endDate).toDate(),'Dates')

}
// Getting TOP 5 OFFERS FOR THE MOBILEAPP
exports.topFiveOffers=function(req,res)
{
    Deployment.findOne(
        {_id: req.query.deployment_id}, {name: 1, settings: 1}, function (err, deployment) {
            if (deployment) {
                //  var _dayCutOff = _.filter(deployment.settings, {name: 'day_sale_cutoff'});
                //  var hr = new Date(_dayCutOff[0].value).getHours();
                // var strtDate=new Date( utils.getResetDate(deployment.settings,req.query.date));
                // console.log(strtDate);
                // console.log('startdate',new Date(moment(strtDate).add(0, 'days').toDate()));
                // console.log('enddate',moment(strtDate).add(1, 'days').toDate());
                var startDate=new Date( utils.getResetDate(deployment.settings,req.query.startDate));
                var endDate = new Date( utils.getResetDate(deployment.settings,req.query.endDate));
                endDate = new Date(endDate.setDate(endDate.getDate() + 1));
                //     var startDate =new Date(req.query.startDate)
                // var endDate = new Date(req.query.endDate)
                console.log('startDate',new Date(startDate),'endDate',endDate)
                var deployment_id=new ObjectId(req.query.deployment_id)
                var allPromise= Q.all([

                    getItemWiseOffers(startDate,endDate,deployment_id),
                    getbillWiseOffers(startDate,endDate,deployment_id),
                    getTotalSaleInGivenPeriod(startDate,endDate,deployment_id)
                ]);

                allPromise.then(function(result)
                {
                    console.log(result);

                    result[0]=result[0].concat(result[1]);
                    result.splice(1,1);
                    var offers=result[0]
                    if(offers.length>0)
                    {
                        for(var i=0;i<offers.length;i++)
                        {
                            if(offers[i].offerName=="Express" ||offers[i].offerName==null )
                            {
                                offers.splice(i,1);
                            }
                        }
                        var netAmount=result[1][0].Amount;
                        var offersSorted= _.sortBy(offers,function(o){
                            return -o.totalAmount;
                        });
                        if(offersSorted.length > 5)
                        {
                            var top=offersSorted.slice(0,5);
                            console.log(netAmount,'netAmount');
                            var offerAndAmount={offers:top,Amount:netAmount}
                            console.log(offerAndAmount,'offerAndAmount')
                            return res.json(200,offerAndAmount)
                        }
                        else
                        {
                            console.log(netAmount,'netAmount');
                            var offerAndAmount={offers:offersSorted,Amount:netAmount}
                            return res.json(200,offerAndAmount)
                        }
                    }
                    else
                    {
                        return res.json(200,{offers:[]})
                    }

                })
            }
        })

}
function getItemWiseOffers(startDate,endDate,deployment_id){
    var d= Q.defer();
    /*Item wise Offer Aggregation*/
    Bill.aggregate([
            { $match: {
                _created: {
                    $gte: startDate,
                    $lte: endDate
                },
                isVoid: {$ne: true},
                isDeleted: {$ne: true},
                complimentary:{$ne:true},
                deployment_id:deployment_id
            }
            },
            {
                $project:{
                    kots:"$_kots"
                }
            },
            {
                $unwind:{path:"$kots"}
            },
            {
                $project:{

                    kots:{$cond: ['$kots.isVoid', {}, "$kots"]}
                }
            },
            {
                $unwind:{path:"$kots.items"}
            },
            {
                $unwind:{path:"$kots.items.discounts",preserveNullAndEmptyArrays:true}
            }
            ,{
                $group:{
                    _id:{offerName:"$kots.items.offer.name"},
                    Discount:{$sum:"$kots.items.discounts.discountAmount"},
                    TotalAmount:{$sum:"$kots.items.subtotal"}
                }
            },
            {
                $project:{
                    _id:0,
                    offerName:"$_id.offerName",
                    totalAmount:{$subtract:["$TotalAmount","$Discount"]}
                }
            },
            {
                $sort:{"discount": -1}
            }
        ],
        function(err,result) {
            if (err) {
                console.log('err', err);
                d.reject(err);
            }
            else {
                d.resolve(result);

            }
        })

    return d.promise
}
function getbillWiseOffers(startDate,endDate,deployment_id)
{
    var d= Q.defer();
    /* Bill wise offer  aggregation*/
    Bill.aggregate([
            { $match: {
                _created: {
                    $gte: startDate,
                    $lte: endDate
                },
                isVoid: {$ne: true},
                isDeleted: {$ne: true},
                complimentary:{$ne:true},
                deployment_id:deployment_id
            }
            },
            {
                $unwind:"$_offers"
            },
            {
                $project:{
                    offers: "$_offers",
                    kots:"$_kots",
                    billDiscountAmount:"$billDiscountAmount"

                }
            },
            {
                $unwind:{path:"$kots"}
            },
            {
                $project:{
                    offers:"$offers",
                    billDiscountAmount:"$billDiscountAmount",
                    kots:{$cond: ['$kots.isVoid', {}, "$kots"]}
                }
            },
            {
                $group:{
                    _id:{billId: "$_id", offer: "$offers", billDiscountAmount:"$billDiscountAmount"},
                    totalAmount:{$sum:"$kots.totalAmount"},
                    totalDiscount:{$sum:"$kots.totalDiscount"}
                }
            },
            {
                $project: {
                    offer: "$_id.offer",
                    billDiscountAmount:"$_id.billDiscountAmount",
                    totalAmount: {$subtract: ["$totalAmount", "$totalDiscount"]}
                }
            },
            {
                $group: {
                    _id: "$offer.offer.name",
                    totalAmount: {$sum: { $subtract: ["$totalAmount",'$billDiscountAmount'] }},
                }
            },
            {
                $project: {
                    _id:0,
                    offerName: "$_id",
                    totalAmount: "$totalAmount"
                }
            },
            {
                $sort:{"totalAmount": -1}
            }
        ],
        function(err,result) {
            if (err) {
                console.log('err', err);
                d.reject(err);
            }
            else {
                d.resolve(result);

            }
        }
    )
    return d.promise
}
function getTotalSaleInGivenPeriod(startDate,endDate,deployment_id){
    var d= Q.defer();
    /*Total amount sale in the given period*/


    Bill.aggregate([
            { $match: {
                _created: {
                    $gte: startDate,
                    $lte: endDate
                },
                isVoid: {$ne: true},
                isDeleted: {$ne: true},
                complimentary:{$ne:true},
                deployment_id:deployment_id
            }
            },
            {
                $project:{
                    kots:"$_kots",
                    billDiscountAmount:"$billDiscountAmount"
                }
            },
            {
                $unwind:{path:"$kots"},

            },
            {
                $project:{
                    billDiscountAmount:"$billDiscountAmount",
                    kots:{$cond: ['$kots.isVoid', {}, "$kots"]}
                }
            },
            {
                $group:{
                    _id:0,
                    Amount:{$sum:"$kots.totalAmount"},
                    Discount:{$sum:{ $add: [ "$kots.totalDiscount", "$billDiscountAmount" ] }}
                }
            },
            {
                $project:{
                    Amount:{$subtract:["$Amount","$Discount"]}
                }
            }],
        function(err,result) {
            if (err) {
                console.log('err', err);
                d.reject(err);
            }
            else {
                d.resolve(result);

            }
        }
    )
    return d.promise
}
function handleError(res, err) {
    return res.send(500, err);
}
