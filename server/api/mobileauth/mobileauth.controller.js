'use strict';

var _ = require('lodash');
var Mobileauth = require('./mobileauth.model');

// Get list of mobileauths
exports.index = function(req, res) {
  Mobileauth.find(function (err, mobileauths) {
    if(err) { return handleError(res, err); }
    setTimeout(function() {
      return res.json(200, []);
    }, 10);
    
  }).maxTime(1);
};

// Get a single mobileauth
exports.show = function(req, res) {
  Mobileauth.findById(req.params.id).maxTime(5).exec(function (err, mobileauth) {
    if(err) { return handleError(res, err); }
   // setTimeout(function() {
       if(!mobileauth) { return res.send(404); }
    return res.json(mobileauth.mobile);
    //}, 2000);
   });
};

// Creates a new mobileauth in the DB.
exports.create = function(req, res) {
  Mobileauth.create(req.body, function(err, mobileauth) {
    if(err) { return handleError(res, err); }
    return res.json(201, mobileauth);
  });
};

// Updates an existing mobileauth in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Mobileauth.findById(req.params.id, function (err, mobileauth) {
    if (err) { return handleError(res, err); }
    if(!mobileauth) { return res.send(404); }
    var updated = _.merge(mobileauth, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, mobileauth);
    });
  });
};

// Deletes a mobileauth from the DB.
exports.destroy = function(req, res) {
  Mobileauth.findById(req.params.id, function (err, mobileauth) {
    if(err) { return handleError(res, err); }
    if(!mobileauth) { return res.send(404); }
    mobileauth.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}