'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MobileauthSchema = new Schema({
  mobile: String,
  tenant_id: Schema.Types.ObjectId,
  deployment_id: Schema.Types.ObjectId,
  password:String,
  created:{type: Date, default: Date.now},
  lastModified:{type: Date},
  firstName:String,
  lastName:String,
  email:String
});

module.exports = mongoose.model('Mobileauth', MobileauthSchema);