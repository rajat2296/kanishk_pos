/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Mobileauth = require('./mobileauth.model');

exports.register = function(socket) {
  Mobileauth.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Mobileauth.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('mobileauth:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('mobileauth:remove', doc);
}