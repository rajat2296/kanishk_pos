'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    StockRequirementSchema=require('mongoose').model('StockRequirement').schema,
    StockTransactionSchema=require('mongoose').model('StockTransaction').schema,
    BillSchema=require('mongoose').model('Bill').schema,
    StockRecipeSchema=require('mongoose').model('StockRecipe').schema,
    ItemSchema=require('mongoose').model('Item').schema,
    StockItemSchema=require('mongoose').model('StockItem').schema;

var StockReportNewSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

 var StockReportNew = mongoose.model('StockReportNew', StockReportNewSchema);
 var StockRequirement =mongoose.model('StockRequirement', StockRequirementSchema);
 var StockTransaction = mongoose.model('StockTransaction', StockTransactionSchema);
 var Bill=mongoose.model('Bill',BillSchema);
 var StockRecipe=mongoose.model('StockRecipe',StockRecipeSchema);
 var StockItem=mongoose.model('StockItem',StockItemSchema);
 var Item=mongoose.model('Item',ItemSchema);
module.exports={
	StockReportNew:StockReportNew,
    StockTransaction:StockTransaction,
  	StockRequirement:StockRequirement,
  	Bill:Bill,
  	StockRecipe:StockRecipe,
    StockItem:StockItem,
    Item:Item
}
