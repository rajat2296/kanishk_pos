/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Stock = require('./stockReportNew.model');

exports.register = function(socket) {
  Stock.StockReportNew.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Stock.StockReportNew.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockReportNew:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockReportNew:remove', doc);
}