'use strict';

var express = require('express');
var controller = require('./stockReportNew.controller');

var router = express.Router();

//router.get('/', controller.index);
// router.get('/:id', controller.show);
// router.post('/', controller.create);
// router.put('/:id', controller.update);
// router.patch('/:id', controller.update);
// router.delete('/:id', controller.destroy);
// router.get('/getRequirementData', controller.getRequirementData);
// router.get('/getStockSummary', controller.getStockSummary);
router.get('/getConsumptionSummary_ItemReport', controller.getConsumptionSummary_ItemReport);
router.get('/getConsumptionSummary', controller.getConsumptionSummary);
router.get('/getConsumptionSummary_IntermediateReport', controller.getConsumptionSummary_IntermediateReport);
router.get('/getConsumptionSummary_FinishedFoodReport', controller.getConsumptionSummary_FinishedFoodReport);
router.get('/getLastPriceOfItem_RawMaterial',controller.getLastPriceOfItem_RawMaterial);
router.get('/getClosingQty', controller.getClosingQty);
router.get('/getClosingQtyViaPhysical', controller.getClosingQtyViaPhysical);
router.get('/getClosingQty_SemiProcessed', controller.getClosingQty_SemiProcessed);
router.get('/getClosingQty_ProcessedFood', controller.getClosingQty_ProcessedFood);
router.get('/testFunction', controller.testFunction);
router.get('/getRecipeQtyInBase', controller.getRecipeQtyInBase);
router.get('/getLastPriceOfItem_FinishedFood',controller.getLastPriceOfItem_FinishedFood);
router.get('/getLastPriceOfItem_Intermediate',controller.getLastPriceOfItem_Intermediate);
router.get('/getClosingQtyOutlet', controller.getClosingQtyOutlet);
router.get('/getClosingQtyOutletViaPhysical', controller.getClosingQtyOutletViaPhysical);


// router.get('/getConsumptionSummary_DateWise', controller.getConsumptionSummary_DateWise);
// router.get('/RawMaterialPricing_Receipe', controller.RawMaterialPricing_Receipe);
module.exports = router;