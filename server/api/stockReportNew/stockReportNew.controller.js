'use strict';

var _ = require('lodash');
var moment = require('moment');
var Q = require('q');
var async = require('async');
var Stock = require('./stockReportNew.model');
var ObjectId = require('mongoose').Types.ObjectId;
var Store = require('../store/store.model');
var config = require('../../config/environment');
var kue = require('kue')
  , queue = kue.createQueue(config.redisConn);

// Get list of stockReportNews
exports.index = function (req, res) {
  StockReportNew.find(function (err, stockReportNews) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, stockReportNews);
  });
};

// Get a single stockReportNew
exports.show = function (req, res) {
  StockReportNew.findById(req.params.id, function (err, stockReportNew) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockReportNew) {
      return res.send(404);
    }
    return res.json(stockReportNew);
  });
};

// Creates a new stockReportNew in the DB.
exports.create = function (req, res) {
  StockReportNew.create(req.body, function (err, stockReportNew) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(201, stockReportNew);
  });
};

// Updates an existing stockReportNew in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  StockReportNew.findById(req.params.id, function (err, stockReportNew) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockReportNew) {
      return res.send(404);
    }
    var updated = _.merge(stockReportNew, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, stockReportNew);
    });
  });
};

// Deletes a stockReportNew from the DB.
exports.destroy = function (req, res) {
  StockReportNew.findById(req.params.id, function (err, stockReportNew) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockReportNew) {
      return res.send(404);
    }
    stockReportNew.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};

exports.testFunction = function (req, res) {
  var startDate = new Date();
  startDate.setDate(startDate.getDate() - 30);
  startDate.setHours(0, 0, 0, 0);
  var endDate = new Date();
  endDate.setHours(0, 0, 0, 0);
  endDate.setDate(endDate.getDate() + 1);
  var data = {};
  var itemId = "5606a4a8c1b489140a2c944e";
  //var toStoreId="5606a613c1b489140a2c9827";
  //var toStoreId="56458b36762aa93c0ede086f";
  //var storeId="5606a60cc1b489140a2c9826";
  //var storeId="567b8c10926b3c8c089681ed";//Main
  //var storeId="56458b36762aa93c0ede086f";//Kitchen
  var storeId = "568e38bd85fa0760167f5453";//test
  //var storeId="56b591726f56c16c2f5d50bc";//ven kitchen
  var itemType = "RawMaterial";
  //var deployment_id="556eadf102fad90807111eac";
  var deployment_id = "556ec68900a0b4200edafd13";
  //var deployment_id="56b0619252a4a4181411eebf";
  //var deployment_id="565d623468d0632d209f1c73";
  var tenant_id = "556e9b0302fad90807111e34";
  //var tenant_id="565d4639036c38514155dc13";
  //var tenant_id="560553df5c70b1a242892596";
  // getStockEntryByItem_ConsumptionReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getStockSaleByItem_ConsumptionReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getRecipesOfBillItems(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  getRecipesFinishedFood(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err);
  });
  // BillgetItemWiseReport(deployment_id,tenant_id,startDate,endDate).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getBillingData(startDate,endDate,deployment_id,tenant_id,"true").then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});/////////////////////////////////////////////////////////////////////
  // getProcessedAndIntermediateReceipeDetails_Sale(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getPhysicalStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  //
  // (startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getOpeningStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getOpeningStockByItem_Intermediate(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateReceipeDetails_IntermediateReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateReceipeDetails_IntermediateEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getStockReceive_RequirementEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getStockReturn_RequirementEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateConsumptionOn_MenuSale(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // // getOpeningStockByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getLastPriceOfItem_RawMaterial(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // dataBetweenDate(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // dataBeforeDate(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getWastageStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Wastage(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Physical(startDate,endDate,deployment_id,tenant_id,storeId).then(function (val) {
  //   return res.json(val);
  // }, function (err) { return handleError(res, err);});
  // getTransferStockByItemFromStore(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getTransferStockByItemToStore(startDate,endDate,deployment_id,tenant_id,toStoreId).then(function(val){
  //   return res.json(val);
  // },function(err){ return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val)
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate,endDate,deployment_id,tenant_id,toStoreId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockSale_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});

  // getWastage_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getPhysical_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getTransfer_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getConsumptionSummary_ItemReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getRecipeQtyInBase(deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockEntryByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockSaleByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Sale_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getPhysicalStockByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getWastageStockByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Wastage_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getTransferStockByItemFromStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getTransferStockByItemToStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStores(deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getReceipeDetails_FinishedFoodEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)})
  // getReceipeDetails_FinishedFood_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)})
  // getReceipeDetails_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockReceive_RequirementEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStoresProcessedCategory(deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
};

function getStores(deployment_id, tenant_id) {
  var d = Q.defer();
  Store.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      },
    },
    {
      $project: {
        storeId: "$_id",
        storeName: "$storeName",
        isKitchen: "$isKitchen"
      }
    }], function (err, stores) {
    if (err) {
      d.reject("error");
    }
    else {
      d.resolve(stores);
    }
  });
  return d.promise;
};
function getStoresProcessedCategory(deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Store.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        _id: new ObjectId(storeId),
        processedFoodCategory: {$ne: undefined},
      },
    },
    {
      $unwind: "$processedFoodCategory"
    },
    {
      $unwind: "$processedFoodCategory.item"
    },
    {
      $project: {
        storeId: "$_id",
        storeName: "$storeName",
        isKitchen: "$isKitchen",
        items: "$processedFoodCategory.item"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$storeId",
          storeName: "$storeName",
          isKitchen: "$isKitchen"
        }
      }
    },
    {
      $project: {
        _id: 0,
        storeId: "$_id.storeId",
        storeName: "$_id.storeName",
        isKitchen: "$_id.isKitchen",
        itemId: "$_id.itemId",
        itemName: "$_id.name"
      }
    }
  ], function (err, stores) {
    if (err) {
      //console.log(err);
      d.reject("error");
    }
    else {
      d.resolve(stores);
    }
  });
  return d.promise;
};
function getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "9",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalStockReturnQty: "$_id.totalQty",
        totalStockReturnAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getStockReturn_AcceptedEntry_FinishedFood(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "9",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_items"
    },
    {
      $project: {
        items: "$_items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalStockReturnQty: "$_id.totalQty",
        totalStockReturnAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "12",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        },
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalStockReceiveQty: "$_id.totalQty",
        totalStockReceiveAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
     {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
     {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        //"menuItems.itemType":"MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
     {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{ $in: ["MenuItem", "IntermediateItem"] },
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      //console.log(result_sale);
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
     {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true},
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items.itemId",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      //console.log(result_sale);
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getIntermediateConsumptionOn_MenuSale(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
     {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{ $in: ["MenuItem", "IntermediateItem"] },
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
     {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_Sale_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
     {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "14",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem_lastDate(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var startDate1 = new Date(endDate);
  startDate1.setDate(startDate1.getDate() - 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate1), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $group: {
        _id: {itemId: "$itemId", name: "$itemName", unitName: "$UnitName", conversionFactor: "$ConversionFactor"},
        totalQty: {$last: "$totalBalanceQty"},
        date: {$last: "$created"},
        billNo: {$last: "$billNo"},
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$date",
        billNo: "$billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  console.log('startDate',startDate)
  console.log('endDate',endDate)
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      // var checkDate;
      // var data=[];
      // _.forEach(result_op,function(itm,i){
      //   if(i==0){
      //     checkDate=new Date(itm.created);
      //   }
      //   var compareDate=new Date(itm.created);
      //   if(checkDate.getTime()<=compareDate.getTime()){
      //     data.push(itm);
      //   }
      // });
      // d.resolve(data);
      console.log('srrt',result_op.length)
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log('Physical Stock', 'sdjksid kdsj ndsk ndskn dskn sdk nds');
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      //console.log('1584', result_op);
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Physical(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  //console.log(tenant_id, deployment_id);
  var beforeDate = new Date();
  beforeDate.setHours(0, 0, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        timeStamp: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};


function getOpeningStockByItem_Intermediate(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var beforeDate = new Date();
  beforeDate.setHours(0, 0, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "10",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        timeStamp: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      //console.log(err);
      //d.reject('error 964');
      d.resolve([]);
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};

function getOpeningStockByItem_FinishedFood(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var beforeDate = new Date();
  beforeDate.setHours(0, 0, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "13",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        timeStamp: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      //console.log(err);
      //d.reject('error 964');
      d.resolve([]);
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};
function getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit",
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};
function getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"RawMaterial",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit",
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};
function getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"RawMaterial",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"RawMaterial",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true},
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.qty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.qty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReturnAcceptDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log('return Accept', storeId);
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "15",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.recipeId", name: "$items.itemName",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalReturnAcceptQty: "$_id.qty",
        totalReturnAcceptAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      //console.log('return', result_sale);
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.UnitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.created",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.UnitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.created",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [
          {"items.itemType": "RawMaterial"},
          {"items.itemType": "Base Kitchen Item"}
        ],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty",
          unitName: "$units.unitName", conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {

    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{ $in: ["MenuItem", "IntermediateItem"] },
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty",
          unitName: "$units.unitName", conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {

    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getProcessedFoodReturn_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "15",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseWastageQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalReturnWastageQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{$in:["MenuItem","IntermediateItem"]},
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{$in:["MenuItem","IntermediateItem"]},
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Transfer_FromStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Transfer_ToStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getLastPrice_AllRaw_ReceipePricing(deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getLastPriceOfItem_RawMaterial(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor",
          lastPrice: "$units.basePrice",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {

      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$_id.lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$_id.totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }

  ], function (err, result_entry) {
    if (err) {

    }
    else {
      _.forEach(result_entry, function (r, i) {
        var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
        r.lastPrice = lp;
      });
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getLastPriceOfItem_Intermediate(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor",
          lastPrice: "$units.basePrice",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {

      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$_id.lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$_id.totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }

  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      _.forEach(result_entry, function (r, i) {
        var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
        r.lastPrice = lp;
      });
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getLastPriceOfItem_FinishedFood(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        date: "$created"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", totalQty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor", lastPrice: "$units.basePrice",
          UnitName: "$units.unitName", created: "$date"
        },
        totalAmt: {$sum: "$units.totalAmount"}
        //lp:{ $divide: [ "$units.basePrice", "$recipeQty" ] }
      }
    },
    {

      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$_id.lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$_id.totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor",
        created: "$_id.created"
      }
    }

  ], function (err, result_entry) {
    if (err) {

    }
    else {
      var data = [];
      _.forEach(result_entry, function (r, i) {
        var index = _.findIndex(data, {itemId: r.itemId});
        var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
        r.lastPrice = lp;
        if (index < 0) {
          data.push(r);
        }
        else {
          if (new Date(r.created).getTime() > new Date(data[index].created).getTime()) {
            data[index] = r;
          }
        }
      });
      //d.resolve(result_entry);
      d.resolve(data);
      // _.forEach(result_entry,function(r,i){
      //   var lp=parseFloat(r.lastPrice)/parseFloat(r.conversionFactor);
      //   r.lastPrice=lp;
      // });
      // d.resolve(result_entry);
    }
  });
  return d.promise;
};

function BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate) {
 //console.log('billDate', startDate, endDate);
  //console.log(startDate);
  //console.log(endDate);

  console.log('billDate start', startDate);
  console.log('billing end date', endDate);
  var d = Q.defer();
  Stock.Bill.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        _created: {
          $gte: moment(new Date(startDate)).toDate(),
          $lte: moment(new Date(endDate)).toDate()
        },
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: {depid: "$deployment_id"},
        _kots: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _created: "$_created"
      }
    },
    {
      $unwind: "$_kots.items"
    },
    {
      $project: {
        _created: 1,
        "items": ["$_kots.items"],
        "addOns": {
          $cond: {
            if: {$and: ["$_kots.items.addOns", {$gt: [{$size: "$_kots.items.addOns"}, 0]}]},
            then: "$_kots.items.addOns",
            else: []
          }
        },
      }
    },
    {
      $project:
      {
        items: {$concatArrays: ["$items", "$addOns"]},
        _created: 1
      }
    },
    {
      $unwind: "$items"
    },
    {
      $project: {
        "items.name": "$items.name",
        "items._id": "$items._id",
        "items.stockQuantity": "$items.stockQuantity",
        "items.unit.conversionFactor": "$items.unit.conversionFactor",
        "items.quantity": "$items.quantity",
        //"items.addOns": "$items.addOns",
        created: "$_created",
        _created: "$_created",

      }
    },
    {
      $sort:
      {
        _created:-1
      }
    },
    {
      $group: {
        _id: {itemId: "$items._id", created: "$created"},
        qty: {$first: "$items.stockQuantity"},
        conF: {$first: "$items.unit.conversionFactor"},
        billQty: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id.itemId",
        totalSaleQty: {$multiply: ["$qty", "$conF"]},
        billQty: "$billQty",
        created: "$_id.created",
        //unitName: "$_id.unitName"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result2) {
    if (err) {
      console.log(err);
      d.reject('error');
    }

//console.log(err);
    //var result = [];
    if (err) {
      console.log('BillgetItemWiseReport');
      console.log(err);
      d.reject('error');
    } else {
      d.resolve(result2);
    }
    // else {
    //   var addOnsDayWise = {};
    //   _.forEach(result2, function (item) {
    //     result.push(item);
    //     var itCreated = new Date(item.created)
    //     var date = moment(itCreated).format('L');
    //     if (!addOnsDayWise[date])
    //       addOnsDayWise[date] = {};
    //     console.log('itemAddons', item.addOns);
    //     _.forEach(item.addOns, function (addOns, i) {
    //       console.log('items addons',addOns);
    //       _.forEach(addOns, function (addOn) {

    //         var add = addOn;
    //         if (!addOnsDayWise[date][addOn.name]) {
    //           addOnsDayWise[date][addOn.name] = {
    //             billQty: 0
    //           };
    //         }
    //         //console.log('name', add.name);
    //         //console.log('_id', addOn._id);
    //         //console.log('add', addOnsDayWise[date]);
    //         //console.log('addOn', addOn);
    //         addOnsDayWise[date][addOn.name].itemId = addOn._id;
    //         addOnsDayWise[date][addOn.name].itemName = addOn.name;
    //         addOnsDayWise[date][addOn.name].billQty += addOn.quantity ? addOn.quantity : 0;
    //         var stockQuantity = addOn.stockQuantity ? addOn.stockQuantity : 0;
    //         var unit = addOn.unit ? addOn.unit : null;
    //         if (unit)
    //           var conversionFactor = addOn.unit.conversionFactor ? addOn.unit.conversionFactor : 0;
    //         addOnsDayWise[date][addOn.name].totalSaleQty = stockQuantity * conversionFactor;
    //         addOnsDayWise[date][addOn.name].created = new Date(item.created);
    //       });
    //     });
    //   });

    //console.log('dayAddOns', addOnsDayWise);
    // _.forEach(addOnsDayWise, function (dayAddOns, day) {
    //   _.forEach(dayAddOns, function (addOn) {
    //     result.push(addOn);
    //   });
    // });
    //console.log('result', result);
    //d.resolve(result);
    //}
    //  d.resolve(result);

  });

  return d.promise;
};

function getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen) {
  var d = Q.defer();
  var data = [];
  if (isKitchen == "true") {
    var allPromise = Q.all([
      getSemiRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id),
      BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate)
    ]);
    allPromise.then(function (value) {
      var recData = value[0];
      var billData = value[1];
      //console.log(billData);
      _.forEach(billData, function (b, i) {
        _.forEach(recData, function (r, ii) {
          //r.itemId,r.totalSaleQty,r.billQty
          if (b.itemId == r.menuId) {
            if (r.itemSaleQty == undefined) {
              r.itemSaleQty = 0;
            }
            ;
            r.recipeId = r.recipeId.toString();
            r.created = b.created;
            //console.log('3893', b.totalSaleQty, r.receipeQtyinBase, b.billQty, r.itemQtyInBase);
            r.itemSaleQty = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemQtyInBase);
            data.push(_.cloneDeep(r));
          }
        });
      });
      d.resolve(data);
    });
  }
  else {
    //console.log(data);
    d.resolve(data);
  }
  return d.promise;
};

function getBillingData_FinishedFood(startDate, endDate, deployment_id, tenant_id, isKitchen) {
  var d = Q.defer();
  var data = [];
  if (isKitchen == "true") {
    var allPromise = Q.all([
      getRecipesFinishedFood(startDate, endDate, deployment_id, tenant_id),
      BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate)
    ]);
    allPromise.then(function (value) {
      var recData = value[0];
      var billData = value[1];
      // console.log(recData);
      // console.log(billData);
      _.forEach(billData, function (b, i) {
        _.forEach(recData, function (r, ii) {
          //r.itemId,r.totalSaleQty,r.billQty
          if (b.itemId == r.menuId) {
            if (r.itemSaleQty == undefined) {
              r.itemSaleQty = 0;
            }
            ;
            r.itemId = r.itemId.toString();
            r.recipeId = r.recipeId.toString();
            r.created = b.created;
            r.itemSaleQty = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemQtyInBase);
            data.push(_.cloneDeep(r));
          }
        });
      });
      d.resolve(data);
    });
  }
  else {
    //console.log(data);
    d.resolve(data);
  }
  return d.promise;
};

function getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId) {
  var d = Q.defer();
  //console.log('storeId: ', storeId);
  var data = [];
  if (isKitchen) {
    //console.log(isKitchen);
    var allPromise = Q.all([
      getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id),
      BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate),
      getStoresProcessedCategory(deployment_id, tenant_id, storeId)
    ]);
    allPromise.then(function (value) {
      var recData = value[0];
      var billData = value[1];
      var items = value[2];
      _.forEach(billData, function (b, i) {
        _.forEach(items, function (it, iii) {
          if (b.itemId == it.itemId) {
            _.forEach(recData, function (r, ii) {
              //r.itemId,r.totalSaleQty,r.billQty
              if (b.itemId == r.menuId) {
                if (r.itemSaleQty == undefined) {
                  r.itemSaleQty = 0;
                }
                ;
                r.recipeId = r.recipeId.toString();
                r.created = b.created;
                //console.log('3971', r.itemName, b.totalSaleQty, r.receipeQtyinBase, b.billQty, r.itemQtyInBase);
                r.itemSaleQty = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemQtyInBase);
                data.push(_.cloneDeep(r));
              }
            });
          }
        });
      });
      d.resolve(data);
    });
  }
  else {
    d.resolve(data);
  }
  return d.promise;
};

function getRecipesOfBillItems_old(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        //receipeDetails:"$receipeDetails",
        rawItems: "$rawItems",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    // {
    //   $project:{
    //     _id:0,
    //     receipeDetails:"$receipeDetails",
    //     receipeQtyinBase:"$receipeQtyinBase",
    //     Qty:{$divide:[menuQty,"$receipeQtyinBase"]}
    //   }
    // },
    {
      $unwind: "$rawItems"
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        receipeQtyinBase: "$receipeQtyinBase",
        itemQtyInBase: "$rawItems.baseQuantity",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        baseUnit: "$baseUnit",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        //itemSaleQty:{$multiply:["$itemQtyInBase","$Qty",billQty]},
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry)
    }
  });
  return d.promise;
};

function getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        rawItems: "$receipeDetails",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    {
      $unwind: "$rawItems"
    },
    {
      $match: {
        "rawItems.isSemiProcessed": {$ne: true}
      }
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        receipeQtyinBase: "$receipeQtyinBase",
        itemQtyInBase: "$rawItems.baseQuantity",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        baseUnit: "$baseUnit",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry)
    }
  });
  return d.promise;
};

function getSemiRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        rawItems: "$receipeDetails",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    {
      $unwind: "$rawItems"
    },
    {
      $match: {
        "rawItems.isSemiProcessed": true
      }
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        receipeQtyinBase: "$receipeQtyinBase",
        itemQtyInBase: "$rawItems.baseQuantity",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        baseUnit: "$baseUnit",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry)
    }
  });
  return d.promise;
};

function getRecipesFinishedFood(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        isSemiProcessed: false
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id",
        stockItemName: "$recipeName",
        itemName: "$recipeName",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        baseUnit: "$unit.baseUnit.name",
        itemQtyInBase: "$recipeQty",
        receipeQtyinBase: "$recipeQty"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry)
    }
  });
  return d.promise;
};

exports.getLastPriceOfItem_RawMaterial = function (req, res) {
  var startDate = new Date();
  var endDate = new Date();
  getLastPriceOfItem_RawMaterial(startDate, endDate, req.query.deployment_id, req.query.tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};
exports.getLastPriceOfItem_Intermediate = function (req, res) {
  var startDate = new Date();
  var endDate = new Date();
  getLastPriceOfItem_Intermediate(startDate, endDate, req.query.deployment_id, req.query.tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};
exports.getLastPriceOfItem_FinishedFood = function (req, res) {
  var startDate = new Date();
  var endDate = new Date();
  getLastPriceOfItem_FinishedFood(startDate, endDate, req.query.deployment_id, req.query.tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};
exports.getConsumptionSummary = function (req, res) {
  //var resetTime = req.query.resetTime;
  var startDate = new Date(req.query.fromDate);
  //startDate.setHours(0, 0, 0, 0);
  var endDate = new Date(req.query.toDate);
  endDate.setDate(endDate.getDate() + 1);
  /*if(!resetTime) {
   console.log('No reset time');
   startDate = new Date(startDate.setHours(0, 0, 0, 0));
   endDate = new Date(endDate.setDate(endDate.getDate() + 1));
   }*/
  //console.log(startDate);
  //console.log(endDate);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;
  var categoryId = req.query.category;
  var isKitchen = false;
  if (req.query.isKitchen != undefined) {
    isKitchen = req.query.isKitchen;
  }


  var minDate = new Date(startDate);
  var data = [];
  var databetween = [];
  var lastPrice = [];
  var returnData = {beforeDate: [], betweenDate: []};
  getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
    if (result_op.length > 0) {
      _.forEach(result_op, function (op, i) {
        minDate = new Date(op.created);
        data.push(op);
      });
      //console.log("minDate" + minDate);
      if (endDate.getTime() < minDate.getTime()) {
        returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
        //console.log("Report not generated due to latest opening is greater than selected end Date");
        return res.json(returnData);
      }
      else if (minDate.getTime() < startDate.getTime()) {

        //console.log("op < startDate");
        dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          returnData.beforeDate = value;
          _.forEach(data, function (d, i) {
            returnData.beforeDate.push(d);
          });
          dataBetweenDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.betweenDate = value;
            return res.json(returnData);
          }, function (err) {
            return handleError(res, err)
          });
        }, function (err) {
          return handleError(res, err)
        });
      }
      else {
        //console.log("op > startDate and op < end");
        dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          //console.log(value)
          returnData.beforeDate = value;
          _.forEach(data, function (d, i) {
            returnData.beforeDate.push(d);
          });
          dataBetweenDate(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            //console.log(value);
            returnData.betweenDate = value;
            return res.json(returnData);
          }, function (err) {
            return handleError(res, err)
          });
        }, function (err) {
          return handleError(res, err)
        });
      }
    }
    else {
      return res.json(returnData);
    }


  }, function (err) {
    //console.log(err);
    return handleError(res, err);
  });
};

exports.getConsumptionSummary_IntermediateReport = function (req, res) {

  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportNews/getConsumptionSummary_IntermediateReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var startDate = new Date(req.query.fromDate);
    //startDate.setHours(0, 0, 0, 0);
    var endDate = new Date(req.query.toDate);
    //endDate.setDate(endDate.getDate() + 1);
    //console.log(startDate, endDate);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }


    var minDate = new Date(startDate);
    var data = [];
    var databetween = [];
    var lastPrice = [];
    var returnData = {beforeDate: [], betweenDate: []};
    getOpeningStockByItem_Intermediate(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
      if (result_op.length > 0) {
        _.forEach(result_op, function (op, i) {
          minDate = new Date(op.created);
          data.push(op);
        });
        //console.log("minDate" + minDate);
        if (endDate.getTime() < minDate.getTime()) {
          returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
          //console.log("Report not generated due to latest opening is greater than selected end Date");
          return res.json(returnData);
        }
        else if (minDate.getTime() < startDate.getTime()) {

          //console.log("op < startDate");
          dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
        else {
          //console.log("op > startDate and op < end");
          dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_IntermediateReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
      }
      else {
        return res.json(returnData);
      }


    }, function (err) {
      //console.log(err);
      return handleError(res, err);
    });
  }
};

exports.getConsumptionSummary_FinishedFoodReport = function (req, res) {

  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportNews/getConsumptionSummary_FinishedFoodReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var startDate = new Date(req.query.fromDate);
    startDate.setHours(0, 0, 0, 0);
    var endDate = new Date(req.query.toDate);
    endDate.setDate(endDate.getDate() + 1);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }


    var minDate = new Date(startDate);
    var data = [];
    var databetween = [];
    var lastPrice = [];
    var returnData = {beforeDate: [], betweenDate: []};
    getOpeningStockByItem_FinishedFood(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
      if (result_op.length > 0) {
        _.forEach(result_op, function (op, i) {
          minDate = new Date(op.created);
          data.push(op);
        });
        //console.log("minDate" + minDate);
        if (endDate.getTime() < minDate.getTime()) {
          returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
          //console.log("Report not generated due to latest opening is greater than selected end Date");
          return res.json(returnData);
        }
        else if (minDate.getTime() < startDate.getTime()) {

          //console.log("op < startDate");
          dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
        else {
          //console.log("op > startDate and op < end");
          dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_FinishedFoodReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
      }
      else {
        return res.json(returnData);
      }


    }, function (err) {
      //console.log(err);
      return handleError(res, err);
    });
  }
};

function dataBeforeDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    //getReceipeDetails_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId),
    getReceipeDetails_Sale_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_FromStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_ToStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getBillingData_FinishedFood(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getReturnAcceptDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReturn_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      });
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBetweenDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    //getReceipeDetails_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId),
    getReceipeDetails_Sale_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_FromStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_ToStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getBillingData_FinishedFood(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getReturnAcceptDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReturn_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBeforeDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  //console.log('dataBeforeDate');
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateConsumptionOn_MenuSale(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),//finished in
    getProcessedAndIntermediateReceipeDetails_Wastage_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen)

  ]);
  allPromise.then(function (value) {
    //console.log(value.length);
    _.forEach(value, function (val, i) {
      //console.log(val.length);
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBetweenDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateConsumptionOn_MenuSale(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),//in Finished
    getProcessedAndIntermediateReceipeDetails_Wastage_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getPhysicalStockByItem_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBetweenDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),
    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBeforeDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),

    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function getStockSale_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits",
        receipeDetails: "$_store.receiver.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getWastage_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        receipeDetails: "$_store.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getPhysical_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        receipeDetails: "$_store.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",

      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getTransfer_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        receipeDetails: "$_store.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",

      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getConsumptionSummary_ItemReportFunction(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getStockSale_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getWastage_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getPhysical_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransfer_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

exports.getConsumptionSummary_ItemReport = function (req, res) {

  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportNews/getConsumptionSummary_ItemReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var startDate = new Date(req.query.fromDate);
    startDate.setHours(0, 0, 0, 0);
    var endDate = new Date(req.query.toDate);
    endDate.setDate(endDate.getDate() + 1);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }
    var minDate = new Date(startDate);
    var data = [];
    var databetween = [];
    var lastPrice = [];
    var returnData = [];

    getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
      if (result_op.length > 0) {
        _.forEach(result_op, function (op, i) {
          minDate = new Date(op.created);
          data.push(op);
        });
        getConsumptionSummary_ItemReportFunction(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result) {
          returnData = result;
          return res.json(returnData);
        }, function (err) {
          return handleError(res, err)
        });
      }
      else {
        return res.json(returnData);
      }
    }, function (err) {
      return handleError(res, err);
    });
  }
};

exports.getRecipeQtyInBase = function (req, res) {
  getRecipeQtyInBase(req.query.deployment_id, req.query.tenant_id).then(function (result) {
    return res.json(result);
  }, function (err) {
    return handleError(res, err)
  });
};

function getRecipeQtyInBase(deployment_id, tenant_id) {
  var d = Q.defer();

  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id",
        itemName: "$recipeName",
        baseUnit: "$selectedUnitId.baseUnit.name",
        receipeQtyinBase: {$multiply: ["$selectedUnitId.conversionFactor", "$quantity"]}
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};


function getStockEntryByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.vendor.category",
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          storeId: "$store._id",
          categoryId: "$category._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        created: "$_id.created"
      }
    }], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getStockSaleByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.receiver,category",
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.receiver.category",
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$itemId",
          name: "$itemName",
          storeId: "$storeId",
          categoryId: "$categoryId",
          unitName: "$UnitName",
          conversionFactor: "$ConversionFactor"
        },
        totalQty: {$last: "$totalBalanceQty"},
        date: {$last: "$created"},
        billNo: {$last: "$billNo"},
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$date",
        billNo: "$billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getWastageStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getTransferStockByItemFromStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getTransferStockByItemToStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$items.toStore._id",
          categoryId: "$items.toStore.toCategory._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$menuItems.toStore._id",
          categoryId: "$menuItems.toStore.toCategory._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getOpeningStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId) {
  console.log(startDate, endDate, deployment_id, tenant_id, storeId);
  var d = Q.defer();
  //console.log(tenant_id, deployment_id);
  var beforeDate = new Date();
  beforeDate.setHours(0, 0, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        timeStamp: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};


function getOpeningStockByItem_ClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id, storeId) {
  console.log(startDate, endDate, deployment_id, tenant_id, storeId);
  var d = Q.defer();
  //console.log(tenant_id, deployment_id);
  var beforeDate = new Date();
  beforeDate.setHours(0, 0, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        timeStamp: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};


function dataBetweenDate_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),
    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId),
    getIndentDeliveryChallanConsumption_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId),
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};




function dataBeforeDate_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),

    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId),
    getIndentDeliveryChallanConsumption_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        if (v.itemId == "56e7d2a775b3e7281c96dd6e") {
          //console.log("Find At: " + i);
        }
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};


function dataBetweenDate_ClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    //getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),
    //getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    //getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    //getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    //getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    //getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    //getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    //getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};


function dataBeforeDate_ClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),


    //getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),

    //getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    //getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    //getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    //getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    //getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    //getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    //getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    //getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)

  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function getIndentDeliveryChallanConsumption_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId) {
  console.log('Inside getIndentDeliveryChallanConsumption');
  console.log(startDate, endDate, deployment_id, tenant_id, storeId)
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "16",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          price : "$units.basePrice",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}

      }
    },
    {
      $project: {
        _id: 0,
        totalIndentingChallanQty: "$_id.qty",
        totalIndentingChallanAmt: "$_id.price",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getIndentDeliveryChallanConsumption');
      console.log(err);
      d.reject('error');
    }
    console.log('getIndentDeliveryChallanConsumption', result);
    d.resolve(result);

  });
  return d.promise;
};

function getUniqueStore(data) {
  var str = [];
  _.forEach(data, function (dd, i) {
    var index = _.findIndex(str, {storeId: dd.storeId});
    if (index < 0) {
      str.push({storeId: dd.storeId, created: new Date(dd.created)});
    }
  });
  return str;
};

exports.getClosingQty = function (req, res) {
  var startDate = moment(new Date(req.query.fromDate)).toDate();
    //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();

  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;

  getClosingQty(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err);
  });
};

exports.getClosingQtyViaPhysical = function (req, res) {
  var startDate = moment(new Date(req.query.fromDate)).toDate();
    //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();

  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;
  console.log('getClosingQtyViaPhysical',startDate, endDate, deployment_id, tenant_id)
  getClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};


exports.getClosingQty_SemiProcessed = function (req, res) {
  var startDate = moment(new Date(req.query.fromDate)).toDate();
    //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();
  endDate.setDate(endDate.getDate() );
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;

  getClosingQty_SemiProcessed(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};

exports.getClosingQty_ProcessedFood = function (req, res) {
  var startDate = moment(new Date(req.query.fromDate)).toDate();
    //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();
  endDate.setDate(endDate.getDate() );
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;

  getClosingQty_ProcessedFood(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};

function getClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  var minDate = new Date(startDate);
  var databetween = [];
  var lastPrice = [];

  var returnData = {beforeDate: [], betweenDate: []};
  getStores(deployment_id, tenant_id).then(function (stores) {

    async.eachSeries(stores, function (s, callback) {
      s.beforeDate = [];
      s.betweenDate = [];

      var storeId = s.storeId.toString();
      var isKitchen = s.isKitchen.toString();
      getOpeningStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
        //getOpeningStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(result_op){
        ///console.log("result_op : "+result_op);
        var data = [];
        if (result_op.length > 0) {
          _.forEach(result_op, function (op, i) {
           var tempDate = moment(new Date(op.created)).toDate();
          //console.log(new Date('8/18/2016'));
          console.log(tempDate);
          tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
          // console.log('tempDate', tempDate);
          minDate = moment(new Date(tempDate)).toDate();
            data.push(op);
          });
          //console.log("minDate" + minDate);
          if (endDate.getTime() < minDate.getTime()) {
            s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
            return res.json(s);
          }
          else if (minDate.getTime() < startDate.getTime()) {
            //console.log("op < startDate");
            dataBeforeDate_ClosingQty(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
                //console.log(d);
              });
              dataBetweenDate_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
          else {
            ///console.log("op > startDate and op < end");
            dataBeforeDate_ClosingQty(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_ClosingQty(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
        }
        else {
          callback();
        }
      }, function (err) {
        //console.log(err);
        return handleError(res, err);
      });

    }, function (err) {
      if (err) {
        d.reject('er');
      } else {
        d.resolve(stores);
      }
    });
  }, function (err) {
    d.reject('error')
  });
  return d.promise;
};

function getClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  var minDate = new Date(startDate);
  var databetween = [];
  var lastPrice = [];

  var returnData = {beforeDate: [], betweenDate: []};
  getStores(deployment_id, tenant_id).then(function (stores) {

    async.eachSeries(stores, function (s, callback) {
      s.beforeDate = [];
      s.betweenDate = [];

      var storeId = s.storeId.toString();
      var isKitchen = s.isKitchen.toString();
      getOpeningStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
        //getOpeningStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(result_op){
        ///console.log("result_op : "+result_op);
        var data = [];
        if (result_op.length > 0) {
          _.forEach(result_op, function (op, i) {
           var tempDate = moment(new Date(op.created)).toDate();
          //console.log(new Date('8/18/2016'));
          console.log(tempDate);
          tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
          // console.log('tempDate', tempDate);
          minDate = moment(new Date(tempDate)).toDate();
            data.push(op);
          });
          //console.log("minDate" + minDate);
          if (endDate.getTime() < minDate.getTime()) {
            s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
            return res.json(s);
          }
          else if (minDate.getTime() < startDate.getTime()) {
            //console.log("op < startDate");
            dataBeforeDate_ClosingQtyViaPhysical(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
                console.log('7305',d);
              });
              dataBetweenDate_ClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
          else {
            ///console.log("op > startDate and op < end");
            dataBeforeDate_ClosingQtyViaPhysical(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_ClosingQtyViaPhysical(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
        }
        else {
          callback();
        }
      }, function (err) {
        //console.log(err);
        return handleError(res, err);
      });

    }, function (err) {
      if (err) {
        d.reject('er');
      } else {
        d.resolve(stores);
      }
    });
  }, function (err) {
    d.reject('error')
  });
  return d.promise;
};

function getClosingQty_SemiProcessed(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  var minDate = new Date(startDate);
  var databetween = [];
  var lastPrice = [];

  var returnData = {beforeDate: [], betweenDate: []};
  getStores(deployment_id, tenant_id).then(function (stores) {

    async.eachSeries(stores, function (s, callback) {
      s.beforeDate = [];
      s.betweenDate = [];

      var storeId = s.storeId.toString();
      var isKitchen = s.isKitchen.toString();
      getOpeningStockByItem_Intermediate(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
        //console.log("result_op : "+result_op);
        var data = [];
        if (result_op.length > 0) {
          _.forEach(result_op, function (op, i) {
           var tempDate = moment(new Date(op.created)).toDate();
          //console.log(new Date('8/18/2016'));
          console.log(tempDate);
          tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
          // console.log('tempDate', tempDate);
          minDate = moment(new Date(tempDate)).toDate();
            data.push(op);
          });
          //console.log("minDate" + minDate);
          if (endDate.getTime() < minDate.getTime()) {
            s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
            return res.json(s);
          }
          else if (minDate.getTime() < startDate.getTime()) {
            //console.log("op < startDate");
            dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
          else {
            //console.log("op > startDate and op < end");
            dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_IntermediateReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
        }
        else {
          callback();
        }
      }, function (err) {
        //console.log(err);
        return handleError(res, err);
      });

    }, function (err) {
      if (err) {
        d.reject('er');
      } else {
        d.resolve(stores);
      }
    });
  }, function (err) {
    d.reject('error')
  });
  return d.promise;
};

function getClosingQty_ProcessedFood(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  var minDate = new Date(startDate);
  var databetween = [];
  var lastPrice = [];

  var returnData = {beforeDate: [], betweenDate: []};
  getStores(deployment_id, tenant_id).then(function (stores) {

    async.eachSeries(stores, function (s, callback) {
      s.beforeDate = [];
      s.betweenDate = [];

      var storeId = s.storeId.toString();
      var isKitchen = s.isKitchen.toString();
      getOpeningStockByItem_FinishedFood(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
        //console.log("result_op : "+result_op);
        var data = [];
        if (result_op.length > 0) {
          _.forEach(result_op, function (op, i) {
         var tempDate = moment(new Date(op.created)).toDate();
          //console.log(new Date('8/18/2016'));
          console.log(tempDate);
          tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
          // console.log('tempDate', tempDate);
          minDate = moment(new Date(tempDate)).toDate();
            data.push(op);
          });
          //console.log("minDate" + minDate);
          if (endDate.getTime() < minDate.getTime()) {
            s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
            return res.json(s);
          }
          else if (minDate.getTime() < startDate.getTime()) {
            //console.log("op < startDate");
            dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
          else {
            //console.log("op > startDate and op < end");
            dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_FinishedFoodReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
        }
        else {
          callback();
        }
      }, function (err) {
        //console.log(err);
        return handleError(res, err);
      });

    }, function (err) {
      if (err) {
        d.reject('er');
      } else {
        d.resolve(stores);
      }
    });
  }, function (err) {
    d.reject('error')
  });
  return d.promise;
};

exports.getClosingQtyOutlet = function (req, res) {
  console.log('getClosingQtyOutlet');
  var startDate = moment(new Date(req.query.fromDate)).toDate();
  //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();

  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;
  var isKitchen=req.query.isKitchen;
  console.log(isKitchen);
  console.log(req.query.isKitchen);
  if(isKitchen =='true') {
    console.log('iffffffffffffffffffff')
    getClosingQtyOutlet(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (val) {
      console.log(val);
      return res.json(200, val);
    }, function (err) {
      var val = {};
    val.errorMessage = 'Opeining Not Available';
    return res.json(val);
    });
  }
  else {
    console.log('elseeeeeeeeeeeeeeeee');
    var val = {};
    val.errorMessage = 'Opening Not Available';
    return res.json(val);
  }
};

exports.getClosingQtyOutletViaPhysical = function (req, res) {
 // console.log('getClosingQtyOutlet');
  var startDate = moment(new Date(req.query.fromDate)).toDate();
  //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();

  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;
  var isKitchen=req.query.isKitchen;
  console.log(isKitchen);
  console.log(req.query.isKitchen);
  if(isKitchen =='true') {
   
    getClosingQtyOutletViaPhysical(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (val) {
      console.log(val);
      return res.json(200, val);
    }, function (err) {
      var val = {};
    val.errorMessage = 'Opeining Not Available';
    return res.json(val);
    });
  }
  else {

    var val = {};
    val.errorMessage = 'Opening Not Available';
    return res.json(val);
  }
};

function getClosingQtyOutletViaPhysical(startDate, endDate, deployment_id, tenant_id,storeId,isKitchen) {
 // console.log('getClosingQtyOutlet2')
  var d = Q.defer();
  var minDate = new Date(startDate);
  var databetween = [];
  var lastPrice = [];
  var s={};
  s.isKitchen=isKitchen
  s.storeId=storeId
  var returnData = {beforeDate: [], betweenDate: []};
  s.beforeDate = [];
  s.betweenDate = [];

  getOpeningStockByItem_ClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
    //getOpeningStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(result_op){
    ///console.log("result_op : "+result_op);
    console.log('getOpeningStockByItem_ClosingQty resolved');
    var data = [];
    if (result_op.length > 0) {
      console.log(result_op.length);
      _.forEach(result_op, function (op, i) {
        var tempDate = moment(new Date(op.created)).toDate();
        //console.log(new Date('8/18/2016'));
        console.log(tempDate);
        tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        // console.log('tempDate', tempDate);
        minDate = moment(new Date(tempDate)).toDate();
        data.push(op);
      });
      console.log('Mindate',minDate);
      console.log('enddate',endDate);
      console.log('startdate',startDate);
      //console.log("minDate" + minDate);
      if (endDate.getTime() < minDate.getTime()) {
        s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
        d.reject(s);
      }
      else if (minDate.getTime() < startDate.getTime()) {

        //console.log("op < startDate");
        dataBeforeDate_ClosingQtyViaPhysical(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          s.beforeDate = value;
          console.log('data before length',value.length);
          _.forEach(data, function (d, i) {
            s.beforeDate.push(d);
            //console.log(d);
          });
          dataBetweenDate_ClosingQtyViaPhysical(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            s.betweenDate = value;
            console.log('data between length',value.length);
            d.resolve(s);
          }, function (err) {
            d.reject(err);
          });
        }, function (err) {
          d.reject(err);
        });
      }
      else {
        ///console.log("op > startDate and op < end");
        dataBeforeDate_ClosingQtyViaPhysical(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          s.beforeDate = value;
          console.log('data before length',value.length);
          _.forEach(data, function (d, i) {
            s.beforeDate.push(d);
          });
          dataBetweenDate_ClosingQtyViaPhysical(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            s.betweenDate = value;
            console.log('data between length',value.length);
            d.resolve(s);
          }, function (err) {
            d.reject(err);
          });
        }, function (err) {
          d.reject(err);
        });
      }
    }
    else {
      console.log('callback');
      d.reject('NO opening found');
    }
  }, function (err) {
    //console.log(err);
    d.reject(err);
  });
  return d.promise;
};


function getClosingQtyOutlet(startDate, endDate, deployment_id, tenant_id,storeId,isKitchen) {
  console.log('getClosingQtyOutlet2')
  var d = Q.defer();
  var minDate = new Date(startDate);
  var databetween = [];
  var lastPrice = [];
  var s={};
  s.isKitchen=isKitchen
  s.storeId=storeId
  var returnData = {beforeDate: [], betweenDate: []};
  s.beforeDate = [];
  s.betweenDate = [];

  getOpeningStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
    //getOpeningStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(result_op){
    ///console.log("result_op : "+result_op);
    console.log('getOpeningStockByItem_ClosingQty resolved');
    var data = [];
    if (result_op.length > 0) {
      console.log(result_op.length);
      _.forEach(result_op, function (op, i) {
        var tempDate = moment(new Date(op.created)).toDate();
        //console.log(new Date('8/18/2016'));
        console.log(tempDate);
        tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        // console.log('tempDate', tempDate);
        minDate = moment(new Date(tempDate)).toDate();
        data.push(op);
      });
      console.log('Mindate',minDate);
      console.log('enddate',endDate);
      console.log('startdate',startDate);
      //console.log("minDate" + minDate);
      if (endDate.getTime() < minDate.getTime()) {
        s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
        d.reject(s);
      }
      else if (minDate.getTime() < startDate.getTime()) {

        //console.log("op < startDate");
        dataBeforeDate_ClosingQty(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          s.beforeDate = value;
          console.log('data before length',value.length);
          _.forEach(data, function (d, i) {
            s.beforeDate.push(d);
            //console.log(d);
          });
          dataBetweenDate_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            s.betweenDate = value;
            console.log('data between length',value.length);
            d.resolve(s);
          }, function (err) {
            d.reject(err);
          });
        }, function (err) {
          d.reject(err);
        });
      }
      else {
        ///console.log("op > startDate and op < end");
        dataBeforeDate_ClosingQty(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          s.beforeDate = value;
          console.log('data before length',value.length);
          _.forEach(data, function (d, i) {
            s.beforeDate.push(d);
          });
          dataBetweenDate_ClosingQty(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            s.betweenDate = value;
            console.log('data between length',value.length);
            d.resolve(s);
          }, function (err) {
            d.reject(err);
          });
        }, function (err) {
          d.reject(err);
        });
      }
    }
    else {
      console.log('callback');
      d.reject('NO opening found');
    }
  }, function (err) {
    //console.log(err);
    d.reject(err);
  });
  return d.promise;
};

function handleError(res, err) {
  return res.send(500, err);
}
