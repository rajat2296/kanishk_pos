'use strict';

var express = require('express');
var controller = require('./freshDesk.controller');

var router = express.Router();

router.get('/startCall', controller.startCall);
router.get('/endCall',controller.endCall)


module.exports = router;