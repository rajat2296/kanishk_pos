/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var PurchaseOrder = require('./purchaseOrder.model');

exports.register = function(socket) {
  PurchaseOrder.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  PurchaseOrder.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('purchaseOrder:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('purchaseOrder:remove', doc);
}