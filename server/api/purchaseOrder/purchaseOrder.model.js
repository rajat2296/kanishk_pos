'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PurchaseOrderSchema = new Schema({

  created: Date,
  updated: Date,
  store: {},
  vendor: {},
  items: [],
  poNumber: Number,
  daySerialNumber: Number,
  user: {},
  deployment_id: String,
  deployment: {},
  tenant_id: String,
  active: Boolean,
  isReceived: Boolean,
  status: String,
  charges: [],
  discountType:{type:String,default:'percent'},
  discount:{type:Number,default:0},
  discountAmt:{type:Number,default:0},
  cartage:{type:Number,default:0},
  
});

//PurchaseOrderSchema.createIndex({created: -1});

module.exports = mongoose.model('PurchaseOrder', PurchaseOrderSchema);
