'use strict';

var _ = require('lodash');
var PurchaseOrder = require('./purchaseOrder.model');
var email   = require("emailjs/email");

var server  = email.server.connect({
  user:    "posist",
  password:"pos123ist",
  host:    "smtp.sendgrid.net",
  ssl:     true
});
function emailSend(body){

  console.log(body);
  server.send({
    text: body.messageText,
    from: "Posist Info <no-reply@posist.info>",
    to: body.to,
    subject: body.subject,
    attachment: [
      {data: body.messageHtml, alternative: true}
    ]
  }, function (err, message) {
    console.log(message);
    console.log(err);
    //res.json(201, err || message);
  });

}

// Get list of purchaseOrders
exports.index = function(req, res) {
  console.log(req.query);
  var query = {};
  if(req.query.deployment_id)
    query.deployment_id = req.query.deployment_id;
  if(req.query.isReceived) {
    if(req.query.isReceived == true || req.query.isReceived == 'true')
      query.isReceived = true;
    else
      query.isReceived = false;
  }
  console.log(query);
  PurchaseOrder.find(query, {poNumber: 1, daySerialNumber: 1, user: 1, vendor: 1, created: 1}, function (err, purchaseOrders) {
    if(err) { return handleError(res, err); }
    return res.json(200, purchaseOrders);
  });
};

// Get a single purchaseOrder
exports.show = function(req, res) {
  PurchaseOrder.findById(req.params.id, function (err, purchaseOrder) {
    if(err) { return handleError(res, err); }
    if(!purchaseOrder) { return res.send(404); }
    return res.json(purchaseOrder);
  });
};

exports.findOne = function (req, res) {
  var query = {};
  if(req.query.id) {
    PurchaseOrder.findById(req.query.id, function (err, purchaseOrder) {
      if (err) {
        return handleError(res, err);
      }
      if (!purchaseOrder) {
        return res.send(404);
      }
      return res.json(purchaseOrder);
    });
  } else {
    res.status(500).send({err: "Please provide a valid id to search!"})
  }
};

// Creates a new purchaseOrder in the DB.
exports.create = function(req, res) {
  var emailBody ={
    messageHtml: req.body.email,
    to: req.body.vendor.email,
    subject: "New Purchase Order From " + req.body.deployment.name
  }
  emailSend(emailBody);
  PurchaseOrder.create(req.body, function(err, purchaseOrder) {
    if(err) { return handleError(res, err); }
    return res.json(201, purchaseOrder);
  });
};

// Updates an existing purchaseOrder in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  PurchaseOrder.findById(req.params.id, function (err, purchaseOrder) {
    if (err) { return handleError(res, err); }
    if(!purchaseOrder) { return res.send(404); }
    var updated = _.merge(purchaseOrder, req.body);
    updated.markModified("items");
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, purchaseOrder);
    });
  });
};

// Deletes a purchaseOrder from the DB.
exports.destroy = function(req, res) {
  PurchaseOrder.findById(req.params.id, function (err, purchaseOrder) {
    if(err) { return handleError(res, err); }
    if(!purchaseOrder) { return res.send(404); }
    purchaseOrder.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.getLastPurchaseOrderNumber = function (req, res) {

  console.log(req.query);
  var query = {};
  query.deployment_id = req.query.deployment_id;

  console.log(query);
  PurchaseOrder.find(query, {poNumber: 1, daySerialNumber: 1, created: 1}, {sort: {poNumber: -1}, limit: 1}, function (err, po) {
    console.log(po[0]);
    if(err)
      return handleError(res, err);
    if(po[0])
      return res.status(200).json({message: 'OK', errorcode: 200, data: po[0]});
    else
      return res.status(400).json({message: 'Not Found', errorcode: 400, data: null});
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
