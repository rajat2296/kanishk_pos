'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var NotificationSchema = new Schema({
  deployment_id: Schema.Types.ObjectId,
  api_key: String,
  active: Boolean,
  created: { type: Date, default: Date.now}
});

module.exports = mongoose.model('Notification', NotificationSchema);