'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var NotificationMessageSchema = new Schema({
  deployment_id: Schema.Types.ObjectId,
  message: String,
  type:String,
  created: { type: Date, default: Date.now}
});

module.exports = mongoose.model('NotificationMessage', NotificationMessageSchema);