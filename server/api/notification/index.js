'use strict';

var express = require('express');
var controller = require('./notification.controller');

var router = express.Router();

router.get('/', controller.index);

router.get('/getByQuery', controller.getByQuery);
router.get('/getByDeployment', controller.getByDeployment);
router.get('/getMessageByDeployment', controller.getMessageByDeployment);
router.get('/getOfferBills', controller.getOfferBills);
router.get('/:id', controller.show);
router.post('/createNMessage', controller.createNMessage);
router.post('/createNMessageForIOS', controller.createNMessageForIOS);
router.post('/DeleteByApiKey', controller.DeleteByApiKey);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;