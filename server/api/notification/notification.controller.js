'use strict';

var _ = require('lodash');
var Notification = require('./notification.model');
var NotificationMessage = require('./notificationmessage.model');
var Bill=require('../bill/bill.model');
var request =require('request');
var apn=require('apn');

// Get list of notifications
exports.index = function(req, res) {
  Notification.find(function (err, notifications) {
    if(err) { return handleError(res, err); }
    return res.json(200, notifications);
  });
};

// Get a single notification
exports.show = function(req, res) {
  Notification.findById(req.params.id, function (err, notification) {
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    return res.json(notification);
  });
};

// Get a single notification
exports.getByQuery = function(req, res) {
  Notification.find({api_key:req.query.key}, function (err, notification) {
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    return res.json(notification);
  });
};
// Get a single notification
exports.getByDeployment = function(req, res) {
  Notification.find({deployment_id:req.query.deployment_id}, function (err, notification) {
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    return res.json(notification);
  });
};

// Get a single notification
exports.getMessageByDeployment = function(req, res) {
  console.log(req.query);
  var page=parseInt(req.query.pageSize)*parseInt(req.query.pageIndex);

  NotificationMessage.find({deployment_id:req.query.deployment_id},{},{sort:{created:-1},limit:parseInt( req.query.pageSize) , skip:page}, function (err, notification) {
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    return res.json(notification);
  });
};
// Get a single notification
exports.getOfferBills = function(req, res) {
  console.log(req.query);
  var page=parseInt(req.query.pageSize)*parseInt(req.query.pageIndex);
  //Bill.find({deployment_id:req.query.deployment_id,$or:[{$gt:['aggregation.billDiscount',0]},{$gt:['aggregation.totalItemDiscount',0]}]},{serialNumber:1,instance:1,aggregation:1,daySerialNumber:1,_created:1,waiter:1,'_currentUser.username':1},{sort:{_created:-1},limit:parseInt( req.query.pageSize) , skip:page}, function (err, bill) {
  Bill.find({deployment_id:req.query.deployment_id, $or:[ {'aggregation.billDiscount':{$gt:0}},{ 'aggregation.totalItemDiscount':{$gt:0}}]},{serialNumber:1,instance:1,aggregation:1,daySerialNumber:1,_created:1,waiter:1,'_currentUser.username':1},{sort:{_created:-1},limit:parseInt( req.query.pageSize) , skip:page}, function (err, bill) {

    if(err) { return handleError(res, err); }
    if(!bill) { return res.send(404); }
    return res.json(bill);
  });
};

// Creates a new notification in the DB.
exports.create = function(req, res) {
  Notification.remove({api_key: req.body.api_key}, function (errr) {
    if (errr) {
      return handleError(res, errr);
    }

    Notification.create(req.body, function (err, notification) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(201, notification);
    });
  })
};


// Creates a new notification in the DB.
exports.createNMessage = function(req, res) {
  /*NotificationMessage.create(req.body, function (err, notification) {
    if (err) {
      return handleError(res, err);
    }
    var key = [];
    Notification.find({deployment_id: req.body.deployment_id}, function (err, nots) {
      _.forEach(nots, function (not) {
        key.push(not.api_key);
      })


      //key.push(req.body.api_key);

      var options = {
        method: 'POST',
        url: 'https://android.googleapis.com/gcm/send',
        headers: {
          'content-type': 'application/json',
          authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
        },
        body: {
          registration_ids: key,
          data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
        },
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          console.log(error);
        }
      })
      return res.json(201, notification);
    });
  });*/

  NotificationMessage.create(req.body, function (err, notification) {
    if (err) {
      return handleError(res, err);
    }
    var keyArr = [];
    Notification.find({deployment_id: req.body.deployment_id}, function (err, nots) {
      if(nots.length>0) {
        //    console.log(nots);
        _.forEach(nots, function (not) {

         //  console.log(not.api_key);
          var key=not.api_key;
          if(!(key == null ||key == undefined)){
            var stringAPN=key.split('@');
            if(stringAPN.length==2){
              var myDevice = new apn.Device(stringAPN[0]);
              var note = new apn.Notification();
              note.badge = 1;
              note.sound = "notification-beep.wav";
              note.alert = {"body": req.body.type};
              note.payload = {
                "message": req.body.message,
                "$state": "tab.account",
                "deployment_id": req.body.deployment_id
              };
              note.device = myDevice;
              var callback = function (errorNum, notification) {
                console.log('Error is: %s', errorNum);
                console.log("Note " + notification);
              }
              var options = {
                gateway: 'gateway.sandbox.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                errorCallback: callback,
                cert: 'PushNewsCert.pem',
                key: 'PushNewsKey.pem',
                passphrase: '1234',
                port: 2195,
                enhanced: true,
                cacheLength: 100,
                debug: true
              }
              var apnsConnection = new apn.Connection(options);
              apnsConnection.sendNotification(note);
            }else{
              keyArr.push(key);
            }
          }
        });
        /*Android*/
        var options = {
          method: 'POST',
          url: 'https://android.googleapis.com/gcm/send',
          headers: {
            'content-type': 'application/json',
            authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
          },
          body: {
            registration_ids: keyArr,
            data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
          },
          json: true
        };

        request(options, function (error, response, body) {
          if (error) {
            console.log(error);
          }
        });
        /*End Android*/
      }
      return res.json(201, {});
    });
  });
};

// Creates a new notification in the DB.
exports.createNMessageForIOS = function(req, res) {
  NotificationMessage.create(req.body, function (err, notification) {
    if (err) {
      return handleError(res, err);
    }
    var keyArr = [];
    Notification.find({deployment_id: req.body.deployment_id}, function (err, nots) {
      if(nots.length>0) {
        //    console.log(nots);
        _.forEach(nots, function (not) {
          //console.log(not);
          //       key.push(not.api_key);


          //key.push(req.body.api_key);

          /*var options = {
           method: 'POST',
           url: 'https://android.googleapis.com/gcm/send',
           headers: {
           'content-type': 'application/json',
           authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
           },
           body: {
           registration_ids: key,
           data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
           },
           json: true
           };

           request(options, function (error, response, body) {
           if (error) {
           console.log(error);
           }
           })*/
          //  console.log(not.api_key);
          var key=not.api_key;
          if(!(key == null ||key == undefined)){
            var stringAPN=key.split('@');
            if(stringAPN.length==2){
              var myDevice = new apn.Device(stringAPN[0]);

              var note = new apn.Notification();
              note.badge = 1;
              note.sound = "notification-beep.wav";
              note.alert = {"body": req.body.type};
              note.payload = {
                "message": req.body.message,
                "$state": "tab.account",
                "deployment_id": req.body.deployment_id
              };

              note.device = myDevice;

              var callback = function (errorNum, notification) {
                console.log('Error is: %s', errorNum);
                console.log("Note " + notification);
              }
              var options = {
                gateway: 'gateway.sandbox.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                errorCallback: callback,
                cert: 'PushNewsCert.pem',
                key: 'PushNewsKey.pem',
                passphrase: '1234',
                port: 2195,
                enhanced: true,
                cacheLength: 100,
                debug: true
              }
              var apnsConnection = new apn.Connection(options);
              apnsConnection.sendNotification(note);
            }else{
              keyArr.push(key);
            }
          }
        });
        /*Android*/
        var options = {
          method: 'POST',
          url: 'https://android.googleapis.com/gcm/send',
          headers: {
            'content-type': 'application/json',
            authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
          },
          body: {
            registration_ids: key,
            data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
          },
          json: true
        };

        request(options, function (error, response, body) {
          if (error) {
            console.log(error);
          }
        });
        /*End Android*/
      }
      return res.json(201, {});
    });
  });
};
function toHex(str) {
  var result = '';
  for (var i=0; i<str.length; i++) {
    result += str.charCodeAt(i).toString(16);
  }
  return result;
}
// Updates an existing notification in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Notification.findById(req.params.id, function (err, notification) {
    if (err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    var updated = _.merge(notification, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, notification);
    });
  });
};

// Deletes a notification from the DB.
exports.DeleteByApiKey = function(req, res) {
  Notification.findOne({api_key:req.body.api_key}, function (err, notification) {
    console.log(notification);
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    notification.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

// Deletes a notification from the DB.
exports.destroy = function(req, res) {
  Notification.findOne({api_key:req.body.api_key}, function (err, notification) {
    if(err) { return handleError(res, err); }
    if(!notification) { return res.send(404); }
    notification.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}