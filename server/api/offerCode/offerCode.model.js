'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OfferCodeSchema = new Schema({
  name: String,
  validFrom: Date,
  validTo: Date,
  codePrefix:String,
  noOfCodes:Number,
  deployment_id:Schema.Types.ObjectId,
  tenant_id:Schema.Types.ObjectId
});

module.exports = mongoose.model('OfferCode', OfferCodeSchema);