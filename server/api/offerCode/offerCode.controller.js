'use strict';

var _ = require('lodash');
var OfferCode = require('./offerCode.model');

// Get list of offerCodes
exports.index = function(req, res) {
  OfferCode.find({ deployment_id: req.query.deployment_id},function (err, offerCodes) {
    if(err) { return handleError(res, err); }
    return res.json(200, offerCodes);
  });
};

exports.getOfferCodeByCode = function(req, res) {
  OfferCode.find({ deployment_id: req.query.deployment_id},function (err, offerCodes) {
    if(err) { return handleError(res, err); }
    return res.json(200, offerCodes);
  });
};

//Get list By Name
exports.findByName = function(req, res) {
  var offerCodes=[];
  OfferCode.findOne({ deployment_id: req.query.deployment_id,name:req.query.name},function (err, offerCode) {
    if(err) { return handleError(res, err); }
    if(offerCode){offerCodes.push(offerCode)};
    return res.json(200, offerCodes);
  });
};
exports.findByPrefix = function(req, res) {
  var offerCodes=[];
  OfferCode.findOne({ deployment_id: req.query.deployment_id,codePrefix:req.query.codePrefix},function (err, offerCode) {
    if(err) { return handleError(res, err); }
    if(offerCode){offerCodes.push(offerCode)};
    return res.json(200, offerCodes);
  });
};

// Get a single offerCode
exports.show = function(req, res) {
  OfferCode.findById(req.params.id, function (err, offerCode) {
    if(err) { return handleError(res, err); }
    if(!offerCode) { return res.send(404); }
    return res.json(offerCode);
  });
};

// Creates a new offerCode in the DB.
exports.create = function(req, res) {
  OfferCode.create(req.body, function(err, offerCode) {
    if(err) { return handleError(res, err); }
    return res.json(201, offerCode);
  });
};

// Updates an existing offerCode in the DB.
exports.update = function(req, res) {
  console.log(req.body);
  if(req.body._id) { delete req.body._id; }
  OfferCode.findById(req.params.id, function (err, offerCode) {
    if (err) { return handleError(res, err); }
    if(!offerCode) { return res.send(404); }
    var updated = _.extend(offerCode, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, offerCode);
    });
  });
};

// Deletes a offerCode from the DB.
exports.destroy = function(req, res) {
  OfferCode.findById(req.params.id, function (err, offerCode) {
    if(err) { return handleError(res, err); }
    if(!offerCode) { return res.send(404); }
    offerCode.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}