/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var OfferCode = require('./offerCode.model');

exports.register = function(socket) {
  OfferCode.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  OfferCode.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('offerCode:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('offerCode:remove', doc);
}