/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Thing = require('./thing.model');
/*var kue = require('kue')
  , queue = kue.createQueue({
  prefix: 'q',
  redis: {
    port: 6379,
    host: '139.162.38.205',
    auth: 'ranjeetsinha'
  }
});*/
//var printer = require('printer');

// Get list of things
exports.index = function(req, res) {
  /*console.log(req.query);
  for(var i=0;i<100;i++){
  var job = queue.create('updateBill', req.query).removeOnComplete(true).save();
//console.log(job);
  job.on('complete', function(result){
    console.log('Job completed with data ', result);
    return res.json(200,{"name":result});
  });*/
//}


  /*Thing.find(function (err, things) {
    if(err) { return handleError(res, err); }
    return res.json(200, things);
  });*/
  
  //var _printers = printer.getPrinters();
  /*var _a = [];
  _printers.forEach(function(iPrinter, i) {
      var pO = printer.getSupportedPrintFormats(iPrinter.name);
      _a.push(pO);
  });*/

  /*printer.printDirect({
    printer: 'HP ePrint',
    data: "print from Node.JS buffer",
    type: "RAW",
    success: function(jobID) {
        console.log("sent to printer with ID: " + jobID);
        //var j = printer.getJob('HP LaserJet 3055 PCL6 Class Driver', jobID);

        //res.json(200, {jId: jobID});


    },
    error: function(err) {
        console.log(err);
        //res.json(200, {error: err});
    }
  });*/

  //console.log(printer.getSupportedJobCommands());

  
  //res.json(200, {});
};

// Get a single thing
exports.show = function(req, res) {
  Thing.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    return res.json(thing);
  });
};

// Creates a new thing in the DB.
exports.create = function(req, res) {
  Thing.create(req.body, function(err, thing) {
    if(err) { return handleError(res, err); }
    return res.json(201, thing);
  });
};

// Updates an existing thing in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Thing.findById(req.params.id, function (err, thing) {
    if (err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    var updated = _.merge(thing, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, thing);
    });
  });
};

// Deletes a thing from the DB.
exports.destroy = function(req, res) {
  Thing.findById(req.params.id, function (err, thing) {
    if(err) { return handleError(res, err); }
    if(!thing) { return res.send(404); }
    thing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}