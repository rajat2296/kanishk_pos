'use strict';

var _ = require('lodash');
var Smstransaction = require('./smstransaction.model');

// Get list of smstransactions
exports.index = function(req, res) {
  Smstransaction.find(function (err, smstransactions) {
    if(err) { return handleError(res, err); }
    return res.json(200, smstransactions);
  });
};

// Get a single smstransaction
exports.show = function(req, res) {
  Smstransaction.findById(req.params.id, function (err, smstransaction) {
    if(err) { return handleError(res, err); }
    if(!smstransaction) { return res.send(404); }
    return res.json(smstransaction);
  });
};

// Creates a new smstransaction in the DB.
exports.create = function(req, res) {
  Smstransaction.create(req.body, function(err, smstransaction) {
    if(err) { return handleError(res, err); }
    return res.json(201, smstransaction);
  });
};

// Updates an existing smstransaction in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Smstransaction.findById(req.params.id, function (err, smstransaction) {
    if (err) { return handleError(res, err); }
    if(!smstransaction) { return res.send(404); }
    var updated = _.merge(smstransaction, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, smstransaction);
    });
  });
};

// Deletes a smstransaction from the DB.
exports.destroy = function(req, res) {
  Smstransaction.findById(req.params.id, function (err, smstransaction) {
    if(err) { return handleError(res, err); }
    if(!smstransaction) { return res.send(404); }
    smstransaction.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}