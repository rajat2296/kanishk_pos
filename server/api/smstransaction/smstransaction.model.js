'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SmstransactionSchema = new Schema({

  type: String,
  timestamp: {type: Date, default: Date.now},
  quantity:Number,
  rate:Number ,
  user:{},
  deployment_id:Schema.Types.ObjectId
});

module.exports = mongoose.model('Smstransaction', SmstransactionSchema);