/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Smstransaction = require('./smstransaction.model');

exports.register = function(socket) {
  Smstransaction.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Smstransaction.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('smstransaction:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('smstransaction:remove', doc);
}