/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockMasterUser = require('./stockMasterUser.model');

exports.register = function(socket) {
  StockMasterUser.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockMasterUser.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockMasterUser:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockMasterUser:remove', doc);
}