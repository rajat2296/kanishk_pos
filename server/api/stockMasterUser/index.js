'use strict';

var express = require('express');
var controller = require('./stockMasterUser.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/getTransactions', controller.getTransactions);
router.get('/getDeploymentName', controller.getDeploymentName);
router.get('/:id', controller.show);
router.post('/deleteTransaction', controller.deleteTransaction);
router.post('/deleteAllTransactions', controller.deleteAllTransactions);
router.post('/getTenantName', controller.getTenantName);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;