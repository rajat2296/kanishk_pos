'use strict';

var _ = require('lodash');
var StockMasterUser = require('./stockMasterUser.model');
var StockTransaction = require('../stockTransaction/stockTransaction.model');
var Tenant = require('../tenant/tenant.model');
var Deployment = require('../deployment/deployment.model');
var StockLogs = require('../stockLogs/stockLogs.model');
var moment = require('moment');
var ObjectId = require('mongoose').Types.ObjectId;

// Get list of stockMasterUsers
exports.index = function(req, res) {
  StockMasterUser.find(function (err, stockMasterUsers) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(stockMasterUsers);
  });
};

// Get a single stockMasterUser
exports.show = function(req, res) {
  StockMasterUser.findById(req.params.id, function (err, stockMasterUser) {
    if(err) { return handleError(res, err); }
    if(!stockMasterUser) { return res.status(404).send('Not Found'); }
    return res.json(stockMasterUser);
  });
};

// Creates a new stockMasterUser in the DB.
exports.create = function(req, res) {
  StockMasterUser.create(req.body, function(err, stockMasterUser) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(stockMasterUser);
  });
};

// Updates an existing stockMasterUser in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  StockMasterUser.findById(req.params.id, function (err, stockMasterUser) {
    if (err) { return handleError(res, err); }
    if(!stockMasterUser) { return res.status(404).send('Not Found'); }
    var updated = _.merge(stockMasterUser, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(stockMasterUser);
    });
  });
};

// Deletes a stockMasterUser from the DB.
exports.destroy = function(req, res) {
  StockMasterUser.findById(req.params.id, function (err, stockMasterUser) {
    if(err) { return handleError(res, err); }
    if(!stockMasterUser) { return res.status(404).send('Not Found'); }
    stockMasterUser.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

exports.deleteTransaction = function(req, res) {
  var query = {};
  if(req.body.transaction)
    query.transaction = req.body.transaction;
  else
    return handleError(res, err);
  
  if(req.body.comment)
    query.comment = req.body.comment;
  else
    return handleError(res, err);

  query.deletionDate = new Date();

  StockTransaction.find({_id: new ObjectId(req.body.transaction._id)}, function (err, stockTransaction) {
    if(err) { return handleError(res, err); }
    if(!stockTransaction) { return res.status(404).send('Not Found'); }
    StockLogs.create(query, function(err, log) {
      if(err)
        return handleError(res, err);
      StockTransaction.remove({_id: new ObjectId(req.body.transaction._id)}, function(err) {
        if(err) { return handleError(res, err); }
        return res.status(200).json({message:'Transaction deleted successfully'});
      });
    });    
  });
}

exports.deleteAllTransactions = function(req, res) {
  var query = {};
  if(req.body.deployment_id)
    query.deployment_id = req.body.deployment_id;
  else
    return handleError(res, err);
  
  if(req.body.comment)
    query.comment = req.body.comment;
  else
    return handleError(res, err);

  if(req.body.deletionType)
    query.deletionType = req.body.deletionType;
  else
    return handleError(res, err);

  query.deletionDate = new Date();

  StockTransaction.find({deployment_id: new ObjectId(req.body.deployment_id)}, function (err, stockTransaction) {
    if(err) { return handleError(res, err); }
    if(!stockTransaction) { return res.status(404).send('Not Found'); }
    StockLogs.create(query, function(err, log) {
      if(err)
        return handleError(res, err);
      StockTransaction.remove({deployment_id: new ObjectId(req.body.deployment_id)}, function(err) {
        if(err) { return handleError(res, err); }
        return res.status(200).json({message:'All Transactions deleted successfully'});
      });
    });    
  });
}

exports.getTransactions = function(req, res) {
  var query = {};

  if(req.query.deployment_id)
    query.deployment_id = req.query.deployment_id;

  if(req.query.fromDate && req.query.toDate)
  {
    query.created = {$gte: new Date(req.query.fromDate), $lte: new Date(req.query.toDate)};
  }
  else
    return handleError(res, err);

  if(req.query.transactionType)
    query.transactionType = req.query.transactionType;
  
  StockTransaction.find(query, function(err, stockTransactions) {
    if(err)
      return handleError(res,err);
    else
      return res.status(200).json(stockTransactions);
  });
}

exports.getTenantName = function(req, res) {
    var query = {};
    query.name = new RegExp(req.body.search, 'i');
    Tenant.find(query,{_id:1,name:1}, function(err, tenants) {
      if (err)
        return handleError(res, err);
        return res.status(200).json(tenants);
    });
}

exports.getDeploymentName = function(req, res) {
    var query = {};
    query.tenant_id = req.query.tenant_id;
    Deployment.find(query,{_id:1,name:1, tenant_id:1}, function(err, deployments) {
      if (err)
        return handleError(res, err);
        return res.status(200).json(deployments);
    });
}

function handleError(res, err) {
  return res.status(500).send(err);
}