'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ShiftTimeSchema = new Schema({
  name: String,
  deployment_id: String,
  tenant_id: String,
  shiftFrom:Date,
  shiftTo:Date
});

module.exports = mongoose.model('ShiftTime', ShiftTimeSchema);