/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var LeadForm = require('./leadForm.model');

exports.register = function(socket) {
  LeadForm.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  LeadForm.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('leadForm:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('leadForm:remove', doc);
}