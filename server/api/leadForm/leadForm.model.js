'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/*
var LeadFormSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});
*/

var LeadformSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                if(v === '') return true;
                return /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(v);
            },
            message: '{VALUE} is not a valid phone number!'
        }
    },
    brandName: {
        type: String,
        required: true
    },
    companyName: String,
    city: String,
    country: String,
    notes: String
});

LeadformSchema
    .path('email')
    .validate(function(email) {
        return email.length;
    }, 'Email cannot be blank');


module.exports = mongoose.model('LeadForm', LeadformSchema);

/*
module.exports = mongoose.model('LeadForm', LeadFormSchema);*/
