'use strict';

var _ = require('lodash');
var LeadForm = require('./leadForm.model');
var Client = require('node-rest-client').Client;
var email   = require("emailjs/email");

var server  = email.server.connect({
  user:    "posist",
  password:"pos123ist",
  host:    "smtp.sendgrid.net",
  ssl:     true
});
function emailSend( body){

  //console.log(req.body);
  server.send({
    text: body.messageText,
    from: "Posist Info <no-reply@posist.info>",
    to: body.to,
    subject: body.messageText,
    attachment: [
      {data: body.messageHtml, alternative: true}
    ]
  }, function (err, message) {
    /*console.log(message);*/
    // console.log(err);
    //res.json(201, err || message);
  });

}

// Get list of leadForms
exports.index = function(req, res) {
  LeadForm.find(function (err, leadForms) {
    if(err) { return handleError(res, err); }
    return res.json(200, leadForms);
  });
};

// Get a single leadForm
exports.show = function(req, res) {
  LeadForm.findById(req.params.id, function (err, leadForm) {
    if(err) { return handleError(res, err); }
    if(!leadForm) { return res.send(404); }
    return res.json(leadForm);
  });
};

// Creates a new leadForm in the DB.
exports.create = function(req, res) {
 /* LeadForm.create(req.body, function(err, leadForm) {
    if(err) { return handleError(res, err); }
    return res.json(201, leadForm);
  });*/
  var data = JSON.parse(req.body.data);
  var leadData = data.leadData;
  console.log(data);
  //console.log(leadData);
  LeadForm.create(data, function (err, leadform) {
    if (err) {
      console.log(err);
      return handleError(res, err);
    }
    else {
      var client = new Client();
      var args = {
        data: leadData,
        headers: {"Content-Type": "application/json"}
      }
      //todo change the leadsquared keys.
      var createURL = "https://api.leadsquared.com/v2/LeadManagement.svc/Lead.Create?accessKey=u$r9d344f6ff6f718db001c9ac1d42af4ca&secretKey=830dd1f073ecabf9eea426ba5a2a1485d1f92c06";
      client.post(createURL, args, function (data1, response) {
        var leadDetail={};
        leadDetail.messageText="New Lead From Posist Website.";
        var mailbody="Hi Admin,<br/> Find below the lead detail<br/> <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td>Name</td><td>"+data.name+"</td></tr><tr><td>Email</td><td>"+data.email+"</td></tr><tr><td>Phone</td><td>"+data.phone+"</td></tr><tr><td>Brand Name</td><td>"+data.brandName+"</td></tr><tr><td>Company Name</td><td>"+data.companyName+"</td></tr><tr><td>City</td><td>"+data.city+"</td></tr><tr><td>Country</td><td>"+data.country+"</td></tr><tr><td>Notes</td><td>"+data.notes+"</td></tr></table> <br/>";
        leadDetail.messageHtml=mailbody;
        leadDetail.to='info@posist.com';
            emailSend(leadDetail);
        var leadA={};
        leadA.messageText="Your enquiry form has been submitted.";
        var mailAbody="Hi "+data.name+",<br/> Thanks for submitting your query. <br/>we will revert back to you soon.<br/><br/>Thanks & Regards,<br/> Posist Team ";
        leadA.messageHtml=mailAbody;
        leadA.to=data.email;
        emailSend(leadA);

        return res.status(200).json({status:"SuccessFull"});

        //  console.log(data.toString('utf8'));
       // data = JSON.parse(data.toString('utf8'));
        //console.log('POST');
       // console.log(data);
       // if (data.Status === 'Success') {
        /* }
        else {
          return res.status(200).json({status:"SuccessFull"});
          //return res.status(500).send(data.ExceptionMessage)
        }*/
      });
    }
  });

};

// Updates an existing leadForm in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  LeadForm.findById(req.params.id, function (err, leadForm) {
    if (err) { return handleError(res, err); }
    if(!leadForm) { return res.send(404); }
    var updated = _.merge(leadForm, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, leadForm);
    });
  });
};

// Deletes a leadForm from the DB.
exports.destroy = function(req, res) {
  LeadForm.findById(req.params.id, function (err, leadForm) {
    if(err) { return handleError(res, err); }
    if(!leadForm) { return res.send(404); }
    leadForm.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}