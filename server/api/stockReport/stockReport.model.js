'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    StockRequirementSchema=require('mongoose').model('StockRequirement').schema,
    StockTransactionSchema=require('mongoose').model('StockTransaction').schema,
    BillSchema=require('mongoose').model('Bill').schema,
    StockRecipeSchema=require('mongoose').model('StockRecipe').schema,
    ItemSchema=require('mongoose').model('Item').schema,
    StockItemSchema=require('mongoose').model('StockItem').schema,
    BaseKitchenItemSchema = require('mongoose').model('baseKitchenItem').schema; 

var StockReportSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

 var StockReport = mongoose.model('StockReport', StockReportSchema);
 var StockRequirement =mongoose.model('StockRequirement', StockRequirementSchema);
 var StockTransaction = mongoose.model('StockTransaction', StockTransactionSchema);
 var Bill=mongoose.model('Bill',BillSchema);
 var StockRecipe=mongoose.model('StockRecipe',StockRecipeSchema);
 var StockItem=mongoose.model('StockItem',StockItemSchema);
 var Item=mongoose.model('Item',ItemSchema);
 var BaseKitchenItem = mongoose.model ('BaseKitchenItem', BaseKitchenItemSchema);
module.exports={
	StockReport:StockReport,
    StockTransaction:StockTransaction,
  	StockRequirement:StockRequirement,
  	Bill:Bill,
  	StockRecipe:StockRecipe,
    StockItem:StockItem,
    Item:Item, 
    BaseKitchenItem:BaseKitchenItem
}
//module.exports = mongoose.model('StockReport', StockReportSchema);