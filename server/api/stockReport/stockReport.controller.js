'use strict';

var _ = require('lodash');
var Q = require('q');
var async = require('async');
var Stock = require('./stockReport.model');
var ObjectId = require('mongoose').Types.ObjectId;
var config = require('../../config/environment');
var kue = require('kue')
  , queue = kue.createQueue(config.redisConn);

exports.lastPrice = function (req, res) {
  var startDate = "11/1/2015 0:00:00 AM";
  var endDate = "11/10/2015 0:00:00 AM";
  var data = {};
  var itemId = "5639ea66fd5db8e011dfb975";//"5606a4a8c1b489140a2c944e";
  //var toStoreId="5606a613c1b489140a2c9827";
  var toStoreId = "5624ba4e60bf650810a03012";
  //var storeId="5606a60cc1b489140a2c9826";
  var storeId = "5624ba4360bf650810a03011";
  var itemType = undefined;//"RawMaterial";
  //var deployment_id="556eadf102fad90807111eac";
  var deployment_id = "556ec68900a0b4200edafd13";
  //var deployment_id="560555825c70b1a24289259f";
  var tenant_id = "556e9b0302fad90807111e34";
  // getLastPriceOfItem_RawMaterial(startDate,endDate,itemId,deployment_id,tenant_id,function(val){
  //   //console.log("lastPrice: "+val[0].lastPrice);
  //   return res.json(val);
  // });
  // getLastPriceOfItem_RawMaterial_InBase(startDate,endDate,itemId,deployment_id,tenant_id,function(val){
  //   //console.log("lastPrice: "+val[0].lastPrice);
  //   return res.json(val);
  // });
  // getWastageReports(startDate,endDate,itemType,function(val){
  //   return res.json(val);
  // });
  getWastageReports(startDate, endDate, itemType, deployment_id, tenant_id, function (val) {
    return res.json(val);
  });
  // getStockEntryByItem_VarianceReport_DateWise(startDate,endDate,itemId,function(val){
  //   return res.json(val);
  // });
  // getStockSaleByItem_VarianceReport_DateWise(startDate,endDate,itemId,function(val){
  //   return res.json(val);
  // });
  // getWastageStockByItem_Variance_DateWise(startDate,endDate,itemId,function(val){
  //   return res.json(val);
  // });
  // getOpeningStockByItem(startDate,endDate,itemId,deployment_id,tenant_id,function(val){
  //     return res.json(val);
  //   });
  // getPhysicalStockByItem(startDate,endDate,itemId,function(val){
  //   return res.json(val);
  // });
  // checkOpeningStock(startDate,function(val){
  //   return res.json(val);
  // });
  // getStockSaleByItem_ConsumptionReport(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getStockSale_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getTransfer_ItemConsumptionReport_MenuItem(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getPhysical_ItemConsumptionReport_MenuItem(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getWastage_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getMenuQtyOfItems(deployment_id,tenant_id,itemId,function(val){
  //   return res.json(val);
  // });
  // getProcessedAndIntermediateReceipeDetails_Sale(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getProcessedAndIntermediateReceipeDetails_Wastage(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getProcessedAndIntermediateReceipeDetails_Physical(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate,endDate,itemId,deployment_id,tenant_id,toStoreId,function(val){
  //   return res.json(val);
  // });
  // getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(val){
  //   return res.json(val);
  // });
  // getTransferStockByItemFromStore(startDate,endDate,itemId,deployment_id,tenant_id,function(val){
  //   return res.json(val);
  // });

  // getTransferStockByItemToStore(startDate,endDate,itemId,deployment_id,tenant_id,toStoreId,function(val){
  //     return res.json(val);
  //   });

  // BillgetItemWiseReport(deployment_id,tenant_id,startDate,endDate,itemId,function(val){
  //   return res.json(val);
  // });
  // getStockEntryByItem_ConsumptionReport(startDate,endDate,itemId,deployment_id,tenant_id,function(val){
  //   return res.json(val);
  // });
  // getRecipesOfBillItems(startDate,endDate,deployment_id,tenant_id,"55c5e9bc744f507c206772db",10,4,function(val){
  //   return res.json(val);
  // });
  // getLatestTransaction(deployment_id,tenant_id,function(val){
  //   return res.json(val);
  // });
  // getLastMonthAveragePrice(deployment_id,tenant_id,itemId,function(val){
  //   return res.json(val);
  // });
  // getAllRawMaterials_ForRecipePricing(deployment_id,tenant_id,function(val){
  //   return res.json(val);
  // });
  // getAllRawMaterials_ForRecipePricingBaseKitchen(deployment_id,tenant_id,function(val){
  //   return res.json(val);
  // });
};

exports.RawMaterialPricing_Receipe = function (req, res) {
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  getAllRawMaterials_ForRecipePricing(deployment_id, tenant_id, function (val) {
    return res.json(val);
  });
};

exports.RawMaterialPricing_ReceipeBasekitchen = function (req, res) {
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  getAllRawMaterials_ForRecipePricingBaseKitchen(deployment_id, tenant_id, function (val) {
    return res.json(val);
  });
}

exports.RawMaterialPricing_ReceipeConsumptionReport = function (req, res) {
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  getAllRawMaterials_ForRecipePricingConsumptionReport(deployment_id, tenant_id, function (val) {
    return res.json(val);
  });
};

exports.RawMaterialPricing_ReceipeBasekitchenConsumptionReport = function (req, res) {
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  getAllRawMaterials_ForRecipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, function (val) {
    return res.json(val);
  });
}




function getAllRawMaterials_ForRecipePricingBaseKitchen(deployment_id, tenant_id, callback1) {
  Stock.BaseKitchenItem.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      var items = [];
      _.forEach(result_entry, function (r, i) {
        var itm = {itemId: r.itemId, itemName: r.itemName, monthAverage: 0.0000, lastPrice: 0.0000};
        items.push(itm);
      });

      getLastMonthAveragePrice_ReceipePricingBaseKitchen(deployment_id, tenant_id, function (val) {
        if (val != null) {
          _.forEach(val, function (v, i) {
            _.forEach(items, function (itm, ii) {
              if (itm.itemId == v.itemId) {
                itm.monthAverage = parseFloat(v.lastPrice).toFixed(4);
              }
            });
          });
          //callback1(items);
          getLastPrice_AllRaw_ReceipePricingBaseKitchen(deployment_id, tenant_id, function (result) {
            if (result != null) {
              _.forEach(result, function (r, i) {
                _.forEach(items, function (itm, ii) {
                  if (itm.itemId == r.itemId) {
                    itm.lastPrice = parseFloat(r.lastPrice).toFixed(4);
                  }
                });
              });
            }
            callback1(items);
          });
        }
      });
      //callback(items);
    }
  });
};

function getAllRawMaterials_ForRecipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, callback1) {
  Stock.BaseKitchenItem.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      var items = [];
      _.forEach(result_entry, function (r, i) {
        var itm = {itemId: r.itemId, itemName: r.itemName, monthAverage: 0, lastPrice: 0};
        items.push(itm);
      });

      getLastMonthAveragePrice_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, function (val) {
        if (val != null) {
          _.forEach(val, function (v, i) {
            _.forEach(items, function (itm, ii) {
              if (itm.itemId == v.itemId) {
                itm.monthAverage = parseFloat(v.lastPrice).toFixed(3);
              }
            });
          });
          //callback1(items);
          getLastPrice_AllRaw_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, function (result) {
            if (result != null) {
              _.forEach(result, function (r, i) {
                _.forEach(items, function (itm, ii) {
                  if (itm.itemId == r.itemId) {
                    itm.lastPrice = parseFloat(r.lastPrice).toFixed(3);
                  }
                });
              });
            }
            callback1(items);
          });
        }
      });
      //callback(items);
    }
  });
};


function getAllRawMaterials_ForRecipePricing(deployment_id, tenant_id, callback1) {
  Stock.StockItem.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      var items = [];
      _.forEach(result_entry, function (r, i) {
        var itm = {itemId: r._id, itemName: r.itemName, monthAverage: 0.0000, lastPrice: 0.0000, yeild: r.yeild || 0};
        console.log('itm',itm)
        items.push(itm);
      });

      getLastMonthAveragePrice_ReceipePricing(deployment_id, tenant_id, function (val) {
        if (val != null) {
          _.forEach(val, function (v, i) {
            _.forEach(items, function (itm, ii) {
              if (itm.itemId == v.itemId) {
                itm.monthAverage = parseFloat(v.lastPrice).toFixed(4);
              }
            });
          });
          //callback1(items);
          getLastPrice_AllRaw_ReceipePricing(deployment_id, tenant_id, function (result) {
            if (result != null) {
              _.forEach(result, function (r, i) {
                _.forEach(items, function (itm, ii) {
                  if (itm.itemId == r.itemId) {
                    itm.lastPrice = parseFloat(r.lastPrice).toFixed(4);
                  }
                });
              });
            }
            callback1(items);
          });
        }
      });
      //callback(items);
    }
  });
};

function getAllRawMaterials_ForRecipePricingConsumptionReport(deployment_id, tenant_id, callback1) {
  Stock.StockItem.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      var items = [];
      _.forEach(result_entry, function (r, i) {
        var itm = {itemId: r._id, itemName: r.itemName, monthAverage: 0, lastPrice: 0};
        items.push(itm);
      });

      getLastMonthAveragePrice_ReceipePricingConsumptionReport(deployment_id, tenant_id, function (val) {
        if (val != null) {
          _.forEach(val, function (v, i) {
            _.forEach(items, function (itm, ii) {
              if (itm.itemId == v.itemId) {
                itm.monthAverage = parseFloat(v.lastPrice).toFixed(3);
              }
            });
          });
          //callback1(items);
          getLastPrice_AllRaw_ReceipePricingConsumptionReport(deployment_id, tenant_id, function (result) {
            if (result != null) {
              _.forEach(result, function (r, i) {
                _.forEach(items, function (itm, ii) {
                  if (itm.itemId == r.itemId) {
                    itm.lastPrice = parseFloat(r.lastPrice).toFixed(3);
                  }
                });
              });
            }
            callback1(items);
          });
        }
      });
      //callback(items);
    }
  });
};

function getLastPrice_AllRaw_ReceipePricingBaseKitchen(deployment_id, tenant_id, callback) {
  Stock.StockRequirement.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        requirementType: 1,
        tenant_id: new ObjectId(tenant_id)
        //created:{$gte:new Date(startDate),$lte:new Date(endDate)}
      }
    },
    {
      $unwind: "$store.vendor.category"
    },
    {
      $unwind: "$store.vendor.category.items"
    },
    {
      $unwind: "$store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });
      callback(result_entry);
    }
  });
};

function getLastPrice_AllRaw_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, callback) {
  Stock.StockRequirement.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        requirementType: 1,
        tenant_id: new ObjectId(tenant_id)
        //created:{$gte:new Date(startDate),$lte:new Date(endDate)}
      }
    },
    {
      $unwind: "$store.vendor.category"
    },
    {
      $unwind: "$store.vendor.category.items"
    },
    {
      $unwind: "$store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      console.log('getLastPrice_AllRaw_ReceipePricingBaseKitchenConsumptionReport',result_entry);
      /*_.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });*/
      callback(result_entry);
    }
  });
};

function getLastPrice_AllRaw_ReceipePricing(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id)
        //created:{$gte:new Date(startDate),$lte:new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
      {
      $sort:{created: 1}
    },
    {
      $sort:{created: 1}
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });
      callback(result_entry);
    }
  });
};

function getLastPrice_AllRaw_ReceipePricingConsumptionReport(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id)
        //created:{$gte:new Date(startDate),$lte:new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
  /*  {
      $sort:_created:-1
    },*/
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
     /* _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });*/
      callback(result_entry);
    }
  });
};

function getLastMonthAveragePrice_ReceipePricingBaseKitchen(deployment_id, tenant_id, callback) {
  console.log('in here')
  var startDate = new Date();
  var endDate = new Date();
  startDate.setDate(endDate.getDate() - 30);
  Stock.StockRequirement.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        //transactionType: 12,
        tenant_id: new ObjectId(tenant_id),
        created:{$gte:new Date(startDate),$lte:new Date(endDate)}
        //"_store.vendor.category.item._id":itemId
      }
    },
    {
      $unwind: "$store.vendor.category"
    },
    {
      $unwind: "$store.vendor.category.items"
    },
    {
      $unwind: "$store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $sort:{_created:-1}
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$avg: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });
      callback(result_entry);
    }
  });
};

function getLastMonthAveragePrice_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, callback) {
  var startDate = new Date();
  var endDate = new Date();
  startDate.setDate(endDate.getDate() - 30);
  Stock.StockRequirement.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        //transactionType: 1,
        tenant_id: new ObjectId(tenant_id)
        // created:{$gte:new Date(startDate),$lte:new Date(endDate)}
        //"_store.vendor.category.item._id":itemId
      }
    },
    {
      $unwind: "$store.vendor.category"
    },
    {
      $unwind: "$store.vendor.category.items"
    },
    {
      $unwind: "$store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$avg: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      console.log('getLastMonthAveragePrice_ReceipePricingBaseKitchenConsumptionReport',result_entry);
     /* _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });*/
      callback(result_entry);
    }
  });
};

function getLastMonthAveragePrice_ReceipePricing(deployment_id, tenant_id, callback) {
  var startDate = new Date();
  var endDate = new Date();
  startDate.setDate(endDate.getDate() - 30);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        created: {$gte: new Date(startDate), $lte: new Date(endDate)}
        //"_store.vendor.category.item._id":itemId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $sort:{_created:-1}
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$avg: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });
      callback(result_entry);
    }
  });
};


function getLastMonthAveragePrice_ReceipePricingConsumptionReport(deployment_id, tenant_id, callback) {
  var startDate = new Date();
  var endDate = new Date();
  startDate.setDate(endDate.getDate() - 30);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        created: {$gte: new Date(startDate), $lte: new Date(endDate)}
        //"_store.vendor.category.item._id":itemId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$avg: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      /*_.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });*/
      callback(result_entry);
    }
  });
};

function getStockSale_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
        //"_store.receiver.category.items._id":itemId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$items.qty",
          receipeDetails: "$items.receipeDetails"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        //totalSaleQty:"$totalQty",
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        receipeDetails: "$_id.receipeDetails"
      }
    }
  ], function (err, result_sale) {
    if (err) {
    }
    else {
      _.forEach(result_sale, function (r1, i) {
        _.forEach(r1.receipeDetails, function (itm, ii) {
          getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, itm._id, deployment_id, tenant_id, storeId, function (val) {
            r1.receipeDetails[ii] = val[0];
          });
        });
      });
      callback(result_sale);
      // async.eachSeries(result_sale, function (r1, callback1){
      //       //console.log(r1.itemName);
      //       r1.totalSaleQty=parseFloat(r1.totalSaleQty);
      //       getMenuIdOfItems_FromReceipe(deployment_id,tenant_id,r1.itemId,function(v3){
      //         console.log(v3);
      //         r1.menuId=v3[0].menuItemId;
      //         getMenuQtyOfItems(deployment_id,tenant_id,r1.menuId,function(val2){
      //           console.log(r1);
      //           if(val2!=undefined){
      //             r1.menuQty=parseFloat(val2[0].MenuQty);
      //           }
      //           console.log(val2);
      //             getRecipesOfBillItems(startDate,endDate,deployment_id,tenant_id,r1.menuId,r1.menuQty,r1.totalSaleQty,function(val){
      //               console.log(val);
      //               r1.items=val;
      //               //callback1();
      //               async.eachSeries(r1.items, function (r2, callback2){
      //                 getLastPriceOfItem_RawMaterial_InBase(startDate,endDate,r2.stockItemId,deployment_id,tenant_id,function(val1){
      //                    //console.log(val1);
      //                    _.forEach(val1,function(v,i){
      //                     var index1=_.findIndex(r1.items,{stockItemId:v.itemId});
      //                     //console.log(index1);
      //                     if(index1<0){
      //                       //sdata.push(v);
      //                     }
      //                     else
      //                     {
      //                       r1.items[index1].itemSaleAmount=parseFloat(r1.items[index1].itemSaleQty)*parseFloat(v.lastPrice);
      //                     }
      //                   });
      //                   callback2();
      //                 });
      //               }, function (err) {
      //               if (err) {
      //                 return handleError(res, err);
      //               } else {
      //                 _.forEachRight(r1.items,function(s,i){
      //                   // if(s.stockItemId==itemId){
      //                   //   if(s.preferedConversionFactor!=undefined){
      //                   //     s.itemSaleQty=parseFloat(s.itemSaleQty)/parseFloat(s.preferedConversionFactor);
      //                   //     s.itemSaleAmount=parseFloat(s.itemSaleAmount)/parseFloat(s.preferedConversionFactor);
      //                   //   }
      //                   //   else{
      //                       s.itemSaleQty=parseFloat(s.itemSaleQty)/parseFloat(s.baseConversionFactor);
      //                       s.itemSaleAmount=parseFloat(s.itemSaleAmount)/parseFloat(s.baseConversionFactor);
      //                   //   }
      //                   // }
      //                 });
      //                 console.log("Billing callback");
      //                 callback1();
      //               }
      //             });
      //           });
      //         });
      //       })
      //     }, function (err) {
      //     if (err) {
      //       callback(err);
      //       //return handleError(res, err);
      //     } else {
      //       callback(result_sale);
      //     }
      //   });
    }
  });
};
function getWastage_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, callback5) {
  storeId = "store123MenuItem";
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
        //"_store.category.items._id":itemId

      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.name",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$items.qty",
          receipeDetails: "$items.receipeDetails"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        //totalSaleQty:"$totalQty",
        _id: 0,
        totalWastageQty: "$_id.qty",
        totalWastageAmt: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        receipeDetails: "$_id.receipeDetails"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      handleError(res, err);
    }
    else {
      //console.log(result_sale);
      async.eachSeries(result_sale, function (r1, callback1) {
        async.eachSeries(r1.receipeDetails, function (itm, callback2) {
          //_.forEach(r1.receipeDetails,function(itm,ii){
          getProcessedAndIntermediateReceipeDetails_Wastage(startDate, endDate, itm._id, deployment_id, tenant_id, storeId, function (val) {
            _.forEach(val, function (v, vi) {
              var i = _.findIndex(r1.receipeDetails, {_id: v.itemId});
              r1.receipeDetails[i] = v;
            });
            callback2();
          });
        }, function (err) {
          if (err) {
            return handleError(res, err);
          } else {
            callback1();
          }
        });
      }, function (err) {
        if (err) {
          return handleError(res, err);
        } else {
          callback5(result_sale);
        }
      });


      // async.eachSeries(result_sale, function (r1, callback1){
      //       //console.log(r1.itemName);
      //       r1.totalWastageQty=parseFloat(r1.totalWastageQty);
      //       getMenuIdOfItems_FromReceipe(deployment_id,tenant_id,r1.itemId,function(v3){
      //         console.log(v3);
      //         r1.menuId=v3[0].menuItemId;
      //         getMenuQtyOfItems(deployment_id,tenant_id,r1.menuId,function(val2){
      //           console.log(r1);
      //           if(val2!=undefined){
      //             r1.menuQty=parseFloat(val2[0].MenuQty);
      //           }
      //           console.log(val2);
      //             getRecipesOfBillItems(startDate,endDate,deployment_id,tenant_id,r1.menuId,r1.menuQty,r1.totalWastageQty,function(val){
      //               console.log(val);
      //               r1.items=val;
      //               //callback1();
      //               async.eachSeries(r1.items, function (r2, callback2){
      //                 getLastPriceOfItem_RawMaterial_InBase(startDate,endDate,r2.stockItemId,deployment_id,tenant_id,function(val1){
      //                    //console.log(val1);
      //                    _.forEach(val1,function(v,i){
      //                     var index1=_.findIndex(r1.items,{stockItemId:v.itemId});
      //                     //console.log(index1);
      //                     if(index1<0){
      //                       //sdata.push(v);
      //                     }
      //                     else
      //                     {
      //                       console.log(v.lastPrice);
      //                       r1.items[index1].itemSaleAmount=parseFloat(r1.items[index1].itemSaleQty)*parseFloat(v.lastPrice);
      //                     }
      //                   });
      //                   callback2();
      //                 });
      //               }, function (err) {
      //               if (err) {
      //                 return handleError(res, err);
      //               } else {
      //                 _.forEachRight(r1.items,function(s,i){
      //                       s.itemSaleQty=parseFloat(s.itemSaleQty)/parseFloat(s.baseConversionFactor);
      //                       s.itemSaleAmount=parseFloat(s.itemSaleAmount)/parseFloat(s.baseConversionFactor);
      //                       s.itemWastageQty=parseFloat(s.itemSaleQty);
      //                       s.itemWastageAmt=parseFloat(s.itemSaleAmount);
      //                 });
      //                 console.log("Billing callback");
      //                 callback1();
      //               }
      //             });
      //           });
      //         });
      //       })
      //     }, function (err) {
      //     if (err) {
      //       //callback(err);
      //       callback5([]);
      //     } else {
      //       var data=[];
      //       _.forEach(result_sale,function(r,i){
      //         var index=_.findIndex(data,{itemId:r.itemId});
      //         if(index<0){
      //           data.push(r);
      //         }
      //         else{
      //           data[index].totalWastageQty=parseFloat(data[index].totalWastageQty)+parseFloat(r.totalWastageQty);
      //           _.forEach(r.items,function(itm,ii){
      //             var itmIndex=_.findIndex(data[index].items,{stockItemId:itm.stockItemId});
      //             if(itmIndex<0){
      //               data[index].items.push(itm);
      //             }
      //             else{
      //               data[index].items[itmIndex].itemSaleQty=parseFloat(data[index].items[itmIndex].itemSaleQty)+parseFloat(itm.itemSaleQty);
      //               data[index].items[itmIndex].itemSaleAmount=parseFloat(data[index].items[itmIndex].itemSaleAmount)+parseFloat(itm.itemSaleAmount);
      //               data[index].items[itmIndex].itemWastageQty=parseFloat(data[index].items[itmIndex].itemWastageQty)+parseFloat(itm.itemWastageQty);
      //               data[index].items[itmIndex].itemWastageAmt=parseFloat(data[index].items[itmIndex].itemWastageAmt)+parseFloat(itm.itemWastageAmt);
      //             }
      //           });
      //         }
      //       });
      //       callback5(data);
      //     }
      //   });
    }
  });
};

function getPhysical_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
        //"_store.category.items._id":itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$items.qty",
          receipeDetails: "$items.receipeDetails"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        //totalSaleQty:"$totalQty",
        _id: 0,
        totalBalanceQty: "$_id.qty",
        totalBalanceAmt: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        receipeDetails: "$_id.receipeDetails"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      //handleError(res, err);
    }
    else {
      //callback(result_sale);
      async.eachSeries(result_sale, function (r1, callback1) {
        async.eachSeries(r1.receipeDetails, function (itm, callback2) {
          //_.forEach(r1.receipeDetails,function(itm,ii){
          getProcessedAndIntermediateReceipeDetails_Physical(startDate, endDate, itm._id, deployment_id, tenant_id, storeId, function (val) {
            _.forEach(val, function (v, vi) {
              var i = _.findIndex(r1.receipeDetails, {_id: v.itemId});
              r1.receipeDetails[i] = v;
            });
            callback2();
          });
        }, function (err) {
          if (err) {
            return handleError(res, err);
          } else {
            callback1();
          }
        });
      }, function (err) {
        if (err) {
          return handleError(res, err);
        } else {
          callback(result_sale);
        }
      });

      // async.eachSeries(result_sale, function (r1, callback1){
      //       //console.log(r1.itemName);
      //       r1.totalBalanceQty=parseFloat(r1.totalBalanceQty);
      //       getMenuIdOfItems_FromReceipe(deployment_id,tenant_id,r1.itemId,function(v3){
      //         console.log(v3);
      //         r1.menuId=v3[0].menuItemId;
      //         getMenuQtyOfItems(deployment_id,tenant_id,r1.menuId,function(val2){
      //           console.log(r1);
      //           if(val2!=undefined){
      //             r1.menuQty=parseFloat(val2[0].MenuQty);
      //           }
      //           console.log(val2);
      //             getRecipesOfBillItems(startDate,endDate,deployment_id,tenant_id,r1.menuId,r1.menuQty,r1.totalBalanceQty,function(val){
      //               console.log(val);
      //               r1.items=val;
      //               //callback1();
      //               async.eachSeries(r1.items, function (r2, callback2){
      //                 getLastPriceOfItem_RawMaterial_InBase(startDate,endDate,r2.stockItemId,deployment_id,tenant_id,function(val1){
      //                    //console.log(val1);
      //                    _.forEach(val1,function(v,i){
      //                     var index1=_.findIndex(r1.items,{stockItemId:v.itemId});
      //                     //console.log(index1);
      //                     if(index1<0){
      //                       //sdata.push(v);
      //                     }
      //                     else
      //                     {
      //                       r1.items[index1].itemSaleAmount=parseFloat(r1.items[index1].itemSaleQty)*parseFloat(v.lastPrice);
      //                     }
      //                   });
      //                   callback2();
      //                 });
      //               }, function (err) {
      //               if (err) {
      //                 return handleError(res, err);
      //               } else {
      //                 _.forEachRight(r1.items,function(s,i){
      //                       s.itemSaleQty=parseFloat(s.itemSaleQty)/parseFloat(s.baseConversionFactor);
      //                       s.itemSaleAmount=parseFloat(s.itemSaleAmount)/parseFloat(s.baseConversionFactor);
      //                       s.itemBalanceQty=parseFloat(s.itemSaleQty);
      //                       s.itemBalanceAmt=parseFloat(s.itemSaleAmount);
      //                 });
      //                 console.log("Billing callback");
      //                 callback1();
      //               }
      //             });
      //           });
      //         });
      //       })
      //     }, function (err) {
      //     if (err) {
      //       //callback(err);
      //       callback([]);
      //     } else {
      //       var data=[];
      //       _.forEach(result_sale,function(r,i){
      //         var index=_.findIndex(data,{itemId:r.itemId});
      //         if(index<0){
      //           data.push(r);
      //         }
      //         else{
      //           data[index].totalTransferQty=parseFloat(data[index].totalTransferQty)+parseFloat(r.totalTransferQty);
      //           _.forEach(r.items,function(itm,ii){
      //             var itmIndex=_.findIndex(data[index].items,{stockItemId:itm.stockItemId});
      //             if(itmIndex<0){
      //               data[index].items.push(itm);
      //             }
      //             else{
      //               data[index].items[itmIndex].itemSaleQty=parseFloat(data[index].items[itmIndex].itemSaleQty)+parseFloat(itm.itemSaleQty);
      //               data[index].items[itmIndex].itemSaleAmount=parseFloat(data[index].items[itmIndex].itemSaleAmount)+parseFloat(itm.itemSaleAmount);
      //               data[index].items[itmIndex].itemBalanceQty=parseFloat(data[index].items[itmIndex].itemBalanceQty)+parseFloat(itm.itemBalanceQty);
      //               data[index].items[itmIndex].itemBalanceAmt=parseFloat(data[index].items[itmIndex].itemBalanceAmt)+parseFloat(itm.itemBalanceAmt);
      //             }
      //           });
      //         }
      //       });
      //       callback(data);
      //     }
      //   });
    }
  });
};

function getTransfer_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
        //"_store.category.items._id":itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$items.qty",
          receipeDetails: "$items.receipeDetails"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        //totalSaleQty:"$totalQty",
        _id: 0,
        totalTransferQty: "$_id.qty",
        totalTransferAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        receipeDetails: "$_id.receipeDetails"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      handleError(res, err);
    }
    else {
      //callback(result_sale);
      async.eachSeries(result_sale, function (r1, callback1) {
        async.eachSeries(r1.receipeDetails, function (itm, callback2) {
          //_.forEach(r1.receipeDetails,function(itm,ii){
          getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, itm._id, deployment_id, tenant_id, storeId, function (val) {

            _.forEach(val, function (v, vi) {
              var i = _.findIndex(r1.receipeDetails, {_id: v.itemId});
              r1.receipeDetails[i] = v;
            });
            callback2();
          });
        }, function (err) {
          if (err) {
            return handleError(res, err);
          } else {
            callback1();
          }
        });
      }, function (err) {
        if (err) {
          return handleError(res, err);
        } else {

          callback(result_sale);
        }
      });

      // async.eachSeries(result_sale, function (r1, callback1){
      //       //console.log(r1.itemName);
      //       r1.totalTransferQty=parseFloat(r1.totalTransferQty);
      //       getMenuIdOfItems_FromReceipe(deployment_id,tenant_id,r1.itemId,function(v3){
      //         console.log(v3);
      //         r1.menuId=v3[0].menuItemId;
      //         getMenuQtyOfItems(deployment_id,tenant_id,r1.menuId,function(val2){
      //           console.log(r1);
      //           if(val2!=undefined){
      //             r1.menuQty=parseFloat(val2[0].MenuQty);
      //           }
      //           console.log(val2);
      //             getRecipesOfBillItems(startDate,endDate,deployment_id,tenant_id,r1.menuId,r1.menuQty,r1.totalTransferQty,function(val){
      //               console.log(val);
      //               r1.items=val;
      //               //callback1();
      //               async.eachSeries(r1.items, function (r2, callback2){
      //                 getLastPriceOfItem_RawMaterial_InBase(startDate,endDate,r2.stockItemId,deployment_id,tenant_id,function(val1){
      //                    //console.log(val1);
      //                    _.forEach(val1,function(v,i){
      //                     var index1=_.findIndex(r1.items,{stockItemId:v.itemId});
      //                     //console.log(index1);
      //                     if(index1<0){
      //                       //sdata.push(v);
      //                     }
      //                     else
      //                     {
      //                       r1.items[index1].itemSaleAmount=parseFloat(r1.items[index1].itemSaleQty)*parseFloat(v.lastPrice);
      //                     }
      //                   });
      //                   callback2();
      //                 });
      //               }, function (err) {
      //               if (err) {
      //                 return handleError(res, err);
      //               } else {
      //                 _.forEachRight(r1.items,function(s,i){
      //                       s.itemSaleQty=parseFloat(s.itemSaleQty)/parseFloat(s.baseConversionFactor);
      //                       s.itemSaleAmount=parseFloat(s.itemSaleAmount)/parseFloat(s.baseConversionFactor);
      //                       s.itemTransferQty=parseFloat(s.itemSaleQty);
      //                       s.itemTransferAmount=parseFloat(s.itemSaleAmount);
      //                 });
      //                 console.log("Billing callback");
      //                 callback1();
      //               }
      //             });
      //           });
      //         });
      //       })
      //     }, function (err) {
      //     if (err) {
      //       //callback(err);
      //       callback([]);
      //     } else {
      //       var data=[];
      //       _.forEach(result_sale,function(r,i){
      //         var index=_.findIndex(data,{itemId:r.itemId});
      //         if(index<0){
      //           data.push(r);
      //         }
      //         else{
      //           data[index].totalTransferQty=parseFloat(data[index].totalTransferQty)+parseFloat(r.totalTransferQty);
      //           _.forEach(r.items,function(itm,ii){
      //             var itmIndex=_.findIndex(data[index].items,{stockItemId:itm.stockItemId});
      //             if(itmIndex<0){
      //               data[index].items.push(itm);
      //             }
      //             else{
      //               data[index].items[itmIndex].itemSaleQty=parseFloat(data[index].items[itmIndex].itemSaleQty)+parseFloat(itm.itemSaleQty);
      //               data[index].items[itmIndex].itemSaleAmount=parseFloat(data[index].items[itmIndex].itemSaleAmount)+parseFloat(itm.itemSaleAmount);
      //               data[index].items[itmIndex].itemTransferQty=parseFloat(data[index].items[itmIndex].itemTransferQty)+parseFloat(itm.itemTransferQty);
      //               data[index].items[itmIndex].itemTransferAmount=parseFloat(data[index].items[itmIndex].itemTransferAmount)+parseFloat(itm.itemTransferAmount);
      //             }
      //           });
      //         }
      //       });
      //       callback(data);
      //     }
      //   });
    }
  });
};

exports.getConsumptionSummary_ItemReport = function (req, res) {

  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReports/getConsumptionSummary_ItemReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var ss = null;
    var startDate = new Date(req.query.fromDate);
    startDate.setHours(0, 0, 0, 0);
    //console.log("Start Date"+ new Date(startDate));
    var endDate = new Date(req.query.toDate);
    endDate.setDate(endDate.getDate() + 1);
    var data = [];
    var items = JSON.parse(req.query.items);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
      //console.log("isKitchen="+isKitchen);
    }
    checkOpeningStock(startDate, deployment_id, tenant_id, function (os) {
      if (os != null) {
        var opendate = new Date(os.created);
        opendate.setHours(0, 0, 0, 0);

        getStockSale_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, function (val) {
          _.forEach(val, function (itm, i) {
            var index = _.findIndex(data, {itemId: itm.itemId});
            if (index < 0) {
              data.push(itm);
            }
            else {
              _.forEach(itm.receipeDetails, function (ritm, ii) {
                var iIndex = _.findIndex(data[index].receipeDetails, {itemId: ritm.itemId});
                if (iIndex < 0) {
                  data[index].receipeDetails.push(ritm);
                }
                else {
                  data[index].receipeDetails[iIndex].totalSaleQty = parseFloat(data[index].receipeDetails[iIndex].totalSaleQty) + parseFloat(ritm.totalSaleQty);
                  data[index].receipeDetails[iIndex].totalSaleAmount = parseFloat(data[index].receipeDetails[iIndex].totalSaleAmount) + parseFloat(ritm.totalSaleAmount);
                }
              });
            }
          });
          getTransfer_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, function (val) {
            _.forEach(val, function (itm, i) {
              var index = _.findIndex(data, {itemId: itm.itemId});
              if (index < 0) {
                data.push(itm);
              }
              else {
                _.forEach(itm.receipeDetails, function (ritm, ii) {
                  var iIndex = _.findIndex(data[index].receipeDetails, {itemId: ritm.itemId});
                  if (iIndex < 0) {
                    data[index].receipeDetails.push(ritm);
                  }
                  else {
                    if (data[index].receipeDetails[iIndex].totalTransferQty == undefined) {
                      data[index].receipeDetails[iIndex].totalTransferQty = 0;
                      data[index].receipeDetails[iIndex].totalTransferAmount = 0;
                    }
                    data[index].receipeDetails[iIndex].totalTransferQty = parseFloat(data[index].receipeDetails[iIndex].totalTransferQty) + parseFloat(ritm.totalTransferQty);
                    data[index].receipeDetails[iIndex].totalTransferAmount = parseFloat(data[index].receipeDetails[iIndex].totalTransferAmount) + parseFloat(ritm.totalTransferAmount);
                  }
                });
              }
            });
            getPhysical_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, function (val) {
              _.forEach(val, function (itm, i) {
                var index = _.findIndex(data, {itemId: itm.itemId});
                if (index < 0) {
                  data.push(itm);
                }
                else {
                  _.forEach(itm.receipeDetails, function (ritm, ii) {
                    var iIndex = _.findIndex(data[index].receipeDetails, {itemId: ritm.itemId});
                    if (iIndex < 0) {
                      data[index].receipeDetails.push(ritm);
                    }
                    else {
                      if (data[index].receipeDetails[iIndex].totalBalanceQty == undefined) {
                        data[index].receipeDetails[iIndex].totalBalanceQty = 0;
                        data[index].receipeDetails[iIndex].totalBalanceAmt = 0;
                      }
                      data[index].receipeDetails[iIndex].totalBalanceQty = parseFloat(data[index].receipeDetails[iIndex].totalBalanceQty) + parseFloat(ritm.totalBalanceQty);
                      data[index].receipeDetails[iIndex].totalBalanceAmt = parseFloat(data[index].receipeDetails[iIndex].totalBalanceAmt) + parseFloat(ritm.totalBalanceAmt);
                    }
                  });
                }
              });
              getWastage_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId, function (val) {
                _.forEach(val, function (itm, i) {
                  var index = _.findIndex(data, {itemId: itm.itemId});
                  if (index < 0) {
                    data.push(itm);
                  }
                  else {
                    _.forEach(itm.receipeDetails, function (ritm, ii) {
                      var iIndex = _.findIndex(data[index].receipeDetails, {itemId: ritm.itemId});
                      if (iIndex < 0) {
                        data[index].receipeDetails.push(ritm);
                      }
                      else {
                        if (data[index].receipeDetails[iIndex].totalWastageQty == undefined) {
                          data[index].receipeDetails[iIndex].totalWastageQty = 0;
                          data[index].receipeDetails[iIndex].totalWastageAmt = 0;
                        }
                        data[index].receipeDetails[iIndex].totalWastageQty = parseFloat(data[index].receipeDetails[iIndex].totalWastageQty) + parseFloat(ritm.totalWastageQty);
                        data[index].receipeDetails[iIndex].totalWastageAmt = parseFloat(data[index].receipeDetails[iIndex].totalWastageAmt) + parseFloat(ritm.totalWastageAmt);
                      }
                    });
                  }
                });
                return res.json(200, data);
              });
            });
          });
        });
      }
      else {
        return res.json(200, data);
      }
    });
  }
};
function getLatestTransaction(deployment_id, tenant_id, callback) {
  var data = [];
  getLatestTransaction_entry(deployment_id, tenant_id, function (entry) {
    _.forEach(entry, function (e, i) {
      data.push(e);
    });
    getLatestTransaction_sale(deployment_id, tenant_id, function (sale) {
      _.forEach(sale, function (e, i) {
        data.push(e);
      });
      getLatestTransaction_Physical(deployment_id, tenant_id, function (physical) {
        _.forEach(physical, function (e, i) {
          data.push(e);
        });
        getLatestTransaction_wastage(deployment_id, tenant_id, function (Wastage) {
          _.forEach(Wastage, function (e, i) {
            data.push(e);
          });
          getLatestTransaction_transfer(deployment_id, tenant_id, function (transfer) {
            _.forEach(transfer, function (e, i) {
              data.push(e);
            });
            getLatestTransaction_Opening(deployment_id, tenant_id, function (opening) {
              _.forEach(opening, function (e, i) {
                data.push(e);
              });
              callback(data);
            });
          });
        });
      });
    });
  });
};

//   myModel.find(filter, {sort: {created_at: -1}, limit: 10}, function(err, items){});
function getLatestTransaction_entry(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "1"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_sale(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "2"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_Physical(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "3"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_wastage(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "4"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_transfer(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "5"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_Opening(deployment_id, tenant_id, callback) {
  Stock.StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "6"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};

function getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.receiver.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        //"units.type":"baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQuantity"
        },
        totalQty: {$sum: {$multiply: ["$items.quantity", "$menuItems.qty", "$items.selectedUnitId.conversionFactor"]}},
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.totalQty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      handleError(res, err);
    }
    else {
      //console.log("recipe :" + result_sale);
      var data = [];
      _.forEach(result_sale, function (r, i) {
        var index = _.findIndex(data, {itemId: r.itemId});
        if (index < 0) {
          data.push(r);
        }
        else {
          data[index].totalSaleQty = parseFloat(data[index].totalSaleQty) + parseFloat(r.totalSaleQty);
          data[index].totalSaleAmount = parseFloat(data[index].totalSaleAmount) + parseFloat(r.totalSaleAmount);
        }
      });


      async.eachSeries(data, function (r2, callback2) {
        getLastPriceOfItem_RawMaterial_InBase(startDate, endDate, r2.itemId, deployment_id, tenant_id, function (val1) {
          _.forEach(val1, function (v, i) {
            var index1 = _.findIndex(data, {itemId: v.itemId});
            if (index1 < 0) {
            }
            else {
              //console.log(v.lastPrice);
              data[index1].totalSaleAmount = parseFloat(data[index1].totalSaleQty) * parseFloat(v.lastPrice);
            }
          });
          callback2();
        });
      }, function (err) {
        if (err) {
          return handleError(res, err);
        } else {
          callback(data);
        }
      });
    }
  });
};

function getStockSaleByItem_ConsumptionReport_MenuItem(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
        //"_store.receiver.category.items._id":itemId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$items.qty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        //totalSaleQty:"$totalQty",
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      handleError(res, err);
    }
    else {
      //callback(result_sale);
      async.eachSeries(result_sale, function (r1, callback1) {
        //console.log(r1.itemName);
        r1.totalSaleQty = parseFloat(r1.totalSaleQty);
        getMenuIdOfItems_FromReceipe(deployment_id, tenant_id, r1.itemId, function (v3) {
          //console.log(v3);
          r1.menuId = v3[0].menuItemId;
          getMenuQtyOfItems(deployment_id, tenant_id, r1.menuId, function (val2) {
            //console.log(r1);
            if (val2 != undefined) {
              r1.menuQty = parseFloat(val2[0].MenuQty);
            }
            //console.log(val2);
            getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id, r1.menuId, r1.menuQty, r1.totalSaleQty, function (val) {
              //console.log(val);
              r1.items = val;
              //callback1();
              async.eachSeries(r1.items, function (r2, callback2) {
                getLastPriceOfItem_RawMaterial_InBase(startDate, endDate, r2.stockItemId, deployment_id, tenant_id, function (val1) {
                  //console.log(val1);
                  _.forEach(val1, function (v, i) {
                    var index1 = _.findIndex(r1.items, {stockItemId: v.itemId});
                    //console.log(index1);
                    if (index1 < 0) {
                      //sdata.push(v);
                    }
                    else {
                      r1.items[index1].itemSaleAmount = parseFloat(r1.items[index1].itemSaleQty) * parseFloat(v.lastPrice);
                    }
                  });
                  callback2();
                });
              }, function (err) {
                if (err) {
                  return handleError(res, err);
                } else {
                  _.forEachRight(r1.items, function (s, i) {
                    // if(s.stockItemId==itemId){
                    //   if(s.preferedConversionFactor!=undefined){
                    //     s.itemSaleQty=parseFloat(s.itemSaleQty)/parseFloat(s.preferedConversionFactor);
                    //     s.itemSaleAmount=parseFloat(s.itemSaleAmount)/parseFloat(s.preferedConversionFactor);
                    //   }
                    //   else{
                    s.itemSaleQty = parseFloat(s.itemSaleQty) / parseFloat(s.baseConversionFactor);
                    s.itemSaleAmount = parseFloat(s.itemSaleAmount) / parseFloat(s.baseConversionFactor);
                    //   }
                    // }
                  });
                  //console.log("Billing callback");
                  callback1();
                }
              });
            });
          });
        })
      }, function (err) {
        if (err) {
          callback(err);
          //return handleError(res, err);
        } else {
          callback(result_sale);
        }
      });
    }
  });
};

function getProcessedAndIntermediateReceipeDetails_Physical(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.category.items.receipeDetails._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuTtems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalBalanceQty:"$totalQty",
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_op) {
    if (err) {
      //handleError(res, err);
      //console.log(err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          if (result_op[0] != null) {
            if (result_op[0].totalBalanceAmt == undefined) {
              result_op[0].totalBalanceAmt = 0;
            }
            result_op[0].totalBalanceAmt = (parseFloat(result_op[0].totalBalanceQty) * parseFloat(val[0].lastPrice));
          }
        }
        callback(result_op);
      });
    }
  });
};

function getProcessedAndIntermediateReceipeDetails_Wastage(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  storeId = "store123MenuItem";
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.category.items.receipeDetails._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuTtems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      handleError(res, err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalWastageAmt == undefined) {
              rs.totalWastageAmt = 0;
            }
            ;
            rs.totalWastageAmt = parseFloat(rs.totalWastageQty) * parseFloat(val[0].lastPrice);
          });
        }
        else {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalWastageAmt == undefined) {
              rs.totalWastageAmt = 0;
            }
            ;
          });
        }
        callback(result_ws);
      });
    }
  });
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.category.items.receipeDetails._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuTtems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      //console.log(err);
      handleError(res, err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount == undefined) {
              rs.totalTransferAmount = 0;
            }
            ;
            rs.totalTransferAmount = parseFloat(rs.totalTransferQty) * parseFloat(val[0].lastPrice);
          });
        }
        else {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount == undefined) {
              rs.totalTransferAmount = 0;
            }
            ;
          });
        }
        //console.log(result_ws);
        callback(result_ws);
      });
    }
  });
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId,
        "_store.category.items.receipeDetails._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuTtems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      //console.log(err);
      handleError(res, err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount_toStore == undefined) {
              rs.totalTransferAmount_toStore = 0;
            }
            ;
            rs.totalTransferAmount_toStore = parseFloat(rs.totalTransferQty_toStore) * parseFloat(val[0].lastPrice);
          });
        }
        else {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount_toStore == undefined) {
              rs.totalTransferAmount_toStore = 0;
            }
            ;
          });
        }
        callback(result_ws);
      });
    }
  });
};

function getRecipesOfBillItems_MenuItems(startDate, endDate, deployment_id, tenant_id, menuItemId, menuQty, billQty, callback) {
  //console.log(menuQty, billQty);
  if (menuQty == undefined || menuQty == null) {
    menuQty = 0;
  }
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        itemId: menuItemId
      }
    },
    {
      $project: {
        _id: 0,
        receipeDetails: "$receipeDetails",
        //receipeQtyinBase:{$multiply:["$quantity","$selectedUnitId.conversionFactor"]}
        receipeQtyinBase: "$selectedUnitId.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        receipeDetails: "$receipeDetails",
        receipeQtyinBase: "$receipeQtyinBase",
        Qty: {$divide: [menuQty, "$receipeQtyinBase"]}
      }
    },
    {
      $unwind: "$receipeDetails"
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$receipeDetails.itemName",
        stockItemId: "$receipeDetails._id",
        itemQtyInBase: {$multiply: ["$receipeDetails.quantity", "$receipeDetails.selectedUnitId.conversionFactor"]},
        baseUnit: "$receipeDetails.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$receipeDetails.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$receipeDetails.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        stockItemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        baseUnit: "$baseUnit",
        itemSaleQty: {$multiply: ["$itemQtyInBase", "$Qty", billQty]},
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      //console.log(result_entry);
      callback(result_entry);
    }
  });
};


function getMenuQtyOfItems(deployment_id, tenant_id, menuItemId, callback) {
  Stock.Item.aggregate([
    {
      $match: {
        _id: new ObjectId(menuItemId),
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id)
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id",
        itemName: "$name",
        MenuQty: {$multiply: ["$stockQuantity", "$unit.conversionFactor"]}
      }
    }
  ], function (err, itm) {
    if (err) {
      callback(err);
    }
    else {
      callback(itm);
    }
  });
};

function getMenuIdOfItems_FromReceipe(deployment_id, tenant_id, recipeId, callback) {
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        _id: new ObjectId(recipeId)
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id",
        itemName: "$recipeName",
        menuItemId: "$itemId"
      }
    }
  ], function (err, itm) {
    if (err) {
      callback(err);
    }
    else {
      callback(itm);
    }
  });
};
function BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate, itemId, callback) {
  // console.log("endDate:"+new Date(endDate));
  // console.log("estartDate:"+new Date(startDate));

  Stock.Bill.aggregate([
    {
      $match: {
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        _created: {
          $gte: new Date(startDate), $lte: new Date(endDate)
        }
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $unwind: "$_kots.items"
    },
    {
      $project: {
        kot: "$_kots",
        items: "$_kots.items"
      }
    },
    // {
    //   $match:{
    //     "items.stockQuantity":{$and:[{"$ne":""},{"$ne":null}]}
    //     //$and:[{"items.unit.conversionFactor":{"$ne":""}},{"items.unit.conversionFactor":{"$ne":null}}]
    //   }
    // },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          itemName: "$items.name",
          qty: "$items.stockQuantity",
          conF: "$items.unit.conversionFactor"
        },
        //totalSaleQtyInBase:{$sum:{$multiply:["$items.stockQuantity","$items.unit.conversionFactor"]} },
        billQty: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id.itemId",
        itemName: "$_id.itemName",
        //totalSaleQty:"$totalSaleQtyInBase",
        totalSaleQty: {$multiply: ["$_id.qty", "$_id.conF"]},
        billQty: "$billQty"
      }
    }
    // {
    //   $match:{
    //     itemId:itemId
    //   }
    // }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      var data = [];
      _.forEach(result_entry, function (r, i) {
        var index = _.findIndex(data, {itemId: r.itemId});
        if (index < 0) {
          //console.log("Sale qty: "+ parseFloat(r.totalSaleQty));
          data.push(r);
        }
        else {
          data[index].totalSaleQty = parseFloat(r.totalSaleQty);//+parseFloat(data[index].totalSaleQty);
          data[index].billQty = parseFloat(r.billQty) + parseFloat(data[index].billQty);
        }
      });

      //console.log(data);

      var sdata = [];
      async.eachSeries(data, function (r, callback1) {
        getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id, r.itemId, r.totalSaleQty, r.billQty, function (val) {
          _.forEach(val, function (v, i) {
            var index2 = _.findIndex(sdata, {stockItemId: v.stockItemId});
            if (index2 < 0) {
              sdata.push(v);
            }
            else {
              sdata[index2].itemSaleQty += parseFloat(v.itemSaleQty);
            }
          });
          callback1();
        });
      }, function (err) {
        if (err) {
          return handleError(res, err);
        } else {
          //callback(getDataBeforeStartDate_DateWise);
          //var sdata=[];
          //console.log("sdata"+ sdata);
          async.eachSeries(sdata, function (r2, callback2) {
            getLastPriceOfItem_RawMaterial_InBase(startDate, endDate, r2.stockItemId, deployment_id, tenant_id, function (val1) {
              //console.log(val1);
              _.forEach(val1, function (v, i) {
                var index1 = _.findIndex(sdata, {stockItemId: v.itemId});
                //console.log(index1);
                if (index1 < 0) {
                  //sdata.push(v);
                }
                else {
                  sdata[index1].itemSaleAmount = parseFloat(sdata[index1].itemSaleQty) * parseFloat(v.lastPrice);
                }
              });
              callback2();
            });
          }, function (err) {
            if (err) {
              return handleError(res, err);
            } else {
              var rdata = [];
              _.forEachRight(sdata, function (s, i) {
                if (s.stockItemId == itemId) {
                  //console.log(s);
                  if (s.preferedConversionFactor != undefined) {
                    s.itemSaleQty = parseFloat(s.itemSaleQty) / parseFloat(s.preferedConversionFactor);
                    s.itemSaleAmount = parseFloat(s.itemSaleAmount) / parseFloat(s.preferedConversionFactor);
                  }
                  else {
                    s.itemSaleQty = parseFloat(s.itemSaleQty) / parseFloat(s.baseConversionFactor);
                    s.itemSaleAmount = parseFloat(s.itemSaleAmount) / parseFloat(s.baseConversionFactor);
                  }
                  rdata.push(s);
                }
              });
              // console.log("Billing callback");
              // console.log(rdata);
              callback(rdata);
            }
          });
        }
      });
      //callback(data);
    }
  });
};

function getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id, menuItemId, menuQty, billQty, callback) {
  //console.log(menuQty, billQty);
  if (menuQty == undefined || menuQty == null) {
    menuQty = 0;
  }
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        itemId: menuItemId
      }
    },
    {
      $project: {
        _id: 0,
        receipeDetails: "$receipeDetails",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    {
      $project: {
        _id: 0,
        receipeDetails: "$receipeDetails",
        receipeQtyinBase: "$receipeQtyinBase",
        Qty: {$divide: [menuQty, "$receipeQtyinBase"]}
      }
    },
    {
      $unwind: "$receipeDetails"
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$receipeDetails.itemName",
        stockItemId: "$receipeDetails._id",
        itemQtyInBase: {$multiply: ["$receipeDetails.quantity", "$receipeDetails.selectedUnitId.conversionFactor"]},
        baseUnit: "$receipeDetails.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$receipeDetails.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$receipeDetails.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        stockItemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        baseUnit: "$baseUnit",
        //itemSaleQty:{$multiply:[{$divide:["$itemQtyInBase","$Qty"]},billQty]},
        itemSaleQty: {$multiply: ["$itemQtyInBase", "$Qty", billQty]},
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      //console.log(result_entry);
      callback(result_entry);
    }
  });
};

function getLastPriceOfItem_RawMaterial_InBase(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  //console.log(startDate,endDate);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          basePrice: "$units.basePrice",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        lastPrice: {$last: "$items.price"}
      }
    },
    {
      $project: {
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        //lastPrice:{$divide:["$lastPrice","$_id.conversionFactor"]}, //,"$lastPrice",
        //lastPrice:"$lastPrice",
        lastPrice: "$_id.basePrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
        //created:"$_id.created"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      //handleError(res, err);
      callback(err);
    }
    else {
      if (result_entry[0] == undefined) {
        result_entry = [];
        var obj = {itemId: itemId, lastPrice: 0};
        result_entry.push(obj);
      }
      else {
        var lp = parseFloat(result_entry[0].lastPrice) / (result_entry[0].conversionFactor);
        result_entry[0].lastPrice = lp;
      }
      callback(result_entry);
    }
  });
};
exports.getRequirementData = function (req, res) {
  Stock.StockRequirement.find(function (err, reqss) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, reqss);
  });
};

function checkOpeningStock(beforeDate, deployment_id, tenant_id, callback) {
  var startDate = new Date(beforeDate);
  startDate.setDate(startDate.getDate() + 1);
  Stock.StockTransaction.findOne({
    transactionType: "6",
    tenant_id: new ObjectId(tenant_id),
    deployment_id: new ObjectId(deployment_id),
    created: {$lte: new Date(startDate)}
  }, {}, {sort: {'created': -1}}, function (err, opt) {
    callback(opt);
  });
};

// Get a Wastage Report
exports.getWastageReport = function (req, res) {
  var startDate = new Date(req.query.fromDate);
  //startDate.setHours(0, 0, 0, 0);
  startDate = new Date(startDate);
  var endDate = new Date(req.query.toDate);
  var itemType = req.query.itemType;
  endDate.setDate(endDate.getDate() + 1);
  endDate = new Date(endDate);
  //endDate.setHours(0, 0, 0, 0);
  //console.log(itemType);
  // var items=JSON.parse(req.query.items);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  if (itemType == undefined) {
    getWastageReports_DayWise(startDate, endDate, deployment_id, tenant_id, function (resWastage) {
      return res.json(resWastage);
    });
  }
  else {
    getWastageReports(startDate, endDate, itemType, deployment_id, tenant_id, function (resWastage) {
      return res.json(resWastage);
    });
  }
};
function getWastageReports_DayWise(startDate, endDate, deployment_id, tenant_id, callback) {
  //console.log(startDate,endDate);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        transactionNumber:"$transactionNumber",
        units: "$_store.category.items.calculateInUnits",
        wastageEntryType: "$wastageEntryType"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          menuName: "$items.name",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          transactionNumber:"$transactionNumber",
          created: "$created",
          itemType: "$items.itemType",
          units: "$items.units",
          wastageEntryType: "$wastageEntryType"
        },
        //totalQty:{$sum:"$items.qty"}
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        billNo: "$_id.billNo",
       transactionNumber:"$_id.transactionNumber",
        created: "$_id.created",
        totalQty: "$totalQty",
        menuName: "$_id.menuName",
        UnitName: "$_id.UnitName",
        itemType: "$_id.itemType",
        units: "$_id.units",
        wastageEntryType: "$_id.wastageEntryType"
      }
    }

  ], function (err, result_entry) {
    var data = [];
    if (err) {
      //handleError(res, err);
      callback(err);
    }
    else {
      console.log("wastage")
      console.log(result_entry);
      _.forEach(result_entry, function (r, i) {
        if (r.itemName == undefined) {
          r.itemName = r.menuName;
        }
        var index = _.findIndex(data, {itemId: r.itemId});
        if (index < 0) {
          //var d=new Date(r.created);
          r.totalQty = parseFloat(r.totalQty).toFixed(3);
          //r.created.setHours(0, 0, 0, 0);
          data.push(r);
        }
        else {
          var d = new Date(r.created);
          //d.setHours(0, 0, 0, 0);
          if (new Date(data[index].created).getTime() == new Date(d).getTime()) {
            data[index].totalQty = parseFloat(parseFloat(data[index].totalQty) + parseFloat(r.totalQty)).toFixed(3);
          }
          else {
            r.totalQty = parseFloat(r.totalQty).toFixed(3);
            //r.created.setHours(0, 0, 0, 0);
            data.push(r);
          }
        }
      });
     //console.log('data',data.length);
      //console.log('data value',data);
      Stock.StockTransaction.aggregate([
        {
          $match: {
            transactionType: "15",
            tenant_id: new ObjectId(tenant_id),
            deployment_id: new ObjectId(deployment_id),
            created: {$gte: new Date(startDate), $lt: new Date(endDate)},
          }
        },
        {
          $unwind: "$_store.category"
        },
        {
          $unwind: "$_store.category.items"
        },
        {
          $unwind: "$_store.category.items.calculateInUnits"
        },
        {
          $project: {
            items: "$_store.category.items",
            created: "$created",
            billNo: "$daySerialNumber",
            transactionNumber:"$transactionNumber",
            units: "$_store.category.items.calculateInUnits"
          }
        },
        {
          $match: {
            "units.type": "baseUnit"
          }       },
        {
          $group: {
            _id: {
              itemId: "$items.recipeId",
              name: "$items.itemName",
              date: "$created",
              billNo: "$billNo",
              transactionNumber:"$transactionNumber",
              units: "$items.units",
              unitName: "$units.unitName",
              returnWastageQuantity:"$items.returnWastageQty",
              returnAcceptQuantity:"$items.returnAcceptQty",
              conversionFactor: "$units.conversionFactor"
            },
            totalQty: {$sum: "$units.baseWastageQty"}
          }
        },
        {
          $project: {
            _id: 0,
            totalQty: "$totalQty",
            itemName: "$_id.name",
            itemId: "$_id.itemId",
            UnitName: "$_id.unitName",
            units: "$_id.units",
            ConversionFactor: "$_id.conversionFactor",
            created: "$_id.date",
            billNo: "$_id.billNo",
            transactionNumber:"$_id.transactionNumber",
            returnWastageQuantity:"$_id.returnWastageQuantity",
            returnAcceptQuantity:"$_id.returnAcceptQuantity",
            itemType: {$literal: "MenuItem"}
          }
        }
      ], function (err, result) {
        //console.log('result', result_entry);
        console.log('result.length',result.length);
        _.forEach(result, function (r, i) {
          console.log('r',r);
          if (r.itemName == undefined) {
            r.itemName = r.menuName;
          }
          //console.log(data);
          var index = _.findIndex(data, {itemId: r.itemId});
          if (index < 0) {
            //var d=new Date(r.created);
            r.totalQty = parseFloat(r.totalQty).toFixed(3);
            //r.created.setHours(0, 0, 0, 0);
            data.push(r);
          }
          else {
            var d = new Date(r.created)
            //d.setHours(0, 0, 0, 0);
            if (new Date(data[index].created).getTime() == new Date(d).getTime()) {
              data[index].totalQty = parseFloat(parseFloat(data[index].totalQty) + parseFloat(r.totalQty)).toFixed(3);
              data.push(r);
            }
            else {
              r.totalQty = parseFloat(r.totalQty).toFixed(3);
              //r.created.setHours(0, 0, 0, 0);
              data.push(r);
            }
          }
        });
        //console.log(data.length);
        callback(data);
      })
    }
  });
};
function getWastageReports(startDate, endDate, itemType, deployment_id, tenant_id, callback) {
  //console.log(startDate,endDate);
  //console.log(startDate);
  //console.log(endDate);
  var iType = "";
  if(itemType == "RawMaterial")
    iType = [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}];
  else
    iType = [{"items.itemType": itemType}];
  console.log(itemType);
  var data = [];
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        //created:{$lt : new Date(endDate)}
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
        //"_store.vendor.category.items._id":itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        wastageEntryType:"$wastageEntryType",
        transactionNumber:"$transactionNumber",
       billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        $or: iType,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          itName: "$items.name",
          qty: "$units.baseQty",
          UnitName: "$units.unitName",
         billNo: "$billNo",
          transactionNumber:"$transactionNumber",
          wastageEntryType:"$wastageEntryType",
          created: "$created"
        },
        //totalQty:{$sum:"$items.qty"}
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
       billNo: "$_id.billNo",
        transactionNumber:"$_id.transactionNumber",
        created: "$_id.created",
        menuName: "$_id.itName",
        wastageEntryType:"$_id.wastageEntryType",
                //totalQty:"$totalQty",
        totalQty: "$_id.qty",
        UnitName: "$_id.UnitName"
        //created:"$_id.created"
      }
    }

  ], function (err, result_entry) {
    if (err) {
      //handleError(res, err);
      callback(err);
    }
    else {
      _.forEach(result_entry, function (r, i) {
        if (r.itemName == undefined) {
          r.itemName = r.menuName;
        }
        data.push(r);
      });
      //console.log(data.length);
      if (itemType == "MenuItem") {
        Stock.StockTransaction.aggregate([
          {
            $match: {
              transactionType: "15",
              tenant_id: new ObjectId(tenant_id),
              deployment_id: new ObjectId(deployment_id),
              created: {$gte: new Date(startDate), $lt: new Date(endDate)},
            }
          },
          {
            $unwind: "$_store.category"
          },
          {
            $unwind: "$_store.category.items"
          },
          {
            $unwind: "$_store.category.items.calculateInUnits"
          },
          {
            $project: {
              items: "$_store.category.items",
              created: "$created",
              billNo: "$daySerialNumber",
              transactionNumber:"$transactionNumber",
              units: "$_store.category.items.calculateInUnits"
            }
          },
          {
            $match: {
              "units.type": "baseUnit"
            }
          },
          {
            $group: {
              _id: {
                itemId: "$items.recipeId",
                name: "$items.itemName",
                date: "$created",
                returnWastageQuantity:"$items.returnWastageQty",
                returnAcceptQuantity:"$items.returnAcceptQty",
                transactionNumber:"$transactionNumber",
                billNo: "$billNo",
                unitName: "$units.unitName",
                conversionFactor: "$units.conversionFactor"
              },
              totalQty: {$sum: "$units.baseWastageQty"}
            }
          },
          {
            $project: {
              _id: 0,
              totalQty: "$totalQty",
              returnWastage: {$literal: "true"},
              itemName: "$_id.name",
              itemId: "$_id.itemId",
              returnWastageQuantity:"$_id.returnWastageQuantity",
              returnAcceptQuantity:"$_id.returnAcceptQuantity",
              UnitName: "$_id.unitName",
              ConversionFactor: "$_id.conversionFactor",
              created: "$_id.date",
              transactionNumber:"$_id.transactionNumber",
              billNo: "$_id.billNo"
            }
          }
        ], function (err, result) {
          //console.log(result.length);
          _.forEach(result, function (res) {
            data.push(res);
          });
          callback(data);
        })
        //console.log(data.length);
      } else {
        callback(data);
      }
    }
  });
};

function getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  //console.log(startDate,endDate);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)},
        //created:{$gte: new Date(startDate), $lt : new Date(endDate)}
        "_store.vendor.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor",
          lastPrice: "$units.basePrice",
          UnitName: "$units.unitName"
        },
        //totalQty:{$sum:"$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"}
        //lastPrice: { $last: "$items.price" }
      }
    },
    {

      $project: {
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$_id.lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$_id.totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }

  ], function (err, result_entry) {
    if (err) {
      //handleError(res, err);
      callback(err);
    }
    else {

      if (result_entry[0] == undefined) {
        result_entry = [];
        var obj = {itemId: itemId, lastPrice: 0};
        result_entry.push(obj);
      }
      else {
        var lp = parseFloat(result_entry[0].lastPrice) / (result_entry[0].conversionFactor);
        result_entry[0].lastPrice = lp;
      }
      callback(result_entry);
    }
  });
};

function getOpeningStockByItem(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  var beforeDate = new Date(endDate);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(beforeDate)},
        "_store._id": storeId,
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$totalQty",
        totalOpeningQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_op) {
    if (err) {
      //handleError(res, err);
      //console.log(err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          _.forEach(result_op, function (r, i) {
            if (r.totalOpeningAmt == undefined) {
              r.totalOpeningAmt = 0;
            }
            r.totalOpeningAmt = (parseFloat(r.totalOpeningQty) * parseFloat(val[0].lastPrice));
          });
        }
        else {
          _.forEach(result_op, function (r, i) {
            if (r.totalOpeningAmt == undefined) {
              r.totalOpeningAmt = 0;
            }
          });
        }
        //console.log(result_op);
        callback(result_op);
      });
      //callback(result_op);
    }
  });
  // Stock.StockTransaction.findOne({transactionType:"6",created:{$lte: new Date(beforeDate)},"_store.category.items._id":itemId},{},{sort:{'created':-1}}, function(err,opt){
  //   callback(opt);
  // });
};
function checkPhysicalStock(beforeDate, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.findOne({
    transactionType: "3",
    tenant_id: new ObjectId(tenant_id),
    deployment_id: new ObjectId(deployment_id),
    created: {$lte: new Date(beforeDate)}
  }, {}, {sort: {'created': -1}}, function (err, opt) {
    ///console.log(opt);
    callback(opt);
  });
};
function getPhysicalStockByItem(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalBalanceQty:"$totalQty",
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_op) {
    if (err) {
      //handleError(res, err);
      //console.log(err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          if (result_op[0] != null) {
            if (result_op[0].totalBalanceAmt == undefined) {
              result_op[0].totalBalanceAmt = 0;
            }
            result_op[0].totalBalanceAmt = (parseFloat(result_op[0].totalBalanceQty) * parseFloat(val[0].lastPrice));
          }
        }
        // console.log(new Date(startDate));
        // console.log(new Date(endDate));
        //console.log(result_op);
        callback(result_op);
      });
    }
  });
};
function checkPhysicalStockBetweenTwoDates(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.findOne({
    transactionType: "3",
    tenant_id: new ObjectId(tenant_id),
    deployment_id: new ObjectId(deployment_id),
    "_store.category.items._id": itemId,
    created: {$gt: new Date(startDate), $lte: new Date(endDate)}
  }, {}, {sort: {'created': -1}}, function (err, opt) {
    callback(opt);
  });
};
function checkPhysicalStockBetweenOpeningAndStart(openingDate, startDate, itemId, deployment_id, tenant_id, storeId, callback1) {
  //console.log("Phy Open"+ startDate);
  Stock.StockTransaction.findOne({
    transactionType: "3",
    tenant_id: new ObjectId(tenant_id),
    deployment_id: new ObjectId(deployment_id),
    "_store._id": storeId,
    "_store.category.items._id": itemId,
    created: {$gte: new Date(openingDate), $lt: new Date(startDate)}
  }, {}, {sort: {'created': -1}}, function (err, opt) {
    callback1(opt);
  });
};

function getWastageStockByItem(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      handleError(res, err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalWastageAmt == undefined) {
              rs.totalWastageAmt = 0;
            }
            ;
            rs.totalWastageAmt = parseFloat(rs.totalWastageQty) * parseFloat(val[0].lastPrice);
          });
        }
        else {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalWastageAmt == undefined) {
              rs.totalWastageAmt = 0;
            }
            ;
          });
        }
        //console.log(result_ws);
        callback(result_ws);
      });
    }
  });
};
function getTransferStockByItemFromStore(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      //console.log(err);
      handleError(res, err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount == undefined) {
              rs.totalTransferAmount = 0;
            }
            ;
            rs.totalTransferAmount = parseFloat(rs.totalTransferQty) * parseFloat(val[0].lastPrice);
          });
        }
        else {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount == undefined) {
              rs.totalTransferAmount = 0;
            }
            ;
          });
        }
        //console.log(result_ws);
        callback(result_ws);
      });
    }
  });
};

function getTransferStockByItemToStore(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId,
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      //console.log(err);
      handleError(res, err);
    }
    else {
      getLastPriceOfItem_RawMaterial(startDate, endDate, itemId, deployment_id, tenant_id, function (val) {
        if (val[0] != null) {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount_toStore == undefined) {
              rs.totalTransferAmount_toStore = 0;
            }
            ;
            rs.totalTransferAmount_toStore = parseFloat(rs.totalTransferQty_toStore) * parseFloat(val[0].lastPrice);
          });
        }
        else {
          _.forEach(result_ws, function (rs, i) {
            if (rs.totalTransferAmount_toStore == undefined) {
              rs.totalTransferAmount_toStore = 0;
            }
            ;
          });
        }
        callback(result_ws);
      });
    }
  });
};

function getOpeningStockForConsumption(startDate, endDate, deployment_id, tenant_id, callback) {
  var op = [];

  checkOpeningStock(startDate, deployment_id, tenant_id, function (oData) {
    if (oData == null) {
      //console.log("Null oData");
      //Check opening Date before end date whether stock started or not;
      // checkOpeningStock(endDate, function(oData1){
      //   callback(oData1);
      // });
    }
    else {
      callback(oData);
    }
  });
};

function getStockEntry_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $project: {
        category: "$_store.vendor.category",
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber"
      }
    },
    {
      $group: {
        _id: {_id: "$items._id", name: "$items.itemName"},
        totalAmt: {$sum: "$items.subTotal"},
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      handleError(res, err);
    }
    else {
      callback(result_entry);
    }
  });
};

function getStockEntryByItem_ConsumptionReport(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  //console.log(itemId,startDate,new Date(endDate));
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.vendor.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        //category:"$_store.vendor.category",
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        //"units.type":"preferedUnit"
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        //_id:{_id:"$_id",created:"$created",itemId:"$items._id", name: "$items.itemName"},
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
        //units:{$last:"$units"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName"
        //SelectedUnits:"$units"
        //created:"$_id.created"
      }
    }
    // {
    //   $unwind:"$SelectedUnits"
    // },
    // {
    //   $match:{
    //     "SelectedUnits.type":"preferedUnit"
    //   }
    // },
    // {
    //   $group:{
    //     _id:{itemId:"$itemId",name:"$itemName",totalQty:"$totalQty",unitName:"$SelectedUnits.unitName",conversionFactor:"$SelectedUnits.conversionFactor"},
    //     totalAmt:{$sum:"$SelectedUnits.totalAmount"}
    //   }
    // },
    // {
    //   $project:{
    //     totalPurchaseQty:"$_id.totalQty",
    //     totalPurchaseAmount:"$totalAmt",
    //     itemName:"$_id.name",
    //     itemId:"$_id.itemId",
    //     UnitName:"$_id.unitName",
    //     ConversionFactor:"$_id.conversionFactor"
    //   }
    // }

  ], function (err, result_entry) {
    if (err) {
      //handleError(res, err);
      callback(err);
    }
    else {
      //console.log(result_entry);
      callback(result_entry);
    }
  });
};

function getStockSale_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $project: {
        category: "$_store.receiver.category",
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber"
      }
    },
    {
      $group: {
        _id: {_id: "$items._id", name: "$items.itemName"},
        totalAmt: {$sum: {$multiply: ["$items.qty", "$items.price"]}},
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        totalSaleQty: "$totalQty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      handleError(res, err);
    }
    else {
      callback(result_sale);
    }
  });
};

function getStockSaleByItem_ConsumptionReport(startDate, endDate, itemId, deployment_id, tenant_id, storeId, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId,
        "_store.receiver.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        //totalAmt:{$sum:{ $multiply: [ "$items.qty", "$items.price" ] }},
        totalAmt: {$sum: "$units.totalAmount"}
        //totalQty:{$sum:"$units.baseQty"}
      }
    },
    {
      $project: {
        //totalSaleQty:"$totalQty",
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      handleError(res, err);
    }
    else {
      callback(result_sale);
    }
  });
};

function getDataBeforeStartDate(opendate, startDate, items, deployment_id, tenant_id, storeId, isKitchen, callback2) {
  //console.log("isKitchen before startDate="+isKitchen);
  var data = [];
  var itemsForLoop = items;
  async.eachSeries(itemsForLoop, function (itm, callback1) {
    checkPhysicalStockBetweenOpeningAndStart(opendate, startDate, itm.id, deployment_id, tenant_id, storeId, function (ph) {
      if (ph != null) {
        var minDate = new Date(ph.created);
        // minDate.setDate(minDate.getDate() + 1);
        // minDate.setHours(0,0,0,0);
        startDate = new Date(minDate);
        startDate.setHours(0, 0, 0, 0);

        // console.log("opendate =" + new Date(opendate));
        // console.log("ph not null="+minDate);
        // console.log("startDate =" + new Date(startDate));
        getStockEntryByItem_ConsumptionReport(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            var index = _.findIndex(data, {itemId: s.itemId});
            if (index < 0) {
              data.push(s);
            }
            else {
              if (data[index].totalPurchaseQty == undefined) {
                data[index].totalPurchaseQty = 0;
                data[index].totalPurchaseAmount = 0;
              }
              data[index].totalPurchaseQty = parseFloat(s.totalPurchaseQty) + parseFloat(data[index].totalPurchaseQty);
              data[index].totalPurchaseAmount = parseFloat(s.totalPurchaseAmount) + parseFloat(data[index].totalPurchaseAmount);
            }
          });

          getStockSaleByItem_ConsumptionReport(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              var index = _.findIndex(data, {itemId: s.itemId});
              if (index < 0) {
                data.push(s);
              }
              else {
                if (s.totalSaleQty == undefined) {
                  s.totalSaleQty = 0;
                  s.totalSaleAmount = 0;
                }
                if (data[index].totalSaleQty == undefined) {
                  data[index].totalSaleQty = 0;
                  data[index].totalSaleAmount = 0;
                }
                data[index].totalSaleQty = parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty);
                data[index].totalSaleAmount = parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount);

                //console.log(s.totalSaleQty);
                // data[index].totalSaleQty=s.totalSaleQty;
                // data[index].totalSaleAmount=s.totalSaleAmount;
              }
              //console.log(s);
            });

            // getOpeningStockByItem(minDate,startDate,itm.id,deployment_id,tenant_id,function(op_stock){
            //   //console.log(op_stock);
            //   _.forEach(op_stock, function(s,i){
            //     var index=_.findIndex(data,{itemId:s.itemId});
            //     if(index<0){
            //       data.push(s);
            //     }
            //     else
            //     {
            //       data[index].totalOpeningQty=s.totalOpeningQty;
            //       data[index].totalOpeningAmt=s.totalOpeningAmt;
            //       console.log(s.totalOpeningAmt);
            //       if(data[index].unitName==undefined){
            //         data[index].unitName=s.unitName;
            //       }
            //     }
            //   });

            getPhysicalStockByItem(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (bs_stock) {
              //console.log(bs_stock);
              _.forEach(bs_stock, function (s, i) {
                var index = _.findIndex(data, {itemId: s.itemId});
                //console.log("Hi physical")
                if (index < 0) {
                  data.push(s);
                }
                else {
                  if (data[index].totalBalanceQty == undefined) {
                    data[index].totalBalanceQty = 0;
                    data[index].totalBalanceAmt = 0;
                  }
                  data[index].totalBalanceQty = s.totalBalanceQty;
                  data[index].totalBalanceAmt = s.totalBalanceAmt;
                }
              });

              getWastageStockByItem(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (ws_stock) {
                _.forEach(ws_stock, function (s, i) {
                  var index = _.findIndex(data, {itemId: s.itemId});
                  if (index < 0) {
                    data.push(s);
                  }
                  else {
                    if (data[index].totalWastageQty == undefined) {
                      data[index].totalWastageQty = 0;
                      data[index].totalWastageAmt = 0;
                    }
                    data[index].totalWastageQty = parseFloat(s.totalWastageQty) + parseFloat(data[index].totalWastageQty);
                    data[index].totalWastageAmt = parseFloat(s.totalWastageAmt) + parseFloat(data[index].totalWastageAmt);

                    // data[index].totalWastageQty=s.totalWastageQty;
                    // data[index].totalWastageAmt=s.totalWastageAmt;
                  }
                });
                getTransferStockByItemFromStore(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_from_stock) {
                  //console.log(st_from_stock);
                  _.forEach(st_from_stock, function (s, i) {
                    var index = _.findIndex(data, {itemId: s.itemId});
                    if (index < 0) {
                      data.push(s);
                    }
                    else {
                      if (data[index].totalTransferQty == undefined) {
                        data[index].totalTransferQty = 0;
                        data[index].totalTransferAmount = 0;
                      }
                      data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                      data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);

                      //data[index].totalTransferQty=s.totalTransferQty;
                    }
                  });
                  //callback1();
                  getTransferStockByItemToStore(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_to_stock) {
                    _.forEach(st_to_stock, function (s, i) {
                      var index = _.findIndex(data, {itemId: s.itemId});
                      if (index < 0) {
                        data.push(s);
                      }
                      else {
                        if (data[index].totalTransferQty_toStore == undefined) {
                          data[index].totalTransferQty_toStore = 0;
                          data[index].totalTransferAmount_toStore = 0;
                        }
                        data[index].totalTransferQty_toStore = parseFloat(s.totalTransferQty_toStore) + parseFloat(data[index].totalTransferQty_toStore);
                        data[index].totalTransferAmount_toStore = parseFloat(s.totalTransferAmount_toStore) + parseFloat(data[index].totalTransferAmount_toStore);
                      }
                    });
                  });
                  //callback1();
                  //return res.json(val);
                  getProcessedAndIntermediateReceipeDetails_Sale(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                    _.forEach(st_sale, function (s, i) {
                      var index = _.findIndex(data, {itemId: s.itemId});
                      if (index < 0) {
                        data.push(s);
                      }
                      else {
                        if (s.totalSaleQty == undefined) {
                          s.totalSaleQty = 0;
                          s.totalSaleAmount = 0;
                        }
                        if (data[index].totalSaleQty == undefined) {
                          data[index].totalSaleQty = 0;
                          data[index].totalSaleAmount = 0;
                        }
                        data[index].totalSaleQty = parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty);
                        data[index].totalSaleAmount = parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount);

                        //console.log(s.totalSaleQty);
                      }
                    });
                    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                      _.forEach(st_sale, function (s, i) {
                        var index = _.findIndex(data, {itemId: s.itemId});
                        if (index < 0) {
                          data.push(s);
                        }
                        else {
                          if (s.totalTransferQty == undefined) {
                            s.totalTransferQty = 0;
                            s.totalTransferAmount = 0;
                          }
                          if (data[index].totalTransferQty == undefined) {
                            data[index].totalTransferQty = 0;
                            data[index].totalTransferAmount = 0;
                          }
                          data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                          data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);
                          //console.log(s.totalSaleQty);
                        }
                      });
                      if (isKitchen == "true") {
                        BillgetItemWiseReport(deployment_id, tenant_id, minDate, startDate, itm.id, function (BillData) {
                          _.forEach(BillData, function (s, i) {
                            var index = _.findIndex(data, {itemId: s.stockItemId});
                            if (index < 0) {
                              s.itemId = s.stockItemId;
                              s.counterSaleQty = s.itemSaleQty;
                              s.counterSaleAmount = s.itemSaleAmount;
                              s.UnitName = s.baseUnit;
                              delete s.stockItemId;
                              delete s.itemSaleAmount;
                              delete s.itemSaleQty;
                              data.push(s);
                            }
                            else {
                              if (data[index].counterSaleQty == undefined) {
                                data[index].counterSaleQty = 0;
                                data[index].counterSaleAmount = 0;
                              }
                              data[index].counterSaleQty = parseFloat(s.itemSaleQty) + parseFloat(data[index].counterSaleQty);
                              data[index].counterSaleAmount = parseFloat(s.itemSaleAmount) + parseFloat(data[index].counterSaleAmount);
                            }
                          });
                          callback1();
                        });
                      }
                      else {
                        callback1();
                      }
                    });
                    //callback1();
                  });
                  //callback1();
                  //});
                });
                //callback1();
              });
              //callback1();
            });
            //callback1();
            //});
            //callback1();

          });
          //callback1();
        });
      }
      else {
        var minDate = new Date(opendate);
        //minDate.setDate(minDate.getDate()-1);
        var dop = new Date(opendate);
        dop.setSeconds(dop.getSeconds() + 1);
        //startDate=new Date(opendate);
        //startDate.setHours(0,0,0,0);
        //startDate.setSeconds(startDate.getSeconds() + 1);
        //console.log("OPen date "+ new Date(minDate));
        //console.log("start date:"+ new Date(startDate));

        getStockEntryByItem_ConsumptionReport(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            var index = _.findIndex(data, {itemId: s.itemId});
            if (index < 0) {
              data.push(s);
            }
            else {
              if (data[index].totalPurchaseQty == undefined) {
                data[index].totalPurchaseQty = 0;
                data[index].totalPurchaseAmount = 0;
              }
              data[index].totalPurchaseQty = parseFloat(parseFloat(s.totalPurchaseQty) + parseFloat(data[index].totalPurchaseQty));
              data[index].totalPurchaseAmount = parseFloat(parseFloat(s.totalPurchaseAmount) + parseFloat(data[index].totalPurchaseAmount));
            }
          });

          getStockSaleByItem_ConsumptionReport(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              //console.log(s);
              var index = _.findIndex(data, {itemId: s.itemId});
              if (index < 0) {
                if (s.totalSaleQty == undefined) {
                  s.totalSaleQty = 0;
                  s.totalSaleAmount = 0;
                }
                data.push(s);
              }
              else {
                if (data[index].totalSaleQty == undefined) {
                  data[index].totalSaleQty = 0;
                  data[index].totalSaleAmount = 0;
                }
                if (s.totalSaleQty == undefined) {
                  s.totalSaleQty = 0;
                  s.totalSaleAmount = 0;
                }
                data[index].totalSaleQty = parseFloat(parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty));
                data[index].totalSaleAmount = parseFloat(parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount));
              }
            });

            getOpeningStockByItem(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (op_stock) {
              //console.log(op_stock);
              _.forEach(op_stock, function (s, i) {
                var index = _.findIndex(data, {itemId: s.itemId});
                if (index < 0) {
                  //console.log(s.totalOpeningQty);
                  data.push(s);
                }
                else {
                  //console.log(data[index].totalOpeningQty);
                  if (data[index].totalOpeningQty == undefined) {
                    data[index].totalOpeningQty = 0;
                    data[index].totalOpeningAmt = 0;
                  }
                  data[index].totalOpeningQty = parseFloat(s.totalOpeningQty) + parseFloat(data[index].totalOpeningQty);
                  data[index].totalOpeningAmt = parseFloat(s.totalOpeningAmt) + parseFloat(data[index].totalOpeningAmt);
                  //console.log(s.totalOpeningAmt);
                  if (data[index].unitName == undefined) {
                    data[index].unitName = s.unitName;
                  }
                }
              });

              // getPhysicalStockByItem(minDate,startDate,itm.id,deployment_id,tenant_id,storeId,function(bs_stock){
              //   //console.log(bs_stock);
              //   _.forEach(bs_stock, function(s,i){
              //     var index=_.findIndex(data,{itemId:s.itemId});
              //     if(index<0){
              //       data.push(s);
              //     }
              //     else
              //     {
              //       if(data[index].totalBalanceQty==undefined){
              //         data[index].totalBalanceQty=0;data[index].totalBalanceAmt=0;
              //       }
              //       data[index].totalBalanceQty=s.totalBalanceQty;
              //       data[index].totalBalanceAmt=s.totalBalanceAmt;
              //     }
              //   });

              getWastageStockByItem(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (ws_stock) {
                _.forEach(ws_stock, function (s, i) {
                  var index = _.findIndex(data, {itemId: s.itemId});
                  if (index < 0) {
                    data.push(s);
                  }
                  else {
                    if (data[index].totalWastageQty == undefined) {
                      data[index].totalWastageQty = 0;
                      data[index].totalWastageAmt = 0;
                    }
                    data[index].totalWastageQty = parseFloat(s.totalWastageQty) + parseFloat(data[index].totalWastageQty);
                    data[index].totalWastageAmt = parseFloat(s.totalWastageAmt) + parseFloat(data[index].totalWastageAmt);
                    // data[index].totalWastageQty=s.totalWastageQty;
                    // data[index].totalWastageAmt=s.totalWastageAmt;
                  }
                });
                getTransferStockByItemFromStore(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_from_stock) {

                  _.forEach(st_from_stock, function (s, i) {
                    var index = _.findIndex(data, {itemId: s.itemId});
                    if (index < 0) {
                      //console.log(s);
                      data.push(s);
                    }
                    else {
                      if (data[index].totalTransferQty == undefined) {
                        data[index].totalTransferQty = 0;
                        data[index].totalTransferAmount = 0;
                      }
                      data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                      data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);
                    }
                  });
                  //callback1();
                  getTransferStockByItemToStore(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_to_stock) {
                    _.forEach(st_to_stock, function (s, i) {
                      var index = _.findIndex(data, {itemId: s.itemId});
                      if (index < 0) {
                        data.push(s);
                      }
                      else {
                        if (data[index].totalTransferQty_toStore == undefined) {
                          data[index].totalTransferQty_toStore = 0;
                          data[index].totalTransferAmount_toStore = 0;
                        }
                        data[index].totalTransferQty_toStore = parseFloat(s.totalTransferQty_toStore) + parseFloat(data[index].totalTransferQty_toStore);
                        data[index].totalTransferAmount_toStore = parseFloat(s.totalTransferAmount_toStore) + parseFloat(data[index].totalTransferAmount_toStore);
                      }
                    });
                    //callback1();
                    //return res.json(val);
                    getProcessedAndIntermediateReceipeDetails_Sale(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                      _.forEach(st_sale, function (s, i) {
                        var index = _.findIndex(data, {itemId: s.itemId});
                        if (index < 0) {
                          data.push(s);
                        }
                        else {
                          if (s.totalSaleQty == undefined) {
                            s.totalSaleQty = 0;
                            s.totalSaleAmount = 0;
                          }
                          if (data[index].totalSaleQty == undefined) {
                            data[index].totalSaleQty = 0;
                            data[index].totalSaleAmount = 0;
                          }
                          data[index].totalSaleQty = parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty);
                          data[index].totalSaleAmount = parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount);

                          //console.log(s.totalSaleQty);
                        }
                      });

                      getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(minDate, startDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                        _.forEach(st_sale, function (s, i) {
                          var index = _.findIndex(data, {itemId: s.itemId});
                          if (index < 0) {
                            data.push(s);
                          }
                          else {
                            if (s.totalTransferQty == undefined) {
                              s.totalTransferQty = 0;
                              s.totalTransferAmount = 0;
                            }
                            if (data[index].totalTransferQty == undefined) {
                              data[index].totalTransferQty = 0;
                              data[index].totalTransferAmount = 0;
                            }
                            data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                            data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);
                            //console.log(s.totalSaleQty);
                          }
                        });
                        if (isKitchen == "true") {
                          BillgetItemWiseReport(deployment_id, tenant_id, minDate, startDate, itm.id, function (BillData) {
                            _.forEach(BillData, function (s, i) {
                              var index = _.findIndex(data, {itemId: s.stockItemId});
                              if (index < 0) {
                                s.itemId = s.stockItemId;
                                s.counterSaleQty = s.itemSaleQty;
                                s.counterSaleAmount = s.itemSaleAmount;
                                s.UnitName = s.baseUnit;
                                delete s.stockItemId;
                                delete s.itemSaleAmount;
                                delete s.itemSaleQty;
                                data.push(s);
                              }
                              else {
                                if (data[index].counterSaleQty == undefined) {
                                  data[index].counterSaleQty = 0;
                                  data[index].counterSaleAmount = 0;
                                }
                                data[index].counterSaleQty = parseFloat(s.itemSaleQty) + parseFloat(data[index].counterSaleQty);
                                data[index].counterSaleAmount = parseFloat(s.itemSaleAmount) + parseFloat(data[index].counterSaleAmount);
                              }
                            });
                            callback1();
                          });
                        }
                        else {
                          callback1();
                        }
                      });
                      //callback1();
                    });
                    //callback1();
                  });
                });
                //callback1();
              });
              //callback1();
              //});
              //callback1();
            });
            //callback1();

          });
          //callback1();
        });
      }
    });
  }, function (err) {
    if (err) {
      return handleError(res, err);
    } else {
      //console.log(data);
      callback2(data);
    }
  });
};

function getDataBetweenStartAndEndDate(opendate, startDate, endDate, items, deployment_id, tenant_id, storeId, isKitchen, callback2) {
  var data = [];
  //console.log("isKitchen betweenStartAndEnd startDate="+isKitchen);
  // var itemsForLoop=[
  //   {id:'5576b650e414d90c03d1e336',itemName:'Potato'},
  //   {id:'5576b65ce414d90c03d1e337',itemName:'Tomato'},
  //   {id:'5576b663e414d90c03d1e338',itemName:'Onion'},
  //   {id:'5576b66de414d90c03d1e339',itemName:'Capsicum'},
  //   {id:'5576b675e414d90c03d1e33a',itemName:'Chicken Boneless'}
  // ];
  var itemsForLoop = items;
  async.eachSeries(itemsForLoop, function (itm, callback1) {
    //console.log(itm);
    checkPhysicalStockBetweenOpeningAndStart(opendate, startDate, itm.id, deployment_id, tenant_id, storeId, function (ph) {
      if (ph != null) {
        var minDate = new Date(ph.created);
        //console.log("ph not null="+minDate);
        //startDate.setHours(0,0,0,0);
        startDate = new Date(minDate);
        startDate.setHours(0, 0, 0, 0);
        //startDate.setSeconds(startDate.getSeconds() + 1);
        getStockEntryByItem_ConsumptionReport(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            //data.push(s);
            //console.log(s.itemId);
            var index = _.findIndex(data, {itemId: s.itemId});
            if (index < 0) {
              data.push(s);
            }
            else {
              if (data[index].totalPurchaseQty == undefined) {
                data[index].totalPurchaseQty = 0;
                data[index].totalPurchaseAmount = 0;
              }
              data[index].totalPurchaseQty = parseFloat(s.totalPurchaseQty) + parseFloat(data[index].totalPurchaseQty);
              data[index].totalPurchaseAmount = parseFloat(s.totalPurchaseAmount) + parseFloat(data[index].totalPurchaseAmount);
            }
          });

          getStockSaleByItem_ConsumptionReport(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              var index = _.findIndex(data, {itemId: s.itemId});
              if (index < 0) {
                data.push(s);
              }
              else {
                if (data[index].totalSaleQty == undefined) {
                  data[index].totalSaleQty = 0;
                  data[index].totalSaleAmount = 0;
                }
                data[index].totalSaleQty = parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty);
                data[index].totalSaleAmount = parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount);
              }
            });

            // getOpeningStockByItem(startDate,endDate,itm.id,function(op_stock){
            //   //console.log(op_stock);
            //   _.forEach(op_stock, function(s,i){
            //     var index=_.findIndex(data,{itemId:s.itemId});
            //     if(index<0){
            //       data.push(s);
            //     }
            //     else
            //     {
            //       data[index].totalOpeningQty=s.totalOpeningQty;
            //     }
            //   });

            //   getPhysicalStockByItem(startDate,endDate,itm.id,function(bs_stock){
            //     //console.log(bs_stock);
            //     _.forEach(bs_stock, function(s,i){
            //       var index=_.findIndex(data,{itemId:s.itemId});
            //       if(index<0){
            //         data.push(s);
            //       }
            //       else
            //       {
            //         data[index].totalBalanceQty=s.totalBalanceQty;
            //       }
            //     });

            getWastageStockByItem(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (ws_stock) {
              _.forEach(ws_stock, function (s, i) {
                var index = _.findIndex(data, {itemId: s.itemId});
                if (index < 0) {
                  data.push(s);
                }
                else {
                  if (data[index].totalWastageQty == undefined) {
                    data[index].totalWastageQty = 0;
                    data[index].totalWastageAmt = 0;
                  }
                  data[index].totalWastageQty = parseFloat(s.totalWastageQty) + parseFloat(data[index].totalWastageQty);
                  data[index].totalWastageAmt = parseFloat(s.totalWastageAmt) + parseFloat(data[index].totalWastageAmt);
                  //data[index].totalWastageQty=s.totalWastageQty;
                }
              });
              getTransferStockByItemFromStore(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_from_stock) {
                //console.log(st_from_stock);
                _.forEach(st_from_stock, function (s, i) {
                  var index = _.findIndex(data, {itemId: s.itemId});
                  if (index < 0) {
                    data.push(s);
                  }
                  else {
                    if (data[index].totalTransferQty == undefined) {
                      data[index].totalTransferQty = 0;
                      data[index].totalTransferAmount = 0;
                    }
                    data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                    data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);

                    //data[index].totalTransferQty=s.totalTransferQty;
                  }
                });
                //callback1();
                getTransferStockByItemToStore(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_to_stock) {
                  _.forEach(st_to_stock, function (s, i) {
                    var index = _.findIndex(data, {itemId: s.itemId});
                    if (index < 0) {
                      data.push(s);
                    }
                    else {
                      if (data[index].totalTransferQty_toStore == undefined) {
                        data[index].totalTransferQty_toStore = 0;
                        data[index].totalTransferAmount_toStore = 0;
                      }
                      data[index].totalTransferQty_toStore = parseFloat(s.totalTransferQty_toStore) + parseFloat(data[index].totalTransferQty_toStore);
                      data[index].totalTransferAmount_toStore = parseFloat(s.totalTransferAmount_toStore) + parseFloat(data[index].totalTransferAmount_toStore);
                    }
                  });

                  getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                    _.forEach(st_sale, function (s, i) {
                      var index = _.findIndex(data, {itemId: s.itemId});
                      if (index < 0) {
                        data.push(s);
                      }
                      else {
                        if (s.totalSaleQty == undefined) {
                          s.totalSaleQty = 0;
                          s.totalSaleAmount = 0;
                        }
                        if (data[index].totalSaleQty == undefined) {
                          data[index].totalSaleQty = 0;
                          data[index].totalSaleAmount = 0;
                        }
                        data[index].totalSaleQty = parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty);
                        data[index].totalSaleAmount = parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount);

                        //console.log(s.totalSaleQty);
                      }
                    });
                    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                      _.forEach(st_sale, function (s, i) {
                        var index = _.findIndex(data, {itemId: s.itemId});
                        if (index < 0) {
                          data.push(s);
                        }
                        else {
                          if (s.totalTransferQty == undefined) {
                            s.totalTransferQty = 0;
                            s.totalTransferAmount = 0;
                          }
                          if (data[index].totalTransferQty == undefined) {
                            data[index].totalTransferQty = 0;
                            data[index].totalTransferAmount = 0;
                          }
                          data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                          data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);
                          //console.log(s.totalSaleQty);
                        }
                      });
                      if (isKitchen == "true") {
                        BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate, itm.id, function (BillData) {
                          _.forEach(BillData, function (s, i) {
                            var index = _.findIndex(data, {itemId: s.stockItemId});
                            if (index < 0) {
                              s.itemId = s.stockItemId;
                              s.counterSaleQty = s.itemSaleQty;
                              s.counterSaleAmount = s.itemSaleAmount;
                              s.UnitName = s.baseUnit;
                              delete s.stockItemId;
                              delete s.itemSaleAmount;
                              delete s.itemSaleQty;
                              data.push(s);
                            }
                            else {
                              if (data[index].counterSaleQty == undefined) {
                                data[index].counterSaleQty = 0;
                                data[index].counterSaleAmount = 0;
                              }
                              data[index].counterSaleQty = parseFloat(s.itemSaleQty) + parseFloat(data[index].counterSaleQty);
                              data[index].counterSaleAmount = parseFloat(s.itemSaleAmount) + parseFloat(data[index].counterSaleAmount);
                            }
                          });
                          callback1();
                        });
                      }
                      else {
                        callback1();
                      }
                    });
                    //callback1();
                  });
                  //callback1();
                });
              });
              //callback1();
            });
            //callback1();
            //});
            //callback1();
            //});
            //callback1();

          });
          //callback1();
        });
      }
      else {
        var minDate = new Date(opendate);
        //minDate.setDate(minDate.getDate()-1);
        //console.log("between minDate="+minDate);
        // startDate=new Date(opendate);
        // startDate.setHours(0,0,0,0);
        //startDate.setSeconds(startDate.getSeconds() + 1);
        // console.log("between startDate="+startDate);
        // console.log("between endDate="+endDate);

        getStockEntryByItem_ConsumptionReport(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            var index = _.findIndex(data, {itemId: s.itemId});
            if (index < 0) {
              data.push(s);
            }
            else {
              if (data[index].totalPurchaseQty == undefined) {
                data[index].totalPurchaseQty = 0;
                data[index].totalPurchaseAmount = 0;
              }
              data[index].totalPurchaseQty = parseFloat(s.totalPurchaseQty) + parseFloat(data[index].totalPurchaseQty);
              data[index].totalPurchaseAmount = parseFloat(s.totalPurchaseAmount) + parseFloat(data[index].totalPurchaseAmount);
            }
          });

          getStockSaleByItem_ConsumptionReport(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              var index = _.findIndex(data, {itemId: s.itemId});
              if (index < 0) {
                data.push(s);
              }
              else {
                if (data[index].totalSaleQty == undefined) {
                  data[index].totalSaleQty = 0;
                  data[index].totalSaleAmount = 0;
                }
                data[index].totalSaleQty = parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty);
                data[index].totalSaleAmount = parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount);
              }
            });

            // getOpeningStockByItem(startDate,endDate,itm.id,function(op_stock){
            //   //console.log(op_stock);
            //   _.forEach(op_stock, function(s,i){
            //     var index=_.findIndex(data,{itemId:s.itemId});
            //     if(index<0){
            //       data.push(s);
            //     }
            //     else
            //     {
            //       data[index].totalOpeningQty=s.totalOpeningQty;
            //     }
            //   });

            //   getPhysicalStockByItem(startDate,endDate,itm.id,function(bs_stock){
            //     //console.log(bs_stock);
            //     _.forEach(bs_stock, function(s,i){
            //       var index=_.findIndex(data,{itemId:s.itemId});
            //       if(index<0){
            //         data.push(s);
            //       }
            //       else
            //       {
            //         data[index].totalBalanceQty=s.totalBalanceQty;
            //       }
            //     });

            getWastageStockByItem(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (ws_stock) {
              _.forEach(ws_stock, function (s, i) {
                var index = _.findIndex(data, {itemId: s.itemId});
                if (index < 0) {
                  data.push(s);
                }
                else {
                  if (data[index].totalWastageQty == undefined) {
                    data[index].totalWastageQty = 0;
                    data[index].totalWastageAmt = 0;
                  }
                  data[index].totalWastageQty = parseFloat(s.totalWastageQty) + parseFloat(data[index].totalWastageQty);
                  data[index].totalWastageAmt = parseFloat(s.totalWastageAmt) + parseFloat(data[index].totalWastageAmt);
                  //data[index].totalWastageQty=s.totalWastageQty;
                }
              });
              getTransferStockByItemFromStore(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_from_stock) {
                _.forEach(st_from_stock, function (s, i) {
                  //console.log(s);
                  var index = _.findIndex(data, {itemId: s.itemId});
                  if (index < 0) {
                    //console.log(s);
                    data.push(s);
                  }
                  else {
                    if (data[index].totalTransferQty == undefined) {
                      data[index].totalTransferQty = 0;
                      data[index].totalTransferAmount = 0;
                    }
                    data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                    data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);

                    //data[index].totalTransferQty=s.totalTransferQty;
                  }
                  //console.log(index);
                  //console.log(data[index]);
                });
                //callback1();
                getTransferStockByItemToStore(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_to_stock) {
                  _.forEach(st_to_stock, function (s, i) {
                    var index = _.findIndex(data, {itemId: s.itemId});
                    if (index < 0) {
                      //console.log(s);
                      data.push(s);
                    }
                    else {
                      if (data[index].totalTransferQty_toStore == undefined) {
                        data[index].totalTransferQty_toStore = 0;
                        data[index].totalTransferAmount_toStore = 0;
                      }
                      data[index].totalTransferQty_toStore = parseFloat(s.totalTransferQty_toStore) + parseFloat(data[index].totalTransferQty_toStore);
                      data[index].totalTransferAmount_toStore = parseFloat(s.totalTransferAmount_toStore) + parseFloat(data[index].totalTransferAmount_toStore);

                      //data[index].totalTransferQty=s.totalTransferQty;
                    }
                    //console.log(index);
                    //console.log(data[index]);
                  });
                  //return res.json(val);
                  getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                    _.forEach(st_sale, function (s, i) {
                      var index = _.findIndex(data, {itemId: s.itemId});
                      if (index < 0) {
                        data.push(s);
                      }
                      else {
                        if (s.totalSaleQty == undefined) {
                          s.totalSaleQty = 0;
                          s.totalSaleAmount = 0;
                        }
                        if (data[index].totalSaleQty == undefined) {
                          data[index].totalSaleQty = 0;
                          data[index].totalSaleAmount = 0;
                        }
                        data[index].totalSaleQty = parseFloat(s.totalSaleQty) + parseFloat(data[index].totalSaleQty);
                        data[index].totalSaleAmount = parseFloat(s.totalSaleAmount) + parseFloat(data[index].totalSaleAmount);

                      }
                    });

                    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, itm.id, deployment_id, tenant_id, storeId, function (st_sale) {
                      _.forEach(st_sale, function (s, i) {
                        var index = _.findIndex(data, {itemId: s.itemId});
                        if (index < 0) {
                          data.push(s);
                        }
                        else {
                          if (s.totalTransferQty == undefined) {
                            s.totalTransferQty = 0;
                            s.totalTransferAmount = 0;
                          }
                          if (data[index].totalTransferQty == undefined) {
                            data[index].totalTransferQty = 0;
                            data[index].totalTransferAmount = 0;
                          }
                          data[index].totalTransferQty = parseFloat(s.totalTransferQty) + parseFloat(data[index].totalTransferQty);
                          data[index].totalTransferAmount = parseFloat(s.totalTransferAmount) + parseFloat(data[index].totalTransferAmount);
                          //console.log(s.totalSaleQty);
                        }
                      });
                      if (isKitchen == "true") {
                        BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate, itm.id, function (BillData) {
                          _.forEach(BillData, function (s, i) {
                            var index = _.findIndex(data, {itemId: s.stockItemId});
                            if (index < 0) {
                              s.itemId = s.stockItemId;
                              s.counterSaleQty = s.itemSaleQty;
                              s.counterSaleAmount = s.itemSaleAmount;
                              s.UnitName = s.baseUnit;
                              delete s.stockItemId;
                              delete s.itemSaleAmount;
                              delete s.itemSaleQty;
                              data.push(s);
                            }
                            else {
                              if (data[index].counterSaleQty == undefined) {
                                data[index].counterSaleQty = 0;
                                data[index].counterSaleAmount = 0;
                              }
                              data[index].counterSaleQty = parseFloat(s.itemSaleQty) + parseFloat(data[index].counterSaleQty);
                              data[index].counterSaleAmount = parseFloat(s.itemSaleAmount) + parseFloat(data[index].counterSaleAmount);
                            }
                          });
                          callback1();
                        });
                      }
                      else {
                        callback1();
                      }
                    });
                    //callback1();
                  });
                });
              });
              //callback1();
            });
            //callback1();
            //});
            //callback1();
            //});
            //callback1();

          });
          //callback1();
        });
      }
    });
  }, function (err) {
    if (err) {
      return handleError(res, err);
    } else {
      //console.log(data);
      callback2(data);
    }
  });
};

function getDataCon(opendate, startDate, endDate, deployment_id, tenant_id, callback) {
  var data = [];
  var itemsForLoop = [
    {id: '5576b650e414d90c03d1e336', itemName: 'Potato'},
    {id: '5576b65ce414d90c03d1e337', itemName: 'Tomato'},
    {id: '5576b663e414d90c03d1e338', itemName: 'Onion'},
    {id: '5576b66de414d90c03d1e339', itemName: 'Capsicum'},
    {id: '5576b675e414d90c03d1e33a', itemName: 'Chicken Boneless'}
  ];
  // _.forEach(itemsForLoop,function(itm,i){
  //   console.log(itm);
  //   checkPhysicalStockBetweenOpeningAndStart(opendate,startDate,itm.id,function(ph){
  //     if(ph!=null){
  //       var minDate=new Date(ph.created);
  //       console.log("Ph not null=" +minDate);
  //     }
  //     else
  //     {
  //       var minDate=new Date(opendate);
  //       minDate.setDate(minDate.getDate()-1);
  //           //console.log(minDate);

  //           getStockEntryByItem_ConsumptionReport(minDate,endDate,itm.id,function(st_entry){
  //              _.forEach(st_entry,function(s,i){
  //                data.push(s);
  //              });
  //           console.log(st_entry);
  //             //return res.json(200,data);
  //           });

  //         }
  //       });
  // });
  // callback(data);


  async.eachSeries(itemsForLoop, function (itm, callback1) {
    //console.log(itm);
    checkPhysicalStockBetweenOpeningAndStart(opendate, startDate, itm.id, deployment_id, tenant_id, function (ph) {
      if (ph != null) {
        var minDate = new Date(ph.created);
        //console.log("ph not null="+minDate);
      }
      else {
        var minDate = new Date(opendate);
        minDate.setDate(minDate.getDate() - 1);
        getStockEntryByItem_ConsumptionReport(minDate, endDate, itm.id, deployment_id, tenant_id, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            data.push(s);
          });

          getStockSaleByItem_ConsumptionReport(minDate, endDate, itm.id, deployment_id, tenant_id, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              var index = _.findIndex(data, {itemId: s.itemId});
              if (index < 0) {
                data.push(s);
              }
              else {
                data[index].totalSaleQty = s.totalSaleQty;
                data[index].totalSaleAmount = s.totalSaleAmount;
              }
            });

            getOpeningStockByItem(opendate, endDate, itm.id, deployment_id, tenant_id, function (op_stock) {
              //console.log(op_stock);
              _.forEach(op_stock, function (s, i) {
                var index = _.findIndex(data, {itemId: s.itemId});
                if (index < 0) {
                  data.push(s);
                }
                else {
                  data[index].totalOpeningQty = s.totalOpeningQty;
                }
              });

              getPhysicalStockByItem(opendate, endDate, itm.id, deployment_id, tenant_id, function (bs_stock) {
                //console.log(bs_stock);
                _.forEach(bs_stock, function (s, i) {
                  var index = _.findIndex(data, {itemId: s.itemId});
                  if (index < 0) {
                    data.push(s);
                  }
                  else {
                    data[index].totalBalanceQty = s.totalBalanceQty;
                  }
                });

                getWastageStockByItem(opendate, endDate, itm.id, deployment_id, tenant_id, function (ws_stock) {
                  _.forEach(ws_stock, function (s, i) {
                    var index = _.findIndex(data, {itemId: s.itemId});
                    if (index < 0) {
                      data.push(s);
                    }
                    else {
                      data[index].totalWastageQty = s.totalWastageQty;
                    }
                  });
                  getTransferStockByItemFromStore(opendate, endDate, itm.id, deployment_id, tenant_id, function (st_from_stock) {
                    //console.log(st_from_stock);
                    _.forEach(st_from_stock, function (s, i) {
                      var index = _.findIndex(data, {itemId: s.itemId});
                      if (index < 0) {
                        data.push(s);
                      }
                      else {
                        data[index].totalTransferQty = s.totalTransferQty;
                      }
                    });
                    callback1();
                  });
                  //callback1();
                });
                //callback1();
              });
              //callback1();
            });
            //callback1();

          });
          //callback1();
        });
      }
    });
  }, function (err) {
    if (err) {
      return handleError(res, err);
    } else {
      //res.json(200, _bills);
      //console.log(data);
      callback(data);
    }
  });

};
exports.getConsumptionSummary = function (req, res) {
  var ss = null;
  //var  startDate="6/30/2015 8:44:21 AM";
  //var endDate="7/6/2015 8:44:21 AM";
  //console.log(req);
  //console.log(req.body);
  var startDate = new Date(req.query.fromDate);
  startDate.setHours(0, 0, 0, 0);
  //startDate.setDate(startDate.getDate() + 1);
  //console.log(new Date(startDate));
  //console.log("Start Date"+ new Date(startDate));
  var endDate = new Date(req.query.toDate);
  endDate.setDate(endDate.getDate() + 1);
  var data = {items: []};
  var items = JSON.parse(req.query.items);
  //console.log(items);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;
  var categoryId = req.query.category;
  var isKitchen = false;
  if (req.query.isKitchen != undefined) {
    isKitchen = req.query.isKitchen;
    //console.log("isKitchen="+isKitchen);
  }
  // console.log(storeId);
  // console.log(categoryId);
  checkOpeningStock(startDate, deployment_id, tenant_id, function (os) {
    if (os != null) {
      // getDataCon(os.created,startDate,endDate,function(data){
      //   return res.json(200,data);
      // });
      var opendate = new Date(os.created);
      opendate.setHours(0, 0, 0, 0);
      getDataBeforeStartDate(opendate, startDate, items, deployment_id, tenant_id, storeId, isKitchen, function (a) {
        console.log(a);
        _.forEach(a, function (itm, i) {
          //console.log(i);
          if (itm.totalWastageQty == undefined || itm.totalWastageAmt == undefined) {
            itm.totalWastageQty = 0;
            itm.totalWastageAmt = 0
          }
          ;
          if (itm.totalSaleQty == undefined || itm.totalSaleAmount == undefined) {
            itm.totalSaleQty = 0;
            itm.totalSaleAmount = 0
          }
          ;
          if (itm.totalPurchaseQty == undefined || itm.totalPurchaseAmount == undefined) {
            itm.totalPurchaseQty = 0;
            itm.totalPurchaseAmount = 0
          }
          ;
          if (itm.totalTransferQty == undefined || itm.totalTransferAmount == undefined) {
            itm.totalTransferQty = 0;
            itm.totalTransferAmount = 0
          }
          ;
          if (itm.totalTransferQty_toStore == undefined || itm.totalTransferAmount_toStore == undefined) {
            itm.totalTransferQty_toStore = 0;
            itm.totalTransferAmount_toStore = 0
          }
          ;
          if (itm.counterSaleQty == undefined || itm.counterSaleAmount == undefined) {
            itm.counterSaleQty = 0;
            itm.counterSaleAmount = 0
          }
          ;
          if (itm.totalOpeningQty == undefined || itm.totalOpeningAmt == undefined) {
            itm.totalOpeningQty = 0;
            itm.totalOpeningAmt = 0;
          }
          ;
          if (itm.totalBalanceQty == undefined || itm.totalBalanceAmt == undefined) {
            itm.totalBalanceQty = 0;
            itm.totalBalanceAmt = 0;
          }
          ;
          // console.log(itm.totalPurchaseQty);
          // console.log(itm.totalOpeningQty);
          // console.log(itm.totalBalanceQty);
          // console.log(itm.totalWastageQty);
          // console.log(itm.totalSaleQty);
          // console.log(itm.totalTransferQty);
          itm.openingQty = parseFloat(parseFloat(itm.totalPurchaseQty) + parseFloat(itm.totalOpeningQty) + parseFloat(itm.totalBalanceQty) + parseFloat(itm.totalTransferQty_toStore) - parseFloat(parseFloat(parseFloat(itm.totalWastageQty) + parseFloat(itm.totalSaleQty) + parseFloat(itm.totalTransferQty) + parseFloat(itm.counterSaleQty)))).toFixed(3);
          itm.openingAmt = parseFloat(parseFloat(itm.totalPurchaseAmount) + parseFloat(itm.totalOpeningAmt) + parseFloat(itm.totalBalanceAmt) + parseFloat(itm.totalTransferAmount_toStore) - parseFloat((parseFloat(itm.totalWastageAmt) + parseFloat(itm.totalSaleAmount) + parseFloat(itm.totalTransferAmount + parseFloat(itm.counterSaleAmount))))).toFixed(2);

          // console.log(parseFloat(itm.totalSaleQty));
          // console.log(itm.totalSaleQty);

          itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
          var openDate = new Date(startDate);
          itm.openningDate = new Date(startDate);
          var closingDate = new Date(endDate);
          itm.closingDate = new Date(closingDate.setDate(closingDate.getDate() - 1));
          itm.totalWastageQty = 0;
          itm.totalWastageAmt = 0;
          itm.totalSaleQty = 0;
          itm.totalSaleAmount = 0;
          itm.totalPurchaseQty = 0;
          itm.totalPurchaseAmount = 0;
          itm.totalTransferQty = 0;
          itm.totalTransferAmount = 0;
          itm.totalTransferQty_toStore = 0;
          itm.totalTransferAmount_toStore = 0;
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
          //console.log(itm);
          data.items.push(itm);
        });
        //console.log(a);
        //data.items=a;

        getDataBetweenStartAndEndDate(opendate, startDate, endDate, items, deployment_id, tenant_id, storeId, isKitchen, function (b) {
          //console.log(b);
          _.forEach(b, function (itm, i) {
            //console.log(itm);
            var index = _.findIndex(data.items, {itemId: itm.itemId});
            //console.log("betweenStartAndEnd:"+itm.itemId, "index="+index);
            //console.log("betweenStartAndEnd:"+itm.itemId, "closingQty="+data.items[i]closingQty);
            //console.log(index);
            if (index < 0) {

              // if(itm.totalWastageQty==undefined){itm.totalWastageQty=0;itm.totalWastageAmt=0};
              // if(itm.totalSaleQty==undefined){itm.totalSaleQty=0;itm.totalSaleAmount=0};
              // if(itm.totalPurchaseQty==undefined){itm.totalPurchaseQty=0;itm.totalPurchaseAmount=0};
              // if(itm.totalTransferQty==undefined){itm.totalTransferQty=0;itm.totalTransferAmount=0};
              // if(itm.openingQty==undefined){itm.openingQty=0;itm.openingAmt=0};
              // itm.closingQty=(itm.totalPurchaseQty+itm.totalTransferQty-(itm.totalWastageQty+itm.totalSaleQty));
              // itm.closingAmount=(itm.totalPurchaseAmount+itm.totalTransferAmount-(itm.totalWastageAmt+itm.totalSaleAmount));
              // itm.openningDate=new Date(startDate);
              // itm.closingDate=new Date(endDate);
              // data.items.push(itm);
            }
            else {
              //console.log(index);
              if (itm.totalWastageQty == undefined || itm.totalWastageAmt == undefined) {
                itm.totalWastageQty = 0;
                itm.totalWastageAmt = 0
              }
              ;
              if (itm.totalSaleQty == undefined || itm.totalSaleAmount == undefined) {
                itm.totalSaleQty = 0;
                itm.totalSaleAmount = 0
              }
              ;
              if (itm.totalPurchaseQty == undefined || itm.totalPurchaseAmount == undefined) {
                itm.totalPurchaseQty = 0;
                itm.totalPurchaseAmount = 0
              }
              ;
              if (itm.totalTransferQty == undefined || itm.totalTransferAmount == undefined) {
                itm.totalTransferQty = 0;
                itm.totalTransferAmount = 0
              }
              ;
              if (itm.totalTransferQty_toStore == undefined || itm.totalTransferAmount_toStore == undefined) {
                itm.totalTransferQty_toStore = 0;
                itm.totalTransferAmount_toStore = 0
              }
              ;
              if (itm.totalBalanceQty == undefined || itm.totalBalanceAmt == undefined) {
                itm.totalBalanceQty = 0;
                itm.totalBalanceAmt = 0;
              }
              ;
              if (itm.counterSaleQty == undefined || itm.counterSaleAmount == undefined) {
                itm.counterSaleQty = 0;
                itm.counterSaleAmount = 0
              }
              ;

              data.items[index].totalWastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
              data.items[index].totalWastageAmt = parseFloat(itm.totalWastageAmt).toFixed(2);
              data.items[index].totalPurchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
              data.items[index].totalPurchaseAmount = parseFloat(itm.totalPurchaseAmount).toFixed(2);
              data.items[index].totalSaleQty = parseFloat(itm.totalSaleQty).toFixed(3);
              data.items[index].totalSaleAmount = parseFloat(itm.totalSaleAmount).toFixed(2);
              data.items[index].totalTransferQty = parseFloat(itm.totalTransferQty).toFixed(3);
              data.items[index].totalTransferAmount = parseFloat(itm.totalTransferAmount).toFixed(2);
              data.items[index].totalTransferQty_toStore = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              data.items[index].totalTransferAmount_toStore = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              data.items[index].counterSaleQty = parseFloat(itm.counterSaleQty).toFixed(3);
              data.items[index].counterSaleAmount = parseFloat(itm.counterSaleAmount).toFixed(2);
              if (data.items[index].UnitName == undefined) {
                data.items[index].UnitName = itm.UnitName;
              }

              data.items[index].purchaseQty = parseFloat(parseFloat(data.items[index].totalPurchaseQty) + parseFloat(data.items[index].totalTransferQty_toStore)).toFixed(3);
              data.items[index].purchaseAmount = parseFloat(parseFloat(data.items[index].totalPurchaseAmount) + parseFloat(data.items[index].totalTransferAmount_toStore)).toFixed(2);
              data.items[index].consumptionQty = parseFloat(parseFloat(itm.counterSaleQty) + parseFloat(data.items[index].totalSaleQty) + parseFloat(data.items[index].totalWastageQty) + parseFloat(data.items[index].totalTransferQty)).toFixed(3);
              data.items[index].consumptionAmount = parseFloat(parseFloat(itm.counterSaleAmount) + parseFloat(data.items[index].totalSaleAmount) + parseFloat(data.items[index].totalWastageAmt) + parseFloat(data.items[index].totalTransferAmount)).toFixed(2);
              data.items[index].closingQty = parseFloat(parseFloat(data.items[index].openingQty) + parseFloat(itm.totalPurchaseQty) + parseFloat(itm.totalTransferQty_toStore) - parseFloat(parseFloat(itm.totalTransferQty) + (parseFloat(itm.totalWastageQty) + parseFloat(itm.totalSaleQty) + parseFloat(itm.counterSaleQty)))).toFixed(3);
              data.items[index].closingAmount = parseFloat(parseFloat(data.items[index].openingAmt) + parseFloat(itm.totalPurchaseAmount) + parseFloat(itm.totalTransferAmount_toStore) - parseFloat(parseFloat(itm.totalTransferAmount) + (parseFloat(itm.totalWastageAmt) + parseFloat(itm.totalSaleAmount) + parseFloat(itm.counterSaleAmount)))).toFixed(2);
              // data.items[index].openningDate=new Date(startDate);
              // data.items[index].closingDate=new Date(endDate.setDate(endDate.getDate() - 1));
            }
          });
          //data.betweenStartAndEnd=b;
          return res.json(200, data);
        });

      });

      //console.log(data);
      //return res.json(200,data);
    }
    else {
      return res.json(200, data);
    }
  });

};

exports.getItemWiseConsumptionSummaryReport = function (req, res) {
  var ss = null;
  var startDate = new Date(req.query.fromDate);
  startDate.setHours(0, 0, 0, 0);
  //console.log("Start Date"+ new Date(startDate));
  var endDate = new Date(req.query.toDate);
  endDate.setDate(endDate.getDate() + 1);
  var data = {items: []};
  var items = JSON.parse(req.query.items);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;
  var categoryId = req.query.category;
  var isKitchen = false;
  if (req.query.isKitchen != undefined) {
    isKitchen = req.query.isKitchen;
    //console.log("isKitchen="+isKitchen);
  }
  checkOpeningStock(startDate, deployment_id, tenant_id, function (os) {
    if (os != null) {
      var opendate = new Date(os.created);
      opendate.setHours(0, 0, 0, 0);

      getDataBeforeStartDate(opendate, startDate, items, deployment_id, tenant_id, storeId, isKitchen, function (a) {
        //console.log(a);
        _.forEach(a, function (itm, i) {
          //console.log(i);
          if (itm.totalWastageQty == undefined || itm.totalWastageAmt == undefined) {
            itm.totalWastageQty = 0;
            itm.totalWastageAmt = 0
          }
          ;
          if (itm.totalSaleQty == undefined || itm.totalSaleAmount == undefined) {
            itm.totalSaleQty = 0;
            itm.totalSaleAmount = 0
          }
          ;
          if (itm.totalPurchaseQty == undefined || itm.totalPurchaseAmount == undefined) {
            itm.totalPurchaseQty = 0;
            itm.totalPurchaseAmount = 0
          }
          ;
          if (itm.totalTransferQty == undefined || itm.totalTransferAmount == undefined) {
            itm.totalTransferQty = 0;
            itm.totalTransferAmount = 0
          }
          ;
          if (itm.totalTransferQty_toStore == undefined || itm.totalTransferAmount_toStore == undefined) {
            itm.totalTransferQty_toStore = 0;
            itm.totalTransferAmount_toStore = 0
          }
          ;
          if (itm.counterSaleQty == undefined || itm.counterSaleAmount == undefined) {
            itm.counterSaleQty = 0;
            itm.counterSaleAmount = 0
          }
          ;
          if (itm.totalOpeningQty == undefined || itm.totalOpeningAmt == undefined) {
            itm.totalOpeningQty = 0;
            itm.totalOpeningAmt = 0;
          }
          ;
          if (itm.totalBalanceQty == undefined || itm.totalBalanceAmt == undefined) {
            itm.totalBalanceQty = 0;
            itm.totalBalanceAmt = 0;
          }
          ;
          itm.openingQty = parseFloat(parseFloat(itm.totalPurchaseQty) + parseFloat(itm.totalOpeningQty) + parseFloat(itm.totalBalanceQty) + parseFloat(itm.totalTransferQty_toStore) - parseFloat(parseFloat(parseFloat(itm.totalWastageQty) + parseFloat(itm.totalSaleQty) + parseFloat(itm.totalTransferQty) + parseFloat(itm.counterSaleQty)))).toFixed(3);
          itm.openingAmt = parseFloat(parseFloat(itm.totalPurchaseAmount) + parseFloat(itm.totalOpeningAmt) + parseFloat(itm.totalBalanceAmt) + parseFloat(itm.totalTransferAmount_toStore) - parseFloat((parseFloat(itm.totalWastageAmt) + parseFloat(itm.totalSaleAmount) + parseFloat(itm.totalTransferAmount + parseFloat(itm.counterSaleAmount))))).toFixed(2);
          itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
          var openDate = new Date(startDate);
          itm.openningDate = new Date(startDate);
          var closingDate = new Date(endDate);
          itm.closingDate = new Date(closingDate.setDate(closingDate.getDate() - 1));
          itm.totalWastageQty = 0;
          itm.totalWastageAmt = 0;
          itm.totalSaleQty = 0;
          itm.totalSaleAmount = 0;
          itm.totalPurchaseQty = 0;
          itm.totalPurchaseAmount = 0;
          itm.totalTransferQty = 0;
          itm.totalTransferAmount = 0;
          itm.totalTransferQty_toStore = 0;
          itm.totalTransferAmount_toStore = 0;
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
          data.items.push(itm);
        });

        getDataBetweenStartAndEndDate(opendate, startDate, endDate, items, deployment_id, tenant_id, storeId, isKitchen, function (b) {
          _.forEach(b, function (itm, i) {
            var index = _.findIndex(data.items, {itemId: itm.itemId});
            if (index < 0) {

            }
            else {

              if (itm.totalWastageQty == undefined || itm.totalWastageAmt == undefined) {
                itm.totalWastageQty = 0;
                itm.totalWastageAmt = 0
              }
              ;
              if (itm.totalSaleQty == undefined || itm.totalSaleAmount == undefined) {
                itm.totalSaleQty = 0;
                itm.totalSaleAmount = 0
              }
              ;
              if (itm.totalPurchaseQty == undefined || itm.totalPurchaseAmount == undefined) {
                itm.totalPurchaseQty = 0;
                itm.totalPurchaseAmount = 0
              }
              ;
              if (itm.totalTransferQty == undefined || itm.totalTransferAmount == undefined) {
                itm.totalTransferQty = 0;
                itm.totalTransferAmount = 0
              }
              ;
              if (itm.totalTransferQty_toStore == undefined || itm.totalTransferAmount_toStore == undefined) {
                itm.totalTransferQty_toStore = 0;
                itm.totalTransferAmount_toStore = 0
              }
              ;
              if (itm.totalBalanceQty == undefined || itm.totalBalanceAmt == undefined) {
                itm.totalBalanceQty = 0;
                itm.totalBalanceAmt = 0;
              }
              ;
              if (itm.counterSaleQty == undefined || itm.counterSaleAmount == undefined) {
                itm.counterSaleQty = 0;
                itm.counterSaleAmount = 0
              }
              ;

              data.items[index].totalWastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
              data.items[index].totalWastageAmt = parseFloat(itm.totalWastageAmt).toFixed(2);
              data.items[index].totalPurchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
              data.items[index].totalPurchaseAmount = parseFloat(itm.totalPurchaseAmount).toFixed(2);
              data.items[index].totalSaleQty = parseFloat(itm.totalSaleQty).toFixed(3);
              data.items[index].totalSaleAmount = parseFloat(itm.totalSaleAmount).toFixed(2);
              data.items[index].totalTransferQty = parseFloat(itm.totalTransferQty).toFixed(3);
              data.items[index].totalTransferAmount = parseFloat(itm.totalTransferAmount).toFixed(2);
              data.items[index].totalTransferQty_toStore = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              data.items[index].totalTransferAmount_toStore = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              data.items[index].counterSaleQty = parseFloat(itm.counterSaleQty).toFixed(3);
              data.items[index].counterSaleAmount = parseFloat(itm.counterSaleAmount).toFixed(2);
              if (data.items[index].UnitName == undefined) {
                data.items[index].UnitName = itm.UnitName;
              }

              data.items[index].purchaseQty = parseFloat(parseFloat(data.items[index].totalPurchaseQty) + parseFloat(data.items[index].totalTransferQty_toStore)).toFixed(3);
              data.items[index].purchaseAmount = parseFloat(parseFloat(data.items[index].totalPurchaseAmount) + parseFloat(data.items[index].totalTransferAmount_toStore)).toFixed(2);
              data.items[index].consumptionQty = parseFloat(parseFloat(itm.counterSaleQty) + parseFloat(data.items[index].totalSaleQty) + parseFloat(data.items[index].totalWastageQty) + parseFloat(data.items[index].totalTransferQty)).toFixed(3);
              data.items[index].consumptionAmount = parseFloat(parseFloat(itm.counterSaleAmount) + parseFloat(data.items[index].totalSaleAmount) + parseFloat(data.items[index].totalWastageAmt) + parseFloat(data.items[index].totalTransferAmount)).toFixed(2);
              data.items[index].closingQty = parseFloat(parseFloat(data.items[index].openingQty) + parseFloat(itm.totalPurchaseQty) + parseFloat(itm.totalTransferQty_toStore) - parseFloat(parseFloat(itm.totalTransferQty) + (parseFloat(itm.totalWastageQty) + parseFloat(itm.totalSaleQty) + parseFloat(itm.counterSaleQty)))).toFixed(3);
              data.items[index].closingAmount = parseFloat(parseFloat(data.items[index].openingAmt) + parseFloat(itm.totalPurchaseAmount) + parseFloat(itm.totalTransferAmount_toStore) - parseFloat(parseFloat(itm.totalTransferAmount) + (parseFloat(itm.totalWastageAmt) + parseFloat(itm.totalSaleAmount) + parseFloat(itm.counterSaleAmount)))).toFixed(2);
            }
          });

        });

      });
    }
    else {
      return res.json(200, data);
    }
  });

};
//Get Stock Summary
exports.getStockSummary = function (req, res) {
  //console.log("Hello Ajit");
  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReports/getStockSummary?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var itemId = "5576b675e414d90c03d1e33a";
    //var  beforeDate=req.query.fromDate;
    var startDate = "6/10/2015 8:44:21 AM";
    var endDate = "7/6/2015 8:44:21 AM";
    var deployment_id = "556eadf102fad90807111eac";
    var tenant_id = "556e9b0302fad90807111e34";
    getOpeningStockForConsumption(startDate, endDate, deployment_id, tenant_id, function (op) {
      //get Last physical Stock
      checkPhysicalStockBetweenTwoDates(op.created, startDate, itemId, deployment_id, tenant_id, function (phy) {
        if (phy == null) {
          //console.log("Phy Null");
          //Calculate between opening stock date to start date
        }
        else {
          //Calculate between physical stock date to start date
          //console.log(phy);
        }
        callback("hi");
      });
      //Get all items data with last physical stock present for item
      //if no physical stock defined then all are zeros
      //Get all datas between physical stock and end date with calculated sum

      //If physical stock not defined then calculate from opening stock date to end date.
    });

    getStockEntry_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, function (result_entry) {
      //console.log(result_entry);
      getStockSale_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, function (result_sale) {
        _.forEach(result_sale, function (s, i) {
          var index = _.findIndex(result_entry, {"itemId": s.itemId});
          if (index < 0) {
            result_entry.push(s);
          }
          else {
            result_entry[index].totalSaleQty = s.totalSaleQty;
            result_entry[index].totalSaleAmount = s.totalSaleAmount;
          }
        });
        res.json(result_entry);
      });
    });
  }
};

exports.getConsumptionSummary_DateWise = function (req, res) {
  var startDate = req.query.fromDate;
  var endDate = req.query.toDate;
  var data = {};
  var items = JSON.parse(req.query.items);
  //console.log(items);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;
  checkOpeningStock(startDate, deployment_id, tenant_id, function (os) {
    if (os != null) {
      getDataBeforeStartDate_DateWise(os.created, startDate, items, deployment_id, tenant_id, function (a) {
        data.items = [];
        _.forEach(a, function (itm, i) {
          var index = _.findIndex(data.items, {itemId: itm.itemId});
          //console.log("indexxxxxxxxxxxx"+index);

          if (index < 0) {
            if (itm.totalWastageQty == undefined || itm.totalWastageAmt == undefined) {
              itm.totalWastageQty = 0;
              itm.totalWastageAmt = 0
            }
            ;
            if (itm.totalSaleQty == undefined || itm.totalSaleAmount == undefined) {
              itm.totalSaleQty = 0;
              itm.totalSaleAmount = 0
            }
            ;
            if (itm.totalPurchaseQty == undefined || itm.totalPurchaseAmount == undefined) {
              itm.totalPurchaseQty = 0;
              itm.totalPurchaseAmount = 0
            }
            ;
            if (itm.totalTransferQty == undefined || itm.totalTransferAmount == undefined) {
              itm.totalTransferQty = 0;
              itm.totalTransferQty = 0
            }
            ;
            if (itm.totalBalanceQty == undefined) {
              itm.totalBalanceQty = 0;
            }
            ;
            itm.openingQty = (itm.totalPurchaseQty - (itm.totalWastageQty + itm.totalSaleQty));
            itm.openingAmt = (itm.totalPurchaseAmount - (itm.totalWastageAmt + itm.totalSaleAmount));
            itm.closingQty = (itm.openingQty);
            itm.closingAmount = (itm.openingAmt);
            itm.openningDate = new Date(startDate);
            itm.closingDate = new Date(endDate);
            itm.totalWastageQty = 0;
            itm.totalWastageAmt = 0;
            itm.totalSaleQty = 0;
            itm.totalSaleAmount = 0;
            itm.totalPurchaseQty = 0;
            itm.totalPurchaseAmount = 0;
            itm.totalTransferQty = 0;
            itm.totalTransferAmount = 0;
            itm.created = new Date(startDate);
            data.items.push(itm);
          }
          else {
            if (itm.totalWastageQty == undefined || itm.totalWastageAmt == undefined) {
              itm.totalWastageQty = 0;
              itm.totalWastageAmt = 0
            }
            ;
            if (itm.totalSaleQty == undefined || itm.totalSaleAmount == undefined) {
              itm.totalSaleQty = 0;
              itm.totalSaleAmount = 0
            }
            ;
            if (itm.totalPurchaseQty == undefined || itm.totalPurchaseAmount == undefined) {
              itm.totalPurchaseQty = 0;
              itm.totalPurchaseAmount = 0
            }
            ;
            if (itm.totalTransferQty == undefined || itm.totalTransferAmount == undefined) {
              itm.totalTransferQty = 0;
              itm.totalTransferQty = 0
            }
            ;
            if (itm.totalBalanceQty == undefined) {
              itm.totalBalanceQty = 0;
            }
            ;
            data.items[index].openingQty += (itm.totalPurchaseQty + itm.totalTransferQty - (itm.totalWastageQty + itm.totalSaleQty));
            data.items[index].openingAmt += (itm.totalPurchaseAmount + itm.totalTransferAmount - (itm.totalWastageAmt + itm.totalSaleAmount));
            // data.items[index].totalBalanceQty+=itm.totalBalanceQty;
            data.items[index].closingQty = data.items[index].openingQty;
            data.items[index].closingAmount = data.items[index].openingAmt;
            data.items[index].totalWastageQty = 0;
            data.items[index].totalWastageAmt = 0;
            data.items[index].totalSaleQty = 0;
            data.items[index].totalSaleAmount = 0;
            data.items[index].totalPurchaseQty = 0;
            data.items[index].totalPurchaseAmount = 0;
            data.items[index].totalTransferQty = 0;
            data.items[index].totalTransferAmount = 0;

            //data.items[index].created=new Date(startDate);
          }
        });
        //data.items=a;
        //console.log(a);
        getDataBetweenStartAndEndDate_DateWise(os.created, startDate, endDate, items, deployment_id, tenant_id, function (b) {

          _.forEach(b, function (itm, i) {
            //   console.log("Purchase:"+itm.totalPurchaseQty);
            // console.log("Wastage:"+itm.totalWastageQty);
            // console.log("itemSaleQty:"+itm.totalSaleQty);
            //   data.items.push(itm);
            // var index=_.findIndex(data.items,{itemId:itm.itemId});
            // // //console.log("betweenStartAndEnd:"+itm.itemId, "index="+index);
            // // //console.log("betweenStartAndEnd:"+itm.itemId, "closingQty="+data.items[i]closingQty);
            //  if(index<0){
            //   data.items.push(itm);
            if (itm.totalWastageQty == undefined) {
              itm.totalWastageQty = 0;
              itm.totalWastageAmt = 0
            }
            ;
            if (itm.totalSaleQty == undefined) {
              itm.totalSaleQty = 0;
              itm.totalSaleAmount = 0
            }
            ;
            if (itm.totalPurchaseQty == undefined) {
              itm.totalPurchaseQty = 0;
              itm.totalPurchaseAmount = 0
            }
            ;
            if (itm.totalTransferQty == undefined) {
              itm.totalTransferQty = 0;
              itm.totalTransferAmount = 0
            }
            ;
            if (itm.openingQty == undefined) {
              itm.openingQty = 0;
              itm.openingAmt = 0
            }
            ;
            if (itm.totalBalanceQty == undefined) {
              itm.totalBalanceQty = 0;
            }
            ;
            itm.closingQty = (itm.totalPurchaseQty + itm.totalTransferQty - (itm.totalWastageQty + itm.totalSaleQty));
            itm.closingAmount = (itm.totalPurchaseAmount + itm.totalTransferAmount - (itm.totalWastageAmt + itm.totalSaleAmount));
            itm.openningDate = new Date(startDate);
            itm.closingDate = new Date(endDate);
            data.items.push(itm);
            //  }
            //  else
            // {
            //   if(itm.totalWastageQty==undefined || itm.totalWastageAmt==undefined){itm.totalWastageQty=0;itm.totalWastageAmt=0};
            //   if(itm.totalSaleQty==undefined || itm.totalSaleAmount==undefined){itm.totalSaleQty=0;itm.totalSaleAmount=0};
            //   if(itm.totalPurchaseQty==undefined || itm.totalPurchaseAmount==undefined){itm.totalPurchaseQty=0;itm.totalPurchaseAmount=0};
            //   if(itm.totalTransferQty==undefined || itm.totalTransferAmount==undefined){itm.totalTransferQty=0;itm.totalTransferAmount=0};
            //   data.items[index].totalWastageQty=itm.totalWastageQty;
            //   data.items[index].totalWastageAmt=itm.totalWastageAmt;
            //   data.items[index].totalPurchaseQty=itm.totalPurchaseQty;
            //   data.items[index].totalPurchaseAmount=itm.totalPurchaseAmount;
            //   data.items[index].totalSaleQty=itm.totalSaleQty;
            //   data.items[index].totalSaleAmount=itm.totalSaleAmount;
            //   data.items[index].totalTransferQty=itm.totalTransferQty;
            //   data.items[index].totalTransferAmount=itm.totalTransferAmount;
            //   data.items[index].closingQty=(data.items[index].openingQty+itm.totalPurchaseQty+itm.totalTransferQty-(itm.totalWastageQty+itm.totalSaleQty));
            //   data.items[index].closingAmount=(data.items[index].openingAmt+itm.totalPurchaseAmount+itm.totalTransferAmount-(itm.totalWastageAmt+itm.totalSaleAmount));
            //   data.items[index].openningDate=new Date(startDate);
            //   data.items[index].closingDate=new Date(endDate);
            //  }
          });
          return res.json(200, data);
        });

      });
    }
    else {
      return res.json(200, data);
    }
  });
};

function getDataBeforeStartDate_DateWise(opendate, startDate, items, deployment_id, tenant_id, callback) {
  var data = [];
  var itemsForLoop = items;
  //console.log(itemsForLoop);
  async.eachSeries(itemsForLoop, function (itm, callback1) {
    //console.log(itm);
    checkPhysicalStockBetweenTwoDates(opendate, startDate, itm.id, deployment_id, tenant_id, function (ph) {
      //console.log('ph:'+ph);
      if (ph != null) {
        var minDate = new Date(ph.created);
        //console.log("ph not null="+minDate);
        getStockEntryByItem_VarianceReport_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            data.push(s);
          });

          getStockSaleByItem_VarianceReport_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              data.push(s);
              // var index=_.findIndex(data,{itemId:s.itemId});
              // if(index<0){
              //   data.push(s);
              // }
              // else
              // {
              //   data[index].totalSaleQty=s.totalSaleQty;
              //   data[index].totalSaleAmount=s.totalSaleAmount;
              // }
            });

            getOpeningStockByItem_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (op_stock) {
              //console.log(op_stock);
              _.forEach(op_stock, function (s, i) {
                data.push(s);
                // var index=_.findIndex(data,{itemId:s.itemId});
                // if(index<0){
                //   data.push(s);
                // }
                // else
                // {
                //   data[index].totalOpeningQty=s.totalOpeningQty;
                // }
              });

              getPhysicalStockByItem_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (bs_stock) {
                //console.log(bs_stock);
                _.forEach(bs_stock, function (s, i) {
                  data.push(s);
                  // var index=_.findIndex(data,{itemId:s.itemId});
                  // if(index<0){
                  //   data.push(s);
                  // }
                  // else
                  // {
                  //   data[index].totalBalanceQty=s.totalBalanceQty;
                  // }
                });

                getWastageStockByItem_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (ws_stock) {
                  _.forEach(ws_stock, function (s, i) {
                    data.push(s);
                    // var index=_.findIndex(data,{itemId:s.itemId});
                    // if(index<0){
                    //   data.push(s);
                    // }
                    // else
                    // {
                    //   data[index].totalWastageQty=s.totalWastageQty;
                    //   data[index].totalWastageAmt=s.totalWastageAmt;
                    // }
                  });
                  getTransferStockByItemFromStore_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (st_from_stock) {
                    //console.log(st_from_stock);
                    _.forEach(st_from_stock, function (s, i) {
                      data.push(s);
                      // var index=_.findIndex(data,{itemId:s.itemId});
                      // if(index<0){
                      //   data.push(s);
                      // }
                      // else
                      // {
                      //   data[index].totalTransferQty=s.totalTransferQty;
                      // }
                    });
                    // getProcessedAndIntermediateReceipeDetails_Sale(startDate,endDate,itemId,deployment_id,tenant_id,storeId,function(sale){
                    //   _.forEach(sale, function(s,i){
                    //     data.push(s);
                    //   });
                    //   callback1();
                    // });
                    callback1();
                  });
                  //callback1();
                });
                //callback1();
              });
              //callback1();
            });
            //callback1();

          });
          //callback1();
        });
      }
      else {
        var minDate = new Date(opendate);
        minDate.setDate(minDate.getDate() - 1);
        //console.log("Monda:"+minDate);
        getStockEntryByItem_VarianceReport_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            data.push(s);
          });

          getStockSaleByItem_VarianceReport_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              data.push(s);
              // var index=_.findIndex(data,{itemId:s.itemId});
              // if(index<0){
              //   data.push(s);
              // }
              // else
              // {
              //   data[index].totalSaleQty=s.totalSaleQty;
              //   data[index].totalSaleAmount=s.totalSaleAmount;
              // }
            });

            getOpeningStockByItem_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (op_stock) {
              //console.log(op_stock);
              _.forEach(op_stock, function (s, i) {
                data.push(s);
                // var index=_.findIndex(data,{itemId:s.itemId});
                // if(index<0){
                //   data.push(s);
                // }
                // else
                // {
                //   data[index].totalOpeningQty=s.totalOpeningQty;
                // }
              });

              getPhysicalStockByItem_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (bs_stock) {
                //console.log(bs_stock);
                _.forEach(bs_stock, function (s, i) {
                  data.push(s);
                  // var index=_.findIndex(data,{itemId:s.itemId});
                  // if(index<0){
                  //   data.push(s);
                  // }
                  // else
                  // {
                  //   data[index].totalBalanceQty=s.totalBalanceQty;
                  // }
                });

                getWastageStockByItem_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (ws_stock) {
                  _.forEach(ws_stock, function (s, i) {
                    data.push(s);
                    // var index=_.findIndex(data,{itemId:s.itemId});
                    // if(index<0){
                    //   data.push(s);
                    // }
                    // else
                    // {
                    //   data[index].totalWastageQty=s.totalWastageQty;
                    //   data[index].totalWastageAmt=s.totalWastageAmt;
                    // }
                  });
                  getTransferStockByItemFromStore_Variance_DateWise(minDate, startDate, itm.id, deployment_id, tenant_id, function (st_from_stock) {
                    //console.log(st_from_stock);
                    _.forEach(st_from_stock, function (s, i) {
                      data.push(s);
                      // var index=_.findIndex(data,{itemId:s.itemId});
                      // if(index<0){
                      //   data.push(s);
                      // }
                      // else
                      // {
                      //   data[index].totalTransferQty=s.totalTransferQty;
                      // }
                    });
                    //console.log(data);
                    callback1();
                  });
                  //callback1();
                });
                //callback1();
              });
              //callback1();
            });
            //callback1();

          });
          //callback1();
        });
      }
    });
  }, function (err) {
    if (err) {
      return handleError(res, err);
    } else {
      //res.json(200, _bills);
      //console.log(data);
      callback(data);
    }
  });
};

function getDataBetweenStartAndEndDate_DateWise(opendate, startDate, endDate, items, deployment_id, tenant_id, callback) {
  var data = [];
  var itemsForLoop = items;
  async.eachSeries(itemsForLoop, function (itm, callback1) {
    //console.log(itm);
    checkPhysicalStockBetweenTwoDates(opendate, startDate, itm.id, deployment_id, tenant_id, function (ph) {
      if (ph != null) {
        var minDate = new Date(ph.created);
        //console.log("ph not null="+minDate);
        getStockEntryByItem_VarianceReport_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            data.push(s);
          });

          getStockSaleByItem_VarianceReport_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              data.push(s);
              // var index=_.findIndex(data,{itemId:s.itemId});
              // if(index<0){
              //   data.push(s);
              // }
              // else
              // {
              //   data[index].totalSaleQty=s.totalSaleQty;
              //   data[index].totalSaleAmount=s.totalSaleAmount;
              // }
            });

            // getOpeningStockByItem_Variance_DateWise(startDate,endDate,itm.id,function(op_stock){
            //   //console.log(op_stock);
            //   _.forEach(op_stock, function(s,i){
            //     data.push(s);
            //     // var index=_.findIndex(data,{itemId:s.itemId});
            //     // if(index<0){
            //     //   data.push(s);
            //     // }
            //     // else
            //     // {
            //     //   data[index].totalOpeningQty=s.totalOpeningQty;
            //     // }
            //   });

            // getPhysicalStockByItem_Variance_DateWise(startDate,endDate,itm.id,function(bs_stock){
            //   //console.log(bs_stock);
            //   _.forEach(bs_stock, function(s,i){
            //     data.push(s);
            //     // var index=_.findIndex(data,{itemId:s.itemId});
            //     // if(index<0){
            //     //   data.push(s);
            //     // }
            //     // else
            //     // {
            //     //   data[index].totalBalanceQty=s.totalBalanceQty;
            //     // }
            //   });

            getWastageStockByItem_Variance_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (ws_stock) {
              _.forEach(ws_stock, function (s, i) {
                data.push(s);
                // var index=_.findIndex(data,{itemId:s.itemId});
                // if(index<0){
                //   data.push(s);
                // }
                // else
                // {
                //   data[index].totalWastageQty=s.totalWastageQty;
                // }
              });
              getTransferStockByItemFromStore_Variance_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (st_from_stock) {
                //console.log(st_from_stock);
                _.forEach(st_from_stock, function (s, i) {
                  data.push(s);
                  // var index=_.findIndex(data,{itemId:s.itemId});
                  // if(index<0){
                  //   data.push(s);
                  // }
                  // else
                  // {
                  //   data[index].totalTransferQty=s.totalTransferQty;
                  // }
                });
                callback1();
              });
              //callback1();
            });
            //callback1();
            //});
            //callback1();
            //});
            //callback1();

          });
          //callback1();
        });
      }
      else {
        var minDate = new Date(opendate);
        minDate.setDate(minDate.getDate() - 1);
        //console.log("ph not defined");
        getStockEntryByItem_VarianceReport_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (st_entry) {
          _.forEach(st_entry, function (s, i) {
            data.push(s);
          });

          getStockSaleByItem_VarianceReport_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (st_sale) {
            _.forEach(st_sale, function (s, i) {
              data.push(s);
              // var index=_.findIndex(data,{itemId:s.itemId});
              // if(index<0){
              //   data.push(s);
              // }
              // else
              // {
              //   data[index].totalSaleQty=s.totalSaleQty;
              //   data[index].totalSaleAmount=s.totalSaleAmount;
              // }
            });

            // getOpeningStockByItem_Variance_DateWise(startDate,endDate,itm.id,function(op_stock){
            //   //console.log(op_stock);
            //   _.forEach(op_stock, function(s,i){
            //     data.push(s);
            //     // var index=_.findIndex(data,{itemId:s.itemId});
            //     // if(index<0){
            //     //   data.push(s);
            //     // }
            //     // else
            //     // {
            //     //   data[index].totalOpeningQty=s.totalOpeningQty;
            //     // }
            //   });

            // getPhysicalStockByItem_Variance_DateWise(startDate,endDate,itm.id,function(bs_stock){
            //   //console.log(bs_stock);
            //   _.forEach(bs_stock, function(s,i){
            //     data.push(s);
            //     // var index=_.findIndex(data,{itemId:s.itemId});
            //     // if(index<0){
            //     //   data.push(s);
            //     // }
            //     // else
            //     // {
            //     //   data[index].totalBalanceQty=s.totalBalanceQty;
            //     // }
            //   });

            getWastageStockByItem_Variance_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (ws_stock) {
              _.forEach(ws_stock, function (s, i) {
                data.push(s);
                // var index=_.findIndex(data,{itemId:s.itemId});
                // if(index<0){
                //   data.push(s);
                // }
                // else
                // {
                //   data[index].totalWastageQty=s.totalWastageQty;
                // }
              });
              getTransferStockByItemFromStore_Variance_DateWise(startDate, endDate, itm.id, deployment_id, tenant_id, function (st_from_stock) {
                //console.log(st_from_stock);
                _.forEach(st_from_stock, function (s, i) {
                  data.push(s);
                  // var index=_.findIndex(data,{itemId:s.itemId});
                  // if(index<0){
                  //   data.push(s);
                  // }
                  // else
                  // {
                  //   data[index].totalTransferQty=s.totalTransferQty;
                  // }
                });
                callback1();
              });
              //callback1();
            });
            //callback1();
            //});
            //callback1();
            //});
            //callback1();

          });
          //callback1();
        });
      }
    });
  }, function (err) {
    if (err) {
      return handleError(res, err);
    } else {
      //res.json(200, _bills);
      //console.log(data);
      callback(data);
    }
  });
};

function getStockEntryByItem_VarianceReport_DateWise(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  //console.log(itemId,startDate,new Date(endDate));
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.vendor.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          qty: "$units.baseQty",
          UnitName: "$units.unitName",
          date: "$created",
          billNo: "$billNo"
        },
        totalAmt: {$sum: "$units.subTotal"},
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalPurchaseQty:"$totalQty",
        totalPurchaseQty: "$_id.qty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        billNo: "$_id.billNo",
        created: "$_id.date"
      }
    }

  ], function (err, result_entry) {
    if (err) {
      //handleError(res, err);
      callback(err);
    }
    else {
      callback(result_entry);
    }
  });
};

function getStockSaleByItem_VarianceReport_DateWise(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.receiver.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          qty: "$units.baseQty",
          date: "$created",
          billNo: "$billNo"
        },
        totalAmt: {$sum: "$units.totalAmount"},
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalSaleQty:"$totalQty",
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.UnitName",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      handleError(res, err);
    }
    else {
      callback(result_sale);
    }
  });
};

function getOpeningStockByItem_Variance_DateWise(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          unitName: "$units.unitName",
          qty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalBalanceQty:"$totalQty",
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_op) {
    if (err) {
      handleError(res, err);
    }
    else {
      callback(result_op);
    }
  });
};

function getPhysicalStockByItem_Variance_DateWise(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          qty: "$units.baseQty",
          date: "$created",
          billNo: "$billNo",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalBalanceQty:"$totalQty",
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_op) {
    if (err) {
      handleError(res, err);
    }
    else {
      callback(result_op);
    }
  });
};

function getWastageStockByItem_Variance_DateWise(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          unitName: "$units.unitName",
          qty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalWastageQty:"$totalQty",
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      //handleError(res, err);
      callback(err);
    }
    else {
      //console.log(result_ws);
      callback(result_ws);
    }
  });
};
function getTransferStockByItemFromStore_Variance_DateWise(startDate, endDate, itemId, deployment_id, tenant_id, callback) {
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items._id": itemId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items._id": itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          qty: "$units.baseQty",
          date: "$created",
          billNo: "$billNo"
        },
        totalQty: {$sum: "$items.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        //totalTransferQty:"$totalQty",
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.UnitName",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      //console.log(err);
      handleError(res, err);
    }
    else {
      callback(result_ws);
    }
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
