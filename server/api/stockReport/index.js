'use strict';

var express = require('express');
var controller = require('./stockReport.controller');

var router = express.Router();

//router.get('/', controller.index);
router.get('/getRequirementData', controller.getRequirementData);
router.get('/getStockSummary', controller.getStockSummary);
router.get('/getConsumptionSummary', controller.getConsumptionSummary);
router.get('/getWastageReport', controller.getWastageReport);
router.get('/lastPrice', controller.lastPrice);
router.get('/getConsumptionSummary_ItemReport', controller.getConsumptionSummary_ItemReport);
router.get('/getConsumptionSummary_DateWise', controller.getConsumptionSummary_DateWise);
router.get('/RawMaterialPricing_Receipe', controller.RawMaterialPricing_Receipe);
router.get('/RawMaterialPricing_ReceipeBasekitchen', controller.RawMaterialPricing_ReceipeBasekitchen);
router.get('/RawMaterialPricing_ReceipeConsumptionReport', controller.RawMaterialPricing_ReceipeConsumptionReport);
router.get('/RawMaterialPricing_ReceipeBasekitchenConsumptionReport', controller.RawMaterialPricing_ReceipeBasekitchenConsumptionReport);

//router.get('/getItemClosingBalance', controller.getClosingBalanceOfItems);
//router.get('/:id', controller.show);
//router.post('/', controller.create);
//router.put('/:id', controller.update);
//router.patch('/:id', controller.update);
//router.delete('/:id', controller.destroy);

module.exports = router;