/**
 * Broadcast updates to client when the model changes
 */

'use strict';

//var StockReport = require('./stockReport.model');
var Stock = require('./stockReport.model');

exports.register = function(socket) {
  Stock.StockReport.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Stock.StockReport.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
  // StockReport.schema.post('save', function (doc) {
  //   onSave(socket, doc);
  // });
  // StockReport.schema.post('remove', function (doc) {
  //   onRemove(socket, doc);
  // });
}

function onSave(socket, doc, cb) {
  socket.emit('stockReport:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockReport:remove', doc);
}