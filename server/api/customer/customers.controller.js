'use strict';

angular.module('posistApp')
  .controller('CustomersCtrl', function ($scope,Customer,Utils,localStorageService,$q,$timeout,growl,$ngBootbox,$modal, Item) {
    var pageNo = 0;
    var billRecords = true;
    var totalBillCustomers = 0;
    var totalNonBillCustomers = 0;
        if ($scope.currentUser.role != 'superadmin') {
          /*  if (!Utils.hasUserPermission($scope.currentUser.selectedPermissions, 'Reports')) {
                growl.error('00ps !! have permission ?', {ttl: 3000});
                $location.path('/login');
            }*/

        }
        $scope.csv = {
        content: null,
        header: true,
        separator: ',',
        result: null
      };

      $scope.pCount=0;
      $scope.tCount=0;
      $scope.loading=true;
      var params = {
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id')
      };
      $scope.categories = [{id:1,name:'All'}];
      $scope.items = [];
      $scope.selectedItems = [{id:1,name:'All'}];
      Item.get(params,function(result){
        $scope.items = angular.copy(result);
        _.forEach(result, function(item){
          if($scope.categories.length>0)
            var temp = _.find($scope.categories, {id : item.category._id});
          if(!temp){
            $scope.categories.push({id:item.category._id,name:item.category.categoryName})
          }
        })
      })

    $scope.addSelected = function(categories){
      $scope.selectedItems = [{id:1,name:'All'}];
      if(categories.length==1 && categories[0]==1){
        _.forEach($scope.items, function(item){
          $scope.selectedItems.push({id:item._id,name:item.name});
        })
      }
      else{
        _.forEach(categories, function(category){
          var items = _.filter($scope.items, function(item){
            return item.category._id === category;
          });
          _.forEach(items, function(item){
            $scope.selectedItems.push({id:item._id,name:item.name});
          })
        })
      }
      console.log($scope.selectedItems)
    } 
    $scope.addSelectedItems = function(items){
      $scope.itemsFilter = [];
      _.forEach(items, function(item){
          $scope.itemsFilter.push(item);
      })
      console.log(items, $scope.itemsFilter)
    } 



    $scope.onFinishImport=function(data){
      //console.log(data);
      if((localStorageService.get('tenant_id')==null||localStorageService.get('deployment_id')==null)){
        growl.error('Deployment not selected',{ttl:3000});
        return false;
      }
      $scope.loading=false;
      $scope.pCount=0;
      $scope.tCount=0;
      if(data.length>0){
        if(!(_.has(data[0],'name')&&_.has(data[0],'mobile')&&_.has(data[0],'flat')&&_.has(data[0],'street')&&_.has(data[0],'city')&& _.has(data[0],'state'))){
          growl.error('File format may not compatible',{ttl:3000});
          $scope.loading=true;
          return false;
        }
      }
      $scope.customers=data;
     /* for(var i= 0;i<10;i++){
        $scope.customers.push(data[i]);
      }*/

      var _customerAll=[];

      _.forEach($scope.customers,function(cust){
        var customerObject={customerId:Utils.guid(),addresses:[], firstname:cust.name,mobile:cust.mobile,addType:'Residence',phone:'',address1:cust.flat,address2:cust.street,city:cust.city,state:cust.state,postCode:'',DOB:null,MA:null,email:'',tenant_id:'',deployment_id:''};
        if(_.has(cust,'phone')){
          customerObject.phone=cust.phone;
        }else{
          //delete customerObject.phone;
        }
        if(_.has(cust,'postcode')){
          customerObject.postCode=cust.postcode;
        }else{
          //delete customerObject.postCode;
        }
        if(_.has(cust,'dob')){
         var dob= Utils.convertDate(cust.dob);
          customerObject.DOB=dob;
        }else{
         // delete customerObject.DOB;
        }
        if(_.has(cust,'ma')){
          var ma= Utils.convertDate(cust.ma);
          customerObject.MA=ma;
        }else{
          //delete customerObject.MA;
        }
        if(_.has(cust,'email')){
          customerObject.email=cust.email;
        }else{
          //delete customerObject.email;
        }
        customerObject.tenant_id=localStorageService.get('tenant_id');
        customerObject.deployment_id=localStorageService.get('deployment_id');
       // console.log(customerObject);
       // _customerAll.push(Customer.createWithCheck(JSON.stringify( customerObject)).$promise);
        _customerAll.push(JSON.stringify( customerObject));
      })
      $scope.tCount=_customerAll.length;
      autoCall(_customerAll,0);
       /*  $q.all(_customerAll).then(function(res){
        console.log(res);
      })*/

    }
      function autoCall(custs,count){
        Customer.createWithCheck(( custs[count])).$promise.then(function(){
          if(count<custs.length-1){
            count+=1;
            $timeout(function(){autoCall(custs,count)},5);
            $scope.pCount=count;
          }else
          {
            $scope.loading=true;
            console.log('done:'+count);
            growl.success('uploading customer data done successfully',{ttl:2000});
          }
        }).catch(function(err){
          $scope.loading=true;
          growl.error('Some problem in uploading data.',{ttl:2000});
        })

      }
        //////////////////////////////CRM////////////////////////////////////////////////
        $scope.selectedDate={
            from: "",//Utils.getDateFormattedDate(new Date()),
            to:""//Utils.getDateFormattedDate(new Date())
        }
        $scope.customerFilter={
            mobile:"",
            name:"",
            amountType:"=",
            frequencyType:"="
        }
        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.openTo = function($event) {
            console.log($scope.openedTo);
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedToo = true;
        };
        $scope.openDOB = function($event) {
            console.log($scope.openedTo);
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedTo = true;
        };
        $scope.openMA = function($event) {
            console.log($scope.openedTo);
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedT = true;
        };
        $scope.customersBillData=[];
        $scope.searchCustomer=function(){
            $scope.customersBillData=[];
            var filter={};
            var query={};
            console.log($scope.selectedDate);
            console.log($scope.frequencyValue)
            if(!($scope.selectedDate.from==""||$scope.selectedDate.from==null)){
                if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)){
                   // if(Utils.convertDate($scope.selectedDate.from) && Utils.convertDate($scope.selectedDate.to)){
                        filter.includeDate=true;
                    /*}else {
                        $ngBootbox.alert("Error in date format.");
                        return false;
                    }*/
                }else{
                    $ngBootbox.alert("Feed in to date.");
                    return false;
                }
            }else{
                if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)) {
                    $ngBootbox.alert("Feed in start date.");
                    return false;
                }

                filter.includeDate=false;
            }

            if(filter.includeDate){
               if(Utils.getDateFormattedDate($scope.selectedDate.from)>Utils.getDateFormattedDate($scope.selectedDate.to)){
                   $ngBootbox.alert("Hey!!  Is from date greater ?");
                   return false;
               }
                query.startDate= Utils.getResetDate(localStorageService.get('settings'), $scope.selectedDate.from);
                query.endDate=Utils.getResetDate(localStorageService.get('settings'), $scope.selectedDate.to);
                query.endDate.setDate(query.endDate.getDate()+1);
            }
            if($scope.customerFilter.mobile!=""){
                query.mobile=$scope.customerFilter.mobile;
            }
            if($scope.customerFilter.name!=""){
                query.name=$scope.customerFilter.name;
            }
            if($scope.customerFilter.address!=""){
                query.address=$scope.customerFilter.address;
            }
            if($scope.customerFilter.dob){
                query.dob=$scope.customerFilter.dob;
            }
            if($scope.customerFilter.anniversary){
                query.anniversary=$scope.customerFilter.anniversary;
            }
            if($scope.customerFilter.amountValue){
                query.amountValue=parseInt($scope.customerFilter.amountValue);
                query.amountType=$scope.customerFilter.amountType;
            }
            if($scope.customerFilter.frequencyValue){
                query.frequencyValue=parseInt($scope.customerFilter.frequencyValue);
                query.frequencyType=$scope.customerFilter.frequencyType;
            }
            if($scope.customerFilter.total){
                query.total=$scope.customerFilter.total;
            }
            if($scope.itemsFilter && $scope.itemsFilter.length>0){
              query.itemsFilter = [];
              _.forEach($scope.itemsFilter, function(temp){
                if(temp != 1)
                  query.itemsFilter.push(temp);
              })
            }
            else if($scope.categoryFilter){
              query.categoryFilter = [];
              _.forEach($scope.categoryFilter, function(temp){
                if(temp != 1)
                  query.categoryFilter.push(temp);
              })
            }
            query.deployment_id=localStorageService.get('deployment_id');
            query.skip=0;
            if($scope.customerFilter.total && $scope.customerFilter.total<15)
              query.limit = parseInt($scope.customerFilter.total);
            else
              query.limit = 15;
            pageNo=0;
            console.log(query,$scope.customerFilter.total);
            $scope.busy=true;
            billRecords=true;
            Customer.getUniqueCustomer(query,function(customers){
              console.log("length=",customers)
              //$scope.returnLength = customers.length;
              $scope.busy=false;
              if(customers.length>0){
                totalBillCustomers = customers[0].count;
                totalNonBillCustomers = customers[0].customerCount;
              }  
              var customerObject =  customers//_.sortBy( customers,['totalAmount']).reverse();
              for(var i=0;i<customerObject.length;i++){
                if(customerObject[i].customer)
                $scope.customersBillData.push(customerObject[i]);
              }
              if(billRecords && (totalBillCustomers <= 15*(pageNo+1)) && totalNonBillCustomers>0){
                  pageNo=0;
                  billRecords=false;
              } 
              else if(billRecords && (totalBillCustomers <= 15*(pageNo+1)) && totalNonBillCustomers==0){
                $scope.busy = true;
              }
              if($scope.customerFilter.total && ($scope.customersBillData.length>=parseInt($scope.customerFilter.total))){
                $scope.busy = true;
              }
              console.log($scope.customerFilter.total,$scope.customersBillData.length,$scope.busy)
            })
        }
        $scope.nextPage = function(){
          console.log("tanvvvvveee")
          if($scope.busy)
            return;
          $scope.busy=true;
          var filter={};
            var query={};
            console.log($scope.selectedDate);
            if(!($scope.selectedDate.from==""||$scope.selectedDate.from==null)){
                if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)){
                   // if(Utils.convertDate($scope.selectedDate.from) && Utils.convertDate($scope.selectedDate.to)){
                        filter.includeDate=true;
                    /*}else {
                        $ngBootbox.alert("Error in date format.");
                        return false;
                    }*/
                }else{
                    $ngBootbox.alert("Feed in to date.");
                    return false;
                }
            }else{
                if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)) {
                    $ngBootbox.alert("Feed in start date.");
                    return false;
                }

                filter.includeDate=false;
            }

            if(filter.includeDate){
               if(Utils.getDateFormattedDate($scope.selectedDate.from)>Utils.getDateFormattedDate($scope.selectedDate.to)){
                   $ngBootbox.alert("Hey!!  Is from date greater ?");
                   return false;
               }
                query.startDate= Utils.getResetDate(localStorageService.get('settings'), $scope.selectedDate.from);
                query.endDate=Utils.getResetDate(localStorageService.get('settings'), $scope.selectedDate.to);
                query.endDate.setDate(query.endDate.getDate()+1);
            }
            if($scope.customerFilter.mobile!=""){
                query.mobile=$scope.customerFilter.mobile;
            }
            if($scope.customerFilter.name!=""){
                query.name=$scope.customerFilter.name;
            }
            if($scope.customerFilter.address!=""){
                query.address=$scope.customerFilter.address;
            }
            if($scope.customerFilter.dob){
                query.dob=$scope.customerFilter.dob;
            }
            if($scope.customerFilter.anniversary){
                query.anniversary=$scope.customerFilter.anniversary;
            }
            if($scope.customerFilter.amountValue){
                query.amountValue=parseInt($scope.customerFilter.amountValue);
                query.amountType=$scope.customerFilter.amountType;
            }
            if($scope.customerFilter.frequencyValue){
                query.frequencyValue=parseInt($scope.customerFilter.frequencyValue);
                query.frequencyType=$scope.customerFilter.frequencyType;
            }
            if($scope.customerFilter.total){
                query.total=$scope.customerFilter.total;
            }
            if($scope.itemsFilter && $scope.itemsFilter.length>0){
              query.itemsFilter=$scope.itemsFilter;
            }
            else if($scope.categoryFilter){
              query.categoryFilter = [];
              _.forEach($scope.categoryFilter, function(temp){
                if(temp != 1)
                  query.categoryFilter.push(temp);
              })
            }

          //var query = {};
          query.limit = 15;
          query.deployment_id=localStorageService.get('deployment_id');
          if(billRecords)
            query.skip= ++pageNo;
          else
            query.skipCust = pageNo++;
          console.log(parseInt($scope.customerFilter.total/15),pageNo)
          // if($scope.customerFilter.total){
          //   if(billRecords){
          //     if(parseInt($scope.customerFilter.total/15) > pageNo)
          //       query.limit = 15;
          //     else
          //       query.limit =  $scope.customerFilter.total%15;
          //   }
          //   else{
          //     if(parseInt($scope.customerFilter.total/15) > (pageNo-1))
          //       query.limit = 15;
          //     else
          //       query.limit =  $scope.customerFilter.total%15;
          //   }  
          // }
          if($scope.customerFilter.total && ($scope.customerFilter.total - $scope.customersBillData.length)<15){
            query.limit = $scope.customerFilter.total - $scope.customersBillData.length
          }
          console.log(query)
          Customer.getUniqueCustomer(query,function(customers){
            $scope.returnLength = customers.length;
            console.log($scope.returnLength)
            $scope.busy=false;
              if(billRecords && (totalBillCustomers <= 15*(pageNo+1)) && totalNonBillCustomers>0){
                  pageNo=0;
                  billRecords=false;
              } 
              else if(billRecords && (totalBillCustomers <= 15*(pageNo+1)) && totalNonBillCustomers==0){
                $scope.busy = true;
              }
              var customerObject =  customers;

              for(var i=0;i<customerObject.length;i++){
                if(customerObject[i].customer)
                  $scope.customersBillData.push(customerObject[i]);
              }
              console.log($scope.customersBillData.length)
              if($scope.customerFilter.total && ($scope.customersBillData.length>=$scope.customerFilter.total)){
                $scope.busy = true;
              }
              if(customers.length==0)
                $scope.busy = true;
          })
        }

        $scope.deleteCustomer = function(index){
          $scope.customersBillData.splice(index,1)
        }

        $scope.showCustomerDetail=function(customer,index){
            var modalInstance = $modal.open({
                templateUrl: 'app/billing/customer/_newCustomer.html',
                controller: 'CustomerCtrl',
                size: "md",
                resolve: {
                    number: function() {
                        return customer;
                    }

                }
            });
            modalInstance.result.then(function (updatedCustomer) {
              $scope.customersBillData[index].customer = updatedCustomer;
            });
        }
        $scope.customersBill = [];
        var billRecordsExcel = true;
        var pageNoExcel = 0;

        $scope.getAllData = function(divId){
          var filter={};
            var query={};
            console.log($scope.selectedDate);
            if(!($scope.selectedDate.from==""||$scope.selectedDate.from==null)){
                if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)){
                   // if(Utils.convertDate($scope.selectedDate.from) && Utils.convertDate($scope.selectedDate.to)){
                        filter.includeDate=true;
                    /*}else {
                        $ngBootbox.alert("Error in date format.");
                        return false;
                    }*/
                }else{
                    $ngBootbox.alert("Feed in to date.");
                    return false;
                }
            }else{
                if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)) {
                    $ngBootbox.alert("Feed in start date.");
                    return false;
                }

                filter.includeDate=false;
            }

            if(filter.includeDate){
               if(Utils.getDateFormattedDate($scope.selectedDate.from)>Utils.getDateFormattedDate($scope.selectedDate.to)){
                   $ngBootbox.alert("Hey!!  Is from date greater ?");
                   return false;
               }
                query.startDate= Utils.getResetDate(localStorageService.get('settings'), $scope.selectedDate.from);
                query.endDate=Utils.getResetDate(localStorageService.get('settings'), $scope.selectedDate.to);
                query.endDate.setDate(query.endDate.getDate()+1);
            }
            if($scope.customerFilter.mobile!=""){
                query.mobile=$scope.customerFilter.mobile;
            }
            if($scope.customerFilter.name!=""){
                query.name=$scope.customerFilter.name;
            }
            if($scope.customerFilter.address!=""){
                query.address=$scope.customerFilter.address;
            }
            if($scope.customerFilter.dob){
                query.dob=$scope.customerFilter.dob;
            }
            if($scope.customerFilter.anniversary){
                query.anniversary=$scope.customerFilter.anniversary;
            }
            if($scope.customerFilter.amountValue){
                query.amountValue=parseInt($scope.customerFilter.amountValue);
                query.amountType=$scope.customerFilter.amountType;
            }
            if($scope.customerFilter.frequencyValue){
                query.frequencyValue=parseInt($scope.customerFilter.frequencyValue);
                query.frequencyType=$scope.customerFilter.frequencyType;
            }
            if($scope.customerFilter.total){
                query.total=$scope.customerFilter.total;
            }
            if($scope.itemsFilter && $scope.itemsFilter.length>0){
              query.itemsFilter=$scope.itemsFilter;
            }
            else if($scope.categoryFilter){
              query.categoryFilter = [];
              _.forEach($scope.categoryFilter, function(temp){
                if(temp != 1)
                  query.categoryFilter.push(temp);
              })
            }

          //var query = {};
          fetchCust(query).then(function () {
            console.log($scope.customersBill);
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById(divId).innerHTML));
          });
        }
        var totalBillCustomers1=0;
        var totalNonBillCustomers1=0;
        function fetchCust(query) {

          var deferred = $q.defer();
          query.limit = 15;
          query.deployment_id=localStorageService.get('deployment_id');
          if(billRecordsExcel)
            query.skip= pageNoExcel++;
          else
            query.skipCust = pageNoExcel++;
          if($scope.customerFilter.total && ($scope.customerFilter.total - $scope.customersBill.length)<15){
            query.limit = $scope.customerFilter.total - $scope.customersBill.length
          }
          console.log(query)
          Customer.getUniqueCustomer(query,function(customers){
            
                if(customers[0] && customers[0].count){
                  totalBillCustomers1 = customers[0].count;
                  totalNonBillCustomers1 = customers[0].customerCount;
                console.log(totalBillCustomers,pageNoExcel,billRecordsExcel,customers[0].count)}
              
                if(billRecordsExcel && (totalBillCustomers1 <= 15*pageNoExcel) && totalNonBillCustomers1>0){
                    pageNoExcel=0;
                    billRecordsExcel=false;
                }  
                else if(billRecordsExcel && (totalBillCustomers1 <= 15*pageNoExcel) && totalNonBillCustomers1>0){
                  deferred.resolve(customers);
                }
                var customerObject =  customers;
                for(var i=0;i<customerObject.length;i++){
                  if(customerObject[i].customer)
                    $scope.customersBill.push(customerObject[i]);
                }
            if(customers.length>0){
              fetchCust(query).then(function (result) {
                deferred.resolve(customers);
              });
            } else {
              deferred.resolve(customers);
            }
          });
          return deferred.promise;
        }

        $scope.exportToExcel = function(divId){
          $scope.customersBill = [];
          billRecordsExcel = true;
          pageNoExcel = 0;
          if($scope.customersBill.length == 0)
          $scope.getAllData(divId);
          // console.log($scope.customersBill.length)
          // if($scope.customersBill.length > 0)
          //   window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById(divId).innerHTML));
        };
    });
