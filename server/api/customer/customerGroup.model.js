'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CustomerGroupSchema = new Schema({
    tenant:{},
    deployments:[],
    updated: {type: Date}
});

CustomerGroupSchema
    .pre('save', function (next) {
        this.updated = new Date;
        next();
    });
module.exports = mongoose.model('CustomerGroup', CustomerGroupSchema);