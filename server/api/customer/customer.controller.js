'use strict';

var _ = require('lodash');
var Customer = require('./customer.model');
var CustomerGroup = require('./customerGroup.model');
var Bill=require('../bill/bill.model');
var utils=require('../Utils/utils');
var async=require('async');
var moment=require('moment');
var mongoose=require('mongoose');
var request = require('request');
var config = require('../../config/environment');
var Q=require('q');



// Get list of customers
exports.index = function (req, res) {
  if(req.query.tenant_id ==null){
   return res.json(200,{error:'Missing tenant id.'});
  }
  Customer.find({tenant_id: req.query.tenant_id}, function (err, customers) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, customers);
  });
};

exports.getCustomerGroupByTenant=function(req,res){
  CustomerGroup.find({'tenant._id':req.body.tenant_id},function(err,cGroups){
    if(err){
     return handleError(res, err);
     }
     return res.json(200,cGroups);
  })
}

exports.getCustomerGroups=function(req,res){
  CustomerGroup.find({},function(err,cGroups){
    if(err){
     return handleError(res, err);
     }
     return res.json(200,cGroups);
  })
}

// Get list of customers by deployment
exports.getCustomerByDeployment = function (req, res) {
  if(req.query.auth!='55fd6726474dde2422e5da26'){
    return res.json(401,{status:'Auth key is invalid.'});
  }
  Customer.find({deployment_id:new mongoose.Types.ObjectId(req.query.deployment_id)},{firstname:1,mobile:1,phone:1,address1:1,address2:1,city:1,state:1,postCode:1,DOB:1,MA:1,email:1}, function (err, customers) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, customers);
  });
};

// Get a single customer
exports.show = function (req, res) {
  Customer.findById(req.params.id, function (err, customer) {
    if (err) {
      return handleError(res, err);
    }
    if (!customer) {
      return res.send(404);
    }
    return res.json(customer);
  });
};
//get unique customer id
exports.getUniqueCustomer=function(req,res) {
    if(config.isQueue){
request(config.reportServer.url+'/api/customers/getUniqueCustomer?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  var _customers=[];
  var query= {
    query:{
      deployment_id: new mongoose.Types.ObjectId( req.query.deployment_id),
      isDeleted: {$ne: true},
      _customer:{}
    },
    skip:parseInt(req.query.skip),
    skipCust : req.query.skipCust,
    limit : parseInt(req.query.limit)
  }

  if(!req.query.skipCust){
    var custQuery = {};
    if(req.query.startDate!=undefined){
      query.query._created= {
        $gte: moment( new Date(req.query.startDate)).toDate(), $lte: moment( new Date(req.query.endDate)).toDate()
      }
    }
    if(req.query.mobile!=undefined) {
      //  cq={'mobile':req.query.mobile};
      // query.query["_customer.mobile"] = new RegExp(req.query.mobile, 'i');
      custQuery["mobile"] = new RegExp(req.query.mobile, 'i');
    }
    if(req.query.name!=undefined){
      //query.query["_customer.firstname"] = new RegExp(req.query.name, 'i')
      custQuery["firstname"] = new RegExp(req.query.name, 'i')
    }
    if(req.query.address!=undefined){
      custQuery["$or"] = [
        {"address1": new RegExp(req.query.address, 'i')},
        {"address2": new RegExp(req.query.address, 'i')},
        {"city": new RegExp(req.query.address, 'i')},
        {"state": new RegExp(req.query.address, 'i')}
      ]
    }
    if(req.query.dob){
      //query.query["_customer.DOB"]= req.query.dob
      custQuery["DOB"]= req.query.dob
    }
    if(req.query.anniversary){
      // query.query["_customer.MA"]= req.query.anniversary
      custQuery["MA"]= req.query.anniversary
    }
    if(req.query.amountValue){
      //query.amount = {type:req.query.amountType,value:req.query.amountValue}
      query.amount = (req.query.amountType == "=")? {$eq: parseFloat(req.query.amountValue)}: (req.query.amountType == "<")? {$lt: parseFloat(req.query.amountValue)}: (req.query.amountType == ">")? {$gt: parseFloat(req.query.amountValue)}: (req.query.amountType == "<=")? {$lte: parseFloat(req.query.amountValue)}: {$gte: parseFloat(req.query.amountValue)} ;
      //console.log(query.amount);
    }
    if(req.query.frequencyValue){
      query.frequency = (req.query.frequencyType == "=")? {$eq: parseFloat(req.query.frequencyValue)}: (req.query.frequencyType == "<")? {$lt: parseFloat(req.query.frequencyValue)}: (req.query.frequencyType == ">")? {$gt: parseFloat(req.query.frequencyValue)}: (req.query.frequencyType == "<=")? {$lte: parseFloat(req.query.frequencyValue)}: {$gte: parseFloat(req.query.frequencyValue)};
      //console.log(query.frequency,req.query.frequencyValue);
    }
    console.log(custQuery)
    if(!_.isEmpty(custQuery)){
      console.log("custQuery")
      custQuery.deployment_id = req.query.deployment_id;
      Customer.find(custQuery,{customerId:1,_id:0},function(err,customers1){
        //console.log(err,customers1.length)
        var customerIds = [];
        _.forEach(customers1,function(cust){
          customerIds.push(cust.customerId);
        })
        query.query._customer = {};
        query.query['_customer.customerId'] = {$in:customerIds};
        if(req.query.categoryFilter && req.query.categoryFilter.length>0){
          if((typeof req.query.categoryFilter) == "string"){
            query.categoryFilter = [];
            query.categoryFilter.push(req.query.categoryFilter);
          }
          else{
            query.categoryFilter = req.query.categoryFilter;
          }
          query.query['_kots.items.category._id'] = {$in: query.categoryFilter};
        }
        if(req.query.itemsFilter && req.query.itemsFilter.length>0){
          if((typeof req.query.itemsFilter) == "string"){
            query.itemsFilter = [];
            query.itemsFilter.push(req.query.itemsFilter);
          }
          else{
            query.itemsFilter = req.query.itemsFilter;
          }
          query.query['_kots.items._id'] = {$in: query.itemsFilter};
        }
        //console.log("hello",query,"hello")
        utils.getCRMCustomers(query).then(function (billTotal) {
          var billResult = billTotal
          var custIds = [];
          _.forEach(billResult,function(bill){
            custIds.push(bill._id);
          })
          //console.log(billTotal)
          Customer.find({customerId : {$in : custIds}, deployment_id: req.query.deployment_id}, function (err, customers) {

            async.eachSeries(billResult, function (bill, callback) {
              var updatedCust = _.find(customers,{customerId:bill._id})
              if(updatedCust)
                bill.customer = updatedCust;
              callback();
            }, function (err, result) {

                res.json(200, billTotal);
            })
          })
        })
      })
    }
    else{
      query.query._customer = {};
      if(req.query.categoryFilter && req.query.categoryFilter.length>0){
        if((typeof req.query.categoryFilter) == "string"){
          query.categoryFilter = [];
          query.categoryFilter.push(req.query.categoryFilter);
        }
        else{
          query.categoryFilter = req.query.categoryFilter;
        }
        query.query['_kots.items.category._id'] = {$in: query.categoryFilter};
      }
      if(req.query.itemsFilter && req.query.itemsFilter.length>0){
        if((typeof req.query.itemsFilter) == "string"){
          query.itemsFilter = [];
          query.itemsFilter.push(req.query.itemsFilter);
        }
        else{
          query.itemsFilter = req.query.itemsFilter;
        }
        query.query['_kots.items._id'] = {$in: query.itemsFilter};
      }
      console.log("hello",query,"hello")
      utils.getCRMCustomers(query).then(function (billTotal) {
        console.log(billTotal)
        var billResult = billTotal
        var custIds = [];
        _.forEach(billResult,function(bill){
          custIds.push(bill._id);
        })
        //console.log(custIds)
        Customer.find({customerId : {$in : custIds}, deployment_id: req.query.deployment_id}, function (err, customers) {
          async.eachSeries(billResult, function (bill, callback) {
            var updatedCust = _.find(customers,{customerId:bill._id})
            if(updatedCust)
              bill.customer = updatedCust;
            callback();
          }, function (err, result) {
            res.json(200, billTotal);
          })
        })
      })
    }

  }
  else if(((req.query.amountValue>0 && (req.query.amountType=='<' || req.query.amountType=='<=')) || (req.query.amountValue==0 && req.query.amountType=='=') || !req.query.amountValue) && ((req.query.frequencyValue>0 && (req.query.frequencyType=='<' || req.query.frequencyType=='<=')) || (req.query.frequencyValue==0 && req.query.frequencyType=='=') || !req.query.frequencyValue) && !req.query.categoryFilter && !req.query.itemsFilter){
    console.log("customers")
    utils.getUniqueCustomerIds(query.query).then(function(custs) {
      //console.log(custs);
      var cq={};
      if(req.query.mobile!=undefined) {
        //  cq={'mobile':req.query.mobile};
        cq = {'mobile':new RegExp(req.query.mobile, 'i')};
      }
      if(req.query.name!=undefined){
        cq.firstname = new RegExp(req.query.name, 'i')
      }

      if(req.query.address!=undefined){
        cq["$or"] = [
          {"address1": new RegExp(req.query.address, 'i')},
          {"address2": new RegExp(req.query.address, 'i')},
          {"city": new RegExp(req.query.address, 'i')},
          {"state": new RegExp(req.query.address, 'i')}
        ]
      }
      if(req.query.dob!=undefined){
        cq["DOB"]= new Date(req.query.dob)
      }
      if(req.query.anniversary!=undefined){
        cq["MA"]= new Date(req.query.anniversary)
      }
      cq.customerId={$not:{$in: custs}};
      cq.deployment_id=query.query.deployment_id;
      var skipRecords = parseInt(query.skipCust) * 15;
      Customer.find(cq, {}, {limit:req.query.limit, skip:skipRecords}, function (err, customers) {

        var customerArr = customers;
        var _customers = []
        if (err) {
          return handleError(res, err);
        }
        async.eachSeries(customerArr, function (customer, callback) {

          var nCustomer = {customer: customer};
          nCustomer.totalAmount = 0;
          nCustomer.totalItems = 0;
          nCustomer.totalBills = 0;
          nCustomer.avgAmount = 0;
          nCustomer.covers = 0;
          _customers.push(nCustomer);
          callback();
        }, function (err, result) {
          res.json(200, _customers);
        })
      })

    })
  }
  else{
    res.json(200, []);
  }
 }
}


// Creates a new customer in the DB.
exports.create = function (req, res) {
  Customer.create(req.body, function (err, customer) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(201, customer);
  });
};
// Create a new Bill with Angular ID.
exports.createWithCheck = function (req, res) {
  delete req.body._id;
  delete req.body.created;
  delete req.body.__v;
  var _customer = req.body;
  if(_customer.addresses!=undefined) {
    _customer.addresses.forEach(function (add) {
      delete add.$$hashKey;
    });
  }
  _customer.isSynced = true;
  //console.log(req.body);
  //Customer.findOne( $and:[{'tenant_id':req.params.tenant_id,'deployment_Id':req.params.deployment_Id},{$or:[ {'mobile' : req.params.mobile}]}], {}, function (err, customer) {
  Customer.findOne( {'tenant_id':req.body.tenant_id,'deployment_id':req.body.deployment_id,'mobile' : req.body.mobile},  function (err, customer) {
    //console.log(customer);
    if (!customer) {
      //_customer={};
      Customer.create(_customer, function (err, sCustomer) {
        if (err) {
          //console.log(err);
          return handleError(res, err);
        }

        return res.json(201, sCustomer);
      });
    } else {
      var updated = _.extend(customer, _customer);
      _customer={};
      updated.markModified("addresses");
      // console.log(updated);
      updated.save(function (err) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, customer);
      })
    }
  });
};

exports.createCustomerGroupWithCheck = function (req, res) {
  delete req.body._id;
  delete req.body.created;
  delete req.body.__v;
  CustomerGroup.findOne( {'tenant._id':req.body.tenant._id},  function (err, customerG) {

    if (!customerG) {
      CustomerGroup.create(req.body, function (err, sCustomerG) {
        if (err) {
          return handleError(res, err);
        }
          return res.json(201, sCustomerG);
      });
    } else {
      var updated = _.extend(customerG, req.body);
      updated.markModified("deployments");
      updated.save(function (err) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, customerG);
      })
    }
  });
};

// Updates an existing customer in the DB.
exports.update = function (req, res) {
  //console.log(req.params,"hello")
  if (req.body._id) {
    delete req.body._id;
  }
  Customer.findById(req.params.id, function (err, customer) {
    if (err) {
      return handleError(res, err);
    }
    if (!customer) {
      return res.send(404);
    }
    var updated = _.merge(customer, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, customer);
    });
  });
};

// Deletes a customer from the DB.
exports.destroy = function (req, res) {
  Customer.findById(req.params.id, function (err, customer) {
    if (err) {
      return handleError(res, err);
    }
    if (!customer) {
      return res.send(404);
    }
    customer.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};

// Search Customer on Multiple fields
exports.search = function (req, res) {
if(config.isQueue){

  request(config.reportServer.url+'/api/customers/search?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{
    if(req.query.q.length>11){
    return  res.json(404,[]);
    }
  var alphaN=req.query.q.match('^[a-zA-Z0-9_]*$');
 // console.log(alphaN);
  if(alphaN==null){return  res.json(404,[]);}
  var re = new RegExp(req.query.q, 'i');
  //console.log(re);
CustomerGroup.findOne({'tenant._id':req.query.tenant_id,'deployments._id':req.query.deployment_id},function(errt,tenantDeps){
  if(errt){
    return handleError(res, errt);
  }
  var query;
  if(tenantDeps){
   // console.log(tenantDeps);
    var deps=[];
    _.forEach(tenantDeps.deployments,function(dep){
      deps.push(dep._id);
    });
      query={'deployment_id':{$in: deps}};
  }else{
      query={'deployment_id':req.query.deployment_id};
    }
 //console.log(query);
    //Customer.find().or([{ 'mobile': req.query.q }]).or({'firstname':re}).or([{phone:req.query.q}]).and({'deployment_id': req.query.deployment_id}).exec(function(err, users) {
      Customer.find().or([{ 'mobile': req.query.q }]).or({'firstname':re}).or([{phone:req.query.q}]).and(query).exec(function(err, users) {
      if (err) {
      return    handleError(req, err);
      }
     return res.json(200, users);
    });
   });
  }
};

exports.getMarketingCustomers=function(req,res){
  Customer.find({deployment_id:new mongoose.Types.ObjectId(req.query.deployment_id)}, function (err, customers) {
    var query={
     query:{
       deployment_id: new mongoose.Types.ObjectId( req.query.deployment_id),
       isDeleted: {$ne: true},
       _customer:{}
     }
    };
    var condition1 = query.query;
    condition1._customer.$exists = true;
    if(req.query.city){
      if(!condition1['_customer.city'])
        condition1['_customer.city'] = {};
      condition1['_customer.city'].$exists=true;
      condition1['_customer.city']=new RegExp(req.query.city,'i')
    }
    console.log(condition1);
    Bill.aggregate([
        {
          $match:condition1
        },
        {
          $project:{
            _id:1,
            totalAmount:"$aggregation.netRoundedAmount",
            covers : {$cond:['$_covers','$_covers',0]},
            customerId:"$_customer.customerId",
            _created:1
          }
        },
        {
          $sort:{
            _created:1
          }
        },
        {
          $group: {
            _id: "$customerId",
            totalBills:{$sum:1},
            totalAmount: {$sum: "$totalAmount"},
            avgAmount : {$avg: "$totalAmount"},
            covers:{$avg:"$covers"},
            _created:{$last:"$_created"}
          }
        },
        {
          $sort : {totalAmount:-1}
        }
      ],function(err,result){
        var foundInfo;
        var markData=[{customer:{},billData:{}}];
      for(var i in customers ){
        foundInfo=0;
        markData[i]={customer:{}};
        markData[i].customer=customers[i];
       for(var j in result){
         if(customers[i].customerId==result[j]._id){
           foundInfo=1;
           markData[i].billData=result[j];
           break;
         }
       }
        if(foundInfo==0){
          markData[i].billData={totalBills:0,
            totalAmount:0,
            avgAmount :0,
            covers:0};
        }
      }
        res.json(200,markData);
    });

  });

};

exports.getCustomersByCity=function(req,res){
  var query={deployment_id:new mongoose.Types.ObjectId(req.query.deployment_id)};
  if(req.query.city)
  query['city']=new RegExp(req.query.city,'i');
  Customer.find(query, function (err, customers) {
    var query={
      query:{
        deployment_id: new mongoose.Types.ObjectId( req.query.deployment_id),
        isDeleted: {$ne: true},
        _customer:{}
      }
    };
    var condition1 = query.query;
    condition1._customer.$exists = true;
   /* if(req.query.city){
      if(!condition1['_customer.city'])
        condition1['_customer.city'] = {};
      condition1['_customer.city'].$exists=true;
      condition1['_customer.city']=new RegExp(req.query.city,'i')
    }*/

    Bill.aggregate([
      {
        $match:condition1
      },
      {
        $project:{
          _id:1,
          totalAmount:"$aggregation.netRoundedAmount",

          covers : {$cond:['$_covers','$_covers',0]},
          customerId:"$_customer.customerId"
        }
      },
      {
        $group: {
          _id: "$customerId",
          totalBills:{$sum:1},
          totalAmount: {$sum: "$totalAmount"},
          avgAmount : {$avg: "$totalAmount"},
          covers:{$avg:"$covers"}
        }
      },
      {
        $sort : {totalAmount:-1}
      }
    ],function(err,result){
      var foundInfo;
      var markData=[{customer:{},billData:{}}];
      for(var i in customers ){
        foundInfo=0;
        markData[i]={customer:{}};
        markData[i].customer=customers[i];
        for(var j in result){
          if(customers[i].customerId==result[j]._id){
            foundInfo=1;
            markData[i].billData=result[j];
            break;
          }
        }
        if(foundInfo==0){
          markData[i].billData={totalBills:0,
            totalAmount:0,
            avgAmount :0,
            covers:0};
        }
      }
      res.json(200,markData);
    });

  });
}


exports.getNoBillCustomers=function(req,res){
  var query={deployment_id:new mongoose.Types.ObjectId(req.query.deployment_id),_customer:{$exists:true},created:{$gte:new Date(req.query.startDate),$lte:new Date(req.query.endDate)}};
  Bill.find(query,{"_customer.customerId":1},function(err,billedCustomers){
    if(billedCustomers){
      var _customers=[];
      _.forEach(billedCustomers,function(customer){_customers.push(customer._customer.customerId)});
      var distinctCustomers= _.uniq(_customers);
      var query={"customerId": {$nin: distinctCustomers},deployment_id:new mongoose.Types.ObjectId(req.query.deployment_id)};
      if(req.query.city) query['city']=new RegExp(req.query.city,'i');
        Customer.find(query, function (err, customers) {
          if (customers) {
            aggregateMarketingDataForCustomers(req.query.deployment_id,customers).then(function(data){
              res.status(200).send(data);
            },function(err){
              return res.status(200).send({status:'error'});
            })
          }
          else return res.status(500).send({status: 'error'});
        })
    }
    else return res.status(500).send({status:'error'});
  });
}

function aggregateMarketingDataForCustomers(deployment_id,customers){
  var deferred= Q.defer();
  var customerIds=[];
  _.forEach(customers,function(cust){
    customerIds.push(cust.customerId);
  });

  var query={deployment_id:new mongoose.Types.ObjectId(deployment_id),_customer:{$exists:true},"_customer.customerId":{$in:customerIds},isDeleted:{$ne:true}};

  Bill.aggregate([
    {
      $match:query
    },
    {
      $project:{
        _id:1,
        totalAmount:"$aggregation.netRoundedAmount",
        covers : {$cond:['$_covers','$_covers',0]},
        customerId:"$_customer.customerId"
      }
    },
    {
      $group: {
        _id: "$customerId",
        totalBills:{$sum:1},
        totalAmount: {$sum: "$totalAmount"},
        avgAmount : {$avg: "$totalAmount"},
        covers:{$avg:"$covers"}
      }
    },
    {
      $sort : {totalAmount:-1}
    }
  ],function(err,result){
     if(result) {
       var foundInfo;
       var markData = [{customer: {}, billData: {}}];
       for (var i in customers) {
         foundInfo = 0;
         markData[i] = {customer: {}};
         markData[i].customer = customers[i];
         for (var j in result) {
           if (customers[i].customerId == result[j]._id) {
             foundInfo = 1;
             markData[i].billData = result[j];
             break;
           }
         }
         if (foundInfo == 0) {
           markData[i].billData = {
             totalBills: 0,
             totalAmount: 0,
             avgAmount: 0,
             covers: 0
           };
         }
       }
       deferred.resolve(markData);
     }
     else deferred.reject({status:'error'});
  });
  return deferred.promise;
}

function handleError(res, err) {
  return res.send(500, err);
}
