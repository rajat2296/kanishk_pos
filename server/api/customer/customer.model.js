'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CustomerSchema = new Schema({
    firstname: String,
    mobile: String,
    addType:String,
    phone:String,
    address1:String,
    address2:String,
    city:String,
    state:String,
    postCode:String,
    DOB:{type:Date},
    MA:{type:Date},
    localId:String,
    customerId:String,
    email: String,
    addresses: [],
    created: { type: Date, default: Date.now },
    tenant_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    updated: {type: Date},
    fakeCustomer : {type:Boolean, default:false}
});

CustomerSchema
    .pre('save', function (next) {
        this.updated = new Date;
        next();
    });
module.exports = mongoose.model('Customer', CustomerSchema);