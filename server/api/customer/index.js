'use strict';

var express = require('express');
var controller = require('./customer.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/getCustomerByDeployment',controller.getCustomerByDeployment);
router.get('/search', controller.search);
router.get('/getUniqueCustomer',controller.getUniqueCustomer);
router.get('/getMarketingCustomers',controller.getMarketingCustomers);
router.get('/getCustomersByCity',controller.getCustomersByCity);
router.get('/getNoBillCustomers',controller.getNoBillCustomers);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/createWithCheck', controller.createWithCheck);
router.post('/createCustomerGroupWithCheck', auth.isAuthenticated(), controller.createCustomerGroupWithCheck);
router.post('/getCustomerGroupByTenant', auth.isAuthenticated(),controller.getCustomerGroupByTenant);
router.post('/getCustomerGroups', auth.isAuthenticated(),controller.getCustomerGroups);


router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
