/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ExpenseCategories = require('./expenseCategories.model');

exports.register = function(socket) {
  ExpenseCategories.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ExpenseCategories.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('expenseCategories:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('expenseCategories:remove', doc);
}