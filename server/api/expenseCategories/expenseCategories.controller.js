'use strict';

var _ = require('lodash');
var ExpenseCategories = require('./expenseCategories.model');
var mongoose = require('mongoose');

// Get list of expenseCategoriess
exports.index = function(req, res) {
  ExpenseCategories.find({
    tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
      deployment_id: req.query.deployment_id
  }, function (err, expenseCategoriess) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(expenseCategoriess);
  });
};

// Get a single expenseCategories
exports.show = function(req, res) {
  ExpenseCategories.findById(req.params.id, function (err, expenseCategories) {
    if(err) { return handleError(res, err); }
    if(!expenseCategories) { return res.status(404).send('Not Found'); }
    return res.json(expenseCategories);
  });
};

// Creates a new expenseCategories in the DB.
exports.create = function(req, res) {
  ExpenseCategories.create(req.body, function(err, expenseCategories) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(expenseCategories);
  });
};

// Updates an existing expenseCategories in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ExpenseCategories.findById(req.params.id, function (err, expenseCategories) {
    if (err) { return handleError(res, err); }
    if(!expenseCategories) { return res.status(404).send('Not Found'); }
    var updated = _.extend(expenseCategories, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(expenseCategories);
    });
  });
};

// Deletes a expenseCategories from the DB.
exports.destroy = function(req, res) {
  ExpenseCategories.findById(req.params.id, function (err, expenseCategories) {
    if(err) { return handleError(res, err); }
    if(!expenseCategories) { return res.status(404).send('Not Found'); }
    expenseCategories.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}