'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ExpenseCategoriesSchema = new Schema({
  tenant_id: Schema.Types.ObjectId,
  deployment_id: String,
  name: String,
  description: String
});

ExpenseCategoriesSchema
    .path('name')
    .validate(function (value, respond) {
        var self = this;
        if (this.isNew) {
            this.constructor.findOne({name:value, deployment_id:this.deployment_id}, function (err, c) {

                if (err) throw err;
                if (c) {
                    if (self.name === c.name) return respond(false);
                    return respond(false);
                }
                respond(true);
            });
        } else {
            respond(true);
        }


    }, 'Duplicate Category Name, please use another name.');

module.exports = mongoose.model('ExpenseCategories', ExpenseCategoriesSchema);