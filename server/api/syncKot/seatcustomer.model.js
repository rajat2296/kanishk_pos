/**
 * Created by HP on 13-06-2016.
 */
'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var SeatcustomerSchema = new Schema({
  merchant_id:String,
  tableNumber:String,
  tableData:{}
});

module.exports = mongoose.model('Seatcustomer', SeatcustomerSchema);
