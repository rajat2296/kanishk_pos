'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SyncAdvanceBookingsSchema = new Schema({
    AdvanceBillId:{ type:Schema.Types.ObjectId,
        required: true,
        unique: true},
    TimeStamp:{type:Date},
    AdvanceBillsData: String,
    OrderTime:{type:Date},
    TabId:String,
    DownLoadStatus:String,
    deployment_id:String
});

module.exports = mongoose.model('SyncAdvanceBookings', SyncAdvanceBookingsSchema);