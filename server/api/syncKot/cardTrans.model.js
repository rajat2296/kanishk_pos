/**
 * Created by Ranjeet Sinha on 6/27/2016.
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CardTransSchema = new Schema({
    cardId:String,
    form:{},

    timeStamp:{type:Date,default: Date.now },
    user:{},
    closeTime:{type:Date}
});
CardTransSchema.set('versionKey', false);
module.exports = mongoose.model('CardTrans', CardTransSchema);