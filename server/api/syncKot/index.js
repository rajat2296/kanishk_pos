'use strict';

var express = require('express');
var controller = require('./syncKot.controller');
var router = express.Router();

router.get('/', controller.index);
router.get('/getByBillId', controller.getByBillId);
router.get('/getByBillIdOpen', controller.getByBillIdOpen);
router.get('/getBillsByDepTab', controller.getBillsByDepTab);
router.get('/getOnlineBills', controller.getOnlineBills);
router.get('/getKotNumber', controller.getKotNumber);
router.get('/getDaySerialNumber', controller.getDaySerialNumber);
router.get('/getBillLockData', controller.getBillLockData);
router.get('/syncBill', controller.syncBill);
router.get('/updateLockBill', controller.updateLockBill);
router.get('/getUnsettledSale',controller.getUnsettledSale);
router.post('/', controller.create);
router.post('/billUpdate', controller.billUpdate);
router.post('/createOnlineBill', controller.createOnlineBill);
router.post('/updateOnlineBill', controller.updateOnlineBill);
router.post('/createAdvanceBill', controller.createAdvanceBill);

/*card transactions start*/
router.post('/createCardTransactions', controller.createCardTransactions);
router.post('/updateCardTransactions', controller.updateCardTransactions);
router.post('/refundCardTransactions', controller.refundCardTransactions);
router.post('/removeCardTransactions', controller.removeCardTransactions);
router.post('/closeOpenedCard', controller.closeOpenedCard);

router.post('/transferCardTransaction', controller.transferCardTransaction);
router.post('/checkCardIfExist', controller.checkCardIfExist);
router.post('/checkMobileIfExist', controller.checkMobileIfExist);
router.post('/getCardByCardId', controller.getCardByCardId);
router.post('/getTransactionsForReport', controller.getTransactionsForReport);
router.post('/getUnsettledAmountForReport', controller.getUnsettledAmountForReport);
router.post('/getUnsettledCardAmount', controller.getUnsettledCardAmount);
router.post('/CardTransactionsCountByBill', controller.CardTransactionsCountByBill);
router.post('/cardTransactionsAmountByBillId', controller.cardTransactionsAmountByBillId);
//router.get('/syncCardTransaction', controller.syncCardTransaction);
/*card transactions ends*/

//--ADDED BY RAJAT FOR INRESTO---//
router.get('/seatcustomer',controller.seatcustomer_index);   // find pending data
router.get('/seatcustomer/delete', controller.seatcustomer_destroy);  // delete updated tableBookings
router.post('/seatcustomer/updatebillstatus',controller.seatcustomer_updateBillStatus);
router.post('/seatcustomer/updatetablestatus',controller.seatcustomer_updateTableStatus);
router.post('/seatcustomer/savebookingfrompos',controller.seatcustomer_saveBookingFromPos);
//--ADDED BY RAJAT FOR INRESTO END--//

router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/destroyBill', controller.destroyBill);
router.get('/getBillsForKds',controller.getBillsForKds);           //ADDED BY RAJAT FOR KDS
router.post('/updateBillItemFromKds',controller.updateBillItemFromKds)  //ADDEd BY RAJAT FOR KDS
router.post('/updateBillAfterPaidTransaction',controller.updateBillAfterPaidTransaction);//Added by Rajat

module.exports = router;
