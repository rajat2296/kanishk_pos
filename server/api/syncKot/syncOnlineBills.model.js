'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SyncOnlineBillsSchema = new Schema({
    OnlineBillId: { type:Schema.Types.ObjectId,
    required: true,
    unique: true},
    OnlineBillNumber: String,
    TimeStamp:{type:Date},
    OnlineBillsData: String,
    OrderTime:{type:Date},
    TabId:String,
    DownloadStatus:String,
    deployment_id:String
});

module.exports = mongoose.model('SyncOnlineBills', SyncOnlineBillsSchema);