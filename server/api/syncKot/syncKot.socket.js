/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var SyncKot = require('./syncKot.model');
var Common = require('./common');
var mongoose = require('mongoose');
var async = require('async');
var Q = require('q');
var moment=require('moment');
//
exports.register = function(socket) {
  SyncKot.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  SyncKot.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
 // console.log(doc);
  //console.log('syncKot_save_'+doc.deployment_id);
  socket.broadcast.emit('syncKot_save_'+doc.deployment_id, doc);
  var currentdate=new Date();
  var resettime=2;
  var startDate = new Date();
  startDate = new Date( startDate.setHours(resettime, 0, 0, 0));
  if (currentdate.getHours() < resettime) {
    startDate = new Date(startDate.setDate(startDate.getDate()-1));
  }
  getDayUnsettledSale({deployment_id:doc.deployment_id,startDate:startDate},socket);
}

function onRemove(socket, doc, cb) {
  //socket.emit('syncKot:remove', doc);
  var currentdate=new Date();
  var resettime=2;
  var startDate = new Date();
  startDate = new Date( startDate.setHours(resettime, 0, 0, 0));
  if (currentdate.getHours() < resettime) {
    startDate = new Date(startDate.setDate(startDate.getDate()-1));
  }
  getDayUnsettledSale({deployment_id:doc.deployment_id,startDate:startDate},socket);
}


function getDayUnsettledSale(query,socket){
  Common.getDayUnsettledSale(query).then(function(result){
    //console.log(result);
    socket.broadcast.emit('syncKot_daySale_'+query.deployment_id, result);
  })
}
