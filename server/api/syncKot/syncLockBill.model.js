'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SyncLockBillSchema = new Schema({
   tables:[],
    nonTables:[],
    deployment_id:{ type:Schema.Types.ObjectId,
    required: true,
    unique: true}
});
SyncLockBillSchema.set('versionKey', false);
module.exports = mongoose.model('SyncLockBill', SyncLockBillSchema);