'use strict';

'use strict';

var Seatcustomer = require('./seatcustomer.model');

exports.register = function(socket) {
  Seatcustomer.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Seatcustomer.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.broadcast.emit('inresto'+doc.merchant_id, doc);
}

function onRemove(socket, doc, cb) {
  console.log("removed");
}
