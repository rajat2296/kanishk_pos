/**
 * Created by Ranjeet Sinha on 3/16/2016.
 */
var SyncKot = require('./syncKot.model');
var mongoose = require('mongoose');
var async = require('async');
var Q = require('q');
var moment=require('moment');

module.exports= {
    getDaySale:function(query){
        var d = Q.defer();
        SyncKot.aggregate([
            {
                $match: {
                    created: {
                        $gte: moment(query.startDate).toDate()
                        // ,$lte: moment(req.query.endDate).toDate()
                    },
                    deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
                    closeTime:{$ne:null},
                    isVoid: {$ne: true}
                }
            },
            {
                $group: {
                    _id: 0,
                    // amount: {$sum: {$add:[ "$totalBill","$totalTax"]}}
                    amount: {$sum: {$add: "$totalBill"}}
                }
            }], function (err, result) {
            if (err) {
                console.log(err);
                //return handleError(res, err);
                d.reject(err);
            }
            console.log(result);
            //if(!syncKot) { return res.send(404); }
            if (result.length > 0) {
                d.resolve(result[0].amount);
            } else {
                d.resolve(0);
            }
        })
        return d.promise;
    },
    getUnsettledSale:function(query){
        console.log(query);
        var d = Q.defer();
        SyncKot.aggregate([
            {
                $match: {
                    deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
                    closeTime:null,
                    isVoid: {$ne: true}
                }
            },
            {
                $group: {
                    _id: 0,
                    // amount: {$sum: {$add:[ "$totalBill","$totalTax"]}}
                    amount: {$sum: {$add: "$totalBill"}}
                }
            }], function (err, result) {
            if (err) {
                //return handleError(res, err);
                d.reject(err);
            }
            //if(!syncKot) { return res.send(404); }
            if (result.length > 0) {
                d.resolve(result[0].amount);
            } else {
                d.resolve(0);
            }
        })
        return d.promise;
    },
    getDayUnsettledSale: function (query) {
        var d =Q.defer();
        var self=this;
        this.getDaySale({deployment_id:query.deployment_id,startDate:query.startDate.toString()}).then(function(daySale){
            self.getUnsettledSale({deployment_id:query.deployment_id,startDate:query.startDate}).then(function(unsettled) {
                var daySaleAmount={'daySale': daySale,'unsettle':unsettled};
                d.resolve(daySaleAmount);
               // console.log(daySaleAmount);
                // socket.broadcast.emit('syncKot_daySale_'+query.deployment_id, daySaleAmount);
            });
        });
        return d.promise;
    }
}

