'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var SyncKotSchema = new Schema({
  id: String,
  serialNumber: Number,
  daySerialNumber: Number,
  billNumber:Number,
  splitNumber:Number,
  created:{type:Date},
  billId:String,
  billData:{},
  isVoid:Boolean,
  isSettled:Boolean,
  isPrinted:Boolean,
  tab:String,
  tabId:String,
  totalBill:Number,
  totalTax:Number,
  deployment_id:Schema.Types.ObjectId,
  tenant_id:Schema.Types.ObjectId,
  isSynced:Boolean,
  syncedOn:{type:Date},
  customer:String,
  customer_address:String,
  deliveryTime:{type:Date},
  closeTime:{type:Date},
  kotNumber:Number,
  instance_id:String,
  instance:{},
  prefix:String,
  lastModified:String,
  tableNumber:String
});


//SyncKotSchema.index({created:-1,billId:1,tabId:1,deployment_id:1,instance_id:1});
module.exports = mongoose.model('SyncKot', SyncKotSchema);
