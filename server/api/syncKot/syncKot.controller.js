'use strict';

var _ = require('lodash');
var SyncKot = require('./syncKot.model');
var AdvanceBill = require('./syncAdvanceBookings.model');
var OnlineBill = require('./syncOnlineBills.model');
var syncLock = require('./syncLockBill.model');
var CardTransactions = require('./cardTransactions.model');
var moment = require('moment');
var mongoose = require('mongoose');
var async = require('async');
var Q = require('q');
var http = require('http');
var request = require('request');
var Common = require('./common');
var io = null;
var Seatcustomer=require('./seatcustomer.model');
// Get list of syncKots
exports.index = function (req, res) {
    SyncKot.find(function (err, syncKots) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, syncKots);
    });
};
// Get list of syncKots
exports.getBillsByDepTab = function (req, res) {
    //console.log(req.query);
    //console.log(cachedData);
    SyncKot.find({
        deployment_id: req.query.deployment_id,
        closeTime: null,
        tabId: req.query.tabId
    }, {'billData._kots':0,'billData._kotsTaxes':0,'billData._currentUser':0,'billData._offers':0,'billData.payments':0,'billData._otherCards':0,'billData._creditCards':0,'billData._debitCards':0,'billData._taxes':0,'billData._items':0,'billData.printItems':0,'billData.printTaxes':0,'billData.rePrint':0,'billData._billNumber':0,'billData._cash':0,'billData.offerItemValidation':0}, {sort: {billNumber: 1, splitNumber: 1}}, function (err, syncKots) {
        if (err) {
            return handleError(res, err);
        }
       /* _.forEach(syncKots, function (kot) {
            try {
                //kot.billData = JSON.stringify(kot.billData);
                *//* if(typeof(kot.billData)=='object'){
                 kot.billData = JSON.stringify(kot.billData);
                 }*//*
            } catch (ex) {
                console.log(ex);
            }
        })*/
        return res.json(200, syncKots);
    });
};

// Get a single syncKot
exports.getByBillId = function (req, res) {
    SyncKot.findOne({billId: req.query.billId}, function (err, syncKot) {
        if (err) {
            return handleError(res, err);
        }
        if (!syncKot) {
            return res.send(404);
        }
        /* if(typeof(syncKot.billData)=='object'){
         syncKot.billData=JSON.stringify( syncKot.billData);
         }*/

        return res.json(syncKot);
    });
};
// Get a single syncKot
exports.getByBillIdOpen = function (req, res) {
    SyncKot.findOne({billId: req.query.billId, closeTime: null}, function (err, syncKot) {
        if (err) {
            return handleError(res, err);
        }
        if (!syncKot) {
            return res.send(404);
        }
        //if(syncKot.billData.table)
        // syncKot.billData=JSON.stringify(syncKot.billData);
        // syncKot.billData=JSON.stringify( syncKot.billData);
        /*if(typeof(syncKot.billData)=='object'){
         syncKot.billData=JSON.stringify( syncKot.billData);
         }*/

        return res.json(syncKot);
    });
};

// Get a single syncKot
exports.getKotNumber = function (req, res) {

    /*SyncKot.find({deployment_id:req.query.deployment_id,instanceId:req.query.instanceId,_created:{ $gte: moment(req.query.startDate).toDate(), $lte: moment(req.query.endDate).toDate()}},{kotNumber:1},{sort:-1,limit:1}, function (err, syncKot) {

     if(err) { return handleError(res, err); }
     if(!syncKot) { return res.send(404); }
     return res.json(syncKot);
     });*/
    // console.log(req.query.startDate);
    //console.log(moment(req.query.startDate).toISOString());

    //console.log(moment(req.query.endDate).toDate());
    var tdate = new Date(req.query.startDate);
    tdate = tdate.setDate(tdate.getDate() + 1);
    try{
   console.log((new Date( req.query.startDate)).toISOString());
}catch(e){
    return res.json({kotNumber: 0});
}
    //console.log(moment(tdate).format());
    SyncKot.aggregate([
        {
            $match: {
                deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
                instance_id: req.query.instance_id
            }
        },
        {
            $unwind: '$billData._kots'
        },
        {
            $match: {
                'billData._kots.created': {
                    $gte: (new Date( req.query.startDate)).toISOString(),
                    $lt: (new Date(tdate)).toISOString()
                }
            }
        }
        , {
            $group: {
                _id: 0,
                kotNumber: {$max: "$billData._kots.kotNumber"}
            }
        }
    ], function (err, result) {
        // console.log(result);
        if (err) {
           // return res.json({kotNumber: 0});
            return handleError(res, err);
        }

        //if(!syncKot) { return res.send(404); }
        if (result.length > 0) {
            return res.json(result[0]);
        } else {
            return res.json({kotNumber: 0});
        }
    })

};
// Get a single syncKot
exports.getDaySerialNumber = function (req, res) {
    Q.all([daySerialNumber(req.query), getSerialNumber(req.query), getBillNumber(req.query), getPastBillNumber(req.query)]).then(function (result) {
        var numbers = {};
        numbers.daySerialNumber = result[0].daySerialNumber;
        numbers.serialNumber = result[1].serialNumber;
        numbers.billNumber = result[2].billNumber > result[3].billNumber ? result[2].billNumber : result[3].billNumber;
        return res.json(numbers);
    })
};

// Get day serial number
function daySerialNumber(query) {
    //console.log((query.startDate).toISOString());
    var tdate = new Date(query.startDate);
    tdate = new Date(tdate.setDate(tdate.getDate() + 1));
    //console.log((tdate).toISOString());
    var d = Q.defer();
    SyncKot.aggregate([
        {
            $match: {
                created: {
                    $gte: moment(query.startDate).toDate()
                    , $lt: moment(tdate).toDate()
                },
                deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
                instance_id: query.instance_id
            }
        },
        {
            $group: {
                _id: 0,
                daySerialNumber: {$max: "$daySerialNumber"}
                // ,serialNumber:{$max:"$serialNumber"},
                // billNumber:{$max:"$billNumber"}
            }
        }], function (err, result) {
        if (err) {
            //return handleError(res, err);
            d.reject(err);
        }
        //if(!syncKot) { return res.send(404); }
        console.log(result);
        if (result.length > 0) {
            d.resolve(result[0]);
        } else {
            d.resolve({daySerialNumber: 0});
        }
    })
    return d.promise;
}


// Get day serial number
function getSerialNumber(query) {
    var d = Q.defer();
    SyncKot.aggregate([
        {
            $match: {
                deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
                instance_id: query.instance_id
            }
        },
        {
            $group: {
                _id: 0,
                serialNumber: {$max: "$serialNumber"}
            }
        }], function (err, result) {
        if (err) {
            d.reject(err);
        }
        if (result.length > 0) {
            d.resolve(result[0]);
        } else {
            d.resolve({serialNumber: 0});
        }
    })
    return d.promise;
}

// Get day serial number
function getBillNumber(query) {
    var d = Q.defer();
    SyncKot.aggregate([
        {
            $match: {
                created: {
                    $gte: moment(query.startDate).toDate()
                    // ,$lte: moment(req.query.endDate).toDate()
                },
                deployment_id: new mongoose.Types.ObjectId(query.deployment_id)
                //, instance_id: query.instance_id
            }
        },
        {
            $group: {
                _id: 0,
                billNumber: {$max: "$billNumber"}
                // ,serialNumber:{$max:"$serialNumber"},
                // billNumber:{$max:"$billNumber"}
            }
        }], function (err, result) {
        if (err) {
            //return handleError(res, err);
            d.reject(err);
        }
        //if(!syncKot) { return res.send(404); }
        if (result.length > 0) {
            d.resolve(result[0]);
        } else {
            d.resolve({billNumber: 0});
        }
    })
    return d.promise;
}

// Get day serial number
function getPastBillNumber(query) {
    var d = Q.defer();
    SyncKot.aggregate([
        {
            $match: {
                /*created: {
                 $gte: moment(query.startDate).toDate()
                 // ,$lte: moment(req.query.endDate).toDate()
                 },*/
                deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
                // instance_id: query.instance_id,
                closeTime: null
            }
        },
        {
            $group: {
                _id: 0,
                billNumber: {$max: "$billNumber"}
                // ,serialNumber:{$max:"$serialNumber"},
                // billNumber:{$max:"$billNumber"}
            }
        }], function (err, result) {
        if (err) {
            //return handleError(res, err);
            d.reject(err);
        }
        //if(!syncKot) { return res.send(404); }
        if (result.length > 0) {
            d.resolve(result[0]);
        } else {
            d.resolve({billNumber: 0});
        }
    })
    return d.promise;
}
//Get day Sale

//Create Dynamic Query
exports.dynamicQuery = function (req, res) {
    SyncKot.find(req.body.query, req.body.fields, req.body.sortData, function (err, kots) {
        if (err) {
            return handleError(res, err);
        }
        if (kots) {
            res.json(200, kots);
        }
    })
}

//Get bill lock intial data
exports.getBillLockData = function (req, res) {
    syncLock.findOne({deployment_id: req.query.deployment_id}, function (err, syncLockData) {
        if (err) {
            return handleError(res, err);
        }
        if (syncLockData) {
            return res.json(200, syncLockData);
        }
        else {
            return res.json(200, {tables: [], nonTables: []});
        }
    })
}

//Get Day and Unsettled Sale
exports.getUnsettledSale = function (req, res) {
    var currentdate = new Date();
    var resettime = 2;
    var startDate = new Date();
    startDate = new Date(startDate.setHours(resettime, 0, 0, 0));
    if (currentdate.getHours() < resettime) {
        startDate = new Date(startDate.setDate(startDate.getDate() - 1));
    }
    Common.getDayUnsettledSale({deployment_id: req.query.deployment_id, startDate: startDate}).then(function (result) {
        return res.json(200, result)
    }, function (err) {
        return res.send(404);
    })
}

// Creates a new syncKot in the DB.
exports.create = function (req, res) {
    console.log(req.body.isCard);
    if (req.body.isSplit == undefined) {
        if (!(req.body.tableNumber == null || req.body.tableNumber == undefined)) {
            // console.log(req.body.deployment_id);
            // console.log(req.body.tabId);
            SyncKot.count({
                deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id),
                tabId: req.body.tabId,
                tableNumber: req.body.tableNumber,
                closeTime: null
            }, function (err, result) {
                if (result == 0) {
                    req.body.lastModified = new mongoose.Types.ObjectId();
                    //req.body.billData=JSON.parse(req.body.billData);
                    // var jsonBill = typeof(bill.billData)=='object'?bill.billData: JSON.parse(bill.billData);
                    req.body.billData = typeof(req.body.billData) == 'object' ? req.body.billData : JSON.parse(req.body.billData);
                    if (req.body.isCard) {
                        insertTransactions(req.body.billData.cardTransaction).then(function(){
                            delete req.body.cardTransaction;
                            SyncKot.create(req.body, function (err, syncKot) {
                                if (err) {
                                    return handleError(res, err);
                                }
                                return res.json(201, syncKot);
                            });
                        },function(err){
                            return res.json(404,{error:'card error'});
                        })
                    } else {
                        SyncKot.create(req.body, function (err, syncKot) {
                            if (err) {
                                return handleError(res, err);
                            }
                            return res.json(201, syncKot);
                        });
                    }

                } else {
                    return res.json(500, {error: "inValid"});
                }
            })
        } else {
            req.body.lastModified = new mongoose.Types.ObjectId();
            //  req.body.billData=JSON.parse(req.body.billData);
            req.body.billData = typeof(req.body.billData) == 'object' ? req.body.billData : JSON.parse(req.body.billData);

            if (req.body.isCard) {
                insertTransactions(req.body.billData.cardTransaction).then(function(){
                    delete req.body.cardTransaction;
                    SyncKot.create(req.body, function (err, syncKot) {
                        if (err) {
                            return handleError(res, err);
                        }
                        return res.json(201, syncKot);
                    });
                },function(err){
                    return res.json(404,{error:'card error'});
                })
            } else {
                SyncKot.create(req.body, function (err, syncKot) {
                    if (err) {
                        return handleError(res, err);
                    }
                    return res.json(201, syncKot);
                });
            }

        }
    } else {
        req.body.lastModified = new mongoose.Types.ObjectId();
        req.body.billData = typeof(req.body.billData) == 'object' ? req.body.billData : JSON.parse(req.body.billData);
        if (req.body.isCard) {
            insertTransactions(req.body.billData.cardTransaction).then(function(){
                SyncKot.create(req.body, function (err, syncKot) {
                    if (err) {
                        return handleError(res, err);
                    }
                    return res.json(201, syncKot);
                });
            },function(err){
                return res.json(404,{error:'card error'});
            })
        } else {
            SyncKot.create(req.body, function (err, syncKot) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(201, syncKot);
            });
        }
    }
    /*// return res.json(500, {error:"inValid"});
     if(req.body.lastModified!=undefined){
     return res.json(500, {error:"inValid"});
     }*/

};

//Get Online Bills
exports.getOnlineBills = function (req, res) {
    OnlineBill.find({
        deployment_id: req.query.deployment_id,
        TabId: req.query.tabId,
        DownloadStatus: req.query.DownloadStatus
    }, {}, {sort: {OnlineBillNumber: 1}}, function (err, onlineBills) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, onlineBills);
    })
}

// Creates a new syncKot in the DB.
exports.createOnlineBill = function (req, res) {
    OnlineBill.create(req.body, function (err, onlineBill) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, onlineBill);
    });
};

// Creates a new syncKot in the DB.
exports.updateOnlineBill = function (req, res) {
    var billid;
    //if(req.body.OnlineBillId) {billid=req.body.OnlineBillId; delete req.body.id; }
    OnlineBill.findOne({OnlineBillId: req.body.OnlineBillId}, function (err, onlineBill) {
        if (err) {
            return handleError(res, err);
        }
        if (!onlineBill) {
            return res.send(404);
        }
        var updated = _.merge(onlineBill, req.body);
        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, onlineBill);
        });
    });
};

// Creates a new syncKot in the DB.
exports.createAdvanceBill = function (req, res) {
    AdvanceBill.create(req.body, function (err, advanceBill) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, advanceBill);
    });
};
// Updates an existing syncKot in the DB.
/*exports.billUpdate = function(req, res) {
 var billid;
 if(req.body.id) {billid=req.body.id; delete req.body.id; }
 SyncKot.findOne({billId:billid}, function (err, syncKot) {
 if (err) { return handleError(res, err); }
 if(!syncKot) { return res.send(404); }
 var updated = _.merge(syncKot, req.body);
 updated.save(function (err) {
 if (err) { return handleError(res, err); }
 return res.json(200, syncKot);
 });
 });
 };*/

exports.billUpdate = function (req, res) {
    console.log(req.body);
    var billid;
    if (req.body.id) {
        billid = req.body.id;
        delete req.body.id;
    }
    SyncKot.findOne({billId: billid}, function (err, syncKot) {
        //console.log((req.body.lastModified));
        //console.log(moment(syncKot.lastModified));
        if (req.body.lastModified != syncKot.lastModified) {
            return res.json(500, {error: "inValid"});
        }
        req.body.lastModified = new mongoose.Types.ObjectId();
        if (err) {
            return handleError(res, err);
        }
        if (!syncKot) {
            return res.send(404);
        }
        req.body.billData = JSON.parse(req.body.billData);
        var updated = _.extend(syncKot, req.body);
        if (req.body.isCard) {
            //console.log(req.body.billData.cardTransaction);
            insertTransactions(req.body.billData.cardTransaction).then(function () {
                updated.save(function (err) {
                    if (err) {
                        return handleError(res, err);
                    }
                    return res.json(200, syncKot);
                });
            }, function (err) {
                return res.json(404, {error: 'card error'});
            })
        }else {
            updated.save(function (err) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(200, syncKot);
            });
        }
    });
};
exports.getTransactionsForReport=function(req,res) {
    console.log(req.body);
    CardTransactions.aggregate([
        {
            $match: {
                'deployment_id': new mongoose.Types.ObjectId(req.body.deployment_id),
                isSynced:null
                /*,
                 closeTime: {
                 $lte: moment( new Date(new Date(req.body.endDate))).add(1,'days').toDate()
                 }*/
            }
        },
        {$unwind: '$transactions'},
        {
            $match: {
                'transactions.serverTime': {
                    $gt: moment(new Date(req.body.startDate)).toDate(),
                    $lte: moment(new Date(req.body.endDate)).toDate()
                }
            }
        },
        {
            $project: {
                _id: 0,
                trans: '$transactions',
                cardId: '$cardId',
                created: '$created',
                name: '$name',
                mobile: '$mobile',
                formNumber: '$formNumber',
                user: '$user.name'
            }
        }
    ], function (err, results) {
        console.log(err);
        getUnsettledAmountForReportFun(req.body.startDate,req.body.endDate,req.body.deployment_id).then(function(unsettleAmountTrans){
          //  console.log(unsettleAmountTrans);
            return res.json(200,{results: results,unsettled:unsettleAmountTrans});
           // return res.json(200,results);

        })

    })
}

exports.getUnsettledAmountForReport=function(req,res){
    CardTransactions.aggregate(

  // Pipeline
  [
    // Stage 1
    {
      $match: {
        created: {
                $gt: moment(new Date(req.body.startDate)).toDate(),
                $lte: moment(new Date(req.body.endDate)).toDate()
            }
            ,deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id),
            isSynced:null,
            closeTime:{$ne:null}
      }
    },

    // Stage 2
    {
      $unwind: "$transactions"
    },

    // Stage 3
    {
      $project: {
        _id:"$_id",
        amount:"$transactions.amount",
        cardId:"$cardId",
        created:"$created",
        securityAmount:"$securityAmount",
        userName:"$user.name"
      }
    },

    // Stage 4
    {
      $group: {
      _id:{_id:"$_id",cardId:"$cardId",created:"$created",userName:"$userName",securityAmount:"$securityAmount"},
      tAmount:{$sum:"$amount"}
      }
    },

    // Stage 5
    {
      $project: {
        _id:0,
        cardId:"$_id.cardId",
        userName:"$_id.userName",
        created:"$_id.created",
        balance:{$add:["$tAmount","$_id.securityAmount"]}
      }
    }

  ], function (err, results) {
        console.log(err);
        return res.json(200, results);
    })
}

function getUnsettledAmountForReportFun(startDate,endDate,deployment_id){
    var d = Q.defer();
    CardTransactions.aggregate(

  // Pipeline
  [
    // Stage 1
    {
      $match: {
        created: {
                $gt: moment(new Date(startDate)).toDate(),
                $lte: moment(new Date(endDate)).toDate()
            }
            ,deployment_id: new mongoose.Types.ObjectId(deployment_id),
              isSynced:null,
            closeTime:{$ne:null}
      }
    },

    // Stage 2
    {
      $unwind: "$transactions"
    },

    // Stage 3
    {
      $project: {
        _id:"$_id",
        amount:"$transactions.amount",
        cardId:"$cardId",
        created:"$created",
        securityAmount:"$securityAmount",
        userName:"$user.name"
      }
    },

    // Stage 4
    {
      $group: {
      _id:{_id:"$_id",cardId:"$cardId",created:"$created",userName:"$userName",securityAmount:"$securityAmount"},
      tAmount:{$sum:"$amount"}
      }
    },

    // Stage 5
    {
      $project: {
        _id:0,
        cardId:"$_id.cardId",
        userName:"$_id.userName",
        created:"$_id.created",
        //balance:{$add:["$tAmount","$_id.securityAmount"]}
        balance:"$tAmount"
      }
    }

  ], function (err, results) {
        console.log(err);
        if(err){d.reject(err)}
        //return res.json(200, results);
    d.resolve(results);
    })
    return d.promise;
}

// Updates an existing syncKot in the DB.
exports.update = function (req, res) {
    console.log(req.body);
    if (req.body._id) {
        delete req.body._id;
    }
    SyncKot.findOne({billId: req.params.id}, function (err, syncKot) {
        if (err) {
            return handleError(res, err);
        }
        if (!syncKot) {
            return res.send(404);
        }
        var updated = _.extend(syncKot, req.body);
        if (req.body.isCard) {
            insertTransactions(req.body.billData.cardTransaction).then(function () {
                delete updated.cardTransaction;
                updated.save(function (err) {
                    if (err) {
                        return handleError(res, err);
                    }
                    return res.json(200, syncKot);
                });
            }, function (err) {
                return res.json(404, {error: 'card error'});
            })
        }else {
            updated.save(function (err) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(200, syncKot);
            });
        }
    });
};

// Sync bill to server
exports.syncBill = function (req, res) {
    var domain = req.query.domain;

    SyncKot.find({isSynced: false, closeTime: {$ne: null}}, {}, {
        sort: {created: 1},
        limit: 50
    }, function (err, syncKots) {
        if (err) {
            return handleError(res, err);
        }
        if (syncKots) {
            async.eachSeries(syncKots, function (bill, callbacks) {
                var jsonBill = typeof(bill.billData) == 'object' ? bill.billData : JSON.parse(bill.billData);
                jsonBill.prefix = bill.instance.preFix;
                jsonBill.instance = bill.instance;

                request({
                    url: 'http://' + domain + '/api/bills/createWithId',
                    method: "POST",
                    json: true,   // <--Very important!!!
                    body: jsonBill
                }, function (err, result, body) {
                    // console.log(result);
                    if (!err) {
                        SyncKot.update({
                            deployment_id: jsonBill.deployment_id,
                            billId: body.billId
                        }, {$set: {isSynced: true}}, {multi: false}, function (errupdate) {
                            callbacks();
                        })
                    }
                    //console.log(err);
                })
            }, function () {
                /*SyncKot.find({isSynced: true,closeTime:{$ne:null}},{},{sort:{created:-1},skip:2}).remove(function(err){
                 console.log(err);
                 });
                 SyncKot.find({isSynced: false,closeTime:{$ne:null}}).skip(2).remove(function(err){
                 console.log(err);
                 });*/
                SyncKot.distinct('instance_id', {}, function (err, results) {
                    async.eachSeries(results, function (instanceid, callbacki) {
                        SyncKot.find({isSynced: true, instance_id: instanceid, closeTime: {$ne: null}}, {}, {
                            sort: {created: -1},
                            skip: 100
                        }, function (err, kots) {
                            console.log(kots.length);
                            async.eachSeries(kots, function (kot, callback) {
                                kot.remove(function (err) {
                                    //console.log(err);
                                    callback();
                                });
                            }, function () {
                                //console.log('n');
                                callbacki();
                            });
                        });              // console.log(kots);
                    });
                    // console.log(kots);
                })
                syncCardTransaction(domain);
               // autoSettleCard();
                res.json(201, {status: 'done'});
            })
        }
    })
}

function syncCardTransaction (domain) {
    //console.log(req.query);
    //var domain = req.query.domain;
   // CardTransactions.find({ closeTime: {$ne: null}}, {}, {
    CardTransactions.find({isSynced: null, closeTime: {$ne: null}}, {}, {
        sort: {created: 1},
        limit: 50
    }, function (err, cardTransactions) {
        if (err) {
            return false;
        }
        //console.log(cardTransactions);
        if (cardTransactions) {
            async.eachSeries(cardTransactions, function (cardTransaction, callbacks) {
              //  cardTransaction.transaction_id=cardTransaction._id;
              // delete cardTransaction._id
                request({
                    url: 'http://' + domain + '/api/billCardTransactions',
                    method: "POST",
                    json: true,   // <--Very important!!!
                    body: cardTransaction
                }, function (err, result, body) {
                    // console.log(result);
                    //console.log(cardTransaction._id);
                    if (!err) {
                        CardTransactions.update({
                           // deployment_id: jsonBill.deployment_id,
                            _id:new mongoose.Types.ObjectId( cardTransaction._id)
                        }, {$set: {isSynced: true}}, {multi: false}, function (errupdate) {
                           // console.log(errupdate);
                            callbacks();
                        })
                        callbacks();
                    }
                   // console.log(err);

                })
            }, function () {
              //  CardTransactions.find({isSynced: true,closeTime:{$ne:null}}).skip(2).remove(function(err){
                CardTransactions.find({isSynced: true, closeTime: {$ne: null}},{},{  sort: {created: -1}, skip: 100},function(err,cards){
                    //console.log(cards);
                    async.eachSeries(cards, function (card, callback) {
                        card.remove(function (err) {
                            //console.log(err);
                            callback();
                        });
                    }, function () {
                        return  false;
                    });
                });

            })

        }
    })
}
/*exports.syncBill=function(req,res) {
 console.log(req.body);
 var domain=req.query.domain;
 SyncKot.distinct('instance_id',{},function(err,results){
 async.eachSeries(results, function (instanceid, callbacki) {
 SyncKot.find({instance_id:instanceid}, {}, {sort: {created: -1}, skip: 1}, function (err, kots) {
 async.eachSeries(kots, function (kot, callback) {
 // callback();
 kot.remove(function (err) {
 //console.log(err);
 callback();
 });
 },function(){
 console.log('n');
 callbacki();
 });
 // console.log(kots);
 })
 })
 })
 res.json(201,{status:'done'})

 }*/
//get Locked bills
/*exports.getLockedBills=function(req,res){
 syncLock.find({deployment_id:req.query.deployment_id},function(err,billLocks){
 if(err){ return handleError(res, err); }
 if(!billLocks){return res.send(404);}
 return res.json(200,billLocks);
 })
 }*/
// unlock bill
exports.updateLockBill = function (req, res) {
    syncLock.update({deployment_id: req.query.deployment_id}, {
        $set: {
            tables: req.query.tables,
            nonTables: req.query.nonTables
        }
    }, {multi: false}, function (err) {
        res.json(200, {status: 'success'});
    })
}
// Deletes a syncKot from the DB.
exports.destroyBill = function (req, res) {
    SyncKot.findOne({billId: req.query.billId}, function (err, syncKot) {
        if (err) {
            return handleError(res, err);
        }
        if (!syncKot) {
            return res.send(404);
        }
        syncKot.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};
/////////////////////////////////////////////////////////////API for KDS//////////////////////////
exports.getBillsForKds = function (req, res) {

    var deployment_id = new mongoose.Types.ObjectId(req.query.deployment_id);
    var tenant_id = new mongoose.Types.ObjectId(req.query.tenant_id);
    SyncKot.find({
        deployment_id: deployment_id,
        tenant_id: tenant_id,
        closeTime: null
        /*,
        isPrinted: false*/
    }, {}, {sort: {billNumber: 1}}, function (err, syncKots) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, syncKots);
    });
}

/*exports.updateBillItemFromKds = function (req, res) {

   SyncKot.findOne({
    deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id),
    _id: req.body._id
  }, function (err, synckot) {
    if (synckot) {
      var _bill = typeof(synckot.billData)=='object'?synckot.billData: JSON.parse(synckot.billData);
      var flag = 0;
      for (var i = 0; i < _bill._kots.length; i++) {
        if (_bill._kots[i].kotNumber == req.body.update.kotNumber) {
          for (var j = 0; j < _bill._kots[i].items.length; j++) {
            if (_bill._kots[i].items[j]._id == req.body.update.itemId) {
              _bill._kots[i].items[j].status = req.body.update.status;
              flag = 1;
              break;
            }
          }
          if (flag == 1)break;
        }
      }
      if(flag!=1) return handleError(res,{error:'not found'});

      var dataToSet={};
      var key = "billData._kots." + i + ".items." + j + ".status";
      dataToSet[key]=req.body.update.status;

      console.log(req.body._id,i,j);
      SyncKot.update({deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id),_id:req.body._id},
        {'$set':dataToSet},
        function (err,numAffected) {
        if (err) {
          console.log(err);
          return handleError(res, err)
        }
        else if(numAffected>0) {
          console.log("numAffected",numAffected);
          if(io)
          io.broadcast.emit('syncKot_update' + req.body.deployment_id, {update: req.body.update});  //socket change by rajat
          res.send(200, {status: "success"});
        }
        else return handleError(res,{status:'error'});
      })
    }
  })
}*/
exports.updateBillItemFromKds=function(req,res){

   SyncKot.findOne({
    deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id),
    _id: req.body._id
  }, function (err, synckot) {
    if (synckot) {
      var _bill = typeof(synckot.billData)=='object'?synckot.billData: JSON.parse(synckot.billData);
      var list=req.body.update.items;
      console.log(list);
      async.each(list,function(item,next) {
       var key=getItemUpdateQueryInBill(_bill,item);
       var dataToSet={};
       if(key==null) next(err);
       if(key==0) next(); 
       dataToSet[key]=item.status;
       SyncKot.update({deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id),_id:req.body._id},{'$set':dataToSet},function (err,numAffected) {
           if(err)
             next(err)
          else
             next();
         })
       },
      function(err){
        if(err)
        return handleError(res,{status:'error'});
        console.log("all done");
         io.broadcast.emit('syncKot_update' + req.body.deployment_id, {update: req.body.update});
         res.status(200).send({status:'ok'});
      });
    }
    else
      return handleError(res,{status:'Not found'});  
  });
}

function getItemUpdateQueryInBill(doc,item){
  var flag =0;
  var isVoid=false;
  for (var i = 0; i < doc._kots.length; i++) {
   if (doc._kots[i].kotNumber == item.kotNumber) {
    if(doc._kots[i].isVoid){
       isVoid=true;break;
     }
     else{
      for (var j = 0; j < doc._kots[i].items.length; j++) {
         if (doc._kots[i].items[j]._id == item.itemId) {
            doc._kots[i].items[j].status = item.status;
            flag = 1;
             break;
            }
          }
         } 
        if (flag == 1)break;
       }
     }
  console.log(i,j,flag);   
  if(isVoid) return 0;   
  else if(flag==1)
    return "billData._kots." + i + ".items." + j + ".status";
  else
    return null;
}
///////////////////////////////////////////////////////API For CARD///////////////////////////////////////////
function insertTransactions(body){
    console.log(body.deployment_id);
    var d = Q.defer();
    if(body.deployment_id==undefined){
        d.resolve();
    }else{

    body.serverTime=new Date();
    CardTransactions.update({
        cardId: body.cardId,
        closeTime: null,
        deployment_id: new mongoose.Types.ObjectId(body.deployment_id)
    }, {$push: {transactions: body}}, {multi: false, safe: true, upsert: true}, function (err, cardTransaction) {
        console.log(cardTransaction);
        if (err) {
            d.reject(err);
        }else{
            console.log(cardTransaction);
          if(cardTransaction==1) {
              d.resolve();
          }else{
              d.reject();
          }
        }
    })
}
    return d.promise;
}
function checkCard(cardid, deployment_id) {
    var d = Q.defer();
    CardTransactions.findOne({
        cardId: cardid,
        closeTime: null,
        deployment_id: new mongoose.Types.ObjectId(deployment_id)
    }, {"transactions": 0}, function (err, cardTransaction) {
        // delete cardTransaction.transactions;
        //console.log(cardTransaction);
        if (err) {
            d.reject(err);
        } else {
            if (cardTransaction) {

                d.resolve(cardTransaction);
            }
            else
                d.reject('na');
        }
    });
    return d.promise;
}


function getCardBalance(cardid, deployment_id) {
    console.log(deployment_id);
    var d = Q.defer();
    CardTransactions.aggregate([
        {
            $match: {
                cardId: cardid,
                closeTime: null,
                deployment_id: new mongoose.Types.ObjectId(deployment_id)
            }
        },

        {
            $unwind: "$transactions"
        },
         {
            $project:{
                amount:"$transactions.amount",
                securityAmount:"$securityAmount"
            }
        },

        {
            $group: {
                _id:{securityAmount:"$securityAmount"},
                amount: {$sum: "$amount"}
            }
        },

        {
            $project:{
                _id:0,
                amount:{$subtract:["$amount","$_id.securityAmount"]}
               // amount:"$amount"
            }
        }
    ], function (err, result) {
        if (err) {
            d.reject(err);
        } else {
            d.resolve(result.length > 0 ? result[0].amount : 0);
        }
    })
    return d.promise;
}
function getCardBalanceWithSecurity(cardid, deployment_id) {
    console.log(deployment_id);
    var d = Q.defer();
    CardTransactions.aggregate([
        {
            $match: {
                cardId: cardid,
                closeTime: null,
                deployment_id: new mongoose.Types.ObjectId(deployment_id)
            }
        },

        {
            $unwind: "$transactions"
        },
         {
            $project:{
                amount:"$transactions.amount",
                securityAmount:"$securityAmount"
            }
        },

        {
            $group: {
                _id:{securityAmount:"$securityAmount"},
                amount: {$sum: "$amount"}
            }
        },

        {
            $project:{
                _id:0,
                amount:{$add:["$amount"]}
               // amount:"$amount"
            }
        }
    ], function (err, result) {
        if (err) {
            d.reject(err);
        } else {
            d.resolve(result.length > 0 ? result[0].amount : 0);
        }
    })
    return d.promise;
}
exports.checkCardIfExist = function (req, res) {
    console.log(req);
    if(!checkHeader(req.headers.referer)){
       return res.json(401,{err:'Authentication Error'});
    }
    getCardBalance(req.body.cardId, req.body.deployment_id).then(function (amt) {
        //console.log(err);
        //console.log(result);
        checkCard(req.body.cardId, req.body.deployment_id).then(function (result) {

             // var cAmount= _.extend(result,{"balance":amt});
            // cAmount.balance=amt;
            return res.json(200, {balance: amt, detail: result});
        }, function (err) {
            res.json(404, {err: err});
        });
    }, function (err) {
        console.log(err);
    });
}

function autoSettleCard() {
    var currentDate = new Date();
    var currenthr=currentDate.getHours();
    if(currenthr>=0 && currenthr<=7){
        return false;
    }
    currentDate.setHours(7, 0, 0, 0);
    CardTransactions.find({created: {$lte: moment(currentDate).toDate()},closeTime:null},{cardId:1,deployment_id:1}, function (err, cardTransactions) {
        if (!err) {
            async.eachSeries(cardTransactions, function (cardTransaction, callback) {
                getCardBalance(cardTransaction.cardId,cardTransaction.deployment_id).then(function (amount) {
                    var transOb = {
                        serverTime: new Date(),
                        paymentType: 'Cash',
                        cardId: cardTransaction.cardId,
                        timeStamp: new Date(),
                        deployment_id: cardTransaction.deployment_id,
                        user: {userId: '', name: 'Admin'},
                        type: 'AutoSettle',
                        amount: -(amount)
                    }
                    CardTransactions.update({
                        _id: new mongoose.Types.ObjectId(cardTransaction._id)
                    }, {$push: {transactions: transOb}, $set: {closeTime: new Date()}}, {
                        multi: false,
                        safe: true,
                        upsert: true
                    }, function (err, cardTran) {
                        callback();
                    })
                }, function (e) {
                    callback();
                })
            })//closed async
        }
    })
}

function checkHeader(header){
    if(header==null||header==undefined){return false;}
    var domainName='';
    var flag=false;
    var headerhArr=header.split('//');
    if(headerhArr.length>1) {
        var headerArr = headerhArr[1].split('/');
       // console.log(headerArr);
        if (headerArr.length > 1) {
            var subdomainarr = headerArr[0].split('.');
            if (subdomainarr.length > 2) {
                domainName = subdomainarr[1] + '.' + subdomainarr[2];
            }
           // console.log(domainName);
            if (domainName == 'posistlocal.net:9000' || domainName == 'posist.org' || domainName == 'posist.net') {
                flag = true;
            }
        }
    }
    return flag;
}


exports.getUnsettledCardAmount=function(req,res){
    var unSettledCards=[];
    CardTransactions.find({created: {$gte: moment(req.body.startDate).toDate(), $lt: moment(req.body.endDate).toDate()},closeTime:null,deployment_id:req.body.deployment_id},{cardId:1,deployment_id:1}, function (err, cardTransactions) {
        if (!err) {
            async.eachSeries(cardTransactions, function (cardTransaction, callback) {
                getCardBalance(cardTransaction.cardId,cardTransaction.deployment_id).then(function (amount) {
                    var transOb = {
                        trans: [],
                        name: cardTransaction.name,
                        mobile: cardTransaction.mobile,
                        formNumber: cardTransaction.formNumber,
                        user: cardTransaction.name,
                        created:cardTransaction.created,
                        cardId: cardTransaction.cardId
                    }
                    var ob={type: 'AutoSettle',
                        amount: -(amount),paymentType:'Cash',serverTime:new Date()};
                    transOb.trans.push(ob);
                    unSettledCards.push(transOb);
                }, function (e) {
                    callback();
                })
            },function(){
                res.json(200,unSettledCards);
            })//closed async
        }
    })
}

exports.checkMobileIfExist = function (req, res) {
    if(!checkHeader(req.headers.referer)){
        return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.findOne({
        mobile: req.body.mobile,
        closeTime: null,
        deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)
    }, {transactions: 0}, function (err, card) {
        if (err) {
            return handleError(res, err);
        }
        //console.log(card);
        if (card) {
            getCardBalance(card.cardId, req.body.deployment_id).then(function (amt) {
                return res.json(200, {balance: amt, detail: card});
            }, function (errb) {
                res.json(404, {err: errb})
            });
        } else {
            res.json(404, {err: 'cardnotfound'});
        }
    })
}
exports.getCardByCardId = function (req, res) {
    if(!checkHeader(req.headers.referer)){
        //return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.findOne({
        cardId: req.body.cardId,
        closeTime: null,
        deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)
    } , function (err, card) {
        if (err) {
            return handleError(res, err);
        }
       // console.log(card);
        if (card) {
          return res.json(200,card);
        } else {
          return  res.json(404, {err: 'cardnotfound'});
        }
    })
}

exports.createCardTransactions = function (req, res) {
    if(!checkHeader(req.headers.referer)){
        return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.count({
        mobile: req.body.mobile,
        closeTime: null,
        deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)
    }, function (errm, result) {
        if (errm) {
            return handleError(res, errm);
        }
        console.log(result);
        if (result > 0) {
            //return res.json(404, {err: "mobileerror"})
        }
        if(_.has(req.body,'transactions')){
            /*if(req.body.transactions.length>0){
                req.body.transactions[0].serverTime=new Date();
            }*/
            for(var i=0;i<req.body.transactions.length;i++){
             req.body.transactions[i].serverTime=new Date();
            }
        }
        CardTransactions.create(req.body, function (err, cardTransaction) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(201, cardTransaction);
        })
    })
}

exports.updateCardTransactions = function (req, res) {
    if(!checkHeader(req.headers.referer)){
        return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.count({cardId: req.body.cardId,
        closeTime: null,
        deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)},function(err,countCard){
        if(countCard>0){

            req.body.serverTime = new Date();
            var tempDate=req.body.serverTime;
            CardTransactions.update({
                cardId: req.body.cardId,
                closeTime: null,
                deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)
            }, {$push: {transactions: req.body}}, {multi: false, safe: true, upsert: true}, function (err, cardTransaction) {
                console.log(cardTransaction);
                if (err) {
                    return handleError(res, err);
                }
                return res.json(200, {updated: cardTransaction,time:tempDate});
            })
        }else{
            return res.json(404,{err:'Card not found.'});
        }
    })
}

exports.refundCardTransactions = function (req, res) {
    if(!checkHeader(req.headers.referer)){
        return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.count({cardId: req.body.cardId,
    closeTime: null,
    deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)},function(err,countCard){
        if(countCard>0){
            req.body.serverTime = new Date();
                var tempDate=req.body.serverTime;
                getCardBalanceWithSecurity(req.body.cardId, req.body.deployment_id).then(function (amount) {
                    if(amount<0){
                        return res.json(404,{err:'Error updating server.'})
                    }
                    req.body.amount = -(amount);
                    CardTransactions.update({
                        cardId: req.body.cardId,
                        closeTime: null,
                        deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)
                    }, {$push: {transactions: req.body}, $set: {closeTime: new Date()}}, {
                        multi: false,
                        safe: true,
                        upsert: true
                    }, function (err, cardTransaction) {
                        console.log(cardTransaction);
                        if (err) {
                            return handleError(res, err);
                        }
                        return res.json(200, {updated: cardTransaction,time:tempDate});
                    })
                })
        }else{
         return res.json(404,{err:'Card not found.'});
        }
    })
}

exports.transferCardTransaction = function (req, res) {
    if(!checkHeader(req.headers.referer)){
        return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.count({
        cardId: req.body.cardId,
        closeTime: null,
        deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)
    }, function (errc, cardCount) {
        console.log(cardCount);
        if (errc)
            return handleError(res, errc);
        if (cardCount > 0) {
            return res.json(404, {err: 'cardexist'});
        }
        CardTransactions.update({
            mobile: req.body.mobile,
            closeTime: null,
            deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)
        }, {$set: {cardId: req.body.cardId}}, {multi: false}, function (err, trans) {
            if (err) {
                return handleError(res, err);
            }
            res.json(200, {responce: trans,time:new Date()});
        });
    })
}

exports.closeOpenedCard=function(req,res){
    if(!checkHeader(req.headers.referer)){
        return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.update({closeTime:null}, {$set: {closeTime:new Date()}}, {
        multi: true,
        safe: true,
        upsert: true
    }, function (err, rowsModified) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, {rowsModified:rowsModified});
    })
}

exports.removeCardTransactions = function (req, res) {
    if(!checkHeader(req.headers.referer)){
        return res.json(401,{err:'Authentication Error'});
    }
    CardTransactions.update({deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)}, {$pull: {transactions: {cardId: req.body.cardId}}}, {
        multi: false,
        safe: true,
        upsert: true
    }, function (err, cardTransaction) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, cardTransaction);
    })
}

/*Bill Offer Integration for Card Transaction*/ 
exports.CardTransactionsCountByBill=function(req,res){
   getCardsByBillID(req.body.billId).then(function(cards){
        return res.json(200,{cards:cards});
    }
    ,function(err){
            handleError(res,err)
    })
}
function getCardsByBillID(billId){
    var d=Q.defer(); 
    CardTransactions.find({"transactions.billDetail.billId":billId,$or:[{closeTime:null},{closeTime:{ $exists:false}}]},{cardId:1},function(err,countTransactions){
        if(err){

            d.reject();
        }
        console.log(countTransactions);
        d.resolve(countTransactions);
    })
    return d.promise;
}
exports.cardTransactionsAmountByBillId=function(req,res){
    getCardsByBillID(req.body.billId).then(function(cards){
    if(cards.length==1){
        CardTransactions.aggregate(
        [
        {
          $match: {
           "cardId": cards[0].cardId, "deployment_id": mongoose.Types.ObjectId(req.body.deployment_id),closeTime:null 
          }
        },
        {
          $unwind: "$transactions"
        },
        {
          $project: {"type": "$transactions.type","billDetail":"$transactions.billDetail","amount":"$transactions.amount"}
        },
        {
          $match: {
            "type":"billTransaction",
            "billDetail.billId":req.body.billId
          }
        },
        {
          $project: {
            "_id":0, "amount": "$amount"
          }
        },
        {
          $group: {
                "_id":0,"tAmount":{"$sum":"$amount"}    
          }
        }
     ],function(err,amount){
        console.log(amount);

    if(err){
        return handleError(res,err);
    }
    if(amount.length>0)
        return res.json(200,{"Amount": amount[0].tAmount});
    else
        return res.json(200,{"Amount": 0});
  });

}else{
   if(cards.length==0){
     return res.json(200,{"Amount": 0});
   } else{
    return res.json(404,{error:'multi card transaction found'});
   }
}
    }
    ,function(err){
            handleError(res,err)
    })
    
}
exports.set = function (socket) {
    io = socket;
}
///////////////////////////////////////////////////////API For CARD ENDS///////////////////////////////////////////
//////////////////////////////////////////////End KDS/////////
exports.updateBillAfterPaidTransaction=function(req,res){
    console.log(req.body.onlineTransaction);

    var query={billId:req.body.billId,deployment_id: new mongoose.Types.ObjectId(req.body.deployment_id)};
    SyncKot.update(query,{$set:{"billData.onlineTransaction":req.body.onlineTransaction}},{},function(err){
        res.status(200).send({status:'updated'});
    });
}

//---ADDED BY RAJAT FOR KDS---//

//---ADDED BY RAJAT FOR INRESTO---//
// Get list of seatcustomers
exports.seatcustomer_index = function(req, res) {
  Seatcustomer.find({merchant_id:req.query.merchant_id},function (err, seatcustomers) {
    if(err) { console.log(err) }
    else {
      return res.status(200).json(seatcustomers);
    }
  });
};

exports.seatcustomer_saveBookingFromPos=function(req,res){
  var booking=new Seatcustomer(req.body);
  booking.save(req.body,function(err,data){
    if(err){console.log(err); res.send(err);}
    else if(data){
      console.log(data);
      res.send(data);
    }
  })
}

// send bill to inresto App

exports.seatcustomer_updateBillStatus=function(req,res){
  console.log(req.body);

  var options = { method: 'POST',
    url: 'http://54.169.68.74:3000/admin/updatebillfrompos',
    headers:
    { 'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache' },
    form:req.body
  };

  request(options, function (error, response, body) {
    if (error) {console.log(error); return handleError(res,{status:'Sending bill to inresto failed'});}
    else
      res.status(200).send(body);
  });
}

//send table status to inresto App
exports.seatcustomer_updateTableStatus = function(req,res){
  console.log(req.body);

  var options = { method: 'POST',
    url: 'http://54.169.68.74:3000/admin/updatetablefrompos',
    headers:
    { 'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache' },
    form:req.body
  };

  request(options, function (error, response, body) {
    if (error) {console.log(error); return handleError(res,{status:"table update failed"});}
    else
      res.send(body);
  });
};



// listens to requests from inresto  and emits event to client
exports.seatcustomer_listen = function(req,res) {
  console.log("body",req.body);
  var body = typeof(req.body)=='object'?req.body: JSON.parse(req.body);

  var bookingData=[];
  var tables=body.tables.split(':');
  for(var i=0;i<tables.length;i++) {
    var booking = {};
    booking.merchant_id = body.merchantId;
    booking.tableNumber=tables[i].substring(1);
    booking.tableData = {customerName: body.customerName, customerPhone: body.customerPhone.substring(2), pax: body.pax};
    bookingData.push(booking);
  }
  console.log(bookingData);
  async.each(bookingData,function(booking,next) {

      Seatcustomer.findOne({merchant_id:booking.merchant_id,tableNumber:booking.tableNumber},function(err,doc){
        if(err) next();
        else if(doc){
          doc.tableData=booking.tableData;
          doc.save(function(err,data){
            next();
          })}
         else if(doc==null){
           var seat=new Seatcustomer(booking);
           seat.save(function(err,data){
             next();
           })
          }
         else next();
        })
  },
    function(){
      console.log("all saved");
      //  Utils.broadcast('inresto', body);    //socket change by rajat
        console.log('socket emitted');
      res.status(200).send({status:'ok'});
    })
}



// Deletes a seatcustomer from the DB.
exports.seatcustomer_destroy = function(req, res) {
  Seatcustomer.findOne({"merchant_id":req.query.merchant_id,"tableNumber":req.query.tableNumber}, function (err, seatcustomer) {
    if(err) { console.log(err); }
    else if(seatcustomer){
      seatcustomer.remove(function (err,data) {
        if (err)
          console.log("error");
        else
        {console.log(data); res.status(200).send({status:'table booking removed'});}
      });
    }
    else res.status(200).send({status:'not found'});
  });
};


// --- added for payment integrations----//


function handleError(res, err) {
    return res.send(500, err);
}
