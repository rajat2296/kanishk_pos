/**
 * Created by Ranjeet Sinha on 6/27/2016.
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CardTransactionsSchema = new Schema({
    cardId:String,
    formNumber:String,
    deployment_id:{ type:Schema.Types.ObjectId},
    name:String,
    mobile:String,
    securityAmount:Number,
    entryFee:Number,
    created:{type:Date,default: Date.now},
    user:{},
    transactions:[],
    closeTime:{type:Date},
    isSynced:Boolean,
    transaction_id:{ type:Schema.Types.ObjectId}

});
CardTransactionsSchema.set('versionKey', false);
module.exports = mongoose.model('CardTransactions', CardTransactionsSchema);