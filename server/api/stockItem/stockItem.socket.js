/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockItem = require('./stockItem.model');

exports.register = function(socket) {
  StockItem.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockItem.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockItem:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockItem:remove', doc);
}