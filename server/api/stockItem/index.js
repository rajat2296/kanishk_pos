'use strict';

var express = require('express');
var controller = require('./stockItem.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/updateItemsAsync', controller.updateItemsAsync);
router.post('/pushStockUnit', controller.pushStockUnit);
//console.log(controller.pushStockItem);
router.post('/pushStockItem', controller.pushStockItem);
router.put('/tranferItemToStore', controller.tranferItemToStore);
router.put('/updateYeild/:id', controller.updateYeild);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;

//for open API
router.use('/partner/*', auth.onlyOpenApi);

router.post('/partner/stockItem', auth.hasCustomerKey, controller.pushStockItem_secure);
router.delete('/partner/stockItem', auth.hasCustomerKey, controller.deleteStockItem_secure);