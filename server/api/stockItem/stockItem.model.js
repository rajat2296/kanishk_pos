'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockItemSchema = new Schema({
  itemName:String,
  units: [],
  tenant_id: Schema.Types.ObjectId,
  preferedUnit:{},
  reorderUnit:{},
  assignedToStore:[],
  reorderLevel:Number,
  yeild:Number,
  itemCode: String,
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  code: String,
  rate: Number,
  created_by: String,
  lastUpdated: {
    date: Date,
    username: String
  },
  //store_id: String,
  active: Boolean
});

StockItemSchema.set('versionKey', false);
StockItemSchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});
StockItemSchema.path('itemName').validate(function(v, respond) {
  var self=this;
    this.constructor.findOne({'itemName':  new RegExp('^'+v+'$', "i"),deployment_id:this.deployment_id}, function (err, storeitems) {

    var flag=true;
    if(err){throw err;}

    if(storeitems)
    {
      if(storeitems.itemName.toLowerCase()===self.itemName.toLowerCase())
      {
        if(storeitems.id!=self.id){
          flag=false;
        }
      }
    }
    //     if(this.isNew){
    //       flag=false;
    //     }
    //     else{
    //       if(storeitems.id===self.id)
    //       {
    //         flag=true;
    //       }
    //       else
    //       {
    //         flag=false;
    //       }
    //     }
    //   }
    //   else
    //   {
    //     flag=true;
    //   }
    // }
    // else
    // {
    //   flag=true;
    // }
    respond(flag);
    });

}, 'This Item Name is already used. Please try  with another name !');

StockItemSchema.path('itemCode').validate(function(v, respond) {
  var self=this;
    this.constructor.findOne({'itemCode':  new RegExp('^'+v+'$', "i"),deployment_id:this.deployment_id}, function (err, storeitems) {

    var flag=true;
    if(err){throw err;}

    if(storeitems)
    {
      if(storeitems.itemCode.toLowerCase()===self.itemCode.toLowerCase())
      {
        if(storeitems.id!=self.id){
          flag=false;
        }
      }
    }
    respond(flag);
    });

}, 'This Item Code is already used. Please try with another code !');

module.exports = mongoose.model('StockItem', StockItemSchema);

//StockItemSchema.path('itemName').validate(function(v, fn) {
  //var StoreItemModel = mongoose.model('StockItem');
  //StoreItemModel.find({'itemName':  new RegExp('^'+v+'$', "i")}, function (err, storeItm) {
    //console.log(err);
    //console.log(storeItm);
    //fn(err || storeItm.length === 0);
  //});
//}, 'This Item is already defined please add with another name !');
