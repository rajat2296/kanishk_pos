'use strict';

var _ = require('lodash');
var StockItem = require('./stockItem.model');
var StockCategory = require('../stockCategory/stockCategory.model');
var Store = require('../store/store.model');
var async = require('async');
var ObjectId = require('mongoose').Types.ObjectId;
var StockUnit = require('../stockUnit/stockUnit.model');
var Deployment = require('../deployment/deployment.model');
var Tenant = require('../tenant/tenant.model');
var Vendor = require('../vendor/vendor.model');
var Receiver = require('../receiver/receiver.model');
var Q = require('q');

// Get list of stockItems
exports.index = function (req, res) {
  StockItem.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}, function (err, stockItems) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, stockItems);
  });
};

// Get a single stockItem
exports.show = function (req, res) {
  StockItem.findById(req.params.id, function (err, stockItem) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockItem) {
      return res.send(404);
    }
    return res.json(stockItem);
  });
};

// Creates a new stockItem in the DB.
exports.create = function (req, res) {
  StockItem.create(req.body, function (err, stockItem) {
    if (err) {
      return res.json({error: err});
      //return handleError(res, err);
    }
    else {
      return res.json(201, stockItem);
    }
  });
};

// Updates an existing stockItem in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  console.log('id', req.params.id);
  StockItem.findById(req.params.id, function (err, stockItem) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockItem) {
      return res.send(404);
    }
    //var updated = _.merge(stockItem, req.body);
    var updated = _.extend(stockItem, req.body);
    updated.save(function (err) {
      if (err) {
        return res.json({error: err});
      }
      return res.json(200, stockItem);
    });
  });
};

exports.updateYeild = function (req, res) {

  var item = req.body;
  StockItem.findByIdAndUpdate(item._id, {$set: {yeild: item.yeild}}, {new: true}, function (err, item) {
    if(err)
      return handleError(res, err);
    return res.status(200).json(item);
  });
}

exports.tranferItemToStore = function (req, res) {

  console.log('tranferItemToStore called', req.body);
  var item = req.body.item;
  var category = req.body.category;
  var store = req.body.store;
  var result = {};

  Q.all([
      updateItem(item),
      updateCategory(category),
      updateStore(store),
      updateVendors(item, category, store),
      updateReceivers(item, category, store)
    ]).then(function success(values) {
      result.item = values[0];
      result.category = values[1];
      result.store = values[2];

      return res.status(200).json(result);
    }).catch(function (err) {
      console.log(err);
      return handleError(res, err);
    });
};

var updateItem = function (item) {

  var deferred = Q.defer();
  StockItem.findById(item._id, function (err, stockItem) {
    if (err) {
      deferred.reject(err);
    }
    if (!stockItem) {
      deferred.reject("Item not found");
    }
    else {
    //var updated = _.merge(stockItem, req.body);
      var updated = _.extend(stockItem, item);
      updated.save(function (err) {
        if (err) {
          deferred.reject(err);
        }
        else
          deferred.resolve(updated);
      });
    }
  });
  return deferred.promise;
}

var updateCategory = function (category) {

  var deferred = Q.defer();
  StockCategory.findById(category._id, function (err, stockCategory) {
    if (err) { deferred.reject(err); }
    if(!stockCategory) { deferred.reject("Category Not found"); }
    else {
      var updated = _.extend(stockCategory, category);
      updated.save(function (err) {
        if (err) { 
          //return handleError(res, err); 
          deferred.reject(err);
        }
        else
          deferred.resolve(updated);
      });
    }
  });
  return deferred.promise;
}

var updateStore = function (store) {

  var deferred = Q.defer();
  Store.findById(store._id, function (err, st) {
    if (err)
    {
      deferred.reject(err);
    }
    if(!store) { deferred.reject("Store Not Found"); }
    //var updated = _.merge(store, req.body);
    else {
      var updated = _.extend(st, store);
      updated.save(function (err) {
        if (err) {
          deferred.reject(err);
          //return handleError(res, err);
        }
        else
          deferred.resolve(updated);
      });
    }
  });
  return deferred.promise;
};

var updateVendors = function (item, category, store) {

  var deferred = Q.defer();
  Vendor.find({"pricing._id": store._id, type: "Local Vendor"}, function (err, vendors) {
    //console.log(vendors.length);
    if(err)
      deferred.reject(err);
    else {
      async.each(vendors, function (v, callback) {
        var isItemAddedtoVendor = false;
          if (v.pricing._id == store._id) {
            _.forEach(v.pricing.category, function (cat, ii) {
              if (cat._id == category._id) {
                var flag = false;
                _.forEach(cat.item, function (itm, iii) {
                  if (itm._id == item._id) {
                    flag = true;
                  }
                });

                if (flag == false) {
                  //add item to category
                  var it = item;
                  for (var p in it) {
                    if (p != "_id" && p != "itemName" && p != "preferedUnit") {
                      _.forEach(it.units, function (u) {
                        for (var p in u) {
                          if (p != "_id" && p != "unitName" && p != "conversionFactor") {
                            delete u[p];
                          }
                        }
                      });
                    }
                  }
                  it["price"] = "";
                  it["fromDate"] = "";
                  it["toDate"] = "";
                  cat.item.push(it);
                  isItemAddedtoVendor = true;
                }
              }
            });
          }
          if (isItemAddedtoVendor == true) {
            //Update vendor
            //console.log("Hello", v);
            v.save(function (err) {
              //$scope.vendors.splice(index,1);
              //console.log(err);
              callback();
            });
          }
      }, function done(err){
        if(err)
          deferred.reject(err);
        else
          deferred.resolve(vendors);
      });
    }
  });
}

var updateReceivers = function (item, category, store) {

  var deferred = Q.defer();
  Receiver.find({"pricing.store._id": store._id}, function (err, receivers) {
    console.log(receivers.length);
    if(err)
      deferred.reject(err);
    else {
      async.each(receivers, function (r, callback) {
        var isItemAddedtoVendor = false;
        if (r.pricing == undefined) {
          r.pricing = {store: []}
        }
        console.log(r.pricing.store.length);
        _.forEach(r.pricing.store, function (str, j) {
          if (str._id == store._id) {
            console.log('store found');
            _.forEach(str.category, function (cat, ii) {
              if (cat._id == category._id) {
                var flag = false;
                console.log('categoryfound');
                _.forEach(cat.item, function (itm, iii) {
                  if (itm._id == item._id) {
                    console.log('item found')
                    flag = true;
                  }
                });
                if (flag == false) {
                  //add item to category
                  var it = item;
                  for (var p in it) {
                    if (p != "_id" && p != "itemName" && p != "preferedUnit") {
                      _.forEach(it.units, function (u) {
                        for (var p in u) {
                          if (p != "_id" && p != "unitName" && p != "conversionFactor") {
                            delete u[p];
                          }
                        }
                      });
                    }
                  }
                  it["price"] = "";
                  //it["fromDate"]="";
                  //it["toDate"]="";
                  cat.item.push(it);
                  console.log('item pushed', item.itemName);
                  isItemAddedtoVendor = true;
                }
              }
            });
          }
        });
        //console.log('hello', JSON.stringify(r));
        console.log('hello', r.receiverName, isItemAddedtoVendor);
        if (isItemAddedtoVendor == true) {
          //Update receiver
          //var updated = _.extend(rec, r);
          // updated.save(function (err) {
          //   callback();
          // });
          Receiver.update({_id: r._id}, {$set: {"pricing": r.pricing}}, function (err, n){
            console.log(err);
            console.log('updating receiver');
            callback();
          });
        }
      }, function done(err){
        if(err)
          deferred.reject(err);
        else
          deferred.resolve(receivers);
      });
    }
  });
}

exports.updateItemsAsync = function (req, resp) {
  var items = req.body.items;
  var category = req.body.category;
  var store = req.body.store;
  var res = {};
  async.eachSeries(items, function (item, callback) {
    var id = item._id;
    delete item._id;
    StockItem.findById(id, function (err, stockItem) {
      if (err)
        res = {'errorcode': 500, error: err};
      if (!stockItem)
        res = {errorcode: 404, error: 'Not Found'}
      var updated = _.extend(stockItem, item);
      updated.save(function (err) {
        if (err)
          res = {errorcode: 500, error: err}
        callback();
      });
    });
  }, function done() {
    //console.log(done);
    StockCategory.update({_id: category._id}, category, function (err, cat){ 

    });
    Store.update({_id: store._id}, store, function (err, store) {
      
    })
    return resp.status(200).json(res);
  });
};

exports.pushStockItem = function (req, res) {

  //console.log(req.body);
  if (_.has(req.query, 'isDelete')) {
    var isDelete = req.query.isDelete;
    if (isDelete == false || isDelete == 'false') {
      if (req.body.deployment_id && req.body.itemName && req.body.code && req.body.tenant_id && req.body.unitLNCode) {
        StockItem.findOne({code: req.body.code, deployment_id: req.body.deployment_id}, function (err, sItem) {
          if (err)
            return handleError(res, err);
          if (!sItem) {
            StockUnit.findOne({lnCode: req.body.unitLNCode, tenant_id: req.body.tenant_id, deployment_id: req.body.deployment_id}, function (err, unit){

              if(err)
                return handleError(res, unit);
              if(!unit)
                return res.send("Unit not found! Please check unitLNCode");
              var itemToSave = _.clone(req.body);
              itemToSave.units = [];
              itemToSave.units.push(unit);
              Store.findOne({
                  store_id: req.body.store_id,
                  deployment_id: req.body.deployment_id}, function (err, store){
                    if(err)
                      return res.send(500, err);
                    if(!store)
                      return res.status(500).send('Specified store not found in database!');
              StockItem.create(itemToSave, function (err, newSItem) {
                if(err)
                  return handleError(res, err);
                  var category = new Object(store.category);
                  var catIndex = _.findIndex(category, function (cat) {
                    return cat.categoryName == "Raw Items";
                  });
                  if (catIndex > -1) {
                    if (category[catIndex].item)
                      category[catIndex].item.push(newSItem);
                    else {
                      category[catIndex].item = [];
                      category[catIndex].item.push(newSItem);
                    }
                  } else {
                    category.push({
                      categoryName: "Raw Items",
                      item: [newSItem]
                    });
                  }
                  //updated.markModified('category');
                  //updated.save(function (err) {
                  //  if(err)
                  //    return handleError(res, err);
                  //  return res.status(200).json(newSItem);
                  //});
                  Store.update({
                    store_id: store.store_id,
                    deployment_id: req.body.deployment_id
                  }, {$set: {category: category}}, function (err, st) {
                    //console.log(err, st);
                    if (err)
                      return handleError(res, err);
                    StockItem.findByIdAndUpdate({_id: newSItem._id}, {
                      $push: {
                        assignedToStore: {
                          _id: store._id,
                          storeName: store.storeName
                        }
                      }
                    }, {new: true}, function (err, it) {
                      //console.log(144, err);
                      //console.log(it);
                      return res.status(200).json({errorcode: 200, message: "Item created successfully!"});
                    });
                  });
                });
              });
            });
          } else {
            var updated = _.extend(sItem, req.body);
            updated.save(function (err) {
              if (err)
                return handleError(res, err);
              //console.log({deployment_id: req.body.deployment_id, $or: [{"category.item._id": sItem._id.toString()}, {store_id: req.body.store_id}]});
              Store.find({deployment_id: req.body.deployment_id, $or: [{"category.item._id": sItem._id.toString()}, {"category.item._id": sItem._id}, {store_id: req.body.store_id}]}, function (err, stores) {
                if (err)
                  return handleError(res, err);
                // console.log(stores.length);
                // console.log(stores);
                async.eachSeries(stores, function (store, callback) {
                  var flag = false;
                  var category = new Object(store.category);
                  console.log(category);
                  var it = new Object(updated);
                  var isItemFound = false;
                  for (var c in category) {
                    //console.log('156', c);
                    for (var i in category[c].item) {
                      //console.log(158, i)
                      //console.log(updated);
                      //console.log(category[c].item[i]._id == it._id);
                      if (category[c].item[i]._id.toString() == it._id.toString()) {
                        //console.log(158, i);
                        category[c].item[i] = it;
                        flag = true;
                        isItemFound = true;
                        break;
                      }
                    }
                    if (flag = true)
                      break;
                  }

                  if (!isItemFound) {
                    console.log(!isItemFound);
                    var cat = _.find(category,function (c) {
                      return c.categoryName == "Raw Items";
                    });
                    if(cat){
                      if (cat.item)
                        cat.item.push(it);
                      else {
                        cat.item = [];
                        cat.item.push(it);
                      }
                    } else {
                      cat = {
                        categoryName: "Raw Items",
                        item: [it]
                      }
                    }
                    console.log('category', category);
                    StockItem.update({_id: it._id}, {
                      $pull: {
                        assignedToStore: {
                          _id: store._id
                        }
                      },
                      $push: {
                        assignedToStore: {
                          _id: store._id,
                          storeName: store.storeName
                        }
                      }
                    }, {new: true}, function (err, sti) {
                      //console.log(194, err, sti);
                    });
                  }
                  Store.update({
                    store_id: store.store_id,
                    deployment_id: req.body.deployment_id
                  }, {$set: {category: category}}, function (err, st) {
                    if (err)
                      return handleError(res, err);
                    //console.log('st', category);
                    callback();
                  });
                });
                return res.status(200).json({errorcode: 200, message: "Item updated successfully!"});
                //store.save(function (err) {
                //  if(err)
                //    return handleError(res, err);
                //  return res.status(200).json(updated);
                //});
                //console.log('171', category);
              });
            });
          }
        });
      } else {
        return res.status(500).send('Please provide all the required attributes.');
      }
    }
    else {
      if (req.query.code && req.query.deployment_id) {
        StockItem.findOne({code: req.query.code, deployment_id: req.query.deployment_id}, function (err, stockItem) {
          if (err) {
            return handleError(res, err);
          }
          if (!stockItem) {
            return res.send(404);
          }
          stockItem.remove(function (err) {
            if (err) {
              return handleError(res, err);
            }
            Store.find({deployment_id: req.query.deployment_id, $or: [{"category.item._id": stockItem._id.toString()}, {"category.item._id": stockItem._id}]}, function (err, stores) {
              if (err)
                return handleError(res, err);
              async.eachSeries(stores, function (store, callback){
                var flag = false;
                var category = new Object(store.category);
                var index = -1;
                for (var c in category) {
                  index = _.findIndex(category[c].item, function (it) {
                    return it.code == req.query.code;
                  });
                  //console.log(219, index);
                  if (index > -1) {
                    category[c].item.splice(index, 1);
                  }
                }
                //console.log(category);
                //store.save(function (err) {
                //  if(err)
                //    return handleError(res, err);
                //  return res.status(200).json(updated);
                //});
                //console.log('171', category);
                Store.update({store_id: store.store_id}, {$set: {category: category}}, function (err, st) {
                  if (err)
                    return handleError(res, err);
                  callback();
                });
              });
              return res.status(200).send('Success');
              //return res.send(204, "Success");
            });
          });
        });
      }
      else {
        return res.status(500).send("Please provide a item code and deployment id!");
      }
    }
  }
  else {
    return res.status(500).send("Please provide required parameters!");
  }
};

exports.pushStockUnit = function (req, res) {

  var baseUnits = [
      {id: 1, name: "Gram", value: 1},
      {id: 2, name: "Litre", value: 0.001},
      {id: 3, name: "Meter", value: 0.001},
      {id: 4, name: "Pc", value: 1}
    ];

    var bU = null;
    var body = req.body;
    if(!body.deployment_id || !body.tenant_id || !body.baseUnitId || !body.unitName || !body.baseConversionFactor || !body.lnCode){
      return res.send("Please provide required parameters!");
    } else {
      if(isNaN(body.baseConversionFactor))
        return res.send("Conversion factor is invalid!");
      else {
        bU = _.find(baseUnits, function(unit){
          return unit.id == body.baseUnitId;
        });
        if(!bU)
          return res.send("Base unit not found! Please check baseUnitId.");
      }
      Tenant.findOne({_id: body.tenant_id}, {_id: 1}, function (err, tenant){
        if(err)
          return handleError(res, err);
        if(!tenant)
          return res.send("Tenant not found! Please check tenant_id.");
        Deployment.findOne({tenant_id: tenant._id, _id: body.deployment_id}, {_id: 1}, function (err, deployment) {
          if(err)
            return handleError(err);
          if(!deployment)
            return res.send("Deployment not found! Please check deployment_id.");
          var unitToSave = {
            unitName: body.unitName,
            baseUnit: bU,
            baseConversionFactor: body.baseConversionFactor,
            conversionFactor: body.baseConversionFactor / bU.value,
            deployment_id: body.deployment_id,
            tenant_id: body.tenant_id,
            lnCode: body.lnCode
          }
          StockUnit.create(unitToSave, function (err, unit) {
            if(err)
              return handleError(res, err);
            return res.status(200).json({errorcode: 200, message: "Unit created successfully!"});
          });
        });
      });
    }
};

// Deletes a stockItem from the DB.
exports.destroy = function (req, res) {
  StockItem.findById(req.params.id, function (err, stockItem) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockItem) {
      return res.send(404);
    }
    stockItem.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

/**************FOR OPEN API*************************/
/* @author - Shubhank
 @functions -
 */

function handleApiError(res, err) {
  return res.status(500).json({code: 500, label: "internal_error", message: "Some error occurred in the system."});
}

function filterResponse(json) {
  return _.omit(json.toObject(), ['deployment_id', 'tenant_id']);
}

exports.pushStockItem_secure = function (req, res) {
  req.body.deployment_id = req.query.deployment_id;
  req.body.tenant_id = req.query.tenant_id;

  if (!_.isString(req.body.store_id))
    res.status(422).json({status: "error", message: "store_id should be a string."});
  else if (!_.isString(req.body.itemName))
    res.status(422).json({status: "error", message: "itemName should be a string."});
  else if (!_.isString(req.body.code))
    res.status(422).json({status: "error", message: "code should be a string."});
  else {
    StockItem.findOne({code: req.body.code, deployment_id: req.body.deployment_id}, function (err, sItem) {
      if (err)
        return handleApiError(res, err);
      if (!sItem) {
        StockItem.create(req.body, function (err, newSItem) {
          if (err)
            return handleApiError(res, err);
          Store.findOne({store_id: req.body.store_id, deployment_id: req.body.deployment_id}, function (err, store) {
            if (err)
              return handleApiError(res, err);
            if (!store)
              return res.status(404).json({status: "error", message: 'Store not found.'});
            var category = new Object(store.category);
            var catIndex = _.findIndex(category, function (cat) {
              return cat.categoryName == "Raw Items";
            });
            if (catIndex > -1) {
              if (category[catIndex].item)
                category[catIndex].item.push(newSItem);
              else {
                category[catIndex].item = [];
                category[catIndex].item.push(newSItem);
              }
            } else {
              category.push({
                categoryName: "Raw Items",
                item: [newSItem]
              })
            }
            Store.update({
              store_id: store.store_id,
              deployment_id: req.body.deployment_id
            }, {$set: {category: category}}, function (err, st) {
              if (err)
                return handleApiError(res, err);
              StockItem.findByIdAndUpdate({_id: newSItem._id}, {
                $push: {
                  assignedToStore: {
                    _id: store._id,
                    storeName: store.storeName
                  }
                }
              }, {new: true}, function (err, it) {
                if (err) return handleApiError(res, err);
                console.log(it);
                return res.status(201).json(filterResponse(newSItem));
              });
            });
          });
        });
      } else {
        var updated = _.extend(sItem, req.body);
        updated.save(function (err) {
          if (err)
            return handleApiError(res, err);
          Store.findOne({store_id: updated.store_id, deployment_id: req.body.deployment_id}, function (err, store) {
            if (err)
              return handleApiError(res, err);
            if (!store)
              return res.status(404).json({status: "error", message: "store not found."});
            console.log(store.storeName);
            var flag = false;
            var category = new Object(store.category);
            console.log(category);
            var it = new Object(updated);
            var isItemFound = false;
            for (var c in category) {
              console.log('156', c);
              for (var i in category[c].item) {
                //console.log(158, i)
                //console.log(updated);
                console.log(category[c].item[i]._id == it._id);
                if (category[c].item[i]._id.toString() == it._id.toString()) {
                  console.log(158, i);
                  category[c].item[i] = it;
                  flag = true;
                  isItemFound = true;
                  break;
                }
              }
              if (flag = true)
                break;
            }

            if (!isItemFound) {
              console.log(!isItemFound);
              if (category.item)
                category.item.push(it);
              else {
                category.item = [];
                category.item.push(it);
              }
              StockItem.update({_id: it._id}, {
                $pull: {
                  assignedToStore: {
                    _id: store._id
                  }
                },
                $push: {
                  assignedToStore: {
                    _id: store._id,
                    storeName: store.storeName
                  }
                }
              }, {new: true}, function (err, sti) {
                console.log(194, err, sti);
              });
            }
            //store.save(function (err) {
            //  if(err)
            //    return handleError(res, err);
            //  return res.status(200).json(updated);
            //});
            //console.log('171', category);
            Store.update({
              store_id: store.store_id,
              deployment_id: req.body.deployment_id
            }, {$set: {category: category}}, function (err, st) {
              if (err)
                return handleApiError(res, err);
              console.log(st);
              return res.status(200).json(filterResponse(updated));
            });
          });
        });
      }
    });
  }
};

exports.deleteStockItem_secure = function (req, res) {
  if (req.query.code && req.query.store_id) {
    StockItem.findOne({
      code: req.query.code,
      store_id: req.query.store_id,
      deployment_id: req.query.deployment_id
    }, function (err, stockItem) {
      if (err) {
        return handleApiError(res, err);
      } else if (!stockItem) {
        return res.status(404).json({status: "error", message: "Stock Item not found."});
      } else {
        stockItem.remove(function (err) {
          if (err) {
            return handleApiError(res, err);
          } else {
            Store.findOne({
              store_id: req.query.store_id,
              deployment_id: req.query.deployment_id
            }, function (err, store) {
              if (err)
                return handleApiError(res, err);
              var flag = false;
              var category = new Object(store.category);
              var index = -1;
              for (var c in category) {
                index = _.findIndex(category[c].item, function (it) {
                  return it.code == req.query.code;
                });
                if (index > -1) {
                  category[c].item.splice(index, 1);
                }
              }
              Store.update({
                store_id: store.store_id,
                deployment_id: req.query.deployment_id
              }, {$set: {category: category}}, function (err, st) {
                if (err)
                  return handleApiError(res, err);
                return res.status(200).json({status: "success", message: "Stock item removed."});
              });
            });
          }
        });
      }
    });
  } else {
    return res.status(422).json({status: "error", message: "Please provide store id and item id to be removed."});
  }
};

/************CLOSE OPEN API**************/
