'use strict';

var _ = require('lodash');
var Btc = require('./btc.model');

// Get list of btcs
exports.index = function(req, res) {
  Btc.find({deployment_id:req.query.deployment_id},function (err, btcs) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(btcs);
  });
};

// Get a single btc
exports.show = function(req, res) {
  Btc.findById(req.params.id, function (err, btc) {
    if(err) { return handleError(res, err); }
    if(!btc) { return res.status(404).send('Not Found'); }
    return res.json(btc);
  });
};

// Creates a new btc in the DB.
exports.create = function(req, res) {
  Btc.create(req.body, function(err, btc) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(btc);
  });
};

// Updates an existing btc in the DB.
exports.update = function(req, res) {
  Btc.update({deployment_id:req.body.deployment_id,_id:req.body._id},req.body,function(err,numAffected){
    if(numAffected) 
      res.status(200).send({status:'updated'});
    else
      return handleError(res,{status:'error'});
  })
};

// Deletes a btc from the DB.
exports.destroy = function(req, res) {
  Btc.findOne({deployment_id:req.query.deployment_id,_id:req.query._id},function (err, btc) {
    if(err) { return handleError(res, err); }
    if(!btc) { return res.status(404).send('Not Found'); }
    btc.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}