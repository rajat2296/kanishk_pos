'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BtcSchema = new Schema({
  deployment_id: Schema.Types.ObjectId,
  tenant_id:Schema.Types.ObjectId,
  companyName: String,
  address1: String,
  address2:String,
  contactPerson:{},
  createdBy:String,
  lastModified:{}
});

module.exports = mongoose.model('Btc', BtcSchema);