/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Btc = require('./btc.model');

exports.register = function(socket) {
  Btc.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Btc.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('btc:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('btc:remove', doc);
}