'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EmailerSchema = new Schema({
    deployment_id:Schema.Types.ObjectId,
    deploymentName:String,
    emails:String,
    mobiles:String,
    contactName:String,
    startDate:{type:Date},
    endDate:{type:Date},
    timeStamp:{type:Date,default:Date.now},
    traffic:[],
    topItemsAmount:[],
    tBillCount:Number,
    amount:{},
    customer:{},
    topWaiter:[],
    topDeliveryBoy:[],
    topItems:[]
});

module.exports = mongoose.model('Emailer', EmailerSchema);