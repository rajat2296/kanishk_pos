/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Emailer = require('./emailer.model');

exports.register = function(socket) {
  Emailer.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Emailer.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('emailer:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('emailer:remove', doc);
}