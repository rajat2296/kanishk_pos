'use strict';

var _ = require('lodash');
var Emailer = require('./emailer.model');
var Bill= require('../bill/bill.model');
var Deployment=require('../deployment/deployment.model');
var utils=require('../Utils/utils');
var eBody=require('../Utils/emailerBody');
var email   = require("emailjs/email");
var async=require('async');
var dateFormat = require('dateformat');
var moment=require('moment');
/*var server  = email.server.connect({
 user:    "ranjeet.sinha@posist.com",
 password:"ranjeet@123",
 host:    "smtp.gmail.com",
 ssl:     true
 });*/
var server  = email.server.connect({
    user:    "posist",
    password:"pos123ist",
    host:    "smtp.sendgrid.net",
    ssl:     true
});
function emailSend( body){

    //console.log(req.body);
    server.send({
        text: body.messageText,
        from: "Posist Info <no-reply@posist.info>",
        to: body.to,
        subject: body.messageText,
        attachment: [
            {data: body.messageHtml, alternative: true}
        ]
    }, function (err, message) {
        /*console.log(message);*/
        // console.log(err);
        //res.json(201, err || message);
    });

}
// Get list of emailers
exports.index = function(req, res) {
    Emailer.find(function (err, emailers) {
        if(err) { return handleError(res, err); }
        return res.json(200, []);
    });
};

exports.getBillForEmailer=function(req,res) {
    //console.log(req.query.date);
//    utils.getBillGTotalForAllDeployments().then(function(depWiseSale) {
    // return   res.json(200, depWiseSale);

    var itemsall;
    var date = req.query.date == undefined ? new Date() : new Date(req.query.date);
    Deployment.find({}, function (err, deployments) {
        if (err) {
            return handleError(res, err)
        }
        async.eachSeries(deployments, function (deployment, callback) {
            if (deployment.emails == undefined) {
              callback();
              return;
            }
            else {
                if (deployment.emails == '') {
                    callback();
                    return;
                    //  console.log('calling callback');
                }
            }
            //  console.log(deployment.emails);
            var body = {
                messageText: 'Quick Round Email for ' + deployment.name,
                to: deployment.emails,
                messageHtml: ''
            };

            var resetDate = utils.getResetDate(deployment.settings, date);
            //console.log(resetDate);
            //callback();
            var condition = {
                deployment_id: deployment._id,
                _created: {
                    $gte: moment(resetDate).add(-8, 'days').toDate(),
                    $lte: moment(resetDate).add(-7, 'days').toDate()
                },
                isDeleted: {$ne: true},
                isVoid: {$ne: true}
            };
            Bill.find({
                deployment_id: deployment._id,
                _created: {
                    $gte: moment(resetDate).add(-1, 'days').toDate(),
                    $lte: moment(resetDate).toDate()
                },
                isDeleted: {$ne: true},
                isVoid: {$ne: true}
            }, {
                '_currentUser.username': 1,
                billDiscountAmount: 1,
                _created: 1,
                tabType: 1,
                _covers:1,
                '_waiter.username': 1,
                '_delivery.username': 1,
                '_kots.items.name': 1,
                '_kots.items._id': 1,
                '_kots.items.quantity': 1,
                '_kots.items.subtotal': 1,
                'aggregation.totalTax':1,
                '_kots.totalAmount': 1,
                '_kots.totalDiscount': 1,
                '_kots.taxAmount': 1,
                '_kots.isVoid':1,
                '_customer._id': 1,
                '_customer.customerId': 1
            }, function (err, bills) {
                // console.log(bills);
                if (err)return handleError(res, err);
                var customer = {all: 0, repeat: 0, new: 0,totalCount:0};
                var wdu = {waiter: [], delivery: [], user: []};
                var tabAmount = {table: 0, takeOut: 0, delivery: 0};
                var totalCovers=0;
                var timeWiseAmount = {
                    "t1": 0,
                    "t2": 0,
                    "t3": 0,
                    "t4": 0,
                    "t5": 0,
                    "t6": 0,
                    "t7": 0,
                    "t8": 0,
                    "t9": 0,
                    "t10": 0,
                    "t11": 0,
                    't12': 0,
                    't13': 0,
                    't14': 0,
                    't15': 0,
                    't16': 0,
                    't17': 0,
                    't18': 0,
                    't19': 0,
                    't20': 0,
                    't21': 0,
                    't22': 0,
                    't23': 0,
                    't24': 0
                };
                var items = [];
                var bItems = [];
                var taxTotalShow=0;
                var tBItems = {'t10': [], 't14': [], 't17': [], 't20': [], 'g23': []};
                var billWiseTotal = 0;
                var grossAmount=0;
                _.forEach(bills, function (bill) {
                    //console.log(bill);
                    billWiseTotal = 0;
                    bItems = [];
                    _.forEach(bill._kots, function (kot) {
                        if (!kot.isVoid ) {
                          //  billWiseTotal += (( kot.totalAmount) - ( kot.totalDiscount) + utils.roundNumber(kot.taxAmount, 2));
                            billWiseTotal += (( kot.totalAmount) - ( kot.totalDiscount) );
                            if (_.has(kot, 'items')) {
                                _.forEach(kot.items, function (it) {
                                    //  console.log(it);
                                    items.push(it);
                                    bItems.push(it);

                                })
                            }
                        }
                    })
                     if(!(bill.aggregation.totalTax==null||bill.aggregation.totalTax==undefined||bill.aggregation.totalTax=='')){
                        if(parseFloat( bill.aggregation.totalTax)){
                            taxTotalShow+= utils.roundNumber(parseFloat( bill.aggregation.totalTax),2);
                            }
                        }



                        if(!(bill._covers==null||bill._covers==undefined||bill._covers=='')){
                                if(parseInt(bill._covers)){

                            totalCovers+= parseInt(bill._covers);
                            }
                        }

                    //console.log(totalCovers);
                    var bTotal = utils.roundNumber((billWiseTotal - utils.roundNumber(bill.billDiscountAmount)), 2);
                    //console.log(taxTotalShow);
                    if (bill.tabType == 'table') {
                        tabAmount.table += bTotal;
                    }
                    ;
                    if (bill.tabType == 'takeout') {
                        tabAmount.takeOut += bTotal;
                    }
                    ;
                    if (bill.tabType == 'delivery') {
                        tabAmount.delivery += bTotal;
                        customer.all += 1;
                    }
                    ;
                    var billTime = new Date(bill._created).getHours();
                    // console.log(items);
                    //////////////////////// time wise amount////////////////////////////
                    if (billTime > 0 && billTime <= 1) {
                        timeWiseAmount.t1 += bTotal;
                    }
                    if (billTime > 1 && billTime <= 2) {
                        timeWiseAmount.t2 += bTotal;
                    }
                    if (billTime > 2 && billTime <= 3) {
                        timeWiseAmount.t3 += bTotal;
                    }
                    if (billTime > 3 && billTime <= 4) {
                        timeWiseAmount.t4 += bTotal;
                    }
                    if (billTime > 4 && billTime <= 5) {
                        timeWiseAmount.t5 += bTotal;
                    }
                    if (billTime > 5 && billTime <= 6) {
                        timeWiseAmount.t6 += bTotal;
                    }
                    if (billTime > 6 && billTime <= 7) {
                        timeWiseAmount.t7 += bTotal;
                    }
                    if (billTime > 7 && billTime <= 8) {
                        timeWiseAmount.t8 += bTotal;
                    }
                    if (billTime > 8 && billTime <= 9) {
                        timeWiseAmount.t9 += bTotal;
                    }
                    if (billTime > 9 && billTime <= 10) {
                        timeWiseAmount.t10 += bTotal;
                    }


                    if (billTime > 10 && billTime <= 11) {
                        timeWiseAmount.t11 += bTotal;
                    }
                    if (billTime > 11 && billTime <= 12) {
                        timeWiseAmount.t12 += bTotal;
                    }
                    if (billTime > 12 && billTime <= 13) {
                        timeWiseAmount.t13 += bTotal;
                    }
                    if (billTime > 13 && billTime <= 14) {
                        timeWiseAmount.t14 += bTotal;
                    }
                    if (billTime > 14 && billTime <= 15) {
                        timeWiseAmount.t15 += bTotal;
                    }
                    if (billTime > 15 && billTime <= 16) {
                        timeWiseAmount.t16 += bTotal;
                    }
                    if (billTime > 16 && billTime <= 17) {
                        timeWiseAmount.t17 += bTotal;
                    }
                    if (billTime > 17 && billTime <= 18) {
                        timeWiseAmount.t18 += bTotal;
                    }
                    if (billTime > 18 && billTime <= 19) {
                        timeWiseAmount.t19 += bTotal;
                    }
                    if (billTime > 19 && billTime <= 20) {
                        timeWiseAmount.t20 += bTotal;
                    }
                    if (billTime > 20 && billTime <= 21) {
                        timeWiseAmount.t21 += bTotal;
                    }
                    if (billTime > 21 && billTime <= 22) {
                        timeWiseAmount.t22 += bTotal;
                    }
                    if (billTime > 22 && billTime <= 23) {
                        timeWiseAmount.t23 += bTotal;
                    }
                    if (billTime > 23 && billTime <= 24) {
                        timeWiseAmount.t24 += bTotal;
                    }

                    ///////////////////// item wise item count////////////////////////////////////
                    if (billTime >= 10 && billTime < 14) {
                        _.forEach(bItems, function (it) {
                            tBItems.t10.push(it);
                        })
                    }
                    if (billTime >= 14 && billTime < 17) {
                        _.forEach(bItems, function (it) {
                            tBItems.t14.push(it);
                        })
                        //tBItems.t14.push(bItems);
                    }
                    if (billTime >= 17 && billTime < 20) {
                        _.forEach(bItems, function (it) {
                            tBItems.t17.push(it);
                        })
                        // tBItems.t17.push(bItems);
                    }
                    if (billTime >= 20 && billTime < 23) {
                        _.forEach(bItems, function (it) {
                            tBItems.t20.push(it);
                        })
                        // tBItems.t20.push(bItems);
                    }
                    if (billTime >= 23 && billTime < 24) {
                        _.forEach(bItems, function (it) {
                            tBItems.g23.push(it);
                        })
                        //tBItems.g23.push(bItems);
                    }
                    if (billTime >= 0 && billTime < 10) {
                        _.forEach(bItems, function (it) {
                            tBItems.g23.push(it);
                        })
                        //tBItems.g23.push(bItems);
                    }
                    ////////////////////////// top w d u////////////////////////////////////
                    // var _b=bill;
                    // if (_.has(bill, '_delivery')) {
                    if (_.has(bill._delivery, 'username')) {
                        wdu.delivery.push(bill._delivery.username)
                    }
                    // }
                    ;
                    // if (_.has(bill, '_waiter')) {
                    if (_.has(bill._waiter, 'username')) {
                        wdu.waiter.push(bill._waiter.username)
                    }
                    // }
                    ;
                    // if (_.has(_b, '_currentUser')) {
                    if (_.has(bill._currentUser, 'username')) {
                        wdu.user.push(bill._currentUser.username)
                    }
                    // }
                    ;
                    ////////////////////////////////customer/////////////////////////////////


                    //Changed by prabh

                  if(!(bill._customer == undefined || bill._customer.customerId == undefined)){
                    customer.totalCount += 1;
                  }
                    if(bill.tabType=='delivery'){
                        if (!(bill._customer == undefined || bill._customer.customerId == undefined)) {
                            if (bill._customer._id == undefined) {
                                customer.new += 1;
                            } else {
                                customer.repeat += 1;
                            }
                        }
                    }


                })

                /*Consolidate item time wise*/
                if (tBItems.t10.length > 0) {
                    tBItems.t10 = getConsolidatedQty(tBItems.t10, false);
                }
                if (tBItems.t14.length > 0) {
                    tBItems.t14 = getConsolidatedQty(tBItems.t14, false);
                }
                if (tBItems.t17.length > 0) {
                    tBItems.t17 = getConsolidatedQty(tBItems.t17, false);
                }
                if (tBItems.t20.length > 0) {
                    tBItems.t20 = getConsolidatedQty(tBItems.t20, false);
                }
                if (tBItems.g23.length > 0) {
                    tBItems.g23 = getConsolidatedQty(tBItems.g23, false);
                }
                /*Consolidate all items */
                // var   _items = getConsolidatedQtyT(items, true);
                /*Consolidate top w d u*/
                // console.log(wdu);
                wdu.waiter = consolidatedWDU(wdu.waiter);
                wdu.delivery = consolidatedWDU(wdu.delivery);
                wdu.user = consolidatedWDU(wdu.user);
                var tAmount = tabAmount.table + tabAmount.takeOut + tabAmount.delivery;
                grossAmount= tAmount + taxTotalShow;
                //console.log(grossAmount);

                var lastDayAmount = 0;
                /* body = {
                 messageText: 'Quick Round Email for '+deployment.name,
                 to: deployment.emails,
                 messageHtml: html
                 };*/
                // for(var i=0;i<50;i++)
                // {body.messageText=body.messageText+ i.toString();  emailSend(body);}
                // emailSend(body);
                utils.getBillGTotalForEachDeployments(condition).then(function (gt) {
                    var data = {
                        deploymentName: deployment.name,
                        tabs: tabAmount,
                        topItems: getConsolidatedQty(items, true),
                        topBItems: tBItems,
                        wdu: wdu,
                        timeWiseAmount: timeWiseAmount,
                        customer: customer,
                        billsCount: bills.length,
                        totalCovers:totalCovers,
                        taxTotalShow:taxTotalShow,
                        grossAmount:grossAmount,
                        tAmount: tAmount,
                        lastDayAmount:gt.length==0?0: Math.round( gt[0].gTotal),
                        startDate: moment(date).add(-1, 'days').toDate() //new Date( date.setDate(date.getDate()-1))
                    };
                    var html = eBody.createBody(data);
                    body.messageHtml = html;


                    server.send({
                        text: body.messageText,
                        from: "Posist Support <support@posist.com>",
                        to: body.to,
                        subject: body.messageText,
                        attachment: [
                            {data: body.messageHtml, alternative: true}
                        ]
                    }, function (err, message) {

                        // console.log(err);
                        //res.json(201, err || message);
                        callback();
                    });

                })

            });
            /*bills api end here*/
            // res.json(200, itemsall);


        }, function () {
            res.json(200, {status: 'done'});
        });
        /*async finished*/
    });
    // })
}
function consolidatedWDU(arr){
    var tempArr = [];
    var uiqW = _.uniq(arr);
    _.forEach(uiqW, function (u) {
        var count = 0;
        _.forEach(arr, function (w) {
            if (w == u) count++;
        });
        var tempOb = {name: u, "count": count};
        tempArr.push(tempOb);
    })
    var sWArr = _.sortBy(tempArr, ['count']).reverse();
    var loopCount = sWArr.length > 3 ? 3 : sWArr.length;
    var fArr = [];
    for (var i = 0; i < loopCount; i++) {
        fArr.push(sWArr[i]);
    }
    return fArr;
}

function getConsolidatedQty(itemsArr,isAll){

    var uniqItems= _.uniq(itemsArr,'_id');
    var topItems = [];
    var sItems =[];
    _.forEach(uniqItems, function (ui) {
        var tempQuantity = 0;
        var tempSubTotal = 0;
        _.forEach(itemsArr, function (_fi) {
            if(_fi._id==ui._id)
                tempQuantity += _fi.quantity;
            // tempSubTotal+=_fi.subtotal;
        })
        //   ui.quantity = tempQuantity;
        var itemOb={name:ui.name,quantity:tempQuantity};
        sItems.push(itemOb);
    });

    var count = 0;
    sItems= _.sortBy(sItems, ['quantity']).reverse();
    if (isAll) {

        count = sItems.length > 10 ? 10 : sItems.length;
        //count = sItems.length
    } else {
        //sItems=  _.sortBy(sItems, ['quantity']).reverse();
        count = sItems.length > 5 ? 5 : sItems.length;
    }

    for (var i = 0; i < count; i++) {
        topItems.push(sItems[i]);
    }

    // console.log(topItems);
    return topItems;
}

// Get a single emailer
exports.show = function(req, res) {
    Emailer.findById(req.params.id, function (err, emailer) {
        if(err) { return handleError(res, err); }
        if(!emailer) { return res.send(404); }
        return res.json(emailer);
    });
};

// Creates a new emailer in the DB.
exports.create = function(req, res) {
    Emailer.create(req.body, function(err, emailer) {
        if(err) { return handleError(res, err); }
        return res.json(201, emailer);
    });
};

// Updates an existing emailer in the DB.
exports.update = function(req, res) {
    if(req.body._id) { delete req.body._id; }
    Emailer.findById(req.params.id, function (err, emailer) {
        if (err) { return handleError(res, err); }
        if(!emailer) { return res.send(404); }
        var updated = _.merge(emailer, req.body);
        updated.save(function (err) {
            if (err) { return handleError(res, err); }
            return res.json(200, emailer);
        });
    });
};

// Deletes a emailer from the DB.
exports.destroy = function(req, res) {
    Emailer.findById(req.params.id, function (err, emailer) {
        if(err) { return handleError(res, err); }
        if(!emailer) { return res.send(404); }
        emailer.remove(function(err) {
            if(err) { return handleError(res, err); }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    return res.send(500, err);
}
