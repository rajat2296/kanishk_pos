'use strict';

var User = require('./user.model');
var mongoose = require('mongoose');
//var ChangeSet = require('./changeSet.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var _ = require('lodash');
var sql = require('mssql');
var moment = require('moment');
var async=require('async');
var AttendanceRecord = require('../attendanceReport/attendanceRecord.model');
var mongoose = require('mongoose')
var validationError = function (res, err) {
  return res.json(422, err);
};

exports.getActivationLink = function (req, res, next) {
  console.log(req.query);
  User.findById(req.query.authId, function (err, user) {
    if (err) return res.send(500, err);
    res.json(200, user);
  });
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {

  User.find({}, '-salt -hashedPassword', function (err, users) {

    if (err) return res.send(500, err);
    res.json(200, users);
  });
};
exports.getUsersWaitersByTenant = function (req, res) {
  console.log('get waiters');
  var tenant_id;
  if(req.query.tenant_id === null) { return res.status(500).json({error: 'Users not found'}); }
  tenant_id = req.query.tenant_id;
  console.log(tenant_id);
  User.find({tenant_id: tenant_id, "selectedRoles.name": "Waiter"},{_id:1,name:1,username:1,firstname:1,lastname:1,deployment_id:1}, function (err, users) {
    if(err) return res.status(500).send(err);
    res.status(200).json(users);
  });
};
/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  //console.log(req.body);
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.authId = '';
  newUser.save(function (err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60 * 12 });
    res.json({ token: token, user:user });
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(404);
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
  User.findByIdAndRemove(req.params.id, function (err, user) {
    if (err) return res.send(500, err);
    return res.send(204);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if (user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function (err) {
        if (err) return validationError(res, err);
        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};

exports.update = function (req, res) {

  /*if (req.body._id) {
   delete req.body._id;
   }*/
  User.findById(req.params.id, function (err, user) {
    if (err) {
      return handleError(res, err);
    }
    if (!user) {
      return res.send(404);
    }

    var updated = _.extend(user, req.body);

    updated.save(function (err, user) {
      console.log(err);
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, user);
    });
  });
};

/**
 * Get my info
 */
exports.me = function (req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.json(404);
    res.json(user);
  });
};

exports.all = function (req, res, next) {
  var paramsquery;
  //console.log(req.query.lastSynced);
  if(req.query.lastSynced==undefined) {
    paramsquery= {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id,role:req.query.role}

  }
  else{
    paramsquery={tenant_id: req.query.tenant_id,role:req.query.role, deployment_id: req.query.deployment_id, updated: { $gte: new Date(req.query.lastSynced),$lt:new Date(req.query.currentSyncTime)}}
  }

  User.find(paramsquery, '-salt -hashedPassword  -authId', function (err, users) {
    // console.log(err);
    if (err) return res.send(500, err);
    res.json(200, users);
  });
};

exports.syncAll = function (req, res, next) {
  var paramsquery;
  //console.log(req.query.lastSynced);
  if(req.query.lastSynced==undefined) {
    paramsquery= {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id,role:req.query.role}

  }
  else{
    paramsquery={tenant_id: req.query.tenant_id,role:req.query.role, deployment_id: req.query.deployment_id, updated: { $gte: new Date(req.query.lastSynced),$lt:new Date(req.query.currentSyncTime)}}
  }

  User.find(paramsquery, '-salt -hashedPassword  -authId -selectedPermissions' , function (err, users) {
    // console.log(err);
    if (err) return res.send(500, err);
    res.json(200, users);
  });
};

exports.getUserByCodeDep = function (req, res, next) {
  var pQuery;
  if(req.query.deployment_id==null){return res.send(500, {eror:'User not found.'});}
  pQuery={ deployment_id: req.query.deployment_id,passcode:req.query.code }
  User.find(pQuery,'-salt -hashedPassword  -authId', function (err, users) {
//console.log(users);
    if (err) return res.send(500, err);
    res.json(200, users);
  });
};

exports.getUsersAttendance = function (req, res, next) {
  //console.log(req.query)
  var paramsquery;
  paramsquery= {tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id), deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id), LogDate:{$lte:new Date(req.query.toDate), $gt:new Date(req.query.fromDate)}};
  console.log(paramsquery)
  AttendanceRecord.aggregate([
      {$match: paramsquery},
      
      {
        $group: {
          //  _id: {customer_id: {$cond:["$_customer.customerId","$_customer.customerId",'']}},
          _id: "$userId",
          punchOut:{$max:"$LogDate"},
          punchIn:{$min:"$LogDate"},
          username:{$first:"$username"},
          firstname:{$first:"$firstname"},
          email:{$first:"$email"},
          mobile:{$first:"$mobile"},
          shifts:{$first:"$shifts"},
          userId:{$first:"$userId"}
        }
      }
    ], function (err, result) {
      console.log(err)
      console.log(result)
      res.json(200, result);
    });
};

exports.getSingleUserAttendance = function (req, res, next) {
  var paramsquery;
  paramsquery= {userId:req.query.userId, tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id), deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id), LogDate:{$lte:new Date(req.query.toDate), $gt:new Date(req.query.fromDate)}};
  console.log(paramsquery)
  AttendanceRecord.aggregate([
      {$match: paramsquery},
      {
        $project:{
          groupDate : {"$concat": [
              {"$substr": [{"$year": "$LogDate"}, 0, 4]},
              '-',
              {
                "$cond": [
                  {"$lte": [{"$month": "$LogDate"}, 9]},
                  {
                    "$concat": [
                      "0",
                      {"$substr": [{"$month": "$LogDate"}, 0, 2]},
                    ]
                  },
                  {"$substr": [{"$month": "$LogDate"}, 0, 2]}
                ]
              },
              '-',
              {
                "$cond": [
                  {"$lte": [{"$dayOfMonth": "$LogDate"}, 9]},
                  {
                    "$concat": [
                      "0",
                      {"$substr": [{"$dayOfMonth": "$LogDate"}, 0, 2]},
                    ]
                  },
                  {"$substr": [{"$dayOfMonth": "$LogDate"}, 0, 2]}
                ]
              }
            ]
          },
          LogDate:"$LogDate",
          punchOut:"$LogDate",
          punchIn:"$LogDate",
          username:"$username",
          firstname:"$firstname",
          email:"$email",
          mobile:"$mobile",
          shifts:"$shifts",
          userId:"$userId"

        }
      },
      
      {
        $group: {
          //  _id: {customer_id: {$cond:["$_customer.customerId","$_customer.customerId",'']}},
          _id: "$groupDate",
          punchOut:{$max:"$LogDate"},
          punchIn:{$min:"$LogDate"},
          username:{$first:"$username"},
          firstname:{$first:"$firstname"},
          email:{$first:"$email"},
          mobile:{$first:"$mobile"},
          shifts:{$first:"$shifts"},
          userId:{$first:"$userId"}
        }
      },
      {
          $sort : {_id:-1}
        }
    ], function (err, result) {
      console.log(err)
      console.log(result)
      res.json(200, result);
    });
 
};
/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
  res.redirect('/');
};

function handleError(res, err) {
  return res.send(500, err);
}
