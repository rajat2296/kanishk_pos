'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

/*router.get('/', auth.hasRole(['admin', 'superadmin', 'user']), controller.index);*/
router.get('/getActivationLink', controller.getActivationLink);
router.post('/:id', controller.update);
router.get('/', controller.index);
router.delete('/:id', auth.hasRole('superadmin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/all', auth.isAuthenticated(), controller.all);
router.get('/syncAll', auth.isAuthenticated(), controller.syncAll);
router.get('/getUsersAttendance', auth.isAuthenticated(), controller.getUsersAttendance);
router.get('/getSingleUserAttendance', auth.isAuthenticated(), controller.getSingleUserAttendance);
router.get('/getUserByCodeDep',auth.isAuthenticated(),  controller.getUserByCodeDep);
router.get('/getUsersWaitersByTenant',auth.isAuthenticated(),  controller.getUsersWaitersByTenant);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);

module.exports = router;
