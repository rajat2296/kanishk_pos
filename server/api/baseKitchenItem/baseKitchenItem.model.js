'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
var BaseKitchenItemSchema = new Schema({  
  itemName:String,
  itemId : String,
  units: [],
  tenant_id: Schema.Types.ObjectId,
  preferedUnit:{},
  category:{},
  deployment_id:String,
  toDeployment_id: String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now}
});

BaseKitchenItemSchema.index({itemName:1,itemId:1},{unique:true, dropDups :true});
BaseKitchenItemSchema.set('versionKey', false);
BaseKitchenItemSchema.path('itemId').validate(function(v, respond) {
  var self=this;
  console.log(self);
  console.log(v);
    this.constructor.findOne({'itemId': v,deployment_id:this.deployment_id,toDeployment_id:this.toDeployment_id}, function (err, storeitems) {
    
    var flag=true;
    if(err){throw err;}
    console.log(storeitems);
    if(storeitems)
    {
      // if(storeitems.itemName.toLowerCase()===self.itemName.toLowerCase())
      // {
      //   if(storeitems.itemId==self.itemId){
      //     flag=false;
      //   }
      // }
      flag=false;
    }
  
    respond(flag);
    });
  
}, 'This Item is already used please try  with another name !');

module.exports = mongoose.model('baseKitchenItem', BaseKitchenItemSchema);

