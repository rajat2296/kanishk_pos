'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var baseKitchenItemCtrlStub = {
  index: 'baseKitchenItemCtrl.index'
};

var routerStub = {
  get: sinon.spy()
};

// require the index with our stubbed out modules
var baseKitchenItemIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './baseKitchenItem.controller': baseKitchenItemCtrlStub
});

describe('BaseKitchenItem API Router:', function() {

  it('should return an express router instance', function() {
    baseKitchenItemIndex.should.equal(routerStub);
  });

  describe('GET /api/baseKitchenItems', function() {

    it('should route to baseKitchenItem.controller.index', function() {
      routerStub.get
        .withArgs('/', 'baseKitchenItemCtrl.index')
        .should.have.been.calledOnce;
    });

  });

});
