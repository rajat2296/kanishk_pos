/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/baseKitchenItems              ->  index
 */

'use strict';

// // Gets a list of BaseKitchenItems
// exports.index = function(req, res) {
//   res.json([]);
// };

var _ = require('lodash');
var BaseKitchenItem = require('./baseKitchenItem.model');
var Deployment = require ('../deployment/deployment.model');
var ObjectId = require('mongoose').Types.ObjectId;

// Get list of BaseKitchenItems
exports.index = function(req, res) {
  BaseKitchenItem.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}, function (err, BaseKitchenItems) {
    if(err) { return handleError(res, err); }
    //console.log("BASE KITCHEN ITEMS", BaseKitchenItems)
    return res.json(200, BaseKitchenItems);
  });
};

exports.getUniqueBaseKitchenItems = function (req, res) {

  
  Deployment.aggregate([
    {
      $match:{
        isBaseKitchen: true,
        //tenant_id: new ObjectId(req.query.tenant_id)
        tenant_id: new ObjectId(req.query.tenant_id)
      }
    },
    {
      $group: {
        _id: null,
        deps: {$addToSet: "$_id"}
      }
    },
    {
      $project: {
        deps: 1,
        _id: 0
      }
    }
  ], function (err, deps) {
    console.log("---------------------------------")
    console.log('deps', err, deps[0]);
    if(err)
      return handleError(res, err);
    if(deps[0]){
        BaseKitchenItem.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id, toDeployment_id: {$in: deps[0].deps}}, function (err, baseKitchenItems) {
        console.log('bki', err, baseKitchenItems.length);
        if(err)
            return handleError(res, err);
        return res.status(200).json(baseKitchenItems);
        })
    } else {
        return res.status(200).json([]);
    }
  })
}



// Get a single BaseKitchenItem
exports.show = function(req, res) {
  BaseKitchenItem.findById(req.params.id, function (err, BaseKitchenItem) {
    if(err) { return handleError(res, err); }
    if(!BaseKitchenItem) { return res.send(404); }
    return res.json(BaseKitchenItem);
  });
};

exports.baseKitchenItemsByDeployment = function(req, res) {
  console.log(req.query)
  BaseKitchenItem.find({deployment_id:req.query.toDeployment_id}, function (err, BaseKitchenItem) {
    if(err) { return handleError(res, err); }
    if(!BaseKitchenItem) { return res.send(404); }
    console.log(BaseKitchenItem);
    return res.json(200,BaseKitchenItem);
  });
};

// Creates a new BaseKitchenItem in the DB.
exports.create = function(req, res) {

  console.log('Helloooooooooo');
  BaseKitchenItem.create(req.body, function(err, BaseKitchenItem) {
    if(err)
    {
      return res.json({error:err});
      //return handleError(res, err);
    }
    else
    {
      return res.json(201, BaseKitchenItem);
    }
  });
};

// Updates an existing BaseKitchenItem in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  BaseKitchenItem.findById(req.params.id, function (err, BaseKitchenItem) {
    if (err)
    {
      return handleError(res, err);
    }
    if(!BaseKitchenItem) { return res.send(404); }
    //var updated = _.merge(BaseKitchenItem, req.body);
    var updated = _.extend(BaseKitchenItem, req.body);
    updated.markModified("units");
    updated.markModified("itemName");
    updated.markModified("preferedUnit");
    updated.markModified("assignedToStore");
    updated.save(function (err) {
      if (err) {
        //return handleError(res, err);
        return res.json({error:err});
      }
      return res.json(200, BaseKitchenItem);
    });
  });
};

// Deletes a BaseKitchenItem from the DB.
exports.destroy = function(req, res) {
  BaseKitchenItem.findById(req.params.id, function (err, BaseKitchenItem) {
    if(err) { return handleError(res, err); }
    if(!BaseKitchenItem) { return res.send(404); }
    BaseKitchenItem.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.deleteBaseKitchenItems = function(req, res) {
  BaseKitchenItem.remove({itemId: req.params.id}, function (err) {
    if(err) { return handleError(res, err); }
    return res.send(204);
  });
};


exports.deleteDuplicates = function (req, res) {
  var duplicates = [];

      BaseKitchenItem.aggregate([
        { $group: { 
          _id: { name: "$itemName", itemId: '$itemId'}, // can be grouped on multiple properties 
          dups: { "$addToSet": "$_id" }, 
          count: { "$sum": 1 } 
        }}, 
        { $match: { 
          count: { "$gt": 1 }    // Duplicates considered as count greater than one
        }}
      ], function(docs){
        _.forEach(docs,function(doc) {
          doc.dups.shift();      // First element skipped for deleting
          doc.dups.forEach( function(dupId){ 
              duplicates.push(dupId);   // Getting all duplicate ids
              }
          )    
      })
        BaseKitchenItem.remove({_id:{$in:duplicates}}) 
      })               // You can display result until this and check duplicates 
      

      // If you want to Check all "_id" which you are deleting else print statement not needed
      //printjson(duplicates);     

      // Remove all duplicates in one go    
    
}

exports.saveAll = function (req, res) {
  console.log('saveAll', req.body);
  var bKItems = req.body.bKItems;
  console.log(bKItems);
  BaseKitchenItem.collection.insert(bKItems, null, function (err, docs){
    console.log('bulk');
    if(err) {
      console.log(err);
      return handleError(res, err);
    }
    return res.status(200).json(docs);
  });
};

exports.updateBaseKitchenItemUnit = function (req, res) {

  console.log('api hit successful');
  var itemId = req.body._id;
  var tenant_id = req.body.tenant_id;
  console.log(req.body.unit);
  BaseKitchenItem.update({tenant_id: tenant_id, itemId: itemId}, {$set: {preferedUnit: req.body.unit}, units: [req.body.unit]}, {multi: true}, function (err, updated){
    if(err){
      console.log(err);
      return handleError(err, res);
    }
    console.log('updated', updated);
    return res.send(200, "done");
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
