'use strict';

var express = require('express');
var controller = require('./baseKitchenItem.controller');

var router = express.Router();

// router.get('/', controller.index);
router.get('/', controller.index);

router.get('/deleteDuplicates', controller.deleteDuplicates);
router.get('/baseKitchenItemsByDeployment', controller.baseKitchenItemsByDeployment);
router.get('/getUniqueBaseKitchenItems', controller.getUniqueBaseKitchenItems);
router.post('/', controller.create);
router.post('/saveAll', controller.saveAll);
router.get('/:id', controller.show);
router.put('/updateBaseKitchenItemUnit/:id', controller.updateBaseKitchenItemUnit);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/deleteBaseKitchenItems/:id', controller.deleteBaseKitchenItems);
router.delete('/:id', controller.destroy);

module.exports = router;
