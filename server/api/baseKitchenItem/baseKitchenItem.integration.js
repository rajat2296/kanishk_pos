'use strict';

var app = require('../..');
var request = require('supertest');

describe('BaseKitchenItem API:', function() {

  describe('GET /api/baseKitchenItems', function() {
    var baseKitchenItems;

    beforeEach(function(done) {
      request(app)
        .get('/api/baseKitchenItems')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          baseKitchenItems = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      baseKitchenItems.should.be.instanceOf(Array);
    });

  });

});
