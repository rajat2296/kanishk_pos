'use strict';

var _ = require('lodash');
var moment=require('moment');
var Offer = require('./offer.model');
var utils=require('../Utils/utils');
var mongoose=require('mongoose');
var request = require('request');
var config = require('../../config/environment');



// Get list of offers
exports.index = function(req, res) {
  if(config.isQueue){

  request(config.reportServer.url+'/api/offers?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  var paramsquery;
  //console.log(req.query.lastSynced);
  if(req.query.tenant_id==null){return handleError(res, "error:'no items found'");}
  if(req.query.lastSynced==undefined) {
    paramsquery= {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
  } else{
    paramsquery={tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id, updated: { $gte: new Date(req.query.lastSynced),$lt:new Date(req.query.currentSyncTime)}}
  }
  Offer.find(paramsquery,function (err, offers) {
    if(err) { return handleError(res, err); }
    return res.json(200, offers);
  });
 }
};

exports.getOffersWithIds = function(req, res) {
if(config.isQueue){

  request(config.reportServer.url+'/api/offers/getOffersWithIds?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  var paramsquery;
  var offerOb={offers:[],ids:[]};
  //console.log(req.query.lastSynced);
  if(req.query.tenant_id==null){return handleError(res, "error:'no items found'");}
  if(req.query.lastSynced==undefined) {
    paramsquery= {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
  } else{
    paramsquery={tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id, updated: { $gte: new Date(req.query.lastSynced),$lt:new Date(req.query.currentSyncTime)}}
  }
  Offer.find({deployment_id:req.query.deployment_id},{_id:1},function (err, ids) {
    if(!err) { offerOb.ids=ids }
    Offer.find(paramsquery,function (err, offers) {
      if(!err) { offerOb.offers=offers;  }
      return res.json(200, offerOb);
    });
  });
 }
};

exports.getOnlineOfferByCode=function(req,res){
  //console.log(req.query);
  var validatedOffer=[];
  Offer.find({deployment_id:req.query.dep_id,$or:[{'type.name':'percent'},{'type.name':'amount'}]},{},function(err,offerCodes){
    // Offer.find({deployment_id:req.query.dep_id},{code:1},function(err,offerCodes){
    if(err){return handleError(req,res);}
    // console.log(offerCodes);
    if(offerCodes){
      var allOffers=offerCodes;
      _.forEach(allOffers,function(offer){
        var status= utils.validateCode(req.query.code,offer);
        // console.log(status);
        if(status) {
          if (utils.offerValidated(offer)) {
            validatedOffer.push(offer);
          }
        }
      })
      return  res.send(200,validatedOffer)
    }
  })
}

exports.getAllOnlineOffer=function(req,res){
  console.log('getAllOnlineOffer', req.query);
  var validatedOffer=[];
  Offer.find({deployment_id:req.query.dep_id, $or:[{'type.name':'percent'},{'type.name':'amount'}],tabs: 'Online', 'code.codeList': {$exists: false}},{},function(err,offerCodes){
    // Offer.find({deployment_id:req.query.dep_id},{code:1},function(err,offerCodes){
    if(err){return handleError(res,err);}
    // console.log(offerCodes);
    if(offerCodes){
      var allOffers=offerCodes;
      _.forEach(allOffers,function(offer){
        var status = offer.tabs.indexOf('Online');
        // console.log(status);
        if(status) {
          if (utils.offerValidated(offer)) {
            validatedOffer.push(offer);
          }
        }
      })
      return  res.send(200,validatedOffer)
    }
  })
}

/*Integration of offer codes by ddeployment*/
exports.getOnlineOfferCodeByDeployment=function(req,res){
  //console.log(req.query);
  var validatedOffer=[];

  Offer.find({deployment_id:new mongoose.Types.ObjectId(req.query.deployment_id)},function(err,offers){
    // Offer.find({deployment_id:req.query.dep_id},{code:1},function(err,offerCodes){
    if(err){return handleError(req,res);}
    // console.log(offerCodes);
    if(offers){
      var allOffers=offers;
      _.forEach(allOffers,function(offer){
        if (utils.offerValidated(offer)) {
          var validatedCodes = [];
          if (offer.code.codeList != undefined) {
            for (var i = 1; i <= offer.code.codeList.noOfCodes + 1; i++) {
              var code = offer.code.codeList.codePrefix + i.toString();
              var status = utils.validateCode(code, offer);
              // console.log(status);
              if (status) {
                // if (utils.offerValidated(offer)) {
                //validatedOffer.push(offer);
                validatedCodes.push(code);
                // }
              }
            }
            if(validatedCodes.length>0)
              validatedOffer.push({name:offer.name,codes:validatedCodes});
          }
        }
      })

      return  res.send(200,validatedOffer)
    }
  })
}


exports.findByName = function(req, res) {
  var Offers=[];
  Offer.findOne({ deployment_id: req.query.deployment_id,name:req.query.name},function (err, offer) {
    if(err) { return handleError(res, err); }
    if(offer){Offers.push(offer)};
    return res.json(200, Offers);
  });
};
// Count offer having codelist or datelist
exports.countOffer=function(req,res){
  var query={};
  //console.log(req.query);
  if(req.query.isCode=='true'){
    //  query={"code.codeList._id":req.query.Id};
    Offer.count({ deployment_id: req.query.deployment_id,"code.codeList._id":req.query.Id},function (err, cnt) {
      if(err) { return handleError(res, err); }
      return res.json(200,{count:cnt});
    });
  }else{
    //query={ "code.valid.dateLists._id":req.query.Id};
    //console.log(req.query.Id);
    Offer.count({ deployment_id: req.query.deployment_id,"valid.dateLists._id":req.query.Id},function (err, cnt) {
      if(err) { return handleError(res, err); }
      return res.json(200,{count:cnt});
    });
  }
};

// Get a single offer
exports.show = function(req, res) {
  Offer.findById(req.params.id, function (err, offer) {
    if(err) { return handleError(res, err); }
    if(!offer) { return res.send(404); }
    return res.json(offer);
  });
};

// Creates a new offer in the DB.
exports.create = function(req, res) {
  console.log(req.body);
  Offer.create(req.body, function(err, offer) {
    if(err) { return handleError(res, err); }
    return res.json(201, offer);
  });
 /*Offer.collection.insert(req.body, null, function (err, offer) {
   if (err) {
   return handleError(res,err);
   }
   return res.json(201, offer);
   });*/
};

// Updates an existing offer in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  req.body.updated=new Date();
  Offer.findById(req.params.id, function (err, offer) {
    if (err) { return handleError(res, err); }
    if(!offer) { return res.send(404); }
    var updated = _.extend(offer, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, offer);
    });
  });
};

// Deletes a offer from the DB.
exports.destroy = function(req, res) {
  Offer.findById(req.params.id, function (err, offer) {
    if(err) { return handleError(res, err); }
    if(!offer) { return res.send(404); }
    offer.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

// OPEN API Functions @author - Shubhank
exports.getOnlineOfferByCode_secured=function(req,res){
  //console.log(req.query);
  var validatedOffer=[];
  Offer.find({deployment_id:req.query.deployment_id ,$or:[{'type.name':'percent'},{'type.name':'amount'}]},'-deployment_id -tenant_id -updated -created -valid',function(err,offerCodes){
    // Offer.find({deployment_id:req.query.dep_id},{code:1},function(err,offerCodes){
    if(err){return handleError(req,res);}
    // console.log(offerCodes);
    if(offerCodes){
      var allOffers=offerCodes;
      _.forEach(allOffers,function(offer){
        var status= utils.validateCode(req.query.code,offer);
        // console.log(status);
        if(status) {
          if (utils.offerValidated(offer)) {
            validatedOffer.push(offer);
          }
        }
      })
      return  res.send(200,validatedOffer)
    }
  })
};
