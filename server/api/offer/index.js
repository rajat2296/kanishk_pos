'use strict';

var express = require('express');
var controller = require('./offer.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/getOnlineOfferByCode', controller.getOnlineOfferByCode);
router.get('/getAllOnlineOffer', controller.getAllOnlineOffer);
router.get('/getOnlineOfferCodeByDeployment', controller.getOnlineOfferCodeByDeployment);
router.get('/findByName', controller.findByName);
router.get('/countOffer', controller.countOffer);
router.get('/getOffersWithIds', controller.getOffersWithIds);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;

router.use('/partner/*', auth.onlyOpenApi);
router.get('/partner/getOnlineOfferByCode', auth.hasCustomerKey ,controller.getOnlineOfferByCode);
