'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OfferSchema = new Schema({
    name: String,
    tabs: [],
    type: {},
    minBillAmount: Number,
    minItemCount: Number,
    comboAmount:Number,
    getItemOnly:Boolean,
    getLeastValueItem:Boolean,
    leastItemValueCount:{buyQuantity:Number,getQuantity:Number},
    sameOrLess: Boolean,
    applicable: {},
    offerApplied: {},
    valid: {},
    code: {},
    deployment_id: Schema.Types.ObjectId,
    tenant_id: Schema.Types.ObjectId,
    created: { type: Date, default: Date.now },
    updated:{type: Date, default: Date.now},
    complimentary:Boolean
});

module.exports = mongoose.model('Offer', OfferSchema);