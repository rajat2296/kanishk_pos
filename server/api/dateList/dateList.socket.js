/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var DateList = require('./dateList.model');

exports.register = function(socket) {
  DateList.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  DateList.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('dateList:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('dateList:remove', doc);
}