'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DateListSchema = new Schema({
      name: String,
      dates:[],
      created:{type:Date,default:Date.now},
      updated:{type:Date,default:Date.now},
      repeat:String,
      tenant_id:Schema.Types.ObjectId,
      deployment_id:Schema.Types.ObjectId
});

DateListSchema.set('versionKey', false);
DateListSchema
    .pre('save', function (next) {
        this.updated = new Date;
        next();
    });
/*
DateListSchema
    .path('name')
    .validate(function (value, respond) {
        var self = this;
        this.constructor.findOne({name:value, deployment_id:this.deployment_id}, function (err, i) {
            if (err) throw err;
            if (i) {
                if (self.name === i.name ){
                    if(!self._id.equals( i._id)){ return respond(false);}
                    return respond(true);
                    //  }
                }
            }
            respond(true);
        });

    }, 'Duplicate Date List Name, please use another name.');
*/


module.exports = mongoose.model('DateList', DateListSchema);