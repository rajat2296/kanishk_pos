'use strict';

var _ = require('lodash');
var DateList = require('./dateList.model');

// Get list of dateLists
exports.index = function(req, res) {
  DateList.find({ deployment_id: req.query.deployment_id},function (err, dateLists) {
    if(err) { return handleError(res, err); }
    return res.json(200, dateLists);
  });
};
//Get list By Name
exports.findByName = function(req, res) {
  var dateLists=[];
  DateList.findOne({ deployment_id: req.query.deployment_id,name:req.query.name},function (err, dateList) {
    if(err) { return handleError(res, err); }
    if(dateList){dateLists.push(dateList)};
    return res.json(200, dateLists);
  });
};

// Get a single dateList
exports.show = function(req, res) {
  DateList.findById(req.params.id, function (err, dateList) {
    if(err) { return handleError(res, err); }
    if(!dateList) { return res.send(404); }
    return res.json(dateList);
  });
};

// Creates a new dateList in the DB.
exports.create = function(req, res) {
  DateList.create(req.body, function(err, dateList) {
    if(err) { return handleError(res, err); }
    return res.json(201, dateList);
  });
};

exports.save = function(req, res) {
  //console.log(req.body);
  DateList.create(req.body, function(err, dateList) {
    if(err) { return handleError(res, err); }
    return res.json(201, dateList);
  });
};

// Updates an existing dateList in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  DateList.findById(req.params.id, function (err, dateList) {
    if (err) { return handleError(res, err); }
    if(!dateList) { return res.send(404); }
    var updated = _.extend(dateList, req.body);
    updated.markModified('dates');
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, dateList);
    });
  });
};

// Deletes a dateList from the DB.
exports.destroy = function(req, res) {
  console.log(req.params.id);
  DateList.findById(req.params.id, function (err, dateList) {
    if(err) { return handleError(res, err); }
    if(!dateList) { return res.send(404); }
    dateList.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}