'use strict';

var express = require('express');
var controller = require('./dateList.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/findByName', controller.findByName);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/save', controller.save);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;