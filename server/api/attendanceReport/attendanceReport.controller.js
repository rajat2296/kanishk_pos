/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var async = require('async');
var User = require('../user/user.model');
var AttendanceRecord = require('./attendanceRecord.model');
var _ = require('lodash');
var ChangeSet = require('./changeSet.model');
var sql = require('mssql');

// Get a single thing
exports.index = function(req, res) {
  
    ChangeSet.findOne({isSynced:true},{},{ sort: { 'lastTimeStamp' : -1 } },function(err,result){
        console.log(result);
        if(result && result.lastTimeStamp){
            var fromDate = new Date(result.lastTimeStamp);
        }
        else{
            var fromDate = new Date('2000-01-01')
        }
        var toDate = new Date();
          fromDate = fromDate.getFullYear()+'-'+(fromDate.getMonth()+1)+'-'+fromDate.getDate()+' '+fromDate.getHours()+':'+fromDate.getMinutes()+':00';
          toDate = toDate.getFullYear()+'-'+(toDate.getMonth()+1)+'-'+toDate.getDate()+' '+toDate.getHours()+':'+toDate.getMinutes()+':00';
          console.log(fromDate,toDate)
          var temp1 = {
                        isSynced:false,
                        lastTimeStamp:new Date()
                    }
          var lastTimeStamp = temp1.lastTimeStamp;
          ChangeSet.create(temp1,function(err,success){
              //var timeStampId = success._id;
              console.log(success)
              sql.connect("mssql://abc:abc@134.213.156.80/eTimetracklite1").then(function(result1) {
                //console.log(result1);
                var query = "select UserId, DATEADD(MINUTE,-330,LogDate) AS LogDate, DeviceId from DeviceLogs where LogDate between '" +fromDate+"' AND '"+toDate+"';"

                new sql.Request().query(query).then(function(recordset) {
                    var attendanceReport = []
                    console.log(recordset.length)
                    var uniqueUsers = []
                    for(var i in recordset){
                        uniqueUsers[recordset[i].UserId]=0;
                    }
                    console.log(Object.keys(uniqueUsers));
                    User.find({'details.userId':{$in:Object.keys(uniqueUsers)}},'-salt -hashedPassword  -authId',function(err,users){
                       // console.log(users);
                        _.forEach(users, function (user) {
                            _.forEach(recordset, function (record) {

                                if(record.UserId == user.details.userId){
                                    var temp = {
                                        firstname : user.firstname,
                                        username : user.username,
                                        email : user.details.email,
                                        mobile : user.mobile,
                                        userId : user.details.userId,
                                        LogDate : record.LogDate,
                                        DeviceId : record.DeviceId,
                                        deployment_id : user.deployment_id,
                                        tenant_id : user.tenant_id,
                                        shifts:user.details.assignedShift
                                    }

                                    attendanceReport.push(temp);

                                }
                            })
                        })

                        AttendanceRecord.create(attendanceReport,function(err,result){
                            ChangeSet.findOneAndUpdate({_id:success._id},{isSynced:true},function(err,res1){
                                console.log(err,res1)
                                res.json(200, result);
                            });              
                        })

                    })
                    
                }).catch(function(err) {
                  console.log(err) 
                });
              })
          })  
    })
};



function handleError(res, err) {
  return res.send(500, err);
}