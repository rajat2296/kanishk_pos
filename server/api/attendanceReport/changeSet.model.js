'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ChangeSetSchema = new Schema({
    isSynced : {type: Boolean, default: false},
    lastTimeStamp : {type: Date}
});


module.exports = mongoose.model('ChangeSet', ChangeSetSchema);
