'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var AttendanceRecordSchema = new Schema({
    userId:{type:String},
    username:{type:String},
    LogDate:{type:Date},
    deployment_id:{type:Schema.Types.ObjectId},
    tenant_id:{type:Schema.Types.ObjectId},
    DeviceId:{type:String},
    firstname:String,
    mobile:String,
    email:String,
    shifts:[]

});


module.exports = mongoose.model('AttendanceRecord', AttendanceRecordSchema);
