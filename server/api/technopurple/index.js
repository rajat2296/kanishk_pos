'use strict';

var express = require('express');
var controller = require('./technopurple.controller');

var router = express.Router();


router.post('/orderStatus',controller.updateStatus);
router.post('/notify',controller.notify);
router.post('/riderStatus',controller.updateDeliveryBoyLocation);
router.post('/orderDetails',controller.editOrderDetails);
router.get('/getDeliveryBoys',controller.getDeliveryBoys);
router.get('/getOrders',controller.getOrders);
router.post('/userLocation',controller.emitUserLocation);
router.get('/getUser',controller.getUser);
router.get('/getUniqueOrder',controller.getUniqueOrder);

module.exports = router;
