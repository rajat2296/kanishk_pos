'use strict';

var _ = require('lodash');
var Technopurple = require('./technopurple.model');
var Bill=require('../bill/bill.model');
var request=require('request');
var User=require('../user/user.model');
var mongoose=require('mongoose');
var io=null;

exports.updateStatus=function(req,res){
  try {
    var status = '';
    var rsn='';
    var obj={};
    obj.location={lat:req.body.lat,lon:req.body.lon};

    console.log(req.body);
    if (typeof(req.body) != 'object') return res.status(400).send({"response":"failure","error":"Invalid json","errorCode":"0"});

     if (!req.body.restaurantId) return handleError(res, {"response":"failure","error":"restaurant id missing","errorCode":"0"});                  //handleErrorCases
    if (!req.body.orderId) return handleError(res, {"response":"failure","error":"orderId missing","errorCode":"0"});
    if (!req.body.status) return handleError(res, {"response":"failure","error": 'Order Status missing',"errorCode":"0"});
    if(req.body.status=='7') return res.status(200).send({"response": "success", "error": "0"});

    if (req.body.status == '3') {obj.status = 'Accepted'; req.body.status='Accepted'; obj.acceptedTime=new Date(Number(req.body.datetime)); }
    else if (req.body.status == '4') {obj.status = 'Picked-up'; req.body.status='Picked-up'; obj.pickedUpTime=new Date(Number(req.body.datetime)); }
    else if (req.body.status == '6') {obj.status = 'Delivered';req.body.status='Delivered'; obj.deliveredTime=new Date(Number(req.body.datetime));}
    //else if (req.body.status == '7') {obj.status = 'Paid'; req.body.status='Paid'; obj.paidTime=new Date(Number(req.body.datetime));}
    else if (req.body.status == '8') {
      var reason=req.body.reason;
      if(reason=='1')  rsn='Bike Problem';
      else if(reason=='2') rsn='Personal Problem';
      else if(reason=='3') rsn='On a Break';
      else if(reason=='4') rsn="Don't know place";
      else if(reason=='5') rsn='Booked';
      obj.status = 'Rejected'+' due to '+rsn; req.body.status='Rejected'+' due to '+rsn;
        obj['$push'] ={
          rejection:{
            username:req.body.username,
            datetime:new Date(Number(req.body.datetime)),
            reason:rsn
          }
        }
      }
    obj.datetime=new Date(Number(req.body.datetime));

    console.log("updating");
    console.log(obj);
    Technopurple.findOneAndUpdate({deployment_id: req.body.restaurantId, "orderId": req.body.orderId}, obj,
       {new:true}, function (err, doc) {
      console.log("data",doc);
      if (err) return handleError(res, {message: "error in updating status"});
      if (!doc) return res.status(404).send({"response":"failure","error":'order not found', "errorCode":'0'});
      if (io)
      {io.broadcast.emit('tpOrderStatus'+req.body.restaurantId,doc); console.log("socket emitted");}
      res.status(200).send({"response": "success", "error": "0"});
    });
    /*Technopurple.findOne({deployment_id:req.body.restaurantId,"order.orderId":req.body.orderId},function(err,data){
      if(data){
        data.status=status; data.datetime=new Date(Number(req.body.datetime));
        if(status=='Accepted') data.order.acceptedTime=new Date(Number(req.body.datetime));
        if(status=='Picked-up') data.order.pickedUpTime=new Date(Number(req.body.datetime));
        if(status=='Delivered') data.order.deliveredTime=req.body.datetime;
        if(status=='Paid') data.order.paidTime=req.body.datetime;
        if(status=='Rejected') {data.order.rejection.push({reason:rsn,datetime:new Date(req.body.datetime),username:req.body.username})};

          data.save(function(err,docs){
            if(docs){
              console.log(docs);
              if(io){io.broadcast.emit('tpOrderStatus'+req.body.restaurantId,docs);console.log("socket emitted");}
              res.status(200).send({"response": "success", "error": "0"})
          }
            else return handleError(res,{"response":"failure",error:"error in updating","errorCode":"0"})
        })
      }
      else return res.status(404).send({response:"failure","error":'order not found',"errorCode":"0"});
    })*/
  }
  catch(e){
    console.log("exception",e);
    return res.status(400).send({response:"failure","error":'Failure','errorCode':'0'});
  }
};

exports.notify=function(req,res){
    var url='http://technopurple.biz/posist/tp/notify/order';
   var domain=req.host.substring(req.host.lastIndexOf('.')+1);
   if(domain=='net')
    url='http://technopurple.in/posist/tp/notify/order';

  try {
    var options = {
      method: 'POST',
      url: url,
      headers:{'content-type':'application/json',
        'accept':'application/json'},
         json:req.body
    };

    request(options, function (error, response, bod) {
      console.log(options);
      // console.log("status", response.statusCode);

      if(error) {
        console.log("error",error);
        return handleError(res, error);
      }
      if (bod) {
        console.log(bod);
        var body = typeof(bod) == 'object' ? (bod) : JSON.parse(bod);
        console.log("body",body.status,body.message);
        if (body.status == 'OK' && body.message == 'Message Accepted')
          saveOrder(req, res);
        else
          return handleError(res, {status: 'failure'});
      }
      else return handleError(res, {status: 'failure'});
    });
  }
  catch(e){
    console.log("exception",e);
    return res.status(500).send({message:'An exception occurred'});
  }
};

function saveOrder(req,res){
  var data={deployment_id:req.body.restaurantId,username:req.body.username,orderId:req.body.orderId,status:'New',datetime:req.body.datetime};
  var tp=new Technopurple(data);
  Technopurple.findOneAndUpdate({deployment_id:req.body.restaurantId,"orderId":req.body.orderId},data,{upsert:true},function(err,data){
    if(err)
      console.log(err);
    else console.log(data);
    res.status(200).send({"response":"success","error":"0", "errorCode":"0"});
  })
}

exports.updateDeliveryBoyLocation=function(req,res) {
  //handle Error Cases
  try {
    if (!req.body.username) return handleError(res, {message: 'username missing'});
    if (!req.body.lat || !req.body.lon || !req.body.action) return handleError(res, {message: 'insufficient information'});

    User.findOne({"username": req.body.username,"selectedRoles": { $elemMatch: { name: "Delivery" }}}, function (err, user) {
      if (user) {
        user.location = {};
        user.location.datetime = new Date(req.body.datetime);
        user.location.lat = req.body.lat;
        user.location.lon = req.body.lon, user.location.action = req.body.action;
        user.markModified("location");
        user.save(function (err, data) {
          if (err) return handleError(res, {response:"failure",error:'error in updating user location',errorCode:'0'});
          console.log("updated user location", data.username, data.location);
          if (io) io.broadcast.emit('tpUserLocation' + data.deployment_id, {
            username: data.username,
            location: data.location
          });
          res.status(200).send({"response": "success", "error": "0", "newOrderAllowed": "0"});
        })
      }
      else res.status(404).send({response: 'failure',"error":'user not found',errorCode:'0'});
    })

  }
  catch(e){
    console.log("exception",e);
    return res.status(400).send({response:'success','error':'invalid request','errorCode':'0'});
  }

}


exports.getDeliveryBoys=function(req,res){
  var deployment_id=mongoose.mongo.ObjectID(req.query.deployment_id);
  User.find({deployment_id:deployment_id, selectedRoles: { $elemMatch: { name: "Delivery" }}},function(err,data){
    if(data) return res.status(200).send(data);
    else return handleError(res,{status:'error'});
  })
}

exports.getOrders=function(req,res){
  console.log(typeof(req.query.orders),req.query.orders);
  var orders=req.query.orders.split(',');
  try {
    Technopurple.find({
      deployment_id: req.query.deployment_id,
      "orderId": {$in: orders}
    },function (err, data) {
      if (data) return res.status(200).send(data);
      console.log(err);
      return handleError(res, {status: 'error'});
    })
  }
  catch(e){
    console.log("exception",e);
    return res.status(400).send({message:"Invalid request"});
  }
}



exports.editOrderDetails=function(req,res){
  try {
    res.status(200).send({"response": "success", "error": "0", "errorCode": "0"});
  }
  catch(e){
    console.log("exception",e);
    return res.status(400).send({message:'Invalid request'});
  }
}

exports.emitUserLocation=function(req,res){
  try{
    if(!req.body.username||!req.body.lat||!req.body.lon) return handleError(res,{response:'failure','error':'missing information in request','errorCode':'0'})
    var username=req.body.username;
    console.log(username.slice(0,username.indexOf('_')));
    if(io)
      io.broadcast.emit('userLocation_'+username.slice(0,username.indexOf('_')),{lat:req.body.lat,lon:req.body.lon,username:req.body.username});
    return res.status(200).send({"response": "success", "error": "0", "errorCode": "0"});
  }
  catch(e){
    console.log("Exception"+e);
    return handleError(res,{response:'failure','error':'invalid request','errorCode':'0'});
  }
}

exports.getUser=function(req,res){
  try{
    var deployment_id=mongoose.mongo.ObjectID(req.query.deployment_id);
   User.findOne({"username":req.query.username,"deployment_id":req.query.deployment_id},{location:1},function(err,doc){
     if(doc && doc.lat && doc.lon){
       res.status(200).send(doc);
     }
     else return handleError(res,{status:'error'});
   })
  }
  catch(e){
    return handleError(res,{error:e});
  }
}

exports.getUniqueOrder=function(req,res){
  try{
    if(!req.query.orderId) return handleError(res,{status:'orderId missing'});
    Technopurple.findOne({"orderId":req.query.orderId},function(err,doc){
      if(doc ) return res.status(200).send(doc);
      else return handleError(res,{status:'error'});
    });
  }
  catch(e){
    return handleError(res,{error:e});
  }
}



exports.set=function(socket){
  io=socket;
};


function handleError(res, err) {
  return res.status(500).send(err);
}
