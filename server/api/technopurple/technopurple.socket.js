/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Technopurple = require('./technopurple.model');

exports.register = function(socket) {
  Technopurple.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Technopurple.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('technopurple:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('technopurple:remove', doc);
}