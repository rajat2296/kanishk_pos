'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TechnopurpleSchema = new Schema({
    deployment_id: String,
    acceptedTime:{type:Date},
    rejection:[],
    pickedUpTime:{type:Date},
    deliveredTime:{type:Date},
    paidTime:{type:Date},
    username:String,
    orderId:String,
    status:String,
    datetime:{type:Date},
    location:{}
});

module.exports = mongoose.model('Technopurple', TechnopurpleSchema);
