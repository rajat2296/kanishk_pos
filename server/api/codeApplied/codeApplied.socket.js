/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var CodeApplied = require('./codeApplied.model');

exports.register = function(socket) {
  CodeApplied.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  CodeApplied.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('codeApplied:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('codeApplied:remove', doc);
}