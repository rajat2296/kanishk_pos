'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CodeAppliedSchema = new Schema({
    offerName:String,
    offer_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    offerCode: String,
    userName:String,
    timeStamp:{ type: Date, default: Date.now }
});

module.exports = mongoose.model('CodeApplied', CodeAppliedSchema);