'use strict';

var _ = require('lodash');
var CodeApplied = require('./codeApplied.model');

// Get list of codeApplieds
exports.index = function(req, res) {
  CodeApplied.find(function (err, codeApplieds) {
    if(err) { return handleError(res, err); }
    return res.json(200, codeApplieds);
  });
};
//count codes in deployment for code wise uses validation
exports.countCodes = function(req, res) {
  CodeApplied.count({deployment_id:req.query.deployment_id, offerCode:req.query.code},function (err, cnt) {
    if(err) { return handleError(res, err); }
    return res.json(200, {count:cnt});
  });
};

// Get a single codeApplied
exports.show = function(req, res) {
  CodeApplied.findById(req.params.id, function (err, codeApplied) {
    if(err) { return handleError(res, err); }
    if(!codeApplied) { return res.send(404); }
    return res.json(codeApplied);
  });
};

// Creates a new codeApplied in the DB.
exports.create = function(req, res) {
  CodeApplied.create(req.body, function(err, codeApplied) {
    if(err) { return handleError(res, err); }
    return res.json(201, codeApplied);
  });
};

// Updates an existing codeApplied in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  CodeApplied.findById(req.params.id, function (err, codeApplied) {
    if (err) { return handleError(res, err); }
    if(!codeApplied) { return res.send(404); }
    var updated = _.merge(codeApplied, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, codeApplied);
    });
  });
};

// Deletes a codeApplied from the DB.
exports.destroy = function(req, res) {
  CodeApplied.findById(req.params.id, function (err, codeApplied) {
    if(err) { return handleError(res, err); }
    if(!codeApplied) { return res.send(404); }
    codeApplied.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}