'use strict';

var express = require('express');
var controller = require('./appVersion.controller');

var router = express.Router();

//router.get('/', controller.index);
router.get('/:appName/:device', controller.show);
router.post('/', controller.create);
router.put('/:appName/:device', controller.update);
router.delete('/:appName/:device', controller.destroy);

module.exports = router;
