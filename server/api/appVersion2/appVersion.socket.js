/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var AppVersion = require('./appVersion.model');

exports.register = function(socket) {
  AppVersion.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  AppVersion.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('appVersion:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('appVersion:remove', doc);
}