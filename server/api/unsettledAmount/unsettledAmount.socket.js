/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var UnsettledAmount = require('./unsettledAmount.model');

exports.register = function(socket) {
  UnsettledAmount.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  UnsettledAmount.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('unsettledAmount:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('unsettledAmount:remove', doc);
}