'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UnsettledAmountSchema = new Schema({
  tenant_id: Schema.Types.ObjectId,
  deployment_id: Schema.Types.ObjectId,
  dateTime: {type: Date, default: Date.now},
  unsettledAmount: Number
}, {versionKey: false});

module.exports = mongoose.model('UnsettledAmount', UnsettledAmountSchema);