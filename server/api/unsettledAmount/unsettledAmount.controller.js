'use strict';

var _ = require('lodash');
var UnsettledAmount = require('./unsettledAmount.model');
var mongoose = require('mongoose');

// Get list of UnsettledAmounts
exports.index = function (req, res) {
  UnsettledAmount.find({
      tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
      deployment_id: req.query.deployment_id
    }, function (err, UnsettledAmounts) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(UnsettledAmounts);
  });
};

// Get a single UnsettledAmount
exports.show = function (req, res) {
  UnsettledAmount.findById(req.params.id, function (err, UnsettledAmount) {
    if (err) {
      return handleError(res, err);
    }
    if (!UnsettledAmount) {
      return res.status(404).send('Not Found');
    }
    return res.json(UnsettledAmount);
  });
};

// Creates a new UnsettledAmount in the DB.
exports.create = function (req, res) {
req.body.dateTime=new Date();
UnsettledAmount.findOneAndUpdate( {
            'tenant_id': req.body.tenant_id,
            'deployment_id': req.body.deployment_id
        },req.body,{upsert:true},function(err,data){
              return res.status(200).json(data);   
        })
 
};

// Updates an existing UnsettledAmount in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  UnsettledAmount.findById(req.params.id, function (err, UnsettledAmount) {
    if (err) {
      return handleError(res, err);
    }
    if (!UnsettledAmount) {
      return res.status(404).send('Not Found');
    }
    var updated = _.extend(UnsettledAmount, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(UnsettledAmount);
    });
  });
};

// Deletes a UnsettledAmount from the DB.
exports.destroy = function (req, res) {
  UnsettledAmount.findById(req.params.id, function (err, UnsettledAmount) {
    if (err) {
      return handleError(res, err);
    }
    if (!UnsettledAmount) {
      return res.status(404).send('Not Found');
    }
    UnsettledAmount.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).send('No Content');
    });
  });
};






function handleError(res, err) {
  return res.status(500).send(err);
}