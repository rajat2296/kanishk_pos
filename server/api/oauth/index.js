'use strict';

var express = require('express');
var controller = require('./oauth.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post('/cipher', controller.cipher);

module.exports = router;