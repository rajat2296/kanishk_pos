'use strict';

var _ = require('lodash');
var AppVersion = require('./appVersion.model');


// Get a single appVersion
exports.show = function(req, res) {
  var appname=req.params.appName.toLowerCase();
  AppVersion.findOne({appName:appname}, function (err, appData) {
    if(err) { return handleError(res, err); }
    if(!appData) { return res.status(404).send('Not Found'); }
    return res.json(appData.appVersion);
  });
};

// Creates a new appVersion in the DB.
exports.create = function(req, res) {
  req.body.appName=req.body.appName.toLowerCase();
  AppVersion.findOne({appName:req.body.appName}, function (err, appData) {
    if(err) { return handleError(res, err); }
    if(!appData) {  AppVersion.create(req.body, function(err, appData) {
      if(err) { return handleError(res, err); }
      return res.status(201).json(appData);
    }); }
    else {
      return res.status(200).send('Already exists');
    }

  });

};
exports.update = function(req, res) {
  var appname=req.params.appName.toLowerCase();
  AppVersion.findOne({appName:appname}, function (err, appData) {
    if (err) { return handleError(res, err); }
    if(!appData) { return res.status(404).send('Not Found'); }
    console.log(appData);
    var updated = _.merge(appData, req.body);
    updated.save(function (err) {
      if (err) {return handleError(res, err); }
      return res.status(200).json(appData);
    });
  });
};
// Deletes a thing from the DB.
exports.destroy = function(req, res) {
  var appname=req.params.appName.toLowerCase();
  AppVersion.findOne({appName:appname}, function (err, appData) {
    if(err) { return handleError(res, err); }
    if(!appData) { return res.status(404).send('Not Found'); }
    appData.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
}

function handleError(res, err) {
  return res.status(500).send(err);
}
