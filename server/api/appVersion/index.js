'use strict';

var express = require('express');
var controller = require('./appVersion.controller');

var router = express.Router();

//router.get('/', controller.index);
router.get('/:appName', controller.show);
router.post('/', controller.create);
router.put('/:appName', controller.update);
router.delete('/:appName', controller.destroy);

module.exports = router;
