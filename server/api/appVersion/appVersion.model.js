'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AppVersionSchema = new Schema({
  appVersion: String,
  appName: String
});

module.exports = mongoose.model('AppVersion', AppVersionSchema);
