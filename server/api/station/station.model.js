'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StationSchema = new Schema({
    stationName: String,
    stationDescription: String,
    stationPrinter: String,
    categories: [],
    tenant_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    created: {type: Date, default: Date.now},
    updated: {type: Date, default: Date.now},
    isMaster:Boolean
});
StationSchema
    .path('stationName')
    .validate(function (value, respond) {
        var self = this;
        var flag=true;
		 if (this.isNew ) {
		    this.constructor.findOne({stationName:value, deployment_id:this.deployment_id}, function (err, s) {
                if (err) throw err;

                if (s) {

                    if (this.stationName === s.stationName && self._id != s._id) return respond(false);
                    return respond(false);
                }
                respond(true);
            });

        } else {
        	 this.constructor.findOne({stationName:value, deployment_id:this.deployment_id}, function (err, s) {
                if (err) throw err;

                if (s) {
                    if (this.stationName === s.stationName && (this._id!=s._id)){ console.log("station exist"); flag=false;  return respond(false);}
                    return respond(flag);
                }
                respond(flag);
            });

           // respond(true);
        }

    }, 'Duplicate station Name, please use another name.');

module.exports = mongoose.model('Station', StationSchema);
