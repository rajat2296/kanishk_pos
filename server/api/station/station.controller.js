'use strict';

var _ = require('lodash');
var Station = require('./station.model');
var Category = require('../category/category.model');
var Item = require('../item/item.model');

// Get list of stations
exports.index = function (req, res) {
   // console.log(req.query);
    Station.find(req.query, function (err, stations) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, stations);
    });
};

//Get Master Station
exports.getMasterStation=function(req,res){
 Station.find({deployment_id:req.query.deployment_id,isMaster:true},{stationName:1,stationPrinter:1,_id:0},function(err,docs){
    if(docs && docs.length>=0)
        return res.status(200).send(docs);
    else
        return handleError(res,{status:'error'});
 })
}
// Get a single station
exports.show = function (req, res) {
    Station.findById(req.params.id, function (err, station) {
        if (err) {
            return handleError(res, err);
        }
        if (!station) {
            return res.send(404);
        }
        return res.json(station);
    });
};

// Creates a new station in the DB.
exports.create = function (req, res) {
    Station.create(req.body, function (err, station) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, station);
    });
};

// Updates an existing station in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    req.body.updated = Date.now();
    Station.findByIdAndUpdate(req.params.id, req.body, {new:true}, function(err, station){
        if(err) return handleError(res, err);
        else if(!station) return res.send(404);
        else {
            var _station = {
                stationName: station.stationName,
                stationDescription: station.stationDescription,
                stationPrinter: station.stationPrinter,
                _id: station.id 
            };

            Category.update({'categoryStation._id': station.id}, {categoryStation: _station, updated: Date.now()}, {multi: true}, function(err, categoriesUpdated){
                if(err) return handleError(res, err)

                if(categoriesUpdated == 0) return res.json(200, station);
                else{
                    Item.update({'category.categoryStation._id': station.id}, {$set:{'category.categoryStation': _station, lastModified:Date.now(), updated: Date.now()}}, {multi:true}, function(err){
                        if(err) return handleError(res,err);
                        return res.json(200, station);
                    });
                }     
            });
        }
    })
};

// Deletes a station from the DB.
exports.destroy = function (req, res) {
    Station.findById(req.params.id, function (err, station) {
        if (err) {
            return handleError(res, err);
        }
        if (!station) {
            return res.send(404);
        }
        station.remove(function (err) {
            if (err) {
                return handleError(res, err);
            } 
            else{
                Category.update({'categoryStation._id': station.id}, {
                    $unset: {
                        categoryStation: ""
                    }, 
                    $set: {
                        updated: Date.now()
                    }
                }, {multi: true}, function(err, categoriesUpdated){
                    if(err) return handleError(res, err)

                    if(categoriesUpdated == 0) 
                        return res.send(204);
                    else{
                        Item.update({'category.categoryStation._id': station.id}, {
                            $unset: {
                                'category.categoryStation': ""
                            },
                            $set: {
                                lastModified:Date.now(), 
                                updated: Date.now()
                            }
                        }, {multi:true}, function(err){
                            if(err) 
                                return handleError(res,err);
                            return res.send(204);
                        });
                    }
                });
            }
        });
    });
};

function handleError(res, err) {
    return res.send(500, err);
}
