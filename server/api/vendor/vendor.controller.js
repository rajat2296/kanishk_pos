'use strict';

var _ = require('lodash');
var Vendor = require('./vendor.model');
var async = require('async');
var Async = require('async');
var baseKitchenItem = require('../baseKitchenItem/baseKitchenItem.model');
var baseKitchenUnit = require('../baseKitchenUnit/baseKitchenUnit.model');
var Item = require('../item/item.model');
var Q = require('q');
var Deployment = require('../deployment/deployment.model');
var Tenant=require('../tenant/tenant.model');




// Get list of All vendors by Tenant Id
//modified
exports.getAll = function(req, res) {
  var query = {};
  query.tenant_id = req.params.id;
  if(!req.query.localVendor) {
    query.type = 'HO Vendor'
  } else {
    query.type = 'Local Vendor';
  }
  console.log(query);
  Vendor.find(query, function (err, vendors) {
    if(err) { return handleError(res, err); }
    return res.json(200, vendors);
  });
};
//end


// Get list of vendors
exports.index = function(req, res) {
  var paramsquery;
  if(req.query.lastStoreSynced === undefined) {
    paramsquery = {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
  }
  else {
    paramsquery = {
      tenant_id: req.query.tenant_id,
      deployment_id: req.query.deployment_id,
      updated: {$gte: new Date(req.query.lastStoreSynced), $lt: new Date(req.query.currentSyncTime)}
    }
  }
  // Vendor.find(paramsquery,function (err, vendors) {
  //   if(err) { return handleError(res, err); }
  //   return res.json(200, vendors);
  // });
  var check = {};
  check.tenant_id = req.query.tenant_id;
  check.deployment_id = req.query.deployment_id;
  if(req.query.type)
    check.type = req.query.type;
  if(req.query.isERPLN == true || req.query.isERPLN == 'true'){
    check.isERPLN = true;
  }

  Vendor.find(check, function (err, vendors) {
    if(err) { return handleError(res, err); }
    return res.json(200, vendors);
  });
};

// Get a single vendor
exports.show = function(req, res) {
  Vendor.findById(req.params.id, function (err, vendor) {
    if(err) { return handleError(res, err); }
    if(!vendor) { return res.send(404); }
    return res.json(vendor);
  });
};

// Creates a new vendor in the DB.
exports.create = function(req, res) {
  Vendor.create(req.body, function(err, vendor) {
    if(err) {
      return res.json({error:err});
      //return handleError(res, err);
    }
    if(vendor.type == "HO Vendor"){
      console.log('ho vendor');
      var bkItems = [];
      _.forEach(vendor.pricing.category, function (category) {
        _.forEach(category.item, function (item) {
          var itemObject = {
            itemName: item.itemName,
            itemId: item._id,
            units: item.units,
            tenant_id: vendor.tenant_id,
            category: item.fromCategory,
            deployment_id: vendor.deployment_id,
            toDeployment_id: vendor.baseKitchenId
          }
          if(_.has(item.units, '0')){
            itemObject.preferedUnit = {
              _id: item.units[0]._id,
              conversionFactor: item.units[0].conversionFactor,
              unitName: item.units[0].unitName
            }
          }
          bkItems.push(itemObject);
        });
      });
      console.log('bkItems', bkItems.length);
      async.each(bkItems, function(item, callback) {
        baseKitchenItem.update({itemName: item.itemName, toDeployment_id: item.toDeployment_id, deployment_id: item.deployment_id}, item, {upsert: true}, function (err, it) {
          callback();
        });
      }, function (err){
        return res.json(201, vendor);
      });
    }
    /*else if(vendor.type == "Outlet Vendor"){
      console.log('Outlet Vendor');
      var bkItems = [];
      _.forEach(vendor.pricing.category, function (category) {
        _.forEach(category.item, function (item) {
          var itemObject = {
            itemName: item.itemName,
            itemId: item._id,
            units: item.units,
            tenant_id: vendor.tenant_id,
            category: item.fromCategory,
            deployment_id: vendor.deployment_id,
            toDeployment_id: vendor.baseKitchenId
          }
          if(_.has(item.units, '0')){
            itemObject.preferedUnit = {
              _id: item.units[0]._id,
              conversionFactor: item.units[0].conversionFactor,
              unitName: item.units[0].unitName
            }
          }
          bkItems.push(itemObject);
        });
      });
      console.log('bkItems', bkItems.length);
      async.each(bkItems, function(item, callback) {
        baseKitchenItem.update({itemName: item.itemName, toDeployment_id: item.toDeployment_id, deployment_id: item.deployment_id}, item, {upsert: true}, function (err, it) {
          callback();
        });
      }, function (err){
        return res.json(201, vendor);
      });
    }*/
     else return res.json(201, vendor);
  });
};

// Updates an existing vendor in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Vendor.findById(req.params.id, function (err, vendor) {
    if (err) { return handleError(res, err); }
    if(!vendor) { return res.send(404); }
    var updated = _.extend(vendor, req.body);
    updated.markModified("vendorName");
    updated.markModified("contactPerson");
    updated.markModified("contactNumber");
    updated.markModified("address");
    updated.markModified("tinNumber");
    updated.markModified("serviceTaxNumber");
    updated.markModified("email");
    updated.markModified("type");
    updated.markModified("pricing");
    updated.save(function (err) {
      if (err) {
        //return handleError(res, err);
        var val={};
        val.errorMessage("Some error occured ")
        return res,json(201,val);
        //return res.json({error:err});
      }
      if(vendor.type == "HO Vendor"){
        var bkItems = [];
      _.forEach(vendor.pricing.category, function (category) {
        _.forEach(category.item, function (item) {
          var itemObject = {
            itemName: item.itemName,
            itemId: item._id,
            units: item.units,
            tenant_id: vendor.tenant_id,
            category: item.fromCategory,
            deployment_id: vendor.deployment_id,
            toDeployment_id: vendor.baseKitchenId
          }
          if(_.has(item.units, '0')){
            itemObject.preferedUnit = {
              _id: item.units[0]._id,
              conversionFactor: item.units[0].conversionFactor,
              unitName: item.units[0].unitName
            }
          }
          bkItems.push(itemObject);
        });
      });
      async.each(bkItems, function(item, callback) {
        baseKitchenItem.update({itemName: item.itemName, deployment_id: item.deployment_id, toDeployment_id: item.toDeployment_id}, item, {upsert: true}, function (err, it) {
          callback();
        });
        _.forEach(item.units, function (unit) {
          var ut = {
            unitName: unit.unitName,
            unitId: unit._id,
            baseUnit: unit.baseUnit,
            conversionFactor: unit.conversionFactor,
            deployment_id: item.deployment_id,
            toDeployment_id: item.toDeployment_id
          }
          baseKitchenUnit.update({unitName: unit.unitName, deployment_id: item.deployment_id, toDeployment_id: item.toDeployment_id}, ut, {upsert: true});
        })
      }, function (err){
        return res.json(201, vendor);
      });
      }
      else
        return res.json(200, vendor);
    });
  });
};

// Deletes a vendor from the DB.
exports.destroy = function(req, res) {
  Vendor.findById(req.params.id, function (err, vendor) {
    if(err) { return handleError(res, err); }
    if(!vendor) { return res.send(404); }
    vendor.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.getNumVendorsHavingCategory = function (req, res) {
  var category_id = req.params.catId;
  console.log('category_id', category_id);
  Vendor.find({"pricing.category._id": category_id}, function (err, vendors) {
    if(err) { return handleError(res, err); }
    return res.status(200).json({length: vendors.length});
  });
};

exports.updateItemUnitInHOVendor = function (req, res) {

  var itemId = req.query.itemId;
  var result = {};
  console.log(req.body);
  Vendor.find({tenant_id: req.body.tenant_id, baseKitchenId: req.body.deployment_id, type: "HO Vendor", "pricing.category.item._id": itemId}, function (err, vendors) {
    if(err) { return handleError(res, err) }
    async.eachSeries(vendors, function(vendor, callback) {

      var itemIndex = -1;
      var catIndex = -1;
      var units = [];
      units.push(req.body.item.unit);
      //console.log('vendor_id', vendor._id);
      var updatedVendor = new Object(vendor);
      _.forEach(updatedVendor.pricing.category, function (cat, cIndex) {
        _.forEach(cat.item, function (it, itIndex) {
          if(it._id == itemId){
            it.units = [];
            it.units.push(req.body.item.unit);
            //console.log(it.units);
            itemIndex = itIndex;
            catIndex = cIndex;
          }
        });
      });
      if(itemIndex > -1){
        var updated = _.extend(vendor, updatedVendor);
        console.log(updated);
        updated.save(function (err) {
          callback();
        });
      }
    }, function done() {
      res.status(200).json({errorcode: 200, message: result.err});
    })
  });
}

exports.deleteItemFromHOVendor = function (req, res) {

  var itemId = req.query.itemId;
  var itemId = req.query.itemId;
  var result = {};
  console.log(req.body);
  Vendor.find({tenant_id: req.body.tenant_id, baseKitchenId: req.body.deployment_id, type: "HO Vendor", "pricing.category.item._id": itemId}, function (err, vendors) {
    if(err) { return handleError(res, err) }
    async.eachSeries(vendors, function(vendor, callback) {

      var itemIndex = -1;
      var catIndex = -1;
      //console.log('vendor_id', vendor._id);
      var updatedVendor = new Object(vendor);
      _.forEach(updatedVendor.pricing.category, function (cat, cIndex) {
        _.forEach(cat.item, function (it, itIndex) {
          if(it._id == itemId){
            itemIndex = itIndex;
            catIndex = cIndex;
          }
        });
      });
      if(itemIndex > -1){
        updatedVendor.pricing.category[catIndex].item.splice(itemIndex, 1);
        var updated = _.extend(vendor, updatedVendor);
        //console.log(updated);
        updated.save(function (err) {
          callback();
        });
      }
    }, function done() {
      res.status(200).json({errorcode: 200, message: result.err});
    })
  });
}

exports.findHOVendorWithItem = function (req, res) {

  var tenant_id = req.query.tenant_id;
  var itemId = req.query.itemId;
  Vendor.findOne({tenant_id: tenant_id, type: "HO Vedor", "pricing.category.item._id": itemId}, {_id: 1}, function(err, vendor) {
    if(err)
      return handleError(res, err);
    if(!vendor)
      return res.status(200).json({vendorFound: false});
    return res.status(200).json({vendorFound: true});
  }); 
};

// function findAndUpdateHOVendorsOnNewRecipe() {

//   Vendor.find({baseKitchenId: req.body.deployment_id, type: "HO Vendor"}, function (err, vendors) {
//     _.forEach(vendors, function (v){

//       var it = {};
//       console.log(v.vendorName);
//       console.log(v);
//       _.forEach(v.pricing.category, function (c) {
//         if (c._id == $scope.receipeForm.category._id) {
//           var updatedItemIndex = _.findIndex(c.item, function (i) {
//             return i._id == $scope.receipeForm.selectedItem._id;
//           });
//           vendor_id = v._id;
//           if (updatedItemIndex == -1) {
//             flag = 1;
//             console.log($scope.receipeForm);
//             console.log($scope.stockRecipes);
//             console.log($scope.receipeForm.selectedItem);
//             it._id = $scope.receipeForm.selectedItem._id;
//             it.itemName = $scope.receipeForm.selectedItem.name;
//             _.forEach($scope.items, function (i) {
//               if (i.name == $scope.receipeForm.selectedItem.name) {
//                 it.units = [];
//                 it.units.push({});
//                 it.units[0]._id = i.unit._id;
//                 it.units[0].baseUnit = i.unit.baseUnit;
//                 it.units[0].unitName = i.unit.unitName;
//                 it.units[0].baseConversionFactor = i.unit.baseConversionFactor;
//                 it.units[0].conversionFactor = i.unit.conversionFactor;
//                 console.log(it.units);
//               }
//             });
//             console.log('push', it);
//             console.log(it);
//             c.item.push(it);
//             console.log(c.item);
//           }
//           /*else {
//            _.forEach($scope.items, function (i) {
//            if (i.name == $scope.receipeForm.selectedItem.name) {
//            c.item[updatedItemIndex].units = [];
//            c.item[updatedItemIndex].units.push({});
//            c.item[updatedItemIndex].units[0]._id = i.unit._id;
//            c.item[updatedItemIndex].units[0].baseUnit = i.unit.baseUnit;
//            c.item[updatedItemIndex].units[0].unitName = i.unit.unitName;
//            c.item[updatedItemIndex].units[0].baseConversionFactor = i.unit.baseConversionFactor;
//            c.item[updatedItemIndex].units[0].conversionFactor = i.unit.conversionFactor;
//            //console.log(it.units);
//            }
//            });
//            }*/
//         }
//       })
//       if (flag == 1) {
//         v.$update(function (data) {
//           console.log('Updated vendor', data);
//         });
//       }
//       /*v.$update(function (data) {
//        console.log('Updated vendor', data);
//        });*/
//       console.log(v)
//     });
//   });
// }

exports.getHOVendor = function(req, res) {
  var query = {};
  query.tenant_id = req.query.tenant_id;
  query.deployment_id = req.query.deployment_id;
  query.type = 'HO Vendor'

  //console.log(query);
  Vendor.find(query, function (err, vendors) {
    if(err) { return handleError(res, err); }
    return res.json(200, vendors);
  });
};

exports.getOutletHOVendor = function(req, res) {
  var query = {};
  query.tenant_id = req.query.tenant_id;
  query.deployment_id = req.query.deployment_id;
  query.type = 'Outlet Vendor'

  //console.log(query);
  Vendor.find(query, function (err, vendors) {
    if(err) { return handleError(res, err); }
    return res.json(200, vendors);
  });
};
exports.updateHOVendorItems = function (req, res) {
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var numVendorsUpdated = 0;
  var numVendors = 0;
  var numItemsUpdated = 0;

  Vendor.find({type: "HO Vendor", tenant_id: tenant_id, deployment_id: deployment_id}, {"pricing": 1, baseKitchenId: 1, vendorName: 1}, function (err, vendors) {
    numVendors = vendors.length;
    if(err)
      return handleError(res, err);
    Async.each(vendors, 
      function (vendor, callback) {
        
        Item.find({tenant_id: tenant_id, deployment_id: vendor.baseKitchenId}, {name: 1}, function (err, items) {
          if(err)
            callback(err);
          else {
            _.forEach(vendor.pricing.category, function(cat) {
              _.forEach(cat.item, function (it) {
                if(!it.itemName){
                  numItemsUpdated++;
                  //console.log('itemName', it.itemName);
                  var reqItem = _.find(items, function (itm) {
                    return itm._id.toString() == it._id.toString();
                  });
                  if(reqItem)
                    it.itemName = reqItem.name;
                }
              });
            });
          }
          Vendor.update({_id: vendor._id}, {$set: {pricing: vendor.pricing}}, function (err, num) {
            if(err){
              callback(err);
            }
            else{
              //console.log('vendor updated', num);
              numVendorsUpdated++;
              callback();
            }
          });
        });
      }, function (err) {
      if(err){
        console.log('updateHOVendor', err);
        return handleError(res, err);
      }
      var msgString = numItemsUpdated + " items updated in " + numVendorsUpdated + " vendors of total " + numVendors + " vendors";
      return res.json(200, {status: "Done", message: msgString});
    }); 
  });
}

exports.updateHOVendorItems = function (req, res) {
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var numVendorsUpdated = 0;
  var numVendors = 0;
  var numItemsUpdated = 0;

  Vendor.find({type: "HO Vendor", tenant_id: tenant_id, deployment_id: deployment_id}, {"pricing": 1, baseKitchenId: 1, vendorName: 1}, function (err, vendors) {
    numVendors = vendors.length;
    if(err)
      return handleError(res, err);
    Async.each(vendors, 
      function (vendor, callback) {
        
        Item.find({tenant_id: tenant_id, deployment_id: vendor.baseKitchenId}, {name: 1}, function (err, items) {
          if(err)
            callback(err);
          else {
            _.forEach(vendor.pricing.category, function(cat) {
              _.forEach(cat.item, function (it) {
                if(!it.itemName){
                  numItemsUpdated++;
                  //console.log('itemName', it.itemName);
                  var reqItem = _.find(items, function (itm) {
                    return itm._id.toString() == it._id.toString();
                  });
                  if(reqItem)
                    it.itemName = reqItem.name;
                }
              });
            });
          }
          Vendor.update({_id: vendor._id}, {$set: {pricing: vendor.pricing}}, function (err, num) {
            if(err){
              callback(err);
            }
            else{
              //console.log('vendor updated', num);
              numVendorsUpdated++;
              callback();
            }
          });
        });
      }, function (err) {
      if(err){
        console.log('updateHOVendor', err);
        return handleError(res, err);
      }
      var msgString = numItemsUpdated + " items updated in " + numVendorsUpdated + " vendors of total " + numVendors + " vendors";
      return res.json(200, {status: "Done", message: msgString});
    }); 
  });
}

exports.checkSameVendors = function (req, res) {

  Vendor.find({deployment_id: {$in: [req.query.deployment1, req.query.deployment2]}, type: "HO Vendor"}, {baseKitchenId: 1, deployment_id: 1}, function(err, vendors) {
    var checkVendors = {};
    _.forEach(vendors, function (vendor) {
      if(!checkVendors[vendor.baseKitchenId])
        checkVendors[vendor.baseKitchenId] = 0;
      if(vendor.deployment_id == req.query.deployment1 || vendor.deployment_id == req.query.deployment2)
        checkVendors[vendor.baseKitchenId] += 1;
    });
    var flag = false;
    _.forEach(checkVendors, function (c) {
      if(c >= 2)
        flag = true;
    });
    if(flag)
    {
      console.log('true');
      return res.json(200,{check:true});
    }
    else
    {
      console.log('false');
      return res.json(200,{check:false});
    }
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
