'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VendorSchema = new Schema({
  vendorName:String,
  contactPerson:String,
  contactNumber:String,
  address:String,
  tinNumber:String,
  serviceTaxNumber:String,
  email:String,
  type:String,
  isERPLN: {
    type: Boolean,
    default: false
  },
  pricing:{
    category: [
      {
        categoryName: String,
        _id: String,
        type: {
          type: String,
          required: false
        },
        item: [
          {
            toDate: String,
            fromDate: String,
            price: String,
            itemName: String,
            selectedUnitId: {},
            selectedUnit: {},
            code: String,
            preferedUnit: String,
            _id: String,
            type: {
              type: String,
              required: false
            },
            units: [],
          }
        ]
      }
    ],
    storeName: String,
    _id: String
  },
  baseKitchenId:String,
  tenant_id: Schema.Types.ObjectId,
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  active: { type:Boolean , default:true }
},{versionKey:false});

VendorSchema.pre('save', function (next) {
    this.updated = new Date;
    next();
});

VendorSchema.path('vendorName').validate(function(v, respond) {
    var self=this;
    this.constructor.findOne({'vendorName':  new RegExp('^'+v+'$', "i"),deployment_id:this.deployment_id}, function (err, vendors) {

        var flag=true;
        if(err){throw err;}

        if(vendors)
        {
            if(vendors.vendorName.toLowerCase()===self.vendorName.toLowerCase())
            {
                if(vendors.id!=self.id){
                    flag=false;
                }
            }
        }
        respond(flag);
        console.log(flag);
    });

}, 'This Vendor Name is already used please try  with another name !');

module.exports = mongoose.model('Vendor', VendorSchema);
