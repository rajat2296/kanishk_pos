'use strict';

var express = require('express');
var controller = require('./vendor.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/findHOVendorWithItem', controller.findHOVendorWithItem);
router.get('/checkSameVendors', controller.checkSameVendors);
router.get('/getHOVendor', controller.getHOVendor);
router.get('/getOutletHOVendor', controller.getOutletHOVendor);
router.get('/updateHOVendorItems', controller.updateHOVendorItems);
//modified
router.get('/getAll/:id', controller.getAll);
//end
router.get('/getNumVendorsHavingCategory/:catId', controller.getNumVendorsHavingCategory);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/updateItemUnitInHOVendor', controller.updateItemUnitInHOVendor);
router.put('/deleteItemFromHOVendor', controller.deleteItemFromHOVendor);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
