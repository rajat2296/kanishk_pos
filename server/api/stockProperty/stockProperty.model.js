'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockPropertySchema = new Schema({
  title: String,
  address: String,
  billFooter: String,
  billRightFooter: String,
  billLeftFooter:String,
  vatTin: String,
  taxTin: String,
  documentationCharge: String,
  resetSerialNumber: {type:Boolean, default:false},
  isPhysicalStock: {type:Boolean, default:false},
  isFinishedFood: {type:Boolean, default:false},
  isYieldEnabled: {type:Boolean, default:false},
  isHidePricing: {type:Boolean, default:false},
  isEditPricing: {type:Boolean, default:false},
  dropdownComments: {type:Boolean, default:false},
  property:{},
  tenant_id: Schema.Types.ObjectId,
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  active: Boolean
},{versionKey:false});

StockPropertySchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});

module.exports = mongoose.model('StockProperty', StockPropertySchema);
