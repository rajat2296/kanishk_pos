/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockProperty = require('./stockProperty.model');

exports.register = function(socket) {
  StockProperty.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockProperty.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockProperty:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockProperty:remove', doc);
}