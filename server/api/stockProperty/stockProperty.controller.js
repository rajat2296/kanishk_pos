'use strict';

var _ = require('lodash');
var StockProperty = require('./stockProperty.model');

// Get list of stockPropertys
exports.index = function(req, res) {
  StockProperty.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}, function (err, stockPropertys) {
    if(err) { return handleError(res, err); }
    return res.json(200, stockPropertys);
  });
};

// Get a single stockProperty
exports.show = function(req, res) {
  StockProperty.findById(req.params.id, function (err, stockProperty) {
    if(err) { return handleError(res, err); }
    if(!stockProperty) { return res.send(404); }
    return res.json(stockProperty);
  });
};

// Creates a new stockProperty in the DB.
exports.create = function(req, res) {
  StockProperty.create(req.body, function(err, stockProperty) {
    if(err) { return handleError(res, err); }
    return res.json(201, stockProperty);
  });
};

// Updates an existing stockProperty in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  StockProperty.findById(req.params.id, function (err, stockProperty) {
    if (err) { return handleError(res, err); }
    if(!stockProperty) { return res.send(404); }
    var updated = _.merge(stockProperty, req.body);
    updated=_.extend(stockProperty, req.body);
    updated.markModified("property");
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, stockProperty);
    });
  });
};

// Deletes a stockProperty from the DB.
exports.destroy = function(req, res) {
  StockProperty.findById(req.params.id, function (err, stockProperty) {
    if(err) { return handleError(res, err); }
    if(!stockProperty) { return res.send(404); }
    stockProperty.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}