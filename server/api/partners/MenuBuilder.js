var _ = require('lodash');

exports.zomato= function(items, charges, deletedItems){

		function UUID(id){
			/*var time = String(parseInt(String(id).slice(0,8), 16));
			//var machine = String(parseInt(id.slice(8,14), 16));
			//var pid = String(parseInt(id.slice(14,18), 16));
			var inc = String(parseInt(String(id).slice(18), 16));
			return Number(time+inc);*/
			return String(id);
		}

		function getTaxes(items){
			var taxes = {};
			if(items.length!=0){
				items.forEach(function(item){
					var item_taxes = item.tabs[0].taxes||[];
					for(var i=0;i<item_taxes.length;i++){
						if(taxes[item_taxes[i]._id])
							continue;
						else{
							taxes[item_taxes[i]._id] = {
								tax_id: UUID(item_taxes[i]._id),
								tax_name: item_taxes[i].name,
								tax_type: item_taxes[i].type.toUpperCase(),
								tax_value: item_taxes[i].value,
								tax_is_active: 1
							}
						}
					}
				});
				return Object.keys(taxes).map(function(tax_id){return taxes[tax_id];});
			}else
				return [];
		}

		function getCharges(charges){
			return charges.map(function(charge){
				return {
					charge_id: UUID(charge._id),
					charge_name: charge.name,
					charge_type: charge.type.toUpperCase(),
					charge_value: charge.value,
					applicable_on: "ORDER"
				}
			});
		}

		function getAdditionalCharges(charges){
			var charge_ids = charges.map(function(charge){return UUID(charge._id);});
			return charge_ids.length>0?[{order_type:"DELIVERY", charges: charge_ids},{order_type:"PICKUP", charges: charge_ids}]:[];
		}

		function parseRate(rate){
			return Number(parseFloat(rate).toFixed(2));
		}

		function calculateItemPrice(item){
			var taxes = item.tabs[0].taxes;
			var finalPrice = parseFloat(item.tabs[0].item.rate);
			for(var i=0;i<taxes.length;i++){
				if(taxes[i].type == 'percentage')
					finalPrice+=Number(item.tabs[0].item.rate)*Number(taxes[i].value)/100;
				else if(taxes[i].type == 'fixed')
					finalPrice+=Number(taxes[i].value);
			}
			return Number(finalPrice.toFixed(2));
		}

		function getItemGroups(items, deletedItemsHash){
			var addon_items = {};
			var combo_items = {};
			items.forEach(function(item){
				if(item.mapItems!=undefined && item.mapItems.length!=0){
					item.mapItems.forEach(function(map_item){
						if(!addon_items[map_item._id]) addon_items[map_item._id] = [];
						addon_items[map_item._id].push({
							item_id: UUID(item._id),
							item_name: item.tabs[0].item.name,
							item_unit_price: parseRate(item.tabs[0].item.rate),
							item_final_price: calculateItemPrice(item),
							item_is_active: deletedItemsHash[item._id]?0:1,
							item_taxes: [{order_type: item.tabs[0].tabType.toUpperCase(), taxes: item.tabs[0].taxes.map(function(tax){return UUID(tax._id);})}]			
						});
					});
				}else if(item.mapComboItems!=undefined && item.mapComboItems.length!=0){
					item.mapComboItems.forEach(function(map_item){
						if(!combo_items[item._id]) combo_items[item._id] = [];
						combo_items[item._id].push({
							item_id: UUID(map_item._id),
							item_name: map_item.name,
							item_unit_price: parseRate(map_item.rate),
							item_is_active: deletedItemsHash[map_item._id]?0:1,
							item_final_price: parseRate(map_item.rate)
						});
					});
				}
			});

			var addon_group = {
				"group_id":1,
                "group_name":"Addons",
                "group_decsription":"Add more to this",
                "group_minimum":0,
                "group_maximum":null,
                "items": []
			};

			var combo_group = {
				"group_id":2,
                "group_name":"Combos",
                "group_decsription":"Combo items",
                "group_minimum":0,
                "group_maximum":0,
                "items": []
			};

			return {addons: addon_items, addon_group: addon_group, combos: combo_items, combo_group: combo_group};
		}

		function getCategories(items, deletedItems){
			var categories={};
			var subcategories={};
			var deletedItemsHash = createDeletedItemsHash(deletedItems);
			var groups = getItemGroups(items, deletedItemsHash);
			var items = items.concat(deletedItems);

			function assignGroup(item_id){
				if(groups.addons[item_id]){
					var g = groups.addon_group;
					g.group_maximum = groups.addons[item_id].length;
					g.items = [];
					for (var i = groups.addons[item_id].length - 1; i >= 0; i--) {
						g.items.push(groups.addons[item_id][i]);
					}
					return g;
				/*}else if(groups.combos[item_id]){
					var g = groups.combo_group;
					g.items = groups.combos[item_id];
					return [g];*/
				}else
					return undefined;
			}

			function assignItems(subcategory_id, items){
				var cat_items = [];
				items.forEach(function(item){
					if(subcategory_id == item.category._id){
						
						var g = assignGroup(item._id);
						
						var zomato_item = {
							item_id: UUID(item._id),
							item_name: item.tabs[0].item.name,
							item_unit_price: parseRate(item.tabs[0].item.rate),
							item_final_price: calculateItemPrice(item),
							item_taxes: [{order_type: item.tabs[0].tabType.toUpperCase(), taxes: item.tabs[0].taxes.map(function(tax){return UUID(tax._id);})}],
							item_is_active: item.isDeleted?0:1,
							groups: []
						};
						if(item.description)
							zomato_item.item_short_description = item.description;
						if(g!=undefined)
							zomato_item.groups.push(g);
						cat_items.push(zomato_item);
					}
				});
				return cat_items;
			}
			var fs=require('fs');

			items.forEach(function(item, index, items){
				if(!categories[item.category.superCategory._id]){
					categories[item.category.superCategory._id]={
						category_id: UUID(item.category.superCategory._id),
						category_name: item.category.superCategory.superCategoryName,
						has_subcategory: 1,
						subcategories: []
					}
				}
			});

			items.forEach(function(item, index, items){
				if(!subcategories[item.category._id] && item.category.isAddons==false){
					var sub_cat = {
						subcategory_id: UUID(item.category._id),
						subcategory_name: item.category.categoryName
					};
					sub_cat.items = assignItems(item.category._id, items);
					categories[item.category.superCategory._id].subcategories.push({
						subcategory_id: sub_cat.subcategory_id,
						subcategory_name: sub_cat.subcategory_name,
						items: _.cloneDeep(sub_cat.items)
					});
					subcategories[item.category._id] = 1;
				}
			});

			return Object.keys(categories).filter(function(cat_id){return categories[cat_id].subcategories.length>0;}).map(function(cat_id){return categories[cat_id];});			
		}

		function createDeletedItemsHash(deletedItems){
			var dl_items = {};
			if(deletedItems.length>0)
				deletedItems.forEach(function(item){dl_items[deletedItems._id]=1;});
			return dl_items;
		}

		return menu = {
			taxes: getTaxes(items), 
			charges: getCharges(charges), 
			order_additional_charges: getAdditionalCharges(charges), 
			categories: getCategories(items, deletedItems)
		};
	}