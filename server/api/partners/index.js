'use strict';

var express = require('express');
var controller = require('./partners.controller');

var router = express.Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.delete('/', controller.destroy);
router.put('/',controller.update);
router.get('/:id',controller.getPartner);
router.post('/execute_event', controller.execute_event);
router.post('/syncZomato', controller.syncZomato, controller.menu_load_zomato);
router.post('/statusZomato', controller.statusZomato);

module.exports = router;