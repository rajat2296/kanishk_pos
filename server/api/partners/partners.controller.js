'use strict'

var _ = require('lodash');
var crypto = require('crypto');
var Partner = require('./partners.model');
var Partner_Sync_Chainset = require('./partner_sync_chainset.model');
var PartnerDetails = require('../partnerDetails/partnerDetails.model');
var TabPartner=require('../tab/tabPartner.model');
var Item=require('../item/item.model');
var DeleteItem = require('../item/deleteItem.model')
var Charges=require('../charges/charges.model');
var Tab=require('../tab/tab.model');
var request = require('request');
var Menu_Builder = require('./MenuBuilder');
var async = require('async');

exports.index = function(req, res){
  PartnerDetails.find({}, 'partner_name client_id class', function(err, all_partners){
    if(err) res.status(500).json();
    else{
      var p = _.map(all_partners, function(partner){return {partner_name: partner.partner_name, class:partner.class, client_id: partner.client_id};});
      if(req.query.deployment_id){
        Partner.find({deployment_id: req.query.deployment_id}).lean().exec(function(err, partners){
          if(err) res.status(500).json();
          else{
            var result = _.map(p, function(p){
                  var elem = _.find(partners, {'partner_name': p.partner_name});
                  if(elem!=undefined){
                    elem.client_id = p.client_id;
                    elem.class = p.class;
                    return elem;
                  }else
                    return p; 
                });
            res.json(result);
          }
        });
      }else{
        res.status(422).send();
      }
    }
  });
};


exports.create = function(req, res){
  var partner = new Partner(req.body);
  var cipher = crypto.createCipheriv('aes-128-cbc', "posist-open-apis", "@@@@&&&&####$$$$");
  partner.customer_key = cipher.update(crypto.randomBytes(8).toString('utf8')+':'+req.body.deployment_id, 'utf8', 'hex')+cipher.final('hex');
      
  partner.save(function(err, partner){
    if(err) res.status(500).json();
    else{
      res.json(partner);
    }
  });
};

exports.update = function(req, res){   
  Partner.findOneAndUpdate({deployment_id: req.query.deployment_id, customer_key: req.query.customer_key}, {partner_merchant_id: req.body.partner_merchant_id},function(err, partner){
    if(err) res.status(500).json();
    else{
      res.json(partner);
    }
  });
};

exports.destroy = function(req, res){
  Partner.findOneAndRemove({deployment_id: req.query.deployment_id, customer_key: req.query.customer_key}, function (err, partner) {
    if(err) { res.status(500).send(); }
    else if(!partner) { res.status(404).send(); }
    else
      res.send();
  });
};

exports.getPartner=function(req,res){
  Partner.findOne({deployment_id:req.query.deployment_id,partner_name:req.params.id},function(err,doc){
    if(doc) return res.status(200).send(doc);
    else
      return res.status(500).send({status:'error'});
  });
}

exports.execute_event = function(req, res){
  var url='http://posistapi.com/api/webhooks/hit?event_name='+req.query.event_name;
  try {
    if(req.body.partners && req.body.partners.length>0){
      var payload = getPayload(req.query.event_name, req.query.deployment_id, req.body);
      console.log(payload);
      var options = {
        method: 'POST',
        url: url,
        headers:{
          'content-type':'application/json'
        },
        json:payload
      };

      request(options, function (error, response, body) {
         if( response && response.statusCode==200){
          return res.status(200).json({message: 'Third party partners have been notified to update menu.'});
         }
         else return res.status(500).json({message: 'Third party partners failed to get notification of menu updates.'});
      });
    }else
      return res.status(200).json({status:200, message: 'Third party partners have been notified to update menu.'});
  }
  catch(e){
    console.log("exception",e);
    return res.status(500).send({message:'An exception occurred'});
  }
}

function getPayload(event_name, deployment_id, body){
  switch(event_name){
    case 'menu_sync': 
      return {partners: body.partners, data: {}};
  }
}

var ZOMATO_CLIENT_ID = 'r1AROcuug';
var ZOMATO_AUTH_KEY = '293fa64feaf29691cd6c9fc092353530';
exports.syncZomato = function(req, res, next){
  try{
    var zomato = req.body.partners.filter(function(partner){return partner.client_id==ZOMATO_CLIENT_ID});
    if(zomato.length!=0 && req.query.event_name=='menu_sync'){
      TabPartner.findOne({deployment_id: req.query.deployment_id, "partners.sourceId": ZOMATO_CLIENT_ID}, 'tab_id', function(err, tabpartner){
        if(err){
            res.status(500).send('You dont have zomato mapped to tab.');
        }else{
          async.series([
            function(step){
              if(!tabpartner){
                Tab.findOne({deployment_id: req.query.deployment_id, tabType: 'delivery'}, function(err, tab){
                  if(err)
                    step('Zomato syncing failed.', null);
                  else
                    step(null, tab._id);
                });
              }else
                step(null, tabpartner.tab_id);
            }, 
            function(step){
              Partner_Sync_Chainset.findOne({deployment_id: req.query.deployment_id, sourceId: ZOMATO_CLIENT_ID, done_time:{$exists:true}}).sort({done_time:-1}).limit(1).exec(function(err, chainset){
                if(err){
                  step('Zomato syncing failed.',null);
                }else if(!chainset){
                  var start_time = new Date(2000,0,1);
                  var new_chainset = new Partner_Sync_Chainset({
                    partner_name: 'Zomato',
                    sourceId: ZOMATO_CLIENT_ID,
                    deployment_id: req.query.deployment_id,
                    start_time: start_time,
                    status: 'start'
                  });
                  new_chainset.save(function(err, set){
                    if(err)
                      step('Zomato syncing failed', null);
                    else
                      step(null, {start_time: start_time, set_id: set._id, tab_id: 'null'});
                  });
                }else{
                  var new_chainset = new Partner_Sync_Chainset({
                    partner_name: 'Zomato',
                    sourceId: ZOMATO_CLIENT_ID,
                    deployment_id: req.query.deployment_id,
                    start_time: chainset.done_time,
                    status: 'start'
                  });
                  new_chainset.save(function(err, set){
                    if(err)
                      step('Zomato syncing failed', null);
                    else
                      step(null, {start_time: chainset.done_time, set_id: set._id, tab_id: chainset.tab_id});
                  });
                }
              });
            }
          ], function(err, results){
              if(err)
                res.status(500).send('Zomato syncing failed.');
              else{
                console.log(results);
                req.query.tab_id = results[0]||undefined;
                req.query.start_time = results[1].start_time||undefined;
                req.query.set_id = results[1].set_id||undefined;
                req.query.action = String(results[0])==String(results[1].tab_id)?'update_menu':'add_menu';
                next();
              }
          });
        }
      });
    }else
      res.status(500).send('Zomato is not your partner.');
  }catch(e){
    return res.status(500).send('Exception occurred.');
  }
}

exports.menu_load_zomato = function(req, res){
  if(!req.query.tab_id||!req.query.start_time||!req.query.set_id||!req.query.action){
    console.log('undefined error')
    res.status(500).send('Zomato syncing failed.');
  }else{
    var done_time = new Date();
    var tab_id = String(req.query.tab_id);
    var start_time = new Date(req.query.start_time);
    var set_id = req.query.set_id;
    var action = req.query.action;
    var range = {$gte: start_time, $lt: done_time};       
    async.series([
      function(callback){
        Item.find({deployment_id: req.query.deployment_id, "tabs._id": tab_id, updated: range},
        {"tabs.$":1, created:1, updated:1, name:1, description:1, category:1, mapItems:1, mapComboItems:1}, function(err, items){
          if(!err){
            if(action=='update_menu'){ //If updated addon items then also send the mapped items
              var mappedItems = [];
              items.forEach(function(item){if(item.category.isAddons) mappedItems=mappedItems.concat(item.mapItems);});
              mappedItems = mappedItems.map(function(item){return String(item._id);});
              Item.find({deployment_id: req.query.deployment_id, "tabs._id": tab_id, $or: [{_id: {$in: mappedItems}}, {"mapItems._id": {$in: mappedItems}}]},
              {"tabs.$":1, created:1, updated:1, name:1, description:1, category:1, mapItems:1, mapComboItems:1}, function(err, mapitems){
                if(!err){
                  items = items.concat(mapitems);
                  //send only unique items
                  var unique_items_ids = {};
                  items.forEach(function(item){if(!unique_items_ids[item._id]) unique_items_ids[item._id] = item;});
                  var unique_items = Object.keys(unique_items_ids).map(function(item_id){return unique_items_ids[item_id];});
                  callback(null, unique_items);
                }else
                  callback(err);
              });
            }else
              callback(null, items);
          }else
            callback(err);
        });
      },
      function(callback){
        Charges.find({deployment_id: req.query.deployment_id, "tabs._id": tab_id}, function(err, charges){
          if(!err)
            callback(null, charges);
          else
            callback(err);
        });
      },
      function(callback){
        if(action=='update_menu'){
          DeleteItem.find({deployment_id: req.query.deployment_id, "tabs._id": tab_id, updated: range},
          {"tabs.$":1, created:1, updated:1, name:1, description:1, category:1, mapItems:1, mapComboItems:1, isDeleted:1}, function(err, items){
            if(!err)
              callback(null, items);
            else
              callback(err);
          });
        }else
          callback(null, []);
      }
    ], function(err, results){
      if(err)
        return res.status(402).json({message: 'Could not push changes to Zomato due to error.'});
      else if(results[0].length==0 && results[2].length==0)
        return res.status(404).json({message: 'Could not find any items to update to Zomato.'});
      else{
        console.log(results[0].length, results[1].length, results[2].length);
        var payload = {
          outlet_id: req.query.deployment_id,
          action: action,
          menu: Menu_Builder['zomato'](results[0], results[1], results[2])
        };
        //require('fs').writeFileSync('zomato-menu.json', JSON.stringify(payload.menu, null, 3));
        var options = {
          method: 'POST',
          url: 'https://api.zomato.com/vendor/menu',
          headers:{
            'content-type':'application/json',
            'auth-key': ZOMATO_AUTH_KEY
          },
          json:payload
        };
        //console.log(options);
        request(options, function (error, response, body) {
          if( response && response.statusCode==200){
            Partner_Sync_Chainset.update({_id: set_id},{$set:{tab_id: tab_id, done_time: done_time, status: 'done', data: JSON.stringify(payload)}}, function(err){
              return res.status(200).json({message: 'Menu has been synced with Zomato.'});
            });
          }
           else{
            console.log(error, body)
            return res.status(500).json({message: 'Menu syncing has failed with Zomato.'});
          }
        });
      }
    });
  }  
}

exports.statusZomato = function(req, res){
  var order = req.body;
  var data = {};
  data.order_id = order.source.order_id;
  var api_call = true;
  switch(req.query.status){
    case 'rejected': 
      data.action = 'reject';
      var rejections = {
        'Items Out of Stock': 1,
        'Delivery boy not available': 2
      };
      data.rejection_message_id = rejections[order.reason]||1;
    break;
    case 'preparing':
      data.action = 'confirm';
      data.pos_order_id = order.billNumber;
      data.delivery_time = 75;
    break;
    case 'out_for_delivery':
      data.action = 'move_to_delivery';
      data.rider_name = order.deliveryInfo?order.deliveryInfo.firstname+' '+order.deliveryInfo.lastname:'';
      data.rider_phone_number = '';
    break;
    case 'delivered':
      data.action = 'delivered';
    break;
    default:
      api_call = false;
  }

  if(api_call){
    var options = {
      method: 'POST',
      url: 'https://api.zomato.com/vendor/update_order_status',
      //url: 'http://localhost:3000/callback',
      headers:{
        'content-type':'application/json',
        'auth-key': ZOMATO_AUTH_KEY
      },
      json: data
    };

    request(options, function (error, response, body) {
        console.log(error,body);
       if( response && response.statusCode==200){
        return res.status(200).json();
       }
       else return res.status(500).json();
    });
  }else
    res.send();
}