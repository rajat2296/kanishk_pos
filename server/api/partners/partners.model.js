'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PartnerSchema = new Schema({
    partner_name: String,
    deployment_id: String,
    customer_key: String,
    tenant_id: String,
    partner_merchant_id:String
});

PartnerSchema.index({ deployment_id: 1, customer_key: 1 });

module.exports = mongoose.model('Partner', PartnerSchema);