'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PartnerSchema = new Schema({
    partner_name: String,
    sourceId: String,
    tab_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    data: String,
    created: {type: Date, default: Date.now},
    start_time: Date,
    done_time: Date,
    status: String
});

module.exports = mongoose.model('PartnerSyncChainset', PartnerSchema);