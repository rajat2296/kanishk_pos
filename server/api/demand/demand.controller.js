'use strict';

var _ = require('lodash');
var Demand = require('./demand.model').Demand;
var Reason = require('./demand.model').Reason;
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var request = require('request');
var Deployment = require('../deployment/deployment.model');
var Tenant = require('../tenant/tenant.model');

// Get list of demands
exports.index = function(req, res) {

  var query = {
    deployment_id: req.query.deployment_id,
    tenant_id: req.query.tenant_id
  }

  if((req.query.isSupplied == 'true' || req.query.isSupplied == true)) {
    query.isSupplied = true;
    query.isReceived = false;
  }
  if((req.query.isReceived == 'true' || req.query.isReceived == true)){
    query.isSupplied = true;
    query.isReceived = true;
  }
  console.log(query);
  Demand.find(query, function (err, demands) {
    if(err) { return handleError(res, err); }
    return res.json(200, demands);
  });
};

// Get a single demand
exports.show = function(req, res) {
  Demand.findById(req.params.id, function (err, demand) {
    if(err) { return handleError(res, err); }
    if(!demand) { return res.send(404); }
    return res.json(demand);
  });
};

// Creates a new demand in the DB.
exports.create = function(req, res) {
  //console.log(req.body);
  console.log(req.body.demandNumber);
  console.log('LN', req.body.xmlRequest);
  var options = { method: 'POST',
    uri: 'http://202.59.2.204/JFPLService_2016/JFPLService.asmx/GenerateDemand',
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body:req.body.xmlRequest,
  };

  request(options, function (error, response, body) {
    console.log(body, error)
    if (error) {
      return handleError(res, error);
    }
    else {
      //var data = req.body;
      //data.accessToken = body.data.access_token;
      //console.log(data)
      Demand.create(req.body, function(err, demand) {
        if(err) { return handleError(res, err); }
        return res.json(201, demand);
      });
    }
  });
};

// Updates an existing demand in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Demand.findById(req.params.id, function (err, demand) {
    if (err) { return handleError(res, err); }
    if(!demand) { return res.send(404); }
    var updated = _.merge(demand, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, demand);
    });
  });
};

// Deletes a demand from the DB.
exports.destroy = function(req, res) {
  Demand.findById(req.params.id, function (err, demand) {
    if(err) { return handleError(res, err); }
    if(!demand) { return res.send(404); }
    demand.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.getLastDemandBillNumber = function (req, res) {
  var currentDate = new Date(req.query.currentDate);
  var deployment_id = new ObjectId(req.query.deployment_id);
  Demand.find({
    created: {
      $lte: currentDate
    },
    deployment_id: deployment_id
  }, {created: 1, demandNumber: 1, demandBillNumber: 1, daySerialNumber: 1}, {sort: {created: -1}, limit: 1}, function (err, demands) {
    if(err) return handleError(res, err);
    if(demands.length > 0)
      return res.status(200).json(demands[0]);
    else return res.status(200).json({});
  });
}

exports.supplyDemand = function(req, res) {

  if(req.body) {
    var valid = true;
    if(!req.body.deployment_id)
      return res.send(200, "Deployment id not found");
    if(!req.body.demandNumber)
      return res.send(200, "Demand Number not found");
    if(!req.body.transferOrderNumber)
      return res.send(200, "Transfer Order Number not found");
    if(!req.body.items)
      return res.send(200, "No supplied items found");
    if(req.body.items.length == 0)
      return res.send(200, "No supplied items found");

    Demand.findOne({deployment_id: req.body.deployment_id, demandNumber: req.body.demandNumber}, function (err, demand) {
      if(err)
        return handleError(res, err);
      if(!demand)
        return res.send("Demand not found in database.");
      _.forEach(demand.items, function (item){
        var itm = _.find(req.body.items, function (it) {
          return it.code == item.code;
        });
        if(itm) {
          if(itm.suppliedQty == null || itm.suppliedQty == undefined || isNaN(itm.suppliedQty))
            return res.send("Please provide valid supply quantity of all items");
          if(itm.supplyRate == null || itm.supplyRate == undefined || isNaN(itm.supplyRate))
            return res.send("Please provide valid rate of all items");
          if(itm.unit == null || itm.unit == undefined)
            return res.send("Unit not found for items");
          if(itm.suppliedQty > item.qty)
            return res.send("Supply quantity is more than asked quantity");
          if(itm.unit != item.selectedUnit.unitName)
            return res.send("Invalid Unit for item found");
          item.suppliedQty = itm.suppliedQty;
          item.supplierComment = itm.supplierComment;
          item.supplyRate = itm.supplyRate;
        }
      });
      demand.isSupplied = true;
      demand.supplyDate = new Date();
      demand.transferOrderNumber = req.body.transferOrderNumber;
      demand.save(function (err) {
        if(err)
          return handleError(res, err);
        else
          return res.send(200, "success");
      });
    });
  }
}

//-------------------------------------------Reason Api-------------------------------------

exports.index = function(req, res) {

  var query = {
    deployment_id: req.query.deployment_id,
    tenant_id: req.query.tenant_id
  }

  console.log(query);
  Reason.find(query, function (err, demands) {
    if(err) { return handleError(res, err); }
    return res.json(200, demands);
  });
};

// Get a single demand
exports.show = function(req, res) {
  Reason.findById(req.params.id, function (err, demand) {
    if(err) { return handleError(res, err); }
    if(!demand) { return res.send(404); }
    return res.json(demand);
  });
};

exports.pushReason = function (req, res) {

  var reason = req.body;
  if(!reason.deployment_id || !reason.reasonCode || !reason.reasonDescription){
    return res.send("Please provide required parameters!");
  }
  Deployment.findOne({_id: reason.deployment_id}, {tenant_id: 1}, function (err, deployment) {
    if(err)
      return handleError(err);
    if(!deployment)
      return res.send("Deployment not found! Please check deployment_id.");
    Tenant.findOne({_id: deployment.tenant_id}, {_id: 1}, function (err, tenant){ 
      if(err)
        return handleError(res, err);
      if(!tenant)
        return res.send("Deployment not associated with any tenant.");
      Reason.findOne({deployment_id: reason.deployment_id, reasonCode: reason.reasonCode}, function (err, r) {
        if(err)
          return handleError(res, err);
        if(reason)
          return res.send("Reason already exists in database.");
        Reason.create(reason, function (err, reason) {
          if(err)
            return handleError(res, err);
          return res.status(200).json({errorcode: 200, message: "Reason added successfully!"});
        });
      });
    });
  });
};



function handleError(res, err) {
  return res.send(500, err);
}
