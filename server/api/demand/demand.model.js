'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DemandSchema = new Schema({
  demandNumber: Number,
  demandBillNumber: Number,
  daySerialNumber:Number,
  transferOrderNumber: String,
  store:{
    _id: String,
    storeName: String,
    store_id: String
  },
  user:{
    username: String,
    _id: String,
    firstname: String,
    lastname: String,
    role: String
  },
  vendor: {},
  items:[{
    qty: Number,
    comment: String,
    price: Number,
    suppliedQty: Number,
    supplierComment: String,
    supplyRate: Number,
    receivedQty: Number,
    receiverComment: String,
    selectedUnit: Object,
    selectedUnitId: Object,
    category: Object,
    units: [],
    toDate: String,
    fromDate: String,
    _id: String,
    itemName: String,
    code: String
  }],
  tenant_id: Schema.Types.ObjectId,
  deployment_id: Schema.Types.ObjectId,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  supplyDate: { type: Date,default:null},
  isSupplied: {type: Boolean, default: false},
  receiveDate: { type: Date,default:null},
  isReceived: {type: Boolean, default: false},
  active: { type: Boolean,default:true}
},{versionKey:false});

var ReasonSchema = new Schema({
  deployment_id: Schema.Types.ObjectId,
  tenant_id: Schema.Types.ObjectId,
  reasonCode: String,
  reasonDescription: String,
  created: {type: Date, default: Date.now()},
  updated: {type: Date, default: Date.now()}
}, {versionKey: false});

ReasonSchema.pre('save', function(next) {
  this.updated = Date.now();
  next();
});

module.exports = {Demand: mongoose.model('Demand', DemandSchema), Reason: mongoose.model('Reason', ReasonSchema)};
