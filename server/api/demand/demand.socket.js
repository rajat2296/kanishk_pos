/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Demand = require('./demand.model');

exports.register = function(socket) {
  Demand.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Demand.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('demand:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('demand:remove', doc);
}