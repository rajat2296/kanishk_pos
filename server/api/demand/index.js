'use strict';

var express = require('express');
var controller = require('./demand.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/getLastDemandBillNumber', controller.getLastDemandBillNumber);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/pushReason', controller.pushReason);
router.post('/supplyDemand', controller.supplyDemand);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
