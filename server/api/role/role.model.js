'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RoleSchema = new Schema({
    name: String,
    permissions: Array,
    tenant_id: Schema.Types.ObjectId,
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Role', RoleSchema);