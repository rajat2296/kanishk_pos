'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ComplementaryHeadSchema = new Schema({
  name: String,
  deployment_id: String,
  tenant_id: String
});

module.exports = mongoose.model('ComplementaryHead', ComplementaryHeadSchema);