var _ = require('lodash');
var moment = require('moment');
var Bill = require('../bill/bill.model');
var Deployment = require('../deployment/deployment.model');
var mongoose = require('mongoose');
var request = require('request');
var Customer = require('../customer/customer.model')
var Q = require('q');
var User = require('../user/user.model');
var htmlparser = require("htmlparser2");
module.exports = {
  createGetUrl:function(query){
    var queryString='';
    for (var key in query) {
  if (query.hasOwnProperty(key)) {
    queryString+=key+'='+query[key]+'&';
  }
}
  console.log(queryString.substring(0, queryString.length-1));
  return (queryString.substring(0, queryString.length-1));
  },
  validateCode: function (code, toffer) {
    var offer = (toffer);
    var currentDate = this.getDateFormattedDate(new Date());
    var flag = false;
    if (_.has(offer.code, 'codeList')) {
      for (var i = 1; i <= offer.code.codeList.noOfCodes; i++) {
        if (code == (offer.code.codeList.codePrefix + i.toString())) {
          if (!(offer.code.codeList.validFrom == null || offer.code.codeList.validFrom == '' || offer.code.codeList.validTo == null || offer.code.codeList.validTo == '')) {
            if (moment(currentDate).isBetween(this.getDateFormattedDate(new Date(offer.code.codeList.validFrom)), this.getDateFormattedDate(new Date(offer.code.codeList.validTo)))) {
              flag = true;
            }
          } else {
            flag = true;
          }
          break;
        }
      }
    }
    return flag;
  },
  hasSetting: function (settingName, settingsArray, tabtype) {
    // console.log(settingsArray);
    var flag = false;
    var _f = _.filter(settingsArray, {name: settingName});
    if (_f.length > 0) {
      if (_.has(_f[0], 'tabs')) {
        var _ft = _.filter(_f[0].tabs, {tabType: tabtype})
        if(_ft.length>0) {
          flag = _ft[0].selected;
        }
      } else {
        flag = _f[0].selected;
      }
    }
    return flag;
  },
  offerValidated: function (offer) {
    var currentDate = this.getDateFormattedDate(new Date());

    /*Validation on date*/
    if (!(offer.valid.date.startDate == null || offer.valid.date.startDate == '' || offer.valid.date.endDate == null || offer.valid.date.endDate == '')) {
      //if( (this.getDateFormattedDate(currentDate)>this.getDateFormattedDate(offer.valid.date.startDate)) &&(this.getDateFormattedDate(currentDate)<this.getDateFormattedDate(offer.valid.date.endDate))){
      if (moment(currentDate).isBetween(this.getDateFormattedDate(new Date(offer.valid.date.startDate)), this.getDateFormattedDate(new Date(offer.valid.date.endDate)))) {
        // console.log('validated date');
      } else {
        return false;
      }
    } else {
      //console.log('validated date');
    }
    var startTime = moment(new Date(this.getDateFormattedDate(new Date()) + " " + moment(new Date(offer.valid.time.startTime)).format("LT")));
    var endTime = moment(new Date(this.getDateFormattedDate(new Date()) + " " + moment(new Date(offer.valid.time.endTime)).add(-1, 'seconds').format("LT")));
    var currentTime = moment();
    if ((startTime.isBefore(currentTime) && currentTime.isBefore(endTime))) {
      //console.log('validated time');
    } else {
      return false;
    }

    /*Validation on days*/
    var isDayFound = false;
    for (var i = 0; i < offer.valid.days.length; i++) {
      if (moment.weekdays(new Date().getDay()).toUpperCase() == offer.valid.days[i].toUpperCase()) {
        isDayFound = true;
        break;
      }
    }
    if (isDayFound) {
      //    console.log('validated day');
    } else {
      return false;
    }

    /*Validate dateList*/
    var dateLists = offer.valid.dateLists;
    var isMatchedDateList = false;
    for (var i = 0; i < dateLists.length; i++) {
      for (var j = 0; j < dateLists[i].dates.length; j++) {
        if (dateLists[i].repeat == 'daily') {
          if (dateLists[i].dates[j].selected) {
            if (this.getDateFormattedDate(new Date(dateLists[i].dates[j].date)) == this.getDateFormattedDate(new Date())) {
              isMatchedDateList = true;
              break;
            }
          }
        }
        if (dateLists[i].repeat == 'monthly') {
          if (dateLists[i].dates[j].selected) {
            if (this.getDate(new Date(dateLists[i].dates[j].date)) == this.getDate(new Date())) {
              isMatchedDateList = true;
              break;
            }
          }
        }
        if (dateLists[i].repeat == 'yearly') {
          if (dateLists[i].dates[j].selected) {
            if (this.getMonthDate(new Date(dateLists[i].dates[j].date)) == this.getMonthDate(new Date())) {
              isMatchedDateList = true;
              break;
            }
          }
        }
      }
    }
    // console.log(isMatchedDateList);
    if (dateLists.length > 0) {
      if (offer.valid.behaviourDateList == 'Include') {
        if (isMatchedDateList) {
          //    console.log('validated dateList');
        } else {
          return false;
        }
      } else {
        if (!isMatchedDateList) {
          //    console.log('validated dateList');
        } else {
          return false;
        }
      }
    }
    return true;
  },
  convertDate: function (d) {
    return (
      d.constructor === Date ? d :
        d.constructor === Array ? new Date(d[0], d[1], d[2]) :
          d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
              typeof d === "object" ? new Date(d.year, d.month, d.date) :
                NaN
    );
  },
  getDateFormatted: function (datetime) {

    var cdate = typeof(datetime)=='string'?new Date(datetime):datetime;
    var month = (cdate.getMonth() + 1).toString();
    var day = (cdate.getDate()).toString();
    var hrs = cdate.getHours().toString();
    var minutes = cdate.getMinutes().toString();
    var ss = cdate.getSeconds().toString();
    month = ((month.length == 1) ? "0" + month : month);
    day = ((day.length == 1) ? "0" + day : day);
    hrs = ((hrs.length == 1) ? "0" + hrs : hrs);
    minutes = ((minutes.length == 1) ? "0" + minutes : minutes);
    ss = ((ss.length == 1) ? "0" + ss : ss);
    //alert(day);
    //if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)
    //  return cdate.getFullYear() + "/" + month + "/" + day + " " + hrs + ":" + minutes + ":" + ss;
    //else
    return cdate.getFullYear() + "-" + month + "-" + day + " " + hrs + ":" + minutes + ":" + ss;
  },
  getDateFormattedDate: function (datetime) {
    var cdate = datetime;
    var month = (cdate.getMonth() + 1).toString();
    var day = (cdate.getDate()).toString();
    month = ((month.length == 1) ? "0" + month : month);
    day = ((day.length == 1) ? "0" + day : day);
    //alert(day);
    return cdate.getFullYear() + "-" + month + "-" + day;
  },
  getMonthDate: function (datetime) {
    var cdate = datetime;
    var month = (cdate.getMonth() + 1).toString();
    var day = (cdate.getDate()).toString();
    month = ((month.length == 1) ? "0" + month : month);
    day = ((day.length == 1) ? "0" + day : day);
    //alert(day);
    return month + "-" + day;
  },
  getDate: function (datetime) {
    var cdate = datetime;
    //var month = (cdate.getMonth() + 1).toString();
    var day = (cdate.getDate()).toString();
    //month = ((month.length == 1) ? "0" + month : month);
    day = ((day.length == 1) ? "0" + day : day);
    //alert(day);
    return day;
  },

   //utils.js api///

 getBills: function (query) {
  // console.log(cust_id);
  console.log('getBills',query);
  var self = this;
  var d = Q.defer();
  var fdate = new Date(query.fromDate);
  var tdate = new Date(query.toDate);
  tdate=tdate.setDate(tdate.getDate()+1);
  // tdate = new Date( tdate.setDate(tdate.getDate() + 1));
  //console.log(query);
  //console.log(query.tabType.toLowerCase())
  if (query.tabType != "") {
    var itemCondition =
    {
      _created: {
        $gte: new Date(fdate), $lte:new Date(tdate)
      },
      deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
      isDeleted: {$ne: true}
      , isVoid: {$ne: true},
      tab: query.tabType
    };

    var modifiedCondition =
    {
      _created: {
        $gte: new Date(fdate), $lte:new Date(tdate)
      },
      deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
      isDeleted: {$ne: true},
      tab: query.tabType,
      isVoid:{$ne:true}
    };
  }else
  if(query.type =='cashier'){
   console.log('inside');
    var itemCondition =
    {
      _created: {
        $gte: new Date(fdate), $lte:new Date(tdate)
      },
      deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
      isDeleted: {$ne: true},
      isVoid: {$ne: true},
      '_currentUser.selectedRoles.name':'Cashier'
    };

    var modifiedCondition =
    {
      _created: {
        $gte: new Date(fdate), $lte:new Date(tdate)
      },
      deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
      isDeleted: {$ne: true},
     // tab: query.tabType,
      isVoid:{$ne:true},
      '_currentUser.selectedRoles.name':'Cashier'
    };
  }else {
    console.log("insideElse");


    var itemCondition =
    {
      _created: {
        $gte: new Date(fdate), $lte:new Date(tdate)
      },
      deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
      isDeleted: {$ne: true}
      , isVoid: {$ne: true}

    };

    var modifiedCondition =
    {
      _created: {
        $gte: new Date(fdate), $lte:new Date(tdate)
      },
      deployment_id: new mongoose.Types.ObjectId(query.deployment_id),
      isDeleted: {$ne: true},

      //, isVoid:{$ne:true}
    };
  }
  /* Bill.find(condition,{serialNumber:1,daySerialNumber:1,splitBillNumber:1,billDiscountAmount:1,'_kots.taxAmount':1,'_kots.totalDiscount':1,'_kots.totalAmount':1,'_kots.isVoid':1,'_currentUser.username':1,'_waiter.username':1,'_delivery.username':1,tab:1},function(err,bills){
   if(err){d.reject()}
   else{*/
  Q.all([
    self.getBillsDetail(modifiedCondition),
    self.getItemsByType(itemCondition, query.type)
  ]).then(function (result) {
    d.resolve({bills: result[0], items: result[1]});
  },
    function (err) {
      d.reject(err);
    });
  /*self.getBillsDetail(modifiedCondition).then(function (bills) {
    self.getItemsByType(itemCondition, query.type).then(function (users) {
      d.resolve({bills: bills, items: users});
    }, function (err) {
      d.reject(err);
    })
  }, function (err) {
    //console.log(err);
    d.reject(err);
  });*/
  //};
  //})
  return d.promise;
},

   getItemsByType: function (modifiedCondition, type) {

    var d = Q.defer();
    /////////////////////////////// Waiter wise////////////
    if (type == 'waiter') {
      Bill.aggregate([{$match: modifiedCondition},
        {$unwind: "$_kots"},
        {$project: {kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]}, user: '$_waiter.username',
          '_kots.items._id':1,
          '_kots.items.name':1,
          '_kots.items.quantity':1,
          '_kots.items.rate':1,
          '_kots.items.subtotal':1,
          '_kots.items.addOns.subtotal':1,
          '_kots.items.addOns.rate':1,
          '_kots.items.addOns.name':1,
          '_kots.items.addOns._id':1,
          '_kots.items.addOns.quantity':1,
          //user:"$user"
        }},

        {$unwind: "$kot.items"},

        {$project: {items: ["$kot.items"],type: "$user",
          "addOns": {$cond: {if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]}, then: "$kot.items.addOns", else: []}}}},
        {$project: {items: {$concatArrays: ["$items", "$addOns"]},type:"$type"}},
        {$unwind: "$items"},
        {$group:{_id:{_id: "$items._id",type:"$type"}, name:{$first:"$items.name"} ,rate: {$first: "$items.rate"}, quantity:{$sum:"$items.quantity"}, subtotal: {$sum:"$items.subtotal"}
        }},
        { $project:{
          _id:0,
          type: {$cond: ["$_id.type", "$_id.type", "--None--"]},
          itemName:"$name",
          rate:"$rate",
          qty:"$quantity",
          amount:"$subtotal"

        }}
      ], function (err, items) {

        if (err) {
          console.log(err);
          d.reject(err)
        }
        else {

          d.resolve(items)
        }
        ;
      })
    }
    //////////////////////////////////User Wise///////////
    if (type == 'user') {
      Bill.aggregate([{$match: modifiedCondition},
        {$unwind: "$_kots"},
        {$project: {kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]}, user: '$_currentUser.username',
          '_kots.items._id':1,
          '_kots.items.name':1,
          '_kots.items.quantity':1,
          '_kots.items.rate':1,
          '_kots.items.subtotal':1,
          '_kots.items.addOns.subtotal':1,
          '_kots.items.addOns.rate':1,
          '_kots.items.addOns.name':1,
          '_kots.items.addOns._id':1,
          '_kots.items.addOns.quantity':1,
          //user:"$user"
        }},

        {$unwind: "$kot.items"},

        {$project: {items: ["$kot.items"],type: "$user",
          "addOns": {$cond: {if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]}, then: "$kot.items.addOns", else: []}}}},
        {$project: {items: {$concatArrays: ["$items", "$addOns"]},type:"$type"}},
        {$unwind: "$items"},
        {$group:{_id:{_id: "$items._id",type:"$type"}, name:{$first:"$items.name"} ,rate: {$first: "$items.rate"}, quantity:{$sum:"$items.quantity"}, subtotal: {$sum:"$items.subtotal"}
        }},
        { $project:{
          _id:0,
          type: {$cond: ["$_id.type", "$_id.type", "--None--"]},
          itemName:"$name",
          rate:"$rate",
          qty:"$quantity",
          amount:"$subtotal"

        }}
      ], function (err, items) {

        if (err) {
          d.reject(err)
        }
        else {

          d.resolve(items)
        }
        ;
      })
    }
    //////////////////////////////cashier//////////////////////
     if (type == 'cashier') {
      Bill.aggregate([{$match: modifiedCondition},
        {$unwind: "$_kots"},
        {$project: {kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]}, user: '$_currentUser.username',
          '_kots.items._id':1,
          '_kots.items.name':1,
          '_kots.items.quantity':1,
          '_kots.items.rate':1,
          '_kots.items.subtotal':1,
          '_kots.items.addOns.subtotal':1,
          '_kots.items.addOns.rate':1,
          '_kots.items.addOns.name':1,
          '_kots.items.addOns._id':1,
          '_kots.items.addOns.quantity':1,
          //user:"$user"
        }},

        {$unwind: "$kot.items"},

        {$project: {items: ["$kot.items"],type: "$user",
          "addOns": {$cond: {if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]}, then: "$kot.items.addOns", else: []}}}},
        {$project: {items: {$concatArrays: ["$items", "$addOns"]},type:"$type"}},
        {$unwind: "$items"},
        {$group:{_id:{_id: "$items._id",type:"$type"}, name:{$first:"$items.name"} ,rate: {$first: "$items.rate"}, quantity:{$sum:"$items.quantity"}, subtotal: {$sum:"$items.subtotal"}
        }},
        { $project:{
          _id:0,
          type: {$cond: ["$_id.type", "$_id.type", "--None--"]},
          itemName:"$name",
          rate:"$rate",
          qty:"$quantity",
          amount:"$subtotal"

        }}
      ], function (err, items) {

        if (err) {
          d.reject(err)
        }
        else {

          d.resolve(items)
        }
        ;
      })
    }
    //////////////////////////////////Tab Wise///////////
    if (type == 'tab') {

      Bill.aggregate([{$match: modifiedCondition},

        {$unwind: "$_kots"},
        {$project: {kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]}, user: '$tab',
          '_kots.items._id':1,
          '_kots.items.name':1,
          '_kots.items.quantity':1,
          '_kots.items.rate':1,
          '_kots.items.subtotal':1,
          '_kots.items.addOns.subtotal':1,
          '_kots.items.addOns.rate':1,
          '_kots.items.addOns.name':1,
          '_kots.items.addOns._id':1,
          '_kots.items.addOns.quantity':1,
          //user:"$user"
        }},

        {$unwind: "$kot.items"},

        {$project: {items: ["$kot.items"],type: "$user",
          "addOns": {$cond: {if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]}, then: "$kot.items.addOns", else: []}}}},
        {$project: {items: {$concatArrays: ["$items", "$addOns"]},type:"$type"}},
        {$unwind: "$items"},
        {$group:{_id:{_id: "$items._id",type:"$type"}, name:{$first:"$items.name"} ,rate: {$first: "$items.rate"}, quantity:{$sum:"$items.quantity"}, subtotal: {$sum:"$items.subtotal"}
        }},
        { $project:{
          _id:0,
          type: {$cond: ["$_id.type", "$_id.type", "--None--"]},
          itemName:"$name",
          rate:"$rate",
          qty:"$quantity",
          amount:"$subtotal"

        }}
      ], function (err, items) {

        if (err) {
          d.reject(err)
        }
        else {

          d.resolve(items)
        }
        ;
      })
    }
    //////////////////////////////////Delivery Wise///////////
    if (type == 'delivery') {
      Bill.aggregate([{$match: modifiedCondition},
        {$unwind: "$_kots"},
        {$project: {kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]}, user: '$tab',
          '_kots.items._id':1,
          '_kots.items.name':1,
          '_kots.items.quantity':1,
          '_kots.items.rate':1,
          '_kots.items.subtotal':1,
          '_kots.items.addOns.subtotal':1,
          '_kots.items.addOns.rate':1,
          '_kots.items.addOns.name':1,
          '_kots.items.addOns._id':1,
          '_kots.items.addOns.quantity':1,
          //user:"$user"
        }},

        {$unwind: "$kot.items"},

        {$project: {items: ["$kot.items"],type: "$user",
          "addOns": {$cond: {if: {$and: ["$kot.items.addOns", {$gt: [{$size: "$kot.items.addOns"}, 0]}]}, then: "$kot.items.addOns", else: []}}}},
        {$project: {items: {$concatArrays: ["$items", "$addOns"]},type:"$type"}},
        {$unwind: "$items"},
        {$group:{_id:{_id: "$items._id",type:"$type"}, name:{$first:"$items.name"} ,rate: {$first: "$items.rate"}, quantity:{$sum:"$items.quantity"}, subtotal: {$sum:"$items.subtotal"}
        }},
        { $project:{
          _id:0,
          type: {$cond: ["$_id.type", "$_id.type", "--None--"]},
          itemName:"$name",
          rate:"$rate",
          qty:"$quantity",
          amount:"$subtotal"

        }}
      ], function (err, items) {

        if (err) {
          d.reject(err)
        }
        else {

          d.resolve(items)
        }
        ;
      })
    }
    return d.promise;
  },

 
 getBillsDetail: function (condition) {
    var d = Q.defer();
    //console.log("insideBillsDetails");
    Bill.aggregate([{$match: condition},
      {$unwind: "$_kots"},
      {
        $project: {
          kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
          billDiscountAmount: '$billDiscountAmount',
          serialNumber: '$serialNumber',
          isVoid: '$isVoid',
          _covers:"$_covers",
          _tableId:"$_tableId",
          prefix:'$prefix',
          billNumber:"$billNumber",
          daySerialNumber: '$daySerialNumber',
          splitBillNumber: '$splitNumber',
          user: '$_currentUser.username',
          'waiter': '$_waiter.username',
          'delivery': '$_delivery.username',
          tab: '$tab',
          id: '$_id',
          created: '$_created',
          _closeTime: '$_closeTime',
          payments: "$payments",
          chargesAmount : "$charges.amount"
        }
      },
      {
        $group: {
          _id: {
            serialNumber: '$serialNumber',
            billDiscountAmount: '$billDiscountAmount',
            isVoid: '$isVoid',
            //serialNumber: '$serialNumber',
            daySerialNumber: '$daySerialNumber',
            splitBillNumber: '$splitBillNumber',
            user: '$user',
            'waiter': '$waiter',
            'delivery': '$delivery',
            tab: '$tab',
            prefix:'$prefix',
            id: '$id',
            created: '$created',
            payments:'$payments',
            billNumber:"$billNumber",
            _covers:"$_covers",
            _tableId:"$_tableId",
            _closeTime:'$_closeTime'
          }
          ,
          chargesAmount:{$first:"$chargesAmount"},
          totalAmount: {$sum: "$kot.totalAmount"},
          taxAmount: {$sum: "$kot.taxAmount"},
          discountAmount: {$sum: "$kot.totalDiscount"}
        }
      },
      {
        $project: {
          _id: "$_id.id",
          serialNumber: '$_id.serialNumber',
          //serialNumber: '$serialNumber',
          billDiscountAmount: '$billDiscountAmount',
          billNumber:"$_id.billNumber",
          // isVoid:'$_id.isVoid',
          //serialNumber: '$_id.serialNumber',
          daySerialNumber: '$_id.daySerialNumber',
          prefix:'$_id.prefix',
          splitBillNumber: '$_id.splitBillNumber',
          user: '$_id.user',
          waiter: {$cond: ['$_id.waiter', '$_id.waiter', 'No Waiter Assigned']},
          delivery: {$cond: ['$_id.delivery', '$_id.delivery', 'No Delivery Boy Assigned']},
          tab: '$_id.tab',
          id: '$_id.id',
          created: '$_id.created',
          isVoid: '$_id.isVoid',
          payments: {$cond: ['$_id.isVoid', [], '$_id.payments']},
          totalAmount: {$cond: ['$_id.isVoid', 0, '$totalAmount']},
          taxAmount: {$cond: ['$_id.isVoid', 0, '$taxAmount']},
          chargesAmount:{$cond: ['$_id.isVoid', 0, '$chargesAmount']},
          _closeTime:'$_id._closeTime',
          _covers:"$_id._covers",
          _tableId:"$_id._tableId",
          discountAmount: {$cond: ['$_id.isVoid', 0, {$add: ['$discountAmount', '$_id.billDiscountAmount']}]},

        }
      },
      {$unwind:{path: "$payments.cash", preserveNullAndEmptyArrays: true}},
      {
        $group:{
          _id:{_id:"$_id", billDiscountAmount: '$billDiscountAmount',
            serialNumber: '$serialNumber',
            isVoid: '$isVoid',
            daySerialNumber: '$daySerialNumber',totalAmount:"$totalAmount",taxAmount:"$taxAmount",discountAmount:"$discountAmount",
            splitBillNumber: '$splitBillNumber',
            user: '$user',
            'waiter': '$waiter',
            'delivery': '$delivery',prefix:'$prefix',
            tab: '$tab',
            billNumber:"$billNumber",
            _closeTime:'$_closeTime',
            _covers:"$_covers",
            _tableId:"$_tableId",
            created: '$created'},
          chargesAmount:{$first:'$chargesAmount'},
          totalCash:{$sum:"$payments.cash"},
          cards: {$first: "$payments.cards"}
        }
      },
      {
        $project:{
          _id:"$_id._id",
          "payments.cards":"$cards",
          //_id: "$_id._id",
          serialNumber: '$_id.serialNumber',
          //serialNumber: '$serialNumber',
          billDiscountAmount: '$billDiscountAmount',
          // isVoid:'$_id.isVoid',
          //serialNumber: '$_id.serialNumber',
          prefix:'$_id.prefix',
          daySerialNumber: '$_id.daySerialNumber',
          splitBillNumber: '$_id.splitBillNumber',
          user: '$_id.user',
          billNumber:"$_id.billNumber",
          'waiter': '$_id.waiter',
          'delivery': '$_id.delivery',
          tab: '$_id.tab',
          id: '$_id.id',
          chargesAmount:'$chargesAmount',
          created: '$_id.created',
          _closeTime:"$_id._closeTime",
          _covers:"$_id._covers",
          _tableId:"$_id._tableId",
          isVoid: '$_id.isVoid',
          totalCash:"$totalCash",
          totalAmount:"$_id.totalAmount",
          taxAmount:"$_id.taxAmount",
          discountAmount:"$_id.discountAmount"
        }
      },

      {$unwind:{path: "$payments.cards", preserveNullAndEmptyArrays: true}},
      {
        $group:{
          _id:{_id:"$_id", billDiscountAmount: '$billDiscountAmount',
            serialNumber: '$serialNumber',
            isVoid: '$isVoid',
            daySerialNumber: '$daySerialNumber',totalAmount:"$totalAmount",taxAmount:"$taxAmount",discountAmount:"$discountAmount",
            splitBillNumber: '$splitBillNumber',
            user: '$user',
            billNumber:"$billNumber",
            'waiter': '$waiter',
            prefix:'$prefix',
            'delivery': '$delivery',
            tab: '$tab',_closeTime:"$_closeTime",_covers:"$_covers",_tableId:"$_tableId",
            created: '$created',totalCash:"$totalCash"},
            chargesAmount:{$first:'$chargesAmount'},
          totalCredit: {$sum: { $cond: [{ $eq: [ "$payments.cards.cardType", "CreditCard" ] }, "$payments.cards.totalAmount", 0 ]} },
          totalDebit:  {$sum: { $cond: [{ $eq: [ "$payments.cards.cardType", "DebitCard" ] }, "$payments.cards.totalAmount", 0 ]}},
          totalOther:  {$sum: { $cond: [{ $eq: [ "$payments.cards.cardType", "Other" ] }, "$payments.cards.totalAmount", 0 ] }},
          totalCoupon:  {$sum: { $cond: [{ $eq: [ "$payments.cards.cardType", "Coupon" ] }, "$payments.cards.totalAmount", 0 ] }},
          totalOnline: {$sum: { $cond: [{ $eq: [ "$payments.cards.cardType", "Online" ] }, "$payments.cards.totalAmount", 0 ] }},

        }
      },
      {$project:{
        _id:"$_id._id",
        // payments:"$_id.payments",
        //_id: "$_id._id",
        serialNumber: '$_id.serialNumber',
        //serialNumber: '$serialNumber',
        billDiscountAmount: '$billDiscountAmount',
        // isVoid:'$_id.isVoid',
        //serialNumber: '$_id.serialNumber',
        daySerialNumber: '$_id.daySerialNumber',
        splitBillNumber: '$_id.splitBillNumber',
        user: '$_id.user',
        prefix:'$_id.prefix',
        billNumber:"$_id.billNumber",
        'waiter': '$_id.waiter',
        'delivery': '$_id.delivery',
        tab: '$_id.tab',
        //id: '$_id._id',
        created: '$_id.created',
        _closeTime:'$_id._closeTime',
        _covers:"$_id._covers",
        _tableId:"$_id._tableId",
        isVoid: '$_id.isVoid',
        totalCash:"$_id.totalCash",
        totalAmount:"$_id.totalAmount",
        taxAmount:"$_id.taxAmount",
        discountAmount:"$_id.discountAmount",
        totalCredit:"$totalCredit",
        totalDebit:"$totalDebit",
        totalOther:"$totalOther",
        totalCoupon:"$totalCoupon",
        totalOnline:"$totalOnline",
        chargesAmount:'$chargesAmount'

      }
      }

    ], function (err, items) {

      if (err) {
        console.log(err);
        d.reject(err)
      }
      else {

        //console.log(items.length);
        d.resolve(items)
      }
      ;
    })
    return d.promise;
  },
  getDeploymentsByTenant: function (tenantId) {
    var d = Q.defer();
    var deployments = [];
    Deployment.find({tenant_id: new mongoose.Types.ObjectId(tenantId)}, {name: 1}, function (err, deps) {
      // console.log(deps);
      if (deps) {
        deployments = deps;
      }
      d.resolve(deployments);
    })
    return d.promise;
  },
  sendSMSExotel: function (to, body) {
    var d = Q.defer();
    console.log(to,body)
    request.post("https://posist:ad3024355ae9bdc0024bf34bbe7e35c393bb3c58@twilix.exotel.in/v1/Accounts/posist/Sms/send",
      {
        form: {"From": "08010133399", "To": to, "Body": body}
      }, function (error, response, body) {
        console.log(error)
        console.log(body);
        d.resolve(body)
      });
    return d.promise;
  },
  getUniqueCustomerIds: function (query) {
    /*var _customer={mobile:}
     if(query.mobile!=undefined) {
     var mobile = query.mobile;
     delete query.mobile;
     query._customer = {mobile: mobile};
     }*/
    query._customer.$exists = true;
    console.log(query);
    query._customer.$exists = true;
    var d = Q.defer();
    Bill.aggregate([
      /*{
       $match: {
       _created: {
       $gte: new Date(req.body.startDate), $lte: new Date(req.body.endDate)
       },
       deployment_id: req.body.deployment_id,
       isDeleted: {$ne: true}
       }
       },*/
      {$match: query},
      {
        $project: {
          customer: "$_customer"
        }
      },
      {
        $group: {
          //  _id: {customer_id: {$cond:["$_customer.customerId","$_customer.customerId",'']}},
          _id: {customer_id: "$customer.customerId"},
          count: {$sum: 1}
        }
      },
      {
        $project: {
          _id: 0,
          id: "$_id.customer_id"
        }
      }
    ], function (err, result) {
      var ids = [];
      if (err) {
        d.resolve(ids);
      }
      result.forEach(function (cust) {
        if (cust.id != null)ids.push(cust.id);
      })
      d.resolve(ids);
    });
    return d.promise;
  },
  getBillGTotal: function (condition) {
    // console.log(cust_id);
    var d = Q.defer();
    Bill.aggregate([
      {
        $match: condition
      },

      {
        $unwind: "$_kots"
      },
      {
        $project: {
          kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
          billDiscountAmount: "$billDiscountAmount"
        }
      },
      {
        $group: {
          _id: 0,
          taxAmount: {$sum: "$kot.taxAmount"},
          totalDiscount: {$sum: "$kot.totalDiscount"},
          totalAmount: {$sum: "$kot.totalAmount"},
          billDiscountAmount: {$sum: "$billDiscountAmount"},
          totalItems: {$sum: "$kot.totalItems"},
          totalBills: {$sum: 1}
        }
      },
      {
        $project: {
          _id: 0,
          gTotal: {$subtract: [{$add: ["$taxAmount", "$totalAmount"]}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]},
          tItems: "$totalItems",
          tBills: "$totalBills"
        }
      }
    ], function (err, result) {
      if (err) {
        d.reject('error');
      } else {
        //console.log(result);
        d.resolve(result);
      }
    })
    return d.promise;
  },
  customerCreateWithCheck: function (customer) {
    var d = Q.defer();
    delete customer._id;
    var _customer = customer;
    if (_.has(_customer, 'addresses')) {
      _customer.addresses.forEach(function (add) {
        delete add.$$hashKey;
      });
    }
    _customer.isSynced = true;
    //console.log(req.body);
    //Customer.findOne( $and:[{'tenant_id':req.params.tenant_id,'deployment_Id':req.params.deployment_Id},{$or:[ {'mobile' : req.params.mobile}]}], {}, function (err, customer) {
    Customer.findOne({
      'tenant_id': customer.tenant_id,
      'deployment_id': customer.deployment_id,
      'mobile': customer.mobile
    }, function (err, customer) {
      //console.log(customer);
      if (!customer) {
        //_customer={};
        Customer.create(_customer, function (err, sCustomer) {
          if (err) {
            //console.log(err);
            //return handleError(res, err);
            d.reject('Error in saving customer');
          }

          //return res.json(201, sCustomer);
          d.resolve(sCustomer);
        });
      } else {

        var updated = _.extend(customer, _customer);
        _customer = {};
        updated.markModified("addresses");
        // console.log(updated);
        updated.save(function (err) {
          if (err) {
            //return handleError(res, err);
            d.reject('Error in updating customer');
          }
          //return res.json(200, customer);
          d.resolve(customer);
        })
      }
    });
    return d.promise;
  },

  getTopItems: function (condition) {
    var d = Q.defer();
    Bill.aggregate([
      {
        $match: condition
      },
      {
        $unwind: "$_kots"
      },
      {
        $unwind: "$_kots.items"
      },
      {
        $project: {
          items: "$_kots.items"
        }
      },
      {
        $group: {
          _id: {
            itemName: "$items.name"
          },
          qty: {$sum: "$items.quantity"}
        }
      },
      {
        $project: {
          _id: 0,
          itemName: "$_id.itemName",
          qty: "$qty"
        }
      },
      {$sort: {qty: -1}},
      {$limit: 30}
    ], function (err, result) {
      // console.log(result);
      if (err) {
        //console.log(err);
        d.reject(err);
      } else {
        d.resolve(result);
      }
    })
    return d.promise;
  },
  roundNumber: function (number, decimals) {
    var value = number;
    var exp = decimals;
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);
    value = +value;
    exp = +exp;
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;
    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));
    // Shift back
    value = value.toString().split('e');
    var tVal = +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
    // console.log(tVal);
    return tVal;
    // return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
  },
  getResetDate: function (settings, date) {
    var resetDate = new Date(date);
    var hr = 0;
    for (var i = 0; i < settings.length; i++) {
      if (settings[i].name == 'day_sale_cutoff') {
        hr = new Date(settings[i].value).getHours();
        break;
      }
    }
    resetDate.setHours(hr, 0, 0, 0);
    console.log(resetDate);
    return resetDate;
  },
  getResetDateForTenant: function (value, date) {
    var resetDate = new Date(date);
    var hr = 0;
    hr = new Date(value).getHours();
    resetDate.setHours(hr, 0, 0, 0);
    console.log(resetDate);
    return resetDate;
  },
  getBillGTotalForEachDeployments: function (condition) {
    // console.log(cust_id);
    var d = Q.defer();
    Bill.aggregate([
      {
        $match: condition
      },
      {
        $unwind: "$_kots"
      },
      {
        $project: {
          kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
          billDiscountAmount: "$billDiscountAmount"
        }
      },
      {
        $group: {
          _id: 0,
          taxAmount: {$sum: "$kot.taxAmount"},
          totalDiscount: {$sum: "$kot.totalDiscount"},
          totalAmount: {$sum: "$kot.totalAmount"},
          billDiscountAmount: {$sum: "$billDiscountAmount"}

        }
      },
      {
        $project: {
          _id: 0,
          //     gTotal: {$subtract: [{$add: ["$taxAmount", "$totalAmount"]}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]},
          gTotal: {$subtract: ["$totalAmount", {$add: ["$totalDiscount", "$billDiscountAmount"]}]}

        }
      }
    ], function (err, result) {
      if (err) {
        d.reject('error');
      } else {
        //console.log(result);
        d.resolve(result);
      }
    })
    return d.promise;
  },
  authenticateUser: function (userDetail) {
    //console.log(userDetail.username);
    var d = Q.defer();
    User.find({
      username: userDetail.username
    }, function (err, users) {
      // console.log(users);
      if (users.length == 0) {
        d.reject('user not found');
      } else {
        var user = users[0];
        //  console.log(user);
        if (err) {
          d.reject(err);
        }
        ;

        if (!user) {
          d.reject("user is not registered.");
        }
        if (_.has(userDetail, 'password')) {
          if (!user.authenticate(userDetail.password)) {

            d.reject("User is not authenticated.");
          }

          if (!user.isActivated()) {
            d.reject("user not activated yet.");
          }
        } else {
          d.reject("require password .");
        }

        /* var userSplit = userDetail.username.split('_');
         var subDomain = '';
         if(userSplit.length > 1) {
         subDomain = userSplit[0];
         }*/
        d.resolve(user);
      }

    }, function () {
      d.reject("error in fetching data");
    });
    return d.promise;
  },
  mailParser: function (vendor, html, brand) {
    var self = this;
    var d = Q.defer();
    if (vendor == 'swiggy') {
      var arr = [];
      var type = '';
      var tempItemName = '';
      var brandName = '';
      var isBrandfound = false;
      var isPaymentTypeFound = false;
      var paymentType = '';
      var isCancelled = false;
      var cancelStatus = false;
      var isOrderNumber = false;
      var orderNumber = '';
      var parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
          //if (name === "td" && attribs.width == "30%" && attribs.style === "text-align:left;vertical-align:middle; border-left: 1px solid #eee; border-bottom: 1px solid #eee; border-right: 0; border-top: 0; word-wrap:break-word;") {
           if (name === "td" && attribs.width == "60%" && attribs.style === " font-family:'Roboto', 'Helvetica Neue',Helvetica,Arial,sans-serif !important; line-height:150%;border-bottom: 1px solid #ddd;  border-left: 1px solid #eee; border-bottom: 1px solid #eee; border-right: 1px solid #eee; border-top: 1px solid #eee;") {
             type = 'itemname';
          }
          //if (name === "td" && attribs.width == "15%" && attribs.style === "text-align:right;vertical-align:middle; border-left: 1px solid #eee; border-bottom: 1px solid #eee; border-right: 0; border-top: 0;") {
          if (name === "td" && attribs.width == "20%" && attribs.style === " border-left: 1px solid #eee; border-bottom: 1px solid #eee; border-right: 1px solid #eee; border-top: 1px solid #eee;") {
            type = 'qty';
          }
          //if (name === 'h3' && attribs.style === "text-align:center; text-transform:uppercase;") {
            if (name === 'p' && attribs.style === "text-align:left;  color:#000000; font-family:'Roboto', 'Helvetica Neue',Helvetica,Arial,sans-serif !important; text-transform:uppercase;line-height:0%; font-size:13px; font-weight:bold; ") {
            isBrandfound = true;
          }
       /*   if (name === 'span' && attribs.style === 'font-size:24px;font-weight:bold;color:#7db701') {
            isPaymentTypeFound = true;
          }
          if (name === 'span' && attribs.style === 'font-size:24px;font-weight:bold;color:#7db701') {
       */
        if (name === 'span' && attribs.style === 'font-size:24px;font-weight:bold;color:#7db701') {     
            isPaymentTypeFound = true;
          }
          if (name === 'h2' && attribs.style === "text-align:center; text-transform:uppercase;") {
            cancelStatus = true;
          }
/*          if (name === 'h2' && attribs.style === "color:#dd292a;display:block;font-family:Arial;font-size:24px;font-weight:bold;margin-top:5px;margin-right:0;margin-bottom:5px;margin-left:0;text-align:left;line-height:100%") {
*/
       if (name === 'p' && attribs.style === "font-family:'Roboto', 'Helvetica Neue',Helvetica,Arial,sans-serif !important; text-align:left; font-size: 14px; margin-right:-10px; font-weight: bold; line-height:50%;") {
            isOrderNumber = true;
          }
        },
        ontext: function (text) {
          if (type == 'itemname') {
            //arr.push(getString(text));
            tempItemName = self.getString(text);
            type = '';
          }
          if (type == 'qty') {
            arr.push({itemName: tempItemName, quantity: parseFloat(self.getString(text))});
            type = '';
          }
          if (isBrandfound) {
            brandName = self.getString(text);
            isBrandfound = false;
          }
          if (isPaymentTypeFound) {
            var paymenttype = self.getString(text);
            if (paymenttype == 'Rs.0') {
              paymentType = 'COD';
            } else {
              paymentType = 'PaidOnline';
            }
            isPaymentTypeFound = false;
          }
          if (cancelStatus) {
            //console.log(text);
            var status = self.getString(text).toUpperCase();
            if (status == 'ORDER CANCELLATION') {
              isCancelled = true;
            }
            cancelStatus = false;
          }
          if (isOrderNumber) {
            // console.log(text);
            orderNumber = self.getString(text);
            isOrderNumber = false;
          }
        }
      }, {decodeEntities: true});
      parser.write(html);
      parser.end();
      /*console.log(arr);
       console.log(brandName);*/
       var orderOb={
        brand: brandName,
        items: arr,
        paymentType: paymentType,
        isCancelled: isCancelled,
        orderNumber: orderNumber
      };
      console.log(orderOb);
      d.resolve(orderOb);
    }
    if (vendor == 'zomato') {
      var arr = [];
      var type = '';
      var isqty = '';
      var tempItemName = '';
      var brandName = '';
      var isBrandfound = false;
      var isPaymentTypeFound = false;
      var paymentType = '';
      var isCancelled = false;
      var cancelStatus = false;
      var isOrderNumber = false;
      var orderNumber = '';
      var iscrm = false;
      var crmtype='name';
      var ccrmtype=0;
      var crm = {userDetails: '', userAddress: ''};
      var customer = {firstname: '', mobile: '', address1: '', address2: '', addType: 'residence'};
      var parser = new htmlparser.Parser({
        /* onopentag: function (name, attribs) {
         if (name === "td" && attribs.width == "70%" && attribs.style === "border-collapse:collapse;font-family:sans-serif;font-size:14px;line-height:30px;color:#2d2d2a;text-align:left") {
         type = 'itemname';
         }
         if (name === "td" && attribs.width == "70%" && attribs.style === "border-collapse:collapse;font-family:sans-serif;font-size:14px;line-height:30px;color:#2d2d2a;padding-bottom:10px;text-align:left") {
         type = 'itemname';
         }
         if (name === "span" && attribs.style === "font-size:12px") {
         isqty = true;
         }
         if (name === "td" && attribs.style === "border-collapse:collapse;font-family:sans-serif;font-size:14px;line-height:21px;color:#2d2d2a;padding-bottom:15px;text-align:left") {
         isPaymentTypeFound = true;
         }
         if (name === 'td' && attribs.style === "border-collapse:collapse;font-family:sans-serif;font-size:12px;line-height:17px;color:#7d7d76;text-align:left") {
         isOrderNumber = true;
         }
         if (name === 'div' && attribs.style === "font-size:15px;color:#6d6d6d;font-weight:600") {
         iscrm = true;
         }
         },
         ontext: function (text) {

         if (type == 'itemname') {
         //console.log(text);
         //arr.push(getString(text));
         tempItemName = self.getString(text);
         //console.log(tempItemName);
         type = '';
         }
         if (isqty) {
         // console.log(text);
         //arr.push(getString(text));
         var qty = self.getString(text);
         qty = (qty.replace('(', ""));
         qty = (qty.replace(')', ""));
         if (qty != undefined) {
         var qtyarr = qty.split('x');
         if (qtyarr.length > 1) {
         arr.push({itemName: tempItemName, quantity: parseFloat(self.getString(qtyarr[0]))});
         }
         }
         //console.log(qty);
         isqty = false;
         }
         if (isPaymentTypeFound) {
         paymentType = self.getString(text);
         isPaymentTypeFound = false;
         }
         if (isOrderNumber) {
         var temporder = self.getString(text);
         if (temporder != undefined) {
         var temporderArr = temporder.split(':');
         if (temporderArr.length > 1) {
         orderNumber = temporderArr[1].trim();
         }
         }
         isOrderNumber = false;
         }
         if (iscrm) {
         var tempCRM = self.getString(text);
         if (tempCRM != undefined) {
         var tempCRMArr = tempCRM.split(':');
         if (tempCRMArr.length > 0) {
         if (tempCRMArr[0] == 'User Details') {
         crm.userDetails = tempCRMArr[1];
         }
         if (tempCRMArr[0] == 'User Address') {
         crm.userAddress = tempCRMArr[1];
         }
         }
         }
         }
         }
         }, {decodeEntities: true});*/
        onopentag: function (name, attribs) {
          /* if (name === "td" && attribs.width == "70%" && attribs.style === "-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: sans-serif;font-size: 14px;line-height: 30px;color: #2d2d2a;text-align: left;") {
           type = 'itemname';
           }
           if (name === "td" && attribs.width == "70%" && attribs.style === "-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: sans-serif;font-size: 14px;line-height: 30px;color: #2d2d2a;padding-bottom: 10px;text-align: left;") {
           type = 'itemname';
           }
           if (name === "td" && attribs.width == "70%" && attribs.style === "-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: sans-serif;font-size: 14px;line-height: 30px;color: #2d2d2a;text-align: left;") {
           type = 'itemname';
           }*/
          if (name === "td" && attribs.width == "60%" && attribs.style === "-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: sans-serif;font-size: 14px;line-height: 19px;color: #2d2d2a;text-align: left;padding-bottom: 10px;") {
            type = 'itemname';
          }
          if(name==="td" && attribs.width=="60%" && attribs.style==="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: sans-serif;font-size: 14px;line-height: 19px;color: #2d2d2a;padding-bottom: 10px;text-align: left;padding-bottom: 10px;"){
            type = 'itemname';
          }
          if (name === "span" && attribs.style === "font-size: 12px;") {
            isqty = true;
          }
          if (name === "td" && attribs.width === "70%" && attribs.style === "ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: sans-serif;font-size: 14px;line-height: 21px;color: #2d2d2a;padding-bottom: 15px;text-align: left;") {
            isPaymentTypeFound = true;
          }
          if (name === 'td' && attribs.class === "ta-left smaller-text zdhl5" && attribs.style === "-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: sans-serif;font-size: 12px;line-height: 17px;color: #7d7d76;text-align: left;") {
            isOrderNumber = true;
          }
          /* if (name === 'div' && attribs.style === "font-size: 15px; color:#6d6d6d; font-weight:600;") {
           iscrm = true;
           }*/
          if (name === 'td' && attribs.style === "text-align: left;font-size: 15px;color: #7d7d76;") {
            iscrm = true;
            ccrmtype++;
            if(crmtype=='name'){
              // crmtype='mobile';
            }
          }
        },
        ontext: function (text) {

          if (type == 'itemname') {
            //console.log(text);
            //arr.push(getString(text));
            tempItemName = self.getString(text);
            //console.log(tempItemName);
            type = '';
          }
          if (isqty) {
            // console.log(text);
            //arr.push(getString(text));
            var qty = self.getString(text);
            qty = (qty.replace('(', ""));
            qty = (qty.replace(')', ""));
            if (qty != undefined) {
              var qtyarr = qty.split('x');
              if (qtyarr.length > 1) {
                arr.push({itemName: tempItemName, quantity: parseFloat(self.getString(qtyarr[0]))});
              }
            }
            //console.log(qty);
            isqty = false;
          }
          if (isPaymentTypeFound) {
            paymentType = self.getString(text);
            isPaymentTypeFound = false;
          }
          if (isOrderNumber) {
            var temporder = self.getString(text);
            if (temporder != undefined) {
              var temporderArr = temporder.split(':');
              if (temporderArr.length > 1) {
                orderNumber = temporderArr[1].trim();
              }
            }
            isOrderNumber = false;
          }
          if (iscrm) {
            var tempCRM = self.getString(text);
            // console.log(ccrmtype);
            iscrm=false;
            /* if (tempCRM != undefined) {
             var tempCRMArr = tempCRM.split(':');
             if (tempCRMArr.length > 0) {
             if (tempCRMArr[0] == 'User Details') {
             crm.userDetails = tempCRMArr[1];
             }
             if (tempCRMArr[0] == 'User Address') {
             crm.userAddress = tempCRMArr[1];
             }
             }
             }*/
            if(ccrmtype ==1){
              customer.firstname=tempCRM;
            }
            if(ccrmtype == 2){
              customer.mobile=tempCRM;
            }
            if(ccrmtype == 3){
              crm.userAddress=tempCRM;
            }

          }
        }
      }, {decodeEntities: true});
      parser.write(html);
      parser.end();

      /*if (crm.userDetails != undefined) {
       var crmDetailArr = crm.userDetails.split(',');
       if (crmDetailArr.length > 1) {
       customer.firstname = self.getString(crmDetailArr[0]).trim();
       customer.mobile = self.getString(crmDetailArr[1]).trim();
       }
       }*/
      if (crm.userAddress != undefined) {
        var crmAddressArr = crm.userAddress.split(',');
        if (crmAddressArr.length > 1) {
          var midLen = Math.round(crmAddressArr.length / 2);
          for (var i = 0; i < crmAddressArr.length; i++) {
            if (i < midLen) {
              customer.address1 += crmAddressArr[i] + ',';
            } else {
              customer.address2 += crmAddressArr[i] + ',';
            }
          }
        }
      }
      customer.address1 = customer.address1.replace(/,\s*$/, '').trim();
      customer.address2 = customer.address2.replace(/,\s*$/, '').trim();
      /*console.log(arr);
       console.log(brandName);*/
      d.resolve({
        brand: brand,
        items: arr,
        paymentType: paymentType,
        isCancelled: isCancelled,
        orderNumber: orderNumber,
        crm: customer
      });
    }
    if (vendor == 'foodpanda') {
      var arr = [];
      var type = '';
      var tempItemName = '';
      var brandName = '';
      var isBrandfound = false;
      var isPaymentTypeFound = false;
      var paymentType = '';
      var isCancelled = false;
      var cancelStatus = false;
      var isOrderNumber = false;
      var isOrderNumberFoundDone = false;
      var orderNumber = '';
      var isContinue = true;
      var position = 0;
      var count = 0;
      var tempQty = '';
      var statustmp = '';
      var tempCustomerAddress = [];
      var customer = {firstname: '', mobile: '', address1: '', address2: '', addType: 'residence'};
      var parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
          if (name === "strong") {
            type = 'itemname';
          }
          if (name == 'div' && attribs.align == 'right' && (!isOrderNumberFoundDone)) {
            isOrderNumber = true;
          }
          if (name === 'td' && attribs.style === "font-family:Arial,Helvetica,sans-serif;font-size:14px;padding:14px;padding-right:5px;") {
            isPaymentTypeFound = true;
          }
          if (name === 'td' && attribs.style === "font-family:Arial,Helvetica,sans-serif;font-size:14px;padding:14px;padding-right:5px") {
            isPaymentTypeFound = true;
          }
        },
        ontext: function (text) {
          if (type == 'itemname') {
            //arr.push(getString(text));
            tempItemName = self.getString(text);
            if (position == 1) {
              //console.log(tempItemName);
              var brandArr = tempItemName.split(',');
              if (brandArr.length > 1) {
                brandName = brandArr[0].trim();
              }
            }
            if (position > 1 && isContinue) {
              if (tempItemName.trim() == 'SUBTOTAL') {
                isContinue = false;
              } else {
                // console.log(tempItemName);
                // console.log(count);
                if (count == 0) {
                  var qtyArr = tempItemName.split('X');
                  if (qtyArr.length > 1) {
                    tempQty = qtyArr[0];
                  }
                }
                if (count == 1) {
                  //console.log(tempItemName);
                  arr.push({itemName: tempItemName, quantity: parseFloat(tempQty.trim())});
                }

                if (count == 2) {
                  count = 0;
                } else {
                  count += 1;
                }

              }
            }
            type = '';
            position += 1;
          }
          if (isOrderNumber) {
            var tempOrderNoArr = self.getString(text).split('#');
            if (tempOrderNoArr.length > 1) {
              // console.log(tempOrderNoArr[1].trim());
              orderNumber = tempOrderNoArr[1].trim();
              isOrderNumber = false;
              isOrderNumberFoundDone = true;
            }
          }
          if (isPaymentTypeFound) {

            console.log(self.getString(text));
            var crmArr = self.getString(text).split('<br>');

            //console.log(crmArr);
            _.forEach(crmArr, function (dataerr) {
              var data = dataerr.trim();

              if (data == 'Delivery to:') {
                statustmp = 'dname';
                //console.log(statustmp);
                // console.log(statustmp +':'+data.trim());
              } else {
                // console.log(statustmp +':'+data.trim());
                if (!(data == '' || statustmp == 'done')) {
                  var mobArr = data.split(':');
                  if (mobArr.length > 1) {
                    if (mobArr[0] == 'Customer mobile') {
                      customer.mobile = mobArr[1].trim().substr(3).trim();
                      statustmp = 'mobile';
                    }
                    if (mobArr[0] == 'Customer Name') {
                      customer.firstname = mobArr[1].replace(/\s+/g, " ").trim();
                      statustmp = 'name';
                    }
                    if (mobArr[0] == 'Payment method') {
                      paymentType = mobArr[1];
                      statustmp = 'done';
                    }
                  }
                  /*}
                   if (data != '') {*/
                  if (statustmp == 'address') {
                    tempCustomerAddress.push(data.replace(/\s+/g, " "));
                  }
                  if (statustmp == 'dname') {
                    // console.log('name found:'+data);
                    customer.firstname = data.replace(/\s+/g, " ");
                    statustmp = 'address'
                  }
                }
              }
            })
            // isPaymentTypeFound=false;
          }
        }
      }, {decodeEntities: true});
      parser.write(html);
      parser.end();
      crmAddressArr = tempCustomerAddress;
      if (crmAddressArr.length > 1) {
        var midLen = Math.round(crmAddressArr.length / 2);
        for (var i = 0; i < crmAddressArr.length; i++) {
          if (i < midLen) {
            customer.address1 += crmAddressArr[i] + ',';
          } else {
            customer.address2 += crmAddressArr[i] + ',';
          }
        }
      }

      customer.address1 = customer.address1.replace(/,\s*$/, '').trim();
      customer.address2 = customer.address2.replace(/,\s*$/, '').trim();
      // console.log(customer);
      //console.log(arr);
      /* console.log(brandName);*/
      d.resolve({
        brand: brandName,
        items: arr,
        paymentType: paymentType.trim().split(' ')[0],
        isCancelled: isCancelled,
        orderNumber: orderNumber,
        crm: customer
      });
    }
    return d.promise;
  },
  getString: function (rawHtml) {
    var string = '';
    string = rawHtml.replace(/(\r\n|\n|\r)/gm, " ");
    return string.trim();
  },

  getCRMCustomers:function(condition){
    var skipRecord = parseInt(condition.skip)*15;
    var d= Q.defer();
    var condition1 = condition.query;
    condition1._customer.$exists = true;
    var condition2 = {};
    if(condition.amount){
      condition2.totalAmount = condition.amount
    }
    if(condition.frequency){
      condition2.totalBills = condition.frequency
    }
    if((condition.skip)!=null && (condition.skip!=undefined)){
      Bill.aggregate([
        {
          $match:condition1
        },
        {
          $project:{
            _id:1,
            totalAmount:"$aggregation.netRoundedAmount",//{$subtract:[{$add:["$taxAmount","$totalAmount"]},{$add:["$totalDiscount","$billDiscountAmount"]}]},
            //totalItems:"$totalItems",
            covers : {$cond:['$_covers','$_covers',0]},
            customerId:"$_customer.customerId"
          }
        },
        {
          $group: {
            _id: "$customerId",//_id: {billId: "$serialNumber", billDiscountAmount: "$billDiscountAmount", customer: "$customer"}
            totalBills:{$sum:1},
            totalAmount: {$sum: "$totalAmount"},
            avgAmount : {$avg: "$totalAmount"},
            covers:{$avg:"$covers"}
          }
        },
        {
          $match:condition2
        },
        {
          $sort : {totalAmount:-1}
        },
        {
          $skip : skipRecord
        },
        {
          $limit : condition.limit
        }
      ], function (err, result) {
        if(err){d.reject('error');}else {
          d.resolve(result);
        }
      })}
    else{
      var result = [];
      d.resolve(result);
    }
    return d.promise;
  }
}
