'use strict';

var _ = require('lodash');
var Shopify = require('./shopify.model');
var User = require('../user/user.model');
var Item = require('../item/item.model');
var shopifyAPI = require('shopify-node-api');
var request = require('request');
var async = require('async');
var shopifyObject;

// Get list of roles
exports.index = function (req, res) {
  console.log(req.query)
  // console.log("helo")
  Shopify.find({tenantId: req.query.tenantId, deploymentId: req.query.deploymentId}, function (err, roles) {
    if (err) {
      return handleError(res, err);
    }
    //console.log(roles)
    return res.send(roles);
  });
};

// Get a single role
exports.show = function (req, res) {
  Shopify.findById(req.params.id, function (err, role) {
    if (err) {
      return handleError(res, err);
    }
    if (!role) {
      return res.send(404);
    }
    return res.json(role);
  });
};

// Creates a new role in the DB.
exports.create = function (req, res) {
  shopifyObject = {
    shopName:req.body.shopName,
    apiKey:req.body.apiKey,
    sharedSecret:req.body.sharedSecret,
    callbackUrl:req.body.callbackUrl,
    tenantId:req.body.tenantId,
    deploymentId:req.body.deploymentId
  }
  var shopifyObj = new shopifyAPI({
                shop: req.body.shopName, // MYSHOP.myshopify.com
                shopify_api_key: req.body.apiKey, // Your API key
                shopify_shared_secret: req.body.sharedSecret, // Your Shared Secret
                shopify_scope: 'write_products,read_products,write_orders,read_orders,read_fulfillments, write_fulfillments',
                redirect_uri: req.body.callbackUrl,
                nonce: 'tanvee' // a randomly selected value unique for each authorization request
            });
  var auth_url = {url:shopifyObj.buildAuthURL()};
  console.log(auth_url)
  request(auth_url.url, function (error, response, body) {
    if(error){
      request(auth_url.url, function (error, response, body) {
        console.log(error)
        console.log(body)
        if(response.statusCode == 200){
          return res.send(auth_url);
        }
        else if(response.statusCode == 400){
          return handleError(res, error);
        }
      })
    }
    else if(response.statusCode == 200){
      return res.send(auth_url);
    }
    else if(response.statusCode == 400){
      return handleError(res, error);
    }
  })
  
};


exports.getToken = function (req, res) {
  console.log(req.query)
  console.log(shopifyObject)
  var shopifyObj = new shopifyAPI({
                shop: shopifyObject.shopName, // MYSHOP.myshopify.com
                shopify_api_key: shopifyObject.apiKey, // Your API key
                shopify_shared_secret: shopifyObject.sharedSecret, // Your Shared Secret
                shopify_scope: 'write_products,read_products,read_fulfillments, write_fulfillments,read_orders, write_orders',
                redirect_uri: shopifyObject.callbackUrl,
                nonce: 'tanvee' // a randomly selected value unique for each authorization request
            });
  var query_params = req.query;
  console.log(req.query)
  shopifyObj.exchange_temporary_token(query_params, function(err, data){
    console.log(data);
    console.log(err);
    if(err){
      return res.send(err)
    }
    else if(data != {}){
      var createShopify = {
      shopName : shopifyObject.shopName,
      apiKey : shopifyObject.apiKey,
      sharedSecret : shopifyObject.sharedSecret,
      accessToken : data.access_token,
      tenantId : shopifyObject.tenantId,
      deploymentId : shopifyObject.deploymentId
      }
      Shopify.create(createShopify, function (err, result) {
          if (err) {
              return handleError(res, err);
          }
          var webhook_data = {
            "webhook": {
              "topic": "orders\/create",
              "address": "http:\/\/test.posist.org\/api\/shopify\/getOrders",
              "format": "json"
            }
          }
          shopifyObj.post('/admin/webhooks.json', webhook_data, function(err, data, headers){
            if(err){
              console.log("err",err);
              shopifyObj.post('/admin/webhooks.json', webhook_data, function(err, data, headers){
                if(err){
                  console.log("err",err);
                  res.redirect('/admin/deployments/deploymentDetails/'+shopifyObject.deploymentId);
                }
                else{
                  console.log("data",data);
                  res.redirect('/admin/deployments/deploymentDetails/'+shopifyObject.deploymentId);
                }
              });
            }
            else{
              console.log("data",data);
              res.redirect('/admin/deployments/deploymentDetails/'+shopifyObject.deploymentId);
            }
          });
          
          //return res.json(201, result);
      });
    }
    else{
      res.redirect('/admin/deployments');
    }
  });
};

//exports.createWebhook = function(req,res){}

exports.addProduct = function(req, res){
  console.log(req.body)
  var shopifyObj = new shopifyAPI({
                shop: req.body.shopDetails.shopName, // MYSHOP.myshopify.com
                shopify_api_key: req.body.shopDetails.apiKey, // Your API key
                shopify_shared_secret: req.body.shopDetails.sharedSecret, // Your Shared Secret
                access_token: req.body.shopDetails.accessToken, //permanent token
            });
  var post_data = {
    "product": {
      "title": req.body.item.name,
      "body_html": req.body.item.description,
      "vendor": "Posist",
      "product_type": req.body.item.category.categoryName,
      "variants": [{
        "type": "Default",
        "price": req.body.item.rate
      }]
    }
  }
  shopifyObj.post('/admin/products.json', post_data, function(err, data, headers){
    if(err){
      console.log(err);
      shopifyObj.post('/admin/products.json', post_data, function(err, data, headers){
        if(err){
          console.log(err);
          return handleError(res, err);
        }
        else{
          console.log(data);
          res.send(data)
        }
      });
    }
    else{
      console.log(data);
      res.send(data)
    }
  });
}

exports.getProducts = function(req,res){
  var shopifyObj = new shopifyAPI({
                shop: req.body.shopDetails.shopName, // MYSHOP.myshopify.com
                shopify_api_key: req.body.shopDetails.apiKey, // Your API key
                shopify_shared_secret: req.body.shopDetails.sharedSecret, // Your Shared Secret
                access_token: req.body.shopDetails.accessToken, //permanent token
            });
  shopifyObj.get('/admin/products.json', function(err, data, headers){
    if(err){
      console.log(err);
      shopifyObj.get('/admin/products.json', function(err, data, headers){
        if(err){
          console.log(err);
          return handleError(res, err);
        }
        else{
          console.log(data);
          res.send(data)
        }
      });
    }
    else{
      console.log(data);
      res.send(data)
    }
  });
}

exports.getOrders = function(req,res){
  var shopifyObj = req.body;
  var shopDomain=req.headers['x-shopify-shop-domain'];
  var shopName=shopDomain.split(".");
  var posObj = {}
  Shopify.find({shopName:shopName[0]},function(err, shopDetails){
    
    if (err) {
           console.log("Item not in POS")
		   return handleError(res, err);
        } 
    else if(shopDetails.length>0){   
      var result = shopDetails[0]
      console.log(shopDetails)
      posObj = {
        tabId: result.deploymentId,
        tabType: 'delivery',
        customer: {
          deployment_id: result.deploymentId,
          tenant_id: result.tenantId,
          mobile:req.body.shipping_address.phone,
          firstname:req.body.customer.first_name,
          email:req.body.customer.email,
          address1:req.body.shipping_address.address1,
          address2:req.body.shipping_address.address2,
          city:req.body.shipping_address.city,
          state:req.body.shipping_address.province,
          postCode:req.body.shipping_address.zip,
          DOB:''},//mobile,addType,firstname,address1,address2,city,state,DOB
        items: [],  //{_id,code,name,quantity}
        instance_id: 123,
        deployment_id: result.deploymentId,
        tenant_id: result.tenantId,
        payments: {amount:req.body.total_price,type:'COD'},//amount,type
        source:{id:1,name:'Shopify'}
      };
      async.forEach(req.body.line_items, function (item1, callback1) {
        var shopifyItem = item1;
        Item.find({shopifyId:shopifyItem.product_id}, function (err, item) {
            if (err) {
               console.log("Item not in POS")
			   return handleError(res, err);
            }
            else if(item.length>0){
              console.log(shopifyItem)
             // var temp = item[0];
              var tempItem={};
              tempItem.id=item[0]._id;
              tempItem.rate=item[0].rate;
              tempItem.name=item[0].name;

              tempItem.quantity=shopifyItem.quantity;
              posObj.items.push(tempItem);
              
            }
            callback1();
        });
      },function(error){
        console.log(posObj)
        if(posObj.items.length>0){
        var requestObj = {
          method: 'POST',
          url: 'http://test.posist.org/api/onlineOrders',
          json: posObj
        }
        request(requestObj, function (error, response, body) {
          if(error)
            return handleError(res, err);
          else{
			console.log(body)
            res.status(200).send({message:"ok"});  
		  }
        })}
      });
    } 
    else{
      return handleError(res, err);
    }
  })
}

exports.deleteProduct = function(req,res){
  var shopifyObj = new shopifyAPI({
                shop: req.body.shopDetails.shopName, // MYSHOP.myshopify.com
                shopify_api_key: req.body.shopDetails.apiKey, // Your API key
                shopify_shared_secret: req.body.shopDetails.sharedSecret, // Your Shared Secret
                access_token: req.body.shopDetails.accessToken, //permanent token
            });
  shopifyObj.delete('/admin/products/'+req.body.shopifyId+'.json', function(err, data, headers){
    if(err){
      console.log(err);
      shopifyObj.delete('/admin/products/'+req.body.shopifyId+'.json', function(err, data, headers){
        if(err){
          console.log(err);
          return handleError(res, err);
        }
        else{
          console.log(data);
          res.send(data)
        }
      });
    }
    else{
      console.log(data);
      res.send(data)
    }
  });
}

exports.updateProduct = function(req,res){
  console.log(req.body)
  var shopifyObj = new shopifyAPI({
                shop: req.body.shopDetails.shopName, // MYSHOP.myshopify.com
                shopify_api_key: req.body.shopDetails.apiKey, // Your API key
                shopify_shared_secret: req.body.shopDetails.sharedSecret, // Your Shared Secret
                access_token: req.body.shopDetails.accessToken, //permanent token
            });
  
  shopifyObj.get('/admin/products/'+req.body.item.shopifyId+'.json',function(err,data,headers){
    if(err){
      console.log(err)
      shopifyObj.get('/admin/products/'+req.body.item.shopifyId+'.json',function(err,data,headers){
        if(err){
          console.log(err)
          return handleError(res, err);
        }
        else{
          //var temp = JSON.parse(data.product)
          console.log(data)
          var put_data = {
            "product": {
              "title": req.body.item.name,
              "body_html": req.body.item.description,
              "vendor": "Posist",
              "product_type": req.body.item.category.categoryName,
              "variants": [{
                "id":data.product.variants[0].id,
                "type": "Default",
                "price": req.body.item.rate
              }]
            }
          }
          console.log(data)
          shopifyObj.put('/admin/products/'+req.body.item.shopifyId+'.json', put_data, function(err, data, headers){
            console.log(data);
            if(err){
              shopifyObj.put('/admin/products/'+req.body.item.shopifyId+'.json', put_data, function(err, data, headers){
                
                if(err){
                  return handleError(res, err);
                }
                else{
                  console.log(data);
                  return res.send(data) 
                }
              });
            }
            else{
              console.log(data);
              return res.send(data) 
            }
          });
        }
      });
    }
    else{
      var put_data = {
        "product": {
          "title": req.body.item.name,
          "body_html": req.body.item.description,
          "vendor": "Posist",
          "product_type": req.body.item.category.categoryName,
          "variants": [{
            "id":data.product.variants[0].id,
            "type": "Default",
            "price": req.body.item.rate
          }]
        }
      }
      shopifyObj.put('/admin/products/'+req.body.item.shopifyId+'.json', put_data, function(err, data, headers){
        console.log(data);
        if(err){
          shopifyObj.put('/admin/products/'+req.body.item.shopifyId+'.json', put_data, function(err, data, headers){
            
            if(err){
              return handleError(res, err);
            }
            else{
              console.log(data);
              return res.send(data) 
            }
          });
        }
        else{
          console.log(data);
          return res.send(data) 
        }
      });
    }

  })
}

exports.addSelectedProducts = function(req, res){
  console.log(req.body);
  var productToAdd = [];
  var shopifyObj = new shopifyAPI({
                shop: req.body.shopDetails.shopName, // MYSHOP.myshopify.com
                shopify_api_key: req.body.shopDetails.apiKey, // Your API key
                shopify_shared_secret: req.body.shopDetails.sharedSecret, // Your Shared Secret
                access_token: req.body.shopDetails.accessToken, //permanent token
            });
  shopifyObj.get('/admin/products.json', function(err, data, headers){
    if(err){
      shopifyObj.get('/admin/products.json', function(err, data, headers){
        if(data){
          for(var i=0;i<data.products.length;i++){

          }
        }
        console.log(data); // Data contains product json information
        //console.log(headers); // Headers returned from request
      });
    }
    console.log(data); // Data contains product json information
    //console.log(headers); // Headers returned from request
  });
}

// Updates an existing role in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Shopify.findById(req.params.id, function (err, role) {
    if (err) {
      return handleError(res, err);
    }
    if (!role) {
      return res.send(404);
    }
    var updated = _.extend(role, req.body);
    updated.markModified("permissions");
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, role);
    });
  });
};

// Deletes a role from the DB.
exports.destroy = function (req, res) {
  Shopify.findById(req.params.id, function (err, role) {
    if (err) {
      return handleError(res, err);
    }
    if (!role) {
      return res.send(404);
    }
    role.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
