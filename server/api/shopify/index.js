'use strict';

var express = require('express');
var controller = require('./shopify.controller');

var router = express.Router();

router.get('/getToken', controller.getToken);
router.post('/addProduct', controller.addProduct);
router.post('/getProducts', controller.getProducts);
router.post('/getOrders', controller.getOrders);
router.post('/deleteProduct', controller.deleteProduct);
router.post('/updateProduct', controller.updateProduct);
router.get('/', controller.index);
//router.post('/addSelectedProducts', controller.addSelectedProducts);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;