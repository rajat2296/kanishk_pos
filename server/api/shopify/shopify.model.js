'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ShopifySchema = new Schema({
    shopName: String,
    apiKey: String,
    sharedSecret: String,
    accessToken: String,
    tenantId: String,
    deploymentId: String,
    active: { type: Boolean, default: false }
});

module.exports = mongoose.model('Shopify', ShopifySchema);