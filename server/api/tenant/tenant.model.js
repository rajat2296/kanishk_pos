'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TenantSchema = new Schema({
    name: String,
    subdomain: { type: String, lowercase: true, trim: true },
    active: { type: Boolean, default: true },
    email: { type: String, lowercase: true },
    created: { type: Date, default: Date.now }
});

/**
 * Validations
 */

TenantSchema
    .path('subdomain')
    .validate(function (value, respond) {
        var self = this;
        this.constructor.findOne({subdomain: value}, function (err, tenant) {
            if (err) throw err;
            if (tenant) {
                respond(false);
            }
            respond(true);
        });
    }, 'Subdomain not available, please select another one.');


module.exports = mongoose.model('Tenant', TenantSchema);