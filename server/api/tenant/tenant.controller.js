'use strict';

var _ = require('lodash');
var Tenant = require('./tenant.model');
var User = require('../user/user.model');

//Get Server Time
exports.getServerTime=function(reg,res){
 return res.json({serverTime:new Date()});
};
// Get list of tenants
/*exports.index = function (req, res) {
    Tenant.find(function (err, tenants) {
        console.log(tenants);
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, tenants);
    });
};*/

// Get a single tenant
exports.show = function (req, res) {
    Tenant.findById(req.params.id, function (err, tenant) {
        if (err) {
            return handleError(res, err);
        }
        if (!tenant) {
            return res.send(404);
        }
        return res.json(tenant);
    });
};
exports.search = function (req, res) {
  var alphaN=req.query.q.match('^[a-zA-Z0-9_]*$');
 //console.log(alphaN);
  if(alphaN==null){return  res.json(404,[]);}
  var re = new RegExp(req.query.q, 'i');
  //console.log(re);

  Tenant.find({subdomain:re}).exec(function(err, tenants) {
    if (err) {
    return    handleError(req, err);
    }
   // console.log(tenants);
   return res.json(200, tenants);
  });
};

// Creates a new tenant in the DB.
exports.create = function (req, res) {
    console.log(req.body);
    User.find({email: req.body.email}, function (err, user) {

        if (user.length > 0) {
            return handleError(res, {
                errors: {
                   email: {
                       message:"Please select another email address.",
                       name:"ValidatorError",
                       path:"email",
                       value:req.body.email
                   }
                },
                message: "Validation failed",
                name: "ValidationError"
            });
        } else {
            Tenant.create(req.body, function (err, tenant) {
                if (err) {
                    return handleError(res, err);
                }
                return res.json(201, tenant);
            });
        }
    });



};

// Updates an existing tenant in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Tenant.findById(req.params.id, function (err, tenant) {
        if (err) {
            return handleError(res, err);
        }
        if (!tenant) {
            return res.send(404);
        }
        var updated = _.merge(tenant, req.body);
        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, tenant);
        });
    });
};

// Deletes a tenant from the DB.
exports.destroy = function (req, res) {
    Tenant.findById(req.params.id, function (err, tenant) {
        if (err) {
            return handleError(res, err);
        }
        if (!tenant) {
            return res.send(404);
        }
        tenant.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    return res.send(500, err);
}