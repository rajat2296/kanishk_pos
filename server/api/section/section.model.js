'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SectionSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  deployment_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  tenant_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  active: Boolean
});

SectionSchema
  .path('name')
  .validate(function (name) {
    return name.length;
  }, 'Name cannot be blank');

SectionSchema
  .path('name')
  .validate(function (value, respond) {
    var self = this;
    this.constructor.findOne({name: value, deployment_id: this.deployment_id}, function (err, section) {
      if (err) throw err;
      if (section) {
        if (self.id === section.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
  }, 'The specified section is already in present.');

module.exports = mongoose.model('Section', SectionSchema);
