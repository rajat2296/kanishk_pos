'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PosToDLFSchema = new Schema({
	brandName:String,
	leaseNumber:String,
	dlfTenantId:String,
	deployment_id: Schema.Types.ObjectId,
    tenant_id: Schema.Types.ObjectId,
    ftpSettings:{},
    autoSync: Boolean
},{versionKey:false});

module.exports = mongoose.model('PosToDLF', PosToDLFSchema);