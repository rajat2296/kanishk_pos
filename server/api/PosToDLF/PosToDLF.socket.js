/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var PosToDLF = require('./PosToDLF.model');

exports.register = function(socket) {
  PosToDLF.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  PosToDLF.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('PosToDLF:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('PosToDLF:remove', doc);
}