'use strict';

var _ = require('lodash');
var PosToDLF = require('./PosToDLF.model');

// Get list of PosToDLFs
exports.index = function(req, res) {
  console.log("test",req.params,req.query);
  PosToDLF.find({deployment_id:req.query.deployment_id,tenant_id:req.query.tenant_id},function (err, PosToDLFs) {
    if(err) { return handleError(res, err); }
    return res.json(200, PosToDLFs);
  });
};

// Get a single PosToDLF
exports.show = function(req, res) {
  PosToDLF.findById(req.params.id, function (err, PosToDLF) {
    if(err) { return handleError(res, err); }
    if(!PosToDLF) { return res.send(404); }
    return res.json(PosToDLF);
  });
};

// Creates a new PosToDLF in the DB.
exports.create = function(req, res) {
  console.log(req.body);
  PosToDLF.create(req.body, function(err, PosToDLF) {
    if(err) { return handleError(res, err); }
    return res.json(201, PosToDLF);
  });
   
};

// Updates an existing PosToDLF in the DB.
exports.update = function(req, res) {
  console.log(req.body);

  PosToDLF.findOneAndUpdate({deployment_id: req.body.deployment_id, "_id": req.body._id}, req.body,
       {new:true}, function (err, doc) {
      console.log("data",doc);
      if(doc)
        res.status(200).send(doc);
      else
        return handleError({status:'error'});
    });
};

// Deletes a PosToDLF from the DB.
exports.destroy = function(req, res) {
  PosToDLF.findById(req.params.id, function (err, PosToDLF) {
    if(err) { return handleError(res, err); }
    if(!PosToDLF) { return res.send(404); }
    PosToDLF.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.updateFtpSettings=function(req,res){
  PosToDLF.update({deployment_id:req.body.deployment_id,_id:req.body._id},{$set:{ftpSettings:req.body.ftpSettings}},function(err,num){
   if(num)
    res.status(200).send({status:'success'});
   else
    return handleError(res,{status:'error'});
  });
}

function handleError(res, err) {
  return res.send(500, err);
}