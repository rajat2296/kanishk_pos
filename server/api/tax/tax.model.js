'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TaxSchema = new Schema({
    name: String,
    details: String,
    type: String,
    value: Number,
    cascadingTaxes: [],
    tenant_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    created: {type: Date, default: Date.now},
    updated: {type: Date, default: Date.now},
    tabs: []
});
TaxSchema.set('versionKey', false);
TaxSchema
    .path('name')
    .validate(function (value, respond) {
        var self = this;

        if (this.isNew) {

            this.constructor.findOne({name:value, deployment_id:this.deployment_id}, function (err, sc) {
                if (err) throw err;

                if (sc) {
                    if (this.name === sc.name) return respond(false);
                    return respond(false);
                }
                respond(true);
            });

        } else {

            respond(true);
        }

    }, 'Duplicate Tax Name, please use another name.');

module.exports = mongoose.model('Tax', TaxSchema);