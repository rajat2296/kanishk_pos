'use strict';

var _ = require('lodash');
var Tax = require('./tax.model');
var Item = require('../item/item.model');
var async = require('async');

// Get list of taxs
exports.index = function (req, res) {
    Tax.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}, function (err, taxs) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, taxs);
    });
};

// Get a single tax
exports.show = function (req, res) {
    Tax.findById(req.params.id, function (err, tax) {
        if (err) {
            return handleError(res, err);
        }
        if (!tax) {
            return res.send(404);
        }
        return res.json(tax);
    });
};

// Creates a new tax in the DB.
exports.create = function (req, res) {
    Tax.create(req.body, function (err, tax) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, tax);
    });
};

// Updates an existing tax in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    var items = _.cloneDeep(req.body.items);
    delete req.body.items;
    req.body.updated = Date.now();
    
    Tax.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function(err,tax){
        if(err) return handleError(res,err);

        if(!tax) return res.send(404);

        else{
            delete req.body.updated;
            delete req.body.created;
            delete req.body.tenant_id;
            delete req.body.deployment_id;
            delete req.body.cascadingTaxes;
            delete req.body.tabs;
            req.body._id = tax._id.toString();

            Tax.update({'cascadingTaxes._id': req.body._id}, {
                $set: {
                    'cascadingTaxes.$.name': req.body.name,
                    'cascadingTaxes.$.value': req.body.value,
                    'cascadingTaxes.$.type': req.body.type,
                    'updated': Date.now()
                }
            }, {multi: true}, function(err,tc){
                if(err) return handleError(res,err);
                else{
                    async.each(items, function (item, callback) {
                        Item.update({_id : item._id}, {$set: {tabs: item.tabs, lastModified: Date.now(), updated: Date.now()}}, function(err){
                            if(err) callback(err);
                            else callback()
                        });
                    }, function(err){
                        if(err) return handleError(res,err);
                        else return res.json(200, tax);
                    });
                }
            });
        }
    });
};

exports.cascade = function(req,res){
    async.each(req.body.taxes, function(tax,cb){
        tax.updated =Date.now();
        Tax.findByIdAndUpdate(tax._id, {$set: tax}, function(err){
            if(err) cb(err);
            else cb();
        });
    }, function(err){
        if(err)
            return handleError(res,err);
        else{
            async.each(req.body.items, function (item, callback) {
                Item.update({_id : item._id}, {$set: {tabs: item.tabs, lastModified: Date.now(), updated: Date.now()}}, function(err){
                    if(err) callback(err);
                    else callback()
                });
            }, function(err){
                if(err) return handleError(res,err);
                else return res.json(200);
            });
        }
    });  
}

// Deletes a tax from the DB.
exports.destroy = function (req, res) {
    Tax.findByIdAndRemove(req.params.id, function (err, tax) {
        if (err) {
            return handleError(res, err);
        }
        if (!tax) {
            return res.send(404);
        } else{
            Tax.update({'cascadingTaxes._id':req.params.id}, {$pull: {cascadingTaxes: {_id: req.params.id }}}, {multi: true}, function(err,tc){
                if(err) return handleError(res,err);
                else{
                    console.log(tc);
                    return res.send(204);
                }
            });
        }
    });
};

function handleError(res, err) {
    return res.send(500, err);
}