'use strict';

var _ = require('lodash');
var Item = require('./item.model');
var DeleteItem = require('./deleteItem.model');
var ObjectId = require('mongoose').Types.ObjectId;
var async = require('async');
var Q = require('q');
var request = require('request');
var config = require('../../config/environment');
var utils=require('../Utils/utils');
var StockTransaction = require('../stockTransaction/stockTransaction.model');
var Recipe = require('../stockRecipe/stockRecipe.model');
var StockRequirement = require('../stockRequirement/stockRequirement.model');
var StockUnit = require('../stockUnit/stockUnit.model');
var email = require("emailjs/email");
var server  = email.server.connect({
  user:    "posist",
  password:"pos123ist",
  host:    "smtp.sendgrid.net",
  ssl:     true
});
// Get list of items
exports.index = function (req, res) {
    if(config.isQueue){

  request(config.reportServer.url+'/api/items?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

    var paramsquery;
    //console.log(req.query.lastSynced);
    if(req.query.tenant_id==null){return handleError(res, "error:'no items found'");}
    if(req.query.lastSynced==undefined) {
        paramsquery= {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
    } else{
        paramsquery={tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id,updated: { $gte: new Date(req.query.lastSynced),$lt:new Date(req.query.currentSyncTime)}}
    }
    //"category.isSemiProcessed":{$ne:true}
    Item.find(paramsquery, function (err, items) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, items);
    });
 }
};

exports.getItemByDepoyment = function (req, res) {
    if(config.isQueue){
  request(config.reportServer.url+'/api/items/getItemByDepoyment?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

    var paramsquery;
    Item.find({deployment_id:req.query.deployment_id},{name:1,rate:1,'category.categoryName':1,'category._id':1}, function (err, items) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, items);
    });
 }
}; 

exports.getItemsName = function (req, res) {
    if(config.isQueue){
  request(config.reportServer.url+'/api/items/getItemsName?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

    var paramsquery;
    //console.log(req.query.lastSynced);
    if(req.query.tenant_id==null){return handleError(res, "error:'no items found'");}
    if(req.query.lastSynced==undefined) {
        paramsquery= {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
    } else{
        paramsquery={tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id, updated: { $gte: new Date(req.query.lastSynced),$lt:new Date(req.query.currentSyncTime)}}
    }

    Item.find(paramsquery,{name:1}, function (err, items) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, items);
    });
 }
};

// Get list of processed items
exports.getProcessedItem = function (req, res) {
    Item.find(
          {
                tenant_id:new ObjectId(req.query.tenant_id),
                deployment_id: new ObjectId(req.query.deployment_id),
                'category.isSemiProcessed':false
          }, function (err, items) {
          if (err) {
              handleError(res, err);
          } else {
              var _items = [];
              _.forEach(items,function(itm,i){
                var item = {};
                item._id = itm._id;
                item.number = itm.number;
                item.category = _.merge(itm.category);
                item.stockQuantity = itm.stockQuantity;
                item.tabs = itm.tabs;
                    item.itemName=itm.name;
                    item.units=[];
                    if(itm.unit==undefined){
                        var obj={_id:'5604fbafeb29d0c80a722280',unitName:'pack',conversionFactor:1};
                        item.units.push(obj);
                    }
                    else{
                        item.units.push(itm.unit);
                    }
                _items.push(item);
              });
            return res.json(_items);
          }
      });
};

exports.getProcessedItemOptimized = function (req, res) {
  Item.find(
    {
      tenant_id:new ObjectId(req.query.tenant_id),
      deployment_id: new ObjectId(req.query.deployment_id),
      'category.isSemiProcessed':false
    }, {_id: 1, name: 1, category: 1, stockQuantity: 1, units: 1, unit: 1, tabs: 1, rate: 1, number: 1}, function (err, items) {
      if (err){
        handleError(res, err);
      } else {
        StockUnit.find({deployment_id: req.query.deployment_id}, function (err, units) {
          if(err)
            return handleError(res, err);
          var _items = [];
          _.forEach(items,function(itm,i){
            var item = {};
            item._id = itm._id;
            item.number = itm.number;
            item.category = _.merge(itm.category);
            item.stockQuantity = itm.stockQuantity;
            item.tabs = itm.tabs;
            item.itemName=itm.name;
            item.units= [];
            if(!itm.unit){
              var obj={_id:'5604fbafeb29d0c80a722280',unitName:'pack',conversionFactor:1};
              item.units.push(obj);
            }
            else{
              item.units = _.filter(units, function (unit) {
                return unit.baseUnit.id == itm.unit.baseUnit.id;
              });
            }
            _items.push(item);
          });
          return res.json(_items);
        });
      }
    });
};

// Get list of semi-processed items
exports.getSemiProcessedItem = function (req, res) {
      Item.find(
          {
                tenant_id:new ObjectId(req.query.tenant_id),
                deployment_id: new ObjectId(req.query.deployment_id),
                'category.isSemiProcessed':true
          }
      , function (err, items) {
          if (err) {
              handleError(res, err);
          } else {
            var _items = [];
            _.forEach(items,function(itm,i){
              var item = {};
              item._id = itm._id;
              item.number = itm.number;
              item.category = _.merge(itm.category);
              item.stockQuantity = itm.stockQuantity;
              item.tabs = itm.tabs;
              item.itemName=itm.name;
              item.units=[];
              if(itm.unit==undefined){
                var obj={_id:'5604fbafeb29d0c80a722280',unitName:'pack',conversionFactor:1};
                item.units.push(obj);
              }
              else{
                item.units.push(itm.unit);
              }
              _items.push(item);
            });
                return res.json(_items);
          }
      });
};

exports.getSemiProcessedItemOptimized = function (req, res) {
  Item.find(
    {
        tenant_id:new ObjectId(req.query.tenant_id),
        deployment_id: new ObjectId(req.query.deployment_id),
        'category.isSemiProcessed':true
    }, {_id: 1, name: 1, category: 1, number: 1, rate: 1, tabs: 1, units: 1, unit: 1, stockQuantity: 1}, function (err, items) {
    if (err) {
      handleError(res, err);
    } else {
      var _items = [];
      _.forEach(items,function(itm,i){
        var item = {};
        item._id = itm._id;
        item.number = itm.number;
        item.category = _.merge(itm.category);
        item.stockQuantity = itm.stockQuantity;
        item.tabs = itm.tabs;
        item.itemName=itm.name;
        item.units=[];
        if(itm.unit==undefined){
          var obj={_id:'5604fbafeb29d0c80a722280',unitName:'pack',conversionFactor:1};
          item.units.push(obj);
        }
        else{
          item.units.push(itm.unit);
        }
        _items.push(item);
      });
      return res.json(_items);
    }
  });
};

// Get a single item
exports.show = function (req, res) {
    Item.findById(req.params.id, function (err, item) {
        if (err) {
            return handleError(res, err);
        }
        if (!item) {
            return res.send(404);
        }
        return res.json(item);
    });
};

// Creates a new item in the DB.
exports.create = function (req, res) {
  var item = new Item(req.body);
  item.save(function(err){
    if (err) {
      console.log(err.errors);
      return handleError(res, err.errors);
    }
    return res.json(201, item);
  });
};

// Updates an existing item in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Item.findById(req.params.id, function (err, item) {
        if (err) {
            return handleError(res, err);
        }
        if (!item) {
            return res.send(404);
        }
        var updated = _.extend(item, req.body);
        updated.markModified("tabs");
        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, item);
        });
    });
};
//Update items with add ons
 exports.updateItemsToAddons=function(req,res){
    //console.log(req.body);
        if(req.body.items==undefined) {
            Item.findOneAndUpdate({_id: req.body.item_id}, {
                    $set: {
                        mapComboItems: req.body.comboItems,
                //      mapItems: req.body.items,
                        updated: new Date()
                    }
                }, {multi: false, new: true},
                function (err, result) {
                    if (err) {
                        return handleError(res, err);
                    }
                    return res.json(200, result);
                }
            );
        }else{
            Item.findOneAndUpdate({_id: req.body.item_id}, {
                    $set: {
                        mapItems: req.body.items,
                        updated: new Date()
                    }
                }, {multi: false, new: true},
                function (err, result) {
                //  console.log(result);
                    if (err) {
                        return handleError(res, err);
                    }
                    return res.json(200, result);
                }
            );
        }   
    }

exports.updateMapEmailOrder=function(req,res) {
//console.log(req.body);
    Item.findOne({_id: req.body.item_id}, {mapOnlineEmailOrder: 1}, function (err, item) {
        var tempMapName ={swiggy:'',foodpanda:'',zomato:'',limetray:''};
        if(_.has(item,'mapOnlineEmailOrder')) {
          tempMapName=item.mapOnlineEmailOrder;
        }
        if(req.body.type=='swiggy'){
            tempMapName.swiggy=req.body.itemName;
        }
        if(req.body.type=='zomato'){
            tempMapName.zomato=req.body.itemName;
        }
        if(req.body.type=='foodpanda'){
            tempMapName.foodpanda=req.body.itemName;
        }
        Item.update({_id: req.body.item_id}, {
                $set: {
                    mapOnlineEmailOrder: tempMapName
                    //,
                    //      mapItems: req.body.items,
                    //updated: new Date()
                }
            }, {multi: false},
            function (err, result) {
                //  console.log(result);
                if (err) {
                    return handleError(res, err);
                }
                return res.json(200, {status: 'updated'});
            }
        );
    })
}
// Deletes a item from the DB.
exports.destroy = function (req, res) {
    Item.findById(req.params.id, function (err, item) {
        if (err) {
            return handleError(res, err);
        }
        if (!item) {
            return res.send(404);
        }
        item.updated = new Date;
        item.lastModified = item.updated;
        item.isDeleted = true;
        DeleteItem.findOneAndUpdate({_id: item._id}, item, {upsert:true}, function(e, r){
          if(!e){
            item.remove(function (err) {
                if (err) {
                    return handleError(res, err);
                }
                //return res.send(204);
                return res.json(200, item);
            });
          }else
            return handleError(res, err);
        });
    });
};

// Delete mutiple items from DB
exports.deleteMultipleItems = function(req,res){
    var items = req.body.items;
    Item.find({_id: {$in: items}}, function(err, it){
      if(!err){
        async.each(it, function(i, cb){
          var newi = new DeleteItem(i);
          newi.updated = new Date;
          newi.lastModified = newi.updated;
          newi.isDeleted = true;
          newi.save(function(err){
            if(err)
              return handleError(res,err);
            else{
              Item.remove({_id: newi._id}, function(err,c){
                if(err)
                  return handleError(res,err);
                else 
                  cb();
              });
            }
          });
        }, function(e){
          return res.json(200,items);
        });
      }else
        return handleError(res,err);
    });
};

exports.setTabRate = function(req,res){
    async.each(req.body.items, function(item, callback){
        if(item.tabId != 'default'){
            Item.update({_id: item.itemId, 'tabs._id': item.tabId},{
                $set:{
                    'tabs.$.item.rate': item.rate,
                    lastModified: Date.now(),
                    updated : Date.now(),
                    updateHistory: item.updateHistory
                }
            }, function(err){
                if(err) callback(err);
                else callback();
            });
        }
        else{
            Item.findOne({_id:item.itemId}, function(err, _item){
                if(err) callback(err);
                else{
                    async.each(_item.tabs, function(tab, cb){
                        tab.item.rate = item.rate;
                        cb();
                    }, function(err){
                        _item.rate = item.rate;
                        _item.updateHistory = item.updateHistory;
                        _item.lastModified = Date.now();
                        _item.markModified("tabs");
                        _item.save(function(err){
                            if(err) callback(err);
                            else callback();
                        });
                    });
                }
            });
        }
    }, function(err){
        if(err) return handleError(res,err)
        else return res.json(200);
    });
};
exports.mapTaxes = function(req,res){
    async.each(req.body.items, function(item,callback){
        Item.update({_id: item._id},{
            $set:{
                tabs: item.tabs,
                lastModified: Date.now(),
                updated : Date.now(),
                updateHistory: item.updateHistory
            }
        }, function(err){
            if(err) callback(err);
            else callback();
        });
    }, function(err){
        if(err) return handleError(res,err)
        else return res.json(200);
    });
};

exports.createMultipleItems = function(req,res){
  Item.create(req.body.items, function(err, items){
    if(arguments[0]) return handleError(res,err)
    else{
      delete arguments[0];
      return res.json(200, {items : arguments});
    }
  });
}

exports.itemDeleteRequestEmail = function (req, res) {

}
exports.sendMailOnDeletion = function (req, res) {
  console.log('req.query',req.query);
var client = {
    text: "A delete request has been generated by the deployment '"+req.query.deploymentName+"' for the item '"+req.query.itemName+"' (id:"+req.query.itemId+")",
    from: "Posist Info <no-reply@posist.info>",
    to:req.query.client,
    subject: "Item Delete Request Generated",
  }
  var sender = {
    text: "You have generated a delete request for Item "+req.query.itemName+" for the deployment "+req.query.deploymentName,
    from: "Posist Info <no-reply@posist.info>",
    to:req.query.company,
    subject: "Item Delete Request from Base Kitchen",
  }

var allPromise=Q.all([
    sendMailToUser(client),
    sendMailToUser(sender)
  ]);
  allPromise.then(function (data) {
    console.log('data',data);
  res.json(200,data);
  }).catch(function (err) {
    console.log('Error in sendMailOnDeletion catch',err);
    return handleError(res, err);
  });
};

function sendMailToUser(data)
  {
    var d=Q.defer();
   server.send(data, function (error, message) {
            if(!error) {
              var obj={message:'Mailed Successfully to '+data.to}
              d.resolve(obj);
            } else
            {
              d.reject(error)
            }
  });
   return d.promise;
  }

  exports.validateItemUnitChange = function (req, res) {

  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var itemId = req.query.itemId;
  var baseUnitId = req.query.baseUnitId;
  var recipeId = null;
  Recipe.findOne({itemId: itemId}, function (err, recipe){
    if(err){
      return handleError(res, err);
    }
    else {
      if(recipe)
        console.log('recipe found');
      else
        console.log('recipe not found');

      StockTransaction.findOne({tenant_id: tenant_id, deployment_id: {$ne: deployment_id}, $and: [{$or: [{transactionType: "2"}, {transactionType: "3"}, {transactionType: "5"}, {transactionType: "7"}, {transactionType: "9"}, {transactionType: "11"}, {transactionType: "12"}, {transactionType: "15"}]},
        {$or: [{"_store.receiver.category.items.receipeDetails.itemId": itemId}, {"_store.category.items.receipeDetails.itemId": itemId}, {"_store.vendor.category.items._id": itemId}, {"_store.processedFoodCategory.items.receipeDetails.itemId": itemId}]}]
      }, function(err, transaction){
        if(err)
          return handleError(res, err);
        if(!transaction){
          console.log('transaction not found');

          StockRequirement.findOne({toDeployment_id: deployment_id, "store.vendor.category.items._id": itemId}, function(err, requirement) {
            if(err)
              return handleError(res, err);
            if(!requirement) {
              console.log('requirement not found');
              Recipe.findOne({tenant_id: tenant_id, "receipeDetails.itemId": itemId}, function (err, oRecipe) {
                if(err)
                  return handleError(res, err);
                if(!oRecipe){
                  console.log('orecipe not found');
                  if(recipe){
                    console.log(recipe.unit.baseUnit);
                    console.log('recipe found', recipe.unit.baseUnit.unitName, recipe.unit.baseUnit.id, baseUnitId)
                    if(recipe.unit.baseUnit.id == baseUnitId)
                        return res.json(200, {unitChangable: true, reason: "Converting to same group", showMessage: false});
                    else
                      return res.json(200, {unitChangable: false, reason: "Please delete recipe first.", showMessage: true});
                  }
                  else
                    return res.json(200, {unitChangable: true, reason: 'No Recipe found', showMessage: false});
                }
                else{
                  console.log('orecipe found');
                  return res.json(200, {unitChangable: false, reason: "Item is being used in outlet recipes.Please delete it from there and then continue", showMessage: true})
                }
              });
            }
            else{
              console.log('requirement found');
              return res.json(200, {unitChangable: false, reason: "Item has been used in transactions. Cannot change unit now!", showMessage: true});
            }
          });
        }
        else{
          console.log('transaction found');
          return res.json(200, {unitChangable: false, reason: "Item has been used in transactions. Cannot change unit now!", showMessage: true});
        }
      });
    }
  });
};

exports.validateItemForDeletion = function (req, res) {
  console.log('Inside ')
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var itemId = req.query.itemId;

  StockTransaction.findOne({tenant_id: tenant_id, deployment_id: {$ne: deployment_id}, $and: [{$or: [{transactionType: "2"}, {transactionType: "3"}, {transactionType: "5"}, {transactionType: "7"}, {transactionType: "9"}, {transactionType: "11"}, {transactionType: "12"}, {transactionType: "15"}]},
        , {$or: [{"_store.receiver.category.items.recipeDetails.itemId": itemId}, {"_store.category.items.recipeDetails.itemId": itemId}, {"_store.vendor.category.items._id": itemId}, {"_store.processedFoodCategory.items.recipeDetails.itemId": itemId}]}]
      }, function(err, transaction){
        console.log('In transaction');
        if(err)
          {
            console.log('error in transaction',err);
            return handleError(res, err);
        }
        else if(!transaction){
            StockRequirement.findOne({toDeployment_id: deployment_id, "store.vendor.category.items._id": itemId}, function(err, requirement) {
              console.log('In requirement');
              if(err)
              {
                console.log('error in requirement',err);
                return handleError(res, err);
              }
              else if(!requirement) {
                //return res.json(200, {itemDeletable: true, reason: "Item has not been used in Indent and Stock Transactions. Cannot delete unit now!", showMessage: false});
                  Recipe.findOne({tenant_id: tenant_id, "receipeDetails.itemId": itemId}, function (err, oRecipe) {
                    if(err)
                      return handleError(res, err);
                    if(!oRecipe){
                      return res.json(200, {itemDeletable: true,openModal:false, reason: "Please delete recipe first.", showMessage: true});
                    }
                      return res.json(200, {itemDeletable: false,openModal:false, reason: "Item is being used in outlet recipes.Please delete it from there and then continue", showMessage: true})
                  });
              }
              else
              {
                return res.json(200, {itemDeletable: false,openModal:true, reason: "Item has been used in Indent Transaction.", showMessage: true});
              }
            });
        }
        else
        {
        return res.json(200, {itemDeletable: false,openModal:true, reason: "Item has been used in Stock Transactions.", showMessage: true});
        }
      });
};

exports.assignShortNameToItems=function(req,res){
   var list=req.body.list;
   console.log(list);
   async.each(list,function(name,next) {
      Item.update({
        deployment_id: req.body.deployment_id,
        _id: name._id
      }, {$set: {shortName: name.shortName}}, function (err, num) {
        if (err)
          next(err);
        else
          next();
      })
    },
    function(err){
      if(err)
       return handleError(res,{status:'error'});
      console.log("all done");
      res.status(200).send({status:'ok'});
    });
}

function handleError(res, err) {
    return res.send(500, err);
}
