'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  async = require('async');

var ItemSchema = new Schema({
  name: String,
  number: Number,
  description: String,
  rate: Number,
  category: {
    type: Schema.Types.Mixed,
    required: true
  },
  quantity: Number,
  stockQuantity: {type: Number, default: null},
  unit: {},
  units: [],
  created: {type: Date, default: Date.now},
  tenant_id: Schema.Types.ObjectId,
  deployment_id: Schema.Types.ObjectId,
  tabs: [],
  updated: {type: Date},
  shopifyId: String,
  urbanPiperId: String,
  mapItems: [],
  mapComboItems: [],
  mapOnlineEmailOrder: {},
  lastModified: {type: Date, default: Date.now},
  updateHistory: [],
  nomnomId: String,
  comments:[],
  shortName: String
});
ItemSchema.set('versionKey', false);
ItemSchema
  .path('name')
  .validate(function (value, respond) {
    var self = this;
    this.constructor.findOne({name:value, deployment_id:this.deployment_id}, function (err, i) {
      if (err) throw err;
      if (i) {
        if (self.name === i.name ){
          if(!self._id.equals( i._id))
            return respond(false);
          return respond(true);
        }
      }
      respond(true);
    });
  }, 'Duplicate Item Name, please use another name.');

ItemSchema
  .path('number')
  .validate(function (value, respond) {
    var self = this;
    this.constructor.findOne({number:value, deployment_id:this.deployment_id}, function (err, i) {
      if (err) throw err;
      if (i) {
        if (self.number === i.number){
          if(!self._id.equals( i._id))
            return respond(false);
          return respond(true);
        }
      }
      respond(true);
    });
  }, 'Duplicate Item Number, please use another number.');

ItemSchema
    .pre('save', function (next) {
        this.updated = new Date;
        var self = this;
        async.each(this.tabs, function(tab, callback){
            if(tab.item){
                tab.item._id = self._id.toString();     
            }
            else{
                tab.item = {
                    "_id": self._id.toString(),
                    "name": self.name,
                    "number": self.number,
                    "rate": self.rate,
                    "category": {
                        "categoryName": self.category.categoryName
                    }
                };
            }
            callback();
        }, 
        function(){
            next();
        });
    });

module.exports = mongoose.model('Item', ItemSchema);
