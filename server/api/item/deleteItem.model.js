'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DeleteItemSchema = new Schema({
    name: String,
    number: Number,
    description: String,
    rate: Number,
    category: {
        type: Schema.Types.Mixed,
        required: true
    },
    quantity: Number,
    stockQuantity: {type: Number, default: null},
    unit: {},
    units: [],
    created: {type: Date, default: Date.now},
    tenant_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    tabs: [],
    updated: {type: Date},
    shopifyId: String,
    urbanPiperId: String,
    mapItems: [],
    mapComboItems: [],
    mapOnlineEmailOrder: {},
    lastModified: {type: Date, default: Date.now},
    updateHistory: [],
    nomnomId: String,
    comments:[],
    shortName: String,
    isDeleted: {type:Boolean, default: true}
});

module.exports = mongoose.model('DeleteItem', DeleteItemSchema);
