'use strict';

var express = require('express');
var controller = require('./item.controller');

var router = express.Router();

router.get('/', controller.index);
//router.get('/getItemsName',controller.getItemsName);
router.get('/getItemsName',controller.getItemsName);
router.get('/getItemByDepoyment',controller.getItemByDepoyment);
router.get('/getProcessedItem', controller.getProcessedItem);
router.get('/getProcessedItemOptimized', controller.getProcessedItemOptimized);
router.get('/getSemiProcessedItem', controller.getSemiProcessedItem);
router.get('/getSemiProcessedItemOptimized', controller.getSemiProcessedItemOptimized);
router.get('/validateItemUnitChange', controller.validateItemUnitChange);
router.get('/validateItemForDeletion', controller.validateItemForDeletion);
router.get('/sendMailOnDeletion', controller.sendMailOnDeletion);
router.get('/:id', controller.show);
router.post('/updateItemsToAddons', controller.updateItemsToAddons);
router.post('/updateMapEmailOrder', controller.updateMapEmailOrder);
router.post('/itemDeleteRequestEmail', controller.itemDeleteRequestEmail);

router.post('/', controller.create);
router.post('/multi', controller.createMultipleItems);
router.put('/setTabRate', controller.setTabRate);
router.put('/mapTaxes', controller.mapTaxes);
router.put('/:id', controller.update);
router.put('/:id/deleteMulti', controller.deleteMultipleItems);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/assignShortName',controller.assignShortNameToItems);

module.exports = router;
