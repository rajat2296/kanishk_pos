/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Supercategory = require('./superCategory.model');

exports.register = function(socket) {
  Supercategory.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Supercategory.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('superCategory:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('superCategory:remove', doc);
}