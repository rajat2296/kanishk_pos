'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SupercategorySchema = new Schema({
    superCategoryName: String,
    superCategoryStation: {},
    tenant_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});

SupercategorySchema
    .path('superCategoryName')
    .validate(function (value, respond) {
        var self = this;

        if (this.isNew) {
            console.log(this.deployment_id);
            this.constructor.findOne({superCategoryName:value, deployment_id: this.deployment_id}, function (err, sc) {
                if (err) throw err;

                if (sc) {
                    if (this.superCategoryName === sc.superCategoryName) return respond(false);
                    return respond(false);
                }
                respond(true);
            });

        } else {

            respond(true);
        }

    }, 'Duplicate Supercategory Name, please use another name.');

module.exports = mongoose.model('Supercategory', SupercategorySchema);
