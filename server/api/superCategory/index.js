'use strict';

var express = require('express');
var controller = require('./superCategory.controller');

var router = express.Router();

router.get('/', controller.index);
//router.get('/:tenant_id/:deployment_id', controller.deploymentSupercategory);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/multi/checkdelete', controller.deleteAuthMulti);
router.put('/:id', controller.update);
router.delete('/:id/checkdelete', controller.DeleteAuth);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;