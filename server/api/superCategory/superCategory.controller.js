'use strict';

var _ = require('lodash');
var Supercategory = require('./superCategory.model');
var Category = require('../category/category.model');
var Item = require('../item/item.model');
var async = require('async');

// Get list of superCategorys by tenant_id
exports.index = function (req, res) {

    Supercategory.find(req.query, function (err, superCategorys) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, superCategorys);
    });
};

// Get list of superCategorys by tenant_id & deployment_id
exports.deploymentSupercategory = function (req, res) {
    Supercategory.find({'deployment_id': req.params.deployment_id, 'tenant_id': req.params.tenant_id}, function (err, superCategorys) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, superCategorys);
    });
};

// Get a single superCategory
exports.show = function (req, res) {
    Supercategory.findById(req.params.id, function (err, superCategory) {
        if (err) {
            return handleError(res, err);
        }
        if (!superCategory) {
            return res.send(404);
        }
        return res.json(superCategory);
    });
};

// Creates a new superCategory in the DB.
exports.create = function (req, res) {
    Supercategory.create(req.body, function (err, superCategory) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, superCategory);
    });

    /* Supercategory.collection.insert(req.body, null, function (err, scategories) {
     if (err) {
     return handleError(res,err);
     }
     return res.json(201, scategories);
     });*/
};

// Updates an existing superCategory in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    req.body.updated = Date.now();
    Supercategory.findByIdAndUpdate(req.params.id, req.body, {new:true}, function(err, sc){
        if(err) return handleError(res, err);
        else if(!sc) return res.send(404);
        else {
            var _sc = {
                superCategoryName : sc.superCategoryName,
                _id : sc.id,
                superCategoryStation : sc.superCategoryStation
            };

            Category.update({'superCategory._id':sc.id}, {superCategory:_sc,updated:Date.now()}, {multi:true}, function(err, categoriesUpdated){
                if(err) return handleError(res, err)

                if(categoriesUpdated == 0) return res.json(200,sc);
                else{
                    Item.update({'category.superCategory._id': sc.id}, {$set:{'category.superCategory':_sc, lastModified:Date.now(), updated: Date.now()}}, {multi:true}, function(err){
                        if(err) return handleError(res,err);
                        return res.json(200,sc);
                    });
                }     
            });
        }
    })
};

// Deletes a superCategory from the DB.
exports.destroy = function (req, res) {
    Supercategory.findById(req.params.id, function (err, superCategory) {
        if (err) {
            return handleError(res, err);
        }
        if (!superCategory) {
            return res.send(404);
        }
        superCategory.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

//Delete multiple SuperCategories
exports.deleteAuthMulti = function(req,res){
    var scategories = req.body.scategories;
    async.each(scategories, function(sc, cb){
        Category.find({superCategoryName: sc}).exec(function (err, cat) {
            if (err) {
                cb(err);
            }
            else {
                if (cat.length > 0) {
                    cb();
                }
                else {
                    Supercategory.remove({_id:sc}, function(err,sc){
                        if(err) cb(err)
                        else cb();
                    });
                }
            }
        });
    }, function(err){
        if(err) return handleError(res,err);
        else return res.json(200,{scategories: scategories});
    });
};

//Check any used id before delete
exports.DeleteAuth = function (req, res, next) {

    var id = req.params.id;
    var tenantid = req.query.tenant_id;
    var a = Category.find({tenant_id: tenantid}).where('superCategory._id').equals(id).select('_id categoryName').exec(function (err, cat) {
        if (err) {
            return handleError(res, err);
        }
        else {
            if (cat.length > 0) {
                var errorstatus = {status: "error", message: "Some of the super categories are used."};
                return res.status(200).send(errorstatus);
            }
            else {
                Supercategory.findById(req.params.id, function (err, superCategory) {
                    if (err) {
                        return handleError(res, err);
                    }
                    if (!superCategory) {
                        return res.send(404);
                    }
                    superCategory.remove(function (err) {
                        if (err) {
                            return handleError(res, err);
                        }
                        return res.send(204);
                    });
                });
            }
        }
    });
//  return res.send(204);


    // var id = req.superCategory._id;
    //  need to put logic here for used supercategory

    /* Supercategory.findById(req.params.id, function (err, superCategory) {
     if(err) { return handleError(res, err); }
     if(!superCategory) { return res.send(404); }
     superCategory.remove(function(err) {
     if(err) { return handleError(res, err); }
     return res.send(204);
     });
     });*/
};


function handleError(res, err) {
    return res.send(500, err);
}
