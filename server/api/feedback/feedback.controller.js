'use strict';

var _ = require('lodash');
var Feedback = require('./feedback.model');
var FeedbackBill = require('./feedbackbill.model');
var moment = require('moment');
var utils = require('../Utils/utils');
var Deployment = require('../deployment/deployment.model');
var ObjectId = require('mongoose').Types.ObjectId;
var request = require('request');
// Get list of feedbacks
exports.getBill = function(req, res) {
    //console.log( req.body);
    if (req.body.apiKey != "55fd6726474dde2422e5da2c") {
        res.json(401, { error: "authentication error." })
    }
    var currentDate = new Date();
    var resetDate = currentDate.setHours(0, 0, 0, 0);
    //console.log(resetDate);
    // if(!isDayBack){resetDate=moment(resetDate).add(1, 'days').toDate()}

    FeedbackBill.find({ deployment_id: req.body.deployment_id, serialNumber: req.body.billNumber }, function(err, feedbacks) {
        if (err) {
            return handleError(res, err); }
        if (feedbacks.length > 0) {
            return res.json(200, feedbacks);
        } else {
            FeedbackBill.find({
                deployment_id: req.body.deployment_id,
                daySerialNumber: req.body.billNumber,
                created: {
                    $gte: moment(resetDate).toDate(),
                    $lte: moment(resetDate).add(1, 'days').toDate()
                }
            }, function(err, feedbacks) {
                if (err) {
                    return handleError(res, err); }
                return res.json(200, feedbacks);
            });
        }
    });
};
exports.getBillById = function(req, res) {
    console.log(req.body);
    if (req.body.apiKey != "55fd6726474dde2422e5da2c") {
        res.json(401, { error: "authentication error." })
    }
    var currentDate = new Date();
    var resetDate = currentDate.setHours(0, 0, 0, 0);
    //console.log(resetDate);
    // if(!isDayBack){resetDate=moment(resetDate).add(1, 'days').toDate()}
    FeedbackBill.find({ billId: req.body.billId }, {}, function(err, feedbacks) {
        if (err) {
            return handleError(res, err); }
        if (feedbacks.length > 0) {
            return res.json(200, feedbacks);
        } else {
            return handleError(res, { error: 'Bill not found' });
        }
    });
};

exports.getBillByCode = function(req, res) {
    console.log(req.body);
    if (req.body.apiKey != "55fd6726474dde2422e5da2c") {
        res.json(401, { error: "authentication error." })
    }
    var currentDate = new Date();
    var resetDate = currentDate.setHours(0, 0, 0, 0);
    //console.log(resetDate);
    // if(!isDayBack){resetDate=moment(resetDate).add(1, 'days').toDate()}
    FeedbackBill.findOne({ 'code.name': req.body.code }, {}, function(err, feedback) {
        if (err) {
            return handleError(res, err); }
        if (feedback) {
            return res.json(200, feedback);
        } else {
            return handleError(res, { error: 'Bill not found' });
        }
    });
};

// Get a single feedback
exports.show = function(req, res) {
    Feedback.findById(req.params.id, function(err, feedback) {
        if (err) {
            return handleError(res, err); }
        if (!feedback) {
            return res.send(404); }
        return res.json(feedback);
    });
};

// Creates a new feedback bill in the DB.
exports.createBill = function(req, res) {

    FeedbackBill.findOne({ billId: req.body.billId }, function(err, feedback) {
        if (err) {
            return handleError(res, err); }
        if (!feedback) {
            Deployment.findOne({ _id: new ObjectId(req.body.deployment_id) }, { name: 1 }, function(err, deployment) {
                if (err) {
                    return handleError(res, err);
                }
                req.body.deploymentName = deployment.name;
                //console.log(req.body);
                FeedbackBill.create(req.body, function(err, feedback) {
                    if (err) {
                        return handleError(res, err); }
                    //console.log(feedback);
                    var mobile = "";
                    if (feedback.customer != undefined) {
                        if (_.has(feedback.customer, 'mobile')) {
                           // sendFeedbackSMS(deployment.name, feedback.code.name, feedback.customer.mobile, req.body.trackingUrl);
                            /*Smuggler Integration*/
                            var smugglrUrl='https://smugglr.co/api/users/'+feedback.customer.mobile+'/message';
                            request.post(smugglrUrl, {form: {deployment_id:req.body.deployment_id,api_key:'qAO0PrGY6xOctZVTIINPpQtt',code:feedback.code.name}}, function (error, response, body) {
                                    if (error) {
                                    }
                                    console.log(response.statusCode);
                                    if(response.statusCode!=200){
                                         sendFeedbackSMS(deployment.name, feedback.code.name, feedback.customer.mobile, req.body.trackingUrl);
                           
                                    }
                                    //sendFeedbackSMS(deployment.name, feedback.code.name, feedback.customer.mobile, req.body.trackingUrl);
                           
                                    /*Need To send sms once approved*/
                                    });
                                  }
                               }

                           
                    
                    return res.json(201, feedback);
                });
            });
        } else {
            var updated = _.extend(feedback, req.body);
            updated.save(function(err) {
                if (err) {
                    return handleError(res, err);
                }
                console.log(feedback);
                var mobile = "";
                if (_.has(feedback, 'customer')) {
                    if (_.has(feedback.customer, 'mobile')) {
                        Deployment.findOne({ _id: new ObjectId(req.body.deployment_id) }, { name: 1 }, function(err, deployment) {
                            if (err) {
                                return handleError(res, err);
                            }
                            console.log('deployment', deployment)
                           // sendFeedbackSMS(deployment.name, feedback.code.name, feedback.customer.mobile, req.body.trackingUrl);
                             var smugglrUrl='https://smugglr.co/api/users/'+feedback.customer.mobile+'/message';
                            request.post(smugglrUrl, {form: {deployment_id:req.body.deployment_id,api_key:'qAO0PrGY6xOctZVTIINPpQtt',code:feedback.code.name}}, function (error, response, body) {
                                    if (error) {
                                    }
                                    if(response.statusCode!=200){
                                         sendFeedbackSMS(deployment.name, feedback.code.name, feedback.customer.mobile, req.body.trackingUrl);
                           
                                    }
                                    /*Need To send sms once approved*/
                                    });
                        });
                    }
                }

                return res.json(200, feedback);
            });
        }
    });
};

function sendFeedbackSMS(restrurant_name, code, mobile, trackingUrl) {
    console.log(mobile);
    var body = "Thanks for eating at " + restrurant_name + ", hope you enjoyed your meal.\r\n";
    //body += "Talk to us on our Chat-Feedback System http://52.66.99.215/" + code + "\r\n";
    //body += "We value, read and take action on your feedback.";
    body += "Talk to us on Facebook Messenger, to join our exclusive club for cash backs, deals and special offers.\r\n";
    body += "https://smugglr.co/p/"+ code +'\r\nSee you again.';
    /*if (trackingUrl)
        body += 'You can track your order at' + trackingUrl //added by rajat*/
    utils.sendSMSExotel(mobile, body);
    // var url = "http://203.212.70.200/smpp/sendsms?username=posistapi&password=del12345&to=" + body + "&from=POSIST&text=" + mobile;

}
// Creates a new feedback in the DB.
exports.create = function(req, res) {
    Feedback.create(req.body, function(err, feedback) {
        if (err) {
            return handleError(res, err); }
        return res.json(201, feedback);
    });
};

// Updates an existing feedback in the DB.
exports.update = function(req, res) {
    if (req.body._id) { delete req.body._id; }
    Feedback.findById(req.params.id, function(err, feedback) {
        if (err) {
            return handleError(res, err); }
        if (!feedback) {
            return res.send(404); }
        var updated = _.merge(feedback, req.body);
        updated.save(function(err) {
            if (err) {
                return handleError(res, err); }
            return res.json(200, feedback);
        });
    });
};

// Deletes a feedback from the DB.
exports.destroy = function(req, res) {
    Feedback.findById(req.params.id, function(err, feedback) {
        if (err) {
            return handleError(res, err); }
        if (!feedback) {
            return res.send(404); }
        feedback.remove(function(err) {
            if (err) {
                return handleError(res, err); }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    return res.send(500, err);
}
