'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FeedbackBillSchema = new Schema({
  billId: String,
  billDate: {type:Date},
  serialNumber: Number,
  daySerialNumber:Number,
  splitBillNumber:Number,
  customer:{},
  created:{type: Date, default: Date.now},
  items:[],
  deployment_id:Schema.Types.ObjectId,
  code:{},
  deploymentName:String
});

module.exports = mongoose.model('FeedbackBill', FeedbackBillSchema);