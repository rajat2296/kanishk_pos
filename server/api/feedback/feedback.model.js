'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FeedbackSchema = new Schema({
  billDetail: {},
  customer: {},
  rating: {},
  created:{type: Date, default: Date.now},
  deployment_id:Schema.Types.ObjectId,
  visitAgain:String,
  source:{id:Number,name:String}
});

module.exports = mongoose.model('Feedback', FeedbackSchema);