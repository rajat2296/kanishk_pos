'use strict';

var express = require('express');
var controller = require('./feedback.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post('/getBillById',  controller.getBillById);
router.post('/getBillByCode',  controller.getBillByCode);
router.post('/', controller.getBill);
router.get('/:id', controller.show);
router.post('/createBill', controller.createBill);
router.post('/create', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
