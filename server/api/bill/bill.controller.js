'use strict';

var _ = require('lodash');
var config = require('../../config/environment');
var moment = require('moment');
var Bill = require('./bill.model');
var BillM = require('./billm.model');
var BillSM = require('./billsm.model');
var async = require('async');
var crypto = require('crypto');
var mongoose = require('mongoose');
var request = require('request');
var SmsTemplate = require('../smstemplate/smstemplate.model');
var Deployment = require('../deployment/deployment.model');
var SmsTransaction = require('../smstransaction/smstransaction.model');
var Q = require('q');
var utils = require('../Utils/utils');
var request = require('request');
var Client = require('node-rest-client').Client;
var client = new Client();
var Report = require('../report/report.controller');
var reportApi = require('../reportFunction/reportApi.js')
var kue = require('kue'),
    queue = kue.createQueue(config.redisConn);
var email   = require("emailjs/email");
var server  = email.server.connect({
  user:    "posist",
  password:"pos123ist",
  host:    "smtp.sendgrid.net",
  ssl:     true
});

/*var Queue=require('bull');
 var queue=Queue('test current connect',config.redisConn);
 */
function randomObjectId() {
  return crypto.createHash('md5').update(Math.random().toString()).digest('hex').substring(0, 24);
}
// Get list of bills
exports.index = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    // console.log(req.query);
    var param = {
      deployment_id: req.query.deployment_id, _created: {$gte: (req.query.fromDate), $lt: (req.query.toDate)},
      isDeleted: {$ne: true}
    };
    // Bill.find(req.query,function (err, bills) {
    Bill.find(param).exec(function (err, bills) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, bills);
    });
  }
};

exports.billsBackUpOld = function (req, res) {
  var bulkInsert = BillSM.collection.initializeUnorderedBulkOp();
  BillM.find({
      _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
      deployment_id: req.query.deployment_id,
      isDeleted: {$ne: true}
    },
    {}, {limit: 1000}, function (err, bills) {
      console.log(bills.length);
      bills.forEach(function (bill) {
        bulkInsert.insert(bill.toObject());
      })
    })
}

exports.sendSMS = function (req, res) {
  var url = "http://sms.technoapex.in/sendurlcomma.aspx?user=20063804&pwd=xzi2zu&senderid=POSIST&mobileno=%mobile%&msgtext=%msg%";
  // var url="http://203.212.70.200/smpp/sendsms?username=posistapi&password=del12345&to=%mobile%&from=POSIST&text=%msg%";

  SmsTemplate.findOne({templateNumber: req.query.templateNumber}, {content: 1, _id: 0}, function (err, smsTemplate) {
    if (err) {
      return handleError(res, err);
    }
    var template = '';
    if (req.query.type == 'Kot') {
      var template = smsTemplate.content;
      template = replaceAll('%name%', ' ' + req.query.name + ',\r\n', template);
      template = replaceAll('%billnumber%', ':' + req.query.billnumber.toString(), template + '. \r');
      template = replaceAll('%content%', ' ' + req.query.content, template + ' ');
      template = replaceAll('%phone%', req.query.phone, template);
    }
    if (req.query.type == 'Dispatch') {
      var template = smsTemplate.content;
      template = replaceAll('%name%', ' ' + req.query.name + ',\r\n', template);
      template = replaceAll('%billnumber%', ':' + req.query.billnumber.toString(), template);
      template = replaceAll('%deliveryboy%', ' ' + req.query.deliveryBoy, template);
      template = replaceAll('%amount%', ' ' + 'Order Amount Rs.' + req.query.amount, template);
    }
    if (req.query.type == 'Delivery') {
      var template = smsTemplate.content;
      template = replaceAll('%name%', ' ' + req.query.name + ',\r\n', template);
      template = replaceAll('%billnumber%', ':' + req.query.billnumber.toString(), template);
      //  template = replaceAll('%deliveryboy%', ' ' + req.query.deliveryBoy, template + ' ');
      template = replaceAll('%amount%', ' ' + 'Rs.' + req.query.amount, template);
    }
    url = replaceAll('%mobile%', req.query.mobile, url);
    url = replaceAll('%msg%', template, url);


    request(url, function (error, response, body) {
      //utils.sendSMSExotel(req.query.mobile,template).then(function(bodyJSON){
      // console.log( req.query);
      var transactionDetail = {
        type: req.query.type,
        quantity: -1,
        rate: 0,
        user: {},
        deployment_id: req.query.deployment_id
      };
      Deployment.update({_id: ( req.query.deployment_id)}, {$inc: {balanceSMS: -1}}, {multi: false}, function (errde, result1) {
        SmsTransaction.create(transactionDetail, function (err1, smsTrans) {
          /* if (err1) {
           return handleError(res, err1);
           }
           return res.json(200, {rowsAffected:result});*/
        });
      });
      // console.log(body);
    });


    return res.json(200, {message: template});
  })
}

exports.sendSaleSMS = function (req, res) {
  var smsUrls = [];
  var interval = 5;
  // var currentDate=new Date(new Date().getTime()+330*60000);
  var currentDate = new Date();
  var currentHour = currentDate.getHours();
  var currentMin = currentDate.getMinutes();
  /*console.log(currentHour);
   console.log(currentMin);*/
  Deployment.find({
    'smsSettings.type': 'Sale',
    'smsSettings.isApplied': true,
    'smsSettings.time.hour': currentHour,
    'smsSettings.time.min': {$gte: currentMin, $lt: currentMin + (interval)}
  }, {smsSettings: 1, settings: 1, name: 1}, function (err, deps) {
    if (err) {
      return handleError(res, err);
    }
    async.eachSeries(deps, function (dep, callback) {
      var templateNumber = 0;
      var settings = dep.smsSettings;
      var mobiles = '';
      var isDayBack = false;
      var _setting = _.filter(settings, {'type': 'Sale'});
      //console.log(_setting);
      if (_setting.length > 0) {
        templateNumber = (_setting[0].templateNumber);
        if (_.has(_setting[0], 'mobiles')) {
          mobiles = _setting[0].mobiles;
        }
        if (_.has(_setting[0], 'dayBack')) {
          isDayBack = _setting[0].dayBack;
        }
      }
      // console.log(mobiles);

      if (mobiles != '') {
        var mobileArr = mobiles.split(',');
        SmsTemplate.findOne({templateNumber: templateNumber.toString()}, {
          content: 1,
          _id: 0
        }, function (err, smsTemplate) {
          if (err) {
            callback();
            // return handleError(res, err);
          }
          var _dayCutOff = _.filter(dep.settings, {name: 'day_sale_cutoff'});
          var hr = new Date(_dayCutOff[0].value).getHours();

          var resetDate = currentDate.setHours(hr, 0, 0, 0);
          if (!isDayBack) {
            resetDate = moment(resetDate).add(1, 'days').toDate()
          }
          //console.log( moment(currentDate).toDate());
          Bill.aggregate([
            {
              $match: {
                _created: {
                  $gte: moment(resetDate).add(-1, 'days').toDate(),
                  $lte: moment(resetDate).toDate()
                },
                deployment_id: dep._id,
                isDeleted: {$ne: true}
              }
            },
            {
              $unwind: "$_kots"
            },
            {
              $project: {
                //kot:  "$_kots",
                // kot:{$cond:{if:'$_kots.isVoid',then:{},else:"$_kots"}},
                kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
                billDiscountAmount: "$billDiscountAmount",
                serialNumber: "$daySerialNumber",
                _created: "$_created",
                isVoid: "$isVoid"
              }
            },
            {
              $group: {
                _id: {
                  billId: "$_id", created: "$_created", serial: "$serialNumber"
                  , billDiscountAmount: "$billDiscountAmount", isVoid: "$isVoid"
                },
                taxAmount: {$sum: "$kot.taxAmount"},
                totalDiscount: {$sum: "$kot.totalDiscount"},
                totalAmount: {$sum: "$kot.totalAmount"}
              }
            },
            {
              $project: {
                _id: 0,
                created: '$_id.created',
                serialNumber: '$_id.serial',
                //gTotal: {$cond:{if:'$_id.isVoid',then:0,else:{$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]}}}
                taxAmount: {$cond: ['$_id.isVoid', 0, '$taxAmount']},
                totalDiscount: {$cond: ['$_id.isVoid', 0, '$totalDiscount']},
                billDiscountAmount: {$cond: ['$_id.isVoid', 0, "$_id.billDiscountAmount"]},
                totalAmount: {$cond: ['$_id.isVoid', 0, '$totalAmount']},
                gTotal: {$cond: ['$_id.isVoid', 0, {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]}]}

              }
            },
            {
              $group: {
                _id: 0,
                gTax: {$sum: '$taxAmount'},
                gDiscount: {$sum: '$totalDiscount'},
                gBDiscount: {$sum: '$billDiscountAmount'},
                gAmount: {$sum: '$totalAmount'},
                gTotal: {$sum: '$gTotal'}
              }
            }
          ], function (err, result) {
            // console.log(err);
            if (err) {
              callback();
              // return handleError(res, err);
            }
            // console.log(result);
            var amountContent = '';
            if (result.length > 0) {
              //var amount = (Math.round(result[0].gTotal));
              amountContent = '\r\nNet Amount :' + result[0].gAmount + '\r\n';
              amountContent += 'Discount      :-' + (result[0].gDiscount + result[0].gBDiscount) + '\r\n';
              amountContent += 'TaxAmount  :' + (result[0].gTax) + '\r\n';
              amountContent += '---------------------\r\n';
              amountContent += 'TotalAmount:' + Math.round(result[0].gTotal);
            } else {
              amountContent = '\r\nNet Amount :' + '0.00' + '\r\n';
              amountContent += 'Discount      :-' + '0.00' + '\r\n';
              amountContent += 'TaxAmount  :' + '0.00' + '\r\n';
              amountContent += '---------------------\r\n';
              amountContent += 'TotalAmount:' + '0.00';
            }
            var name = dep.name;
            //    var mobile = '9891077809';
            var url = "http://sms.technoapex.in/sendurlcomma.aspx?user=20063804&pwd=xzi2zu&senderid=POSIST&mobileno=%mobile%&msgtext=%msg%";
            // var url="http://203.212.70.200/smpp/sendsms?username=posistapi&password=del12345&to=%mobile%&from=POSIST&text=%msg%";
            var template = smsTemplate.content;
            template = replaceAll('%name%', ' ' + name + ',\r\n', template);
            template = replaceAll('%date%', ':' + getFormattedDate(moment(resetDate).toDate()) + ' ', template);
            template = replaceAll('%amount%', ' ' + '' + amountContent + '\r\n Thank', template);
            url = replaceAll('%msg%', template, url);
            console.log(url);
            mobileArr.forEach(function (mobile) {
              //console.log(mobile);
              url = replaceAll('%mobile%', mobile, url);
              smsUrls.push(url);
              request(url, function (error, response, body) {
                //utils.sendSMSExotel(mobile,template).then(function(body){
                console.log(body);
              });
            })
            callback();

          })
        });
      } else {
        callback();
      }
    }, function () {
      //  excuteSMSUrls(smsUrls);
      res.json(200, {message: 'done'});
    })


  }, function () {

  })
  /*SmsTemplate.findOne({templateNumber: req.query.templateNumber}, {
   content: req.query.templateNumber,
   _id: 0
   }, function (err, smsTemplate) {
   if (err) {
   return handleError(res, err);
   }
   });*/
}

exports.triggerSupportApp = function (req, res) {
  var url = "http://support.posist.in/Email_SMS_ReminderHandler.ashx";
  request(url, function (error, response, body) {
    //utils.sendSMSExotel(mobile,template).then(function(body){
    // console.log(body);
  });
  res.json(200, {"status:": "called successfully"});
}

exports.getItemWiseEnterprise = function (req, res ,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getItemWiseEnterprise', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getItemWiseEnterprise',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.itemWiseEnterpriseAggregation(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
}

function excuteSMSUrls(smsurls) {
  async.eachSeries(smsurls, function (url, callback) {
    //console.log(url);
    callback();
  })
}

function getFormattedDate(datetime) {
  var cdate = datetime;
  var month = (cdate.getMonth() + 1).toString();
  var day = (cdate.getDate()).toString();
  month = ((month.length == 1) ? "0" + month : month);
  day = ((day.length == 1) ? "0" + day : day);
  //alert(day);
  return cdate.getFullYear() + "-" + month + "-" + day;
}

function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}

exports.billsBackUp = function (req, res) {
  /* var billId = req.params.id;*/
  var date = new Date();
  //console.log(req.body);
  var objectid = randomObjectId();// ObjectID();
  BillM.count({
    _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
    deployment_id: req.query.deployment_id
  }, function (aa, cnt) {
    console.log(cnt);
    var startTime = Date.now();
    var count = 0;
    var c = Math.round(cnt / 990);
    console.log(c);
    var fakeArr = [];
    var size = 999;
    //fakeArr.length = c;
    var docsSaved = 0;
    var a = '';
    for (var i = 0; i <= c; i++) {
      fakeArr.push(i)
    }
    ;
    //console.log(fakeArr);
    async.each(fakeArr, function (item, callback) {
      BillM.find({
          _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
          deployment_id: req.query.deployment_id,
          isDeleted: {$ne: true}
        }
        , {}, {skip: count, limit: size}, function (err, bills) {
          if (err) {
            return handleError(res, errInsert);
          }

          var dataObject = [];
          bills.forEach(function (bill) {
            bill._id = new mongoose.Types.ObjectId();
            // bill._id=undefined;
            //console.log( bill._id);
            dataObject.push(bill.toObject());
          })
          var sliced = bills;
          //.slice(count, count + 999);

          console.log(sliced.length);
          count = count + size;
          if (sliced.length > 0) {
            /*  BillSM.collection.insert(sliced, function (err, docs) {
             //  docsSaved += docs.ops.length
             callback();
             });*/

            Bill.collection.insert(dataObject, function (errInsert, docs) {
              if (errInsert) {
                console.log(errInsert);
                return handleError(res, errInsert);
              }
              //  docsSaved += docs.ops.length
              console.log(errInsert);
              console.log('inserted:' + docs.length);
              callback();
            });
          } else {
            callback();
          }
        })
      //console.log(a);
    }, function (err) {
      if (err) {
        console.log(err);
        return handleError(res, err);
      }
      console.log(" gameResult.js > BULK INSERT AMOUNT: ", cnt, "docsSaved  ", docsSaved, " DIFF TIME:", Date.now() - startTime);
      res.json(200, {'docs': cnt});
    });
  })
}

function onInsert(err, docs) {
  if (err) {
    // TODO: handle error
  } else {
    console.log('%d potatoes were successfully stored.', docs.length);
    // res.send();
  }
}

exports.billsRestore = function (req, res) {
  /* var billId = req.params.id;*/
  var date = new Date();
  //console.log(req.body);
  var objectid = randomObjectId();// ObjectID();
  Bill.find({
    _created: {$gte: new Date(req.body.startDate), $lte: new Date(req.body.endDate)},
    deployment_id: req.body.dep_id, 'copy_id.isModified': true
  }, {copy_id: 1}, function (err, billCopy) {
    console.log(billCopy.length);
    async.each(billCopy, function (cBill, callback) {
      Bill.update({_id: cBill._id}, {
          $set: {
            _kots: cBill.copy_id._kots,
            billDiscountAmount: cBill.copy_id.billDiscountAmount,
            _offers: cBill.copy_id._offers,
            payments: cBill.copy_id.payments
          }
        }, {multi: false},
        function (err) {
          callback()
        }
      );
    })
    Bill.update({
        _created: {$gte: new Date(req.body.startDate), $lte: new Date(req.body.endDate)},
        deployment_id: req.body.dep_id
      }
      , {$set: {isDeleted: false}}, {multi: true}, function (err) {
        res.send({updated: true});
      });

  })

  var _bills = 0;
}

exports.billsForManipulation = function (req, res) {
  Bill.aggregate([
    {
      $match: {
        _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
        deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
        isDeleted: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        kot: "$_kots"
      }
    },
    {
      $group: {
        _id: {
          billId: "$_id"
        },
        totalItems: {$sum: "$kot.totalItems"}
      }
    },
    {
      $project: {
        _id: 0,
        gTotal: "$totalItems"
        , billId: "$_id.billId"
      }
    },
    {$sort: {gTotal: -1}},
    {$limit: 40}
    , {$project: {_id: 0, billId: '$billId'}}
    ////////////////////////////////////////////////////////

  ], function (err, result) {
    // console.log(result);
    if (err) {
      return handleError(res, err);
    }
    var ids = [];
    result.forEach(function (bill) {
      ids.push(bill.billId);
    })
    Bill.find({
        '_id': {
          $in: ids
        }
      }
      //,{_kots:1,payments:1,billDiscountAmount:1,ng_id:1,_offers:1}
      , function (erb, bills) {
        if (erb) {
          console.log(erb);
          return handleError(res, erb);
        }
        //  console.log(bills);
        return res.send(bills);
      })
  })
}

exports.billsTotalAmount = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/billsTotalAmount?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    Bill.aggregate([
      {
        $match: {
          _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
          deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
          isDeleted: {$ne: true}
        }
      },
      {
        $unwind: "$_kots"
      },
      {
        $project: {
          //kot:  "$_kots",
          // kot:{$cond:{if:'$_kots.isVoid',then:{},else:"$_kots"}},
          kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
          billDiscountAmount: "$billDiscountAmount",
          serialNumber: "$daySerialNumber",
          _created: "$_created",
          isVoid: "$isVoid"
        }
      },
      {
        $group: {
          _id: {
            billId: "$_id", created: "$_created", serial: "$serialNumber"
            , billDiscountAmount: "$billDiscountAmount", isVoid: "$isVoid"
          },
          taxAmount: {$sum: "$kot.taxAmount"},
          totalDiscount: {$sum: "$kot.totalDiscount"},
          totalAmount: {$sum: "$kot.totalAmount"}
        }
      },
      {
        $project: {
          _id: 0,
          created: '$_id.created',
          serialNumber: '$_id.serial',
          //gTotal: {$cond:{if:'$_id.isVoid',then:0,else:{$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]}}}
          gTotal: {$cond: ['$_id.isVoid', 0, {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]}]}

        }
      },
      {
        $group: {
          _id: 0,
          gTotal: {$sum: '$gTotal'}
        }
      }
    ], function (err, result) {
      // console.log(result);
      if (err) {
        return handleError(res, err);
      }
      return res.send(result);
    })
  }
}

function getTopItems(condition) {
  var d = Q.defer();
  Bill.aggregate([
    {
      $match: condition
    },
    {
      $unwind: "$_kots"
    },
    {
      $unwind: "$_kots.items"
    },
    {
      $project: {
        items: "$_kots.items"
      }
    },
    {
      $group: {
        _id: {
          itemName: "$items.name"
        },
        qty: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.itemName",
        qty: "$qty"
      }
    },
    {$sort: {qty: -1}},
    {$limit: 5}
  ], function (err, result) {
    // console.log(result);
    if (err) {
      console.log(err);
      d.reject('done');
    } else {
      d.resolve(result);
    }
  })
  return d.promise;
}
function getBillGTotal(condition, cust_id) {
  // console.log(cust_id);
  var d = Q.defer();
  Bill.aggregate([
    {
      $match: condition
    },

    {
      $unwind: "$_kots"
    },
    {
      $project: {
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        billDiscountAmount: "$billDiscountAmount"
      }
    },
    {
      $group: {
        _id: 0,
        taxAmount: {$sum: "$kot.taxAmount"},
        totalDiscount: {$sum: "$kot.totalDiscount"},
        totalAmount: {$sum: "$kot.totalAmount"},
        billDiscountAmount: {$sum: "$billDiscountAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        gTotal: {$subtract: [{$add: ["$taxAmount", "$totalAmount"]}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]}
      }
    }
  ], function (err, result) {
    if (err) {
      d.reject('error');
    } else {
      //console.log(result);
      d.resolve(result.length == 0 ? 0 : result[0].gTotal);
    }
  })
  return d.promise;
}

exports.CRMDetailFromBill = function (req, res) {
  // console.log(req.query);
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/CRMDetailFromBill?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    getBillGTotal({
      isDeleted: {$ne: true}, isVoid: {$ne: true}
      , '_customer.customerId': req.query.customerId
    }).then(function (gTotal) {
      getBillGTotal({
        isDeleted: {$ne: true},
        isVoid: {$ne: true},
        _created: {$gte: moment(new Date()).add(-10, 'days').toDate(), $lte: moment(new Date()).toDate()}
        ,
        '_customer.customerId': ( req.query.customerId)
      }).then(function (mTotal) {
        Bill.findOne({
          isDeleted: {$ne: true},
          isVoid: {$ne: true},
          '_customer.customerId': ( req.query.customerId)
        }, {_id: 1}, {sort: {_created: -1}}, function (err, bill) {
          if (bill) {
            getBillGTotal({
              _id: bill._id
            }).then(function (lbill) {
              getTopItems({
                isDeleted: {$ne: true},
                '_customer.customerId': (req.query.customerId),
                isVoid: {$ne: true},
                _created: {
                  $gte: moment(new Date()).add(-30, 'days').toDate(),
                  $lte: moment(new Date()).toDate()
                }
              }).then(function (topItems) {
                return res.send({
                  month: Math.round(mTotal),
                  gTotal: Math.round(gTotal),
                  lBill: Math.round(lbill),
                  topItems: topItems
                });
              }, function (err) {
                return handleError(res, err)
              })
            }, function (err) {
              return handleError(res, err)
            })
          } else {
            getTopItems({
              isDeleted: {$ne: true},
              '_customer.customerId': (req.query.customerId),
              isVoid: {$ne: true},
              _created: {
                $gte: moment(new Date()).add(-10, 'days').toDate(),
                $lte: moment(new Date()).toDate()
              }
            }).then(function (topItems) {
              return res.send({
                month: Math.round(mTotal),
                gTotal: Math.round(gTotal),
                lBill: 0,
                topItems: topItems
              });
            }, function (err) {
              return handleError(res, err)
            })
          }
        })
      }, function (err) {
        return handleError(res, err)
      })

    }, function (err) {
      return handleError(res, err)
    })
  }
}

exports.billsAmountCount = function (req, res) {
  /* var billId = req.params.id;*/


  if (config.isQueue) {

    request.post(config.reportServer.url + '/api/bills/billsAmountCount', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  }
  else {

    Bill.aggregate([
      {
        $match: {
          _created: {
            $gte: new Date(req.body.startDate), $lte: new Date(req.body.endDate)
          },
          deployment_id: req.body.deployment_id,
          isDeleted: {$ne: true}
        }
      },
      {
        $unwind: "$_kots"
      },
      {
        $project: {
          kot: "$_kots",
          billDiscountAmount: "$billDiscountAmount"
        }
      },
      {
        $group: {
          _id: {billId: "$_id", billDiscountAmount: "$billDiscountAmount"},
          taxAmount: {$sum: "$kot.taxAmount"},
          totalDiscount: {$sum: "$kot.totalDiscount"},
          totalAmount: {$sum: "$kot.totalAmount"}
        }
      },
      {
        $project: {
          _id: 0,
          gTotal: {$subtract: [{$add: ['$totalAmount', '$taxAmount']}, {$add: ["$totalDiscount", "$_id.billDiscountAmount"]}]}
          , billId: "$_id.billId"
        }
      },
      {$sort: {gTotal: -1}},
      {$limit: 50}
      , {$project: {_id: 0, billId: '$billId'}}
    ], function (err, result) {
      if (err) {
        return handleError(res, err);
      }
      var ids = [];
      result.forEach(function (bill) {
        ids.push(bill.billId);
      })
      Bill.find({
        '_id': {
          $in: ids
        }
      }, function (erb, bills) {
        if (erb) {
          console.log(erb);
          return handleError(res, erb);
        }
        return res.send(bills);
      })
    })
  }
}

/* Get Date Range Bill */
exports.getDataRangeBills = function (req, res) {
  if(config.isQueue){
    request(config.reportServer.url+'/api/bills/getDataRangeBills?'+utils.createGetUrl(req.query),function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    //console.log(req.query);
    //console.log(moment(moment(req.query.fromDate)).format('YYYY-MM-DD HH:mm:ss'));
    //console.log(moment(moment(req.query.toDate)).format('YYYY-MM-DD HH:mm:ss'));
    var fdate = new Date(req.query.fromDate);
    var tdate = new Date(req.query.toDate);
    tdate = tdate.setDate(tdate.getDate() + 1);
    console.log(tdate);
    var _bills = [];
    var query = {
      deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
      _created: {
        $gte: new Date(fdate), $lte: new Date(tdate)
      },
      isDeleted: {$ne: true}
    };
    //async.eachSeries(_dates, function (date, callback) {
    if (req.query.complimentary != undefined) {
      // console.log(req.query.complimentary);
      if (req.query.complimentary == 'true') {

        query = {
          complimentary: req.query.complimentary,
         // tenant_id: req.query.tenant_id,
          deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
          _created: {
            $gte: new Date(fdate), $lte: new Date(tdate)
          },
          isDeleted: {$ne: true}
        }
      }
    }
    Bill.find(
      query
      , function (err, bills) {
        if (err) {
          callback(err);
        }

        /* bills.forEach(function (bill) {
         _bills.push(bill);
         });*/
        res.json(200, bills);
      })
  }
};


exports.getDataRangeBillsNew = function (req, res ,cb) {
  if(config.isQueue){
    request(config.reportServer.url+'/api/bills/getDataRangeBillsNew?'+utils.createGetUrl(req.query),function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if (req.query.backend === 'true') {
    var reqBreak = {};
    var fdate = moment(req.query.fromDate);
    var tdate = moment(req.query.toDate);
    tdate = tdate.add(1, 'days');
    var query = {
      deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
      _created: {
        $gte: new Date(fdate), $lte: new Date(tdate)
      },
      isDeleted: {$ne: true},
      isVoid: {$ne: true}
    };
    if (req.query.tabType != "") {
      query = {
        _created: {
          $gte: new Date(fdate), $lte:new Date(tdate)
        },
        deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
        isDeleted: {$ne: true},
        isVoid: {$ne: true},
        tab: req.query.tabType
      };
    }
    dateBreakupBalancing({type:'bill', fromDate:req.query.fromDate, toDate:req.query.toDate, query:query}).then(function(result){
      // console.log(result)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.query.fromDate = moment(date.fromDate)
          reqBreak.query.toDate = moment(date.toDate)
          queue.create('getDataRangeBillsNew',reqBreak.query).attempts(3).removeOnComplete(false).save(function(err){
            cb()
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,{})
      } else {
        var job = queue.create('getDataRangeBillsNew',req.query).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, {});
        });
      }
    }).catch(function(err){
      if(err)
        return res.json(404, {error: 'error in data'})
    })
  } else {
    //console.log(new Date(tdate));
    // console.log(tdate);
    var conditions=req.query;
    //console.log(conditions);
    utils.getBills(conditions).then(function(bills){
      if(req.query.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    })
  }
};

/* Check for New Customers */
exports.getCustomerCount = function (req, res) {

  /*
   Bill.find()
   .where('tenant_id')
   .equals(req.query.tenant_id)
   .where('deployment_id')
   .equals(req.query.deployment_id)
   .where('_created')
   .lte(req.query.beforeDate)
   .where('_customer.customerId')
   .equals(req.query.customer_id)
   .where('isDeleted')
   .ne(true)
   .count()
   .exec(function (err, count) {

   if (err) {
   return handleError(res, err);
   }
   */

  //console.log(req.query);
  //console.log(count);

  return res.json(201, {customerId: req.query.customer_id, count: 0, gBillIndex: parseInt(req.query.gBillIndex)});
};


// Get a single bill
exports.show = function (req, res) {
  Bill.findById(req.params.id, function (err, bill) {
    if (err) {
      return handleError(res, err);
    }
    if (!bill) {
      return res.send(404);
    }
    return res.json(bill);
  });
};

// Creates a new bill in the DB.
exports.create = function (req, res) {
  Bill.create(req.body, function (err, bill) {
    if (err) {
      return handleError(res, err);
    }

    return res.json(201, bill);
  });
};

// Create a new Bill with Angular ID.
exports.createWithId = function (req, res) {
  //setTimeout(function() {
  /* var billId = req.params.id;*/
 // console.log(req.body);
  var billId = req.body._id;
  var _bill = req.body;
  /* Get this shit out */
  delete req.body._id;
  delete req.body.db;

  _bill.isSynced = true;
  _bill.syncedOn = new Date();
  _bill.created = new Date();
  _bill.ng_id = billId;
  var bills = [];

  /* Bill.findOne({ 'ng_id' : req.params.id}, {}, function (err, bill) {
   */
  Bill.findOne({'ng_id': billId, deployment_id: _bill.deployment_id}, {
    deployment_id: 1,
    _created: 1,
    serialNumber: 1
  }, function (err, bill) {
    if (!bill) {
      Bill.create(_bill, function (err, sbill) {
        if (err) {
          console.log(err);
          return handleError(res, err);
        }
        _bill = {};
        sbill = {};
        bill = null;
        // console.log(sbill);
        //  setTimeout(function(){},200)
        return res.json(201, {billId: billId});
      });
    } else {
      if (bill.deployment_id == _bill.deployment_id && bill.serialNumber == _bill.serialNumber && bill._created.toISOString() == new Date(_bill._created).toISOString()) {
        bill = null;
        _bill = null;
        return res.json(200, {billId: billId});
      } else {
        bill = null;
        _bill = null;
        console.log('Duplicate Key');
        return res.json(200, {billId: 'duplicateKey'});
      }
      return res.json(200, {billId: 'duplicateKey'});
    }
  });
  //}, 10000);
};

// Create a new Bill with Angular ID.
exports.updateWithId = function (req, res) {
  /* var billId = req.params.id;*/
  // console.log(req.body);
  var billId = req.body.id;
  var _bill = req.body;
  /* Get this shit out */
  delete req.body._id;
  delete req.body.db;

  _bill.isSynced = true;
  _bill.syncedOn = new Date();
  // _bill.created = new Date();
  Bill.findOne({'_id': new mongoose.Types.ObjectId(billId)}, function (err, bill) {
    var updated = _.extend(bill, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, {billId: billId});
    });
  });
}

// Create a new Bill with Angular ID.
exports.getLatestBill = function (req, res) {
  /* var billId = req.params.id;*/
  //  console.log(req.params);
  Bill.findOne({
    'tenant_id': req.params.tenantId, 'deployment_id': req.params.deploymentId,
    isDeleted: {$ne: true}
  }, {}, {sort: {'_created': -1}}, function (err, bill) {
    if (err) {
      return handleError(res, err);
    }
  //console.log(bill)
    if (bill == null)
      return res.json(200, []);
    else
      return res.json(200, [bill]);
  });
  // res.json(200, "{status:HELLO}");
}

exports.billItemsCount = function (req, res) {
  /* var billId = req.params.id;*/
  //console.log(req.params);
  return res.json(200, []);
  var currentDate = new Date();
  var items = [];
  var lastDate = new Date(currentDate.setDate(currentDate.getDate() - 7));
  // console.log(lastDate);
  Bill.find({
    'tenant_id': req.params.tenantId,
    'deployment_id': req.params.deploymentId,
    _created: {$gte: lastDate, $lt: new Date()},
    isDeleted: {$ne: true}
  }, {_kots: 1, _id: 0}, function (err, bills) {
    //Bill.find({}, {}, function (err, bills) {
    if (err) {
      return handleError(res, err);
    }
    //console.log(bills)
    _.forEach(bills, function (bill) {
      _.forEach(bill._kots, function (kot) {
        _.forEach(kot.items, function (item) {
          var itemOb = {_id: item._id, quantity: item.quantity};
          items.push(itemOb);
        });
      });
    });
    var uItems = [];
    _.chain(items).groupBy('_id').map(function (itm) {
      var tQty = 0;
      for (var i = 0; i < itm.length; i++) {
        tQty += itm[i].quantity;
      }
      var uOb = {_id: itm[0]._id, quantity: tQty};
      uItems.push(uOb);
      //console.log(i);
    });
    return res.json(200, uItems);
  });
  // res.json(200, "{status:HELLO}");
}


// Updates an existing bill in the DB.
exports.update = function (req, res) {
  console.log(req.body);
  if (req.body._id) {
    delete req.body._id;
  }
  Bill.findById(req.params.id, function (err, bill) {
    if (err) {
      return handleError(res, err);
    }
    if (!bill) {
      return res.send(404);
    }
    var updated = _.merge(bill, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, bill);
    });
  });
};

// Updates an existing bill in the DB.
exports.updateById = function (req, res) {
  //console.log(req.body);

  Bill.findOne({ng_id: req.body.ng_id, deployment_id: req.body.deployment_id}, function (err, bill) {
    if (err) {
      return handleError(res, err);
    }
    if (!bill) {
      return res.send(404);
    }
    var updated = _.merge(bill, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, bill);
    });
  });
};

exports.updateBill = function (req, res) {
  // console.log(req.body.bill.length);
  async.eachSeries(req.body.bill, function (mbill, callback) {
    var id = ( mbill._id);
    if (mbill._id) {
      delete mbill._id;
    }
    Bill.findById(id, function (err, bill) {
      if (err) {
        callback();
      }
      if (!bill) {
        callback();
      }
      var updated = _.extend(bill, mbill);
      updated.save(function (err) {
        if (err) {
          //    return handleError(res, err);
          callback();
        }
        // return res.json(200, bill);
        callback();
      });
    });
  }, function (err, results) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, {'count': req.body.bill.length});
  });

  /*console.log(req.body.bill);
   Bill.collection.insert(req.body.bill,function(err,result){
   if (err) {
   return handleError(res, err);
   }
   return res.json(200, result);
   })*/
};
exports.markDeleteBill = function (req, res) {
  console.log(req.query);
  if (req.query.type == 'dateWise') {
    Bill.update({
        _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
        deployment_id: req.query.deployment_id
      }
      , {$set: {isDeleted: true}}, {multi: true}, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        // console.log(result);
        if ((req.query.cards != null )) {
          Bill.find({
              _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
              deployment_id: req.query.deployment_id
              //  , 'payments.cards':{$elemMatch:{cardType:'CreditCard',totalAmount:{$gt:0}}}
            }
            , {payments: 1}, {}, function (err, pCards) {
              if (err) {
                return handleError(res, err);
              }
              // console.log(req.query.cards);
              // console.log(pCards);
              var unSelectedCards = req.query.cards;
              async.eachSeries(pCards, function (pCard, callback) {
                console.log(pCard);
                var isFound = false;
                for (var i = 0; i < unSelectedCards.length; i++) {
                  for (var j = 0; j < pCard.payments.cards.length; j++) {
                    if (pCard.payments.cards[j].cardType == unSelectedCards[i]) {
                      if (pCard.payments.cards[j].totalAmount > 0) {
                        isFound = true;
                        break;
                      }
                    }
                  }
                }
                if (isFound) {

                  Bill.update({_id: pCard._id}, {$set: {isDeleted: false}}, {multi: false}, function (err, r) {
                    //  console.log(r);
                    console.log(r);
                    callback();
                  })
                } else {
                  callback();
                }
              }, function () {
                res.send({status: 'done'});
              })
              // console.log(result);

            });
        }else{
        res.send({status: 'done'})
        }
      });
  }
  if (req.query.type == 'billWise') {
    var startDate = req.query.startDate;
    Bill.update({
        _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
        deployment_id: req.query.deployment_id,
        $or: [{
          'serialNumber': {
            $gte: parseInt(req.query.serialFrom),
            $lt: parseInt(req.query.serialTo) + 1
          }
        }, {'daySerialNumber': {$gte: parseInt(req.query.serialFrom), $lt: parseInt(req.query.serialTo)}}]
      }
      , {$set: {isDeleted: true}}, {multi: true}, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        //  console.log(result);
        if ((req.query.cards != null )) {
          Bill.find({
              _created: {$gte: new Date(req.query.startDate), $lte: new Date(req.query.endDate)},
              deployment_id: req.query.deployment_id,
              $or: [{
                'serialNumber': {
                  $gte: parseInt(req.query.serialFrom),
                  $lt: parseInt(req.query.serialTo)
                }
              }, {'daySerialNumber': {$gte: parseInt(req.query.serialFrom), $lt: parseInt(req.query.serialTo)}}]
              //  , 'payments.cards':{$elemMatch:{cardType:'CreditCard',totalAmount:{$gt:0}}}
            }
            , {payments: 1}, {}, function (err, pCards) {
              if (err) {
                return handleError(res, err);
              }
              //            console.log(req.query.cards);
              var unSelectedCards = req.query.cards;
              async.eachSeries(pCards, function (pCard, callback) {
                var isFound = false;
                for (var i = 0; i < unSelectedCards.length; i++) {
                  for (var j = 0; j < pCard.payments.cards.length; j++) {
                    if (pCard.payments.cards[j].cardType == unSelectedCards[i]) {
                      if (pCard.payments.cards[j].totalAmount > 0) {
                        isFound = true;
                      }
                    }
                  }
                }
                if (isFound) {
                  Bill.update({_id: pCard._id}, {$set: {isDeleted: false}}, {multi: false}, function (err, r) {
                    //  console.log(r);
                    callback();
                  })
                } else {
                  callback();
                }
              }, function () {
                res.send({status: 'done'});
              })
              // console.log(result);

            });
        }else{
        res.send({status: 'done'})
        }
      });
  }
};

//Update set
exports.updateBillSet = function (req, res) {
  console.log(req.body);
  var voidDetail={time:new Date()};
  //console.log(_.has( req.body.sets,'voidDetail'));
  if(_.has( req.body.sets,'voidDetail')){
    voidDetail=req.body.sets.voidDetail;
  }
  Bill.update({ng_id: req.body.q.ng_id, deployment_id: req.body.q.deployment_id}, {
    isVoid: true,
    payments: [],
    _cash: [],
    billDiscountAmount: 0,
    voidComment: req.body.voidComment,
    voidDetail:voidDetail
  }, function (err, affected, resp) {
    if (err) {
      console.log(err);
      return handleError(res, err);
    }
    //  console.log(resp);
    return res.json(200, resp);
  });
  // res.json(200,{method:'updateBillSet'});
};
// Deletes a bill from the DB.
exports.destroy = function (req, res) {
  Bill.findById(req.params.id, function (err, bill) {
    if (err) {
      return handleError(res, err);
    }
    if (!bill) {
      return res.send(404);
    }
    bill.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};
/*Kanishk report made for Instances*/
//gets consolidated complimentary items
//gets consolidated complimentary items
exports.getWaiterBillsInDateRange = function (req, res) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getWaiterBillsInDateRange', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  }
  else {
    var startDate = new Date(req.body.arg.startDate);
    var endDate = new Date(req.body.arg.endDate);
    endDate = endDate.setDate(endDate.getDate() + 1);
    var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
    var waiter_id = req.body.arg.waiter_id;
    console.log(waiter_id);

    Bill.find({
      _created: {
        $gte: moment(startDate).toDate(),
        $lte: moment(endDate).toDate()
      },
      deployment_id: deployment_id,
      "_waiter._id": waiter_id,
      isDeleted: {$ne: true}
    }, function (err, bills) {

      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(bills);
    });
  }
};

exports.getComplimentaryConsolidated = function (req, res) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getComplimentaryConsolidated', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var startDate = new Date(req.body.arg.startDate);
    var endDate = new Date(req.body.arg.endDate);
    endDate = endDate.setDate(endDate.getDate() + 1);

    //console.log(req.body);
    Bill.aggregate([
      {
        $match: {
          _created: {
            $gte: moment(startDate).toDate(),
            $lte: moment(endDate).toDate()
          },
          deployment_id: new mongoose.Types.ObjectId(req.body.arg.deps),
          isDeleted: {$ne: true},
          isVoid: {$ne: true}
        }
      },
      {
        $unwind: "$_offers"
      },
      {
        $match: {
          "_offers.offer.complimentary": true
        }
      },
      {
        $unwind: "$_kots"
      },
      {
        $project: {
          _id: {depid: "$deployment_id"},
          kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
          _created: "$_created",
          tab: "$tab"
        }
      },
      {
        $unwind: "$kot.items"
      },
      {
        $project: {
          items: "$kot.items",
          segment: "$kot.items.category.categoryName",
          created: {
            "$concat": [
              {"$substr": [{"$year": "$_created"}, 0, 4]},
              '-',
              {
                "$cond": [
                  {"$lte": [{"$month": "$_created"}, 9]},
                  {
                    "$concat": [
                      "0",
                      {"$substr": [{"$month": "$_created"}, 0, 2]},
                    ]
                  },
                  {"$substr": [{"$month": "$_created"}, 0, 2]}
                ]
              },
              '-',
              {
                "$cond": [
                  {"$lte": [{"$dayOfMonth": "$_created"}, 9]},
                  {
                    "$concat": [
                      "0",
                      {"$substr": [{"$dayOfMonth": "$_created"}, 0, 2]},
                    ]
                  },
                  {"$substr": [{"$dayOfMonth": "$_created"}, 0, 2]}
                ]
              }
            ]
          }
        }
      },
      {
        $group: {
          _id: {
            deployment: "$_id.depid"
            , itemName: "$items.name",
            segment: "$segment"
          },
          qty: {$sum: "$items.quantity"},
          amount: {$sum: "$items.subtotal"},//,
        }
      },
      {
        $project: {
          _id: 0,
          created: "$_id.created",
          deployment: "$_id.deployment",
          itemName: "$_id.itemName",
          category: "$_id.category",
          qty: "$qty",
          amount: "$amount",
          segment: "$_id.segment"
        }
      }
    ], function (err, result) {

      if (err)
        handleError(res, err);
      if (!result)
        res.status(400).send({status: 'Not Found'});
      //console.log(result.length);
      res.status(200).json(result);
    });
  }
};

exports.getComplimentary = function (req, res , cb) {

  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getComplimentary',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var dep = req.body.arg.deployment_id ;
    var reqBreak = {};
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id: new mongoose.Types.ObjectId(dep),
      $or: [{"_kots.items.isDiscounted": true}, {"_offers.0": {$exists: true}}],
      isDeleted: {$ne: true},
      isVoid: {$ne: true}
    }
    dateBreakupBalancing({type:'bill', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getComplimentary',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getComplimentary',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  } else {
   reportApi.complimentary(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
    cb(bills)
  else
     res.json(200, bills);
  });
  }
}

exports.getConsolidatedOtherPayments = function (req, res) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getConsolidatedOtherPayments',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else {

    var startDate = new Date(req.body.arg.startDate);
    var endDate = new Date(req.body.arg.endDate);
    endDate = endDate.setDate(endDate.getDate() + 1);

    Bill.aggregate([
      {
        $match: {
          _created: {
            $gte: moment(startDate).toDate(),
            $lte: moment(endDate).toDate()
          },
          deployment_id: new mongoose.Types.ObjectId(req.body.arg.deps),
          isDeleted: {$ne: true},
          isVoid: {$ne: true}
        }

      },
      {
        $unwind: "$payments.cards"
      },
      {
        $match: {
          "payments.cards.cardType": "Other"
        }
      },
      {
        $unwind: "$payments.cards.detail"
      },
      {
        $project: {
          source: "$payments.cards.detail.otherName",
          type: "$payments.cards.detail.otherType",
          amount: "$payments.cards.detail.amount"
        }
      }
    ], function (err, result) {

      if (err)
        handleError(res, err);
      if (!result)
        res.status(400).send({status: 'Not Found'});
      res.status(200).json(result);
    });
  }
};

exports.getDiscountReports = function (req, res , cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getDiscountReports', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var dep = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
    var reqBreak = {};
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id: dep,
      $or: [{"_kots.items.isDiscounted": true}, {"_offers.0": {$exists: true}}],
      isDeleted: {$ne: true},
      isVoid: {$ne: true}
    }
    dateBreakupBalancing({type:'bill', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getDiscountReports',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getDiscountReports',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  } else {
    reportApi.discountReport(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getItemwiseDiscountReports = function (req, res , cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getItemwiseDiscountReports', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var dep = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
    var reqBreak = {};
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id: dep,
      $or: [{"_kots.items.isDiscounted": true}, {"_offers.0": {$exists: true}}],
      isDeleted: {$ne: true},
      isVoid: {$ne: true}
    }
    dateBreakupBalancing({type:'bill', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getItemwiseDiscountReports',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getItemwiseDiscountReports',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  } else {
    reportApi.itemwiseDiscountReport(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getOffersReport = function (req, res , cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getOffersReport', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var job = queue.create('getOffersReport',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.offersData(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getInstanceWiseReports = function (req, res,cb) {

  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getInstanceWiseReports',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var dep = req.body.arg.deployment_id ;
    var reqBreak = {};
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id: new mongoose.Types.ObjectId(dep)
    }
    dateBreakupBalancing({type:'bill', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getInstanceWiseReports',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getInstanceWiseReports',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  } else {
   reportApi.instanceWiseReport(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getReprintReports = function (req, res,cb) {

  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getReprintReports', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getReprintReports',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
        res.json(200, []);
    });
  } else {
     reportApi.reprintReport(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
    cb(bills)
  else
     res.json(200, bills);
  });
  }
};

exports.getBillsInDateRange = function (req, res ,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getBillsInDateRange', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'Error in data'})
      } else {
        // console.log(body);
        //return res.json(200, JSON.parse(body));
        return res.status(200).json(JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var reqBreak = {};
    var dep = req.body.arg.deployment_id ;
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id:new mongoose.Types.ObjectId(dep),
    }
    dateBreakupBalancing({type:'kot', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getBillsInDateRange',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getBillsInDateRange',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  } else {
     reportApi.billsInDateRange(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
    cb(bills)
  else
     res.json(200, bills);
  });
  }
};

exports.getBillsInDateRangeForOtherPayment = function (req, res) {
  if (config.isQueue) {

    request.post(config.reportServer.url + '/api/bills/getBillsInDateRangeForOtherPayment', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'Error in data'})
      } else {
        // console.log(body);
        //return res.json(200, JSON.parse(body));
        return res.status(200).json(JSON.parse(body));
      }
    })
  }
  else {


  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);

  Bill.find({
    _created: {
      $gte: moment(startDate).toDate(),
      $lte: moment(endDate).toDate()
    },
    deployment_id: deployment_id,
    'payments.cards.cardType': "Other",
    'payments.cards.detail.0': {$exists: true}
    /*isDeleted: {$ne: true},
     isVoid: {$ne: true}*/
  }, {
    _created: 1,
    tabType: 1,
    _closeTime: 1,
    prefix: 1,
    billNumber: 1,
    daySerialNumber: 1,
    serialNumber: 1,
    isVoid: 1,
    payments: 1

  }, function (err, bills) {

    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(bills);
  });
 }
};

exports.getSuperCategories = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getSuperCategories?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    var startDate = new Date(req.query.startDate);
    var endDate = new Date(req.query.endDate);
    endDate = endDate.setDate(endDate.getDate() + 1);
    var deployment_id = new mongoose.Types.ObjectId(req.query.deployment_id);
    Bill.aggregate([
      {
        $match: {
          _created: {
            $gte: new Date(startDate),
            $lte: new Date(endDate)
          },
          deployment_id: deployment_id
        },
      },
      {
        $unwind: "$_kots",
      },
      {
        $unwind: "$_kots.items"
      },
      {
        $group: {
          _id: {superCategory: "$_kots.items.category.superCategory.superCategoryName"}
        }
      },
      {
        $project: {
          _id: 0,
          superCategory: {$cond: ["$_id.superCategory", "$_id.superCategory", "NA"]}
        }
      }
    ], function (err, superCategories) {
      console.log(err);
      if (err)
        return handleError(res, err);
      //console.log(superCategories);
      var data = superCategories;
      Bill.aggregate([
        {
          $match: {
            _created: {
              $gte: new Date(startDate),
              $lte: new Date(endDate)
            },
            deployment_id: deployment_id
          },
        },
        {
          $unwind: "$_kots",
        },
        {
          $unwind: "$_kots.items"
        },
        {
          $unwind: "$_kots.items.addOns"
        },
        {
          $group: {
            _id: {superCategory: "$_kots.items.addOns.category.superCategory.superCategoryName"}
          }
        },
        {
          $project: {
            _id: 0,
            superCategory: {$cond: ["$_id.superCategory", "$_id.superCategory", "NA"]}
          }
        }
      ], function (err, sup_cats) {
        if (err)
          return handleError(res, err);
        _.forEach(sup_cats, function (s) {
          data.push(s);
        });
        data = _.uniq(data)
        res.status(200).json(superCategories);
      });
    });
  }
};

exports.getCategories = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getCategories?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    var startDate = new Date(req.query.startDate);
    var endDate = new Date(req.query.endDate);
    endDate = endDate.setDate(endDate.getDate() + 1);
    var deployment_id = new mongoose.Types.ObjectId(req.query.deployment_id);
    Bill.aggregate([
      {
        $match: {
          _created: {
            $gte: new Date(startDate),
            $lte: new Date(endDate)
          },
          deployment_id: deployment_id
        },
      },
      {
        $unwind: "$_kots",
      },
      {
        $unwind: "$_kots.items"
      },
      {
        $group: {
          _id: {
            category: "$_kots.items.category.categoryName",
            superCategory: "$_kots.items.category.superCategory.superCategoryName"
          }
        }
      },
      {
        $project: {
          _id: 0,
          categoryName: {$cond: ["$_id.category", "$_id.category", "NA"]},
          superCategory: {
            superCategoryName: {$cond: ["$_id.superCategory", "$_id.superCategory", "NA"]}
          }
        }
      }
    ], function (err, categories) {
      console.log(err);
      if (err)
        return handleError(res, err);
      var data = categories;
      Bill.aggregate([
        {
          $match: {
            _created: {
              $gte: new Date(startDate),
              $lte: new Date(endDate)
            },
            deployment_id: deployment_id
          },
        },
        {
          $unwind: "$_kots",
        },
        {
          $unwind: "$_kots.items"
        },
        {
          $unwind: "$_kots.items.addOns"
        },
        {
          $group: {
            _id: {
              category: "$_kots.items.addOns.category.categoryName",
              superCategory: "$_kots.items.addOns.category.superCategory.superCategoryName"
            }
          }
        },
        {
          $project: {
            _id: 0,
            categoryName: {$cond: ["$_id.category", "$_id.category", "NA"]},
            superCategory: {
              superCategoryName: {$cond: ["$_id.superCategory", "$_id.superCategory", "NA"]}
            }
          }
        }
      ], function (err, cats) {
        if (err)
          return handleError(res, err);
        _.forEach(cats, function (c) {
          data.push(c);
        });
        console.log(data);
        data = _.uniq(data);
        res.status(200).json(data);
      });
    });
  }
};

/*For pathfinder integration*/
exports.getInvoicePartner = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getInvoicePartner?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    console.log(req.query);
    var startDate = new Date(req.query.StartDate);
    var endDate = new Date(req.query.EndDate)
    endDate = new Date(endDate.setDate(endDate.getDate() + 1));
    var apiKey = req.query.apiKey;
    var deployment_id = req.query.DeploymentId;
    var resetDaily;

    if (apiKey === "CDD7DA45-C028-4DFA-ABF5-B8B152297FC5") {

      Deployment.findOne({_id: new mongoose.Types.ObjectId(deployment_id)}, {
        "settings.name": 1,
        "settings.selected": 1,
        "settings.value": 1
      }, function (err, deployment) {

        if (err)
          return handleError(res, err);
        if (!deployment)
          return res.status(400).send({errorCode: 1, message: "Not Found"});
        for (var i in deployment.settings) {
          if (deployment.settings[i].name === "reset_serial_daily") {
            if (deployment.settings[i].selected)
              resetDaily = true;
            else resetDaily = false;
            break;
          }
        }

        var fromDateWithTime = new Date(getDateFormatted(getResetDate(deployment.settings, startDate)));
        var toDateWithTime = new Date(getDateFormatted(getResetDate(deployment.settings, endDate)));

        var query = {
          _created: {
            $gte: moment(fromDateWithTime).toDate(),
            $lte: moment(toDateWithTime).toDate()
          },
          deployment_id: deployment_id,
          isDeleted: {$ne: true},
          isVoid: {$ne: true}
        }

        var projection = {
          serialNumber: 1,
          daySerialNumber: 1,
          billDiscountAmount: 1,
          _created: 1,
          _closeTime: 1,
          _covers: 1,
          payments: 1,
          _kots: 1,
          instance: 1,
          _tableId: 1
        };

        Bill.find(query, projection, function (err, bills) {
          if (err)
            return handleError(res, err);
          var formattedBills = [];
          _.forEach(bills, function (bill) {
            var instancePrefix = "";
            var taxes = {};
            if (bill.instance) {
              instancePrefix = (bill.instance.preFix || "");
            }
            var fBill = {};
            fBill.BillId = bill._id;
            fBill.BillNumber = resetDaily ? (instancePrefix + bill.daySerialNumber) : (instancePrefix + bill.serialNumber);
            fBill.CloseTime = bill._closeTime;
            fBill.OpenTime = bill._created;
            fBill.Discount = Number(bill.billDiscountAmount);
            fBill.TotalAmount = 0;
            fBill.FullCoupon = 0;
            fBill.IsEdited = false;
            fBill.TableNumber = bill._tableId;
            fBill.Covers = bill._covers || 0;
            _.forEach(bill._kots, function (kot) {
              if (!kot.isVoid) {
                fBill.Discount += Number(kot.totalDiscount);
                fBill.TotalAmount += (Number(kot.totalAmount));
                _.forEach(kot.taxes, function (tax) {
                  if (!fBill[tax.name]) {
                    fBill[tax.name] = 0;
                  }
                  fBill[tax.name] += parseFloat(tax.tax_amount);
                });
              }
            });

            _.forEach(bill.payments.cash, function (cash) {
              if (!fBill.Cash)
                fBill.Cash = 0;
              fBill.Cash += Number(cash);
            });

            _.forEach(bill.payments.cards, function (card) {
              if (card.cardType === "CreditCard") {
                if (!fBill.CreditCard)
                  fBill.CreditCard = 0;
                fBill.CreditCard += card.totalAmount;
              } else if (card.cardType === "DebitCard") {
                if (!fBill.DebitCard)
                  fBill.DebitCard = 0;
                fBill.DebitCard += card.totalAmount;
              } else if (card.cardType === "Other") {
                _.forEach(card.detail, function (detail) {
                  if (!fBill[detail.otherName + " " + detail.otherType])
                    fBill[detail.otherName + " " + detail.otherType] = 0;
                  fBill[detail.otherName + " " + detail.otherType] += Number(detail.amount);
                });
              }
            });
            formattedBills.push(fBill);
          });
          return res.status(200).json(formattedBills);
        });
      });
    } else {
      return handleError(res, {errorCode: 1, message: "Unaouthorized Access"});
    }
  }
};

exports.getBillsWithPagination = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getBillsWithPagination?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    var skip = req.query.skip;
    var limit = req.query.limit;
    var startDate = new Date(req.query.startDate);
    var endDate = new Date(req.query.endDate);
    endDate = new Date(endDate.setDate(endDate.getDate() + 1));
    var apiKey = req.query.apiKey;
    var deployment_id = req.query.deployment_id;
    var query = {
      deployment_id: deployment_id,
      _created: {
        $gte: moment(startDate).toDate(),
        $lte: moment(endDate).toDate()
      }
    }
    Bill.find(query, {}, {skip: skip, limit: limit, sort: {_created: 1}}, function (err, bills) {
      if (err)
        return handleError(res, err);
      return res.status(200).json(bills);
    });
  }

  Bill.find(query, {}, {skip: skip, limit: limit, sort: {_created: 1}}, function(err, bills) {
    if(err)
      return handleError(res, err);
    return res.status(200).json(bills);
  });
};


exports.transferBill = function (req, res) {

  var url = req.body.url;
  url = "https://" + "flipkart.tablogs.info" + "/api/bills/getBillsInDateRange"
  console.log(url);
  var apiKey = "1234";
  var bill = req.body.bill;
  var args = {
    headers: {"Content-Type": "application/json"},
    data: {
      apiKey: apiKey,
      bill: bill
    }
  };
  var temp = {
    startDate: '5/1/2016',
    endDate: '6/27/2016',
    deployment_id: "56fb8758d402a6dd7e846b6c"
  }
  /*request.post({url:url, form: temp}, function(err,httpResponse,body){
   console.log(err);
   console.log(httpResponse);
   console.log(body);
   })*/
  client.post(url, args, function (data, response) {
    // parsed response body as js object
    console.log(data);
    // raw response<td>{{t.bill.getKotsTotalBill() | number: 2}}</td>
    console.log(response);
  });
};

exports.receiveBill = function (req, res) {

  var apiKey = req.body.apiKey;
  var bill = req.body.bill;
  if (apiKey == "1234") {
    Bill.create(bill, function (err, nBill) {
      if (err)
        return handleError(res, err);
      return res.status(200).json(nBill);
    });
  } else return res.status(401).json({error: "Unauthorized"});
};

function getDateFormatted(datetime) {
  var cdate = new Date(datetime);
  var month = (cdate.getMonth() + 1).toString();
  var day = (cdate.getDate()).toString();
  var hrs = cdate.getHours().toString();
  var minutes = cdate.getMinutes().toString();
  var ss = cdate.getSeconds().toString();
  month = ((month.length == 1) ? "0" + month : month);
  day = ((day.length == 1) ? "0" + day : day);
  hrs = ((hrs.length == 1) ? "0" + hrs : hrs);
  minutes = ((minutes.length == 1) ? "0" + minutes : minutes);
  ss = ((ss.length == 1) ? "0" + ss : ss);
  //alert(day);
  return cdate.getFullYear() + "-" + month + "-" + day + " " + hrs + ":" + minutes + ":" + ss;
}
exports.getBillsByCustomer = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getBillsByCustomer?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    var param = {
      deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
      '_customer.customerId': req.query.customerId,
      isDeleted: {$ne: true}
    };
   // console.log(param)
    Bill.aggregate([
      {$match: param},
      {$unwind: "$_kots"},
      {
        $project: {
          kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
          //billDiscountAmount: "$billDiscountAmount",
          id: "$_id",
          payments: "$payments",
          created: "$_created",
          BillNumber: "$billNumber",
          serialNumber: "$serialNumber",
          daySerialNumber: "$daySerialNumber",
          billDiscountAmount: {$cond: ['$billDiscountAmount', '$billDiscountAmount', 0]},
          covers: "$_covers"
        }
      },
      {
        $group: {
          _id: "$id",//_id: {billId: "$serialNumber", billDiscountAmount: "$billDiscountAmount", customer: "$customer"}
          taxAmount: {$sum: "$kot.taxAmount"},
          totalDiscount: {$sum: "$kot.totalDiscount"},
          totalAmount: {$sum: "$kot.totalAmount"},
          billDiscountAmount: {$first: "$billDiscountAmount"},
          itemsArr: {$push: {items: "$kot.items"}},
          payments: {$first: "$payments"},
          created: {$first: "$created"},
          BillNumber: {$first: "$BillNumber"},
          serialNumber: {$first: "$serialNumber"},
          daySerialNumber: {$first: "$daySerialNumber"},
          covers: {$first: "$covers"}
        }
      },
      {$unwind: "$itemsArr"},
      {$unwind: "$itemsArr.items"},
      {
        $group: {
          _id: "$_id",
          items: {
            $push: {
              name: "$itemsArr.items.name",
              qty: "$itemsArr.items.quantity",
              catName: "$itemsArr.items.category.categoryName",
              addOns: "$itemsArr.items.addOns"
            }
          },
          taxAmount: {$first: "$taxAmount"},
          totalDiscount: {$first: "$totalDiscount"},
          totalAmount: {$first: "$totalAmount"},
          billDiscountAmount: {$first: "$billDiscountAmount"},
          payments: {$first: "$payments"},
          created: {$first: "$created"},
          BillNumber: {$first: "$BillNumber"},
          serialNumber: {$first: "$serialNumber"},
          daySerialNumber: {$first: "$daySerialNumber"},
          covers: {$first: "$covers"}
        }
      },
      {$sort: {created: -1}},
      {
        $project: {
          billId: "$_id",
          items: "$items",
          payments: "$payments",
          created: "$created",
          BillNumber: "$BillNumber",
          serialNumber: "$serialNumber",
          daySerialNumber: "$daySerialNumber",
          covers: "$covers",
          totalAmount: {$subtract: [{$add: ["$taxAmount", "$totalAmount"]}, {$add: ["$totalDiscount", "$billDiscountAmount"]}]}
        }
      }
    ], function (err, bills) {
      console.log(err)
      if (err) {
        return handleError(res, err);
      }
      Bill.aggregate([
        {$match: param},
        {$unwind: "$_kots"},
        {$unwind: "$_kots.items"},
        {
          $group: {
            _id: "$_kots.items._id",//_id: {billId: "$serialNumber", billDiscountAmount: "$billDiscountAmount", customer: "$customer"}
            count: {"$sum": 1},
            name: {"$first": "$_kots.items.name"}
          }
        },
        {$sort: {count: -1}},
        {$limit: 5},
        {
          $project: {
            _id: 0,
            name: 1,
            count: 1
          }
        }
      ], function (err, topItems) {
        if (err) {
          return handleError(res, err);
        }
        var result = {bills: bills, topItems: topItems}
        Bill.aggregate([
          {$match: param},
          {$unwind: "$_kots"},
          {$unwind: "$_kots.items"},
          {
            $group: {
              _id: "$_kots.items.category._id",//_id: {billId: "$serialNumber", billDiscountAmount: "$billDiscountAmount", customer: "$customer"}
              count: {"$sum": 1},
              name: {"$first": "$_kots.items.category.categoryName"}
            }
          },
          {$sort: {count: -1}},
          {$limit: 5},
          {
            $project: {
              _id: 0,
              name: 1,
              count: 1
            }
          }
        ], function (err, topCategories) {
          if (err) {
            return handleError(res, err);
          }
          result.topCategories = topCategories;
          return res.json(200, result);
        })
      })
    });
  }
};

exports.getAllBills = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getAllBills?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    console.log(req.query)
    var query = {
      deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
      isDeleted: {$ne: true},
      _customer: {$exists: true},
      "_customer.mobile": {$exists: true}
    }
    if (req.query.customerId != undefined) {
      query["_customer.customerId"] = req.query.customerId;
    }
    if (req.query.startDate != undefined) {
      query._created = {
        $gte: moment(new Date(req.query.startDate)).toDate(), $lte: moment(new Date(req.query.endDate)).toDate()
      }
    }
    if (req.query.mobile != undefined) {
      query["_customer.mobile"] = new RegExp(req.query.mobile, 'i');
    }
    if (req.query.name != undefined) {
      query["_customer.firstname"] = new RegExp(req.query.name, 'i')
    }
    if (req.query.address != undefined) {
      query["$or"] = [
        {"_customer.address1": new RegExp(req.query.address, 'i')},
        {"_customer.address2": new RegExp(req.query.address, 'i')},
        {"_customer.city": new RegExp(req.query.address, 'i')},
        {"_customer.state": new RegExp(req.query.address, 'i')}
      ]
    }
    if (req.query.dob) {
      query["_customer.DOB"] = req.query.dob
    }
    if (req.query.anniversary) {
      query["_customer.MA"] = req.query.anniversary
    }
    var condition2 = {};
    if (req.query.amountValue) {
      condition2.amount = (req.query.amountType == "=") ? {$eq: parseFloat(req.query.amountValue)} : (req.query.amountType == "<") ? {$lt: parseFloat(req.query.amountValue)} : (req.query.amountType == ">") ? {$gt: parseFloat(req.query.amountValue)} : (req.query.amountType == "<=") ? {$lte: parseFloat(req.query.amountValue)} : {$gte: parseFloat(req.query.amountValue)};
    }
    if (req.query.frequencyValue) {
      condition2.frequency = (req.query.frequencyType == "=") ? {$eq: parseFloat(req.query.frequencyValue)} : (req.query.frequencyType == "<") ? {$lt: parseFloat(req.query.frequencyValue)} : (req.query.frequencyType == ">") ? {$gt: parseFloat(req.query.frequencyValue)} : (req.query.frequencyType == "<=") ? {$lte: parseFloat(req.query.frequencyValue)} : {$gte: parseFloat(req.query.frequencyValue)};
    }
    var skipRecords = parseInt(req.query.skip) * parseInt(req.query.limit);
    console.log(skipRecords);
    console.log("query", query, skipRecords);
    Bill.aggregate([
      {$match: query},
      {$unwind: "$_kots"},
      {
        $project: {
          kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
          id: "$_id",
          _customer: "$_customer",
          created: "$_created",
          BillNumber: "$billNumber",
          daySerialNumber: "$daySerialNumber",
          serialNumber: "$serialNumber",
          covers: {$cond: ['$_covers', '$_covers', 0]},
          tab: "$tab"
        }
      },
      {
        $group: {
          _id: "$id",
          itemsArr: {$push: {items: "$kot.items"}},
          _customer: {$first: "$_customer"},
          created: {$first: "$created"},
          BillNumber: {$first: "$BillNumber"},
          serialNumber: {$first: "$serialNumber"},
          daySerialNumber: {$first: "$daySerialNumber"},
          covers: {$first: "$covers"},
          tab: {$first: "$tab"}
        }
      },
      {$unwind: "$itemsArr"},
      {$unwind: "$itemsArr.items"},
      {
        $project: {
          name: "$_customer.firstname",
          mobile: "$_customer.mobile",
          address1: "$_customer.address1",
          address2: "$_customer.address2",
          city: "$_customer.city",
          state: "$_customer.state",
          postCode: "$_customer.postCode",
          email: "$_customer.email",
          item: "$itemsArr.items.name",
          qty: "$itemsArr.items.quantity",
          catName: "$itemsArr.items.category.categoryName",
          rate: "$itemsArr.items.rate",
          priceQuantity: "$itemsArr.items.priceQuantity",
          addOns: "$itemsArr.items.addOns",
          created: "$created",
          BillNumber: "$BillNumber",
          daySerialNumber: "$daySerialNumber",
          serialNumber: "$serialNumber",
          covers: "$covers",
          tab: "$tab"
        }
      },
      {$sort: {created: -1}},
      {$skip: skipRecords},
      {$limit: parseInt(req.query.limit)}

    ], function (err, bills) {
      console.log(err)
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, bills);
    });
  }
}

//add this code to bill controller
exports.getItemWiseConsolidate = function (req, res,cb) {

  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getItemWiseConsolidate', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getItemWiseConsolidate',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.itemWiseConsolidateAggregation(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};
//bill controller
exports.getItemWiseConsolidateForProfitSharing = function (req, res,cb) {

  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getItemWiseConsolidateForProfitSharing',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getItemWiseConsolidateForProfitSharing',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    console.log('getItemWConsolidate');
    // console.log(req.body.arg);
    reportApi.ItemWiseConsolidateForProfitSharing(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
    cb(bills)
  else
     res.json(200, bills);
  });
  }
}

exports.getBillsInDateRangeForDeliveryReport = function(req, res,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getBillsInDateRangeForDeliveryReport', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getBillsInDateRangeForDeliveryReport',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.deliveryBoy(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getBillConsolidate = function (req, res,cb) {

  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getBillConsolidate', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getBillConsolidate',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.billConslidateAggregation(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
}
//bill controller
exports.getDataRangeBillsForInvoice = function (req, res,cb) {
  /* console.log(req.query);
   console.log('Hello');*/
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getDataRangeBillsForInvoice?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        return res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.query.backend === 'true'){
    var reqBreak = {};
    var fdate = moment(req.query.fromDate);
    var tdate = moment(req.query.toDate);
    tdate = tdate.add(1, 'days');
    var query = {
      deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
      _created: {
        $gte: new Date(fdate), $lte: new Date(tdate)
      },
      isDeleted: {$ne: true}
    };
    if (req.query.complimentary != undefined) {
      if (req.query.complimentary == 'true') {
        query = {
          complimentary: req.query.complimentary,
          deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
          _created: {
            $gte: new Date(fdate), $lte: new Date(tdate)
          },
          isDeleted: {$ne: true}
        }
      }
    }
    dateBreakupBalancing({type:'bill', fromDate:req.query.fromDate, toDate:req.query.toDate, query:query}).then(function(result){
      // console.log(result)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.query.fromDate = moment(date.fromDate)
          reqBreak.query.toDate = moment(date.toDate)
          queue.create('getDataRangeBillsForInvoice',reqBreak.query).attempts(3).removeOnComplete(false).save(function(err){
            cb()
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getDataRangeBillsForInvoice',req.query).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    }).catch(function(err){
      if(err)
        return res.json(404, {error: 'error in data'})
    })
  } else {
     reportApi.forInvoiceReport(req,res).then(function (bills) {
      if(req.query.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getDataRangeBillsForKot = function (req, res,cb) {
  if (config.isQueue) {

    request(config.reportServer.url + '/api/bills/getDataRangeBillsForKot?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.query.backend === 'true') {
    var fdate = moment(req.query.fromDate);
    var tdate = moment(req.query.toDate);
    tdate = tdate.add(1, 'days');
    var reqBreak = {};
    var query = {
      tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
      deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
      _created: {
        $gte: new Date(fdate), $lte: new Date(tdate)
      },
      isDeleted: {$ne: true}
    };
    if (req.query.complimentary != undefined) {
      // console.log(req.query.complimentary);
      if (req.query.complimentary == 'true') {
        query = {
          complimentary: req.query.complimentary,
          tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
          deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
          _created: {
            $gte: new Date(fdate), $lte: new Date(tdate)
          },
          isDeleted: {$ne: true}
        }
      }
    }
    dateBreakupBalancing({type:'kot', fromDate:req.query.fromDate, toDate:req.query.toDate, query:query}).then(function(result){
      // console.log(result)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.query.fromDate = moment(date.fromDate)
          reqBreak.query.toDate = moment(date.toDate)
          queue.create('getDataRangeBillsForKot',reqBreak.query).attempts(3).removeOnComplete(false).save(function(err){
            cb()
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getDataRangeBillsForKot',req.query).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    }).catch(function(err){
      if(err)
        return res.json(404, {error: 'error in data'})
    })
  } else {
    reportApi.forKotReport(req,res).then(function (bills) {
      if(req.query.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getBillConsolidateForHourlySales = function (req, res,cb) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillConsolidateForHourlySales',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    ////req.query.qId=new mongoose.Types.ObjectId();
    //res.json(200, []);
    var job = queue.create('getBillConsolidateForHourlySales',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      
    });
  } else {
    // console.log(req.body.arg);
    reportApi.hourlyAggregation(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
       res.json(200, bills);
    });
  }
}
exports.getBillsInDateRangeForCustomerData = function (req, res,cb) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillsInDateRangeForCustomerData',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    ////req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getBillsInDateRangeForCustomerData',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
     // res.json(200, []);
    });
  } else {
    reportApi.customerDataReport(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
    cb(bills)
  else
     res.json(200, bills);
  });
  }   
};

exports.getBillsInDateRangeForDelivery = function (req, res,cb) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillsInDateRangeForDelivery',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
   // //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getBillsInDateRangeForDelivery',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
     // res.json(200, []);
    });
  } else {

    reportApi.deliveryReport(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
  });
  }
};


exports.getBillsInDateRangeForDailySales = function (req, res,cb) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillsInDateRangeForDailySales',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var reqBreak = {};
    var dep = req.body.arg.deployment_id ;
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id:new mongoose.Types.ObjectId(dep),
      isDeleted: {$ne: true},
    }
    dateBreakupBalancing({type:'bill', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getBillsInDateRangeForDailySales',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getBillsInDateRangeForDailySales',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  } else {
   reportApi.dailySalesAggregation(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
}

exports.getBillsInDateRangeForTenantWise = function(req, res){
  if(config.isQueue){

    request.post(config.reportServer.url+'/api/bills/getBillsInDateRangeForTenantWise',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  }
  else{

    var startDate = new Date(req.body.arg.startDate);
    var endDate = new Date(req.body.arg.endDate);
    endDate = endDate.setDate(endDate.getDate() + 1);
    var tenant_id = new mongoose.Types.ObjectId(req.body.arg.tenant_id);
    Bill.aggregate([
      {
        $match:{

          _created: {
            $gte: moment(startDate).toDate(),
            $lte: moment(endDate).toDate()
          },
          tenant_id: tenant_id,
          isDeleted: {$ne: true},
          isVoid:{$ne:true},


        }},
      {
        $unwind:"$_kots"
      },
      {
        $project:{
          totalNet: {$cond: ['$_kots.isVoid', 0, {$subtract: [{$add: ["$_kots.totalAmount", "$_kots.taxAmount"]}, "$_kots.totalDiscount"]}]},
          _created: "$_created",
          deployment_id:"$deployment_id",
          tenant_id:"$tenant_id",
          _id:"$_id",
          billDiscountAmount:{$ifNull:["$billDiscountAmount",0]},
          createdAtYear: { $year: "$_created" },
          createdAtMonth: { $month: "$_created" }
        },

      },
      {
        $group:{
          _id:{_id:"$_id",createdAtYear:"$createdAtYear",deployment_id:"$deployment_id",billDiscountAmount:"$billDiscountAmount",createdAtMonth:"$createdAtMonth"},
          totalNet:{$sum:"$totalNet"}
        }
      },
      {$project:{
        _id:0,
        deployment_id:"$_id.deployment_id",
        createdAtMonth:"$_id.createdAtMonth",
        createdAtYear:"$_id.createdAtYear",
        totalNet:{$subtract: ["$totalNet", "$_id.billDiscountAmount"]},

      }},
      {
        $group:{
          _id:{
            deployment_id:"$deployment_id",createdAtMonth:"$createdAtMonth",createdAtYear:"$createdAtYear"
          }
          ,

          totalNet:{$sum:"$totalNet"}
        }
      },
      {
        $project:{
          _id:0,
          deployment_id:"$_id.deployment_id",
          createdAtMonth:"$_id.createdAtMonth",
          createdAtYear:"$_id.createdAtYear",
          totalNet:"$totalNet"

        }
      }


    ] ,function (err, bills) {

      if(err) {
        return handleError(res, err);
      }
      return res.status(200).json(bills);
    });
   }
};

exports.getBillConsolidateForKotDelete=function(req,res,cb){
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillConsolidateForKotDelete',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var dep = req.body.arg.deployment_id ;
    var reqBreak = {};
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id:new mongoose.Types.ObjectId(dep),
      isDeleted: {$ne: true},
      $or: [ {"isVoid": true}, {"_kots.isVoid":true},
        {"_kots.deleteHistory.0": {$exists: true}}]
    }
    dateBreakupBalancing({type:'kot', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getBillConsolidateForKotDelete',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getBillConsolidateForKotDelete',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  }  else {
    // console.log(req.body.arg);
     reportApi.kotDelete(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
  });
   
  }
};

exports.getBillConsolidateForKotTracking=function(req,res,cb){

  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillConsolidateForKotTracking',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    var reqBreak = {};
    var fdate=moment(req.body.arg.startDate);
    var tdate=moment(req.body.arg.endDate);
        tdate = tdate.add(1,'days');
    var dep = req.body.arg.deployment_id ;
    var query = {
      _created: {
        $gte: moment(fdate).toDate(),
        $lte: moment(tdate).toDate()
      },
      deployment_id:new mongoose.Types.ObjectId(dep),
      isDeleted: {$ne: true},
    }
    dateBreakupBalancing({type:'kot', fromDate:req.body.arg.startDate, toDate:req.body.arg.endDate, query:query}).then(function(result){
      console.log(result.length)
      if(result.length > 1) {
        async.eachSeries(result,function(date,cb){
          reqBreak = {}
          reqBreak = req
          reqBreak.body.arg.startDate = moment(date.fromDate)
          reqBreak.body.arg.endDate = moment(date.toDate)
          //console.log(reqBreak.body)
          queue.create('getBillConsolidateForKotTracking',{form:reqBreak.body}).attempts(3).removeOnComplete(false).save(function(err){
            cb();
          });
        },function(err){
          if(err)
            console.log(err)
        })
        res.json(200,[])
      } else {
        var job = queue.create('getBillConsolidateForKotTracking',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
          res.json(200, []);
        });
      }
    })
  } else {
    // console.log(req.body.arg);
     reportApi.kotTracking(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
  });
   
   
  }
};

exports.getBillsInDateRangeForTenantWise = function(req, res) {

  var startDate = new Date(req.body.arg.startDate);
  var endDate = new Date(req.body.arg.endDate);
  endDate = endDate.setDate(endDate.getDate() + 1);
  var tenant_id = new mongoose.Types.ObjectId(req.body.arg.tenant_id);

  Deployment.aggregate([
    {
      $match: {
        tenant_id: tenant_id,
        isBaseKitchen: {$ne: true}
      }
    },
    {
      $unwind: "$settings"
    },
    {
      $match: {
        'settings.name': 'day_sale_cutoff'
      }
    },
    {
      $project: {
        deployName:"$name",
        deployment_id: "$_id",
        tenant_id: "$tenant_id",
        value: "$settings.value",
        name: "$settings.name"
      }
    }
  ], function (err, deployment) {
    if (err) {
      return handleError(res, err);
    }
    //return res.status(200).json(bills);


    _.forEach(deployment,function(dep){
      dep.fromDate =  new Date(utils.getDateFormatted(utils.getResetDateForTenant(dep.value, utils.convertDate(startDate)))),
        dep.toDate = new Date(utils.getDateFormatted(utils.getResetDateForTenant(dep.value, utils.convertDate(endDate))))
    });
    console.log(deployment);
    Bill.aggregate([
        {
          $match: {
            _created: {
              $gte: moment(startDate).toDate(),
              $lte: moment(endDate).toDate()
            },
            tenant_id: tenant_id,
            isDeleted: {$ne: true},
            isVoid: {$ne: true},


          }
        },
        {
          $unwind: "$_kots"
        },
        {
          $project: {
            totalNet: {$cond: ['$_kots.isVoid', 0, {$subtract: [{$add: ["$_kots.totalAmount", "$_kots.taxAmount"]}, "$_kots.totalDiscount"]}]},
            _created: "$_created",
            deployment_id: "$deployment_id",
            tenant_id: "$tenant_id",
            _id: "$_id",
            billDiscountAmount: {$ifNull: ["$billDiscountAmount", 0]},
            createdAtYear: {$year: "$_created"},
            createdAtMonth: {$month: "$_created"},
            chargesAmount:  { $ifNull: [ "$charges.amount", 0 ] }
          },

        },
        {
          $group: {
            _id: {
              _id: "$_id",
              createdAtYear: "$createdAtYear",
              deployment_id: "$deployment_id",
              billDiscountAmount: "$billDiscountAmount",
              createdAtMonth: "$createdAtMonth"
            },

            chargesAmount:{$first:'$chargesAmount'},
            totalNet: {$sum: "$totalNet"}

          }

        },
        {
          $project: {
            _id: 0,
            deployment_id: "$_id.deployment_id",
            createdAtMonth: "$_id.createdAtMonth",
            createdAtYear: "$_id.createdAtYear",
             totalNet:{$subtract:[{$add:["$totalNet","$chargesAmount"]},"$_id.billDiscountAmount"]},

          }
        },
        {
          $group: {
            _id: {
              deployment_id: "$deployment_id", createdAtMonth: "$createdAtMonth", createdAtYear: "$createdAtYear"
            }
            ,

            totalNet: {$sum: "$totalNet"}
          }
        },
        {
          $project: {
            _id: 0,
            deployment_id: "$_id.deployment_id",
            createdAtMonth: "$_id.createdAtMonth",
            createdAtYear: "$_id.createdAtYear",
            totalNet: "$totalNet"

          }
        }


      ], function (err, bills) {

        if (err) {
          return handleError(res, err);
        }
        return res.status(200).json(bills);
      }
    )
  })
};

exports.getAggregatedOtherPaymentsDetail = function(req, res,cb) {
  if(config.isQueue){
    request(config.reportServer.url+'/api/bills/getAggregatedOtherPaymentsDetail?'+utils.createGetUrl(req.query),function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.query.backend === 'true') {
    ////req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getAggregatedOtherPaymentsDetail',req.query).attempts(3).removeOnComplete(false).save(function(err){
     res.json(200, []);
    });
  } else {
    //console.log('getAggregatedOtherPaymentsDetail', req.query);
    reportApi.aggregateOtherPaymentDetail(req,res).then(function (bills) {
   if(req.query.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
  });
   
   
  }
};

exports.getAggregatedOtherPaymentsConsolidated = function (req, res,cb) {
  if(config.isQueue){
    request(config.reportServer.url+'/api/bills/getAggregatedOtherPaymentsConsolidated?'+utils.createGetUrl(req.query),function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  }else if(req.query.backend === 'true') {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getAggregatedOtherPaymentsConsolidated',req.query).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
     reportApi.aggregateConsolidate(req,res).then(function (bills) {
   if(req.query.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
  });
  }
}

exports.getBillsInDateRangeForCompareWaiters = function(req, res){

  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillsInDateRangeForCompareWaiters',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  }
  else {
    var startDate = new Date(req.body.arg.startDate);
    var endDate = new Date(req.body.arg.endDate);
    endDate = endDate.setDate(endDate.getDate() + 1);
    var deployment_id = new mongoose.Types.ObjectId(req.body.arg.deployment_id);
    Bill.find({
      _created: {
        $gte: moment(startDate).toDate(),
        $lte: moment(endDate).toDate()
      },
      deployment_id: deployment_id
      //isDeleted: {$ne: true},
       //isVoid: {$ne: true}*/
    },{
      isVoid:1,
      _created: 1,
      tabType: 1,
      tab: 1,
      _closeTime:1,
      '_waiter._id':1,
      '_waiter.firstname':1,
      '_waiter.lastname':1,
      prefix:1,
      billNumber:1,
      daySerialNumber:1,
      serialNumber:1,
      splitNumber: 1,
      payments:1,
      _tableId:1,
      _covers:1,
      '_kots.isVoid':1,
      '_kots.items.name': 1,
      '_kots.items._id': 1,
      '_kots.items.quantity': 1,
      '_kots.items.subtotal': 1,
      '_kots.items.rate': 1,
      '_kots.items.category.categoryName':1,
      '_kots.items.category.superCategory.superCategoryName':1,
      '_kots.items.taxes._id':1,
      '_kots.items.taxes.name':1,
      '_kots.items.taxes.tax_amount':1,
      '_kots.items.taxes.baseRate_preTax':1,
      '_kots.items.taxes.cascadingTaxes._id':1,
      '_kots.items.taxes.cascadingTaxes.name':1,
      '_kots.items.taxes.cascadingTaxes.tax_amount':1,
      '_kots.items.taxes.cascadingTaxes.selected':1,
      '_kots.items.taxes.cascadingTaxes.baseRate_preTax':1,
      '_kots.items.addOns._id':1,
      '_kots.items.addOns.name':1,
      '_kots.items.addOns.rate':1,
      '_kots.items.addOns.subtotal':1,
      '_kots.items.addOns.quantity':1,
      '_kots.items.addOns.category.categoryName':1,
      '_kots.items.addOns.category.superCategory.superCategoryName':1,
      '_kots.items.addOns.taxes._id':1,
      '_kots.items.addOns.taxes.name':1,
      '_kots.items.addOns.taxes.tax_amount':1,
      '_kots.items.addOns.taxes.baseRate_preTax':1,
      '_kots.items.addOns.taxes.cascadingTaxes.baseRate_preTax':1,
      '_kots.items.addOns.taxes.cascadingTaxes._id':1,
      '_kots.items.addOns.taxes.cascadingTaxes.name':1,
      '_kots.items.addOns.taxes.cascadingTaxes.selected':1,
      '_kots.items.addOns.taxes.cascadingTaxes.tax_amount':1,
      '_kots.items.discounts.discountAmount':1,
      '_kots.items.discounts.type':1,
      '_kots.items.discount.value':1,
      '_kots.items.isDiscounted':1,
      '_kots.deleteHistory.voidItemTime':1,
      '_kots.deleteHistory.name':1,
      '_kots.deleteHistory.rate':1,
      '_kots.deleteHistory.subtotal':1,
      '_kots.deleteHistory.quantity':1,
      '_kots.deleteHistory.addOns.subtotal':1,
      '_kots.deleteHistory.addOns.name':1,
      '_kots.deleteHistory.addOns.rate':1,
      '_kots.deleteHistory.addOns.quantity':1,
      'instance.name':1,
      'instance.prefix':1,
      billDiscountAmount:1,
      charges:1


    }, function (err, bills) {

      if(err) {
        return handleError(res, err);
      }
      return res.status(200).json(bills);
    });
  }
};

/* Added By KshitijK */
exports.getRemovedTaxes = function(req,res,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getRemovedTaxes', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'error in data'})
      } else {
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getRemovedTaxes',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
   reportApi.removeTaxes(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
  });
  }
}

exports.getComplimentaryHeadwise = function(req,res,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getComplimentaryHeadwise', {form: req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'error in data'})
      } else {
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getComplimentaryHeadwise',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
     reportApi.complimentaryHeadwise(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
       res.json(200, bills);
    });
  }
}

exports.getDataRangeBillsForDailySalesInvoice = function (req, res, cb) {
  // console.log(req.body.arg);
  // console.log('Hello');
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getDataRangeBillsForDailySalesInvoice?', {form:req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        return res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getDataRangeBillsForDailySalesInvoice',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.forDailySalesInvoice(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
      res.json(200, bills);
    });
  }
};

exports.getDataForMenuMix = function(req,res,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getDataForMenuMix?', {form:req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        return res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getDataForMenuMix',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.forItemMenuMix(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getDataForEmployeePerformance = function(req,res,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getDataForEmployeePerformance?', {form:req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        return res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getDataForEmployeePerformance',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    reportApi.forEmployeePerformance(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getDataForEnterpriseSettlement = function(req,res,cb) {
  if (config.isQueue) {
    request.post(config.reportServer.url + '/api/bills/getDataForEnterpriseSettlement?', {form:req.body}, function (error, response, body) {
      if (error) {
        console.log(error);
        return res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getDataForEnterpriseSettlement',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    Q.all([
      reportApi.forEnterpriseSettlement(req,res)
      // reportApi.forEnterpriseTaxes(req,res)
    ]).then(function (bills) {
      // _.forEach(bills[0], function(bill) {
      //   var taxes = _.filter(bills[1], {_id:bill.deployment_id});
      //   if(taxes.length > 0) {
      //     bill.taxes = taxes[0].tax;
      //   }
      // })
      if(req.body.arg.backend === 'csvTrue')
        cb(bills[0])
      else
        res.json(200, bills[0]);
    });
  }
};
/*End*/

exports.getSettlement = function (req, res,cb) {
  console.log('inside')
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getSettlement',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getSettlement',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
   reportApi.settlementReport(req,res).then(function (bills) {
    if(req.body.arg.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
    });
  }
}

exports.getAggregatedCouponDetail = function(req, res,cb) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getAggregatedCouponDetail',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getAggregatedCouponDetail',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    //console.log('getAggregatedOtherPaymentsDetail', req.query);
     reportApi.couponDetail(req,res).then(function (bills) {
   if(req.body.arg.backend === 'csvTrue')
      cb(bills)
    else
     res.json(200, bills);
  });
   
  }
};

exports.getDataRangeBillsForBoh = function(req, res,cb) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getDataRangeBillsForBoh',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.body.arg.backend === 'true' || req.body.arg.backend === true) {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getDataRangeBillsForBoh',{form:req.body}).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    //console.log('getAggregatedOtherPaymentsDetail', req.query);
    reportApi.bohReport(req,res).then(function (bills) {
      if(req.body.arg.backend === 'csvTrue')
        cb(bills)
      else
        res.json(200, bills);
    });
  }
};

exports.getAggregatedNewOtherPaymentsDetail = function(req, res,cb) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/bills/getAggregatedNewOtherPaymentsDetail?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'error in data'})
      } else {
        return res.json(200, JSON.parse(body));
      }
    })
  } else if(req.query.backend === 'true') {
    //req.query.qId=new mongoose.Types.ObjectId();
    var job = queue.create('getAggregatedNewOtherPaymentsDetail',req.query).attempts(3).removeOnComplete(false).save(function(err){
      res.json(200, []);
    });
  } else {
    // console.log('getAggregatedOtherPaymentsDetail', req.query);
    reportApi.newOtherPayment(req,res).then(function (bills) {
      if(req.query.backend === 'csvTrue')
       cb(bills)
      else
        res.json(200, bills);
    });
  }
};

function getResetDate(settings, date) {
  var resetDate = new Date(date);
  var hr = 0;
  for (var i = 0; i < settings.length; i++) {
    if (settings[i].name == 'day_sale_cutoff') {
      hr = new Date(settings[i].value).getHours();
      break;
    }
  }
  resetDate.setHours(hr, 0, 0, 0);
  return resetDate;
}

exports.getBillsInDateRangeForAdvanceBooking = function (req, res) {
  if(config.isQueue){
    request.post(config.reportServer.url+'/api/bills/getBillsInDateRangeForAdvanceBooking',{form:req.body},function(error,response,body){
      if(error){
        console.log(error);
        res.json(404,{error:'eeror in data'})
      }else{
       // console.log(body);
        return res.json(200, JSON.parse(body));
       }
    })
  } else {
    reportApi.forAdvanceBooking(req,res).then(function (bills) {
      if(req.query.backend === 'csvTrue')
       cb(bills)
      else
        res.json(200, bills);
    }); 
  }
};

var emailSend = function(body,cb){
  console.log(body);
  server.send({
    from: "Posist Info <no-reply@posist.info>",
    to: body.email,
    subject: "Max Cap Exceeded",
    attachment: [
      {data: 'Dear Client<br>The Report You Requested Has Exceeded The Max Capacity, Hence It Will Be Broken Into A series Of Reports And Mailed To You<br><table border="1"><tr><th>No. Of Report</th><td>'+body.number+'</td><td>'+body.report+'</td></tr></table>' , alternative: true},
    ]
  },function(result){
    cb(null,"SUCCESS")
  },function(err){
    cb(err,"FAIL")
  })
};

var dateBreakupBalancing = function(data){
  var d = Q.defer();
  var dateBreakup = [];
  if(data.type == 'bill') {
    Bill.count(data.query, function(err,count){
      if(err) {
        d.reject(err)
      } else {
        dateBreakup = dateBreaker(data.fromDate, data.toDate, count)
        d.resolve(dateBreakup);
      }
    });
  } else {
    console.log(data.query)
    Bill.aggregate([
      {
        $match:data.query
    },{
      $unwind:'$_kots'
    },{
      $unwind:'$_kots.items'
    },{
        $count:'no'
    }], function(err,count){
      // console.log(count)
      if(err) {
        d.reject(err)
      } else {
        dateBreakup = dateBreaker(data.fromDate, data.toDate, count[0].no)
        d.resolve(dateBreakup);
      }
    });
  }
  return d.promise
}
var dateBreaker = function(fromDate, toDate, count) {
  var duration = moment.duration(moment(toDate).diff(moment(fromDate))).asMonths().toFixed(1);
  var ratio = Number(((config.max_cap*duration)/count).toFixed(1));
  var dataBreakup = [];
  console.log(count);
  console.log(duration)
  console.log(ratio)
  if(count>config.max_cap && ratio > 0.5 && duration != 0) {
    ratio = Number(ratio.toFixed(0));
    var n = Number((duration/ratio).toFixed(1));
    var newMonth = '';
    var num = 0;
    async.whilst(function(){
      return num<n;
    }, function(cb){
      if(num == 0) {
        dataBreakup.push({
          fromDate: new Date(moment(fromDate)),
          toDate: new Date(moment(fromDate).add(ratio,'month').startOf('month').set('hour', moment(fromDate).get('hour')).set('minute', moment(fromDate).get('minute')).set('seconds',0).set('milliseconds',0).subtract(1,'days'))
        });
        newMonth = new Date(moment(fromDate).add(ratio,'month').startOf('month').set('hour', moment(fromDate).get('hour')).set('minute', moment(fromDate).get('minute')).set('seconds',0).set('milliseconds',0));
        num=num+1;
        cb()
      } else if(num < n-1) {
        dataBreakup.push({
          fromDate: new Date(moment(newMonth)),
          toDate: new Date(moment(newMonth).add(ratio,'month').startOf('month').set('hour', moment(fromDate).get('hour')).set('minute', moment(fromDate).get('minute')).set('seconds',0).set('milliseconds',0).subtract(1,'days'))
        });
        num=num+1;
        newMonth = new Date(moment(newMonth).add(ratio,'month').startOf('month').set('hour', moment(fromDate).get('hour')).set('minute', moment(fromDate).get('minute')).set('seconds',0).set('milliseconds',0))
        cb()
      } else {
        var a = moment.duration(moment(newMonth).diff(moment(newMonth).add(ratio,'month'))).asMonths()
        var b = moment.duration(moment(newMonth).diff(moment(toDate))).asMonths()
        if( a > b && (a+1) < b) {
          dataBreakup.push({
            fromDate: new Date(moment(newMonth)),
            toDate: new Date(moment(newMonth).add(ratio,'month').startOf('month').set('hour', moment(fromDate).get('hour')).set('minute', moment(fromDate).get('minute')).set('seconds',0).set('milliseconds',0).subtract(1,'days'))
          });
          dataBreakup.push({
            fromDate: new Date(moment(newMonth).add(ratio,'month').set('hour', moment(fromDate).get('hour')).set('minute', moment(fromDate).get('minute')).set('seconds',0).set('milliseconds',0)),
            toDate: new Date(moment(toDate))
          });
          num=num+1
          cb()
        } else {
          dataBreakup.push({
            fromDate: new Date(moment(newMonth)),
            toDate: new Date(moment(toDate))
          });
          num=num+1
          cb()
        }
      }
    }, function(err) {
      if(err)
        console.log(err)
    })
    // console.log(dataBreakup)
    return dataBreakup
  } else {
    return [{fromDate:fromDate, toDate:toDate}]
  }
}
/*   Kanishk report made for Instances end here*/
function handleError(res, err) {
  return res.send(500, err);
}