'use strict';

var express = require('express');
var controller = require('./bill.controller');

var router = express.Router();
router.get('/triggerSupportApp', controller.triggerSupportApp);
router.get('/billsAmount', controller.billsAmountCount);
router.get('/CRMDetailFromBill', controller.CRMDetailFromBill);
router.get('/billsTotalAmount', controller.billsTotalAmount);
router.get('/sendSMS', controller.sendSMS);
router.get('/getDataRangeBillsForInvoice', controller.getDataRangeBillsForInvoice );
router.get('/getDataRangeBillsForKot', controller.getDataRangeBillsForKot);
router.post('/getBillConsolidateForKotDelete', controller.getBillConsolidateForKotDelete);
router.post('/getBillConsolidateForKotTracking', controller.getBillConsolidateForKotTracking);
router.get('/sendSaleSMS', controller.sendSaleSMS);
router.post('/getItemWiseEnterprise', controller.getItemWiseEnterprise);
router.post('/getItemWiseConsolidate',controller.getItemWiseConsolidate);
router.post('/getItemWiseConsolidateForProfitSharing',controller.getItemWiseConsolidateForProfitSharing);
router.get('/markDeleteBill',controller.markDeleteBill);
router.get('/billsBackup', controller.billsBackUp);
router.post('/billsRestore', controller.billsRestore);
router.get('/billsForManipulation', controller.billsForManipulation);
router.get('/getBillsByCustomer', controller.getBillsByCustomer);
router.get('/getAllBills', controller.getAllBills);
router.get('/', controller.index);
router.get('/getAggregatedNewOtherPaymentsDetail', controller.getAggregatedNewOtherPaymentsDetail);
router.get('/getDataRangeBillsNew', controller.getDataRangeBillsNew);
router.get('/getDataRangeBills', controller.getDataRangeBills);
router.get('/getCustomerCount', controller.getCustomerCount);
router.get('/getInvoicePartner', controller.getInvoicePartner);
router.get('/getSuperCategories', controller.getSuperCategories);
router.get('/getCategories', controller.getCategories);
router.get('/getBillsWithPagination', controller.getBillsWithPagination);
router.get('/getAggregatedOtherPaymentsDetail', controller.getAggregatedOtherPaymentsDetail);
router.get('/getAggregatedOtherPaymentsConsolidated', controller.getAggregatedOtherPaymentsConsolidated);
router.get('/getAggregatedNewOtherPaymentsDetail', controller.getAggregatedNewOtherPaymentsDetail);

router.get('/:id', controller.show);
router.get('/getLatestBill/:tenantId/:deploymentId', controller.getLatestBill);
router.get('/billItemsCount/:tenantId/:deploymentId', controller.billItemsCount);


router.post('/', controller.create);
router.post('/updateBillSet', controller.updateBillSet);
router.post('/updateBill', controller.updateBill);
router.post('/updateById', controller.updateById);
//Kanish api for report
router.post('/getWaiterBillsInDateRange', controller.getWaiterBillsInDateRange);
router.post('/getComplimentaryConsolidated', controller.getComplimentaryConsolidated);
router.post('/getComplimentary', controller.getComplimentary);
router.post('/getConsolidatedOtherPayments', controller.getConsolidatedOtherPayments);
router.post('/getDiscountReports', controller.getDiscountReports);
router.post('/getInstanceWiseReports', controller.getInstanceWiseReports);
router.post('/getReprintReports', controller.getReprintReports);
router.post('/getBillsInDateRange', controller.getBillsInDateRange);
router.post('/getBillsInDateRangeForDelivery', controller.getBillsInDateRangeForDelivery);
router.post('/getBillsInDateRangeForDeliveryReport', controller.getBillsInDateRangeForDeliveryReport);
router.post('/getBillsInDateRangeForOtherPayment', controller.getBillsInDateRangeForOtherPayment);
router.post('/getBillsInDateRangeForCustomerData', controller.getBillsInDateRangeForCustomerData);
router.post('/getBillsInDateRangeForAdvanceBooking', controller.getBillsInDateRangeForAdvanceBooking);
router.post('/getBillsInDateRangeForCompareWaiters', controller.getBillsInDateRangeForCompareWaiters);
router.post('/getBillConsolidate', controller.getBillConsolidate);
router.post('/getBillConsolidateForHourlySales', controller.getBillConsolidateForHourlySales);
router.post('/getBillsInDateRangeForDailySales', controller.getBillsInDateRangeForDailySales);
router.post('/getBillsInDateRangeForTenantWise', controller.getBillsInDateRangeForTenantWise);
router.post('/transferBill', controller.transferBill);
router.post('/getBillsInDateRangeForTenantWise', controller.getBillsInDateRangeForTenantWise);
router.post('/getBillsInDateRangeForCompareWaiters', controller.getBillsInDateRangeForCompareWaiters);
router.post('/getBillsInDateRangeForDailySales', controller.getBillsInDateRangeForDailySales);
router.post('/getBillConsolidateForKotDelete', controller.getBillConsolidateForKotDelete);
router.post('/getBillConsolidateForKotTracking', controller.getBillConsolidateForKotTracking);
router.post('/getRemovedTaxes', controller.getRemovedTaxes);
router.post('/getComplimentaryHeadwise', controller.getComplimentaryHeadwise);	
router.post('/getAggregatedCouponDetail', controller.getAggregatedCouponDetail);
router.post('/getSettlement', controller.getSettlement);
router.post('/getDataRangeBillsForDailySalesInvoice', controller.getDataRangeBillsForDailySalesInvoice);
router.post('/getDataRangeBillsForBoh', controller.getDataRangeBillsForBoh);
router.post('/getDataForMenuMix', controller.getDataForMenuMix);
router.post('/getItemwiseDiscountReports', controller.getItemwiseDiscountReports);
router.post('/getOffersReport', controller.getOffersReport);
router.post('/getDataForEmployeePerformance', controller.getDataForEmployeePerformance);
router.post('/getDataForEnterpriseSettlement', controller.getDataForEnterpriseSettlement);
//Kanish api for report here
router.post('/:createWithId', controller.createWithId);




router.put('/:updateWithId', controller.updateWithId);
router.put('/:id', controller.update);

router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
module.exports = router;
