/**
 * Created by Ranjeet Sinha on 8/8/2015.
 */
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// Added new schema to bulk copy data in manipulation data into another collection

var BillMSchema = new Schema({
    tab: String,
    tabId: String,
    tabType: String,
    _waiter: {},
    _delivery: {},
    _currentUser: {},
    selectedPermissions: [],
    _isTakeOut: Boolean,
    _isDelivery: Boolean,
    _covers: Number,
    _tableId: Number,
    _tabDetails: Boolean,
    _customer: {},
    _items: [],
    _taxes: [],
    _kots: [],
    _cash: [],
    _debitCards: [],
    _creditCards: [],
    _manualKotNumber: String,
    instance_id: String,
    deployment_id: Schema.Types.ObjectId,
    tenant_id: Schema.Types.ObjectId,
    ng_id:  String ,
    printItems: [],
    printTaxes: [],
    isPrinted: Boolean,
    isSettled: Boolean,
    isVoid: Boolean,
    voidComment: String,
    isSynced: Boolean,
    creditCardForm: {},
    debitCardForm: {},
    _created: {type: Date}, // Offline Creation
    created: {type: Date, default: Date.now}, // Online Creation
    billNumber: Number,
    daySerialNumber: Number,
    serialNumber: Number,
    splitNumber: Number,
    kotNumber: Number,
    _closeTime: {type: Date},
    payments: {},
    voidBillTime: {type: Date},
    billPrintTime: {type: Date},
    billDiscountAmount:Number,
    copy_id:{}
});

/*
BillMSchema
    .path('ng_id')
    .validate(function (value, respond) {
        var self = this;

        if (this.isNew) {

            this.constructor.findOne({ng_id:value, deployment_id:this.deployment_id}, function (err, sc) {
                if (err) throw err;

                if (sc) {
                    if (this.billId === sc.ng_id) return respond(false);
                    return respond(false);
                }
                respond(true);
            });

        } else {

            respond(true);
        }

    }, 'Duplicate ng_id.');*/
module.exports = mongoose.model('BillM', BillMSchema);