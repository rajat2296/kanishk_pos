/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Bill = require('./bill.model');

exports.register = function(socket) {
  Bill.schema.post('save', function (doc) {
    //console.log('socket'+ doc);
    onSave(socket, doc);
  });
  Bill.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
  socket.on('init', function (data) {
    console.log('init done'+data);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('bill:save'+doc.deployment_id, doc);
}


function onRemove(socket, doc, cb) {
  socket.emit('bill:remove', doc);
}