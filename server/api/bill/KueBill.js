var Bill = require('./bill.model');
module.exports = {
  doQueue: function(queue) {
  queue.process('getDataRangeBillsForInvoice',4,function(job, done){
  var fdate=new Date( job.data.fromDate);
  var tdate= new Date(job.data.toDate);
  tdate=tdate.setDate(tdate.getDate()+1);
 
      var _bills = [];
  var query={
    //complimentary:req.query.complimentary,
    tenant_id: job.data.tenant_id,
    deployment_id: job.data.deployment_id,
    _created: {
      $gte: new Date(fdate), $lte:new Date(tdate)
    },
    isDeleted:{$ne:true}
  };
  //async.eachSeries(_dates, function (date, callback) {
  if(job.data.complimentary!=undefined) {
    // console.log(req.query.complimentary);
    if(job.data.complimentary=='true'){

      query={
        complimentary:job.data.complimentary,
        tenant_id: job.data.tenant_id,
        deployment_id: job.data.deployment_id,
        _created: {
          $gte: new Date(fdate), $lte:new Date(tdate)
        },
        isDeleted:{$ne:true}
      }
    }
  }

  Bill.find(

    query
    ,{
      isVoid:1,
      _created: 1,
      tabType: 1,
      _closeTime:1,
      prefix:1,
      billNumber:1,
      daySerialNumber:1,
      serialNumber:1,
      payments:1,
      _tableId:1,
      _covers:1,
      '_kots.isVoid':1,
      '_kots.items.name': 1,
      '_kots.items._id': 1,
      '_kots.items.quantity': 1,
      '_kots.items.subtotal': 1,
      '_kots.items.rate': 1,
      '_kots.items.category.categoryName':1,
      '_kots.items.category.superCategory.superCategoryName':1,
      '_kots.items.taxes._id':1,
      '_kots.items.taxes.name':1,
      '_kots.items.taxes.tax_amount':1,
      '_kots.items.taxes.baseRate_preTax':1,
      '_kots.items.taxes.cascadingTaxes._id':1,
      '_kots.items.taxes.cascadingTaxes.name':1,
      '_kots.items.taxes.cascadingTaxes.tax_amount':1,
      '_kots.items.taxes.cascadingTaxes.baseRate_preTax':1,
      '_kots.items.taxes.cascadingTaxes.selected':1,
      '_kots.items.addOns._id':1,
      '_kots.items.addOns.name':1,
      '_kots.items.addOns.rate':1,
      '_kots.items.addOns.subtotal':1,
      '_kots.items.addOns.quantity':1,
      '_kots.items.addOns.category.categoryName':1,
      '_kots.items.addOns.category.superCategory.superCategoryName':1,
      '_kots.items.addOns.taxes._id':1,
      '_kots.items.addOns.taxes.name':1,
      '_kots.items.addOns.taxes.tax_amount':1,
      '_kots.items.addOns.taxes.baseRate_preTax':1,
      '_kots.items.addOns.taxes.cascadingTaxes.baseRate_preTax':1,
      '_kots.items.addOns.taxes.cascadingTaxes.selected':1,
      '_kots.items.addOns.taxes.cascadingTaxes._id':1,
      '_kots.items.addOns.taxes.cascadingTaxes.name':1,
      '_kots.items.addOns.taxes.cascadingTaxes.tax_amount':1,
      '_kots.items.discounts.discountAmount':1,
      '_kots.items.discounts.type':1,
      '_kots.items.discount.value':1,
      '_kots.items.isDiscounted':1,
      'instance.name':1,
      'instance.prefix':1,
      billDiscountAmount:1


    }, function (err, bills) {
      if (err) {
        done(null,[])
      }
      done(null,[]);
  },function(){done(null,[])});
   // done(null,[]);      
    });
  }
}