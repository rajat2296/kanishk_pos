'use strict';

var express = require('express');
var controller = require('./favouriteOrder.controller');

var router = express.Router();

router.get('/getFavourites',controller.getFavourites);
router.get('/countByName',controller.countByName);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;