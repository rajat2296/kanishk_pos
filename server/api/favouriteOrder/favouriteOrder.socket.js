/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var FavouriteOrder = require('./favouriteOrder.model');

exports.register = function(socket) {
  FavouriteOrder.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  FavouriteOrder.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('favouriteOrder:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('favouriteOrder:remove', doc);
}