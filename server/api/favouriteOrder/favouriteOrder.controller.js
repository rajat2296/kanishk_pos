'use strict';

var _ = require('lodash');
var FavouriteOrder = require('./favouriteOrder.model');

// Get list of favouriteOrders
exports.index = function(req, res) {
  FavouriteOrder.find(function (err, favouriteOrders) {
    if(err) { return handleError(res, err); }
    return res.json(200, []);
  });
};

exports.getFavourites=function(req,res){
  console.log(req.query) ;
  FavouriteOrder.find({tenant_id:req.query.tenant_id,"customer.mobile":req.query.mobile},function (err, favouriteOrders) {
    console.log(favouriteOrders);
    if(err) { return handleError(res, err); }
    return res.json(200,favouriteOrders);
  });
}
exports.countByName=function(req,res){
  console.log(req.query) ;
  FavouriteOrder.count({tenant_id:req.query.tenant_id,favouriteName:req.query.favouriteName,"customer.mobile":req.query.mobile},function (err, favouriteOrdersCount) {
   console.log(favouriteOrdersCount);
    if(err) { return handleError(res, err); }
    return res.json(200, {count:favouriteOrdersCount});
  });
}
// Get a single favouriteOrder
exports.show = function(req, res) {
  FavouriteOrder.findById(req.params.id, function (err, favouriteOrder) {
    if(err) { return handleError(res, err); }
    if(!favouriteOrder) { return res.send(404); }
    return res.json(favouriteOrder);
  });
};

// Creates a new favouriteOrder in the DB.
exports.create = function(req, res) {
  FavouriteOrder.create(req.body, function(err, favouriteOrder) {
    if(err) { return handleError(res, err); }
    return res.json(201, favouriteOrder);
  });
};

// Updates an existing favouriteOrder in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  FavouriteOrder.findById(req.params.id, function (err, favouriteOrder) {
    if (err) { return handleError(res, err); }
    if(!favouriteOrder) { return res.send(404); }
    var updated = _.merge(favouriteOrder, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, favouriteOrder);
    });
  });
};

// Deletes a favouriteOrder from the DB.
exports.destroy = function(req, res) {
  FavouriteOrder.findById(req.params.id, function (err, favouriteOrder) {
    if(err) { return handleError(res, err); }
    if(!favouriteOrder) { return res.send(404); }
    favouriteOrder.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}