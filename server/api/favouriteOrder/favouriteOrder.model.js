'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FavouriteOrderSchema = new Schema({
    tab: String,
    tabId: String,
    tabType: String,
    customer: {},
    items: [],
    billOfferDiscount:[],
    instance_id: String,
    deployment_id: Schema.Types.ObjectId,
    tenant_id: Schema.Types.ObjectId,
    isSynced: Boolean,
    created: {type: Date, default: Date.now}, // Online Creation
    billNumber: Number,
    closeTime: {type: Date},
    payments: {},
    source:{},
    isRejected:Boolean,
    trigger:{},
    favouriteName:String
});

module.exports = mongoose.model('FavouriteOrder', FavouriteOrderSchema);