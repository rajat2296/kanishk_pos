'use strict';

var _ = require('lodash');
var Data = require('../bill/bill.model');     //import bill schema
var request=require('request');
var mongoose=require('mongoose');
var Stock=require('../stockTransaction/stockTransaction.model');

// Get list of bills
exports.index = function(req, res) {

  var startDate=new Date(req.query.startDate);

  var endDate=new Date(req.query.endDate);
 // var endDate=new Date();
   endDate.setHours(23,59,59);

  console.log(startDate);
  console.log(endDate);

  var deployment_id=mongoose.mongo.ObjectID(req.query.deployment_id);
  //console.log(req.query.deployment_id);

  if(req.query.startBillNo==-1)          // if not filtered acording to bill no
  {
    Data.find({"deployment_id":deployment_id,"isVoid":false,created: {$gte:startDate, $lte:endDate}}, function (err, datas) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(datas);
    });
  }
  else                                // if  filtered according to bill no.
  {
    Data.find({"deployment_id":deployment_id,"isVoid":false,created: {$gte: startDate, $lte:endDate},billNumber:{$gte:req.query.startBillNo, $lte:req.query.endBillNo}}, function (err, datas) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(datas);
    });
  }

};


// Get a single data
exports.show = function(req, res) {
  Data.findById(req.params.id, function (err, data) {
    if(err) { return handleError(res, err); }
    if(!data) { return res.status(404).send('Not Found'); }
    return res.json(data);
  });
};

// Creates a new data in the DB.                          //used to push vouchers in tally
exports.create = function(req, res) {
  var postdata=req.body.data;
  console.log(postdata);
  request.post(
    {url:req.body.url,
      body :postdata,
      'Content-Type': 'text/xml'
    },
    function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body);
        res.status(200).send(body);
      }
      else
      {
        res.status(500).send(error);
      }
    }
  );
};

// Updates an existing data in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Data.findById(req.params.id, function (err, data) {
    if (err) { return handleError(res, err); }
    if(!data) { return res.status(404).send('Not Found'); }
    var updated = _.merge(data, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(data);
    });
  });
};

// Deletes a data from the DB.
exports.destroy = function(req, res) {
  Data.findById(req.params.id, function (err, data) {
    if(err) { return handleError(res, err); }
    if(!data) { return res.status(404).send('Not Found'); }
    data.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

/**
 * */
exports.testConnection=function(req,res){

  console.log("inside accounts api");
  request.post(
    {url:req.body.url,
      body:req.body.data,
      'Content-Type': 'text/xml'
    },
    function ( error,response, body) {
      if (!error && response.statusCode == 200) {
        res.send(body);
      }
      else
      { console.log(error);
        res.status(500).send(error);}
    }
  );
};

exports.getStock=function(req,res){
  console.log('inside getstock');
  var startDate=new Date(req.query.startDate);

  var endDate=new Date(req.query.endDate);
  // var endDate=new Date();
  endDate.setHours(23,59,59);

  console.log(endDate);

  var deployment_id=mongoose.mongo.ObjectID(req.query.deployment_id);

  console.log(deployment_id);

  Stock.find({$and:[{"deployment_id":deployment_id},{$or:[{"transactionType":"1"},{"transactionType":"2"}]},{created: {$gte: startDate, $lte:endDate}}]}, function (err, datas) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(datas);
  });

};

function handleError(res, err) {
  return res.status(500).send(err);
}
