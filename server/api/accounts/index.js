'use strict';

var express = require('express');
var controller = require('./accounts.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/',auth.isAuthenticated() , controller.index);                         //used to get bills from mongodb
//router.get('/:id', controller.show);
router.post('/',auth.isAuthenticated(),controller.create);                      // used to push vouchers to tally
router.put('/:id',auth.isAuthenticated(), controller.update);
router.patch('/:id',auth.isAuthenticated(), controller.update);
router.delete('/:id',auth.isAuthenticated(), controller.destroy);
router.post('/connect',auth.isAuthenticated(),controller.testConnection);   //used to test connection with tally
router.get('/getStock',auth.isAuthenticated(),controller.getStock);
module.exports = router;
