/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Accounts = require('./accounts.model');

exports.register = function(socket) {
  Accounts.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Accounts.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('accounts:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('accounts:remove', doc);
}