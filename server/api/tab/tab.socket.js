/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Tab = require('./tab.model');

exports.register = function(socket) {
  Tab.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Tab.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('tab:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('tab:remove', doc);
}