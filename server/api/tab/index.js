'use strict';

var express = require('express');
var controller = require('./tab.controller');

var router = express.Router();

/*router.get('/:tenant_id/:deployment_id', controller.tenantDeployment);*/
router.get('/getTenantTabs', controller.getTenantTabs);
router.get('/getTabsName',controller.getTabsName);
router.get('/', controller.index);
router.get('/tabPartners',controller.getTabPartners);
router.get('/getTabsByType', controller.getTabsByType);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.put('/:id/mapCategories', controller.mapCategories);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/deliveryPartnerMap',controller.mapDeliveryPartnersToTab);


module.exports = router;
