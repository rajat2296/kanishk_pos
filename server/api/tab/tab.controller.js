'use strict';

var _ = require('lodash');
var Tab = require('./tab.model');
var Tax = require('../tax/tax.model');
var Item = require('../item/item.model');
var async = require('async');
var request = require('request');
var config = require('../../config/environment');
var utils=require('../Utils/utils');
var mongoose=require('mongoose');
var TabPartner=require('./tabPartner.model');
var Deployment=require('../deployment/deployment.model');

// Get list of tabs for specific to Tenant
exports.index = function (req, res) {
    if(config.isQueue){

  request(config.reportServer.url+'/api/tabs?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

    var paramsquery;
    //console.log(req.query.lastSynced == undefined);
    if (req.query.lastSynced === undefined) {
        paramsquery = {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
    }
    else {
        paramsquery = {
            tenant_id: req.query.tenant_id,
            deployment_id: req.query.deployment_id,
            updated: {$gte: new Date(req.query.lastSynced), $lt: new Date(req.query.currentSyncTime)}
        }
    }
   // console.log(paramsquery);
    Tab.find(paramsquery, function (err, tabs) {
        var tabsarr = [];
        //  console.log(req.query.lastSynced);
        if (req.query.lastSynced === undefined) {
            tabsarr = tabs
        } else {
            _.forEach(tabs, function (t) {
                var tab = {_id: t._id, tabName: t.tabName, tabType: t.tabType, route: t.route, created: t.created, tenant_id: t.tenant_id, deployment_id: t.deployment_id};
                tabsarr.push(tab);
            });
        }
        if (err) {
            return handleError(res, err);
        }
      //console.log(tabs);
        var tabr = ( String(req.query.lastSynced) === "undefined") ? tabs : tabsarr;
        return res.json(200, ( tabsarr));
    });
 }
};

// Get list of tabs specific to Deployment & Tenant
exports.tenantDeployment = function (req, res) {
    /*console.log(req.params);*/
    Tab.find(
        {
            'tenant_id': req.params.tenant_id,
            'deployment_id': req.params.deployment_id
        }, function (err, tabs) {

            if (err) {
                return handleError(res, err);
            }
            return res.json(200, tabs);
        });

};

exports.getTenantTabs = function (req, res) {
   // console.log(req.query);
    Tab.find({tenant_id:req.query.tenant_id}, function (err, tenantTabs) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, tenantTabs);
    });
};

// Get a single tab
exports.show = function (req, res) {
    /*console.log(req.params);*/
    Tab.findById(req.params.id, function (err, tab) {
        if (err) {
            return handleError(res, err);
        }
        if (!tab) {
            return res.send(404);
        }
        return res.json(tab);
    });
};

// Creates a new tab in the DB.
exports.create = function (req, res) {
    Tab.create(req.body, function (err, tab) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, tab);
    });
    /*Tab.collection.insert(req.body, null, function (err, tabs) {
     if (err) {
     return handleError(res, err);
     }
     return res.json(201, tabs);
     });*/
};

// Updates an existing tab in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    req.body.updated = Date.now();

    Tab.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function(err, tab){
        if(err) return handleError(res, err);

        else if(!tab) return res.send(404);

        else {
            delete req.body.updated;
            delete req.body.tabDetails;
            delete req.body.categories;
            req.body._id = tab._id.toString();

            Tax.update({'tabs._id':req.params.id}, {$set: {updated: Date.now(), 'tabs.$': req.body}}, {multi:true}, function(err, tax){
                if(err) return handleError(res,err);

                else{
                    Item.update({'tabs._id':req.params.id},
                        {$set: {
                            'lastModified': Date.now(),
                            'updated': Date.now(),
                            'tabs.$.tabName':req.body.tabName,
                            'tabs.$.tabType':req.body.tabType
                            }
                        },
                        {multi:true},
                        function(err){
                            if(err) return handleError(res,err);
                            else return res.json(200,tab);
                        }
                    );
                }
            });
        }
    });
};

exports.mapCategories  = function(req,res){
    var category = req.body.category;

    if (req.body._id) {
        delete req.body._id;
    }

    if(req.body.push){
        Tab.update({_id: req.params.id,'categories._id':{$ne: category._id}}, {updated: Date.now(), $push: {categories: req.body.category}}, {new: true}, function(err, tab){
            if(err) return handleError(res, err);

            else if(!tab) return res.send(200);

            else {
                delete req.body.tab.categories;

                async.each(req.body.items, function(item, callback){
                    req.body.tab.item = item;
                    Item.update({
                        '_id': item._id,
                        'category._id': req.body.category._id,
                        'tabs._id': {$ne: req.params.id }
                    },
                    {
                        $push: {tabs: req.body.tab},
                        lastModified: Date.now(),
                        updated: Date.now()
                    },
                    function (err,ic){
                        console.log(ic);
                        if(err) callback(err);
                        else callback();
                    });
                }, function(err){
                    if(err) return handleError(res,err);
                    else return res.json(200);
                });
            }
        });
    } else{
        Tab.update({_id: req.params.id, 'categories._id': category._id}, {updated: Date.now(), $pull: {categories: {_id: req.body.category._id}}}, {new: true}, function(err, tab){
            if(err) return handleError(res, err);

            else if(!tab) return res.send(200);

            else {
                delete req.body.tab.categories;

                Item.update({
                    'category._id': req.body.category._id,
                    'tabs._id': req.params.id
                },
                {
                    $pull: {tabs: {_id: req.body.tab._id}},
                    lastModified: Date.now(),
                    updated: Date.now()
                },
                {multi: true},
                function (err,ic){
                    if(err) return handleError(res,err);
                    else return res.json(200);
                });
            }
        });
    }
}

exports.getTabsByType = function (req, res) {
   // console.log(req.query);
    Tab.find({
            'tenant_id': req.query.tenant_id,
            'deployment_id': req.query.deployment_id,
            'tabType': req.query.tabType
        }, function (err, tenantTabs) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, tenantTabs);
    });
};

exports.getTabsByType = function (req, res) {
   // console.log(req.query);
    Tab.find({
            'tenant_id': req.query.tenant_id,
            'deployment_id': req.query.deployment_id,
            'tabType': req.query.tabType
        }, function (err, tenantTabs) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, tenantTabs);
    });
};

// Deletes a tab from the DB.
exports.destroy = function (req, res) {
    Tab.findById(req.params.id, function (err, tab) {
        if (err) {
            return handleError(res, err);
        }
        if (!tab) {
            return res.send(404);
        }
        tab.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

exports.mapDeliveryPartnersToTab=function(req,res){
     console.log(req.body);
     var obj=req.body;
    TabPartner.findOneAndUpdate({deployment_id:obj.deployment_id,tab_id:obj.tab_id},obj,{upsert:true},function(err,doc){
        if(doc)
            return res.status(200).send({status:'ok'});
        else
            return handleError(res,{status:'error'});

    });
}

exports.getTabPartners=function(req,res){
 TabPartner.find({deployment_id:req.query.deployment_id},function(err,docs){
    console.log(err,docs);
    if(docs)
        return res.status(200).send(docs);
    else
        return handleError({status:'error'});
 })
}

exports.getTabsName=function(req,res){
console.log(req.query);
  Deployment.find({$and:[{"settings.name":"Inresto MerchantId"},{"settings.selected":true},{"settings.value":req.query.merchant_id}]},{tenant_id:1},function(err,docs){
    console.log(docs);
    if(docs.length>0)
    {
      Tab.find({
        'tenant_id': docs[0].tenant_id,
        'deployment_id': docs[0]._id,
        'tabType': 'table'
      },{tabName:1}, function (err, tenantTabs) {
        console.log(tenantTabs);
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, tenantTabs);
      });
    }
    else
      return res.status(500).send({status:'Merchant ID not found'});
  })
}

function handleError(res, err) {
    return res.send(500, err);
}
