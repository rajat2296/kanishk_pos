'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TabSchema = new Schema({
    tabName: String,
    tabDetails: String,
    tabType: String,
    route: String,
    categories: [],
    tenant_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    created: { type: Date, default: Date.now },
    updated:{type: Date}
});

TabSchema.set('versionKey', false);

TabSchema
    .path('tabName')
    .validate(function (value, respond) {
        var self = this;
        if (this.isNew) {
            this.constructor.findOne({tabName:value, deployment_id:this.deployment_id}, function (err, t) {

                if (err) throw err;
                if (t) {
                    if (self.tabName === t.tabName) return respond(false);
                    return respond(false);
                }
                respond(true);
            });
        } else {
            respond(true);
        }

    }, 'Duplicate Tab Name, please use another name.');
TabSchema
    .pre('save', function (next) {
        this.updated = new Date;
        next();
    });

module.exports = mongoose.model('Tab', TabSchema);
