'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TabPartnerSchema = new Schema({
    tabName: String,
    tabType: String,
    tenant_id: String,
    deployment_id: String,
    tab_id: String,
    partners:[]
});

module.exports = mongoose.model('TabPartner', TabPartnerSchema);
