'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
var BaseKitchenUnitSchema = new Schema({
  unitName:String,
  unitId: String,
  baseUnit: {},
  conversionFactor: Number,
  baseConversionFactor:Number,
  tenant_id: Schema.Types.ObjectId,
  deployment_id:String,
  toDeployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
});

BaseKitchenUnitSchema.set('versionKey', false);
BaseKitchenUnitSchema.path('unitId').validate(function(v, respond) {
  var self=this; 
  //  this.constructor.findOne({'unitName':  new RegExp('^'+v+'$', "i"),toDeployment_id:this.toDeployment_id}, function (err, storeunits) {
  this.constructor.findOne({'unitId': v,toDeployment_id:this.toDeployment_id}, function (err, storeunits) {
 
    var flag=true;
    if(err){throw err;}
    
    if(storeunits)
    {
      // if(storeunits.unitName.toLowerCase()===self.unitName.toLowerCase())
      // {
      //   if(storeunits.unitId!=self.unitId){
          flag=false;
      //   }
      // }
    }

    respond(flag);
    });
  
}, 'This Unit is already used please try  with another name !');

module.exports = mongoose.model('baseKitchenUnit', BaseKitchenUnitSchema);
