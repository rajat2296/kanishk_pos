'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var baseKitchenUnitCtrlStub = {
  index: 'baseKitchenUnitCtrl.index'
};

var routerStub = {
  get: sinon.spy()
};

// require the index with our stubbed out modules
var baseKitchenUnitIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './baseKitchenUnit.controller': baseKitchenUnitCtrlStub
});

describe('BaseKitchenUnit API Router:', function() {

  it('should return an express router instance', function() {
    baseKitchenUnitIndex.should.equal(routerStub);
  });

  describe('GET /api/baseKitchenUnits', function() {

    it('should route to baseKitchenUnit.controller.index', function() {
      routerStub.get
        .withArgs('/', 'baseKitchenUnitCtrl.index')
        .should.have.been.calledOnce;
    });

  });

});
