/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/baseKitchenUnits              ->  index
 */

// 'use strict';

// exports.index = function(req, res) {
//   res.json([]);
// };


'use strict';

var _ = require('lodash');
var baseKitchenUnit = require('./baseKitchenUnit.model');

// Get list of baseKitchenUnits
exports.index = function(req, res) {
  baseKitchenUnit.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}, function (err, baseKitchenUnits) {
    if(err) { return handleError(res, err); }
        console.log("BASE KITCHEN UNITS", baseKitchenUnits)

    return res.json(200, baseKitchenUnits);
  });
};

// Get a single baseKitchenUnit
exports.show = function(req, res) {
  baseKitchenUnit.findById(req.params.id, function (err, baseKitchenUnit) {
    if(err) { return handleError(res, err); }
    if(!baseKitchenUnit) { return res.send(404); }
    return res.json(baseKitchenUnit);
  });
};

// Creates a new baseKitchenUnit in the DB.
exports.create = function(req, res) {
  baseKitchenUnit.create(req.body, function(err, baseKitchenUnit) {
    if(err) 
      { 
        res.json({error:err});
        //return handleError(res, err); 
      }
      else
      {
        return res.json(201, baseKitchenUnit);
      }
  });
};

// Updates an existing baseKitchenUnit in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  baseKitchenUnit.findById(req.params.id, function (err, baseKitchenUnit) {
    if (err) { return handleError(res, err); }
    if(!baseKitchenUnit) { return res.send(404); }
    //var updated = _.merge(baseKitchenUnit, req.body);
    var updated=_.extend(baseKitchenUnit, req.body);
    updated.markModified("unitName");
    updated.markModified("baseUnit");
    updated.markModified("conversionFactor");
    updated.markModified("baseConversionFactor");
    updated.save(function (err) {
      if (err) { 
        //return handleError(res, err); 
        res.json({error:err});
      }
      return res.json(200, baseKitchenUnit);
    });
  });
};

// Deletes a baseKitchenUnit from the DB.
exports.destroy = function(req, res) {
  baseKitchenUnit.findById(req.params.id, function (err, baseKitchenUnit) {
    if(err) { return handleError(res, err); }
    if(!baseKitchenUnit) { return res.send(404); }
    baseKitchenUnit.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
