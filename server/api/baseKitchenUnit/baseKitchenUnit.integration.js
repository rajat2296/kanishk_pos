'use strict';

var app = require('../..');
var request = require('supertest');

describe('BaseKitchenUnit API:', function() {

  describe('GET /api/baseKitchenUnits', function() {
    var baseKitchenUnits;

    beforeEach(function(done) {
      request(app)
        .get('/api/baseKitchenUnits')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          baseKitchenUnits = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      baseKitchenUnits.should.be.instanceOf(Array);
    });

  });

});
