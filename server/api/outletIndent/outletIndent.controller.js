'use strict';

var _ = require('lodash');
var OutletIndent = require('./outletIndent.model');
var Q = require('q');
var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;

exports.getLastOutletIndentNumber = function (req, res) {
  console.log(req.query);
  var query = {};
  query.deployment_id = req.query.deployment_id;

  console.log(query);
  OutletIndent.find(query, {requirementNumber: 1, daySerialNumber: 1, created: 1}, {sort: {requirementNumber: -1}, limit: 1}, function (err, po) {
    console.log(po[0]);
    if(err)
      return handleError(res, err);
    if(po[0])
      return res.status(200).json({message: 'OK', errorcode: 200, data: po[0]});
    else
      return res.status(400).json({message: 'Not Found', errorcode: 400, data: null});
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

// Get count of Stock requirements
exports.getCount = function (req, res) {
  console.log(req.query,"get count");
  var tenant_id= new ObjectId(req.query.tenant_id);
  var toDeployment_id=req.query.toDeployment_id;

  var check = {};
  check.tenant_id=new ObjectId(req.query.tenant_id)
  if(req.query.isSupplied==1) {
    //console.log('true req');
    check.supplyDate = {$exists: true, $ne: null};
    // check.ReceiveDate={$or:[null , {$ne:null}]};
    check.toDeployment_id=req.query.toDeployment_id;

  } else if(req.query.isSupplied==0){
    //console.log('false req');
    check.ReceiveDate =  null;
    check.supplyDate =  null;
    check.toDeployment_id=req.query.toDeployment_id;

  } else {
    console.log('receive req');
    check.ReceiveDate =  null;
    check.supplyDate={$ne:null};
    check.deployment_id=req.query.deployment_id;

  }

  OutletIndent.count(check, function (err, OutletIndent) {
    console.log(err);
    console.log(OutletIndent);
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json({count: OutletIndent});
  });
};


exports.getRequirementsPagination = function (req, res) {
  // console.log('pagination');
  // console.log(req.query);
  var check = {
    tenant_id:req.query.tenant_id
  }
  if(req.query.isSupplied==1) {
    //console.log('true req');
    check.supplyDate = {$exists: true, $ne: null};
    // check.ReceiveDate={$or:[null , {$ne:null}]};
    check.toDeployment_id=req.query.toDeployment_id;
  } else if(req.query.isSupplied==0){
    //console.log('false req');
    check.supplyDate =  null;
    check.toDeployment_id=req.query.toDeployment_id;
  } else {
    //console.log('receive req');
    check.ReceiveDate =  null;
    check.supplyDate={$ne:null};
    check.deployment_id=req.query.deployment_id;

  }
  console.log(check);
  OutletIndent.find(check, {created: 1, supplyDate: 1, ReceiveDate: 1, requirementNumber: 1, deploymentName: 1, "user.username": 1, daySerialNumber: 1, "vendor.vendorName": 1,}, {sort: {created: -1}, skip: req.query.skip, limit: req.query.limit}, function (err, OutletIndents) {
    if (err) {
      console.log(err);
      return handleError(res, err);
    }
    return res.json(200, OutletIndents);
  });
};
// Get list of OutletIndents
exports.index = function (req, res) {
  //console.log("QUERY", req.query)
  var check;
  if (req.query.toDeployment_id) {
    check = {tenant_id: req.query.tenant_id, toDeployment_id: req.query.toDeployment_id}
  } else {
    check = {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
  }
  if(req.query.store_id){
    check["store._id"] = req.query.store_id
  }
  if(req.query.vendor_id){
    check["store.vendor._id"] = req.query.vendor_id;
  }
  if(req.query.ReceiveDate){
    var startDate = new Date(req.query.ReceiveDate);
    startDate.setHours(0, 0, 0, 0);
    var endDate = new Date(req.query.ReceiveDate);
    endDate = new Date(endDate.setDate(endDate.getDate() + 1));
    console.log(endDate);
    check["ReceiveDate"] = {$gte: startDate, $lte: endDate};
  }
  console.log("CHECK OBJECT", check)
  OutletIndent.find(check, function (err, OutletIndents) {
    if (err) {
      console.log(err);
      return handleError(res, err);
    }
    return res.json(200, OutletIndents);
  });
};

// Get a single OutletIndent
exports.show = function (req, res) {
  OutletIndent.findById(req.params.id, function (err, OutletIndent) {
    if (err) {
      return handleError(res, err);
    }
    if (!OutletIndent) {
      return res.send(404);
    }
    return res.json(OutletIndent);
  });
};

// Creates a new OutletIndent in the DB.
exports.create = function (req, res) {
  OutletIndent.create(req.body, function (err, OutletIndent) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(201, OutletIndent);
  });
};

exports.createMultipart = function(req, res) {

  var r = JSON.parse(req.files.bt2.buffer.toString('utf-8'));
  console.log(r);
  if (r._id) {
    delete r._id;
  }
  OutletIndent.create(r, function(err, requirement){
    if(err)
      return handleError(res, err);
    return res.status(200).json(requirement);
  });
};

/*
 // Updates an existing OutletIndent in the DB.
 exports.update = function(req, res) {
 if(req.body._id) { delete req.body._id; }
 OutletIndent.findById(req.params.id, function (err, OutletIndent) {
 if (err) { return handleError(res, err); }
 if(!OutletIndent) { return res.send(404); }
 //var updated = _.merge(OutletIndent, req.body);
 var updated=_.extend(OutletIndent, req.body);
 updated.save(function (err) {
 if (err) { return handleError(res, err); }
 return res.json(200, OutletIndent);
 });
 });
 };
 */
// Updates an existing OutletIndent in the DB.
exports.update = function (req, res) {
  var r = req.body;
  OutletIndent.findById(req.params.id, function (err, OutletIndent) {
    if (err) {
      return handleError(res, err);
    }
    if (!OutletIndent) {
      return res.send(404);
    }
    var updated = _.merge(OutletIndent, r);
    updated.markModified('items');
    console.log(updated);
    updated.save(function (err) {
      console.log(err);
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, OutletIndent);
    });
  });
};

// Deletes a OutletIndent from the DB.
exports.destroy = function (req, res) {
  OutletIndent.findById(req.params.id, function (err, OutletIndent) {
    if (err) {
      return handleError(res, err);
    }
    if (!OutletIndent) {
      return res.send(404);
    }
    OutletIndent.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};
//modified
exports.getInitialData = function(req, res) {

  var check;
  if (req.query.toDeployment_id) {
    check = {tenant_id: req.query.tenant_id, toDeployment_id: req.query.toDeployment_id}
  } else {
    check = {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
  }
  OutletIndent.find(check, {created: 1, supplyDate: 1, ReceiveDate: 1, requirementNumber: 1, deploymentName: 1, "user.username": 1, daySerialNumber: 1, "vendor.vendorName": 1}, function(err, requirements) {
    if(err)
      return handleError(res, err);
    return res.status(200).json(requirements);
  })
};

exports.getStoreNItems = function(req, res){

  var id = req.params.id;
  console.log(id);

  OutletIndent.findOne({_id: id}, {"store": 1, items: 1, toDeployment_id: 1}, function(err, requirement){
    if(err)
      return handleError(res, err);
    if(!requirement)
      return res.status(400).send({error: 'Not Found'});
    return res.status(200).json(requirement);
  });
};

exports.getItemsInSelectedRequirements = function(req, res){

  var ids = JSON.parse(req.query.ids);
  console.log(ids);
  var check = {$or: []};
  for(var id in ids){
    if(ids[id]){
      check.$or.push({_id: id});
    }
  }
  OutletIndent.find(check, {items: 1}, function(err, requirements) {

    console.log(err);
    if(err)
      return handleError(res, err);
    return res.status(200).json(requirements);
  });
};
//end

function formatQueryParamater(q) {
  _.forIn(q, function (value, key) {
    if (value == "" || key == "startDate" || key == "endDate" || key == "tenant_id" || key == "deployment_id") {
      delete q[key];
    }
  });
  return q;
};

exports.getRequirementReport = function (req, res) {
  // req.query={
  //   startDate:'6/1/2015',
  //   endDate:'7/22/2015',
  //   tenant_id:'556e9b0302fad90807111e34',
  //   deployment_id:'556eadf102fad90807111eac'
  // };


  var startDate = new Date(req.query.startDate);
  startDate.setHours(0, 0, 0, 0);
  var endDate = new Date(req.query.endDate);
  endDate.setDate(endDate.getDate() + 1);
  endDate.setHours(0, 0, 0, 0);

  var isBK = req.query.isBaseKitchen;
  // var sss=JSON.parse(JSON.stringify(req.query));
  // var ssa=formatQueryParamater(sss);
  // var conditions = Object.keys(ssa).map(function(key,value) {
  //     var obj = {},
  //         newKey = "";
  //     if ( key == "vendor_id" ) {
  //       newKey = key;
  //     }
  //     else
  //     {
  //       newKey=key;
  //       obj[newKey] = req.query[key];
  //     }
  //     return obj;
  // });
  // console.log(conditions);
  console.log(isBK);
  if (isBK != undefined) {
    getRequirements_BK(req.query.deployment_id, req.query.tenant_id, startDate, endDate).then(function (r) {
      return res.json(r);
    })
  }
  else {
    getRequirements(req.query.deployment_id, req.query.tenant_id, startDate, endDate).then(function (r) {
      return res.json(r);
    })
  }
};
function getRequirements_BK(deployment_id, tenant_id, startDate, endDate) {
  console.log("BaseKitchen");
  var d = Q.defer();
  OutletIndent.aggregate([
    {
      $match: {
        requirementType: 1,
        created: {$gte: new Date(startDate), $lte: new Date(endDate)},
        tenant_id: new ObjectId(tenant_id),
        toDeployment_id: deployment_id
      }
    },
    {
      $project: {
        created: "$created",
        supplyDate: "$supplyDate",
        billNo: "$daySerialNumber",
        receiveDate: "$ReceiveDate",
        vendorName: "$store.vendor.vendorName",
        items: "$items",
        deploymentName: "$deploymentName"
      }
    }], function (err, stores) {
    if (err) {
      d.reject("error");
    }
    else {
      d.resolve(stores);
    }
  });
  return d.promise;
};
function getRequirements(deployment_id, tenant_id, startDate, endDate) {
  console.log("OutLet");
  var d = Q.defer();
  OutletIndent.aggregate([
    {
      $match: {
        requirementType: 1,
        created: {$gte: new Date(startDate), $lte: new Date(endDate)},
        tenant_id: new ObjectId(tenant_id),
        deployment_id: deployment_id

      }
    },
    {
      $project: {
        created: "$created",
        supplyDate: "$supplyDate",
        billNo: "$daySerialNumber",
        receiveDate: "$ReceiveDate",
        vendorName: "$store.vendor.vendorName",
        items: "$items"
      }
    }], function (err, stores) {
    if (err) {
      d.reject("error");
    }
    else {
      d.resolve(stores);
    }
  });
  return d.promise;
};
exports.getSuplyRequirementReport = function (req, res) {
  // req.query={
  //   startDate:'6/1/2015',
  //   endDate:'7/22/2015',
  //   tenant_id:'556e9b0302fad90807111e34',
  //   deployment_id:'556eadf102fad90807111eac'
  // };
  var startDate = new Date(req.query.startDate);
  startDate.setHours(0, 0, 0, 0);
  var endDate = new Date(req.query.endDate);
  endDate.setDate(endDate.getDate() + 1);
  endDate.setHours(0, 0, 0, 0);
  //console.log(req.query);
  //console.log(new Date(req.query.endDate));

  OutletIndent.aggregate([
    {
      $match: {
        requirementType: 1,
        created: {$gte: new Date(startDate), $lte: new Date(endDate)},
        tenant_id: new ObjectId(req.query.tenant_id),
        deployment_id: req.query.deployment_id,
        supplyDate: {$ne: null}
      }
    },
    {
      $project: {
        created: "$created",
        supplyDate: "$supplyDate",
        billNo: "$daySerialNumber",
        receiveDate: "$ReceiveDate",
        vendorName: "$store.vendor.vendorName",
        items: "$items"
      }
    }
  ], function (err, result) {
    if (err) {
      console.log(err);
      handleError(res, err)
    } else {
      //console.log(result);
      return res.json(result);

    }
  });
};

function handleError(res, err) {
  return res.send(500, err);
};

exports.getLastRequirementBillNo = function (req, res) {

  OutletIndent.find({
    requirementType: 1,
    deployment_id: req.query.deployment_id,
    tenant_id: new ObjectId(req.query.tenant_id)
  }, {_id: 1}, {sort: {'created': -1}}, function (err, post) {
    if (err) {
      handleError(res, err);
    }
    else {
      var no = 1;
      if (post != null) {
        no = parseInt(post.length) + 1;
      }
      //console.log("BillNo :"+ no);
      var aa = {billNo: no};
      return res.json(aa);
    }
  });
}
// Create a new Transaction with Angular ID.
exports.createWithId = function (req, res) {

  //console.log("created");
  var transactionId = req.params.id;
  var _data = req.body;
  delete req.body._id;
  delete req.body.db;

  _data.transactionId = transactionId;

  var today = new Date(_data.created);
  today.setHours(0, 0, 0, 0);
  var endDate = new Date();
  endDate.setDate(today.getDate() + 1);
  endDate.setHours(0, 0, 0, 0);
  //console.log("Date:" +today);
  OutletIndent.find({
    'requirementType': _data.requirementType,
    deployment_id: _data.deployment_id,
    tenant_id: new ObjectId(_data.tenant_id)
  }, {}, {sort: {'created': -1}}, function (err, post) {

    if (post != null) {
      //console.log( "post = "+post );
      _data.requirementNumber = parseInt(post.length) + 1;
    }
    else {
      _data.requirementNumber = 1;
    }

    OutletIndent.find({
      'requirementType': _data.requirementType,
      deployment_id: _data.deployment_id,
      tenant_id: new ObjectId(_data.tenant_id),
      created: {$gte: today, $lt: endDate}
    }, {}, {sort: {'created': -1}}, function (err, post1) {
      if (post1 != null) {
        _data.daySerialNumber = parseInt(post1.length) + 1;
      }
      else {
        _data.daySerialNumber = 1;
      }

      OutletIndent.create(_data, function (err, trans) {
        if (err) {
          //console.log(err);
          return handleError(res, err);
        }

        return res.json(201, trans);
      });

    });


  });

};

exports.createWithIdMultipart = function(req, res) {

  console.log('createWithIdMultipart');
  var r = JSON.parse(req.files.bt2.buffer.toString('utf-8'));
  var transactionId = req.params.id;
  var _data = r;
  delete r._id;
  delete r.db;

  _data.transactionId = transactionId;

  var today = new Date(_data.created);
  today.setHours(0, 0, 0, 0);
  var endDate = new Date();
  endDate.setDate(today.getDate() + 1);
  endDate.setHours(0, 0, 0, 0);
  //console.log("Date:" +today);
  OutletIndent.find({
    'requirementType': _data.requirementType,
    deployment_id: _data.deployment_id,
    tenant_id: new ObjectId(_data.tenant_id)
  }, {}, {sort: {'created': -1}}, function (err, post) {

    if (post != null) {
      //console.log( "post = "+post );
      _data.requirementNumber = parseInt(post.length) + 1;
    }
    else {
      _data.requirementNumber = 1;
    }

    OutletIndent.find({
      'requirementType': _data.requirementType,
      deployment_id: _data.deployment_id,
      tenant_id: new ObjectId(_data.tenant_id),
      created: {$gte: today, $lt: endDate}
    }, {}, {sort: {'created': -1}}, function (err, post1) {
      if (post1 != null) {
        _data.daySerialNumber = parseInt(post1.length) + 1;
      }
      else {
        _data.daySerialNumber = 1;
      }

      OutletIndent.create(_data, function (err, trans) {
        if (err) {
          //console.log(err);
          return handleError(res, err);
        }

        return res.json(201, trans);
      });
    });
  });
};