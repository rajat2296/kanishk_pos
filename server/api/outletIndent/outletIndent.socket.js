/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var OutletIndent = require('./outletIndent.model');

exports.register = function(socket) {
  OutletIndent.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  OutletIndent.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('outletIndent:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('outletIndent:remove', doc);
}