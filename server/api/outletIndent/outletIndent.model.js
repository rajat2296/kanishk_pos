'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OutletIndentSchema = new Schema({
  transactionId:String,
  requirementType:Number,
  requirementNumber:Number,
  daySerialNumber:Number,
  store:{},
  vendor:{},
  user:{},
  items:[],
  documentationCharge:String,
  deploymentName:String,
  deploymentAddress:String,
  tenant_id: Schema.Types.ObjectId,
  deployment_id:String,
  toDeployment_id: String,
  fromDeployment_id: String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  supplyDate: { type: Date,default:null},
  ReceiveDate: { type: Date,default:null},
  active: { type: Boolean,default:true}
}, {versionKey: false});

module.exports = mongoose.model('OutletIndent', OutletIndentSchema);
