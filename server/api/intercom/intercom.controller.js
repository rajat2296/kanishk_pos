'use strict';

var _ = require('lodash');
//var Intercom = require('./intercom.model')
var request=require('request');
var async=require('async');
var Intercom=require('intercom-client');
var app_id='pgllgbav';
var app_api_key='e02da2475a895e01a22c4ed1ab6c5593ba8405bf';
var access_token='dG9rOjY4NGI0ZDM5X2M3ZTZfNGM1YV85MTI5XzBlOTE5MWQyOTRjNDoxOjA='

var client = new Intercom.Client(app_id,app_api_key);

// Get list of intercoms
exports.index = function(req, res) {
  Intercom.find(function (err, intercoms) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(intercoms);
  });
};

// Get a single intercom
exports.show = function(req, res) {
  Intercom.findById(req.params.id, function (err, intercom) {
    if(err) { return handleError(res, err); }
    if(!intercom) { return res.status(404).send('Not Found'); }
    return res.json(intercom);
  });
};

// Creates a new intercom in the DB.
exports.createUser = function(req, res) {
  console.log(req.body);
  client.users.create(req.body,function(err,data){
    if(err)
    { console.log(err.body);
      res.status(500).send(err.body);
    }
    else if(data) {
      console.log(data.body);
      res.status(200).send(data.body);
    }
  });
};

// Updates an existing intercom in the DB.
exports.update = function(req, res) {
  client.users.create(req.body,function(err,data){
    if(err)
    { console.log(err.body);
      res.status(500).send(err.body);
    }
    else if(data) {
      console.log(data.body);
      res.status(200).send(data.body);
    }
  });
};

// Deletes a intercom from the DB.
exports.destroy = function(req, res) {
  Intercom.findById(req.params.id, function (err, intercom) {
    if(err) { return handleError(res, err); }
    if(!intercom) { return res.status(404).send('Not Found'); }
    intercom.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

exports.sendEvent=function(req,res){
  var failedEvents=[];
  async.each(req.body.data,function(event,next) {
    client.events.create(event,function(err,data) {
      if(err)
        failedEvents.push(event);
      next();
    });
  },
    function(err){
     console.log("all done")
     res.status(200).send(failedEvents);
    });
}

function handleError(res, err) {
  return res.status(500).send(err);
}
