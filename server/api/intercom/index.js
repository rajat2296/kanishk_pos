'use strict';

var express = require('express');
var controller = require('./intercom.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.createUser);          //create or update user
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/event',controller.sendEvent);     // send an event

module.exports = router;
