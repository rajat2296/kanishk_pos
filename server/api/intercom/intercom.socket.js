/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Intercom = require('./intercom.model');

exports.register = function(socket) {
  Intercom.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Intercom.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('intercom:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('intercom:remove', doc);
}