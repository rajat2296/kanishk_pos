'use strict';

var _ = require('lodash');
var CardTransaction = require('./cardTransaction.model');

// Get list of cardTransactions
exports.index = function(req, res) {
  CardTransaction.find(function (err, cardTransactions) {
    if(err) { return handleError(res, err); }
    return res.json(200, cardTransactions);
  });
};

// Get a single cardTransaction
exports.show = function(req, res) {
  CardTransaction.findById(req.params.id, function (err, cardTransaction) {
    if(err) { return handleError(res, err); }
    if(!cardTransaction) { return res.send(404); }
    return res.json(cardTransaction);
  });
};

// Creates a new cardTransaction in the DB.
exports.create = function(req, res) {
  console.log(req.body);

  delete req.body._id;
  CardTransaction.create(req.body, function(err, cardTransaction) {
    console.log(err);
    if(err) { return handleError(res, err); }
    return res.json(201, cardTransaction);
  });
};

// Updates an existing cardTransaction in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  CardTransaction.findById(req.params.id, function (err, cardTransaction) {
    if (err) { return handleError(res, err); }
    if(!cardTransaction) { return res.send(404); }
    var updated = _.merge(cardTransaction, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, cardTransaction);
    });
  });
};

// Deletes a cardTransaction from the DB.
exports.destroy = function(req, res) {
  CardTransaction.findById(req.params.id, function (err, cardTransaction) {
    if(err) { return handleError(res, err); }
    if(!cardTransaction) { return res.send(404); }
    cardTransaction.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}