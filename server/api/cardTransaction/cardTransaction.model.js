'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CardTransactionSchema = new Schema({
    cardId:String,
    formNumber:String,
    deployment_id:{ type:Schema.Types.ObjectId},
    name:String,
    mobile:String,
    securityAmount:Number,
    entryFee:Number,
    created:{type:Date,default:new Date()},
    user:{},
    transactions:[],
    closeTime:{type:Date},
    isSynced:Boolean,
    cardTransaction_id:{ type:Schema.Types.ObjectId}
});
CardTransactionSchema.set('versionKey', false);
module.exports = mongoose.model('CardTransaction', CardTransactionSchema);