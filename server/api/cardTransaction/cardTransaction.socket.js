/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var CardTransaction = require('./cardTransaction.model');

exports.register = function(socket) {
  CardTransaction.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  CardTransaction.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('cardTransaction:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('cardTransaction:remove', doc);
}