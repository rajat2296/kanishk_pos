'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DeliverySchema = new Schema({
  deployment_id: String,
  dispatchTime:{type:Date},
  deliveredTime:{type:Date},
  acceptedTime:{type:Date},
  rejection:[],
  pickedUpTime:{type:Date},
  deliveryBoyMobile:String,
  deliveryBoyName:String,
  orderId:String,
  zomatoOrderId:String,
  status:String,
  datetime:{type:Date},
  trackingUrl:String,
  location:{},
  runnrOrderId:String,
  isLogistics:{type:Boolean,default:false}
});

module.exports = mongoose.model('Delivery', DeliverySchema);
