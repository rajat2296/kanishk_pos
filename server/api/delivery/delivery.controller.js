'use strict';

var _ = require('lodash');
var Delivery = require('./delivery.model');
var io=null;
var request=require('request');
var Q=require('q');
var roadrunnr_testUrl='http://apitest.roadrunnr.in';
var roadrunnr_productionUrl='https://runnr.in';
var grab_testUrl='http://uatapi.grab.in/pos';
var grab_productionUrl='http://api.grab.in/pos';
var crypto=require('crypto');

exports.getOrders=function(req,res){
  var orders=req.query.orders.split(',');
  try {
    Delivery.find({
      deployment_id: req.query.deployment_id,
      "orderId": {$in: orders}
    },function (err, data) {
      if (data) return res.status(200).send(data);
      console.log(err);
      return handleError(res, {status: 'error'});
    })
  }
  catch(e){
    console.log("exception",e);
    return res.status(400).send({message:"Invalid request"});
  }
}

exports.orderAccepted_secured=function(req,res){

 var err=checkForErrors(req.body,{event:'accepted'});
 if(err) return handleError(res,err,422);
 var obj={status:'Accepted',acceptedTime:new Date(Number(req.body.timestamp))}
 updateOrder(req.body.order_id,obj,res);
}

exports.orderRejected_secured=function(req,res){
   var err=checkForErrors(req.body,{event:'rejected'});
 if(err) return handleError(res,err,422);
  var obj={};
  obj.status='Rejected';
  if(req.body.reason) obj.status='Rejected due to'+req.body.reason;
  obj[$push]={rejection:{name:req.body.delivery_boy_name,dateTime:new Date(Number(req.body.timestamp)),reason:req.body.reason}};
 updateOrder(req.body.oreder_id,obj,res);
}

exports.getTrackingUrl_secured=function(req,res){
    var err=checkForErrors(req.body,{event:'tracking'});
 if(err) return handleError(res,err,422);
 var obj={trackingUrl:req.body.tracking_url}
updateOrder(req.body.order_id,obj,res);
}

exports.orderPickedUp_secured=function(req,res){
   var err=checkForErrors(req.body,{event:'picked-up'});
 if(err) return handleError(res,err,422);
 var obj={status:'Picked-up',pickedUpTime:new Date(Number(req.body.timestamp))};
 updateOrder(req.body.order_id,obj,res);
}

exports.orderDelivered_secured=function(req,res){
   var err=checkForErrors(req.body,{event:'delivered'});
 if(err) return handleError(res,err,422);
 var obj={status:'Delivered',deliveredTime:new Date(Number(req.body.timestamp))};
 updateOrder(req.body.order_id,obj,res);
}

exports.orderAssigned_secured=function(req,res){
     var err=checkForErrors(req.body,{event:'assign'});
 if(err) return handleError(res,err,422);
  var obj={deliveryBoyMobile:req.body.delivery_boy_mobile};
  if(req.body.delivery_boy_name) obj.deliveryBoyName=req.body.delivery_boy_name;
 updateOrder(req.body.order_id,obj,res);
}

function updateOrder(orderId,obj,res){
  Delivery.findOneAndUpdate({'orderId':orderId},obj,{new:true},function(err,doc){
    if(doc){
      if(io) io.broadcast.emit('delivery_order_update'+doc.deployment_id,doc);
      return res.status(200).json({"message":"order updated",status:"success"});
    }
    else if(!doc) return handleError(res,{message:'order not found'},404);
    else return handleError(res,{message:'Something went wrong'},500);
  })
}

function checkForErrors(body,method){
  if(!body.order_id) return {message:'order_id missing'}

  if(method.event=='tracking'){
    if(!body.tracking_url) return {message:'tracking_url missing'};
  }
  else if(method.event=='assign'){
    if(!body.delivery_boy_mobile) return {message:'delivery boy mobile missing'};
  }
  else {
    if(!body.timestamp) return {message:'timestamp missing'}
    if(!((new Date(Number(body.timestamp))).getTime()>0)) return {message:'invalid timestamp'}
  }
 return null;
}

exports.webhook_notify=function(req,res){
   var url='http://posistapi.com/api/webhooks/hit?event_name=assign_delivery&client_id='+req.query.clientId;
    try {
      var options = {
        method: 'POST',
        url: url,
        headers:{'content-type':'application/json',
          'accept':'application/json',
        },
        json:req.body
      };

      request(options, function (error, response, body) {
        console.log(options,error);
         if(response && response.statusCode==200)
          saveOrder(req,res,body).then(function(success){
            return res.status(200).send({status:'success'});
          },function(err){
            return handleError(res,{sttaus:'error'});
          })
         else return handleError(res, {status: 'failure'},500);
      });
    }
    catch(e){
      console.log("exception",e);
      return res.status(500).send({message:'An exception occurred'});
    }
}

function saveOrder(req,res,body){
   try {
     var deferred=Q.defer();
     console.log(body);
     var data = {
      deployment_id: req.query.deployment_id,
      orderId: req.query.billId,
      status: 'New',
      datetime: new Date(),
      dispatchTime:new Date()
    };

    if(req.body.delivery_boy_mobile) data.deliveryBoyMobile=req.body.delivery_boy_mobile;
    else if(body && body.deliveryBoyMobile) data.deliveryBoyMobile=body.deliveryBoyMobile;

    if(req.body.delivery_boy_name) data.deliveryBoyName=req.body.delivery_boy_name;
    else if(body && body.deliveryBoyName) data.deliveryBoyName=body.deliveryBoyName;

    if(body && body.tracking_url) data.trackingUrl=body.tracking_url;
    data.isLogistics=(body.isLogistics)?true:false

    if(body.runnrOrderId) data.runnrOrderId=body.runnrOrderId;

    Delivery.findOneAndUpdate({
      "deployment_id": req.query.deployment_id,
      "orderId": req.query.billId
    }, data, {upsert: true}, function (err, data) {
       console.log("data",data);
      if (data) deferred.resolve(true);
      else deferred.reject(false);
    });
  }
  catch(e){
    console.log(e);
     deferred.reject(false);
  }
  return deferred.promise;
}

exports.delhivery_notify=function(req,res){
  var url='https://odxapi-staging.delhivery.com/v1/orders';
    try {
      var options = {
        method: 'POST',
        url: url,
        headers:{'content-type':'application/json',
          'accept':'application/json',
          'Authorization':'Bearer '+req.query.token
        },
        json:req.body
      };

      request(options, function (error, response, body) {
        console.log(options);
        console.log(response,body,error);
        if(response && response.statusCode==200)
          saveOrder(req,res,body)
         else return handleError(res, {status: 'error'},500);
      });
    }
    catch(e){
      console.log("exception",e);
      return res.status(500).send({message:'An exception occurred'});
    }
}


exports.roadrunnr_pushOrder=function(req,res){
  roadrunnr_getToken(req).then(function(token){
    roadrunnr_checkServiceability(req,token).then(function(expectedPickUpTime){
      console.log("expec",expectedPickUpTime);
      var url=roadrunnr_testUrl+'/v1/orders/ship';
      var domain=req.host.substring(req.host.lastIndexOf('.')+1);
      if(domain=='net')
       url=roadrunnr_productionUrl+'v1/orders/ship/';
        var options = {
         method: 'POST',
         url: url,
         headers:{'content-type':'application/json',
           'Authorization':'Bearer '+token
         },
         json:req.body.data
       };

       request(options, function(error, response, body){
         if(body && body.status && body.status.code==200){
           body.tracking_url=body.tracking_link?body.tracking_link:null;
           body.deliveryBoyName=body.driver_name?body.driver_name:null;
           body.deliveryBoyMobile=body.driver_phone?body.driver_phone:null;
           body.runnrOrderId=body.order_id.toLowerCase();
           body.isLogistics=true;

          saveOrder(req,res,body).then(function(success){
            return res.status(200).send({message:'Order sent.Expected PickUp Time: '+expectedPickUpTime});
          },function(err){
            return res.status(500).send({message:'Order not sent'});
          })
         }
         else res.status(500).send({message: 'Order not sent'});
       });
    },function(err){
     return res.status(500).send({message:'Sorry! This order is not serviceable'});
   })
  },function(err){
     return res.status(500).send({message:'Unauthenticated Request'});
  })
}

function roadrunnr_getToken(req){
     var deferred=Q.defer();
      var url=roadrunnr_testUrl+'/oauth/token';
      var domain=req.host.substring(req.host.lastIndexOf('.')+1);
      if(domain=='net')
       url=roadrunnr_productionUrl+'/oauth/token';
       var options = {
         method: 'POST',
         url: url,
         formData:{
           client_id:req.body.client_id,client_secret:req.body.client_secret,grant_type:'client_credentials'
         }
      };

       request(options, function(error, response, body){
         body=(typeof body=='object')?body:JSON.parse(body);
         if(body && body.access_token)
          deferred.resolve(body.access_token);
         else deferred.reject(false);
      });
     return deferred.promise;
  }

 function roadrunnr_checkServiceability(req,token){
      var deferred=Q.defer();
      var url=roadrunnr_testUrl+'/v1/orders/serviceability';
      var domain=req.host.substring(req.host.lastIndexOf('.')+1);
      if(domain=='net')
       url=roadrunnr_productionUrl+'/v1/orders/serviceabilty';
       var options = {
         method: 'POST',
         url: url,
         headers:{'content-type':'application/json',
           'Authorization':'Bearer '+token
         },
         json:req.body.data
       };

       request(options, function(error, response, body){
         console.log("body",body);
         if(body && body.serviceable==true)
          deferred.resolve(body.expected_time_for_pickup.value+ ' '+body.expected_time_for_pickup.unit);
         else deferred.reject(false);
       });
    return deferred.promise;
 }

function handleError(res, err,code) {
  console.log("handle error");
  if(code==200)
    err.status="success";
  else err.status="error";
  return res.status(code).send(err);
}

exports.roadrunnr_status=function(req,res){
  var data={};
  data.deliveryBoyName=req.body.driver_name;
  data.deliveryBoyMobile=req.body.driver_number;

  var status=req.body.status;
  switch(req.body.status){
    case 'DRIVER_ASSIGNED':  status='Accepted'; data.acceptedTime=new Date(req.body.timestamp); break;
    case 'REACHED_PICKUP':status='Reached Pickup location'; break;
    case 'SHIPPED': status='Shipped'; data.pickedUpTime=new Date(req.body.timestamp); break;
    case 'COMPLETE':  status='Delivered'; data.deliveredTime=new Date(req.body.timestamp); break;
    case 'DRIVER_ASSIGN_FAILED':status='Assignment Failed'; break;
  }
  data.status=status;
  Delivery.findOneAndUpdate({runnrOrderId:req.body.order_id},data,{new:true},function(err,doc){
    console.log(err,doc);
    if(doc){
      io.broadcast.emit('delivery_order_update'+doc.deployment_id, doc);
      return res.status(200).send({status: 'ok'});
    }
    else return res.status(500).send({status:'error'});
  });
}

exports.grab_pushOrder=function(req,res) {
 // console.log(req.body.privateKey,JSON.stringify(req.body.data));
  var hash=crypto.createHmac('sha256', req.body.privateKey).update(JSON.stringify(req.body.data)).digest('hex');
      var url=grab_testUrl+'/pushorder/';
      var domain=req.host.substring(req.host.lastIndexOf('.')+1);
      if(domain=='net')
        url=grab_productionUrl+'/pushorder/';
      var options = {
        method: 'POST',
        url: url,
        headers:{
          'X-Public':req.body.publicKey,
          'X-Hash':hash,
          'content-type':'application/json'
        },
        json:req.body.data
      };

      request(options, function(error, response, body){
        console.log(options,body,error);
        if(body && body.success=="true"){
          body.isLogistics=true;
          saveOrder(req,res,body).then(function(success){
            return res.status(200).send({message:'Order sent'});
          },function(err){
            return res.status(500).send({message:'Order not sent'});
          })
        }
        else res.status(500).send({message: 'Order not sent'},500);
      });
  }

function grab_createHash(req){
  var deferred= Q.defer();
  deferred.resolve('');
  return deferred.promise;
}

exports.grab_status=function(req,res){
  var data={};
  data.deliveryBoyName=req.body.riderName;
  data.deliveryBoyMobile=req.body.riderPhone;
  var status=req.body.orderStatus;
  switch(Number(req.body.orderStatus)){
    case 1: status="New";break;
    case 2: status="In Process";break;
    case 3: status="Accepted";data.acceptedTime=new Date(req.body.dttm);break;
    case 4: status="To be Picked";break;
    case 5: status="Out for Delivery";data.pickedUpTime=new Date(req.body.dttm);break;
    case 6: status="Delivered";data.deliveredTime=new Date(req.body.dttm);break;
    case 99: status="Cancelled";break;
    case 101: status="Accepted by Grab";break;
    case 102: status="Rejected by Grab";break;
  }
 // console.log("status",status);
  data.status=status;
  Delivery.findOneAndUpdate({orderId:req.body.clientOrderId},data,function(err,doc){
    if(doc) {
      io.broadcast.emit('delivery_order_update' + doc.deployment_id, doc);
      res.status(200).send({status: 'ok'});
    }
    else res.status(500).send({status:'error'});
  })
}

exports.set=function(socket){
  io=socket;
}
