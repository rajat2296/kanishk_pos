'use strict';

var express = require('express');
var controller = require('./delivery.controller')
var auth = require('../../auth/auth.service');

var router = express.Router();
router.get('/orders',controller.getOrders);
router.post('/webhook_notify',controller.webhook_notify);
router.post('/delhivery_notify',controller.delhivery_notify);
router.post('/roadrunnr/pushOrder',controller.roadrunnr_pushOrder);
router.post('/roadrunnr/status',controller.roadrunnr_status);
router.post('/grab/pushOrder',controller.grab_pushOrder);
router.post('/grab/status',controller.grab_status);
//for open API
router.use('/partner/*', auth.onlyOpenApi);

router.post('/partner/accepted', auth.hasCustomerKey, controller.orderAccepted_secured);
router.post('/partner/rejected',auth.hasCustomerKey,controller.orderRejected_secured);
router.post('/partner/picked_up',auth.hasCustomerKey,controller.orderPickedUp_secured);
router.post('/partner/delivered',auth.hasCustomerKey,controller.orderDelivered_secured);
router.post('/partner/tracking',auth.hasCustomerKey,controller.getTrackingUrl_secured);
router.post('/partner/assign',auth.hasCustomerKey,controller.orderAssigned_secured)


module.exports = router;
