'use strict';

var express = require('express');
var controller = require('./stockReportResetTime.controller.js');

var router = express.Router();

//router.get('/', controller.index);
// router.get('/:id', controller.show);
// router.post('/', controller.create);
// router.put('/:id', controller.update);
// router.patch('/:id', controller.update);
// router.delete('/:id', controller.destroy);
// router.get('/getRequirementData', controller.getRequirementData);
// router.get('/getStockSummary', controller.getStockSummary);

router.get('/getConsumptionSummary_ItemWiseReport', controller.getConsumptionSummary_ItemWiseReport);
router.get('/getBillsInDataForFoodCost', controller.getBillsInDataForFoodCost);
router.get('/vendorDataForCockpit', controller.vendorDataForCockpit);
router.get('/cockpitBillingData', controller.cockpitBillingData);
router.get('/getConsumptionSummary_ItemReport', controller.getConsumptionSummary_ItemReport);
router.get('/getConsumptionViaPhysical', controller.getConsumptionViaPhysical);
router.get('/getConsumptionSummary', controller.getConsumptionSummary);
router.get('/getConsumptionSummary_IntermediateReport', controller.getConsumptionSummary_IntermediateReport);
router.get('/getConsumptionSummary_FinishedFoodReport', controller.getConsumptionSummary_FinishedFoodReport);
router.get('/getLastPriceOfItem_RawMaterial',controller.getLastPriceOfItem_RawMaterial);
router.get('/getClosingQty', controller.getClosingQty);
router.get('/getClosingQty_SemiProcessed', controller.getClosingQty_SemiProcessed);
router.get('/getClosingQty_ProcessedFood', controller.getClosingQty_ProcessedFood);
router.get('/testFunction', controller.testFunction);
router.get('/getRecipeQtyInBase', controller.getRecipeQtyInBase);
router.get('/getLastPriceOfItem_FinishedFood',controller.getLastPriceOfItem_FinishedFood);
router.get('/getLastPriceOfItem_Intermediate',controller.getLastPriceOfItem_Intermediate);
router.get('/getGrowthReport',controller.getGrowthReport);
router.get('/cockpitData',controller.cockpitData);
router.get('/getConsumptionSummaryFoodCosting',controller.getConsumptionSummaryFoodCosting);
router.get('/getFoodCostingForMenuMix',controller.getFoodCostingForMenuMix);

// router.get('/getConsumptionSummary_DateWise', controller.getConsumptionSummary_DateWise);
// router.get('/RawMaterialPricing_Receipe', controller.RawMaterialPricing_Receipe);
module.exports = router;
