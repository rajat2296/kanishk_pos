'use strict';

var _ = require('lodash');
var Settle = require('./settle.model');
var Bank = require('./bank.model');

// Get list of settles
exports.index = function(req, res) {
  Settle.find(function (err, settles) {
    if(err) { return handleError(res, err); }
    return res.json(200, settles);
  });
};

// Get list of settles
exports.getBank = function(req, res) {
  Bank.find(function (err, banks) {
    if(err) { return handleError(res, err); }
    return res.json(200, banks);
  });
};
// Get list of settles
exports.getCoupon = function(req, res) {
  Coupon.find(function (err, coupons) {
    if(err) { return handleError(res, err); }
    return res.json(200, coupons);
  });
};

// Get a single settle
exports.show = function(req, res) {
  Settle.findById(req.params.id, function (err, settle) {
    if(err) { return handleError(res, err); }
    if(!settle) { return res.send(404); }
    return res.json(settle);
  });
};

// Creates a new settle in the DB.
exports.create = function(req, res) {
  Settle.create(req.body, function(err, settle) {
    if(err) { return handleError(res, err); }
    return res.json(201, settle);
  });
};

// Creates a new settle in the DB.
exports.createBank = function(req, res) {
  Bank.create(req.body, function(err, bank) {
    if(err) { return handleError(res, err); }
    return res.json(201, bank);
  });
};
// Creates a new settle in the DB.
exports.createCoupon = function(req, res) {
  Coupon.create(req.body, function(err, coupon) {
    if(err) { return handleError(res, err); }
    return res.json(201, coupon);
  });
};

// Updates an existing settle in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Settle.findById(req.params.id, function (err, settle) {
    if (err) { return handleError(res, err); }
    if(!settle) { return res.send(404); }
    var updated = _.merge(settle, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, settle);
    });
  });
};

// Updates an existing settle in the DB.
exports.updateBank = function(req, res) {
  var id=req.body.bankId;
  if(req.body._id) { delete req.body._id; }
  Bank.findById(id, function (err, bank) {
    if (err) { return handleError(res, err); }
    if(!bank) { return res.send(404); }
    var updated = _.merge(bank, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, bank);
    });
  });
};


// Updates an existing settle in the DB.
exports.updateCoupon = function(req, res) {
  var id=req.body.couponId;
  if(req.body._id) { delete req.body._id; }
  Coupon.findById(id, function (err, coupon) {
    if (err) { return handleError(res, err); }
    if(!coupon) { return res.send(404); }
    var updated = _.merge(coupon, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, coupon);
    });
  });
};

// Deletes a settle from the DB.
exports.destroy = function(req, res) {
 // console.log(req.body);
  Settle.findById(req.params.id, function (err, settle) {
    if(err) { return handleError(res, err); }
    if(!settle) { return res.send(404); }
    settle.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};
// Deletes a settle from the DB.
exports.destroyBank = function(req, res) {
 // console.log(req.body);
  Bank.findById(req.body.id, function (err, bank) {
    if(err) { return handleError(res, err); }
    if(!bank) { return res.send(404); }
    bank.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

// Deletes a settle from the DB.
exports.destroyCoupon = function(req, res) {
 // console.log(req.body);
  Coupon.findById(req.body.id, function (err, coupon) {
    if(err) { return handleError(res, err); }
    if(!coupon) { return res.send(404); }
    coupon.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}