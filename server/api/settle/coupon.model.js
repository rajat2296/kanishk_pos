'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CouponSchema = new Schema({
    name: String,
    created: { type: Date, default: Date.now },
    updated:{type: Date, default: Date.now}


});

module.exports = mongoose.model('Coupon', BankSchema);