/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Settle = require('./settle.model');

exports.register = function(socket) {
  Settle.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Settle.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('settle:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('settle:remove', doc);
}