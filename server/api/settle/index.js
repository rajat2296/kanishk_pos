'use strict';

var express = require('express');
var controller = require('./settle.controller');

var router = express.Router();


router.get('/getBank', controller.getBank);
router.get('/getCoupon', controller.getCoupon);
router.get('/', controller.index);
router.get('/:id', controller.show);

router.post('/', controller.create);
router.post('/createBank', controller.createBank);
router.post('/updateBank', controller.updateBank);
router.post('/destroyBank', controller.destroyBank);
router.post('/createCoupon', controller.createCoupon);
router.post('/updateCoupon', controller.updateCoupon);
router.post('/destroyCoupon', controller.destroyCoupon);

router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;