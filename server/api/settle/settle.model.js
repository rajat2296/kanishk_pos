'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var SettleSchema = new Schema({
    name: String,
    isOnlineOrder:Boolean,
    created: { type: Date, default: Date.now },
    updated:{type: Date, default: Date.now}
});

module.exports = mongoose.model('Settle', SettleSchema);