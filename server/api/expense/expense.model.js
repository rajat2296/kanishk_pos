'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ExpenseSchema = new Schema({
  tenant_id: Schema.Types.ObjectId,
  deployment_id: String,
  billNo: Number,
  billDate: Date,
  expenses: [{}],
  total: Number
}, {versionKey: false});

module.exports = mongoose.model('Expense', ExpenseSchema);