'use strict';

var _ = require('lodash');
var Expense = require('./expense.model');
var mongoose = require('mongoose');

// Get list of expenses
exports.index = function (req, res) {
  Expense.find({
      tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
      deployment_id: req.query.deployment_id
    }, function (err, expenses) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(expenses);
  });
};

// Get a single expense
exports.show = function (req, res) {
  Expense.findById(req.params.id, function (err, expense) {
    if (err) {
      return handleError(res, err);
    }
    if (!expense) {
      return res.status(404).send('Not Found');
    }
    return res.json(expense);
  });
};

// Creates a new expense in the DB.
exports.create = function (req, res) {
  Expense.create(req.body, function (err, expense) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(expense);
  });
};

// Updates an existing expense in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Expense.findById(req.params.id, function (err, expense) {
    if (err) {
      return handleError(res, err);
    }
    if (!expense) {
      return res.status(404).send('Not Found');
    }
    var updated = _.extend(expense, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(expense);
    });
  });
};

// Deletes a expense from the DB.
exports.destroy = function (req, res) {
  Expense.findById(req.params.id, function (err, expense) {
    if (err) {
      return handleError(res, err);
    }
    if (!expense) {
      return res.status(404).send('Not Found');
    }
    expense.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(204).send('No Content');
    });
  });
};

exports.getExpenseBillNo = function (req, res) {
  Expense.aggregate([{
    $match: {
      billDate: {
        $eq: new Date(req.query.billDate)
      },
      tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
      deployment_id: req.query.deployment_id
    }
 }, {
    $group: {
      _id: null,
      billNo: {
        $max: '$billNo'
      }
    }
 }], function (err, response) {
    if (err) {
      return handleError(res, err);
    } else if (!response.length) {
      return res.status(200).json([{
        _id: null,
        billNo: 0
      }]);
    }
    return res.status(200).json(response);
  });
};

exports.getBillReportBetweenDates = function (req, res) {
  Expense.aggregate([{
      $match: {
        billDate: {
          $gte: new Date(req.query.startDate),
          $lte: new Date(req.query.endDate)
        },
        tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
        deployment_id: req.query.deployment_id
      }
    },
    {
      $sort: {
        billDate: 1,
        billNo: 1
      }
    }], function (err, response) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(response);
  });
};

exports.getCategoryReportBetweenDates = function (req, res) {
  Expense.aggregate([{
      $match: {
        billDate: {
          $gte: new Date(req.query.startDate),
          $lte: new Date(req.query.endDate)
        },
        tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
        deployment_id: req.query.deployment_id
      }
    },
    {
      $unwind: '$expenses'
    },
    {
      $group: {
        _id: {
          category: '$expenses.category.name',
        },
        bills: {
          $push: {
            billNo: '$billNo',
            billDate: '$billDate',
            total: '$total',
            vendor: '$expenses.vendor',
            item: '$expenses.item',
            rate: '$expenses.rate',
            qty: '$expenses.qty',
            amount: '$expenses.amount'
          }
        },
        total: {
          $sum: '$expenses.amount'
        }
      }
    },
    {
      $project: {
        category: '$_id.category',
        total: 1,
        bills: 1
      }
    },
    {
      $sort: {
        category: 1
      }
    }], function (err, response) {
      if (err) {
        return handleError(res, err);
      }
      return res.status(200).json(response);
    });
};

exports.getItemReportBetweenDates = function (req, res) {
  Expense.aggregate([{
      $match: {
        billDate: {
          $gte: new Date(req.query.startDate),
          $lte: new Date(req.query.endDate)
        },
        tenant_id: new mongoose.Types.ObjectId(req.query.tenant_id),
        deployment_id: req.query.deployment_id
      }
    },
    {
      $unwind: '$expenses'
    },
    {
      $group: {
        _id: {
          item: '$expenses.item',
        },
        bills: {
          $push: {
            billNo: '$billNo',
            billDate: '$billDate',
            total: '$total',
            vendor: '$expenses.vendor',
            category: '$expenses.category.name',
            rate: '$expenses.rate',
            qty: '$expenses.qty',
            amount: '$expenses.amount'
          }
        },
        total: {
          $sum: '$expenses.amount'
        }
      }
    },
    {
      $project: {
        item: '$_id.item',
        total: 1,
        bills: 1
      }
    },
    {
      $sort: {
        item: 1
      }
    }], function (err, response) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(response);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}