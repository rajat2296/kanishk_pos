'use strict';

var _ = require('lodash');
var ZomatoTrace = require('./zomatoTrace.model');
var Delivery=require('../delivery/delivery.model');
var request=require('request');
var io=null;
var moment=require('moment');

exports.dispatch=function(req,res){
var url='https://jooleh.com/api/v1/admin/orders',method='POST'
if(req.query.mode=='edit')
{url='https://jooleh.com/api/v1/admin/orders/'+req.body.oid; method='PUT';}
    try {
      var options = {
        method: method,
        url: url,
        headers:{'content-type':'application/json',
          'accept':'application/json',
          'X-Admin-Token':req.query.apiKey,
          'X-Admin-Username':req.query.username
        },
        json:req.body
      };

      request(options, function (error, response, body) {
        console.log(options);
        if(error) {console.log("error",error); return handleError(res,error);}
        if (body) {
          console.log("body",body);
          if (body.oid)
            saveOrder(req,res,body);
          else if(body.response_type=='success')
            res.status(200).send({status:'success'});
          else
            return handleError(res, {status: 'failure'});
        }
        else return handleError(res, {status: 'failure'});
      });
    }
    catch(e){
      console.log("exception",e);
      return res.status(500).send({message:'An exception occurred'});
    }
}

function saveOrder(req,res,body){
  try {
    var data = {
      deployment_id: req.query.deployment_id,
      orderId: req.query.billId,
      zomatoOrderId: body.oid, status: body.status,
      datetime: new Date(), 
      trackingUrl: body.tracking_url
    };

    if(req.body.delivery_boy_mobile)
      data.deliveryBoyMobile=req.body.delivery_boy_mobile;

    if(body.dispatched_at)
      data.dispatchTime=convertToDate(body.dispatched_at);

    Delivery.findOneAndUpdate({
      "deployment_id": req.query.deployment_id,
      "orderId": req.query.billId
    }, data, {upsert: true}, function (err, data) {
       console.log("data",data);
      if (data) return res.status(200).send({status: 'ok'});
      else {if(err) {console.log(err); return handleError(res, {status: 'error'}); }}
    });
  }
  catch(e){
    return handleError({status:'An exception occurred'});
  }
}

exports.getOrders=function(req,res){
  var orders=req.query.orders.split(',');
  try {
    Delivery.find({
      deployment_id: req.query.deployment_id,
      "orderId": {$in: orders}
    },function (err, data) {
      if (data) return res.status(200).send(data);
      console.log(err);
      return handleError(res, {status: 'error'});
    })
  }
  catch(e){
    console.log("exception",e);
    return res.status(400).send({message:"Invalid request"});
  }
}

exports.orderStatus=function(req,res){
 try{ 
var obj={};
obj.status=req.body.status;

 if(req.body.dispatched_at) obj.dispatchTime=convertToDate(req.body.dispatched_at);
 if(req.body.picked_at) {obj.pickedUpTime=convertToDate(req.body.picked_at);  obj.status='Picked-up';}
 if(req.body.delivery_guy_mobile) obj.deliveryBoyMobile=req.body.delivery_guy_mobile;
 if(req.body.tracking_url) obj.trackingUrl=req.body.tracking_url;
 if(req.body.status=='Delivered') {obj.status='Delivered'; obj.deliveredTime=convertToDate(req.body.delivered_at);
 obj.location={lat:req.body.delivery_latitude,lon:req.body.delivery_longitude};
}
  obj.datetime=convertToDate(req.body.updated_at);
  Delivery.findOneAndUpdate({"zomatoOrderId": req.body.oid}, obj,
    {new:true}, function (err, doc) {
      if(err) console.log("error",err);
      if(doc){
      console.log("data",doc);
      if (io)
      {io.broadcast.emit('zomatoOrderStatus'+doc.deployment_id,doc); console.log("socket emitted");}
     }
      res.status(200).send({message:'callback received'});
    });
 }
 catch(e){
  res.status(200).send({message:'ok'});
 }
}

exports.getStatus=function(req,res){
  var url='https://jooleh.com/api/v1/admin/orders/'+req.query.oid;
    try {
      var options = {
        method: 'GET',
        url: url,
        headers:{'content-type':'application/json',
          'accept':'application/json',
          'X-Admin-Token':req.query.apiKey,
          'X-Admin-Username':req.query.username
        }
      };

      request(options, function (error, response, bod) {
        console.log(options);
        if(error) {console.log("error",error); return handleError(res,error);}
        else if (bod) {
          var body = typeof(bod) == 'object' ? (bod) : JSON.parse(bod);
          console.log("body",body);
          if (body.oid)
            updateOrder(req,res,body);
          else
            return handleError(res, {status: 'failure'});
        }
        else return handleError(res, {status: 'failure'});
      });
    }
    catch(e){
      console.log("exception",e);
      return res.status(500).send({message:'An exception occurred'});
    }
}

function updateOrder(req,res,body){
  var obj={};
//console.log(convertToDate(body.delivered_at));
obj.status=body.status;

 if(body.dispatched_at) obj.dispatchTime=convertToDate(body.dispatched_at);
 if(body.picked_at) {obj.pickedUpTime=convertToDate(body.picked_at); obj.status='Picked-Up';}
 if(body.delivery_guy_mobile) obj.deliveryBoyMobile=body.delivery_guy_mobile;
 if(body.tracking_url) obj.trackingUrl=body.tracking_url;
 if(body.status=='Delivered') {obj.status='Delivered'; obj.deliveredTime=convertToDate(body.delivered_at);
 body.location={lat:body.delivery_latitude,lon:body.delivery_longitude};
}
  obj.datetime=convertToDate(body.updated_at);
  Delivery.findOneAndUpdate({"zomatoOrderId": body.oid}, obj,
    {new:true}, function (err, doc) {
      if(doc) res.status(200).send(doc);
       else { if(err)console.log(err); return handleError(res,{status:'error'});} 
    });
}

function handleError(res, err) {
  return res.status(500).send(err);
}

function convertToDate(str){
  try{
  var date=str.substring(0,str.indexOf('/'));
  var month=str.substring(str.indexOf('/')+1,str.lastIndexOf('/'));
  var rest=str.substring(str.lastIndexOf('/')+1);
  var d=month+'/'+date+'/'+rest;
  return new Date(Date.parse(d.substring(0,str.length-2)+' '+d.substring(str.length-2,d.length)));
 }
 catch(e){
  console.log("ex",e);
  return new Date();
 }
}

exports.set=function(socket){
  io=socket;
}
