'use strict';

var express = require('express');
var controller = require('./zomatoTrace.controller');

var router = express.Router();

router.post('/dispatch', controller.dispatch);
router.get('/orders',controller.getOrders);
router.post('/status',controller.orderStatus);
router.get('/getStatus',controller.getStatus);

module.exports = router;
