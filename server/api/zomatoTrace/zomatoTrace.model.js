'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ZomatoTraceSchema = new Schema({
  deployment_id: String,
  dispatchTime:{type:Date},
  deliveredTime:{type:Date},
  pickedUpTime:{type:Date},
  deliveryBoyMobile:String,
  billId:String,
  zomatoOrderId:String,
  status:String,
  datetime:{type:Date},
  trackingUrl:String,
  location:{}
});

module.exports = mongoose.model('ZomatoTrace', ZomatoTraceSchema);
