/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ZomatoTrace = require('./zomatoTrace.model');

exports.register = function(socket) {
  ZomatoTrace.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ZomatoTrace.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('zomatoTrace:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('zomatoTrace:remove', doc);
}