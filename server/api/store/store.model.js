'use strict';
var _ = require('lodash');
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StoreSchema = new Schema({
  storeName:String,
  storeLocation: String,
  storeUID: String,
  isKitchen: Boolean,
  isMain: Boolean,
  category:[],
  processedFoodCategory:[],
  tenant_id: Schema.Types.ObjectId,
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  store_id: String,
  lastUpdated: {
    date: Date,
    username: String
  },
  active: Boolean
});
StoreSchema.set('versionKey', false);
StoreSchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});

StoreSchema.index({storeName:1,deployment_id:1},{unique:true});


StoreSchema.path('storeName').validate(function(v, respond) {
  //console.log(v);
  var self=this;
 // if(this.isNew)
 //  {
    this.constructor.findOne({'storeName':  new RegExp('^'+v+'$', "i"),deployment_id:this.deployment_id}, function (err, store) {
    //console.log(err);
    var flag=true;
    if(err){throw err;}

    if(store)
    {
      //console.log(store +"self"+self+"this"+this);
      if(store.storeName.toLowerCase()===self.storeName.toLowerCase())
      {
        if(store.id!=self.id){
          flag=false;
        }
      }
    }
    //     else{
    //       if(store.id===self.id)
    //       {
    //         flag=true;
    //       }
    //       else
    //       {
    //         flag=false;
    //       }
    //     }
    //   }
    //   else
    //   {
    //     flag=true;
    //   }
    // }
    // else
    // {
    //   flag=true;
    // }
    respond(flag);
    //console.log(flag);
    });

}, 'This Store is already used please try  with another name !');

module.exports=mongoose.model('Store', StoreSchema);
