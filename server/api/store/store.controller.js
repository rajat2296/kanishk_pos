'use strict';

var _ = require('lodash');
var Store = require('./store.model');
var ObjectId = require('mongoose').Types.ObjectId;
var Deployment = require('../deployment/deployment.model');

// Get list of stores
exports.index = function(req, res) {
  var paramsquery;
  //if(req.query.lastStoreSynced === undefined) {
  //  paramsquery = {tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}
  //}
  //else {
  //  paramsquery = {
  //    tenant_id: req.query.tenant_id,
  //    deployment_id: req.query.deployment_id,
  //    updated: {$gte: new Date(req.query.lastStoreSynced), $lt: new Date(req.query.currentSyncTime)}
  //  }
  //}
  // Store.find(paramsquery,function (err, stores) {
  //   if(err) { return handleError(res, err); }
  //   return res.json(200, stores);
  // });
  paramsquery = {
    deployment_id: req.query.deployment_id
  }
  if(req.query.tenant_id){
    paramsquery.tenant_id = new ObjectId(req.query.tenant_id)
  }

  if(req.query.isLNStore == true || req.query.isLNStore == 'true') {
    paramsquery.store_id = {$exists: true};
  }
  var projection = {};
  if(req.query.projection){
    try{
      projection = JSON.parse(req.query.projection);
    } catch(err){
      console.log(err);
      return handleError(res, err);
    }
  }
  console.log(projection);
  console.log(paramsquery);
  Store.find(paramsquery, projection,function (err, stores) {
    if(err) { return handleError(res, err); }
    //console.log(stores);
    return res.json(200, stores);
  });
};

exports.pushStockStore = function (req, res) {

  console.log(req.body);
  if(_.has(req.query, 'isDelete')) {
    var isDelete = req.query.isDelete;
    if (isDelete == false || isDelete == 'false') {
      if (req.body.store_id && req.body.deployment_id && req.body.storeName && req.body.tenant_id) {
        Deployment.findOne({_id: req.body.deployment_id}, {tenant_id: 1}, function (err, deployment){
          if(err)
            return handleError(res, err);
          if(!deployment)
            return res.send({errorcode: 400, message: "Deployment not found in database. Please check deployment id."});
          if(deployment.tenant_id.toString() != req.body.tenant_id)
            return res.send({errorcode: 400, message: "Deployment not associated with the tenant specified. Please check tenant id."});
        
          Store.findOne({store_id: req.body.store_id, deployment_id: req.body.deployment_id}, function (err, store) {
            if (err)
              return handleError(res, err);
            if (!store) {
              console.log('!store');
              var s = req.body;
              s.isKitchen = false;
              s.isMain = false;
              s.category = [{
                _id: ObjectId(),
                isERPLNCategory: true,
                categoryName: "LN Items",
                item: []
              }];
              Store.create(s, function (err, newStore) {
                if (err)
                  return handleError(res, err);
                return res.status(200).json({statusCode: 200, message: "Store creation successfull"});
              });
            }
            else {
              console.log('store');
              var updated = _.extend(store, req.body);
              updated.save(function (err) {
                if(err)
                  return handleError(res, err);
                return res.status(200).json({statusCode: 200, message: "Store updation successfull"});
              });
            }
          });
        });
      } else {
        return res.status(500).send('Please provide all the required attributes.');
      }
    } else {
      if(req.query.store_id && req.query.deployment_id) {
        Store.findOne({store_id: req.query.store_id, deployment_id: req.query.deployment_id}, function (err, store) {
          if (err) {
            return handleError(res, err);
          }
          if (!store) {
            return res.send(404, "Store Not found!");
          }
          store.remove(function (err) {
            if (err) {
              return handleError(res, err);
            }
            return res.status(200).json({statusCode: 200, message: "Store deletion successfull"});
          });
        });
      } else {
        return res.status(500).send("Please provide a store id and a deployment id!");
      }
    }
  } else {
    return res.status(500).send("Please provide required parameters!");
  }
};

// Get a single store
exports.show = function(req, res) {
  Store.findById(req.params.id, function (err, store) {
    if(err) { return handleError(res, err); }
    if(!store) { return res.send(404); }
    return res.json(store);
  });
};

// Creates a new store in the DB.
exports.create = function(req, res) {
  Store.create(req.body, function(err, store) {
    if(err) {
      //console.log(err);
      return res.json({error:err});
    }
    else
    {
      //console.log(store);
      return res.json(201, store);
    }
  });
};



// Updates an existing store in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Store.findById(req.params.id, function (err, store) {
    if (err)
    {
      return handleError(res, err);
    }
    if(!store) { return res.send(404); }
    //var updated = _.merge(store, req.body);
    var updated = _.extend(store,req.body);
    updated.markModified("processedFoodCategory");
    updated.markModified("category");
    updated.save(function (err) {
      if (err) {
        return res.json({error:err});
        //return handleError(res, err);
      }
      return res.json(200, store);
    });
  });
};

// Deletes a store from the DB.
exports.destroy = function(req, res) {
  Store.findById(req.params.id, function (err, store) {
    if(err) { return handleError(res, err); }
    if(!store) { return res.send(404); }
    store.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

/**************FOR OPEN API*************************/
/* @author - Shubhank
   @functions -
*/
function handleApiError(res, err){
  return res.status(500).json({code:500, label: "internal_error", message:"Some error occurred in the system."});
}

function filterResponse(json){
  return _.omit(json.toObject(), ['deployment_id', 'tenant_id']);
}

exports.pushStockStore_secure = function(req, res){
  req.body.deployment_id = req.query.deployment_id;
  req.body.tenant_id = req.query.tenant_id;

  if(!_.isString(req.body.store_id))
    res.status(422).json({status: "error", message:"store_id should be string."});
  else if(!_.isString(req.body.storeName))
    res.status(422).json({status: "error", message:"storeName should be string."});
  else{
    Store.findOne({store_id: req.body.store_id, deployment_id:req.body.deployment_id}, function (err, store) {
      if (err)
        return handleApiError(res, err);
      if (!store) {
        var s = req.body;
        s.isKitchen = false;
        s.isMain = false;
        s.category = [{
          categoryName: "Raw Items",
          item: []
        }];
        Store.create(s, function (err, newStore) {
          if (err)
            return handleApiError(res, err);
          return res.status(201).json(filterResponse(newStore));
        });
      }
      else {
        var updated = _.extend(store, req.body);
        updated.save(function (err) {
          if(err)
            return handleApiError(res, err);
          return res.status(200).json(filterResponse(updated));
        });
      }
    });
  }
};

exports.deleteStockStore_secure = function(req, res){
  Store.findOne({store_id: req.query.store_id, deployment_id: req.query.deployment_id}, function (err, store) {
    if (err) {
      return handleApiError(res, err);
    }
    if (!store) {
      return res.status(404).json({status:"error", message: 'Store does not exist.'});
    }
    store.remove(function (err) {
      if (err) {
        return handleApiError(res, err);
      }
      return res.status(200).json({status:"success", message:"Store successfully removed."});
    });
  });
};

exports.cockpitStore = function(req, res) {
  var paramsquery;
console.log('storess');
  paramsquery = {
    deployment_id: req.query.deployment_id
  }
 
  Store.find(paramsquery, {storeName:1,_id:1,isMain:1,isKitchen:1},function (err, stores) {
    if(err) { return handleError(res, err); }
    //console.log(stores);
    return res.json(200, stores);
  });
};
/****************************************************/
