'use strict';

var express = require('express');
var controller = require('./store.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/cockpitStore', controller.cockpitStore);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/pushStockStore', controller.pushStockStore);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;

//for open API
router.use('/partner/*', auth.onlyOpenApi);

router.post('/partner/stockStore', auth.hasCustomerKey, controller.pushStockStore_secure);
router.delete('/partner/stockStore', auth.hasCustomerKey, controller.deleteStockStore_secure);
