/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockRequirement = require('./stockRequirement.model');

exports.register = function(socket) {
  StockRequirement.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockRequirement.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockRequirement:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockRequirement:remove', doc);
}