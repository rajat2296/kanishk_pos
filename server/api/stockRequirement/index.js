'use strict';

var express = require('express');
var controller = require('./stockRequirement.controller');

var router = express.Router();
//modified
//router.get('/', controller.index);
router.get('/', controller.getInitialData);
router.get('/getConsolidatedInventoryReport', controller.getConsolidatedInventoryReport);
router.get('/getRequirements', controller.index);
router.get('/getCount', controller.getCount);
router.get('/getRequirementsPagination', controller.getRequirementsPagination);
router.get('/getLastRequirementBillNo', controller.getLastRequirementBillNo);
router.get('/getRequirementReport', controller.getRequirementReport);
router.get('/getSuplyRequirementReport', controller.getSuplyRequirementReport);
//modified
router.get('/getItemsInSelectedRequirements', controller.getItemsInSelectedRequirements);
router.get('/checkIfItemExistsIndentExists', controller.checkIfItemExistsIndentExists);
router.get('/getStoreNItems/:id', controller.getStoreNItems);
//end
router.get('/:id', controller.show);
//router.post('/', controller.create);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.post('/:id', controller.createWithIdMultipart);
router.delete('/:id', controller.destroy);

module.exports = router;
