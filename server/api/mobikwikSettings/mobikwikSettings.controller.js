'use strict';

var _ = require('lodash');
var MobikwikSettings = require('./mobikwikSettings.model');

// Get list of mobikwikSettingss
exports.index = function(req, res) {
  MobikwikSettings.find({deployment_id:req.query.deployment_id,tenant_id:req.query.tenant_id},function (err, mobikwikSettings) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(mobikwikSettings);
  });
};

// Get a single mobikwikSettings


// Creates a new mobikwikSettings in the DB.
exports.create = function(req, res) {
   console.log("entered");
  MobikwikSettings.create(req.body, function(err, mobikwikSettings) {
    console.log("asved");
    if(err) { return handleError(res, err); }
    return res.status(200).json(mobikwikSettings);
  });
};

exports.update=function(req,res){
  console.log("entered",req.body);
  MobikwikSettings.findOne({deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id},function(err,data){
    if(data){
      data.merchant_id=req.body.merchant_id; data.secret_key=req.body.secret_key; data.merchant_name=req.body.merchant_name;
      data.save(function(err,data){
        if(data) res.status(200).send({"updated":true});
        else return handleError(res,err);
      })
    }
    else return handleError(res,{"updated":false});
  });
}


function handleError(res, err) {
  return res.status(500).send(err);
}
