/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var MobikwikSettings = require('./mobikwikSettings.model');

exports.register = function(socket) {
  MobikwikSettings.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  MobikwikSettings.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('mobikwikSettings:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('mobikwikSettings:remove', doc);
}