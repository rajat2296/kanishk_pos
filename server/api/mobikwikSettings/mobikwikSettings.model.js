'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MobikwikSettingsSchema = new Schema({
  merchant_name: String,
  merchant_id: String,
  store_id: String,
  pos_id: String,
  secret_key: String,
  deployment_id:String,
  tenant_id:String
});

module.exports = mongoose.model('MobikwikSettings', MobikwikSettingsSchema);
