'use strict';

var express = require('express');
var controller = require('./mobikwikSettings.controller');

var router = express.Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.post('/update',controller.update);

module.exports = router;
