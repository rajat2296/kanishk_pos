/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Smstemplate = require('./smstemplate.model');

exports.register = function(socket) {
  Smstemplate.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Smstemplate.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('smstemplate:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('smstemplate:remove', doc);
}