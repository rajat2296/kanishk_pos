'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SmstemplateSchema = new Schema({
  type: String,
  content: String,
  templateNumber: Number,
  created:{type: Date, default: Date.now}

});
SmstemplateSchema
    .path('templateNumber')
    .validate(function (value, respond) {
        var self = this;
        if (this.isNew) {
            this.constructor.findOne({templateNumber:value}, function (err, i) {

                if (err) throw err;
                if (i) {
                    if (self.templateNumber === i.templateNumber) return respond(false);
                    return respond(false);
                }
                respond(true);
            });
        } else {
            respond(true);
        }


    }, 'Duplicate Template Number, please use another template number.');

module.exports = mongoose.model('Smstemplate', SmstemplateSchema);