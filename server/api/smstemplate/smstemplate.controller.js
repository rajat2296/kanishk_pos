'use strict';

var _ = require('lodash');
var Smstemplate = require('./smstemplate.model');

// Get list of smstemplates
exports.index = function(req, res) {
  Smstemplate.find(function (err, smstemplates) {
    if(err) { return handleError(res, err); }
    return res.json(200, smstemplates);
  });
};

// Get a single smstemplate
exports.show = function(req, res) {
  Smstemplate.findById(req.params.id, function (err, smstemplate) {
    if(err) { return handleError(res, err); }
    if(!smstemplate) { return res.send(404); }
    return res.json(smstemplate);
  });
};

// Creates a new smstemplate in the DB.
exports.create = function(req, res) {
  Smstemplate.create(req.body, function(err, smstemplate) {
    if(err) { return handleError(res, err); }
    return res.json(201, smstemplate);
  });
};

// Updates an existing smstemplate in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Smstemplate.findById(req.params.id, function (err, smstemplate) {
    if (err) { return handleError(res, err); }
    if(!smstemplate) { return res.send(404); }
    var updated = _.merge(smstemplate, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, smstemplate);
    });
  });
};

// Deletes a smstemplate from the DB.
exports.destroy = function(req, res) {
  Smstemplate.findById(req.params.id, function (err, smstemplate) {
    if(err) { return handleError(res, err); }
    if(!smstemplate) { return res.send(404); }
    smstemplate.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}