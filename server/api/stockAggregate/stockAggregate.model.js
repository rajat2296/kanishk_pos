'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockAggregateSchema = new Schema({
  tenant_id: String,
  deployment_id: String,
  store_id: String,
  created: Date,
  date: {
  	type: Date,
  	default: new Date()
  },
  purchase: Number,
  purchaseAmount: Number,
  consumption: Number,
  consumptionAmount: Number,
  wastage: Number,
  wastageAmt: Number,
  totalPurchaseQty: Number,
  totalPurchaseAmount: Number,
  totalIntermediateQty: Number,
  totalIntermediateAmt: Number,
  totalSaleQty: Number,
  totalSaleAmount: Number,
  totalTransferQty_toStore: Number,
  totalTransferAmount_toStore: Number,
  totalTransferQty: Number,
  totalTransferAmount: Number,
  itemSaleQty: Number,
  itemSaleAmt: Number,
  totalStockReceiveQty: Number,
  totalStockReceiveAmount: Number,
  totalStockReturnQty: Number,
  totalStockReturnAmount: Number,
  totalIndentingChallanQty:Number,
  totalIndentingChallanAmt: Number,
  itemId: String,
  itemName: String,
  UnitName: String
});

module.exports = mongoose.model('StockAggregate', StockAggregateSchema);