'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockStatusSchema = new Schema({
  tenant_id: String,
  deployment_id: String,
  store_id: String,
  created: Date,
  status: {
  	type: String,
  	default: 'NA'
  },
  date: {
  	type: Date,
  	defalut: new Date()
  }
});

module.exports = mongoose.model('StockStatus', StockStatusSchema);