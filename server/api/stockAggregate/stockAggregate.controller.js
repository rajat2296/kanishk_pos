'use strict';

var _ = require('lodash');
var StockAggregate = require('./stockAggregate.model');
var StockStatus = require('./stockStatus.model');
var Tenant = require('../tenant/tenant.model');
var Deployment = require('../deployment/deployment.model');
var Store = require('../store/store.model');
var StockTransaction = require('../stockTransaction/stockTransaction.model');
var BaseKitchenItem = require('../baseKitchenItem/baseKitchenItem.model');
var StockRequirement = require('../stockRequirement/stockRequirement.model');
var StockItem = require('../stockItem/stockItem.model');
var Bill = require('../bill/bill.model');
var StockRecipe = require('../stockRecipe/stockRecipe.model');
var ObjectId = require('mongoose').Types.ObjectId;
var Async = require('async');
var Q = require('q');
var moment = require('moment');

exports.getData = function (req, res) {

  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var store_id = req.query.store_id;
  var startDate = moment(new Date(req.query.startDate)).toDate();
  var endDate = moment(new Date(req.query.endDate)).toDate();
  var deferred = Q.defer();
  var data = [];
  var minDate = new Date(startDate);
  var returnData = {beforeDate: [], betweenDate: []};

  console.log(tenant_id, deployment_id, store_id, startDate, endDate);

  getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, store_id).then(function (result_op) {
    if (result_op.length > 0) {
      //console.log('length > 0')
      _.forEach(result_op, function (op, i) {
        //console.log('opeening date',op.created);
        var tempDate = moment(new Date(op.created)).toDate();
        var startOfOpeningDay = moment(new Date(op.created)).startOf('day').toDate();
        startOfOpeningDay.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        if (startOfOpeningDay > tempDate)
          moment(tempDate).subtract('days', 1);
        tempDate = moment(tempDate).startOf('day').toDate();
        tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        //console.log('tempDate', tempDate);
        //console.log('temp', tempDate);
        minDate = new Date(tempDate);
        op.created = minDate;
        data.push(op);
        //console.log(data);
      });
      //console.log('endDate', endDate);
      //console.log('minDate', minDate);
      if (new Date(endDate).getTime() <= new Date(minDate).getTime()) {
        returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
        //console.log("Report not generated due to latest opening is greater than selected end Date");
        return res.json(returnData);
      }
      else if (new Date(minDate).getTime() <= new Date(startDate).getTime()) {
        //console.log('----------------------');
        //console.log('else if');

        //console.log("op < startDate");

        Q.all([
          dataInDateRange(minDate, startDate, deployment_id, tenant_id, store_id), // Data Before Date
          dataInDateRange(startDate, endDate, deployment_id, tenant_id, store_id)  // Data Between Date
        ]).then(function (result) {
          //console.log('dataInDateRange found');
          returnData.beforeDate = result[0];
          _.forEach(data, function (d, i) {
            returnData.beforeDate.push(d);
          });
          returnData.betweenDate = result[1];
          return res.json(returnData);
        }).catch(function (err) {
          return handleError(res, err);
        });

        // dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
        //   returnData.beforeDate = value;
        //   _.forEach(data, function (d, i) {
        //     returnData.beforeDate.push(d);
        //   });
        //   dataBetweenDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
        //     returnData.betweenDate = value;
        //     return res.json(returnData);
        //   }, function (err) {
        //     return handleError(res, err)
        //   });
        // }, function (err) {
        //   return handleError(res, err)
        // });
      }
      else {
        //console.log('----------------------');
        //console.log('else');
        //console.log("op > startDate and op < end");

        Q.all([
          dataInDateRange(minDate, startDate, deployment_id, tenant_id, store_id), // Data Before Date
          dataInDateRange(minDate, endDate, deployment_id, tenant_id, store_id)  // Data Between Date
        ]).then(function (result) {
          //console.log('dataInDateRange found')
          returnData.beforeDate = result[0];
          _.forEach(data, function (d, i) {
            returnData.beforeDate.push(d);
          });
          returnData.betweenDate = result[1];
          return res.json(returnData);
        }).catch(function (err) {
          return handleError(res, err);
        });

        // dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
        //   //console.log(value)
        //   returnData.beforeDate = value;
        //   _.forEach(data, function (d, i) {
        //     returnData.beforeDate.push(d);
        //   });
        //   dataBetweenDate(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
        //     //console.log(value);
        //     returnData.betweenDate = value;
        //     return res.json(returnData);
        //   }, function (err) {
        //     return handleError(res, err)
        //   });
        // }, function (err) {
        //   return handleError(res, err)
        // });
      }
    }
    else {
      return res.json(returnData);
    }
  }, function (err) {
    //console.log(err);
    returnData.errorMessage = err;
    //console.log("Report not generated due to latest opening is greater than selected end Date");
    return res.json(returnData);
    //return handleError(res, err);
  });
}

exports.getFoodCostData = function (req, res) {
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  
  var startDate = moment(new Date(req.query.startDate)).toDate();
  var endDate = moment(new Date(req.query.endDate)).toDate();
  var minDate = new Date(startDate);
  var outlet=[],arr=[];
  var store_id
  console.log("foodCost")
  var tenantQuery={}
  tenantQuery.deployment_id = deployment_id
  tenantQuery.tenant_id = tenant_id
  tenantQuery.isKitchen = true
  Store.find(tenantQuery, {_id: 1,storeName:1}, function (err, stores) {
    
 // getStores(deployment_id,tenant_id).then(function(stores){
    console.log("stores",stores)
    Async.each(stores, function (stores, callback) {
        console.log("inside Async")
        store_id=stores._id;
        var storeName=stores.storeName
        storewiseData(deployment_id,tenant_id,stores._id,startDate,endDate).then(function(storewiseData){
          var store={}
          store.store_id=store_id
          store.storeName=storeName
          console.log("storeWise Returned")

          store.data=storewiseData
          
          outlet.push(store)
          console.log('storewiseData', storewiseData.beforeDate.length)
          console.log('storewiseData', storewiseData.betweenDate.length)

          callback()
        }, function (err) {
          callback(err)
        })
      }, 
      function (err) {
          //console.log(err);
          if(err)
        {
          console.log("err in Async")
          return res.status(200).json({message: 'success Stores End'});
        }
        else
        {
          console.log("async success")
          console.log(outlet)
          return res.status(200).json(outlet)
          //callback()
          //callback(arr)
          //callback(outlet)
          
        }
       })
    console.log("get stores outlet")
    

  });
  
 //return res.json(outlet)
};

function storewiseData(deployment_id, tenant_id,store_id,startDate,endDate) {
  console.log("storewise Function")
  var deferred = Q.defer();
  var data = [];
  var returnData = {beforeDate: [], betweenDate: []};
  var startDate=new Date(startDate)
  var endDate=new Date(endDate)
  var tenant_id=tenant_id
  var deployment_id=deployment_id
  var store_id=store_id

  var minDate = new Date(startDate);
  //console.log(tenant_id, deployment_id, store_id, startDate, endDate);
  // console.log(store_id)
  // console.log(deployment_id)
  // console.log(tenant_id)
  console.log("opening dates")
  //console.log(storeName)
  // console.log(startDate)
  // console.log(endDate)
  //var store_id="580dc29ebdb6aa4c0f245079"
  getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, store_id.toString()).then(function (result_op) {
    console.log("opening length",result_op.length)
    if (result_op.length > 0) {
      console.log('length > 0')
      _.forEach(result_op, function (op, i) {

        console.log('opeening date',op.created);
        var tempDate = moment(new Date(op.created)).toDate();
        var startOfOpeningDay = moment(new Date(op.created)).startOf('day').toDate();
        startOfOpeningDay.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        if(startOfOpeningDay > tempDate)
          moment(tempDate).subtract('days', 1);
        tempDate = moment(tempDate).startOf('day').toDate();
        tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        //console.log('tempDate', tempDate);
        //console.log('temp', tempDate);
        minDate = new Date(tempDate);
        op.created = minDate;
        data.push(op);
        //console.log(data);
      });
      console.log("min Date");
      console.log('endDate', endDate);
      console.log('minDate', minDate);
      console.log('startDate', startDate);
      if (new Date(endDate).getTime() <= new Date(minDate).getTime()) {
        returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
        //console.log("Report not generated due to latest opening is greater than selected end Date");
        deferred.reject(returnData);
      }
      else if (new Date(minDate).getTime() <= new Date(startDate).getTime()) {
        //console.log('----------------------');
        //console.log('else if');

        //console.log("op < startDate");

        Q.all([
            dataInDateRangeFoodCost(minDate, startDate, deployment_id, tenant_id, store_id), // Data Before Date
            dataInDateRangeFoodCost(startDate, endDate, deployment_id, tenant_id, store_id)  // Data Between Date          
          ]).then(function (result) {
            console.log('dataInDateRange found');
            returnData.beforeDate = result[0];
            console.log("returnData.beforeDate",returnData.beforeDate.length)
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            returnData.betweenDate = result[1];
            console.log("returnData.betweenDate",returnData.betweenDate.length)
            deferred.resolve(returnData)
          }).catch(function (err) {
            deferred.reject(err)
          }); 

        
      }
      else {
       

        Q.all([
            dataInDateRangeFoodCost(minDate, startDate, deployment_id, tenant_id, store_id), // Data Before Date
            dataInDateRangeFoodCost(minDate, endDate, deployment_id, tenant_id, store_id)  // Data Between Date          
          ]).then(function (result) {
            //console.log('dataInDateRange found')
            returnData.beforeDate = result[0];
            console.log("returnData.beforeDate",returnData.beforeDate.length)
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            returnData.betweenDate = result[1];
            console.log("returnData.betweenDate",returnData.betweenDate.length)
            deferred.resolve(returnData)
          }).catch(function (err) {
            deferred.reject(err)
          }); 

       
      }
    }
    else {
      console.log("opening not found");
      deferred.resolve(returnData)
    }
  }, function (err) {
    //console.log(err);
    deferred.reject(err)
    console.log("Report not generated due to latest opening is greater than selected end Date");
    //return returnData;
    //return handleError(res, err);
  });
  return deferred.promise
 };



exports.calculateDayAggregateFromLatestOpening = function (req, res) {
  console.log("--------------------------------------------------")
  //console.log('req.body',req.body);
  var tenantQuery = {
    active: true
  };
  var sDate;
  if (req.body.tenant_id)
    tenantQuery._id = req.body.tenant_id;
  if (req.body.startDate)
    sDate = new Date(req.body.startDate);
  else
    sDate = new Date();
  //console.log('sDate',sDate)
  Tenant.find(tenantQuery, {_id: 1}, function (err, tenants) {
    if (err)
      return handleError(res, err);
    console.log('Tenants', tenants.length);
    Async.each(tenants, function (tenant, callback) {
      var deploymentQuery = {
        tenant_id: tenant._id
      };
      if (req.body.deployment_id)
        deploymentQuery._id = req.body.deployment_id;
      Deployment.find(deploymentQuery, {
        _id: 1,
        name: 1,
        "settings.name": 1,
        "settings.value": 1
      }, function (err, deployments) {
        if (err)
          callback(err);
        else {
          //console.log('Deployments', deployments.length);
          Async.each(deployments, function (deployment, callbac) {
            console.log('deployment.name', deployment.name);
            var cutOffSetting = _.find(deployment.settings, function (setting) {
              return setting.name == 'day_sale_cutoff';
            });

            var lastPrice = [];
            Q.all([
              getAllRawMaterials_ForRecipePricingConsumptionReport(deployment._id, tenant._id),
              getAllRawMaterials_ForRecipePricingBaseKitchenConsumptionReport(deployment._id, tenant._id)
            ]).then(function (prices) {
              //callback();
              //console.log(prices);
              //console.log('prices', prices[0].length, prices[1].length)
              _.forEach(prices[0], function (v) {
                lastPrice.push(v);
              });
              _.forEach(prices[1], function (v) {
                lastPrice.push(v);
              });

              cutOffSetting = new Date(cutOffSetting.value);
              //console.log('cutOffSetting', cutOffSetting)
              var startDate = moment(new Date(sDate)).startOf('day').toDate();
              startDate = new Date(startDate.setHours(cutOffSetting.getHours(), cutOffSetting.getMinutes(), 0, 0));

              var endDate = new Date(moment(new Date(startDate)).add('days', 1));
              //console.log('startDate', new Date(startDate), 'endDate', endDate);

              /*var startDate = sDate;
               startDate = new Date(startDate.setHours(0, 0, 0, 0))
               var endDate = new Date(startDate);
               endDate = new Date(endDate.setDate(endDate.getDate() + 1))
               *///console.log(startDate, endDate);
              var storeQuery = {
                deployment_id: deployment._id
              };
              if (req.body.store_id)
                storeQuery._id = req.body.store_id;
              Store.find(storeQuery, {_id: 1, isKitchen: 1, isMain: 1, storeName: 1}, function (err, stores) {
                if (err)
                  callbac(err);
                else {
                  //console.log('Stores', stores.length);
                  Async.each(stores, function (store, callback) {
                    //console.log('checking for ', store.storeName);
                    //console.log(startDate, deployment._id.toString(), tenant._id.toString(), store._id.toString())
                    getLatestOpening(startDate, deployment._id.toString(), tenant._id.toString(), store._id.toString()).then(function (result) {
                      
                      //console.log('store_id', store._id);
                      if(!result[0])
                        {
                          //console.log('!result[0]')
                          callback();
                        }
                      var dateArray = [];
                      var openingDate = moment(new Date(result[0].created)).startOf('day').toDate();
                      openingDate = new Date(openingDate.setHours(cutOffSetting.getHours(), cutOffSetting.getMinutes(), 0, 0));
                      var currentDate = moment(new Date()).startOf('day').toDate();
                      currentDate = new Date(currentDate.setHours(cutOffSetting.getHours(), cutOffSetting.getMinutes(), 0, 0));
                      dateArray.push(new Date(openingDate));
                      //console.log(openingDate,currentDate)
                      while (openingDate.getTime() != currentDate.getTime()) {
                        openingDate = new Date(moment(new Date(openingDate)).add('days', 1));
                        dateArray.push(new Date(openingDate));
                      }
                      Async.each(dateArray, function (date, callback) {
                        //console.log(date, eDate, deployment._id.toString(), tenant._id.toString(), store._id.toString ());
                        //console.log('storeName',store.storeName);
                        var eDate = new Date(moment(new Date(date)).add('days', 1));
                        getConsumptionAndPurchase(date, eDate, deployment._id.toString(), tenant._id.toString(), store._id.toString(), store.isKitchen, lastPrice).then(function (items) {
                           // console.log('items', items.length);
                          Async.each(items, function (item, callback) {
                            var saveItem = {
                              tenant_id: tenant._id,
                              deployment_id: deployment._id,
                              store_id: store._id,
                              created: item.created,
                              itemId: item.itemId,
                              itemName: item.itemName,
                              UnitName: item.UnitName,
                              date:date
                            };
                            console.log("itemmmm")
                            if (!item.purchaseQty || isNaN(item.purchaseQty) || !item.consumptionQty || isNaN(item.consumptionQty) || !item.wastageQty || isNaN(item.wastageQty) || !item.purchaseAmount || isNaN(item.purchaseAmount) || !item.consumptionAmount || isNaN(item.consumptionAmount) || !item.wastageAmt || isNaN(item.wastageAmt) || !item.totalPurchaseQty || isNaN(item.totalPurchaseQty) || !item.totalPurchaseAmount || isNaN(item.totalPurchaseAmount)
                              || !item.totalIntermediateQty || isNaN(item.totalIntermediateQty) || !item.totalIntermediateAmt || isNaN(item.totalIntermediateAmt) || !item.totalSaleQty || isNaN(item.totalSaleQty) || !item.totalSaleAmount || isNaN(item.totalSaleAmount) || !item.totalTransferQty_toStore || isNaN(item.totalTransferQty_toStore)
                              || !item.totalTransferAmount_toStore || isNaN(item.totalTransferAmount_toStore) || !item.totalTransferQty || isNaN(item.totalTransferQty) || !item.totalTransferAmount || isNaN(item.totalTransferAmount) || !item.itemSaleQty || isNaN(item.itemSaleQty) || !item.itemSaleAmt || isNaN(item.itemSaleAmt)
                              || !item.totalStockReceiveQty || isNaN(item.totalStockReceiveQty) || !item.totalStockReceiveAmount || isNaN(item.totalStockReceiveAmount) || !item.totalStockReturnQty || isNaN(item.totalStockReturnQty) || !item.totalStockReturnAmount || isNaN(item.totalStockReturnAmount) || !item.totalIndentingChallanQty || isNaN(item.totalIndentingChallanQty) || !item.totalIndentingChallanAmt || isNaN(item.totalIndentingChallanAmt)  )
                              {
                                saveItem.status = 'Failed';
                                console.log("Failed",item.itemName)
                              }
                            else {
                              console.log("-------")
                              console.log(saveItem.itemName);
                              saveItem.status = 'Finished';
                              saveItem.purchase = item.purchaseQty;
                              saveItem.purchaseAmount = item.purchaseAmount;
                              saveItem.consumption = item.consumptionQty;
                              saveItem.consumptionAmount = item.consumptionAmount;
                              saveItem.wastage = item.wastageQty;
                              saveItem.wastageAmt = item.wastageAmt;
                              saveItem.totalPurchaseQty = item.totalPurchaseQty;
                              saveItem.totalPurchaseAmount = item.totalPurchaseAmount;
                              saveItem.totalIntermediateQty = item.totalIntermediateQty;
                              saveItem.totalIntermediateAmt = item.totalIntermediateAmt;
                              saveItem.totalSaleQty = item.totalSaleQty;
                              saveItem.totalSaleAmount = item.totalSaleAmount;
                              saveItem.totalTransferQty_toStore = item.totalTransferQty_toStore;
                              saveItem.totalTransferAmount_toStore = item.totalTransferAmount_toStore;
                              saveItem.totalTransferQty = item.totalTransferQty;
                              saveItem.totalTransferAmount = item.totalTransferAmount;
                              saveItem.itemSaleQty = item.itemSaleQty;
                              saveItem.itemSaleAmt = item.itemSaleAmt;
                              saveItem.totalStockReceiveQty = item.totalStockReceiveQty;
                              saveItem.totalStockReceiveAmount = item.totalStockReceiveAmount;
                              saveItem.totalStockReturnQty = item.totalStockReturnQty;
                              saveItem.totalStockReturnAmount = item.totalStockReturnAmount;
                              saveItem.totalIndentingChallanQty= item.totalIndentingChallanQty;
                              saveItem.totalIndentingChallanAmt= item.totalIndentingChallanAmt;
                            }
                            //console.log('86', saveItem.itemName);
                            if (saveItem.status == 'Finished') {
                               StockAggregate.findOne(
                                {
                                  itemId: saveItem.itemId,
                                  date: date,
                                  deployment_id: deployment._id.toString(),
                                  store_id: store._id.toString()
                                }, function (err, Aggregate) {
                                 if(Aggregate)
                                      //console.log("founddddddddd")
                                    if (err) { console.log(err); }
                                     if(!Aggregate)
                                     {  


                                        StockAggregate.create(saveItem, function(err, Stock) {
                                          if(err) { 
                                            console.log("err",err);
                                            callback()
                                          }
                                          else
                                          {
                                            console.log("item Saved")
                                            callback()  
                                          }
                                        });
                                    }
                                    else
                                    {

                                    var updated = _.extend(Aggregate, saveItem);
                                    //console.log();
                                    if(Aggregate)
                                      updated.save(function (err) {
                                        //console.log("updateddddddddddd")
                                        if (err) { 
                                          console.log('error', err)
                                          callback(err);
                                        }
                                        else
                                          {console.log("updated");callback();}
                                      });

                                    }
                                  });
                            }
                          }, function (err) {
                                  //console.log("items async finished",store.storeName);

                            if (err) {
                              StockStatus.update({
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate
                              }, {
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate,
                                status: 'Failed'
                              }, {upsert: true}, function (err) {
                                //console.log('storeName 327', store.storeName);
                                callback();
                              });
                            } else {
                              StockStatus.update({
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate
                              }, {
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate,
                                status: 'Finished'
                              }, {upsert: true}, function (err) {
                                //console.log('storeName 343', store.storeName);
                                callback();
                              });
                            }
                          });
                        }).catch(function (err) {
                          console.log('err',err)
                          callback(err);
                        });
                      }, function (err) {
                              //console.log("date array async finished",store.storeName);

                        if(err)
                          callback(err);
                        else
                          callback();
                      });
                    })

                  }, function (err) {
                          //console.log("stores async finished ");

                    if (err)
                      callbac(err);
                    else
                      callbac();
                  });
                }
              });
            }, function (err) {
              callbac(err);
            });
          }, function (err) {
            console.log("deployment async finished ");

            if (err)
              callback(err);
            else
              callback();
          });
        }
      });
    }, function (err) {
     console.log("tenant async finished");
      console.log(err);
      return res.status(200).json({message: 'success'});
    });
  });
};


exports.calculateDayAggregateFromLatestOpening2 = function (req, res) {
  console.log("------------------------------")
 // console.log('req.body', req.body);
  var tenantQuery = {
    active: true
  };
  var sDate;
  if (req.body.tenant_id)
    tenantQuery._id = req.body.tenant_id;
  if (req.body.startDate)
    sDate = new Date(req.body.startDate);
  else
    sDate = new Date();

  Tenant.find(tenantQuery, {_id: 1}, function (err, tenants) {
    if (err)
      return handleError(res, err);
    //console.log('Tenants', tenants.length);
    Async.each(tenants, function (tenant, callback) {
      var deploymentQuery = {
        tenant_id: tenant._id
      };
      if (req.body.deployment_id)
        deploymentQuery._id = req.body.deployment_id;
      Deployment.find(deploymentQuery, {_id: 1, "settings.name": 1, "settings.value": 1}, function (err, deployments) {
        if (err)
          callback(err);
        else {
          //console.log('Deployments', deployments.length);
          Async.each(deployments, function (deployment, callback) {
            var cutOffSetting = _.find(deployment.settings, function (setting) {
              return setting.name == 'day_sale_cutoff';
            });

            var lastPrice = [];
            Q.all([
              getAllRawMaterials_ForRecipePricingConsumptionReport(deployment._id, tenant._id),
              getAllRawMaterials_ForRecipePricingBaseKitchenConsumptionReport(deployment._id, tenant._id)
            ]).then(function (prices) {
              //console.log(prices);
              //console.log('prices', prices[0].length, prices[1].length)
              _.forEach(prices[0], function (v) {
                lastPrice.push(v);
              });
              _.forEach(prices[1], function (v) {
                lastPrice.push(v);
              });

              cutOffSetting = new Date(cutOffSetting.value);
              //console.log('cutOffSetting', cutOffSetting)
              var startDate = moment(new Date(sDate)).startOf('day').toDate();
              startDate = new Date(startDate.setHours(cutOffSetting.getHours(), cutOffSetting.getMinutes(), 0, 0));

              var endDate = new Date(moment(new Date(startDate)).add('days', 1));
              //console.log('startDate', new Date(startDate), 'endDate', endDate);

              /*var startDate = sDate;
               startDate = new Date(startDate.setHours(0, 0, 0, 0))
               var endDate = new Date(startDate);
               endDate = new Date(endDate.setDate(endDate.getDate() + 1))
               *///console.log(startDate, endDate);
              var storeQuery = {
                deployment_id: deployment._id
              };
              if (req.body.store_id)
                storeQuery._id = req.body.store_id;
              Store.find(storeQuery, {_id: 1, isKitchen: 1, isMain: 1, storeName: 1}, function (err, stores) {
                if (err)
                  callback(err);
                else {
                  //console.log('Stores', stores.length);
                  Async.each(stores, function (store, callback) {
                   getLatestOpening(startDate, deployment._id.toString(), tenant._id.toString(), store._id.toString()).then(function (result) {
                    if(!result[0])
                        {
                          //console.log('!result[0]')
                          callback();
                        }
                      var dateArray = [];
                      var openingDate = moment(new Date(result[0].created)).startOf('day').toDate();
                      openingDate = new Date(openingDate.setHours(cutOffSetting.getHours(), cutOffSetting.getMinutes(), 0, 0));
                      var currentDate = moment(new Date()).startOf('day').toDate();
                      currentDate = new Date(currentDate.setHours(cutOffSetting.getHours(), cutOffSetting.getMinutes(), 0, 0));
                      dateArray.push(new Date(openingDate));
                      //console.log(openingDate,currentDate)
                      while (openingDate.getTime() != currentDate.getTime()) {
                        openingDate = new Date(moment(new Date(openingDate)).add('days', 1));
                        dateArray.push(new Date(openingDate));
                      }
                       Async.each(dateArray, function (date, callback) {
                        //callback();
                        var eDate = new Date(moment(new Date(date)).add('days', 1));
                        getConsumptionAndPurchase(date, eDate, deployment._id.toString(), tenant._id.toString(), store._id.toString (), store.isKitchen, lastPrice).then(function (items) {
                          //console.log('items', items.length);
                          //callback()
                           Async.each(items, function (item, callback) {
                            //callback();
                            var saveItem = {
                              tenant_id: tenant._id,
                              deployment_id: deployment._id,
                              store_id: store._id,
                              created: item.created,
                              itemId: item.itemId,
                              itemName: item.itemName,
                              UnitName: item.UnitName
                            };
                            if (!item.purchaseQty || isNaN(item.purchaseQty) || !item.consumptionQty || isNaN(item.consumptionQty) || !item.wastageQty || isNaN(item.wastageQty) || !item.purchaseAmount || isNaN(item.purchaseAmount) || !item.consumptionAmount || isNaN(item.consumptionAmount) || !item.wastageAmt || isNaN(item.wastageAmt) || !item.totalPurchaseQty || isNaN(item.totalPurchaseQty) || !item.totalPurchaseAmount || isNaN(item.totalPurchaseAmount)
                              || !item.totalIntermediateQty || isNaN(item.totalIntermediateQty) || !item.totalIntermediateAmt || isNaN(item.totalIntermediateAmt) || !item.totalSaleQty || isNaN(item.totalSaleQty) || !item.totalSaleAmount || isNaN(item.totalSaleAmount) || !item.totalTransferQty_toStore || isNaN(item.totalTransferQty_toStore)
                              || !item.totalTransferAmount_toStore || isNaN(item.totalTransferAmount_toStore) || !item.totalTransferQty || isNaN(item.totalTransferQty) || !item.totalTransferAmount || isNaN(item.totalTransferAmount) || !item.itemSaleQty || isNaN(item.itemSaleQty) || !item.itemSaleAmt || isNaN(item.itemSaleAmt)
                              || !item.totalStockReceiveQty || isNaN(item.totalStockReceiveQty) || !item.totalStockReceiveAmount || isNaN(item.totalStockReceiveAmount) || !item.totalStockReturnQty || isNaN(item.totalStockReturnQty) || !item.totalStockReturnAmount || isNaN(item.totalStockReturnAmount))
                              saveItem.status = 'Failed';
                            else {
                              console.log(saveItem.itemName);
                              saveItem.status = 'Finished';
                              saveItem.purchase = item.purchaseQty;
                              saveItem.purchaseAmount = item.purchaseAmount;
                              saveItem.consumption = item.consumptionQty;
                              saveItem.consumptionAmount = item.consumptionAmount;
                              saveItem.wastage = item.wastageQty;
                              saveItem.wastageAmt = item.wastageAmt;
                              saveItem.totalPurchaseQty = item.totalPurchaseQty;
                              saveItem.totalPurchaseAmount = item.totalPurchaseAmount;
                              saveItem.totalIntermediateQty = item.totalIntermediateQty;
                              saveItem.totalIntermediateAmt = item.totalIntermediateAmt;
                              saveItem.totalSaleQty = item.totalSaleQty;
                              saveItem.totalSaleAmount = item.totalSaleAmount;
                              saveItem.totalTransferQty_toStore = item.totalTransferQty_toStore;
                              saveItem.totalTransferAmount_toStore = item.totalTransferAmount_toStore;
                              saveItem.totalTransferQty = item.totalTransferQty;
                              saveItem.totalTransferAmount = item.totalTransferAmount;
                              saveItem.itemSaleQty = item.itemSaleQty;
                              saveItem.itemSaleAmt = item.itemSaleAmt;
                              saveItem.totalStockReceiveQty = item.totalStockReceiveQty;
                              saveItem.totalStockReceiveAmount = item.totalStockReceiveAmount;
                              saveItem.totalStockReturnQty = item.totalStockReturnQty;
                              saveItem.totalStockReturnAmount = item.totalStockReturnAmount;
                            }
                            //console.log('86', saveItem);
                            if (saveItem.status == 'Finished') {
                              StockAggregate.update({
                                itemId: saveItem.itemId,
                                date: startDate,
                                deployment_id: deployment._id.toString(),
                                store_id: store._id.toString()
                              }, saveItem, {upsert: true}, function (err) {
                                if (err) {
                                  console.log('error', err)
                                  callback(err);
                                }
                                else {
                                  console.log('name', saveItem.itemName);
                                  callback();
                                }
                              });
                            }
                            else
                            {
                              callback();
                            }

                          }, function (err) {
                            console.log("items async finished",store.storeName);
                                                        if (err) {
                              StockStatus.update({
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate
                              }, {
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate,
                                status: 'Failed'
                              }, {upsert: true}, function (err) {
                                console.log('storeName 327', store.storeName);
                                callback();
                              });
                            } else {
                              StockStatus.update({
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate
                              }, {
                                tenant_id: tenant._id,
                                deployment_id: deployment._id,
                                store_id: store._id,
                                date: startDate,
                                status: 'Finished'
                              }, {upsert: true}, function (err) {
                                console.log('storeName 343', store.storeName);
                                callback();
                              });
                            }
                          });//ending of items async
                        }).catch(function (err) {
                          console.log('err',err)
                          callback(err);
                        });//ending of getConsumption and Purchase
                      }, function (err) {
                              console.log("date array async finished",store.storeName);

                        if(err)
                          callback(err);
                        else
                          callback();
                      });//ending of adate array async
                    })//ending of get Latest Opening

                  }, function (err) {
                    if (err)
                      callback(err);
                    else
                      callback();
                  });
                }
              });
            });
          }, function (err) {
            if (err)
              callback(err);
            else
              callback();
          });
        }
      });
    }, function (err) {
      console.log(err);
      return res.status(200).json({message: 'success'});
    });
  });
};

exports.calculateDayAggregate = function (req, res) {

  console.log('req.body', req.body);
  var tenantQuery = {
    active: true
  };
  var sDate;
  if (req.body.tenant_id)
    tenantQuery._id = req.body.tenant_id;
  if (req.body.startDate)
    sDate = new Date(req.body.startDate);
  else
    sDate = new Date();

  Tenant.find(tenantQuery, {_id: 1}, function (err, tenants) {
    if (err)
      return handleError(res, err);
    console.log('Tenants', tenants.length);
    Async.each(tenants, function (tenant, callback) {
      var deploymentQuery = {
        tenant_id: tenant._id
      };
      if (req.body.deployment_id)
        deploymentQuery._id = req.body.deployment_id;
      Deployment.find(deploymentQuery, {_id: 1, "settings.name": 1, "settings.value": 1}, function (err, deployments) {
        if (err)
          callback(err);
        else {
          console.log('Deployments', deployments.length);
          Async.each(deployments, function (deployment, callback) {
            var cutOffSetting = _.find(deployment.settings, function (setting) {
              return setting.name == 'day_sale_cutoff';
            });

            var lastPrice = [];
            Q.all([
              getAllRawMaterials_ForRecipePricingConsumptionReport(deployment._id, tenant._id),
              getAllRawMaterials_ForRecipePricingBaseKitchenConsumptionReport(deployment._id, tenant._id)
            ]).then(function (prices) {
              //console.log(prices);
              //console.log('prices', prices[0].length, prices[1].length)
              _.forEach(prices[0], function (v) {
                lastPrice.push(v);
              });
              _.forEach(prices[1], function (v) {
                lastPrice.push(v);
              });

              cutOffSetting = new Date(cutOffSetting.value);
              //console.log('cutOffSetting', cutOffSetting)
              var startDate = moment(new Date(sDate)).startOf('day').toDate();
              startDate = new Date(startDate.setHours(cutOffSetting.getHours(), cutOffSetting.getMinutes(), 0, 0));

              var endDate = new Date(moment(new Date(startDate)).add('days', 1));
              //console.log('startDate', new Date(startDate), 'endDate', endDate);

              /*var startDate = sDate;
               startDate = new Date(startDate.setHours(0, 0, 0, 0))
               var endDate = new Date(startDate);
               endDate = new Date(endDate.setDate(endDate.getDate() + 1))
               *///console.log(startDate, endDate);
              var storeQuery = {
                deployment_id: deployment._id
              };
              if (req.body.store_id)
                storeQuery._id = req.body.store_id;
              Store.find(storeQuery, {_id: 1, isKitchen: 1, isMain: 1, storeName: 1}, function (err, stores) {
                if (err)
                  callback(err);
                else {
                  //console.log('Stores', stores.length);
                  Async.each(stores, function (store, callback) {
                    getConsumptionAndPurchase(startDate, endDate, deployment._id.toString(), tenant._id.toString(), store._id.toString(), store.isKitchen, lastPrice).then(function (items) {
                      //console.log('items', items.length);
                      Async.each(items, function (item, callback) {
                        var saveItem = {
                          tenant_id: tenant._id,
                          deployment_id: deployment._id,
                          store_id: store._id,
                          created: item.created,
                          itemId: item.itemId,
                          itemName: item.itemName,
                          UnitName: item.UnitName
                        };
                        if (!item.purchaseQty || isNaN(item.purchaseQty) || !item.consumptionQty || isNaN(item.consumptionQty) || !item.wastageQty || isNaN(item.wastageQty) || !item.purchaseAmount || isNaN(item.purchaseAmount) || !item.consumptionAmount || isNaN(item.consumptionAmount) || !item.wastageAmt || isNaN(item.wastageAmt) || !item.totalPurchaseQty || isNaN(item.totalPurchaseQty) || !item.totalPurchaseAmount || isNaN(item.totalPurchaseAmount)
                          || !item.totalIntermediateQty || isNaN(item.totalIntermediateQty) || !item.totalIntermediateAmt || isNaN(item.totalIntermediateAmt) || !item.totalSaleQty || isNaN(item.totalSaleQty) || !item.totalSaleAmount || isNaN(item.totalSaleAmount) || !item.totalTransferQty_toStore || isNaN(item.totalTransferQty_toStore)
                          || !item.totalTransferAmount_toStore || isNaN(item.totalTransferAmount_toStore) || !item.totalTransferQty || isNaN(item.totalTransferQty) || !item.totalTransferAmount || isNaN(item.totalTransferAmount) || !item.itemSaleQty || isNaN(item.itemSaleQty) || !item.itemSaleAmt || isNaN(item.itemSaleAmt)
                          || !item.totalStockReceiveQty || isNaN(item.totalStockReceiveQty) || !item.totalStockReceiveAmount || isNaN(item.totalStockReceiveAmount) || !item.totalStockReturnQty || isNaN(item.totalStockReturnQty) || !item.totalStockReturnAmount || isNaN(item.totalStockReturnAmount))
                          saveItem.status = 'Failed';
                        else {
                          //console.log(item);
                          saveItem.status = 'Finished';
                          saveItem.purchase = item.purchaseQty;
                          saveItem.purchaseAmount = item.purchaseAmount;
                          saveItem.consumption = item.consumptionQty;
                          saveItem.consumptionAmount = item.consumptionAmount;
                          saveItem.wastage = item.wastageQty;
                          saveItem.wastageAmt = item.wastageAmt;
                          saveItem.totalPurchaseQty = item.totalPurchaseQty;
                          saveItem.totalPurchaseAmount = item.totalPurchaseAmount;
                          saveItem.totalIntermediateQty = item.totalIntermediateQty;
                          saveItem.totalIntermediateAmt = item.totalIntermediateAmt;
                          saveItem.totalSaleQty = item.totalSaleQty;
                          saveItem.totalSaleAmount = item.totalSaleAmount;
                          saveItem.totalTransferQty_toStore = item.totalTransferQty_toStore;
                          saveItem.totalTransferAmount_toStore = item.totalTransferAmount_toStore;
                          saveItem.totalTransferQty = item.totalTransferQty;
                          saveItem.totalTransferAmount = item.totalTransferAmount;
                          saveItem.itemSaleQty = item.itemSaleQty;
                          saveItem.itemSaleAmt = item.itemSaleAmt;
                          saveItem.totalStockReceiveQty = item.totalStockReceiveQty;
                          saveItem.totalStockReceiveAmount = item.totalStockReceiveAmount;
                          saveItem.totalStockReturnQty = item.totalStockReturnQty;
                          saveItem.totalStockReturnAmount = item.totalStockReturnAmount;
                        }
                        console.log('86', saveItem.itemName);
                        StockAggregate.update({
                          itemId: saveItem.itemId,
                          date: startDate,
                          deployment_id: deployment._id.toString(),
                          store_id: store._id.toString()
                        }, saveItem, {upsert: true}, function (err) {
                          if (err) {
                            console.log('error', err)
                            callback(err);
                          }
                          else {
                            console.log('no error')
                            callback();
                          }
                        });
                      }, function (err) {
                        if (err) {
                          StockStatus.update({
                            tenant_id: tenant._id,
                            deployment_id: deployment._id,
                            store_id: store._id,
                            date: startDate
                          }, {
                            tenant_id: tenant._id,
                            deployment_id: deployment._id,
                            store_id: store._id,
                            date: startDate,
                            status: 'Failed'
                          }, {upsert: true}, function (err) {
                            callback();
                          });
                        } else {
                          StockStatus.update({
                            tenant_id: tenant._id,
                            deployment_id: deployment._id,
                            store_id: store._id,
                            date: startDate
                          }, {
                            tenant_id: tenant._id,
                            deployment_id: deployment._id,
                            store_id: store._id,
                            date: startDate,
                            status: 'Finished'
                          }, {upsert: true}, function (err) {
                            callback();
                          });
                        }
                      });
                    }).catch(function (err) {
                      callback(err);
                    });
                  }, function (err) {
                    if (err)
                      callback(err);
                    else
                      callback();
                  });
                }
              });
            });
          }, function (err) {
            if (err)
              callback(err);
            else
              callback();
          });
        }
      });
    }, function (err) {
      console.log(err);
      return res.status(200).json({message: 'success'});
    });
  });
};

function dataInDateRange(startDate, endDate, deployment_id, tenant_id, storeId) {

  console.log('dataInDateRange called');
  var deferred = Q.defer();
  Q.all([
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockAggregates(startDate, endDate, deployment_id, tenant_id, storeId)
  ]).then(function (result) {
    var transactions = [];
    _.forEach(result, function (val) {
      _.forEach(val, function (transaction) {
        transactions.push(transaction);
      });
    });
    deferred.resolve(transactions);
  }).catch(function (err) {
    deferred.reject(err);
  });
  return deferred.promise;
}

function dataInDateRangeFoodCost(startDate, endDate, deployment_id, tenant_id, storeId) {

  console.log('dataInDateRange called');
  var deferred = Q.defer();
  Q.all([
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockAggregatesFoodCost(startDate, endDate, deployment_id, tenant_id, storeId)
  ]).then(function (result) {
    var transactions = [];
    _.forEach(result, function (val) {
      _.forEach(val, function (transaction) {
        transactions.push(transaction);
      });
    });
    deferred.resolve(transactions);
  }).catch(function (err) {
    deferred.reject(err);
  });
  return deferred.promise;
}

function getStockAggregates(startDate, endDate, deployment_id, tenant_id, storeId) {

  console.log('getStockAggregates called');
  var deferred = Q.defer();
  StockAggregate.find({
    created: {$gte: startDate, $lt: endDate},
    deployment_id: deployment_id,
    tenant_id: tenant_id,
    store_id: storeId.toString()
  }, function (err, aggregates) {
    if (err)
      deferred.reject(err);
    else {
      //console.log(aggregates);
      deferred.resolve(aggregates);
    }
  });
  return deferred.promise;
}


function getStockAggregatesFoodCost(startDate, endDate, deployment_id, tenant_id, storeId) {

  console.log('getStockAggregates called');
  var deferred = Q.defer();
  StockAggregate.find({created: {$gte: startDate, $lt: endDate}, deployment_id: deployment_id, tenant_id: tenant_id, store_id: storeId},{itemId:1,created:1,itemName:1,UnitName:1,_id:1,date:1,deployment_id:1,tenant_id:1,store_id:1,wastageAmt:1,wastage:1,totalPurchaseQty:1,totalPurchaseAmount:1,totalIntermediateQty:1,totalIntermediateAmt:1,totalSaleQty:1,totalSaleAmount:1,totalTransferQty_toStore:1,totalTransferAmount_toStore:1,totalTransferQty:1,totalTransferAmount:1,itemSaleQty:1,itemSaleAmt:1,totalStockReceiveQty:1,totalStockReceiveAmount:1,totalStockReturnQty:1,totalStockReturnAmount:1,totalIndentingChallanQty:1,totalIndentingChallanAmt:1}, function (err, aggregates) {
    if(err)
      deferred.reject(err);
    else{
      //console.log(aggregates);
      deferred.resolve(aggregates);
    }
  });
  return deferred.promise;
}

function getLatestOpening(startDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  //console.log(tenant_id,deployment_id);
  var beforeDate = moment(new Date()).toDate();
  //var st = new Date(startDate);
  //var tst = moment(st).add('hours', 5.5).toDate();
  //console.log('tst', tst);
  var hour = new Date(startDate).getHours();
  var minutes = new Date(startDate).getMinutes();
  beforeDate.setHours(hour, minutes, 0, 0);
  //beforeDate.setHours(0,0,0,0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  /*console.log({
   transactionType: "6",
   tenant_id: new ObjectId(tenant_id),
   deployment_id: new ObjectId(deployment_id),
   created: {$lte: new Date(beforeDate)},
   "_store._id": storeId
   });*/
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(beforeDate)},
        "_store._id": storeId.toString()
      }
    },
    {
      $project: {
        created: 1
      }
    },
    {
      $sort: {created: -1}
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('getLatestOpening');
      console.log(err);
      d.reject('error');
    }
    /*else {
     //console.log(result);
     var checkDate;
     var data = [];
     var isOpeningValid = true;
     var st = moment(new Date(startDate)).toDate();
     _.forEach(result, function (itm, i) {
     if (i == 0) {
     checkDate = moment(new Date(itm.created)).toDate();
     if (new Date(checkDate) < moment(st).subtract('months', 2).toDate())
     isOpeningValid = false;
     }
     var compareDate = moment(new Date(itm.created)).toDate();
     if (checkDate.getTime() <= compareDate.getTime()) {
     data.push(itm);
     }
     });
     if (isOpeningValid)
     d.resolve(data);
     else
     d.reject("Your last opening is older than 2 months from your start date. Please enter a new opening!")
     }*/

    d.resolve(result);
  });
  return d.promise;
};

function getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  //console.log(tenant_id,deployment_id);
  var beforeDate = moment(new Date()).toDate();
  //var st = new Date(startDate);
  //var tst = moment(st).add('hours', 5.5).toDate();
  //console.log('tst', tst);
  var hour = new Date(startDate).getHours();
  var minutes = new Date(startDate).getMinutes();
  beforeDate.setHours(hour, minutes, 0, 0);
  //beforeDate.setHours(0,0,0,0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  //console.log(new Date(beforeDate));
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(beforeDate)},
        "_store._id": storeId.toString()
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('getOpeningStockByItem');
      console.log(err);
      d.reject('error');
    }
    else {
      //console.log(result);
      var checkDate;
      var data = [];
      var isOpeningValid = true;
      var st = moment(new Date(startDate)).toDate();
      _.forEach(result, function (itm, i) {
        if (i == 0) {
          checkDate = moment(new Date(itm.created)).toDate();
          if (new Date(checkDate) < moment(st).subtract('months', 2).toDate())
            isOpeningValid = false;
        }
        var compareDate = moment(new Date(itm.created)).toDate();
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      if (isOpeningValid)
        d.resolve(data);
      else
        d.reject("Your last opening is older than 2 months from your start date. Please enter a new opening!")
    }


  });
  return d.promise;
};

function getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId.toString()
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getPhysicalStockByItem');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log('getStockEntryByItem_ConsumptionReport')
  //console.log(startDate, endDate, deployment_id, tenant_id, storeId);
  //console.log(new Date(startDat));
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockEntryByItem_ConsumptionReport');
      console.log(err);
      d.reject('error');
    }
    //console.log("entryyyyyyyyyyyyy",result)
    //console.log('entry', result);
    d.resolve(result);

  });
  return d.promise;
};

function getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log('getStockSaleByItem_ConsumptionReport');
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $project: {
        "_store.receiver": {$cond: {if: "$_.store.receiver", then: "$_store.receiver", else: "$_receiver"}},
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockSaleByItem_ConsumptionReport');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        //"menuItems.itemType":"MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Sale');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        $or: [{"items.itemType": "RawMaterial"}, {"items.itemType": "Base Kitchen Item"}],
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getWastageStockByItem');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty",
          unitName: "$units.unitName", conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Wastage_old')
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{ $in: ["MenuItem", "IntermediateItem"] },
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty",
          unitName: "$units.unitName", conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {

    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getTransferStockByItemFromStore');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getTransferStockByItemToStore');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_FromStore');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_ToStore');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{$in:["MenuItem","IntermediateItem"]},
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Transfer_FromStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Transfer_ToStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId) {
  var d = Q.defer();
  //console.log('storeId: ', storeId);
  var data = [];
  if (isKitchen) {
    //console.log(isKitchen);
    var allPromise = Q.all([
      getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id),
      BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate),
      getStoresProcessedCategory(deployment_id, tenant_id, storeId)
    ]);
    allPromise.then(function (value) {
      var recData = value[0];
      //  console.log('recDat a', recData[0]);
      var billData = value[1];
      //console.log('billData', billData);
      var items = value[2];
      _.forEach(billData, function (b, i) {
        //console.log(b);
        _.forEach(items, function (it, iii) {
          if (b.itemId == it.itemId) {
            _.forEach(recData, function (r, ii) {
              //console.log(r.unitName);
              //r.itemId,r.totalSaleQty,r.billQty
              if (b.itemId == r.menuId) {
                if (r.itemSaleQty == undefined) {
                  r.itemSaleQty = 0;
                }
                ;
                r.recipeId = r.recipeId.toString();
                r.created = b.created;
                //console.log('3971', r.itemName, b.totalSaleQty, r.receipeQtyinBase, b.billQty, r.itemQtyInBase);

                r.itemSaleQty = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemQtyInBase);
                r.itemSaleAmt = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemAmountInBase);


                //console.log(r.menuName, b.totalSaleQty, r.receipeQtyinBase, b.billQty, r.itemAmountInBase,r.itemSaleAmt);
                data.push(_.cloneDeep(r));
              }
            });
          }
        });
        ;
      });
      // console.log('data', data);
      d.resolve(data);
    });
  }
  else {
    d.resolve(data);
  }

  return d.promise;
};

function getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        rawItems: "$receipeDetails",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    {
      $unwind: "$rawItems"
    },
    {
      $match: {
        "rawItems.isSemiProcessed": {$ne: true}
      }
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        receipeQtyinBase: "$receipeQtyinBase",
        UnitName: "$rawItems.selectedUnitName",
        itemQtyInBase: "$rawItems.baseQuantity",
        itemAmountInBase: "$rawItems.calculatedAveragePrice",
        isBaseKitchenItem: "$rawItems.isBaseKitchenItem",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        baseUnit: "$baseUnit",
        isBaseKitchenItem: "$isBaseKitchenItem",
        itemAmountInBase: "$itemAmountInBase",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        UnitName: "$UnitName",
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getRecipesOfBillItems');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate) {

  var d = Q.defer();
  Bill.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        _created: {
          $gte: moment(new Date(startDate)).toDate(),
          $lte: moment(new Date(endDate)).toDate()
        },
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: {depid: "$deployment_id"},
        _kots: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _created: "$_created"
      }
    },
    {
      $unwind: "$_kots.items"
    },
    {
      $project: {
        _created: 1,
        "items": ["$_kots.items"],
        "addOns": {
          $cond: {
            if: {$and: ["$_kots.items.addOns", {$gt: [{$size: "$_kots.items.addOns"}, 0]}]},
            then: "$_kots.items.addOns",
            else: []
          }
        },
      }
    },
    {
      $project: {
        items: {$concatArrays: ["$items", "$addOns"]},
        _created: 1
      }
    },
    {
      $unwind: "$items"
    },
    {
      $project: {
        "items.name": "$items.name",
        "items._id": "$items._id",
        "items.stockQuantity": "$items.stockQuantity",
        "items.unit.conversionFactor": "$items.unit.conversionFactor",
        "items.quantity": "$items.quantity",
        //"items.addOns": "$items.addOns",
        created: "$_created",
        _created: "$_created",

      }
    },
    {
      $sort: {
        _created: -1
      }
    },
    {
      $group: {
        _id: {itemId: "$items._id", created: "$created"},
        qty: {$first: "$items.stockQuantity"},
        conF: {$first: "$items.unit.conversionFactor"},
        billQty: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id.itemId",
        totalSaleQty: {$multiply: ["$qty", "$conF"]},
        billQty: "$billQty",
        created: "$_id.created",
        //unitName: "$_id.unitName"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result2) {
    if (err) {
      console.log(err);
      d.reject('error');
    }


    if (err) {
      console.log('BillgetItemWiseReport');
      console.log(err);
      d.reject('error');
    } else {
      d.resolve(result2);
    }
    // else {
    //   var addOnsDayWise = {};
    //   _.forEach(result2, function (item) {
    //     result.push(item);
    //     var itCreated = new Date(item.created)
    //     var date = moment(itCreated).format('L');
    //     if (!addOnsDayWise[date])
    //       addOnsDayWise[date] = {};
    //     console.log('itemAddons', item.addOns);
    //     _.forEach(item.addOns, function (addOns, i) {
    //       console.log('items addons',addOns);
    //       _.forEach(addOns, function (addOn) {

    //         var add = addOn;
    //         if (!addOnsDayWise[date][addOn.name]) {
    //           addOnsDayWise[date][addOn.name] = {
    //             billQty: 0
    //           };
    //         }
    //         //console.log('name', add.name);
    //         //console.log('_id', addOn._id);
    //         //console.log('add', addOnsDayWise[date]);
    //         //console.log('addOn', addOn);
    //         addOnsDayWise[date][addOn.name].itemId = addOn._id;
    //         addOnsDayWise[date][addOn.name].itemName = addOn.name;
    //         addOnsDayWise[date][addOn.name].billQty += addOn.quantity ? addOn.quantity : 0;
    //         var stockQuantity = addOn.stockQuantity ? addOn.stockQuantity : 0;
    //         var unit = addOn.unit ? addOn.unit : null;
    //         if (unit)
    //           var conversionFactor = addOn.unit.conversionFactor ? addOn.unit.conversionFactor : 0;
    //         addOnsDayWise[date][addOn.name].totalSaleQty = stockQuantity * conversionFactor;
    //         addOnsDayWise[date][addOn.name].created = new Date(item.created);
    //       });
    //     });
    //   });

    //console.log('dayAddOns', addOnsDayWise);
    // _.forEach(addOnsDayWise, function (dayAddOns, day) {
    //   _.forEach(dayAddOns, function (addOn) {
    //     result.push(addOn);
    //   });
    // });
    //console.log('result', result);
    //d.resolve(result);
    //}
    //  d.resolve(result);

  });

  return d.promise;
};

function getStoresProcessedCategory(deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Store.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        _id: new ObjectId(storeId),
        processedFoodCategory: {$ne: undefined},
      },
    },
    {
      $unwind: "$processedFoodCategory"
    },
    {
      $unwind: "$processedFoodCategory.item"
    },
    {
      $project: {
        storeId: "$_id",
        storeName: "$storeName",
        isKitchen: "$isKitchen",
        items: "$processedFoodCategory.item"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$storeId",
          storeName: "$storeName",
          isKitchen: "$isKitchen"
        }
      }
    },
    {
      $project: {
        _id: 0,
        storeId: "$_id.storeId",
        storeName: "$_id.storeName",
        isKitchen: "$_id.isKitchen",
        itemId: "$_id.itemId",
        itemName: "$_id.name"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStoresProcessedCategory');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getIntermediateReceipeDetails_IntermediateEntry');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit",
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "12",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        },
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalStockReceiveQty: "$_id.totalQty",
        totalStockReceiveAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockReceive_RequirementEntry');
      ;

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "9",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lte: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalStockReturnQty: "$_id.totalQty",
        totalStockReturnAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockReturn_RequirementEntry');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"RawMaterial",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getReceipeDetails_FinishedFoodEntry');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"RawMaterial",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true},
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getReceipeDetails_FinishedFoodEntry_PreviousBkItems');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{$in:["MenuItem","IntermediateItem"]},
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getIndentDeliveryChallanConsumption(startDate, endDate, deployment_id, tenant_id, storeId) {
   // console.log('Inside getIndentDeliveryChallanConsumption');
  //console.log(startDate, endDate, deployment_id, tenant_id, storeId)
  var d = Q.defer();
  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "16",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId.toString()
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          price: "$units.basePrice",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}

      }
    },
    {
      $project: {
        _id: 0,
        totalIndentingChallanQty: "$_id.qty",
        totalIndentingChallanAmt: "$_id.price",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getIndentDeliveryChallanConsumption');
      console.log(err);
      d.reject('error');
    }
    //console.log('getIndentDeliveryChallanConsumption', result);
    d.resolve(result);

  });
  //console.log("outlet")
  return d.promise;
};



function getConsumptionAndPurchase(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen, lastPrice) {

  //console.log('getConsumptionAndPurchase');
  var deferred = Q.defer();
  Q.all([
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId),
    getIndentDeliveryChallanConsumption(startDate, endDate, deployment_id, tenant_id, storeId),
  ]).then(function (result) {
    //console.log('getConsumption length', result.length);
    var transactions = [];
    _.forEach(result, function (r) {
      //console.log(r.length);
      _.forEach(r, function (transaction) {
        transactions.push(transaction);
        //console.log('t',transaction)
      });
    });

    var itemsWithPrice = [];
    //console.log("before calculate")
    var items = calculateConsumptionAndPurchase(transactions, lastPrice);
    //console.log("itemssssss",items.length)
    deferred.resolve(items);
  }).catch(function (err) {
    console.log(err);
    deferred.reject(err);
  });
  return deferred.promise;
}

function calculateConsumptionAndPurchase(result, lastPrice) {
  //console.log('sddsdlk', lastPrice);
  result.items = [];
  _.forEach(result, function (itm, i) {

    var p = _.find(lastPrice, function (price) {
      return price.itemId == itm.itemId;
    });
    var price;
    if (p)
      price = p.monthAverage;
    else price = 0;

    if (itm.totalPurchaseQty != undefined) {
      if (isNaN(itm.totalPurchaseQty))
        itm.totalPurchaseQty = 0;
      if (isNaN(itm.totalPurchaseAmount))
        itm.totalPurchaseAmount = itm.totalPurchaseQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
        }
        /*if(itm.totalPurchaseQty == undefined){
         itm.totalPurchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
         itm.totalPurchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
         }*/
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        itm.purchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
        //console.log('lastPrice',itm.totalPurchaseAmount);
        itm.purchaseAmount = parseFloat(itm.totalPurchaseAmount).toFixed(2);
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].purchaseQty = parseFloat(parseFloat(result.items[itmIndex].purchaseQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].purchaseAmount = parseFloat(parseFloat(result.items[itmIndex].purchaseAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
        result.items[itmIndex].totalPurchaseQty = parseFloat(parseFloat(result.items[itmIndex].totalPurchaseQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].totalPurchaseAmount = parseFloat(parseFloat(result.items[itmIndex].totalPurchaseAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
      }
    }
    else if (itm.totalIntermediateQty != undefined) {

      if (isNaN(itm.totalIntermediateQty))
        itm.totalIntermediateQty = 0;
      if (isNaN(itm.totalIntermediateAmt))
          itm.totalIntermediateAmt = itm.totalIntermediateQty * price;
        var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        if (itmIndex < 0) {
          if (itm.consumptionQty == undefined) {
            itm.consumptionQty = parseFloat(0).toFixed(3);
            itm.consumptionAmount = parseFloat(0).toFixed(2);
          }
          if (itm.purchaseQty == undefined) {
            itm.purchaseQty = parseFloat(0).toFixed(3);
            itm.purchaseAmount = parseFloat(0).toFixed(2);
          }
          if (itm.wastageQty == undefined) {
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
          }
          if (itm.totalPurchaseQty == undefined) {
            itm.totalPurchaseQty = parseFloat(0).toFixed(3);
            itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
          }
          /*if(itm.totalIntermediateQty == undefined){
           itm.totalIntermediateQty=parseFloat(0).toFixed(3);
           itm.totalIntermediateAmt=parseFloat(0).toFixed(2);
           }*/
          if (itm.totalSaleQty == undefined) {
            itm.totalSaleQty = parseFloat(0).toFixed(3);
            itm.totalSaleAmount = parseFloat(0).toFixed(2);
          }
          if (itm.totalTransferQty_toStore == undefined) {
            itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
            itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
          }
          if (itm.totalTransferQty == undefined) {
            itm.totalTransferQty = parseFloat(0).toFixed(3);
            itm.totalTransferAmount = parseFloat(0).toFixed(2);
          }
          if (itm.itemSaleQty == undefined) {
            itm.itemSaleQty = parseFloat(0).toFixed(3);
            itm.itemSaleAmt = parseFloat(0).toFixed(2);
          }
          if (itm.totalStockReceiveQty == undefined) {
            itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
            itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
          }
          if (itm.totalStockReturnQty == undefined) {
            itm.totalStockReturnQty = parseFloat(0).toFixed(3);
            itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
          }
           if (itm.totalIndentingChallanQty == undefined) {
            itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
            itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
          }
          itm.consumptionQty = parseFloat(itm.totalIntermediateQty).toFixed(3);
          itm.consumptionAmount = parseFloat(itm.totalIntermediateAmt).toFixed(2);
          result.items.push(itm);
        }
        else {
          result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalIntermediateQty)).toFixed(3);
          result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalIntermediateAmt)).toFixed(2);
          result.items[itmIndex].totalIntermediateQty = parseFloat(parseFloat(result.items[itmIndex].totalIntermediateQty) + parseFloat(itm.totalIntermediateQty)).toFixed(3);
          result.items[itmIndex].totalIntermediateAmt = parseFloat(parseFloat(result.items[itmIndex].totalIntermediateAmt) + parseFloat(itm.totalIntermediateAmt)).toFixed(2);
        }
    }
    else if (itm.totalSaleQty != undefined) {
      if (isNaN(itm.totalSaleQty))
        itm.totalSaleQty = 0;
      if (isNaN(itm.totalSaleAmount))
        itm.totalSaleAmount = itm.totalSaleQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        itm.consumptionQty = parseFloat(itm.totalSaleQty).toFixed(3);
        itm.consumptionAmount = parseFloat(itm.totalSaleAmount).toFixed(2);
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalSaleAmount)).toFixed(2);
        result.items[itmIndex].totalSaleQty = parseFloat(parseFloat(result.items[itmIndex].totalSaleQty) + parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].totalSaleAmount = parseFloat(parseFloat(result.items[itmIndex].totalSaleAmount) + parseFloat(itm.totalSaleAmount)).toFixed(2);
      }
    }
    else if (itm.totalWastageQty != undefined) {
      if (isNaN(itm.totalWastageQty))
        itm.totalWastageQty = 0;
      if (isNaN(itm.totalWastageAmt))
        itm.totalWastageAmt = itm.totalWastageQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        itm.wastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
        itm.wastageAmt = parseFloat(itm.totalWastageAmt).toFixed(2);
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].wastageQty = parseFloat(parseFloat(result.items[itmIndex].wastageQty) + parseFloat(itm.totalWastageQty)).toFixed(3);
        result.items[itmIndex].wastageAmt = parseFloat(parseFloat(result.items[itmIndex].wastageAmt) + parseFloat(itm.totalWastageAmt)).toFixed(2);
      }
    }
    else if (itm.totalTransferQty_toStore != undefined) {
      if (isNaN(itm.totalTransferQty_toStore))
        itm.totalTransferQty_toStore = 0;
      if (isNaN(itm.totalTransferAmount_toStore))
        itm.totalTransferAmount_toStore = itm.totalTransferQty_toStore * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        /*if(itm.totalTransferQty_toStore == undefined){
         itm.totalTransferQty_toStore=parseFloat(0).toFixed(3);
         itm.totalTransferAmount_toStore=parseFloat(0).toFixed(2);
         }*/
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        itm.purchaseQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        itm.purchaseAmount = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].purchaseQty = parseFloat(parseFloat(result.items[itmIndex].purchaseQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].purchaseAmount = parseFloat(parseFloat(result.items[itmIndex].purchaseAmount) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
        result.items[itmIndex].totalTransferQty_toStore = parseFloat(parseFloat(result.items[itmIndex].totalTransferQty_toStore) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].totalTransferAmount_toStore = parseFloat(parseFloat(result.items[itmIndex].totalTransferAmount_toStore) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
      }
    }
    else if (itm.totalTransferQty != undefined) {
      if (isNaN(itm.totalTransferQty))
        itm.totalTransferQty = 0;
      if (isNaN(itm.totalTransferAmount))
        itm.totalTransferAmount = itm.totalTransferQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        itm.consumptionQty = parseFloat(itm.totalTransferQty).toFixed(3);
        itm.consumptionAmount = parseFloat(itm.totalTransferAmount).toFixed(2);
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalTransferAmount)).toFixed(2);
        result.items[itmIndex].totalTransferQty = parseFloat(parseFloat(result.items[itmIndex].totalTransferQty) + parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].totalTransferAmount = parseFloat(parseFloat(result.items[itmIndex].totalTransferAmount) + parseFloat(itm.totalTransferAmount)).toFixed(2);
      }
    }
    else if (itm.itemSaleQty != undefined) {
      if (isNaN(itm.itemSaleQty))
        itm.itemSaleQty = 0;
      if (isNaN(itm.itemSaleAmt))
        itm.itemSaleAmt = itm.itemSaleQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        /*if(itm.itemSaleQty == undefined){
         itm.itemSaleQty=parseFloat(0).toFixed(3);
         itm.itemSaleAmt=parseFloat(0).toFixed(2);
         }*/
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        itm.consumptionQty = parseFloat(itm.itemSaleQty).toFixed(3);
        itm.consumptionAmount = parseFloat(itm.itemSaleAmt).toFixed(2);
        itm.UnitName = itm.baseUnit;
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.itemSaleAmt)).toFixed(2);
        result.items[itmIndex].itemSaleQty = parseFloat(parseFloat(result.items[itmIndex].itemSaleQty) + parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].itemSaleAmt = parseFloat(parseFloat(result.items[itmIndex].itemSaleAmt) + parseFloat(itm.itemSaleAmt)).toFixed(2);
      }
    }
    else if (itm.totalStockReceiveQty != undefined) {
      if (isNaN(itm.totalStockReceiveQty))
        itm.totalStockReceiveQty = 0;
      if (isNaN(itm.totalStockReceiveAmount))
        itm.totalStockReceiveAmount = itm.totalStockReceiveQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      //console.log(itmIndex);
      if (itmIndex < 0) {
        //console.log(itm.itemName);
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(2);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        /*if(itm.totalStockReceiveQty == undefined){
         itm.totalStockReceiveQty=parseFloat(0).toFixed(3);
         itm.totalStockReceiveAmount=parseFloat(0).toFixed(2);
         }*/
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        itm.purchaseQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
        itm.purchaseAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(2);
        result.items.push(itm);
      }
      else {
        //console.log(itmIndex);
        result.items[itmIndex].purchaseQty = parseFloat(parseFloat(result.items[itmIndex].purchaseQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].purchaseAmount = parseFloat(parseFloat(result.items[itmIndex].purchaseAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
        result.items[itmIndex].totalStockReceiveQty = parseFloat(parseFloat(result.items[itmIndex].totalStockReceiveQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].totalStockReceiveAmount = parseFloat(parseFloat(result.items[itmIndex].totalStockReceiveAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
      }
    }
    else if (itm.totalStockReturnQty != undefined) {
      if (isNaN(itm.totalStockReturnQty))
        itm.totalStockReturnQty = 0;
      if (isNaN(itm.totalStockReturnAmount))
        itm.totalStockReturnAmount = itm.totalStockReturnQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(3);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(3);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(3);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
         if (itm.totalIndentingChallanQty == undefined) {
          itm.totalIndentingChallanQty = parseFloat(0).toFixed(3);
          itm.totalIndentingChallanAmt = parseFloat(0).toFixed(2);
        }
        /*if(itm.totalStockReturnQty == undefined){
         itm.totalStockReturnQty=parseFloat(0).toFixed(3);
         itm.totalStockReturnAmount=parseFloat(0).toFixed(2);
         }*/
        itm.consumptionQty = parseFloat(itm.totalStockReturnQty).toFixed(3);
        itm.consumptionAmount = parseFloat(itm.totalStockReturnAmount).toFixed(2);
        result.items.push(itm);
      }

      else {
        result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReturnAmount)).toFixed(2);
        result.items[itmIndex].totalStockReturnQty = parseFloat(parseFloat(result.items[itmIndex].totalStockReturnQty) + parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].totalStockReturnAmount = parseFloat(parseFloat(result.items[itmIndex].totalStockReturnAmount) + parseFloat(itm.totalStockReturnAmount)).toFixed(2);
      }
    }
    else if (itm.totalIndentingChallanQty != undefined) {
      if (isNaN(itm.totalIndentingChallanQty))
        itm.totalIndentingChallanQty = 0;
      if (isNaN(itm.totalIndentingChallanAmt))
        itm.totalIndentingChallanAmt = itm.totalStockReturnQty * price;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        if (itm.consumptionQty == undefined) {
          itm.consumptionQty = parseFloat(0).toFixed(3);
          itm.consumptionAmount = parseFloat(0).toFixed(3);
        }
        if (itm.purchaseQty == undefined) {
          itm.purchaseQty = parseFloat(0).toFixed(3);
          itm.purchaseAmount = parseFloat(0).toFixed(3);
        }
        if (itm.wastageQty == undefined) {
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(3);
        }
        if (itm.totalPurchaseQty == undefined) {
          itm.totalPurchaseQty = parseFloat(0).toFixed(3);
          itm.totalPurchaseAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalIntermediateQty == undefined) {
          itm.totalIntermediateQty = parseFloat(0).toFixed(3);
          itm.totalIntermediateAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalSaleQty == undefined) {
          itm.totalSaleQty = parseFloat(0).toFixed(3);
          itm.totalSaleAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty_toStore == undefined) {
          itm.totalTransferQty_toStore = parseFloat(0).toFixed(3);
          itm.totalTransferAmount_toStore = parseFloat(0).toFixed(2);
        }
        if (itm.totalTransferQty == undefined) {
          itm.totalTransferQty = parseFloat(0).toFixed(3);
          itm.totalTransferAmount = parseFloat(0).toFixed(2);
        }
        if (itm.itemSaleQty == undefined) {
          itm.itemSaleQty = parseFloat(0).toFixed(3);
          itm.itemSaleAmt = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReceiveQty == undefined) {
          itm.totalStockReceiveQty = parseFloat(0).toFixed(3);
          itm.totalStockReceiveAmount = parseFloat(0).toFixed(2);
        }
        if (itm.totalStockReturnQty == undefined) {
          itm.totalStockReturnQty = parseFloat(0).toFixed(3);
          itm.totalStockReturnAmount = parseFloat(0).toFixed(2);
        }
        /*if(itm.totalStockReturnQty == undefined){
         itm.totalStockReturnQty=parseFloat(0).toFixed(3);
         itm.totalStockReturnAmount=parseFloat(0).toFixed(2);
         }*/
        itm.consumptionQty = parseFloat(itm.totalIndentingChallanQty).toFixed(3);
        itm.consumptionAmount = parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
        result.items.push(itm);
      }
      
      else {
        result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
        result.items[itmIndex].totalIndentingChallanQty = parseFloat(parseFloat(result.items[itmIndex].totalIndentingChallanQty) + parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
        result.items[itmIndex].totalIndentingChallanAmt = parseFloat(parseFloat(result.items[itmIndex].totalIndentingChallanAmt) + parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
      }
    }


  });
  
  return result.items;
}

function handleError(res, err) {
  return res.send(500, err);
}

function getAllRawMaterials_ForRecipePricingConsumptionReport(deployment_id, tenant_id, callback1) {
  var deferred = Q.defer();
  StockItem.find(
    {
      deployment_id: deployment_id,
      tenant_id: tenant_id
    }, function (err, result_entry) {
      //console.log('result_entry', result_entry.length);
      if (err) {
        deferred.reject(err);
      }
      else {
        var items = [];
        _.forEach(result_entry, function (r, i) {
          var itm = {itemId: r._id, itemName: r.itemName, monthAverage: 0, lastPrice: 0};
          items.push(itm);
        });
        //console.log('pricing items', items.length);

        getLastMonthAveragePrice_ReceipePricingConsumptionReport(deployment_id, tenant_id, function (val) {
          if (val != null) {
            _.forEach(val, function (v, i) {
              _.forEach(items, function (itm, ii) {
                if (itm.itemId == v.itemId) {
                  itm.monthAverage = parseFloat(v.lastPrice).toFixed(3);
                }
              });
            });
            //callback1(items);
            getLastPrice_AllRaw_ReceipePricingConsumptionReport(deployment_id, tenant_id, function (result) {
              if (result != null) {
                _.forEach(result, function (r, i) {
                  _.forEach(items, function (itm, ii) {
                    if (itm.itemId == r.itemId) {
                      itm.lastPrice = parseFloat(r.lastPrice).toFixed(3);
                    }
                  });
                });
              }
              //console.log('pricing items 2', items.length);
              deferred.resolve(items);
            });
          }
        });
        //callback(items);
      }
    });
  return deferred.promise;
};

function getLastMonthAveragePrice_ReceipePricingConsumptionReport(deployment_id, tenant_id, callback) {
  var startDate = new Date();
  var endDate = new Date();
  startDate.setDate(endDate.getDate() - 30);
  StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        created: {$gte: new Date(startDate), $lte: new Date(endDate)}
        //"_store.vendor.category.item._id":itemId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $sort: {_created: -1}
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$avg: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      /*_.forEach(result_entry, function (r, i) {
       if (r.UnitName == "Litre" || r.UnitName == "Meter") {
       r.lastPrice = parseFloat(r.lastPrice) / 1000;
       }
       });*/
      callback(result_entry);
    }
  });
};

function getLastPrice_AllRaw_ReceipePricingConsumptionReport(deployment_id, tenant_id, callback) {
  StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id)
        //created:{$gte:new Date(startDate),$lte:new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      /* _.forEach(result_entry, function (r, i) {
       if (r.UnitName == "Litre" || r.UnitName == "Meter") {
       r.lastPrice = parseFloat(r.lastPrice) / 1000;
       }
       });*/
      callback(result_entry);
    }
  });
};

function getAllRawMaterials_ForRecipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, callback1) {
  var deferred = Q.defer();
  BaseKitchenItem.find(
    {
      deployment_id: deployment_id,
      tenant_id: tenant_id
    }, function (err, result_entry) {
      if (err) {
        deferred.reject(err);
      }
      else {
        var items = [];
        _.forEach(result_entry, function (r, i) {
          var itm = {itemId: r.itemId, itemName: r.itemName, monthAverage: 0, lastPrice: 0};
          items.push(itm);
        });

        getLastMonthAveragePrice_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, function (val) {
          if (val != null) {
            _.forEach(val, function (v, i) {
              _.forEach(items, function (itm, ii) {
                if (itm.itemId == v.itemId) {
                  itm.monthAverage = parseFloat(v.lastPrice).toFixed(3);
                }
              });
            });
            //callback1(items);
            getLastPrice_AllRaw_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, function (result) {
              if (result != null) {
                _.forEach(result, function (r, i) {
                  _.forEach(items, function (itm, ii) {
                    if (itm.itemId == r.itemId) {
                      itm.lastPrice = parseFloat(r.lastPrice).toFixed(3);
                    }
                  });
                });
              }
              deferred.resolve(items);
            });
          }
        });
        //callback(items);
      }
    });
  return deferred.prmoise;
};

function getLastMonthAveragePrice_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, callback) {
  var startDate = new Date();
  var endDate = new Date();
  startDate.setDate(endDate.getDate() - 30);
  StockRequirement.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        //transactionType: 1,
        tenant_id: new ObjectId(tenant_id)
        // created:{$gte:new Date(startDate),$lte:new Date(endDate)}
        //"_store.vendor.category.item._id":itemId
      }
    },
    {
      $unwind: "$store.vendor.category"
    },
    {
      $unwind: "$store.vendor.category.items"
    },
    {
      $unwind: "$store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$avg: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      //console.log('getLastMonthAveragePrice_ReceipePricingBaseKitchenConsumptionReport');
      /* _.forEach(result_entry, function (r, i) {
       if (r.UnitName == "Litre" || r.UnitName == "Meter") {
       r.lastPrice = parseFloat(r.lastPrice) / 1000;
       }
       });*/
      callback(result_entry);
    }
  });
};

function getLastPrice_AllRaw_ReceipePricingBaseKitchenConsumptionReport(deployment_id, tenant_id, callback) {
  StockRequirement.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        requirementType: 1,
        tenant_id: new ObjectId(tenant_id)
        //created:{$gte:new Date(startDate),$lte:new Date(endDate)}
      }
    },
    {
      $unwind: "$store.vendor.category"
    },
    {
      $unwind: "$store.vendor.category.items"
    },
    {
      $unwind: "$store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items._id":itemId,
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        //lastPrice:{ $avg: "$items.price" }
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      callback(err);
    }
    else {
      //console.log('getLastPrice_AllRaw_ReceipePricingBaseKitchenConsumptionReport');
      /*_.forEach(result_entry, function (r, i) {
       if (r.UnitName == "Litre" || r.UnitName == "Meter") {
       r.lastPrice = parseFloat(r.lastPrice) / 1000;
       }
       });*/
      callback(result_entry);
    }
  });
};

function calculatePrice(result, lastPrice) {
  //console.log(result);
  _.forEach(result.beforeDate, function (itm, i) {
    //console.log(JSON.stringify(itm));
    var index = _.findIndex(lastPrice, {itemId: itm.itemId});
    //console.log(itm.itemName, itm.totalPurchaseQty, $scope.lastPrice[index].monthAverage, itm.ConversionFactor, itm.UnitName);
    //if(index>=0){
    if (itm.totalOpeningQty != undefined) {
      if (itm.totalOpeningAmt == undefined || itm.totalOpeningAmt == 0) {
        if (index >= 0) {
          itm.totalOpeningAmt = parseFloat(itm.totalOpeningQty) * parseFloat($scope.lastPrice[index].monthAverage);
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
        }
        else {
          itm.totalOpeningAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          itm.totalOpeningAmt = parseFloat(itm.totalOpeningQty) * parseFloat($scope.lastPrice[index].monthAverage);
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
        }
        else {
          itm.totalOpeningAmt = 0.00;
        }
      }
    }
    else if (itm.totalBalanceQty != undefined) {
      if (itm.totalBalanceAmt == undefined || itm.totalBalanceAmt == 0) {
        if (index >= 0) {
          itm.totalBalanceAmt = parseFloat(itm.totalBalanceQty) * parseFloat(lastPrice[index].monthAverage);
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
        } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
        else {
          itm.totalBalanceAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          itm.totalBalanceAmt = parseFloat(itm.totalBalanceQty) * parseFloat(lastPrice[index].monthAverage);
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
        } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
        else {
          itm.totalBalanceAmt = 0.00;
        }
      }
    }
    else if (itm.totalPurchaseQty != undefined) {
      // if(itm.totalPurchaseAmount==undefined){
      //   itm.totalPurchaseAmount=0.00;
      // }
      var p = isNaN(itm.totalPurchaseAmount) ? lastPrice[index].monthAverage : itm.totalPurchaseAmount;
      if (itm.totalPurchaseAmount == undefined || itm.totalPurchaseAmount == 0) {
        if (index >= 0) {
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.totalPurchaseAmount = parseFloat(parseFloat(itm.totalPurchaseQty) * parseFloat(p) * parseFloat(itm.ConversionFactor)).toFixed(2);
        }
        else {
          itm.totalPurchaseAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.totalPurchaseAmount = parseFloat(parseFloat(itm.totalPurchaseQty) * parseFloat(p) * parseFloat(itm.ConversionFactor)).toFixed(2);
        }
        else {
          itm.totalPurchaseAmount = 0.00;
        }
      }
    }
    else if (itm.totalSaleQty != undefined) {
      var p = isNaN(itm.totalSaleAmount) ? lastPrice[index].monthAverage : itm.totalSaleAmount;
      if (itm.totalSaleAmount == undefined || itm.totalSaleAmount == 0) {
        if (index >= 0) {
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);

          itm.totalSaleAmount = parseFloat(itm.totalSaleQty) * parseFloat(p);
        }
        else {
          itm.totalSaleAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);

          itm.totalSaleAmount = parseFloat(itm.totalSaleQty) * parseFloat(p);
        }
        else {
          itm.totalSaleAmount = 0.00;
        }
      }
    }
    else if (itm.totalWastageQty != undefined) {
      if (itm.totalWastageAmt == undefined || itm.totalWastageAmt == 0) {
        if (index >= 0) {
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalWastageAmt = parseFloat(itm.totalWastageQty) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalWastageAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalWastageAmt = parseFloat(itm.totalWastageQty) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalWastageAmt = 0.00;
        }
      }
    }
    else if (itm.totalTransferQty != undefined) {
      if (itm.totalTransferAmount == undefined || itm.totalTransferAmount == 0) {
        if (index >= 0) {
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount = parseFloat(itm.totalTransferQty) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalTransferAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount = parseFloat(itm.totalTransferQty) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalTransferAmount = 0.00;
        }
      }
    }
    else if (itm.totalTransferQty_toStore != undefined) {
      if (itm.totalTransferAmount_toStore == undefined || itm.totalTransferAmount_toStore == 0) {
        if (index >= 0) {
          //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount_toStore = parseFloat(itm.totalTransferQty_toStore) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalTransferAmount_toStore = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount_toStore = parseFloat(itm.totalTransferQty_toStore) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalTransferAmount_toStore = 0.00;
        }
      }
    }
    else if (itm.itemSaleQty != undefined) {
      var p = isNaN(itm.itemSaleAmt) ? lastPrice[index].monthAverage : itm.itemSaleAmt;
      if (itm.itemSaleAmt == undefined || itm.itemSaleAmt == 0) {
        if (index >= 0) {
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

          itm.itemSaleAmt = parseFloat(itm.itemSaleQty) * parseFloat(p);
        }
        else {
          itm.itemSaleAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

          itm.itemSaleAmt = parseFloat(itm.itemSaleQty) * parseFloat(p);
        }
        else {
          itm.itemSaleAmt = 0.00;
        }
      }
    }
    else if (itm.totalIntermediateQty != undefined) {
      if (itm.totalIntermediateAmt == undefined || itm.totalIntermediateAmt == 0) {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalIntermediateAmt = parseFloat(itm.totalIntermediateQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalIntermediateAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalIntermediateAmt = parseFloat(itm.totalIntermediateQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalIntermediateAmt = 0.00;
        }
      }
    }
    else if (itm.totalStockReturnQty != undefined) {
      //console.log('price', itm.itemName, itm.itemId, index);
      var p = isNaN(itm.totalStockReturnAmount) ? lastPrice[index].monthAverage : itm.totalStockReturnAmount;
      if (itm.totalStockReturnAmount == undefined || itm.totalStockReturnAmount == 0) {
        if (index >= 0) {
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReturnAmount = parseFloat(itm.totalStockReturnQty) * parseFloat(p);

          //console.log(itm, $scope.lastPrice[index]);
        }
        else {
          itm.totalStockReturnAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReturnAmount = parseFloat(itm.totalStockReturnQty) * parseFloat(p);

          //console.log(itm, $scope.lastPrice[index]);
        }
        else {
          itm.totalStockReturnAmount = 0.00;
        }
      }
    }
    else if (itm.totalStockReceiveQty != undefined) {
      var p = isNaN(itm.totalStockReceiveAmount) ? lastPrice[index].monthAverage : itm.totalStockReceiveAmount;
      if (itm.totalStockReceiveAmount == undefined || itm.totalStockReceiveAmount == 0) {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReceiveAmount = parseFloat(itm.totalStockReceiveQty) * parseFloat(p);
        }
        else {
          itm.totalStockReceiveAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReceiveAmount = parseFloat(itm.totalStockReceiveQty) * parseFloat(p);
        }
        else {
          itm.totalStockReceiveAmount = 0.00;
        }
      }
    }
    //}
  });
  _.forEach(result.betweenDate, function (itm, i) {
    //console.log(JSON.stringify(itm));
    var index = _.findIndex($scope.lastPrice, {itemId: itm.itemId});

    //if(index>=0){
    if (itm.totalOpeningQty != undefined) {
      if (itm.totalOpeningAmt == undefined || itm.totalOpeningAmt == 0) {
        if (index >= 0) {
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalOpeningAmt = parseFloat(itm.totalOpeningQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalOpeningAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalOpeningAmt = parseFloat(itm.totalOpeningQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalOpeningAmt = 0.00;
        }
      }
    }
    else if (itm.totalBalanceQty != undefined) {
      if (itm.totalBalanceAmt == undefined || itm.totalBalanceAmt == 0) {
        if (index >= 0) {
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalBalanceAmt = parseFloat(itm.totalBalanceQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalBalanceAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalBalanceAmt = parseFloat(itm.totalBalanceQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalBalanceAmt = 0.00;
        }
      }
    }
    else if (itm.totalPurchaseQty != undefined) {
      /* if(itm.totalPurchaseAmount==undefined){
       itm.totalPurchaseAmount=0.00;
       }*/
      var p = isNaN(itm.totalPurchaseAmount) ? lastPrice[index].monthAverage : itm.totalPurchaseAmount;
      if (itm.totalPurchaseAmount == undefined || itm.totalPurchaseAmount == 0) {
        if (index >= 0) {
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.totalPurchaseAmount = parseFloat(parseFloat(itm.totalPurchaseQty) * parseFloat(p) * parseFloat(itm.ConversionFactor)).toFixed(2);
        }
        else {
          itm.totalPurchaseAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.totalPurchaseAmount = parseFloat(parseFloat(itm.totalPurchaseQty) * parseFloat(p) * parseFloat(itm.ConversionFactor)).toFixed(2);
        }
        else {
          itm.totalPurchaseAmount = 0.00;
        }
      }
    }
    else if (itm.totalSaleQty != undefined) {
      var p = isNaN(itm.totalSaleAmount) ? lastPrice[index].monthAverage : itm.totalSaleAmount;
      if (itm.totalSaleAmount == undefined || itm.totalSaleAmount == 0) {
        if (index >= 0) {
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);

          itm.totalSaleAmount = parseFloat(itm.totalSaleQty) * parseFloat(p);
        }
        else {
          itm.totalSaleAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);

          itm.totalSaleAmount = parseFloat(itm.totalSaleQty) * parseFloat(p);
        }
        else {
          itm.totalSaleAmount = 0.00;
        }
      }
    }
    else if (itm.totalWastageQty != undefined) {
      if (itm.totalWastageAmt == undefined || itm.totalWastageAmt == 0) {
        if (index >= 0) {
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalWastageAmt = parseFloat(itm.totalWastageQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalWastageAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalWastageAmt = parseFloat(itm.totalWastageQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalWastageAmt = 0.00;
        }
      }
    }
    else if (itm.totalTransferQty != undefined) {
      if (itm.totalTransferAmount == undefined || itm.totalTransferAmount == 0) {
        if (index >= 0) {
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount = parseFloat(itm.totalTransferQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalTransferAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount = parseFloat(itm.totalTransferQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalTransferAmount = 0.00;
        }
      }
    }
    else if (itm.totalTransferQty_toStore != undefined) {
      if (itm.totalTransferAmount_toStore == undefined || itm.totalTransferAmount_toStore == 0) {
        if (index >= 0) {
          //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount_toStore = parseFloat(itm.totalTransferQty_toStore) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalTransferAmount_toStore = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalTransferAmount_toStore = parseFloat(itm.totalTransferQty_toStore) * parseFloat(lastPrice[index].monthAverage);

        }
        else {
          itm.totalTransferAmount_toStore = 0.00;
        }
      }
    }
    else if (itm.itemSaleQty != undefined) {
      //  console.log('price', itm.itemName, itm.itemId, index);
      var p = isNaN(itm.itemSaleAmt) ? lastPrice[index].monthAverage : itm.itemSaleAmt;
      if (itm.itemSaleAmt == undefined || itm.itemSaleAmt == 0) {
        if (index >= 0) {
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

          itm.itemSaleAmt = parseFloat(itm.itemSaleQty) * parseFloat(p);
          //console.log(itm, $scope.lastPrice[index]);
        }
        else {
          itm.itemSaleAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

          itm.itemSaleAmt = parseFloat(itm.itemSaleQty) * parseFloat(p);
          //console.log(itm, $scope.lastPrice[index]);
        }
        else {
          itm.itemSaleAmt = 0.00;
        }
      }
    }
    else if (itm.totalStockReturnQty != undefined) {
      var p = isNaN(itm.totalStockReturnAmount) ? lastPrice[index].monthAverage : itm.totalStockReturnAmount;
      //console.log('price', itm.itemName, itm.itemId, index);
      if (itm.totalStockReturnAmount == undefined || itm.totalStockReturnAmount == 0) {
        if (index >= 0) {
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReturnAmount = parseFloat(itm.totalStockReturnQty) * parseFloat(p);

          //console.log(itm, $scope.lastPrice[index]);
        }
        else {
          itm.totalStockReturnAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReturnAmount = parseFloat(itm.totalStockReturnQty) * parseFloat(p);

          //console.log(itm, $scope.lastPrice[index]);
        }
        else {
          itm.totalStockReturnAmount = 0.00;
        }
      }
    }
    else if (itm.totalIntermediateQty != undefined) {
      if (itm.totalIntermediateAmt == undefined || itm.totalIntermediateAmt == 0) {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalIntermediateAmt = parseFloat(itm.totalIntermediateQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalIntermediateAmt = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalIntermediateAmt = parseFloat(itm.totalIntermediateQty) * parseFloat(lastPrice[index].monthAverage);
        }
        else {
          itm.totalIntermediateAmt = 0.00;
        }
      }
    }
    else if (itm.totalStockReceiveQty != undefined) {
      var p = isNaN() ? lastPrice[index].monthAverage : itm.totalStockReceiveAmount;
      if (itm.totalStockReceiveAmount == undefined || itm.totalStockReceiveAmount == 0) {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReceiveAmount = parseFloat(itm.totalStockReceiveQty) * parseFloat(p);
        }
        else {
          itm.totalStockReceiveAmount = 0.00;
        }
      }
      else {
        if (index >= 0) {
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalStockReceiveAmount = parseFloat(itm.totalStockReceiveQty) * parseFloat(p);
        }
        else {
          itm.totalStockReceiveAmount = 0.00;
        }
      }
    }
    //}
  });
  return result;
};
