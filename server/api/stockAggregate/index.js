'use strict';

var express = require('express');
var controller = require('./stockAggregate.controller');

var router = express.Router();

//router.get('/', controller.index);
//router.get('/:id', controller.show);
router.get('/getFoodCostData', controller.getFoodCostData);
router.get('/getData', controller.getData);
//router.post('/', controller.create);
router.post('/calculateDayAggregate', controller.calculateDayAggregate);
router.post('/calculateDayAggregateFromLatestOpening', controller.calculateDayAggregateFromLatestOpening);
//router.put('/:id', controller.update);
//router.patch('/:id', controller.update);
//router.delete('/:id', controller.destroy);

module.exports = router;