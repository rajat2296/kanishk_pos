/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockAggregate = require('./stockAggregate.model');

exports.register = function(socket) {
  StockAggregate.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockAggregate.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockAggregate:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockAggregate:remove', doc);
}