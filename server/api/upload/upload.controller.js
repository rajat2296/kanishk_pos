'use strict';

var _ = require('lodash');
var parse = require('csv-parse');
var Upload = require('./upload.model');
var Item = require('../item/item.model');
var Category = require('../category/category.model');
var Supercategory = require('../superCategory/superCategory.model');
var fs = require('fs');
var path = require('path');
var async = require('async');
var xl = require('excel4node');
var request = require('request')
var config = require('../../config/environment');


function fullTrim (val) {
    return val.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');
};

// Get list of uploads
exports.index = function (req, res) {
     // console.log('Downloading...');
    var Key = new Buffer(req.query.reportType+':'+req.query.deployment_id).toString('base64');
    var _f = req.params.file.replace(Key,'');
    res.setHeader('Content-Type', 'application/csv');
    res.setHeader('Content-Disposition', 'attachment; filename="'+_f+'"');
    if(config.isQueue) {
        request.get(config.reportServer.url+'/api/uploads/'+req.params.file+'?deployment_id='+req.query.deployment_id+'&reportType='+req.query.reportType).on('error', function(err) {console.log(err)}).pipe(res)
    } else {

        var _p = path.join('/var/www/posistApp', 'uploads', req.params.file);
        var r = fs.createReadStream(_p)
        r.on('open',function(){
            r.pipe(res)
        })
    }
    
    // res.send(_p);
};

// Get a single upload
exports.show = function (req, res) {
    console.log("Downloading...");
    Upload.findById(req.params.id, function (err, upload) {
        if (err) {
            return handleError(res, err);
        }
        if (!upload) {
            return res.send(404);
        }
        return res.json(upload);
    });
};

// Creates a new upload in the DB.
exports.create = function (req, res) {

    var _p = path.join(process.cwd(), 'uploads', req.files.file.name);

    var hasHeader = true;
    var parser = parse({delimiter: ','}, function(err, data) {

        var _items = [];
        _.forEach(data, function (i, index) {
            if (hasHeader && index != 0) {
                _items.push({
                    number: i[0],
                    name: fullTrim(i[1]),
                    description: i[2],
                    rate: parseFloat(i[3]),
                    category: fullTrim(i[4]),
                    superCategoryName: fullTrim(i[5]),
                    tenant_id: "54b79d460f1ecfe41edef130",
                    deployment_id: "54b79d8f0f1ecfe41edef132"
                });
            }
        });

        var _uCats = _.uniq(_items, 'category');

        //Check if Categories exists
        var _catErrors = [];
        var _catFound = [];
        var _allCats = [];
        async.each(_uCats, function (cat, callback) {
            var q = Category.findOne({categoryName: cat.category});
            q.exec(function (err, c) {
                if (c === null) {
                    _catErrors.push(
                        {
                            category: cat.category,
                            superCategory: cat.superCategoryName
                        }
                    );
                    callback();

                } else {

                    if (c.superCategory.superCategoryName === cat.superCategoryName) {
                        _catFound.push({
                            category: cat.category,
                            superCategory: cat.superCategoryName
                        });
                        _allCats.push(c);
                        callback();

                    } else {
                        _catErrors.push({
                            category: cat.category,
                            superCategory: cat.superCategoryName
                        });
                        callback();
                    }

                }
            });
        }, function (err) {

            if (_catErrors.length > 0 ) {

                res.json(500, {
                    notfound: _catErrors,
                    found: _catFound
                });

            } else {

                var _itemsInserted = [];
                var _itemsInsertError = [];
                 //All Categories Exists - Start adding Items
                async.each(_items, function (item, callback) {

                    var _cat = _.filter(_allCats, {categoryName: item.category});
                    if (_cat.length > 0) {
                        item.category = _cat[0];
                        console.log(item);
                        Item.create(item, function (err, item) {
                            if (err) {
                                _itemsInsertError.push(err);
                                callback();
                            } else {
                                _itemsInserted.push(item);
                                callback();
                            }
                        });
                    }

                }, function (err) {

                    res.json(201,
                        {
                            items: _itemsInserted,
                            errors: _itemsInsertError
                        }
                    );

                });


            }

        });

    });

    fs.createReadStream(_p).pipe(parser);

};

// Updates an existing upload in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Upload.findById(req.params.id, function (err, upload) {
        if (err) {
            return handleError(res, err);
        }
        if (!upload) {
            return res.send(404);
        }
        var updated = _.merge(upload, req.body);
        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, upload);
        });
    });
};

// Deletes a upload from the DB.
exports.destroy = function (req, res) {
    Upload.findById(req.params.id, function (err, upload) {
        if (err) {
            return handleError(res, err);
        }
        if (!upload) {
            return res.send(404);
        }
        upload.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    return res.send(500, err);
}
