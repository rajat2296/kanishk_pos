'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PaymentsSchema = new Schema({
  deployment_id:String,
  tenant_id:String,
  paymentType:String,
  billData:{}
});

module.exports = mongoose.model('Payments', PaymentsSchema);
