'use strict';

var express = require('express');
var controller = require('./payments.controller');

var router = express.Router();


router.post('/cashfree', controller.cashfree_create);
router.post('/mobikwik/createInvoice',controller.mobikwik_createInvoice);
router.post('/ruplee/', controller.ruplee_sendBill);
router.get('/',controller.find);
router.post('/cashfree/status',controller.cashfree_status);
router.post('/cashfree/delete',controller.cashfree_delete);
router.post('/cashfree/callback',controller.cashfree_callback);
router.post('/mobikwik/delete',controller.mobikwik_delete);
router.post('/mobikwik/checkStatus',controller.mobikwik_checkStatus);
router.post('/mobikwik/callback',controller.mobikwik_callback);
router.post('/ruplee/callback',controller.ruplee_callback);
router.get('/ruplee/delete',controller.ruplee_delete);
router.post('/ruplee/status',controller.ruplee_status);
router.get('/ruplee/cancel',controller.ruplee_cancel);
router.post('/mobikwik/cancel',controller.mobikwik_cancel);
router.post('/binge/cancel',controller.binge_cancel);
router.post('/binge/sendBill',controller.binge_sendBill);
router.post('/binge/status',controller.binge_status);
router.post('/binge/delete',controller.binge_delete);
router.post('/binge/callback',controller.binge_callback);
router.post('/paytm/withdraw',controller.paytm_withdraw);
router.post('/paytm/validatePayment',controller.paytm_validatePayment);
router.post('/paytm/resendOtp',controller.paytm_resendOtp);
router.post('/paytm/checkStatus',controller.paytm_checkStatus);
router.post('/paytm/delete',controller.paytm_delete);
router.post('/paytm/callback',controller.paytm_callback);
router.post('/paytm/Webcallback',controller.paytm_Webcallback);
router.post('/paytm/refund',controller.paytm_refund)
router.post('/paytm/cancel',controller.paytm_cancel);
router.post('/paytm/delete',controller.paytm_delete);
router.post('/paytm/checksum',controller.paytm_generateChecksum);
router.post('/urbanpiper/validateAccount',controller.urbanpiper_validateAccount);
router.post('/urbanpiper/sendBill',controller.urbanpiper_sendBill);
router.post('/urbanpiper/validatePayment',controller.urbanpiper_validatePaymentByPhone);
router.post('/urbanpiper/status',controller.urbanpiper_checkStatus);
router.post('/urbanpiper/addPoints',controller.urbanpiper_addPoints);
router.post('/myoperator/webhook',controller.emitUserCalling);
router.post('/loyalty/check_balance',controller.loyalty_check_balance);
router.post('/loyalty/redeem',controller.loyalty_redeem);
router.post('/loyalty/credit_balance',controller.loyalty_credit_balance)
router.post('/ikaaz/sendBill',controller.ikaaz_sendBill);
router.post('/proxy',controller.proxyRequest);

router.get('/ikaaz/getIkaazReaderData',controller.getIkaazReaderData);
module.exports = router;
