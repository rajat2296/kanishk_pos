'use strict';

var _ = require('lodash');
var Payments = require('./payments.model');
var request=require('request');
var cashfree_appId = '586787515ee57363b1983885';
var cashfree_secretKey = 'c23482dabae1fcb3604da49df105cd3b40b1bffe';
var MobikwikSettings=require('../mobikwikSettings/mobikwikSettings.model');
var crypto=require('crypto');
var checksum=require('./checksum/checksum');
var io = null;

var production=true;             //to be changed in production

var mobikwikUrl='https://test.mobikwik.com';
var rupleeUrl='http://service.dev.ruplee.com';
var bingeUrl='http://xpos.letsbinge.com';
var paytmUrl='http://trust-uat.paytm.in';
var urbanpiperUrl='https://staging.urbanpiper.com';
var ikaazUrl='http://103.231.76.123:8280';

if(production){
  mobikwikUrl='https://walletapi.mobikwik.com';
  rupleeUrl='http://service.new.ruplee.com';
  bingeUrl='https://pos.letsbinge.com';
  paytmUrl='https://trust.paytm.in';
  urbanpiperUrl='https://api.urbanpiper.com';
  ikaazUrl='https://www.ikaaz.com:28000';
}

// send Bill to Cashfree
exports.cashfree_create = function(req, res) {

  Payments.find({deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,"billData.id":req.body.id,paymentType:'cashfree'},function(err, datas) {
    if (err) {
      console.log(err); return handleError(res,err);
    }
    else if(datas && datas.length>0)
    {
      req.body.cashFreeBill.appId=cashfree_appId;
      req.body.cashFreeBill.secretKey=cashfree_secretKey;

      delete req.body.cashFreeBill.customerPhone; delete req.body.cashFreeBill.notifyCustomer;


      var options = { method: 'POST',
        url: 'https://test.gocashfree.com/api/v1/order/update',
        headers:
        { 'content-type': 'application/x-www-form-urlencoded',
          'cache-control': 'no-cache' },
        form:req.body.cashFreeBill
      };

      request(options, function (error, response, body) {
        if (error){console.log("error"); res.status(500).send(error);}
        // console.log(response.statusCode);
        else
          res.status(200).send({body:body,method:'update'});
      });
    }
    else if(datas && datas.length==0){
      req.body.cashFreeBill.appId=cashfree_appId;
      req.body.cashFreeBill.secretKey=cashfree_secretKey;
      console.log(req.body);

      var options = { method: 'POST',
        url: 'https://test.gocashfree.com/api/v1/order/create',
        headers:
        { 'content-type': 'application/x-www-form-urlencoded',
          'cache-control': 'no-cache' },
        form:req.body.cashFreeBill
      };

      request(options, function (error, response, bod) {
        if (error){console.log("error"); return handleError(res,error)}
        // console.log(response.statusCode);
        else if(bod) {
          var body = JSON.parse(bod);
          if (body.status == 'OK') {
            console.log(body);
            cashfree_saveInvoice(req, res, body);
          }
          else
          {console.log(body);
            console.log(body.status);
            return handleError(res, {status: 'Something went wrong'});
          }
        }
        else return handleError(res,{status:"something went wrong"});
      });
    }
    else
      return handleError(res,{status:'Something went wrong'});
  });
};

//save cashfreeBill in db

function cashfree_saveInvoice(req,res,body){
  console.log("save invoice");
  var req={deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'cashfree',
    billData:{ cfid: req.body.cashFreeBill.cfid,
      id:req.body.id,
      orderId: req.body.cashFreeBill.orderId,
      orderStatus: 'active',
      deployment_id: req.body.deployment_id,
      tenant_id:req.body.tenant_id}
  }
  var cashfree=new Payments(req);

  cashfree.save(function(err, data){
    if(err){
      console.log(err);
      res.status(500).send(data);
    }else {
      console.log(data);
      res.status(200).send(data);
    }})
}

//create invoice
exports.mobikwik_createInvoice=function(req,res){
  console.log('create');

  var string="'"+req.body.bill.merchantid+"''"+req.body.bill.storeid+"''"+req.body.bill.posid+"''"+req.body.bill.orderid+"''"+req.body.bill.txnamount+"''"+req.body.bill.responseurl+"'";
  console.log(string);
  var secret=req.body.secretKey;
  var hash=crypto.createHmac('sha256', secret).update(string).digest('hex');

  console.log(hash);
  req.body.bill.checksum=hash;

  var options = { method: 'POST',
    url:mobikwikUrl+'/walletapis/merchantpayment/createinvoice',
    headers:
    { 'content-type':'application/json',
      'cache-control':'no-cache'
    },
    json:req.body.bill
  };

  request(options, function (error, response, body) {
    console.log(options);
    if (error){console.log("error",error);
      return handleError(res,error);
    }
    // console.log(response.statusCode);
    else {
      console.log(body);
      if(body.txnStatusCode==0) {
        mobikwik_saveInvoice(req,res,body);
      }
      else
        return handleError(res,body);
    }
  });

}

function mobikwik_saveInvoice(req,res,body){
  Payments.find({deployment_id:req.body.deployment_id,"billData.order_id":req.body.bill.orderid,paymentType:'mobikwik',"billData.status":'active'},function(err,docs) {
    if(err){return handleError(res,err);}
    else if(docs && docs.length>0){ console.log("bill already present"); return handleError(res,{txnStatusCode:20,txnStatusMsg:"Bill already sent to user!"})}
    else if(docs && docs.length==0) {
      console.log("create new");
      var bill = {
        merchant_id: req.body.bill.merchantid,
        store_id: req.body.bill.storeid,
        bill_id:req.body.billId,            //changed after changes in mobikwik
        pos_id: req.body.bill.posid,
        secret_key: req.body.secretKey,
        order_id: req.body.bill.orderid,
        status: 'active',
        transactionStatus:'Mkw Proc..',
        posBillId:req.body.posBillId
      };
      var request={deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'mobikwik',billData:bill};
      var mobikwik = new Payments(request);
      mobikwik.save(function (err, docs) {
        if (err) {
          console.log(err);
          return handleError(res, err);
        }
        else {
          res.status(200).send(docs);
        }
      })
    }
    else
      return handleError(res,{status:"Looks Like there is something Wrong"});
  });
}

exports.ruplee_sendBill = function(req, res) {
  console.log(req.body.tabData);

  Payments.find({"deployment_id": req.body.special_key, "billData.bill_no": req.body.bill_no,"billData.status":'active'}, function (err, data) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    }
    else if (data && data.length > 0) {
      console.log("update user-bill");
      req.body.bill.bill_id = data[0].ruplee_bill_id;
      var options = {
        method: 'POST',
        url: rupleeUrl+'/rupleeBill/update-user-bill',
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'cache-control': 'no-cache'
        },
        form: req.body.bill
      };

      request(options, function (error, response, body) {
        //console.log(body);
        //console.log(response.statusCode);
        if (error) {
          console.log(error);
          return handleError(res,error);
        }
        else if (body && response.statusCode == 500) {
          res.status(500).send(body);
        }
        else if (body && response.statusCode == 200) {
          res.status(200).send({body:body,method:'update'});

        }
      });
    }
    else if (data && data.length == 0) {
      console.log("save new  bill");

      var options = {
        method: 'POST',
        url: rupleeUrl+'/rupleeBill/user-bill-detail',
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'cache-control': 'no-cache'
        },
        form: req.body.bill
      };

      request(options, function (error, response, bod) {

        if (error) {
          console.log(error);
          res.status(500).send(error);
        }
        else if (bod) {
          console.log(bod);
          if ( bod[0] == '<') return handleError(res, {status: 0});
          var body = JSON.parse(bod);
          // console.log(body["ruplee_bill_id"]);
          // if (body["ruplee_bill_id"])
          //  console.log(body.data);
          //  console.log(response.statusCode);

          if (body && response.statusCode == 500) {
            res.status(500).send(body);
          }
          else if (body && response.statusCode == 200) {
            var rupleeBill = {
              deployment_id: req.body.bill.special_key, tenant_id: req.body.tenant_id, paymentType: 'ruplee',
              billData: {
                billId: req.body.bill.bill_no,
                status: 'active',
                ruplee_bill_id: body.ruplee_bill_id,
                transactionStatus:'Rple Proc..',
                posBillId:req.body.posBillId
              }
            };

            console.log(rupleeBill);
            var bill = new Payments(rupleeBill);
            bill.save(function (err, data) {
              if (err) {
                console.log(err);
                return handleError(res, err);
              }
              else {
                res.status(200).send(data);
              }
            })
          }
          else  return handleError(res,{error:"Bill not sent"});
        }
        else
          res.status(500).send({error: "Bill not sent!"});
      });
    }
    else return handleError(res,{error:'Bill not sent'});
  })
}

exports.ruplee_status=function(req,res){
  console.log(req.body);
  Payments.findOne({deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,"billData.billId":req.body.billId},function(err,data){
    if(data){
      console.log(data);
      if(data.billData.status=='paid') res.status(200).send({status:'Payment Successful'});
      else if(data.billData.status=='failed') return handleError(res,{status:'Payment failed'});
      else if(data.billData.status=='active') return handleError(res,{status:'Payment in progress'});
      else return handleError(res,{status:'Something went wrong'});
    }
    else
      return handleError(res,{status:'Something went wrong'});
  })
}

exports.find=function(req,res){
  Payments.find({deployment_id:req.query.deployment_id,tenant_id:req.query.tenant_id,$or:[{"billData.status":'active'},{"billData.status":'paid'},{"billData.orderStatus":'active'}]},function(err,docs){
    if(err) return handleError(res,err);
    else res.status(200).send(docs);
  })
}

exports.cashfree_status=function(req,res){
  req.body.appId=cashfree_appId;
  req.body.secretKey=cashfree_secretKey;

  var options = { method: 'POST',
    url: 'https://test.gocashfree.com/api/v1/order/info/status',
    headers:
    { 'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache' },
    form:req.body
  };

  request(options, function (error, response, body) {
    if (error) {console.log(error); res.status(500).send(error);}
    //console.log(response.statusCode);'
    else
      res.send(body);
  });
}

exports.cashfree_delete=function(req,res){
  req.body.appId=cashfree_appId;
  req.body.secretKey=cashfree_secretKey;

  var cash=JSON.parse(JSON.stringify(req.body));   //create a copy of req.body

  var options = { method: 'POST',
    url: 'https://test.gocashfree.com/api/v1/order/delete/',
    headers:
    { 'content-type': 'application/x-www-form-urlencoded',
      'cache-control': 'no-cache' },
    form:req.body
  };
  var orderId=(Number(req.body.orderId)).toString();

  request(options, function (error, response, body) {
    if (error) {console.log(error); return handleError(res,error)}
    else{
      console.log(body);
      Payments.findOne({"billData.orderId":Number(orderId),paymentType:'cashfree',deployment_id:cash.deployment_id,tenant_id:cash.tenant_id}, function(err,bill) {
        if(err)console.log(err);
        else if(bill)
        {   console.log("bill",bill);
          bill.remove(function(err,data){
            if(err)console.log(err);
            else
            {console.log(data); res.status(200).send(body);}
          })
        }
        else {console.log("not found"); return handleError(res,{status:'Not found'});}
      });

    }
    //console.log(response.statusCode);
  });
}

exports.cashfree_callback=function(req,res){

  console.log(req.body);

  if(req.body.transactionStatus.toLowerCase()=="success") {
    Payments.findOne({paymentType:'cashfree',"billData.orderId": Number((req.body.orderId).toString()), "billData.cfid": req.body.cfid}, function (err, data) {
      if (err){console.log(err);  res.status(200).send({status:'callback received'});}
      else if(data) {
        if(io)
          io.broadcast.emit("cashfree", data);
        console.log("socket emitted", data);
        res.status(200).send({status:'callback received'});
      }
      else  res.status(200).send({status:'callback received'});
    })
  }
  else  res.status(200).send({status:'callback reeived'});
}

exports.mobikwik_checkStatus=function(req,res) {
  var string="'"+req.body.merchantid+"''"+req.body.storeid+"''"+req.body.posid+"''"+req.body.orderid+"'";
  console.log(string);
  var secret=req.body.secretKey;
  var hash=crypto.createHmac('sha256', secret).update(string).digest('hex');
  req.body.checksum=hash;
  delete req.body.secretKey;

  var options = { method: 'POST',
    url:mobikwikUrl+'/walletapis/merchantpayment/checkstatus',
    headers:
    { 'content-type': 'application/json',
      'cache-control': 'no-cache'
    },
    json:req.body
  };

  request(options, function (error, response, body) {
    if (error) {
      console.log("error");
      return handleError(res, error);
    }
    // console.log(response.statusCode);
    else {
      console.log(body);
      if (body.statuscode == 0) {
        res.status(200).send(body);
      }
      else
        return handleError(res, body);
    }
  })
}

//payment callback
exports.mobikwik_callback=function(req,res){
  if(req.body.txnstatuscode=="0"||req.body.statuscode=="0") {
    Payments.findOne({"billData.store_id": req.body.storeid,"billData.status":'active',"billData.order_id":req.body.orderid}, function (err, data) {
      if (err) {
        console.log(err);
        return res.status(200).send({status:"callback received"});
      }
      else {
        if (data) {
          console.log(data);
          console.log(req.body.orderid);
          //var data={merchant_id:req.body.merchantid,store_id:req.body.storeid,pos_id:req.body.posid,order_id:req.body.orderid,secret_key:setting.secret_key,status:'paid'};
          if(io) {
            io.broadcast.emit("mobikwik_bill_paid_" + data.deployment_id, data);
            console.log("socket emitted", data);
            res.status(200).send({status:'callback reeived'});
          }
        } else {console.log("not found"); res.status(200).send({status:"callback received"});}
      }
    })
  }

}

exports.mobikwik_delete=function(req,res) {
  console.log("delete");
  if (req.body.status)
    mobikwik_removeInvoice(req, res);
  else {
    var string = "'" + req.body.merchantid + "''" + req.body.storeid + "''" + req.body.posid + "''" + req.body.orderid + "'";
    console.log(string);
    var secret = req.body.secretKey;
    var hash = crypto.createHmac('sha256', secret).update(string).digest('hex');

    console.log(hash);
    req.body.checksum = hash;
    delete req.body.secretKey;
    delete req.body.status;
    delete req.body.filter;

    var options = {
      method: 'POST',
      url: mobikwikUrl+'/walletapis/merchantpayment/cancelinvoice',
      headers: {
        'content-type': 'application/json',
        'cache-control': 'no-cache',
      },
      json: req.body
    };

    request(options, function (error, response, body) {
      if (error) {
        console.log("error");
        return handleError(res, error);
      }
      // console.log(response.statusCode);
      else {
        console.log("delete mobikwik", body);
        if (body.statuscode == 0) {
          mobikwik_removeInvoice(req, res);
        }
        else
          return handleError(res, body);
      }
    })
  }
}

function mobikwik_removeInvoice(req,res){
  console.log("remove");
  console.log(req.body.orderid,req.body.merchantid);
  Payments.findOne({paymentType:'mobikwik',"billData.order_id":req.body.orderid,"billData.merchant_id":req.body.merchantid}, function(err,bill) {
    if(err){console.log("error",err); return handleError(res,err);}
    else if(bill)
    {console.log("bill",bill);
      bill.billData.status='closed';
      bill.markModified("billData");
      bill.save(function(err,data){
        if(err){console.log(err); return handleError(res,err);}
        else
        {console.log(data); res.status(200).send({txnstatuscode:0});
        }})
    }
    else return handleError(res,{status:"Not found"});
  })
}

exports.mobikwik_cancel=function(req,res){
  var string = "'" + req.body.merchantid + "''" + req.body.storeid + "''" + req.body.posid + "''" + req.body.orderid + "'";
  console.log(string);
  console.log(req.body.secretKey);
  var secret = req.body.secretKey;
  var hash = crypto.createHmac('sha256', secret).update(string).digest('hex');

  console.log(hash);
  req.body.checksum = hash;
  delete req.body.secretKey;
  delete req.body.status;
  delete req.body.filter;

  var options = {
    method: 'POST',
    url: mobikwikUrl+'/walletapis/merchantpayment/cancelinvoice',
    headers: {
      'content-type': 'application/json',
      'cache-control': 'no-cache',
    },
    json: req.body
  };

  request(options, function (error, response, body) {
    if (error) {
      console.log("error");
      return handleError(res, error);
    }
    // console.log(response.statusCode);
    else {
      console.log("delete mobikwik", body);
      if (body.statuscode == 0) {
        mobikwik_cancelInvoice(req, res);
      }
      else
        return handleError(res, body);
    }
  })
}

function mobikwik_cancelInvoice(req,res){
  console.log(req.body.orderid,req.body.merchantid);
  Payments.findOne({paymentType:'mobikwik',"billData.order_id":req.body.orderid,"billData.merchant_id":req.body.merchantid,"billData.status":'active'}, function(err,bill) {
    if(err){console.log("error",err); return handleError(res,err);}
    else if(bill)
    {console.log("bill",bill);
      bill.billData.status='cancelled';
      bill.markModified("billData");
      bill.save(function(err,data){
        if(err){console.log(err); return handleError(res,err);}
        else
        {console.log(data); res.status(200).send({txnstatuscode:0});
        }})
    }
    else return handleError(res,{status:"Not found"});
  })
}

exports.ruplee_callback=function(req,res){
  console.log("body",req.body);
  if(req.body.paymentStatusCode==1){
    Payments.findOne({"deployment_id":req.body.specialKey,"billData.billId":req.body.invoiceNo,"billData.status":'active'},function(err,bill){
      if(bill) {
        bill.billData.status='paid';
        bill.markModified("billData");
        bill.save(function (err, data) {
          if (err)console.log(err);
          else
          {console.log(data);  if(io){ io.broadcast.emit('ruplee'+req.body.specialKey,data);}  res.status(200).send({message:'Callback received'});}
        })
      }
      else{
        console.log("not found"); res.status(200).send({status:'Callback received'});
      }
    })
  }
  else if(req.body.paymentStatusCode==0){
    Payments.findOne({"deployment_id":req.body.specialKey,"billData.billId":req.body.invoiceNo},function(err,bill){
      if(bill) {
        bill.billData.status='failed';
        bill.markModified("billData");
        bill.save(function (err, data) {
          if (err)console.log(err);
          else
          {console.log(data);  if(io){ io.broadcast.emit('ruplee'+req.body.specialKey,data);}  res.status(200).send({message:'Callback received'});}
        })
      }
      else{
        console.log("not found"); res.status(200).send({status:'Callback received'});
      }
    })
  }

}


exports.ruplee_delete=function(req,res){
  Payments.findOne({"deployment_id":req.query.deployment_id,"paymentType":'ruplee',"billData.bill_no":req.query.bill_no},function(err,bill){
    if(bill)
    {
      bill.billData.status='closed';
      bill.markModified("billData");
      bill.save(function(err,data){
        if(err) console.log(err);
        else
        {console.log(data); res.status(200).send({status:'Bill closed'});}
      })
    }
    else return handleError(res,{status:"Ruplee bill not found"});
  })
}

exports.ruplee_cancel=function(req,res) {
  Payments.findOne({
    "deployment_id": req.query.deployment_id,
    "paymentType": 'ruplee',
    "billData.billId": req.query.billId,
    "billData.status":'active'
  }, function (err, bill) {
    if (bill) {
      bill.billData.status = 'cancelled';
      bill.markModified("billData");
      bill.save(function (err, data) {
        if (err) console.log(err);
        else {
          console.log(data);
          res.status(200).send({status:'Bill Cancelled'});
        }
      })
    }
    else return handleError(res, {status: "Ruplee bill not found"});
  })
}

exports.binge_sendBill = function(req, res) {
  var options = {
    method: 'POST',
    url: bingeUrl+'/userorder',
    headers: {'content-type':'application/json',authorization:'Basic '+req.body.auth},
    json:req.body.request
  };

  request(options, function (error, response, body) {
    console.log(options);
    //console.log(req.body.request.client.order);
    if (error) {
      console.log("error", error);
      return handleError(res, error);
    }
    else { console.log(body);
      if(body.server && body.server.request && body.server.request.status=="true" && body.server.order)
        binge_saveInvoice(req,res,body);
      else if(body.server.error) return handleError(res,{status:body.server.error.message});
      else return handleError(res,{status:'Something went wrong'});
    }
  })
};

function binge_saveInvoice(req,res,body){
  var bill={deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'binge',
    billData:{
      billId:req.body.billId,
      bingeOrderNo:body.server.order.orderno,
      bingeId:body.server.order.bingeid,
      status:'active',
      transactionStatus:'Bng Proc..',
      restaurantId:body.server.restaurant.id,
      bill:req.body.request,
      username:req.body.username,
      password:req.body.password,
      posBillId:req.body.posBillId
    }}
  var _bill=new Payments(bill);
  _bill.save(function(err,data){
    if(data) res.status(200).send(data);
    else return handleError(res,{status:'Something went wrong!'});
  })
}

exports.binge_status=function(req,res){
  var options = {
    method: 'POST',
    url: bingeUrl+'/checkpayment',
    headers: {'content-type':'application/json',authorization:'Basic '+req.body.auth},
    json:req.body.request
  };

  request(options, function (error, response, body) {
    console.log(options);
    // console.log(req.body.request.client.order);
    if (error) {
      console.log("error", error);
      return handleError(res, error);
    }
    else { console.log(body);
      if(body.server && body.server.order && body.server.order.payments && body.server.order.payments.status=="true")
        res.status(200).send({"status":"payment successful"});
      else return handleError(res,{status:'payment still active'});
    }
  })
}

exports.binge_callback=function(req,res){
  console.log(req.body.restaurantid,req.body.orderno);
  Payments.findOne({paymentType:'binge',"billData.restaurantId":req.body.restaurantid,"billData.bingeOrderNo":Number(req.body.orderno),"billData.status":'active'},function(err,data){
    if(data){
      console.log(data);  if(io){ io.broadcast.emit('binge'+data.deployment_id,data);}  res.status(200).send({message:'Callback received'});
    }
    else  { console.log("Not found"); res.status(200).send({"status":'callback received'});}
  })
}

exports.binge_delete = function(req, res) {
  var options = {
    method: 'POST',
    url: bingeUrl+'/closebill',
    headers: {'content-type':'application/json',authorization:'Basic '+req.body.auth},
    json:req.body.request
  };

  request(options, function (error, response, body) {
    //console.log(req.body.request.client.order);
    if (error) {
      console.log("error", error);
      return handleError(res, error);
    }
    else { console.log(body);
      if(body.server && body.server.request && body.server.request.status=="true")
        binge_removeInvoice(req,res);
      else return handleError(res,{status:'Binge order not closed'});
    }
  })
};

function binge_removeInvoice(req,res){
  console.log("deleting binge order from mongo");
  Payments.findOne({deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'binge',"billData.billId":req.body.billId},function(err,data){
    if(data){
      data.billData.status='closed';
      data.markModified("billData");
      data.save(function(err,datas){
        if(datas) res.status(200).send({status:'Binge order closed'});
        else return handleError(res,{status:"Binge order not deleted"});
      })
    }
    else return handleError(res,{status:'Binge order not found'});
  })
}

exports.binge_cancel=function(req,res){
  var options = {
    method: 'POST',
    url: bingeUrl+'/cancelorder',
    headers: {'content-type':'application/json',authorization:'Basic '+req.body.auth},
    json:req.body.request
  };

  request(options, function (error, response, body) {
    //console.log(req.body.request.client.order);
    if (error) {
      console.log("error", error);
      return handleError(res, error);
    }
    else { console.log(body);
      if(body.server && body.server.request && body.server.request.status=="true")
        binge_cancelInvoice(req,res);
      else return handleError(res,{status:'Binge order not closed'});
    }
  })
}

function binge_cancelInvoice(req,res){
  Payments.findOne({deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'binge',"billData.billId":req.body.billId,"billData.status":'active'},function(err,data){
    if(data){
      data.billData.status='cancelled';
      data.markModified("billData");
      data.save(function(err,datas){
        if(datas) res.status(200).send({status:'Binge order closed'});
        else return handleError(res,{status:"Binge order not deleted"});
      })
    }
    else return handleError(res,{status:'Binge order not found'});
  })
}

exports.paytm_Webcallback=function(req,res){
  console.log("body",req.body);
  if(req.body.STATUS=="TXN_SUCCESS"){
      res.writeHead(302, {
    'Location': 'http://134.213.156.80:8088/order-placed/'
  });
  res.end();
  }
  if(req.body.STATUS=="TXN_FAILURE")
  {
     res.writeHead(302, {
    'Location': 'http://134.213.156.80:8088/payment-failure/'
  });
  res.end();
  }

}

exports.paytm_withdraw=function(req,res){
  checksum.genchecksumbystring(JSON.stringify(req.body.bill),req.body.secret_key,function(err,data){
    if(data){
      var hash=data;

      var options = { method: 'POST',
        url:paytmUrl+'/wallet-web/v7/withdraw',
        headers:
        { 'content-type': 'application/json',
          'checksumhash':hash,
          'mid':req.body.mid,
          'phone':req.body.mobile,
          'otp':req.body.bill.request.totp
        },
        json:req.body.bill

      };
      var option={};
      option.method='POST';
      option.url='http://flipkart.posist.org/api/payments/proxy'
      option.headers={'content-type':'application/json'};
      option.json=options;

      request(option, function (error, response, body) {
        console.log(option);
        if (error) {
          console.log("error",error);
          return handleError(res, error);
        }
        else if(body && body.status=="SUCCESS") {
          console.log(body);
           //res.status(200).send({body:body});
           paytm_saveInvoice(req,res,body);
        }
        else if(body && body.statusMessage) {
          console.log("body",body);
          return handleError(res, {status:body.statusMessage});
        }
        else {
          console.log("body",response);
          return handleError(res, {status: 'Something went wrong!'});
        }
      });
    }
    else
      return handleError(res,{status:"Something went wrong!"})
  })
}

function paytm_saveInvoice(req,res,body){
  var bill={deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'paytm',
   billData:{
    order_id:req.body.bill.request.merchantOrderId,
    amount:req.body.bill.request.totalAmount,
    merchant_guid:req.body.mid,secret_key:req.body.secret_key,status:'paid',txnId:body.response.walletSystemTxnId,mobile:req.body.mobile,
    transactionStatus:'Ptm Proc..',posBillId:req.body.posBillId
  }}
  Payments.findOneAndUpdate({deployment_id:req.body.deployment_id,paymentType:'paytm',"billData.posBillId":bill.billData.posBillId},bill,{upsert:true},function(err,doc){
    if(doc) res.status(200).send({status:'ok'});
    else
      return handleError(res,{status:'error'});
  });
}

exports.paytm_cancel=function(req,res){
  checksum.genchecksumbystring(JSON.stringify(req.body.body),req.body.secret_key,function(err,data) {
    if (data) {
      var hash = data;

      var options = {
        method: 'POST',
        url: paytmUrl+'/wallet-web/editWithdrawDetail',
        headers: {
          'content-type': 'application/json',
          'checksumhash': hash,
          'mid': req.body.mid,
          'phone': req.body.mobile
        },
        json: req.body.body
      };

      request(options, function (error, response, body) {
        console.log(options);
        if (error) {
          console.log("error", error);
          return handleError(res, error);
        }
        // console.log(response.statusCode);
        else if (body && body.statusCode == 'WM_5003')
          paytm_cancelInvoice(req, res, body);

        else if (body) {
          console.log("body", body);
          return handleError(res, {status: body.statusMessage});
        }
        else {
          console.log("body", response);
          return handleError(res, {status: 'Bill could not be cancelled'});
        }
      })
    }
    else return handleError(res, {status: 'Bill Could not be cancelled'});
  })
}

function paytm_cancelInvoice(req,res,body){
  Payments.findOne({deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'paytm',"billData.status":'active',"billData.order_id":req.body.body.request.merchantOrderId},function(err,bill){
    if(bill){
      console.log("bill",bill);
      bill.billData.status='cancelled';
      bill.markModified('billData');
      bill.save(function(err,data){
        if(data) res.status(200).send({status:'Bill Cancelled'});
        else
          return handleError(res,{status:'Bill Could not be cancelled'});
      })
    }
    else return handleError(res,{status:'Bill could not be cancelled'});
  })
}



exports.paytm_validatePayment=function(req,res){
  console.log(req.body.secret_key);
  checksum.genchecksumbystring(JSON.stringify(req.body.data),req.body.secret_key,function(err,data){
    if(data){
      var hash=data;

      var options = { method: 'POST',
        url:paytmUrl+'/wallet-web/v5/validateTransaction',
        headers:
        { 'content-type': 'application/json',
          'checksumhash':hash,
          'mid':req.body.mid,
          'phone':req.body.mobile
        },
        json:req.body.data
      };

      request(options, function (error, response, body) {
        console.log(options);
        if (error) {
          console.log("error",error);
          return handleError(res, error);
        }
        // console.log(response.statusCode);
        else if(body && body && body.statusCode=='SS_0001') {
          console.log(body);
          res.status(200).send({status:body.statusMessage});
        }
        else if(body && body.statusMessage) {
          console.log("body",body);
          return handleError(res, {status:body.statusMessage});
        }
        else
          return handleError(res,{status:'Something went wrong!'});
      })
    }
    else
      return handleError(res,{status:"Something went wrong!"})
  })
}

exports.paytm_resendOtp=function(req,res){

  checksum.genchecksumbystring(JSON.stringify(req.body.body),req.body.secret_key,function(err,data){
    if(data){
      var hash=data;

      var options = { method: 'POST',
        url:paytmUrl+'/wallet-web/v3/resendOtp',
        headers:
        { 'content-type': 'application/json',
          'checksumhash':hash,
          'mid':req.body.mid,
          'phone':req.body.mobile
        },
        json:req.body.body
      };

      request(options, function (error, response, body) {
        console.log(options);
        if (error) {
          console.log("error",error);
          return handleError(res, error);
        }
        // console.log(response.statusCode);
        else if(body && body && body.statusCode=='OTP_1001') {
          console.log(body);
          res.status(200).send({status:body.statusMessage});
        }
        else if(body) {
          console.log("body",body);
          return handleError(res, {status:body.statusMessage});
        }
        else
          return handleError(res,{status:'Something went wrong!'});
      })
    }
    else
      return handleError(res,{status:"Something went wrong!"})
  })
}

exports.paytm_checkStatus=function(req,res){
   /*Payments.findOne({deployment_id:req.body.deployment_id,paymentType:'paytm',"billData.status":'paid',"billData.posBillId":req.body.posBillId},function(err,doc){
       if(doc)
          res.status(200).send({status:'success',amount:doc.billData.amount});
         else
          return handleError(res,{status:'error'});
       })*/
  checksum.genchecksumbystring(JSON.stringify(req.body.bill),req.body.secret_key,function(err,data){
     if(data){
       var hash=data;
       var options = { method: 'POST',
         url:paytmUrl+'/wallet-web/txnStatusList',
         headers:
          { 'content-type': 'application/json',
            'checksumhash':hash,
            'mid':req.body.mid
          },
        json:req.body.bill
      };

      request(options, function (error, response, body) {
        console.log(options,body);
        if (error) {
          console.log("error",error);
          return handleError(res, error);
        }
        else if(body && body.txnList && body.txnList[0] && body.txnList[0].status==1)
           return res.status(200).send({status:'success',amount:body.txnList[0].txnAmount});
        else
          return handleError(res,{status:'Either No transaction or transaction not successful'});
      });
    }
    else
      return handleError(res,{status:"Something went wrong!"})
   })
 }


exports.paytm_refund=function(){
  checksum.genchecksumbystring(JSON.stringify(req.body.body),req.body.secret_key,function(err,data){
    if(data){
      var hash=data;
      var options = { method: 'POST',
        url:paytmUrl+'/wallet-web/refundWalletTxn',
        headers:
        { 'content-type': 'application/json',
           'checksumhash':hash,
           'mid':req.body.mid,
           'phone':req.body.mobile
        },
        json:req.body.bill
      };

   Payments.findOne({deployment_id:req.body.deployment_id,paymentType:'paytm',status:'closed',posBillId:req.body.posBillId},function(err,doc){
     if(doc){
        options.json.txnGuid=doc.txnId;

        request(options, function (error, response, body) {
         if(body && body.response &&  body.response.refundTxnStatus=='success'){
          Payments.update({deployment_id:req.body.deployment_id,paymentType:'paytm',posBillId:req.body.posBillId},{$set:{"billData.status":'refunded'}},function(err,num){
            res.status(200).send({status:'success'});
         });
        }
         else
          return handleError(res,{status:body.statusMessage});
       })
      }
      else return handleError(res,{status:'Transaction not found'});
     });
    }
    else
      return handleError(res,{status:"Something went wrong!"})
  })
}

exports.set=function(socket){
  io=socket;
}

exports.paytm_delete=function(req,res){
  Payments.findOne({paymentType:'paytm',deployment_id:req.body.deployment_id,"billData.order_id":req.body.order_id},function(err,bill){
    if(bill) {
      bill.billData.status='closed';
      bill.markModified("billData");
      bill.save(function (err, data) {
        if (err){console.log(err); return handleError(res,{status:'bill not closed'});}
        else
        { res.status(200).send({status:'bill closed'});}
      })
    }
    else{
      console.log("not found"); res.status(500).send({status:'not found'});
    }
  })
}

exports.paytm_callback=function(req,res){
  console.log("body",req.body);
  return res.status(200).send(req.body)
  if(req.body.statusCode=="SUCCESS"){
    Payments.findOne({paymentType:'paytm',"billData.order_id":req.body.orderId},function(err,bill){
      if(bill) {
        bill.billData.status='paid';
        bill.markModified("billData");
        bill.save(function (err, data) {
          if (err)console.log(err);
          else
          {console.log(data);  if(io){ io.broadcast.emit('paytm'+data.deployment_id,data);}  res.status(200).send({message:'Callback received'});}
        })
      }
      else{
        console.log("not found"); res.status(200).send({status:'Callback received'});
      }
    })
  }
}


exports.urbanpiper_sendBill=function(req,res){

  if(req.body.method=='phone')
    var url=urbanpiperUrl+'/api/v2/card/balance/?format=json&customer_phone=%2B91'+req.body.phone+'&amount=-'+req.body.amount;
  else if(req.body.method=='card')
    var url=urbanpiperUrl+'/api/v2/card/balance/?format=json&card_number='+req.body.cardNumber+'&amount=-'+req.body.amount;
  var options = { method: 'POST',
    url: url,
    headers:
    {'authorization':'apikey '+req.body.username+':'+req.body.apiKey },
  };

  request(options, function (error, response, bod) {
    console.log(options);
   // console.log("status",response.statusCode);
    if (error){console.log("error",error); return handleError(res,error)}
    else if(bod){
      var body = typeof(bod) == 'object' ? bod : JSON.parse(bod);
      console.log(body);
      if(req.body.method=='phone' && body.result && body.result=='error.invalid_pin')
        res.status(200).send({status:'success',method:'phone'});

      else if(req.body.method=='card' && body.result && body.result=='success')
        res.status(200).send({status:'success',method:'card'});

      else return handleError(res,{status:body.message});
    }
    else return handleError(res,{status:'Something went wrong'});
  });
}


exports.urbanpiper_validatePaymentByPhone=function(req,res){
  var options = { method: 'POST',
    url: urbanpiperUrl+'/api/v2/card/balance/?format=json&customer_phone=%2B91'+req.body.phone+'&amount=-'+req.body.amount+'&pin='+req.body.pin +'&rc=1' ,
    headers:
    {'authorization':'apikey '+req.body.username+':'+req.body.apiKey },
  };

  request(options, function (error, response, bod) {
    console.log(options);
    if (error){console.log("error"); return handleError(res,error)}
    else if(bod){
      var body = typeof(bod) == 'object' ? bod : JSON.parse(bod);
      console.log(body);
      if(body.result && body.result=='success')
        urbanpiper_saveInvoice(req,res);
      else return handleError(res,{status:body.message});
    }
    else return handleError(res,{status:'Payment Could not be processed'});
  });
}

exports.urbanpiper_checkStatus=function(req,res){
  Payments.findOne({deployment_id:req.body.deployment_id,paymentType:'urbanpiper',"billData.status":'paid',"billData.posBillId":req.body.posBillId},function(err,doc){
       if(doc)
          res.status(200).send({status:'success',amount:doc.billData.amount});
         else
          return handleError(res,{status:'error'});
   });
}

function urbanpiper_saveInvoice(req,res){
 var body={deployment_id:req.query.deployment_id,tenant_id:req.query.tenant_id,paymentType:'urbanpiper',
            billData:{posBillId:req.query.posBillId,status:'paid',amount:req.body.amount}
            }
  Payments.findOneAndUpdate({deployment_id:req.body.deployment_id,paymentType:'urbanpiper',"billData.posBillId":body.billData.posBillId},body,{upsert:true},function(err,doc){
    if(doc) res.status(200).send({status:'ok'});
    else
      return handleError(res,{status:'error'});
  });
}

exports.urbanpiper_validateAccount=function(req,res){

  if(req.body.method=='phone')
    var url=urbanpiperUrl+'/api/v2/card/balance/?format=json&customer_phone=%2B91'+req.body.phone+'&amount=0';

  var options = { method: 'POST',
    url: url,
    headers:
    {'Authorization':'apikey '+req.body.username+':'+req.body.apiKey },
    //form:{customer_mobile:'%2091'+req.body.phone,format:'json'}
  };

  request(options, function (error, response, bod) {
    console.log(options);
    if(response && response.statusCode)
    console.log(response.statusCode);
    if (error){console.log("error",error); return handleError(res,error)}
    else if(bod){
      var body = typeof(bod) == 'object' ? bod : JSON.parse(bod);
      console.log(body);
      if (body.result=='success')
        res.status(200).send(body);
      else if(body.result=='error not_registered')
      //urbanpiper_registerAccount(req,res);
        return handleError(res,{status:'User not registered'});
      else return handleError(res,{status:body.message});
    }
    else return handleError(res,{status:'Something went wrong'});
  });
}

/*function urbanpiper_registerAccount(req,res){
 if(req.body.method=='phone')
 var url=urbanpiperUrl+'/api/v2/card/?format=json&customer_phone=%2B91'+req.body.phone;
 else if(req.body.method=='card')
 var url=urbanpiperUrl+'/api/v2/card/?format=json&card_number='+req.body.cardNumber;

 var options = { method: 'POST',
 url: url,
 headers:
 {'Authorization':'apikey '+req.body.username+':'+req.body.apiKey },
 //form:{customer_mobile:'%2091'+req.body.phone,format:'json'}
 };

 request(options, function (error, response, bod) {
 console.log(options);
 if (error){console.log("error",error); return handleError(res,error)}
 else if(bod){
 var body=JSON.parse(bod);
 console.log(body);
 if (body.result=='success')
 res.status(200).send(body);
 else return handleError(res,{status:body.message});
 }
 else return handleError(res,{status:'Something went wrong'});
 });
 }

 exports.paytm_barcode_withdraw=function(req,res){
 checksum.genchecksumbystring(JSON.stringify(req.body.bill),req.body.secret_key,function(err,data){
 if(data){
 var hash=data;

 var options = { method: 'POST',
 url:paytmBarcodeUrl+'/wallet-web/v6/withdraw',
 headers:
 { 'content-type': 'application/json',
 'checksumhash':hash,
 'mid':req.body.mid,
 'code':req.body.barcode
 },
 json:req.body.bill
 };

 request(options, function (error, response, body) {
 console.log(options);
 if (error) {
 console.log("error",error);
 return handleError(res, error);
 }
 // console.log(response.statusCode);
 else if(body && body.response && body.statusCode=='SS_0001') {
 res.status(200).send({status:'success'});
 }
 else if(body) {
 console.log("body",body);
 return handleError(res, {status:body.statusMessage});
 }
 else { console.log("body",response);
 return handleError(res, {status: 'Something went wrong!'});
 }
 })
 }
 else
 return handleError(res,{status:"Something went wrong!"})
 })
 }*/

exports.urbanpiper_addPoints=function(req,res){
  req.body.phone = req.body.phone.substring(req.body.phone.length-10);
  var url=urbanpiperUrl+'/api/v2/card/?format=json&customer_phone=%2B91'+req.body.phone+'&customer_name='+req.body.name;

  var options = { method: 'POST',
    url: url,
    headers:
    {'Authorization':'apikey '+req.body.username+':'+req.body.apiKey },
  };

  request(options, function (error, response, bod) {
    console.log(options);
    if (error){console.log("error",error); return handleError(res,error)}
    else if(bod){
      var body = typeof(bod) == 'object' ? bod : JSON.parse(bod);
      console.log(body);
      if (body.result=='success'||body.result=='error.phone_exists')
        addPointsToUrbanPiper(req,res);
      else return handleError(res,{status:body.message});
    }
    else return handleError(res,{status:'Something went wrong'});
  });
}

function addPointsToUrbanPiper(req,res){
  req.body.phone = req.body.phone.substring(req.body.phone.length-10);
  var url=urbanpiperUrl+'/api/v2/purchase/?format=json'+'&amount='+Math.round(Number(req.body.amount))+'&customer_phone=%2B91'+req.body.phone+'&bill_id='+req.body.bill_id;
  var options = { method: 'POST',
    url: url,
    json:{sku_data:req.body.sku_data},
    headers:
    {'authorization':'apikey '+req.body.username+':'+req.body.apiKey },
  };

  request(options, function (error, response, bod) {
    console.log(options);
    if (error){console.log("error"); return handleError(res,error)}

    else if(bod){
      var body = typeof(bod) == 'object' ? bod : JSON.parse(bod);
      console.log(body);
      if(body.result=='success') res.status(200).send({status:body.message});
      else return handleError(res,{status:body.message});
    }
    else return handleError(res,{status:'Something went wrong'});
  });
}

exports.emitUserCalling=function(req,res){
  if(io)
    io.broadcast.emit('customerCalling_'+req.body._ci,{companyId:req.body._ci,_cl:req.body._cl,callerNumber:req.body._cr,callerName:req.body._cm});   //socket change by rajat
  res.status(200).send({message:'ok'});
}

exports.loyalty_check_balance=function(req,res){
 var url='http://posistapi.com:8000/api/webhooks/hit?event_name=check_customer_balance='+req.body.client_id;
    try {
      var options = {
        method: 'POST',
        url: url,
        headers:{'content-type':'application/json',
          'accept':'application/json',
        },
        json:req.body
      };

      request(options, function (error, response, body) {
        console.log(options);
         if( response && response.statusCode==200 && body.customer_balance)
          res.status(200).send({customer_balance:body.customer_balance});
         else return handleError(res, {status: 'failure'},500);
      });
    }
    catch(e){
      console.log("exception",e);
      return res.status(500).send({message:'An exception occurred'});
    }
}

exports.loyalty_redeem=function(req,res){
   var url='http://posistapi.com:8000/api/webhooks/hit?event_name=assign_delivery&client_id='+req.body.client_id;
    try {
      var options = {
        method: 'POST',
        url: url,
        headers:{'content-type':'application/json',
          'accept':'application/json',
        },
        json:req.body
      };

      request(options, function (error, response, body) {
        console.log(options);
         if( response && response.statusCode==200)
          res.status(200).send({status:'ok'});
         else return handleError(res, {status: 'failure'},500);
      });
    }
    catch(e){
      console.log("exception",e);
      return res.status(500).send({message:'An exception occurred'});
    }
}

exports.loyalty_credit_balance=function(req,res){
   var url='http://posistapi.com:8000/api/webhooks/hit?event_name=assign_delivery&client_id='+req.body.client_id;
    try {
      var options = {
        method: 'POST',
        url: url,
        headers:{'content-type':'application/json',
          'accept':'application/json',
        },
        json:req.body
      };

      request(options, function (error, response, body) {
        console.log(options);
         if( response && response.statusCode==200)
          res.status(200).send({status:'ok'});
         else return handleError(res, {status: 'failure'},500);
      });
    }
    catch(e){
      console.log("exception",e);
      return res.status(500).send({message:'An exception occurred'});
    }
}

exports.ikaaz_sendBill=function(req,res){
   var options = { method: 'POST',
        url:ikaazUrl+'/iMapAdmin/imap/processRequest',
        /*headers:
        { 'content-type': 'application/json'},*/
        formData:req.body.bill
      };

      request(options, function (error, response, bod) {
        console.log(options,bod);

        if(!bod) return handleError(res,{status:"error"});
      var body = typeof(bod) == 'object' ? bod : JSON.parse(bod);
      if(body && body.errorCode=="0"){
        var balance=body.errorMessage.substring(body.errorMessage.indexOf('&')+1,body.errorMessage.length);
        ikaaz_saveInvoice(req,res,balance);
      }
      else
        return handleError(res,{status:'error'});
    });
}

function ikaaz_saveInvoice(req,res,balance){
  var body={deployment_id:req.body.deployment_id,tenant_id:req.body.tenant_id,paymentType:'ikaaz',
            billData:{posBillId:req.body.posBillId,status:'paid',amount:req.body.bill.aamount}
            }
  Payments.findOneAndUpdate({deployment_id:req.body.deployment_id,paymentType:'ikaaz',"billData.posBillId":body.billData.posBillId},body,{upsert:true},function(err,doc){
    if(doc) res.status(200).send({status:'ok',balance:balance});
    else
      return handleError(res,{status:'error'});
  });
}

exports.ikaaz_checkStatus=function(req,res){
   Payments.findOne({deployment_id:req.body.deployment_id,paymentType:'ikaaz',"billData.status":'paid',"billData.posBillId":req.body.posBillId},function(err,doc){
       if(doc)
          res.status(200).send({status:'success',amount:doc.billData.amount});
         else
          return handleError(res,{status:'error'});
   });
 }

 exports.getIkaazReaderData=function(req,res){
    var options = { method: 'GET',
        url:'http://localhost:8080/TapPay/Execute',
      };
      console.log("start");
      request(options, function (error, response, body) {
         console.log("body",body);
          return res.status(200).send(body);
      });
 }
exports.proxyRequest=function(req,res){
   request(req.body, function (error, response, body) {
        res.status(200).send(body);
      });
}

exports.paytm_generateChecksum=function(req,res){
console.log("POST Order start");
        var paramlist = req.body;
        var paramarray = {};
        //console.log(paramlist);
        for (var name in paramlist)
        {
          if (name == 'PAYTM_MERCHANT_KEY') {
               var PAYTM_MERCHANT_KEY = paramlist[name] ;
            }else
            {
            paramarray[name] = paramlist[name] ;
            }
        }
        //console.log(paramarray);
        paramarray['CALLBACK_URL'] = 'http://localhost:3000/response';  // in case if you want to send callback
        console.log(PAYTM_MERCHANT_KEY);

checksum.genchecksum(paramarray, PAYTM_MERCHANT_KEY, function (err, result)
{
console.log(result);
if(!err)
    return res.status(200).json(result);
    else
    return handleError(res, {status: 'error'});
});
}

function handleError(res, err) {
  return res.status(500).send(err);
}
