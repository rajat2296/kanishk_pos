/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Payments = require('./payments.model');

exports.register = function(socket) {
  Payments.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Payments.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('payments:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('payments:remove', doc);
}