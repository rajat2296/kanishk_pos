'use strict';

var _ = require('lodash');
var OnlineOrderSettings = require('./onlineOrderSettings.model');

// Get list of onlineOrderSettingss
exports.index = function(req, res) {
  console.log(req.body);
  var a=req.body;
  OnlineOrderSettings.update({tenant_id:req.body.tenant_id,deploymentId:req.body.deploymentId},req.body,{upsert:true},function (err, onlineOrderSettingss) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(onlineOrderSettingss);
  });
};

// Get a single onlineOrderSettings
exports.show = function(req, res) {
  OnlineOrderSettings.findOne({deploymentId:req.params.id}, function (err, onlineOrderSettings) {
    if(err) { return handleError(res, err); }
    if(!onlineOrderSettings) { return res.status(404).send('Not Found'); }
    return res.json(onlineOrderSettings);
  });
};

// Creates a new onlineOrderSettings in the DB.
exports.create = function(req, res) {
  // OnlineOrderSettings.create(req.body, function(err, onlineOrderSettings) {
  //   if(err) { return handleError(res, err); }
  //   return res.status(201).json(onlineOrderSettings);
  // });
  console.log(req.body);
  delete req.body._id
  OnlineOrderSettings.update({tenant_id:req.body.tenant_id,deploymentId:req.body.deploymentId},req.body,{upsert:true},function (err, onlineOrderSettingss) {
    console.log(err)
    if(err) { return handleError(res, err); }
    return res.status(200).json(onlineOrderSettingss);
  });
};

exports.getSettingsByTenant = function(req, res) {
  console.log(req.body);
  OnlineOrderSettings.find({tenant_id:req.query.tenant_id},function (err, onlineOrderSettingss) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(onlineOrderSettingss);
  });
};

// Updates an existing onlineOrderSettings in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  OnlineOrderSettings.findById(req.params.id, function (err, onlineOrderSettings) {
    if (err) { return handleError(res, err); }
    if(!onlineOrderSettings) { return res.status(404).send('Not Found'); }
    var updated = _.merge(onlineOrderSettings, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(onlineOrderSettings);
    });
  });
};

// Deletes a onlineOrderSettings from the DB.
exports.destroy = function(req, res) {
  OnlineOrderSettings.findById(req.params.id, function (err, onlineOrderSettings) {
    if(err) { return handleError(res, err); }
    if(!onlineOrderSettings) { return res.status(404).send('Not Found'); }
    onlineOrderSettings.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}