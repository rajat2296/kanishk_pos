'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OnlineOrderSettingsSchema = new Schema({
  deploymentId: String,
  cashierMobile: String,
  ownerMobile: String,
  delivery_from_time:Date,
  delivery_to_time:Date,
  deliveryLocations : [],
  tenant_id : String
});

module.exports = mongoose.model('OnlineOrderSettings', OnlineOrderSettingsSchema);