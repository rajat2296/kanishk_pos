/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var OnlineOrderSettings = require('./onlineOrderSettings.model');

exports.register = function(socket) {
  OnlineOrderSettings.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  OnlineOrderSettings.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('onlineOrderSettings:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('onlineOrderSettings:remove', doc);
}