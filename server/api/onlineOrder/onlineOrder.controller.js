
var _ = require('lodash');
var OnlineOrder = require('./onlineOrder.model');
var OnlineOrderActivation = require('./onlineOrderActivation.model');
var User=require('../user/user.model');
var FavouriteOrder=require('../favouriteOrder/favouriteOrder.model');
var Bill=require('../bill/bill.model');
var Tenant=require('../tenant/tenant.model');
var Tab=require('../tab/tab.model');
var Item=require('../item/item.model');
var DeleteItem=require('../item/deleteItem.model');
var Deployment=require('../deployment/deployment.model');
var MobileAuth=require('../mobileauth/mobileauth.model');
var Customer=require('../customer/customer.model');
var EmailOrder=require('./emailOrder.model');
var async = require('async');
var request=require('request');
var utils=require('../Utils/utils');
var mongoose=require('mongoose');
var moment=require('moment');
var Q=require('q');
var socket=null;
var htmlparser=require("htmlparser2");
var config = require('../../config/environment');
var sha512 = require('js-sha512');
//Nitin code
var moment = require( 'moment' );
var onlineOrderSettings=require('../onlineOrderSettings/onlineOrderSettings.model');
var payUMobile=require('./payUMobile.model')
exports.registerOnline=function(Socket){
  //console.log(Socket);
  socket=Socket;
}

// Get deployment by tenant
exports.getDeployment=function(req,res){
  console.log(req);

  Deployment.find({tenant_id:req.query.tenantId,isMaster:false},{name:1,city:1,state:1,'settings.name':1,'settings.value':1,'settings.selected':1},function(err,deploymnet){
    if(err){return handleError(res,err)}
    // console.log(deploymnet);
    return res.json(200,deploymnet);
  });
};
//
exports.getItemByTab=function(req,res)
{
  if(config.isQueue)
  {
  request(config.reportServer.url+'/api/onlineOrders/getItemByTab?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
  }
  else{

    Tab.find({'tabType':req.query.tabtype,deployment_id:req.query.deploymentId},{categories:1},{limit:1},function(err,tab){
    if(err){return handleError(res,err);}
    var categoryIds=[];

    if(tab.length>0) {
      _.forEach(tab[0].categories, function (cat) {categoryIds.push(cat._id);});
    }else{
      return res.json(200,[]);
    }

    utils.getTopItems({_created: {$gte: moment( new Date( new Date().setDate(new Date().getDate()-30))).toDate(), $lte: moment(new Date()).toDate()},
      deployment_id:new mongoose.Types.ObjectId( req.query.deploymentId)}).then(function(topItems){
      //console.log(topItems);
      //Item.find({deployment_id:req.query.deploymentId,"category._id":{$in:categoryIds}},{name:1,description:1,itemNumber:1,category:1,rate:1,tabs:1,"mapComboItems._id":1, "mapItems._id":1
        Item.find({deployment_id:req.query.deploymentId,"category._id":{$in:categoryIds}},{name:1,description:1,itemNumber:1,category:1,rate:1,tabs:1,mapComboItems:1, mapItems:1}
        ,function(erri,items){
        if(erri){return handleError(res,erri)};
        var nItems=[];
        items.forEach(function(item){
         //var itemOb={_id:item._id,description:item.description,name:item.name,itemNumber:item.itemNumber,category:{_id: item.category._id,name:item.category.categoryName, isAddOn:item.category.isAddons, isCombo:item.category.isCombos, isLiquor:item.category.isliquor},rate:item.rate, mapAddonItems: item.mapItems, mapComboItems: item.mapComboItems, taxes:null,count:0};
          var itemOb={_id:item._id,description:item.description,name:item.name,itemNumber:item.itemNumber,category:{_id: item.category._id,name:item.category.categoryName},rate:item.rate,taxes:null,count:0,mapComboItems:item.mapComboItems,mapItems:item.mapItems};
          //console.log(item.tabs);
          var _filterTab= _.filter(item.tabs,{'_id':tab[0]._id.toString()});
          if(_filterTab.length>0){
            if(_.has(_filterTab[0],'taxes')){
              itemOb.taxes=_filterTab[0].taxes;
            }
            if(_.has(_filterTab[0],'item')){
              if(_.has(_filterTab[0].item,'rate')){
                if(_filterTab[0].item.rate!=null){
                  itemOb.rate=_filterTab[0].item.rate;
                }
              }
            }
            var _f_topItem= _.filter(topItems,{itemName:item.name});
            if(_f_topItem.length>0){
              itemOb.count=  _f_topItem[0].qty;
            }
            //console.log(_filterTab);
          }
          nItems.push(itemOb);
        })
        return res.json(200,nItems);
      })

    })
    //console.log(categoryIds);
  })
}
}

exports.getItemByTabPartner=function(req,res){
  console.log(req.query);

 /* var job = queue.create('getItemByTabPartner', req.query);
  job.on('complete', function(result){
    console.log('Job completed with data ', result);
  });
*/
  if(req.query.apikey!="557c3f5334e397b40fe90146"){
    return res.json(404, {status: 'error', message: 'not authenticated with this key .'});
  }
  if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/getItemByTabPartner?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  Tab.find({'tabType':req.query.tabtype,deployment_id:req.query.deployment_id},{categories:1},{limit:1},function(err,tab){
    if(err){return handleError(res,err);}
    var categoryIds=[];
    // console.log(tab);
    if(tab.length>0) {
      _.forEach(tab[0].categories, function (cat) {categoryIds.push(cat._id);});
    }

//    Item.find({deployment_id:req.query.deployment_id,"category._id":{$in:categoryIds}},{name:1,description:1,itemNumber:1,category:1,rate:1,tabs:1},function(erri,items){
  Item.find(query,{
        name:1,
        description:1,
        itemNumber:1,
        category:1,
        rate:1,
        tabs:1, 
        "mapComboItems._id":1,
        "mapComboItems.name":1,
        "mapComboItems.rate":1,
        "mapComboItems.category.categoryName":1,
        "mapComboItems.category.superCategory.superCategoryName":1,
        "mapItems._id":1,
        "mapItems.name":1,
        "mapItems.rate":1,
        "mapItems.category.categoryName":1,
        "mapItems.category.superCategory.superCategoryName":1
      },function(erri,items){
      if(erri){return handleError(res,erri)};
      var nItems=[];
      items.forEach(function(item){
       // var itemOb={_id:item._id,description:item.description,name:item.name,itemNumber:item.itemNumber,category:{_id: item.category._id,name:item.category.categoryName},rate:item.rate,taxes:null,count:0};
        var itemOb={_id:item._id,description:item.description,name:item.name,itemNumber:item.itemNumber,category:{_id: item.category._id,name:item.category.categoryName, isAddOn:item.category.isAddons, isCombo:item.category.isCombos, isLiquor:item.category.isliquor},rate:item.rate, mapAddonItems: item.mapItems, mapComboItems: item.mapComboItems, taxes:null,count:0};
        var _filterTab= _.filter(item.tabs,{tabType:req.query.tabtype});
        if(_filterTab.length>0){
          if(_.has(_filterTab[0],'taxes')){
            itemOb.taxes=_filterTab[0].taxes;
          }
          if(_.has(_filterTab[0],'item')){
            if(_.has(_filterTab[0].item,'rate')){
              if(_filterTab[0].item.rate!=null){
                itemOb.rate=_filterTab[0].item.rate;
              }
            }
          }
        }
        nItems.push(itemOb);
      })
      return res.json(200,nItems);
    })
  })
 }
}
//getting addresses of customer for online bills
exports.getCustomersByTenant=function(req,res) {
  //isCustomerApproved(req).then(function(){
    if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/getCustomersByTenant?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  utils.getDeploymentsByTenant(req.query.tenant_id).then(function (deps) {
    var depsString = [];
    _.forEach(deps, function (dep) {
      depsString.push(dep._id);
    })
    console.log(depsString);
    Customer.find({deployment_id: {$in: depsString},$or:[ {mobile:req.query.mobile},{phone:req.query.mobile}] }, function (err, customers) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200,customers);
    })
  });
  //},function(err){
  //  return res.json(404,{error:'Customer not authenticated'});
  //});
 }
}
//getting all the online orders page wise

exports.getOnlineOrderPageWise=function(req,res){
  if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/getOnlineOrderPageWise?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  isCustomerApproved(req).then(function(){

    var page=parseInt(req.query.pageSize)*parseInt(req.query.page);
    console.log(page);
    OnlineOrder.find({tenant_id:req.query.tenant_id,"customer.mobile": req.query.mobile},{},{sort:{created:-1},limit:req.query.pageSize,skip:page},function(err,onlineOrders){
      if (err) {
        return handleError(res, err);
      }
      return res.json(200,onlineOrders);
    })

  },function(err){
    return res.json(404,{error:'Customer not authenticated'});
  });
 }
}

exports.getOnlineOrderPageWiseccp = function(req, res) {
    if (config.isQueue) {
        request(config.reportServer.url + '/api/onlineOrders/getOnlineOrderPageWise?' + utils.createGetUrl(req.query), function(error, response, body) {
            if (error) {
                console.log(error);
                res.json(404, { error: 'eeror in data' })
            } else {
                // console.log(body);
                return res.json(200, JSON.parse(body));
            }
        })
    } else {

        var page = parseInt(req.query.pageSize) * parseInt(req.query.page);
        console.log(page);
        OnlineOrder.find({ tenant_id: req.query.tenant_id, "customer.mobile": req.query.mobile }, {}, { sort: { created: -1 }, limit: req.query.pageSize, skip: page }, function(err, onlineOrders) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, onlineOrders);
        })


    }
}

exports.getOrderPageWiseO=function(req,res){
 if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/getOrderPageWiseO?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  isCustomerApproved(req).then(function(){

    var page=parseInt(req.query.pageSize)*parseInt(req.query.page);
    console.log(page);
    OnlineOrder.find({tenant_id:req.query.tenant_id,"customer.mobile": req.query.mobile},{},{sort:{created:-1},limit:req.query.pageSize,skip:page},function(err,onlineOrders){
      if (err) {
        return handleError(res, err);
      }
      return res.json(200,onlineOrders);
    })

  },function(err){
    return res.json(404,{error:'Customer not authenticated'});
  });
 }
}

exports.getOrderPageWiseO=function(req,res){
  if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/getOrderPageWiseO?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  isCustomerApproved(req).then(function(){

    var page=parseInt(req.query.pageSize)*parseInt(req.query.page);
    console.log(page);
    OnlineOrder.find({tenant_id:req.query.tenant_id,"customer.mobile": req.query.mobile},{},{sort:{created:-1},limit:req.query.pageSize,skip:page},function(err,onlineOrders){
      if (err) {
        return handleError(res, err);
      }
      return res.json(200,onlineOrders);
    })

  },function(err){
    return res.json(404,{error:'Customer not authenticated'});
  });
 }
}

//getting all the online orders page wise

exports.getOpenOrderByDeploymentPageWise=function(req,res){
 if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/getOpenOrderByDeploymentPageWise?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{

  var page=parseInt(req.query.pageSize)*parseInt(req.query.page);
  //console.log(page);
  OnlineOrder.find({deployment_id:req.query.deployment_id,"settleTime": null},{},{sort:{created:-1},limit:req.query.pageSize,skip:page},function(err,onlineOrders){
    if (err) {
      return handleError(res, err);
    }
    return res.json(200,onlineOrders);
  })
 }
}
exports.payUSuccess = function(req, res) {
 // console.log(req);
 res.redirect('localhost:9000/order-placed');
};

exports.payUFailure = function(req, res) {
 res.redirect('localhost:9000/payment-failure');
};

exports.getStatus = function(req, res) {
 var options = { method: 'POST',
   url: req.body.url,
   qs: { form: '2' },
   formData: req.body.data
 };

 request(options, function (error, response, body) {
   if (error) throw new Error(error);

   return res.status(200).json(body);
 });
};
//Total count of orders
exports.getOnlineOrdersCount=function(req,res){
  OnlineOrder.count({tenant_id:req.query.tenant_id,"customer.mobile": req.query.mobile},function(err,onlineOrdersCount){
    if (err){
      return handleError(res, err);
    }
    return res.json(200,{count:onlineOrdersCount});
  })
}
exports.getOnlineOrderActivation = function(req,res) {
  //console.log(req.body.auth_key);
  if(req.body.auth_key!="554b15eb5de647cc01141320"){
    return res.json(404,{error:"not authenticated."})
  }
  OnlineOrderActivation.findOne({auth_id: req.body.auth_id}, function (err, onlineOrderActivation) {
    if (err) {
      return handleError(res, err);
    }
    if(onlineOrderActivation){
      if(onlineOrderActivation.enabled) {
        return res.json(200, {tenant_id: onlineOrderActivation.tenant_id});
      }else{
        return res.json(401,{error:"not enabled"});
      }
    }else{
      return res.json(401,{error:"not found"});
    }
  })
};



//mobile authentication check
exports.checkMobile = function(req, res) {
  // console.log(req.query);
/*if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/checkMobile?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{*/

  MobileAuth.findOne({tenant_id:req.query.tenantId,mobile:req.query.mobile},function (err, mobileAuth) {
    if(err) { return handleError(res, err); }
    if (mobileAuth) {
      utils.getDeploymentsByTenant(req.query.tenantId).then(function(deps){
        var depsString=[];
        _.forEach(deps,function(dep){
          depsString.push(dep._id);
        })
        if(req.query.updateProfile){

          //var updated = _.extend(mobileAuth, req.query);
          MobileAuth.update({tenant_id:req.query.tenantId,mobile:req.query.mobile},{$set:{firstName:req.query.firstName==undefined?"":req.query.firstName,lastName:req.query.lastName==undefined?"":req.query.lastName,email:req.query.email==undefined?"":req.query.email}}, function (err,response) {
            console.log(response);
            if (err) { return handleError(res, err); }
            return res.json(200,{status: response});
          });
        }else {
          //console.log(depsString);
          var text = "Thanks for using our online ordering system. Your system generated password is " + mobileAuth.password + " .Please save it for future use.";
          var url = "http://203.212.70.200/smpp/sendsms?username=posistapi&password=del12345&to=" + req.query.mobile + "&from=POSIST&text=" + text;
          if (req.query.resendPassword == 'true') {
            /* request(url, function (error, response, body) {
             //  console.log(body);
             //return res.json(200, {message:{status:'notfound',body:'mobile number not found.'}});
             });*/
            utils.sendSMSExotel(req.query.mobile, text);
          }
          if (req.query.authenticatePassword == 'true') {
            if (mobileAuth.password == req.query.password) {
              //   Customer.find({deployment_id:{$in:depsString },mobile:req.query.mobile}, function (custerr, customers) {
              Customer.find({
                deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
                mobile: req.query.mobile
              }, function (custerr, customers) {
                if (custerr) {
                  return handleError(res, custerr);
                }
                return res.json(200, {message: {status: 'matched', body: 'password matched.', data: customers}});
              });
              //return res.json(200, {message: {status: 'matched', body: 'password matched.'}});
            }
            else
              return res.json(401, {message: {status: 'notmatched', body: 'password not matched.'}});
          } else {
            //Customer.find({deployment_id:{$in:depsString },mobile:req.query.mobile}, function (custerr, customers) {
            Customer.find({
              deployment_id: new mongoose.Types.ObjectId(req.query.deployment_id),
              mobile: req.query.mobile
            }, function (custerr, customers) {
              if (custerr) {
                return handleError(res, custerr);
              }
              return res.json(200, {message: {status: 'found', body: 'mobile number found.', data: customers}});
            })

          }
        }
        ///////Deployemnts Close////
      });

    } else {
      //isCustomerApproved(req).then(function(){
      var num = Math.floor(Math.random() * 900000) + 100000;
      var text = "Thanks for using our online ordering system. Your system generated password is " + num + " .Please save it for future use.";
      var url = "http://203.212.70.200/smpp/sendsms?username=posistapi&password=del12345&to=" + req.query.mobile + "&from=POSIST&text=" + text;
      MobileAuth.create({
        tenant_id: req.query.tenantId,
        mobile: req.query.mobile,
        deployment_id: req.query.deploymentId,
        password: num,
        lastModified: new Date()
      }, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        /* request(url, function (error, response, body) {
         console.log(body);
         //return res.json(200, {message:{status:'notfound',body:'mobile number not found.'}});
         });*/
        utils.sendSMSExotel(req.query.mobile,text);
        return res.json(200, {message: {status: 'notfound', body: 'mobile number not found.'}});
      })
      /*},function(err){
       return res.json(404,{error:'Customer not authenticated'});
       });*/

    }
  });
// }
};

//Get online User profile
exports.getUserProfile=function(req,res){
  isCustomerApproved(req).then(function(result){
    // console.log(result);
    MobileAuth.find({tenant_id:req.query.tenant_id,mobile:req.query.mobile},function(err,user){
      if(err){return handleError(res,err);}
      delete user.password;
      res.json(200,user);
    })
  },function(err){
    return res.json(404,{error:'Customer not authenticated'});
  })
}
function isCustomerApproved(req){
  var d= Q.defer();
  MobileAuth.findOne({tenant_id:req.query.tenant_id,mobile:req.query.mobile,password:req.query.password},function(err,user){
    if(err){d.reject('not found'); }
    console.log(user);
    if(user){
      d.resolve('found');
    }else{
      d.reject('not found');
    }
  })
  return d.promise;
}
// Get list of onlineOrders
exports.index = function(req, res) {
  console.log(req.query);
  /*OnlineOrder.find(function (err, onlineOrders) {
   if(err) { return handleError(res, err); }
   return res.json(200, onlineOrders);
   });*/
  var arr=[];
  OnlineOrder.update({_id: req.query.id},{closeTime:new Date()},function(err,affected,resp){
    if(err){console.log(err); return handleError(res,err);}
    //  console.log(resp);
    arr.push({re:affected});
    return res.json(200,arr);
  });

  /* OnlineOrder.findById(req.query.id, function (err, onlineOrder) {
   if (err) { return handleError(res, err); }
   if(!onlineOrder) { return res.send(404); }
   var updated = _.merge(onlineOrder, req.query);
   updated.save(function (err) {
   if (err) { return handleError(res, err); }
   return res.json(200, []);
   });
   });
   return res.json(200, []);*/
};
exports.updateOrderModify = function(req, res) {
  // console.log(req.body.id);
  var rBody = req.body;
  if (_.has(rBody, 'isRejected')) {
    OnlineOrder.update({_id: req.body.id}, {isRejected:rBody.isRejected, status: 'rejected'}, function (err, affected, resp) {
      if (err) {
        console.log(err);
        return handleError(res, err);
      }
      socket.broadcast.emit('onlineOrder_update'+req.body.deployment_id, req.body);
      return res.json(200, req.body);
    });
  } else {
    if(_.has(rBody,'status')){
      if(rBody.status=='kot'){
        OnlineOrder.update({_id: req.body.id}, {kotPrintTime: new Date(), status: 'preparing'}, function (err, affected, resp) {
          if (err) {
            console.log(err);
            return handleError(res, err);
          }
          socket.broadcast.emit('onlineOrder_update'+req.body.deployment_id, req.body);
          return res.json(200, req.body);
        });
      }
      if(rBody.status=='bill'){
        OnlineOrder.update({_id: req.body.id}, {billPrintTime: new Date(), status: 'out for delivery'}, function (err, affected, resp) {
          if (err) {
            console.log(err);
            return handleError(res, err);
          }
          socket.broadcast.emit('onlineOrder_update'+req.body.deployment_id, req.body);
          return res.json(200, req.body);
        });
      }
      if(rBody.status=='dispatch'){
        OnlineOrder.update({_id: req.body.id}, {dispatchTime: new Date(), status: 'delivered'}, function (err, affected, resp) {
          if (err) {
            console.log(err);
            return handleError(res, err);
          }
          socket.broadcast.emit('onlineOrder_update'+req.body.deployment_id, req.body);
          return res.json(200, req.body);
        });
      }
      if(rBody.status=='settle'){
        OnlineOrder.update({_id: req.body.id}, {settleTime: new Date(), status: 'payment received'}, function (err, affected, resp) {
          if (err) {
            console.log(err);
            return handleError(res, err);
          }
          socket.broadcast.emit('onlineOrder_update'+req.body.deployment_id, req.body);
          return res.json(200, req.body);
        });
      }
    }else {
      OnlineOrder.update({_id: req.body.id}, {closeTime: new Date()}, function (err, affected, resp) {
        if (err) {
          console.log(err);
          return handleError(res, err);
        }
        socket.broadcast.emit('onlineOrder_update'+req.body.deployment_id, req.body);
        return res.json(200, req.body);
      });
    }
  }
};
exports.createFavourite=function(req,res){
  console.log(req.body);
  saveFavourite(req.body).then(function(status){
    console.log(status);
    if(status=='error')
      return res.json(401,{status:'error'});
    else
      return res.json(200,{status:status});
  })
};

// Get a single onlineOrder
exports.show = function(req, res) {
  OnlineOrder.findById(req.params.id, function (err, onlineOrder) {
    if(err) { return handleError(res, err); }
    if(!onlineOrder) { return res.send(404); }
    return res.json(onlineOrder);
  });
};

// Creates a new onlineOrder in the DB.
exports.create = function(req, res) {
  var order = req.body;
   if(order.tabType=="delivery")
  {
    onlineOrderSettings.findOne({deploymentId:order.deployment_id},function(err,settings)
    {

      if(settings!=null)
      {
        console.log(settings,'settings')
        if(err){return handleError(res,err);}
        var serverdate=moment(new Date()).format("HH:mm");
        var fromtime=moment(new Date(settings.delivery_from_time)).format("HH:mm")
        var totime=moment(new Date(settings.delivery_to_time)).format("HH:mm");
        console.log('serverdate',serverdate);
        console.log('fromtime.getTime()',fromtime,'serverdate.getTime()',serverdate,'totime.getTime()',totime)
        if(fromtime<=serverdate && serverdate<=totime)
        {
          console.log(order);
  /*Validation*/
  if (!_.has(order, 'customer')) {
    return res.json(404, {status: 'error', message: 'Customer entity is required.'});
  } else {
    if (!_.has(order.customer, 'firstname')) {
      return res.json(404, {status: 'error', message: 'Customer firstname entity is required.'});
    }
    if (!_.has(order.customer, 'mobile')) {
      return res.json(404, {status: 'error', message: 'Customer mobile entity is required.'});
    }
  }
  OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
    //console.log("billNumber"+onlinebill);
    var obill=onlinebill;
    if(onlinebill) {
      if(_.has(obill,'billNumber')){order.billNumber =1;}else
      { order.billNumber =  (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);}

    }else{
      order.billNumber = 1;
    }

    Tab.find({
      'tabType': order.tabType,
      deployment_id: order.deployment_id
    }, {categories: 1}, {limit: 1}, function (err, tab) {
      if (err) {
        return handleError(res, err);
      }
      // console.log(tab);
      if (tab) {
        if(tab.length>0) {
          order.tabId = tab[0]._id;
          /* if(order.source.id==1){
           var trigger={acceptUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',rejectUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',autoRejectAt:0}
           order.trigger=trigger;
           }*/

          OnlineOrder.create(order, function (err, onlineOrder) {
            if (err) {
              return handleError(res, err);
            }
            if(order.favouriteName!=undefined){
              saveFavourite(order);
            }
            if(order.customer!=undefined){
              utils.customerCreateWithCheck(order.customer).then(function(status){
                // console.log('customer save'+status);
                 // Nitin Code goes Here
              onlineOrderSettings.findOne({deploymentId:order.deployment_id},function(err,settings)
              {
                console.log(settings,'settings')
                if(settings!=null)
                {
                  if(settings.cashierMobile!=undefined)
                  {
                    console.log(settings.cashierMobile)
                    var text="An order has been placed on your oulet.  Check it out"

                    utils.sendSMSExotel(settings.cashierMobile,text);
                  }
                   if(settings.ownerMobile!=undefined)
                  {
                    console.log(settings.ownerMobile)
                    var text="A order has been placed on your oulet. Check it out"
                    utils.sendSMSExotel(settings.ownerMobile,text);
                  }
                }
              });
               // Nitin Code ends Here
                return res.json(201, onlineOrder);
              },function(err){
                return res.json(200, {status: 'customer', message: 'Error saving in customer. Error:'+err});
              });
            }else{
              //Nitin Code goes Here
              console.log('going for onlineOrderSettings');
              onlineOrderSettings.findOne({deploymentId:req.query.deploymentId},function(err,settings)
              {
                console.log(settings,'settings')
                if(settings!=null)
                {
                  if(settings.cashierMobile!=undefined)
                  {
                    console.log(settings.cashierMobile)
                    var text="A order has been placed on your oulet.  Check it out"
                    utils.sendSMSExotel(settings.cashierMobile,text);
                  }
                   if(settings.ownerMobile!=undefined)
                  {
                    console.log(settings.ownerMobile)
                    var text="A order has been placed on your oulet. Check it out"
                    utils.sendSMSExotel(settings.ownerMobile,text);
                  }
                }
              });
              //Nitin Code ends Here
              return res.json(201, onlineOrder);
            }

          });
        }else{
          return res.json(404, {status: 'error', message: 'Tab not found in database.'});
        }
      } else {
        return res.json(404, {status: 'error', message: 'Tab not found in database.'});
      }
    });
  });
        }
        else
        {
          return res.json(404,{error:'Outlet do not provide delivery service at this time'})
        }
      }
      else
      {
        console.log(order);
  /*Validation*/
  if (!_.has(order, 'customer')) {
    return res.json(404, {status: 'error', message: 'Customer entity is required.'});
  } else {
    if (!_.has(order.customer, 'firstname')) {
      return res.json(404, {status: 'error', message: 'Customer firstname entity is required.'});
    }
    if (!_.has(order.customer, 'mobile')) {
      return res.json(404, {status: 'error', message: 'Customer mobile entity is required.'});
    }
  }
  OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
    //console.log("billNumber"+onlinebill);
    var obill=onlinebill;
    if(onlinebill) {
      if(_.has(obill,'billNumber')){order.billNumber =1;}else
      { order.billNumber =  (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);}

    }else{
      order.billNumber = 1;
    }

    Tab.find({
      'tabType': order.tabType,
      deployment_id: order.deployment_id
    }, {categories: 1}, {limit: 1}, function (err, tab) {
      if (err) {
        return handleError(res, err);
      }
      // console.log(tab);
      if (tab) {
        if(tab.length>0) {
          order.tabId = tab[0]._id;
          /* if(order.source.id==1){
           var trigger={acceptUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',rejectUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',autoRejectAt:0}
           order.trigger=trigger;
           }*/

          OnlineOrder.create(order, function (err, onlineOrder) {
            if (err) {
              return handleError(res, err);
            }
            if(order.favouriteName!=undefined){
              saveFavourite(order);
            }
            if(order.customer!=undefined){
              utils.customerCreateWithCheck(order.customer).then(function(status){
                // console.log('customer save'+status);
                 // Nitin Code goes Here
              onlineOrderSettings.findOne({deploymentId:order.deployment_id},function(err,settings)
              {
                console.log(settings,'settings')
                if(settings!=null)
                {
                  if(settings.cashierMobile!=undefined)
                  {
                    console.log(settings.cashierMobile)
                    var text="A order has been placed on your oulet.  Check it out"
                    utils.sendSMSExotel(settings.cashierMobile,text);
                  }
                   if(settings.ownerMobile!=undefined)
                  {
                    console.log(settings.ownerMobile)
                    var text="A order has been placed on your oulet. Check it out"
                    utils.sendSMSExotel(settings.ownerMobile,text);
                  }
                }
              });
               // Nitin Code ends Here
                return res.json(201, onlineOrder);
              },function(err){
                return res.json(200, {status: 'customer', message: 'Error saving in customer. Error:'+err});
              });
            }else{
              //Nitin Code goes Here
              console.log('going for onlineOrderSettings');
              onlineOrderSettings.findOne({deploymentId:req.query.deploymentId},function(err,settings)
              {
                console.log(settings,'settings')
                if(settings!=null)
                {
                  if(settings.cashierMobile!=undefined)
                  {
                    console.log(settings.cashierMobile)
                    var text="A order has been placed on your oulet.  Check it out"
                    utils.sendSMSExotel(settings.cashierMobile,text);
                  }
                   if(settings.ownerMobile!=undefined)
                  {
                    console.log(settings.ownerMobile)
                    var text="A order has been placed on your oulet. Check it out"
                    utils.sendSMSExotel(settings.ownerMobile,text);
                  }
                }
              });
              //Nitin Code ends Here
              return res.json(201, onlineOrder);
            }

          });
        }else{
          return res.json(404, {status: 'error', message: 'Tab not found in database.'});
        }
      } else {
        return res.json(404, {status: 'error', message: 'Tab not found in database.'});
      }
    });
  });
      }
    })
  }
  else
  {
    console.log(order);
  /*Validation*/
  if (!_.has(order, 'customer')) {
    return res.json(404, {status: 'error', message: 'Customer entity is required.'});
  } else {
    if (!_.has(order.customer, 'firstname')) {
      return res.json(404, {status: 'error', message: 'Customer firstname entity is required.'});
    }
    if (!_.has(order.customer, 'mobile')) {
      return res.json(404, {status: 'error', message: 'Customer mobile entity is required.'});
    }
  }
  OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
    //console.log("billNumber"+onlinebill);
    var obill=onlinebill;
    if(onlinebill) {
      if(_.has(obill,'billNumber')){order.billNumber =1;}else
      { order.billNumber =  (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);}

    }else{
      order.billNumber = 1;
    }

    Tab.find({
      'tabType': order.tabType,
      deployment_id: order.deployment_id
    }, {categories: 1}, {limit: 1}, function (err, tab) {
      if (err) {
        return handleError(res, err);
      }
      // console.log(tab);
      if (tab) {
        if(tab.length>0) {
          order.tabId = tab[0]._id;
          /* if(order.source.id==1){
           var trigger={acceptUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',rejectUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',autoRejectAt:0}
           order.trigger=trigger;
           }*/

          OnlineOrder.create(order, function (err, onlineOrder) {
            if (err) {
              return handleError(res, err);
            }
            if(order.favouriteName!=undefined){
              saveFavourite(order);
            }
            if(order.customer!=undefined){
              utils.customerCreateWithCheck(order.customer).then(function(status){
                // console.log('customer save'+status);
                 // Nitin Code goes Here
              onlineOrderSettings.findOne({deploymentId:order.deployment_id},function(err,settings)
              {
                console.log(settings,'settings')
                if(settings!=null)
                {
                  if(settings.cashierMobile!=undefined)
                  {
                    console.log(settings.cashierMobile)
                    var text="A order has been placed on your oulet.  Check it out"
                    utils.sendSMSExotel(settings.cashierMobile,text);
                  }
                   if(settings.ownerMobile!=undefined)
                  {
                    console.log(settings.ownerMobile)
                    var text="A order has been placed on your oulet. Check it out"
                    utils.sendSMSExotel(settings.ownerMobile,text);
                  }
                }
              });
               // Nitin Code ends Here
                return res.json(201, onlineOrder);
              },function(err){
                return res.json(200, {status: 'customer', message: 'Error saving in customer. Error:'+err});
              });
            }else{
              //Nitin Code goes Here
              console.log('going for onlineOrderSettings');
              onlineOrderSettings.findOne({deploymentId:req.query.deploymentId},function(err,settings)
              {
                console.log(settings,'settings')
                if(settings!=null)
                {
                  if(settings.cashierMobile!=undefined)
                  {
                    console.log(settings.cashierMobile)
                    var text="A order has been placed on your oulet.  Check it out"
                    utils.sendSMSExotel(settings.cashierMobile,text);
                  }
                   if(settings.ownerMobile!=undefined)
                  {
                    console.log(settings.ownerMobile)
                    var text="A order has been placed on your oulet. Check it out"
                    utils.sendSMSExotel(settings.ownerMobile,text);
                  }
                }
              });
              //Nitin Code ends Here
              return res.json(201, onlineOrder);
            }

          });
        }else{
          return res.json(404, {status: 'error', message: 'Tab not found in database.'});
        }
      } else {
        return res.json(404, {status: 'error', message: 'Tab not found in database.'});
      }
    });
  });
  }
  
};

exports.createdOnlineOrderActivation=function(req,res) {
  if (req.body.auth_key != "554b15eb5de647cc01141320") {
    return res.json(404, {error: "not authenticated."})
  }
  req.body.auth_id = new mongoose.Types.ObjectId();
  OnlineOrderActivation.findOne({"tenant_id": req.body.tenant_id}, function (err1, onlineOrderAct) {
    if (err1) {
      return handleError(res, err1);
    }
    if(onlineOrderAct){
      return res.json(501, {error:"tenant already found."});
    }
    OnlineOrderActivation.create(req.body, function (err, onlineOrderActivation) {
      if (err) {
        return handleError(res, err);
      }
      if (onlineOrderActivation) {
        return res.json(201, onlineOrderActivation);
      }
    })
  })
};

exports.setOnlineOrderPartner = function(req, res) {
  if(req.body.apikey !="557c3f5334e397b40fe90146"){
    return res.json(404, {status: 'error', message: 'not authenticated with this key .'});
  }
  var order = req.body;
  /*Validation*/
  if (!_.has(order, 'customer')) {
    return res.json(404, {status: 'error', message: 'Customer entity is required.'});
  } else {
    if (!_.has(order.customer, 'firstname')) {
      return res.json(404, {status: 'error', message: 'Customer firstname entity is required.'});
    }
    if (!_.has(order.customer, 'mobile')) {
      return res.json(404, {status: 'error', message: 'Customer mobile entity is required.'});
    }
  }
  OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
    //console.log("billNumber"+onlinebill);
    var obill=onlinebill;
    if(onlinebill) {
      if(_.has(obill,'billNumber')){order.billNumber =1;}else
      { order.billNumber =  (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);}

    }else{
      order.billNumber = 1;
    }
   // var tab;
    if(order.tabName){
      Tab.find({
        'tabName': order.tabName,
        deployment_id: order.deployment_id
      }, {categories: 1}, {limit: 1}, function (err, tab) {
        if (err) {
          return handleError(res, err);
        }
        console.log(tab);
        if (tab && tab.length>0) {
          order.tabId = tab[0]._id;
          //if(order.source.id==1){
          var trigger={modifyUrl:order.modifyUrl,acceptUrl:order.acceptUrl,rejectUrl:order.rejectUrl,billPrintUrl:order.billPrintUrl,dispatchUrl:order.dispatchUrl,settleUrl:order.settleUrl,autoRejectAt:0}
          order.trigger=trigger;
          //}

          OnlineOrder.create(order, function (err, onlineOrder) {
            if (err) {
              return handleError(res, err);
            }
            return res.json(201, onlineOrder);
          });
        }
        else {
          Tab.find({
            'tabType': order.tabType,
            deployment_id: order.deployment_id
          }, {categories: 1}, {limit: 1}, function (err, tab1) {
            if (err) {
              return handleError(res, err);
            }
            console.log(tab1);
            if (tab1 && tab1.length>0) {
              order.tabId = tab1[0]._id;
              //if(order.source.id==1){
              var trigger={modifyUrl:order.modifyUrl,acceptUrl:order.acceptUrl,rejectUrl:order.rejectUrl,billPrintUrl:order.billPrintUrl,dispatchUrl:order.dispatchUrl,settleUrl:order.settleUrl,autoRejectAt:0}
              order.trigger=trigger;
              //}

              OnlineOrder.create(order, function (err, onlineOrder) {
                if (err) {
                  return handleError(res, err);
                }
                return res.json(201, onlineOrder);
              });
            } else {
              return res.json(404, {status: 'error', message: 'Tab not found in database.'});
            }
          });
        }
      }); 
    }
    else{
      Tab.find({
        'tabType': order.tabType,
        deployment_id: order.deployment_id
      }, {categories: 1}, {limit: 1}, function (err, tab) {
        if (err) {
          return handleError(res, err);
        }
        console.log(tab);
        if (tab && tab.length>0) {
          order.tabId = tab[0]._id;
          //if(order.source.id==1){
          var trigger={modifyUrl:order.modifyUrl,acceptUrl:order.acceptUrl,rejectUrl:order.rejectUrl,billPrintUrl:order.billPrintUrl,dispatchUrl:order.dispatchUrl,settleUrl:order.settleUrl,autoRejectAt:0}
          order.trigger=trigger;
          //}

          OnlineOrder.create(order, function (err, onlineOrder) {
            if (err) {
              return handleError(res, err);
            }
            return res.json(201, onlineOrder);
          });
        } else {
          return res.json(404, {status: 'error', message: 'Tab not found in database.'});
        }
      });
    }

    // Tab.find({
    //   'tabType': order.tabType,
    //   deployment_id: order.deployment_id
    // }, {categories: 1}, {limit: 1}, function (err, tab) {
    //   if (err) {
    //     return handleError(res, err);
    //   }
    // console.log(tab);
    // if (tab) {
    //   if(tab.length>0) {
    //     order.tabId = tab[0]._id;
    //     //if(order.source.id==1){
    //     var trigger={modifyUrl:order.modifyUrl,acceptUrl:order.acceptUrl,rejectUrl:order.rejectUrl,billPrintUrl:order.billPrintUrl,dispatchUrl:order.dispatchUrl,settleUrl:order.settleUrl,autoRejectAt:0}
    //     order.trigger=trigger;
    //     //}

    //     OnlineOrder.create(order, function (err, onlineOrder) {
    //       if (err) {
    //         return handleError(res, err);
    //       }
    //       return res.json(201, onlineOrder);
    //     });
    //   }else{
    //     return res.json(404, {status: 'error', message: 'Tab not found in database.'});
    //   }
    // } else {
    //   return res.json(404, {status: 'error', message: 'Tab not found in database.'});
    // }
    //});

  });
};
function saveFavourite(order) {
  var d= Q.defer();
  console.log(order);
  //console.log( order.customer.mobile);
  FavouriteOrder.find({
    tenant_id: order.tenant_id,
    "customer.mobile": order.customer.mobile,
    favouriteName:order.favouriteName
  }, {favouriteName: 1}, function (err, fOrders) {
    //console.log(fOrders);
    if (fOrders) {
      if (fOrders.length == 0) {
        FavouriteOrder.create(order, function (err, onlineOrder) {
          if(err){d.resolve('error');}
          d.resolve('successfull');
        });
      }else{
        d.resolve('error');
      }
    }else{
      d.resolve('error');
    }
  })
  return d.promise;
}

exports.executeUrl=function(req,res){
  console.log("executeurl",req.body);
  
  OnlineOrder.findOne({_id: req.body.id}).lean().exec(function(err, order){
    if(err)
      return handleError(res, err);
    else{
        //for accept and reject
        if(req.body.isRejected==true){
          order.reason = req.body.reason;
        }else if(req.body.deliveryInfo){
          order.deliveryInfo = req.body.deliveryInfo;
        }

        if(req.body.url){
          var options = {
            method : 'POST',
            url : req.body.url,
            json : order
          }
          request(options, function (error, response, body) {
            console.log("successfully",body);
          });
        }
        return res.send(200,{"status":"Executed successfully."});
    }
  });  
}

exports.modifyUrl=function(req,res){
  var url=req.body.url;
  //console.log("modifyUrl",req.body);
  var options = {
    method : 'POST',
    uri : req.body.url,
    json : req.body.order
  }
  request(options, function (error, response, body) {
    //console.log(body);
  });
  return res.send(200,{"status":"Executed successfully."})
}

exports.executeSMS=function(req,res){
  var url=req.query.url;
  console.log(req.query);
  request.post("https://posist:ad3024355ae9bdc0024bf34bbe7e35c393bb3c58@twilix.exotel.in/v1/Accounts/posist/Sms/send",
    // {form:   req.query.body}
    {form:{"From":"08010133399","To":"9891077809","Body":"Hi Ranjeet, your number 989888 is now turned on."}
    }, function(error, response, body){
      console.log(body);
    });
  return res.send(200,{"status":"Executed successfully."})
}
// Updates an existing onlineOrder in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  OnlineOrder.findById(req.params.id, function (err, onlineOrder) {
    if (err) { return handleError(res, err); }
    if(!onlineOrder) { return res.send(404); }
    var updated = _.merge(onlineOrder, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, onlineOrder);
    });
  });
};

exports.checkEmailOrder=function(req,res){
  //console.log(req.body);
  
   var subject=req.body.subject;
    var subjectArr=subject.split('at');
    var brand='';
    // console.log(subjectArr);
    if(subjectArr.length>0){
      brand=subject.replace(subjectArr[0] +'at','');

    }
    brand=brand.trim();
    console.log(brand);
  utils.mailParser(req.body.vendor, req.body.html,req.body.brand).then(function (result) {
      console.log(result);
      res.json(200,result);
    })
}

exports.getEmailOrder=function(req,res){
  //EmailOrder.find({_id:new mongoose.Types.ObjectId('5770ff1224e2dd1d22ff8047')},{},{},function(err,email) {
   //console.log(req.body.maildata);
   // utils.mailParser('swiggy', req.body.maildata).then(function (result) {
   //  });
//});
//return false;
  // console.log(req.body);
  //var emailBody=req.body.mailinMsg;
  //var emailBodyJSON=JSON.parse(emailBody);

  EmailOrder.create({orderEmail:req.body.maildata}, function (err, emailOrder) {
    // console.log(emailOrder);
    /* if (err) {
     return handleError(res, err);
     }*/
    //EmailOrder.find({_id:new mongoose.Types.ObjectId('5770ff1224e2dd1d22ff8047')},{},{},function(err,email) {
    //console.log(email);
    // var emailJSON=JSON.parse(email);

    // utils.mailParser('Swiggy', emailOrder.orderEmail.html).then(function (result) {
    //  console.log(email[0].orderEmail.subject);
    // var emailOrder=email[0];
    var orderFrom='';
    var forwardedEmail=emailOrder.orderEmail.headers["x-forwarded-for"] ;
    if(forwardedEmail==undefined){
      var email=emailOrder.orderEmail.headers.to;
      if(email=='orderswiggy@gmail.com'|| email=='mail@blueshiftit.com'){
        orderFrom='swiggy';
      }
      if(email=='orderzomato@gmail.com'|| email=='zomato@blueshiftit.com'){
        orderFrom='zomato';
      }
      if(email=='orderfpanda@gmail.com'|| email=='foodpanda@blueshiftit.com'){
        orderFrom='foodpanda';
      }
    }else {
      _.forEach(forwardedEmail.split(' '), function (email) {
        console.log(email);
        if (email == 'orderswiggy@gmail.com' || email == 'mail@blueshiftit.com') {
          orderFrom = 'swiggy';
        }
        if (email == 'orderzomato@gmail.com' || email == 'zomato@blueshiftit.com') {
          orderFrom = 'zomato';
        }
        if (email == 'orderfpanda@gmail.com' || email == 'foodpanda@blueshiftit.com') {
          orderFrom = 'foodpanda';
        }
      })
    }
    /////////testing/////////////
    //orderFrom='foodpanda';
    ///////////////////
    var subject=emailOrder.orderEmail.subject;
    var subjectArr=subject.split('at');
    var brand='';
    // console.log(subjectArr);
   if(subjectArr.length>0){
      brand=subject.replace(subjectArr[0] +'at','');
    }
    brand=brand.trim();
    var vendor=orderFrom;
    // vendor='zomato';
    utils.mailParser(vendor, emailOrder.orderEmail.html,brand).then(function (result) {
      console.log(result);
      if(result.isCancelled){
        return res.json(200,{message:'Cancelled Order'})
      }
      var orderedItems=[];

      var brandName= (vendor=='swiggy'?result.brand.toUpperCase():(vendor=='foodpanda'?result.brand: brand));

      if(brandName.trim() ==''||brandName==undefined ||brandName==null){
        return res.json(200, {message: 'brandname not found'})
      }
//      Deployment.findOne({'mapDeployment.swiggy': result.brand.toUpperCase()}, {name: 1}, function (err, deployment) {

      /*Order Start For Swiggy*/
      console.log(brandName);
      if(vendor=='swiggy') {
        Deployment.findOne({'mapDeployment.swiggy': brandName}, {name: 1}, function (err, deployment) {
          console.log(deployment);

          if (err) {
            return handleError(res, err);
          }
          if (!deployment) {
            return res.json(200, {message: 'deployment not found'})
          }

          _.forEach(result.items, function (item) {
            orderedItems.push(item.itemName);
          })
          // Item.find({deployment_id:new mongoose.Types.ObjectId('555baa76034de05173e9fe3b'),'name': {$in: orderedItems}}, {name:1}, function (err, items) {
          Item.find({
            deployment_id: deployment._id,
            $or: [{'name': {$in: orderedItems}}, {'mapOnlineEmailOrder.swiggy': {$in: orderedItems}}]
          }, {name: 1, 'mapOnlineEmailOrder.swiggy': 1}, function (err, items) {
            // console.log(items);
            var copiedItems = [];
            _.forEach(items, function (item) {
              var tempItemCopy = {name: item.name, _id: item._id};
              if (item.mapOnlineEmailOrder != undefined) {
                if (!((item.mapOnlineEmailOrder.swiggy == undefined) || (item.mapOnlineEmailOrder.swiggy == null) || (item.mapOnlineEmailOrder.swiggy == ''))) {
                  tempItemCopy.name = item.mapOnlineEmailOrder.swiggy;
                }
              }
              copiedItems.push(tempItemCopy);
            })
            //console.log(copiedItems);
            var tempItems = [];
            _.forEach(result.items, function (item) {
              var _f = _.filter(copiedItems, {name: item.itemName});
              if (_f.length > 0) {
                tempItems.push({id: _f[0]._id, name: item.itemName, quantity: item.quantity});
              } else {
                tempItems.push({id: null, name: item.itemName, quantity: item.quantity});
              }
            })
            var order = {
              deployment_id: deployment._id,
              tabType: 'delivery',
              items: tempItems,
              customer: {firstname: '', mobile: '', address1: '', address2: '', addType: 'residence'},
              payments: {type: result.paymentType},
              source: {name: 'Swiggy', id: 10}
            }
            ///////////////////////////////////////////////////////Order Push////////////////////////////////////
            OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
              //console.log("billNumber"+onlinebill);
              var obill = onlinebill;
              if (onlinebill) {
                if (_.has(obill, 'billNumber')) {
                  order.billNumber = 1;
                } else {
                  order.billNumber = (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);
                }

              } else {
                order.billNumber = 1;
              }

              Tab.find({
                'tabType': order.tabType,
                deployment_id: order.deployment_id
              }, {categories: 1}, {limit: 1}, function (err, tab) {
                if (err) {
                  return handleError(res, err);
                }
                // console.log(tab);
                if (tab) {
                  if (tab.length > 0) {
                    order.tabId = tab[0]._id;
                    /* if(order.source.id==1){
                     var trigger={acceptUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',rejectUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',autoRejectAt:0}
                     order.trigger=trigger;
                     }*/

                    OnlineOrder.create(order, function (err1, onlineOrder) {
                      if (err1) {
                        return handleError(res, err1);
                      }
                      /*if(order.customer!=undefined){
                       utils.customerCreateWithCheck(order.customer).then(function(status){
                       console.log('customer save'+status);
                       return res.json(201, onlineOrder);
                       },function(err){
                       return res.json(200, {status: 'customer', message: 'Error saving in customer. Error:'+err});
                       });
                       }else{
                       return res.json(201, onlineOrder);
                       }*/
                      return res.json(201, onlineOrder);
                    });
                  }

                }
              });
            });

            //////////////////////////////////////////////////////Order Push End////////////////////////////////
          });
        });
      }
      /*Order end For Swiggy*/
      /*Order Start For Zomato*/
      if(vendor=='zomato') {
        Deployment.findOne({'mapDeployment.zomato': brandName}, {name: 1}, function (err, deployment) {
          console.log(deployment);

          if (err) {
            return handleError(res, err);
          }
          if (!deployment) {
            return res.json(200, {message: 'deployment not found'})
          }

          _.forEach(result.items, function (item) {
            orderedItems.push(item.itemName);
          })
          // Item.find({deployment_id:new mongoose.Types.ObjectId('555baa76034de05173e9fe3b'),'name': {$in: orderedItems}}, {name:1}, function (err, items) {
          Item.find({
            deployment_id: deployment._id,
            $or: [{'name': {$in: orderedItems}}, {'mapOnlineEmailOrder.zomato': {$in: orderedItems}}]
          }, {name: 1, 'mapOnlineEmailOrder.zomato': 1}, function (err, items) {
            // console.log(items);
            var copiedItems = [];
            _.forEach(items, function (item) {
              var tempItemCopy = {name: item.name, _id: item._id};
              if (item.mapOnlineEmailOrder != undefined) {
                if (!((item.mapOnlineEmailOrder.zomato == undefined) || (item.mapOnlineEmailOrder.zomato == null) || (item.mapOnlineEmailOrder.zomato == ''))) {
                  tempItemCopy.name = item.mapOnlineEmailOrder.zomato;
                }
              }
              copiedItems.push(tempItemCopy);
            })
            //console.log(copiedItems);
            var tempItems = [];
            _.forEach(result.items, function (item) {
              var _f = _.filter(copiedItems, {name: item.itemName});
              if (_f.length > 0) {
                tempItems.push({id: _f[0]._id, name: item.itemName, quantity: item.quantity});
              } else {
                tempItems.push({id: null, name: item.itemName, quantity: item.quantity});
              }
            })
            var order = {
              deployment_id: deployment._id,
              tabType: 'delivery',
              items: tempItems,
              customer:result.crm, //{firstname: '', mobile: '', address1: '', address2: '', addType: 'residence'},
              payments: {type: result.paymentType},
              source: {name: 'Zomato', id: 11}
            }
            ///////////////////////////////////////////////////////Order Push////////////////////////////////////
            OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
              //console.log("billNumber"+onlinebill);
              var obill = onlinebill;
              if (onlinebill) {
                if (_.has(obill, 'billNumber')) {
                  order.billNumber = 1;
                } else {
                  order.billNumber = (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);
                }

              } else {
                order.billNumber = 1;
              }

              Tab.find({
                'tabType': order.tabType,
                deployment_id: order.deployment_id
              }, {categories: 1}, {limit: 1}, function (err, tab) {
                if (err) {
                  return handleError(res, err);
                }
                // console.log(tab);
                if (tab) {
                  if (tab.length > 0) {
                    order.tabId = tab[0]._id;
                    /* if(order.source.id==1){
                     var trigger={acceptUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',rejectUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',autoRejectAt:0}
                     order.trigger=trigger;
                     }*/

                    OnlineOrder.create(order, function (err1, onlineOrder) {
                      if (err1) {
                        return handleError(res, err1);
                      }
                      /*if(order.customer!=undefined){
                       utils.customerCreateWithCheck(order.customer).then(function(status){
                       console.log('customer save'+status);
                       return res.json(201, onlineOrder);
                       },function(err){
                       return res.json(200, {status: 'customer', message: 'Error saving in customer. Error:'+err});
                       });
                       }else{
                       return res.json(201, onlineOrder);
                       }*/
                      return res.json(201, onlineOrder);
                    });
                  }
                }
              });
            });

            //////////////////////////////////////////////////////Order Push End////////////////////////////////
          });
        });
      }
      /*Order End For Zomato*/
      /*Order Start For Foodpanda*/
      if(vendor=='foodpanda') {
        console.log(brandName);
        Deployment.findOne({'mapDeployment.foodpanda': result.brand}, {name: 1}, function (err, deployment) {
          console.log(deployment);

          if (err) {
            return handleError(res, err);
          }
          if (!deployment) {
            return res.json(200, {message: 'deployment not found'})
          }

          _.forEach(result.items, function (item) {
            orderedItems.push(item.itemName);
          })
          // Item.find({deployment_id:new mongoose.Types.ObjectId('555baa76034de05173e9fe3b'),'name': {$in: orderedItems}}, {name:1}, function (err, items) {
          Item.find({
            deployment_id: deployment._id,
            $or: [{'name': {$in: orderedItems}}, {'mapOnlineEmailOrder.foodpanda': {$in: orderedItems}}]
          }, {name: 1, 'mapOnlineEmailOrder.foodpanda': 1}, function (err, items) {
            // console.log(items);
            var copiedItems = [];
            _.forEach(items, function (item) {
              var tempItemCopy = {name: item.name, _id: item._id};
              if (item.mapOnlineEmailOrder != undefined) {
                if (!((item.mapOnlineEmailOrder.foodpanda == undefined) || (item.mapOnlineEmailOrder.foodpanda == null) || (item.mapOnlineEmailOrder.foodpanda == ''))) {
                  tempItemCopy.name = item.mapOnlineEmailOrder.foodpanda;
                }
              }
              copiedItems.push(tempItemCopy);
            })
            //console.log(copiedItems);
            var tempItems = [];
            _.forEach(result.items, function (item) {
              var _f = _.filter(copiedItems, {name: item.itemName});
              if (_f.length > 0) {
                tempItems.push({id: _f[0]._id, name: item.itemName, quantity: item.quantity});
              } else {
                tempItems.push({id: null, name: item.itemName, quantity: item.quantity});
              }
            })
            var order = {
              deployment_id: deployment._id,
              tabType: 'delivery',
              items: tempItems,
              customer:result.crm, //{firstname: '', mobile: '', address1: '', address2: '', addType: 'residence'},
              payments: {type: result.paymentType},
              source: {name: 'FoodPanda', id: 12}
            }
            ///////////////////////////////////////////////////////Order Push////////////////////////////////////
            OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
              //console.log("billNumber"+onlinebill);
              var obill = onlinebill;
              if (onlinebill) {
                if (_.has(obill, 'billNumber')) {
                  order.billNumber = 1;
                } else {
                  order.billNumber = (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);
                }

              } else {
                order.billNumber = 1;
              }

              Tab.find({
                'tabType': order.tabType,
                deployment_id: order.deployment_id
              }, {categories: 1}, {limit: 1}, function (err, tab) {
                if (err) {
                  return handleError(res, err);
                }
                // console.log(tab);
                if (tab) {
                  if (tab.length > 0) {
                    order.tabId = tab[0]._id;
                    /* if(order.source.id==1){
                     var trigger={acceptUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',rejectUrl:'http://bercos.posistlocal.net:9000/api/onlineorders/executeurl',autoRejectAt:0}
                     order.trigger=trigger;
                     }*/

                    OnlineOrder.create(order, function (err1, onlineOrder) {
                      if (err1) {
                        return handleError(res, err1);
                      }
                      /*if(order.customer!=undefined){
                       utils.customerCreateWithCheck(order.customer).then(function(status){
                       console.log('customer save'+status);
                       return res.json(201, onlineOrder);
                       },function(err){
                       return res.json(200, {status: 'customer', message: 'Error saving in customer. Error:'+err});
                       });
                       }else{
                       return res.json(201, onlineOrder);
                       }*/
                      return res.json(201, onlineOrder);
                    });
                  }

                }
              });
            });

            //////////////////////////////////////////////////////Order Push End////////////////////////////////
          });
        });
      }
      /*Order End For Foodpanda*/

    });
    //return res.json(201, {"message": "done"});
  });
  //});
}

/*function  getString(rawHtml){
 var string='';
 string= rawHtml.replace(/(\r\n|\n|\r)/gm,"");
 return string.trim();
 }*/
// Deletes a onlineOrder from the DB.
exports.destroy = function(req, res) {
  OnlineOrder.findById(req.params.id, function (err, onlineOrder) {
    if(err) { return handleError(res, err); }
    if(!onlineOrder) { return res.send(404); }
    onlineOrder.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

/**************FOR OPEN API*************************/
/* @author - Shubhank
 @functions - getItemByTabPartner_secured, setOnlineOrderPartner_secured, createFavourite_secured
 */
function handleApiError(res, err){
  return res.status(500).json({code:500, label: "internal_error", message:"Some error occurred in the system."});
}

exports.getItemByTab_secured=function(req,res){
  if(!req.query.tabtype){
    res.status(422).json({status:"error", message:"missing query parameters."});
  }else{
   /* if(config.isQueue){
  request(config.reportServer.url+'/api/onlineOrders/getItemByTab_secured?'+utils.createGetUrl(req.query),function(error,response,body){
    if(error){
      console.log(error);
      res.json(404,{error:'eeror in data'})
    }else{
     // console.log(body);
      return res.json(200, JSON.parse(body));
     }
  })
}else{*/

    Tab.find({'tabType':req.query.tabtype,deployment_id:req.query.deployment_id},{categories:1},{limit:1},function(err,tab){
      if(err){return handleApiError(res,err);}
      var categoryIds=[];
      // console.log(tab);
      if(tab.length>0) {
        _.forEach(tab[0].categories, function (cat) {categoryIds.push(cat._id);});
      }

      if(req.query.lastSyncTime!=undefined || req.query.currentSyncTime!=undefined){
        if(req.query.lastSyncTime!=undefined && req.query.currentSyncTime!=undefined){
          var start_date = new Date(parseInt(req.query.lastSyncTime));
          var end_date = new Date(parseInt(req.query.currentSyncTime));
          if(start_date.getTime()!=NaN && end_date.getTime()!=NaN){
            var query={
              deployment_id: req.query.deployment_id,
              updated: { $gte: start_date,$lt: end_date},
              "category._id":{$in:categoryIds}
            }

          }else
            res.status(422).json({status:"error", message:"invalid query parameters, lastSyncTime and currentSyncTime should be a valid date."});  
        }else
          res.status(422).json({status:"error", message:"missing query parameters - Both lastSyncTime and currentSyncTime should be present."});
      }else{
        var query={
          deployment_id: req.query.deployment_id,
          "category._id":{$in:categoryIds}
        }
      }

      async.series([
          function(callback){
            Item.find(query,{
              name:1,
              description:1,
              itemNumber:1,
              category:1,
              rate:1,
              tabs:1, 
              "mapComboItems._id":1,
              "mapComboItems.name":1,
              "mapItems._id":1,
              "mapItems.name":1,
              "mapItems.category":1,
              "mapItems.category._id":1,
              "mapItems.category.categoryName":1
            },function(erri,items){
              if(erri)
                callback(erri);
              else
                callback(null, items);
            });
          },
          function(callback){
            if(req.query.lastSyncTime&&req.query.currentSyncTime){
              DeleteItem.find(query,{
                name:1,
                description:1,
                itemNumber:1,
                category:1,
                rate:1,
                tabs:1, 
                "mapComboItems._id":1,
                "mapComboItems.name":1,
                "mapItems._id":1,
                "mapItems.name":1,
                "mapItems.category":1,
                "mapItems.category._id":1,
                "mapItems.category.categoryName":1,
                isDeleted:1
              },function(erri,items){
                if(erri)
                  callback(erri);
                else
                  callback(null, items);
              });
            }else
              callback(null, []);
          }
        ], function(erri, item_arr){
          var items = item_arr[0].concat(item_arr[1]);
          if(erri){return handleApiError(res,erri)};
            var nItems=[];
            items.forEach(function(item){
              var itemOb={_id:item._id,description:item.description,name:item.name,itemNumber:item.itemNumber,category:{_id: item.category._id,name:item.category.categoryName,superCategoryName:item.category.superCategory.superCategoryName,superCategoryId:item.category.superCategory._id, isAddOn:item.category.isAddons, isCombo:item.category.isCombos, isLiquor:item.category.isliquor},rate:item.rate, mapAddonItems: item.mapItems, mapComboItems: item.mapComboItems, taxes:null, count:0};
              if(item.isDeleted)
                itemOb.isDeleted = item.isDeleted;
              var _filterTab= _.filter(item.tabs,{tabType:req.query.tabtype});
              if(_filterTab.length>0){
                if(_.has(_filterTab[0],'taxes')){
                  itemOb.taxes=_filterTab[0].taxes.map(function(tax){
                    var tax = _.omit(tax, ['deployment_id', 'tenant_id', 'tabs', 'created']);
                    tax.cascadingTaxes = tax.cascadingTaxes.filter(function(cascade){return cascade.selected==true;}).map(function(cascade){return _.omit(cascade, ['deployment_id', 'tenant_id', 'selected', 'created'])});
                    return tax;
                  });
                }
                if(_.has(_filterTab[0],'item')){
                  if(_.has(_filterTab[0].item,'rate')){
                    if(_filterTab[0].item.rate!=null){
                      itemOb.rate=_filterTab[0].item.rate;
                    }
                  }
                }
              }
              nItems.push(itemOb);
            })
            return res.json(200,nItems);
        });
    });
   }
//  }
};

exports.setOnlineOrder_secured = function(req, res) {
  var order = req.body;
  order.deployment_id = req.query.deployment_id;
  //console.log(req.query);
  //console.log(order);
  /*Validation*/
  var error = 0;
  if(!_.isString(order.tabType)){
    return res.json(422, {status: 'error', message: 'tabType[string] is required in body.'});
  }else if(_.isArray(order.payments) || !_.isObject(order.payments)){
    return res.json(422, {status: 'error', message: 'payments[Object] is required in body.'});
  }else if(!_.has(order.payments, 'type') || !_.isString(order.payments.type)){
    return res.json(422, {status: 'error', message: 'payments type[string] is required in body.'});
  }else if(_.isArray(order.source) || !_.isObject(order.source)){
    return res.json(422, {status: 'error', message: 'source[Object] is required in body.'});
  }else if(!_.has(order.source, 'name') || !_.isString(order.source.name)){
    return res.json(422, {status: 'error', message: 'source name[string] is required in body.'});
  }else if(!_.has(order.source, 'id') || !_.isString(order.source.id)){
    return res.json(422, {status: 'error', message: 'source id[string] is required in body.'});
  }else if(_.isArray(order.customer) || !_.isObject(order.customer)){
    return res.json(422, {status: 'error', message: 'customer[Object] is required in body.'});
  }else if(!_.has(order.customer, 'firstname') || !_.isString(order.customer.firstname)){
    return res.json(422, {status: 'error', message: 'customer firstname[string] is required in body.'});
  }else if(!_.has(order.customer, 'mobile') || !_.isString(order.customer.mobile)){
    return res.json(422, {status: 'error', message: 'customer mobile[string] is required in body.'});
  }else if(!_.isArray(order.items)){
    return res.json(422, {status: 'error', message: 'items[array] is required in body.'});
  }else if(order.items.length==0)
    return res.json(422, {status: 'error', message: 'items[array] is empty in body.'});
  else{
   for(var i=0;i<order.items.length;i++){
        var item = order.items[i];
        if(!_.has(item, 'id') || !_.has(item, 'quantity')){
          res.json(422, {status: 'error', message: 'item id or quantity is missing in items or is invalid.'});
          break;
        }else if(!_.isString(item.id) || !_.isNumber(item.quantity) || item.quantity<=0){
          res.json(422, {status: 'error', message: 'type error item id[string] or quantity[int].'});
          break;
        }
        if(_.has(item, 'discounts')){
          try{
            var invalid_discounts = item.discounts.filter(function(discount){if((discount.type!='percentage'&&discount.type!='fixed')||discount.value<=0) return true; else return false;});
            if(invalid_discounts.length>0){
              res.json(422, {status: 'error', message: 'Item discount is invalid.'});
              break;
            }
          }catch(e){
            res.json(422, {status: 'error', message: 'Item discount is invalid.'});
          }
        }
        if(_.has(item, 'addOns')){
          try{
            var invalid_addons = item.addOns.filter(function(addon){if(addon._id==undefined || addon.quantity<=0) return true; else return false;});
            if(invalid_addons.length>0){
              res.json(422, {status: 'error', message: 'Item addOns are invalid.'});
              break;
            }
          }catch(e){
            res.json(422, {status: 'error', message: 'Item addOns are invalid.'});
          }
        }
      }
     }

    if(i!==order.items.length)
      error=1;

  if(_.has(order, 'discount')){
      if(!order.discount.type || !order.discount.value)
        return res.json(422, {status: 'error', message: 'discount.type or discount.value is missing in body.'});
      else if((order.discount.type!='fixed'&&order.discount.type!='percentage') || order.discount.value<=0)
        return res.json(422, {status: 'error', message: 'discount.type or discount.value is invalid in body.'});
  } 

  if(error==0){
    OnlineOrder.findOne({deployment_id: order.deployment_id}, {billNumber: 1}, {sort: {billNumber: -1}}, function (err, onlinebill) {
      if(err)
        handleApiError(res, err);
      else{
        var obill=onlinebill;
        if(onlinebill) {
          if(_.has(obill,'billNumber')){order.billNumber =1;}else
          { order.billNumber =  (obill.billNumber == null || obill.billNumber == 'undefined') ? 1 : ( obill.billNumber + 1);}

        }else{
          order.billNumber = 1;
        }
        Tab.find({'tabType': order.tabType,deployment_id: order.deployment_id}, {categories: 1}, {limit: 1}, function (err, tab) {
          if (err) {
            return handleApiError(res, err);
          }
          if (tab) {
            if(tab.length>0) {
              order.tabId = tab[0]._id;
              //if(order.source.id==1){
              var trigger={modifyUrl:order.modifyUrl,acceptUrl:order.acceptUrl,rejectUrl:order.rejectUrl,billPrintUrl:order.billPrintUrl,dispatchUrl:order.dispatchUrl,settleUrl:order.settleUrl,autoRejectAt:0}
              order.trigger=trigger;
              //}

              OnlineOrder.create(order, function (err, onlineOrder) {
                if (err) {
                  return handleApiError(res, err);
                }
                if(order.favouriteName!=undefined){
                  FavouriteOrder.findOneAndUpdate({
                    "customer.mobile": order.customer.mobile,
                    favouriteName:order.favouriteName,
                    "wwssource.id": order.source.id
                  }, order, {upsert:true}, function (err, fOrders) {
                   /*Condition applied for zomato*/
                    if(order.source.id == "r1AROcuug")
                      return res.json(200, {status:'success', message:'Order placed.', code:200});
                    else
                      return res.json(201,_.omit(onlineOrder.toObject(),['deployment_id']));
                    //return res.json(201,_.omit(onlineOrder.toObject(),['deployment_id']));
                  });
                }
                /*Condition applied for zomato*/
                    if(order.source.id == "r1AROcuug")
                      return res.json(200, {status:'success', message:'Order placed.', code:200});
                    else
                      return res.json(201,_.omit(onlineOrder.toObject(),['deployment_id']));
                //return res.json(201,_.omit(onlineOrder.toObject(),['deployment_id']));
              });
            }else{
              return res.json(404, {status: 'error', message: 'Tab not found in database.'});
            }
          } else {
            return res.json(404, {status: 'error', message: 'Tab not found in database.'});
          }
        });
      }
    });
  }
};

exports.createFavourite_secured=function(req,res){
  var order = req.body;
  order.deployment_id = req.query.deployment_id;
  order.tenant_id = req.query.tenant_id;

  FavouriteOrder.find({
    tenant_id: order.tenant_id,
    "customer.mobile": order.customer.mobile,
    favouriteName: order.favouriteName
  }, {favouriteName: 1}, function (err, fOrders) {
    if(err){
      handleApiError(res, err);
    }else if(fOrders.length == 0) {
      FavouriteOrder.create(order, function (err, onlineOrder) {
        if(err){
          handleApiError(res, err);
        }else{
          res.status(200).json({status:"success", message: 'Order marked as favourite.'});
          //res.json(onlineOrder);
        }
      });
    }else if(fOrders.length!=0){
      res.status(422).json({status:"error", message: 'Favourite order name already exists.'});
    }
  });
};

exports.getFavourite_secured = function(req, res){
  if(!req.query.mobile){
    res.status(422).json({status:"error", message:"missing query parameters."});
  }else{
    FavouriteOrder.find({"source.id": req.query.source_id,"customer.mobile":req.query.mobile, deployment_id: req.query.deployment_id}, '-deployment_id -tenant_id',function (err, favouriteOrders) {
      console.log(favouriteOrders);
      if(err) { return handleApiError(res, err); }
      return res.json(200,favouriteOrders);
    });
  }
};

exports.getDeployment_secured=function(req,res){
  Deployment.find({tenant_id:req.query.tenant_id,isMaster:false},{name:1,city:1},function(err,deployment){
    if(err){return handleApiError(res,err)}
    // console.log(deploymnet);
    return res.json(200,deployment);
  });
};

exports.getCustomersByTenant_secured=function(req,res) {
  //isCustomerApproved(req).then(function(){
  utils.getDeploymentsByTenant(req.query.tenant_id).then(function (deps) {
    var depsString = [];
    _.forEach(deps, function (dep) {
      depsString.push(dep._id);
    })
    console.log(depsString);
    Customer.find({deployment_id: {$in: depsString},$or:[ {mobile:req.query.mobile},{phone:req.query.mobile}] }, '-deployment_id -tenant_id', function (err, customers) {
      if (err) {
        return handleApiError(res, err);
      }
      return res.json(200,customers);
    })
  });
  //},function(err){
  //  return res.json(404,{error:'Customer not authenticated'});
  //});
};

exports.getOnlineOrderPageWise_secured = function(req,res){
  req.query.pageSize = 10; //only 10 orders per request
  if(!req.query.mobile||!req.query.source_id||!req.query.page){
    res.status(422).json({status:"error", message:"missing query parameters."});
  }else{
    var page=parseInt(req.query.pageSize)*parseInt(req.query.page-1);
    OnlineOrder.find({deployment_id:req.query.deployment_id,"customer.mobile": req.query.mobile, "source.id": req.query.source_id},'-deployment_id -tenant_id',{sort:{created:-1},limit:req.query.pageSize,skip:page},function(err,onlineOrders){
      if (err) {
        return handleApiError(res, err);
      }
      return res.json(200,onlineOrders);
    });
  }
};

exports.getDeploymentTabs_secured = function (req, res) {
  Tab.find({tenant_id:req.query.tenant_id, deployment_id:req.query.deployment_id},'tabType tabName', function (err, tenantTabs) {
    if (err) {
      return handleApiError(res, err);
    }
    return res.json(200, tenantTabs);
  });
};

exports.identifyDeployment_secured = function(req, res){
  Deployment.findOne({_id:req.query.deployment_id,isMaster:false},{name:1,city:1},function(err,deployment){
    if(err){return handleApiError(res,err)}
    return res.json(200,deployment);
  });
};

exports.convertOrderForPartner = function(req, res, next){
  console.log(req.query);
  if(req.query.partner_name == 'Zomato'){
    req.body = convertZomatoOrder(req.body.order);
    next();
  }else
    next();

  function convertZomatoOrder(order){
    var o = {
      "source": {
        "name": "Zomato",
        "id": "r1AROcuug",
        "order_id": order.order_id
      }
    };
    o.tabType = order.order_type=='DELIVERY'?'delivery':'takeout';
    o.payments = order.payment_mode=='CASH'?{type: "COD"}:{type: "ONLINE"};
    if(order.order_additional_charges.length!=0){
      o.charges = order.order_additional_charges.map(function(charge){return {name: charge.charge_name, value: charge.charge_value}})
    }
    if(order.order_discounts.length!=0){
      o.discount = {type: order.order_discounts[0].discount_type.toLowerCase(), value: order.order_discounts[0].discount_value}
    }
    o.customer = {
      firstname: order.customer_details.name,
      mobile: order.customer_details.phone_number,
      email: order.customer_details.email,
      address1: order.customer_details.address,
      address2: order.customer_details.delivery_area||null,
      city: order.customer_details.city,
      state: order.customer_details.state||null,
    }
    o.items = order.order_items.map(function(item){
      var o_item = {
        id: item.item_id.toString(), 
        quantity: item.item_quantity 
      };
      
      if(item.item_discount)
        o_item.discounts = item.item_discount.map(function(discount){
          return {
            type: order.order_discounts[0].discount_type.toLowerCase(), 
            value: order.order_discounts[0].discount_value
          }
        });
      
      if(item.groups)
        o_item.addOns = item.groups.reduce(function(a, b){
          var c = b.items.map(function(item){return {_id: item.item_id.toString(), quantity: item.item_quantity}});
          return a.concat(c);
        }, [])

      return o_item;
    });
    o.rejectUrl = 'http://flipkart.posist.net/api/partners/statusZomato?status=rejected';
    o.acceptUrl = 'http://flipkart.posist.net/api/partners/statusZomato?status=preparing';
    o.billPrintUrl = 'http://flipkart.posist.net/api/partners/statusZomato?status=out_for_delivery';
    o.dispatchUrl = 'http://flipkart.posist.net/api/partners/statusZomato?status=delivered';
    o.settleUrl = 'http://flipkart.posist.net/api/partners/statusZomato?status=delivered';
    return o;
  }
}
/************************************POSIST API********************************************************/
exports.payUSuccess = function(req, res) {
  res.writeHead(302, {
    'Location': 'http://order.posist.co/order-placed/'
  });
  res.end();
  //res.redirect('http://order.posist.co/order-placed?auth_id=5767efc754b2f3a458f6b9a3');
  //http://order.posist.co/pay-order?auth_id=57cfbca74962bb2e58289a60
};

exports.payUFailure = function(req, res) {

  res.writeHead(302, {
    'Location': 'http://order.posist.co/payment-failure/'
  });
  res.end();
  //res.redirect('order.posist.co/payment-failure?auth_id=5767efc754b2f3a458f6b9a3');
};

exports.getPayUStatus = function(req, res) {
  var options = { method: 'POST',
    url: req.body.url,
    qs: { form: '2' },
    formData: req.body.data
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);

    return res.status(200).json(body);
  });
};

exports.refundTransaction = function(req, res){
  OnlineOrderActivation.findOne({"tenant_id": req.body.tenant_id}, function (err1, onlineOrderAct) {
    if (err1) {
      return handleError(res, err1);
    }
    if(onlineOrderAct){
      var hash = sha512(onlineOrderAct.payUKey+'|'+'cancel_refund_transaction'+'|'+req.body.onlineTransaction.transactionId+'|'+onlineOrderAct.payUSalt);
      var formData = {
        key : onlineOrderAct.payUKey,
        command : 'cancel_refund_transaction',
        hash : hash,
        var1 : req.body.onlineTransaction.transactionId,
        var2 : new Date().getTime(),
        var3 : req.body.amount
      }
      var options = { method: 'POST',
        url: 'https://test.payu.in/merchant/postservice.php?form=2',
        form:formData
      };
      request(options, function(err,httpResponse,body){ 
        if(err){
          return res.send(500, {message: "some error occured"});
        }
        console.log('', httpResponse)

        if(httpResponse.statusCode == 302){
          return res.send(200, {response:httpResponse});
        }  
        else
          return res.send(500, {message: "some error occured"});
      })
    }
  })
}

exports.redirectToPayU = function(req,res){
    console.log(req.body)
    OnlineOrderActivation.findOne({tenant_id: req.body.tenant_id}, function (err, onlineOrderActivation) {
      console.log(onlineOrderActivation)
      var hash = sha512(onlineOrderActivation.payUKey+'|'+req.body.txnid+'|'+req.body.amount+'|'+req.body.productinfo+'|'+req.body.firstname+'|'+req.body.email+'|||||||||||'+onlineOrderActivation.payUSalt);
      var formData = {
        firstname : req.body.firstname,
        lastname : "",
        surl : req.body.surl,
        phone : req.body.mobile,
        key : onlineOrderActivation.payUKey,
        hash : hash,
        curl : req.body.curl,
        furl : req.body.furl,
        txnid : req.body.txnid,
        productinfo : req.body.productinfo,
        email : req.body.email,
        amount : req.body.amount
      }
      console.log(formData)
      var options = { method: 'POST',
        url: req.body.payuUrl,
        form:formData
      };
      request(options, function(err,httpResponse,body){ 
        if(err){
          console.log(err);
          return;
        }
        console.log(httpResponse)

        if(httpResponse.statusCode == 302){
          

          return res.send(200, {location:httpResponse.headers.location});
        }  
        else
          return res.send(500, {message: "some error occured"});
      })
    })
}

exports.payUMobilePost =function(req,res)
{
  console.log(req.body);
  payUMobile.update({tenant_id:req.body.tenant_id},req.body,{upsert:true},function(err,payumobile)
  {
    if(err){return handleError(res,err);}
    console.log(payumobile)
    return res.status(200).json({updated:true})
  })
}
exports.payUMobileDelete = function(req, res) {
  payUMobile.find({tenant_id:req.params.id}, function (err, payUMobileresult) {
    if(err) { return handleError(res, err); }
    if(!payUMobileresult) { return res.send(404); }
    payUMobile.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};
exports.payUMobileRedirect = function(req,res){
    console.log(req.body)
    payUMobile.findOne({tenant_id: req.body.tenant_id}, function (err, payUMobileresult) {
      console.log(payUMobileresult)
      var hash = sha512(payUMobileresult.payUKey+'|'+req.body.txnid+'|'+req.body.amount+'|'+req.body.productinfo+'|'+req.body.firstname+'|'+req.body.email+'|||||||||||'+payUMobileresult.payUSalt);
      var formData = {
        firstname : req.body.firstname,
        lastname : "",
        surl : req.body.surl,
        phone : req.body.mobile,
        key : payUMobileresult.payUKey,
        hash : hash,
        curl : req.body.curl,
        furl : req.body.furl,
        txnid : req.body.txnid,
        productinfo : req.body.productinfo,
        email : req.body.email,
        amount : req.body.amount
      }
      console.log(formData)
      var options = { method: 'POST',
        url: req.body.payuUrl,
        form:formData
      };
      request(options, function(err,httpResponse,body){ 
        if(err){
          console.log(err);
          return;
        }
        console.log('', httpResponse)
        if(httpResponse.statusCode == 302)
          return res.send(200, {location:httpResponse.headers.location});
        else
          return res.send(500, {message: "some error occured"});
      })
    })
}


exports.payUMobilePost =function(req,res)
{
  console.log(req.body);
  payUMobile.update({tenant_id:req.body.tenant_id},req.body,{upsert:true},function(err,payumobile)
  {
    if(err){return handleError(res,err);}
    console.log(payumobile)
    return res.status(200).json({updated:true})
  })
}
exports.payUMobileDelete = function(req, res) {
  payUMobile.find({tenant_id:req.params.id}, function (err, payUMobileresult) {
    if(err) { return handleError(res, err); }
    if(!payUMobileresult) { return res.send(404); }
    payUMobile.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};
exports.payUMobileRedirect = function(req,res){
    console.log(req.body)
    payUMobile.findOne({tenant_id: req.body.tenant_id}, function (err, payUMobileresult) {
      console.log(payUMobileresult)
      var hash = sha512(payUMobileresult.payUKey+'|'+req.body.txnid+'|'+req.body.amount+'|'+req.body.productinfo+'|'+req.body.firstname+'|'+req.body.email+'|||||||||||'+payUMobileresult.payUSalt);
      var formData = {
        firstname : req.body.firstname,
        lastname : "",
        surl : req.body.surl,
        phone : req.body.mobile,
        key : payUMobileresult.payUKey,
        hash : hash,
        curl : req.body.curl,
        furl : req.body.furl,
        txnid : req.body.txnid,
        productinfo : req.body.productinfo,
        email : req.body.email,
        amount : req.body.amount
      }
      console.log(formData)
      var options = { method: 'POST',
        url: req.body.payuUrl,
        form:formData
      };
      request(options, function(err,httpResponse,body){ 
        if(err){
          console.log(err);
          return;
        }
        console.log('', httpResponse)
        if(httpResponse.statusCode == 302)
          return res.send(200, {location:httpResponse.headers.location});
        else
          return res.send(500, {message: "some error occured"});
      })
    })
}


/******************CLOSE OPEN API FUNCTIONS************************************/
