'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OnlineOrderSchema = new Schema({
    tab: String,
    tabId: String,
    tabType: String,
    customer: {},
    items: [],
    billOfferDiscount:[],
    instance_id: String,
    deployment_id: Schema.Types.ObjectId,
    tenant_id: Schema.Types.ObjectId,
    isSynced: Boolean,
    created: {type: Date, default: Date.now}, // Online Creation
    billNumber: Number,
    closeTime: {type: Date},
    kotPrintTime: {type: Date},
    billPrintTime: {type: Date},
    dispatchTime: {type: Date},
    settleTime: {type: Date},
    payments: {},
    source:{},
    isRejected:Boolean,
    trigger:{},
    favouriteName:String,
    acceptUrl:String,
    rejectUrl:String,
    kotPrintUrl:String,
    billPrintUrl:String,
    dispatchUrl:String,
    settleUrl:String,
    status:String,
    discount:{},
    charges:[],
    onlineTransactions:[],
    orderScheduleTime : {type:Date}
});

module.exports = mongoose.model('OnlineOrder', OnlineOrderSchema);