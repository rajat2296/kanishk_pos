
module.exports = {
  doQueue: function(queue) {

      queue.process('getItemByTabPartner',function(job, done){
      console.log(job.data);

      Tab.find({'tabType':job.data.tabtype,deployment_id:job.data.deployment_id},{categories:1},{limit:1},function(err,tab){
        if(err){ return handleError(res,err); }
        
        var categoryIds=[];
        // console.log(tab);
        if(tab.length>0) {
          _.forEach(tab[0].categories, function (cat) {categoryIds.push(cat._id);});
        }

        Item.find({deployment_id:job.data.deployment_id,"category._id":{$in:categoryIds}},{name:1,description:1,itemNumber:1,category:1,rate:1,tabs:1},function(erri,items){
          if(erri){return handleError(res,erri)};
          var nItems=[];
          
          items.forEach(function(item){
            var itemOb={_id:item._id,description:item.description,name:item.name,itemNumber:item.itemNumber,category:{_id: item.category._id,name:item.category.categoryName},rate:item.rate,taxes:null,count:0};
            var _filterTab= _.filter(item.tabs,{tabType:job.data.tabtype});
            if(_filterTab.length>0){
              if(_.has(_filterTab[0],'taxes')){
                itemOb.taxes=_filterTab[0].taxes;
              }
              if(_.has(_filterTab[0],'item')){
                if(_.has(_filterTab[0].item,'rate')){
                  if(_filterTab[0].item.rate!=null){
                    itemOb.rate=_filterTab[0].item.rate;
                  }
                }
              }
            }
            nItems.push(itemOb);
          })

          done(null, nItems);
        });
      })
    });
   
  
  }
}