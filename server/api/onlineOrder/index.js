'use strict';

var express = require('express');
var controller = require('./onlineOrder.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post('/executeUrl', controller.executeUrl);
router.post('/refundTransaction', controller.refundTransaction);
router.post('/modifyUrl', controller.modifyUrl);
router.post('/updateOrderModify', controller.updateOrderModify);
router.get('/executeSMS', controller.executeSMS);
router.get('/getDeployment', controller.getDeployment);
router.get('/checkMobile', controller.checkMobile);
router.get('/getItemByTab', controller.getItemByTab);
router.get('/getItemByTabPartner', controller.getItemByTabPartner);
router.get('/getCustomersByTenant', controller.getCustomersByTenant);
router.get('/getOnlineOrderPageWise', controller.getOnlineOrderPageWise);
router.get('/getOnlineOrderPageWiseccp', controller.getOnlineOrderPageWiseccp);
router.get('/getOpenOrderByDeploymentPageWise', controller.getOpenOrderByDeploymentPageWise);
router.get('/getOnlineOrdersCount', controller.getOnlineOrdersCount);
router.get('/getUserProfile', controller.getUserProfile);

router.get('/', controller.index);
router.get('/:id', controller.show);

router.post('/createFavourite', controller.createFavourite);
router.post('/setOnlineOrderPartner', controller.setOnlineOrderPartner);
router.post('/getEmailOrder',controller.getEmailOrder);
router.post('/checkEmailOrder',controller.checkEmailOrder);
router.post('/payUSuccess',controller.payUSuccess);
router.post('/payUFailure',controller.payUFailure);
router.post('/payUMobile', controller.payUMobilePost);
router.post('/payUMobileRedirect', controller.payUMobileRedirect);
router.post('/getPauUStatus',controller.getPayUStatus);
router.post('/createdOnlineOrderActivation',controller.createdOnlineOrderActivation);
router.post('/redirectToPayU',controller.redirectToPayU);
router.post('/getOnlineOrderActivation', controller.getOnlineOrderActivation);
//Tanvee payu integration
router.post('/payUSuccess', controller.payUSuccess);
router.post('/payUFailure', controller.payUFailure);
router.post('/getStatus', controller.getStatus);

router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/payUMobile/:id', controller.payUMobileDelete);
router.delete('/:id', controller.destroy);

module.exports = router;

//for open API
router.use('/partner/*', auth.onlyOpenApi);

router.get('/partner/getItemByTab', auth.hasCustomerKey, controller.getItemByTab_secured);
router.get('/partner/getFavourite', auth.hasCustomerKey, controller.getFavourite_secured);
router.get('/partner/deployment/identify', auth.hasCustomerKey, controller.identifyDeployment_secured);
router.get('/partner/getDeployment', auth.hasCustomerKey, controller.getDeployment_secured);
router.get('/partner/getCustomersByTenant', auth.hasCustomerKey, controller.getCustomersByTenant_secured);
router.get('/partner/getOnlineOrderPageWise', auth.hasCustomerKey, controller.getOnlineOrderPageWise_secured);
router.get('/partner/getDeploymentTabs', auth.hasCustomerKey, controller.getDeploymentTabs_secured);

router.post('/partner/setOnlineOrder', auth.hasCustomerKey, controller.convertOrderForPartner, controller.setOnlineOrder_secured);
router.post('/partner/createFavourite', auth.hasCustomerKey, controller.createFavourite_secured);
