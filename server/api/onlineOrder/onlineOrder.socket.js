/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var OnlineOrder = require('./onlineOrder.model');
var AdvanceBooking=require('../advancebooking/advancebooking.model');
var moment=require('moment');
var _ = require('lodash');
var Client = require('node-rest-client').Client;
var client = new Client();
exports.register = function(socket) {
  OnlineOrder.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  OnlineOrder.schema.post('update', function (doc) {
    console.log('update socket');
    //onUpdate(socket, doc);
  });
/*  OnlineOrder.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });*/
  runInfinite(socket);
}

function onSave(socket, doc, cb) {
  console.log('save'+doc.deployment_id);
  socket.broadcast.emit('onlineOrder_save'+doc.deployment_id, doc);
}
/*function onUpdate(socket, doc, cb) {
  console.log('update'+doc);
  socket.broadcast.emit('onlineOrder_save'+doc.deployment_id, doc);
}*/
function runInfinite(socket){
  var minutes = 2;
  var the_interval = minutes * 60*1000 ;
  var stopTimeMin=30;
  var ids=[];
  setInterval(function() {
    ids=[];
    OnlineOrder.find({closeTime:null},function(err,orders){
      if(!err){
        _.forEach(orders,function(order){
          socket.broadcast.emit('onlineOrder_save'+order.deployment_id, order);
          var ms = moment(new Date()).diff(moment(new Date( order.created)));
          //console.log(ms/(1000*60));
          if((ms/(1000*60))>stopTimeMin){
            ids.push(order._id);
          }
        })
      }
     // console.log(ids);
       OnlineOrder.update({_id:{$in:ids}},{$set:{closeTime:new Date(),status:"Auto rejected from server"}},{multi:true},function(err,res){
        
       });
      
    });
    var cdate=moment(new Date()).add(30, 'minutes').toDate();
    AdvanceBooking.find({_closeTime:null,'advance.datetime':{$lte:cdate}},function(err,advanceBookings){
      if(!err){
        _.forEach(advanceBookings,function(advanceBooking){
         socket.broadcast.emit('advanceBooking_save'+advanceBooking.deployment_id, advanceBooking);
        })
      }
    })
    //socket.broadcast.emit('onlineOrder_save'+'557c3f4e34e397b40fe90110', {"ranjeet":1});
  }, the_interval);
}

/*
function onRemove(socket, doc, cb) {
  socket.emit('onlineOrder:remove', doc);
}*/
