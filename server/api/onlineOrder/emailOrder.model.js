/**
 * Created by Ranjeet Sinha on 5/31/2016.
 */
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EmailOrderSchema = new Schema({
    created: {type: Date, default: Date.now}, // Online Creation
    orderEmail:{}
});

module.exports = mongoose.model('EmailOrder', EmailOrderSchema);
