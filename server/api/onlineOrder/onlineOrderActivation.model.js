/**
 * Created by Ranjeet Sinha on 6/20/2016.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OnlineOrderActivationSchema = new Schema({
    created: {type: Date, default: Date.now}, // Online Creation
    tenant_id:String,
    auth_id:String,
    auth_key:String,
    enabled:Boolean,
    payUKey : String,
    payUSalt : String
});

module.exports = mongoose.model('OnlineOrderActivation', OnlineOrderActivationSchema);