'use strict';

var _ = require('lodash');
var Category = require('./category.model');
var Item = require('../item/item.model');
var Tab = require('../tab/tab.model');
var Q=require('q');

// Get list of categorys
exports.index = function (req, res) {
    /*console.log(req.query);*/
    if(req.query.tenant_id==null){return handleError(res, []);}
    //Category.find({deployment_id: req.query.deployment_id}, function (err, categorys) {
    Category.find(req.query, function (err, categorys) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, categorys);
    });
};

// Get a single category
exports.show = function (req, res) {
    Category.findById(req.params.id, function (err, category) {
        if (err) {
            return handleError(res, err);
        }
        if (!category) {
            return res.send(404);
        }
        return res.json(category);
    });
};

// Creates a new category in the DB.
exports.create = function (req, res) {

    Category.create(req.body, function (err, category) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, category);
    });

    /*Category.collection.insert(req.body, null, function (err, categories) {
     if (err) {
     return handleError(res,err);
     }
     return res.json(201, categories);
     });*/
};
exports.updateSectionInCategoryAndItems = function (req, res) {

  Category.update({_id: req.params.id}, {$set: {section: req.body.section, updated: Date.now()}}, function (err, category){
    if(err)
      return res.status(500).send("Error");
    return res.status(200).send("Success");
  });
};

// Updates an existing category in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    req.body.updated = Date.now();

    Category.findByIdAndUpdate(req.params.id, req.body, {new:true}, function(err, category){
        if(err) return handleError(res, err);
        else if(!category) return res.send(404);
        else {
            getCleanCategory(category, function(cat){
                Item.update({'category._id': cat._id}, {$set: {category:cat, lastModified:Date.now(), updated: Date.now()}}, {multi:true}, function(err,ic){
                    if(err) return handleError(res,err);

                    else{
                        Tab.update({'categories._id':cat._id}, {$set:{updated: Date.now(), 'categories.$':cat}}, {multi:true}, function(err,tc){
                            if(err) return handleError(res,err);
                            return res.json(200,category);
                        });
                    }
                });
            });
        }
    })
};

exports.updateMultiple = function (req, res) {
    console.log(req.body);
    var updateCategory = function(){
        var d= Q.defer();
        if(req.body.liquor.length>0){
            Category.update({_id:{$in:req.body.liquor}}, {$set:{isliquor:true,updated: Date.now()}}, {multi:true},function(err, category){
                if(err) d.reject('error');

                else if(req.body.food.length>0){
                    Category.update({_id:{$in:req.body.food.length>0}}, {$set:{isliquor:false,updated: Date.now()}}, {multi:true},function(err, category){
                        if(err) d.reject('error');

                        else {
                            console.log("success category")
                            d.resolve('success');
                        }
                    });
                }
                else{
                    d.resolve('success');
                }
            });
        }
        else if(req.body.food.length>0){
            Category.update({_id:{$in:req.body.food}}, {$set:{isliquor:false,updated: Date.now()}}, {multi:true},function(err, category){
                if(err) d.reject('error');

                else {
                    console.log("success category")
                    d.resolve('success');
                }
            });
        }
        else{
            d.resolve('success');
        }
        return d.promise;
    }
    updateCategory().then(function (data) {
        if(req.body.liquor.length>0){
            Item.update({'category._id':{$in:req.body.liquor}}, {$set: {'category.isliquor':true, lastModified:Date.now(), updated: Date.now()}}, {multi:true},function(err,ic){
                if(err) return handleError(res, err);

                else if(req.body.food.length>0){
                    Item.update({'category._id':{$in:req.body.food}}, {$set: {'category.isliquor':false, lastModified:Date.now(), updated: Date.now()}}, {multi:true},function(err,ic){
                        if(err) return handleError(res, err);

                        else {
                            console.log("success items")
                            return res.json(200,'success');
                        }
                    });
                }
                else{
                    return res.json(200,'success');
                }
            });
        }
        else if(req.body.food.length>0){
            Item.update({'category._id':{$in:req.body.food}}, {$set: {'category.isliquor':false, lastModified:Date.now(), updated: Date.now()}}, {multi:true},function(err,ic){
                if(err) return handleError(res, err);

                else {
                    console.log("success items")
                    return res.json(200,'success');
                }
            });
        }
        else{
            return res.json(200,'success');
        }

    }, function (err) {
        console.log("promise err")
      return handleError(res, err);
    })

    /*Category.update({_id:{$in:req.body.liquor}}, {$set:{isliquor:true,updated: Date.now()}}, {multi:true},function(err, category){
        if(err) return handleError(res, err);

        else {
            Item.update({category._id:{$in:req.body.liquor}}, {$set: {category.isliquor:true, lastModified:Date.now(), updated: Date.now()}}, {multi:true},function(err,ic){
                if(err) return handleError(res,err);

                else{
                    Category.update({_id:{$in:req.body.food}}, {$set:{isliquor:false,updated: Date.now()}}, {multi:true},function(err, category){
                        if(err) return handleError(res, err);

                        else {
                            Item.update({category._id:{$in:req.body.food}}, {$set: {category.isliquor:false, lastModified:Date.now(), updated: Date.now()}}, {multi:true},function(err,ic){
                                if(err) return handleError(res,err);

                                else{
                                    return res.json(200,category);
                                }
                            });
                        }
                    })
                }
            });
        }
    })*/
};

exports.updateSectionInCategoryAndItems = function (req, res) {

  Category.update({_id: req.params.id}, {$set: {section: req.body.section, updated: Date.now()}}, function (err, category){
    if(err)
      return res.status(500).send("Error");
    return res.status(200).send("Success");
  });
};

//Delete Multiple Categories
exports.deleteMultipleCategories = function(req,res){
    var categories = req.body.categories;
    Category.remove({_id:{$in:categories}}, function(err,c){
        if(err) return handleError(res,err)
        else return res.json(200,{categories: categories});
    });
};

// Deletes a category from the DB.
exports.destroy = function (req, res) {
    Category.findById(req.params.id, function (err, category) {
        if (err) {
            return handleError(res, err);
        }
        if (!category) {
            return res.send(404);
        }
        category.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

function getCleanCategory(category, cb){
    var cat = _.cloneDeep(category._doc);
    delete cat.tenant_id;
    delete cat.deployment_id;
    delete cat.created;
    delete cat.updated;
    delete cat.__v;
    cat._id = category._id.toString();
    return cb(cat);
};

function handleError(res, err) {
    return res.send(500, err);
};
