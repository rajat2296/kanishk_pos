'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CategorySchema = new Schema({
    categoryName: String,
    isSemiProcessed:{type:Boolean,default:false},
    isAddons:{type:Boolean,default:false},
    isCombos:{type:Boolean,default:false},
    tenant_id: Schema.Types.ObjectId,
    deployment_id: Schema.Types.ObjectId,
    superCategory: {},
    categoryStation: {},
    updated: {type: Date, default: Date.now},
    created: { type: Date, default: Date.now },
    isliquor:{type:Boolean,default:false},
    section: {
      name: {
        type: String,
      },
      _id: {
        type: Schema.Types.ObjectId,
      }
    }
});


CategorySchema
    .path('categoryName')
    .validate(function (value, respond) {
        var self = this;
        if (this.isNew) {
            this.constructor.findOne({categoryName:value, deployment_id:this.deployment_id}, function (err, c) {

                if (err) throw err;
                if (c) {
                    if (self.categoryName === c.categoryName) return respond(false);
                    return respond(false);
                }
                respond(true);
            });
        } else {
            respond(true);
        }


    }, 'Duplicate Category Name, please use another name.');


module.exports = mongoose.model('Category', CategorySchema);
