'use strict';

var _ = require('lodash');
var StockUnit = require('./stockUnit.model');

// Get list of stockUnits
exports.index = function(req, res) {
  StockUnit.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}, function (err, stockUnits) {
    if(err) { return handleError(res, err); }
    return res.json(200, stockUnits);
  });
};

// Get a single stockUnit
exports.show = function(req, res) {
  StockUnit.findById(req.params.id, function (err, stockUnit) {
    if(err) { return handleError(res, err); }
    if(!stockUnit) { return res.send(404); }
    return res.json(stockUnit);
  });
};

// Creates a new stockUnit in the DB.
exports.create = function(req, res) {
  StockUnit.create(req.body, function(err, stockUnit) {
    if(err) 
      { 
        res.json({error:err});
        //return handleError(res, err); 
      }
      else
      {
        return res.json(201, stockUnit);
      }
  });
};

// Updates an existing stockUnit in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  StockUnit.findById(req.params.id, function (err, stockUnit) {
    if (err) { return handleError(res, err); }
    if(!stockUnit) { return res.send(404); }
    //var updated = _.merge(stockUnit, req.body);
    var updated=_.extend(stockUnit, req.body);
    updated.markModified("unitName");
    updated.markModified("baseUnit");
    updated.markModified("conversionFactor");
    updated.markModified("baseConversionFactor");
    updated.save(function (err) {
      if (err) { 
        //return handleError(res, err); 
        res.json({error:err});
      }
      return res.json(200, stockUnit);
    });
  });
};

// Deletes a stockUnit from the DB.
exports.destroy = function(req, res) {
  StockUnit.findById(req.params.id, function (err, stockUnit) {
    if(err) { return handleError(res, err); }
    if(!stockUnit) { return res.send(404); }
    stockUnit.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}