'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockUnitSchema = new Schema({
  unitName:String,
  baseUnit: {},
  conversionFactor: Number,
  baseConversionFactor:Number,
  tenant_id: Schema.Types.ObjectId,
  lnCode: String,
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  lastUpdated: {
    date: Date,
    username: String
  },
  active: Boolean
});

StockUnitSchema.set('versionKey', false);

StockUnitSchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});

StockUnitSchema.path('unitName').validate(function(v, respond) {
  var self=this;
 // if(this.isNew)
 //  { 
    this.constructor.findOne({'unitName':  new RegExp('^'+v+'$', "i"),deployment_id:this.deployment_id}, function (err, storeunits) {
    
    var flag=true;
    if(err){throw err;}
    
    if(storeunits)
    {
      if(storeunits.unitName.toLowerCase()===self.unitName.toLowerCase())
      {
        if(storeunits.id!=self.id){
          flag=false;
        }
      }
    }
    //     else{
    //       if(storeunits.id===self.id)
    //       {
    //         flag=true;
    //       }
    //       else
    //       {
    //         flag=false;
    //       }
    //     }
    //   }
    //   else
    //   {
    //     flag=true; 
    //   }      
    // }
    // else
    // {
    //   flag=true;
    // }
    respond(flag);
    //console.log(flag);
    });
  
}, 'This Unit is already used please try  with another name !');

module.exports = mongoose.model('StockUnit', StockUnitSchema);

//StockUnitSchema.path('unitName').validate(function(v, fn) {
  //var StockUnitModel = mongoose.model('StockUnit');
  //StockUnitModel.find({'unitName':  new RegExp('^'+v+'$', "i")}, function (err, su) {
    //console.log(err);
    //console.log(su);
    //fn(err || su.length === 0);
  //});
//}, 'This Unit is already used please try  with another name !');