/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockUnit = require('./stockUnit.model');

exports.register = function(socket) {
  StockUnit.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockUnit.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockUnit:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockUnit:remove', doc);
}