'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockCategorySchema = new Schema({
	categoryName:String,
	discription:String,
	store:{},
  item:[],
	tenant_id: Schema.Types.ObjectId,
	deployment_id:String,
	created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
	active: Boolean,
  lastUpdated: {
    date: Date,
    username: String
  },
});

StockCategorySchema.set('versionKey', false);
StockCategorySchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});

StockCategorySchema.path('categoryName').validate(function(v, respond) {
  var self=this;
  //console.log(this.store.storeName);
    this.constructor.findOne({'categoryName':  new RegExp('^'+v+'$', "i"),'store.storeName':this.store.storeName,deployment_id:this.deployment_id}, function (err, storeCat) {
    var flag=true;
    if(err){throw err;}
    if(storeCat)
    {
      if(storeCat.categoryName.toLowerCase()===self.categoryName.toLowerCase())
      {
        if(storeCat.id!=self.id){
          flag=false;
        }
      }
    }
    //     else{
    //       if(storeCat.id===self.id)
    //       {
    //         flag=true;
    //       }
    //       else
    //       {
    //         flag=false;
    //       }
    //     }
    //   }
    //   else
    //   {
    //     flag=true; 
    //   }      
    // }
    // else
    // {
    //   flag=true;
    // }
    respond(flag);
    });
  
}, 'This Category name is already used please try  with another name !');

module.exports = mongoose.model('StockCategory', StockCategorySchema);