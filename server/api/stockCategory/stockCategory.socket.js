/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockCategory = require('./stockCategory.model');

exports.register = function(socket) {
  StockCategory.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockCategory.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockCategory:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockCategory:remove', doc);
}