'use strict';

var _ = require('lodash');
var StockCategory = require('./stockCategory.model');

// Get list of stockCategorys
exports.index = function(req, res) {
  StockCategory.find({tenant_id: req.query.tenant_id, deployment_id: req.query.deployment_id}, function (err, stockCategorys) {
    if(err) { return handleError(res, err); }
    return res.json(200, stockCategorys);
  });
};

// Get a single stockCategory
exports.show = function(req, res) {
  StockCategory.findById(req.params.id, function (err, stockCategory) {
    if(err) { return handleError(res, err); }
    if(!stockCategory) { return res.send(404); }
    return res.json(stockCategory);
  });
};

// Creates a new stockCategory in the DB.
exports.create = function(req, res) {
  StockCategory.create(req.body, function(err, stockCategory) {
    if(err) { 
     return  res.json({error:err});
      //return handleError(res, err); 
    }
    else
    {
      return res.json(201, stockCategory);
    }
  });
};

// Updates an existing stockCategory in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  StockCategory.findById(req.params.id, function (err, stockCategory) {
    if (err) { return handleError(res, err); }
    if(!stockCategory) { return res.send(404); }
    var updated = _.extend(stockCategory, req.body);
    updated.markModified("store");
    updated.markModified("item");
    updated.save(function (err) {
      if (err) { 
        //return handleError(res, err); 
        return  res.json({error:err});
      }
      return res.json(200, stockCategory);
    });
  });
};

// Deletes a stockCategory from the DB.
exports.destroy = function(req, res) {
  StockCategory.findById(req.params.id, function (err, stockCategory) {
    if(err) { return handleError(res, err); }
    if(!stockCategory) { return res.send(404); }
    stockCategory.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}