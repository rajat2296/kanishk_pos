/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var EditTransactionHistory = require('./editTransactionHistory.model');

exports.register = function(socket) {
  EditTransactionHistory.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  EditTransactionHistory.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('editTransactionHistory:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('editTransactionHistory:remove', doc);
}