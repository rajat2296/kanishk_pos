'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EditTransactionHistorySchema = new Schema({
  editHistoryId: {type: String},
  transactionId:String,
  mainTransactionId:String,
  billNo:String,
  invoiceNumber:String,
  discountType:{type:String,default:'percent'},
  discount:{type:Number,default:0},
  items:[],
  user:{},
  tenant_id: Schema.Types.ObjectId,  
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  active:  { type: Boolean , default: true },
  charges: []
},{versionKey:false});

module.exports = mongoose.model('EditTransactionHistory', EditTransactionHistorySchema);