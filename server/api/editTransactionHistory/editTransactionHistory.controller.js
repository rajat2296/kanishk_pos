'use strict';

var _ = require('lodash');
var EditTransactionHistory = require('./editTransactionHistory.model');

// Get list of editTransactionHistorys
exports.index = function(req, res) {
  EditTransactionHistory.find(function (err, editTransactionHistorys) {
    if(err) { return handleError(res, err); }
    return res.json(200, editTransactionHistorys);
  });
};

// Get a single editTransactionHistory
exports.show = function(req, res) {
  EditTransactionHistory.findById(req.params.id, function (err, editTransactionHistory) {
    if(err) { return handleError(res, err); }
    if(!editTransactionHistory) { return res.send(404); }
    return res.json(editTransactionHistory);
  });
};

// Creates a new editTransactionHistory in the DB.
exports.create = function(req, res) {
  //console.log(req.body);
  EditTransactionHistory.create(req.body, function(err, editTransactionHistory) {
    if(err) { return handleError(res, err); }
    return res.json(201, editTransactionHistory);
  });
};

//Get by Main Id
exports.getHistoryByMainTransactionId=function(req,res){
console.log(req.query.mainTransactionId);
  EditTransactionHistory.find({'mainTransactionId':req.query.mainTransactionId}, function (err, editTransactionHistory) {
    if(err) { 
      return handleError(res, err); 
    }
    if(!editTransactionHistory) { return res.send(404); }
    return res.json(200, editTransactionHistory);
  });
};

// Updates an existing editTransactionHistory in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  EditTransactionHistory.findById(req.params.id, function (err, editTransactionHistory) {
    if (err) { return handleError(res, err); }
    if(!editTransactionHistory) { return res.send(404); }
    var updated = _.merge(editTransactionHistory, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, editTransactionHistory);
    });
  });
};

// Deletes a editTransactionHistory from the DB.
exports.destroy = function(req, res) {
  EditTransactionHistory.findById(req.params.id, function (err, editTransactionHistory) {
    if(err) { return handleError(res, err); }
    if(!editTransactionHistory) { return res.send(404); }
    editTransactionHistory.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}