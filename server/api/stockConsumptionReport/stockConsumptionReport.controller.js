
'use strict';

var _ = require('lodash');
var moment = require('moment');
var Q = require('q');
var async = require('async');
var Stock = require('./stockConsumptionReport.model.js');
var ObjectId = require('mongoose').Types.ObjectId;
var Store = require('../store/store.model');
var config = require('../../config/environment');
//var kue = require('kue'), queue = kue.createQueue(config.redisConn);
var utils = require('../Utils/utils');
var request = require('request');
var $scope = {};
var xl = require('excel4node'), async = require('async');
var path = require('path');
var fs = require('fs');
var moment = require('moment');
var request = require('request');
var Utils = require('../Utils/utils');
var email = require("emailjs/email");
var baseUrl = "localhost:9000";
var options = {
  jszip: {
    compression: 'DEFLATE'
  },
  dateFormat: 'm/d/yy'
  
};
var wsOpts = {
  margins: {
    left: .75,
    right: .75,
    top: 1.0,
    bottom: 1.0,
    footer: .5,
    header: .5
  },
  printOptions: {
    centerHorizontal: true,
    centerVertical: false
  },
  view: {
    zoom: 100
  }
};
var server = email.server.connect({
  user:    "posist",
  password:"pos123ist",
  host:    "smtp.sendgrid.net",
  ssl:     true
});

// Get list of stockReportNews
exports.index = function (req, res) {
  StockReportNew.find(function (err, stockReportNews) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, stockReportNews);
  });
};

//----------------------------------------------------//

exports.testFunction = function (req, res) {
  var startDate = new Date("7/16/2016");
  //startDate.setDate(startDate.getDate() - 30);
  startDate.setHours(2, 0, 0, 0);
  var endDate = new Date("7/30/2016");
  endDate.setHours(2, 0, 0, 0);
  endDate.setDate(endDate.getDate() + 1);
  var data = {};
  var itemId = "574e6c595e996a000e02f8f1";
  //var toStoreId="5606a613c1b489140a2c9827";
  //var toStoreId="56458b36762aa93c0ede086f";
  //var storeId="5606a60cc1b489140a2c9826";
  //var storeId="567b8c10926b3c8c089681ed";//Main
  //var storeId="56458b36762aa93c0ede086f";//Kitchen
  var storeId = "574e6a5b5e996a000e02f8d8";//test
  //var storeId="56b591726f56c16c2f5d50bc";//ven kitchen
  var itemType = "RawMaterial";
  //var deployment_id="556eadf102fad90807111eac";
  var deployment_id = "573aeffad77db8a80bba3306";
  //var deployment_id="56b0619252a4a4181411eebf";
  //var deployment_id="565d623468d0632d209f1c73";
  var tenant_id = "573aed74d77db8a80bba32e4";
  //var tenant_id="565d4639036c38514155dc13";
  //var tenant_id="560553df5c70b1a242892596";
  // getStockEntryByItem_ConsumptionReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getStockSaleByItem_ConsumptionReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getRecipesOfBillItems(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  //getRecipesFinishedFood(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //  return res.json(val);
  //},function(err){return handleError(res,err);});
  // BillgetItemWiseReport(deployment_id,tenant_id,startDate,endDate).then(function(val){
  //   return res.json(valueget);
  // },function(err){return handleError(res,err);});
  //console.log('testFunction');
  //getBillDataForGrowthReport(startDate, endDate, deployment_id, tenant_id).then(function (result){
  //  //console.log(res);
  //  return res.status(200).json(result)
  //}, function (err) {
  //  console.log(err)
  //  return res.send(500, err)
  //});
  //console.log(moment().subtract('days', 7).calendar());
  //console.log(moment(startDate).format('L'));
  //console.log(moment(startDate).add(7, 'days').calendar());
  var week1 = new Date(startDate);
  var week2 = new Date(endDate);
  getGrowthData(startDate, moment(week1).add('days', 7).toDate(), moment(week2).subtract('days', 7).toDate(), endDate, deployment_id).then(function (result) {
    return res.status(200).json(result);
  });
  //getBillingData(startDate, endDate, deployment_id, tenant_id, "true", storeId).then(function (val) {
  //  return res.json(val);
  //}, function (err) {
  //  return handleError(res, err);
  //});/////////////////////////////////////////////////////////////////////
  // getProcessedAndIntermediateReceipeDetails_Sale(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getPhysicalStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getOpeningStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getOpeningStockByItem_Intermediate(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateReceipeDetails_IntermediateReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateReceipeDetails_IntermediateEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getStockReceive_RequirementEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getStockReturn_RequirementEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getIntermediateConsumptionOn_MenuSale(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // // getOpeningStockByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getLastPriceOfItem_RawMaterial(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // dataBetweenDate(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // dataBeforeDate(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getWastageStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Wastage(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Physical(startDate,endDate,deployment_id,tenant_id,storeId).then(function (val) {
  //   return res.json(val);
  // }, function (err) { return handleError(res, err);});
  // getTransferStockByItemFromStore(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err);});
  // getTransferStockByItemToStore(startDate,endDate,deployment_id,tenant_id,toStoreId).then(function(val){
  //   return res.json(val);
  // },function(err){ return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val)
  // },function(err){return handleError(res,err);});
  // getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate,endDate,deployment_id,tenant_id,toStoreId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockSale_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});

  // getWastage_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getPhysical_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getTransfer_ItemConsumptionReport_MenuItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getConsumptionSummary_ItemReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getRecipeQtyInBase(deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockEntryByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockSaleByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Sale_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getPhysicalStockByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getWastageStockByItem_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Wastage_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getTransferStockByItemFromStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getTransferStockByItemToStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_ClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStores(deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getClosingQty(startDate,endDate,deployment_id,tenant_id).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getReceipeDetails_FinishedFoodEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)})
  // getReceipeDetails_FinishedFood_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)})
  // getReceipeDetails_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStockReceive_RequirementEntry(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getStoresProcessedCategory(deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
  // getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId).then(function(val){
  //   return res.json(val);
  // },function(err){return handleError(res,err)});
};

function getStores(deployment_id, tenant_id) {
  var d = Q.defer();
  Store.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      },
    },
    {
      $project: {
        storeId: "$_id",
        storeName: "$storeName",
        isKitchen: "$isKitchen"
      }
    }], function (err, stores) {
    if (err) {
      d.reject("error");
    }
    else {
      d.resolve(stores);
    }
  });
  return d.promise;
};

function getStoresProcessedCategory(deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Store.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        _id: new ObjectId(storeId),
        processedFoodCategory: {$ne: undefined},
      },
    },
    {
      $unwind: "$processedFoodCategory"
    },
    {
      $unwind: "$processedFoodCategory.item"
    },
    {
      $project: {
        storeId: "$_id",
        storeName: "$storeName",
        isKitchen: "$isKitchen",
        items: "$processedFoodCategory.item"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$storeId",
          storeName: "$storeName",
          isKitchen: "$isKitchen"
        }
      }
    },
    {
      $project: {
        _id: 0,
        storeId: "$_id.storeId",
        storeName: "$_id.storeName",
        isKitchen: "$_id.isKitchen",
        itemId: "$_id.itemId",
        itemName: "$_id.name"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStoresProcessedCategory');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "9",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lte: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalStockReturnQty: "$_id.totalQty",
        totalStockReturnAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockReturn_RequirementEntry');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};
function getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "12",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        },
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalStockReceiveQty: "$_id.totalQty",
        totalStockReceiveAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockReceive_RequirementEntry');;

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log('getStockEntryByItem_ConsumptionReport')
  //console.log(startDate, endDate, deployment_id, tenant_id, storeId);
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockEntryByItem_ConsumptionReport');
      console.log(err);
      d.reject('error');
    }
    //console.log('entry', result);
    d.resolve(result);

  });
  return d.promise;
};

function getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log('getStockSaleByItem_ConsumptionReport');
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getStockSaleByItem_ConsumptionReport');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        //"menuItems.itemType":"MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Sale');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{ $in: ["MenuItem", "IntermediateItem"] },
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      //console.log(result_sale);
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true},
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items.itemId",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      //console.log(result_sale);
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getIntermediateConsumptionOn_MenuSale(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{ $in: ["MenuItem", "IntermediateItem"] },
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_Sale_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $project:{
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        "created": 1,
        daySerialNumber: 1,
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "14",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem_lastDate(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var startDate1 = new Date(endDate);
  startDate1.setDate(startDate1.getDate() - 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate1), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $group: {
        _id: {itemId: "$itemId", name: "$itemName", unitName: "$UnitName", conversionFactor: "$ConversionFactor"},
        totalQty: {$last: "$totalBalanceQty"},
        date: {$last: "$created"},
        billNo: {$last: "$billNo"},
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$date",
        billNo: "$billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getPhysicalStockByItem');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getPhysicalStockByItem_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log(startDate);
  //console.log(endDate);
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items.itemType":"IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        itemtType: {$literal: "IntermediateItem"}
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        //"items.itemType":"MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor",
          menuItem: {$ifNull: ["$items.recipeDetails", false]},
          isSemiProcessed: "$items.isSemiProcessed"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $match: {
        "_id.isSemiProcessed": false
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        itemType: {$literal: "MenuItem"}
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Physical(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  //console.log(tenant_id,deployment_id);
  var beforeDate = moment(new Date()).toDate();
  //var st = new Date(startDate);
  //var tst = moment(st).add('hours', 5.5).toDate();
  //console.log('tst', tst);
  var hour = new Date(startDate).getHours();
  var minutes = new Date(startDate).getMinutes();
  beforeDate.setHours(hour, minutes, 0, 0);
  //beforeDate.setHours(0,0,0,0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  //console.log(new Date(beforeDate));
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('getOpeningStockByItem');
      console.log(err);
      d.reject('error');
    }
    else {
      //console.log(result);
      var checkDate;
      var data = [];
      var isOpeningValid = true;
      var st = moment(new Date(startDate)).toDate();
      _.forEach(result, function (itm, i) {
        if (i == 0) {
          checkDate = moment(new Date(itm.created)).toDate();
          if (new Date(checkDate) < moment(st).subtract('months', 2).toDate())
            isOpeningValid = false;
        }
        var compareDate = moment(new Date(itm.created)).toDate();
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      if (isOpeningValid)
        d.resolve(data);
      else
        d.reject("Your last opening is older than 2 months from your start date. Please enter a new opening!")
    }


  });
  return d.promise;
};

function getOpeningStockByItem2(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  //console.log(tenant_id,deployment_id);
  var beforeDate = new Date();
  var hour = new Date(startDate).getHours();
  var minutes = new Date(startDate).getMinutes();
  beforeDate.setHours(hour, minutes, 0, 0);
  //beforeDate.setHours(0,0,0,0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  //console.log(new Date(beforeDate));
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      var checkDate;
      var data = [];
      var isOpeningValid = true;
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
          if (new Date(checkDate) < moment(startDate).subtract('months', 2).toDate())
            isOpeningValid = false;
        }
        var compareDate = new Date(itm.created);
        //if(checkDate.getTime()<=compareDate.getTime()){
        //  data.push(itm);
        //}
        //console.log(itm.itemName, moment(checkDate).format('L'), moment(compareDate).format('L'), moment(checkDate).format('L') <= moment(compareDate).format('L'))
        if (moment(checkDate).format('L') <= moment(compareDate).format('L')) {
          var a = _.findIndex(data, function (item) {
            return item.itemId == itm.itemId;
          });
          if (a > -1) {
            if (new Date(data[a].created).getTime() < new Date(itm.created).getTime()) {
              data[a] = itm;
            }
          } else {
            data.push(itm);
          }
        }
      });
      //console.log('isoPENINGvALID', isOpeningValid);
      if (isOpeningValid)
        d.resolve(data);
      else
        d.reject("Your last opening is older than 2 months from your start date. Please enter a new opening!")
    }
  });
  return d.promise;
};

function getOpeningStockByItem_Intermediate(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var beforeDate = new Date();
  var hour = new Date(startDate).getHours();
  var minutes = new Date(startDate).getMinutes();
  //beforeDate.setHours(0,0,0,0);
  beforeDate.setHours(hour, minutes, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "10",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        timeStamp: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      //console.log(err);
      //d.reject('error 964');
      d.resolve([]);
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};

function getOpeningStockByItem_FinishedFood(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var beforeDate = new Date();
  var hour = new Date(startDate).getHours();
  var minutes = new Date(startDate).getMinutes();
  beforeDate.setHours(hour, minutes, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "13",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        timeStamp: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      //console.log(err);
      //d.reject('error 964');
      d.resolve([]);
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getIntermediateReceipeDetails_IntermediateEntry');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit",
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"RawMaterial",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getReceipeDetails_FinishedFoodEntry');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit",
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalIntermediateQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};



function getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"RawMaterial",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true},
        "items.isBaseKitchenItem": true
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.itemId", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getReceipeDetails_FinishedFoodEntry_PreviousBkItems');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.qty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.qty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getIntermediateReceipeDetails_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": "IntermediateItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.UnitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.created",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getReceipeDetails_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":"MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": true
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          menuName: "$menuItems.itemName",
          menuId: "$menuItems._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        UnitName: "$_id.UnitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.created",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getWastageStockByItem');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId) {

  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty",
          unitName: "$units.unitName", conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Wastage_old')
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{ $in: ["MenuItem", "IntermediateItem"] },
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit",
        "items.isSemiProcessed": {$ne: true}
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty",
          unitName: "$units.unitName", conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ], function (err, result_ws) {
    if (err) {

    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getTransferStockByItemFromStore');
      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getTransferStockByItemToStore');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_FromStore');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_ToStore');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};



function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{$in:["MenuItem","IntermediateItem"]},
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "IntermediateItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Transfer_FromStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedFoodReceipeDetails_Transfer_ToStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store.category.items.toStore._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getLastPrice_AllRaw_ReceipePricing(deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          UnitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"},
        totalAmt: {$sum: "$units.totalAmount"},
        lastPrice: {$last: "$units.basePrice"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      _.forEach(result_entry, function (r, i) {
        if (r.UnitName == "Litre" || r.UnitName == "Meter") {
          r.lastPrice = parseFloat(r.lastPrice) / 1000;
        }
      });
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getLastPriceOfItem_RawMaterial(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor",
          lastPrice: "$units.basePrice",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {

      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$_id.lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$_id.totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }

  ], function (err, result_entry) {
    if (err) {
      d.reject(err);
    }
    else {
      _.forEach(result_entry, function (r, i) {
        var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
        r.lastPrice = lp;
      });
      Stock.StockRequirement.aggregate([
        {
          $match: {
            tenant_id: new ObjectId(tenant_id),
            deployment_id: deployment_id,
            ReceiveDate: {$ne: null}
          }
        },
        {
          $unwind: "$store.vendor.category"
        },
        {
          $unwind: "$store.vendor.category.items"
        },
        {
          $unwind: "$store.vendor.category.items.calculateInUnits"
        },
        {
          $project: {
            items: "$store.vendor.category.items",
            created: "$created",
            billNo: "$daySerialNumber",
            units: "$store.vendor.category.items.calculateInUnits"
          }
        },
        {
          $match: {
            "units.type": "baseUnit"
          }
        },
        {
          $group: {
            _id: {
              itemId: "$items._id",
              name: "$items.itemName",
              totalQty: "$units.baseQty",
              conversionFactor: "$units.conversionFactor",
              lastPrice: "$units.basePrice",
              UnitName: "$units.unitName"
            },
            totalAmt: {$sum: "$units.totalAmount"}
          }
        },
        {
          $project: {
            _id: 0,
            itemName: "$_id.name",
            itemId: "$_id.itemId",
            lastPrice: "$_id.lastPrice",
            totalPurchaseAmount: "$totalAmt",
            totalPurchaseQty: "$_id.totalQty",
            UnitName: "$_id.UnitName",
            conversionFactor: "$_id.conversionFactor"
          }
        }
      ], function (err, result_requirement) {
        if (err) {
          d.reject(err);
          //console.log('3749', err);
        }
        else {
          //console.log('3752', result_requirement);
          _.forEach(result_requirement, function (r, i) {
            var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
            r.lastPrice = lp;
            result_entry.push(r);
          });
          d.resolve(result_entry);
          //console.log(result_entry);
        }
      });
    }
  });
  return d.promise;
};

function getLastPriceOfItem_Intermediate(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "7",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor",
          lastPrice: "$units.basePrice",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {

      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$_id.lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$_id.totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      _.forEach(result_entry, function (r, i) {
        var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
        r.lastPrice = lp;
      });
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getLastPriceOfItem_FinishedFood(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "11",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        date: "$created"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", totalQty: "$units.baseQty",
          conversionFactor: "$units.conversionFactor", lastPrice: "$units.basePrice",
          UnitName: "$units.unitName", created: "$date"
        },
        totalAmt: {$sum: "$units.totalAmount"}
        //lp:{ $divide: [ "$units.basePrice", "$recipeQty" ] }
      }
    },
    {

      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$_id.lastPrice",
        totalPurchaseAmount: "$totalAmt",
        totalPurchaseQty: "$_id.totalQty",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor",
        created: "$_id.created"
      }
    }

  ], function (err, result_entry) {
    if (err) {

    }
    else {
      var data = [];
      _.forEach(result_entry, function (r, i) {
        var index = _.findIndex(data, {itemId: r.itemId});
        var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
        r.lastPrice = lp;
        if (index < 0) {
          data.push(r);
        }
        else {
          if (new Date(r.created).getTime() > new Date(data[index].created).getTime()) {
            data[index] = r;
          }
        }
      });
      //d.resolve(result_entry);
      d.resolve(data);
      // _.forEach(result_entry,function(r,i){
      //   var lp=parseFloat(r.lastPrice)/parseFloat(r.conversionFactor);
      //   r.lastPrice=lp;
      // });
      // d.resolve(result_entry);
    }
  });
  return d.promise;
};

function BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate) {
  //console.log('billDate', startDate, endDate);
  //console.log(startDate);
  //console.log(endDate);

  //console.log('billDate start', startDate);
  //console.log('billing end date', endDate);
  var d = Q.defer();
  Stock.Bill.aggregate([
    {
      $match: {
        deployment_id: new ObjectId(deployment_id),
        _created: {
          $gte: moment(new Date(startDate)).toDate(),
          $lte: moment(new Date(endDate)).toDate()
        },
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        _id: {depid: "$deployment_id"},
        _kots: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _created: "$_created"
      }
    },
    {
      $unwind: "$_kots.items"
    },
    {
      $project: {
        _created: 1,
        "items": ["$_kots.items"],
        "addOns": {
          $cond: {
            if: {$and: ["$_kots.items.addOns", {$gt: [{$size: "$_kots.items.addOns"}, 0]}]},
            then: "$_kots.items.addOns",
            else: []
          }
        },
      }
    },
    {
      $project:
      {
        items: {$concatArrays: ["$items", "$addOns"]},
        _created: 1
      }
    },
    {
      $unwind: "$items"
    },
    {
      $project: {
        "items.name": "$items.name",
        "items._id": "$items._id",
        "items.stockQuantity": "$items.stockQuantity",
        "items.unit.conversionFactor": "$items.unit.conversionFactor",
        "items.quantity": "$items.quantity",
        //"items.addOns": "$items.addOns",
        created: "$_created",
        _created: "$_created",

      }
    },
    {
      $sort:
      {
        _created:-1
      }
    },
    {
      $group: {
        _id: {itemId: "$items._id", created: "$created"},
        qty: {$first: "$items.stockQuantity"},
        conF: {$first: "$items.unit.conversionFactor"},
        billQty: {$sum: "$items.quantity"}
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id.itemId",
        totalSaleQty: {$multiply: ["$qty", "$conF"]},
        billQty: "$billQty",
        created: "$_id.created",
        //unitName: "$_id.unitName"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result2) {
    if (err) {
      console.log(err);
      d.reject('error');
    }

//console.log(err);
    //var result = [];
    if (err) {
      console.log('BillgetItemWiseReport');
      console.log(err);
      d.reject('error');
    } else {
      d.resolve(result2);
    }
    // else {
    //   var addOnsDayWise = {};
    //   _.forEach(result2, function (item) {
    //     result.push(item);
    //     var itCreated = new Date(item.created)
    //     var date = moment(itCreated).format('L');
    //     if (!addOnsDayWise[date])
    //       addOnsDayWise[date] = {};
    //     console.log('itemAddons', item.addOns);
    //     _.forEach(item.addOns, function (addOns, i) {
    //       console.log('items addons',addOns);
    //       _.forEach(addOns, function (addOn) {

    //         var add = addOn;
    //         if (!addOnsDayWise[date][addOn.name]) {
    //           addOnsDayWise[date][addOn.name] = {
    //             billQty: 0
    //           };
    //         }
    //         //console.log('name', add.name);
    //         //console.log('_id', addOn._id);
    //         //console.log('add', addOnsDayWise[date]);
    //         //console.log('addOn', addOn);
    //         addOnsDayWise[date][addOn.name].itemId = addOn._id;
    //         addOnsDayWise[date][addOn.name].itemName = addOn.name;
    //         addOnsDayWise[date][addOn.name].billQty += addOn.quantity ? addOn.quantity : 0;
    //         var stockQuantity = addOn.stockQuantity ? addOn.stockQuantity : 0;
    //         var unit = addOn.unit ? addOn.unit : null;
    //         if (unit)
    //           var conversionFactor = addOn.unit.conversionFactor ? addOn.unit.conversionFactor : 0;
    //         addOnsDayWise[date][addOn.name].totalSaleQty = stockQuantity * conversionFactor;
    //         addOnsDayWise[date][addOn.name].created = new Date(item.created);
    //       });
    //     });
    //   });

    //console.log('dayAddOns', addOnsDayWise);
    // _.forEach(addOnsDayWise, function (dayAddOns, day) {
    //   _.forEach(dayAddOns, function (addOn) {
    //     result.push(addOn);
    //   });
    // });
    //console.log('result', result);
    //d.resolve(result);
    //}
    //  d.resolve(result);

  });

  return d.promise;
};



function getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen) {
  var d = Q.defer();
  var data = [];
  if (isKitchen == "true") {
    var allPromise = Q.all([
      getSemiRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id),
      BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate)
    ]);
    allPromise.then(function (value) {
      var recData = value[0];
      var billData = value[1];
      //console.log(billData);
      _.forEach(billData, function (b, i) {
        _.forEach(recData, function (r, ii) {
          //r.itemId,r.totalSaleQty,r.billQty
          if (b.itemId == r.menuId) {
            if (r.itemSaleQty == undefined) {
              r.itemSaleQty = 0;
            }
            ;
            r.recipeId = r.recipeId.toString();
            r.created = b.created;
            //console.log('3893', b.totalSaleQty, r.receipeQtyinBase, b.billQty, r.itemQtyInBase);
            r.itemSaleQty = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemQtyInBase);
            data.push(_.cloneDeep(r));
          }
        });
      });
      d.resolve(data);
    });
  }
  else {
    //console.log(data);
    d.resolve(data);
  }
  return d.promise;
};

function getBillingData_FinishedFood(startDate, endDate, deployment_id, tenant_id, isKitchen) {
  var d = Q.defer();
  var data = [];
  if (isKitchen == "true") {
    var allPromise = Q.all([
      getRecipesFinishedFood(startDate, endDate, deployment_id, tenant_id),
      BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate)
    ]);
    allPromise.then(function (value) {
      var recData = value[0];
      var billData = value[1];
      // console.log(recData);
      // console.log(billData);
      _.forEach(billData, function (b, i) {
        _.forEach(recData, function (r, ii) {
          //r.itemId,r.totalSaleQty,r.billQty
          if (b.itemId == r.menuId) {
            if (r.itemSaleQty == undefined) {
              r.itemSaleQty = 0;
            }
            ;
            r.itemId = r.itemId.toString();
            r.recipeId = r.recipeId.toString();
            r.created = b.created;
            r.itemSaleQty = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemQtyInBase);
            data.push(_.cloneDeep(r));
          }
        });
      });
      d.resolve(data);
    });
  }
  else {
    //console.log(data);
    d.resolve(data);
  }
  return d.promise;
};

function getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId) {
  var d = Q.defer();
  //console.log('storeId: ', storeId);
  var data = [];
  if (isKitchen) {
    //console.log(isKitchen);
    var allPromise = Q.all([
      getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id),
      BillgetItemWiseReport(deployment_id, tenant_id, startDate, endDate),
      getStoresProcessedCategory(deployment_id, tenant_id, storeId)
    ]);
    allPromise.then(function (value) {
      var recData = value[0];
      //  console.log('recDat a', recData[0]);
      var billData = value[1];
      //console.log('billData', billData);
      var items = value[2];
      _.forEach(billData, function (b, i) {
        //console.log(b);
        _.forEach(items, function (it, iii) {
          if (b.itemId == it.itemId) {
            _.forEach(recData, function (r, ii) {
              //console.log(r.unitName);
              //r.itemId,r.totalSaleQty,r.billQty
              if (b.itemId == r.menuId) {
                if (r.itemSaleQty == undefined ) {
                  r.itemSaleQty = 0;
                }
                ;
                r.recipeId = r.recipeId.toString();
                r.created = b.created;
                //console.log('3971', r.itemName, b.totalSaleQty, r.receipeQtyinBase, b.billQty, r.itemQtyInBase);

                r.itemSaleQty = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemQtyInBase);
                r.itemSaleAmt = parseFloat(b.totalSaleQty) / parseFloat(r.receipeQtyinBase) * parseFloat(b.billQty) * parseFloat(r.itemAmountInBase);


                //console.log(r.menuName, b.totalSaleQty, r.receipeQtyinBase, b.billQty, r.itemAmountInBase,r.itemSaleAmt);
                data.push(_.cloneDeep(r));
              }
            });
          }
        });
        ;
      });
      // console.log('data', data);
      d.resolve(data);
    });
  }
  else {
    d.resolve(data);
  }

  return d.promise;
};

function getRecipesOfBillItems_old(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        //receipeDetails:"$receipeDetails",
        rawItems: "$rawItems",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    // {
    //   $project:{
    //     _id:0,
    //     receipeDetails:"$receipeDetails",
    //     receipeQtyinBase:"$receipeQtyinBase",
    //     Qty:{$divide:[menuQty,"$receipeQtyinBase"]}
    //   }
    // },
    {
      $unwind: "$rawItems"
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        menuId: "$menuId",
        recipeId: "$recipeId",
        UnitName: "$rawItems.selectedUnitName",
        recipeName: "$recipeName",
        receipeQtyinBase: "$receipeQtyinBase",
        itemQtyInBase: "$rawItems.baseQuantity",
        isBaseKitchenItem: "$rawItems.isBaseKitchenItem",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        baseUnit: "$baseUnit",
        isBaseKitchenItem: "$isBaseKitchenItem",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        UnitName: "$UnitName",
        //itemSaleQty:{$multiply:["$itemQtyInBase","$Qty",billQty]},
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry)
    }
  });
  return d.promise;
};

function getRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        rawItems: "$receipeDetails",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    {
      $unwind: "$rawItems"
    },
    {
      $match: {
        "rawItems.isSemiProcessed": {$ne: true}
      }
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        receipeQtyinBase: "$receipeQtyinBase",
        UnitName: "$rawItems.selectedUnitName",
        itemQtyInBase: "$rawItems.baseQuantity",
        itemAmountInBase: "$rawItems.calculatedAveragePrice",
        isBaseKitchenItem: "$rawItems.isBaseKitchenItem",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        baseUnit: "$baseUnit",
        isBaseKitchenItem: "$isBaseKitchenItem",
        itemAmountInBase: "$rawItems.calculatedAveragePrice",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        UnitName: "$UnitName",
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getRecipesOfBillItems');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};



function getSemiRecipesOfBillItems(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        rawItems: "$receipeDetails",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    {
      $unwind: "$rawItems"
    },
    {
      $match: {
        "rawItems.isSemiProcessed": true
      }
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        receipeQtyinBase: "$receipeQtyinBase",
        itemQtyInBase: "$rawItems.baseQuantity",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        baseUnit: "$baseUnit",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry)
    }
  });
  return d.promise;
};

function getRecipesFinishedFood(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id),
        isSemiProcessed: false
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id",
        stockItemName: "$recipeName",
        itemName: "$recipeName",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        baseUnit: "$unit.baseUnit.name",
        itemQtyInBase: "$recipeQty",
        receipeQtyinBase: "$recipeQty"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry)
    }
  });
  return d.promise;
};

exports.getLastPriceOfItem_RawMaterial = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportResetTime/getLastPriceOfItem_RawMaterial?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var startDate = new Date();
    var endDate = new Date();
    getLastPriceOfItem_RawMaterial(startDate, endDate, req.query.deployment_id, req.query.tenant_id).then(function (val) {
      return res.json(val);
    }, function (err) {
      return handleError(res, err)
    });
  }
};
exports.getLastPriceOfItem_Intermediate = function (req, res) {
  var startDate = new Date();
  var endDate = new Date();
  getLastPriceOfItem_Intermediate(startDate, endDate, req.query.deployment_id, req.query.tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};
exports.getLastPriceOfItem_FinishedFood = function (req, res) {
  var startDate = new Date();
  var endDate = new Date();
  getLastPriceOfItem_FinishedFood(startDate, endDate, req.query.deployment_id, req.query.tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};
exports.getConsumptionSummary = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportResetTime/getConsumptionSummary?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {

    var resetTime = req.query.resetTime;
    var startDate = moment(new Date(req.query.fromDate)).toDate();
    //startDate.setHours(0, 0, 0, 0);
    var endDate = moment(new Date(req.query.toDate)).toDate();
    //console.log('start',startDate);
    //console.log('end',endDate);
    //endDate.setDate(endDate.getDate() + 1);
    //if(!resetTime) {
    //  console.log('No reset time');
    //  startDate = new Date(startDate.setHours(0, 0, 0, 0));
    //  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
    //}
    //console.log(startDate);
    //console.log(endDate);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }


    var minDate = moment(new Date(startDate)).toDate();
    var data = [];
    var databetween = [];
    var lastPrice = [];
    var returnData = {beforeDate: [], betweenDate: []};
    getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
      //console.log(result_op);
      //console.log('opening qty', result_op.length);
      /**
       * if (result_op.length > 0) {
      _.forEach(result_op, function (op, i) {
        var tempDate = new Date(op.created);
        //console.log(tempDate);
        tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        //console.log('tempDate', tempDate);
        minDate = new Date(tempDate);
        data.push(op);
      });
     */

      if (result_op.length > 0) {
        _.forEach(result_op, function (op, i) {
          var tempDate = moment(new Date(op.created)).toDate();
          //console.log(new Date('8/18/2016'));
          //console.log(tempDate);
          tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
          // console.log('tempDate', tempDate);
          minDate = moment(new Date(tempDate)).toDate();
          data.push(op);
        });
        //console.log("minDate" + minDate);
        //console.log('start date',startDate);
        //console.log('end date',endDate);
        //console.log('min date',minDate);
        if (endDate.getTime() < minDate.getTime()) {
          returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
          //console.log("Report not generated due to latest opening is greater than selected end Date");
          return res.json(returnData);
        }
        else if (minDate.getTime() < startDate.getTime()) {
          //console.log('----------------------');
          //console.log('else if');

          //console.log("op < startDate");
          dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
        else {
          //console.log('----------------------');
          //console.log('else if');
          //console.log("op > startDate and op < end");
          dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            //console.log(value)
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              //console.log(value);
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
      }
      else {
        return res.json(returnData);
      }

    }, function (err) {
      console.log(err);
      returnData.errorMessage = err;
      //console.log("Report not generated due to latest opening is greater than selected end Date");
      return res.json(returnData);
      //return handleError(res, err);
    });
  }
};

exports.getConsumptionSummary_IntermediateReport = function (req, res) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportResetTime/getConsumptionSummary_IntermediateReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var resetTime = req.query.resetTime;
    var startDate = moment(new Date(req.query.fromDate)).toDate();
    //startDate.setHours(0, 0, 0, 0);
    var endDate = moment(new Date(req.query.toDate)).toDate();
    //endDate.setDate(endDate.getDate() + 1);
    //console.log(startDate, endDate)
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    //console.log('store', storeId);
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }


    var minDate = new Date(startDate);
    var data = [];
    var databetween = [];
    var lastPrice = [];
    var returnData = {beforeDate: [], betweenDate: []};
    getOpeningStockByItem_Intermediate(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
      if (result_op.length > 0) {
        _.forEach(result_op, function (op, i) {
          minDate = new Date(op.created);
          data.push(op);
        });
        //console.log("minDate" + minDate);
        if (endDate.getTime() < minDate.getTime()) {
          returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
          //console.log("Report not generated due to latest opening is greater than selected end Date");
          return res.json(returnData);
        }
        else if (minDate.getTime() < startDate.getTime()) {

          //console.log("op < startDate");
          dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
        else {
          //console.log("op > startDate and op < end");
          dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_IntermediateReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
      }
      else {
        return res.json(returnData);
      }


    }, function (err) {
      console.log(err);
      return handleError(res, err);
    });
  }
};

exports.getConsumptionSummary_FinishedFoodReport = function (req, res) {

  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportResetTime/getConsumptionSummary_FinishedFoodReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var resetTime = req.query.resetTime;
    var startDate = moment(new Date(req.query.fromDate)).toDate();
    //startDate.setHours(0, 0, 0, 0);
    var endDate = moment(new Date(req.query.toDate)).toDate();
    //endDate.setDate(endDate.getDate() + 1);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }


    var minDate = new Date(startDate);
    var data = [];
    var databetween = [];
    var lastPrice = [];
    var returnData = {beforeDate: [], betweenDate: []};
    getOpeningStockByItem_FinishedFood(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
      if (result_op.length > 0) {
        _.forEach(result_op, function (op, i) {
          minDate = new Date(op.created);
          data.push(op);
        });
        //console.log("minDate" + minDate);
        if (endDate.getTime() < minDate.getTime()) {
          returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
          //console.log("Report not generated due to latest opening is greater than selected end Date");
          return res.json(returnData);
        }
        else if (minDate.getTime() < startDate.getTime()) {

          //console.log("op < startDate");
          dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
        else {
          //console.log("op > startDate and op < end");
          dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            returnData.beforeDate = value;
            _.forEach(data, function (d, i) {
              returnData.beforeDate.push(d);
            });
            dataBetweenDate_FinishedFoodReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              returnData.betweenDate = value;
              return res.json(returnData);
            }, function (err) {
              return handleError(res, err)
            });
          }, function (err) {
            return handleError(res, err)
          });
        }
      }
      else {
        return res.json(returnData);
      }


    }, function (err) {
      console.log(err);
      return handleError(res, err);
    });
  }
};

function dataBeforeDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    //getReceipeDetails_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId),
    getReceipeDetails_Sale_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_FromStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_ToStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getBillingData_FinishedFood(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getReturnAcceptDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReturn_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function getReturnAcceptDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  //console.log('return Accept', storeId);
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "15",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items.recipeId", name: "$items.itemName",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalReturnAcceptQty: "$_id.qty",
        totalReturnAcceptAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      //console.log('return', result_sale);
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedFoodReturn_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "15",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseWastageQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalReturnWastageQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function dataBetweenDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    //getReceipeDetails_FinishedFoodReport(startDate,endDate,deployment_id,tenant_id,storeId),
    getReceipeDetails_Sale_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_DeliveryChallan_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getProcessedFoodReceipeDetails_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_FromStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReceipeDetails_Transfer_ToStore_New_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getBillingData_FinishedFood(startDate, endDate, deployment_id, tenant_id, isKitchen),
    getReturnAcceptDetails_FinishedFood_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedFoodReturn_Wastage_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBeforeDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateConsumptionOn_MenuSale(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),//finished in
    getProcessedAndIntermediateReceipeDetails_Wastage_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen)

  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBetweenDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateConsumptionOn_MenuSale(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId),//finished in
    getProcessedAndIntermediateReceipeDetails_Wastage_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getBillingData_Intermediate(startDate, endDate, deployment_id, tenant_id, isKitchen)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  // console.log('intermediate', data);
  return d.promise;
};

function dataBetweenDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  //console.log(startDate, endDate);
  //console.log('bewtween start', startDate);
  //console.log('bewtween end date', endDate);
  var data = [];
  var allPromise = Q.all([
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),
    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      //console.log('betBefDate', val.length);
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBeforeDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),

    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      //console.log('befDate', val.length);
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function getStockSale_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.receiver.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits",
        receipeDetails: "$_store.receiver.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getWastage_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        receipeDetails: "$_store.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getPhysical_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        receipeDetails: "$_store.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",

      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getTransfer_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits",
        receipeDetails: "$_store.category.items.receipeDetails"
      }
    },
    {
      $match: {
        "items.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          rawId: "$receipeDetails._id",
          rawName: "$receipeDetails.itemName",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          qty: "$units.baseQty",
          isSemi: "$receipeDetails.isSemiProcessed",
          semiUnitName: "$items.selectedUnitId.unitName",
          semiRaw: "$receipeDetails.rawItems.itemName",
          semiRawId: "$receipeDetails.rawItems._id",
          semiRawQty: "$receipeDetails.rawItems.baseQuantity",
          created: "$created",
          receipeDetails: "$items.receipeDetails",
          rawUnitName: "$receipeDetails.selectedUnitId.unitName",
          rawQuntityInBase: "$receipeDetails.quantity"
        }
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQtyinbase: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id._id",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",

        isSemi: "$_id.isSemi",
        semiRawMaterials: "$_id.semiRaw",
        semiRawMaterialsId: "$_id.semiRawId",
        semiRawMaterialsQty: "$_id.semiRawQty",
        semiUnitName: "$_id.semiUnitName",

        rawId: "$_id.rawId",
        rawName: "$_id.rawName",
        rawQuantity: "$_id.rawQuntityInBase",
        rawUnitName: "$_id.rawUnitName",

      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getConsumptionSummary_ItemReportFunction(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getStockSale_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getWastage_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getPhysical_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransfer_ItemConsumptionReport_MenuItem(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

exports.getConsumptionSummary_ItemReport = function (req, res) {

  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportResetTime/getConsumptionSummary_ItemReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var startDate = new Date(req.query.fromDate);
    //startDate.setHours(0, 0, 0, 0);
    var endDate = new Date(req.query.toDate);
    endDate.setDate(endDate.getDate() + 1);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }
    var minDate = new Date(startDate);
    var data = [];
    var databetween = [];
    var lastPrice = [];
    var returnData = [];

    getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
      if (result_op.length > 0) {
        _.forEach(result_op, function (op, i) {
          minDate = new Date(op.created);
          data.push(op);
        });
        getConsumptionSummary_ItemReportFunction(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result) {
          returnData = result;
          return res.json(returnData);
        }, function (err) {
          return handleError(res, err)
        });
      }
      else {
        return res.json(returnData);
      }
    }, function (err) {
      return handleError(res, err);
    });
  }
};

exports.getRecipeQtyInBase = function (req, res) {
  getRecipeQtyInBase(req.query.deployment_id, req.query.tenant_id).then(function (result) {
    return res.json(result);
  }, function (err) {
    return handleError(res, err)
  });
};

function getRecipeQtyInBase(deployment_id, tenant_id) {
  var d = Q.defer();

  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id,
        tenant_id: new ObjectId(tenant_id)
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id",
        itemName: "$recipeName",
        baseUnit: "$selectedUnitId.baseUnit.name",
        receipeQtyinBase: {$multiply: ["$selectedUnitId.conversionFactor", "$quantity"]}
      }
    }
  ], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};


function getStockEntryByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.vendor.category",
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          storeId: "$store._id",
          categoryId: "$category._id",
          created: "$created",
          billNo: "$billNo",
          name: "$items.itemName",
          totalQty: "$units.baseQty",
          UnitName: "$units.unitName"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalPurchaseQty: "$_id.totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.UnitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        created: "$_id.created"
      }
    }], function (err, result_entry) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_entry);
    }
  });
  return d.promise;
};

function getStockSaleByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.receiver,category",
        items: "$_store.receiver.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Sale_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "2",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.receiver.category"
    },
    {
      $unwind: "$_store.receiver.category.items"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems"
    },
    {
      $unwind: "$_store.receiver.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.receiver.category",
        menuItems: "$_store.receiver.category.items",
        items: "$_store.receiver.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.receiver.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          _id: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          conversionFactor: "$items.selectedUnitId.conversionFactor",
          itemType: "$items.itemType",
          UnitName: "$units.unitName",
          billNo: "$billNo",
          created: "$created",
          qty: "$units.baseQty"
        },
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        totalSaleQty: "$_id.qty",
        totalSaleAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        conversionFactor: "$_id.conversionFactor",
        itemType: "$_id.itemType",
        UnitName: "$_id.UnitName",
        created: "$_id.created"
      }
    }
  ], function (err, result_sale) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_sale);
    }
  });
  return d.promise;
};

function getPhysicalStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "3",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$itemId",
          name: "$itemName",
          storeId: "$storeId",
          categoryId: "$categoryId",
          unitName: "$UnitName",
          conversionFactor: "$ConversionFactor"
        },
        totalQty: {$last: "$totalBalanceQty"},
        date: {$last: "$created"},
        billNo: {$last: "$billNo"},
      }
    },
    {
      $project: {
        _id: 0,
        totalBalanceQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$date",
        billNo: "$billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_op);
    }
  });
  return d.promise;
};

function getWastageStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws)
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Wastage_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "4",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalWastageQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getTransferStockByItemFromStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getTransferStockByItemToStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "items.itemType": "RawMaterial",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$items.toStore._id",
          categoryId: "$items.toStore.toCategory._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        store: "$_store",
        category: "$_store.category",
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$store._id",
          categoryId: "$category._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_ClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)}
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.rawItems"
    },
    {
      $unwind: "$_store.category.items.rawItems.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.rawItems",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.rawItems.calculateInUnits"
      }
    },
    {
      $match: {
        "menuItems.itemType": {$in: ["MenuItem", "IntermediateItem"]},
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          storeId: "$menuItems.toStore._id",
          categoryId: "$menuItems.toStore.toCategory._id",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty_toStore: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        storeId: "$_id.storeId",
        categoryId: "$_id.categoryId",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    }
  ], function (err, result_ws) {
    if (err) {
      d.reject('error');
    }
    else {
      d.resolve(result_ws);
    }
  });
  return d.promise;
};

function getOpeningStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  //console.log(tenant_id,deployment_id);
  var beforeDate = new Date();
  var hour = new Date(startDate).getHours();
  var minutes = new Date(startDate).getMinutes();
  beforeDate.setHours(hour, minutes, 0, 0);
  beforeDate.setDate(beforeDate.getDate() + 1);
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "6",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$lte: new Date(beforeDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.category.items",
        created: "$timeStamp",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          date: "$created",
          billNo: "$billNo",
          qty: "$units.baseQty",
          unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$units.baseQty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalOpeningQty: "$totalQty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo"
      }
    },
    {
      $sort: {created: -1}
    }
  ], function (err, result_op) {
    if (err) {
      d.reject('error');
    }
    else {
      var checkDate;
      var data = [];
      _.forEach(result_op, function (itm, i) {
        if (i == 0) {
          checkDate = new Date(itm.created);
        }
        var compareDate = new Date(itm.created);
        if (checkDate.getTime() <= compareDate.getTime()) {
          data.push(itm);
        }
      });
      d.resolve(data);
    }
  });
  return d.promise;
};

function dataBetweenDate_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),
    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      _.forEach(val, function (v, ii) {
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function dataBeforeDate_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen) {
  var d = Q.defer();
  var data = [];
  var allPromise = Q.all([
    getPhysicalStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockEntryByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockSaleByItem_ConsumptionReport(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Sale(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_New(startDate,endDate,deployment_id,tenant_id,storeId),
    //getProcessedAndIntermediateReceipeDetails_Sale_BkMatchOldRecipe(startDate,endDate,deployment_id,tenant_id,storeId),

    getWastageStockByItem(startDate, endDate, deployment_id, tenant_id, storeId),
    //getProcessedAndIntermediateReceipeDetails_Wastage_New(startDate,endDate,deployment_id,tenant_id,storeId),
    getProcessedAndIntermediateReceipeDetails_Wastage_old(startDate, endDate, deployment_id, tenant_id, storeId),

    getTransferStockByItemFromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getTransferStockByItemToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId),
    getProcessedAndIntermediateReceipeDetails_Transfer_ToStore_New(startDate, endDate, deployment_id, tenant_id, storeId),

    getBillingData(startDate, endDate, deployment_id, tenant_id, isKitchen, storeId),
    getIntermediateReceipeDetails_IntermediateEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getIntermediateReceipeDetails_IntermediateEntry_BkPreviousItems(startDate, endDate, deployment_id, tenant_id, storeId),

    getStockReceive_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getStockReturn_RequirementEntry(startDate, endDate, deployment_id, tenant_id, storeId),

    getReceipeDetails_FinishedFoodEntry(startDate, endDate, deployment_id, tenant_id, storeId),
    getReceipeDetails_FinishedFoodEntry_PreviousBkItems(startDate, endDate, deployment_id, tenant_id, storeId)
  ]);
  allPromise.then(function (value) {
    _.forEach(value, function (val, i) {
      //console.log('befbetDate', val.length);
      _.forEach(val, function (v, ii) {
        if (v.itemId == "56e7d2a775b3e7281c96dd6e") {
          //console.log("Find At: " + i);
        }
        data.push(v);
      })
    });
    d.resolve(data);
  });
  return d.promise;
};

function getUniqueStore(data) {
  var str = [];
  _.forEach(data, function (dd, i) {
    var index = _.findIndex(str, {storeId: dd.storeId});
    if (index < 0) {
      str.push({storeId: dd.storeId, created: new Date(dd.created)});
    }
  });
  return str;
};

exports.getClosingQty = function (req, res) {
  var startDate = moment(new Date(req.query.fromDate)).toDate();
  //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();
  endDate.setDate(endDate.getDate() + 1);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;

  getClosingQty(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};

exports.getClosingQty_SemiProcessed = function (req, res) {
  var startDate = moment(new Date(req.query.fromDate)).toDate();
  //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();
  endDate.setDate(endDate.getDate() + 1);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;

  getClosingQty_SemiProcessed(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};

exports.getClosingQty_ProcessedFood = function (req, res) {
  var startDate = moment(new Date(req.query.fromDate)).toDate();
  //startDate.setHours(0, 0, 0, 0);
  var endDate = moment(new Date(req.query.toDate)).toDate();
  endDate.setDate(endDate.getDate() + 1);
  var tenant_id = req.query.tenant_id;
  var deployment_id = req.query.deployment_id;
  var storeId = req.query.storeId;

  getClosingQty_ProcessedFood(startDate, endDate, deployment_id, tenant_id).then(function (val) {
    return res.json(val);
  }, function (err) {
    return handleError(res, err)
  });
};

function getClosingQty(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  var minDate = moment(new Date(startDate)).toDate();
  var databetween = [];
  var lastPrice = [];

  var returnData = {beforeDate: [], betweenDate: []};
  getStores(deployment_id, tenant_id).then(function (stores) {

    async.eachSeries(stores, function (s, callback) {
      s.beforeDate = [];
      s.betweenDate = [];

      var storeId = s.storeId.toString();
      var isKitchen = s.isKitchen.toString();
      getOpeningStockByItem_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
        //getOpeningStockByItem(startDate,endDate,deployment_id,tenant_id,storeId).then(function(result_op){
        ///console.log("result_op : "+result_op);
        var data = [];
        if (result_op.length > 0) {
          _.forEach(result_op, function (op, i) {
            var tempDate = moment(new Date(op.created)).toDate();
            //console.log(new Date('8/18/2016'));
            //console.log(tempDate);
            tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
            // console.log('tempDate', tempDate);
            minDate = moment(new Date(tempDate)).toDate();
            data.push(op);
          });
          //console.log("minDate" + minDate);
          if (endDate.getTime() < minDate.getTime()) {
            s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
            return res.json(s);
          }
          else if (minDate.getTime() < startDate.getTime()) {
            //console.log("op < startDate");
            dataBeforeDate_ClosingQty(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
                //console.log(d);
              });
              dataBetweenDate_ClosingQty(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
          else {
            //console.log("op > startDate and op < end");
            dataBeforeDate_ClosingQty(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_ClosingQty(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
        }
        else {
          callback();
        }
      }, function (err) {
        console.log(err);
        return handleError(res, err);
      });

    }, function (err) {
      if (err) {
        d.reject('er');
      } else {
        d.resolve(stores);
      }
    });
  }, function (err) {
    d.reject('error')
  });
  return d.promise;
};

function getClosingQty_SemiProcessed(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  var minDate = moment(new Date(startDate)).toDate();
  var databetween = [];
  var lastPrice = [];

  var returnData = {beforeDate: [], betweenDate: []};
  getStores(deployment_id, tenant_id).then(function (stores) {

    async.eachSeries(stores, function (s, callback) {
      s.beforeDate = [];
      s.betweenDate = [];

      var storeId = s.storeId.toString();
      var isKitchen = s.isKitchen.toString();
      getOpeningStockByItem_Intermediate(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
        //console.log("result_op : "+result_op);
        var data = [];
        if (result_op.length > 0) {
          _.forEach(result_op, function (op, i) {
            var tempDate = moment(new Date(op.created)).toDate();
            //console.log(new Date('8/18/2016'));
            //console.log(tempDate);
            tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
            // console.log('tempDate', tempDate);
            minDate = moment(new Date(tempDate)).toDate();
            data.push(op);
          });
          //console.log("minDate" + minDate);
          if (endDate.getTime() < minDate.getTime()) {
            s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
            return res.json(s);
          }
          else if (minDate.getTime() < startDate.getTime()) {
            //console.log("op < startDate");
            dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_IntermediateReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
          else {
            //console.log("op > startDate and op < end");
            dataBeforeDate_IntermediateReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_IntermediateReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
        }
        else {
          callback();
        }
      }, function (err) {
        console.log(err);
        return handleError(res, err);
      });

    }, function (err) {
      if (err) {
        d.reject('er');
      } else {
        d.resolve(stores);
      }
    });
  }, function (err) {
    d.reject('error')
  });
  return d.promise;
};

function getClosingQty_ProcessedFood(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  var minDate = moment(new Date(startDate)).toDate();
  var databetween = [];
  var lastPrice = [];

  var returnData = {beforeDate: [], betweenDate: []};
  getStores(deployment_id, tenant_id).then(function (stores) {

    async.eachSeries(stores, function (s, callback) {
      s.beforeDate = [];
      s.betweenDate = [];

      var storeId = s.storeId.toString();
      var isKitchen = s.isKitchen.toString();
      getOpeningStockByItem_FinishedFood(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
        //console.log("result_op : "+result_op);
        var data = [];
        if (result_op.length > 0) {
          _.forEach(result_op, function (op, i) {
            var tempDate = moment(new Date(op.created)).toDate();
            //console.log(new Date('8/18/2016'));
            //console.log(tempDate);
            tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
            // console.log('tempDate', tempDate);
            minDate = moment(new Date(tempDate)).toDate();
            data.push(op);
          });
          //console.log("minDate" + minDate);
          if (endDate.getTime() < minDate.getTime()) {
            s.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
            return res.json(s);
          }
          else if (minDate.getTime() < startDate.getTime()) {
            //console.log("op < startDate");
            dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_FinishedFoodReport(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
          else {
            //console.log("op > startDate and op < end");
            dataBeforeDate_FinishedFoodReport(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
              s.beforeDate = value;
              _.forEach(data, function (d, i) {
                s.beforeDate.push(d);
              });
              dataBetweenDate_FinishedFoodReport(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
                s.betweenDate = value;
                callback();
              }, function (err) {
                return handleError(res, err)
              });
            }, function (err) {
              return handleError(res, err)
            });
          }
        }
        else {
          callback();
        }
      }, function (err) {
        console.log(err);
        return handleError(res, err);
      });

    }, function (err) {
      if (err) {
        d.reject('er');
      } else {
        d.resolve(stores);
      }
    });
  }, function (err) {
    d.reject('error')
  });
  return d.promise;
};

function getBillDataForGrowthReport(startDate, endDate, deployment_id) {

  //console.log('getBillDataForGrowthReport');
  //console.log(startDate);
  //console.log(endDate);
  var d = Q.defer();
  Stock.Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: new Date(startDate),
          $lte: new Date(endDate)
        },
        deployment_id: new ObjectId(deployment_id),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        //_id:{depid:"$deployment_id"},
        _id: "$_id",
        depId: "$deployment_id",
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _created: "$_created",
        tabType: "$tabType",
        billDiscountAmount: "$billDiscountAmount"
      }
    },
    {
      $group: {
        _id: {
          billId: "$_id",
          depId: "$deplId",
          created: "$_created",
          tabType: "$tabType",
          billDiscountAmount: "$billDiscountAmount"
        },
        billTotal: {$sum: "$kot.totalAmount"},
        billDiscount: {$sum: "$kot.totalDiscount"},
        billTaxAmount: {$sum: "$kot.taxAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        depId: "$_id.depId",
        billId: "$_id.billId",
        created: "$_id.created",
        billAmount: {$subtract: [{$subtract: ["$billTotal", "$billDiscount"]}, "$_id.billDiscountAmount"]},
        testAmount: "$billTotal",
        billDiscountAmount: {$add: ["$billDiscount", "$_id.billDiscountAmount"]},
        tabType: "$_id.tabType"
      }
    },
    {
      $group: {
        _id: "$tabType",
        total: {$sum: "$billAmount"},
        numBills: {$sum: 1},
        testAmount: {$sum: "$testAmount"},
        billDiscountAmount: {$sum: "$billDiscountAmount"}
      }
    }

  ], function (err, res) {
    if (err)
      d.reject(err);
    else
      d.resolve(res);
  });
  return d.promise;
}

function getWastageForGrowthReport(startDate, endDate, deployment_id) {

  var d = Q.defer();
  Q.all([
    getLastPriceOfItem_RawMaterial_GrowthReport(startDate, endDate, deployment_id),
    getLastPriceOfItem_Intermediate_GrowthReport(startDate, endDate, deployment_id)
  ]).then(function (result) {
    var lastPrices = result[0];
    var intermediateLastPrices = result[1];
    //console.log('last price', result[0]);
    Stock.StockTransaction.aggregate([
      {
        $match: {
          transactionType: "4",
          deployment_id: new ObjectId(deployment_id),
          created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        }
      },
      {
        $unwind: "$_store.category"
      },
      {
        $unwind: "$_store.category.items"
      },
      {
        $unwind: "$_store.category.items.calculateInUnits"
      },
      {
        $project: {
          items: "$_store.category.items",
          created: "$created",
          units: "$_store.category.items.calculateInUnits"
        }
      },
      {
        $match: {
          "units.type": "baseUnit"
        }
      },
      {
        $sort: {
          created: -1
        }
      },
      {
        $group: {
          _id: {
            itemId: "$items._id",
          },
          name: {$first: "$items.itemName"},
          qty: {$sum: "$units.baseQty"},
          itemType: {$first: "$items.itemType"},
          unitName: {$first: "$units.unitName"},
          conversionFactor: {$first: "$units.conversionFactor"},
          totalQty: {$sum: "$items.qty"}
        }
      },
      {
        $project: {
          _id: 0,
          totalWastageQty: "$qty",
          itemName: "$name",
          itemType: "$itemType",
          itemId: "$_id.itemId",
          UnitName: "$unitName",
          ConversionFactor: "$ conversionFactor",
        }
      }
    ], function (err, res) {
      if (err)
        d.reject(err);
      else {
        //console.log('res',res);
        var totalWastageCost = 0;
        _.forEach(res, function (item) {
          if (item.itemType == "RawMaterial") {
            var p = _.find(lastPrices, function (price) {
              return price.itemId.toString() == item.itemId.toString();
            });
            //console.log(p.lastPrice, item.totalWastageQty);
            totalWastageCost += p ? item.totalWastageQty * Number(p.lastPrice) : 0;
          }
          else if (item.itemType == "IntermediateItem") {
            var p = _.find(intermediateLastPrices, function (iPrice) {
              return iPrice.itemId == item.itemId;
            });
            //console.log(p.lastPrice, item.totalWastageQty);
            totalWastageCost += p ? item.totalWastageQty * Number(p.lastPrice) : 0;
          }
        });
        d.resolve({totalWastageCost: totalWastageCost});
      }
    });
  });
  return d.promise;
}

function getFoodCostForGrowthReport(startDate, endDate, deployment_id) {
  var d = Q.defer();
  //console.log('getFoodCostForGrowthReport');
  //console.log(startDate);
  //console.log(endDate);

  Q.all([
    getBilledItemsForGrowthReport2(startDate, endDate, deployment_id),
    getRecipetTotal(startDate, endDate, deployment_id)
  ]).then(function (result) {
    var menuItems = result[0];
    //console.log('menuItems', menuItems);
    var recipeItems = result[1];
    //console.log('recipeItem', recipeItems);
    var totalCost = 0;
    _.forEach(menuItems, function (item) {
      _.forEach(recipeItems, function (rItem) {
        //console.log(item.itemId.toString() == rItem.menuId.toString());
        if (item.itemId.toString() == rItem.menuId.toString()) {
          if (!rItem.itemQtyInBase)
            rItem.itemQtyInBase = 1;
          //console.log(item.itemName, rItem.itemName, item.totalQuantity, rItem.receipeQtyinBase, item.stockQuantity, rItem.itemQtyInBase, rItem.lastPrice);
          totalCost += Number(item.totalQuantity) / Number(rItem.receipeQtyinBase) * Number(item.stockQuantity) * Number(rItem.itemQtyInBase) * Number(rItem.lastPrice);
        }
      });
    });
    d.resolve({totalFoodCost: totalCost});
  }).catch(function (err) {
    d.reject(err);
  });
  return d.promise;
};

function getBilledItemsForGrowthReport(startDate, endDate, deployment_id) {
  var d = Q.defer();
  Stock.Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: new Date(startDate),
          $lte: new Date(endDate)
        },
        deployment_id: new ObjectId(deployment_id),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        //_id:{depid:"$deployment_id"},
        _id: "$_id",
        depId: "$deployment_id",
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _created: "$_created",
      }
    },
    {
      $unwind: "$kot.items"
    },
    {
      $group: {
        _id: {itemId: "$kot.items._id", unit: "$kot.items.unit", stockQuantity: "$kot.items.stockQuantity"},
        itemName: {$first: "$kot.items.name"},
        totalQuantity: {$sum: "$kot.items.quantity"},
        addOns: {$push: "$kot.items.addOns"}
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id.itemId",
        itemName: "$itemName",
        unit: "$_id.unit",
        stockQuantity: "$_id.stockQuantity",
        totalQuantity: {$multiply: ["$totalQuantity", "$_id.unit.conversionFactor"]},
        addOns: "$addOns"
      }
    }
  ], function (err, result) {
    if (err)
      d.reject(err);
    else {
      var addOnsDayWise = {};
      //console.log('items', result);
      _.forEach(result, function (item) {
        var allAddOns = [];
        if (_.has(item, 'addOns')) {
          _.forEach(item.addOns, function (addOns, i) {

            _.forEach(addOns, function (addOn) {

              addOn.itemId = addOn._id;
              addOn.unit = addOn.unit || null;
              addOn.stockQuantity = addOn.stockQuantity || null;
              addOn.totalQuantity = 0;
              addOn.totalQuantity += addOn.quantity ? addOn.quantity : 0;
              allAddOns.push(addOn);
            });
            //var addOnGrp = _.groupBy(addOns, function (addOn){
            //  return addOn._id;
            //});
            //_.forEach(addOnGrp, function (adds){
            //  var ad = {
            //    totalQuantity: 0
            //  }
            //  _.forEach(adds, function (addOn){
            //    ad.itemId = addOn._id;
            //    ad.unit = addOn.unit;
            //    ad.stockQuantity = addOn.stockQuantity;
            //    ad.totalQuantity += addOn.quantity ? addOn.quantity : 0;
            //  });
            //  result.push(ad);
            //});
          });
        }
        if (allAddOns.length > 0) {
          var grpAddOns = _.groupBy(allAddOns, function (addOn) {
            return addOn._id;
          });
          _.forEach(grpAddOns, function (addOns) {
            var ad = {
              totalQuantity: 0,
              unit: null,
              stockQuantity: null
            };
            _.forEach(addOns, function (addOn) {
              ad.itemName = addOn.name;
              ad.itemId = addOn._id;
              if (addOn.unit && !ad.unit) {
                ad.unit = addOn.unit;
              }
              if (addOn.stockQuantity && !ad.stockQuantity)
                ad.stockQuantity = addOn.stockQuantity;
              ad.totalQuantity += addOn.quantity ? (addOn.quantity) : 0;
            });
            var cf = 1;
            if (ad.unit)
              cf = ad.unit.conversionFactor;
            ad.totalQuantity = ad.totalQuantity * cf;
            result.push(ad);
          });
        }
      });

      //console.log('dayAddOns', addOnsDayWise);
      //console.log('result', result);
      d.resolve(result);
    }
  });
  return d.promise;
}

function getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New(startDate, endDate, deployment_id, tenant_id, storeId) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "5",
        tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {$gte: new Date(startDate), $lt: new Date(endDate)},
        "_store._id": storeId
      }
    },
    {
      $unwind: "$_store.category"
    },
    {
      $unwind: "$_store.category.items"
    },
    {
      $unwind: "$_store.category.items.receipeDetails"
    },
    {
      $unwind: "$_store.category.items.receipeDetails.calculateInUnits"
    },
    {
      $project: {
        menuItems: "$_store.category.items",
        items: "$_store.category.items.receipeDetails",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.category.items.receipeDetails.calculateInUnits"
      }
    },
    {
      $match: {
        //"menuItems.itemType":{$in:["MenuItem","IntermediateItem"]},
        "menuItems.itemType": "MenuItem",
        "units.type": "baseUnit"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id", name: "$items.itemName", menuName: "$menuItems.itemName", menuId: "$menuItems._id",
          date: "$created", billNo: "$billNo", qty: "$units.baseQty", unitName: "$units.unitName",
          conversionFactor: "$units.conversionFactor"
        },
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        _id: 0,
        totalTransferQty: "$_id.qty",
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        UnitName: "$_id.unitName",
        ConversionFactor: "$_id.conversionFactor",
        created: "$_id.date",
        billNo: "$_id.billNo",
        menuName: "$_id.menuName",
        menuId: "$_id.menuId"
      }
    }
  ]).allowDiskUse(true).exec(function (err, result) {
    if (err) {
      console.log('error in getProcessedAndIntermediateReceipeDetails_Transfer_FromStore_New');

      console.log(err);
      d.reject('error');
    }
    d.resolve(result);

  });
  return d.promise;
};

function getBilledItemsForGrowthReport2(startDate, endDate, deployment_id) {
  var d = Q.defer();
  Stock.Bill.aggregate([
    {
      $match: {
        _created: {
          $gte: new Date(startDate),
          $lte: new Date(endDate)
        },
        deployment_id: new ObjectId(deployment_id),
        isDeleted: {$ne: true},
        isVoid: {$ne: true}
      }
    },
    {
      $unwind: "$_kots"
    },
    {
      $project: {
        //_id:{depid:"$deployment_id"},
        _id: "$_id",
        depId: "$deployment_id",
        kot: {$cond: ['$_kots.isVoid', {}, "$_kots"]},
        _created: "$_created",
      }
    },
    {
      $unwind: "$kot.items"
    },
    {
      $unwind: {path: "$kot.items.addOns", preserveNullAndEmptyArrays: true}
    },
    {
      $group: {
        _id: {
          _id: "$_id",
          kotNumber: "$kot.kotNumber",
          itemId: "$kot.items._id",
          itemName: "$kot.items.name",
          unit: "$kot.items.unit"
          ,
          stockQuantity: "$kot.items.stockQuantity",
          quantity: "$kot.items.quantity",
          mapComboItems: "$kot.items.mapComboItems",
          isCombo: "$kot.items.category.isCombos"
        },
        addOns: {
          $push: {
            _id: "$kot.items.addOns._id", name: "$kot.items.addOns.name", unit: "$kot.items.addOns.unit",
            stockQuantity: "$kot.items.addOns.stockQuantity", quantity: "$kot.items.addOns.quantity"
          }
        }
      }
    },
    {
      $project: {
        "kot.items._id": "$_id.itemId",
        "kot.items.unit": "$_id.unit",
        "kot.items.stockQuantity": "$_id.stockQuantity",
        "kot.items.name": "$_id.itemName",
        "kot.items.category.isCombos": "$_id.isCombo",
        "kot.items.quantity": "$_id.quantity",
        "kot.items.addOns": "$addOns",
        "kot.items.mapComboItems": "$_id.mapComboItems"
      }
    },
    {
      $unwind: {path: "$kot.items.mapComboItems", preserveNullAndEmptyArrays: true}
    },
    {
      $group: {
        _id: {
          id: "$_id",
          kotNumber: "$kot.kotNumber",
          itemId: "$kot.items._id",
          itemName: "$kot.items.name",
          unit: "$kot.items.unit"
          ,
          stockQuantity: "$kot.items.stockQuantity",
          quantity: "$kot.items.quantity",
          addOns: "$kot.items.addOns",
          isCombos: "$kot.items.category.isCombos"
        },
        mapComboItems: {
          $push: {
            _id: "$kot.items.mapComboItems._id",
            name: "$kot.items.mapComboItems.name",
            unit: "$kot.items.mapComboItems.unit",
            stockQuantity: "$kot.items.mapComboItems.stockQuantity",
            quantity: "$kot.items.mapComboItems.quantity"
          }
        }
      }
    },
    {
      $project: {
        "kot.items._id": "$_id.itemId",
        "kot.items.unit": "$_id.unit",
        "kot.items.stockQuantity": "$_id.stockQuantity",
        "kot.items.name": "$_id.itemName",
        "kot.items.category.isCombos": "$_id.isCombos",
        "kot.items.quantity": "$_id.quantity",
        "kot.items.addOns": "$_id.addOns",
        "kot.items.mapComboItems": "$mapComboItems"
      }
    },
    {
      $group: {
        _id: {
          itemId: "$kot.items._id",
          unit: "$kot.items.unit",
          stockQuantity: "$kot.items.stockQuantity",
          isCombos: "$kot.items.category.isCombos",
          mapComboItems: "$kot.items.mapComboItems"
        },
        itemName: {$first: "$kot.items.name"},
        totalQuantity: {$sum: "$kot.items.quantity"},
        addOns: {$push: "$kot.items.addOns"}
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$_id.itemId",
        itemName: "$itemName",
        isCombos: "$_id.isCombos",
        unit: "$_id.unit",
        stockQuantity: "$_id.stockQuantity",
        //totalQuantity: {$multiply: ["$totalQuantity", "$_id.unit.conversionFactor"]},
        totalQuantity: "$totalQuantity",
        addOns: "$addOns",
        mapComboItems: "$_id.mapComboItems"
      }
    }
  ], function (err, result) {
    if (err)
      d.reject(err);
    else {
      var addOnsDayWise = {};
      //console.log('items', result);
      var tempResult = [];
      _.forEach(result, function (item) {
        var isCombo = false;
        var allAddOns = [];
        if (_.has(item, 'addOns')) {
          _.forEach(item.addOns, function (addOns, i) {

            _.forEach(addOns, function (addOn) {
              //console.log(addOn);
              if (_.has(addOn, '_id')) {
                if (addOn.unit)
                  tempResult.push({itemId: addOn._id, itemName: addOn.name, stockQuantity: addOn.stockQuantity, totalQuantity: addOn.quantity, unit: addOn.unit});
                //allAddOns.push(addOn);
              }
            });
          });
        }
        //if(allAddOns.length > 0) {
        //  var grpAddOns =  _.groupBy(allAddOns, function (addOn){
        //    return addOn._id;
        //  });
        //  _.forEach(grpAddOns, function(addOns){
        //    var ad = {
        //      totalQuantity: 0,
        //      unit: null,
        //      stockQuantity: null
        //    };
        //    _.forEach(addOns, function (addOn){
        //      ad.itemName = addOn.name;
        //      ad.itemId = addOn._id;
        //      if(addOn.unit && !ad.unit) {
        //        ad.unit = addOn.unit;
        //      }
        //      if(addOn.stockQuantity && !ad.stockQuantity)
        //        ad.stockQuantity = addOn.stockQuantity;
        //      ad.totalQuantity += addOn.quantity ? (addOn.quantity) : 0;
        //    });
        //    var cf = 1;
        //    if(ad.unit)
        //      cf = ad.unit.conversionFactor;
        //    ad.totalQuantity = ad.totalQuantity * cf;
        //    result.push(ad);
        //  });
        //}
        if (item.isCombos) {
          //console.log('totq', item.totalQuantity);
          isCombo = true;
          if (item.mapComboItems.length > 0) {
            _.forEach(item.mapComboItems, function (combo) {
              //console.log(combo.name, combo.quantity);
              tempResult.push({itemId: combo._id, itemName: combo.name, totalQuantity: item.totalQuantity, unit: combo.unit, stockQuantity: combo.stockQuantity});
            });
          }
        }
        if (!isCombo) {
          tempResult.push(item);
        }
      });

      //console.log('result', tempResult);

      var grpByItemId = _.groupBy(tempResult, function (tmpIt) {
        return tmpIt.itemId;
      });

      var finalResult = [];

      _.forEach(grpByItemId, function (gpIts, i) {
        var it = {
          totalQuantity: 0,
          unit: null,
          stockQuantity: 0
        };
        _.forEach(gpIts, function (item) {
          it.itemName = item.itemName;
          it.itemId = item.itemId;
          if (item.unit && !it.unit) {
            it.unit = item.unit;
          }
          if (item.stockQuantity && !it.stockQuantity)
            it.stockQuantity = item.stockQuantity;
          it.totalQuantity += item.totalQuantity? item.totalQuantity: 0;
        });
        var cf = 1;
        if(it.unit) {
          cf = it.unit.conversionFactor;
        }
        it.totalQuantity = it.totalQuantity * cf;
        finalResult.push(it);
      });

      //console.log('dayAddOns', addOnsDayWise);
      //console.log('result', finalResult);
      d.resolve(finalResult);
    }
  });
  return d.promise;
}

function getRecipes(deployment_id) {
  var d = Q.defer();
  Stock.StockRecipe.aggregate([
    {
      $match: {
        deployment_id: deployment_id
      },
    },
    {
      $project: {
        _id: 0,
        rawItems: "$receipeDetails",
        menuId: "$itemId",
        recipeId: "$_id",
        recipeName: "$recipeName",
        isSemiProcessed: "$isSemiProcessed",
        receipeQtyinBase: {$multiply: ["$quantity", "$selectedUnitId.conversionFactor"]}
      }
    },
    {
      $unwind: "$rawItems"
    },
    {
      $project: {
        _id: 0,
        Qty: "$Qty",
        stockItemName: "$rawItems.itemName",
        stockItemId: "$rawItems._id",
        intermediateId: "$rawItems.itemId",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        isSemiProcessed: "$rawItems.isSemiProcessed",
        receipeQtyinBase: "$receipeQtyinBase",
        UnitName: "$rawItems.selectedUnitName",
        itemQtyInBase: "$rawItems.baseQuantity",
        itemAmountInBase: "$rawItems.calculatedAveragePrice",
        isBaseKitchenItem: "$rawItems.isBaseKitchenItem",
        baseUnit: "$rawItems.selectedUnitId.baseUnit.name",
        baseConversionFactor: "$rawItems.selectedUnitId.conversionFactor",
        preferedConversionFactor: "$rawItems.preferedUnit.conversionFactor"
      }
    },
    {
      $project: {
        _id: 0,
        itemId: "$stockItemId",
        stockItemName: "$stockItemName",
        intermediateId: "$intermediateId",
        itemName: "$stockItemName",
        menuId: "$menuId",
        recipeId: "$recipeId",
        recipeName: "$recipeName",
        isSemiProcessed: "$isSemiProcessed",
        baseUnit: "$baseUnit",
        isBaseKitchenItem: "$isBaseKitchenItem",
        itemAmountInBase: "$rawItems.calculatedAveragePrice",
        itemQtyInBase: "$itemQtyInBase",
        receipeQtyinBase: "$receipeQtyinBase",
        UnitName: "$UnitName",
        preferedConversionFactor: "$preferedConversionFactor",
        baseConversionFactor: "$baseConversionFactor"
      }
    }
  ], function (err, result) {
    if (err)
      d.reject(err);
    else {
      //console.log(result);
      //console.log('recipe', result);
      d.resolve(result);
    }
  });
  return d.promise;
}

function getRecipetTotal(startDate, endDate, deployment_id) {
  var d = Q.defer();
  Q.all([
    getRecipes(deployment_id),
    getLastPriceOfItem_RawMaterial_GrowthReport(startDate, endDate, deployment_id),
    getLastPriceOfItem_Intermediate_GrowthReport(startDate, endDate, deployment_id)
  ]).then(function (result) {
    var lastPrices = result[1];
    var recipeItems = result[0];
    //console.log('recipeItems', recipeItems);
    var intermediateLastPrices = result[2];
    //console.log('irPrices', intermediateLastPrices);
    var recipeItemsWithPrice = [];
    _.forEach(recipeItems, function (item) {
      if (!item.isSemiProcessed) {
        var lp = _.find(lastPrices, function (price) {
          return item.itemId.toString() == price.itemId.toString();
        });
        if (!lp)
          item.lastPrice = 0;
        else
          item.lastPrice = lp.lastPrice;
      } else {

        var lp = _.find(intermediateLastPrices, function (iPrice) {
          //console.log('iPrice', iPrice);
          return item.itemId.toString() == iPrice.recipeId.toString();
        });
        //console.log('lp', lp);
        //console.log(item.itemName, item.itemId, lp);
        if (!lp) {
          item.lastPrice = 0;
        }
        else {
          //console.log('priceCheck', item.itemName, lp.lastPrice);
          item.lastPrice = lp.lastPrice;
        }
      }
    });
    //console.log('lp', recipeItems);
    d.resolve(recipeItems);
  }).catch(function (err) {
    d.reject(err)
  });
  return d.promise;
}

exports.getGrowthReport = function (req, res) {

  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportResetTime/getGrowthReport?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    var startDate = new Date(req.query.startDate);
    var endDate = new Date(req.query.endDate);
    var midDate = new Date(endDate);
    midDate = moment(midDate).subtract('days', 7).toDate();
    //var week2StartDate = new Date(moment(req.query.endDate).subtract(7, 'days'));
    var deployment_id = req.query.deployment_id;

    getGrowthData(startDate, midDate, midDate, endDate, deployment_id).then(function (result) {
      return res.status(200).json(result);
    }, function (err) {
      console.log(err);
      return res.status(500).send(err);
    });
  }
};

function getLastPriceOfItem_RawMaterial_GrowthReport(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  Stock.StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        //tenant_id: new ObjectId(tenant_id),
        deployment_id: new ObjectId(deployment_id),
        created: {
          $lte: new Date(endDate)
        }
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $unwind: "$_store.vendor.category.items.calculateInUnits"
    },
    {
      $project: {
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber",
        units: "$_store.vendor.category.items.calculateInUnits"
      }
    },
    {
      $match: {
        "units.type": "baseUnit"
      }
    },
    {
      $sort: {
        created: -1
      }
    },
    {
      $group: {
        _id: {
          itemId: "$items._id",
          name: "$items.itemName",
          conversionFactor: "$units.conversionFactor",
          UnitName: "$units.unitName",
        },
        lastPrice: {$first: "$units.basePrice"},
        totalAmt: {$sum: "$units.totalAmount"}
      }
    },
    {
      $project: {
        _id: 0,
        itemName: "$_id.name",
        itemId: "$_id.itemId",
        lastPrice: "$lastPrice",
        totalPurchaseAmount: "$totalAmt",
        UnitName: "$_id.UnitName",
        conversionFactor: "$_id.conversionFactor"
      }
    }

  ], function (err, result_entry) {
    if (err) {
      d.reject(err);
    }
    else {
      _.forEach(result_entry, function (r, i) {
        var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
        r.lastPrice = lp;
      });
      Stock.StockRequirement.aggregate([
        {
          $match: {
            deployment_id: deployment_id,
            created: {$lte: new Date(endDate)},
            ReceiveDate: {$ne: null}
          }
        },
        {
          $unwind: "$store.vendor.category"
        },
        {
          $unwind: "$store.vendor.category.items"
        },
        {
          $unwind: "$store.vendor.category.items.calculateInUnits"
        },
        {
          $project: {
            items: "$store.vendor.category.items",
            created: "$created",
            billNo: "$daySerialNumber",
            units: "$store.vendor.category.items.calculateInUnits"
          }
        },
        {
          $match: {
            "units.type": "baseUnit"
          }
        },
        {
          $sort: {
            created: -1
          }
        },
        {
          $group: {
            _id: {
              itemId: "$items._id",
              name: "$items.itemName",
              conversionFactor: "$units.conversionFactor",
              UnitName: "$units.unitName"
            },
            lastPrice: {$first: "$units.basePrice"},
            totalAmt: {$sum: "$units.totalAmount"}
          }
        },
        {
          $project: {
            _id: 0,
            itemName: "$_id.name",
            itemId: "$_id.itemId",
            lastPrice: "$lastPrice",
            totalPurchaseAmount: "$totalAmt",
            UnitName: "$_id.UnitName",
            conversionFactor: "$_id.conversionFactor"
          }
        }
      ], function (err, result_requirement) {
        //console.log('reqmts', result_requirement);
        if (err) {
          d.reject(err);
          console.log(err);
          //console.log('3749', err);
        }
        else {
          //console.log('3752', result_requirement);
          _.forEach(result_requirement, function (r, i) {
            var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
            r.lastPrice = lp;
            result_entry.push(r);
          });
          d.resolve(result_entry);
          //console.log(result_entry);
        }
      });
    }
  });
  return d.promise;
};

function getLastPriceOfItem_Intermediate_GrowthReport(startDate, endDate, deployment_id, tenant_id) {
  var d = Q.defer();
  //Stock.StockTransaction.aggregate([
  //  {
  //    $match: {
  //      transactionType: "7",
  //      //tenant_id: new ObjectId(tenant_id),
  //      deployment_id: new ObjectId(deployment_id),
  //      created: {$lte: new Date(endDate)}
  //    }
  //  },
  //  {
  //    $unwind: "$_store.category"
  //  },
  //  {
  //    $unwind: "$_store.category.items"
  //  },
  //  {
  //    $unwind: "$_store.category.items.calculateInUnits"
  //  },
  //  {
  //    $project: {
  //      items: "$_store.category.items",
  //      created: "$created",
  //      billNo: "$daySerialNumber",
  //      units: "$_store.category.items.calculateInUnits"
  //    }
  //  },
  //  {
  //    $match: {
  //      "units.type": "baseUnit"
  //    }
  //  },
  //  {
  //    $group: {
  //      _id: {
  //        recipeId: "$items._id",
  //        name: "$items.itemName",
  //        totalQty: "$units.baseQty",
  //        conversionFactor: "$units.conversionFactor",
  //        lastPrice: "$units.basePrice",
  //        UnitName: "$units.unitName"
  //      },
  //      totalAmt: {$sum: "$units.totalAmount"}
  //    }
  //  },
  //  {
  //
  //    $project: {
  //      _id: 0,
  //      itemName: "$_id.name",
  //      recipeId: "$_id.recipeId",
  //      lastPrice: "$_id.lastPrice",
  //      totalPurchaseAmount: "$totalAmt",
  //      totalPurchaseQty: {$multiply: ["$_id.totalQty", "$_id.conversionFactor"]},
  //      UnitName: "$_id.UnitName",
  //      conversionFactor: "$_id.conversionFactor"
  //    }
  //  }
  //], function (err, result_entry) {
  //  if (err) {
  //    d.reject('error');
  //  }
  //  else {
  //    console.log('intermediate', result_entry);
  //    _.forEach(result_entry, function (r, i) {
  //      var lp = parseFloat(r.lastPrice) / parseFloat(r.conversionFactor);
  //      r.lastPrice = lp;
  //    });
  //    d.resolve(result_entry);
  //  }
  //});
  getLastPriceOfItem_RawMaterial_GrowthReport(startDate, endDate, deployment_id).then(function (prices) {
    Stock.StockRecipe.find({isSemiProcessed: true}, {receipeDetails: 1, isSemiProcessed: 1}, function (err, result) {
      if (err)
        d.reject(err);
      else {
        var r = [];
        _.forEach(result, function (res) {
          var t = new Object(res);
          t.recipeId = res._id;
          t.lastPrice = 0;
          _.forEach(res.receipeDetails, function (detail) {
            var lp = _.find(prices, function (price) {
              return price.itemId.toString() == detail._id.toString();
            });
            if (lp)
              t.lastPrice += parseFloat(lp.lastPrice) / parseFloat(lp.conversionFactor);
          });
          r.push(t);
        });
        d.resolve(r);
      }
    });
  });
  return d.promise;
};

function getGrowthData(startDateWeek1, endDateWeek1, startDateWeek2, endDateWeek2, deployment_id) {

  //console.log(startDateWeek1, endDateWeek1, startDateWeek2, endDateWeek2, deployment_id);
  var d = Q.defer();
  Q.all([
    getBillDataForGrowthReport(startDateWeek1, endDateWeek1, deployment_id),
    getFoodCostForGrowthReport(startDateWeek1, endDateWeek1, deployment_id),
    getWastageForGrowthReport(startDateWeek1, endDateWeek1, deployment_id),
    getBillDataForGrowthReport(startDateWeek2, endDateWeek2, deployment_id),
    getFoodCostForGrowthReport(startDateWeek2, endDateWeek2, deployment_id),
    getWastageForGrowthReport(startDateWeek2, endDateWeek2, deployment_id)
  ]).then(function (result) {
    //console.log(result);
    d.resolve({
      lastWeek: {billingData: result[0], foodCost: result[1], wastageCost: result[2]},
      thisWeek: {billingData: result[3], foodCost: result[4], wastageCost: result[5]}
    });
  }, function (err) {
    d.reject(err);
  });
  return d.promise;
}

//replaced

//============================================Mailer Code=================================================

function getStockItems(tenant_id, deployment_id) {
  var deferred = Q.defer();
  Stock.StockItem.find({tenant_id: tenant_id, deployment_id: deployment_id}, function (err, stockItems) {
    if (err) {
      deferred.rejecT(err);
    }
    else
      deferred.resolve(stockItems);
  });
  return deferred.promise;
}

function getBaseKitchenItems(tenant_id, deployment_id) {
  var deferred = Q.defer();
  Stock.BaseKitchenItem.find({tenant_id: tenant_id, deployment_id: deployment_id}, function (err, BaseKitchenItems) {
    if(err) { deferred.reject(err); }
    else
     deferred.resolve(BaseKitchenItems)
  });
  return deferred.promise;
}

function spliceAllItemsBeforePhysical(result, deployment, endDate){
  var data=[];
  _.forEach(result.beforeDate, function(r,i){
    if(r.totalBalanceQty!=undefined){
      var d = new Date(r.created);
      var a = new Date(d);
      a.setHours(0,0,0,0);
      a = new Date(a);
      var b = new Date(d);
      b = new Date(getDateFormatted(getResetDate(deployment.settings, convertDate(b))));
      if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

      }else {
        //console.log('splice all', d);
        d.setDate(d.getDate() + 1);
      }
      d = new Date(getDateFormatted(getResetDate(deployment.settings, convertDate(d))));
      //d.setHours(0, 0, 0, 0);
      r.created=d;
      data.push(r);
    };
  });
  _.forEach(data,function(d,i){
    _.forEachRight(result.beforeDate,function(itm,ii){
      if(itm.itemId==d.itemId){
        var phyDate=new Date(d.created);
        var compareDate=new Date(itm.created);
        if(phyDate.getTime()>compareDate.getTime()){
          result.beforeDate.splice(ii,1);
        }
      }
    });
  });

  // var data2=[];
  _.forEachRight(result.betweenDate,function(r,i){
    if(r.totalBalanceQty!=undefined){
      //console.log(r.created);
      var d=new Date(r.created);
      var cdate=new Date(endDate);
      var a = new Date(d);
      a.setHours(0,0,0,0);
      a = new Date(a);
      var b = new Date(d);
      b = new Date(getDateFormatted(getResetDate(deployment.settings, convertDate(b))));
      // console.log(a)
      // console.log(b)
      // console.log(angular.copy(d))
      if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

      }else {
        //console.log('splice all', d);
        d.setDate(d.getDate() + 1);
      }
      //d.setDate(d.getDate() + 1);
      d = new Date(getDateFormatted(getResetDate(deployment.settings, convertDate(d))));
      r.created=d;
      //console.log(r.created);
      // if(cdate.getTime()>d.getTime()){
      //   data2.push(r);
      // }
      // else
      // {
      //   result.betweenDate.splice(i,1);
      // }
    };
  });
  // _.forEach(data2,function(d,i){
  //   _.forEachRight(result.betweenDate,function(itm,ii){
  //     if(itm.itemId==d.itemId){
  //       if(itm.totalBalanceQty!=undefined){
  //           var phyDate=new Date(d.created);
  //           var compareDate=new Date(itm.created);
  //           if(phyDate.getTime()>compareDate.getTime()){
  //             result.betweenDate.splice(ii,1);
  //           }
  //         }
  //     }
  //   });
  //});

  return result;
}

function calculatePrice(result, deployment){
  //console.log('calculating price');
  _.forEach(result.beforeDate,function(itm,i){
    var index=_.findIndex($scope.lastPrice, function (price) {
      return price.itemId.toString() == itm.itemId;
    });
    //if(index>=0){
    if(itm.totalOpeningQty!=undefined){
      if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalOpeningAmt=0.00;   
        }
        else
        {
          itm.totalOpeningAmt=0.00;
        }
      }
    }
    else if(itm.totalBalanceQty!=undefined){
      if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalBalanceAmt=0.00;
        }
        else
        {
          itm.totalBalanceAmt=0.00;
        }
      }
    }
    else if(itm.totalPurchaseQty!=undefined){
      if(itm.totalPurchaseAmount==undefined){
        itm.totalPurchaseAmount=0.00;
      }
    }
    else if(itm.totalSaleQty!=undefined){
      if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalSaleAmount=0.00;
        }
        else
        {
          itm.totalSaleAmount=0.00;
        }
      }
    }
    else if(itm.totalWastageQty!=undefined){
      if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalWastageAmt = 0.00;
        }
        else
        {
          itm.totalWastageAmt=0.00;
        }
      }
    }
    else if(itm.totalTransferQty!=undefined){
      if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalTransferAmount = 0.00;
        }
        else
        {
          itm.totalTransferAmount=0.00;
        }
      }
    }
    else if(itm.totalTransferQty_toStore!=undefined){
      if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalTransferAmount_toStore = 0.00;
        }
        else
        {
          itm.totalTransferAmount_toStore=0.00;
        }
      }
    }
    else if(itm.itemSaleQty!=undefined){
      if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.itemSaleAmt = 0.00;
        }
        else
        {
          itm.itemSaleAmt=0.00;
        }
      }
    }
    else if(itm.totalIntermediateQty!=undefined){
      if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalIntermediateAmt = 0.00;
        }
        else
        {
          itm.totalIntermediateAmt=0.00;
        }
      }
    }
    //}
  });
  _.forEach(result.betweenDate,function(itm,i){
    var index=_.findIndex($scope.lastPrice, function (price) {
      return itm.itemId.toString() == price.itemId.toString();
    });
    //if(index>=0){
    if(itm.totalOpeningQty!=undefined){
      if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalOpeningAmt = 0.00;
        }
        else
        {
          itm.totalOpeningAmt=0.00;
        }
      }
    }
    else if(itm.totalBalanceQty!=undefined){
      if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalBalanceAmt = 0.00;
        }
        else
        {
          itm.totalBalanceAmt=0.00;
        }
      }
    }
    else if(itm.totalPurchaseQty!=undefined){
      if(itm.totalPurchaseAmount==undefined){
        itm.totalPurchaseAmount=0.00;
      }
    }
    else if(itm.totalSaleQty!=undefined){
      if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalSaleAmount = 0.00;
        }
        else
        {
          itm.totalSaleAmount=0.00;
        }
      }
    }
    else if(itm.totalWastageQty!=undefined){
      if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalWastageAmt = 0.00;
        }
        else
        {
          itm.totalWastageAmt=0.00;
        }
      }
    }
    else if(itm.totalTransferQty!=undefined){
      if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalTransferAmount = 0.00;
        }
        else
        {
          itm.totalTransferAmount=0.00;
        }
      }
    }
    else if(itm.totalTransferQty_toStore!=undefined){
      if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalTransferAmount_toStore = 0.00;
        }
        else
        {
          itm.totalTransferAmount_toStore=0.00;
        }
      }
    }
    else if(itm.itemSaleQty!=undefined){
    //  console.log('price', itm.itemName, itm.itemId, index);
      if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.itemSaleAmt = 0.00;
          //console.log(itm, $scope.lastPrice[index]);
        }
        else
        {
          itm.itemSaleAmt=0.00;
        }
      }
    }
    else if(itm.totalStockReturnQty != undefined) {
      //console.log('price', itm.itemName, itm.itemId, index);
      if(itm.totalStockReturnQty==undefined || itm.totalStockReturnQty==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalStockReturnAmount=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalStockReturnAmount = 0.00;
          //console.log(itm, $scope.lastPrice[index]);
        }
        else
        {
          itm.totalStockReturnAmount=0.00;
        }
      }
    }
    else if(itm.totalIntermediateQty!=undefined){
      if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
        if(index>=0){
          if(!isNaN($scope.lastPrice[index].lastPrice))
            itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
          else
            itm.totalIntermediateAmt = 0.00;
        }
        else
        {
          itm.totalIntermediateAmt=0.00;
        }
      }
    }
    //}
  });
  return result;
};

function formatDataForReport(result,startDate,endDate, deployment){
    //console.log('result', result);
    result.items = [];
    var endDateToShow = new Date(endDate);
    endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
    _.forEach(result.beforeDate,function(itm,i){
      if(itm.totalOpeningQty!=undefined){
        if(isNaN(itm.totalOpeningQty))
          itm.totalOpeningQty = 0;
        if(isNaN(itm.totalOpeningAmt))
          itm.totalOpeningAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
          itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
        }
      }
      else if(itm.totalBalanceQty!=undefined){
        if(isNaN(itm.totalBalanceQty))
          itm.totalBalanceQty = 0;
        if(isNaN(itm.totalBalanceAmt))
          itm.totalBalanceAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
          itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          //console.log('total balance', itm.totalBalanceQty);
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
        }
      }
      else if(itm.totalPurchaseQty!=undefined){
        if(isNaN(itm.totalPurchaseQty))
          itm.totalPurchaseQty = 0;
        if(isNaN(itm.totalPurchaseAmount))
          itm.totalPurchaseAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
          itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
        }
      }
      else if(itm.totalIntermediateQty!=undefined){
        if(isNaN(itm.totalIntermediateQty))
          itm.totalIntermediateQty = 0;
        if(isNaN(itm.totalIntermediateAmt))
          itm.totalIntermediateAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
          itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
        }
      }
      else if(itm.totalSaleQty!=undefined){
        if(isNaN(itm.totalSaleQty))
          itm.totalSaleQty = 0;
        if(isNaN(itm.totalSaleAmount))
          itm.totalSaleAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
          itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
        }
      }
      else if(itm.totalWastageQty!=undefined){
        if(isNaN(itm.totalWastageQty))
          itm.totalWastageQty = 0;
        if(isNaN(itm.totalWastageAmt))
          itm.totalWastageAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
          itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
        }
      }
      else if(itm.totalTransferQty!=undefined){
        if(isNaN(itm.totalTransferQty))
          itm.totalTransferQty = 0;
        if(isNaN(itm.totalTransferAmount))
          itm.totalTransferAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
          itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
        }
      }
      else if(itm.totalTransferQty_toStore!=undefined){
        if(isNaN(itm.totalTransferQty_toStore))
          itm.totalTransferQty_toStore = 0;
        if(isNaN(itm.totalTransferAmount_toStore))
          itm.totalTransferAmount_toStore = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
          itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
        }
      }
      else if(itm.itemSaleQty!=undefined){
        if(isNaN(itm.itemSaleQty))
          itm.itemSaleQty = 0;
        if(isNaN(itm.itemSaleAmt))
          itm.itemSaleAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
          itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          itm.UnitName=itm.baseUnit;
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
        }
      }
      else if(itm.totalStockReturnQty!=undefined){
        if(isNaN(itm.totalStockReturnQty))
          itm.totalStockReturnQty = 0;
        if(isNaN(itm.totalStockReturnAmount))
          itm.totalStockReturnAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          itm.openingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
          itm.openingAmt=parseFloat(-itm.totalStockReturnAmount).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
        }
      }
      else if(itm.totalStockReceiveQty!=undefined){
        if(isNaN(itm.totalStockReceiveQty))
          itm.totalStockReceiveQty = 0;
        if(isNaN(itm.totalStockReceiveAmount))
          itm.totalStockReceiveAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        //console.log(itmIndex);
        if(itmIndex<0){
          //console.log(itm.itemName)
          itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
          itm.openingAmt=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
          result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
        }
      }
    });
    _.forEach(result.betweenDate,function(itm,i){
      if(itm.totalPurchaseQty!=undefined){
        if(isNaN(itm.totalPurchaseQty))
          itm.totalPurchaseQty = 0;
        if(isNaN(itm.totalPurchaseAmount))
          itm.totalPurchaseAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.consumptionQty==undefined){
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
          itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
          result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
        }
      }
      else if(itm.totalBalanceQty!=undefined){
        if(isNaN(itm.totalBalanceQty))
          itm.totalBalanceQty = 0;
        if(isNaN(itm.totalBalanceAmt))
          itm.totalBalanceAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        //console.log('itm', itm);
        var cDate=new Date(itm.created);
        //cDate.setDate(cDate.getDate()-1);
        //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
        //var ed = new Date(endDate);
        //ed = new Date(ed.setDate(ed.getDate()-1));
        //console.log(cDate);
        //console.log(endDate);
        //console.log(ed);
        if(itmIndex<0){
          itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
          itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(2);
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(2);
          itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
          itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = parseFloat(0).toFixed(2);
          itm.varianceInPercent = parseFloat(0).toFixed(2);
          itm.physicalQty = parseFloat(0).toFixed(3);
          if(endDate.getTime()==cDate.getTime()){
            itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
          }
          else{
            itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
            itm.physicalDate = new Date(itm.created);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
          result.items.push(itm);
        }
        else
        {
          if(endDate.getTime()==cDate.getTime()){
            result.items[itmIndex].physicalQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
            result.items[itmIndex].physicalAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
            result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
            result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);

            //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
            //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
          }
          else {
            //console.log(itm.itemName, " else");
            //console.log(result.items[itmIndex].closingQty);
            //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
            if(result.items[itmIndex].physicalDate) {
              //console.log(result.items[itmIndex].physicalDate)
              //console.log(cDate)
              if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                //console.log('if');
                result.items[itmIndex].closingQty = roundNumber(parseFloat(result.items[itmIndex].closingQty), 3) + roundNumber(parseFloat(itm.totalBalanceQty), 3);
                //console.log(result.items[itmIndex].closingQty);
              } else {
                //console.log('else');
                result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].physicalDate = cDate;
              }
            } else {
              result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].physicalDate = cDate;
            }
            // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
            // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
            // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
          }
        }
      }
      else if(itm.totalIntermediateQty!=undefined){
        if(isNaN(itm.totalIntermediateQty))
          itm.totalIntermediateQty = 0;
        if(isNaN(itm.totalIntermediateAmt))
          itm.totalIntermediateAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.purchaseQty==undefined){
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
          itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
          result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIntermediateAmt)).toFixed(2);
        }
      }
      else if(itm.totalSaleQty!=undefined){
        if(isNaN(itm.totalSaleQty))
          itm.totalSaleQty = 0;
        if(isNaN(itm.totalSaleAmount))
          itm.totalSaleAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.purchaseQty==undefined){
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
          itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
          result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
        }
      }
      else if(itm.totalWastageQty!=undefined){
        if(isNaN(itm.totalWastageQty))
          itm.totalWastageQty = 0;
        if(isNaN(itm.totalWastageAmt))
          itm.totalWastageAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.totalWastageQty!=undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.purchaseQty==undefined){
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
          itm.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
          result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
        }
      }
      else if(itm.totalTransferQty_toStore!=undefined){
        if(isNaN(itm.totalTransferQty_toStore))
          itm.totalTransferQty_toStore = 0;
        if(isNaN(itm.totalTransferAmount_toStore))
          itm.totalTransferAmount_toStore = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.consumptionQty==undefined){
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
          itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
          result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
        }
      }
      else if(itm.totalTransferQty!=undefined){
        if(isNaN(itm.totalTransferQty))
          itm.totalTransferQty = 0;
        if(isNaN(itm.totalTransferAmount))
          itm.totalTransferAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.purchaseQty==undefined){
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
          itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
          result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
        }
      }
      else if(itm.itemSaleQty!=undefined){
        if(isNaN(itm.itemSaleQty))
          itm.itemSaleQty = 0;
        if(isNaN(itm.itemSaleAmt))
          itm.itemSaleAmt = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.purchaseQty==undefined){
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
          itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
          itm.UnitName=itm.baseUnit;
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
          result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
        }
      }
      else if(itm.totalStockReceiveQty!=undefined){
        if(isNaN(itm.totalStockReceiveQty))
          itm.totalStockReceiveQty = 0;
        if(isNaN(itm.totalStockReceiveAmount))
          itm.totalStockReceiveAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        //console.log(itmIndex);
        if(itmIndex<0){
          //console.log(itm.itemName);
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.consumptionQty==undefined){
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.purchaseQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
          itm.purchaseAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          //console.log(itmIndex);
          result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
          result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
        }
      }
      else if(itm.totalStockReturnQty!=undefined){
        if(isNaN(itm.totalStockReturnQty))
          itm.totalStockReturnQty = 0;
        if(isNaN(itm.totalStockReturnAmount))
          itm.totalStockReturnAmount = 0;
        var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        if(itmIndex<0){
          if(itm.openingQty==undefined){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.closingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(3);
            itm.closingAmount=parseFloat(-itm.totalStockReturnAmount).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDate);
            itm.closingDate=new Date(endDateToShow);
          }
          if(itm.purchaseQty==undefined){
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(3);
          }
          if(itm.wastageQty == undefined) {
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.wastageAmount=parseFloat(0).toFixed(3);
          }
          if(itm.varianceQty == undefined) {
            itm.varianceQty=parseFloat(0).toFixed(3);
          }
          itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
          itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
          result.items.push(itm);
        }
        else
        {
          result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
          result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);

          result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
          result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
        }
      }

    });
    //console.log(JSON.stringify(result));
    return result;
};

function spliceForCategoryAndItem(result,items, itemId, deployment){
  //console.log('items',items);
   //console.log('result',result);
   if(itemId){
    _.forEachRight(result.items,function(itm,i){
      var index=_.findIndex(items,{id:itemId});
      if(index<0){
        result.items.splice(i,1);
      }
      else
      {
        itm.itemName=items[index].itemName;
      }
    });
  }
  return result;
};

function convertItemInPreferredUnit(result, deployment){
  //console.log('result cvi',result);
  //console.log('converting', result.items.length)
  _.forEach(result.items,function(itm,i){
    var indeex=_.findIndex($scope.stockitems,function(it) {
      return it._id.toString() == itm.itemId;
    });
    //console.log('index', indeex)
    if(indeex>=0){
      var item=$scope.stockitems[indeex];
      //console.log(item.preferredUnit);
      if(item.preferedUnit!=undefined){
        _.forEach(item.units,function(u,i){
          if(u._id==item.preferedUnit){
            var conversionFactor=1;
            var pconFac=parseFloat(u.conversionFactor);
            itm.UnitName=u.unitName;
            if(u.baseUnit.id==2 || u.baseUnit.id==3){
              conversionFactor=parseFloat(u.baseConversionFactor);
              if(pconFac>conversionFactor){
                itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                }
              }
              else if(pconFac<conversionFactor){
                itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                }
              }
            }
            else
            {
              if(pconFac>conversionFactor){
                itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                }
                itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
              }
              else if(pconFac<conversionFactor){
                itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                }
                itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
              }
            }
          }
        });
      }
    }
  });
  return result;
};

function formatDataForItemReport(result, deployment){
      //console.log('formatting data');
      result.items=[];
      //_.forEach(result.beforeDate,function(itm,i){
      // if(itm.totalOpeningQty!=undefined){
      //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //   if(itmIndex<0){
      //     itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
      //     itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
      //     itm.purchaseQty=parseFloat(0).toFixed(3);
      //     itm.purchaseAmount=parseFloat(0).toFixed(2);
      //     itm.consumptionQty=parseFloat(0).toFixed(3);
      //     itm.consumptionAmount=parseFloat(0).toFixed(2);
      //     itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
      //     itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
      //     itm.openingDate=new Date(startDate);
      //     itm.closingDate=new Date(endDate);
      //     result.items.push(itm);
      //   }
      //   else
      //   {
      //     result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
      //     result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

      //     result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
      //     result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
      //   }
      // }
      // else if(itm.totalBalanceQty!=undefined){
      //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //   if(itmIndex<0){
      //     itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
      //     itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
      //     itm.purchaseQty=parseFloat(0).toFixed(3);
      //     itm.purchaseAmount=parseFloat(0).toFixed(2);
      //     itm.consumptionQty=parseFloat(0).toFixed(3);
      //     itm.consumptionAmount=parseFloat(0).toFixed(2);
      //     itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
      //     itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
      //     itm.openingDate=new Date(startDate);
      //     itm.closingDate=new Date(endDate);
      //     result.items.push(itm);
      //   }
      //   else
      //   {
      //     result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
      //     result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

      //     result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
      //     result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
      //   }
      // }
      // else if(itm.totalPurchaseQty!=undefined){
      //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //   if(itmIndex<0){
      //     itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
      //     itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
      //     itm.purchaseQty=parseFloat(0).toFixed(3);
      //     itm.purchaseAmount=parseFloat(0).toFixed(2);
      //     itm.consumptionQty=parseFloat(0).toFixed(3);
      //     itm.consumptionAmount=parseFloat(0).toFixed(2);
      //     itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
      //     itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
      //     itm.openingDate=new Date(startDate);
      //     itm.closingDate=new Date(endDate);
      //     result.items.push(itm);
      //   }
      //   else
      //   {
      //     result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
      //     result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

      //     result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
      //     result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
      //   }
      // }
      // else
      //     if(itm.totalSaleQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
      //       }
      //     }
      //     else if(itm.totalWastageQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);
      //       }
      //     }
      //     else if(itm.totalTransferQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
      //       }
      //     }
      //     else if(itm.itemSaleQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);
      //       }
      //     }
      //     else if(itm.totalIntermediateQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
      //       }
      //     }
      // });
      _.forEach(result.betweenDate,function(itm,i){
        if(itm.totalSaleQty!=undefined){
          if(isNaN(itm.totalSaleQty))
            itm.totalSaleQty = 0;
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined) {
          if(isNaN(itm.totalWastageQty))
            itm.totalWastageQty = 0;
          var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
          if (itmIndex < 0) {
            itm.consumptionQty = parseFloat(itm.totalWastageQty).toFixed(3);
            itm.consumptionAmount = parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
          }
          else {
            result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalStockReturnQty!=undefined){
          if(isNaN(itm.totalStockReturnQty))
            itm.totalStockReturnQty = 0;
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {

            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        }
        // else if(itm.totalStockReceiveQty!=undefined){
        //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
        //       if(itmIndex<0){
        //         itm.consumptionQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
        //         itm.consumptionAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
        //         result.items.push(itm);
        //       }
        //       else
        //       {
        //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
        //       }
        //     }
                    else if(itm.totalTransferQty!=undefined){
                      if(isNaN(itm.totalTransferQty))
                        itm.totalTransferQty = 0;
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
                      }
                    }
                    else if(itm.itemSaleQty!=undefined){
                      if(isNaN(itm.itemSaleQty))
                        itm.itemSaleQty = 0;
                      //alert(itm.itemName);
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      //alert(itmIndex);
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);
                      }
                    }
                    else if(itm.totalIntermediateQty!=undefined){
                      if(isNaN(itm.totalIntermediateQty))
                        itm.totalIntermediateQty = 0;
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                      }
                    }
                  });
      return result;
    };

function getItemdetails(data, deployment){
  //console.log('getting item details');
      var items=data.items;
      //console.log(data.items);
      _.forEach(items,function(it,i){
        it.items=[];
        if(it.isBaseKitchenItem)
          it.UnitName = it.baseUnit;
        //it.items.push(angular.copy(it));
        _.forEach(data.betweenDate,function(itm,i){
          //console.log('---------------------------------------------------');
          //console.log(itm);
          //console.log('itm',itm);
          // if(itm.isBaseKitchenItem)
          //   console.log("BASE");
          //alert(itm.itemName);
          if(itm.itemId==it.itemId && itm.menuId == undefined){
            if(itm.totalSaleQty!=undefined){
              if(isNaN(itm.totalSaleQty))
                itm.totalSaleQty = 0;
              if(isNaN(itm.totalSaleAmount))
                itm.totalSaleAmount = 0;
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                it.items.push(obj);
              }
              else {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
              //}
            }
            else if(itm.totalWastageQty!=undefined){
              if(isNaN(itm.totalWastageQty))
                itm.totalWastageQty = 0;
              if(isNaN(itm.totalWastageAmt))
                itm.totalWastageAmt = 0;
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
              //}
            }
            else if(itm.totalTransferQty!=undefined){
              if(isNaN(itm.totalTransferQty))
                itm.totalTransferQty = 0;
              if(isNaN(itm.totalTransferAmount))
                itm.totalTransferAmount = 0;
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              //}
            }
            else if(itm.totalStockReturnQty!=undefined){
              if(isNaN(itm.totalStockReturnQty))
                itm.totalStockReturnQty = 0;
              if(isNaN(itm.totalStockReturnAmount))
                itm.totalStockReturnAmount = 0;
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              //}
            }
            else if(itm.totalStockReceiveQty!=undefined){
              if(isNaN(itm.totalStockReceiveQty))
                itm.totalStockReceiveQty = 0;
              if(isNaN(itm.totalStockReceiveAmount))
                itm.totalStockReceiveAmount = 0;
 // console.log('itm in tsr',itm);
  //if(itm.menuId!=undefined){
      var itmIndex=_.findIndex(it.items,function (item){
        return item.itemId == itm.itemId;
      });
      if(itmIndex<0){
        var obj={};
        obj.itemId=itm.itemId;
        obj.itemName=itm.itemName;
        if(itm.isBaseKitchenItem)
          obj.UnitName=itm.UnitName;
        else obj.UnitName=itm.UnitName;
        obj.consumptionQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
        obj.consumptionAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
        it.items.push(obj);
      }
      else
      {
        it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
      }

      it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReceiveQty)).toFixed(3);
      it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
      //}
    }

            else if(itm.itemSaleQty!=undefined){
              if(isNaN(itm.itemSaleQty))
                itm.itemSaleQty = 0;
              if(isNaN(itm.itemSaleAmt))
                itm.itemSaleAmt = 0;
              if(itm.recipeId!=undefined){
                //alert(itm.recipeId);
                //var itmIndex=_.findIndex(it.items,{itemId:itm.recipeId});
                //if(itmIndex<0){
                alert(itm.baseUnit);
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.recipeName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                it.items.push(obj);
                //}
                //else
                //{
                //  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                //  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);
                //}
                it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);

                //it.items[0].consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                //it.items[0].consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }
          } else if(itm.itemId==it.itemId && itm.menuId != undefined){
            if(itm.totalSaleQty!=undefined){
              if(isNaN(itm.totalSaleQty))
                itm.totalSaleQty = 0;
              if(isNaN(itm.totalSaleAmount))
                itm.totalSaleAmount = 0;
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.menuId == itm.menuId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.menuId;
                obj.itemName=itm.menuName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
              //}
            }
            else if(itm.totalWastageQty!=undefined){
              if(isNaN(itm.totalWastageQty))
                itm.totalWastageQty = 0;
              if(isNaN(itm.totalWastageAmt))
                itm.totalWastageAmt = 0;
              //console.log('itmWastage',itm);
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.menuId == itm.menuId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.menuId;
                obj.itemName=itm.menuName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
              //}
            }
            else if(itm.totalTransferQty!=undefined){
              if(isNaN(itm.totalTransferQty))
                itm.totalTransferQty = 0;
              if(isNaN(itm.totalTransferAmount))
                itm.totalTransferAmount = 0;
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.menuId == itm.menuId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.menuId;
                obj.itemName=itm.menuName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              //}
            }
            else if(itm.totalStockReturnQty!=undefined){
              if(isNaN(itm.totalStockReturnQty))
                itm.totalStockReturnQty = 0;
              if(isNaN(itm.totalStockReturnAmount))
                itm.totalStockReturnAmount = 0;
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.menuId == itm.menuId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.menuId;
                obj.itemName=itm.menuName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              //}
            }
            else if(itm.itemSaleQty!=undefined){
              if(isNaN(itm.itemSaleQty))
                itm.itemSaleQty = 0;
              if(isNaN(itm.itemSaleAmt))
                itm.itemSaleAmt = 0;
              if(itm.recipeId!=undefined){
                //alert(itm.recipeId);
                //var itmIndex=_.findIndex(it.items,{itemId:itm.recipeId});
                //if(itmIndex<0){
                var obj={};
                obj.itemId=itm.menuId;
                obj.itemName=itm.recipeName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                it.items.push(obj);
                //}
                //else
                //{
                //  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                //  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);
                //}
                it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);

                //it.items[0].consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                //it.items[0].consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }
          }
        });
      });
      return items;
    };

function convertItemInPreferredUnit_ItemReport_New(result, deployment){
      //console.log('converting to preferred');
      // console.log(result);
      _.forEach(result,function(r,ii){
        _.forEach(r.items,function(itm, i){
          //alert(itm.itemName + " " + itm.UnitName)
          var indeex=_.findIndex($scope.stockitems,function (it) {
            return it._id.toString() == itm.itemId;
          });
          //console.log(indeex, itm.itemName);
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
            //console.log('item 101',item)
            var unitId;
            if(typeof item.preferedUnit == Object){
              //console.log(item.itemName);
              unitId = item.preferedUnit._id;
            } else{
              //console.log('else', item.itemName);
              unitId = item.preferedUnit;
            }
            if(item.preferedUnit!=undefined){
              _.forEach(item.units,function(u,i){
                if(u._id==unitId){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  //console.log(itm);
                  //console.log(u);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>=conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
            else
            {
            _.forEach(item.units,function(u,i){
                
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  //console.log(itm);
                  //console.log(u);
                  itm.UnitName=u.baseUnit.name;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>=conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                
              });
            }
          }
        });
      });
      return result;
    };

function spliceForCategoryAndItem_ItemReport(d,items){
  _.forEachRight(d,function(itm,i){
    var index=_.findIndex(items,{id:itm.itemId});
    if(index<0){
      d.splice(i,1);
    }
    else
    {
      itm.itemName=items[index].itemName;
    }
  });
  return d;
};

function getDeployment(deployment_id) {
  var deferred = Q.defer();
  Stock.Deployment.findOne( {_id: deployment_id}, {settings: 1}, function (err, deployment) {
    if(err)
      deferred.reject(err);
    else
      deferred.resolve(deployment);
  });
  return deferred.promise;
}

function formatDataForReport_DateWise (data, startDate, endDate) {
  var startDate = startDate;
  var endDate = endDate;
  var endDateToShow = new Date(endDate.setDate(endDate.getDate() - 1));
  var result = data;
  result.items = [];
  _.forEach(result.beforeDate, function (itm, i) {
    if (itm.totalOpeningQty != undefined) {
      if(isNaN(itm.totalOpeningQty))
        itm.totalOpeningQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(itm.totalOpeningQty).toFixed(3);
        itm.openingAmt = parseFloat(itm.totalOpeningAmt).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(3);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalOpeningAmt)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalOpeningAmt)).toFixed(2);
      }
    }
    else if (itm.totalBalanceQty != undefined) {
      if(isNaN(itm.totalBalanceQty))
        itm.totalBalanceQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
        itm.openingAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalBalanceQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalBalanceAmt)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalBalanceQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalBalanceAmt)).toFixed(2);
      }
    }
    else if (itm.totalPurchaseQty != undefined) {
      if(isNaN(itm.totalPurchaseQty))
        itm.totalPurchaseQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
        itm.openingAmt = parseFloat(itm.totalPurchaseAmount).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].getVarianceDateWiseReportpeningAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
      }
    }
    else if (itm.totalIntermediateQty != undefined) {
      if(isNaN(itm.totalIntermediateQty))
        itm.totalIntermediateQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
        itm.openingAmt = parseFloat(-itm.totalIntermediateAmt).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalIntermediateQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalIntermediateAmt)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalIntermediateQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalIntermediateAmt)).toFixed(2);
      }
    }
    else if (itm.totalSaleQty != undefined) {
      if(isNaN(itm.totalSaleQty))
        itm.totalSaleQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
        itm.openingAmt = parseFloat(-itm.totalSaleAmount).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalSaleAmount)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalSaleAmount)).toFixed(2);
      }
    }
    else if (itm.totalWastageQty != undefined) {
      if(isNaN(itm.totalWastageQty))
        itm.totalWastageQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
        itm.openingAmt = parseFloat(-itm.totalWastageAmt).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalWastageQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalWastageAmt)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalWastageQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalWastageAmt)).toFixed(2);
      }
    }
    else if (itm.totalTransferQty != undefined) {
      if(isNaN(itm.totalTransferQty))
        itm.totalTransferQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
        itm.openingAmt = parseFloat(-itm.totalTransferAmount).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalTransferAmount)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalTransferAmount)).toFixed(2);
      }
    }
    else if (itm.totalTransferQty_toStore != undefined) {
      if(isNaN(itm.totalTransferQty_toStore))
        itm.totalTransferQty_toStore = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        itm.openingAmt = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
      }
    }
    else if (itm.itemSaleQty != undefined) {
      if(isNaN(itm.itemSaleQty))
        itm.itemSaleQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
        itm.openingAmt = parseFloat(-itm.itemSaleAmt).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.UnitName = itm.baseUnit;
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.itemSaleAmt)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.itemSaleAmt)).toFixed(2);
      }
    }
    else if (itm.totalStockReturnQty != undefined) {
      if(isNaN(itm.totalStockReturnQty))
        itm.totalStockReturnQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
        itm.openingAmt = parseFloat(-itm.totalStockReturnAmount).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalStockReturnAmount)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalStockReturnAmount)).toFixed(2);
      }
    }
    else if (itm.totalStockReceiveQty != undefined) {
      if(isNaN(itm.totalStockReceiveQty))
        itm.totalStockReceiveQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itmIndex < 0) {
        itm.openingQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
        itm.openingAmt = parseFloat(itm.totalStockReceiveAmount).toFixed(2);
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(2);
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(2);
        itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        result.items.push(itm);
      }
      else {
        result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

        result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
      }
    }
  });
  _.forEach(result.betweenDate, function (itm, i) {
    if (itm.totalPurchaseQty != undefined) {
      if(isNaN(itm.totalPurchaseQty))
        itm.totalPurchaseQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(itm.totalPurchaseAmount).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.consumptionQty == undefined) {
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.purchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
      itm.purchaseAmount = parseFloat(itm.totalPurchaseAmount).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.totalBalanceQty != undefined) {
      if(isNaN(itm.totalBalanceQty))
        itm.totalBalanceQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(0).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(0).toFixed(3);
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDate);
      }
      if (itm.consumptionQty == undefined) {
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      if (itm.purchaseQty == undefined) {
        itm.purchaseQty = parseFloat(0).toFixed(3);
      }
      itm.varianceQty = parseFloat(0).toFixed(3);
      itm.varianceInPercent = parseFloat(0).toFixed(2);
      itm.physicalQty = parseFloat(itm.totalBalanceQty).toFixed(3);
      itm.physicalAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.totalIntermediateQty != undefined) {
      if(isNaN(itm.totalIntermediateQty))
        itm.totalIntermediateQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(-itm.totalIntermediateAmt).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.purchaseQty == undefined) {
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.consumptionQty = parseFloat(itm.totalIntermediateQty).toFixed(3);
      itm.consumptionAmount = parseFloat(itm.totalIntermediateAmt).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.totalSaleQty != undefined) {
      if(isNaN(itm.totalSaleQty))
        itm.totalSaleQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(-itm.totalSaleAmount).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.purchaseQty == undefined) {
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.consumptionQty = parseFloat(itm.totalSaleQty).toFixed(3);
      itm.consumptionAmount = parseFloat(itm.totalSaleAmount).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.totalWastageQty != undefined) {
      if(isNaN(itm.totalWastageQty))
        itm.totalWastageQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(-itm.totalWastageAmt).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.purchaseQty == undefined) {
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(3);
      }
      if (itm.consumptionQty == undefined) {
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(3);
      }
      itm.wastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
      itm.wastageAmt = parseFloat(itm.totalWastageAmt).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.totalTransferQty_toStore != undefined) {
      if(isNaN(itm.totalTransferQty_toStore))
        itm.totalTransferQty_toStore = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.consumptionQty == undefined) {
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.purchaseQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
      itm.purchaseAmount = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.totalTransferQty != undefined) {
      if(isNaN(itm.totalTransferQty))
        itm.totalTransferQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(-itm.totalTransferAmount).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.purchaseQty == undefined) {
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.consumptionQty = parseFloat(itm.totalTransferQty).toFixed(3);
      itm.consumptionAmount = parseFloat(itm.totalTransferAmount).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.itemSaleQty != undefined) {
      if(isNaN(itm.itemSaleQty))
        itm.itemSaleQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(-itm.itemSaleAmt).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.purchaseQty == undefined) {
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.consumptionQty = parseFloat(itm.itemSaleQty).toFixed(3);
      itm.consumptionAmount = parseFloat(itm.itemSaleAmt).toFixed(2);
      itm.UnitName = itm.baseUnit;
      result.items.push(itm);
    }
    else if (itm.totalStockReceiveQty != undefined) {
      if(isNaN(itm.totalStockReceiveQty))
        itm.totalStockReceiveQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.consumptionQty == undefined) {
        itm.consumptionQty = parseFloat(0).toFixed(3);
        itm.consumptionAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.purchaseQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
      itm.purchaseAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(2);
      result.items.push(itm);
    }
    else if (itm.totalStockReturnQty != undefined) {
      if(isNaN(itm.totalStockReturnQty))
        itm.totalStockReturnQty = 0;
      var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
      if (itm.openingQty == undefined) {
        itm.openingQty = parseFloat(0).toFixed(3);
        itm.closingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
        itm.openingAmt = parseFloat(0).toFixed(3);
        itm.closingAmount = parseFloat(-itm.totalStockReturnAmount).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.physicalQty = "NA";
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.openingDate = new Date(startDate);
        itm.closingDate = new Date(endDateToShow);
      }
      if (itm.purchaseQty == undefined) {
        itm.purchaseQty = parseFloat(0).toFixed(3);
        itm.purchaseAmount = parseFloat(0).toFixed(3);
      }
      if (itm.wastageQty == undefined) {
        itm.wastageQty = parseFloat(0).toFixed(3);
      }
      itm.consumptionQty = parseFloat(itm.totalStockReturnQty).toFixed(3);
      itm.consumptionAmount = parseFloat(itm.totalStockReturnAmount).toFixed(2);
      result.items.push(itm);
    }
  });
  return result;
}

function convertItemInPreferredUnit_DateWise(data) {
  var result = data;
  var items = $scope.stockitems;
  _.forEach(result.items, function (itm, i) {
    var indeex = _.findIndex(items, function (it) {
      return it._id.toString() == itm.itemId;
    });
    if (indeex >= 0) {
      var item = items[indeex];
      if (item.preferedUnit != undefined) {
        _.forEach(item.units, function (u, i) {
          if (u._id == item.preferedUnit) {
            var conversionFactor = 1;
            var pconFac = parseFloat(u.conversionFactor);
            itm.UnitName = u.unitName;
            if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
              conversionFactor = parseFloat(u.baseConversionFactor);
              if (pconFac > conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(conversionFactor)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(conversionFactor)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(conversionFactor)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(conversionFactor)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                if (itm.physicalQty != "NA") {
                  if (itm.physicalQty == undefined) {
                    itm.physicalQty = parseFloat(0).toFixed(3);
                  }
                  else {
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                  }
                }
              }
              else if (pconFac < conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(conversionFactor)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(conversionFactor)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(conversionFactor)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(conversionFactor)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                if (itm.physicalQty != "NA") {
                  if (itm.physicalQty == undefined) {
                    itm.physicalQty = parseFloat(0).toFixed(3);
                  }
                  else {
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                  }
                }
              }
            }
            else {
              if (pconFac > conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(pconFac)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(pconFac)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(pconFac)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(pconFac)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                if (itm.physicalQty != "NA") {
                  if (itm.physicalQty == undefined) {
                    itm.physicalQty = parseFloat(0).toFixed(3);
                  }
                  else {
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                  }
                  itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
                }
              }
              else if (pconFac < conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(pconFac)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(pconFac)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(pconFac)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(pconFac)).toFixed(3);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                if (itm.physicalQty != "NA") {
                  if (itm.physicalQty == undefined) {
                    itm.physicalQty = parseFloat(0).toFixed(3);
                  }
                  else {
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                  }
                  itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
                }
              }
            }
          }
        });
      }
    }
  });
  return result;
}

function generateDateWiseVarianceReport (items, days) {
  
  items.sort(function (a, b) {
    return new Date(b.created) - new Date(a.created);
  });
  //console.log(items);
  _.forEach(days, function (d, i) {
    d.items = [];
    var pdate = new Date(d.date);
    pdate.setDate(pdate.getDate() - 1);
    //console.log('1610', pdate);
    d.items = getPreviousDate_Data(days, pdate);
    //console.log(angular.copy(d.items));
    var resetDate = new Date(d.date);
    resetDate = new Date(resetDate.setDate(resetDate.getDate() + 1));
    var ind = -1;
    _.forEachRight(items, function (itm, ii) {
      var cDate = new Date(itm.created);
      //cDate.setHours(0,0,0,0);
      //var resetDate = new Date(d.date.setDate(d.date.getDate() + 1));
      //console.log(d.date);
      //console.log(cDate);
      if (new Date(d.date) > new Date(cDate)) {
        //console.log('if');
        var index = _.findIndex(d.items, {itemId: itm.itemId});
        ind = index;
        if (index < 0) {
          if (index == -1) {
            itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
          }
          else {
            itm.openingQty = parseFloat(getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
            itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
          }
          d.items.push(itm);
          items.splice(ii, 1);
        }
      }
      else if (new Date(d.date) <= new Date(cDate) && new Date(cDate) <= resetDate) {
        //console.log('else');
        var index = _.findIndex(d.items, {itemId: itm.itemId});
        ind = index;
        if (index < 0) {
          if (index == -1) {
            itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
          }
          else {
            itm.openingQty = parseFloat(getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
            //console.log(d.date, itm.openingQty);
            itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
          }
          d.items.push(itm);
          items.splice(ii, 1);
        }
        else {
          d.items[index].openingQty = parseFloat(parseFloat(d.items[index].openingQty) + parseFloat(itm.openingQty)).toFixed(3);
          d.items[index].purchaseQty = parseFloat(parseFloat(d.items[index].purchaseQty) + parseFloat(itm.purchaseQty)).toFixed(3);
          d.items[index].consumptionQty = parseFloat(parseFloat(d.items[index].consumptionQty) + parseFloat(itm.consumptionQty)).toFixed(3);
          d.items[index].wastageQty = parseFloat(parseFloat(d.items[index].wastageQty) + parseFloat(itm.wastageQty)).toFixed(3);
          d.items[index].closingQty = parseFloat(parseFloat(d.items[index].closingQty) + parseFloat(itm.closingQty)).toFixed(3);
          if (itm.physicalQty != "NA") {
            if (d.items[index].physicalQty != "NA") {
              d.items[index].physicalQty = parseFloat(parseFloat(d.items[index].physicalQty) + parseFloat(itm.physicalQty)).toFixed(3);
            }
            else {
              //console.log(itm, d.date);
              d.items[index].physicalQty = parseFloat(parseFloat(itm.physicalQty)).toFixed(3);
            }
            //d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
            //if(d.items[index].physicalQty!="NA"){
            //  var dNO=parseFloat(i+1);
            //
            //  //d.items[index].openingQty=parseFloat(d.items[index].physicalQty).toFixed(3);
            //  d.items[index].closingQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(d.items[index].purchaseQty)-parseFloat(d.items[index].wastageQty)-parseFloat(d.items[index].consumptionQty)).toFixed(3);
            //  d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
            //  d.items[index].varianceQty=parseFloat(parseFloat(d.items[index].closingQty)-parseFloat(d.items[index].physicalQty)).toFixed(3);
            //  d.items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(d.items[index].varianceQty)/parseFloat(d.items[index].closingQty))*parseFloat(100)).toFixed(2);
            //if(dNO>=0){
            //  days[dNO].items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
            //  days[dNO].items[index].varianceQty=parseFloat(parseFloat(days[dNO].items[index].closingQty)-parseFloat(days[dNO].items[index].physicalQty)).toFixed(3);
            //  days[dNO].items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(days[dNO].items[index].varianceQty)/parseFloat(days[dNO].items[index].closingQty))*parseFloat(100)).toFixed(2);
            //}
            //d.items[index].physicalQty="NA";
          }
          items.splice(ii, 1);
        }
      }
    })
    _.forEach(d.items, function (itm){
      if(itm.physicalQty != "NA"){
        itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
        itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
      }
    });
  });
  return days;
}

function getClosingQtyPreviousDate (items, pdate, item) {
  var cQTy = 0;
  var index = _.findIndex(items, {date: pdate});
  if (index >= 0) {
    var itmIndex = _.findIndex(items[index].items, {itemId: item.itemId});
    if (itmIndex >= 0) {
      if (items[index].items[itmIndex].physicalQty != "NA") {
        cQTy = parseFloat(items[index].items[itmIndex].closingQty).toFixed(3);
      }
      else {
        cQTy = parseFloat(items[index].items[itmIndex].physicalQty).toFixed(3);
      }
    }
  }
  return cQTy;
}

function getPreviousDate_Data (items, pdate) {
  var data = [];
  var index = _.findIndex(items, {date: pdate});
  if (index >= 0) {
    var d = _.cloneDeep(items[index].items);
    _.forEach(d, function (itm, i) {
      if(itm.physicalQty != "NA")
        itm.openingQty = parseFloat(itm.physicalQty).toFixed(3);
      else
        itm.openingQty = parseFloat(itm.closingQty).toFixed(3);
      itm.closingQty = itm.openingQty;
      itm.purchaseQty = parseFloat(0).toFixed(3);
      itm.consumptionQty = parseFloat(0).toFixed(3);
      itm.wastageQty = parseFloat(0).toFixed(3);
      itm.physicalQty = "NA";
      itm.varianceQty = "NA";
      itm.varianceInPercent = "NA";
      data.push(itm);
    });
  }
  return data;
}

function getDates (startDate, endDate) {
  //console.log('dates');
  var dateArray = new Array();
  var currentDate = new Date(startDate);
  var toDate = new Date(endDate);
  //console.log('28 ', toDate);
  toDate.setDate(toDate.getDate() + 1);
  while (currentDate <= toDate) {
    dateArray.push({date: new Date(currentDate)});
    currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));
    //console.log(currentDate);
  }
  return dateArray;
}

exports.getConsumptionSummaryMailer = function (req, response) {
  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockReportResetTime/getConsumptionSummary?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        //console.log(error);
        response.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return response.json(200, JSON.parse(body));
      }
    })
  } else {

    var resetTime = req.query.resetTime;
    var startDate = new Date(req.query.fromDate);
    //startDate.setHours(0, 0, 0, 0);
    var endDate = new Date(req.query.toDate);
    //console.log('start',startDate);
    //console.log('end',endDate);
    //endDate.setDate(endDate.getDate() + 1);
    //if(!resetTime) {
    //  console.log('No reset time');
    //  startDate = new Date(startDate.setHours(0, 0, 0, 0));
    //  endDate = new Date(endDate.setDate(endDate.getDate() + 1));
    //}
    //console.log(startDate);
    //console.log(endDate);
    var tenant_id = req.query.tenant_id;
    var deployment_id = req.query.deployment_id;
    var storeId = req.query.storeId;
    var categoryId = req.query.category;
    var isKitchen = false;
    var itemId = req.query.itemId;
    var email = req.query.email;
    if (req.query.isKitchen != undefined) {
      isKitchen = req.query.isKitchen;
    }
    var report = req.query.report;
    response.json(200, {successMessage: "Report will be delivered to you on the email provided, soon."})
    Q.all([
        getConsumptionData(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen, categoryId),
        getStockItems(tenant_id, deployment_id),
        getBaseKitchenItems(tenant_id, deployment_id),
        getLastPriceOfItem_RawMaterial(startDate, endDate, deployment_id, tenant_id),
        getDeployment(deployment_id)
      ]).then(function (val) {
        //response.json(200, val[0])
        $scope.stockitems = val[1];
        //console.log($scope.stockitems);
        var deployment = val[4];
        _.forEach(val[2], function (item) {
          $scope.stockitems.push(item);
        });
        $scope.lastPrice = val[3];
        //console.log(val[0].errorMessage);
        if(val[0].errorMessage!=undefined){
          emailSend({email: email}, null, null, tenant_id, deployment_id, "Report not generated due to: " + val[0].errorMessage)
        }
        else
        {
          //response.status(200).json(val[0]);
          if(report == 1){
            //console.log('result', _.cloneDeep(result[0]));
            //spliceandmovePhysical(result,$scope.purchaseConsumptionForm.fromDate)
            //console.log('val', val[3]);
            var result=spliceAllItemsBeforePhysical(val[0], deployment, endDate);
            //console.log('splice', result[0]);
            var cr=calculatePrice(JSON.parse(JSON.stringify(result)), deployment);
            //console.log('price', _.cloneDeep(result[0]));
            cr.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            cr.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            var fr = formatDataForReport(cr,startDate,endDate, deployment);
            //console.log('format', JSON.stringify(fr));
            var sr = spliceForCategoryAndItem(fr, itemId, itemId, deployment);
            var rest=convertItemInPreferredUnit(sr, deployment);
            //console.log(JSON.stringify(rest));
            renderExcelConsumptionReport(rest, email);
            //renderExcelVarianceConsolidated(rest, email);
          }
          else if(report == 2){
            var result=spliceAllItemsBeforePhysical(val[0], deployment, endDate);
            //console.log('splice', result[0]);
            var cr=calculatePrice(JSON.parse(JSON.stringify(result)), deployment);
            //console.log('price', _.cloneDeep(result[0]));
            cr.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            cr.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            var fr = formatDataForReport(cr,startDate,endDate, deployment);
            //console.log('format', JSON.stringify(fr));
            var sr=spliceForCategoryAndItem(fr, itemId, itemId, deployment);
            //console.log('spliceForCategoryAndItem',JSON.stringify(sr));
            var rest=convertItemInPreferredUnit(sr, deployment);
            //console.log(JSON.stringify(rest.items));
            //console.log('convertItemInPreferredUnit',angular.copy(res));
            //$scope.purchaseConsumptionForm.reportData=res;
            // var html = consumptionReport(res.items);
            // document.getElementById('consumptionReport').innerHTML = html;
            // $scope.purchaseConsumptionForm.isSearch=false;
            //console.log(result);
            renderExcelStockSummary(rest, email); 
          }
          if(report == 3){
            //console.log(val[0]);
            //console.log('3rd report', val[0].beforeDate.length, val[0].betweenDate.length);
            var result=calculatePrice(val[0]);
            console.log('price calculated', result.beforeDate.length, result.betweenDate.length);
            var data=formatDataForItemReport(result, deployment);
            console.log('formatted', data.beforeDate.length, data.betweenDate.length)
            //console.log(angular.copy(data));
            var d=getItemdetails(data, deployment);
            console.log('item details', d.length);
            //console.log(JSON.stringify(d));
            //console.log(angular.copy(d));
            d=convertItemInPreferredUnit_ItemReport_New(d, deployment);
            console.log('converted', d.length);
            //console.log(angular.copy(d));
            //d=spliceForCategoryAndItem_ItemReport(d,item);
            renderExcelItemWiseConsumption(d, email);
          }
          if(report == 4){

            var result=spliceAllItemsBeforePhysical(val[0], deployment, endDate);
            var cr=calculatePrice(JSON.parse(JSON.stringify(result)), deployment);
            cr.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            cr.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            var fr = formatDataForVarianceReport(cr, deployment, startDate, endDate);
            var sr=spliceForCategoryAndItem(fr, itemId, itemId, deployment);
            var rest=convertItemInPreferredUnitVariance(sr, deployment);
            //renderExcelConsumptionReport(rest, email);
            renderExcelVarianceConsolidated(rest, email);
          }
          if(report == 5) {

            //console.log(5);
            var data = val[0];
              //console.log(JSON.stringify(result.beforeDate));
            var result = spliceAllItemsBeforePhysical(data, deployment, endDate);
            result.beforeDate.sort(function (a, b) {
              return new Date(a.created) - new Date(b.created);
            });
            //console.log('sorting');
            result.betweenDate.sort(function (a, b) {
              return new Date(a.created) - new Date(b.created);
            });
            //console.log('sorting again');
            result = formatDataForReport_DateWise(result, startDate, endDate);
            //console.log('format');
            //result = spliceForCategoryAndItem(result, itmeId, itemId, deployment);
            //console.log('splicing');
            var res = convertItemInPreferredUnit_DateWise(result);
            //console.log('converting preferred');            //var res=self.convertItemInPreferredUnit();
            var days = getDates(startDate, endDate);
            //console.log('days');
            var gtR = generateDateWiseVarianceReport(res.items, days);
            //console.log('gTR')
            var index = gtR.length - 1;
            gtR.splice(index, 1);
            //console.log('5',5);
            renderExcelDateWiseVariance(gtR, email, days);
          }
          $scope = {};
          return;
        }
      }).catch(function (err) {
        console.log(err);
        emailSend({email: email}, null, null, tenant_id, deployment_id, "Some error occured while generating report. Please try again!")
      });
  }
};

function getConsumptionData(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen, categoryId) {
  var deferred = Q.defer();
  var returnData = {beforeDate: [], betweenDate: []};
  var minDate = new Date(startDate);
  var data = [];
  var databetween = [];
  var lastPrice = [];
  getOpeningStockByItem(startDate, endDate, deployment_id, tenant_id, storeId).then(function (result_op) {
    //console.log(result_op);
    //console.log('opening qty', result_op.length);
    /**
     * if (result_op.length > 0) {
    _.forEach(result_op, function (op, i) {
      var tempDate = new Date(op.created);
      //console.log(tempDate);
      tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
      //console.log('tempDate', tempDate);
      minDate = new Date(tempDate);
      data.push(op);
    });
   */
    //console.log('opening', result_op.length);
    if (result_op.length > 0) {
      _.forEach(result_op, function (op, i) {
        var tempDate = moment(new Date(op.created)).toDate();
        // console.log(moment(new Date(op.created)).startOf('day').toDate());
        //console.log(new Date('8/18/2016'));
        //console.log(tempDate);
        var startOfOpeningDay = moment(new Date(op.created)).startOf('day').toDate();
        startOfOpeningDay.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        //console.log('startOfOpeningDay', startOfOpeningDay);
        if(startOfOpeningDay > tempDate)
          moment(tempDate).subtract('days', 1);
        tempDate = moment(tempDate).startOf('day').toDate();
        tempDate.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0);
        // console.log('tempDate', tempDate);
        //console.log('temp', tempDate);
        minDate = new Date(tempDate);
        data.push(op);
      });
       //console.log("minDate" + minDate);
       //console.log('start date',startDate);
       //console.log('end date',endDate);
       //console.log('min date',minDate);
      if (endDate.getTime() < minDate.getTime()) {
        returnData.errorMessage = "Report not generated due to latest opening( " + moment(minDate).format('DD-MMM-YYYY') + " ) is greater than selected end Date";
        //console.log("Report not generated due to latest opening is greater than selected end Date");
        deferred.resolve(returnData);
      }
      else if (minDate.getTime() < startDate.getTime()) {
        //console.log('----------------------');
        //console.log('else if');

        //console.log("op < startDate");
        //console.log('min < start')
        dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          //console.log('beforeDate', value.length);
          returnData.beforeDate = value;
          _.forEach(data, function (d, i) {
            returnData.beforeDate.push(d);
          })
          dataBetweenDate(startDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            //console.log('betweenDate', value.length);
            returnData.betweenDate = value;
            deferred.resolve(returnData);
          }, function (err) {
            console.log(err);
            deferred.reject(err);
          });
        }, function (err) {
          console.log(err);
          deferred.reject(err);
        });
      }
      else {
        //console.log('----------------------');
        //console.log('else if');
        //console.log("op > startDate and op < end");
        //console.log('min > start')
        dataBeforeDate(minDate, startDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
          //console.log(value)
          //console.log('beforeDate', value.length);
          returnData.beforeDate = value;
          _.forEach(data, function (d, i) {
            returnData.beforeDate.push(d);
          });
          dataBetweenDate(minDate, endDate, deployment_id, tenant_id, storeId, isKitchen).then(function (value) {
            //console.log(value);
            //console.log('betweenDate', value.length);
            returnData.betweenDate = value;
            deferred.resolve(returnData);
          }, function (err) {
            console.log(err);
            deferred.reject(err);
          });
        }, function (err) {
          console.log(err);
          deferred.reject(err);
        });
      }
    }
    else {
      deferred.resolve(returnData);
    }

  }, function (err) {
    console.log(err);
    returnData.errorMessage = err;
    //console.log("Report not generated due to latest opening is greater than selected end Date");
    deferred.resolve(returnData);
    //return handleError(res, err);
  });
  return deferred.promise;
}

//===============================================Utility Functions=========================================

function getResetDate(settings,date){
 var resetDate=convertDate(date);
  var hr=0;
  var min = 0;
  for(var i=0;i<settings.length;i++){
    if(settings[i].name=='day_sale_cutoff'){
      hr=convertDate(settings[i].value).getHours();
      min = convertDate(settings[i].value).getMinutes();
      //console.log(this.convertDate(settings[i].value),hr)
      break;
    }
  }
  resetDate.setHours(hr,min,0,0);
  return resetDate;
}

function convertDate(d) {
  return (
    d.constructor === Date ? d :
      d.constructor === Array ? new Date(d[0], d[1], d[2]) :
        d.constructor === Number ? new Date(d) :
          d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year, d.month, d.date) :
              NaN
  );
}

function getDateFormatted(datetime) {
  // if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
  //   var datetime = datetime.toString();
  //   var cdate = new Date(datetime.replace(' ', 'T'));
  // }
  // else{
  //   var cdate = datetime;
  // }
  var cdate = typeof(datetime)=='string'?new Date(datetime):datetime;
  var month = (cdate.getMonth() + 1).toString();
  var day = (cdate.getDate()).toString();
  var hrs = cdate.getHours().toString();
  var minutes = cdate.getMinutes().toString();
  var ss = cdate.getSeconds().toString();
  month = ((month.length == 1) ? "0" + month : month);
  day = ((day.length == 1) ? "0" + day : day);
  hrs = ((hrs.length == 1) ? "0" + hrs : hrs);
  minutes = ((minutes.length == 1) ? "0" + minutes : minutes);
  ss = ((ss.length == 1) ? "0" + ss : ss);
  //alert(day);
  //if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)
  //  return cdate.getFullYear() + "/" + month + "/" + day + " " + hrs + ":" + minutes + ":" + ss;
  //else
    return cdate.getFullYear() + "-" + month + "-" + day + " " + hrs + ":" + minutes + ":" + ss;
}

function roundNumber (number, decimals) {
  var value = number;
  var exp = decimals;
  if (typeof exp === 'undefined' || +exp === 0)
    return Math.round(value);
  value = +value;
  exp = +exp;
  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
    return NaN;
  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));
  // Shift back
  value = value.toString().split('e');
 // return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  var rounded= +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  return parseFloat( rounded.toFixed(decimals));
}

function renderExcelConsumptionReport (data, email, tenant_id, deployment_id) {
  //console.log(JSON.stringify(data.items));
  var wb = new xl.WorkBook(options);
  var ws = wb.WorkSheet('Consumption Summary', wsOpts);

  var myStyle = wb.Style();
  myStyle.Font.Underline();
  myStyle.Font.Bold();
  myStyle.Font.Italics();
  myStyle.Font.Size(18);
  myStyle.Font.Family('Helvetica');
  myStyle.Font.Color('#A9A9F5');
  myStyle.Number.Format("$#,##0.00;($#,##0.00);-");
  myStyle.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );

  var myStyle2 = wb.Style();
  myStyle2.Font.Size(16);
  myStyle2.Font.Color('#404040');
  myStyle2.Fill.Pattern('solid');
  myStyle2.Fill.Color('#B8CCE4');
  var myStyle3 = wb.Style();
  myStyle3.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle3.Font.Alignment.Vertical('top');
  myStyle3.Font.Alignment.Horizontal('left');
  myStyle3.Font.WrapText(true);
  myStyle3.Font.Color('#222222');

  var myStyle4 = wb.Style();
  myStyle4.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle4.Font.Alignment.Horizontal('right');
  myStyle4.Font.Color('FF222222');

  var myStyle5 = wb.Style();
  myStyle5.Font.Alignment.Vertical('top');
  myStyle5.Font.Alignment.Horizontal('right');
  myStyle5.Font.Bold();
  myStyle5.Font.WrapText(true);
  myStyle5.Fill.Pattern('solid');
  myStyle5.Fill.Color('FF888888');

  var myStyle6 = wb.Style();
  myStyle6.Font.Size(16);
  myStyle6.Border(
    {
      top:{
        style:'thin'
      },
      bottom:{
        style:'thin'
      }
    }
  );
  myStyle6.Font.Alignment.Vertical('top');
  myStyle6.Font.Alignment.Horizontal('right');
  myStyle6.Font.Bold();
  myStyle6.Font.WrapText(true);
  myStyle6.Fill.Pattern('solid');
  myStyle6.Fill.Color('FF888888');
  myStyle6.Fill.Color('#B8CCE4');
  ws.Cell(1,2).String("Opening Balance").Style(myStyle);
  ws.Cell(1,6).String("Purchase").Style(myStyle);
  ws.Cell(1,9).String("Consumption").Style(myStyle);
  ws.Cell(1,12).String("Closing Balance").Style(myStyle);
  ws.Row(2).Freeze();
  ws.Cell(2, 1).String("Item Name").Style(myStyle2);
  ws.Cell(2, 2).String("Date").Style(myStyle2);
  ws.Cell(2, 3).String("Qty").Style(myStyle2);
  ws.Cell(2, 4).String("Unit").Style(myStyle2);
  ws.Cell(2, 5).String("Value").Style(myStyle2);
  ws.Cell(2, 6).String("Qty").Style(myStyle2);
  ws.Cell(2, 7).String("Unit").Style(myStyle2);
  ws.Cell(2, 8).String("Value").Style(myStyle2);
  ws.Cell(2, 9).String("Qty").Style(myStyle2);
  ws.Cell(2, 10).String("Unit").Style(myStyle2);
  ws.Cell(2, 11).String("Value").Style(myStyle2);
  ws.Cell(2, 12).String("Date").Style(myStyle2);
  ws.Cell(2, 13).String("Qty").Style(myStyle2);
  ws.Cell(2, 14).String("Unit").Style(myStyle2);
  ws.Cell(2, 15).String("Value").Style(myStyle2);
  ws.Row(3).Freeze();
  var currentRow = 3;
  _.forEach(data.items, function (item) {
    ws.Cell(currentRow, 1).String(item.itemName).Style(myStyle3);
    ws.Cell(currentRow, 2).String(moment(item.openingDate).format('DD/MM/YYYY')).Style(myStyle3);
    ws.Cell(currentRow, 3).Number(isNaN(item.openingQty)? 0: item.openingQty).Style(myStyle3);
    ws.Cell(currentRow, 4).String(item.UnitName).Style(myStyle3);
    ws.Cell(currentRow, 5).Number(isNaN(item.openingAmt)? 0: item.openingAmt).Style(myStyle3);
    ws.Cell(currentRow, 6).Number(isNaN(item.purchaseQty)? 0: item.purchaseQty).Style(myStyle3);
    ws.Cell(currentRow, 7).String(item.UnitName).Style(myStyle3);
    ws.Cell(currentRow, 8).Number(isNaN(item.purchaseAmount)? 0: item.purchaseAmount).Style(myStyle3);
    ws.Cell(currentRow, 9).Number(isNaN(item.consumptionQty)?0: item.consumptionQty).Style(myStyle3);
    ws.Cell(currentRow, 10).String(item.UnitName).Style(myStyle3);
    ws.Cell(currentRow, 11).Number(isNaN(item.consumptionAmount)?0: item.consumptionAmount).Style(myStyle3);
    ws.Cell(currentRow, 12).String(moment(item.closingDate).format('DD/MM/YYYY')).Style(myStyle3);
    ws.Cell(currentRow, 13).Number(isNaN(item.closingQty)?0: item.closingQty).Style(myStyle3);
    ws.Cell(currentRow, 14).String(item.UnitName).Style(myStyle3);
    ws.Cell(currentRow, 15).Number(isNaN(item.closingAmount)?0:item.closingAmount).Style(myStyle3);
    currentRow++;
  });  

  var type = "Consumption Report";
  var _filename = "Consumption Summary-" + Date.now() + ".xlsx";
  var _p = path.join(process.cwd(), 'uploads', _filename);
  //console.log(_filename);
  // console.log(_p);


  wb.write(_p, function(err){
    if(err){
      console.log(err);
      /* //res.json(200, {built: false, filename: ''});
       } else {
       res.json(200, {built: true, filename: _filename});*/
    }
    emailSend({email: email},_filename,_p, tenant_id, deployment_id, "Your report is ready for download. Please use foloowing link - " + baseUrl + "/api/uploads/" + _filename)
  });
}

function renderExcelStockSummary (data, email, tenant_id, deployment_id) {
  //console.log(JSON.stringify(data.items));
  var wb = new xl.WorkBook(options);
  var ws = wb.WorkSheet('Consumption Summary', wsOpts);

  var myStyle = wb.Style();
  myStyle.Font.Underline();
  myStyle.Font.Bold();
  myStyle.Font.Italics();
  myStyle.Font.Size(18);
  myStyle.Font.Family('Helvetica');
  myStyle.Font.Color('#A9A9F5');
  myStyle.Number.Format("$#,##0.00;($#,##0.00);-");
  myStyle.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );

  var myStyle2 = wb.Style();
  myStyle2.Font.Size(16);
  myStyle2.Font.Color('#404040');
  myStyle2.Fill.Pattern('solid');
  myStyle2.Fill.Color('#B8CCE4');
  var myStyle3 = wb.Style();
  myStyle3.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle3.Font.Alignment.Vertical('top');
  myStyle3.Font.Alignment.Horizontal('left');
  myStyle3.Font.WrapText(true);
  myStyle3.Font.Color('#222222');

  var myStyle4 = wb.Style();
  myStyle4.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle4.Font.Alignment.Horizontal('right');
  myStyle4.Font.Color('FF222222');

  var myStyle5 = wb.Style();
  myStyle5.Font.Alignment.Vertical('top');
  myStyle5.Font.Alignment.Horizontal('right');
  myStyle5.Font.Bold();
  myStyle5.Font.WrapText(true);
  myStyle5.Fill.Pattern('solid');
  myStyle5.Fill.Color('FF888888');

  var myStyle6 = wb.Style();
  myStyle6.Font.Size(16);
  myStyle6.Border(
    {
      top:{
        style:'thin'
      },
      bottom:{
        style:'thin'
      }
    }
  );
  myStyle6.Font.Alignment.Vertical('top');
  myStyle6.Font.Alignment.Horizontal('right');
  myStyle6.Font.Bold();
  myStyle6.Font.WrapText(true);
  myStyle6.Fill.Pattern('solid');
  myStyle6.Fill.Color('FF888888');
  myStyle6.Fill.Color('#B8CCE4');
  ws.Cell(1,2).String("Opening Balance").Style(myStyle);
  ws.Cell(1,6).String("Closing Balance").Style(myStyle);
  ws.Row(2).Freeze();
  ws.Cell(2, 1).String("Item Name").Style(myStyle2);
  ws.Cell(2, 2).String("Date").Style(myStyle2);
  ws.Cell(2, 3).String("Qty").Style(myStyle2);
  ws.Cell(2, 4).String("Unit").Style(myStyle2);
  ws.Cell(2, 5).String("Value").Style(myStyle2);
  ws.Cell(2, 6).String("Date").Style(myStyle2);
  ws.Cell(2, 7).String("Qty").Style(myStyle2);
  ws.Cell(2, 8).String("Unit").Style(myStyle2);
  ws.Cell(2, 9).String("Value").Style(myStyle2);
  ws.Row(3).Freeze();
  var currentRow = 3;
  _.forEach(data.items, function (item) {
    ws.Cell(currentRow, 1).String(item.itemName).Style(myStyle3);
    ws.Cell(currentRow, 2).String(moment(item.openingDate).format('DD/MM/YYYY')).Style(myStyle3);
    ws.Cell(currentRow, 3).Number(isNaN(item.openingQty)? 0: item.openingQty).Style(myStyle3);
    ws.Cell(currentRow, 4).String(item.UnitName).Style(myStyle3);
    ws.Cell(currentRow, 5).Number(isNaN(item.openingAmt)? 0: item.openingAmt).Style(myStyle3);
    ws.Cell(currentRow, 6).String(moment(item.closingDate).format('DD/MM/YYYY')).Style(myStyle3);
    ws.Cell(currentRow, 7).Number(isNaN(item.closingQty)?0: item.closingQty).Style(myStyle3);
    ws.Cell(currentRow, 8).String(item.UnitName).Style(myStyle3);
    ws.Cell(currentRow, 9).Number(isNaN(item.closingAmount)?0:item.closingAmount).Style(myStyle3);
    currentRow++;
  });  

  var type = "Stock Summary Report";
  var _filename = "Stock Summary-" + Date.now() + ".xlsx";
  var _p = path.join(process.cwd(), 'uploads', _filename);
  //console.log(_filename);
  // console.log(_p);


  wb.write(_p, function(err){
    if(err){
      console.log(err);
      /* //res.json(200, {built: false, filename: ''});
       } else {
       res.json(200, {built: true, filename: _filename});*/
    }
    emailSend({email: email},_filename,_p, tenant_id, deployment_id, "Your report is ready for download. Please use foloowing link - ")
  });
}

function renderExcelItemWiseConsumption (data, email, tenant_id, deployment_id) {
  //console.log(JSON.stringify(data));
  var wb = new xl.WorkBook(options);
  var ws = wb.WorkSheet('Item Wise Consumption', wsOpts);

  var myStyle = wb.Style();
  myStyle.Font.Underline();
  myStyle.Font.Bold();
  myStyle.Font.Italics();
  myStyle.Font.Size(18);
  myStyle.Font.Family('Helvetica');
  myStyle.Font.Color('#A9A9F5');
  myStyle.Number.Format("$#,##0.00;($#,##0.00);-");
  myStyle.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );

  var myStyle2 = wb.Style();
  myStyle2.Font.Size(16);
  myStyle2.Font.Color('#404040');
  myStyle2.Fill.Pattern('solid');
  myStyle2.Fill.Color('#B8CCE4');
  var myStyle3 = wb.Style();
  myStyle3.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle3.Font.Alignment.Vertical('top');
  myStyle3.Font.Alignment.Horizontal('left');
  myStyle3.Font.WrapText(true);
  myStyle3.Font.Color('#222222');

  var myStyle4 = wb.Style();
  myStyle4.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle4.Font.Alignment.Horizontal('right');
  myStyle4.Font.Color('FF222222');

  var myStyle5 = wb.Style();
  myStyle5.Font.Alignment.Vertical('top');
  myStyle5.Font.Alignment.Horizontal('right');
  myStyle5.Font.Bold();
  myStyle5.Font.WrapText(true);
  myStyle5.Fill.Pattern('solid');
  myStyle5.Fill.Color('FF888888');

  var myStyle6 = wb.Style();
  myStyle6.Font.Size(16);
  myStyle6.Border(
    {
      top:{
        style:'thin'
      },
      bottom:{
        style:'thin'
      }
    }
  );
  myStyle6.Font.Alignment.Vertical('top');
  myStyle6.Font.Alignment.Horizontal('right');
  myStyle6.Font.Bold();
  myStyle6.Font.WrapText(true);
  myStyle6.Fill.Pattern('solid');
  myStyle6.Fill.Color('FF888888');
  myStyle6.Fill.Color('#B8CCE4');
  ws.Cell(1,1).String("Products").Style(myStyle);
  ws.Cell(1,2).String("Consumption").Style(myStyle);
  ws.Cell(1,3).String("Unit").Style(myStyle);
  ws.Cell(1,4).String("Value").Style(myStyle);
  
  var currentRow = 2;
  _.forEach(data, function (item) {
    //console.log('11944', itm.items);
    ws.Cell(currentRow,1).String(item.itemName).Style(myStyle2);
    var totalQty = 0;
    var unit = item.UnitName;
    var totalAmount = 0;
    currentRow++;
    //console.log(item.items);
    _.forEach(item.items, function (ditm) {
      console.log('11951', ditm);
      ws.Cell(currentRow, 1).String(ditm.itemName).Style(myStyle3);
      if(isNaN(ditm.consumptionQty))
        ws.Cell(currentRow, 2).Number(0).Style(myStyle3);
      else
        ws.Cell(currentRow, 2).Number(ditm.consumptionQty).Style(myStyle3);
      ws.Cell(currentRow, 3).String(ditm.UnitName || unit).Style(myStyle3);
      if(isNaN(ditm.consumptionAmount))
        ws.Cell(currentRow, 4).Number(0).Style(myStyle3);  
      else
        ws.Cell(currentRow, 4).Number(ditm.consumptionAmount).Style(myStyle3);
      totalQty += isNaN(ditm.consumptionQty)? 0: Number(ditm.consumptionQty);
      totalAmount += isNaN(ditm.consumptionAmount)? 0: ditm.consumptionAmount;
      currentRow++;
    });
    ws.Cell(currentRow, 1).String("Total").Style(myStyle4);
    ws.Cell(currentRow, 2).Number(totalQty).Style(myStyle4);
    ws.Cell(currentRow, 3).String(unit).Style(myStyle4);
    ws.Cell(currentRow, 4).Number(totalAmount).Style(myStyle4);
    currentRow++;
  })

  var type = "Item Wise Consumption Report";
  var _filename = "Item Wise Consumption-" + Date.now() + ".xlsx";
  var _p = path.join(process.cwd(), 'uploads', _filename);
  //console.log(_filename);
  // console.log(_p);


  wb.write(_p, function(err){
    if(err){
      console.log(err);
      /* //res.json(200, {built: false, filename: ''});
       } else {
       res.json(200, {built: true, filename: _filename});*/
    }
    emailSend({email: email},_filename,_p, tenant_id, deployment_id, "Your report is ready for download. Please use foloowing link - ")
  });
}

function renderExcelVarianceConsolidated (data, email, tenant_id, deployment_id) {
  //console.log(JSON.stringify(data));
  var wb = new xl.WorkBook(options);
  var ws = wb.WorkSheet('Variance Consolidated', wsOpts);

  var myStyle = wb.Style();
  myStyle.Font.Underline();
  myStyle.Font.Bold();
  myStyle.Font.Italics();
  myStyle.Font.Size(18);
  myStyle.Font.Family('Helvetica');
  myStyle.Font.Color('#A9A9F5');
  myStyle.Number.Format("$#,##0.00;($#,##0.00);-");
  myStyle.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );

  var myStyle2 = wb.Style();
  myStyle2.Font.Size(16);
  myStyle2.Font.Color('#404040');
  myStyle2.Fill.Pattern('solid');
  myStyle2.Fill.Color('#B8CCE4');
  var myStyle3 = wb.Style();
  myStyle3.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle3.Font.Alignment.Vertical('top');
  myStyle3.Font.Alignment.Horizontal('left');
  myStyle3.Font.WrapText(true);
  myStyle3.Font.Color('#222222');

  var myStyle4 = wb.Style();
  myStyle4.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle4.Font.Alignment.Horizontal('right');
  myStyle4.Font.Color('FF222222');

  var myStyle5 = wb.Style();
  myStyle5.Font.Alignment.Vertical('top');
  myStyle5.Font.Alignment.Horizontal('right');
  myStyle5.Font.Bold();
  myStyle5.Font.WrapText(true);
  myStyle5.Fill.Pattern('solid');
  myStyle5.Fill.Color('FF888888');

  var myStyle6 = wb.Style();
  myStyle6.Font.Size(16);
  myStyle6.Border(
    {
      top:{
        style:'thin'
      },
      bottom:{
        style:'thin'
      }
    }
  );
  myStyle6.Font.Alignment.Vertical('top');
  myStyle6.Font.Alignment.Horizontal('right');
  myStyle6.Font.Bold();
  myStyle6.Font.WrapText(true);
  myStyle6.Fill.Pattern('solid');
  myStyle6.Fill.Color('FF888888');
  myStyle6.Fill.Color('#B8CCE4');
  ws.Cell(1,1).String("Item Name").Style(myStyle);
  ws.Cell(1,2).String("Opening Stock").Style(myStyle);
  ws.Cell(1,3).String("Purchases").Style(myStyle);
  ws.Cell(1,4).String("Consumption").Style(myStyle);
  ws.Cell(1,5).String("Wastage").Style(myStyle);
  ws.Cell(1,6).String("Closing Qty").Style(myStyle);
  ws.Cell(1,7).String("Physical Stock").Style(myStyle);
  ws.Cell(1,8).String("Variance").Style(myStyle);
  ws.Cell(1,9).String("Unit").Style(myStyle);
  ws.Cell(1,10).String("Variance(%)").Style(myStyle);
  
  var currentRow = 2;
  _.forEach(data.items, function (item) {
    ws.Cell(currentRow, 1).String(item.itemName).Style(myStyle2);
    ws.Cell(currentRow, 2).Number(item.openingQty).Style(myStyle4);
    ws.Cell(currentRow, 3).Number(item.purchaseQty).Style(myStyle4);
    ws.Cell(currentRow, 4).String(item.consumptionQty).Style(myStyle4);
    ws.Cell(currentRow, 5).Number(item.wastageQty).Style(myStyle4);
    ws.Cell(currentRow, 6).Number(item.closingQty).Style(myStyle4);
    //console.log('render phys', item.physicalQty);
    if(isNaN(item.physicalQty))
      ws.Cell(currentRow, 7).String(item.physicalQty).Style(myStyle4);  
    else
      ws.Cell(currentRow, 7).Number(item.physicalQty).Style(myStyle4);
    if(isNaN(item.varianceQty))
      ws.Cell(currentRow, 8).String(item.varianceQty).Style(myStyle4);
    else
      ws.Cell(currentRow, 8).Number(item.varianceQty).Style(myStyle4);
    ws.Cell(currentRow, 9).String(item.UnitName).Style(myStyle4);
    if(isNaN(item.varianceInPercent))
      ws.Cell(currentRow, 10).String(item.varianceInPercent).Style(myStyle4);
    else
      ws.Cell(currentRow, 10).Number(item.varianceInPercent).Style(myStyle4);
    currentRow++;
  })

  var type = "Variance Report";
  var _filename = "Variance Consolidated-" + Date.now() + ".xlsx";
  var _p = path.join(process.cwd(), 'uploads', _filename);
  // console.log(_filename);
  // console.log(_p);


  wb.write(_p, function(err){
    if(err){
      console.log(err);
      /* //res.json(200, {built: false, filename: ''});
       } else {
       res.json(200, {built: true, filename: _filename});*/
    }
    emailSend({email: email},_filename,_p, tenant_id, deployment_id, "Your report is ready for download. Please use foloowing link - ");
  });
}

function emailSend(body,filename,_p, tenant_id, deployment_id, text){

  //console.log(req.body);
  var data = {
    text: "from ",
    from: "Posist Info <no-reply@posist.info>",
    to: body.email,
    subject: "Email You Requested For.",
  }
  if(text)
    data.text = text;
  if(filename && _p){
    data.attachment = [
      {data: "<html><body><p>" + filename + " is ready to be downloaded. Please use following link.</p><a href='"+ baseUrl + "/api/uploads/" + filename + "'>" + baseUrl + "/api/uploads/" + filename + "</a></body></html>", alternative: true},
      //{path:_p, type:"application/xlsx", name: filename}
    ]
  }
  //console.log(data);
  server.send(data, function (error, message) {
    /*console.log(message);*/
    // var e = JSON.parse(err);
    // console.log(e);
    // console.log(e.Error);
    console.log(error);
    if(!error) {
      Stock.StockConsumptionReport.create({tenant_id: tenant_id, deployment_id: deployment_id, email: body.email, reportName: filename, created: new Date(), status: true}, function (err, report) {
        //console.log('report ' + filename + ' mailed successully');
      });
    } else {
      Stock.StockConsumptionReport.create({tenant_id: tenant_id, deployment_id: deployment_id, email: body.email, reportName: filename, created: new Date(), status: false}, function (err, report) {
        //console.log('failed to mail report ' + filename + ' due to ' + error);
      });
    }
    //res.json(201, err || message);
  });
}

function renderExcelDateWiseVariance (data, email , days, tenant_id, deployment_id) {
  //console.log(JSON.stringify(data.items));
  //console.log('render', JSON.stringify(data))
  var wb = new xl.WorkBook(options);
  var ws = wb.WorkSheet('Date Wise Variance', wsOpts);

  var myStyle = wb.Style();
  myStyle.Font.Underline();
  myStyle.Font.Bold();
  myStyle.Font.Italics();
  myStyle.Font.Size(18);
  myStyle.Font.Family('Helvetica');
  myStyle.Font.Color('#A9A9F5');
  myStyle.Number.Format("$#,##0.00;($#,##0.00);-");
  myStyle.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );

  var myStyle2 = wb.Style();
  myStyle2.Font.Size(16);
  myStyle2.Font.Color('#404040');
  myStyle2.Fill.Pattern('solid');
  myStyle2.Fill.Color('#B8CCE4');
  var myStyle3 = wb.Style();
  myStyle3.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle3.Font.Alignment.Vertical('top');
  myStyle3.Font.Alignment.Horizontal('left');
  myStyle3.Font.WrapText(true);
  myStyle3.Font.Color('#222222');

  var myStyle4 = wb.Style();
  myStyle4.Border(
    {
      left: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      },
      top: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      }
    }
  );
  myStyle4.Font.Alignment.Horizontal('right');
  myStyle4.Font.Color('FF222222');

  var myStyle5 = wb.Style();
  myStyle5.Font.Alignment.Vertical('top');
  myStyle5.Font.Alignment.Horizontal('right');
  myStyle5.Font.Bold();
  myStyle5.Font.WrapText(true);
  myStyle5.Fill.Pattern('solid');
  myStyle5.Fill.Color('FF888888');

  var myStyle6 = wb.Style();
  myStyle6.Font.Size(16);
  myStyle6.Border(
    {
      top:{
        style:'thin'
      },
      bottom:{
        style:'thin'
      }
    }
  );
  myStyle6.Font.Alignment.Vertical('top');
  myStyle6.Font.Alignment.Horizontal('right');
  myStyle6.Font.Bold();
  myStyle6.Font.WrapText(true);
  myStyle6.Fill.Pattern('solid');
  myStyle6.Fill.Color('FF888888');
  myStyle6.Fill.Color('#B8CCE4');
  ws.Cell(1,1).String("Item Name").Style(myStyle);  
  ws.Cell(1,2).String("Opening Balance").Style(myStyle);  
  ws.Cell(1,3).String("Purchases").Style(myStyle);  
  ws.Cell(1,4).String("Consumption").Style(myStyle);  
  ws.Cell(1,5).String("Wastage").Style(myStyle);  
  ws.Cell(1,6).String("Closing Qty").Style(myStyle);  
  ws.Cell(1,7).String("Physical Stock").Style(myStyle);  
  ws.Cell(1,8).String("Variance").Style(myStyle);  
  ws.Cell(1,9).String("Unit").Style(myStyle);  
  ws.Cell(1,10).String("Variance(In %)").Style(myStyle);  
  ws.Row(2).Freeze();
  var currentRow = 2;
  _.forEach(data, function (d) {
    //console.log(d)
    var date=d.date.getDate() + '/' + (d.date.getMonth()+1) + '/' + d.date.getFullYear();
    ws.Cell(currentRow, 1).String(date).Style(myStyle2);
    ws.Cell(currentRow, 2).String("").Style(myStyle2);
    ws.Cell(currentRow, 3).String("").Style(myStyle2);
    ws.Cell(currentRow, 4).String("").Style(myStyle2);
    ws.Cell(currentRow, 5).String("").Style(myStyle2);
    ws.Cell(currentRow, 6).String("").Style(myStyle2);
    ws.Cell(currentRow, 7).String("").Style(myStyle2);
    ws.Cell(currentRow, 8).String("").Style(myStyle2);
    ws.Cell(currentRow, 9).String("").Style(myStyle2);
    ws.Cell(currentRow, 10).String("").Style(myStyle2);
    currentRow++;
    _.forEach(d.items, function (item) {
      ws.Cell(currentRow, 1).String(item.itemName).Style(myStyle3);
      ws.Cell(currentRow, 2).String(item.openingQty).Style(myStyle3);
      ws.Cell(currentRow, 3).String(item.purchaseQty).Style(myStyle3);
      ws.Cell(currentRow, 4).String(item.consumptionQty).Style(myStyle3);
      ws.Cell(currentRow, 5).String(item.wastageQty).Style(myStyle3);
      ws.Cell(currentRow, 6).String(item.closingQty).Style(myStyle3);
      ws.Cell(currentRow, 7).String(item.physicalQty).Style(myStyle3);
      ws.Cell(currentRow, 8).String(item.varianceQty).Style(myStyle3);
      ws.Cell(currentRow, 9).String(item.UnitName).Style(myStyle3);
      ws.Cell(currentRow, 10).String(item.varianceInPercent).Style(myStyle3);
      currentRow++;
    });
  });

  var type = "Date Wise Variance Report";
  var _filename = "Date Wise Variance-" + Date.now() + ".xlsx";
  var _p = path.join(process.cwd(), 'uploads', _filename);
  //console.log(_filename);
  // console.log(_p);


  wb.write(_p, function(err){
    if(err){
      console.log(err);
      /* //res.json(200, {built: false, filename: ''});
       } else {
       res.json(200, {built: true, filename: _filename});*/
    }
    emailSend({email: email},_filename,_p, tenant_id, deployment_id, "Your report is ready for download. Please use foloowing link - " + baseUrl + "/api/uploads/" + _filename)
  });
}

function formatDataForVarianceReport(data, deployment, fromDate, toDate) {
  var startDate = fromDate;
  var endDate = toDate;
  var result = data;
  var deployment = deployment;
  var count =0;
  var count2 = 0;
  result.items = [];
  var endDateToShow = new Date(endDate);
  endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
  _.forEach(result.beforeDate,function(itm,i){
    if(itm.totalOpeningQty!=undefined){
      if(isNaN(itm.totalOpeningQty))
        itm.totalOpeningQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
        itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
      }
    }
    else if(itm.totalBalanceQty!=undefined){
      if(isNaN(itm.totalBalanceQty))
        itm.totalBalanceQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
        itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        //console.log('total balance', itm.totalBalanceQty);
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
      }
    }
    else if(itm.totalPurchaseQty!=undefined){
      if(isNaN(itm.totalPurchaseQty))
        itm.totalPurchaseQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
        itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
      }
    }
    else if(itm.totalIntermediateQty!=undefined){
      if(isNaN(itm.totalIntermediateQty))
        itm.totalIntermediateQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
        itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
      }
    }
    else if(itm.totalSaleQty!=undefined){
      if(isNaN(itm.totalSaleQty))
        itm.totalSaleQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
        itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
      }
    }
    else if(itm.totalWastageQty!=undefined){
      if(isNaN(itm.totalWastageQty))
        itm.totalWastageQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
        itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
      }
    }
    else if(itm.totalTransferQty!=undefined){
      if(isNaN(itm.totalTransferQty))
        itm.totalTransferQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
        itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
      }
    }
    else if(itm.totalTransferQty_toStore!=undefined){
      if(isNaN(itm.totalTransferQty_toStore))
        itm.totalTransferQty_toStore = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
      }
    }
    else if(itm.itemSaleQty!=undefined){
      if(isNaN(itm.itemSaleQty))
        itm.itemSaleQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
        itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        itm.UnitName=itm.baseUnit;
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
      }
    }
    else if(itm.totalStockReturnQty!=undefined){
      if(isNaN(itm.totalStockReturnQty))
        itm.totalStockReturnQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
        itm.openingAmt=parseFloat(-itm.totalStockReturnAmount).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
      }
    }
    else if(itm.totalStockReceiveQty!=undefined){
      if(isNaN(itm.totalStockReceiveQty))
        itm.totalStockReceiveQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
        itm.openingAmt=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
      }
    }

  });
  _.forEach(result.betweenDate,function(itm,i){
    if(itm.totalPurchaseQty!=undefined){
      if(isNaN(itm.totalPurchaseQty))
        itm.totalPurchaseQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.consumptionQty==undefined){
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
        itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
      }
    }
    else if(itm.totalBalanceQty!=undefined){
      //console.log('totalBalanceQty', itm.totalBalanceQty);
      if(isNaN(itm.totalBalanceQty))
        itm.totalBalanceQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //console.log('itm', itm);
      var cDate=new Date(itm.created);
      //cDate.setDate(cDate.getDate()-1);
      //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
      //var ed = new Date(endDate);
      //ed = new Date(ed.setDate(ed.getDate()-1));
      //console.log(cDate);
      //console.log(endDate);
      //console.log(ed);
      if(itmIndex<0){
        itm.openingQty=parseFloat(0).toFixed(3);
        itm.openingAmt=parseFloat(0).toFixed(2);
        itm.purchaseQty=parseFloat(0).toFixed(3);
        itm.purchaseAmount=parseFloat(0).toFixed(2);
        itm.consumptionQty=parseFloat(0).toFixed(3);
        itm.consumptionAmount=parseFloat(0).toFixed(2);
        itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
        itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
        itm.wastageQty = parseFloat(0).toFixed(3);
        //itm.totalBalanceQty = parseFloat(0).toFixed(3);
        itm.wastageAmt = parseFloat(0).toFixed(2);
        itm.varianceQty = "NA";
        itm.varianceInPercent = "NA";
        itm.physicalQty = "NA";
        //console.log('physical--------', endDate, cDate);
        if(endDate.getTime()==cDate.getTime()){
          //console.log('end = c');
          itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
          if(isNaN(itm.physicalQty))
            count2++;
          //console.log('phys', itm.physicalQty);
          //console.log('iphys', itm.physicalQty);
          itm.physicalAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
          itm.varianceQty = parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
          itm.varianceInPercent = parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
          if(itm.varianceInPercent == -Infinity)
            itm.varianceInPercent = -100;
          if(itm.varianceInPercent == Infinity)
            itm.varianceInPercent = 100;
        }
        else{
          itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
          itm.physicalDate = new Date(itm.created);
          itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);

          // if(result.items[itmIndex].physicalDate) {
          //   console.log(result.items[itmIndex].physicalDate)
          //   console.log(cDate)
          //   if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
          //     console.log('if');
          //     result.items[itmIndex].closingQty = Utils.roundNumber(parseFloat(result.items[itmIndex].closingQty), 3) + Utils.roundNumber(parseFloat(itm.totalBalanceQty), 3);
          //     console.log(result.items[itmIndex].closingQty);
          //   } else {
          //     console.log('else');
          //     result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
          //     result.items[itmIndex].physicalDate = cDate;
          //   }
          // } else {
          //   result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
          //   result.items[itmIndex].physicalDate = cDate;
          // }
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.openingDate=new Date(startDate);
        itm.closingDate=new Date(endDateToShow);
        //console.log('phys', itm.physicalQty);
        result.items.push(itm);
      }
      else
      {
        if(endDate.getTime()==cDate.getTime()){

          result.items[itmIndex].physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
          result.items[itmIndex].physicalAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
          result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
          result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);
          if(isNaN(result.items[itmIndex].physicalQty))
            count++;
          //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
          //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
        }
        else {
          //console.log(itm.itemName, " else");
          //console.log(result.items[itmIndex].closingQty);
          //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
          if(result.items[itmIndex].physicalDate) {
            //console.log(result.items[itmIndex].physicalDate)
            //console.log(cDate)
            if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
              //console.log('if');
              result.items[itmIndex].closingQty = Utils.roundNumber(parseFloat(result.items[itmIndex].closingQty), 3) + Utils.roundNumber(parseFloat(itm.totalBalanceQty), 3);
              //console.log(result.items[itmIndex].closingQty);
            } else {
              //console.log('else');
              result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].physicalDate = cDate;
            }
          } else {
            result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
            result.items[itmIndex].physicalDate = cDate;
          }
          // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
          // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
          // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
        }
      }
    }
    else if(itm.totalIntermediateQty!=undefined){
      if(isNaN(itm.totalIntermediateQty))
        itm.totalIntermediateQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.purchaseQty==undefined){
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
        itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIntermediateAmt)).toFixed(2);
      }
    }
    else if(itm.totalSaleQty!=undefined){
      if(isNaN(itm.totalSaleQty))
        itm.totalSaleQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.purchaseQty==undefined){
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
        itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
      }
    }
    else if(itm.totalWastageQty!=undefined){
      if(isNaN(itm.totalWastageQty))
        itm.totalWastageQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.purchaseQty==undefined){
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
        itm.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].wastageQty=parseFloat(parseFloat(result.items[itmIndex].wastageQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
        result.items[itmIndex].wastageAmount=parseFloat(parseFloat(result.items[itmIndex].wastageAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
      }
    }
    else if(itm.totalTransferQty_toStore!=undefined){
      if(isNaN(itm.totalTransferQty_toStore))
        itm.totalTransferQty_toStore = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.consumptionQty==undefined){
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
      }
    }
    else if(itm.totalTransferQty!=undefined){
      if(isNaN(itm.totalTransferQty))
        itm.totalTransferQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.purchaseQty==undefined){
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
        itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
      }
    }
    else if(itm.itemSaleQty!=undefined){
      if(isNaN(itm.itemSaleQty))
        itm.itemSaleQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.purchaseQty==undefined){
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
        itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
        itm.UnitName=itm.baseUnit;
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
      }
    }
    else if(itm.totalStockReceiveQty!=undefined){
      if(isNaN(itm.totalStockReceiveQty))
        itm.totalStockReceiveQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.consumptionQty==undefined){
          itm.consumptionQty=parseFloat(0).toFixed(3);
          itm.consumptionAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.purchaseQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
        itm.purchaseAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        //console.log(itmIndex);
        result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
      }
    }
    else if(itm.totalStockReturnQty!=undefined){
      if(isNaN(itm.totalStockReturnQty))
        itm.totalStockReturnQty = 0;
      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      if(itmIndex<0){
        if(itm.openingQty==undefined){
          itm.openingQty=parseFloat(0).toFixed(3);
          itm.closingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
          itm.openingAmt=parseFloat(0).toFixed(3);
          itm.closingAmount=parseFloat(-itm.totalStockReturnAmount).toFixed(3);
          itm.wastageQty = parseFloat(0).toFixed(3);
          itm.totalBalanceQty = parseFloat(0).toFixed(3);
          itm.wastageAmt = parseFloat(0).toFixed(2);
          itm.varianceQty = "NA";
          itm.varianceInPercent = "NA";
          itm.physicalQty = "NA";
          itm.openingDate=new Date(startDate);
          itm.closingDate=new Date(endDateToShow);
        }
        if(itm.purchaseQty==undefined){
          itm.purchaseQty=parseFloat(0).toFixed(3);
          itm.purchaseAmount=parseFloat(0).toFixed(3);
        }
        if(itm.wastageQty == undefined) {
          itm.wastageQty=parseFloat(0).toFixed(3);
          itm.wastageAmount=parseFloat(0).toFixed(3);
        }
        if(itm.varianceQty == undefined) {
          itm.varianceQty=parseFloat(0).toFixed(3);
        }
        itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
        itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
        result.items.push(itm);
      }
      else
      {
        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);

        result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
        result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
      }
    }

  });
console.log('count :-   ',count);
console.log('count2 :-   ',count2);
  return result;
}

function convertItemInPreferredUnitVariance (data) {
  var result = data;
  var items = $scope.stockitems;
  var count3=0;
  var count4=0;
  var count5=0;
  var count6=0;
  //console.log('sdlkm', JSON.stringify($scope.stockitems)) 
  _.forEach(result.items, function (itm, i) {
    var indeex = _.findIndex(items, function (item){
      return item._id.toString() == itm.itemId.toString();
    });
    console.log('ind', indeex);
    if (indeex >= 0) {
      var item = items[indeex];
      console.log('pref', item.preferedUnit);
      if (item.preferedUnit != undefined) {
        _.forEach(item.units, function (u, i) {
          if (u._id == item.preferedUnit) {
            var conversionFactor = 1;
            var pconFac = parseFloat(u.conversionFactor);
            itm.UnitName = u.unitName;
            if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
              conversionFactor = parseFloat(u.baseConversionFactor);
              if (pconFac > conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(conversionFactor)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(conversionFactor)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(conversionFactor)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(conversionFactor)).toFixed(3);
                //console.log('wastage', itm.wastageQty);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  console.log(itm.physicalQty, conversionFactor);
                  if(itm.physicalQty != "NA")
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                  if(conversionFactor==undefined)
                    count3++;
                }
              }
              else if (pconFac < conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(conversionFactor)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(conversionFactor)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(conversionFactor)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(conversionFactor)).toFixed(3);
                //console.log('wastage', itm.wastageQty);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  console.log(itm.physicalQty, conversionFactor);
                  if(itm.physicalQty != "NA")
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                  if(conversionFactor==undefined)
                    count4++;
                }
              }
            }
            else {
              if (pconFac > conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(pconFac)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(pconFac)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(pconFac)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(pconFac)).toFixed(3);
                //console.log('wastage', itm.wastageQty);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  console.log(itm.physicalQty, pconFac);
                  if(itm.physicalQty != "NA")
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                  if(pconFac==undefined)
                    count5++;
                }
                if(itm.varianceQty != "NA")
                  itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
              }
              else if (pconFac < conversionFactor) {
                itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(pconFac)).toFixed(3);
                itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(pconFac)).toFixed(3);
                itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(pconFac)).toFixed(3);
                itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(pconFac)).toFixed(3);
                //console.log('wastage', itm.wastageQty);
                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                if (itm.physicalQty == undefined) {
                  itm.physicalQty = parseFloat(0).toFixed(3);
                }
                else {
                  console.log(itm.physicalQty,pconFac);
                  if(itm.physicalQty != "NA"){
                    itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                      if(pconFac==undefined)
                      count6++;
                  }
                }
                if(itm.varianceQty != "NA")
                  itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
              }
            }
          }
        });
      }
    }
  });
console.log('count3 :-  ',count3);
console.log('count4 :-  ',count4);
console.log('count5 :-  ',count5);
console.log('count6 :-  ',count6);
  return result;
}

function handleError(res, err) {
  return res.send(500, err);
}