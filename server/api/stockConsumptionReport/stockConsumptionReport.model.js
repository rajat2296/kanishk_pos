'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    StockRequirementSchema=require('mongoose').model('StockRequirement').schema,
    StockTransactionSchema=require('mongoose').model('StockTransaction').schema,
    BillSchema=require('mongoose').model('Bill').schema,
    StockRecipeSchema=require('mongoose').model('StockRecipe').schema,
    ItemSchema=require('mongoose').model('Item').schema,
    StockItemSchema=require('mongoose').model('StockItem').schema,
    VendorSchema = require('mongoose').model('Vendor').schema,
    DeploymentSchema = require('mongoose').model('Deployment').schema,
    BaseKitchenItemSchema = require('mongoose').model('BaseKitchenItem').schema;

var StockConsumptionReportSchema = new Schema({
  tenant_id: Schema.Types.ObjectId,
  deployment_id: Schema.Types.ObjectId,
  email: String,
  created: {
    type: Date,
    default: Date.now()
  },
  reportName: String,
  status: Boolean
});

 var StockConsumptionReport = mongoose.model('StockConsumptionReport', StockConsumptionReportSchema);
 var StockRequirement =mongoose.model('StockRequirement', StockRequirementSchema);
 var StockTransaction = mongoose.model('StockTransaction', StockTransactionSchema);
 var Bill=mongoose.model('Bill',BillSchema);
 var StockRecipe=mongoose.model('StockRecipe',StockRecipeSchema);
 var StockItem=mongoose.model('StockItem',StockItemSchema);
 var Item=mongoose.model('Item',ItemSchema);
 var Vendor=mongoose.model('Vendor',VendorSchema);
 var Deployment = mongoose.model('Deployment', DeploymentSchema);
 var BaseKitchenItem = mongoose.model('BaseKitchenItem', BaseKitchenItemSchema);
module.exports={
	StockConsumptionReport:StockConsumptionReport,
  StockTransaction: StockTransaction,
	StockRequirement:StockRequirement,
	Bill:Bill,
	StockRecipe:StockRecipe,
  StockItem:StockItem,
  Item:Item,
  Vendor: Vendor,
  Deployment: Deployment,
  BaseKitchenItem: BaseKitchenItem
}
