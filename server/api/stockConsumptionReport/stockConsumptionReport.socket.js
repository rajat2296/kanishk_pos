/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockConsumptionReport = require('./stockConsumptionReport.model');

exports.register = function(socket) {
  StockConsumptionReport.StockConsumptionReport.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockConsumptionReport.StockConsumptionReport.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockConsumptionReport:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockConsumptionReport:remove', doc);
}