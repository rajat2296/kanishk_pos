'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ForumSchema = new Schema({
  name: String,
  username: String,
  question: String,
  answers:[],
  follow:[],
  created: { type: Date, default: Date.now},
  lastModified: Date
});
ForumSchema.set('versionKey', false);
module.exports = mongoose.model('Forum', ForumSchema);
