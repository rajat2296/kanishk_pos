'use strict';

var _ = require('lodash');
var Forum = require('./Forum.model');
var Notification= require('./forumNotification.model');
var User = require('../user/user.model');
var request =require('request');
var apn=require('apn');
var logs=require('./ForumLogs.model');
// Get list of Forums
exports.index = function(req, res) {
  var page=parseInt(req.query.pageSize)*parseInt(req.query.pageIndex);
  Forum.find({},{},{sort:{lastModified:-1},limit:parseInt( req.query.pageSize) , skip:page},function (err, Forums) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(Forums);
  });
};

// Get a single Forum
exports.show = function(req, res) {
  Forum.findById(req.params.id, function (err, Forum) {
    if(err) { return handleError(res, err); }
    if(!Forum) { return res.status(404).send('Not Found'); }
    return res.json(Forum);
  });
};
exports.removetoken=function(req,res)
{
  console.log('remove token')
  Notification.remove({api_key: req.query.api_key}, function (errr,notifcation) {
    if (errr) {
      return handleError(res, errr);
    }
    return res.json(201, {success:"success"});
  })
}
exports.registerToken = function(req, res) {
  console.log(req.body)
  Notification.remove({api_key: req.body.api_key}, function (errr) {
    if (errr) {
      return handleError(res, errr);
    }

    Notification.create(req.body, function (err, notification) {
      if (err) {
        return handleError(res, err);
      }
      User.findOne({username:req.body.username},function(err,user)
      {
        if(err)
        {
          return handleError(res, err);
        }
        return res.json(201, user.firstname? user.firstname : user.name);
      })

    });
  })
}
// Creates a new Forum in the DB.

exports.create = function(req, res) {
  req.body.lastModified=new Date();
  Forum.create(req.body, function(err, Forum) {
    if(err) { return handleError(res, err); }
    var keyArr = [];
    Notification.find({username:{$ne:Forum.username}},function(req,nots)
    {
      if(nots.length>0) {
        //    console.log(nots);
        _.forEach(nots, function (not) {

          //  console.log(not.api_key);
          var key=not.api_key;
          if(!(key == null ||key == undefined)){
            var stringAPN=key.split('@');
            if(stringAPN.length==2){
              var myDevice = new apn.Device(stringAPN[0]);
              var note = new apn.Notification();
              note.badge = 1;
              note.sound = "notification-beep.wav";
              note.alert = {"body": 'New Question Asked'};
              note.payload = {
                "message": Forum.name +' asked' + Forum.question,
                $state: "forum"
                //"deployment_id": req.body.deployment_id
              };
              note.device = myDevice;
              var callback = function (errorNum, notification) {
                console.log('Error is: %s', errorNum);
                console.log("Note " + notification);
              }
              // var options = {
              //   gateway: 'gateway.sandbox.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
              //   errorCallback: callback,
              //   cert: __dirname +'/cockpitdevelopmentcert.pem',
              //   key: __dirname+'/cockpit.pem',
              //   passphrase: '1234',
              //   port: 2195,
              //   enhanced: true,
              //   cacheLength: 100,
              //   debug: true
              // }
               var options = {
               gateway: 'gateway.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
               errorCallback: callback,
               cert: __dirname +'/cockpitdistributioncert.pem',
              key: __dirname +'/cockpit.pem',
               passphrase: '1234',
               port: 2195,
               enhanced: true,
               cacheLength: 100,
               debug:true
            }
              var apnsConnection = new apn.Connection(options);
              apnsConnection.sendNotification(note);
            }else{
              keyArr.push(key)
              ;
            }
          }
        });
        /*Android*/
        var options = {
          method: 'POST',
          url: 'https://android.googleapis.com/gcm/send',
          headers: {
            'content-type': 'application/json',
            authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
          },
          body: {
            registration_ids: keyArr,
            //data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
            data: {message:Forum.name +' asked ' + Forum.question +'?', title: 'New Question Asked',$state: 'forum'}
          },
          json: true
        };

        request(options, function (error, response, body) {
          if (error) {
            console.log(error);
          }
        });
        /*End Android*/
      }
    })
    return res.status(201).json(Forum);
  });
};

// Updates an existing Forum in the DB.
exports.update = function(req, res) {
  console.log(req.params);
  if(req.params.unfollow=="true")
  {
     Forum.update({_id:req.params.id},{$pull:{follow:req.body.name}},function(d)
     {
      Forum.findById(req.params.id, function (err, Forum) {
        return res.status(200).json(Forum);
      })
     })
  }
 else if(req.params.isFollow=="true")
  {
    console.log(req.query)
    console.log(req.body);
     // return res.status(200);
     Forum.update({_id:req.params.id},{$push:{follow:req.body.name}},function(d)
     {
      Forum.findById(req.params.id, function (err, Forum) {
        return res.status(200).json(Forum);
      })
     })
  }
  else
  {
    var Modified=new Date()
      Forum.update({_id:req.params.id},{$push:{answers:req.body},$set:{lastModified:Modified}},function(data)
  {
    
    Forum.findById(req.params.id, function (err, Forum) {
    // if (err) { return handleError(res, err); }
    // if(!Forum) { return res.status(404).send('Not Found'); }
    // var updated = _.merge(Forum, req.body);
    var keyArr = [];
    var answerArr=[]
    var followarr=[]
    // updated.save(function (err) {
    //   if (err) { return handleError(res, err);
    //    }
       var usernameOfAnswers=[]
       
       _.forEach(Forum.answers,function(answer)
       {
            var matched=false;
            
            for(var i=0;i<usernameOfAnswers.length;i++)
            {
              if(usernameOfAnswers==answer.username)
              {
                matched=true;
                break;
              }
            }
            if(matched==false)
            {
               if(Forum.answers[Forum.answers.length - 1].username !=answer.username)
              {
                usernameOfAnswers.push(answer.username);
                Notification.find({username:answer.username},function(abc,nots)
                {
                  if(nots.length>0) {
                    //console.log(nots);
                    _.forEach(nots, function (not) {

                      //  console.log(not.api_key);
                      var key=not.api_key;
                      if(!(key == null ||key == undefined)){
                        var stringAPN=key.split('@');
                        if(stringAPN.length==2){
                          var myDevice = new apn.Device(stringAPN[0]);
                          var note = new apn.Notification();
                          note.badge = 1;
                          note.sound = "notification-beep.wav";
                          note.alert = {"body": Forum.answers[Forum.answers.length - 1].name +' answered '+Forum.question +'?'};
                          note.payload = {
                            "message": Forum.answers[Forum.answers.length - 1].answer,
                            $state: 'forum'
                          };
                          note.device = myDevice;
                          var callback = function (errorNum, notification) {
                            console.log('Error is: %s', errorNum);
                            console.log("Note " + notification);
                          }
                          // var options = {
                          //   gateway: 'gateway.sandbox.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                          //   errorCallback: callback,
                          //   cert: __dirname + '/cockpitdevelopmentcert.pem',
                          //   key: __dirname +'/cockpit.pem',
                          //   passphrase: '1234',
                          //   port: 2195,
                          //   enhanced: true,
                          //   cacheLength: 100,
                          //   debug: true
                          // }
                               var options = {
                             gateway: 'gateway.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                             errorCallback: callback,
                             cert: __dirname +'/cockpitdistributioncert.pem',
                            key: __dirname +'/cockpit.pem',
                             passphrase: '1234',
                             port: 2195,
                             enhanced: true,
                             cacheLength: 100,
                             debug:true
                            }
                          var apnsConnection = new apn.Connection(options);
                          apnsConnection.sendNotification(note);
                        }else{
                          answerArr.push(key);
                        }
                      }
                    });
                    /*Android*/
                    var options = {
                      method: 'POST',
                      url: 'https://android.googleapis.com/gcm/send',
                      headers: {
                        'content-type': 'application/json',
                        authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
                      },
                      body: {
                        registration_ids: answerArr,
                        //data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
                        data: {message:  Forum.answers[Forum.answers.length - 1].answer, title: Forum.answers[Forum.answers.length - 1].name +' answered '+Forum.question +'?',$state: 'forum'}
                      },
                      json: true
                    };

                    request(options, function (error, response, body) {
                      if (error) {
                        console.log(error);
                      }
                    });
                    /*End Android*/
                  }
                })
              }
            }
      })
      _.forEach(Forum.follow,function(name)
      {

            var matched=false;
            
            for(var i=0;i<usernameOfAnswers.length;i++)
            {
              if(usernameOfAnswers==name)
              {
                matched=true;
                break;
              }
            }
            if(matched==false)
            {
               if(Forum.answers[Forum.answers.length - 1].username !=name)
              {
                usernameOfAnswers.push(name);
                Notification.find({username:name},function(abc,nots)
                {
                  if(nots.length>0) {
                    //console.log(nots);
                    _.forEach(nots, function (not) {

                      //  console.log(not.api_key);
                      var key=not.api_key;
                      if(!(key == null ||key == undefined)){
                        var stringAPN=key.split('@');
                        if(stringAPN.length==2){
                          var myDevice = new apn.Device(stringAPN[0]);
                          var note = new apn.Notification();
                          note.badge = 1;
                          note.sound = "notification-beep.wav";
                          note.alert = {"body": Forum.answers[Forum.answers.length - 1].name +' answered '+Forum.question +'?'};
                          note.payload = {
                            "message": Forum.answers[Forum.answers.length - 1].answer,
                            $state: 'forum'
                          };
                          note.device = myDevice;
                          var callback = function (errorNum, notification) {
                            console.log('Error is: %s', errorNum);
                            console.log("Note " + notification);
                          }
                          // var options = {
                          //   gateway: 'gateway.sandbox.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                          //   errorCallback: callback,
                          //   cert: __dirname + '/cockpitdevelopmentcert.pem',
                          //   key: __dirname +'/cockpit.pem',
                          //   passphrase: '1234',
                          //   port: 2195,
                          //   enhanced: true,
                          //   cacheLength: 100,
                          //   debug: true
                          // }
                          var options = {
                         gateway: 'gateway.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                         errorCallback: callback,
                         cert: __dirname +'/cockpitdistributioncert.pem',
                          key: __dirname +'/cockpit.pem',
                         passphrase: '1234',
                         port: 2195,
                         enhanced: true,
                         cacheLength: 100,
                         debug:true
                          }
                          var apnsConnection = new apn.Connection(options);
                          apnsConnection.sendNotification(note);
                        }else{
                          followarr.push(key);
                        }
                      }
                    });
                    /*Android*/
                    var options = {
                      method: 'POST',
                      url: 'https://android.googleapis.com/gcm/send',
                      headers: {
                        'content-type': 'application/json',
                        authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
                      },
                      body: {
                        registration_ids: followarr,
                        //data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
                        data: {message:  Forum.answers[Forum.answers.length - 1].answer, title: Forum.answers[Forum.answers.length - 1].name +' answered '+Forum.question +'?',$state: 'forum'}
                      },
                      json: true
                    };

                    request(options, function (error, response, body) {
                      if (error) {
                        console.log(error);
                      }
                    });
                    /*End Android*/
                  }
                })
              }
            }
      })

       
       
      if(Forum.answers[Forum.answers.length - 1].username!=Forum.username)
      {
        Notification.find({username:Forum.username},function(abc,nots)
        {
          if(nots.length>0) {
            //console.log(nots);
            _.forEach(nots, function (not) {

              //  console.log(not.api_key);
              var key=not.api_key;
              if(!(key == null ||key == undefined)){
                var stringAPN=key.split('@');
                if(stringAPN.length==2){
                  var myDevice = new apn.Device(stringAPN[0]);
                  var note = new apn.Notification();
                  note.badge = 1;
                  note.sound = "notification-beep.wav";
                  note.alert = {"body": Forum.answers[Forum.answers.length - 1].name +' answered your Question'};
                  note.payload = {
                    "message": Forum.answers[Forum.answers.length - 1].answer,
                    $state: 'forum'
                  };
                  note.device = myDevice;
                  var callback = function (errorNum, notification) {
                    console.log('Error is: %s', errorNum);
                    console.log("Note " + notification);
                  }
                  // var options = {
                  //   gateway: 'gateway.sandbox.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                  //   errorCallback: callback,
                  //   cert: __dirname + '/cockpitdevelopmentcert.pem',
                  //   key: __dirname +'/cockpit.pem',
                  //   passphrase: '1234',
                  //   port: 2195,
                  //   enhanced: true,
                  //   cacheLength: 100,
                  //   debug: true
                  // }
                        var options = {
                       gateway: 'gateway.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
                       errorCallback: callback,
                       cert: __dirname +'/cockpitdistributioncert.pem',
                      key: __dirname +'/cockpit.pem',
                       passphrase: '1234',
                       port: 2195,
                       enhanced: true,
                       cacheLength: 100,
                       debug:true
                       }
                  var apnsConnection = new apn.Connection(options);
                  apnsConnection.sendNotification(note);
                }else{
                  keyArr.push(key);
                }
              }
            });
            /*Android*/
            var options = {
              method: 'POST',
              url: 'https://android.googleapis.com/gcm/send',
              headers: {
                'content-type': 'application/json',
                authorization: 'key=AIzaSyBb_cc4yAwhB6vM0wOhJCVNG1S7NZJCpLc'
              },
              body: {
                registration_ids: keyArr,
                //data: {message: req.body.message, title: req.body.type, $state: 'tab.account',deployment_id:req.body.deployment_id}
                data: {message:  Forum.answers[Forum.answers.length - 1].answer, title: Forum.answers[Forum.answers.length - 1].name +' answered your Question',$state: 'forum'}
              },
              json: true
            };

            request(options, function (error, response, body) {
              if (error) {
                console.log(error);
              }
            });
            /*End Android*/
          }
        })
      }

      return res.status(200).json(Forum);
    });
  });
  }
};
//To be changed
// Deletes a Forum from the DB.
exports.destroy = function(req, res) {
  
  Forum.findById(req.params.id, function (err, forumdata) {
        if(err) { return handleError(res, err); }
       var beforechange=forumdata
       var u={}
       u.email=req.body.email
       u.name=req.body.name
       u.status="Deleted"
       u.Content=beforechange.question
       u.ContentType="Question"
       u.from= beforechange.username
      
      
      Forum.findById(req.params.id, function (err, Forum) {
      if(err) { return handleError(res, err); }
          logs.create(u, function (err, notification) {
        if (err) {
          return handleError(res, err);
        }
      })
      if(!Forum) { return res.status(404).send('Not Found'); }
      Forum.remove(function(err) {
        if(err) { return handleError(res, err); }
        return res.status(204).send('No Content');
      });
    });
  })
};
//new to be added
exports.deleteComment = function(req, res) {
  
   Forum.findById(req.params.id, function (err, Forumdata) {
        if(err) { return handleError(res, err); }
        var beforechange=Forumdata
     
   var u={}
   u.email=req.body.email
   u.name=req.body.name
   u.status="Deleted"
   u.Content=req.body.answer.answer
   u.ContentType="Comment"
   u.from= req.body.answer.username
 
 
  Forum.update({_id:req.params.id},{$pull:{answers:req.body.answer}},function(err,data)
  {
    if(err) { return handleError(res, err); }
      logs.create(u, function (err, notification) {
      if (err) {
        return handleError(res, err);
      }
    })
   
    Forum.findById(req.params.id, function (err, forumdata) {
        return res.status(200).json(forumdata);
      })
    // return res.status(201);
  })
   })
};
exports.editComment = function(req, res) {
  
     Forum.findById(req.params.id, function (err, Forumdata) {
        if(err) { return handleError(res, err); }
        var beforechange=Forumdata
      
   var u={}
   u.email=req.body.email
   u.name=req.body.name
   u.status="Edited"
   u.Content=req.body.beforeupdate.answer
   u.ContentType="Comment"
   u.ChangedTo=req.body.afterupdate.answer
   
    u.from= req.body.beforeupdate.username
  Forum.update({_id:req.params.id,answers:{$elemMatch:req.body.beforeupdate}},{$set:{"answers.$":req.body.afterupdate}},function(err,data)
  {
    if(err) { return handleError(res, err); }
   
    logs.create(u, function (err, notification) {
      if (err) {
        return handleError(res, err);
      }
    })
    Forum.findById(req.params.id, function (err, forumdata) {
        return res.status(200).json(forumdata);
      })
    // return res.status(201).send('done');
  })
  })
};

exports.editquestion = function(req, res) {
  
   Forum.findById(req.params.id, function (err, Forumdata) {
        if(err) { return handleError(res, err); }
        var beforechange=Forumdata
         var u={}
   u.email=req.body.email
   u.name=req.body.name
   u.Content=beforechange.question
   u.status="Edited"
   u.ContentType="Question"
   u.ChangedTo=req.body.question
   u.from= beforechange.username
   
  
     Forum.update({_id:req.params.id},{$set:{"question":req.body.question}},function(err,data)
    {
    if(err) { return handleError(res, err); }

    logs.create(u, function (err, notification) {
      if (err) {
        return handleError(res, err);
      }
    })
    
    Forum.findById(req.params.id, function (err, forumdata) {
        return res.status(200).json(forumdata);
      })
    // return res.status(201).send('done');
    })
  })
  

};

// exports.editComment = function(req, res) {
//   console.log(req.params.id,req.body)
//   Forum.update({_id:req.params.id},{$pull:{answers:{$elemMatch:req.body.beforeupdate}}},{$push:{answers:{$each:[req.body.afterupdate],$position:req.body.position}}},function(err,data)
//   {
//     if(err) { return handleError(res, err); }
//     console.log(data,'data')
//     return res.status(201).send('done');
//   })
// };
exports.logsGet = function(req, res) {
  var page=parseInt(req.query.pageSize)*parseInt(req.query.pageIndex);
  logs.find({},{},{sort:{created:-1},limit:parseInt( req.query.pageSize) , skip:page},function (err, Forums) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(Forums);
  });
};
function handleError(res, err) {
  return res.status(500).send(err);
}
