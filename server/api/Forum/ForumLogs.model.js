'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ForumNotificationSchema = new Schema({
  email:String,
  name: String,
  status:String,
  Content:String,
  ContentType:String,
  ChangedTo:String,
  from:String,
  created: { type: Date, default: Date.now}
});
ForumNotificationSchema.set('versionKey', false);
module.exports = mongoose.model('forumLogs', ForumNotificationSchema);
