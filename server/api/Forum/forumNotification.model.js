'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ForumNotificationSchema = new Schema({
  username:String,
  api_key: String,
  created: { type: Date, default: Date.now}
});
ForumNotificationSchema.set('versionKey', false);
module.exports = mongoose.model('ForumNotification', ForumNotificationSchema);
