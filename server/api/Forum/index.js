'use strict';

var express = require('express');
var controller = require('./Forum.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/logsGet', controller.logsGet);//Change
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/:isFollow/:unfollow', controller.update);
router.patch('/:id', controller.update);
router.delete('/removetoken',controller.removetoken);
router.put('/:id', controller.destroy);//Change
router.post('/registerToken',controller.registerToken);
router.put('/deleteComment/:id',controller.deleteComment);
router.put('/editComment/:id',controller.editComment);
router.put('/editquestion/:id',controller.editquestion);

module.exports = router;
