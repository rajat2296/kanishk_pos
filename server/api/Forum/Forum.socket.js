/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Forum = require('./Forum.model');

exports.register = function(socket) {
  Forum.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Forum.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('Forum:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('Forum:remove', doc);
}