'use strict';
var express = require('express');
var controller = require('./stockTransaction.controller');

var router = express.Router();


router.get('/', controller.index);
router.get('/editEntriesRedirect/:id', controller.editEntriesRedirect);
router.get('/updateTransactionUnits', controller.updateTransactionUnits);
router.get('/updateTransaction', controller.updateTransaction);
router.get('/updateTransactionUnits', controller.updateTransactionUnits);
router.get('/getAllByTranasactionAndDateRange', controller.getAllByTranasactionAndDateRange);
router.get('/getClosingQty', controller.getClosingQty);
router.get('/getBasicReports', controller.getBasicReports);
router.get('/getBasicEntryReportsForPurchaseSummary', controller.getBasicEntryReportsForPurchaseSummary);
router.get('/getBasicEntryReportsForPurchaseDetail', controller.getBasicEntryReportsForPurchaseDetail);
router.get('/getBasicEntryReportsForEntries', controller.getBasicEntryReportsForEntries);
router.get('/getAllByTranasactionAndDateRangeNew', controller.getAllByTranasactionAndDateRangeNew);
router.get('/getBasicEntryReports', controller.getBasicEntryReports);
router.get('/getStockSummary', controller.getStockSummary);
router.get('/getPagination', controller.getPagination);
router.get('/getLatestTransaction', controller.getLatestTransaction);
router.get('/getLatestTransactionOfType', controller.getLatestTransactionOfType);
router.get('/checkIfMenuItemExistsInTransaction', controller.checkIfMenuItemExistsInTransaction);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/createMultiple', controller.createMultiple);
router.post('/:id', controller.createWithId);
router.post('/createWithTransactionId/:id', controller.createWithTransactionId);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;

