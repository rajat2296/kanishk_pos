'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockTransactionSchema = new Schema({
 	transactionId: String,
 	transactionType:String,
 	daySerialNumber:Number,
 	transactionNumber:Number,
    invoiceNumber:String,
    tempTransactionNumber:Number,
 	user:{},
 	  _items: [],
    _store: {},
    _vendor: {},
    _receiver:{},
    discountType:{type:String,default:'percent'},
    discount:{type:Number,default:0},
    serviceChargeType:{type:String,default:'percent'},
    serviceCharge:{type:Number,default:0},
    cartage:{type:Number,default:0},
    vatType:{type:String,default:'percent'},
    vat:{type:Number,default:0},
    payment:{type:Number,default:0},
    heading:String,
    templateName:String,
    stockReturnId:String,
    isStockReturn:Boolean,
    wastageEntryType:String,
    wastageItemType:String,
    isStockReturnAccepted:{type: Boolean, default: false},
    deployment_id: Schema.Types.ObjectId,
    fromDeployment_id: String,
    toDeployment_id : String,
    tenant_id: Schema.Types.ObjectId,
    isPOEntry: Boolean,
    poNumber: Number,
    poDaySerialNumber: Number,
    isSynced: Boolean,
    syncedOn: String,
    _created: {type: Date, default: Date.now},//online
    timeStamp:{type:Date},
    updated: { type: Date, default: Date.now},
    returnAcceptDate: Date,
    created: { type: Date},
    indentingType : String,
    batchNumber : String,
    charges: [] // offline Creation
});
StockTransactionSchema.set('versionKey', false);

StockTransactionSchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});
module.exports = mongoose.model('StockTransaction', StockTransactionSchema);
