/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockTransaction = require('./stockTransaction.model');


exports.register = function(socket) {
  StockTransaction.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockTransaction.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockTransaction:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockTransaction:remove', doc);
}