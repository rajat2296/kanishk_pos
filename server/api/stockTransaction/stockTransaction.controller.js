'use strict';

var _ = require('lodash');
var StockTransaction = require('./stockTransaction.model');
var ObjectId = require('mongoose').Types.ObjectId;
var async = require('async');
var config = require('../../config/environment');
var Q = require('q');
var kue = require('kue')
  , queue = kue.createQueue(config.redisConn);
var request = require('request');
var Recipe = require('../stockRecipe/stockRecipe.model');
var utils=require('../Utils/utils');

exports.getPagination = function (req, res) {
  console.log("Pagination", req.query);
  var check = {};
  if (req.query.tenant_id)
    check.tenant_id = req.query.tenant_id;
  if (req.query.deployment_id)
    check.deployment_id = req.query.deployment_id;
  else if (req.query.toDeploymentId)
    check.toDeployment_id = req.query.toDeploymentId;
  if (req.query.transactionType)
    check.transactionType = req.query.transactionType;
  if(req.query.returnAcceptDate == null || req.query.returnAcceptDate == 'null') {
    check['$or'] = [{returnAcceptDate: {$exists: false}}]
  }
  console.log('check', check);
  StockTransaction.find(check,{},{skip: req.query.skip, limit: req.query.limit}, function (err, stockTransactions) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, stockTransactions);
  });
};

// Get list of stockTransactions
exports.index = function (req, res) {
  //console.log("Hello orld");
  var check = {};
  if (req.query.tenant_id)
    check.tenant_id = req.query.tenant_id;
  if (req.query.deployment_id)
    check.deployment_id = req.query.deployment_id;
  else if (req.query.toDeploymentId)
    check.deployment_id = req.query.deployment_id;
  if (req.query.transactionType)
    check.transactionType = req.query.transactionType;
  if(req.query.isStockReturnAccepted){
    check.isStockReturnAccepted = req.query.isStockReturnAccepted;
  }
  //console.log('check', check);
  StockTransaction.find(check, function (err, stockTransactions) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, stockTransactions);
  });
};

// Get a single stockTransaction
exports.show = function (req, res) {
  StockTransaction.findById(req.params.id, function (err, stockTransaction) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockTransaction) {
      return res.send(404);
    }
    return res.json(stockTransaction);
  });
};

exports.editEntriesRedirect = function (req, res) {
  
  getTransaction(req.params.id).then(function(result){

    var obj={};
    obj=result[0]
   // console.log(result,'-----------');
    return res.json(200,obj);
  });// END of getBillData

};


function getTransaction(id) {
  
  var d = Q.defer();

  StockTransaction.aggregate(
    [
      {
        $match: {
          _id: new ObjectId(id)
        }
      },

      {
        $project:

        { _created:1,
          _id:1,
          _items:1,
          _receiver:1,
          _vendor:1,
          isSynced:1,
          syncedOn:1,
          heading:1,
          timeStamp:1,
          updated:1,
          isStockReturnAccepted:1,
          daySerialNumber:1,
          tenant_id:1,
          deployment_id:1,
          discount:1,
          created:1,
          discountType:1,
          serviceCharge:1,
          serviceChargeType:1,
          vat:1,
          vatType:1,
          invoiceNumber:1,
          payment:1,
          transactionId:1,
          transactionNumber:1,
          transactionType:1,
          cartage:1,
          charges:1,
          '_store._id':1,
          '_store.storeName': 1,
          '_store.storeLocation': 1,
          '_store.vendor':1,
          '_store.category':1,
          "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },

        }

      },
      {
        $project: {
          _created:1,
          _id:1,
          _items:1,
          _receiver:1,
          _vendor:1,
          isSynced:1,
          syncedOn:1,
          heading:1,
          timeStamp:1,
          updated:1,
          isStockReturnAccepted:1,
          daySerialNumber:1,
          tenant_id:1,
          deployment_id:1,
          discount:1,
          created:1,
          discountType:1,
          serviceCharge:1,
          serviceChargeType:1,
          vat:1,
          vatType:1,
          invoiceNumber:1,
          payment:1,
          transactionId:1,
          transactionNumber:1,
          transactionType:1,
          cartage:1,
          charges:1,
          '_store._id':1,
          '_store.storeName': 1,
          '_store.storeLocation': 1,
          '_store.vendor':1,
          '_store.category':1,
          "_store.receiver":1


        }
      }]

    ,  function (err, res) {
       console.log(res)
      if (err)
        d.reject(err);
      else
        d.resolve(res);
    });

  return d.promise;
}


//Get get All By Tranasaction And DateRange

//Get get All By Tranasaction And DateRange
exports.getAllByTranasactionAndDateRange = function (req, res) {
  //console.log("Hello Ajit");
  var sss=JSON.parse(JSON.stringify(req.query));
  var startDate = new Date(req.query.fromDate);
  startDate.setDate(startDate.getDate());
  //console.log(new Date(startDa  var sss = JSON.parse(JSON.stringify(req.query));


  var endDate = new Date(req.query.toDate);
  endDate.setDate(endDate.getDate() + 1);
  //console.log(new Date(endDate));

  var ssa = formatQueryParamater(sss);
  var conditions = Object.keys(ssa).map(function (key, value) {
    //console.log(key);
    var obj = {},
      newKey = "";
    if (key == "itemId") {
      newKey = key;
    }
    else {
      newKey = key;
      obj[newKey] = req.query[key];
    }
    return obj;
  });

//console.log(conditions);
  //if(req.query.store._id!=""){
  StockTransaction.aggregate([
    {
      $match: {
        tenant_id: new ObjectId(req.query.tenant_id),
        deployment_id: new ObjectId(req.query.deployment_id),
        //transactionType: req.query.transactionType,
        created: {$gte: new Date(startDate), $lte: new Date(endDate)},
        $and: conditions
      }
    },
    {
      $project:
      {   daySerialNumber:1,
        tenant_id:1,
        deployment_id:1,
        discount:1,
        created:1,
        discountType:1,
        serviceCharge:1,
        serviceChargeType:1,
        vat:1,
        vatType:1,
        invoiceNumber:1,
        batchNumber:1,
        payment:1,
        transactionNumber:1,
        transactionType:1,
        cartage:1,
        charges:1,
        '_store._id':1,
        '_store.storeName': 1,
        '_store.vendor._id':1,
        '_store.vendor.vendorName':1,
        '_store.vendor.category.items.itemName':1,
        '_store.vendor.category.items.qty':1,
        '_store.vendor.category.items.price':1,
        '_store.vendor.category.items.subTotal':1,
        '_store.vendor.category.items.addedAmt':1,
        '_store.vendor.category.items.vatPercent':1,
        '_store.vendor.category.items.calculateInUnits.unitName':1,
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        '_store.category.items.returnWastageQty':1,
        '_store.category.items.qty':1,
        '_store.category.items._id':1,
        '_store.category.items.price':1,
        '_store.category.items.itemName':1,
        '_store.category.items.calculateInUnits.unitName':1
      }

    },
    {
      $project: {
        daySerialNumber:1,
        tenant_id:1,
        deployment_id:1,
        discount:1,
        created:1,
        discountType:1,
        serviceCharge:1,
        serviceChargeType:1,
        vat:1,
        vatType:1,
        invoiceNumber:1,
        batchNumber:1,
        payment:1,
        transactionNumber:1,
        transactionType:1,
        cartage:1,
        charges:1,
        '_store._id':1,
        '_store.storeName': 1,
        '_store.vendor._id':1,
        '_store.vendor.vendorName':1,
        '_store.vendor.category.items.itemName':1,
        '_store.vendor.category.items.qty':1,
        '_store.vendor.category.items.price':1,
        '_store.vendor.category.items.subTotal':1,
        '_store.vendor.category.items.addedAmt':1,
        '_store.vendor.category.items.vatPercent':1,
        '_store.vendor.category.items.vat':1,
        '_store.vendor.category.items.calculateInUnits.unitName':1,
        '_store.receiver._id':1,
        '_store.receiver.receiverName':1,
        '_store.receiver.category.items.itemName':1,
        '_store.receiver.category.items.price':1,
        '_store.receiver.category.items.vatPercent':1,
        '_store.receiver.category.items.calculateInUnits.unitName':1,
        '_store.receiver.category.items.total':1,
        '_store.receiver.category.items.qty':1,
        '_store.category.items.returnWastageQty':1,
        '_store.category.items.qty':1,
        '_store.category.items._id':1,
        '_store.category.items.price':1,
        '_store.category.items.itemName':1,
        '_store.category.items.calculateInUnits.unitName':1
      }
    }

  ], function (err, result) {
    if (err) {
      handleError(res, err);
      //next(err);
    } else {
      return res.json(result);
    }
  });
  //}
  // else
  // {
  //   StockTransaction.aggregate([
  //         {
  //             $match: {
  //                 tenant_id:new ObjectId(req.query.tenant_id),
  //                 deployment_id: new ObjectId(req.query.deployment_id),
  //                 transactionType: req.query.transactionType,
  //                 created: {$gte: new Date(startDate), $lte : new Date(endDate)}
  //                 //$and: conditions
  //             }
  //         }
  //     ], function (err, result) {
  //         if (err) {
  //             handleError(res, err);
  //             //next(err);
  //         } else {
  //             return res.json(result);
  //         }
  //     });
  // }
};


//Get get All By Tranasaction And DateRange
exports.getClosingQty = function (req, res) {
  //console.log("Hello Ajit");

  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1",
        created: {$gt: new Date(req.query.fromDate), $lte: new Date(req.query.toDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $project: {
        category: "$_store.vendor.category",
        items: "$_store.vendor.category.items",
        //created:"$created",
        billNo: "$daySerialNumber"
        //vendor:"$_store.vendor"
      }
    },
    {
      $group: {
        _id: "$_id",
        totalAmt: {$sum: "$items.subTotal"},
        //totalVat:0,
        totalQty: {$sum: "$items.qty"}
        //totalDiscount:0
      }
    },
    {
      $project: {
        totalQty: "$totalQty",
        totalAmt: "$totalAmt",
        created: "$created",
        billNo: "$daySerialNumber"
      }
    }
  ], function (err, result) {
    if (err) {
      handleError(res, err);
      //next(err);
    } else {
      StockTransaction.aggregate([
        {
          $match: {
            transactionType: "2",
            created: {$gt: new Date(req.query.fromDate), $lte: new Date(req.query.toDate)}
          }
        },
        {
          $unwind: "$_store.receiver.category"
        },
        {
          $unwind: "$_store.receiver.category.items"
        },
        {
          $project: {
            category: "$_store.receiver.category",
            items: "$_store.receiver.category.items"
          }
        },
        {
          $group: {
            _id: "$_id",
            totalAmt: {$sum: {$multiply: ["$items.qty", "$items.price"]}},
            //totalVat:0,
            totalQty: {$sum: "$items.qty"}
            //totalDiscount:0
          }
        }
      ], function (err, result1) {
        if (err) {
          handleError(res, err);
          //next(err);
        } else {
          _.forEach(result1, function (r, i) {
            result.push(r);
          });
          //return res.json(result);
          StockTransaction.aggregate([
            {
              $match: {
                created: {$gt: new Date(req.query.fromDate), $lte: new Date(req.query.toDate)},
                $or: [{transactionType: "3"}, {transactionType: "4"}, {transactionType: "5"}]
              }
            },
            {
              $unwind: "$_store.category"
            },
            {
              $unwind: "$_store.category.items"
            },
            {
              $project: {
                category: "$_store.category",
                items: "$_store.category.items"
              }
            },
            {
              $group: {
                _id: "$_id",
                totalQty: {$sum: "$items.qty"}
              }
            }
          ], function (err, result2) {
            if (err) {
              handleError(res, err);
              //next(err);
            } else {
              _.forEach(result2, function (r, i) {
                result.push(r);
              });
              return res.json(result);
            }
          });
        }
      });
      //return res.json(result);
    }
  });
};

exports.getBasicReports = function (req, res) {
  // Initial data
  var request = {"storeName": "GoDown", "transactionType": "1"};

  // Transform to array
  var conditions = Object.keys(request).map(function (key, value) {
    //console.log(key,value);
    var obj = {}, newKey = "";
    if (key == "storeName") {
      newKey = "_store." + key;
    } else {
      newKey = key;
    }

    obj[newKey] = request[key];
    return obj;
  });
  //console.log(conditions);
  //StockTransaction.find({ "$or": conditions });
  StockTransaction.find({"$or": conditions}, function (err, stockTransactions) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, stockTransactions);
  });
};

function formatQueryParamater(q) {
  console.log('q',q);
  _.forIn(q, function (value, key) {
    if (value == "" || key == "fromDate" || key == "toDate" || key == "tenant_id" || key == "deployment_id") {
      delete q[key];
    }
  });
  return q;
};
exports.getBasicEntryReports = function (req, res) {
  //console.log("Basic Report");
  console.log(req.query);
  var sss = JSON.parse(JSON.stringify(req.query));
  var ssa = formatQueryParamater(sss);
  var entryType = req.query.transactionType;
  var conditions = Object.keys(ssa).map(function (key, value) {
    var obj = {}, newKey = "";
    if (key == "itemId") {
      newKey = key;
    }
    else {
      newKey = key;
      obj[newKey] = req.query[key];
    }
    return obj;
  });

  var toDate = new Date(req.query.toDate);
  toDate.setDate(toDate.getDate() + 1);
  //console.log(conditions);
  var obj = {
    tenant_id: new ObjectId(req.query.tenant_id),
    deployment_id: new ObjectId(req.query.deployment_id),
    created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
    $and: conditions
  }

  console.log(obj);
  StockTransaction.aggregate([
    {
      $match: {
        tenant_id: new ObjectId(req.query.tenant_id),
        deployment_id: new ObjectId(req.query.deployment_id),
        created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
        $and: conditions
      }
    }
  ], function (err, result) {
    var data = result;
    console.log(result);
    if (err) {
      handleError(res, err);
    } else {
      if (entryType == "4") {
        StockTransaction.aggregate([
          {
            $match: {
              tenant_id: new ObjectId(req.query.tenant_id),
              deployment_id: new ObjectId(req.query.deployment_id),
              created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
              transactionType: "15"
            }
          }
        ], function (err, result2) {
          if (err) {
            handleError(res, err);
          } else {
            _.forEach(result2, function (r) {
              data.push(r);
            });
            return res.json(data);
          }
        });
      }
      else {
        return res.json(data);
      }
    }
  });
};

exports.getBasicEntryReportsForPurchaseSummary = function (req, res) {
  //console.log("Basic Report");
  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockTransactions/getBasicEntryReportsForPurchaseSummary?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    console.log(req.query);
    var sss = JSON.parse(JSON.stringify(req.query));
    var ssa = formatQueryParamater(sss);
    var entryType = req.query.transactionType;
    var conditions = Object.keys(ssa).map(function (key, value) {
      var obj = {}, newKey = "";
      if (key == "itemId") {
        newKey = key;
      }
      else {
        newKey = key;
        obj[newKey] = req.query[key];
      }
      return obj;
    });

    var toDate = new Date(req.query.toDate);
    toDate.setDate(toDate.getDate() + 1);
    //console.log(conditions);
    var obj = {
      tenant_id: new ObjectId(req.query.tenant_id),
      deployment_id: new ObjectId(req.query.deployment_id),
      created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
      $and: conditions
    }

    console.log(obj);
    StockTransaction.aggregate([
      {
        $match: {
          tenant_id: new ObjectId(req.query.tenant_id),
          deployment_id: new ObjectId(req.query.deployment_id),
          created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
          $and: conditions
        },
      },
      {
        $project: {
          _id: 1,
          discount:1,
          discountType:1,
          serviceCharge:1,
          serviceChargeType:1,
          cartage:1,
          charges:1,
          "_store._id": 1,
          "_store.vendor._id": 1,
          "_store.vendor.category._id": 1,
          "_store.vendor.category.items._id": 1,
          "_store.vendor.category.items.itemName": 1,
          "_store.vendor.category.items.subTotal": 1,
          "_store.vendor.category.items.qty": 1,
          "_store.vendor.category.items.price": 1,
          "_store.vendor.category.items.vatPercent": 1,
          "_store.vendor.category.items.calculateInUnits": 1
        }
      }
    ], function (err, result) {
      var data = result;
      console.log(result);
      if (err) {
        handleError(res, err);
      } else {
        if (entryType == "4") {
          StockTransaction.aggregate([
            {
              $match: {
                tenant_id: new ObjectId(req.query.tenant_id),
                deployment_id: new ObjectId(req.query.deployment_id),
                created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
                transactionType: "15"
              }
            }
          ], function (err, result2) {
            if (err) {
              handleError(res, err);
            } else {
              _.forEach(result2, function (r) {
                data.push(r);
              });
              return res.json(data);
            }
          });
        }
        else {
          return res.json(data);
        }
      }
    });
  }
};

exports.getBasicEntryReportsForPurchaseDetail = function (req, res) {
  //console.log("Basic Report");
  if (config.isQueue) {
    request(config.reportServer.url + '/api/stockTransactions/getBasicEntryReportsForPurchaseDetail?' + utils.createGetUrl(req.query), function (error, response, body) {
      if (error) {
        console.log(error);
        res.json(404, {error: 'eeror in data'})
      } else {
        // console.log(body);
        return res.json(200, JSON.parse(body));
      }
    })
  } else {
    console.log(req.query);
    var sss = JSON.parse(JSON.stringify(req.query));
    var ssa = formatQueryParamater(sss);
    var entryType = req.query.transactionType;
    var conditions = Object.keys(ssa).map(function (key, value) {
      var obj = {}, newKey = "";
      if (key == "itemId") {
        newKey = key;
      }
      else {
        newKey = key;
        obj[newKey] = req.query[key];
      }
      return obj;
    });

    var toDate = new Date(req.query.toDate);
    toDate.setDate(toDate.getDate() + 1);
    //console.log(conditions);
    var obj = {
      tenant_id: new ObjectId(req.query.tenant_id),
      deployment_id: new ObjectId(req.query.deployment_id),
      created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
      $and: conditions
    }

    console.log(obj);
    StockTransaction.aggregate([
      {
        $match: {
          tenant_id: new ObjectId(req.query.tenant_id),
          deployment_id: new ObjectId(req.query.deployment_id),
          created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
          $and: conditions
        },
      },
      {
        $project: {
          _id: 1,
          created: 1,
          daySerialNumber: 1,
          transactionNumber: 1,
          transactionType:1,
          cartage:1,
          discount:1,
          discountType:1,
          vat:1,
          vatType:1,
          invoiceNumber:1,
          batchNumber:1,
          charges:1,
          "_store._id": 1,
          "_store.vendor._id": 1,
          "_store.vendor.category._id": 1,
          "_store.vendor.category.items._id": 1,
          "_store.vendor.category.items.qty": 1,
          "_store.vendor.category.items.price": 1,
          "_store.vendor.category.items.vatPercent": 1,
          "_store.vendor.category.items.itemName": 1,
          "_store.vendor.category.items.calculateInUnits": 1
        }
      }
    ], function (err, result) {
      var data = result;
      console.log(result);
      if (err) {
        handleError(res, err);
      } else {
        if (entryType == "4") {
          StockTransaction.aggregate([
            {
              $match: {
                tenant_id: new ObjectId(req.query.tenant_id),
                deployment_id: new ObjectId(req.query.deployment_id),
                created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
                transactionType: "15"
              }
            }
          ], function (err, result2) {
            if (err) {
              handleError(res, err);
            } else {
              _.forEach(result2, function (r) {
                data.push(r);
              });
              return res.json(data);
            }
          });
        }
        else {
          return res.json(data);
        }
      }
    });
  }
};



exports.updateTransactionUnits = function(req,res)
{
  console.log('req',req.query);
  if(req.query.baseQtyBaseUnit)
    var baseQtyBaseUnit=req.query.baseQtyBaseUnit
  if(req.query.selectedUnitBaseQty)
    var selectedUnitBaseQty=req.query.selectedUnitBaseQty
  if(req.query.preferedUnitBaseQty)
    var preferedUnitBaseQty=req.query.preferedUnitBaseQty

  if(req.query.baseQtyConversionFactor)
    var baseQtyConversionFactor=req.query.baseQtyConversionFactor
  if(req.query.selectedUnitConversionFactor)
    var selectedUnitConversionFactor=req.query.selectedUnitConversionFactor
  if(req.query.preferedUnitConversionFactor)
    var preferedUnitConversionFactor=req.query.preferedUnitConversionFactor

  if(req.query.preferedUnitBaseConversionFactor)
    var preferedUnitBaseConversionFactor=req.query.preferedUnitBaseConversionFactor
  if(req.query.itemId)
    var itemId=req.query.itemId
  if(req.query.entryType)
    var entryType=req.query.entryType
  

  //var deployment_id = new ObjectId(req.query.deployment_id);
  //var tenant_id = new ObjectId(req.query.tenant_id);
  var transaction_id = req.query._id;
  if(entryType==undefined)
  {

  console.log("No entry type")
  StockTransaction.findOne({
    //deployment_id : deployment_id,
    //tenant_id : tenant_id,
    _id : transaction_id
  },function(err,stockTransaction)
  {
//   console.log(stockTransaction);
    if(stockTransaction!=undefined)
    var transaction = stockTransaction;
    //console.log('transaction',transaction);
     if(transaction._store!=undefined)
      var stores = transaction._store;
     // console.log("before for")
      _.forEach(stores.category, function (c, i) {
        _.forEach(c.items, function (itm, ii) {
        //  console.log('itm',ii);
         // console.log("itemid",itemId);
          if(itm._id==itemId)
            console.log("item found");
          {
              _.forEach(itm.calculateInUnits,function(sUnits){
               // console.log("inside cal in unit");
              //  console.log(sUnits)
                if(baseQtyBaseUnit!=undefined)
                {
                 // console.log("inside if");
                  if(sUnits.type=='baseUnit')
                    sUnits.baseQty=baseQtyBaseUnit/baseQtyConversionFactor

                }
                if(selectedUnitBaseQty!=undefined)
                {
                  if(sUnits.type=="selectedUnit")
                    sUnits.baseQty=selectedUnitBaseQty/selectedUnitConversionFactor

                }
                if(preferedUnitBaseQty!=undefined)
                {
                  if(sUnits.type=="preferedUnit")
                    sUnits.baseQty=preferedUnitBaseQty/preferedUnitConversionFactor

                }
                if(preferedUnitBaseConversionFactor!=undefined)
                {
                  if(sunits.type=="preferedUnit")
                  sunits.baseConversionFactor=preferedUnitBaseConversionFactor
                }



              })
          }

        });
      });
      transaction._store=stores;
      var updated = _.merge(stockTransaction, transaction);
    updated.markModified("_store");
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
    //  console.log('stockTransaction',stockTransaction._store.vendor.category[0].items[0])
      return res.json(200, stockTransaction);
    });
      //console.log('stores',transaction);
      //return res.json(201,stores);
       // console.log('-----t2---',transaction)
  })

  
}
else if(entryType==2)
{
  console.log("Sale");
   StockTransaction.findOne({
    //deployment_id : deployment_id,
    //tenant_id : tenant_id,
    _id : transaction_id
  },function(err,stockTransaction)
  {
//   console.log(stockTransaction);
      if(stockTransaction!=undefined)
      var transaction = stockTransaction;
      //console.log('transaction',transaction);
       if(transaction._store!=undefined)
       {
          var stores = transaction._store;
         // console.log("before for")
          if(stores.receiver!=undefined)
          {
              _.forEach(stores.receiver.category, function (c, i) {
                _.forEach(c.items, function (itm, ii) {
                //  console.log('itm',ii);
                 // console.log("itemid",itemId);
                  if(itm._id==itemId)
                  //  console.log("item found");
                  {
                      _.forEach(itm.calculateInUnits,function(sUnits){
                      //  console.log("inside cal in unit");
                        //console.log(sUnits)
                        if(baseQtyBaseUnit!=undefined)
                        {
                          console.log("inside if");
                          if(sUnits.type=='baseUnit')
                            sUnits.baseQty=baseQtyBaseUnit/baseQtyConversionFactor
                           // console.log(sUnits)
                        }
                        if(selectedUnitBaseQty!=undefined)
                        {
                          if(sUnits.type=="selectedUnit")
                            sUnits.baseQty=selectedUnitBaseQty/selectedUnitConversionFactor

                        }
                        if(preferedUnitBaseQty!=undefined)
                        {
                          if(sUnits.type=="preferedUnit")
                            sUnits.baseQty=preferedUnitBaseQty/preferedUnitConversionFactor

                        }
                        if(preferedUnitBaseConversionFactor!=undefined)
                        {
                          if(sunits.type=="preferedUnit")
                          sunits.baseConversionFactor=preferedUnitBaseConversionFactor
                        }



                      })
                    }

                });
              });
              transaction._store=stores;
              var updated = _.merge(stockTransaction, transaction);
            updated.markModified("_store");
            updated.save(function (err) {
              if (err) {
                return handleError(res, err);
              }
            //  console.log('stockTransaction',stockTransaction._store.vendor.category[0].items[0])
              return res.json(200, stockTransaction);
            });
              //console.log('stores',transaction);
              //return res.json(201,stores);
               // console.log('-----t2---',transaction)

          }
      }   // end of store Check
  })



}
else if(entryType==1)
{
  console.log("Entry");
  StockTransaction.findOne({
    //deployment_id : deployment_id,
    //tenant_id : tenant_id,
    _id : transaction_id
  },function(err,stockTransaction)
  {
//   console.log(stockTransaction);
    if(stockTransaction!=undefined)
    var transaction = stockTransaction;
    //console.log('transaction',transaction);
     if(transaction._store!=undefined)
      var stores = transaction._store;
      //console.log("before for")
      if(stores.vendor!=undefined)
      {
            _.forEach(stores.vendor.category, function (c, i) {
              _.forEach(c.items, function (itm, ii) {
                //console.log('itm',ii);
               // console.log("itemid",itemId);
                if(itm._id==itemId)
                  console.log("item found");
                {
                    _.forEach(itm.calculateInUnits,function(sUnits){
                    //  console.log("inside cal in unit");
                      //console.log(sUnits)
                      if(baseQtyBaseUnit!=undefined)
                      {
                       // console.log("inside if");
                        if(sUnits.type=='baseUnit')
                          sUnits.baseQty=baseQtyBaseUnit/baseQtyConversionFactor
                         // console.log(sUnits)
                      }
                      if(selectedUnitBaseQty!=undefined)
                      {
                        if(sUnits.type=="selectedUnit")
                          sUnits.baseQty=selectedUnitBaseQty/selectedUnitConversionFactor

                      }
                      if(preferedUnitBaseQty!=undefined)
                      {
                        if(sUnits.type=="preferedUnit")
                          sUnits.baseQty=preferedUnitBaseQty/preferedUnitConversionFactor

                      }
                      if(preferedUnitBaseConversionFactor!=undefined)
                      {
                        if(sunits.type=="preferedUnit")
                        sunits.baseConversionFactor=preferedUnitBaseConversionFactor
                      }



                    })
                }

              });
            });
            transaction._store=stores;
            var updated = _.merge(stockTransaction, transaction);
          updated.markModified("_store");
          updated.save(function (err) {
            if (err) {
              return handleError(res, err);
            }
          //  console.log('stockTransaction',stockTransaction._store.vendor.category[0].items[0])
            return res.json(200, stockTransaction);
          });
            //console.log('stores',transaction);
            //return res.json(201,stores);
             // console.log('-----t2---',transaction)

      } //end of vendors check

   })
  }
}
exports.getAllByTranasactionAndDateRangeNew = function (req, res) {
  //console.log("Hello Ajit");
  console.log('new');
  var sss = JSON.parse(JSON.stringify(req.query));
  var startDate = new Date(req.query.fromDate);
  startDate.setDate(startDate.getDate());
  //console.log(new Date(startDate));

  var endDate = new Date(req.query.toDate);
  endDate.setDate(endDate.getDate() + 1);
  //console.log(new Date(endDate));

  var ssa = formatQueryParamater(sss);
  var conditions = Object.keys(ssa).map(function (key, value) {
    //console.log(key);
    var obj = {},
      newKey = "";
    if (key == "itemId") {
      newKey = key;
    }
    else {
      newKey = key;
      obj[newKey] = req.query[key];
    }
    return obj;
  });

//console.log(conditions);
  //if(req.query.store._id!=""){
  StockTransaction.aggregate([
    {
      $match: {
        tenant_id: new ObjectId(req.query.tenant_id),
        deployment_id: new ObjectId(req.query.deployment_id),
        //transactionType: req.query.transactionType,
        created: {$gte: new Date(startDate), $lte: new Date(endDate)},
        $and: conditions
      }
    },
    {
      $project:
      {   daySerialNumber:1,
        tenant_id:1,
        deployment_id:1,
        discount:1,
        created:1,
        discountType:1,
        serviceCharge:1,
        serviceChargeType:1,
        vat:1,
        vatType:1,
        invoiceNumber:1,
        batchNumber:1,
        payment:1,
        transactionNumber:1,
        transactionType:1,
        cartage:1,
        charges:1,
        '_store._id':1,
        '_store.storeName': 1,
        '_store.vendor._id':1,
        '_store.vendor.vendorName':1,
        '_store.vendor.category.items.itemName':1,
        '_store.vendor.category.items.qty':1,
        '_store.vendor.category.items.price':1,
        '_store.vendor.category.items.subTotal':1,
        '_store.vendor.category.items.addedAmt':1,
        '_store.vendor.category.items.vatPercent':1,
        '_store.vendor.category.items.calculateInUnits.unitName':1,
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } }

      }

    },
    {
      $project: {
        daySerialNumber:1,
        tenant_id:1,
        deployment_id:1,
        discount:1,
        created:1,
        discountType:1,
        serviceCharge:1,
        serviceChargeType:1,
        vat:1,
        vatType:1,
        invoiceNumber:1,
        batchNumber:1,
        payment:1,
        transactionNumber:1,
        transactionType:1,
        cartage:1,
        charges:1,
        '_store._id':1,
        '_store.storeName': 1,
        '_store.vendor._id':1,
        '_store.vendor.vendorName':1,
        '_store.vendor.category.items.itemName':1,
        '_store.vendor.category.items.qty':1,
        '_store.vendor.category.items.price':1,
        '_store.vendor.category.items.subTotal':1,
        '_store.vendor.category.items.addedAmt':1,
        '_store.vendor.category.items.vatPercent':1,
        '_store.vendor.category.items.calculateInUnits.unitName':1,
        '_store.receiver._id':1,
        '_store.receiver.receiverName':1,
        '_store.receiver.category.items.itemName':1,
        '_store.receiver.category.items.price':1,
        '_store.receiver.category.items.vatPercent':1,
        '_store.receiver.category.items.calculateInUnits.unitName':1,
        '_store.receiver.category.items.total':1,
        '_store.receiver.category.items.qty':1

      }
    }

  ], function (err, result) {
    if (err) {
      handleError(res, err);
      //next(err);
    } else {
      return res.json(result);
    }
  });
  //}
  // else
  // {
  //   StockTransaction.aggregate([
  //         {
  //             $match: {
  //                 tenant_id:new ObjectId(req.query.tenant_id),
  //                 deployment_id: new ObjectId(req.query.deployment_id),
  //                 transactionType: req.query.transactionType,
  //                 created: {$gte: new Date(startDate), $lte : new Date(endDate)}
  //                 //$and: conditions
  //             }
  //         }
  //     ], function (err, result) {
  //         if (err) {
  //             handleError(res, err);
  //             //next(err);
  //         } else {
  //             return res.json(result);
  //         }
  //     });
  // }
};
exports.getBasicEntryReportsForEntries = function (req, res) {
  //console.log('fuck stock');
  //console.log("Basic Report");
  //console.log(req.query);
  var sss = JSON.parse(JSON.stringify(req.query));
  var ssa = formatQueryParamater(sss);
  var entryType = req.query.transactionType;
  var conditions = Object.keys(ssa).map(function (key, value) {
    var obj = {}, newKey = "";
    if (key == "itemId") {
      newKey = key;
      obj[newKey] = req.query[key];
    }
    else if(key == "_store.receiver._id")
    {
      newKey = '_receiver._id';

      obj[newKey] = req.query[key];
    }
    else {
      newKey = key;
      obj[newKey] = req.query[key];
    }
    return obj;
  });
  console.log('conditions',conditions);
  var toDate = new Date(req.query.toDate);
  toDate.setDate(toDate.getDate() + 1);
  //console.log(conditions);
  var obj = {
    tenant_id: new ObjectId(req.query.tenant_id),
    deployment_id: new ObjectId(req.query.deployment_id),
    created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
    $and: conditions
  }

  //console.log(obj);
  StockTransaction.aggregate([
    {
      $match: {
        tenant_id: new ObjectId(req.query.tenant_id),
        deployment_id: new ObjectId(req.query.deployment_id),
        created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
        $and: conditions
      }
    },
    //{
    //  $unwind:"$_store.receiver.category",
    //},
    //{
    //  $unwind:"$_store.receiver.category.items",
    //},
    //{
    //  $unwind:"$_store.receiver.category.items.calculateInUnits",
    //},
    {
      $project:
      {   daySerialNumber:1,
        tenant_id:1,
        deployment_id:1,
        discount:1,
        created:1,
        discountType:1,
        serviceCharge:1,
        serviceChargeType:1,
        vat:1,
        vatType:1,
        invoiceNumber:1,
        batchNumber:1,
        payment:1,
        transactionNumber:1,
        transactionType:1,
        cartage:1,
        wastageEntryType:1,
        charges:1,
        '_store._id':1,
        '_store.storeName': 1,
        '_store.vendor._id':1,
        '_store.vendor.vendorName':1,
        '_store.vendor.category.items.itemName':1,
        '_store.vendor.category.items.qty':1,
        '_store.vendor.category.items.price':1,
        '_store.vendor.category.items.itemCode':1,
        '_store.vendor.category.items.total':1,
        '_store.vendor.category.items.vatPercent':1,
        '_store.vendor.category.items.calculateInUnits':1,
        "_store.receiver":{ $cond: { if:  "$_.store.receiver", then: "$_store.receiver", else: "$_receiver" } },
        '_store.category.items.returnWastageQty':1,
        '_store.category.items.qty':1,
        '_store.category.items.toStore.storeName':1,
        '_store.category.items._id':1,
        '_store.category.items.price':1,
        '_store.category.items.itemCode':1,
        '_store.category.items.itemPrice':1,
        '_store.category.items.itemName':1,
        '_store.category.items.calculateInUnits':1,
      }

    },
    {
      $project: {
        daySerialNumber:1,
        tenant_id:1,
        deployment_id:1,
        discount:1,
        created:1,
        discountType:1,
        serviceCharge:1,
        serviceChargeType:1,
        vat:1,
        vatType:1,
        invoiceNumber:1,
        batchNumber:1,
        payment:1,
        transactionNumber:1,
        transactionType:1,
        cartage:1,
        wastageEntryType:1,
        charges:1,
        '_store._id':1,
        '_store.storeName': 1,
        '_store.vendor._id':1,
        '_store.vendor.vendorName':1,
        '_store.vendor.category.items.itemName':1,
        '_store.vendor.category.items.qty':1,
        '_store.vendor.category.items.price':1,
        '_store.vendor.category.items.itemCode':1,
        '_store.vendor.category.items.total':1,
        '_store.vendor.category.items.vatPercent':1,
        '_store.vendor.category.items.calculateInUnits':1,
        '_store.receiver._id':1,
        '_store.receiver.receiverName':1,
        '_store.receiver.category.items.itemName':1,
        '_store.receiver.category.items.price':1,
        '_store.receiver.category.items.itemCode':1,
        '_store.receiver.category.items.vatPercent':1,
        '_store.receiver.category.items.calculateInUnits':1,
        '_store.receiver.category.items.total':1,
        '_store.receiver.category.items.qty':1,
        '_store.category.items.returnWastageQty':1,
        '_store.category.items.qty':1,
        '_store.category.items._id':1,
        '_store.category.items.toStore.storeName':1,
        '_store.category.items.price':1,
        '_store.category.items.itemCode':1,
        '_store.category.items.itemPrice':1,
        '_store.category.items.itemName':1,
        '_store.category.items.calculateInUnits':1
      }
    }
  ], function (err, result) {

    var data = result;

    //console.log(result);
    if (err) {
      handleError(res, err);
    } else {
      if (entryType == "4") {
        StockTransaction.aggregate([
          {
            $match: {
              tenant_id: new ObjectId(req.query.tenant_id),
              deployment_id: new ObjectId(req.query.deployment_id),
              created: {$gt: new Date(req.query.fromDate), $lte: new Date(toDate)},
              transactionType: "15"
            }
          },
          {
            $project:
            {
              daySerialNumber:1,
              created:1,
              transactionNumber:1,
              transactionType:1,
              '_store._id':1,
              '_store.category.items.returnWastageQty':1,
              '_store.category.items.qty':1,
              '_store.category.items.price':1,
              '_store.category.items.itemCode':1,
              '_store.category.items.itemName':1,
              '_store.category.items.calculateInUnits.unitName':1
            }
          }
        ], function (err, result2) {
          if (err) {
            handleError(res, err);
          } else {
            _.forEach(result2, function (r) {
              data.push(r);
            });
            return res.json(data);
          }
        });
      }
      else {
        return res.json(data);
      }
    }
  });
};




// Creates a new stockTransaction in the DB.
exports.create = function (req, res) {
  StockTransaction.create(req.body, function (err, stockTransaction) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(201, stockTransaction);
  });
};

exports.createMultiple = function (req, res) {

  //console.log('createMultiple');
  //console.log(req.body.transactions);
  var rtransactions = req.body.transactions;
  //console.log(rtransactions.length);
  var response = {};
  async.eachSeries(rtransactions, function (rtransaction, callback) {

    //console.log(rtransaction);
    var transactionId = rtransaction._id;
    //console.log(transactionId);
    var _data = rtransaction;
    delete rtransaction.transactionNumber;
    delete rtransaction.daySerialNumber;
    delete rtransaction._id;
    delete rtransaction.db;
    _data.isSynced = true;
    _data.syncedOn = new Date().toISOString();
    _data.transaction_id = transactionId;
    StockTransaction.findOne({
      'transactionId': transactionId,
      deployment_id: new ObjectId(rtransaction.deployment_id),
      tenant_id: new ObjectId(rtransaction.tenant_id)
    }, {}, function (err, transactions) {
      //console.log('findOne');
      if (!transactions) {
        //console.log('not transactions');
        var today = new Date(_data.created);
        today.setHours(0, 0, 0, 0);
        _data.timeStamp = today;
        var endDate = new Date();
        endDate.setDate(today.getDate() + 1);
        endDate.setHours(0, 0, 0, 0);

        StockTransaction.find({
          'transactionType': _data.transactionType,
          deployment_id: new ObjectId(rtransaction.deployment_id),
          tenant_id: new ObjectId(rtransaction.tenant_id)
        }, {}, {sort: {'created': -1}}, function (err, post) {

          //console.log('check null');
          if (post != null) {
            //console.log('not null');
            //console.log( "post = "+post );
            _data.transactionNumber = parseInt(post.length) + 1;
          }
          else {
            _data.transactionNumber = 1;
          }

          StockTransaction.find({
            'transactionType': _data.transactionType,
            deployment_id: new ObjectId(rtransaction.deployment_id),
            tenant_id: new ObjectId(rtransaction.tenant_id),
            created: {$gte: today, $lt: endDate}
          }, {}, {sort: {'created': -1}}, function (err, post1) {

            //console.log('check null1');
            if (post1 != null) {
              //console.log('not null1');
              _data.daySerialNumber = parseInt(post1.length) + 1;
            }
            else {
              _data.daySerialNumber = 1;
            }

            StockTransaction.create(_data, function (err, trans) {
              console.log(err);
              if (err) {
                return handleError(res, err);
              }
              //console.log('callback');
              callback();
            });

          });
        });

      } else {

        //console.log('else');
        var updated = _.extend(transactions, rtransaction);
        updated.save(function (err) {
          if (err) {
            return handleError(res, err);
          }
          callback();
        });
      }
    });
  }, function done() {
    //console.log('done');
    if (res.err)
      return res.status(500).json({errorcode: 500, message: response.err});
    return res.status(200).json({errorcode: 200, message: "OK"});
  });
};


function sendMail() {

}

exports.updateTransaction = function(req,res)
{
  console.log('req',req.query);
  //var deployment_id = new ObjectId(req.query.deployment_id);
  //var tenant_id = new ObjectId(req.query.tenant_id);
  var transaction_id = req.query._id;

  StockTransaction.findOne({
    //deployment_id : deployment_id,
    //tenant_id : tenant_id,
    _id : transaction_id
  },function(err,stockTransaction)
  {
    var transaction = stockTransaction;
    //console.log('transaction',transaction);
      var stores = transaction._store;
      stores.category = [];
      _.forEach(transaction._items, function (itm, i) {
        if(itm.fromCategory)
        {
        var index = _.findIndex(stores.category, {_id: itm.fromCategory._id} );
        if (index < 0) {

          stores.category.push(itm.fromCategory);
        }
        }
      });
      _.forEach(stores.category, function (c, i) {
        c.items = [];
        _.forEachRight(transaction._items, function (itm, ii) {
          if(itm.fromCategory)
          {
          if (c._id == itm.fromCategory._id) {
            delete itm.fromCategory;
            c.items.push(itm);
            //item.splice(ii, 1);
          }
          }
        });
      });
      transaction._store=stores;
      var updated = _.merge(stockTransaction, transaction);
    updated.markModified("_store");
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      console.log('stockTransaction',stockTransaction)
      return res.json(200, stockTransaction);
    });
      //console.log('stores',transaction);
      //return res.json(201,stores);
       // console.log('-----t2---',transaction)
  })
}

exports.createWithTransactionId = function (req, res) {
  var transactionId = req.params.id;
 console.log(req.body);
  var _data = req.body
  delete req.body._id;
  delete req.body.db;
  _data.isSynced = true;
  _data.syncedOn = new Date().toISOString();
  _data.transaction_id = transactionId;

  StockTransaction.findOne({
    'transactionId': req.params.id,
    deployment_id: new ObjectId(req.body.deployment_id),
    tenant_id: new ObjectId(req.body.tenant_id)
  }, {_id: 1, transactionId: 1}, function (err, transaction) {
    if(err){
     // console.log(err);
      return handleError(res, err);
    }
    if (!transaction) {
      console.log('Transaction',transaction);
      var today = new Date(_data.created);
      _data.timeStamp = today;
      StockTransaction.create(_data, function (err, trans) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(201, trans);
      });
    }
    else {
      console.log("err");
      return res.send(500, {errorcode: 1});
    }
  });
};

// Create a new Transaction with Angular ID.
exports.createWithId = function (req, res) {
  var transactionId = req.params.id;
  var _data = req.body;
  delete req.body.transactionNumber;
  delete req.body.daySerialNumber;
  delete req.body._id;
  delete req.body.db;
  _data.isSynced = true;
  _data.syncedOn = new Date().toISOString();
  _data.transaction_id = transactionId;

  StockTransaction.findOne({
    'transactionId': req.params.id,
    deployment_id: new ObjectId(req.body.deployment_id),
    tenant_id: new ObjectId(req.body.tenant_id)
  }, {}, function (err, transactions) {
    if (!transactions) {
      var today = new Date(_data.created);
      today.setHours(0, 0, 0, 0);
      _data.timeStamp = today;
      var endDate = new Date();
      endDate.setDate(today.getDate() + 1);
      endDate.setHours(0, 0, 0, 0);

      StockTransaction.count({
        'transactionType': _data.transactionType,
        deployment_id: new ObjectId(req.body.deployment_id),
        tenant_id: new ObjectId(req.body.tenant_id)
      }, function (err, count1) {

        _data.transactionNumber = count1 + 1;
        console.log('count0', count1);
        //if (post != null) {
        //  //console.log( "post = "+post );
        //  _data.transactionNumber = parseInt(post.length) + 1;
        //}
        //else {
        //  _data.transactionNumber = 1;
        //}

        StockTransaction.count({
          'transactionType': _data.transactionType,
          deployment_id: new ObjectId(req.body.deployment_id),
          tenant_id: new ObjectId(req.body.tenant_id),
          created: {$gte: today, $lt: endDate}
        }, function (err, count) {

          _data.daySerialNumber = count + 1;
          console.log('count', count);
          StockTransaction.create(_data, function (err, trans) {
            if (err) {
              return handleError(res, err);
            }
            return res.json(201, trans);
          });
        });
      });
      //
      //    if(post1!=null){
      //      _data.daySerialNumber= parseInt(post1.length) + 1 ;
      //    }
      //    else
      //    {
      //      _data.daySerialNumber=1 ;
      //    }
      //
    }
    else {
      return res.send(500, {errorcode: 1});
    }
  });

};
// Updates an existing stockTransaction in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  //console.log(req.body);
  StockTransaction.findById(req.params.id, function (err, stockTransaction) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockTransaction) {
      return res.send(404);
    }
    //var updated = _.merge(stockTransaction, req.body);
    var updated = _.extend(stockTransaction, req.body);
    updated.markModified("_store");
    updated.markModified("discountType");
    updated.markModified("discount");
    updated.markModified("serviceChargeType");
    updated.markModified("serviceCharge");
    updated.markModified("cartage");
    updated.markModified("vatType");
    updated.markModified("vat");
    updated.markModified("payment");
    updated.markModified("heading");
    updated.markModified("templateName");
    updated.markModified("wastageEntryType");
    updated.markModified("wastageItemType");

    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, stockTransaction);
    });
  });
};
// Deletes a stockTransaction from the DB By Id.
exports.destroy = function (req, res) {
  StockTransaction.findById(req.params.id, function (err, stockTransaction) {
    if (err) {
      return handleError(res, err);
    }
    if (!stockTransaction) {
      return res.send(404);
    }
    stockTransaction.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};

//Get Stock Summary
exports.getStockSummary = function (req, res) {
  //console.log("Hello Ajit");

  StockTransaction.aggregate([
    {
      $match: {
        transactionType: "1"
        //created:{$gt: new Date(req.query.fromDate), $lte : new Date(req.query.toDate)}
      }
    },
    {
      $unwind: "$_store.vendor.category"
    },
    {
      $unwind: "$_store.vendor.category.items"
    },
    {
      $project: {
        category: "$_store.vendor.category",
        items: "$_store.vendor.category.items",
        created: "$created",
        billNo: "$daySerialNumber"
      }
    },
    {
      $group: {
        //_id:"$items._id",
        _id: {_id: "$items._id", name: "$items.itemName"},
        totalAmt: {$sum: "$items.subTotal"},
        totalQty: {$sum: "$items.qty"}
      }
    },
    {
      $project: {
        totalPurchaseQty: "$totalQty",
        totalPurchaseAmount: "$totalAmt",
        itemName: "$_id.name",
        itemId: "$_id._id"
      }
    }
  ], function (err, result_entry) {
    if (err) {
      handleError(res, err);
    }
    else {
      //return res.json(result);
      StockTransaction.aggregate([
        {
          $match: {
            transactionType: "2"
            //created:{$gt: new Date(req.query.fromDate), $lte : new Date(req.query.toDate)}
          }
        },
        {
          $unwind: "$_store.receiver.category"
        },
        {
          $unwind: "$_store.receiver.category.items"
        },
        {
          $project: {
            category: "$_store.receiver.category",
            items: "$_store.receiver.category.items",
            created: "$created",
            billNo: "$daySerialNumber"
          }
        },
        {
          $group: {
            //_id:"$items._id",
            _id: {_id: "$items._id", name: "$items.itemName"},
            totalAmt: {$sum: {$multiply: ["$items.qty", "$items.price"]}},
            totalQty: {$sum: "$items.qty"}
          }
        },
        {
          $project: {
            totalSaleQty: "$totalQty",
            totalSaleAmount: "$totalAmt",
            itemName: "$_id.name",
            itemId: "$_id._id"
          }
        }
      ], function (err, result_sale) {
        if (err) {
          handleError(res, err);
        }
        else {
          _.forEach(result_sale, function (s, i) {
            var index = _.findIndex(result_entry, {"itemId": s.itemId});
            if (index < 0) {
              result_entry.push(s);
            }
            else {
              result_entry[index].totalSaleQty = s.totalSaleQty;
              result_entry[index].totalSaleAmount = s.totalSaleAmount;
            }
          });

          return res.json(result_entry);
        }
      });
    }
  });
};

// Create a new Bill with Angular ID.
exports.getLatestTransaction = function (req, res) {
  var tenant_id = req.query.tenantId;
  var deployment_id = req.query.deploymentId;
  getLatestTransaction(deployment_id, tenant_id, function (result) {
    return res.json(result);
  });
};

exports.getLatestTransactionOfType = function (req, res) {
  var tenant_id = req.query.tenantId;
  var deployment_id = req.query.deploymentId;
  var transactionType = req.query.transactionType.toString();
  console.log(tenant_id, deployment_id, transactionType);
  getLatestTransactionOfType(deployment_id, tenant_id, transactionType, function (result) {
    return res.json(result);
  });
};

exports.getLatestTransactionOfType = function (req, res) {
  var tenant_id = req.query.tenantId;
  var deployment_id = req.query.deploymentId;
  var transactionType = req.query.transactionId;
  console.log(tenant_id, deployment_id, transactionId);
  //getLatestTransactionOfType(deployment_id, tenant_id, transactionType, function (result) {
  //return res.json(result);
  //});
};

function getLatestTransaction(deployment_id, tenant_id, callback) {
  var data = [];
  getLatestTransaction_entry(deployment_id, tenant_id, function (entry) {
    _.forEach(entry, function (e, i) {
      data.push(e);
    });
    getLatestTransaction_sale(deployment_id, tenant_id, function (sale) {
      _.forEach(sale, function (e, i) {
        data.push(e);
      });
      getLatestTransaction_Physical(deployment_id, tenant_id, function (physical) {
        _.forEach(physical, function (e, i) {
          data.push(e);
        });
        getLatestTransaction_wastage(deployment_id, tenant_id, function (Wastage) {
          _.forEach(Wastage, function (e, i) {
            data.push(e);
          });
          getLatestTransaction_transfer(deployment_id, tenant_id, function (transfer) {
            _.forEach(transfer, function (e, i) {
              data.push(e);
            });
            getLatestTransaction_Opening(deployment_id, tenant_id, function (opening) {
              _.forEach(opening, function (e, i) {
                data.push(e);
              });
              getAllTemplates(deployment_id, tenant_id, function (err, templates) {
                _.forEach(templates, function (e, i) {
                  data.push(e);
                });
                callback(data);
              })
            });
          });
        });
      });
    });
  });
};

function getAllTemplates (deployment_id, tenant_id, callback) {

  StockTransaction.find({
    tenant_id: tenant_id,
    deployment_id: deployment_id,
    transactionType: {$in: ["101", "102", "103", "104", "105", "106"]}
  }, function (err, templates) {
    console.log("alltemplates", templates);
    if(err)
      callback(err);
    else
      callback(null, templates);
  });
}

function getLatestTransactionOfType(deployment_id, tenant_id, transactionType, callback) {

  getLatestTransaction_Type(deployment_id, tenant_id, transactionType, function (transaction) {
    callback(transaction);
  });
};

function getLatestTransaction_Type (deployment_id, tenant_id, transactionType, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': transactionType
  }, {_id: 1, transactionNumber: 1, daySerialNumber: 1, created: 1}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, transaction) {
    if (err) {
      callback(err);
    }
    else {
      callback(transaction);
    }
  });
}

function getLatestTransaction_IntermediateEntry (deployment_id, tenant_id, callback){
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "7"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
}

function getLatestTransaction_PreparedEntry(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "11"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
}

function getLatestTransaction_IntermediateOpening(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "10"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
}

function getLatestTransaction_PreparedOpening(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "13"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
}

//   myModel.find(filter, {sort: {created_at: -1}, limit: 10}, function(err, items){});
function getLatestTransaction_entry(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "1"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_sale(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "2"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_Physical(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "3"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_wastage(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "4"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_transfer(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "5"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};
function getLatestTransaction_Opening(deployment_id, tenant_id, callback) {
  StockTransaction.find({
    'tenant_id': new ObjectId(tenant_id),
    'deployment_id': new ObjectId(deployment_id),
    'transactionType': "6"
  }, {}, {sort: {'transactionNumber': -1}, limit: 1}, function (err, bill_entry) {
    if (err) {
      callback(err);
    }
    else {
      callback(bill_entry);
    }
  });
};

exports.checkIfMenuItemExistsInTransaction = function (req, res) {
  
  var deployment_id = req.query.deployment_id;
  var tenant_id = req.query.tenant_id;
  var itemId = req.query.itemId;

  Recipe.findOne({itemId: itemId}, {_id: 1, recipeName: 1}, function (err, recipe) {
    if(err)
      return handleError(res, err);
    if(!recipe)
      return res.status(200).json({transactionFound: false});
    console.log(recipe._id);
    StockTransaction.find({
      tenant_id: new ObjectId(tenant_id),
      $or: [{"_store.category.items._id": recipe._id}, {"_store.receiver.category.items._id": recipe._id}, {"_store.receiver.category.items.receipeDetails._id": recipe._id}, {"_store.category.items.receipeDetails._id": recipe._id}]
    }, function (err, transaction) {
      //console.log(transaction);
      if(err)
        return handleError(res, err);
      if(!transaction)
        return res.status(200).json({transactionFound: false});
      return res.status(200).json({transactionFound: true});
    });
  });
};

exports.updateTransactionUnits = function(req,res)
{
  console.log('req',req.query);
  if(req.query.baseQtyBaseUnit)
    var baseQtyBaseUnit=req.query.baseQtyBaseUnit
  if(req.query.selectedUnitBaseQty)
    var selectedUnitBaseQty=req.query.selectedUnitBaseQty
  if(req.query.preferedUnitBaseQty)
    var preferedUnitBaseQty=req.query.preferedUnitBaseQty

  if(req.query.baseQtyConversionFactor)
    var baseQtyConversionFactor=req.query.baseQtyConversionFactor
  if(req.query.selectedUnitConversionFactor)
    var selectedUnitConversionFactor=req.query.selectedUnitConversionFactor
  if(req.query.preferedUnitConversionFactor)
    var preferedUnitConversionFactor=req.query.preferedUnitConversionFactor

  if(req.query.preferedUnitBaseConversionFactor)
    var preferedUnitBaseConversionFactor=req.query.preferedUnitBaseConversionFactor
  if(req.query.itemId)
    var itemId=req.query.itemId
  if(req.query.entryType)
    var entryType=req.query.entryType
  

  //var deployment_id = new ObjectId(req.query.deployment_id);
  //var tenant_id = new ObjectId(req.query.tenant_id);
  var transaction_id = req.query._id;
  if(entryType==undefined)
  {

  console.log("No entry type")
  StockTransaction.findOne({
    //deployment_id : deployment_id,
    //tenant_id : tenant_id,
    _id : transaction_id
  },function(err,stockTransaction)
  {
//   console.log(stockTransaction);
    if(stockTransaction!=undefined)
    var transaction = stockTransaction;
    //console.log('transaction',transaction);
     if(transaction._store!=undefined)
      var stores = transaction._store;
     // console.log("before for")
      _.forEach(stores.category, function (c, i) {
        _.forEach(c.items, function (itm, ii) {
        //  console.log('itm',ii);
         // console.log("itemid",itemId);
          if(itm._id==itemId)
            console.log("item found");
          {
              _.forEach(itm.calculateInUnits,function(sUnits){
               // console.log("inside cal in unit");
              //  console.log(sUnits)
                if(baseQtyBaseUnit!=undefined)
                {
                 // console.log("inside if");
                  if(sUnits.type=='baseUnit')
                    sUnits.baseQty=baseQtyBaseUnit/baseQtyConversionFactor

                }
                if(selectedUnitBaseQty!=undefined)
                {
                  if(sUnits.type=="selectedUnit")
                    sUnits.baseQty=selectedUnitBaseQty/selectedUnitConversionFactor

                }
                if(preferedUnitBaseQty!=undefined)
                {
                  if(sUnits.type=="preferedUnit")
                    sUnits.baseQty=preferedUnitBaseQty/preferedUnitConversionFactor

                }
                if(preferedUnitBaseConversionFactor!=undefined)
                {
                  if(sunits.type=="preferedUnit")
                  sunits.baseConversionFactor=preferedUnitBaseConversionFactor
                }



              })
          }

        });
      });
      transaction._store=stores;
      var updated = _.merge(stockTransaction, transaction);
    updated.markModified("_store");
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
    //  console.log('stockTransaction',stockTransaction._store.vendor.category[0].items[0])
      return res.json(200, stockTransaction);
    });
      //console.log('stores',transaction);
      //return res.json(201,stores);
       // console.log('-----t2---',transaction)
  })

  
}
else if(entryType==2)
{
  console.log("Sale");
   StockTransaction.findOne({
    //deployment_id : deployment_id,
    //tenant_id : tenant_id,
    _id : transaction_id
  },function(err,stockTransaction)
  {
//   console.log(stockTransaction);
      if(stockTransaction!=undefined)
      var transaction = stockTransaction;
      //console.log('transaction',transaction);
       if(transaction._store!=undefined)
       {
          var stores = transaction._store;
         // console.log("before for")
          if(stores.receiver!=undefined)
          {
              _.forEach(stores.receiver.category, function (c, i) {
                _.forEach(c.items, function (itm, ii) {
                //  console.log('itm',ii);
                 // console.log("itemid",itemId);
                  if(itm._id==itemId)
                  //  console.log("item found");
                  {
                      _.forEach(itm.calculateInUnits,function(sUnits){
                      //  console.log("inside cal in unit");
                        //console.log(sUnits)
                        if(baseQtyBaseUnit!=undefined)
                        {
                          console.log("inside if");
                          if(sUnits.type=='baseUnit')
                            sUnits.baseQty=baseQtyBaseUnit/baseQtyConversionFactor
                           // console.log(sUnits)
                        }
                        if(selectedUnitBaseQty!=undefined)
                        {
                          if(sUnits.type=="selectedUnit")
                            sUnits.baseQty=selectedUnitBaseQty/selectedUnitConversionFactor

                        }
                        if(preferedUnitBaseQty!=undefined)
                        {
                          if(sUnits.type=="preferedUnit")
                            sUnits.baseQty=preferedUnitBaseQty/preferedUnitConversionFactor

                        }
                        if(preferedUnitBaseConversionFactor!=undefined)
                        {
                          if(sunits.type=="preferedUnit")
                          sunits.baseConversionFactor=preferedUnitBaseConversionFactor
                        }



                      })
                    }

                });
              });
              transaction._store=stores;
              var updated = _.merge(stockTransaction, transaction);
            updated.markModified("_store");
            updated.save(function (err) {
              if (err) {
                return handleError(res, err);
              }
            //  console.log('stockTransaction',stockTransaction._store.vendor.category[0].items[0])
              return res.json(200, stockTransaction);
            });
              //console.log('stores',transaction);
              //return res.json(201,stores);
               // console.log('-----t2---',transaction)

          }
      }   // end of store Check
  })



}
else if(entryType==1)
{
  console.log("Entry");
  StockTransaction.findOne({
    //deployment_id : deployment_id,
    //tenant_id : tenant_id,
    _id : transaction_id
  },function(err,stockTransaction)
  {
//   console.log(stockTransaction);
    if(stockTransaction!=undefined)
    var transaction = stockTransaction;
    //console.log('transaction',transaction);
     if(transaction._store!=undefined)
      var stores = transaction._store;
      //console.log("before for")
      if(stores.vendor!=undefined)
      {
            _.forEach(stores.vendor.category, function (c, i) {
              _.forEach(c.items, function (itm, ii) {
                //console.log('itm',ii);
               // console.log("itemid",itemId);
                if(itm._id==itemId)
                  console.log("item found");
                {
                    _.forEach(itm.calculateInUnits,function(sUnits){
                    //  console.log("inside cal in unit");
                      //console.log(sUnits)
                      if(baseQtyBaseUnit!=undefined)
                      {
                       // console.log("inside if");
                        if(sUnits.type=='baseUnit')
                          sUnits.baseQty=baseQtyBaseUnit/baseQtyConversionFactor
                         // console.log(sUnits)
                      }
                      if(selectedUnitBaseQty!=undefined)
                      {
                        if(sUnits.type=="selectedUnit")
                          sUnits.baseQty=selectedUnitBaseQty/selectedUnitConversionFactor

                      }
                      if(preferedUnitBaseQty!=undefined)
                      {
                        if(sUnits.type=="preferedUnit")
                          sUnits.baseQty=preferedUnitBaseQty/preferedUnitConversionFactor

                      }
                      if(preferedUnitBaseConversionFactor!=undefined)
                      {
                        if(sunits.type=="preferedUnit")
                        sunits.baseConversionFactor=preferedUnitBaseConversionFactor
                      }



                    })
                }

              });
            });
            transaction._store=stores;
            var updated = _.merge(stockTransaction, transaction);
          updated.markModified("_store");
          updated.save(function (err) {
            if (err) {
              return handleError(res, err);
            }
          //  console.log('stockTransaction',stockTransaction._store.vendor.category[0].items[0])
            return res.json(200, stockTransaction);
          });
            //console.log('stores',transaction);
            //return res.json(201,stores);
             // console.log('-----t2---',transaction)

      } //end of vendors check

  })
}
}

function handleError(res, err) {
  return res.send(500, err);
}




