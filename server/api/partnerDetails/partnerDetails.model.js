'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PartnerDetailsSchema = new Schema({
    partner_name: String,
    client_id: String,
    created: {
    	type: Date,
    	default: Date.now
    },
    class:[]
});

module.exports = mongoose.model('PartnerDetail', PartnerDetailsSchema);