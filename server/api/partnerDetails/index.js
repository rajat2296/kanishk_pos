'use strict';

var express = require('express');
var controller = require('./partnerDetails.controller');

var router = express.Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.put('/', controller.update);
router.delete('/:id', controller.destroy);
router.get('/:id',controller.getPartnersByDeployment);

module.exports = router;