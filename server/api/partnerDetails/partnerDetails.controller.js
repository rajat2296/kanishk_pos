'use strict'

var _ = require('lodash');
var PartnerDetails = require('./partnerDetails.model');
var Partner = require('../partners/partners.model');

exports.index = function(req, res){
  PartnerDetails.find({}, function(err, partners){
    if(err) res.status(500).json();
    else{
      res.json(partners);
    }
  });
};

exports.create = function(req, res){
  PartnerDetails.findOne({partner_name: req.body.partner_name}, function(err, partner){
    if(err) { res.status(500).send(err); }
    else if(!partner){
      var newpartner = new PartnerDetails(req.body);
      console.log("partner",req.body);
      newpartner.save(function(err, partner){
        if(err) res.status(500).json();
        else{
          res.json(partner);
        }
      });
    }else{
      res.status(401).send('Partner name already exist.');
    }
  });
};

exports.update = function(req, res){
  PartnerDetails.findOneAndUpdate({partner_name: req.body.partner_name}, {$set: {client_id:req.body.client_id,class:req.body.class}}, function(err, partner){
    if(err) res.status(500).json();
    else{
      res.json(partner);
    }
  });
};

exports.destroy = function(req, res){
  PartnerDetails.findByIdAndRemove(req.params.id, function (err, partner) {
    if(err) { res.status(500).send(); }
    else if(!partner) { res.status(404).send(); }
    else{
      Partner.remove({partner_name: partner.partner_name}, function(err, p){
        if(err){
          res.status(500).send(err);
          PartnerDetails.create({partner_name: partner.partner_name});
        }else
          res.send();
      });
    }
  });
};

exports.getPartnersByDeployment=function(req,res){
  Partner.find({deployment_id:req.params.id},{partner_name:1},function(err,docs){
    if(docs){
     var list=[];
     _.forEach(docs,function(d) {
       list.push(d.partner_name);
     });
      console.log(list);
      PartnerDetails.find({partner_name:{$in:list}},function(err,docs){
        if(docs) return res.status(200).send(docs);
        else
        return res.status(500).send({status:'error'});
      })
   }
   else
    return res.status(500).send({status:'error'});
  })
}
