'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VendorPaymentHistorySchema = new Schema({
  paymentId:String,
  mainTranasctionId:String,
  transactionId:String,
  paidAmount:Number,
  user:{},
  tenant_id: Schema.Types.ObjectId,  
  deployment_id:String,
  created: { type: Date, default: Date.now},
  updated: { type: Date, default: Date.now},
  active: { type:Boolean , default:true }
},{versionKey:false});

VendorPaymentHistorySchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});

module.exports = mongoose.model('VendorPaymentHistory', VendorPaymentHistorySchema);