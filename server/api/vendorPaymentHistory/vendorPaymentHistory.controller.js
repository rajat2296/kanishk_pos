'use strict';

var _ = require('lodash');
var VendorPaymentHistory = require('./vendorPaymentHistory.model');

// Get list of vendorPaymentHistorys
exports.index = function(req, res) {
  VendorPaymentHistory.find(function (err, vendorPaymentHistorys) {
    if(err) { return handleError(res, err); }
    return res.json(200, vendorPaymentHistorys);
  });
};

// Get a single vendorPaymentHistory
exports.show = function(req, res) {
  VendorPaymentHistory.findById(req.params.id, function (err, vendorPaymentHistory) {
    if(err) { return handleError(res, err); }
    if(!vendorPaymentHistory) { return res.send(404); }
    return res.json(vendorPaymentHistory);
  });
};

// Creates a new vendorPaymentHistory in the DB.
exports.create = function(req, res) {
  VendorPaymentHistory.create(req.body, function(err, vendorPaymentHistory) {
    if(err) { return handleError(res, err); }
    return res.json(201, vendorPaymentHistory);
  });
};

// Updates an existing vendorPaymentHistory in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  VendorPaymentHistory.findById(req.params.id, function (err, vendorPaymentHistory) {
    if (err) { return handleError(res, err); }
    if(!vendorPaymentHistory) { return res.send(404); }
    var updated = _.merge(vendorPaymentHistory, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, vendorPaymentHistory);
    });
  });
};

//Get History by transactionId
exports.getTransactionHistoryByTransactionId=function(req,res){
  //console.log("Hello Ranjit");
  var data=[];
  //if(req.query.transactionId!=""){
    VendorPaymentHistory.aggregate([
          // {
          //   $match: {
          //         mainTranasctionId: req.query.mainTranasctionId
          //     }
          // },
          {
            $group :{
              _id:"$mainTranasctionId",
              //mainTranasctionId:"$mainTranasctionId",
              totalPaidAmount:{$sum:"$paidAmount"}
            }  
          }
      ],function (err, result) {
          if (err) {
              handleError(res, err);
              //next(err);
          } else {
              //console.log(result);
              //console.log(typeof result);
              return res.json(result);
          }
      });  
  //}
};

//Get by Main Id
exports.getByMainTransactionId=function(req,res){
  // VendorPaymentHistory.find(function (err, vendorPaymentHistorys) {
  //   if(err) { return handleError(res, err); }
  //   return res.json(200, vendorPaymentHistorys);
  // });
//console.log(req.query.mainTranasctionId);
  VendorPaymentHistory.find({'mainTranasctionId':req.query.mainTranasctionId}, function (err, vendorPaymentHistorys) {
    if(err) { 
      return handleError(res, err); 
    }
    if(!vendorPaymentHistorys) { return res.send(404); }
    return res.json(200, vendorPaymentHistorys);
  });
};

// Deletes a vendorPaymentHistory from the DB.
exports.destroy = function(req, res) {
  VendorPaymentHistory.findById(req.params.id, function (err, vendorPaymentHistory) {
    if(err) { return handleError(res, err); }
    if(!vendorPaymentHistory) { return res.send(404); }
    vendorPaymentHistory.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}