/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var VendorPaymentHistory = require('./vendorPaymentHistory.model');

exports.register = function(socket) {
  VendorPaymentHistory.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  VendorPaymentHistory.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('vendorPaymentHistory:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('vendorPaymentHistory:remove', doc);
}