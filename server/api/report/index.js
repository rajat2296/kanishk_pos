'use strict';

var express = require('express');
var controller = require('./report.controller');

var router = express.Router();

router.get('/', controller.reportType);
//router.post('/excelInvoiceDetails', controller.excelInvoiceDetails);
// router.post('/excelPaymentDetails', controller.excelPaymentDetails);
// //router.post('/excelTaxSummary', controller.excelTaxSummary);
// router.post('/excelItemCategory', controller.excelItemCategory);
// router.post('/excelTabBreakDown', controller.excelTabBreakDown);
// router.post('/excelUserBreakDown', controller.excelUserBreakDown);
// router.post('/excelWaiterBreakDown', controller.excelWaiterBreakDown);
// router.post('/excelKotDetail', controller.excelKotDetail);
// router.post('/excelKotDeleteHistory', controller.excelKotDeleteHistory);
// router.post('/excelAverageBill', controller.excelAverageBill);
router.post('/callDetails', controller.callDetailsReport);       // added by rajat
router.get('/callDetailsBills', controller.getBillsForCallDetails);  //added by rajat
router.get('/myOperator/getCompanyId', controller.getCompanyId);     //added by rajat
router.get('/:id', controller.reportType);
// router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
