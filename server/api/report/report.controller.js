'use strict';
var _ = require('lodash');
var Report = require('./report.model');
var async = require('async');
var path = require('path');
var moment = require('moment');
var request=require('request');
var Deployment=require('../deployment/deployment.model');
var Utils=require('../Utils/utils');
var mongoose=require('mongoose');
var bill=require('../bill/bill.model');
var fs = require('fs');
var email   = require("emailjs/email");
var Client = require('node-rest-client').Client;
var fs = require('fs');
var reportUtils = require('../reportFunction/reportFunc.js')
var options = {
  jszip: {
    compression: 'DEFLATE'
  }
};
var server  = email.server.connect({
  user:    "posist",
  password:"pos123ist",
  host:    "smtp.sendgrid.net",
  ssl:     true
});

var getDynamicPaymentDetails = function (bills) {
  //  console.log(bills);
  var html = reportNameG+"\n\r\n\r";
  var gtotal = {
    amount: 0,
    discount: 0,
    net: 0,
    roundOff: 0,
    afterRoundOff: 0,
    totalCash: 0,
    totalCredit: 0,
    totalDebit: 0,
    totalOther: 0,
    totalCoupon:0,
    totalOnline:0,
    covers:0

  }
  chargesName = _.uniq(chargesName);
  // console.log(chargesName);
  html += "";
  html += "";
  html += "Bill #,";
  html += "Open Time,";
  html += "Table #,";
  html += "Tab Type,";

  html += "Covers,";
  html += "Close Time,";
  html += "Total Amount,";
  if(!discountValue)
  html += "Discount,";
  _.forEach(allTaxesFor, function (tax) {
    html += "" + tax + ",";
  })
  _.forEach(chargesName,function(ch){
    html += "" + ch + ",";
  })
  html += "Net Amount,";
  html += "Round Off,";
  html += "G. Total,";
  html += "Cash,";
  html += "Credit Card,";
  html += "Debit Card,";
  html += "Other Card,";
  html += "Coupon,";
  html += "Online,";
  html += "\r\n";
  /* _.forEach(tabWiseBreakdown, function (bills, tab) {
   var tabTotal = {
   amount: 0,
   discount: 0,
   net: 0,
   roundOff: 0,
   afterRoundOff: 0,
   totalCash: 0,
   totalCredit: 0,
   totalDebit: 0,
   totalOther: 0
   }
   html += "";
   html += "" + tab + ",";
   html += "\r\n";*/
  var sortByTime = _.sortBy(bills, function (bill) {
    return bill._created;
  });
  var grpByCloseTime = _.groupBy(sortByTime, function (bill) {
    return bill.closeTime;
  })
  for (var day in grpByCloseTime) {
    html += "";
    html += " " + moment(day).format('DD-MMM-YY') + ",";
    html += "\r\n";
    //console.log(grpByCloseTime[day]);
    var orderByBillSerial = _.sortBy(grpByCloseTime[day], function (d) {
      return d.serialNumber;
    })
    var total = {
      amount: 0,
      discount: 0,
      net: 0,
      roundOff: 0,
      afterRoundOff: 0,
      totalCash: 0,
      totalCredit: 0,
      totalDebit: 0,
      totalOther: 0,
      totalOnline:0,
      totalCoupon:0,
      covers: 0

    }
    _.forEach(orderByBillSerial, function (bill) {

      html += "";
      if (bill.splitNumber != null)
        html += "" +(bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + "-" + bill.splitNumber + ",";
      else  html += "" + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + ",";
      html += "" + moment(bill._created).format("H:mm:ss a") + ",";
      if (bill._tableId != null)
        html += "" + bill._tableId + ",";
      else  html += ",";
      if (bill.tab != null)
        html += "" + bill.tab + ",";
      else  html += ",";
      if (bill._covers != null){
        html += "" + bill._covers + ",";
      }
      else  html += ",";
      html += "" + moment(bill._closeTime).format("H:mm:ss a") + ",";
      if(discountValue)
      html += "" + (Utils.roundNumber(bill.aggregate.amount, 2)  - Utils.roundNumber(bill.aggregate.discount,2))+ ",";
     else
     {  html += "" + Utils.roundNumber(bill.aggregate.amount, 2)  + ",";

      html += "" + Utils.roundNumber(bill.aggregate.discount, 2) + ",";
    }
      _.forEach(allTaxesFor, function (tax, id) {
        html += "" + Utils.roundNumber(reportUtils.getBillTaxTotal(id, bill.aggregate.taxes), 2) + ",";
      })
       _.forEach(chargesName, function (ch) {
        html += "" + Utils.roundNumber(reportUtils.getChargesTotal(ch, bill.aggregate.charges), 2) + ",";
      })

      html += "" + Utils.roundNumber(bill.aggregate.netAmount, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.roundOff, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.grandTotal, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.cash, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.credit, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.debit, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.other, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.coupon, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.online, 2) + ",";
      html += "\r\n";
      
      total.discount += parseFloat(Utils.roundNumber(bill.aggregate.discount, 2));
      total.net += parseFloat(Utils.roundNumber(bill.aggregate.netAmount, 2));
      total.roundOff += parseFloat(Utils.roundNumber(bill.aggregate.roundOff, 2));
      total.afterRoundOff += parseFloat(bill.aggregate.grandTotal);
      total.totalCash += parseFloat(bill.aggregate.cash);
      total.totalCredit += parseFloat(bill.aggregate.credit);
      total.totalDebit += parseFloat(bill.aggregate.debit);
      total.totalOther += parseFloat(bill.aggregate.other);
      total.totalCoupon += parseFloat(bill.aggregate.coupon);
      total.totalOnline += parseFloat(bill.aggregate.online);

      if(bill._covers && !bill.isVoid)
        total.covers += bill._covers;

      /* tabTotal.amount += parseFloat(Utils.roundNumber(bill.aggregate.amount, 2));
       tabTotal.discount += parseFloat(Utils.roundNumber(bill.aggregate.discount, 2));
       tabTotal.net += parseFloat(Utils.roundNumber(bill.aggregate.netAmount, 2));
       tabTotal.roundOff += parseFloat(Utils.roundNumber(bill.aggregate.roundOff, 2));
       tabTotal.afterRoundOff += parseFloat(bill.aggregate.grandTotal);
       tabTotal.totalCash += parseFloat(bill.aggregate.cash);
       tabTotal.totalCredit += parseFloat(bill.aggregate.credit);
       tabTotal.totalDebit += parseFloat(bill.aggregate.debit);
       tabTotal.totalOther += parseFloat(bill.aggregate.other);*/
       if(discountValue)
     {
      gtotal.amount += (parseFloat(Utils.roundNumber(bill.aggregate.amount, 2)) -  parseFloat(Utils.roundNumber(bill.aggregate.discount, 2)));
    total.amount += (parseFloat(Utils.roundNumber(bill.aggregate.amount, 2)) - parseFloat(Utils.roundNumber(bill.aggregate.discount, 2)));
    
    }else
       {
        gtotal.amount += parseFloat(Utils.roundNumber(bill.aggregate.amount, 2));
        total.amount += parseFloat(Utils.roundNumber(bill.aggregate.amount, 2));
      }

      gtotal.discount += parseFloat(Utils.roundNumber(bill.aggregate.discount, 2));
      gtotal.net += parseFloat(Utils.roundNumber(bill.aggregate.netAmount, 2));
      gtotal.roundOff += parseFloat(Utils.roundNumber(bill.aggregate.roundOff, 2));
      gtotal.afterRoundOff += parseFloat(bill.aggregate.grandTotal);
      gtotal.totalCash += parseFloat(bill.aggregate.cash);
      gtotal.totalCredit += parseFloat(bill.aggregate.credit);
      gtotal.totalDebit += parseFloat(bill.aggregate.debit);
      gtotal.totalOther += parseFloat(bill.aggregate.other);
      gtotal.totalCoupon += parseFloat(bill.aggregate.coupon);
      gtotal.totalOnline += parseFloat(bill.aggregate.online);
      if(bill._covers && !bill.isVoid)
        gtotal.covers += bill._covers;



    })
    html += "";
    html += "Day Total,";

    html += ",";

    html += ",";
    html += ",";
    html += "" + total.covers + ",";
    html += ",";
    if(discountValue)
    html += "" + Utils.roundNumber(total.amount, 2) + ",";
  else
   { html += "" + Utils.roundNumber(total.amount, 2) + ",";
    html += "" + Utils.roundNumber(total.discount, 2) + ",";
  }
    _.forEach(allTaxesFor, function (tax, id) {
      html += "" + Utils.roundNumber(reportUtils.getTotalTaxPaymentDetails(id, orderByBillSerial), 2) + ",";
    })
    _.forEach(chargesName, function (ch) {
      html += "" + Utils.roundNumber(reportUtils.getTotalChargesPaymentDetails(ch, orderByBillSerial), 2) + ",";
    })
    html += "" + Utils.roundNumber(total.net, 2) + ",";
    html += "" + Utils.roundNumber(total.roundOff, 2) + ",";
    html += "" + Utils.roundNumber(total.afterRoundOff, 2) + ",";
    html += "" + Utils.roundNumber(total.totalCash, 2) + ",";
    html += "" + Utils.roundNumber(total.totalCredit, 2) + ",";
    html += "" + Utils.roundNumber(total.totalDebit, 2) + ",";
    html += "" + Utils.roundNumber(total.totalOther, 2) + ",";
    html += "" + Utils.roundNumber(total.totalCoupon, 2) + ",";
    html += "" + Utils.roundNumber(total.totalOnline, 2) + ",";
    html += "\r\n";
  }
  /*   html += "";
   html += "Tab Total,";
   html += ",";
   html += ",";
   html += ",";
   html += "" + Utils.roundNumber(tabTotal.amount, 2) + ",";
   html += "" + Utils.roundNumber(tabTotal.discount, 2) + ",";
   _.forEach(allTaxesFor, function (tax, id) {
   html += "" + Utils.roundNumber(reportUtils.getTotalTaxPaymentDetails(id, bills), 2) + ",";
   })
   html += "" + Utils.roundNumber(tabTotal.net, 2) + ",";
   html += "" + Utils.roundNumber(tabTotal.roundOff, 2) + ",";
   html += "" + Utils.roundNumber(tabTotal.afterRoundOff, 2) + ",";
   html += "" + Utils.roundNumber(tabTotal.totalCash, 2) + ",";
   html += "" + Utils.roundNumber(tabTotal.totalCredit, 2) + ",";
   html += "" + Utils.roundNumber(tabTotal.totalDebit, 2) + ",";
   html += "" + Utils.roundNumber(tabTotal.totalOther, 2) + ",";
   html += "\r\n";
   });*/
  html += "";
  html += "Grand Total,,";
  html += ",";
  html += ",";
  html += "" + gtotal.covers + " ,";
  html += ",";
  if(discountValue)
  html += "" + Utils.roundNumber(gtotal.amount, 2) + ",";
  else
  {
    html += "" + Utils.roundNumber(gtotal.amount, 2) + ",";
    html += "" + Utils.roundNumber(gtotal.discount, 2) + ",";
  }
  _.forEach(allTaxesFor, function (tax, id) {
    html += "" + Utils.roundNumber(reportUtils.getTotalTaxPaymentDetails(id, bills), 2) + ",";
  })
   _.forEach(chargesName, function (ch) {
    html += "" + Utils.roundNumber(reportUtils.getTotalChargesPaymentDetails(ch, bills), 2) + ",";
  })
  html += "" + Utils.roundNumber(gtotal.net, 2) + ",";
  html += "" + Utils.roundNumber(gtotal.roundOff, 2) + ",";
  html += "" + Utils.roundNumber(gtotal.afterRoundOff, 2) + ",";
  html += "" + Utils.roundNumber(gtotal.totalCash, 2) + ",";
  html += "" + Utils.roundNumber(gtotal.totalCredit, 2) + ",";
  html += "" + Utils.roundNumber(gtotal.totalDebit, 2) + ",";
  html += "" + Utils.roundNumber(gtotal.totalOther, 2) + ",";
  html += "" + Utils.roundNumber(gtotal.totalCoupon, 2) + ",";
   html += "" + Utils.roundNumber(gtotal.totalOnline, 2) + ",";
  html += "\r\n";
  html += "";
  exportToCsv(html)
};
var dynamicGetTaxSummary = function () {
  var html = reportNameG+"\n\r\n\r";
  chargesName = _.uniq(chargesName)
  html += ""
  html += "";
  var lol = Object.keys(allTaxesFor).length + 18 + chargesName.length;
  // console.log('lol', lol);
  var colspan = 'colspan=' + '"' + lol + '"';
  html += "Tax Summary,";

  html += "\r\n";
  html += "";
  html += "Date,";
  html += "No of Bills,";
  html += "Amount,";
  if(!discountValue)
  html += "Discount,";
  _.forEach(allTaxesFor, function (tax, id) {
    html += "" + tax + ",";
  })
   _.forEach(chargesName, function (name) {
    html += "" + name + ",";
  })

  html += "Gross Amount,";
  html += "RoundOff,";
  html += "G. Total,";
  html += "Cash,";
  html += "Credit Card,";
  html += "Debit Card,";
  html += "Other,";
  html += "Coupon,";
  html += "Online,";
  html += "\r\n";
  var total = {
    bills: 0,
    amount: 0,
    discount: 0,
    net: 0,
    roundOff: 0,
    afterRoundOff: 0,
    totalCash: 0,
    totalDebit: 0,
    totalCredit: 0,
    totalOther: 0,
    totalCoupon:0,
    totalOnline:0
  };
  _.forEach(dateWiseTaxBreakdown, function (row, date) {
    html += "";
    html += "" + date + ",";
    html += "" + row.noOfBills + ",";
    if(discountValue)
      html += "" + (Utils.roundNumber(row.amount, 2) - Utils.roundNumber(row.discount, 2)) + ",";
    else
    {
      html += "" + Utils.roundNumber(row.amount, 2) + ",";
      html += "" + Utils.roundNumber(row.discount, 2) + ",";
    }
    _.forEach(allTaxesFor, function (tax, id) {
      html += "" + Utils.roundNumber(reportUtils.getTaxForTaxSummary(id, row.taxes), 2) + ",";
    });
    // console.log(row.charges);
    _.forEach(chargesName, function (name) {
      html += "" + Utils.roundNumber(reportUtils.getChargeForTaxSummary(name, row), 2) + ",";
    });
    html += "" + Utils.roundNumber(row.netAmount, 2) + ",";
    html += "" + Utils.roundNumber(row.roundOff, 2) + ",";
    html += "" + Utils.roundNumber(row.grandTotal, 2) + ",";
    html += "" + Utils.roundNumber(row.cash, 2) + ",";
    html += "" + Utils.roundNumber(row.credit, 2) + ",";
    html += "" + Utils.roundNumber(row.debit, 2) + ",";
    html += "" + Utils.roundNumber(row.other, 2) + ",";
    html += "" + Utils.roundNumber(row.coupon, 2) + ",";
    html += "" + Utils.roundNumber(row.online, 2) + ",";
    html += "\r\n";
    total.bills += parseFloat(row.noOfBills);
    total.amount += parseFloat(row.amount);
    total.discount += parseFloat(Utils.roundNumber(row.discount, 2));
    total.net += parseFloat(Utils.roundNumber(row.netAmount, 2));
    total.roundOff += parseFloat(row.roundOff);
    total.afterRoundOff += parseFloat(row.grandTotal);
    total.totalCash += parseFloat(row.cash);
    total.totalCredit += parseFloat(row.credit);
    total.totalDebit += parseFloat(row.debit);
    total.totalOther += parseFloat(row.other);
    total.totalCoupon += parseFloat(row.coupon);
    total.totalOnline += parseFloat(row.online);
    _.forEach(allTaxesFor, function (tax, id) {
      if(!total[id])
        total[id] = 0;
      var x = _.find(row.taxes, function (tx) {
        return tx.id == id;
      });
      if(x)
        total[id] += x.tax_amount;
    })
    
  })
  html += "";
  html += "Total,";
  html += "" + total.bills + ",";
  if(discountValue)
   html += "" + (Utils.roundNumber(total.amount, 2) - Utils.roundNumber(total.discount, 2))  + ",";  
  else
  {
    html += "" + Utils.roundNumber(total.amount, 2) + ",";
    html += "" + Utils.roundNumber(total.discount, 2) + ",";
  }
  _.forEach(allTaxesFor, function (tax, id) {
    html += "" + Utils.roundNumber(total[id], 2) + ",";
  });
  _.forEach(chargesName, function (name) {
    html += "" + Utils.roundNumber(reportUtils.getTotalChargesTaxSummary(name,dateWiseTaxBreakdown), 2) + ",";
  });
  html += "" + Utils.roundNumber(total.net, 2) + ",";
  html += "" + Utils.roundNumber(total.roundOff, 2) + ",";
  html += "" + Utils.roundNumber(total.afterRoundOff, 2) + ",";
  html += "" + Utils.roundNumber(total.totalCash, 2) + ",";
  html += "" + Utils.roundNumber(total.totalCredit, 2) + ",";
  html += "" + Utils.roundNumber(total.totalDebit, 2) + ",";
  html += "" + Utils.roundNumber(total.totalOther, 2) + ",";
  html += "" + Utils.roundNumber(total.totalCoupon, 2) + ",";
  html += "" + Utils.roundNumber(total.totalOnline, 2) + ","
  html += "\r\n";
  html + ""
  exportToCsv(html)
};
var dynamicGetInvoiceDetails = function (bills) {

  var html = reportNameG+"\n\r\n\r";
  /* Populate Global Taxes */
  /* var _globaTaxes = [];
   _.forEach(bills, function (bill) {
   _.forEach(bill._taxes, function (tax) {

   _globaTaxes.push({
   id: tax.id,
   name: tax.name2
   });

   });
   });
   var globalTaxes = _.uniq(_globaTaxes, 'id');
   report.allTaxesFor = globalTaxes;*/
  bills = _.sortBy(bills, function (bill) {
    return bill._created;
  });
  var grpByDay = _.groupBy(bills, function (bill) {
    return bill.closeTime;
  });
  var gt = {
    gtQty: 0,
    gtNet: 0,
    gtTotalAmt: 0,
    gtDiscount: 0
  };
  chargesName = _.uniq(chargesName);
  html += "";
  for (var day in grpByDay) {
    var _t = {
      totalQty: 0,
      totalNet: 0,
      totalAmount: 0,
      totalDiscount: 0
    };
    // console.log(grpByDay[day]);
    html += "";
    html += "\r\n";
    html += "";
    html += "" + moment(day).format('DD-MMM-YY') + "";

    html += ",";
    html += "\r\n";
    html += "";
    var grpBySerial = _.sortBy(grpByDay[day], function (d) {
      return d.serialNumber;
    })
    _.forEach(grpBySerial, function (bill) {

      if (bill.splitNumber != undefined) {
        html += "";
        html += "Bill# " + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + "-" + bill.splitNumber + ",~Day Serial# " + bill.daySerialNumber + ", ~Tab: " + bill.tab + " ~ , Open: " + moment(bill._created).format('MMMM Do YYYY, h:mm:ss a') + ",  Close: " + moment(bill._closeTime).format('MMMM Do YYYY, h:mm:ss a') + "";
        html += ",";
        html += "\r\n";
      } else {
        html += "";
        html += "Bill# " + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + ",~Day Serial# " + bill.daySerialNumber + ", ~Tab: " + bill.tab + " ~ , Open: " + moment(bill._created).format('MMMM Do YYYY, h:mm:ss a') + ",  Close: " + moment(bill._closeTime).format('MMMM Do YYYY, h:mm:ss a') + "";
        html += ",";
        html += "\r\n";
      }
      html += "";
      html += "Item Name,";
      html += "Rate,";
      html += "Qty,";
      html += "Amount,";
      // console.log(discountValue)
      if(!discountValue)
        html += "Discount,";

      // console.log(allTaxesFor);
      _.forEach(allTaxesFor, function (tax) {
        //console.log(tax.name);
        html += "" + tax + ","
      });
      _.forEach(chargesName, function (name) {
        //console.log(tax.name);
        html += "" + name + ","
      });

      html += "Net Amount,";
      html += "\r\n";
      bill.totalQty = 0;
      _.forEach(bill._items, function (items) {
        _.forEach(items,function(item){
        // console.log(item)
        bill.totalQty += item.qty;
        html += "";
        html += "" + item.name + ",";
        html += "" + item.rate + ",";
        html += "" + item.qty + ",";
        // console.log(discountValue);
        if(discountValue)
         html += "" + (parseFloat(item.amount) - parseFloat(item.discount)) + ","; 
        else {   
          html += "" + item.amount + ",";
          html += "" + item.discount + ",";
        }
        _.forEach(allTaxesFor, function (tax, id) {
          html += "" + Utils.roundNumber(reportUtils.getItemTaxInvoiceReport(id, item), 2) + ",";
        });
        _.forEach(chargesName, function (name) {
          html += "0,";
        });
        // console.log(item.taxAmount);
        html += "" + Utils.roundNumber(item.amount + item.taxAmount - item.discount, 2) + ",";
        html += "\r\n";
        });
      });
      html += "";
      html += "BILL TOTAL,";
      html+=","
      html += "" + bill.totalQty + ",";
      if(!discountValue)
     { html += "" + Utils.roundNumber(bill.aggregate.amount, 2) + ",";
      html += "" + Utils.roundNumber(bill.aggregate.discount, 2) + ",";
    }else html += "" + (Utils.roundNumber(bill.aggregate.amount, 2) - Utils.roundNumber(bill.aggregate.discount, 2)) + ","
      _.forEach(allTaxesFor, function (tax, id) {
        var tax = _.find(bill.aggregate.taxes, function (tax) {
          return tax.id == id;
        })
        if (tax) {
          html += "" + Utils.roundNumber(tax.tax_amount,2) + ",";
        } else {
          html += "" + 0 + ",";
        }
      });
       _.forEach(chargesName, function (charge) {
        var charge = _.find(bill.aggregate.charges, function (ch) {
          return ch.name == charge ;
        })
        if (charge) {
          html += "" + Utils.roundNumber(charge.amount,2) + ",";
        } else {
          html += "" + 0 + ",";
        }
      });
      html += "" + Utils.roundNumber(bill.aggregate.netAmount, 2) + ",";
      html += "\r\n\r\n";
      //if(bill.billDiscountAmount!=undefined || bill.billDiscountAmount!=0) {
      //  _t.totalDiscount += parseFloat(Utils.roundNumber(bill.billDiscountAmount, 2));
      //  gt.gtDiscount += parseFloat(Utils.roundNumber(bill.billDiscountAmount, 2));
      //  _t.totalNet-=parseFloat(bill.billDiscountAmount);
      //  gt.gtNet-=parseFloat(bill.billDiscountAmount);
      //}
      _t.totalQty += bill.totalQty;
      _t.totalAmount += parseFloat(bill.aggregate.amount);
      _t.totalDiscount += parseFloat(Utils.roundNumber(bill.aggregate.discount, 2));
      _t.totalNet += parseFloat(bill.aggregate.netAmount);
      gt.gtQty += bill.totalQty;
      gt.gtTotalAmt += parseFloat(bill.aggregate.amount);
      gt.gtDiscount += parseFloat(Utils.roundNumber(bill.aggregate.discount, 2));
      gt.gtNet += parseFloat(bill.aggregate.netAmount);
    })
    html += "";
    html += "Day TOTAL,";
    html+=","
    html += "" + _t.totalQty + ",";
    if(discountValue)
    html += "" +(Utils.roundNumber(_t.totalAmount, 2) - Utils.roundNumber(_t.totalDiscount, 2))  + ",";
    
    else{
       html += "" + Utils.roundNumber(_t.totalAmount, 2) + ",";
      html += "" + Utils.roundNumber(_t.totalDiscount, 2) + ",";
    }
    _.forEach(allTaxesFor, function (tax, id) {
      html += "" + Utils.roundNumber(reportUtils.getDayTaxInvoiceReport(id, grpByDay[day]), 2) + ",";
    });
    _.forEach(chargesName, function (name) {
      html += "" + Utils.roundNumber(reportUtils.getDayChargeInvoiceReport(name, grpByDay[day]), 2) + ",";
    });
    html += "" + Utils.roundNumber(_t.totalNet, 2) + ",";
    html += "\r\n\r\n";
  }
  html += "\r\n";
  html += "Grand TOTAL,";
  html +=",";
  html += "" + gt.gtQty + ",";
  if(discountValue)
  html += "" + (Utils.roundNumber(gt.gtTotalAmt, 2) -  Utils.roundNumber(gt.gtDiscount, 2))  + ",";
  else {
  html += "" + Utils.roundNumber(gt.gtTotalAmt, 2) + ",";
  html += "" + Utils.roundNumber(gt.gtDiscount, 2) + ",";
  }
  _.forEach(allTaxesFor, function (tax, id) {
    html += "" + Utils.roundNumber(reportUtils.getTotalTaxInvoiceReport(id, bills), 2) + ",";
  });
  _.forEach(chargesName, function (name) {
    html += "" + Utils.roundNumber(reportUtils.getTotalChargeInvoiceReport(name, bills), 2) + ",";
  });
  html += "" + Utils.roundNumber(gt.gtNet, 2) + ",";
  html += "\r\n\r\n"
  html += "";
  exportToCsv(html);
};
var getBilldetail = function(bills,type,breakDownBy) {
  // console.log('ASAS')
  var tTotalAmt = 0;
  var tDisAmt = 0;
  var tTaxAmt = 0;
  var tNetAmt = 0;
  var tRound = 0;
  var tGTotal = 0;
  var tcash=0;
  var tcredit=0;
  var tdebit=0;
  var tother=0;
  var tcovers=0;
  var tcoupon=0;
  var tonline = 0;
  var tchargesAmount = 0;
  var tDiscountAmount = 0;
  var dTotalAmt = 0;
  var dDisAmt = 0;
  var dTaxAmt = 0;
  var dNetAmt = 0;
  var dRound = 0;
  var dcash=0;
  var dcredit=0;
  var ddebit=0;
  var dother=0;
  var dcovers=0;
  var dcoupon=0;
  var donline=0;
  var dchargesAmount = 0;
  var dGTotal = 0;
  var dDiscountAmount=0;
  var gtTotalAmt = 0;
  var gtcovers=0;
  var gtcoupon=0;
  var gtDisAmt = 0;
  var gtTaxAmt = 0;
  var gtNetAmt = 0;
  var gtRound = 0;
  var gtGTotal = 0;
  var gtcash=0;
  var gtcredit=0;
  var gtdebit=0;
  var gtother=0;
  var gtonline=0;
  var gtchargesAmount = 0;
  var gNoOfBills=0;
  var gtDiscountAmount=0;

  var billR = bills.bills
  var items = bills.items
  var htmlr = 'DETAILED \n\r  ';
  var htmlCons = '\n\r CONSOLIDATED \n\r ';
  // console.log("ASASASASAS")
  _.forEach(billR,function(uData){

    uData._created=uData.created;
    // console.log("ASASASASAS")
    // console.log(uData)
    // console.log(resetDaily)
    var _cutOffTime = moment(moment(cutOffTimeSettings));
    var _cutOffTimeMeridian = moment(moment(cutOffTimeSettings)).format('a');
    var _cutOffTimeHour = parseInt(moment(moment(_cutOffTime)).format('H'));
    var _cutOffTimeMin = parseInt(moment(moment(_cutOffTime)).format('mm'));
    // console.log(uData.created);
    var _billOpenTime = moment(moment(uData.created));
    // console.log( _billOpenTime);
    var _billCloseTime = moment(moment(uData._closeTime));

    var _billOpenTimeStartOfDay = moment(moment(uData.created)).startOf('day');
    // console.log(_billOpenTimeStartOfDay);

    var _billOpenTimeEnfOfDay = moment(moment(uData.created)).endOf('day');
    //console.log(_billOpenTimeEnfOfDay);
    var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour,'hours').add(_cutOffTimeMin,'minutes');
    //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));
    //console.log(_billOpenTimeOffsetCutOff);
    //console.log(_billOpenTime.isAfter(_billOpenTimeOffsetCutOff));
    if (_cutOffTimeMeridian === 'am') {
      //console.log("inside ",_cutOffTimeMeridian);
      if (_billOpenTime>_billOpenTimeOffsetCutOff && _billOpenTime<_billOpenTimeEnfOfDay) {
        uData.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
      }

      if (_billOpenTime>_billOpenTimeStartOfDay && _billOpenTime<_billOpenTimeOffsetCutOff) {
        uData.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
      }
    }
    //*!* Changing for groupBy to work *!/*/
    uData.created = moment(uData.created).format('YYYY-MM-DD');
    //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

    ///!* Create Faux Date Hack *!/
    if (uData.fauxCloseTime != null) {
      //console.log('fsux time');
      uData.closeTime = uData.fauxCloseTime;
    } else {
      //console.log('no faux time');
      if (_cutOffTimeMeridian != 'am') {
        //console.log('meridian  sm');
        uData.closeTime = uData.created;
      }
    }

    /* /!* Replacing serialNumber with daySerialNumber *!/*/
    if (resetDaily==true) {
      uData.serialNumber =uData.daySerialNumber;
    }
    /*/!*fine dine fix*!/*/
    // console.log(uData.prefix);
    // console.log(uData.closeTime);
    if(uData.prefix!=null){
      uData.serialNumber=uData.prefix + uData.serialNumber;
    }
  });
  //console.log(discountValue)
  if(!discountValue)
    htmlr += "Bill Number,Open Time,Covers,TableNo,CloseTime,Total Amount,Discount,TaxAmount,Total Charges,NetAmount,RoundOff,GTotal,Cash,CreditCard,DebitCard,Other,Online,Coupon,\r\n";
  else
    htmlr += "Bill Number,Open Time,Covers,TableNo,CloseTime,Total Amount,TaxAmount,Total Charges,NetAmount,RoundOff,GTotal,Cash,CreditCard,DebitCard,Other,Online,Coupon,\r\n";
    
      //htmlCons += "Tab Name,No Of Bills,Total Amount,Discount,TaxAmount,NetAmount,RoundOff,GTotal,";
      var uniqType = [];
      // uniqType.push('--None--');
      // if(type=='tab') {
        if(type =='cashier')
          type ='user';
      var uType = _.uniq(billR, type);
      // console.log(uType);
      _.forEach(uType, function (tType) {
        if (type == 'user') uniqType.push(tType.user);
        if (type == 'tab') uniqType.push(tType.tab);
        if (type == 'waiter') uniqType.push(tType.waiter);
        if (type == 'delivery') uniqType.push(tType.delivery);
      })
      if(!discountValue)
      htmlCons += ""+(breakDownBy || '')+",No Of Bills,Total Amount,Discount,TaxAmount,Total Charge,NetAmount,RoundOff,GTotal,Cash,CreditCard,DebitCard,Others,Online,Coupon,\r\n  ";
      else
         htmlCons += ""+(breakDownBy || '')+",No Of Bills,Total Amount,TaxAmount,Total Charge,NetAmount,RoundOff,GTotal,Cash,CreditCard,DebitCard,Others,Online,Coupon,\r\n  ";
      

      ///////////////////////////////////////  For --None-- Type //////////////////////////////////

      ///////////////////////////////////////////////////////////End --None-- Type///////////////////////////////////////
      // console.log(uniqType);
      _.forEach(uniqType, function (renderType) {
        //    console.log(renderType);
        htmlr += "" + renderType + ",\r\n";
        //htmlCons += "" + renderType + ",\r\n";

        var uniqData = [];
        if (type == 'tab')
          uniqData = _.filter(billR, {'tab': renderType});
        if (type == 'user')
          uniqData = _.filter(billR, {'user': renderType});
        if (type == 'waiter')
          uniqData = _.filter(billR, {'waiter': renderType});
        if (type == 'delivery')
          uniqData = _.filter(billR, {'delivery': renderType});

        var noOfBills= uniqData.length;
        // var uDates = getUniqueDates(uniqData);
        var uDates = _.sortBy(uniqData,function(date){
          return  date.closeTime
        });
        uDates = _.groupBy(uDates,function(data){
          return data.closeTime
        });

        _.forEach(uDates, function (udata,date) {
          //var uniqDateTypeData = getFilterByDate(uniqData, udate);
          htmlr += '\r\n' + date + ',\r\n';

          // console.log(udata[date]);
          //console.log(udata);
          var uniqDateTypeData = _.sortBy(udata,function(uData){
            return uData.serialNumber;
          })
          _.forEach(uniqDateTypeData, function (uData) {
            var netAmt = Utils.roundNumber(uData.totalAmount - uData.discountAmount + Utils.roundNumber(uData.taxAmount, 2) + Utils.roundNumber(uData.chargesAmount,2), 2);
            var roundOff = Utils.roundNumber(reportUtils.getRoundOff(netAmt), 2);
            tTotalAmt += Utils.roundNumber(uData.totalAmount,2);
            tDisAmt += Utils.roundNumber(uData.discountAmount,2);
            tTaxAmt += Utils.roundNumber(uData.taxAmount, 2);
            tNetAmt += netAmt;
            tRound += Utils.roundNumber(roundOff,2);
            tcash+= uData.totalCash;
            tcredit+= uData.totalCredit;
            tdebit+= uData.totalDebit;
            tother+= uData.totalOther;
            tcoupon+=uData.totalCoupon;
            tonline+=uData.totalOnline;
            dTotalAmt += Utils.roundNumber(uData.totalAmount,2);;
            dDisAmt += Utils.roundNumber(uData.discountAmount,2);
            dTaxAmt += Utils.roundNumber(uData.taxAmount, 2);
            dNetAmt += netAmt;
            dRound += Utils.roundNumber(roundOff,2);
            dcash+= uData.totalCash;
            dcredit+= uData.totalCredit;
            ddebit+= uData.totalDebit;
            dother+= uData.totalOther;
            dcoupon+=uData.totalCoupon;
            donline+=uData.totalOnline;
            gtTotalAmt += Utils.roundNumber(uData.totalAmount,2);;
            gtDisAmt += Utils.roundNumber(uData.discountAmount,2);
            gtTaxAmt += Utils.roundNumber(uData.taxAmount, 2);
            gtNetAmt += netAmt;
            gtRound += Utils.roundNumber(roundOff,2);
            gtcash+= uData.totalCash;
            gtcredit+= uData.totalCredit;
            gtdebit+= uData.totalDebit;
            gtother+= uData.totalOther;
            gtonline+= uData.totalOnline;
            gtcoupon+=uData.totalCoupon;
            if(uData.chargesAmount == undefined)
              uData.chargesAmount = 0;
            dchargesAmount += Utils.roundNumber(uData.chargesAmount,2);
            tchargesAmount += Utils.roundNumber(uData.chargesAmount,2);
            gtchargesAmount += Utils.roundNumber(uData.chargesAmount,2);
            dDiscountAmount+=(Utils.roundNumber(uData.totalAmount,2) - Utils.roundNumber(uData.discountAmount,2));
            tDiscountAmount+=(Utils.roundNumber(uData.totalAmount,2) - Utils.roundNumber(uData.discountAmount,2));
            gtDiscountAmount+=(Utils.roundNumber(uData.totalAmount,2) - Utils.roundNumber(uData.discountAmount,2));


            if(uData._covers==null)
              uData._covers="";
            else{
              dcovers+=uData._covers;
              tcovers+=uData._covers;
              gtcovers+=uData._covers;
            }
            if(uData._tableId==null)
              uData._tableId="";
            tGTotal += Utils.roundNumber(netAmt);
            dGTotal += Utils.roundNumber(netAmt);
            gtGTotal += Utils.roundNumber(netAmt);

            if(discountValue)
            htmlr += '' +  uData.serialNumber + ','+ moment(uData._created).format("LT ") +','+uData._covers+','+uData._tableId+','+ moment(uData._closeTime).format("LT ") + ',' +( Utils.roundNumber(uData.totalAmount,2) - Utils.roundNumber(uData.discountAmount,2))+ ',' + Utils.roundNumber(uData.taxAmount, 2) + ',' + Utils.roundNumber(uData.chargesAmount, 2) + ',' + Utils.roundNumber(netAmt, 2) + ',' + roundOff + ',' + Math.round(netAmt) +  ','+Utils.roundNumber(uData.totalCash,2)+','+Utils.roundNumber(uData.totalCredit,2)+','+Utils.roundNumber(uData.totalDebit,2)+','+Utils.roundNumber(uData.totalOther,2)+','+Utils.roundNumber(uData.totalOnline,2)+','+Utils.roundNumber(uData.totalCoupon,2)+',\r\n';
          else
           htmlr += '' +  uData.serialNumber + ','+ moment(uData._created).format("LT ") +','+uData._covers+','+uData._tableId+','+ moment(uData._closeTime).format("LT ") + ',' + Utils.roundNumber(uData.totalAmount,2) + ',' + Utils.roundNumber(uData.discountAmount,2) + ',' + Utils.roundNumber(uData.taxAmount, 2) + ',' + Utils.roundNumber(uData.chargesAmount, 2) + ',' + Utils.roundNumber(netAmt, 2) + ',' + roundOff + ',' + Math.round(netAmt) +  ','+Utils.roundNumber(uData.totalCash,2)+','+Utils.roundNumber(uData.totalCredit,2)+','+Utils.roundNumber(uData.totalDebit,2)+','+Utils.roundNumber(uData.totalOther,2)+','+Utils.roundNumber(uData.totalOnline,2)+','+Utils.roundNumber(uData.totalCoupon,2)+',\r\n';
         
          });
          if(discountValue)
          htmlr += 'Date Total,,'+dcovers+',,,' + Utils.roundNumber(dDiscountAmount,2)  + ',' + dTaxAmt.toFixed(2) + ',' + Utils.roundNumber(dchargesAmount,2) + ',' + dNetAmt.toFixed(2) + ',' + dRound.toFixed(2) + ',' + dGTotal.toFixed(2) + ','+dcash.toFixed(2)+','+dcredit.toFixed(2)+','+ddebit.toFixed(2)+','+dother.toFixed(2)+','+donline.toFixed(2)+','+Utils.roundNumber(dcoupon,2)+',\r\n';
          else
            htmlr += 'Date Total,,'+dcovers+',,,' + Utils.roundNumber(dTotalAmt,2) + ',' + Utils.roundNumber(dDisAmt,2) + ',' + dTaxAmt.toFixed(2) + ',' + Utils.roundNumber(dchargesAmount,2) + ',' + dNetAmt.toFixed(2) + ',' + dRound.toFixed(2) + ',' + dGTotal.toFixed(2) + ','+dcash.toFixed(2)+','+dcredit.toFixed(2)+','+ddebit.toFixed(2)+','+dother.toFixed(2)+','+donline.toFixed(2)+','+Utils.roundNumber(dcoupon,2)+',\r\n';
          
          //htmlCons += '' + noOfBills + ',' + dTotalAmt + ',' + dDisAmt + ',' + dTaxAmt.toFixed(2) + ',' + dNetAmt.toFixed(2) + ',' + dRound.toFixed(2) + ',' + dGTotal.toFixed(2) + ',\r\n';
          //noOfBills=0;
          dTotalAmt = 0;
          dDisAmt = 0;
          dTaxAmt = 0;
          dNetAmt = 0;
          dRound = 0;
          dGTotal = 0;
          dcash=0;
          dcredit=0;
          ddebit=0;
          dother=0;
          dcovers=0;
          dcoupon=0;
          donline=0;
          dchargesAmount=0;
          dDiscountAmount=0;
        });

        gNoOfBills+=noOfBills;
        /////////////////////////////Date Wise end//////////////////////////
        if(discountValue) {
          htmlr += '' + renderType + ' Total,,'+ tcovers+',,,' +Utils.roundNumber(tDiscountAmount,2) + ',' + tTaxAmt.toFixed(2) +',' + tchargesAmount.toFixed(2) + ',' + tNetAmt.toFixed(2) + ',' + tRound.toFixed(2) + ',' + tGTotal.toFixed(2)+','+tcash.toFixed(2)+','+tcredit.toFixed(2)+','+tdebit.toFixed(2)+','+tother.toFixed(2)+','+tonline.toFixed(2)+','+Utils.roundNumber(tcoupon,2)+',\r\n';
          htmlCons += '' + renderType + ' Total,' + noOfBills + ',' + Utils.roundNumber(tDiscountAmount,2)+ ',' + tTaxAmt.toFixed(2) +',' + tchargesAmount.toFixed(2) + ',' + tNetAmt.toFixed(2) + ',' + tRound.toFixed(2) + ',' + tGTotal.toFixed(2) + ','+tcash.toFixed(2)+','+tcredit.toFixed(2)+','+tdebit.toFixed(2)+','+tother.toFixed(2)+','+tonline.toFixed(2)+','+Utils.roundNumber(tcoupon,2)+',\r\n';
        } else {
          htmlr += '' + renderType + ' Total,,'+ tcovers+',,,' + tTotalAmt.toFixed(2) + ',' + tDisAmt.toFixed(2) + ',' + tTaxAmt.toFixed(2) +',' + tchargesAmount.toFixed(2) + ',' + tNetAmt.toFixed(2) + ',' + tRound.toFixed(2) + ',' + tGTotal.toFixed(2)+','+tcash.toFixed(2)+','+tcredit.toFixed(2)+','+tdebit.toFixed(2)+','+tother.toFixed(2)+','+tonline.toFixed(2)+','+Utils.roundNumber(tcoupon,2)+',\r\n';
          htmlCons += '' + renderType + ' Total,' + noOfBills + ',' + tTotalAmt.toFixed(2) + ',' + tDisAmt.toFixed(2) + ',' + tTaxAmt.toFixed(2) +',' + tchargesAmount.toFixed(2) + ',' + tNetAmt.toFixed(2) + ',' + tRound.toFixed(2) + ',' + tGTotal.toFixed(2) + ','+tcash.toFixed(2)+','+tcredit.toFixed(2)+','+tdebit.toFixed(2)+','+tother.toFixed(2)+','+tonline.toFixed(2)+','+Utils.roundNumber(tcoupon,2)+',\r\n';
        }
        tTotalAmt = 0;
        tDisAmt = 0;
        tTaxAmt = 0;
        tNetAmt = 0;
        tRound = 0;
        tGTotal = 0;
        tcash=0;
        tcredit=0;
        tdebit=0;
        tother=0;
        tonline=0;
        tcoupon=0;
        tcovers=0;
        tchargesAmount=0;
        tDiscountAmount=0;
      })
      ///////////////////////////////Type wise end //////////////////////////////////
      if(discountValue) { 
        htmlr += 'Grand Total,,'+gtcovers+',,,' + Utils.roundNumber(gtDiscountAmount,2)  + ',' + gtTaxAmt.toFixed(2) +',' + gtchargesAmount.toFixed(2) + ',' + gtNetAmt.toFixed(2) + ',' + gtRound.toFixed(2) + ',' + gtGTotal.toFixed(2)+','+gtcash+','+gtcredit+','+gtdebit+','+gtother.toFixed(2)+','+gtonline.toFixed(2)+','+Utils.roundNumber(gtcoupon)+',\r\n';
        htmlCons += 'Grand Total,'+gNoOfBills+',' + Utils.roundNumber(gtDiscountAmount,2) + ',' + gtTaxAmt.toFixed(2) + ',' + gtchargesAmount.toFixed(2) + ',' + gtNetAmt.toFixed(2) + ',' + gtRound.toFixed(2) + ',' + Utils.roundNumber(gtGTotal,2)+','+Utils.roundNumber(gtcash,2)+','+Utils.roundNumber(gtcredit,2)+','+Utils.roundNumber(gtdebit,2)+','+Utils.roundNumber(gtother,2)+','+Utils.roundNumber(gtonline,2)+','+Utils.roundNumber(gtcoupon,2)+',\r\n';
      } else {
        htmlr += 'Grand Total,,'+gtcovers+',,,' + Utils.roundNumber(gtTotalAmt,2) + ',' + Utils.roundNumber(gtDisAmt,2) + ',' + gtTaxAmt.toFixed(2) +',' + gtchargesAmount.toFixed(2) + ',' + gtNetAmt.toFixed(2) + ',' + gtRound.toFixed(2) + ',' + gtGTotal.toFixed(2)+','+gtcash+','+gtcredit+','+gtdebit+','+gtother.toFixed(2)+','+gtonline.toFixed(2)+','+Utils.roundNumber(gtcoupon)+',\r\n';
        htmlCons += 'Grand Total,'+gNoOfBills+',' + Utils.roundNumber(gtTotalAmt,2) + ',' + Utils.roundNumber(gtDisAmt,2) + ',' + gtTaxAmt.toFixed(2) + ',' + gtchargesAmount.toFixed(2) + ',' + gtNetAmt.toFixed(2) + ',' + gtRound.toFixed(2) + ',' + Utils.roundNumber(gtGTotal,2)+','+Utils.roundNumber(gtcash,2)+','+Utils.roundNumber(gtcredit,2)+','+Utils.roundNumber(gtdebit,2)+','+Utils.roundNumber(gtother,2)+','+Utils.roundNumber(gtonline,2)+','+Utils.roundNumber(gtcoupon,2)+',\r\n';
      }

  var html = reportNameG+"\n\r\n\r"+ htmlr;
  html += "\n\r" + htmlCons;

  var uTypes = [];
  var tQty = 0;
  var tAmount = 0;
  var gQty = 0;
  var gAmount = 0;
  var htmlq = '\n\r ITEMWISE\n\r  ';
  ;
  htmlq += 'ItemName,Rate,Quantity,Amount,\r\n';
  var uTypeR = _.uniq(items, 'type');
  _.forEach(uTypeR, function (utype) {
    uTypes.push(utype.type);
  })
  _.forEach(uTypes, function (type) {
    htmlq += '' + type + ',\r\n';
    var _fItems = _.filter(items, {type: type});
    _.forEach(_fItems, function (item) {
      tQty += item.qty;
      tAmount += item.amount;
      gQty += item.qty;
      gAmount += item.amount;

      htmlq += '' + item.itemName + ',' + item.rate + ',' + item.qty + ',' + Utils.roundNumber(item.amount,2) + ',\r\n';
    })
    htmlq += '' + type + ' Wise Total ,,' + tQty + ',' + tAmount + ',\r\n';
    tQty = 0;
    tAmount = 0;
  })
  htmlq += 'Grand Total,,' + gQty + ',' + gAmount + ',\r\n';
  html += "\n\r" + htmlq
  exportToCsv(html)
};
var dynamicGetKotDetail = function (kots) {
  var html = reportNameG+"\n\r\n\r";
  // console.log(kots)
  // var rows = _.sortBy(kots, 'serialNumber');
  var grpByDate = _.groupBy(kots, function (row) {
    return row.closeDate;
  });
  var _gt = {
    totalSubtotal: 0,
    totalQuantity: 0
  }
  html += " ";
  html += "";
  html += "";
  html += "Item Name,";
  html += "Rate,"
  html += "Quantity,";
  html += "Amount,";
  html += "\r\n";
  html += "";
  for (var day in grpByDate) {
    html += "";
    html += "" + day + ",";
    html += "\r\n";

    var grpBySerial = _.groupBy(grpByDate[day], function (d) {
      return (d.prefix+d.serialNumber);
    });
    for (var serial in grpBySerial) {
      var _t = {
        totalSubtotal: 0,
        totalQuantity: 0
      }
      html += "";
      html += "Bill Serial#" + serial + ",";
      html += "\r\n";
      _.forEach(grpBySerial[serial], function (kot) {
        html += "";
        html += "";
        html += "KOT#" + kot.kotNumber + ",";
        html += "\r\n";
        html += "";
        _.forEach(kot.items, function (item) {
          html += "";
          html += "" + item.name + ",";
          html += "" + item.rate + ","
          html += "" + item.quantity + ","
          html += "" + Utils.roundNumber(item.subtotal, 2) + ",";
          html += "\r\n";
          _t.totalSubtotal += parseFloat(item.subtotal);
          _t.totalQuantity += parseFloat(item.quantity);
          _gt.totalSubtotal += parseFloat(item.subtotal);
          _gt.totalQuantity += parseFloat(item.quantity);

        })

      });
      html += ""
      html += "Bill Total,";
      html += ",";
      html += "" + _t.totalQuantity + ",";
      html += "" + _t.totalSubtotal + ",";
      html += "\r\n";
    }
  }
  html += "";
  html += "Grand Total,";
  html += ",";
  html += "" + _gt.totalQuantity + ",";
  html += "" + Utils.roundNumber(_gt.totalSubtotal, 2) + ",";
  html += "\r\n";
  html += "";
  exportToCsv(html);
};
var dynamicGetKotDelete = function (kots) {
  // console.log(kots)
  var html = reportNameG+"\n\r\n\r"
  var gt = {q:0,a:0}
  var grpByCloseTime = _.groupBy(kots,'closeTime')
  for(var closeTime in grpByCloseTime) {
    var dt = {q:0,a:0}
    html+=""+closeTime+"\r\n"
    var grpBySerial = _.groupBy(grpByCloseTime[closeTime],'serialNumber')
    html+='KOT#,Item,Order Time,Delete Time,Rate,Quantity,Total,Comment,Username,Deletion User,\r\n'
    for(var serial in grpBySerial) {
      var bt = {q:0,a:0}
      html+='Bill#: '+serial+', Bill Close Time: '+grpBySerial[serial][0]._time+',\r\n'
      _.forEach(grpBySerial[serial],function(kot) {
        // console.log(kot)
        html+=''+kot.kotNumber+','
        html+=''+kot.name+','
        html+='"'+moment(kot.openTime).format('MMMM Do YYYY, h:mm:ss a')+'",'
        html+='"'+moment(kot.deleteTime).format('MMMM Do YYYY, h:mm:ss a')+'",'
        html+=''+(Number(kot.rate).toFixed(2))+','
        html+=''+kot.quantity+','
        html+=''+kot.subtotal.toFixed(2)+','
        html+=''+(kot.comment || 'NA')+','
        html+=''+(kot.username || 'NA')+','
        html+=''+(kot.deleteUser || 'NA')+'\r\n'
        gt.q += kot.quantity
        dt.q += kot.quantity
        bt.q += kot.quantity
        gt.a += kot.subtotal
        dt.a += kot.subtotal
        bt.a += kot.subtotal
      })
      html+='BILL TOTAL,,,,,'+bt.q+','+bt.a+'\r\n'
    }
    html+='DAY TOTAL,,,,,'+dt.q+','+dt.a+'\r\n\r\n'
  }
  html+='GRAND TOTAL,,,,,'+gt.q+','+gt.a+'\r\n'
  exportToCsv(html)
};
var populateDynamicHTMLConsolidatedWise = function (itemWiseEnterprise) {
  // uniqDeps=[]
  var gt = {q: 0, a: 0};
  var html = "";
  html += "S.No,Item,";
  _.forEach(dates, function (date) {
    html += "" + moment(date).format('YYYY-MM-DD').toString() + ",,";
  })
  html += "Total,,\r\n";
  html += ",,";
  _.forEach(dates, function (date) {
    html += "Qty,Amt,";
  })
  html += "Qty,Amt,\r\n";
  var _sfI = _.uniq(itemWiseEnterprise, 'itemName');
  _sfI = _.sortBy(_sfI, ['itemName']);
  _.forEach(_sfI, function (depRecord, sn) {
    var totalItem = {qty: 0, amount: 0};
    html += "" + (sn + 1) + ", " + depRecord.itemName + ","
    _.forEach(dates, function (date) {
      var _fQtyAmt = _.filter(itemWiseEnterprise, {
        created: moment(date).format('YYYY-MM-DD'),
        itemName: depRecord.itemName
      });
      var catWise = {qty: 0, amt: 0};
      _.forEach(_fQtyAmt, function (catItem) {
        catWise.qty += catItem.qty;
        catWise.amt += Utils.roundNumber(catItem.amount, 2);
      })
      //if(moment(date)==depRecord.created){
      if (_fQtyAmt.length > 0) {
        html += "" + catWise.qty + "," + Utils.roundNumber(catWise.amt, 2) + ",";
        totalItem.qty += catWise.qty;
        totalItem.amount += Utils.roundNumber(catWise.amt, 2);
      } else {
        html += ",,";
        //  html+=""+totalItem.qty+","+totalItem.amount+",";
      }
    })
    html += "" + totalItem.qty + "," + Utils.roundNumber(totalItem.amount, 2) + ",\r\n";
  })
  //   }
  //=====================Total for Each Deployment Date Wise==========================
  html += "Grand Total:,,";
  var gTotalDep = {qty: 0, amt: 0};
  _.forEach(dates, function (date) {
    var _filterDepDate = _.filter(itemWiseEnterprise, {created: moment(date).format('YYYY-MM-DD')});
    var depTotal = {qty: 0, amt: 0}
    if (_filterDepDate.length > 0) {
      _.forEach(_filterDepDate, function (depdate) {
        depTotal.qty += depdate.qty;
        depTotal.amt += Utils.roundNumber(depdate.amount, 2);
        gTotalDep.qty += depdate.qty;
        gTotalDep.amt += Utils.roundNumber(depdate.amount, 2);
      })
    }
    html += "" + depTotal.qty + "," + Utils.roundNumber(depTotal.amt, 2) + ",";
  })
  html += "" + gTotalDep.qty + "," + Utils.roundNumber(gTotalDep.amt, 2) + ",\r\n";
  gt.q += gTotalDep.qty;
  gt.a += gTotalDep.amt;
  //=======================================================================================

  html += "";
  return html;
};
var populateDynamicHTMLCategoryWise = function (itemWiseEnterprise) {
  // uniqDeps=[]
  var gt = {q: 0, a: 0};
  var html = "";
  html += "S.No,Item,";
  _.forEach(dates, function (date) {
    html += "" + moment(date).format('YYYY-MM-DD').toString() + ",,";
  })
  html += "Total,,\r\n";
  html += ",,";
  _.forEach(dates, function (date) {
    html += "Qty,Amt,";
  })
  html += "Qty,Amt,\r\n";
  _.forEach(uniqCats, function (cat) {

    html += "" + cat + ",\r\n";
    var _fI = _.filter(itemWiseEnterprise, {category: cat})
    if (_fI.length > 0) {
      var _sfI = _.uniq(_fI, 'itemName');
      _sfI = _.sortBy(_sfI, ['itemName']);
      _.forEach(_sfI, function (depRecord, sn) {
        var totalItem = {qty: 0, amount: 0};
        html += "" + (sn + 1) + ", " + depRecord.itemName + ","
        _.forEach(dates, function (date) {
          var _fQtyAmt = _.filter(_fI, {created: moment(date).format('YYYY-MM-DD'), itemName: depRecord.itemName});
          var catWise = {qty: 0, amt: 0};
          _.forEach(_fQtyAmt, function (catItem) {
            catWise.qty += catItem.qty;
            catWise.amt += Utils.roundNumber(catItem.amount, 2);
          })
          //if(moment(date)==depRecord.created){
          if (_fQtyAmt.length > 0) {
            html += "" + catWise.qty + "," + Utils.roundNumber(catWise.amt, 2) + ",";
            totalItem.qty += catWise.qty;
            totalItem.amount += Utils.roundNumber(catWise.amt, 2);
          } else {
            html += ",,";
            //  html+=""+totalItem.qty+","+totalItem.amount+",";
          }
        })
        html += "" + totalItem.qty + "," + Utils.roundNumber(totalItem.amount, 2) + ",\r\n";
      })
    }
    //=====================Total for Each Deployment Date Wise==========================
    html += "Category Wise Total:,,";
    var gTotalDep = {qty: 0, amt: 0};
    _.forEach(dates, function (date) {
      var _filterDepDate = _.filter(itemWiseEnterprise, {
        category: cat,
        created: moment(date).format('YYYY-MM-DD')
      });
      var depTotal = {qty: 0, amt: 0}
      if (_filterDepDate.length > 0) {
        _.forEach(_filterDepDate, function (depdate) {
          depTotal.qty += depdate.qty;
          depTotal.amt += Utils.roundNumber(depdate.amount, 2);
          gTotalDep.qty += depdate.qty;
          gTotalDep.amt += Utils.roundNumber(depdate.amount, 2);
        })
      }
      html += "" + depTotal.qty + "," + Utils.roundNumber(depTotal.amt, 2) + ",";
    })
    html += "" + gTotalDep.qty + "," + Utils.roundNumber(gTotalDep.amt, 2) + ",\r\n";
    gt.q += gTotalDep.qty;
    gt.a += gTotalDep.amt;
    //=======================================================================================
  });
  html += "Grand Total:,,";
  var gTotal = {qty: 0, amt: 0};
  _.forEach(dates, function (date) {
    var _filterDepDate = _.filter(itemWiseEnterprise, {created: moment(date).format('YYYY-MM-DD')});
    var Total = {qty: 0, amt: 0}
    if (_filterDepDate.length > 0) {
      _.forEach(_filterDepDate, function (depdate) {
        Total.qty += depdate.qty;
        Total.amt += Utils.roundNumber(depdate.amount, 2);
        gTotal.qty += depdate.qty;
        gTotal.amt += Utils.roundNumber(depdate.amount, 2);
      })
    }
    html += "" + Total.qty + "," + Utils.roundNumber(Total.amt, 2) + ",";
  })
  html += "" + gTotal.qty + "," + Utils.roundNumber(gTotal.amt, 2) + ",\r\n";

  html += "";

  html += "";
  return html;
};
var populateDynamicHTMLDeploymentWise = function (itemWiseEnterprise) {
  // uniqDeps=[]
  var gt = {q: 0, a: 0};
  var html = "";
  html += "S.No,Item,";
  _.forEach(dates, function (date) {
    html += "" + moment(date).format('YYYY-MM-DD').toString() + ",,";
  })
  html += "Total,,\r\n";
  html += ",,";
  _.forEach(dates, function (date) {
    html += "Qty,Amt,";
  })
  html += "Qty,Amt,\r\n";


  _.forEach(uniqDeps, function (dep) {

    html += "" + dep.name + ",\r\n";
    var _fI = _.filter(itemWiseEnterprise, {deployment: new mongoose.Types.ObjectId(dep._id)})
    if (_fI.length > 0) {
      var _sfI = _.uniq(_fI, 'itemName');
      _sfI = _.sortBy(_sfI, ['itemName']);
      _.forEach(_sfI, function (depRecord, sn) {
        var totalItem = {qty: 0, amount: 0};
        html += "" + (sn + 1) + ", " + depRecord.itemName + ","
        _.forEach(dates, function (date) {
          var _fQtyAmt = _.filter(_fI, {created: moment(date).format('YYYY-MM-DD'), itemName: depRecord.itemName});
          //if(moment(date)==depRecord.created){
          if (_fQtyAmt.length > 0) {
            html += "" + _fQtyAmt[0].qty + "," + Utils.roundNumber(_fQtyAmt[0].amount, 2) + ",";
            totalItem.qty += _fQtyAmt[0].qty;
            totalItem.amount += Utils.roundNumber(_fQtyAmt[0].amount, 2);
          } else {
            html += ",,";
            //  html+=""+totalItem.qty+","+totalItem.amount+",";
          }
        })
        html += "" + totalItem.qty + "," + Utils.roundNumber(totalItem.amount, 2) + ",\r\n";
      })
    }
    //=====================Total for Each Deployment Date Wise==========================
    html += "Deployment Wise Total:,,";
    var gTotalDep = {qty: 0, amt: 0};
    _.forEach(dates, function (date) {
      var _filterDepDate = _.filter(itemWiseEnterprise, {
        deployment: new mongoose.Types.ObjectId(dep._id),
        created: moment(date).format('YYYY-MM-DD')
      });
      var depTotal = {qty: 0, amt: 0}
      if (_filterDepDate.length > 0) {
        _.forEach(_filterDepDate, function (depdate) {
          depTotal.qty += depdate.qty;
          depTotal.amt += Utils.roundNumber(depdate.amount, 2);
          gTotalDep.qty += depdate.qty;
          gTotalDep.amt += Utils.roundNumber(depdate.amount, 2);
        })
      }
      html += "" + depTotal.qty + "," + Utils.roundNumber(depTotal.amt, 2) + ",";
    })
    html += "" + gTotalDep.qty + "," + Utils.roundNumber(gTotalDep.amt, 2) + ",\r\n";
    gt.q += gTotalDep.qty;
    gt.a += Utils.roundNumber(gTotalDep.amt, 2);
    //=======================================================================================
  });
  html += "Grand Total:,,";
  var gTotal = {qty: 0, amt: 0};
  _.forEach(dates, function (date) {
    var _filterDepDate = _.filter(itemWiseEnterprise, {created: moment(date).format('YYYY-MM-DD')});
    var Total = {qty: 0, amt: 0}
    if (_filterDepDate.length > 0) {
      _.forEach(_filterDepDate, function (depdate) {
        Total.qty += depdate.qty;
        Total.amt += Utils.roundNumber(depdate.amount, 2);
        gTotal.qty += depdate.qty;
        gTotal.amt += Utils.roundNumber(depdate.amount, 2);
      })
    }
    html += "" + Total.qty + "," + Utils.roundNumber(Total.amt, 2) + ",";
  })
  html += "" + gTotal.qty + "," + Utils.roundNumber(gTotal.amt, 2) + ",\r\n";

  html += "";
  //  console.log(html);
  return html;
};
var populateDynamicHTMLComplementary = function() {
  var gt = { q: 0, a: 0 };
  var _dep = _.filter(deployments, { _id: $scope.report.filters.deployment_id });

  var grpByDate = _.groupBy($scope.bills, 'formattedDate');
  //console.log(grpByDate);

  var resetDaily = $scope.getSettingSelected('reset_serial_daily', _dep[0].settings);
  var html = "";
  html += "Bill Number,";
  html += "Item Name,";
  html += "Rate,";
  html += "Qty,";
  html += "Amount,";
  html += "Segment,";
  html += "Consumer,";
  html += "User Name,";
  html += "Discount User,";
  html += "Comment,";
  html += "Complimentary Head,\r\n";
  for (var day in grpByDate) {

    var flag = 0;
    var dt = { a: 0, q: 0 };
    var formattedBills = [];
    for (var i = 0; i < grpByDate[day].length; i++) {

      var userName;
      if (grpByDate[day][i]._currentUser.firstname) {
        if (grpByDate[day][i]._currentUser.lastname)
          userName = grpByDate[day][i]._currentUser.firstname + " " + grpByDate[day][i]._currentUser.lastname;
        else userName = grpByDate[day][i]._currentUser.firstname
      } else userName = "NA";
      //console.log('bills');
      //html += "" + Total.qty + "," + Total.amt + ",";
      var isBillComplementary = false;
      _.forEach(grpByDate[day][i]._offers, function(offer) {
        var discountUser = "";
        if (grpByDate[day][i]._offers[0].offer.complimentary) {
          isBillComplementary = true;
          discountUser = grpByDate[day][i]._offers[0].user ? grpByDate[day][i]._offers[0].user : grpByDate[day][i]._waiter ? grpByDate[day][i]._waiter.username : (grpByDate[day][i]._currentUser ? grpByDate[day][i]._currentUser.username : "NA");
          //discountUser = grpByDate[day][i]._waiter ? grpByDate[day][i]._waiter.username : (grpByDate[day][i]._currentUser ? grpByDate[day][i]._currentUser.username : "NA");
          flag = 1;
          for (var k = 0; k < grpByDate[day][i]._kots.length; k++) {

            if (!grpByDate[day][i]._kots[k].isVoid) {
              for (var l = 0; l < grpByDate[day][i]._kots[k].items.length; l++) {
                formattedBills.push({
                  serialNumber: grpByDate[day][i].serialNumber,
                  name: grpByDate[day][i]._kots[k].items[l].name,
                  rate: grpByDate[day][i]._kots[k].items[l].rate,
                  quantity: grpByDate[day][i]._kots[k].items[l].quantity,
                  subtotal: grpByDate[day][i]._kots[k].items[l].subtotal,
                  tabType: grpByDate[day][i].tabType,
                  customer: (grpByDate[day][i]._customer.firstname || 'NA'),
                  user: userName,
                  discountUser: discountUser,
                  comment: (grpByDate[day][i]._kots[k].items[l].comment || grpByDate[day][i].complimentaryComment || 'NA'),
                  complimentaryHead: (grpByDate[day][i].complimentaryHead || 'NA')
                });
              }
            }
          }
        }
      });
      //console.log('offers');
      if (!isBillComplementary) {
        for (var k = 0; k < grpByDate[day][i]._kots.length; k++) {
          if (!grpByDate[day][i]._kots[k].isVoid) {
            for (var l = 0; l < grpByDate[day][i]._kots[k].items.length; l++) {
              var discountUser = "";
              if (grpByDate[day][i]._kots[k].items[l].isDiscounted) {
                discountUser = grpByDate[day][i]._kots[k].waiter ? grpByDate[day][i]._kots[k].waiter.username : (grpByDate[day][i]._currentUser ? grpByDate[day][i]._currentUser.username : "NA");
                var discount = 0;
                var comment;
                for (var m = 0; m < grpByDate[day][i]._kots[k].items[l].discounts.length; m++) {
                  if(grpByDate[day][i]._kots[k].items[l].discounts[m].user){
                    discountUser = grpByDate[day][i]._kots[k].items[l].discounts[m].user
                  }
                  discount += grpByDate[day][i]._kots[k].items[l].discounts[m].discountAmount;
                  comment = (grpByDate[day][i]._kots[k].items[l].discounts[m].comment || grpByDate[day][i].complimentaryComment);
                }
                if (discount === grpByDate[day][i]._kots[k].items[l].subtotal) {
                  flag = 1;
                  formattedBills.push({
                    serialNumber: grpByDate[day][i].serialNumber,
                    name: grpByDate[day][i]._kots[k].items[l].name,
                    rate: grpByDate[day][i]._kots[k].items[l].rate,
                    quantity: grpByDate[day][i]._kots[k].items[l].quantity,
                    subtotal: grpByDate[day][i]._kots[k].items[l].subtotal,
                    tabType: grpByDate[day][i].tabType,
                    customer: (grpByDate[day][i]._customer.firstname || 'NA'),
                    user: userName,
                    discountUser: discountUser,
                    comment: (comment || 'NA'),
                    complimentaryHead: (grpByDate[day][i].complimentaryHead || 'NA')
                  });
                }

              }
            }
          }
        }
      }
    }

    if (flag > 0) {

      html += "";
      html += "" + day + ",";
      html += ",";
      html += "\r\n";
    }

    _.forEach(formattedBills, function(bill) {

      html += "" + bill.serialNumber + ",";
      html += "" + bill.name + ",";
      html += "" + bill.rate + ",";
      html += "" + bill.quantity + ",";
      gt.q += bill.quantity;
      dt.q += bill.quantity;
      html += "" + Utils.roundNumber(bill.subtotal, 2) + ",";
      gt.a += Utils.roundNumber(bill.subtotal, 2);
      dt.a += Utils.roundNumber(bill.subtotal, 2);
      html += "" + bill.tabType + ",";
      html += "" + bill.customer + ",";
      html += "" + bill.user + ",";
      html += "" + bill.discountUser + ",";
      html += "" + bill.comment + ",";
      html += "" + bill.complimentaryHead + ",";
      html += "\r\n";
    });

    if (flag > 0) {
      html += "Day Total:,";
      html += "" + dt.q + ",";
      html += "" + Utils.roundNumber(dt.a, 2) + ",";
      html += ",\r\n";
    }
  }
  //=====================Total for Each Deployment Date Wise==========================
  html += "Grand Total:,";
  html += "" + gt.q + ",";
  html += "" + Utils.roundNumber(gt.a, 2) + ",";
  html += ",\r\n";
  //=======================================================================================

  html += "";
  document.getElementById('iteReportECons').innerHTML = html;
  //   console.log(html);
  $scope.CSV = html;
  return html;
};
var populateDynamicHTMLComplementaryItemWise = function (bills) {
  // uniqDeps=[]
  // console.log('Populate')
  var gt = {q: 0, a: 0};
  var formattedBills = [];
  var html = reportNameG+"\n\r\n\r"
  html += "";
  html += "";
  html += "SN,";
  html += "Item Name,";
  html += "Rate,";
  html += "Qty,";
  html += "Amount,";
  html += "Segment,";
  html += "\r\n"
  for (var i = 0; i < bills.length; i++) {
    //console.log('bills');
    //html += "" + Total.qty + "," + Total.amt + ",";
    if (bills[i]._offers.length > 0) {
      //console.log('offers');
      if (bills[i]._offers[0].offer.complimentary) {
        for (var k = 0; k < bills[i]._kots.length; k++) {
          if (!bills[i]._kots[k].isVoid) {
            for (var l = 0; l < bills[i]._kots[k].items.length; l++) {
              formattedBills.push({
                name: bills[i]._kots[k].items[l].name,
                rate: bills[i]._kots[k].items[l].rate,
                quantity: bills[i]._kots[k].items[l].quantity,
                amount: bills[i]._kots[k].items[l].subtotal,
                segment: bills[i].tabType
              });
            }
          }
        }
      }
      else {
        for (var k = 0; k < bills[i]._kots.length; k++) {
          if (!bills[i]._kots[k].isVoid) {
            for (var l = 0; l < bills[i]._kots[k].items.length; l++) {
              if (bills[i]._kots[k].items[l].isDiscounted) {
                var discount = 0;
                var comment;
                for (var m = 0; m < bills[i]._kots[k].items[l].discounts.length; m++) {
                  discount += bills[i]._kots[k].items[l].discounts[m].discountAmount;
                  comment = bills[i]._kots[k].items[l].discounts[m].comment;
                }
                if (discount === bills[i]._kots[k].items[l].subtotal) {
                  formattedBills.push({
                    name: bills[i]._kots[k].items[l].name,
                    rate: bills[i]._kots[k].items[l].rate,
                    quantity: bills[i]._kots[k].items[l].quantity,
                    amount: bills[i]._kots[k].items[l].subtotal,
                    segment: bills[i].tabType
                  });
                }

              }
            }
          }
        }
      }
    } else {
      for (var k = 0; k < bills[i]._kots.length; k++) {
        if (!bills[i]._kots[k].isVoid) {
          for (var l = 0; l < bills[i]._kots[k].items.length; l++) {
            if (bills[i]._kots[k].items[l].isDiscounted) {
              var discount = 0;
              var comment;
              for (var m = 0; m < bills[i]._kots[k].items[l].discounts.length; m++) {
                discount += bills[i]._kots[k].items[l].discounts[m].discountAmount;
                comment = bills[i]._kots[k].items[l].discounts[m].comment;
              }
              if (discount === bills[i]._kots[k].items[l].subtotal) {
                formattedBills.push({
                  name: bills[i]._kots[k].items[l].name,
                  rate: bills[i]._kots[k].items[l].rate,
                  quantity: bills[i]._kots[k].items[l].quantity,
                  amount: bills[i]._kots[k].items[l].subtotal,
                  segment: bills[i].tabType
                });
              }

            }
          }
        }
      }
    }
  }

  //console.log(formattedBills);
  var grpByItem = _.groupBy(formattedBills, 'name');
  //console.log(grpByItem);

  var index = 0;
  for (var key in grpByItem) {
    var grpByTab = _.groupBy(grpByItem[key], 'segment');
    ///(grpByTab);
    for (var key2 in grpByTab) {
      html += ""
      html += "" + (++index) + ",";
      html += "" + grpByTab[key2][0].name + ",";
      html += "" + grpByTab[key2][0].rate + ",";
      var ct = {q: 0, a: 0};
      for (var i = 0; i < grpByTab[key2].length; i++) {

        ct.q += grpByTab[key2][i].quantity;
        ct.a += Utils.roundNumber(grpByTab[key2][i].amount, 2);
        gt.q += grpByTab[key2][i].quantity;
        gt.a += Utils.roundNumber(grpByTab[key2][i].amount, 2);
      }
      html += "" + ct.q + ",";
      html += "" + Utils.roundNumber(ct.a, 2) + ",";
      html += "" + grpByTab[key2][0].segment + ",";
      html += "\r\n";
    }
  }

  //=====================Total for Each Deployment Date Wise==========================
  html += "Grand Total:,";
  html += "" + gt.q + "," + Utils.roundNumber(gt.a, 2) + ",,\r\n";
  //=======================================================================================

  html += "";
  exportToCsv(html)
};
var populateDynamicHTMLComplimentaryHeadwise = function(bills){
  var gt = 0;
  _.forEach(bills, function (rep) {
    rep.formattedDate = reportUtils.getFormattedDateByCutoffTime(new Date(rep._created),cutOffTimeSettings);
    if (!rep.prefix) {
      rep.instance = {
        name: '',
        preFix: ''
      }
    } else {
      rep.instance = {
        name: '',
        preFix: rep.prefix
      }
    }
    if(rep.complimentaryHead == ""){
      rep.complimentaryHead = 'NA'
    }
  });
  var html = reportNameG+"\n\r\n\r"+"";
      html += "Bill Number,";
      html += "Amount,";
      html += "Segment,";
      html += "Consumer,";
      html += "Username,";
      html += "Comment,\r\n";
  var grpByComplimentaryHead = _.groupBy(bills, 'complimentaryHead');
  for(var head in grpByComplimentaryHead){
      var ht = 0;
      html += "";
      html += "" + head + ",";
      html += "\r\n";
      var grpByDate = _.groupBy(grpByComplimentaryHead[head],'formattedDate')
      for(var day in grpByDate) {
        var dt = 0;
        html += "";
        html += "" + day + ",";
        html += ",";
        html += "\r\n";
        grpByDate[day] = _.sortBy(grpByDate[day], 'serialNumber', false);
        grpByDate[day].forEach(function(bill){
          if(bill._customer == undefined){
            bill['_customer'] = {firstname: 'NA'}
          }
          var userName = bill._offers[0] ? bill._offers[0].user : bill._waiter ? bill._waiter.username : (bill._currentUser ? bill._currentUser.username : "NA");
          html += "" + (bill.instance.preFix + " " + bill.serialNumber) + ",";
          html += ""+bill.aggregation.totalAmount.toFixed(0)+",";
          dt += bill.aggregation.totalAmount;
          gt += bill.aggregation.totalAmount; 
          ht += bill.aggregation.totalAmount;
          html += ""+bill.tabType+",";
          html += ""+bill._customer.firstname+",";
          html += ""+userName+",";
          html += ""+(bill.complimentaryComment || 'NA')+",\r\n";
        })
        html += "Day Total:,";
        html += "" + dt.toFixed(0) + ",";
        html += ",\r\n";
      }
      html += "Head Total:,";
      html += "" + ht.toFixed(0) + ",";
      html += ",\r\n";
  }
  html += "Grand Total:,";
  html += "" + gt.toFixed(0) + ",";
  html += ",\r\n";  
  html += "";
  exportToCsv(html)
};
function renderDynamicOnlinePaymentDetailsReport(data) {
  var html = reportNameG+"\n\r\n\r";
  var groupBillsByPaymentMode = _.groupBy(data, function (bill) {
    return bill.otherName;
  });
  _.forEach(groupBillsByPaymentMode, function (bills, mode) {
    var gt = {
      codAmount: 0,
      onlinePaymentAmount: 0
    };
    html += "";
    html += "" + mode + "\r\n";
    html += "";
    html += "";
    html += "Bill #,";
    html += "COD,";
    html += "Online Payment,"
    html += "\r\n";
    html += ""
    var grpByDate = _.groupBy(bills, function (bill) {
      return bill.closeTime;
    });
    _.forEach(grpByDate, function (bills, date) {
      html += ""
      html += " " + date + ",";
      html += "\r\n";
      var tot = {
        codAmount: 0,
        onlinePaymentAmount: 0
      }
      _.forEach(bills, function (bill) {
        html += "";
        html += "" + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + ",";

        html += "" + Utils.roundNumber(bill.codPayment, 2) + ",";
        html += "" + Utils.roundNumber(bill.onlinePayment, 2) + ",";

        html += "\r\n";
        tot.codAmount += bill.codPayment;
        tot.onlinePaymentAmount += bill.onlinePayment;
        gt.codAmount += bill.codPayment;
        gt.onlinePaymentAmount += bill.onlinePayment;
      });
      html += ""
      html += "Day Total,";
      html += "" + Utils.roundNumber(tot.codAmount, 2) + ",";
      html += "" + Utils.roundNumber(tot.onlinePaymentAmount, 2) + ",";
      html += "\r\n";
    });
    html += "";
    html += "Grand Total,";
    html += "" + Utils.roundNumber(gt.codAmount, 2) + ",";
    html += "" + Utils.roundNumber(gt.onlinePaymentAmount, 2) + ",";
    html += "\r\n\r\n";
    html += "";
  });
  exportToCsv(html)
};
function renderDynamicOnlinePaymentConsolidatedReport(data) {
  var html = reportNameG+"\n\r\n\r";
  html += "";
  html += "#,";
  html += "Source,";
  html += "Type,";
  html += "Amount,";
  html += "Number of Bills,\r\n";
  for (var i = 0; i < data.length; i++) {
    //html += "" + Total.qty + "," + Total.amt + ",";
    html += "" + (i + 1) + ",";
    html += "" + data[i].otherName + ",";
    html += "" + data[i].otherType + ",";

    html += "" + Utils.roundNumber(data[i].amount, 2) + ",";

    html += "" + data[i].numBills + ",";
    html += "\r\n";
  }


  //   }
  //=====================Total for Each Deployment Date Wise==========================
  /*html += "Grand Total:,";
   html += "" + gt.q + "," + gt.a + ",,\r\n";*/
  //=======================================================================================

  html += "";
  exportToCsv(html)
};
var populateDynamicHTMLInstanceWiseReports = function (bills) {
  var numTaxes = allTaxesFor.length;
  chargesName = _.uniq(chargesName);
  _.forEach(bills, function (rep) {
    rep.formattedDate = reportUtils.getFormattedDateByCutoffTime(new Date(rep._created),cutOffTimeSettings);
    if (!rep.hasOwnProperty('instance')) {
      rep.instance = {
        name: "None",
        preFix: ""
      }
    } else if (!rep.instance.hasOwnProperty('name')) {
      instance.name = "none",
        instance.preFix = ""
    }
    rep['instanceName'] = rep.instance.name;
    // console.log(rep.instanceName)
  });
  var reps = _.groupBy(bills, 'formattedDate');
  reps = reportUtils.getObjectKeysSorted(reps);
  // console.log(reps)
  if(discountValue)
    var gt = {t: 0};
  else
    var gt = {t: 0, d: 0};
  var html = reportNameG+"\n\r\n\r"+"";
  html += "INV NO.,";
  html += "Open Time,";
  html += "Total Amt.,";
  if(!discountValue)
      html += "Discount,";
  _.forEach(allTaxesFor, function (tax) {
    html += "" + tax + ",";
  })
  _.forEach(chargesName,function(charge){
    html += "" + charge + ",";
  })
  html += "Net Amt.,";
  html += "Round Off.,";
  html += "G.Total,\r\n";

  for (var rep in reps) {
    if(discountValue)
      var dt = {t: 0};
    else
      var dt = {t: 0, d: 0};
    // console.log(rep)
    html += ""
    html += ","
    html += "" + rep + ",";
    html += "\r\n";
    var groupsByInstance = _.groupBy(reps[rep], 'instanceName');
    //console.log(groupsByInstance);
    for (var rep in groupsByInstance) {
      html += "";
      html += "Instance:,";
      html += "" + rep + ",";
      html += ",";
      html += "\r\n";
      if(discountValue)
        var it = {t: 0};
      else
        var it = {t: 0, d: 0};
      for (var r in groupsByInstance[rep]) {
        html += "";
        // console.log(groupsByInstance[rep][r].aggregate)
        html += "" + groupsByInstance[rep][r].serialNumber + ",";
        html += "" + moment(groupsByInstance[rep][r]._created).format('LT') + ",";
        if(discountValue) {
          html += "" + (Utils.roundNumber(groupsByInstance[rep][r].aggregate.amount,2) - Utils.roundNumber(groupsByInstance[rep][r].aggregate.discount,2)) + ",";
          if (!isNaN(groupsByInstance[rep][r].totalAmount)) {
            gt.t += groupsByInstance[rep][r].aggregate.amount - groupsByInstance[rep][r].aggregate.discount ;
            it.t += groupsByInstance[rep][r].aggregate.amount - groupsByInstance[rep][r].aggregate.discount;
            dt.t += groupsByInstance[rep][r].aggregate.amount - groupsByInstance[rep][r].aggregate.discount;
          }
        } else {
          html += "" + Utils.roundNumber(groupsByInstance[rep][r].aggregate.amount, 2) + ",";
          if (!isNaN(groupsByInstance[rep][r].aggregate.amount)) {
            gt.t += groupsByInstance[rep][r].aggregate.amount;
            it.t += groupsByInstance[rep][r].aggregate.amount;
            dt.t += groupsByInstance[rep][r].aggregate.amount;
          }
          var discount = Number(groupsByInstance[rep][r].aggregate.discount);
          html += "" + Utils.roundNumber(discount, 2) + ",";
          if (!isNaN(discount)) {
            gt.d += Number(discount);
            it.d += Number(discount);
            dt.d += Number(discount);
          }
        }

        _.forEach(allTaxesFor, function (tax) {
          var t = _.find(groupsByInstance[rep][r].aggregate.taxes, function (bt) {
            return bt.name === tax;
          });
          if (t) {
            if (gt[t.name])
              gt[t.name] += Number(t.tax_amount);
            else {
              gt[t.name] = 0;
              gt[t.name] += t.tax_amount;
            }
            if (it[t.name])
              it[t.name] += t.tax_amount;
            else {
              it[t.name] = 0;
              it[t.name] += t.tax_amount;
            }
            if (dt[t.name])
              dt[t.name] += t.tax_amount;
            else {
              dt[t.name] = 0;
              dt[t.name] += t.tax_amount;
            }
            html += "" + Utils.roundNumber(t.tax_amount, 2) + ",";
          } else {

            html += "" + 0 + ",";
            if (gt[tax])
              gt[tax] += 0;
            else {
              gt[tax] = 0;
              gt[tax] += 0;
            }
            if (it[tax])
              it[tax] += 0;
            else {
              it[tax] = 0;
              it[tax] += 0;
            }
            if (dt[tax])
              dt[tax] += 0;
            else {
              dt[tax] = 0;
              dt[tax] += 0;
            }
          }
        });
        _.forEach(chargesName, function (name) {
          // console.log(groupsByInstance[rep][r].charges);
          var t = _.find(groupsByInstance[rep][r].charges.detail, function (bt) {
            return bt.name === name;
          });
          if (t) {
            if (gt[t.name])
              gt[t.name] += Number(t.amount);
            else {
              gt[t.name] = 0;
              gt[t.name] += t.amount;
            }
            if (it[t.name])
              it[t.name] += t.amount;
            else {
              it[t.name] = 0;
              it[t.name] += t.amount;
            }
            if (dt[t.name])
              dt[t.name] += t.amount;
            else {
              dt[t.name] = 0;
              dt[t.name] += t.amount;
            }
            html += "" + Utils.roundNumber(t.amount, 2) + ",";
          } else {

            html += "" + 0 + ",";
            if (gt[name])
              gt[name] += 0;
            else {
              gt[name] = 0;
              gt[name] += 0;
            }
            if (it[name])
              it[name] += 0;
            else {
              it[name] = 0;
              it[name] += 0;
            }
            if (dt[name])
              dt[name] += 0;
            else {
              dt[name] = 0;
              dt[name] += 0;
            }
          }
        });
        html += "" + Utils.roundNumber(groupsByInstance[rep][r].aggregate.netAmount, 2) + ",";
        if (!isNaN(groupsByInstance[rep][r].aggregate.netAmount)) {
          if (gt.n)
            gt.n += groupsByInstance[rep][r].aggregate.netAmount;
          else {
            gt.n = 0;
            gt.n += groupsByInstance[rep][r].aggregate.netAmount;
          }
          if (it.n)
            it.n += groupsByInstance[rep][r].aggregate.netAmount;
          else {
            it.n = 0;
            it.n += groupsByInstance[rep][r].aggregate.netAmount;
          }
          if (dt.n)
            dt.n += groupsByInstance[rep][r].aggregate.netAmount;
          else {
            dt.n = 0;
            dt.n += groupsByInstance[rep][r].aggregate.netAmount;
          }
        }
        html += "" + Utils.roundNumber(reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount), 2) + ",";
        if (!isNaN(reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount))) {
          if (gt.ro)
            gt.ro += reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          else {
            gt.ro = 0;
            gt.ro += reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          }
          if (it.ro)
            it.ro += reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          else {
            it.ro = 0;
            it.ro += reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          }
          if (dt.ro)
            dt.ro += reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          else {
            dt.ro = 0;
            dt.ro += reportUtils.getRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          }
        }
        html += "" + Utils.roundNumber(reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount), 0) + ",";
        if (!isNaN(reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount))) {
          if (gt.c)
            gt.c += reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          else {
            gt.c = 0;
            gt.c += reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          }
          if (it.c)
            it.c += reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          else {
            it.c = 0;
            it.c += reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          }
          if (dt.c)
            dt.c += reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          else {
            dt.c = 0;
            dt.c += reportUtils.getAfterRoundOff(groupsByInstance[rep][r].aggregate.netAmount);
          }
        }
        html += "\r\n";
        }
      html += "";
      html += "Instance Total,,";
      for (var i in it) {
        html += "" + Utils.roundNumber(it[i], 2) + ",";
      }
      html += "\r\n";
     }
    /*html+="";
     html+=",\r\n";
     html+="";
     html+=","
     html+="\r\n";
     html+="";
     html+="\r\n"*/
    html += "";
    html += "Day Total,,";
    for (var i in dt) {
      html += "" + Utils.roundNumber(dt[i], 2) + ",";
    }
    html += "\r\n";
  };
  html += "\r\n";
  html += "";
  html += "Grand Total,,";
  //console.log('gt', gt);
  for (var i in gt) {
    html += "" + Utils.roundNumber(gt[i], 2) + ",";
  }
  html += "\r\n";
  //console.log(Utils.roundNumber(2.3333333221, 2));
  //   }
  //=====================Total for Each Deployment Date Wise==========================
  /*html += "Grand Total:,";
   html += "" + gt.q + "," + gt.a + ",,\r\n";*/
  //=======================================================================================

  html += "";
  // console.log(bills)
  // console.log(allTaxesFor)
  // exportToCsv(html)
  exportToCsv(html)
};
var populateDynamicHTMLDiscountReports = function (bills) {
  _.forEach(bills, function (bill) {
    bill.itemAdd = [];
    bill.totalAmounts = 0;
    bill['formattedDate']=reportUtils.getFormattedDateByCutoffTime(new Date(bill._created),cutOffTimeSettings)
    _.forEach(bill._kots, function (kot) {
      if(!kot.isVoid){
        _.forEach(kot.items, function (item) {

          bill.itemAdd.push(item);
          bill.totalAmounts += parseFloat(item.subtotal);
          _.forEach(item.addOns, function (addOn) {
            if (_.has(addOn, "subtotal")) {
              bill.itemAdd.push(addOn);
              bill.totalAmounts += parseFloat(addOn.subtotal);
            }
          })
        })
      }
    })
  });

  var gt = {q: 0, a: 0};
  var html = reportNameG+"\n\r\n\r"+"";
  var reps = _.groupBy(bills, 'formattedDate');
  reps = reportUtils.getObjectKeysSorted(reps);
  // console.log(reps);
  html += "";
  html += "Bill Number,";
  html += "Total Amount,";
  html += "Discount,";
  html += "Customer,";
  html += "Offer Name,";
  html += "Waiter,";
  html += "Discount User,";
  html += "Comments,";
  html += "\r\n";

  for (var rep in reps) {
    html += "";
    html += "" + rep + ","
    html += ",";
    html += ",";
    html += ",";
    html += ",";
    html += ",";
    html += ",";
    html += "\r\n";

    for (var i = 0; i < reps[rep].length; i++) {
      // console.log(reps[rep]);
      // console.log(reps[rep].length);
      var offers = [];
      var comments = [];
      var discount = 0;

      var offerFlag = 0;
      var itemFlag = 0;
      var discountUser = "";
      //console.log(reps[rep][i]._offers);

      for (var j = 0; j < reps[rep][i]._offers.length; j++) {

        if (!reps[rep][i]._offers[j].offer.complimentary) {
          discountUser = reps[rep][i]._offers[j].user ? reps[rep][i]._offers[j].user : reps[rep][i]._waiter ? reps[rep][i]._waiter.username : (reps[rep][i]._currentUser ? reps[rep][i]._currentUser.username : "NA");
          discount += Number(reps[rep][i]._offers[j].offerAmount);
          offers.push(reps[rep][i]._offers[j].offer.name);
          offerFlag = 1;
        }
      }


      for (var j = 0; j < reps[rep][i]._kots.length; j++) {
        if (!reps[rep][i]._kots[j].isVoid) {
          for (var k = 0; k < reps[rep][i]._kots[j].items.length; k++) {
            var comm = [];
            var off = [];
            var disc = 0;
            if (reps[rep][i]._kots[j].items[k].isDiscounted) {
              discountUser = reps[rep][i]._kots[j].waiter ? reps[rep][i]._kots[j].waiter.username : (reps[rep][i]._currentUser ? reps[rep][i]._currentUser.username : "NA");
              for (var l = 0; l < reps[rep][i]._kots[j].items[k].discounts.length; l++) {
                disc = Number(reps[rep][i]._kots[j].items[k].discounts[l].discountAmount);
                off.push(reps[rep][i]._kots[j].items[k].discounts[l].offerName);
                comm.push(reps[rep][i]._kots[j].items[k].discounts[l].comment);
              }
              if (disc !== reps[rep][i]._kots[j].items[k].subtotal) {
                itemFlag = 1;
                offers = _.union(offers, off);
                comments = _.union(comments, comm);
                // console.log(reps[rep][i].daySerialNumber, 'offers', offers);
                // console.log(reps[rep][i].daySerialNumber, 'comments', comments);
                discount += disc;
              }
            }
          }
        }
      }
      if (itemFlag === 1 || offerFlag === 1) {
        html += "";
        html += "" + reps[rep][i].serialNumber + ",";
        html += "" + (Utils.roundNumber(reps[rep][i].totalAmounts,2) || 'NA') + ",";
        html += "" + discount + ",";

        if (reps[rep][i]._customer) {
          html += "" + reps[rep][i]._customer.firstname + ",";
        } else html += "NA,";

        html += "" + offers.join(' | ') + ",";

        if (reps[rep][i]._waiter) {
          html += "" + reps[rep][i]._waiter.firstname + ",";
        } else html += "NA,";
        html += "" + discountUser + ",";
        html += "" + comments.join(' | ') + ",";
        html += "\r\n";
      }
    }
  }

  html += "";
  
  
  exportToCsv(html)
};
var populateDynamicHTMLReprintReports = function (bills) {
  var gt = {q: 0, a: 0};
   _.forEach(bills, function (rep) {
      rep.formattedDate = reportUtils.getFormattedDateByCutoffTime(new Date(rep._created),cutOffTimeSettings);
      if (!rep.prefix) {

        rep.instance = {

          name: '',
          preFix: ''
        }
      } else {
        rep.instance = {
          name: '',
          preFix: rep.prefix
        }
      }
    });
  var html = reportNameG+"\n\r\n\r"+"";
  var reps = _.groupBy(bills, 'formattedDate');
  reps = reportUtils.getObjectKeysSorted(reps);

  for (var rep in reps) {
    html += "";
    html += "" + rep + ",";
    html += "\r\n";
    for (var i = 0; i < reps[rep].length; i++) {
      var userName;
      if (reps[rep][i]._currentUser == undefined) {
        reps[rep][i]._currentUser = {};
      }
      if (reps[rep][i]._currentUser.firstname) {
        if (reps[rep][i]._currentUser.lastname)
          userName = reps[rep][i]._currentUser.firstname + " " + reps[rep][i]._currentUser.lastname;
        else userName = reps[rep][i]._currentUser.firstname;
      } else userName = "NA";
      if(Array.isArray(reps[rep][i].rePrint)){
        if(reps[rep][i].rePrint.length > 1){
          //var number = resetDaily ? reps[rep][i].daySerialNumber : reps[rep][i].serialNumber;
          //html += "" + Total.qty + "," + Total.amt + ",";
          html += "";
          if (resetDaily) {
            html += "Bill Serial# ~ " + (reps[rep][i].instance.preFix + " " + reps[rep][i].daySerialNumber) + "";
          }
          else {
            html += "Bill Serial# ~ " + (reps[rep][i].instance.preFix + " " + reps[rep][i].serialNumber) + "";
          }
          html += "(Originally printed By ~ " + userName + ")"
          html += ",";
          html += "\r\n";

          html += "";
          html += "Print Time,";
          html += "User,";
          html += "\r\n";
          if (Array.isArray(reps[rep][i].rePrint)) {
            for (var j = 0; j < reps[rep][i].rePrint.length; j++) {
              if(j > 0){
                html += "";
                html += "" + (moment(reps[rep][i].rePrint[j].printTime).format('LLL') || 'NA') + ",";
                html += "" + (reps[rep][i].rePrint[j].user || 'NA') + ",";
                html += "\r\n";
              }
            }
          } else {
            html += "";
            html += "" + moment(reps[rep][i].rePrint).format('LLL') + ",";
            html += "" + "NA" + ",";
            html += "\r\n";
          }
        }
      }
    }
  }
  //   }
  //=====================Total for Each Deployment Date Wise==========================
  /*html += "Grand Total:,";
   html += "" + gt.q + "," + gt.a + ",,\r\n";*/
  //=======================================================================================

  html += "";
  exportToCsv(html)
};
var populateDynamicHTMLCategoryWiseReports = function (categoryReports,cats,txes,collectionMode) {
  // console.log(txes)
  // console.log(cats)
  var colspan = 0;
  for (var i = 0; i < dates.length + 1; i++) {
    colspan++;
  }

  var html = reportNameG+"\n\r\n\r"+"";
  html += "";

  //-------------------------------------Table Headers---------------------------------------------------
  html += "Category,";
  for (var i = 0; i < dates.length; i++) {

    html += "" + dates[i] + ",";
  }
  html += "Total,";
  html += "\r\n";

  var grpByDate = _.groupBy(categoryReports, 'closeTime');
  //console.log(grpByDate);

  //-------------------------------------Category Wise Reports-----------------------------------------------
  var dtCategory = {};
  var tTCategory = {a: 0};
  var reportsCategory = {};

  var dtComplementaryCategory = {};
  var tTComplementaryCategory = {a: 0};
  var reportsComplementaryCategory = {};

  var dtDiscount = {};
  var tTDiscount = {a: 0};

  var dtComplementaryDiscount = {};
  var tTComplementaryDiscount = {a: 0};

  var dtTax = {};
  var tTTax = {a: 0};
  var reportsTax = {};

  var dtCharge = {};
  var tTCharge = {a: 0};
  var reportsCharge = {};

  var dtAvgCover = {};
  var dtBillsWithCovers = {};
  var tTAvgCover = {avg: 0};
  var tTBillsWithCovers = {a: 0};
  var numCovers = {};

  var dtAvgBill = {};
  var tTAvgBill = {a: 0};
  var dtBills = {};
  var tTBills = {a: 0};

  var dtCollection = {};
  var tTCollection = {a: 0};
  var reportsCollection = {};

  var dTNetSales = {};
  var tTNetSales = {};

  var dtGrossSales = {};
  var tTGrossSales = {a: 0};

  _.forEach(dates, function (date) {
    if (!dtCategory[date]) {
      dtCategory[date] = {a: 0};
    }

    if (!dtComplementaryCategory[date]) {
      dtComplementaryCategory[date] = {a: 0};
    }

    if (!dtDiscount[date]) {
      dtDiscount[date] = {a: 0};
    }

    if (!dtComplementaryDiscount[date]) {
      dtComplementaryDiscount[date] = {a: 0};
    }

    if (!dtAvgCover[date]) {
      dtAvgCover[date] = {avg: 0}
    }

    if (!numCovers[date]) {
      numCovers[date] = {n: 0};
    }

    if (!dtBillsWithCovers[date]) {
      dtBillsWithCovers[date] = {a: 0};
    }

    if (!dtBills[date]) {
      dtBills[date] = {a: 0};
    }

    if (!dtAvgBill[date]) {
      dtAvgBill[date] = {a: 0};
    }

    if (!dtTax[date]) {
      dtTax[date] = {a: 0};
    }
    if (!dtCharge[date]) {
      dtCharge[date] = {a: 0};
    }

    if (!dtCollection[date]) {
      dtCollection[date] = {a: 0};
    }

    if (!dTNetSales[date]) {
      dTNetSales[date] = {a: 0};
    }

    if (!dtGrossSales[date]) {
      dtGrossSales[date] = {a: 0};
    }
  });

  _.forEach(cats, function (cat) {
    reportsCategory[cat] = {};
    reportsComplementaryCategory[cat] = {};
    _.forEach(dates, function (date) {
      reportsCategory[cat][date] = {
        date: date,
        amount: 0
      };

      reportsComplementaryCategory[cat][date] = {
        date: date,
        amount: 0
      };
    });
  });

  _.forEach(txes, function (tax) {
    reportsTax[tax] = {};
    _.forEach(dates, function (date) {
      reportsTax[tax][date] = {
        date: date,
        amount: 0
      };
    });
  });

  _.forEach(chargesName, function (charge) {
    reportsCharge[charge] = {};
    _.forEach(dates, function (date) {
      reportsCharge[charge][date] = {
        date: date,
        amount: 0
      };
    });
  });

  _.forEach(collectionMode, function (mode) {
    reportsCollection[mode] = {};
    _.forEach(dates, function (date) {
      reportsCollection[mode][date] = {
        date: date,
        amount: 0
      };
    });
  });

  for (var date in grpByDate) {
    _.forEach(grpByDate[date], function (bill) {
      if(!bill.isVoid){
        var compDiscount = 0;
        var totalDiscount = 0;
        if (Utils.roundNumber(bill.aggregate.amount) != Utils.roundNumber(bill.aggregate.discount) && bill.complimentary == false) {

          _.forEach(bill._offers, function (offer) {
            if(_.has(offer,'offer')){
            if (!offer.offer.complimentary)
              totalDiscount += Number(offer.offerAmount);
           }
          });
          //console.log(bill.serialNumber, bill._items);
          _.forEach(bill._kots, function (kot) {
            if (!kot.isVoid) {
              for (var it in kot.items) {
                if (_.has(kot.items[it], 'addOns')) {
                  _.forEach(kot.items[it].addOns, function (addon) {
                    //item.subtotal+=addon.subtotal;
                    //_.forEach(addon.taxes,function(at){
                    //  item.taxes.push(at);
                    //})
                    kot.items.push(addon);
                  })
                }
              }
              // console.log(angular.copy(kot.items));
              _.forEach(kot.items, function (item) {
                if(!item.category){
                  item.category = {categoryName: "NA", superCategory: {superCategoryName: "NA"}}
                } else {
                  if(!item.category.categoryName)
                    item.category.categoryName = "NA";
                  if(!item.category.superCategory){
                    item.category.superCategory = {superCategoryName: "NA"}
                  } else{
                    if(!item.category.superCategory.superCategoryName)
                      item.category.superCategory.superCategoryName = "NA";
                  }
                }
                item.discount = 0;
                _.forEach(item.discounts, function (discount) {
                  item.discount += discount.discountAmount;
                  //console.log('discountAmount', discount.discountAmount);
                });
                if (item.discount != item.subtotal) {
                  // console.log(item.category.superCategory.superCategoryName)
                  reportsCategory[item.category.superCategory.superCategoryName][date].amount += item.subtotal;
                  dtCategory[date].a += item.subtotal;
                  totalDiscount += item.discount;
                } else {
                  reportsComplementaryCategory[item.category.superCategory.superCategoryName][date].amount += item.subtotal;
                  dtComplementaryCategory[date].a += item.subtotal;
                  compDiscount += item.discount;
                }
              });
            }
          });

          dtDiscount[date].a += Number(totalDiscount);
        } else {
          _.forEach(bill._kots, function (kot) {
            if (!kot.isVoid) {
              _.forEach(kot.items, function (item) {
                if(!item.category){
                  item.category = {categoryName: "NA", superCategory: {superCategoryName: "NA"}}
                } else {
                  if(!item.category.categoryName)
                    item.category.categoryName = "NA";
                  if(!item.category.superCategory){
                    item.category.superCategory = {superCategoryName: "NA"}
                  } else{
                    if(!item.category.superCategory.superCategoryName)
                      item.category.superCategory.superCategoryName = "NA";
                  }
                }
                reportsComplementaryCategory[item.category.superCategory.superCategoryName][date].amount += item.subtotal;
                dtComplementaryCategory[date].a += item.subtotal;
              });
            }
          });
        }

        // console.log('complementary', date, bill.serialNumber, bill.totalAmount, dtComplementaryCategory[date].a);

        _.forEach(bill.aggregate.taxes, function (tax) {
          if (tax.isConsidered) {
            reportsTax[tax.name][date].amount += tax.tax_amount;
            dtTax[date].a += tax.tax_amount;
          }
        });

        _.forEach(bill.charges.detail, function (charge) {
          // console.log(charge.name);
          reportsCharge[charge.name][date].amount += Utils.roundNumber(charge.amount,2);
          dtCharge[date].a += charge.amount;
        });

        _.forEach(bill.payments.cash, function (cash) {
          //console.log(date, cash);
          reportsCollection["Cash"][date].amount += Number(cash);
        });

        _.forEach(bill.payments.cards, function (card) {
          if (card.cardType === "CreditCard") {
            //console.log(date, card.totalAmount);
            reportsCollection["Credit Card"][date].amount += Number(card.totalAmount);
          } else if (card.cardType === "DebitCard") {
            //console.log(date, card.totalAmount);
            reportsCollection["Debit Card"][date].amount += Number(card.totalAmount);
          }else  if (card.cardType === "Coupon") {
            //console.log(date, card.totalAmount);
            reportsCollection["Coupon"][date].amount += Number(card.totalAmount);
          }
        });

        _.forEach(bill.payments.others, function (card) {
          reportsCollection[card.mode][date].amount += Number(card.amount);
        });
         _.forEach(bill.payments.online, function (card) {
          reportsCollection[card.mode][date].amount += Number(card.amount);
        });

        /*_.forEach(bill._debitCards, function (card) {
         console.log(date, card.amount);
         reportsCollection["Debit Card"][date].amount += Number(card.amount);
         });

         _.forEach(bill._otherCards, function (card) {
         console.log(date, card.amount);
         reportsCollection[card.mode][date].amount += Number(card.amount);
         });*/

        if (bill._covers && bill.tabType == 'table') {
          numCovers[date].n += Number(bill._covers);
          dtBillsWithCovers[date].a += Number(bill.aggregate.netAmount);
          // console.log(date,'-->',bill.aggregate.netAmount,'-->',bill._closeTime)
          // console.log(bill.aggregate)
        }

        dtBills[date].a += 1;
      }
    });
  }

  //console.log(dtBillsWithCovers);

  for (var cat in reportsCategory) {
    var tt = {a: 0};
    html += "";
    html += "" + cat + ",";
    _.forEach(dates, function (date) {
      // console.log(reportsCategory[cat][date].amount,2);
      tt.a += Utils.roundNumber(reportsCategory[cat][date].amount,2);
      html += "" + Utils.roundNumber(reportsCategory[cat][date].amount,2) + ",";
    });
    html += "" + Utils.roundNumber(tt.a,2) + ",";
    html += "\r\n";
  }

  //---------------------------------------------Total Sales----------------------------------------------------

  html += "";
  html += "Total Sales,";
  for (var day in dtCategory) {
    html += "" + Utils.roundNumber(dtCategory[day].a,2) + ","
    tTCategory.a += Utils.roundNumber(dtCategory[day].a,2);
  }
  html += "" + Utils.roundNumber(tTCategory.a,2) + ","
  html += "\r\n";


  //--------------------------------------------Discounts-------------------------------------------------------

  html += "";
  html += "Discount Amount,";

  _.forEach(dates, function (date) {

    tTDiscount.a += Utils.roundNumber(dtDiscount[date].a,2);
    html += "" + Utils.roundNumber(dtDiscount[date].a, 2) + ",";
  });

  html += "" + Utils.roundNumber(tTDiscount.a, 2) + ","
  html += "\r\n";

  //--------------------------------------------Complementary Sales---------------------------------------------

  /*html += "";

   html += "Complementary Sales,";

   html += "\r\n";

   for (var cat in reportsComplementaryCategory) {
   var tt = {a: 0};
   html += "";
   html += "" + cat + ",";
   _.forEach(dates, function (date) {
   tt.a += reportsComplementaryCategory[cat][date].amount;
   html += "" + reportsComplementaryCategory[cat][date].amount + ",";
   });
   html += "" + tt.a + ",";
   html += "\r\n";
   }*/

  //--------------------------------------------Net Sales Total-------------------------------------------------
   html += "";

  html += "Net Sales Total,";
  for (var day in dates) {
    dTNetSales[dates[day]].a = Utils.roundNumber((dtCategory[dates[day]].a - dtDiscount[dates[day]].a), 2);
    html += "" + Utils.roundNumber((dtCategory[dates[day]].a - dtDiscount[dates[day]].a), 2) + ",";
  }
  html += "" + Utils.roundNumber((tTCategory.a - tTDiscount.a),2) + ","
  tTNetSales.a = Utils.roundNumber((tTCategory.a - tTDiscount.a), 2);

  html += "\r\n";

  //--------------------------------------------Tax Break Up & Other Charges------------------------------------

  html += "\r\n";

  html += "Tax Breakup,";

  html += "\r\n";

  for (var tax in reportsTax) {
    html += "";
    html += "" + tax + ",";
    var tT = {a: 0, r: 0};
    _.forEach(dates, function (date) {

      html += "" + Utils.roundNumber(reportsTax[tax][date].amount, 2) + ",";
      tT.a += Number(Utils.roundNumber(reportsTax[tax][date].amount, 2));
    });

    //console.log(dtTax)

    html += "" + Utils.roundNumber(tT.a, 2) + ",";
    html += "\r\n";
  }
  html += "";
  html += "Tax Breakup Total,";
  //console.log(dtTax);
  for (var day in dtTax) {
    html += "" + Utils.roundNumber(dtTax[day].a, 2) + ",";
    tTTax.a += Utils.roundNumber(parseFloat(dtTax[day].a),2);
  }
  html += "" + Utils.roundNumber(tTTax.a, 2) + ","
  html += "\r\n";

  //--------------------------------------------Charges-------------------------------------------------------

  html += "\r\n";

  html += "Charges,";

  html += "\r\n";

  for (var tax in reportsCharge) {
    html += "";
    html += "" + tax + ",";
    var tTChargesAmt = {a: 0, r: 0};
    _.forEach(dates, function (date) {

      html += "" + Utils.roundNumber(reportsCharge[tax][date].amount, 2) + ",";
      tTChargesAmt.a += Number(Utils.roundNumber(reportsCharge[tax][date].amount, 2));
    });

    //console.log(dtTax)

    html += "" + Utils.roundNumber(tTChargesAmt.a, 2) + ",";
    html += "\r\n";
  }
  html += "";
  html += "Charges Breakup Total,";
  //console.log(dtTax);
  for (var day in dtCharge) {
    // console.log(dtCharge[day].a);
    // console.log(tTCharge.a);
    html += "" + Utils.roundNumber(dtCharge[day].a, 2) + ",";
    tTCharge.a += Utils.roundNumber(parseFloat(dtCharge[day].a),2);
  }
  html += "" + Utils.roundNumber(tTCharge.a, 2) + ","
  html += "\r\n";      

  //--------------------------------------------Net Total ----------------------------------------------------

  html += "";

  html += "\r\nNet Total,";
  for (var day in dates) {
    html += "" + Utils.roundNumber((dTNetSales[dates[day]].a + dtTax[dates[day]].a + dtCharge[dates[day]].a), 2) + ","
  }
  html += "" + Utils.roundNumber((tTNetSales.a + tTTax.a + tTCharge.a), 2) + ","

  html += "\r\n";

  //--------------------------------------------Round Off------------------------------------------------------

  html += "";

  html += "Round Off,";
  for (var day in dates) {
    var net = Utils.roundNumber((dtCategory[dates[day]].a - dtDiscount[dates[day]].a + dtTax[dates[day]].a+ dtCharge[dates[day]].a), 2);
    var grand = Utils.roundNumber((dtCategory[dates[day]].a - dtDiscount[dates[day]].a + dtTax[dates[day]].a+ dtCharge[dates[day]].a));
    html += "" + Utils.roundNumber((grand - net), 2) + ","
  }
  var netT = Utils.roundNumber((tTCategory.a - tTDiscount.a + tTTax.a+ tTCharge.a), 2);
  var grandT = Utils.roundNumber((tTCategory.a - tTDiscount.a + tTTax.a+ tTCharge.a));
  html += "" + Utils.roundNumber((grandT - netT), 2) + ","

  html += "\r\n";

  //--------------------------------------------Gross Sale ----------------------------------------------------

  html += "";

  html += "Gross Sales,";
  _.forEach(dates, function (date) {
    if (!dtGrossSales[date]) {
      dtGrossSales[date] = {a: 0};
    }
    dtGrossSales[date].a = Utils.roundNumber((dtCategory[date].a - dtDiscount[date].a + dtTax[date].a+ dtCharge[date].a));
    html += "" + Utils.roundNumber((dtCategory[date].a - dtDiscount[date].a + dtTax[date].a+ dtCharge[date].a)) + ","
  });
  //console.log(dtGrossSales);
  html += "" + Utils.roundNumber((tTCategory.a - tTDiscount.a + tTTax.a+ tTCharge.a)) + ","
  tTGrossSales.a = Utils.roundNumber((tTCategory.a - tTDiscount.a + tTTax.a + tTCharge.a));

  html += "\r\n";

  //-------------------------------------------Collection Breakup----------------------------------------------


  html += "";

  html += "\r\nCollection Breakup,";

  html += "\r\n";

  _.forEach(collectionMode, function (mode) {

    html += "";
    html += "" + mode + ",";
    var cT = {a: 0};
    _.forEach(dates, function (date) {
      cT.a += reportsCollection[mode][date].amount;
      dtCollection[date].a += reportsCollection[mode][date].amount;
      html += "" + Utils.roundNumber(reportsCollection[mode][date].amount, 2) + ",";
    });
    html += "" + Utils.roundNumber(cT.a, 2) + ",";
    html += "\r\n";
  });

  html += "";
  html += "Gross Sales,";
  _.forEach(dates, function (date) {

    tTCollection.a += Utils.roundNumber(dtCollection[date].a,2);
    html += "" + Utils.roundNumber(dtCollection[date].a, 2) + ",";
  });

  html += "" + Utils.roundNumber(tTCollection.a, 2) + ",";

  html += "\r\n";
  //--------------------------------------------Average Per Cover----------------------------------------------

  html += "\r\n";

  html += "Covers,";

  var tTNumCovers = 0;
  _.forEach(dates, function (date) {
    tTNumCovers += numCovers[date].n;
    if (numCovers[date].n !== 0)
      dtAvgCover[date].avg = Utils.roundNumber((dtBillsWithCovers[date].a / numCovers[date].n),2);
    else dtAvgCover[date].avg = 0;
    html += "" + numCovers[date].n + ",";
  });
  html += "" + tTNumCovers + ",";
  html += "\r\n";

  html += "";
  html += "" + 'Average per Cover' + ","
  _.forEach(dates, function (date) {
    tTAvgCover.avg += Utils.roundNumber(dtAvgCover[date].avg,2);
    html += "" + Utils.roundNumber(dtAvgCover[date].avg,2) + ","
  });
  html += "" + Utils.roundNumber(tTAvgCover.avg,2) + ","
  html += "\r\n";

  //--------------------------------------------Average per bill-----------------------------------------------

  html += "";

  html += "No of Bills,";

  _.forEach(dates, function (date) {

    //console.log(dtGrossSales);
    //console.log(dtAvgBill[date].a);
    //console.log(dtGrossSales[date].a);
    //console.log(dtBills[date].a);
    if (dtBills[date].a !== 0) {
      dtAvgBill[date].a = Utils.roundNumber((dtGrossSales[date].a / dtBills[date].a),2);
    }
    tTBills.a += dtBills[date].a;
    html += "" + dtBills[date].a + ",";
  });

  html += "" + tTBills.a + ",";
  if (tTBills.a !== 0) {
    tTAvgBill.a = Utils.roundNumber((tTGrossSales.a / tTBills.a),2);
  }
  html += "\r\n";

  html += "";
  html += "Average Per Bill,";
  _.forEach(dates, function (date) {

    html += "" + Utils.roundNumber(dtAvgBill[date].a,2) + ",";
  });

  html += "" + Utils.roundNumber(tTAvgBill.a,2) + ","

  html += "\r\n";
  //--------------------------------------------GS AVG---------------------------------------------------------

  html += "";

  html += "Gross Sales Avg,";
  html += "" + (Utils.roundNumber(tTGrossSales.a / (colspan - 1), 2)) + ","
  html += "\r\n";

  //--------------------------------------------End of Report--------------------------------------------------
  html += "";
  exportToCsv(html)
};
var renderCompareWaiter = function(data){
  // 
  //       
  //       
  //         <label></label>,
  //         Waiter,
  //         Total Bills,
  //          Bills Served,
  //         Tables Void,
  //         Total Kots,
  //         Kots Void,
  //         Items Cancelled,
  //          Items Cancelled,
  //         Bills Discount Given,
  //         Covers,
  //         APC,
  //         APT,
  //         Total Revenue,
  //         Turn Around Time,
  //       \r\n
  //       
  //       
  //       
  //         <label></label>,
  //         {{report.waiterName}},
  //         {{utils.roundNumber(report.numBills)}},
  //         {{utils.roundNumber(report.numBillsServedPercent, 2)}},
  //         {{utils.roundNumber(report.tablesVoid)}},
  //         {{utils.roundNumber(report.numKots)}},
  //         {{utils.roundNumber(report.kotsVoid)}},
  //         {{utils.roundNumber(report.itemsCancelled)}},
  //         {{utils.roundNumber(report.percentItemsCancelled, 2)}},
  //         {{utils.roundNumber(report.discount.discountedBillsPercent, 2)}},
  //         {{utils.roundNumber(report.numPeople)}},
  //         {{utils.roundNumber(report.avgPerCover, 2)}},
  //         {{utils.roundNumber(report.avgPerTable, 2)}},
  //         {{utils.roundNumber(report.totalRevenue)}},
  //         {{report.avgTableTurAroundTimeHrs}},
  //       \r\n
  //       
  //       
  //         <label></label>,,
  //         {{report.waiterName}},
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //         -,
  //       \r\n
  //       
  //         ,
  //         Restaurant Average,
  //         {{utils.roundNumber(total.numBills)}},
  //         {{utils.roundNumber(total.numBillsServedPercent, 2)}},
  //         {{utils.roundNumber(total.tablesVoid)}},
  //         {{utils.roundNumber(total.numKots)}},
  //         {{utils.roundNumber(total.kotsVoid)}},
  //         {{utils.roundNumber(total.itemsCancelled)}},
  //         {{utils.roundNumber(total.percentBillsItemsCancelled, 2)}},
  //         {{utils.roundNumber(total.discount.discountedBillsPercent, 2)}},
  //         {{utils.roundNumber(total.numPeople)}},
  //         {{utils.roundNumber(total.avgPerCover, 2)}},
  //         {{utils.roundNumber(total.avgPerTable, 2)}},
  //         {{utils.roundNumber(total.totalRevenue)}},
  //         {{total.avgTableTurAroundTimeHrs}},
};
var getDynamicHourlySales = function (bills) {
  // console.log("Inside")
  var grpByHour = _.groupBy(bills, function (bill) {
    bill.hour = new Date(bill._created).getHours();
    return bill.hour;
  });
  var aggregatedBills = {};

  for (var i = 0; i < 24; i++) {
    aggregatedBills[i] = {
      totalBills: 0,
      totalAmount: 0,
      totalCovers: 0
    };
    if (i == 0)
      aggregatedBills[i].openTime = "00:00 AM - 01:00 AM";
    else if (i == 1)
      aggregatedBills[i].openTime = "01:00 AM - 02:00 AM";
    else if (i == 2)
      aggregatedBills[i].openTime = "02:00 AM - 03:00 AM";
    else if (i == 3)
      aggregatedBills[i].openTime = "03:00 AM - 04:00 AM";
    else if (i == 4)
      aggregatedBills[i].openTime = "04:00 AM - 05:00 AM";
    else if (i == 5)
      aggregatedBills[i].openTime = "05:00 AM - 06:00 AM";
    else if (i == 6)
      aggregatedBills[i].openTime = "06:00 AM - 07:00 AM";
    else if (i == 7)
      aggregatedBills[i].openTime = "07:00 AM - 08:00 AM";
    else if (i == 8)
      aggregatedBills[i].openTime = "08:00 AM - 09:00 AM";
    else if (i == 9)
      aggregatedBills[i].openTime = "09:00 AM - 10:00 AM";
    else if (i == 10)
      aggregatedBills[i].openTime = "10:00 AM - 11:00 AM";
    else if (i == 11)
      aggregatedBills[i].openTime = "11:00 AM - 12:00 PM";
    else if (i == 12)
      aggregatedBills[i].openTime = "12:00 PM - 01:00 PM";
    else if (i == 13)
      aggregatedBills[i].openTime = "01:00 PM - 02:00 PM";
    else if (i == 14)
      aggregatedBills[i].openTime = "02:00 PM - 03:00 PM";
    else if (i == 15)
      aggregatedBills[i].openTime = "03:00 PM - 04:00 PM";
    else if (i == 16)
      aggregatedBills[i].openTime = "04:00 PM - 05:00 PM";
    else if (i == 17)
      aggregatedBills[i].openTime = "05:00 PM - 06:00 PM";
    else if (i == 18)
      aggregatedBills[i].openTime = "06:00 PM - 07:00 PM";
    else if (i == 19)
      aggregatedBills[i].openTime = "07:00 PM - 08:00 PM";
    else if (i == 20)
      aggregatedBills[i].openTime = "08:00 PM - 09:00 PM";
    else if (i == 21)
      aggregatedBills[i].openTime = "09:00 PM - 10:00 PM";
    else if (i == 22)
      aggregatedBills[i].openTime = "10:00 PM - 11:00 PM";
    else if (i == 23)
      aggregatedBills[i].openTime = "11:00 PM - 00:00 AM";
  }

  var total = {
    totalBills: 0,
    totalAmount: 0,
    totalCovers: 0
  };
  for (var hour in grpByHour) {
    _.forEach(grpByHour[hour], function (bill) {

      if (!bill.isVoid) {
        aggregatedBills[hour].totalBills += 1;
        aggregatedBills[hour].totalAmount += Utils.roundNumber(bill.totalNet);
        aggregatedBills[hour].totalCovers += bill._covers || 0;
        total.totalBills += 1;
        total.totalAmount += Utils.roundNumber(bill.totalNet);
        total.totalCovers += bill._covers || 0;
      }
    });
  }
  var html = reportNameG+"\n\r\n\r";
  html += ""
  html += "";
  html += ""
  html += "Open Time,";
  html += "Number of Tickets,";
  html += "Covers,";
  html += "Amount,";
  html += "\r\n";
  html += "";
  _.forEach(aggregatedBills, function (bill) {
    html += "";
    html += "" + bill.openTime + ",";
    html += "" + bill.totalBills + ",";
    html += "" + bill.totalCovers + ",";
    html += "" + bill.totalAmount + ",";
    html += "\r\n";
  })
  html += ""
  html += "Total,";
  html += "" + total.totalBills + ",";
  html += "" + total.totalCovers + ",";
  html += "" + total.totalAmount + ",";
  html += "\r\n";
  html += " ";
  exportToCsv(html)
};
var dynamicGetBillConsolidate = function(bills){
  _.forEach(bills, function (bill) {
    bill.roundOff = reportUtils.getRoundOffs(bill.totalNet);
    bill.afterRoundOff = reportUtils.getAfterRoundOff(bill.totalNet);

  });
  var bills = _.sortBy(bills, 'serialNumber');
  var grpByCloseTime = _.sortBy(bills, function (bill) {
    return bill.closeTime;
  });
  var grpByCloseTime = _.groupBy(grpByCloseTime, function (bill) {
    return bill.closeTime;
  });

 /* dayTotal = {};
  grandTotal = {
    totalNet: 0,
    roundOff: 0,
    afterRoundOff: 0,
    covers: 0
  };*/

  for (var day in grpByCloseTime) {
    _.forEach(grpByCloseTime[day], function (bill) {
      if (!bill.isVoid) {
        if (bill._customer) {
          if (bill._customer.address1 || bill._customer.address2 || bill._customer.city || bill._customer.state || bill._customer.postCode) {
            bill._customer.fullAddress = (bill._customer.address1 ? (bill._customer.address1 + ", ") : "") + (bill._customer.address2 ? (bill._customer.address2 + ", ") : "") + (bill._customer.city ? (bill._customer.city + ", ") : "") + (bill._customer.state ? (bill._customer.state + ", ") : "") + (bill._customer.postCode ? (bill._customer.postCode + ", ") : "");
          } else {
            bill._customer.fullAddress = "NA";
          }
        } else {
          bill._customer = {};
        }
      }
    });
  }
  // var userAgent = $window.navigator.userAgent;
  // console.log(userAgent);

  var html="";
  html+='';
  html+='';
  html+='';
  html+='Bill #,';
  html+='Open Time,';
  html+='Tab Name,';
  html+='Covers,';
  html+= 'Close Time,';
  html+='Gross Amount,';
  html+='Round Off,';
  html+='Grand Total,';
  html+='Customer Name,';
  html+='Mobile Number,';
  html+='Phone Number,';
  html+='Email,';
  html+='Address,';
  html+= '\r\n';
  html+= '';
  var grandTotal = {
    totalNet: 0,
    roundOff: 0,
    afterRoundOff: 0,
    covers: 0
  };
  _.forEach(grpByCloseTime,function(closeTime,key){
    // console.log(key);
    html+=''
    html+=' '+key+',';
    html+='\r\n';
    var dayTotal =
    {
      totalNet: 0,
        roundOff: 0,
      afterRoundOff: 0,
      covers: 0
    };
    _.forEach(closeTime,function(bill)
    {
      // console.log(bill);
      dayTotal.totalNet += Utils.roundNumber(bill.totalNet, 2);
      dayTotal.roundOff += Utils.roundNumber(bill.roundOff, 2);
      dayTotal.afterRoundOff += bill.afterRoundOff;
      dayTotal.covers += bill._covers ? bill._covers : 0;
      grandTotal.totalNet += Utils.roundNumber(bill.totalNet, 2);
      grandTotal.roundOff += Utils.roundNumber(bill.roundOff, 2);
      grandTotal.afterRoundOff += bill.afterRoundOff;
      grandTotal.covers += bill._covers ? bill._covers : 0;
      html+=''
      html+=''+(bill.prefix!=undefined?bill.prefix:'')+bill.serialNumber+',';
      html+=''+moment(bill._created).format('LT')+',';
      html+=''+bill.tabType+',';
      if(bill._covers!=null)
      html+=''+bill._covers+',';
    else html+='0,';
      html+=''+moment(bill._closeTime).format('LT')+',';
      html+=''+Utils.roundNumber(bill.totalNet, 2)+',';
      html+=''+Utils.roundNumber(bill.roundOff, 2)+',';
      html+=''+Utils.roundNumber(bill.afterRoundOff, 2)+',';
     if(bill._customer.firstname)
      html+=''+bill._customer.firstname+',';
      else html+='NA,';
      if(bill._customer.mobile)
    html+=''+bill._customer.mobile+',';
      else html+='NA,';
      if(bill._customer.phone)
      html+=''+bill._customer.phone+',';
    else html+='NA,';

      if(bill._customer.email)
    html+=''+bill._customer.email+',';
      else html+='NA,';
      if(bill._customer.fullAddress)
    html+='\"'+bill._customer.fullAddress+'\",';
         else html+='NA,'
    html+='\r\n';
    })
    html+='';
    html+= 'Day Total,';
    html+=',';
    html+=',';
    html+=''+Utils.roundNumber(dayTotal.covers)+',';
    html+=',';

    html+=''+Utils.roundNumber(dayTotal.totalNet, 2)+',';
    html+=''+Utils.roundNumber(dayTotal.roundOff, 2)+',';
    html+=''+Utils.roundNumber(dayTotal.afterRoundOff)+',';
    html+=',';
    html+= '\r\n';
  })
  html+='';
  html+= 'Grand Total,';
  html+=',';
  html+=',';
  html+=''+Utils.roundNumber(grandTotal.covers)+',';
  html+=','
  html+=''+Utils.roundNumber(grandTotal.totalNet, 2)+',';
  html+=''+Utils.roundNumber(grandTotal.roundOff, 2)+',';
  html+=''+Utils.roundNumber(grandTotal.afterRoundOff)+',';
  html+=',';
  html+=  '\r\n';
  html+='';
  exportToCsv(html)
};
var populateDynamicHTMLKOTTracking = function (bills) {
  var bills = _.sortBy(bills, 'serialNumber');
  var billByCloseTime = {};
  _.forEach(bills, function (bill) {
    if (!billByCloseTime[bill.closeTime])
      billByCloseTime[bill.closeTime] = {};
    _.forEach(bill._kots, function (kot) {
      for (var it in kot.items) {
        if (_.has(kot.items[it], 'addOns')) {
          _.forEach(kot.items[it].addOns, function (addon) {
            kot.items.push(addon);
          });
        }
      }
      var _kot = {};
      if(!kot.waiter)
        kot.waiter = {};
      _kot.kotNumber = kot.kotNumber;
      _kot.kotTime = kot.created;
      _kot.billNumber = bill.serialNumber;
      _kot.billTime = bill._created;
      _kot.createdBy = kot.waiter.firstname ? (kot.waiter.firstname + " " + kot.waiter.lastname) : (bill._waiter ? (bill._waiter.firstname + " " + bill._waiter.lastname) : "NA");
      _kot.status = bill.isVoid ? "Bill Voided" : kot.isVoid ? "KOT Voided" : "Billed";
      _kot.items = kot.items;
      _kot.deletedItems = kot.deleteHistory;
      _.forEach(_kot.deletedItems, function (dItem) {
        if (_.has(dItem, 'addOns')) {
          _.forEach(dItem.addOns, function (addon) {
            _kot.deletedItems.push(addon);
          });
        }
      });
      if (bill.instance) {
        if (bill.instance.name) {
          if (!billByCloseTime[bill.closeTime][bill.instance.name])
            billByCloseTime[bill.closeTime][bill.instance.name] = [];
          billByCloseTime[bill.closeTime][bill.instance.name].push(_kot);
        }
      }
      else {
        if (!billByCloseTime[bill.closeTime]["No Instance"])
          billByCloseTime[bill.closeTime]["No Instance"] = [];
        billByCloseTime[bill.closeTime]["No Instance"].push(_kot);
      }
    });
  });
  var html = reportNameG+"\n\r\n\r";
  html += "";
  html += "";
  html += "KOT Number,";
  html += "KOT Time,";
  html += "Bill Number,";
  html += "Bill Time,";
  html += "Created By,";
  html += "Status,";
  html += "\r\n";
  _.forEach(billByCloseTime, function (dayBills, date) {
    html += "";
    html += " " + date + ","
    html += "\r\n";
    _.forEach (dayBills, function (kots, instance) {
      html += "";
      html += "Instance:  " + instance + ","
      html += "\r\n";
      _.forEach (kots, function (kot) {
        html += "";
        html += "#  " + kot.kotNumber + ",";
        html += "  " + moment(kot.kotTime).format('LT') + ",";
        html += "#  " + kot.billNumber + ",";
        html += "  " + moment(kot.billTime).format('LT') + ",";
        html += "  " + kot.createdBy + ",";
        html += "  " + kot.status + ",";
        html += "\r\n";

        var kotTotal = {
          rate: 0,
          amount: 0,
          qty: 0
        };
        html += "";
        html += "";
        html += "";
        html += "";
        html += "Item Name,";
        html += "Qty,";
        html += "Rate,";
        html += "Amount,";
        html += "Status,";
        html += "\r\n";
        var itemStatus = (kot.status == 'Bill Voided' || kot.status == 'KOT Voided') ? "Deleted" : "Billed";
        _.forEach(kot.items, function (item) {
          html += "";
          html += "" + item.name + ",";
          html += "" + item.quantity + ",";
          html += "" + item.rate + ",";
          html += "" + item.subtotal + ",";
          html += "" + itemStatus + ",";
          html += "\r\n";
          if (!(kot.status == 'Bill Voided' || kot.status == 'KOT Voided')) {
            kotTotal.rate += Number(item.rate);
            kotTotal.qty += Number(item.quantity);
            kotTotal.amount += Number(item.subtotal);
          }
        });
        _.forEach(kot.deletedItems, function (item) {
          html += "";
          html += "" + item.name + ",";
          html += "" + item.quantity + ",";
          html += "" + Utils.roundNumber(item.rate, 2) + ",";
          html += "" + Utils.roundNumber(item.subtotal, 2) + ",";
          html += "Deleted,";
          html += "\r\n";
        });
        html += "";
        html += "KOT Total,";
        html += "" + kotTotal.qty + ",";
        html += "" + Utils.roundNumber(kotTotal.rate, 2) + ",";
        html += "" + Utils.roundNumber(kotTotal.amount, 2) + ",";
        html += ",";
        html += "";
        html += "";
        html += ",";
        html += "\r\n";
      });
    });
  });
  html += "";
  exportToCsv(html)
};
var dynamicCustomerDataReport = function (bills) {

  _.forEach(bills, function (bill) {
    if (!bill.isVoid) {
      if (bill._customer) {
        if (bill._customer.address1 || bill._customer.address2 || bill._customer.city || bill._customer.state || bill._customer.postCode) {
          bill._customer.fullAddress = (bill._customer.address1 ? (bill._customer.address1 + ", ") : "") + (bill._customer.address2 ? (bill._customer.address2 + ", ") : "") + (bill._customer.city ? (bill._customer.city + ", ") : "") + (bill._customer.state ? (bill._customer.state + ", ") : "") + (bill._customer.postCode ? (bill._customer.postCode + ", ") : "");
        } else {
          bill._customer.fullAddress = "NA";
        }
      } else {
        bill._customer = {};
      }
    }
  });
  // console.log(bills)

  var html = reportNameG+"\n\r\n\r";
  html += "";
  html += "";
  html += "";
  html += "sr,";
  html += "Bill #,";
  html += "Bill Date,";
  html += "Name,";
  html += "Mobile Number,";
  html += "Phone Number,";
  html += "Email,";
  html += "Address,";
  html += "City,";
  html += "Item Name,";
  html += "Item Qty,";
  html += "Price,";
  html += "Source,";
  html += "\r\n";
  html += "";

  _.forEach(bills, function (bill, i) {
    html += "";
    html += "" + (i + 1) + ",";
    html += "" + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + ",";
    if (bill.closeTime)
      html += "" + bill.closeTime + ",";
    else html += "NA,";
    if (bill._customer.firstname)
      html += "" + bill._customer.firstname + ",";
    else html += "NA,";
    if (bill._customer.mobile)
      html += "" + bill._customer.mobile + ",";
    else html += "NA,";
    if (bill._customer.phone)
      html += "" + bill._customer.phone + ",";
    else html += "NA,";
    if (bill._customer.email)
      html += "" + bill._customer.email + ",";
    else html += "NA,";
    if (bill._customer.fullAddress)
      html += "\"" + bill._customer.fullAddress + "\",";
    else html += "NA,";
    if (bill._customer.city)
      html += "" + bill._customer.city + ",";
    else html += "NA,";
    html += "" + bill.name + ",";
    html += "" + bill.qty + ",";
    html += "" + Utils.roundNumber(bill.subtotal, 2) + ",";
    html += "" + bill.onlineBill + ",";
    html += "\r\n";
  })
  html += "";
  exportToCsv(html)
};
var getDeliveryReportDynamic = function (bills) {
  bills = _.sortBy(bills, 'serialNumber');
  // console.log(bills)
  _.forEach(bills, function (bill) {
    bill.paymentMode = [];
    bill.category = [];
    bill.item = [];
    bill.i = [];
    if (!bill.isVoid) {
      if (bill._customer) {
        if (bill._customer.address1 || bill._customer.address2 || bill._customer.city || bill._customer.state || bill._customer.postCode) {
          bill._customer.fullAddress = (bill._customer.address1 ? (bill._customer.address1 + ", ") : "") + (bill._customer.address2 ? (bill._customer.address2 + ", ") : "") + (bill._customer.city ? (bill._customer.city + ", ") : "") + (bill._customer.state ? (bill._customer.state + ", ") : "") + (bill._customer.postCode ? (bill._customer.postCode + ", ") : "");
        } else {
          bill._customer.fullAddress = "NA";
        }
      } else {
        bill._customer = {};
      }
    
      _.forEach(bill._kots, function (kot) {
        _.forEach(kot.items, function (item) {
          bill.item.push(item);
          if (item.addOns) {
            _.forEach(item.addOns, function (add) {
              bill.item.push(add);
              bill.category.push(add.category.categoryName)
            })
          }


          bill.category.push(item.category.categoryName);
        })
      })
      //console.log(bill.item);
      bill.item = _.groupBy(bill.item, function (i) {
        return i.name
      })

      _.forEach(bill.item, function (item) {
        var total = {
          totalQty: 0
          //totalAmount:0
        }
        // console.log(bill);
        _.forEach(item, function (b) {
          total.name = b.name;
          total.totalQty += parseFloat(b.quantity);
          //total.totalAmount+=parseFloat(b.subtotal);
        })
        bill.i.push(total);
      })
      // console.log(bill.i)
      bill.category = _.uniq(bill.category);
      if(!bill._delivery){
        bill._delivery={}
      }
      //console.log(bill.item);
      if (bill._delivery.deliveryTime) {
        var chkStart = moment(bill.billPrintTime);
        var chkEnd = moment(bill._delivery.deliveryTime);
        var duration = moment.duration(chkEnd.diff(chkStart));
        var hours = parseInt(duration.asHours());
        var minutes = parseInt(duration.asMinutes()) - hours * 60;

        bill._delivery.deliveryTime = (hours + ":" + minutes);
        // console.log(bill._delivery.deliveryTime);
      } else bill._delivery.deliveryTime = 'NA';
      if (bill.payments) {
        _.forEach(bill.payments.cards, function (mode) {
          if (mode.totalAmount != 0) {
            bill.paymentMode.push(mode.cardType)
          }
        })
        if (bill.payments.cash.length != 0) {
          bill.paymentMode.push("Cash");
        }
      }
    }
  })
  var html = reportNameG+"\n\r\n\r";
  html += "";
  html += "";
  html += "";
  html += "Bill #,";
  html += "Bill Date,";
  html += "Kot Time,";
  html += "Guest Name,";
  html += "Mobile Number,";
  html += "Address,";
  html += "Location,";
  html += "Product,";
  html += "Category,";
  html += "Delivery Boy,";
  html += "Delivery Time(hr:min),";
  html += "Net Amount,";
  html += "Nature,";
  html += "\r\n";
  html += "";
  _.forEach(bills, function (bill) {
    html += "";
    html += "" + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + ",";
    html += "" + bill.closeTime + ",";
    html += "" + moment(bill._created).format("h:mma") + ",";
    if (bill._customer.firstname)
      html += "" + bill._customer.firstname + ",";
    else html += "NA,";
    if (bill._customer.mobile)
      html += "" + bill._customer.mobile + ",";
    else html += "NA,";
    if (bill._customer.fullAddress)
      html += "\"" + bill._customer.fullAddress + "\",";
    else html += "NA,";
    if (bill._customer.address2)
      html += "" + bill._customer.address2 + ",";
    else html += "NA,";
    html += "\"";

    _.forEach(bill.i, function (it) {
      html += it.name + ":" + it.totalQty + ",";
    })
    html += "\",";
    html += "\"" +
      _.forEach(bill.category, function (cat) {
        +cat + ","
      })
    html += "\",";
    if (bill._delivery.firstname)
      html += "" + bill._delivery.firstname + " " + bill._delivery.lastname + ","
    else html += "NA,";
    html += "" + bill._delivery.deliveryTime + ",";
    html += "" + Utils.roundNumber(bill.totalNet, 2) + ",";
    if (bill.onlineBill!= undefined && bill.onlineBill.source)
      html += "" + bill.onlineBill.source.name + ",";
    else html += "Direct Call,";
    html += "\r\n";
  })
  html += "";
  exportToCsv(html)
};
var getDeliveryBoyDynamic = function (bills) {
  bills = _.sortBy(bills, 'serialNumber');
  var sortBill = {};
  var grpByDate = _.groupBy(bills, function (bill) {
    return bill.closeTime;
  });
  _.forEach(grpByDate, function (dayBills, date) {
      sortBill[date] = _.groupBy(dayBills, function (bill) {
        if(!bill._delivery) {
          bill._delivery={}
        }
      return bill._delivery.username;
    })
  })
  var dayTotal = {};
  var grandTotal = {
    totalNet: 0,
    timeTaken: null
  };
  var grandTotalTime = 0;
  var html = reportNameG+"\n\r\n\r"+"";
  html += "";
  html += "";
  html += "Bill Date,";
  html += "Delivery Boy,";
  html += "Bill #,";
  html += "Bill Amount,";
  html += "Dispatch Time,";
  html += "Delivered Time,";
  html += "Time Taken,";
  html += "\r\n";
  html += "";
  html += "";
  _.forEach(sortBill, function (sortBil) {

    for (var username in sortBil) {
      var dayTotal = {
        totalNet: 0,
        timeTaken: null
      }
      var total = 0;
      var duration = 0;
      // console.log(sortBil[username]);
      _.forEach(sortBil[username], function (b) {
        //console.log(b._delivery.deliveryTime);
        //console.log(b._closeTime);

        if (b._delivery.deliveryTime != undefined) {
          var chkStart = new Date(b.billPrintTime);
          chkStart.setMilliseconds(0);
          chkStart.setSeconds(0);
          var chkEnd = new Date(b._delivery.deliveryTime);
          chkEnd.setMilliseconds(0);
          chkEnd.setSeconds(0);
          var cS = chkStart.getTime();
          var cE = chkEnd.getTime();
          duration = Math.abs(cE - cS);
        } else {
          // console.log("inside else");
          /*console.log(new Date(b._closeTime));
           console.log(new Date(b.billPrintTime));*/
          var chkStart = new Date(b.billPrintTime);
          chkStart.setMilliseconds(0);
          chkStart.setSeconds(0);
          var chkEnd = new Date(b._closeTime);
          chkEnd.setMilliseconds(0);
          chkEnd.setSeconds(0);
          var cS = chkStart.getTime();
          var cE = chkEnd.getTime();
          // console.log(cS, cE)
          if (b.billPrintTime != undefined || b._closeTime != undefined)
            duration = Math.abs(cE - cS);
          else duration = 0;

        }
        // console.log(duration);
        if (isNaN(duration))
          duration = 0;


        total += Math.abs(duration);
        // console.log(total);
        b.timeTaken = reportUtils.msToTimes(duration);
        /* }else
         {
         duration=0;
         total += duration;
         console.log(total);
         b.timeTaken = reportUtils.msToTimes(duration);
         }*/
        // console.log(b.timeTaken);
        if (!dayTotal[username]) {
          dayTotal[username] = {
            totalNet: 0,
            timeTaken: null
          };
        }


        dayTotal[username].totalNet += Utils.roundNumber(b.totalNet, 2);
        grandTotal.totalNet += Utils.roundNumber(b.totalNet, 2);
        // console.log("bill print time" + b.billPrintTime);
        // console.log("bill close time" + b._closeTime);
        // console.log("bill delivery time" + b._delivery.deliveryTime);
        // console.log("bill number" + b.billNumber);

        dayTotal[username].timeTaken = reportUtils.msToTimes(total);
        // if(duration!=0||!isNaN(duration)) {
        grandTotalTime += Math.abs(duration);
        // console.log(grandTotalTime);
        b.timeTaken = reportUtils.msToTimes(duration);
        /*}else
         {
         duration=0;
         grandTotalTime+=duration;
         console.log(grandTotalTime);
         b.timeTaken = reportUtils.msToTimes(duration);
         }*/


        html += "";
        html += "" + b.closeTime + ",";
        if (b._delivery.username != undefined) {
          html += "" + b._delivery.firstname + " " + b._delivery.lastname + ",";
        } else html += "NA,";
        html += "" + (b.prefix!=undefined?b.prefix:'')+b.serialNumber + ",";
        html += "" + Utils.roundNumber(b.totalNet, 2) + ",";
        if (b.billPrintTime != undefined)
          html += "" + moment(b.billPrintTime).format('LT') + ",";
        else html += "NA,"
        if (b._delivery.deliveryTime != undefined)
          html += "" + moment(b._delivery.deliveryTime).format('LT') + ",";
        else html += "" + moment(b._closeTime).format('LT') + ",";

        html += "" + b.timeTaken + ",";

        html += "\r\n";

      })
      html += ""
      html += ",";
      html += "Delivery Boy Total,";
      html += "" + Utils.roundNumber(dayTotal[username].totalNet, 2) + ",";
      html += "Total Time Taken,";
      html += "" + dayTotal[username].timeTaken + ",";
      html += "\r\n";
      html += " ";

      total = 0;
    }
  })
  grandTotal.timeTaken = reportUtils.msToTimes(grandTotalTime);
  html += "";
  html += "";
  html += "Grand Total,";
  html += "Delivery Boy's Total,";
  html += "" + Utils.roundNumber(grandTotal.totalNet, 2) + ",";
  html += "Total Time Taken,";
  html += "" + grandTotal.timeTaken + ",";
  html += "\r\n";
  html += "";
  html += "";
  exportToCsv(html)
};
var dynamicGetItemWiseConsolidate = function (bills, sSuperCategory, sCategory) {
  var totaladdOns = [];
  var grandTotal = {
    totalDiscount: 0,
    totalTax: 0,
    totalNet: 0,
    totalQty: 0,
    totalAmount: 0,
    complimentary: 0,
    nonComplimentary: 0,
    roundOff:0,
    afterRoundOff:0
  }

  var uniqSuperCategories = {};
  var uniqCategories = {};
  var html = reportNameG+"\n\r\n\r"+" ";
  var groupSuperCat = _.groupBy(bills, function (gCat) {
    if ((gCat.category.superCategory == undefined || gCat.category.superCategory == null)) {
      gCat.category.superCategory = {};
      gCat.category.superCategory.superCategoryName = 'NA';
      gCat.category.superCategory._id = null;

    }
    if(!uniqCategories[gCat.category.categoryName]){
      uniqCategories[gCat.category.categoryName] = [];
      uniqCategories[gCat.category.categoryName].push({categoryName: gCat.category.categoryName, superCategory: {superCategoryName: gCat.category.superCategory.superCategoryName}});
    } else {
      if(!uniqSuperCategories[gCat.category.superCategory.superCategoryName])
        uniqCategories[gCat.category.categoryName].push({categoryName: gCat.category.categoryName, superCategory: {superCategoryName: gCat.category.superCategory.superCategoryName}});
    }


    if(!uniqSuperCategories[gCat.category.superCategory.superCategoryName]){
      uniqSuperCategories[gCat.category.superCategory.superCategoryName] = {superCategory: gCat.category.superCategory.superCategoryName};
    }

    //superCategoriesForConsolidatedItemwise.push();
    if(sSuperCategory){
      //console.log(gCat.category.superCategory.superCategoryName, sSuperCategory, (gCat.category.superCategory == sSuperCategory));
      if(gCat.category.superCategory.superCategoryName == sSuperCategory)
        return gCat.category.superCategory.superCategoryName;
      else
        return 'exclude';

    }
    else return gCat.category.superCategory.superCategoryName;
    //return gCat.category.superCategory.superCategoryName;
  });
  //console.log(groupSuperCat);

  var grpdBills = {};
  _.forEach(groupSuperCat, function (cat, scat) {

    grpdBills[scat] = _.groupBy(cat, function (cate) {

      if(sCategory){
        if(sCategory == cate.category.categoryName)
          return cate.category.categoryName;
        else
          return 'exclude';
      }
      else return cate.category.categoryName;
      //return cate.category.categoryName;
    });
  });
  //console.log(grpdBills);



  Object.keys(grpdBills).forEach(function (key) {

    if(key != 'exclude') {
      var _superCatTotal = {
        totalSubtotal: 0,
        totalQty: 0,
        totalDiscount: 0,
        totalTax: 0,
        totalNet: 0,
        complimentary: 0,
        nonComplimentary: 0,
        roundOff:0,
        afterRoundOff:0
      }
      //var supertotalNetAmt=0;
      //console.log(key);
      html += "";
      html += "" + key + ",";
      html += "\r\n";
    }
      _.forEach(grpdBills[key], function (category, catt) {
        if(catt != 'exclude' && key != 'exclude'){
          var _catTotal = {
            totalSubtotal: 0,
            totalQty: 0,
            totalDiscount: 0,
            totalTax: 0,
            totalNet: 0,
            complimentary: 0,
            nonComplimentary: 0,
            roundOff:0,
            afterRoundOff:0
          }
          var totalNetAmt = 0;
          html += "";
          html += " " + category[0].category.categoryName + ",";
          html += "\r\n";

          html += "";
          html += "Item Name,";
          html += "Rate,";
          html += "Qty,";
          html += "Complimentary,";
          html += "Non-Complimentary,"
          if(!discountValue)
          html += "Discount,";
          html += "Net Amt,";
          html += "Total Tax,";
          html += "Gross Amount,";
          html += "Round Off,";
          html += "G. Total,";
          html += "\r\n";
        }
          /*
           console.log(grpdBills[key]);
           console.log(category);
           */

          _.forEach(category, function (item) {

            if (item.addOns.length != 0) {
              _.forEach(item.addOns, function (addOn) {
                _.forEach(addOn, function (add) {

                  //console.log(add);
                  if (_.has(add, "_id")) {
                    totaladdOns.push(add);
                  }

                })
              })
            }
            if(catt != 'exclude' && key != 'exclude'){
            var total = {
              discount: 0,
              tax: 0,
              totalNet: 0,
              nonComplimentary: 0,
              roundOff:0,
              afterRoundOff:0
            }

            //console.log(item);
            html += "";
            html += "" + item.name + ",";
            html += "" + item.rate + ",";
            html += "" + item.quantity + ",";
            html += "" + item.complimentary + ",";
            total.nonComplimentary = item.quantity - item.complimentary;
            html += "" + total.nonComplimentary + ",";
            if(item.discountAmount == undefined)
              item.discountAmount =0

            item.showSubtotal =  parseFloat(item.subtotal) - (parseFloat(item.discountAmount));
            // console.log(item.name,item.discountAmount,item.itemBillDiscount,item.subtotal)
            // console.log(item.showSubtotal);
            grandTotal.totalAmount +=  parseFloat(item.subtotal) - (parseFloat(item.discountAmount));
            grandTotal.totalQty += parseFloat(item.quantity);
            grandTotal.complimentary += parseFloat(item.complimentary);
            grandTotal.nonComplimentary += parseFloat(total.nonComplimentary);
            _catTotal.totalSubtotal += parseFloat(item.subtotal) - (parseFloat(item.discountAmount));
            _catTotal.totalQty += parseFloat(item.quantity);
            _catTotal.complimentary += parseFloat(item.complimentary);
            _catTotal.nonComplimentary += parseFloat(total.nonComplimentary);
            _superCatTotal.totalSubtotal +=  parseFloat(item.subtotal) - (parseFloat(item.discountAmount))
            _superCatTotal.totalQty += parseFloat(item.quantity);
            _superCatTotal.complimentary += parseFloat(item.complimentary);
            _superCatTotal.nonComplimentary += parseFloat(total.nonComplimentary)
            if (item.discountAmount != 0) {

              total.discount += parseFloat(item.discountAmount) ;
              _catTotal.totalDiscount += parseFloat(item.discountAmount) ;
              _superCatTotal.totalDiscount += parseFloat(item.discountAmount) ;
              grandTotal.totalDiscount += parseFloat(item.discountAmount) ;

            }
            if(!discountValue)
            {
              if (total.discount != 0)
              html += "" + Utils.roundNumber(total.discount, 2) + ",";
            else html += "" + 0 + ",";
            if(item.discountAmount == undefined)
              item.discountAmount =0;
            html += "" + Utils.roundNumber(item.showSubtotal, 2)  + ",";
          }else{
             if(item.discountAmount == undefined)
              item.discountAmount =0;
             html += "" + Utils.roundNumber(item.showSubtotal, 2)  + ",";
          }
            if (item.taxAmount != 0) {

              total.tax += Utils.roundNumber(item.taxAmount, 2);
              _catTotal.totalTax += Utils.roundNumber(item.taxAmount, 2);
              _superCatTotal.totalTax += Utils.roundNumber(item.taxAmount, 2);
              grandTotal.totalTax += Utils.roundNumber(item.taxAmount, 2);
            }


            if (total.tax != 0)
              html += "" + Utils.roundNumber(total.tax, 2) + ",";
            else html += "" + 0 + ",";
            total.totalNet = (parseFloat(item.showSubtotal) + parseFloat(total.tax)) ;
            //totalNetAmt=total.totalNet;
            total.roundOff = reportUtils.getRoundOffs(total.totalNet);
            total.afterRoundOff = reportUtils.getAfterRoundOff(total.totalNet)
            _catTotal.roundOff += parseFloat(total.roundOff);
            _superCatTotal.roundOff += parseFloat(total.roundOff);
            grandTotal.roundOff += parseFloat(total.roundOff);
            _catTotal.afterRoundOff += parseFloat(total.afterRoundOff);
            _superCatTotal.afterRoundOff += parseFloat(total.afterRoundOff);
            grandTotal.afterRoundOff += parseFloat(total.afterRoundOff);

            _catTotal.totalNet += parseFloat(total.totalNet);
            _superCatTotal.totalNet += parseFloat(total.totalNet);
            grandTotal.totalNet += parseFloat(total.totalNet);
            html += "" + Utils.roundNumber(total.totalNet, 2) + ",";
            html += "" + Utils.roundNumber(total.roundOff, 2) + ",";
            html += "" + Utils.roundNumber(total.afterRoundOff, 2) + ",";
            html += "\r\n";
          }
          });

        if(catt != 'exclude' && key != 'exclude'){
          html += "";
          html += "  Category Total,"
          html+=",";
          html += " " + _catTotal.totalQty + "";
          html += ",";
          html += "" + _catTotal.complimentary + ",";
          html += "" + _catTotal.nonComplimentary + ",";

         if(!discountValue)
          {if (_catTotal.totalDiscount != 0)
            html += "" + Utils.roundNumber(_catTotal.totalDiscount, 2) + ",";
          else html += "" + 0 + ",";
           html += "" + Utils.roundNumber(_catTotal.totalSubtotal, 2) + ",";
         }else
          html += "" + (Utils.roundNumber(_catTotal.totalSubtotal, 2) - Utils.roundNumber(_catTotal.totalDiscount, 2)) + ",";
          html += "" + Utils.roundNumber(_catTotal.totalTax, 2) + ",";
          html += "" + Utils.roundNumber(_catTotal.totalNet, 2) + ",";
          html += "" + Utils.roundNumber(_catTotal.roundOff, 2) + ",";
            html += "" + Utils.roundNumber(_catTotal.afterRoundOff) + ",";
          html += "\r\n";
        }
      });
    if(key != 'exclude') {
      html += "";
      html += "  SuperCategory Total,";
     html+=",";
      html += " " + _superCatTotal.totalQty + "";
      html += ",";
      html += "" + _superCatTotal.complimentary + ",";
      html += "" + _superCatTotal.nonComplimentary + ",";
      if(!discountValue)
      {
        if (_superCatTotal.totalDiscount != 0)
        html += "" + Utils.roundNumber(_superCatTotal.totalDiscount, 2) + ",";
      else html += "" + 0 + ",";
      html += "" + Utils.roundNumber(_superCatTotal.totalSubtotal, 2) + ",";
    }else  html += "" + (Utils.roundNumber(_superCatTotal.totalSubtotal, 2) - Utils.roundNumber(_superCatTotal.totalDiscount, 2)) + ",";
      html += "" + Utils.roundNumber(_superCatTotal.totalTax, 2) + ",";
      html += "" + Utils.roundNumber(_superCatTotal.totalNet, 2) + ",";
       html += "" + Utils.roundNumber(_superCatTotal.roundOff, 2) + ",";
            html += "" + Utils.roundNumber(_superCatTotal.afterRoundOff) + ",";
      html += "\r\n";
    }
  });
  var groupedAddOns = {};
  var grpAddon = _.groupBy(totaladdOns, function (add) {
    if(add.category){
      if(!add.category.categoryName)
        add.category.categoryName = "NA";
      if(add.category.superCategory){
        if(!add.category.superCategory.superCategoryName)
          add.category.superCategory.superCategoryName = "NA";
      } else {
        add.category.superCategory = {superCategoryName: "NA"};
      }
    } else {
      add.category = {categoryName: "NA", superCategory: {superCategoryName: "NA"}};
    }

    if(!uniqCategories[add.category.categoryName]){
      uniqCategories[add.category.categoryName] = [];
      uniqCategories[add.category.categoryName].push({categoryName: add.category.categoryName, superCategory: {superCategoryName: add.category.superCategory.superCategoryName}});
    } else {
      if(!uniqSuperCategories[add.category.superCategory.superCategoryName])
        uniqCategories[add.category.categoryName].push({categoryName: add.category.categoryName, superCategory: {superCategoryName: add.category.superCategory.superCategoryName}});
    }

    if(!uniqSuperCategories[add.category.superCategory.superCategoryName]){
      uniqSuperCategories[add.category.superCategory.superCategoryName] = {superCategory: add.category.superCategory.superCategoryName};
    }

    if(sSuperCategory){
      if(sSuperCategory == add.category.superCategory.superCategoryName)
        return add.category.superCategory.superCategoryName;
      else return 'exclude';
    } else
        return add.category.superCategory.superCategoryName;
    //return add.category.superCategory.superCategoryName;
  });
  _.forEach(grpAddon, function (category, subcat) {
    groupedAddOns[subcat] = {};
    var grpcat = _.groupBy(category, function (c) {

      if(sCategory){
        if(sCategory == c.category.categoryName)
          return c.category.categoryName;
        else
          return 'exclude';
      }
      else return c.category.categoryName;
      //return c.category.categoryName;
    });
    _.forEach(grpcat, function (addOn, cat) {
      groupedAddOns[subcat][cat] = _.groupBy(addOn, function (add) {
        return add.name;
      })
    })
    // console.log(groupedAddOns[category] );
  })
  //console.log(groupedAddOns);

  Object.keys(groupedAddOns).forEach(function (key) {
        if(key != 'exclude'){
        var superCatTotal = {
          discount: 0,
          totalTax: 0,
          totalNet: 0,
          totalQty: 0,
          totalAmount: 0,
          complimentary: 0,
          nonComplimentary: 0,
          roundOff:0,
          afterRoundOff:0
        }
        html += "";
        html += "" + key + ",";
        html += "\r\n";
        Object.keys(groupedAddOns[key]).forEach(function (catKey) {
          //console.log( groupedAddOns[key][catKey]);
            if(catKey != 'exclude'){
            var catTotal = {
              discount: 0,
              totalTax: 0,
              totalNet: 0,
              totalQty: 0,
              totalAmount: 0,
              complimentary: 0,
              nonComplimentary: 0,
               roundOff:0,
          afterRoundOff:0
            }
            html += "";
            html += " " + catKey + ",";
            html += "\r\n";
            html += "";
            html += "Item Name,";
            html += "Rate,";
            html += "Qty,";
            html += "Complimentary,";
            html += "Non-complimentary,"
           if(!discountValue)
            html += "Discount,";
             html += "Amt,";
            html += "Total Tax,";
            html += "Net,";
            html += "Round Off,";
            html += "G. Total,";
            html += "\r\n";
            Object.keys(groupedAddOns[key][catKey]).forEach(function (addKey) {

              var total = {
                discount: 0,
                tax: 0,
                totalNet: 0,
                totalQty: 0,
                totalAmount: 0,
                complimentary: 0,
                nonComplimentary: 0,
                 roundOff:0,
                afterRoundOff:0
              }
              _.forEach(groupedAddOns[key][catKey][addKey], function (item) {

                total.name = item.name;
                total.rate = item.rate;
                total.totalQty += parseFloat(item.quantity);
                total.complimentary += 0;
                total.nonComplimentary += parseFloat(item.quantity);
                catTotal.nonComplimentary += parseFloat(item.quantity);
                superCatTotal.nonComplimentary += parseFloat(item.quantity);
                catTotal.totalQty += parseFloat(item.quantity);
                superCatTotal.totalQty += parseFloat(item.quantity);

                total.totalAmount += parseFloat(item.subtotal);
                catTotal.totalAmount += parseFloat(item.subtotal);
                superCatTotal.totalAmount += parseFloat(item.subtotal);
                grandTotal.totalQty += parseFloat(item.quantity);
                grandTotal.nonComplimentary += parseFloat(item.quantity);
                grandTotal.totalAmount += parseFloat(item.subtotal);
                if (item.taxAmount != 0) {

                  total.tax += parseFloat(item.taxAmount);
                  catTotal.totalTax += parseFloat(item.taxAmount);
                  superCatTotal.totalTax += parseFloat(item.taxAmount);
                  grandTotal.totalTax += parseFloat(item.taxAmount);

                }


              })


              html += "";
              html += "" + total.name + ",";
              html += "" + total.rate + ",";
              html += "" + Utils.roundNumber(total.totalQty, 2) + ",";
              html += "" + total.complimentary + ",";
              html += "" + total.nonComplimentary + ",";
              

              if(!discountValue)
              {
                if (total.discount != 0)
                html += "" + Utils.roundNumber(total.discount, 2) + ",";
              else html += "" + 0 + ",";
              html += "" + Utils.roundNumber(total.totalAmount, 2) + ",";
                }
                else  {
                  if(total.discount == undefined || !total.discount)
                    total.discount = 0;
                  html += "" + Utils.roundNumber(total.totalAmount, 2) + ",";
                }
              if (total.tax != 0)
                html += "" + Utils.roundNumber(total.tax, 2) + ",";
              else html += "" + 0 + ",";

              if(total.discount == undefined)
                total.discount = 0;
              total.totalNet = parseFloat(total.totalAmount) + parseFloat(total.tax) ;
              // totalNetAmt=total.totalNet;
              // console.log(total.totalNet)
              // console.log(total.totalAmount)
               total.roundOff = reportUtils.getRoundOffs(total.totalNet);
            total.afterRoundOff = reportUtils.getAfterRoundOff(total.totalNet)
            catTotal.roundOff += parseFloat(total.roundOff);
            superCatTotal.roundOff += parseFloat(total.roundOff);
            grandTotal.roundOff += parseFloat(total.roundOff);
            catTotal.afterRoundOff += parseFloat(total.afterRoundOff);
            superCatTotal.afterRoundOff += parseFloat(total.afterRoundOff);
            grandTotal.afterRoundOff += parseFloat(total.afterRoundOff);
              catTotal.totalNet += parseFloat(total.totalNet);
              //supertotalNetAmt+=parseFloat(totalNetAmt);
              superCatTotal.totalNet += parseFloat(total.totalNet);
              grandTotal.totalNet += parseFloat(total.totalNet);
              html += "" + Utils.roundNumber(total.totalNet, 2) + ",";
              html += "" + Utils.roundNumber(total.roundOff, 2) + ",";
              html += "" + Utils.roundNumber(total.afterRoundOff) + ",";
              html += "\r\n";

          });
          html += "";
          html += "  Category Total,"
              html+=",";
          html += " " + catTotal.totalQty + "";
          html += ",";
          html += "" + catTotal.complimentary + ",";
          html += "" + catTotal.nonComplimentary + ",";
         if(!discountValue)
         {
          if (catTotal.totalDiscount != undefined)
            html += "" + Utils.roundNumber(catTotal.totalDiscount, 2) + ",";
          else html += "" + 0 + ",";
           html += "" + Utils.roundNumber(catTotal.totalAmount, 2) + ",";
         }
         else html += "" + Utils.roundNumber(catTotal.totalAmount, 2)  + ",";
          if (catTotal.totalTax != undefined)
            html += "" + Utils.roundNumber(catTotal.totalTax, 2) + ",";
          else html += "" + 0 + ",";
          html += "" + Utils.roundNumber(catTotal.totalNet, 2) + ",";
          html += "" + Utils.roundNumber(catTotal.roundOff, 2) + ",";
          html += "" + Utils.roundNumber(catTotal.afterRoundOff) + ",";
          html += "\r\n";
        }
      })
      html += "";
      html += "  SuperCategory Total,";
          html+=",";
      html += " " + superCatTotal.totalQty + "";
      html += ",";
      html += "" + superCatTotal.complimentary + ",";
      html += "" + superCatTotal.nonComplimentary + ",";
     if(!discountValue)
      {if (superCatTotal.totalDiscount != undefined)
        html += "" + Utils.roundNumber(superCatTotal.totalDiscount, 2) + ",";
      else html += "" + 0 + ",";
       html += "" + Utils.roundNumber(superCatTotal.totalAmount, 2) + ",";
     }else html += "" + Utils.roundNumber(superCatTotal.totalAmount, 2)  + ",";
      if (superCatTotal.totalTax != undefined)
        html += "" + Utils.roundNumber(superCatTotal.totalTax, 2) + ",";
      else html += "" + 0 + ",";
      html += "" + Utils.roundNumber(superCatTotal.totalNet, 2) + ",";
      html += "" + Utils.roundNumber(superCatTotal.roundOff, 2) + ",";
      html += "" + Utils.roundNumber(superCatTotal.afterRoundOff) + ",";
    }
    html+="\r\n";
  });
  html += "";
  html += "  Grand Total,";
  html+=",";
  html += " " + grandTotal.totalQty + "";
  html += ",";
  html += "" + grandTotal.complimentary + ",";
  html += "" + grandTotal.nonComplimentary + ",";
 if(!discountValue)
 {
  if ((grandTotal.totalDiscount != undefined || grandTotal.totalDiscount != 0))
    html += "" + Utils.roundNumber(grandTotal.totalDiscount, 2) + ",";
  else html += "" + 0 + ",";
   html += "" + Utils.roundNumber(grandTotal.totalAmount, 2) + ",";
 }else 
 html+=  "" + Utils.roundNumber((Utils.roundNumber(grandTotal.totalAmount, 2) -  Utils.roundNumber(grandTotal.totalDiscount, 2)),2)  + ",";
 // console.log(grandTotal.totalDiscount)
  if ((grandTotal.totalTax != undefined || grandTotal.totalTax != 0))
    html += "" + Utils.roundNumber(grandTotal.totalTax, 2) + ",";
  else html += "" + 0 + ",";
  html += "" + Utils.roundNumber(grandTotal.totalNet, 2) + ",";
  html += "" + Utils.roundNumber(grandTotal.roundOff, 2) + ",";
  html += "" + Utils.roundNumber(grandTotal.afterRoundOff) + ",";
  html += "\r\n";
  html += "";
  exportToCsv(html)
};
var dynamicGetItemWiseProfit = function (bills) {
  //console.log("inside");
  var totalItems = [];
  var grandNet = 0;
  var html = reportNameG+"\n\r\n\r";

  totalItems  = _.sortBy(bills,function(item){
    if(item.category == undefined)
    {
      item.category={};
      item.category.categoryName = 'NA';
      item.category.superCategory={};
      item.category.superCategory.superCategoryName ="NA";
    }else if(item.category.superCategory == undefined)
    {
      item.category.superCategory={};
      item.category.superCategory.superCategoryName ='NA';
    }
    else if(item.category.superCategory.superCategoryName == undefined || item.category.superCategory.superCategoryName == null )
      item.category.superCategory.superCategoryName ='NA';
    return item.category.superCategory.superCategoryName;
  })
  _.forEach(totalItems, function (item) {
    if (item.discountAmount == undefined) {
      item.discountAmount = 0;
    }
    item.net = Utils.roundNumber(parseFloat((item.subtotal) - item.discountAmount), 2);
    // console.log(item.net);
    grandNet += parseFloat(item.net);
  })
  // console.log(Utils.roundNumber(grandNet, 2));

  html += "";
  html += "";
  html += "Item No,"
  html += "SuperCategoryName,";
  html += "CategoryName,";
  html += "ItemName,";
  html += "Rate,";
  html += "Quantity,";
  html += "Amount,";
  if(!discountValue)
    html += "Discount,";
  html += "Net,";
  html += "Profit,";
  html += "\r\n";
  var grandTotal = {
    quantity: 0,
    amount: 0,
    tax: 0,
    discount: 0,
    profit: 0,
    discountAmount:0
  }
  _.forEach(totalItems, function (item, i) {
    //console.log(item.net);
    item.profit = ((Utils.roundNumber(item.net, 2) / Utils.roundNumber(grandNet, 2)) * 100).toFixed(3);
    html += "";
    html += "" + (i + 1) + ",";
    html += "" + item.category.superCategory.superCategoryName + ",";
    html += "" + item.category.categoryName + ",";
    html += "" + item.name + ",";
    html += "" + item.rate + ",";
    html += "" + item.quantity + ",";
    if(discountValue) {
      if(item.discountAmount!=undefined)
        html += "" + (Utils.roundNumber(item.subtotal, 2)-Utils.roundNumber(item.discountAmount,2)) + ",";
      else html+=""+Utils.roundNumber(item.subtotal, 2)+",";
    } else {
      html += "" + Utils.roundNumber(item.subtotal, 2) + ",";
      if (item.discountAmount != undefined) {
        html += "" + Utils.roundNumber(item.discountAmount, 2) + ",";
        grandTotal.discount += parseFloat(item.discountAmount);
      } else {
        item.discountAmount = 0;
        grandTotal.discount += 0;
        html += "0,";
      }
    }
    html += "" + Utils.roundNumber(item.net, 2) + ",";
    html += "" + Utils.roundNumber(item.profit, 3) + "%" + ",";
    html += "\r\n";
    grandTotal.quantity += parseFloat(item.quantity);
    grandTotal.taxAmount = parseFloat(item.taxAmount);
    grandTotal.amount += parseFloat(item.subtotal);
    grandTotal.profit += parseFloat(item.profit);
    grandTotal.discountAmount+=( Utils.roundNumber(item.subtotal,2)-Utils.roundNumber(item.discountAmount,2))
  });
  // console.log(grandTotal.profit);
  html += "";
  html += "GrandTotal,";

  html += ",";
  html += ",";
  html += ",";
  html += ",";
  html += "" + grandTotal.quantity + ",";
  if(discountValue)
    html+=""+Utils.roundNumber(grandTotal.discountAmount,2)+",";
  else {
    html += "" + Utils.roundNumber(grandTotal.amount, 2) + ",";
    if (grandTotal.discount != undefined) {
      html += "" + Utils.roundNumber(grandTotal.discount, 2) + ",";
    } else {
      html += "0,";
    }
  }
  html += "" + Utils.roundNumber(grandNet, 2) + ",";
  html += "" + Utils.roundNumber(grandTotal.profit, 0) + ",";
  html += "\r\n";
  html += "";
  exportToCsv(html)
};
var populateDynamicHTMLRemovedTaxesReport = function(bills) {
  bills = _.sortBy(bills, function (rep) {
    return new Date(rep._created).getTime();
  });
  _.forEach(bills, function (rep) {
    rep.formattedDate = reportUtils.getFormattedDateByCutoffTime(new Date(rep._created),cutOffTimeSettings);
    if (!rep.prefix) {
      rep.instance = {
        name: '',
        preFix: ''
      }
    } else {
      rep.instance = {
        name: '',
        preFix: rep.prefix
      }
    }
  });
  var html = reportNameG+"\n\r\n\r"+"";
  var remTaxBills = _.groupBy(bills, 'formattedDate');
  remTaxBills = reportUtils.getObjectKeysSorted(remTaxBills);

  for (var bill in remTaxBills) {
    html += "";
    html += "" + bill + ",";
    html += "\r\n";
    for (var i = 0; i < remTaxBills[bill].length; i++) {
      var userName;
      if (remTaxBills[bill][i]._currentUser == undefined) {
        remTaxBills[bill][i]._currentUser = {};
      }
      if (remTaxBills[bill][i]._currentUser.username) {
          userName = remTaxBills[bill][i]._currentUser.username;
      } else userName = "NA";
      if(Array.isArray(remTaxBills[bill][i].removedTaxes)){
        if(remTaxBills[bill][i].removedTaxes.length >= 1){
          html += "";
          html += "Bill Serial# ~ " + (remTaxBills[bill][i].serialNumber) + "";
          html += "(Taxes Removed By ~ " + userName + ")"
          html += ",";
          html += "\r\n";

          html += "";
          html += "Tax,";
          html += "Username,";
          html += "\r\n";
          if (Array.isArray(remTaxBills[bill][i].removedTaxes)) {
            for (var j = 0; j < remTaxBills[bill][i].removedTaxes.length; j++) {
              html += "";
              html += "" + remTaxBills[bill][i].removedTaxes[j].name + ",";
              html += "" + (remTaxBills[bill][i].removedTaxes[j].userName || 'NA') + ",";
              html += "\r\n";
            }
          } 
        }
      }
    }
  }
  html += "";
  exportToCsv(html)
};
var exportToCsvForCategory = function(bills){
  var _allItemsFor = [];
  _.forEach(bills,function(bill){
    var _allTaxes = [];
    var _allItems = [];
    if (bill.isVoid != true) {
      _.forEach(bill._kots, function (kot) {
        if (!kot.isVoid) {
          bill._items = []
          for(var it in kot.items){
            if(_.has(kot.items[it],'addOns')) {
              _.forEach(kot.items[it].addOns,function(addon){
                _allItems.push(addon);
              })
            }
          }
          _.forEach(kot.items, function (item) {
            _allItems.push(item);
          });
        }
      });
      _.chain(_allItems).groupBy('_id').map(function (i) {
        var _itemAmount = 0,
          _itemQ = 0,
          _itemD = 0,
          _compItems = 0,
          _nonCompItems = 0,
          _compDiscount = 0,
          _nonCompDiscount = 0;
        var _itemBillDiscount=0;
        _.forEach(i, function (it) {
          _itemAmount += Utils.roundNumber(parseFloat(it.subtotal),2);
          _itemQ += parseFloat(it.quantity);
          /* Discounts */
          var d = 0;
          if (_.has(it, 'isDiscounted') && it.isDiscounted === true) {
            _.forEach(it.discounts, function (discount) {
              if (discount.type === 'fixed') {
                _itemD += parseFloat(discount.discountAmount);
                d += discount.discountAmount;
              }
              if (discount.type === 'percentage') {
                _itemD += parseFloat(discount.discountAmount);
                d += discount.discountAmount;
              }
            });
          }
           _itemBillDiscount+=Utils.roundNumber(it.billDiscountAmount,2);
          if(Utils.roundNumber(d) === Utils.roundNumber(it.subtotal)){
            _compItems += it.quantity;
            _compDiscount += Utils.roundNumber(d,2);
          } else {
            _nonCompItems += it.quantity;
            _nonCompDiscount += d;
          }
        });
        /* Flatten Tax */
        var _t = [];
        var _itemTotalTax = 0;
        var _allItemTaxes = [];
        _.forEach(i, function (_ite) {
          _.forEach(_ite.taxes, function (tax) {
            _allItemTaxes.push({
              id: tax._id + '_amt',
              name: tax.name,
              tax_amount: tax.baseRate_preTax? parseFloat(tax.baseRate_preTax): 0,
              isConsidered: false
            });
            _allItemTaxes.push({
              id: tax._id,
              name: tax.name,
              tax_amount: tax.tax_amount? parseFloat(tax.tax_amount): 0,
              isConsidered: true
            });
            _.forEach(tax.cascadingTaxes, function (_cTax) {
              if (_cTax.selected) {
                _allItemTaxes.push({
                  id:_cTax._id + "_amt",
                  name: _cTax.name,
                  tax_amount: (_.has(_cTax, 'baseRate_preTax')) ? Utils.roundNumber(parseFloat(_cTax.baseRate_preTax),2) : 0,
                  isConsidered: false
                });
                _allItemTaxes.push({
                  id: _cTax._id,
                  name: _cTax.name,
                  tax_amount: _cTax.tax_amount? Utils.roundNumber(parseFloat(_cTax.tax_amount),2): 0,
                  isConsidered: true
                });
              }
            });
          });
        });
        var _groupedItemTaxes = [];
        _.chain(_allItemTaxes).groupBy('id').map(function (_ts) {
          var _tsTotal = 0;
          _.forEach(_ts, function (_tswa) {
            _tsTotal += Utils.roundNumber(parseFloat(_tswa.tax_amount),2);
          });
          _groupedItemTaxes.push({
            id: _ts[0].id,
            name: _ts[0].name,
            isConsidered: _ts[0].isConsidered,
            tax_amount: parseFloat(_tsTotal)
          });
        });
        /* Calc Total Tax */
        _.forEach(_groupedItemTaxes, function (_taxwa) {
          if (_taxwa.isConsidered) {
            _itemTotalTax += Utils.roundNumber(parseFloat(_taxwa.tax_amount),2);
          }
        });
        if(i[0].category){
          if((i[0].category.superCategory== undefined||i[0].category.superCategory==null)) {
            i[0].category.superCategory = {};
            i[0].category.superCategory.superCategoryName = 'NA';
            i[0].category.superCategory._id = null;
          } else if(!i[0].category.superCategory.superCategoryName)
            i[0].category.superCategory.superCategoryName = "NA";
        } else {
          i[0].category = {};
          i[0].category._id = null;
          i[0].category.categoryName = "NA";
          i[0].category.superCategory = {};
          i[0].category.superCategory.superCategoryName = 'NA';
          i[0].category.superCategory._id = null;
        }
        if(!i[0].category){
          i[0].category = {
            categoryName: "NA",
            superCategory: {
              superCategoryName: "NA"
            }
          };
        } else {
          if(!i[0].category.superCategory){
            i[0].category.superCategory = {
              superCategoryName: "NA"
            }
          } else {
            if(!i[0].category.superCategory)
              i[0].category.superCategory.superCategoryName = "NA";
          }
        }
        var _i = {
          "id": i[0]._id,
          "name": i[0].name,
          "rate": i[0].rate,
          "quantity": _itemQ,
          "subtotal": parseFloat(_itemAmount),
          "taxes": _groupedItemTaxes,
          "itemNet": (parseFloat(_itemAmount) - parseFloat(_itemD)) + parseFloat(_itemTotalTax),
          "discount": _itemD,
          "tab": bill.tab,
          "billDiscountAmount": _itemBillDiscount,
          "category": i[0].category.categoryName,
          "superCategory":i[0].category.superCategory.superCategoryName,
          "compItems": _compItems,
          "_compDiscount": _compDiscount,
          "nonCompItems": _nonCompItems,
          "_nonCompDiscount": _nonCompDiscount
        };
        bill._items.push(_i);
      });
    }
  })

  _.forEach(bills, function (bill) {
    var isBillComplementary = false;
    if(bill.complimentary){
      isBillComplementary = true;
    }
    _.forEach(bill._items, function (item) {
      if(isBillComplementary){
        item.complementaryItemQty = item.quantity;
        item.nonComplementaryItemQty = 0;
      }
      else {
        item.complementaryItemQty = item.compItems;
        item.nonComplementaryItemQty = item.nonCompItems;
      }
      item.billCloseTime = bill.closeTime
      /*End Add Ons*/
      _allItemsFor.push(item);
    });
  });
  /* Pipeline: Group by Date */
  var _groupedItems = [];
  _.chain(_allItemsFor).groupBy('billDay').map(function (items) {
    _.chain(items).groupBy('id').map(function (gItemsById) {
      //console.log(gItemsById);

      var _itemTotalInEachTab = {
        _qty: 0,
        _amount: 0,
        discountedAmount:0,
        _tax: 0,
        _discount: 0,
        _taxes: [],
        _complementaryItemQty: 0,
        _nonComplementaryItemQty: 0,
        totalAmount:0
      };

      _.forEach(gItemsById, function (itemInTab) {
        delete itemInTab['itemNet']; // Remove old Item Net and Recalculate
        //console.log(itemInTab);
        if(itemInTab.billDiscountAmount == undefined || !itemInTab.billDiscountAmount)
          itemInTab.billDiscountAmount = 0;
        _itemTotalInEachTab._qty += parseFloat(itemInTab.quantity);
        _itemTotalInEachTab._complementaryItemQty += parseFloat(itemInTab.complementaryItemQty);
        _itemTotalInEachTab._nonComplementaryItemQty += parseFloat(itemInTab.nonComplementaryItemQty);
        _itemTotalInEachTab.totalAmount += parseFloat(itemInTab.subtotal) ;
       
        _itemTotalInEachTab._amount += parseFloat(itemInTab.subtotal) - (parseFloat(itemInTab.billDiscountAmount) + parseFloat(itemInTab.discount));

        _itemTotalInEachTab._discount += parseFloat(itemInTab.discount) + parseFloat(itemInTab.billDiscountAmount);
        /* if( _itemTotalInEachTab.totalAmount ==  _itemTotalInEachTab._discount)
          { console.log('insideIf')
            itemInTab.billDiscountAmount = 0;
           _itemTotalInEachTab._discount += parseFloat(itemInTab.discount) 
          }*/
        _itemTotalInEachTab.discountedAmount += (parseFloat(itemInTab.subtotal) - (parseFloat(itemInTab.discount)+ parseFloat(itemInTab.billDiscountAmount)))

        /* Aggregate Taxes */
        _.forEach(itemInTab.taxes, function (iTax) {
          //console.log(iTax.tax_amount);
          if (iTax.isConsidered) {
            _itemTotalInEachTab._tax += parseFloat(iTax.tax_amount);
          }

          /* Tax Subtotal Amount */
          _itemTotalInEachTab._taxes.push({
            id: iTax.id,
            name: iTax.name + ' Amount',
            tax_amount: (!_.isNaN(iTax.baseRate_preTax)) ? parseFloat(iTax.baseRate_preTax) : 0,
            isConsidered: false
          });
          /* Tax Amount */
          _itemTotalInEachTab._taxes.push({
            id: iTax.id,
            name: iTax.name,
            tax_amount: parseFloat(iTax.tax_amount),
            isConsidered: true
          });

          if (iTax.length > 0) {
            _.forEach(iTax.cascadingTaxes, function (cITax) {
              _itemTotalInEachTab._tax += parseFloat(cITax.tax_amount);
              /* Tax Subtotal Amount */
              _itemTotalInEachTab._taxes.push({
                id: iTax.id,
                name: iTax.name + ' Amount',
                tax_amount: parseFloat(iTax.baseRate_preTax),
                isConsidered: false
              });
              /* Tax Amount */
              _itemTotalInEachTab._taxes.push({
                id: iTax.id,
                name: iTax.name,
                tax_amount: parseFloat(iTax.tax_amount),
                isConsidered: true
              });
            });
          }

        });
      });

      var allItemTaxes = [];
      _.forEach(gItemsById, function (_it) {
        _.forEach(_it.taxes, function (_itax) {
          allItemTaxes.push(_itax);
        });
      });

      //console.log("ALL ITEMS:", gItemsById);
      var _t = [];
      _.chain(allItemTaxes).groupBy('id').map(function (_iTaxes) {
        //console.log("GTA " ,_iTaxes);
        var _tAmt = 0;
        _.forEach(_iTaxes, function (_t) {
          _tAmt += parseFloat(_t.tax_amount);
        });
        _t.push({
          id: _iTaxes[0].id,
          name: _iTaxes[0].name,
          isConsidered: _iTaxes[0].isConsidered,
          tax_amount: _tAmt
        })
      });


      var _net = 0;
      //_net = (parseFloat(_itemTotalInEachTab._amount) - parseFloat(_itemTotalInEachTab._discount)) + parseFloat(_itemTotalInEachTab._tax);
      /*Changes done by ranjeet to accomodate billwise discount*/
      _net = (parseFloat(_itemTotalInEachTab.totalAmount) - parseFloat(_itemTotalInEachTab._discount)) + parseFloat(_itemTotalInEachTab._tax);
      _groupedItems.push({
        id: gItemsById[0].id,
        name: gItemsById[0].name,
        tab: gItemsById[0].tab,
        billTime: gItemsById[0].billTime,
        billDay: gItemsById[0].billDay,
        billCloseTime: gItemsById[0].billCloseTime,
        category: gItemsById[0].category,
        superCategory: gItemsById[0].superCategory,
        quantity: _itemTotalInEachTab._qty,
        totalAmount: _itemTotalInEachTab._amount,
        subtotal:  Number(_itemTotalInEachTab.totalAmount.toFixed(2)),
        totalDiscount: _itemTotalInEachTab._discount,
        discountAmount: _itemTotalInEachTab.discountedAmount,
        totalTax: Number(_itemTotalInEachTab._tax.toFixed(2)),
        net: _net,
        roundOff: reportUtils.getRoundOff(_net),
        afterRoundOff: reportUtils.getAfterRoundOff(_net),
        taxes: _t,
        complementaryItemQty: _itemTotalInEachTab._complementaryItemQty,
        nonComplementaryItemQty: _itemTotalInEachTab._nonComplementaryItemQty
      });
    });
  });
  var csvRows = [];
  var grandTotal = {
    net: 0,
    roundOff: 0,
    afterRoundOff: 0,
    totalQty: 0,
    comp:0,
    nonComp:0,
    amount:0,
    totalTax:0,
    discount:0
  };

  var i = 0;
  csvRows.push(reportNameG)
  i = i+1
  // console.log(csvRows);
  var grpByCloseTime = _.groupBy(_groupedItems,function(bill){
    return bill.billCloseTime
  })
  // console.log(Nbills)
  // console.log(grpByCloseTime)
  _.forEach(grpByCloseTime, function (closeTime, key) {
    csvRows.push(key);
    i=i+1;
    var grpBySuperCategory = _.groupBy(grpByCloseTime[key],function(time){
      return time.superCategory;
    })
    // console.log(grpBySuperCategory)
    _.forEach(grpBySuperCategory, function (sItems , sc) {
      var superCategoryTotal = {
        net: 0,
        roundOff: 0,
        afterRoundOff: 0,
        totalQty: 0,
        comp:0,
        nonComp:0,
        amount:0,
        totalTax:0,
        discount:0
      };
      // csvRows.push('')
      csvRows.push('SuperCategory:'+sc);
      i = i+1;
      var cItems = _.groupBy(sItems,function(s){
        return s.category;
      })
        // console.log(cItems)
      _.forEach(cItems,function(cWise,cat){
        // console.log(cat);
        // console.log(cWise);
        csvRows.push('Category:'+cat);
        i = i+1;
        var category = {
          net: 0,
          roundOff: 0,
          afterRoundOff: 0,
          totalQty: 0,
          comp:0,
          nonComp:0,
          amount:0,
          discount:0,
          totalTax:0
        };
        if(!discountValue)
            csvRows.push('Item Name,Qty,Comp,Non Comp,Amount,Discount');
        else
          csvRows.push('Item Name,Qty,Comp,Non Comp,Amount');
        _.forEach(allTaxesFor,function(t){
            csvRows[i] = csvRows[i] + "," + t;
        })
        csvRows[i] = csvRows[i]+','+"Total Tax , Net Amount , Round Off , G.Total";
        i=i+1
        _.forEach(cWise,function(item,index){
          if(!discountValue)
            csvRows.push(item.name+','+item.quantity+','+item.complementaryItemQty+','+item.nonComplementaryItemQty+','+item.subtotal+','+item.totalDiscount);
          else
            csvRows.push(item.name+','+item.quantity+','+item.complementaryItemQty+','+item.nonComplementaryItemQty+','+(item.subtotal-item.totalDiscount));
          _.forEach(allTaxesFor,function(t,key){
            csvRows[i] = csvRows[i] + ','  + reportUtils.getTax(key, item).toFixed(2);
          })
          csvRows[i]=csvRows[i]+','+item.totalTax.toFixed(2) + ',' + item.net.toFixed(2) +','+item.roundOff.toFixed(2) +','+item.afterRoundOff.toFixed(2);
          i=i+1;

          if(!discountValue) {
            grandTotal.amount += item.subtotal;
            grandTotal.discount += item.totalDiscount; 
            superCategoryTotal.amount += item.subtotal;
            superCategoryTotal.discount += item.totalDiscount;
            category.amount += item.subtotal;
            category.discount += item.totalDiscount;
          } else {
            grandTotal.amount += item.subtotal - item.totalDiscount
            superCategoryTotal.amount += item.subtotal - item.totalDiscount
            category.amount += item.subtotal - item.totalDiscount
          }
          grandTotal.net += Number(item.net.toFixed(2));
          grandTotal.roundOff += Number(item.roundOff.toFixed(2));
          grandTotal.afterRoundOff += Number(item.afterRoundOff.toFixed(2));
          grandTotal.totalQty += item.quantity;
          grandTotal.comp += item.complementaryItemQty;
          grandTotal.nonComp += item.nonComplementaryItemQty;
          grandTotal.totalTax += Number(item.totalTax.toFixed(2));
          superCategoryTotal.net += Number(item.net.toFixed(2));
          superCategoryTotal.roundOff += Number(item.roundOff.toFixed(2));
          superCategoryTotal.afterRoundOff += Number(item.afterRoundOff.toFixed(2));
          superCategoryTotal.totalQty += item.quantity;
          superCategoryTotal.comp += item.complementaryItemQty;
          superCategoryTotal.nonComp += item.nonComplementaryItemQty;
          superCategoryTotal.totalTax += Number(item.totalTax.toFixed(2));
          category.net += Number(item.net.toFixed(2));
          category.roundOff += Number(item.roundOff.toFixed(2));
          category.afterRoundOff += Number(item.afterRoundOff.toFixed(2));
          category.totalQty += item.quantity;
          category.comp += item.complementaryItemQty;
          category.nonComp += item.nonComplementaryItemQty;
          category.totalTax += Number(item.totalTax.toFixed(2));
        })
        if(!discountValue) {
          csvRows.push('Category Total,'+category.totalQty+','+category.comp+','+category.nonComp+','+category.amount+','+category.discount);
        } else {
          csvRows.push('Category Total,'+category.totalQty+','+category.comp+','+category.nonComp+','+category.amount);
        }
        _.forEach(allTaxesFor,function(t,key){
          csvRows[i] = csvRows[i] + ','  + reportUtils.getTotalTaxRows(key,cWise).toFixed(2);
        })
        csvRows[i]=csvRows[i]+','+category.totalTax.toFixed(2) + ',' + category.net.toFixed(2) +','+category.roundOff.toFixed(2) +','+category.afterRoundOff.toFixed(2);
        i=i+1;
      })
      if(!discountValue) {
        csvRows.push('SuperCategory Total,'+superCategoryTotal.totalQty+','+superCategoryTotal.comp+','+superCategoryTotal.nonComp+','+superCategoryTotal.amount+','+superCategoryTotal.discount); 
      } else {
        csvRows.push('SuperCategory Total,'+superCategoryTotal.totalQty+','+superCategoryTotal.comp+','+superCategoryTotal.nonComp+','+superCategoryTotal.amount); 
      }
      _.forEach(allTaxesFor,function(t,key){
        csvRows[i] = csvRows[i] + ','  + reportUtils.getTotalTaxRows(key,sItems).toFixed(2);
      })
      csvRows[i]=csvRows[i]+','+superCategoryTotal.totalTax.toFixed(2) + ',' + superCategoryTotal.net.toFixed(2) +','+superCategoryTotal.roundOff.toFixed(2) +','+superCategoryTotal.afterRoundOff.toFixed(2);
      i=i+1;
    });
    csvRows.push('')
    i=i+1
  });
  csvRows.push('')
  i=i+1
  if(!discountValue){
    csvRows.push('Grand Total,'+grandTotal.totalQty+','+grandTotal.comp+','+grandTotal.nonComp+','+grandTotal.amount+','+grandTotal.discount);  
  } else {
    csvRows.push('Grand Total,'+grandTotal.totalQty+','+grandTotal.comp+','+grandTotal.nonComp+','+grandTotal.amount);
  }
  
  _.forEach(allTaxesFor,function(t,key){
    csvRows[i] = csvRows[i] + ','  + reportUtils.getTotalTaxRows(key,_groupedItems).toFixed(2);
  })
  csvRows[i]=csvRows[i]+','+grandTotal.totalTax.toFixed(2) + ',' + grandTotal.net.toFixed(2) +','+grandTotal.roundOff.toFixed(2) +','+grandTotal.afterRoundOff.toFixed(2);

  var csvString = csvRows.join("\r\n");
  // console.log(csvString)
  //var Key = new Buffer(reportNameG+':'+deployment).toString('base64');
  // detail.reportType = reportNameG
  // var _filename = reportNameG+'_'+(type2||'')+'('+moment(detail.fromDate).format('YYYY.MM.DD')+'--'+moment(detail.toDate).format('YYYY.MM.DD')+')'+Key+'.csv'
  //var _p = path.join(process.cwd(), 'uploads', '_filename.csv');
  // detail.filename = _filename
  // var exportButton = document.createElement('a');
  fs.writeFile(_p, csvString, function(err) {
    if(err) {
        return console.log(err);
    }
  })
  exportToCsv(csvString)
}
var renderDaySalesReport = function (bills, sessions, categories) {
  // console.log(sessions)
  var grpBySession = {};
  var billDiscountAmount = 0;
  var disc = 0;
  var amt = 0;
  _.forEach(bills, function (bill) {
    //if(!bill.isVoid) {
    var includeBill = true;
    if(bill.isVoid)
      includeBill = false;
    var _cutOffTime = moment(moment(cutOffTimeSettings));
    var _cutOffTimeMeridian = moment(moment(cutOffTimeSettings)).format('a');
    var _cutOffTimeHour = parseInt(moment(moment(_cutOffTime)).format('H'));
    var _cutOffTimeMin = parseInt(moment(moment(_cutOffTime)).format('mm'));

    var _billOpenTime = moment(moment(bill._created));
    var _billCloseTime = moment(moment(bill._closeTime));

    var _billOpenTimeStartOfDay = moment(moment(bill._created)).startOf('day');
    var _billOpenTimeEnfOfDay = moment(moment(bill._created)).endOf('day');
    var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour, 'hours').add(_cutOffTimeMin, 'minutes');
    //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

    if (_cutOffTimeMeridian === 'am') {
      if (_billOpenTime>_billOpenTimeOffsetCutOff && _billOpenTime<_billOpenTimeEnfOfDay) {
        bill.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
      }

      if (_billOpenTime>_billOpenTimeStartOfDay && _billOpenTime<_billOpenTimeOffsetCutOff) {
        bill.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
      }
    }
    //*!* Changing for groupBy to work *!/*/
    bill.created = moment(bill._created).format('YYYY-MM-DD');
    //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

    ///!* Create Faux Date Hack *!/
    if (bill.fauxCloseTime != null) {
      bill.closeTime = bill.fauxCloseTime;
    } else {
      if (_cutOffTimeMeridian != 'am') {
        bill.closeTime = bill.created;
      }
    }
    _.forEach(sessions, function (session) {
      //console.log(session);
      var billHour = (new Date(bill._created)).getHours();
      var billMinutes = (new Date(bill._created)).getMinutes();
      var sessionFromHour = (new Date(session.fromTime)).getHours();
      var sessionFromMinutes = (new Date(session.fromTime)).getMinutes();
      var sessionToHour = (new Date(session.toTime)).getHours();
      var sessionToMinutes = (new Date(session.toTime)).getMinutes();
      var date = new Date(bill._created);

      //console.log(billHour);
      //console.log(billMinutes);
      //console.log(sessionFromHour);
      //console.log(sessionFromMinutes);
      //console.log(sessionToHour);
      //console.log(sessionToMinutes);
      //console.log(billHour <= sessionToHour && billHour >= sessionFromHour && billMinutes <= sessionToMinutes && billMinutes >= sessionFromMinutes);
      //console.log(billHour <= sessionFromHour && billHour >= sessionToHour)

      //console.log(sessionFromHour < sessionToHour);
      //if(billHour >= sessionFromHour && billHour <= sessionToHour){
      //  if (sessionFromHour < sessionToHour) {
      //    sessionFromHour < session;
      //  } else if (sessionFromHour == sessionToHour) {
      //    if (billMinutes <= sessionToMinutes && billMinutes >= sessionFromMinutes)
      //      bill.session = session;
      //  }
      //} else if ()
      if (sessionFromHour < sessionToHour) {
        var sessionFromDate = new Date(bill.closeTime);
        var sessionToDate = new Date(bill.closeTime);
        sessionFromDate.setHours(sessionFromHour, sessionFromMinutes, 0, 0);
        sessionToDate.setHours(sessionToHour, sessionToMinutes, 0, 0);
        if (new Date(bill._created).getTime() >= new Date(sessionFromDate).getTime() && new Date(bill._created).getTime() <= new Date(sessionToDate).getTime()) {
          bill.session = session;
        }
      }
      //else if (sessionFromHour == sessionToHour) {
      //  if (billHour >= sessionFromHour && billHour <= sessionToHour) {
      //    if (billMinutes <= sessionToMinutes && billMinutes >= sessionFromMinutes)
      //      bill.session = session;
      //  }
      else if (sessionFromHour > sessionToHour) {
        var sessionFromDate = new Date(bill.closeTime);
        var sessionToDate = new Date(bill.closeTime);
        sessionFromDate.setHours(sessionFromHour, sessionFromMinutes, 0, 0);
        sessionToDate.setHours(sessionToHour, sessionToMinutes, 0, 0);
        sessionToDate = new Date((new Date(sessionToDate).setDate((new Date(sessionToDate).getDate() + 1))))
        //console.log(sessionToDate, sessionFromDate);
        if (new Date(bill._created).getTime() < sessionToDate.getTime() && new Date(bill._created).getTime() > sessionFromDate.getTime()) {
          bill.session = session;
        }
      }
    });
    //console.log(bill.session);
    //var includeBill = true;
    //var isItemPresent = false;
    if (!bill.session){
      //includeBill = false;
      bill.session = {sessionName: "NA"};
    }
    //if (includeBill) {
    //console.log(bill DiscountAmount);
    if (!grpBySession[bill.session.sessionName])
      grpBySession[bill.session.sessionName] = {
        covers: 0,
        numBills: 0
      };
    // var totalItemsInBills = 0;
    // _.forEach(bill._kots, function (kot) {
    //   if(!kot.isVoid){
    //   //console.log(kot.totalItems);
    //     _.forEach(kot.items, function (item) {
    //       totalItemsInBills += 1;
    //       if(item.addOns && Array.isArray(item.addOns)){
    //         totalItemsInBills += item.addOns.length;
    //       }
    //     });
    //   }
    // });
    //console.log(billDiscountAmount, totalItemsInBills);
    //var discToSubtract = bill.billDiscountAmount / totalItemsInBills;
    //console.log(bill.billDiscountAmount, totalItemsInBills, discToSubtract);
    _.forEach(bill._kots, function (kot) {
      if (!kot.isVoid && includeBill) {
        _.forEach(kot.items, function (item) {
          var includeItem = true;
          if (!item.category) {
            item.category = {categoryName: "NA", section: {name: "NA"}}
            includeItem = false;
          }
          else {
            var cat = _.find(categories, function (category) {
              return category._id == item.category._id
            });
            var section = null;
            if (cat) {
              section = cat.section;
            }

            if (section) {
              item.category.section = section;
            } else {
              //includeItem = false;
              item.category.section = {name: "NA"};
            }
          }
          //if (includeItem) {
          //isItemPresent = true;
          if (!grpBySession[bill.session.sessionName][item.category.section.name])
            grpBySession[bill.session.sessionName][item.category.section.name] = {
              amount: 0,
              discount: 0,
              covers: 0
            };
          grpBySession[bill.session.sessionName][item.category.section.name].amount += Utils.roundNumber(item.subtotal - (item.billDiscountAmount?item.billDiscountAmount: 0) , 2);
          amt += item.subtotal;
          disc += item.billDiscountAmount? item.billDiscountAmount: 0;

          //console.log('itemAmoun', Utils.roundNumber(item.subtotal - discToSubtract, 2));
          _.forEach(item.discounts, function (discount) {
            grpBySession[bill.session.sessionName][item.category.section.name].discount += Utils.roundNumber(discount.discountAmount, 2);
            disc += discount.discountAmount;
            //billDiscountAmount += Utils.roundNumber(discount.discountAmount, 2)
            //console.log(grpBySession[bill.session.sessionName][item.category.section.name].discount);
          });
          //}
          _.forEach(item.addOns, function (addOn) {
            //var includeAddOn = true;
            if (!addOn.category) {
              addOn.category = {categoryName: "NA", section: {name: "NA"}}
            }

            if (!addOn.category.section) {
              addOn.category.section = {name: "NA"};
              //includeAddOn = false;
            }

            //if (includeAddOn) {
            //isItemPresent = true;
            if (!grpBySession[bill.session.sessionName][addOn.category.section.name])
              grpBySession[bill.session.sessionName][addOn.category.section.name] = {
                amount: 0,
                discount: 0,
                covers: 0
              };
            //console.log('addOnAmount', Utils.roundNumber(addOn.subtotal - discToSubtract, 2));
            grpBySession[bill.session.sessionName][addOn.category.section.name].amount += Utils.roundNumber(addOn.subtotal - (addOn.billDiscountAmount? addOn.billDiscountAmount: 0), 2);
            //console.log('addOnAmoun', Utils.roundNumber(addOn.subtotal - discToSubtract, 2));
            _.forEach(addOn.discounts, function (discount) {
              grpBySession[bill.session.sessionName][addOn.category.section.name].discount += Utils.roundNumber(discount.discountAmount, 2);
              //billDiscountAmount += Utils.roundNumber(discount.discountAmount, 2)
            });
            //}
          });
        });
      } else if(kot.isVoid && includeBill){
        _.forEach(kot.items, function (item) {
          var includeItem = true;
          if (!item.category) {
            item.category = {categoryName: "NA", section: {name: "NA"}}
            includeItem = false;
          }
          else {
            var cat = _.find(categories, function (category) {
              return category._id == item.category._id
            });
            var section = null;
            if (cat) {
              section = cat.section;
            }

            if (section) {
              item.category.section = section;
            } else {
              //includeItem = false;
              item.category.section = {name: "NA"};
            }
          }
          //if (includeItem) {
          //isItemPresent = true;
          if (!grpBySession[bill.session.sessionName][item.category.section.name])
            grpBySession[bill.session.sessionName][item.category.section.name] = {
              amount: 0,
              discount: 0,
              covers: 0
            };
          grpBySession[bill.session.sessionName][item.category.section.name].amount -= Utils.roundNumber((item.billDiscountAmount?item.billDiscountAmount: 0) , 2);
          amt += item.subtotal;
          disc += item.billDiscountAmount? item.billDiscountAmount: 0;
        });
      }
    });
    //if (isItemPresent)
    grpBySession[bill.session.sessionName].covers += bill._covers ? bill._covers : 0;
    grpBySession[bill.session.sessionName].numBills += 1;
    //}
    //}
  });
  var html = reportNameG+"\n\r\n\r";
  html += ""
  html += "";
  html += "Daily Sales Report,,\r\n";
  html += "";
  html += "Session,";
  html += "Time Slot,";
  html += "Sale Type,";
  html += "Amount,";
  html += "\r\n";
  var grandTotal = {
    amount: 0,
    discount: 0,
    covers: 0,
    numBills: 0
  };
  var sectionGT = {};
  _.forEach(grpBySession, function (sections, sessionName) {
    var session = _.find(sessions, function (session) {
      return session.sessionName == sessionName;
    });
    var sectionTotal = {
      amount: 0,
      discount: 0,
      covers: 0,
      numBills: 0
    };
    //console.log(sections);
    _.forEach(sections, function (section, sectionName) {
      //console.log(section);
      if (sectionName != "covers" && sectionName != "numBills") {
        html += "";
        html += "" + sessionName + ",";
        if (session)
          html += "" + moment(session.fromTime).format('hh:mm a') + "   -   " + moment(session.toTime).format('hh:mm a') + ",";
        else
          html += "NA,";
        html += "" + sectionName + ",";
        html += "" + Utils.roundNumber(section.amount - section.discount, 2) + ",";
        html += "\r\n";

        sectionTotal.amount += Utils.roundNumber(section.amount, 2);
        sectionTotal.discount += Utils.roundNumber(section.discount, 2);

        grandTotal.amount += Utils.roundNumber(section.amount, 2);
        grandTotal.discount += Utils.roundNumber(section.discount, 2);
        if (!sectionGT[sectionName])
          sectionGT[sectionName] = 0;
        sectionGT[sectionName] += Utils.roundNumber((section.amount - section.discount), 2);
      }
    });
    sectionTotal.covers += sections['covers'];
    sectionTotal.numBills += sections['numBills'];
    grandTotal.covers += sections['covers'];
    grandTotal.numBills += sections['numBills'];
    var tapc = sectionTotal.covers ? Utils.roundNumber((sectionTotal.amount - sectionTotal.discount) / sectionTotal.covers, 2) : 0;
    html += "";
    html += "Total Covers (" + sectionTotal.covers + ") / Total Bills (" + sectionTotal.numBills + "),";
    html += "Total APC (" + tapc + "),";
    html += "Total Sales,";
    html += "" + Utils.roundNumber(sectionTotal.amount - sectionTotal.discount, 2) + ",";
    html += "\r\n";
  });
  html += "\r\n\r\n";
  html += ",";
  html += "";
  html += "";
  html += "Grand Total\r\n"
  _.forEach(sectionGT, function (amount, section) {
    html += "";
    html += "Total " + section + " Sale,";
    html += "" + Utils.roundNumber(amount, 2) + ",";
    html += "\r\n";
  });

  // html += "";
  // html += "Total Discount,";
  // html += "" + Utils.roundNumber(billDiscountAmount, 2) + ",";
  // html += "\r\n";

  html += "";
  html += "Total Sale,";
  html += "" + Utils.roundNumber(grandTotal.amount - grandTotal.discount, 2) + ",";
  html += "\r\n";

  html += "";
  html += "Total Bills,";
  html += "" + (grandTotal.numBills) + ",";
  html += "\r\n";

  html += "";
  html += "Total Covers,";
  html += "" + (grandTotal.covers) + ",";
  html += "\r\n";

  html += "";
  html += "Total APC,";
  html += "" + (grandTotal.covers ? Utils.roundNumber((grandTotal.amount - grandTotal.discount) / grandTotal.covers, 2) : 0) + ",";
  html += "\r\n";

  html += "";
  html += ",";
  exportToCsv(html)
};
var dynamicBaggaryHTML = function(bills, dep) {
  var html = reportNameG+"\n\r\n\r"+"Bill Number,"
  html += 'Created,'
  html += 'Deployemnt,'
  html += 'MPXT,'
  html += 'Item Name,'
  html += '+/-,'
  html += 'Quantity,'
  html += 'Rate,'
  html += 'Amount,'
  html += 'TAX,'
  html += 'Taxable Amount,\r\n'
  
  bills.forEach(function(bill){
    var totalTax = 0
    var gt = 0
    if(!bill.isVoid){
      // _.forEach(bill._items,function(item){
      //   gt+= 
      // })
      if(resetDaily){
        html += ""
                +((bill.prefix||'') + " " + bill.daySerialNumber)+",\""
                +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                +dep+","
                +"M,"
                +","
                +"+,"
                +","
                +","
                +(bill.aggregate.amount-bill.aggregate.discount+bill.aggregate.totalTax).toFixed(2)+","
                +","
                +",\r\n";
      }else {
        html += ""
                +((bill.prefix||'') + " " + bill.serialNumber)+",\""
                +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                +dep+","
                +"M,"
                +","
                +"+,"
                +","
                +","
                +(bill.aggregate.amount-bill.aggregate.discount+bill.aggregate.totalTax).toFixed(2)+","
                +","
                +",\r\n";
      }
      _.forEach(bill._items,function(items){
        _.forEach(items, function(item){
          totalTax += item.taxAmount
          var taxTItem = 0;
          _.forEach(item.taxes, function(tax) {
            if(tax.isConsidered == true && tax.tax_amount != 0) {
              taxTItem += tax.value  
            }
          })
          if(resetDaily){
            html += ""
                    +((bill.prefix||'') + " " + bill.daySerialNumber)+",\""
                    +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                    +","
                    +"P,"
                    +item.name+","
                    +"+,"
                    +item.qty+","
                    +item.rate+","
                    +(item.amount-item.discount).toFixed(2)+","
                    +","
                    +",\r\n";

            html += ""
                    +((bill.prefix||'') + " " + bill.daySerialNumber)+",\""
                    +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                    +","
                    +"X,"
                    +","
                    +"-,"
                    +","
                    +","
                    +item.taxAmount.toFixed(2)+","
                    +"Tax@"+taxTItem+","
                    +(item.amount-item.discount).toFixed(2)+",\r\n";
          } else {
            html += ""
                    +(bill.prefix + " " + bill.serialNumber)+",\""
                    +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                    +","
                    +"P,"
                    +item.name+","
                    +"+,"
                    +item.qty+","
                    +item.rate+","
                    +(item.amount-item.discount).toFixed(2)+","
                    +","
                    +",\r\n";

            html += ""
                    +((bill.prefix||'') + " " + bill.serialNumber)+",\""
                    +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                    +","
                    +"X,"
                    +","
                    +"-,"
                    +","
                    +","
                    +item.taxAmount.toFixed(2)+","
                    +"Tax@"+taxTItem+","
                    +(item.amount-item.discount).toFixed(2)+",\r\n";
          }
        })
      })
      if(resetDaily){
        html += ""
                +((bill.prefix||'') + " " + bill.daySerialNumber)+",\""
                +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                +","
                +"T,"
                +","
                +"+,"
                +","
                +","
                +totalTax.toFixed(2)+","
                +","
                +",\r\n";
      }else {
        html += ""
                +((bill.prefix||'') + " " + bill.serialNumber)+",\""
                +moment(bill._created).format('MMMM Do YYYY, h:mm:ss a')+"\","
                +","
                +"T,"
                +","
                +"+,"
                +","
                +","
                +totalTax.toFixed(2)+","
                +","
                +",\r\n";
      }
    }
  })
  html+=''
  exportToCsv(html)
};
function renderDynamicSettlementDetailsReport(data) {
  var html = reportNameG+"\n\r\n\r";
  data = _.sortBy(data,function(d){
    return d.serialNumber
  })
  var groupBillsByPaymentMode = _.groupBy(data, function (bill) {
    return bill.name;
  });
  _.forEach(groupBillsByPaymentMode, function (bills, mode) {
    var gt = {
      amount: 0,
    };
    html += "";
    html += "" + mode + "";
    html += "";
    html += "";
    html += "Date,"
    html += "Bill #,";
    html += "Amount,";
    html += "Bank Name,";
    html += "Bank Type,"
    html += "\r\n";
    html += ""
    var grpByDate = _.groupBy(bills,'closeTime')

    _.forEach(grpByDate, function (bills, date) {
      
      var tot = {
        amount: 0,
       
      }
      _.forEach(bills, function (bill) {
        html += "";
         html += "" + bill.closeTime + ",";
        html += "" + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + ",";

        html += "" + Utils.roundNumber(bill.amount ,2) + ",";
        html += "" + bill.bankName + ",";
        html += "" + bill.bankType + ",";

        html += "\r\n";
        tot.amount += parseFloat(Utils.roundNumber(bill.amount,2));
        //tot.onlinePaymentAmount += bill.onlinePayment;
        gt.amount += parseFloat(Utils.roundNumber(bill.amount,2));
        //gt.onlinePaymentAmount += bill.onlinePayment;
      });
      html += ""
      html += "Day Total,";
      html+=",";
      html += "" + Utils.roundNumber(tot.amount, 2) + ",";
      html+=",";
      html+=",";
      //html += "" + Utils.roundNumber(tot.onlinePaymentAmount, 2) + ",";
      html += "\r\n";
    });
    html += "";
    html += "Grand Total,";
     html+=",";
    html += "" + Utils.roundNumber(gt.amount, 2) + ",";
     html+=",";
      html+=",";
    //html += "" + Utils.roundNumber(gt.onlinePaymentAmount, 2) + ",";
    html += "\r\n";
    html += "";
  });
  exportToCsv(html)
};
function renderDynamicSettlementConsolidatedReport(data) {
  var html = reportNameG+"\n\r\n\r";
  var grpByUnique = _.groupBy(data,function(bill){
  bill.unique = bill.bankName + bill.name + bill.bankType
  return bill.unique
  })

  html += "";
  html += "#,";
  html += "Source,";
  html += "Bank Name,";
  html += "Bank Type,";
  html += "Amount,";
  html += "Number of Bills,\r\n";
  var total = {};
  var i = 0;
  _.forEach(grpByUnique,function(data,key){
    var numBills = data.length
  _.forEach(data,function(bill){
    if(total[bill.unique]==undefined)
      total[bill.unique] = 0;
    total[bill.unique] +=parseFloat(Utils.roundNumber(bill.amount,2))
  })
   i=i+1;
    //html += "" + Total.qty + "," + Total.amt + ",";
    html += "" + i + ",";
    html += "" + data[0].name + ",";
    html += "" + data[0].bankName + ",";
    html += "" + data[0].bankType + ",";

    html += "" + Utils.roundNumber(total[key], 2) + ",";

    html += "" + numBills + ",";
    html += "\r\n";
  })


  //   }
  //=====================Total for Each Deployment Date Wise==========================
  /*html += "Grand Total:,";
   html += "" + gt.q + "," + gt.a + ",,\r\n";*/
  //=======================================================================================

  html += "";
  exportToCsv(html)
};
var renderDynamicCouponDetailsReport = function(data){
  var html = reportNameG+"\n\r\n\r";
  var groupBillsByPaymentMode = _.groupBy(data, function (bill) {
    return bill.otherName;
  });
   var gt = {
      codAmount: 0,
      
    };
  _.forEach(groupBillsByPaymentMode, function (bills, mode) {
   
    html += "";
    html += "" + mode + "";
    html += "";
    html += "";
     html += "Date,";
    html += "Bill #,";
    html += "Amount,";
    
    html += "\r\n";
    html += ""
    var grpByDate = _.groupBy(bills, function (bill) {
      return bill.closeTime;
    });
    _.forEach(grpByDate, function (bills, date) {
     
      var tot = {
        codAmount: 0,
        
      }
      _.forEach(bills, function (bill) {
        html += "";
        html += "" + bill.closeTime + ",";

        html += "" + (bill.prefix!=undefined?bill.prefix:'') + bill.serialNumber + ",";

        html += "" + Utils.roundNumber(bill.amount, 2) + ",";
       // html += "" + Utils.roundNumber(bill.onlinePayment, 2) + ",";

        html += "\r\n";
        tot.codAmount += parseFloat(Utils.roundNumber(bill.amount,2));
       // tot.onlinePaymentAmount += bill.onlinePayment;
        gt.codAmount += parseFloat(Utils.roundNumber(bill.amount,2));
        //gt.onlinePaymentAmount += bill.onlinePayment;
      });
        html += ""
        html += "Day Total,";
        html += ",";
        html += "" + Utils.roundNumber(tot.codAmount, 2) + ",";
       // html += "" + Utils.roundNumber(tot.onlinePaymentAmount, 2) + ",";
        html += "\r\n";
    });
  });
  html += "";
  html += "Grand Total,";
  html += ",";
  html += "" + Utils.roundNumber(gt.codAmount, 2) + ",";
 
  html += "\r\n";
  html += "";
  exportToCsv(html)
};
function  renderDynamicCouponConsolidateReport(data) {
  var html = reportNameG+"\n\r\n\r";
  var grpByUnique = _.groupBy(data,function(bill){
  //bill.unique = bill.bankName + bill.name + bill.bankType
    return bill.otherName
  })

  html += "";
  html += "#,";
 // html += "Source,";
  html += "Coupon Name,";
  
  html += "Amount,";
  html += "Number of Bills,\r\n";
  var total = {};
  var i = 0;
   _.forEach(grpByUnique,function(data,key){
    var numBills = data.length
    _.forEach(data,function(bill){
      if(total[bill.otherName]==undefined)
        total[bill.otherName] = 0;
      total[bill.otherName] +=parseFloat(Utils.roundNumber(bill.amount,2))
    })
    i=i+1;
    //html += "" + Total.qty + "," + Total.amt + ",";
    html += "" + i + ",";
    html += "" + data[0].otherName + ",";
   

    html += "" + Utils.roundNumber(total[key], 2) + ",";

    html += "" + numBills + ",";
    html += "\r\n";
  })


  //   }
  //=====================Total for Each Deployment Date Wise==========================
  /*html += "Grand Total:,";
   html += "" + gt.q + "," + gt.a + ",,\r\n";*/
  //=======================================================================================
  html += "";
  exportToCsv(html)
};
function renderDynamicNewOtherPaymentDetailsReport(data) {
  // console.log(data);
  var html = reportNameG+"\n\r\n\r";
  var groupBillsByPaymentMode = _.groupBy(data, function (bill) {
    return bill.otherName;
  });
  _.forEach(groupBillsByPaymentMode, function (bills, mode) {
    var gt = {
      Amount: 0,
     // onlinePaymentAmount: 0
    };
    html += "";
    html += "\r\n" + mode + "\r\n";
    html += "";
    html += "";
    html += "Bill #,";
    html += "Amount,";
   // html += "Online Payment,"
    html += "\r\n";
    html += ""
    var grpByDate = _.groupBy(bills,'closeTime');
    _.forEach(grpByDate, function (bills, date) {
      html += ""
      html += "" + date + ",";
      html += "\r\n";
      var tot = {
        Amount: 0,
        //onlinePaymentAmount: 0
      }
      _.forEach(bills, function (bill) {
        html += "";
        html += "" + (bill.prefix!=undefined?bill.prefix:'')+bill.serialNumber + ",";

        html += "" + Utils.roundNumber(bill.amount, 2) + ",";
        //html += "" + Utils.roundNumber(bill.onlinePayment, 2) + ",";

        html += "\r\n";
        tot.Amount += parseFloat(Utils.roundNumber(bill.amount,2));
       // tot.onlinePaymentAmount += bill.onlinePayment;
        gt.Amount +=  parseFloat(Utils.roundNumber(bill.amount,2));
        //gt.onlinePaymentAmount += bill.onlinePayment;
      });
      html += ""
      html += "Day Total,";
      html += "" + Utils.roundNumber(tot.Amount, 2) + ",";
      //html += "" + Utils.roundNumber(tot.onlinePaymentAmount, 2) + ",";
      html += "\r\n";
    });
    html += "";
    html += "Grand Total,";
    html += "" + Utils.roundNumber(gt.Amount, 2) + ",";
    //html += "" + Utils.roundNumber(gt.onlinePaymentAmount, 2) + ",";
    html += "\r\n";
    html += "";
  });
  exportToCsv(html)
};
function renderDynamicNewOtherPaymentConsolidatedReport(data) {
  var html = reportNameG+"\n\r\n\r";
  var grpByUnique = _.groupBy(data,function(bill){
   //bill.unique = bill.bankName + bill.name + bill.bankType
   return bill.otherName
  })

  html += "";
  html += "#,";
 // html += "Source,";
  html += "Other Name,";
  
  html += "Amount,";
  html += "Number of Bills,\r\n";
  var total = {};
  var i = 0;

 _.forEach(grpByUnique,function(data,key){
  var numBills = data.length
  _.forEach(data,function(bill){
    if(total[bill.otherName]==undefined)
      total[bill.otherName] = 0;
    total[bill.otherName] +=parseFloat(Utils.roundNumber(bill.amount,2))
  })
   i=i+1;
    //html += "" + Total.qty + "," + Total.amt + ",";
    html += "" + i + ",";
    html += "" + data[0].otherName + ",";
   

    html += "" + Utils.roundNumber(total[key], 2) + ",";

    html += "" + numBills + ",";
    html += "\r\n";
  })
  html += "";
  exportToCsv(html)
};
var renderDaySalesReportForConsolidate = function(bills, sessions, categories){
  var grpBySession = {};
  var billDiscountAmount = 0;
  var disc = 0;
  var amt = 0;
  var allTaxes=[];
  var voidDetails = {
    complimentary:0,
    void:0,
    discount:0
  }

  _.forEach(bills, function (bill) {
    //if(!bill.isVoid) {
   /* var includeBill = true;
    if(bill.isVoid)
      includeBill = false;*/
    var _cutOffTime = moment(moment(cutOffTimeSettings));
    var _cutOffTimeMeridian = moment(moment(cutOffTimeSettings)).format('a');
    var _cutOffTimeHour = parseInt(moment(moment(_cutOffTime)).format('H'));
    var _cutOffTimeMin = parseInt(moment(moment(_cutOffTime)).format('mm'));

    var _billOpenTime = moment(moment(bill._created));
    var _billCloseTime = moment(moment(bill._closeTime));

    var _billOpenTimeStartOfDay = moment(moment(bill._created)).startOf('day');
    var _billOpenTimeEnfOfDay = moment(moment(bill._created)).endOf('day');
    var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour, 'hours').add(_cutOffTimeMin, 'minutes');
    //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

    if (_cutOffTimeMeridian === 'am') {
      if (_billOpenTime>_billOpenTimeOffsetCutOff && _billOpenTime<_billOpenTimeEnfOfDay) {
        bill.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
      }

      if (_billOpenTime>_billOpenTimeStartOfDay && _billOpenTime<_billOpenTimeOffsetCutOff) {
        bill.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
      }
    }
    //*!* Changing for groupBy to work *!/*/
    bill.created = moment(bill._created).format('YYYY-MM-DD');
    //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

    ///!* Create Faux Date Hack *!/
    if (bill.fauxCloseTime != null) {
      bill.closeTime = bill.fauxCloseTime;
    } else {
      if (_cutOffTimeMeridian != 'am') {
        bill.closeTime = bill.created;
      }
    }
  
    // var totalItemsInBills = 0;
    // _.forEach(bill._kots, function (kot) {
    //   if(!kot.isVoid){
    //   //console.log(kot.totalItems);
    //     _.forEach(kot.items, function (item) {
    //       totalItemsInBills += 1;
    //       if(item.addOns && Array.isArray(item.addOns)){
    //         totalItemsInBills += item.addOns.length;
    //       }
    //     });
    //   }
    // });
    //console.log(billDiscountAmount, totalItemsInBills);
    //var discToSubtract = bill.billDiscountAmount / totalItemsInBills;
    //console.log(bill.billDiscountAmount, totalItemsInBills, discToSubtract);
    _.forEach(bill._kots, function (kot) {
        if(_.has(kot,"deleteHistory"))
        {
          // console.log('inside first if')
         if( kot.deleteHistory.length>0)
         {
            // console.log('inside second if')
           // console.log(kot.deleteHistory);
            _.forEach(kot.deleteHistory,function(item){
            // console.log(item.subtotal)
             if (!item.category) {
              item.category = {categoryName: "NA", section: {name: "NA"}}
              includeItem = false;
             }
              else {
              var cat = _.find(categories, function (category) {
                return category._id == item.category._id
              });
              var section = null;
              if (cat) {
                section = cat.section;
              }

              if (section) {
                item.category.section = section;
              } else {
                //includeItem = false;
                item.category.section = {name: "NA"};
              }
            }
            //if (includeItem) {
            //isItemPresent = true;
            if (!grpBySession[item.category.section.name])
              grpBySession[item.category.section.name] = {
                amount: 0,
                discount: 0,
                //taxes:[],
                //totalTaxes:0,
                
              };
            grpBySession[item.category.section.name].amount += Utils.roundNumber(item.subtotal,2);
            voidDetails.void += Utils.roundNumber(item.subtotal,2);
              
            if(_.has(item,'addOns')) {
              _.forEach(item.addOns,function(addon){
                if (!addon.category) {
                    addon.category = {categoryName: "NA", section: {name: "NA"}}
                    includeItem = false;
                  }
                  else {
                    var cat = _.find(categories, function (category) {
                      return category._id == item.category._id
                    });
                    var section = null;
                    if (cat) {
                      section = cat.section;
                    }

                    if (section) {
                      addon.category.section = section;
                    } else {
                      //includeItem = false;
                      addon.category.section = {name: "NA"};
                    }
                  }
                  //if (includeItem) {
                  //isItemPresent = true;
                  if (!grpBySession[addon.category.section.name])
                    grpBySession[addon.category.section.name] = {
                      amount: 0,
                      discount: 0,
                      //taxes:[],
                      //totalTaxes:0,
                      
                    };
                  grpBySession[addon.category.section.name].amount += Utils.roundNumber(addOn.subtotal,2);
                voidDetails.void += Utils.roundNumber(addOn.subtotal,2);
              })
            }

            })
         }
         
        }
        _.forEach(kot.items, function (item) {
          var includeItem = true;
          if (!item.category) {
            item.category = {categoryName: "NA", section: {name: "NA"}}
            includeItem = false;
          }
          else {
            var cat = _.find(categories, function (category) {
              return category._id == item.category._id
            });
            var section = null;
            if (cat) {
              section = cat.section;
            }

            if (section) {
              item.category.section = section;
            } else {
              //includeItem = false;
              item.category.section = {name: "NA"};
            }
          }
          //if (includeItem) {
          //isItemPresent = true;
          if (!grpBySession[item.category.section.name])
            grpBySession[item.category.section.name] = {
              amount: 0,
              discount: 0,
              //taxes:[],
              //totalTaxes:0,
              
            };
          grpBySession[item.category.section.name].amount += Utils.roundNumber(item.subtotal,2);
          amt += item.subtotal;
          disc += item.billDiscountAmount? item.billDiscountAmount: 0;
          if(item.billDiscountAmount == undefined || !item.billDiscountAmount)
            item.billDiscountAmount = 0;

          // console.log(item.billDiscountAmount);

          //console.log('itemAmoun', Utils.roundNumber(item.subtotal - discToSubtract, 2));
          var itemDiscount=0;
          if(item.isDiscounted)
          {
          _.forEach(item.discounts, function (discount) {
            grpBySession[item.category.section.name].discount += Utils.roundNumber(discount.discountAmount, 2);
            disc += discount.discountAmount;
            itemDiscount +=Utils.roundNumber(discount.discountAmount,2);
            //billDiscountAmount += Utils.roundNumber(discount.discountAmount, 2)
            //console.log(grpBySession[bill.session.sessionName][item.category.section.name].discount);
          });
        }
          //}

          if(((bill.complimentary ||  (item.subtotal == itemDiscount)) && !bill.isVoid))
            voidDetails.complimentary += Utils.roundNumber(item.subtotal,2);
          else
            if(bill.isVoid||kot.isVoid)
              voidDetails.void += Utils.roundNumber(item.subtotal,2);
           else
            voidDetails.discount += Utils.roundNumber(itemDiscount,2) + Utils.roundNumber(item.billDiscountAmount,2);
          // console.log(bill.isVoid,kot.isVoid,bill.complimentary)
            if(!bill.isVoid && !bill.complimentary) 
            {   
            // console.log("inside if for bill taxes")    
             if(!kot.isVoid)
                 {
                  // console.log("inside if for kot taxes")
              _.forEach(item.taxes, function (tax) {

                  if (_.has(tax, 'cascadingTaxes')) {
                    _.forEach(tax.cascadingTaxes, function (cTax) {
                      allTaxes.push({
                        id: cTax._id,
                        name: cTax.name,
                        tax_amount: cTax.tax_amount ? !isNaN(cTax.tax_amount) ? Utils.roundNumber(parseFloat(cTax.tax_amount),2) : 0 : 0,
                        isConsidered: true
                      });

                   });
                  }
                 

                  //console.log('ctax', _cTax.tax_amount);

                  allTaxes.push({
                    id: tax._id,
                    name: tax.name,
                    tax_amount: tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0,
                    isConsidered: true
                  });
              });
            }
            }else {
              // console.log('else for tax')
            }

                _.forEach(item.addOns, function (addOn) {
                  if(!bill.isVoid && !bill.isComplimentary)
                  {
                    if(!kot.isVoid)
                  {
                  _.forEach(addOn.taxes, function (tax) {
                  if (_.has(tax, 'cascadingTaxes')) {
                    _.forEach(tax.cascadingTaxes, function (cTax) {
                     
                      allTaxes.push({
                        id: cTax._id,
                        name: cTax.name,
                        tax_amount: cTax.tax_amount ? !isNaN(cTax.tax_amount) ? Utils.roundNumber(parseFloat(cTax.tax_amount),2) : 0 : 0,
                        isConsidered: true
                      });

                    });
                  }
                 

                  allTaxes.push({
                    id: tax._id,
                    name: tax.name,
                    tax_amount: tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0,
                    isConsidered: true
                  });

                 
                });
              }
            }
              //var includeAddOn = true;
              if (!addOn.category) {
                addOn.category = {categoryName: "NA", section: {name: "NA"}}
              }

              if (!addOn.category.section) {
                addOn.category.section = {name: "NA"};
                //includeAddOn = false;
              }

              //if (includeAddOn) {
              //isItemPresent = true;
              if (!grpBySession[addOn.category.section.name])
                grpBySession[addOn.category.section.name] = {
                  amount: 0,
                  discount: 0,
                  
                };
              //console.log('addOnAmount', Utils.roundNumber(addOn.subtotal - discToSubtract, 2));
              grpBySession[addOn.category.section.name].amount += Utils.roundNumber(addOn.subtotal, 2);
              //console.log('addOnAmoun', Utils.roundNumber(addOn.subtotal - discToSubtract, 2));
              var addOnDiscount = 0;
              _.forEach(addOn.discounts, function (discount) {
                grpBySession[addOn.category.section.name].discount += Utils.roundNumber(discount.discountAmount, 2);
                //billDiscountAmount += Utils.roundNumber(discount.discountAmount, 2)
                addOnDiscount +=  Utils.roundNumber(discount.discountAmount, 2)
              });

              if(bill.isComplimentary ||  (addOn.subtotal == addOnDiscount))
              voidDetails.complimentary += Utils.roundNumber(item.subtotal,2);
            else
              if(bill.isVoid||kot.isVoid)
                voidDetails.void += Utils.roundNumber(addOn.subtotal,2);

              voidDetails.discount +=Utils.roundNumber(addOnDiscount,2);
              //}
            });
          });
       
      });
      //if (isItemPresent)
     // grpBySession[bill.session.sessionName].covers += bill._covers ? bill._covers : 0;
      //grpBySession[bill.session.sessionName].numBills += 1;
      //}
      //}
  });
  var html = "";
  var sectionTotal = 0 ;
  html += ""
  html += "";

  html += "Daily Sales Report,,\r\n";
  _.forEach(grpBySession,function(section,key){
     html += "";
     html+=""+key+",";
     html+=""+Utils.roundNumber(section.amount,2)+",";
     html+="\r\n";
     sectionTotal += Utils.roundNumber(section.amount,2)
  })

  html+="";
  html+="Section Total,";
  html+=""+Utils.roundNumber(sectionTotal,2)+",";
  html+="\r\n"
  html+="\r\n";
  html+="\r\n";
  html+="\r\n";
  html+=""
  html+="Complimentary,";
  html+=""+Utils.roundNumber(voidDetails.complimentary,2)+",";
  html+="\r\n"
   html+=""
  html+="Void,";
  html+=""+Utils.roundNumber(voidDetails.void,2)+",";
  html+="\r\n"
   html+=""
  html+="Discount,";
  html+=""+Utils.roundNumber(voidDetails.discount,2)+",";
  html+="\r\n"
  var voidTotal = Utils.roundNumber(voidDetails.complimentary,2) + Utils.roundNumber(voidDetails.void,2) + Utils.roundNumber(voidDetails.discount,2);
  var netSales = Utils.roundNumber(sectionTotal,2) - Utils.roundNumber(voidTotal,2);
  html+=""
  html+="Net,";
  html+=""+Utils.roundNumber(netSales,2)+",";
  html+="\r\n";
  var grpTaxesById = _.groupBy(allTaxes,function(tax){
    return tax.id
  })
  var totalTax=0;
  var total={};
  _.forEach(grpTaxesById,function(tax){
    // console.log(tax);
    _.forEach(tax,function(t){
      if(total[t.name] == undefined)
        total[t.name]=0;

      total[t.name]+=Utils.roundNumber(t.tax_amount,2)
      totalTax += Utils.roundNumber(t.tax_amount,2);
    })
  })
  // console.log(total,totalTax);
  _.forEach(total,function(t,key){
     html+=""
  html+=""+key+",";
  html+=""+Utils.roundNumber(t,2)+",";
  html+="\r\n"
  })
  netSales = Utils.roundNumber(netSales,2) + Utils.roundNumber(totalTax,2);
  html+=""
  html+="Payable Amount,";
  html+=""+Utils.roundNumber(netSales,2)+",";
  html+="\r\n";

  html += "";
  html += ",";
  exportToCsv(html)
};
var getDynamicSectionCategoryWise = function(bills, categories){
  var billItem = [];
  _.forEach(bills, function (bill) {
    if(!bill.isVoid) {
     _.forEach(bill._kots, function (kot) {
      if(!kot.isVoid)
      {
        _.forEach(kot.items, function (item) {
          var includeItem = true;
          if (!item.category) {
            item.category = {categoryName: "NA", section: {name: "NA"}}
            includeItem = false;
          }
          else {
            var cat = _.find(categories, function (category) {
              return category._id == item.category._id
            });
            var section = null;
            if (cat) {
              section = cat.section;
            }

            if (section) {
              item.category.section = section;
            } else {
              //includeItem = false;
              item.category.section = {name: "NA"};
            }
          }
          //if (includeItem) {
          //isItemPresent = true;
          
         
          if(item.billDiscountAmount == undefined || !item.billDiscountAmount)
            item.billDiscountAmount = 0;

          // console.log(item.billDiscountAmount);

          //console.log('itemAmoun', Utils.roundNumber(item.subtotal - discToSubtract, 2));
          var itemDiscount=0;
          if(item.isDiscounted)
          {
          _.forEach(item.discounts, function (discount) {
            //grpBySession[item.category.section.name].discount += Utils.roundNumber(discount.discountAmount, 2);
           itemDiscount +=discount.discountAmount;
            //billDiscountAmount += Utils.roundNumber(discount.discountAmount, 2)
            //console.log(grpBySession[bill.session.sessionName][item.category.section.name].discount);
          });
        }

       // console.log(itemDiscount,item.billDiscountAmount);

        item.discountAmount = parseFloat(item.billDiscountAmount) + parseFloat(itemDiscount);
         billItem.push(item);
   
        _.forEach(item.addOns, function (addOn) {
         
              //var includeAddOn = true;
              if (!addOn.category) {
                addOn.category = {categoryName: "NA", section: {name: "NA"}}
              }

              if (!addOn.category.section) {
                addOn.category.section = {name: "NA"};
                //includeAddOn = false;
              }

              //if (includeAddOn) {
              //isItemPresent = true;
             
              //console.log('addOnAmoun', Utils.roundNumber(addOn.subtotal - discToSubtract, 2));
              var addOnDiscount = 0;
              _.forEach(addOn.discounts, function (discount) {
               // grpBySession[addOn.category.section.name].discount += Utils.roundNumber(discount.discountAmount, 2);
                //billDiscountAmount += Utils.roundNumber(discount.discountAmount, 2)
                addOnDiscount +=  Utils.roundNumber(discount.discountAmount, 2)
              });
              addOn.discountAmount = addOnDiscount;
              billItem.push(addOn);
              //}
            });
          });
        }
      });
    }
  })
  var grpBySection = _.groupBy(billItem,function(section){
    return section.category.section.name;
  })

  _.forEach(grpBySection,function(sec,key){
    grpBySection[key] = _.groupBy(grpBySection[key],function(section){
     //console.log(section);
     return section.category.categoryName;
    })
  })
  var html=reportNameG+"\n\r\n\r";
    html += ""
    html += "";
    html+="";
    html+="Category,";
    html+="Amount,";
    html+="\r\n"
    var totalSale=0;
   // html += "Daily Sales Report,,\r\n";
    _.forEach(grpBySection,function(section,key){
     html+="";
     html+=""+key+","
     html+=",";
     html+="\r\n";
     var sectionValue = 0;
     _.forEach(grpBySection[key],function(sec,k){
      html+=""
      html+=""+k+",";
      var value=0;
      _.forEach(sec,function(it){
      value += Utils.roundNumber(it.subtotal,2) - Utils.roundNumber(it.discountAmount,2);
      sectionValue+= Utils.roundNumber(it.subtotal,2) - Utils.roundNumber(it.discountAmount,2);
      totalSale+= Utils.roundNumber(it.subtotal,2) - Utils.roundNumber(it.discountAmount,2);

      })
      html+=""+Utils.roundNumber(value,2)+",";
      html+="\r\n";
     })
     html+="";
     html+="Net "+key+" Sales,"
     html+=""+Utils.roundNumber(sectionValue,2)+",";
     html+="\r\n\r\n";
  })
  html+="";
  html+="Total Sale,";
  html+=""+Utils.roundNumber(totalSale,2)+",";
  html+="\r\n"
   html += "";
  html += ",";
  exportToCsv(html)
};
var dynamicHTMLBoH = function(bills) {
  _.forEach(bills, function(rep) {
    rep.formattedDate = reportUtils.getFormattedDateByCutoffTime(new Date(rep._created),cutOffTimeSettings);
  })
  var grpByDate = _.groupBy(bills, 'formattedDate')
  var gt = 0
  var flag = false
  var bohAmount = 0
  var modes = []
  var html = ''
  html += ''
  html += 'DATE,'
  html += 'TABLE NO,'
  html += 'STATUS,'
  html += 'GUEST NAME,'
  html += 'BILL NO,'
  html += 'AMOUNT,'
  html += 'MODE OF PAYMENT,'
  html += 'SETTLED ON,'
  html += 'AUTHORIZED BY,'
  html += 'TIME,'
  html += '\r\n'
  for (var date in grpByDate) {
    console.log(date)
    var dt = 0
    _.forEach(grpByDate[date], function(bill) {
      modes = []
      flag = false
      if (bill.payments.cash && bill.payments.cash.length > 0)
        modes.push('Cash')
      if (bill.payments.cards && bill.payments.cards.length > 0) {
        _.forEach(bill.payments.cards, function(oCards) {
          if (oCards.cardType === "Online") {
            _.forEach(oCards.detail, function(det) {
              modes.push(det.onlineName + " " + det.onlineType)
            });
          }
          if (oCards.cardType === "Other") {
            _.forEach(oCards.detail, function(det) {
              modes.push(det.otherName)
              if (det.otherName.toLowerCase() == 'boh')
                flag = true
                bohAmount = Number(det.amount)
            });
          }
          if (oCards.cardType === "Coupon") {
            _.forEach(oCards.detail, function(det) {
              modes.push(det.couponName)
            });
          }
          if (oCards.cardType != "Coupon" && oCards.cardType != "Online" && oCards.cardType != "Other" && oCards.totalAmount > 0)
            modes.push(oCards.cardType)
        });
      }
      if (flag == true) {
        dt += bohAmount
        html += ''
        html += '' + date + ','
        html += '' + (bill._tableId || 'NA') + ','
        html += ','
        if (bill._customer)
          html += '' + (bill._customer.firstname || 'NA') + ','
        else
          html += '' + 'NA' + ','
        html += '' + (bill.prefix || '') + (resetDaily ? bill.daySerialNumber : bill.serialNumber) + ','
        html += '' + bohAmount + ','
        html += '' + modes.join(" | ") + ','
        html += '' + moment(bill._closeTime).format('YYYY-MM-DD') + ','
        if (bill._currentUser)
          html += '' + (bill._currentUser.firstname + ' ' + (bill._currentUser.lastname || '') || 'NA') + ','
        else
          html += '' + 'NA' + ','
        html += '' + moment(bill._closeTime).format('h:mm a') + ','
        html += '\r\n'
      }
    })
    if(dt > 0) {
      html += ''
      html += ','
      html += ','
      html += ','
      html += ','
      html += 'Day Total,'
      html += '' + dt.toFixed(2) + ','
      html += ','
      html += ','
      html += ','
      html += ','
      html += '\r\n'
      gt += dt
    }
  }
  html += ''
  html += ','
  html += ','
  html += ','
  html += ','
  html += 'Grand Total,'
  html += '' + gt.toFixed(2) + ','
  html += ','
  html += ','
  html += ','
  html += ','
  html += '\r\n'
  html += ''
  exportToCsv(html)
};

var emailSend = function(body,filename,_p){
  //console.log(req.body);
  server.send({
    from: "Posist Info <no-reply@posist.info>",
    // to: body.email,
    to: detail.email,
    subject: detail.reportType+' (From: '+moment(detail.fromDate).format('YYYY/MM/DD')+' To: '+moment(detail.toDate).format('YYYY/MM/DD')+')',
    attachment: [
      {data: 'Dear Client<br>Here is the report for '+detail.reportType+' you requested' , alternative: true},
      {path:_p, type:"application/csv", name:detail.reportType+' ('+moment(detail.fromDate).format('YYYY.MM.DD')+'--'+moment(detail.toDate).format('YYYY.MM.DD')+').csv'}
    ]
  }, function (err, message) {
    if(err)
      console.log(err);  
  });
};
var exportToCsv = function(CSV){
  var csvString = CSV;
  var Key = new Buffer(reportNameG+':'+deployment).toString('base64');
  // console.log(detail)
  var _filename = reportNameG+'('+moment(detail.fromDate).format('YYYY.MM.DD')+'--'+moment(detail.toDate).format('YYYY.MM.DD')+')'+Key+'.csv'
  var _p = path.join(process.cwd(), 'uploads', _filename);
  // var exportButton = document.createElement('a');
  detail.filename = _filename
  console.log(reportNameG)
  detail.reportType = reportNameG
  fs.writeFile(_p, csvString, function(err) {
    if(err) {
      return console.log(err);
    }
    request.post('http://pos.posist.net/api/reports/',{form:detail}, function(err,httpResponse,body){if(!err)console.log('SUCCESSFULL')});
    // Report.create(detail,function(err,result){if(err) {console.log("NOT SAVED", err)} else console.log(result)})
    emailSend(null,_filename,_p) 
  });
};

var detail = {};
var deployment = '';
var reportNameG = '';
var uniqDeps = [];
var uniqCats = [];
var dates = [];
var cutOffTimeSettings = '';
var resetDaily = false;
var discountValue = false;
var type2 = '';
exports.reportCalculation=function(bills,cuttOffTimeSetting,deploymentSetting,reportName,fromDate,toDate,deployment_id,tenant_id,email,type,deps,discount) { 
  // console.log('OK')
  if(_.isEmpty(bills) || bills.length == 0)
    return false
  detail = {};
  allTaxesFor = {}
  chargesName = []
  dateWiseTaxBreakdown = {}
  detail.fromDate = moment(fromDate);
  detail.toDate = moment(toDate);
  detail.deployment_id=deployment_id;
  detail.tenant_id = tenant_id;
  detail.email = email;
  type2=type;
  discountValue = (discount==='true'|| discount===true?true:false);
  dates = reportUtils.getDates(new Date(fromDate), new Date(toDate));
  deployment = deployment_id;
  reportNameG = reportName;
  if(reportName == 'Invoice')
    reportNameG = 'Bill_Item_Detail'
  if(type != null && type != undefined && _.isString(type))
    reportNameG = reportNameG + '_' + type
  cutOffTimeSettings = moment(cuttOffTimeSetting);
  resetDaily = (deploymentSetting==='true'|| deploymentSetting===true?true:false);
  // console.log('INSIDE')
  if(reportName == 'Item_Wise_Enterprise') {
    uniqDeps = [];
    uniqCats = [];
    var uniqdepsTemp = _.uniq(bills, deployment);
    var uniqCatsTemp = _.uniq(bills, 'category');
    _.forEach(uniqdepsTemp, function (dep) {
      var _fDep = _.filter(deps, {_id: dep.deployment.toString()});
      // console.log(typeof(_fDep[0]._id))
      // console.log(typeof(dep.deployment))
      // console.log(_fDep[0]._id == dep.deployment)
      if (_fDep[0]._id == dep.deployment.toString()) {
        uniqDeps.push(_fDep[0]);
      }
    });
    _.forEach(uniqCatsTemp, function (dep) {
      uniqCats.push(dep.category);
    });
    var html = reportNameG+"\n\r\n\r"+'CONSOLIDATE ITEMWISE\n\r' + populateDynamicHTMLConsolidatedWise(bills) + '\n\r'
    html += 'CONSOLIDATE CATEGORYWISE\n\r' + populateDynamicHTMLCategoryWise(bills) + '\n\r'
    html += 'CONSOLIDATE DEPLOYMENTWISE\n\r' + populateDynamicHTMLDeploymentWise(bills) + '\n\r'
    exportToCsv(html)
  } else {
    if(reportName!='Tab-User-Waiter_Breakdown' && reportName!='Daily_Sales_Detail' && reportName!='Daily_Sales_Consolidated'){
      _.forEach(bills, function (bill) {
        if(reportName=='Online_Payment_Details' || reportName=='Online_Payment_Consolidated' || reportName=='Hourly_Sales' || reportName=="KOT_Tracking" || reportName=="CRM" || reportName=="Delivery" || reportName=="Delivery_Boy" || reportName=="Settlement" || reportName=="Other_Payment_Details" || reportName=="Other_Payment_Consolidated" || reportName=="Coupon_Detail" || reportName=="Coupon_Consolidated" || reportName=="Removed_Taxes" || reportName=="BOH")
          handleSerialNumberAndCutOffTime(bill, cuttOffTimeSetting, deploymentSetting);
        else
          getTotalCalculations(bill, cuttOffTimeSetting, deploymentSetting, reportName);
      });
    }
    // console.log('INSIDE')
    // detail.fromDate = moment(fromDate);
    // detail.toDate = moment(toDate);
    // detail.deployment_id=deployment_id;
    // detail.tenant_id = tenant_id;
    // detail.email = email;
    if(reportName=="Payment_Details")
      getDynamicPaymentDetails(bills)
    else if(reportName=="Tax_Summary")
      dynamicGetTaxSummary(bills)
    else if(reportName=="Invoice")
      dynamicGetInvoiceDetails(bills) 
    else if(reportName=="Tab-User-Waiter_Breakdown")
      getBilldetail(bills,type)
    else if(reportName=="Complimentary")
      populateDynamicHTMLComplementary(bills)
    else if(reportName=="Complimentary_Itemwise")
      populateDynamicHTMLComplementaryItemWise(bills)
    else if(reportName=="Complimentary_Headwise")
      populateDynamicHTMLComplimentaryHeadwise(bills)
    else if(reportName=="Online_Payment_Details")
      renderDynamicOnlinePaymentDetailsReport(bills)
    else if(reportName=="Online_Payment_Consolidated")
      renderDynamicOnlinePaymentConsolidatedReport(bills)
    else if(reportName=="Instance_wise")
      populateDynamicHTMLInstanceWiseReports(bills)
    else if(reportName=="Discount")
      populateDynamicHTMLDiscountReports(bills)
    else if(reportName=="Reprint")
      populateDynamicHTMLReprintReports(bills)
    else if(reportName=="Sales") {
      var categories = [];
      var taxes = [];
      var collections = ["Cash", "Credit Card", "Debit Card", "Coupon"];
      bills = _.sortBy(bills, 'serialNumber', false)
      _.forEach(bills, function (bill) {
        _.forEach(bill._items, function (items) {
          _.forEach(items, function (item) {
            // if(item.superCategory == 'Addons SC (Outlet)')
            //   console.log(item)
            if(item.superCategory)
              categories.push(item.superCategory);
            else
              categories.push("NA");
          });
        });

        _.forEach(bill.aggregate.taxes, function (tax) {
          if (tax.isConsidered)
            taxes.push(tax.name);
        });

        bill.payments.others = [];
        bill.payments.online=[];

        _.forEach(bill.payments.cards, function (oCards) {
           if (oCards.cardType === "Online") {
            _.forEach(oCards.detail, function (det) {
              oCards.mode = det.onlineName + " " + det.onlineType;
              collections.push(det.onlineName + " " + det.onlineType);
              bill.payments.online.push({
                mode: oCards.mode,
                amount: det.amount,
                cardType: "Online"
              })
            });
          }
          if (oCards.cardType === "Other") {
            _.forEach(oCards.detail, function (det) {
              oCards.mode = det.otherName ;
              collections.push(det.otherName );
              bill.payments.others.push({
                mode: oCards.mode,
                amount: det.amount,
                cardType: "Other"
              })
            });
          }
        });
      });
      var cats = _.uniq(categories);
      var txes = _.uniq(taxes);
      var collectionMode = _.uniq(collections);
      // console.log(cats)
      populateDynamicHTMLCategoryWiseReports(bills,cats,txes,collectionMode)
    }
    else if(reportName=="Hourly_Sales")
      getDynamicHourlySales(bills)
    else if(reportName=="Bill_And_Customer")
      dynamicGetBillConsolidate(bills)
    else if(reportName=="KOT_Tracking")
      populateDynamicHTMLKOTTracking(bills)
    else if(reportName=="CRM")
      dynamicCustomerDataReport(bills)
    else if(reportName=="Delivery")
      getDeliveryReportDynamic(bills)
    else if(reportName=="Delivery_Boy")
      getDeliveryBoyDynamic(bills)
    else if(reportName=="Item_Category")
      dynamicGetItemWiseConsolidate(bills,false,false)
    else if(reportName=="Itemwise_Profit")
      dynamicGetItemWiseProfit(bills)
    else if(reportName=="Removed_Taxes")
      populateDynamicHTMLRemovedTaxesReport(bills)
    else if(reportName=="Sales_By_Item_Category")
      exportToCsvForCategory(bills)
    else if(reportName=="Daily_Sales_Detail")
      renderDaySalesReport(bills,type,deps)
    else if(reportName=="Taxation_BillWise")
      dynamicBaggaryHTML(bills,deps)
    else if(reportName=="Settlement"){
      if(type=='Detail')
        renderDynamicSettlementDetailsReport(bills)
      else
        renderDynamicSettlementConsolidatedReport(bills)
    }
    else if(reportName=="Coupon_Detail")
      renderDynamicCouponDetailsReport(bills)
    else if(reportName=="Coupon_Consolidated")
      renderDynamicCouponConsolidateReport(bills)
    else if(reportName=="Other_Payment_Details")
      renderDynamicNewOtherPaymentDetailsReport(bills)
    else if(reportName=="Other_Payment_Consolidated")
      renderDynamicNewOtherPaymentConsolidatedReport(bills)
    else if(reportName=="Daily_Sales_Consolidated")
      renderDaySalesReportForConsolidate(bills,type,deps)
    else if(reportName=="Section_Category_Wise")
      getDynamicSectionCategoryWise(bills,deps)
    else if(reportName=="Bill_On_Hold")
      dynamicHTMLBoH(bills)
    else console.log("No Match");
  }
}
var allTaxesFor = {}
var chargesName = []
var dateWiseTaxBreakdown = {}
var getTotalCalculations = function(bill, cutoffTimeSetting, deploymentSettings, report) {
  //allTaxesFor = {};
  // console.log(cutoffTimeSetting, deploymentSettings);
  //console.log(deploymentSettings);
  var enableInvoice = false;
  var allItems = [];
  var allTaxes = [];
  var aggregate = {
    amount: 0,
    discount: (bill.billDiscountAmount ? (!isNaN(bill.billDiscountAmount) ? bill.billDiscountAmount : Number(bill.billDiscountAmount)) : 0),
    taxes: [],
    totalTax: 0,
    cash: 0,
    debit: 0,
    credit: 0,
    other: 0,
    coupon:0,
    charges:[],
    online:0

  }

  var _items = {};

  var _cutOffTime = moment(moment(cutoffTimeSetting));
  var _cutOffTimeMeridian = moment(moment(cutoffTimeSetting)).format('a');
  var _cutOffTimeHour = parseInt(moment(moment(_cutOffTime)).format('H'));
  var _cutOffTimeMin = parseInt(moment(moment(_cutOffTime)).format('mm'));

  var _billOpenTime = moment(moment(bill._created));
  var _billCloseTime = moment(moment(bill._closeTime));

  var _billOpenTimeStartOfDay = moment(moment(bill._created)).startOf('day');
  var _billOpenTimeEnfOfDay = moment(moment(bill._created)).endOf('day');
  var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour, 'hours').add(_cutOffTimeMin, 'minutes');
  //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

  if (_cutOffTimeMeridian === 'am') {
    if (_billOpenTime>_billOpenTimeOffsetCutOff && _billOpenTime<_billOpenTimeEnfOfDay) {
      bill.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
    }

    if (_billOpenTime>_billOpenTimeStartOfDay && _billOpenTime<_billOpenTimeOffsetCutOff) {
      bill.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
    }
  }
  //*!* Changing for groupBy to work *!/*/
  bill.created = moment(bill._created).format('YYYY-MM-DD');
  //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

  ///!* Create Faux Date Hack *!/
  if (bill.fauxCloseTime != null) {
    bill.closeTime = bill.fauxCloseTime;
  } else {
    if (_cutOffTimeMeridian != 'am') {
      bill.closeTime = bill.created;
    }
  }

  /* /!* Replacing serialNumber with daySerialNumber *!/*/
  if (deploymentSettings===true || deploymentSettings==='true') {
    // console.log("indisese deployment",bill.daySerialNumber);
    //console.log("Entering reset_serial_daily");
    bill.serialNumber = bill.daySerialNumber;
  }
  /*/!*fine dine fix*!/*/
  // if (bill.prefix != null) {
  //   bill.serialNumber = bill.prefix +' '+ bill.serialNumber;
  // }
  if (report) {
    if (report.toLowerCase() != "tax_summary")
      enableInvoice = true;

    if (report.toLowerCase() == "tax_summary") {
      if (!dateWiseTaxBreakdown[bill.closeTime]) {
        dateWiseTaxBreakdown[bill.closeTime] = {
          noOfBills: 0,
          amount: 0,
          discount: 0,
          taxes: [],
          totalTax: 0,
          cash: 0,
          debit: 0,
          credit: 0,
          other: 0,
          coupon:0,
          online:0,
          netAmount: 0,
          roundOff: 0,
          grandTotal: 0,
          charges:[]
        }
      }
    }
  }

  if (!bill.isVoid && !bill.isDeleted) {
   
    if((bill.charges!=undefined||bill.charges!=null))
    {
      _.forEach(bill.charges.detail,function(charge){
        // console.log(charge);
        // console.log(charge.name);
        chargesName.push(charge.name);
        aggregate.charges.push(charge);
        if(report.toLowerCase() == "tax_summary")
         dateWiseTaxBreakdown[bill.closeTime].charges.push(charge);

      })
    }
    else
    {
      bill.charges = {};
      bill.charges.detail = [];
      bill.charges.amount = 0;
    }
    _.forEach(bill._kots, function (kot) {

      if (!kot.isVoid) {

        _.forEach(kot.items, function (item) {

          var itemTaxes = [];
          if (!_items[item._id]) {
            _items[item._id] = {
              rate: 0,
              qty: 0,
              amount: 0,
              discount: 0,
              netAmount: 0,
              taxAmount: 0,
              taxes: [],
              value: 0
            };
          }
          // Add Bill/ KOT info to Item
          // item.billNumber = bill.billNumber;
          // item.serialNumber = bill.serialNumber;
          // item.daySerialNumber = bill.serialNumber;
          _items[item._id].closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
          _items[item._id].Day = moment(bill._closeTime).format('YYYY-MM-DD');
          _items[item._id].Time = moment(bill._closeTime).format('hh:mm:ss');
          if(item.category && item.category.superCategory)
            _items[item._id].superCategory = item.category.superCategory.superCategoryName;
          else
            _items[item._id].superCategory = 'NA'
          // _items[item._id].superCategoryId = item.category.superCategory._id;
          // _items[item._id].categoryId = item.category._id;
          if(item.category)
            _items[item._id].category = item.category.categoryName;
          else
            _items[item._id].category = 'NA'

          _items[item._id].tab = bill.tab;
          _items[item._id].name = item.name;
          _items[item._id].rate = item.rate;
          _items[item._id].qty += item.quantity;
          _items[item._id].amount += item.subtotal;
           // console.log(item.billDiscountAmount)
          if(item.billDiscountAmount == undefined || !item.billDiscountAmount)
            item.billDiscountAmount = 0;
          
          _items[item._id].discount += Utils.roundNumber(item.billDiscountAmount,2)
          if (enableInvoice) {
            _.forEach(item.discounts, function (discount) {
              _items[item._id].discount += Utils.roundNumber(discount.discountAmount,2);
            });
          }

          allItems.push(item);
          _.forEach(item.taxes, function (tax) {
            if (_.has(tax, 'cascadingTaxes')) {
              _.forEach(tax.cascadingTaxes, function (cTax) {
                allTaxes.push({
                  id: cTax._id + "_amt",
                  name: cTax.name + " Amount",
                  value: cTax.value,
                  tax_amount: (_.has(cTax, 'baseRate_preTax')) ? !isNaN(cTax.baseRate_preTax) ? Utils.roundNumber(parseFloat(cTax.baseRate_preTax),2) : 0 : 0,
                  isConsidered: false
                });

                //console.log('ctax', _cTax.tax_amount);

                allTaxes.push({
                  id: cTax._id,
                  name: cTax.name,
                  value: cTax.value,
                  tax_amount: cTax.tax_amount ? !isNaN(cTax.tax_amount) ? Utils.roundNumber(parseFloat(cTax.tax_amount),2) : 0 : 0,
                  isConsidered: true
                });

                itemTaxes.push({
                  id: cTax._id + "_amt",
                  name: cTax.name + " Amount",
                  value: cTax.value,
                  tax_amount: (_.has(cTax, 'baseRate_preTax')) ? !isNaN(cTax.baseRate_preTax) ? Utils.roundNumber(parseFloat(cTax.baseRate_preTax),2) : 0 : 0,
                  isConsidered: false
                });

                //console.log('ctax', _cTax.tax_amount);

                itemTaxes.push({
                  id: cTax._id,
                  name: cTax.name,
                  value: cTax.value,
                  tax_amount: cTax.tax_amount ? !isNaN(cTax.tax_amount) ? Utils.roundNumber(parseFloat(cTax.tax_amount),2) : 0 : 0,
                  isConsidered: true
                });
              });
            }
            allTaxes.push({
              id: tax._id + "_amt",
              name: tax.name + " Amount",
              value: tax.value,
              tax_amount: (_.has(tax, 'baseRate_preTax')) ? !isNaN(tax.baseRate_preTax) ? Utils.roundNumber(parseFloat(tax.baseRate_preTax),2) : 0 : 0,
              isConsidered: false
            });

            //console.log('ctax', _cTax.tax_amount);

            allTaxes.push({
              id: tax._id,
              name: tax.name,
              value: tax.value,
              tax_amount: tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0,
              isConsidered: true
            });

            itemTaxes.push({
              id: tax._id + "_amt",
              name: tax.name + " Amount",
              value: tax.value,
              tax_amount: (_.has(tax, 'baseRate_preTax')) ? !isNaN(tax.baseRate_preTax) ? Utils.roundNumber(parseFloat(tax.baseRate_preTax),2) : 0 : 0,
              isConsidered: false
            });

            //console.log('ctax', _cTax.tax_amount);

            itemTaxes.push({
              id: tax._id,
              name: tax.name,
              value: tax.value,
              tax_amount: tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0,
              isConsidered: true
            });
          });
          if (_.has(item, 'addOns')) {
            
            _.forEach(item.addOns, function (addOn) {
              var addOnTaxes = [];
              allItems.push(addOn);
              if (!_items[addOn._id]) {
                _items[addOn._id] = {
                  name: "",
                  rate: 0,
                  qty: 0,
                  amount: 0,
                  discount: 0,
                  netAmount: 0,
                  taxAmount: 0,
                  taxes: [],
                };
              }

              // Add Bill/ KOT info to Item
              // item.billNumber = bill.billNumber;
              // item.serialNumber = bill.serialNumber;
              // item.daySerialNumber = bill.serialNumber;
              _items[addOn._id].closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
              _items[addOn._id].Day = moment(bill._closeTime).format('YYYY-MM-DD');
              _items[addOn._id].Time = moment(bill._closeTime).format('hh:mm:ss');
              if(addOn.category && addOn.category.superCategory)
                _items[addOn._id].superCategory = addOn.category.superCategory.superCategoryName;
              else 
                _items[addOn._id].superCategory = 'NA'
              // _items[addOn._id].superCategoryId = item.category.superCategory._id;
              // _items[addOn._id].categoryId = item.category._id;
              if(addOn.category)
                _items[addOn._id].category = addOn.category.categoryName;
              else
                _items[addOn._id].category = 'NA'

              _items[addOn._id].tab = bill.tab;
              _items[addOn._id].name = addOn.name;
              _items[addOn._id].rate = addOn.rate;
              _items[addOn._id].qty += addOn.quantity;
              _items[addOn._id].amount += Utils.roundNumber(addOn.subtotal,2);
              //_items[addOn._id].discount += addOn.discount;

              _.forEach(addOn.taxes, function (tax) {

                if (_.has(tax, 'cascadingTaxes')) {
                  _.forEach(tax.cascadingTaxes, function (cTax) {
                    allTaxes.push({
                      id: cTax._id + "_amt",
                      name: cTax.name +" Amount",
                      value: cTax.value,
                      tax_amount: (_.has(cTax, 'baseRate_preTax')) ? !isNaN(cTax.baseRate_preTax) ? Utils.roundNumber(parseFloat(cTax.baseRate_preTax),2) : 0 : 0,
                      isConsidered: false
                    });

                    //console.log('ctax', _cTax.tax_amount);

                    allTaxes.push({
                      id: cTax._id,
                      name: cTax.name,
                      value: cTax.value,
                      tax_amount: cTax.tax_amount ? !isNaN(cTax.tax_amount) ? Utils.roundNumber(parseFloat(cTax.tax_amount),2) : 0 : 0,
                      isConsidered: true
                    });

                    addOnTaxes.push({
                      id: cTax._id + "_amt",
                      name: cTax.name+" Amount",
                      value: cTax.value,
                      tax_amount: (_.has(cTax, 'baseRate_preTax')) ? !isNaN(cTax.baseRate_preTax) ? Utils.roundNumber(parseFloat(cTax.baseRate_preTax),2) : 0 : 0,
                      isConsidered: false
                    });

                    //console.log('ctax', _cTax.tax_amount);

                    addOnTaxes.push({
                      id: cTax._id,
                      name: cTax.name,
                      value: cTax.value,
                      tax_amount: cTax.tax_amount ? !isNaN(cTax.tax_amount) ? Utils.roundNumber(parseFloat(cTax.tax_amount),2) : 0 : 0,
                      isConsidered: true
                    });
                  });
                }
                allTaxes.push({
                  id: tax._id + "_amt",
                  name: tax.name + " Amount",
                  value: tax.value,
                  tax_amount: (_.has(tax, 'baseRate_preTax')) ? !isNaN(tax.baseRate_preTax) ? Utils.roundNumber(parseFloat(tax.baseRate_preTax),2) : 0 : 0,
                  isConsidered: false
                });

                //console.log('ctax', _cTax.tax_amount);

                allTaxes.push({
                  id: tax._id,
                  name: tax.name,
                  value: tax.value,
                  tax_amount: tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0,
                  isConsidered: true
                });

                addOnTaxes.push({
                  id: tax._id + "_amt",
                  name: tax.name + " Amount",
                  value: tax.value,
                  tax_amount: (_.has(tax, 'baseRate_preTax')) ? !isNaN(tax.baseRate_preTax) ? Utils.roundNumber(parseFloat(tax.baseRate_preTax),2) : 0 : 0,
                  isConsidered: false
                });

                //console.log('ctax', _cTax.tax_amount);

                addOnTaxes.push({
                  id: tax._id,
                  name: tax.name,
                  value: tax.value,
                  tax_amount: tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0,
                  isConsidered: true
                });
              });

              if (enableInvoice) {
                var grpItemTaxesById = _.groupBy(addOnTaxes, function (tax) {
                  return tax.id;
                });

                //console.log(grpTaxesById);
                // console.log(enableInvoice);
                var taxAmount = 0;
                _.forEach(grpItemTaxesById, function (taxes) {
                  var _t = {
                    tax_amount: 0
                  };
                  _.forEach(taxes, function (tax) {

                    allTaxesFor[tax.id] = tax.name;
                    // console.log(isNaN(tax.tax_amount));
                    _t.id = tax.id;
                    _t.name = tax.name;
                    _t.value = tax.value;
                    _t.tax_amount += tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0;
                    _t.isConsidered = tax.isConsidered;
                  });
                  _items[addOn._id].taxes.push(_t);
                  if (_t.isConsidered)
                    tax_amount += Utils.roundNumber(_t.taxAmount,2);
                });
                _items[addOn._id].taxAmount += Utils.roundNumber(taxAmount,2);
              }
            });
          }

          if (enableInvoice) {
            var grpItemTaxesById = _.groupBy(itemTaxes, function (tax) {
              return tax.id;
            });

            //console.log(grpTaxesById);

            var tax_amount = 0;
            _.forEach(grpItemTaxesById, function (taxes) {
              var _t = {
                tax_amount: 0
              };
              _.forEach(taxes, function (tax) {
                allTaxesFor[tax.id] = tax.name;
                //console.log(tax.tax_amount);
                _t.id = tax.id;
                _t.name = tax.name;
                //if(bill.complimentary) {
                //  _t.tax_amount += 0;
                //} else
                _t.value = tax.value;
                _t.tax_amount += tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(tax.tax_amount,2) : 0 : 0;
                _t.isConsidered = tax.isConsidered;
              });
              _items[item._id].taxes.push(_t);
              if (_t.isConsidered)
                tax_amount += Utils.roundNumber(_t.tax_amount,2);
            });
            _items[item._id].taxAmount += Utils.roundNumber(tax_amount,2);
          }
        });
      }
    });

    if (bill.payments) {
      _.forEach(bill.payments.cash, function (payment) {
        aggregate.cash += payment;
      });

      _.forEach(bill.payments.cards, function (payment) {
        if (payment.cardType == "CreditCard")
          aggregate.credit += payment.totalAmount;
        if (payment.cardType == "DebitCard")
          aggregate.debit += payment.totalAmount;
        if (payment.cardType == "Other")
          aggregate.other += payment.totalAmount;
        if(payment.cardType == "Coupon")
          aggregate.coupon+=payment.totalAmount;
        if(payment.cardType == "Online")
          aggregate.online+=payment.totalAmount;
      });
    }

    _.forEach(allItems, function (item) {
      aggregate.amount += Utils.roundNumber(item.subtotal,2);
      if (_.has(item, 'discounts')) {
        _.forEach(item.discounts, function (discount) {
          aggregate.discount += Utils.roundNumber(discount.discountAmount,2);
        });
      }
    });

    //console.log(allTaxes);

    var grpTaxesById = _.groupBy(allTaxes, function (tax) {
      return tax.id;
    });

    //console.log(grpTaxesById);

    //console.log(grpTaxesById);
    _.forEach(grpTaxesById, function (taxes) {
      var _t = {
        tax_amount: 0
      };
      _.forEach(taxes, function (tax) {

        allTaxesFor[tax.id] = tax.name;
        //console.log(tax.tax_amount);
        _t.id = tax.id;
        _t.name = tax.name;
        _t.value = tax.value;
        _t.tax_amount += tax.tax_amount ? !isNaN(tax.tax_amount) ? Utils.roundNumber(parseFloat(tax.tax_amount),2) : 0 : 0;
        _t.isConsidered = tax.isConsidered;
      });
      if (_t.isConsidered == true) {
        aggregate.totalTax += Utils.roundNumber(_t.tax_amount,2);
      }
      aggregate.taxes.push(_t);
      if (report.toLowerCase() == "tax_summary") {
        var tx = _.find(dateWiseTaxBreakdown[bill.closeTime].taxes, function (x) {
          //console.log(_t.id, tax.id);
          return x.id.toString() == _t.id.toString();
        });
        //console.log(tx)
        if (tx)
          tx.tax_amount += Utils.roundNumber(_t.tax_amount,2);
        else
          dateWiseTaxBreakdown[bill.closeTime].taxes.push(_t);
      }
    });
    //console.log('agrTAx', aggregate.totalTax);
  } else {
    aggregate.amount = 0;
    aggregate.discount = 0;
    if(bill.charges == undefined || bill.charges == null || bill.charges.amount == undefined){
      bill.charges = {
        detail:[],
        amount:0
      }
    }
    // console.log(bill.serialNumber)
    bill.charges.amount =0;
  }
  bill.aggregate = aggregate;
  // console.log(aggregate.taxes);
  //console.log(bill.aggregate.amount, bill.aggregate.discount, bill.aggregate.totalTax);
  bill.aggregate.netAmount = bill.aggregate.amount - bill.aggregate.discount + bill.aggregate.totalTax+bill.charges.amount;
  bill.aggregate.roundOff = Utils.roundNumber((Utils.roundNumber(bill.aggregate.netAmount) - bill.aggregate.netAmount),2);
  bill.aggregate.grandTotal = Utils.roundNumber(bill.aggregate.netAmount);
  //console.log(bill);
  bill._items = _items;
  // console.log(bill._items);
  if (report.toLowerCase() == "tax_summary") {
    dateWiseTaxBreakdown[bill.closeTime].noOfBills += 1;
    dateWiseTaxBreakdown[bill.closeTime].amount += bill.aggregate.amount;
    dateWiseTaxBreakdown[bill.closeTime].discount += bill.aggregate.discount;
    dateWiseTaxBreakdown[bill.closeTime].totalTax += bill.aggregate.totalTax;
    dateWiseTaxBreakdown[bill.closeTime].cash += bill.aggregate.cash;
    dateWiseTaxBreakdown[bill.closeTime].credit += bill.aggregate.credit;
    dateWiseTaxBreakdown[bill.closeTime].debit += bill.aggregate.debit;
    dateWiseTaxBreakdown[bill.closeTime].other += bill.aggregate.other;
    dateWiseTaxBreakdown[bill.closeTime].coupon += bill.aggregate.coupon;
    dateWiseTaxBreakdown[bill.closeTime].online += bill.aggregate.online;
    dateWiseTaxBreakdown[bill.closeTime].netAmount += bill.aggregate.netAmount;
    dateWiseTaxBreakdown[bill.closeTime].roundOff += Utils.roundNumber(bill.aggregate.roundOff,2);
    dateWiseTaxBreakdown[bill.closeTime].grandTotal += bill.aggregate.grandTotal;
    //dateWiseTaxBreakdown[bill.closeTime].charges.push(bill.aggregate.charges);
    //console.log(dateWiseTaxBreakdown);
  }

  /*if (!tabWiseBreakdown[bill.tab])
   tabWiseBreakdown[bill.tab] = [];
   tabWiseBreakdown[bill.tab].push(bill);*/
}
function handleSerialNumberAndCutOffTime(bill, cutoffTimeSetting, deploymentSetting) {
  var _cutOffTime = moment(moment(cutoffTimeSetting));
  var _cutOffTimeMeridian = moment(moment(cutoffTimeSetting)).format('a');
  var _cutOffTimeHour = parseInt(moment(moment(_cutOffTime)).format('H'));
  var _cutOffTimeMin = parseInt(moment(moment(_cutOffTime)).format('mm'));

  var _billOpenTime = moment(moment(bill._created));
  var _billCloseTime = moment(moment(bill._closeTime));

  var _billOpenTimeStartOfDay = moment(moment(bill._created)).startOf('day');
  var _billOpenTimeEnfOfDay = moment(moment(bill._created)).endOf('day');
  var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour, 'hours').add(_cutOffTimeMin, 'minutes');
  //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

  if (_cutOffTimeMeridian === 'am') {
    if (_billOpenTime>_billOpenTimeOffsetCutOff && _billOpenTime<_billOpenTimeEnfOfDay) {
      bill.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
    }

    if (_billOpenTime>_billOpenTimeStartOfDay && _billOpenTime<_billOpenTimeOffsetCutOff) {
      bill.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
    }
  }
  //*!* Changing for groupBy to work *!/*/
  bill.created = moment(bill._created).format('YYYY-MM-DD');
  //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

  ///!* Create Faux Date Hack *!/
  if (bill.fauxCloseTime != null) {
    bill.closeTime = bill.fauxCloseTime;
  } else {
    if (_cutOffTimeMeridian != 'am') {
      bill.closeTime = bill.created;
    }
  }

  /* /!* Replacing serialNumber with daySerialNumber *!/*/
  if (resetDaily===true || resetDaily==='true') {
    //console.log("Entering reset_serial_daily");
    bill.serialNumber = bill.daySerialNumber;
  }
  /*/!*fine dine fix*!/*/
  if (bill.prefix != null) {
    bill.serialNumber = bill.prefix + bill.serialNumber;
  }
}

exports.reportCalculationsForKot = function(bills,cuttOffTimeSetting,deploymentSetting,reportName,fromDate,toDate,deployment_id,tenant_id,email,type,deps,discount) {
  if(_.isEmpty(bills) || bills.length == 0)
    return false

  _kots=[]
  // console.log(cuttOffTimeSetting);
  detail.fromDate = moment(fromDate);
  detail.toDate = moment(toDate);
  detail.deployment_id=deployment_id;
  detail.tenant_id = tenant_id;
  detail.email = email;
  type=type;
  discountValue = (discount==='true'|| discount===true?true:false);
  dates = reportUtils.getDates(new Date(fromDate), new Date(toDate));
  deployment = deployment_id;
  reportNameG = reportName;
  cutOffTimeSettings = moment(cuttOffTimeSetting);
  resetDaily = (deploymentSetting==='true'|| deploymentSetting===true?true:false);
  _.forEach(bills, function (bill) {
    totalCalculationsForKot(bill, cuttOffTimeSetting, deploymentSetting, reportName);
    //console.log(bill._items);
  });
  dynamicGetKotDetail(kotDetailService(_kots));
}
var _kots=[]
var kotDetailService=function(_kots){
  _.forEach(_kots, function (kot) {
    _.forEach(kot.items,function(item){
      //  console.log(item);
      if (_.has(item, 'addOns')) {
        if (item.addOns.length > 0) {
          _.forEach(item.addOns, function (addon) {
            //console.log("inside adding addOn",index);
            kot.items.push(addon);
          })
        }
      }
    })
  });
  return _kots
}
var totalCalculationsForKot = function(bill,cutoffTimeSetting,deploymentSetting,reportName) {
  // console.log("fasfa");
  var _cutOffTime = moment(moment(cutoffTimeSetting));
  //console.log(_cutOffTime);
  var _cutOffTimeMeridian = moment(moment(cutoffTimeSetting)).format('a');
  //console.log(moment(moment(cutoffTimeSetting)).format('a'));
  var _cutOffTimeHour = new Date(cutoffTimeSetting).getHours();
  var _cutOffTimeMin =new Date(cutoffTimeSetting).getMinutes();
  // var billOpenHours = new Date(bill._created).getHours();
  //var billOpenMin = new Date(bill._created).getMinutes();
  var _billOpenTime = moment(moment(bill._created));
  var _billCloseTime = moment(moment(bill._closeTime));

  var _billOpenTimeStartOfDay = moment(moment(bill._created)).startOf('day');
  var _billOpenTimeEnfOfDay = moment(moment(bill._created)).endOf('day');
  var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour, 'hours').add(_cutOffTimeMin, 'minutes');
  //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

  if (_cutOffTimeMeridian === 'am') {
    if ((_billOpenTime>_billOpenTimeOffsetCutOff) && (_billOpenTime<_billOpenTimeEnfOfDay)) {
      bill.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
    }

    if ((_billOpenTime>_billOpenTimeStartOfDay) && (_billOpenTime<_billOpenTimeOffsetCutOff)) {
      bill.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
    }
  }
  //!*!* Changing for groupBy to work *!/!*!/
  bill.created = moment(bill._created).format('YYYY-MM-DD');
  //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

  ///!* Create Faux Date Hack *!/
  if (bill.fauxCloseTime != null) {
    bill.closeTime = bill.fauxCloseTime;
  } else {
    if (_cutOffTimeMeridian != 'am') {
      bill.closeTime = bill.created;
    }
  }
  // console.log(bill.closeTime);

  /* /!* Replacing serialNumber with daySerialNumber *!/*/
  //console.log("deployment setting",typeof deploymentSetting);
  if(resetDaily==="true" || resetDaily===true)
  {
    //console.log("Entering reset_serial_daily",bill.serialNumber);
    bill.serialNumber = bill.daySerialNumber;
  }
  /*/!*fine dine fix*!/*/
  if (bill.prefix != null) {
    bill.serialNumber = bill.prefix + bill.serialNumber;
  }
    /* Add Bill Details to KOT */
      /* Don't consider Void Bills */
      if (!bill.isVoid) {
        _.forEach(bill._kots, function (kot) {
          if (!kot.isVoid) {
            kot.billNumber = bill.billNumber;
            kot.closeTime = bill._closeTime;
            kot.closeDate = moment(bill._closeTime).format('YYYY-MM-DD');
            kot.tab = bill.tab;
            kot.tabType = bill.tabType;
            kot.daySerialNumber = bill.daySerialNumber;
            kot.serialNumber = bill.serialNumber;
            kot.prefix = (bill.prefix != undefined? bill.prefix:'')
            _kots.push(kot);
          }
        });
      }
}

var _kotsDeleteHistory = [];
exports.reportCalculationsForKotDelete = function(bills,cuttOffTimeSetting,deploymentSetting,reportName,fromDate,toDate,deployment_id,tenant_id,email,type,deps,discount) {
  if(_.isEmpty(bills) || bills.length == 0)
    return false

  // console.log(cuttOffTimeSetting);
  _kotsDeleteHistory = [];
  detail.fromDate = moment(fromDate);
  detail.toDate = moment(toDate);
  detail.deployment_id=deployment_id;
  detail.tenant_id = tenant_id;
  detail.email = email;
  type=type;
  discountValue = (discount==='true'|| discount===true?true:false);
  dates = reportUtils.getDates(new Date(fromDate), new Date(toDate));
  deployment = deployment_id;
  reportNameG = reportName;
  cutOffTimeSettings = moment(cuttOffTimeSetting);
  resetDaily = (deploymentSetting==='true'|| deploymentSetting===true?true:false);
  _.forEach(bills, function (bill) {
    totalCalculationsForKotDelete(bill, moment(cuttOffTimeSetting), deploymentSetting, reportName);
    //console.log(bill._items);
  });
  dynamicGetKotDelete(_kotsDeleteHistory)
}
var totalCalculationsForKotDelete=function(bill,cutoffTimeSetting,deploymentSetting,reportName) {
  var _cutOffTime = moment(moment(cutoffTimeSetting));
  //console.log(_cutOffTime);
  var _cutOffTimeMeridian = moment(moment(cutoffTimeSetting)).format('a');
  //console.log(moment(moment(cutoffTimeSetting)).format('a'));
  var _cutOffTimeHour = new Date(cutoffTimeSetting).getHours();
  var _cutOffTimeMin =new Date(cutoffTimeSetting).getMinutes();
  // var billOpenHours = new Date(bill._created).getHours();
  //var billOpenMin = new Date(bill._created).getMinutes();
  var _billOpenTime = moment(moment(bill._created));
  var _billCloseTime = moment(moment(bill._closeTime));

  var _billOpenTimeStartOfDay = moment(moment(bill._created)).startOf('day');
  var _billOpenTimeEnfOfDay = moment(moment(bill._created)).endOf('day');
  var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour, 'hours').add(_cutOffTimeMin, 'minutes');
  //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

  if (_cutOffTimeMeridian === 'am') {
    if ((_billOpenTime>_billOpenTimeOffsetCutOff) && (_billOpenTime<_billOpenTimeEnfOfDay)) {
      bill.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
    }

    if ((_billOpenTime>_billOpenTimeStartOfDay) && (_billOpenTime<_billOpenTimeOffsetCutOff)) {
      bill.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
    }
  }
  //!*!* Changing for groupBy to work *!/!*!/
  bill.created = moment(bill._created).format('YYYY-MM-DD');
  //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

  ///!* Create Faux Date Hack *!/
  if (bill.fauxCloseTime != null) {
    bill.closeTime = bill.fauxCloseTime;
  } else {
    if (_cutOffTimeMeridian != 'am') {
      bill.closeTime = bill.created;
    }
  }
  // console.log(bill.closeTime);

  /* /!* Replacing serialNumber with daySerialNumber *!/*/
  //console.log("deployment setting",typeof deploymentSetting);
  if(resetDaily==="true"||resetDaily===true)
  {
    //console.log("Entering reset_serial_daily",bill.serialNumber);
    bill.serialNumber = bill.daySerialNumber;
  }
  /*/!*fine dine fix*!/*/
  if (bill.prefix != null) {
    bill.serialNumber = bill.prefix + bill.serialNumber;
  }

  /* Add Bill Details to KOT */


  /* Don't consider Void Bills */

    _.forEach(bill._kots, function (kot) {

      if (bill.isVoid) {
        /* Get all KOT Items from Void Bill*/
        if(_.has(kot,"deleteHistory"))
        {
          if(kot.deleteHistory.length > 0){
            _.forEach(kot.deleteHistory, function (delItem){
              delItem.billNumber = bill.billNumber;
              delItem.serialNumber = bill.serialNumber;
              delItem.prefix = (bill.prefix != undefined? bill.prefix:'')
              delItem.daySerialNumber = bill.daySerialNumber;
              delItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
              delItem._time = moment(bill._closeTime).format('hh:mm:ss a');
              delItem.kotNumber = kot.kotNumber;
              delItem.username = bill._currentUser.username;
              delItem.openTime = kot.created;
              delItem.deleteTime = delItem.voidItemTime;
              delItem.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");
              _kotsDeleteHistory.push(delItem);
              /*Adding Add Ons*/
              if(_.has(delItem,'addOns')) {
                _.forEach(delItem.addOns,function(addon){
                  addon.billNumber = bill.billNumber;
                  addon.serialNumber = bill.serialNumber;
                  addon.prefix = (bill.prefix != undefined? bill.prefix:'')
                  addon.daySerialNumber = bill.daySerialNumber;
                  addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                  addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                  addon.kotNumber = kot.kotNumber;
                  addon.username = bill._currentUser.username;
                  addon.openTime = kot.created;
                  addon.deleteTime = delItem.voidItemTime;
                  addon.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");

                  _kotsDeleteHistory.push(addon);
                })
              }
              /*End Add Ons*/
            });
          }
        }
        _.forEach(kot.items, function (voidBillItem) {
          voidBillItem.billNumber = bill.billNumber;
          voidBillItem.serialNumber = bill.serialNumber;
          voidBillItem.prefix = (bill.prefix != undefined? bill.prefix:'')
          voidBillItem.daySerialNumber = bill.daySerialNumber;
          voidBillItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
          voidBillItem._time = moment(bill._closeTime).format('hh:mm:ss a');
          voidBillItem.kotNumber = kot.kotNumber;
          voidBillItem.username = bill._currentUser.username;
          voidBillItem.comment = bill.voidComment;
          voidBillItem.openTime = kot.created;
          voidBillItem.deleteTime = bill.voidBillTime;
          voidBillItem.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");

          _kotsDeleteHistory.push(voidBillItem);

          /*Adding Add Ons*/
          if(_.has(voidBillItem,'addOns')) {
            _.forEach(voidBillItem.addOns,function(addon){
              addon.billNumber = bill.billNumber;
              addon.serialNumber = bill.serialNumber;
              addon.prefix = (bill.prefix != undefined? bill.prefix:'')
              addon.daySerialNumber = bill.daySerialNumber;
              addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
              addon._time = moment(bill._closeTime).format('hh:mm:ss a');
              addon.kotNumber = kot.kotNumber;
              addon.username = bill._currentUser.username;
              addon.comment = bill.voidComment;
              addon.openTime = kot.created;
              addon.deleteTime = bill.voidBillTime;
              addon.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");

              _kotsDeleteHistory.push(addon);
            })
          }
          /*End Add Ons*/

        });
      } else {

        if (kot.isVoid) {
          /* Get all Items from Void KOT */
          _.forEach(kot.items, function (voidKotItem) {
            voidKotItem.billNumber = bill.billNumber;
            voidKotItem.serialNumber = bill.serialNumber;
            voidKotItem.prefix = (bill.prefix != undefined? bill.prefix:'')
            voidKotItem.daySerialNumber = bill.daySerialNumber;
            voidKotItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
            voidKotItem._time = moment(bill._closeTime).format('hh:mm:ss a');
            voidKotItem.kotNumber = kot.kotNumber;
            voidKotItem.username = bill._currentUser.username;
            voidKotItem.comment = kot.comment;
            voidKotItem.openTime = kot.created;
            voidKotItem.deleteTime = kot.voidKotTime;
            voidKotItem.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");
            _kotsDeleteHistory.push(voidKotItem);
            /*Adding Add Ons*/
            if(_.has(voidKotItem,'addOns')) {
              _.forEach(voidKotItem.addOns,function(addon){
                addon.billNumber = bill.billNumber;
                addon.serialNumber = bill.serialNumber;
                addon.prefix = (bill.prefix != undefined? bill.prefix:'')
                addon.daySerialNumber = bill.daySerialNumber;
                addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                addon.kotNumber = kot.kotNumber;
                addon.username = bill._currentUser.username;
                addon.comment = kot.comment;
                addon.openTime = kot.created;
                addon.deleteTime = kot.voidKotTime;
                addon.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");

                _kotsDeleteHistory.push(addon);
              })
            }
            /*End Add Ons*/
          });

        } else {
          /* Get Items from Delete History */
          _.forEach(kot.deleteHistory, function (delItem){
            delItem.billNumber = bill.billNumber;
            delItem.serialNumber = bill.serialNumber;
            delItem.prefix = (bill.prefix != undefined? bill.prefix:'')
            delItem.daySerialNumber = bill.daySerialNumber;
            delItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
            delItem._time = moment(bill._closeTime).format('hh:mm:ss a');
            delItem.kotNumber = kot.kotNumber;
            delItem.username = bill._currentUser.username;
            delItem.openTime = kot.created;
            delItem.deleteTime = delItem.voidItemTime;
            delItem.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");
            _kotsDeleteHistory.push(delItem);
            /*Adding Add Ons*/
            if(_.has(delItem,'addOns')) {
              _.forEach(delItem.addOns,function(addon){
                addon.billNumber = bill.billNumber;
                addon.serialNumber = bill.serialNumber;
                addon.prefix = (bill.prefix != undefined? bill.prefix:'')
                addon.daySerialNumber = bill.daySerialNumber;
                addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                addon.kotNumber = kot.kotNumber;
                addon.username = bill._currentUser.username;
                addon.openTime = kot.created;
                addon.deleteTime = delItem.voidItemTime;
                addon.deleteUser = kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA");

                _kotsDeleteHistory.push(addon);
              })
            }
            /*End Add Ons*/
          });
        }
      }
    })
}



//to find on the basis of report types
exports.reportType= function(req, res) {
  //console.log(req.query.quotationId);
  //console.log("inside ");
  Report.find({deployment_id:req.params.id},function (err, report) {
    //console.log(report);
    if(err) { return handleError(res, err); }
    if(report ==null || report == undefined){
      return res.json({error:"error in report"})
    }
    return res.json(report);

  });
};
// Creates a new report in the DB.

exports.create = function (req, res) {
  Report.create(req.body, function (err, report) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(201, report);
  });
};

// Updates an existing report in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Report.findById(req.params.id, function (err, report) {
    if (err) {
      return handleError(res, err);
    }
    if (!report) {
      return res.send(404);
    }
    var updated = _.merge(report, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, report);
    });
  });
};

// Deletes a report from the DB.
exports.destroy = function (req, res) {
  Report.findById(req.params.id, function (err, report) {
    if (err) {
      return handleError(res, err);
    }
    if (!report) {
      return res.send(404);
    }
    report.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};

exports.getBillsForCallDetails=function(req,res){
  var deployment_id= mongoose.mongo.ObjectID(req.query.deployment_id);
  bill.find({deployment_id:deployment_id,_created: {$gte: new Date(req.query.fromDate), $lte:new Date(req.query.toDate)}},{billNumber:1,_created:1,_customer:1},function(err,data){
    if(data){return res.status(200).send(data);}
    return handleError(res,err);
  })
};

exports.callDetailsReport=function(req,res){

  try {
    var options = {
      method: 'POST',
      url: 'https://developers.myoperator.co/search',
      headers:{'content-type':'application/json',
        'accept':'application/json'},
      json:req.body
    };

    request(options, function (error, response, bod) {
      console.log(options);
      // console.log("status", response.statusCode);
      if(error) console.log("error",error);
      if (bod) {
        var body = typeof(bod) == 'object' ? (bod) : JSON.parse(bod);
        console.log("body",body);
        if (body.status == 'success' && body.code == '200')
          res.status(200).send(body);
        else
          return handleError(res, {status: 'failure'});
      }
      else return handleError(res, {status: 'failure'});
    });
  }
  catch(e){
    console.log("exception",e);
    return res.status(500).send({message:'An exception occurred'});
  }
};

exports.getCompanyId=function(req,res){
  try {
    var options = {
      method: 'GET',
      url: 'https://developers.myoperator.co/user?token='+req.query.token+'&page-size='+req.query['page-size'],
      headers:{'content-type':'application/json',
        'accept':'application/json'},
    };

    request(options, function (error, response, bod) {
      console.log(options);
      // console.log("status", response.statusCode);
      if(error) console.log("error",error);
      if (bod) {
        var body = typeof(bod) == 'object' ? (bod) : JSON.parse(bod);
        console.log("body",body);
        if (body.status == 'success' && body.data.length>0)
          res.status(200).send({id:body.data[0].company_id});
        else
          return handleError(res, {status: 'failure'});
      }
      else return handleError(res, {status: 'failure'});
    });
  }
  catch(e){
    console.log("exception",e);
    return res.status(500).send({message:'An exception occurred'});
  }
};

function handleError(res, err) {
  return res.send(500, err);
}