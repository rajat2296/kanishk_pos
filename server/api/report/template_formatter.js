var _ = require('lodash');
var mongoose=require('mongoose');
var fs = require('fs');
var file = ''
fs.readFile('report.controller.js', function(err,data) {
	file = convertToCSV(data.toString())
	console.log(file)
})

var convertToCSV = function(CSV){
  // CSV = CSV.replace(/,/g,',')
  CSV = CSV.replace(/<td\w*[a-zA-Z0-9=\\'"-:;# ]*>/g,'')
  CSV = CSV.replace(/<th\w*[a-zA-Z0-9=\\'"-:;# ]*>/g,'')
  CSV = CSV.replace(/<h2\w*[a-zA-Z0-9=\\'"-:;# ]*>/g,'')
  CSV = CSV.replace(/<table\w*[a-zA-Z=\\"'-:;#0-1 ]*>/g,'')
  CSV = CSV.replace(/<tfoot\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/<div\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/<caption\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/<thead\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/<tbody\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/<strong\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/<i\w*[a-zA-Z=\\'"-:;# ]*>/g,'')
  CSV = CSV.replace(/<tr\w*[a-zA-Z=\\'"-:;# ]*>/g,'')
  CSV = CSV.replace(/<span\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/<h[0-9]*\w*[a-zA-Z=\\"'-:;# ]*>/g,'')
  CSV = CSV.replace(/&nbsp;/g,'')
  CSV = CSV.replace(/&nbsp/g,'')
  CSV = CSV.replace(/<\/span>/g,'')
  CSV = CSV.replace(/<\/h[0-9]*>/g,'')
  CSV = CSV.replace(/<\/i>/g,'')
  CSV = CSV.replace(/<\/thead>/g,'')
  CSV = CSV.replace(/<\/tbody>/g,'')
  CSV = CSV.replace(/<\/strong>/g,'')
  CSV = CSV.replace(/<\/table>/g,'')
  CSV = CSV.replace(/<\/tfoot>/g,'')
  CSV = CSV.replace(/<\/caption>/g,'')
  CSV = CSV.replace(/<\/th>/g,',')
  CSV = CSV.replace(/<\/div>/g,',')
  CSV = CSV.replace(/<\/h2>/g,',')
  CSV = CSV.replace(/<\/td>/g,',')
  CSV = CSV.replace(/<\/tr>/g,"\\r\\n")
  fs.writeFile('reportTemplate',CSV, function(err) {
	})
  return CSV
};