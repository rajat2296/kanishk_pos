'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ReportSchema = new Schema({
   deployment_id: Schema.Types.ObjectId,
   tenant_id: Schema.Types.ObjectId,
   fromDate: {type:Date},
   date : {type:Date,default:Date.now()},
   toDate:{type:Date},
   filename:{type:String},
   email:{type:String},
   reportType:{type:String}
});

module.exports = mongoose.model('Report', ReportSchema);
