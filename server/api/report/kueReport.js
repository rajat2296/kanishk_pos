var bill = require('../bill/bill.controller.js');
var _ = require('lodash');
var moment = require('moment');
var async = require('async');
var mongoose = require('mongoose');
var request = require('request');
var Q = require('q');
var utils = require('../Utils/utils');
var Report = require('../report/report.controller');

module.exports = {
  doQueue: function(queue) {
    queue.process('getItemWiseEnterprise', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getItemWiseEnterprise(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,job.data.form.arg.deps,job.data.form.arg.discountValue);
      })
    });

    queue.process('getDataRangeBillsNew', 1, function(job , done) {
      job.data.backend ="csvTrue"
      var req={query:job.data}
      bill.getDataRangeBillsNew(req, {}, function(bills){
        done(null,{});
        Report.reportCalculation(bills, job.data.cutOffTimeSetting, job.data.deploymentSetting, job.data.reportName, job.data.fromDate, job.data.toDate, job.data.deployment_id, job.data.tenant_id, job.data.email, job.data.type,null,job.data.discountValue);
      })
    });

    queue.process('getComplimentary', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getComplimentary(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,job.data.form.arg.deps,job.data.form.arg.discountValue);          
      })
    });

    queue.process('getDiscountReports', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getDiscountReports(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getInstanceWiseReports', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getInstanceWiseReports(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getReprintReports', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getReprintReports(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getBillsInDateRange', 1, function(job,done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillsInDateRange(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);          
      })
    });

    queue.process('getItemWiseConsolidate', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getItemWiseConsolidate(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,job.data.form.arg.deps,job.data.form.arg.discountValue);
      })
    });

    queue.process('getItemWiseConsolidateForProfitSharing', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getItemWiseConsolidateForProfitSharing(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,job.data.form.arg.deps,job.data.form.arg.discountValue);    
      })
    });

    queue.process('getBillsInDateRangeForDeliveryReport', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillsInDateRangeForDeliveryReport(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getBillConsolidate', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillConsolidate(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getDataRangeBillsForInvoice', 1, function(job, done) {
      job.data.backend ="csvTrue"
      var req={query:job.data}
      bill.getDataRangeBillsForInvoice(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.cutOffTimeSetting, job.data.deploymentSetting, job.data.reportName, job.data.fromDate, job.data.toDate, job.data.deployment_id, job.data.tenant_id, job.data.email,null,job.data.dep,job.data.discountValue);
      })
    });

    queue.process('getDataRangeBillsForKot', 1, function(job, done) {
      job.data.backend ="csvTrue"
      var req={query:job.data}
      bill.getDataRangeBillsForKot(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculationsForKot(bills, job.data.cutOffTimeSetting, job.data.deploymentSetting, job.data.reportName, job.data.fromDate, job.data.toDate, job.data.deployment_id, job.data.tenant_id, job.data.email,null,null,job.data.discountValue);
      })
    });

    queue.process('getBillConsolidateForHourlySales', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillConsolidateForHourlySales(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getBillsInDateRangeForCustomerData', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillsInDateRangeForCustomerData(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getBillsInDateRangeForDelivery', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillsInDateRangeForDelivery(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getBillsInDateRangeForDailySales', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillsInDateRangeForDailySales(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,job.data.form.arg.sessions,job.data.form.arg.categories,job.data.form.arg.discountValue);
      })
    });

    queue.process('getBillConsolidateForKotDelete', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillConsolidateForKotDelete(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculationsForKotDelete(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getBillConsolidateForKotTracking', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getBillConsolidateForKotTracking(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);          
      })
    });

    queue.process('getAggregatedOtherPaymentsDetail', 1, function(job, done) {
      job.data.backend ="csvTrue"
      var req={query:job.data}
      bill.getAggregatedOtherPaymentsDetail(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.cutOffTimeSetting, job.data.deploymentSetting, job.data.reportName, job.data.startDate, job.data.endDate, job.data.deployment_id, job.data.tenant_id, job.data.email,null,null,job.data.discountValue);
      })
    });

    queue.process('getAggregatedOtherPaymentsConsolidated', 1, function(job, done) {
      job.data.backend ="csvTrue"
      var req={query:job.data}
      bill.getAggregatedOtherPaymentsConsolidated(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.cutOffTimeSetting, job.data.deploymentSetting, job.data.reportName, job.data.startDate, job.data.endDate, job.data.deployment_id, job.data.tenant_id, job.data.email,null,null,job.data.discountValue);
      })
    });

    queue.process('getRemovedTaxes', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getRemovedTaxes(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id,job.data.form.arg.email,null,job.data.form.arg.deps,job.data.form.arg.discountValue);
      })
    });

    queue.process('getComplimentaryHeadwise', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getComplimentaryHeadwise(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getSettlement', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getSettlement(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,job.data.form.arg.type,null,job.data.form.arg.discountValue);
      })
    });

    queue.process('getAggregatedCouponDetail', 1, function(job, done) {
      job.data.form.arg.backend = "csvTrue"
      var req={body:job.data.form}
      bill.getAggregatedCouponDetail(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);      
      })
    });

    queue.process('getAggregatedNewOtherPaymentsDetail', 1, function(job,done) {
      job.data.backend ="csvTrue"
      var req={query:job.data}
      bill.getAggregatedNewOtherPaymentsDetail(req, {}, function(bills){
        done(null,[]);
        Report.reportCalculation(bills, job.data.cutOffTimeSetting, job.data.deploymentSetting, job.data.reportName, job.data.startDate, job.data.endDate, job.data.deployment_id, job.data.tenant_id, job.data.email,null,null,job.data.discountValue);
      })
    });
    // queue.process('getDataRangeBillsForBoh', 1, function(job,done) {
    //   job.data.form.arg.backend = "csvTrue"
    //   var req={body:job.data.form}
    //   bill.getDataRangeBillsForBoh(req, {}, function(bills){
    //     done(null,[]);
    //     Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);      
    //   })
    // });

    // queue.process('getDataRangeBillsForDailySalesInvoice', 1, function(job,done) {
    //   job.data.form.arg.backend = "csvTrue"
    //   var req={body:job.data.form}
    //   bill.getDataRangeBillsForDailySalesInvoice(req, {}, function(bills){
    //     done(null,[]);
    //     Report.reportCalculation(bills, job.data.form.arg.cutOffTimeSetting, job.data.form.arg.deploymentSetting, job.data.form.arg.reportName, job.data.form.arg.startDate, job.data.form.arg.endDate, job.data.form.arg.deployment_id, job.data.form.arg.tenant_id, job.data.form.arg.email,null,null,job.data.form.arg.discountValue);      
    //   })
    // });
  }
}