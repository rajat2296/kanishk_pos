'use strict';

var _ = require('lodash');
var StockComment = require('./stockComment.model');

// Get list of stockComments
exports.index = function(req, res) {

  var query = {};
  if(!req.query.deployment_id)
    return handleError(res, "Deployment Id not found");
  else {
    query.deployment_id = req.query.deployment_id;
  }
  console.log(query);
  StockComment.find(query, function (err, stockComments) {
    console.log(err, stockComments);
    if(err) { return handleError(res, err); }
    return res.status(200).json(stockComments);
  });
};

// Get a single stockComment
exports.show = function(req, res) {
  StockComment.findById(req.params.id, function (err, stockComment) {
    if(err) { return handleError(res, err); }
    if(!stockComment) { return res.status(404).send('Not Found'); }
    return res.json(stockComment);
  });
};

// Creates a new stockComment in the DB.
exports.create = function(req, res) {
  StockComment.create(req.body, function(err, stockComment) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(stockComment);
  });
};

// Updates an existing stockComment in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  StockComment.findById(req.params.id, function (err, stockComment) {
    if (err) { return handleError(res, err); }
    if(!stockComment) { return res.status(404).send('Not Found'); }
    var updated = _.merge(stockComment, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(stockComment);
    });
  });
};

// Deletes a stockComment from the DB.
exports.destroy = function(req, res) {
  StockComment.findById(req.params.id, function (err, stockComment) {
    if(err) { return handleError(res, err); }
    if(!stockComment) { return res.status(404).send('Not Found'); }
    stockComment.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}