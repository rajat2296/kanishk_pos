'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StockCommentSchema = new Schema({
  description: {
    required: true,
    type:String
  },
  commentFor: {
    id: {
      type: Number,
      required: true
    },
    description: {
      type: String,
      required: true
    }
  },
  updated: {
    type: Date,
    default: Date.now()
  },
  created: {
    type: Date,
    default: Date.now()
  },
  deployment_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  tenant_id: {
    type: Schema.Types.ObjectId,
    required: true
  }
}, {versionKey: false});

StockCommentSchema.pre('save', function (next) {
  this.updated = new Date;
  next();
});

StockCommentSchema.path('description').validate(function(v, respond) {
  var self=this;
    this.constructor.findOne({'description':  new RegExp('^'+v+'$', "i"),'commentFor.id': this.commentFor.id,deployment_id:this.deployment_id}, function (err, comment) {
    var flag=true;
    if(err){throw err;}
    if(comment)
    {
      if(comment.description.toLowerCase()===self.description.toLowerCase())
      {
        if(comment.id!=self.id){
          flag=false;
        }
      }
    }
    respond(flag);
    });
  
}, 'This Comment already exists!');


module.exports = mongoose.model('StockComment', StockCommentSchema);