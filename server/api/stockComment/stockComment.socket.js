/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var StockComment = require('./stockComment.model');

exports.register = function(socket) {
  StockComment.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  StockComment.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('stockComment:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('stockComment:remove', doc);
}