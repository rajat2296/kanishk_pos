var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

exports.setup = function (User, Tenant, config) {
    passport.use(new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password', // this is the virtual field on the model
            passReqToCallback: true
        },
        function (req, username, password, done) {

            User.findOne({
                username: username.toLowerCase()
            }, function (err, user) {
                if (err) return done(err);

                if (!user) {
                    return done(null, false, { message: 'This username is not registered.' });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, { message: 'This password is not correct.' });
                }
                
                if (!user.isActivated()) {
                    return done(null, false, {message:"You are not activated."});
                }

                if (!user.belongsToSubdomain(req.body.subdomain)) {
                    return done(null, false, { message: "You don't belong here! :-("});
                }
                if(user.isCallcenterUser!=undefined){
                    if(user.isCallcenterUser){
                        return done(null,false,{message:"User not belong to application."});
                    }
                }
               // console.log(user);

                return done(null, user);
            });
        }
    ));
};