'use strict';

var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/environment');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
var User = require('../api/user/user.model');
var Partner = require('../api/partners/partners.model');
var validateJwt = expressJwt({secret: config.secrets.session});
var crypto = require('crypto'); 

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated() {
  return compose()
  // Validate jwt
    .use(function (req, res, next) {
      //console.log(   req.query);
      // allow access_token to be passed through query parameter as well
      if (req.query && req.query.hasOwnProperty('access_token')) {
        req.headers.authorization = 'Bearer ' + req.query.access_token;
      }
      validateJwt(req, res, next);
    })
    // Attach user to request
    .use(function (req, res, next) {
      User.findById(req.user._id, function (err, user) {
        if (err) return next(err);
        if (!user) return res.send(401);

        req.user = user;
        next();
      });
    });
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole(roleRequired) {
  if (!roleRequired) throw new Error('Required role needs to be set');

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
      if (config.userRoles.indexOf(req.user.role) >= config.userRoles.indexOf(roleRequired)) {
        next();
      }
      else {
        res.send(403);
      }
    });
}

/**
 * Returns a jwt token signed by the app secret
 */
function signToken(id) {

  return jwt.sign({_id: id}, config.secrets.session, {expiresInMinutes: (2*60*24)});

}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie(req, res) {
  if (!req.user) return res.json(404, {message: 'Something went wrong, please try again.'});
  var token = signToken(req.user._id, req.user.role);
  res.cookie('token', JSON.stringify(token));
  res.redirect('/');
}

/**
* Is open for third parties
*/

function decipherCustomerKey(customer_key, cb){
    var decipher = crypto.createDecipheriv('aes-128-cbc', "posist-open-apis", "@@@@&&&&####$$$$");
    try{
      var deployment_id = (decipher.update(customer_key, 'hex', 'utf8') + decipher.final('utf8')).split(":")[1];
      console.log(deployment_id);
      cb(deployment_id);
    }catch(err){
      cb(null);
    }
}
function hasCustomerKey(req, res, next){
  decipherCustomerKey(req.query.customer_key, function(deployment_id){
    if(deployment_id==null)
      res.status(403).json({"message":"You donot have permissions from this client.","code":109, "label":"customer_key_error"});
    else{
      Partner.findOne({deployment_id: deployment_id, customer_key:req.query.customer_key}, function(err, partner){
        if(err||!partner)
          res.status(403).json({"message":"You donot have permissions from this client.","code":109, "label":"customer_key_error"});
        else{
          req.query.tenant_id = partner.tenant_id
          req.query.deployment_id = partner.deployment_id;
          req.query.partner_name = partner.partner_name;
          next();
        }
      });
    }
  });
}

function onlyOpenApi(req, res, next){
  //console.log(req.ip+' trying to request.');
  //console.log(req.headers);
  if(config.white_ips.indexOf(req.headers['x-real-ip'])>-1)
    next();
  else
    res.status(403).send("You donot have permissions. Try a smarter way. :D");
}
function decipherOauthKey(req, res, next) {
  console.log(req.body);
  var decipher = crypto.createDecipheriv('aes-128-cbc', "posist-oauth-123", "@@@@&&&&####$$$$");
  var credentials = (decipher.update(req.body.key, 'hex', 'utf8') + decipher.final('utf8')).split(":");
  req.body.username=credentials[1];
  req.body.subdomain=credentials[2];
  req.body.password=credentials[3];
  next();
}

exports.isAuthenticated = isAuthenticated;
exports.hasRole = hasRole;
exports.signToken = signToken;
exports.setTokenCookie = setTokenCookie;
exports.hasCustomerKey = hasCustomerKey;
exports.onlyOpenApi = onlyOpenApi;
exports.decipherOauthKey = decipherOauthKey;