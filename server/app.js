/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');
var multer = require('multer');
var bodyParser = require('body-parser');
var cors=require('cors');
var cluster = require('cluster');
var os = require('os');
var async = require('async');
// var User = require('./api/user/user.model');
// var AttendanceRecord = require('./api/user/attendanceRecord.model');
// var _ = require('lodash');
// var ChangeSet = require('./api/user/changeSet.model');
// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
var db = mongoose.createConnection(config.mongo.uri, config.mongo.options);

// Populate DB with sample data
if (config.seedDB) {
    require('./config/seed');
}

// Setup server

function serve(port) {
    var app = express();

// Use multer for multipart/form-data
//app.use(multer({dest: './uploads/'}));

    /*Extend Limits to 50 mb*/
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    app.use(cors());
    app.disable('etag');

    var server = require('http').createServer(app);
    var socketio = require('socket.io').listen(server);
    //var redis = require('socket.io-redis');
  //  socketio.adapter(redis({ host: 'localhost', port: 6379 }));
    require('./config/socketio')(socketio);
    require('./config/express')(app);
    require('./routes')(app);
    server.listen(parseInt( port), config.ip, function () {
        console.log('Express server listening on %d, in %s mode', parseInt( port), app.get('env'));
    });
    exports = module.exports = app;
    
    var sql = require('mssql');
    // ChangeSet.findOne({isSynced:true},{},{ sort: { 'lastTimeStamp' : -1 } },function(err,result){
    //     console.log(result);
    //     if(result && result.lastTimeStamp){
    //         var fromDate = new Date(result.lastTimeStamp);
    //     }
    //     else{
    //         var fromDate = new Date('2000-01-01')
    //     }
    //     var toDate = new Date();
    //       fromDate = fromDate.getFullYear()+'-'+(fromDate.getMonth()+1)+'-'+fromDate.getDate()+' '+fromDate.getHours()+':'+fromDate.getMinutes()+':00';
    //       toDate = toDate.getFullYear()+'-'+(toDate.getMonth()+1)+'-'+toDate.getDate()+' '+toDate.getHours()+':'+toDate.getMinutes()+':00';
    //       console.log(fromDate,toDate)
    //       var temp1 = {
    //                     isSynced:false,
    //                     lastTimeStamp:new Date()
    //                 }
    //       var lastTimeStamp = temp1.lastTimeStamp;
    //       ChangeSet.create(temp1,function(err,success){
    //           //var timeStampId = success._id;
    //           console.log(success)
    //           sql.connect("mssql://abc:abc@134.213.156.80/eTimetracklite1").then(function(result1) {
    //             //console.log(result1);
    //             var query = "select UserId, DATEADD(MINUTE,-330,LogDate) AS LogDate, DeviceId from DeviceLogs where LogDate between '" +fromDate+"' AND '"+toDate+"';"
    //             console.log(query)
    //             new sql.Request().query(query).then(function(recordset) {
    //                 var attendanceReport = []
    //                 console.log(recordset.length)
    //                 var uniqueUsers = []
    //                 for(var i in recordset){
    //                     uniqueUsers[recordset[i].UserId]=0;
    //                 }
    //                 console.log(Object.keys(uniqueUsers));
    //                 User.find({'details.userId':{$in:Object.keys(uniqueUsers)}},'-salt -hashedPassword  -authId',function(err,users){
    //                    // console.log(users);
    //                     _.forEach(users, function (user) {
    //                         _.forEach(recordset, function (record) {
    //                             if(record.UserId == user.details.userId){
    //                                 var temp = {
    //                                     firstname : user.firstname,
    //                                     username : user.username,
    //                                     email : user.email,
    //                                     mobile : user.mobile,
    //                                     userId : user.details.userId,
    //                                     LogDate : record.LogDate,
    //                                     DeviceId : record.DeviceId,
    //                                     deployment_id : user.deployment_id,
    //                                     tenant_id : user.tenant_id,
    //                                     shifts:user.details.assignedShift
    //                                 }
    //                                 attendanceReport.push(temp);

    //                             }
    //                         })
    //                     })
    //                     console.log(attendanceReport.length)
    //                     AttendanceRecord.create(attendanceReport,function(err,result){
    //                         ChangeSet.findOneAndUpdate({_id:success._id},{isSynced:true},function(err,res){
    //                             console.log(err,res)
    //                         });              
    //                     })

    //                 })
                    
    //             }).catch(function(err) {
    //               console.log(err) 
    //             });
    //           })
    //       })  
    // })
    //})
    

}
serve(2000);
//serve(9001);
//serve(9002);
// Expose app


/*Tried implementing clustering but socket problem came in*/

/**
 * Main application file
 *//*


 'use strict';

 // Set default node environment to development
 process.env.NODE_ENV = process.env.NODE_ENV || 'development';
 var cluster=require('cluster');

 if(cluster.isMaster){
 var server = require('http').createServer();
 var socketio = require('socket.io').listen(server);
 require('./config/socketio')(socketio);
 var cpuCount=require('os').cpus().length;
 for(var i=0;i<cpuCount;i++){
 cluster.fork();
 }
 cluster.on('exit',function(worker){
 console.log('Worker'+worker.id+'died;');
 cluster.fork();
 })
 }else {


 // Connect to database
 if(cluster.isWorker) {

 }


 // Setup server
 //var app = express();

 // Use multer for multipart/form-data
 //app.use(multer({dest: './uploads/'}));

 */
/*Extend Limits to 50 mb*//*



 if(cluster.isWorker) {
 var express = require('express');
 var mongoose = require('mongoose');
 var config = require('./config/environment');
 var multer = require('multer');
 var bodyParser = require('body-parser');
 mongoose.connect(config.mongo.uri, config.mongo.options);
 // Populate DB with sample data
 if (config.seedDB) {
 require('./config/seed');
 }

 var app = express();
 app.use(bodyParser.json({limit: '50mb'}));
 app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

 var server = require('http').createServer(app);
 var socketio = require('socket.io').listen(server);
 // require('./config/socketio')(socketio);

 //////////////////////////////////////////////Socket to remove//////////////////

 /////////////////////////////////////////////Done//////////////////////////////

 }
 require('./config/express')(app);
 require('./routes')(app);

 // Start server
 server.listen(config.port, config.ip, function () {
 console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
 });
 }
 // Expose app
 exports = module.exports = app;
 */
