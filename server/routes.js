/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function (app) {
    app.use('/api/shopify', require('./api/shopify'));
    app.use('/api/editTransactionHistorys', require('./api/editTransactionHistory'));
    app.use('/api/vendorPaymentHistorys', require('./api/vendorPaymentHistory'));
    app.use('/api/stockRequirements', require('./api/stockRequirement'));
    app.use('/api/deploymentclones', require('./api/deploymentclone'));
    app.use('/api/reports', require('./api/report'));
    app.use('/api/uploads', require('./api/upload'));
    app.use('/api/stations', require('./api/station'));
    app.use('/api/emails', require('./api/email'));
    app.use('/api/PosToDLFs', require('./api/PosToDLF'));
    app.use('/api/PosToMIs', require('./api/PosToMI'));
    app.use('/api/customers', require('./api/customer'));
    app.use('/api/stockRecipes', require('./api/stockRecipe'));
    app.use('/api/stockPropertys', require('./api/stockProperty'));
    app.use('/api/vendors', require('./api/vendor'));
    app.use('/api/receivers', require('./api/receiver'));
    app.use('/api/stockTransactions', require('./api/stockTransaction'));
    app.use('/api/stockItems', require('./api/stockItem'));
    app.use('/api/stockUnits', require('./api/stockUnit'));
    app.use('/api/baseKitchenUnits', require('./api/baseKitchenUnit'));
    app.use('/api/baseKitchenItems', require('./api/baseKitchenItem'));
    app.use('/api/stockCategorys', require('./api/stockCategory'));
    app.use('/api/stock/stores', require('./api/store'));
    app.use('/api/bills', require('./api/bill'));
    app.use('/api/dashboards', require('./api/dashboard'));
    app.use('/api/stockReports', require('./api/stockReport'));
    app.use('/api/stockReportNews', require('./api/stockReportNew'));
    app.use('/api/stockReportResetTime', require('./api/stockReportResetTime'));
    app.use('/api/stockConsumptionReport', require('./api/stockConsumptionReport'));
    app.use('/api/stockMasterUsers',require('./api/stockMasterUser'));
    app.use('/api/roles', require('./api/role'));
    app.use('/api/items', require('./api/item'));
    app.use('/api/superCategorys', require('./api/superCategory'));
    app.use('/api/deployments', require('./api/deployment'));
    app.use('/api/offers', require('./api/offer'));
    app.use('/api/datelists', require('./api/dateList'));
    app.use('/api/offercodes', require('./api/offerCode'));
    app.use('/api/taxs', require('./api/tax'));
    app.use('/api/categorys', require('./api/category'));
    app.use('/api/tabs', require('./api/tab'));
    app.use('/api/tenants', require('./api/tenant'));
    app.use('/api/things', require('./api/thing'));
    app.use('/api/users', require('./api/user'));
    app.use('/api/codeapplieds', require('./api/codeApplied'));
    app.use('/api/onlineOrders', require('./api/onlineOrder'));
    app.use('/api/mobileApps', require('./api/mobileApp'));
    app.use('/api/smstemplates', require('./api/smstemplate'));
    app.use('/api/smstransactions', require('./api/smstransaction'));
    app.use('/api/mobileauths', require('./api/mobileauth'));
    app.use('/api/favouriteOrders', require('./api/favouriteOrder'));
    app.use('/api/emailers', require('./api/emailer'));
    app.use('/api/expenseCategories', require('./api/expenseCategories'));
    app.use('/api/expenses', require('./api/expense'));
    app.use('/api/settles', require('./api/settle'));
    app.use('/api/advancebookings', require('./api/advancebooking'));
    app.use('/api/feedbacks', require('./api/feedback'));
    app.use('/api/leadForms', require('./api/leadForm'));
    app.use('/api/syncKots', require('./api/syncKot'));
    app.use('/api/accounts', require('./api/accounts'));
    app.use('/api/notifications', require('./api/notification'));
    app.use('/api/intercom', require('./api/intercom'));
    app.use('/api/nomnom', require('./api/nomnom'));
    app.use('/api/payments', require('./api/payments'));
    app.use('/api/mobikwikSettings', require('./api/mobikwikSettings'));
    app.use('/api/billCardTransactions', require('./api/billCardTransaction'));
    app.use('/api/urbanPiper', require('./api/urbanPiper'));
    app.use('/api/appVersion', require('./api/appVersion'));
    app.use('/api/appVersion2', require('./api/appVersion2'));
    app.use('/api/freshDesks', require('./api/freshDesk'));
    app.use('/api/purchaseOrders', require('./api/purchaseOrder'));
    app.use('/api/outletIndents', require('./api/outletIndent'));
    app.use('/api/forums', require('./api/Forum'));
    app.use('/api/partners', require('./api/partners'));
    app.use('/api/partnerDetails', require('./api/partnerDetails'));
    app.use('/api/technopurple',require('./api/technopurple'));
    app.use('/api/demands',require('./api/demand'));
    app.use('/api/sections',require('./api/section'));
    app.use('/api/dayBreakup',require('./api/dayBreakup'));
    app.use('/api/complementaryHead',require('./api/complimentaryHead'));
    app.use('/auth', require('./auth'));
    //app.use('/api/socket',require('./api/socket'));
    app.use('/api/sections',require('./api/section'));
    app.use('/api/dayBreakup',require('./api/dayBreakup'));

    app.use('/api/zomatoTrace',require('./api/zomatoTrace'))
    //app.use('/api/stockAggregate', require('./api/stockAggregate'));
    app.use('/api/delivery',require('./api/delivery'))
    app.use('/api/charges',require('./api/charges'))
    app.use('/api/kdsSettings',require('./api/kdsSettings'))
    app.use('/api/comment',require('./api/comment'));
    app.use('/api/stockComments',require('./api/stockComment'));

    app.use('/api/stockAggregate', require('./api/stockAggregate'));
    app.use('/api/onlineOrderSettings', require('./api/onlineOrderSettings'));
    app.use('/api/btc',require('./api/btc'));
    app.use('/api/onlineOrderSettings', require('./api/onlineOrderSettings'));

    app.use('/api/attendanceReport', require('./api/attendanceReport'));
    app.use('/api/shiftTime', require('./api/shiftTime'));
    app.use('/api/oauth', require('./api/oauth'));
    app.use('/api/unsettledAmount', require('./api/unsettledAmount'));
    app.post('/seatcustomer',/*require('./api/syncKot/SyncKot.controller').injectSocket*/require('./api/syncKot/syncKot.controller').seatcustomer_listen);
    app.use('/api/ledgers',require('./api/ledgers'));
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function (req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
