'use strict';

angular.module('posistApp')
    .controller('NavbarCtrl', function ($rootScope, $scope, $location, Auth, subdomain, localStorageService, $aside, $state, Utils) {

        $scope.menu = [
            {
                'title': 'Dashboard',
                'link': '/dashboard'
            }
        ];

        $scope.showReportAside = function () {

            $aside.open({
                templateUrl: 'app/reports/aside_menuReports.html',
                placement: 'left',
                size: 'lg',
                backdrop: true,
                controller: ['$scope', '$modalInstance', '$state', function($scope, $modalInstance, $state) {
                    $scope.gotoState = function (e, stateUrl) {
                        $state.go(stateUrl);
                        //e.stopPropagation();
                        $modalInstance.close("Clicked & Closed");
                    };
                    $scope.ok = function(e, stateUrl) {
                        $modalInstance.close("Clicked");
                        e.stopPropagation();
                    };
                    $scope.cancel = function(e) {
                        $modalInstance.dismiss();
                        e.stopPropagation();
                    };
                }]
            }).result.then(function (err) {
                console.log(err);
            }, function (err) {
                console.log(err);
            });


        };

        $scope.currentUser = localStorageService.get('currentUser');

        $scope.subdomain = subdomain;
       /* console.log($scope.subdomain);*/

        $scope.isCollapsed = true;
        $scope.isLoggedIn = Auth.isLoggedIn;
        $scope.isAdmin = Auth.isAdmin;

        $scope.isUser = function () {
        //    console.log($scope.currentUser);
            var flag=false;
            if($scope.currentUser!=null) {
                if ($scope.currentUser.role === 'user') {
                   // flag = true;
                    if(Utils.hasUserPermission($scope.currentUser.selectedPermissions,'Menu')||Utils.hasUserPermission($scope.currentUser.selectedPermissions,'Reports') ||Utils.hasUserPermission($scope.currentUser.selectedPermissions,'Settings')){
                      //  flag = true;
                    }
                }else{
                    flag =true;
                }
            }

            return flag;
        };
        $scope.isUserPermission = function (permissionName) {

            if ($scope.currentUser.role === 'superadmin') {
                return true;
            } else {
                return Utils.hasUserPermission($scope.currentUser.selectedPermissions,permissionName);
            }
        };
        $scope.isPermission=function(permissions){
            // console.log(currentUser);
            var flag = false;
            if ($scope.currentUser.role === 'user') {
                // flag= true;
                //   } else {
                var permissionArr = permissions.split(',');
                // console.log(permissionArr);
                _.forEach(permissionArr, function (permname) {
                    if (Utils.hasPermission($scope.currentUser.selectedPermissions, permname)) {
                        flag = true;
                    }
                })
            }else{
                flag=true;
            }
            return flag;
        }
        $scope.isGroupPermission=function(permissions){
            // console.log(currentUser);
            var flag = false;
            if ($scope.currentUser.role === 'user') {
                // flag= true;
                //   } else {
                var permissionArr = permissions.split(',');
                // console.log(permissionArr);
                _.forEach(permissionArr, function (permname) {
                    if (Utils.hasUserPermission($scope.currentUser.selectedPermissions, permname)) {
                        flag = true;
                    }
                })
            }else{
                flag=true;
            }
            return flag;
        }
        $scope.showSignupButton = function () {
            return ($scope.subdomain === null) ? true : false;
        };

        Auth.isLoggedInAsync(function (isLoggedIn) {
            if (isLoggedIn) {
                //$scope.currentUser = Auth.getCurrentUser();
            }
        });


        $scope.logout = function () {
            Auth.logout();
            $location.path('/login');

        };

        $scope.isActive = function (route) {
            return route === $location.path();
        };
    });
