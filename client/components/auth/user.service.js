'use strict';

angular.module('posistApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id/:controller', {
        id: '@_id'
      },
      {
        changePassword: {
          method: 'PUT',
          params: {
            controller: 'password'
          }
        },
        get: {
          method: 'GET',
          params: {
            id: 'me'
          }
        },
        all: {
          method: 'GET',
          params: {
            controller: 'all'
          },
          isArray: true

        },
        syncAll: {
          method: 'GET',
          params: {
            controller: 'syncAll'
          },
          isArray: true

        },

        getUserByCodeDep: {
          method: 'GET',
          params: {
            controller: 'getUserByCodeDep'
          },
          isArray: true

        },
        getActivationLink: {
          method: 'GET',
          params: {
            controller: 'getActivationLink'
          }
        },
        getUsersWaitersByTenant: {
          method: 'GET',
          params: {
            controller: 'getUsersWaitersByTenant'
          },
          isArray: true
        },
        getUsersAttendance: {
          method: 'GET',
          params: {
            controller: 'getUsersAttendance'
          },
          isArray: true
        },
        getSingleUserAttendance: {
          method: 'GET',
          params: {
            controller: 'getSingleUserAttendance'
          },
          isArray: true
        }
      });
  });
