'use strict';

angular.module('posistApp')
  .factory('stockUnit', function ($resource) {
    return $resource('/api/stockUnits/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      }
    );
  });
