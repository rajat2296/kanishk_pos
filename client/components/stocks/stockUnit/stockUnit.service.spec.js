'use strict';

describe('Service: stockUnit', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockUnit;
  beforeEach(inject(function (_stockUnit_) {
    stockUnit = _stockUnit_;
  }));

  it('should do something', function () {
    expect(!!stockUnit).toBe(true);
  });

});
