'use strict';

angular.module('posistApp')
  .factory('StockTransaction', function ($q, $resource, $timeout, $http, $webSql, $modal, $state, Utils, $rootScope, growl, localStorageService) {
    // Define the constructor function.
    function StockTransaction(currentUser, db, items, store, category, vendor, receiver, transactionType, transactionNumber, id, daySerialNumber, invoiceNumber, batchNumber, charges) {
      var self = this;
      /*self.mode = 'new';*/
      //self.db = db || angular.noop;
      self._id = id || guid();
      self.transactionId = self._id;
      self._localId = undefined; // WebSQL ID
      self._currentUser = currentUser || "";
      self._items = items || [];
      self._store = store || {};
      self._vendor = vendor || {};
      self._category = category || {};
      self._receiver = receiver || {};
      self._transactionType = parseFloat(transactionType) || "";
      self.transactionType = parseFloat(transactionType) || "";
      self._transactionNumber = transactionNumber || "";
      self.transactionNumber = transactionNumber || "";
      self._daySerialNumber = "";
      self.daySerialNumber = daySerialNumber || "";
      self.invoiceNumber = invoiceNumber || "";
      self.batchNumber = batchNumber || "";
      self.charges = charges || [];
      self.deployment_id = localStorageService.get('deployment_id');
      self.tenant_id = localStorageService.get('tenant_id');
      self.resource = $resource('/api/stockTransactions/:id',
        {
          id: "@_id"
        },
        {
          saveData: {method: 'POST', isArray: true},
          get: {method: 'GET', isArray: true},
          update: {method: 'PUT'}
        }
      );
      // Synced
      self.isSynced = false;
      self.syncedOn = "";
      self._created = new Date().toISOString();
      self.isOpen = true;
      self._data = null;
      //setDaySerialNumberAndTransactionNumber(self).then(function (bill) {
      //  self.daySerialNumber = bill.daySerialNumber;
      //  self.transactionNumber = bill.transactionNumber;
      //});
    };


    var guid = (function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }

      return function () {
        return (s4() + s4()
        + '-' +
        s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4()
        + '-'+
        s4() + s4() + s4());
      };
    })();


    StockTransaction.prototype = {
       basePrefferedAnsSelectedUnitsByItems: function (eitems, selectedUnits) {
        console.log(selectedUnits);
        var items = angular.copy(eitems);
        _.forEach(items, function (item, i) {
          console.log('item ',item);
          if (item.qty != "" || item.qty != undefined || !isNaN(item.qty)) {
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            _.forEach(selectedUnits, function (u, ii) {
              if (ii == item._id) {
                selectedConversionFactor = u.conversionFactor;
                var selectedUnit = {
                  "_id": u._id,
                  "unitName": u.unitName,
                  "baseQty": parseFloat(item.qty),
                  "type": "selectedUnit",
                  "conversionFactor": parseFloat(u.conversionFactor)
                };
                item["selectedUnit1"] = selectedUnit;

                if (item.preferedUnit == undefined) {
                  var pUnit = {
                    "_id": u._id,
                    "unitName": u.unitName,
                    "baseQty": parseFloat(item.qty),
                    "type": "preferedUnit",
                    "conversionFactor": parseFloat(u.conversionFactor)
                  };
                  item["preferedUnits"] = pUnit;
                  item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
                }
                /*else {
                  var pUnit = {
                    "_id": u._id,
                    "unitName": u.unitName,
                    "baseQty": parseFloat(item.qty),
                    "type": "preferedUnit",
                    "conversionFactor": parseFloat(u.conversionFactor)
                  };
                  item["preferedUnits"] = pUnit;
                  item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
                }*/
              }
            });
            var baseUnit = {};
            console.log(item);
            if (item.units[0].baseUnit != undefined) {
              baseUnit = item.units[0].baseUnit;
              baseUnit.unitName = baseUnit.name;
            }
            else {
              var ind = _.findIndex(item.units, {"type": "baseUnit"});
              baseUnit = item.units[ind];
              baseUnit.unitName = baseUnit.name;
            }

            var baseQty = parseFloat(item.qty * selectedConversionFactor);
            baseUnit.baseQty = baseQty;
            baseUnit.type = "baseUnit";
            baseUnit.conversionFactor = conversionFactor;
            item.baseUnit = baseUnit;

            var pInd = _.findIndex(item.units, {_id: item.preferedUnit});
            if (pInd >= 0) {
              var preferedUnits = angular.copy(item.units[pInd]);
              var preferedconversionFactor = item.units[pInd].conversionFactor;
              if (preferedconversionFactor == conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
              }
              else if (preferedconversionFactor > conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
              }
              else if (preferedconversionFactor < conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
              }
              preferedUnits.type = "preferedUnit";
              delete preferedUnits.baseUnit;
              preferedUnits.conversionFactor = preferedconversionFactor;
              item.preferedUnits = preferedUnits;
            }
            if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
              var cf = 1000;
              if (selectedConversionFactor >= cf) {
                item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
              }
              else if (cf > selectedConversionFactor) {
                item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
              }
              ;
            }
            ;
            item.calculateInUnits = [];
            item.calculateInUnits.push(item.baseUnit);
            item.calculateInUnits.push(item.preferedUnits);
            item.calculateInUnits.push(item.selectedUnit1);
          }
        });
        return items;
      },
      basePrefferedAnsSelectedUnitsByItemsWithCheck: function (eitems, selectedUnits, stockUnits) {
        console.log(selectedUnits);
        var items = angular.copy(eitems);
        _.forEach(items, function (item, i) {
          console.log('item ',item);
          if (item.qty != "" || item.qty != undefined || !isNaN(item.qty)) {
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            _.forEach(selectedUnits, function (u, ii) {
              if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
              {
                var ind = _.findIndex(stockUnits, {_id: u._id});
                if(ind >= 0)
                {
                  u.baseUnit = stockUnits[ind].baseUnit;
                  u.conversionFactor = stockUnits[ind].conversionFactor;
                  u.baseConversionFactor = stockUnits[ind].baseConversionFactor;
                }  
              }
              if (ii == item._id) {
                selectedConversionFactor = u.conversionFactor;
                var selectedUnit = {
                  "_id": u._id,
                  "unitName": u.unitName,
                  "baseQty": parseFloat(item.qty),
                  "type": "selectedUnit",
                  "conversionFactor": parseFloat(u.conversionFactor)
                };
                item["selectedUnit1"] = selectedUnit;

                if (item.preferedUnit == undefined) {
                  var pUnit = {
                    "_id": u._id,
                    "unitName": u.unitName,
                    "baseQty": parseFloat(item.qty),
                    "type": "preferedUnit",
                    "conversionFactor": parseFloat(u.conversionFactor)
                  };
                  item["preferedUnits"] = pUnit;
                  item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
                }
                /*else {
                  var pUnit = {
                    "_id": u._id,
                    "unitName": u.unitName,
                    "baseQty": parseFloat(item.qty),
                    "type": "preferedUnit",
                    "conversionFactor": parseFloat(u.conversionFactor)
                  };
                  item["preferedUnits"] = pUnit;
                  item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
                }*/
              }
            });
            var baseUnit = {};
            console.log(item);
            if (item.units[0].baseUnit != undefined) {
              baseUnit = item.units[0].baseUnit;
              baseUnit.unitName = baseUnit.name;
            }
            else {
              console.log('inside else');
              var ind = _.findIndex(item.units, {"type": "baseUnit"});
              if(ind >= 0)
              {
                console.log('inside ind >=0');
              baseUnit = item.units[ind];
              baseUnit.unitName = baseUnit.name;
              }
              else
              {
              console.log('inside else of ind >=0');
              var ind = _.findIndex(stockUnits, {_id: item.units[0]._id});
              if(ind >= 0)
              baseUnit = stockUnits[ind].baseUnit;
              baseUnit.unitName = baseUnit.name;  
              }
            }

            var baseQty = parseFloat(item.qty * selectedConversionFactor);
            baseUnit.baseQty = baseQty;
            baseUnit.type = "baseUnit";
            baseUnit.conversionFactor = conversionFactor;
            item.baseUnit = baseUnit;

            var pInd = _.findIndex(item.units, {_id: item.preferedUnit});
            if (pInd >= 0) {
              var preferedUnits = angular.copy(item.units[pInd]);
              var preferedconversionFactor = item.units[pInd].conversionFactor;
              if (preferedconversionFactor == conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
              }
              else if (preferedconversionFactor > conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
              }
              else if (preferedconversionFactor < conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
              }
              preferedUnits.type = "preferedUnit";
              delete preferedUnits.baseUnit;
              preferedUnits.conversionFactor = preferedconversionFactor;
              item.preferedUnits = preferedUnits;
            }
            if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
              var cf = 1000;
              if (selectedConversionFactor >= cf) {
                item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
              }
              else if (cf > selectedConversionFactor) {
                item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
              }
              ;
            }
            ;
            item.calculateInUnits = [];
            item.calculateInUnits.push(item.baseUnit);
            item.calculateInUnits.push(item.preferedUnits);
            item.calculateInUnits.push(item.selectedUnit1);
          }
        });
        return items;
      },
      basePrefferedAnsSelectedUnitsByItemsStockTransfer: function (eitems, selectedUnits) {
        console.log(selectedUnits);
        var items = angular.copy(eitems);
        _.forEach(items, function (item, i) {
          if (item.qty != "" || item.qty != undefined || !isNaN(item.qty)) {
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            _.forEach(selectedUnits, function (u, ii) {
              if (ii == item._id) {
                selectedConversionFactor = u.conversionFactor;
                var selectedUnit = {
                  "_id": u._id,
                  "unitName": u.unitName,
                  "baseQty": parseFloat(item.qty),
                  "type": "selectedUnit",
                  "conversionFactor": parseFloat(u.conversionFactor)
                };
                item["selectedUnit1"] = selectedUnit;

                if (item.preferedUnit == undefined) {
                  var pUnit = {
                    "_id": u._id,
                    "unitName": u.unitName,
                    "baseQty": parseFloat(item.qty),
                    "type": "preferedUnit",
                    "conversionFactor": parseFloat(u.conversionFactor)
                  };
                  item["preferedUnits"] = pUnit;
                  item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
                }
              }
            });
            var baseUnit = {};
            if (item.units[0].baseUnit != undefined) {
              baseUnit = item.units[0].baseUnit;
              baseUnit.unitName = baseUnit.name;
            }
            else {
              var ind = _.findIndex(item.units, {"type": "baseUnit"});
              baseUnit = item.units[ind];
              baseUnit.unitName = baseUnit.name;
            }

            var baseQty = parseFloat(item.qty);
            baseUnit.baseQty = baseQty;
            baseUnit.type = "baseUnit";
            baseUnit.conversionFactor = conversionFactor;
            item.baseUnit = baseUnit;

            var pInd = _.findIndex(item.units, {_id: item.preferedUnit});
            if (pInd >= 0) {
              var preferedUnits = angular.copy(item.units[pInd]);
              var preferedconversionFactor = item.units[pInd].conversionFactor;
              if (preferedconversionFactor == conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
              }
              else if (preferedconversionFactor > conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
              }
              else if (preferedconversionFactor < conversionFactor) {
                preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
              }
              preferedUnits.type = "preferedUnit";
              delete preferedUnits.baseUnit;
              preferedUnits.conversionFactor = preferedconversionFactor;
              item.preferedUnits = preferedUnits;
            }
            if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
              var cf = 1000;
              if (selectedConversionFactor >= cf) {
                item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty);
              }
              else if (cf > selectedConversionFactor) {
                item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty);
              }
              ;
            }
            ;
            item.calculateInUnits = [];
            item.calculateInUnits.push(item.baseUnit);
            item.calculateInUnits.push(item.preferedUnits);
            item.calculateInUnits.push(item.selectedUnit1);
          }
        });
        return items;
      },
      calculateForProcessedAndSemiProcessed_sale: function (items) {
        if (items.itemType == "MenuItem" || items.itemType == "IntermediateItem") {
          // _.forEach(items.receipeDetails,function(r,i){
          //     r.baseQuantity=parseFloat(parseFloat(r.quantity)*parseFloat(r.selectedUnitId.conversionFactor)).toFixed(3);
          // });

          _.forEach(items.rawItems, function (item, i) {
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            var selectedUnit1 = {};
            if (item.selectedUnitId != undefined) {
              selectedConversionFactor = item.selectedUnitId.conversionFactor;
              selectedUnit1 = {
                "_id": item.selectedUnitId._id,
                "unitName": item.selectedUnitId.unitName,
                "type": "selectedUnit",
                "conversionFactor": parseFloat(item.selectedUnitId.conversionFactor)
              };
              selectedUnit1.baseQty = parseFloat(item.baseQuantity) * parseFloat(items.qty);
            }
            var onePortion = parseFloat(items.menuQty) / parseFloat(items.recipeQty);
            var noOfPlates = parseFloat(items.recipeQty) / parseFloat(items.menuQty);
            var qtyOf1Plate = parseFloat(item.baseQuantity) / parseFloat(noOfPlates);
            if (items.selectedUnit == undefined) {
              items.selectedUnit = items.selectedUnit1;
            }
            var rawQty = parseFloat(items.qty) * parseFloat(items.selectedUnit.conversionFactor);
            var noOfPlatesMade = parseFloat(rawQty) / parseFloat(items.menuQty);
            //var noOfquntityUsed=parseFloat(parseFloat(items.qty)*parseFloat(item.baseQuantity)*parseFloat(onePortion)).toFixed(3);//parseFloat(items.quantity);
            var noOfquntityUsed = parseFloat(parseFloat(noOfPlatesMade) * parseFloat(qtyOf1Plate)).toFixed(3);
            var baseUnit = {};
            baseUnit = angular.copy(item.selectedUnitId.baseUnit);
            baseUnit.unitName = baseUnit.name;
            baseUnit.type = "baseUnit";
            //if(conversionFactor==selectedConversionFactor){
            baseUnit.baseQty = parseFloat(noOfquntityUsed);
            // }
            // else if(conversionFactor<selectedConversionFactor){
            //     baseUnit.baseQty=parseFloat(noOfquntityUsed)*parseFloat(selectedConversionFactor);
            // }
            // else if(conversionFactor>selectedConversionFactor){
            //     baseUnit.baseQty=parseFloat(noOfquntityUsed)/parseFloat(selectedConversionFactor);
            // }

            var preferedUnits = {};
            if (item.preferedUnit != undefined) {
              var pInd = _.findIndex(item.units, {_id: item.preferedUnit._id});
              if (pInd >= 0) {
                preferedUnits = angular.copy(item.units[pInd]);
                var preferedconversionFactor = item.units[pInd].conversionFactor;
                console.log("sFactor_Menu" + selectedConversionFactor);
                console.log("pFactor_Menu" + preferedconversionFactor);
                if (preferedconversionFactor == selectedConversionFactor) {
                  preferedUnits = angular.copy(selectedUnit1);
                  preferedUnits.type = "preferedUnit";
                }
                else if (preferedconversionFactor > selectedConversionFactor) {
                  preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                }
                else if (preferedconversionFactor < selectedConversionFactor) {
                  preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                }
              }
            }
            else {
              preferedUnits = angular.copy(selectedUnit1);
              preferedUnits.type = "preferedUnit";
            }
            if (baseUnit.id == 2 || baseUnit.id == 3) {
              var cf = 1000;
              if (selectedConversionFactor == cf) {
                baseUnit.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(cf);
              }
              else if (selectedConversionFactor > cf) {
                baseUnit.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(cf);
              }
              else if (cf > selectedConversionFactor) {
                baseUnit.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(cf);
              }
              ;
            }
            ;
            item.calculateInUnits = [];
            item.calculateInUnits.push(baseUnit);
            item.calculateInUnits.push(preferedUnits);
            item.calculateInUnits.push(selectedUnit1);
            console.log(item);
          });
          return items;
        }
      },
      calculateForRecipeDetails: function (items) {
        //if(items.itemType=="MenuItem" || items.itemType=="IntermediateItem"){
        // _.forEach(items.receipeDetails,function(r,i){
        //     r.baseQuantity=parseFloat(parseFloat(r.quantity)*parseFloat(r.selectedUnitId.conversionFactor)).toFixed(3);
        // });
        var self = this;
        _.forEach(items.receipeDetails, function (item, i) {
          if (items.itemType != "MenuItem" && items.itemType != "IntermediateItem") {
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            var selectedUnit1 = {};
            if (item.baseQuantity == undefined) {
              item.baseQuantity = parseFloat(item.quantity) * parseFloat(item.selectedUnitId.conversionFactor);
            }
            if (item.selectedUnitId != undefined) {
              selectedConversionFactor = item.selectedUnitId.conversionFactor;
              selectedUnit1 = {
                "_id": item.selectedUnitId._id,
                "unitName": item.selectedUnitId.unitName,
                "type": "selectedUnit",
                "conversionFactor": parseFloat(item.selectedUnitId.conversionFactor)
              };
              selectedUnit1.baseQty = parseFloat(item.baseQuantity) * parseFloat(items.qty);
            }
            var onePortion = parseFloat(items.menuQty) / parseFloat(items.recipeQty);
            var noOfPlates = parseFloat(items.recipeQty) / parseFloat(items.menuQty);
            var qtyOf1Plate = parseFloat(item.baseQuantity) / parseFloat(noOfPlates);
            if (items.selectedUnit == undefined) {
              items.selectedUnit = items.selectedUnit1;
            }
            var rawQty = parseFloat(items.qty) * parseFloat(items.selectedUnit.conversionFactor);
            var noOfPlatesMade = parseFloat(rawQty) / parseFloat(items.menuQty);
            //var noOfquntityUsed=parseFloat(parseFloat(items.qty)*parseFloat(item.baseQuantity)*parseFloat(onePortion)).toFixed(3);//parseFloat(items.quantity);
            var noOfquntityUsed = parseFloat(parseFloat(noOfPlatesMade) * parseFloat(qtyOf1Plate)).toFixed(3);
            var baseUnit = {};
            baseUnit = angular.copy(item.selectedUnitId.baseUnit);
            baseUnit.unitName = baseUnit.name;
            baseUnit.type = "baseUnit";
            //if(conversionFactor==selectedConversionFactor){
            baseUnit.baseQty = parseFloat(noOfquntityUsed);
            // }
            // else if(conversionFactor<selectedConversionFactor){
            //     baseUnit.baseQty=parseFloat(noOfquntityUsed)*parseFloat(selectedConversionFactor);
            // }
            // else if(conversionFactor>selectedConversionFactor){
            //     baseUnit.baseQty=parseFloat(noOfquntityUsed)/parseFloat(selectedConversionFactor);
            // }

            var preferedUnits = {};
            if (item.preferedUnit != undefined) {
              var pInd = _.findIndex(item.units, {_id: item.preferedUnit._id});
              if (pInd >= 0) {
                preferedUnits = angular.copy(item.units[pInd]);
                var preferedconversionFactor = item.units[pInd].conversionFactor;
                console.log("sFactor_Menu" + selectedConversionFactor);
                console.log("pFactor_Menu" + preferedconversionFactor);
                if (preferedconversionFactor == selectedConversionFactor) {
                  preferedUnits = angular.copy(selectedUnit1);
                  preferedUnits.type = "preferedUnit";
                }
                else if (preferedconversionFactor > selectedConversionFactor) {
                  preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                }
                else if (preferedconversionFactor < selectedConversionFactor) {
                  preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                }
              }
            }
            else {
              preferedUnits = angular.copy(selectedUnit1);
              preferedUnits.type = "preferedUnit";
            }
            if (baseUnit.id == 2 || baseUnit.id == 3) {
              var cf = 1000;
              if (selectedConversionFactor == cf) {
                baseUnit.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(cf);
              }
              else if (selectedConversionFactor > cf) {
                baseUnit.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(cf);
              }
              else if (cf > selectedConversionFactor) {
                baseUnit.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(cf);
              }
              ;
            }
            ;
            item.calculateInUnits = [];
            item.calculateInUnits.push(baseUnit);
            item.calculateInUnits.push(preferedUnits);
            item.calculateInUnits.push(selectedUnit1);
            console.log(item);
          }
          else {
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            var selectedUnit1 = {};
            if (item.baseQuantity == undefined) {
              item.baseQuantity = parseFloat(item.quantity) * parseFloat(item.selectedUnitId.conversionFactor);
            }
            if (item.selectedUnitId != undefined) {
              selectedConversionFactor = item.selectedUnitId.conversionFactor;
              selectedUnit1 = {
                "_id": item.selectedUnitId._id,
                "unitName": item.selectedUnitId.unitName,
                "type": "selectedUnit",
                "conversionFactor": parseFloat(item.selectedUnitId.conversionFactor)
              };
              selectedUnit1.baseQty = parseFloat(item.baseQuantity) * parseFloat(items.qty);
            }
            var onePortion = parseFloat(items.menuQty) / parseFloat(items.recipeQty);
            var noOfPlates = parseFloat(items.recipeQty) / parseFloat(items.menuQty);
            var qtyOf1Plate = parseFloat(item.baseQuantity) / parseFloat(noOfPlates);
            if (items.selectedUnit == undefined) {
              items.selectedUnit = items.selectedUnit1;
            }
            var rawQty = parseFloat(items.qty) * parseFloat(items.selectedUnit.conversionFactor);
            var noOfPlatesMade = parseFloat(rawQty) / parseFloat(items.menuQty);
            //var noOfquntityUsed=parseFloat(parseFloat(items.qty)*parseFloat(item.baseQuantity)*parseFloat(onePortion)).toFixed(3);//parseFloat(items.quantity);
            var noOfquntityUsed = parseFloat(parseFloat(noOfPlatesMade) * parseFloat(qtyOf1Plate)).toFixed(3);
            var baseUnit = {};
            baseUnit = angular.copy(item.selectedUnitId.baseUnit);
            baseUnit.unitName = baseUnit.name;
            baseUnit.type = "baseUnit";
            //if(conversionFactor==selectedConversionFactor){
            baseUnit.baseQty = parseFloat(noOfquntityUsed);
            // }
            // else if(conversionFactor<selectedConversionFactor){
            //     baseUnit.baseQty=parseFloat(noOfquntityUsed)*parseFloat(selectedConversionFactor);
            // }
            // else if(conversionFactor>selectedConversionFactor){
            //     baseUnit.baseQty=parseFloat(noOfquntityUsed)/parseFloat(selectedConversionFactor);
            // }

            var preferedUnits = {};
            if (item.preferedUnit != undefined) {
              var pInd = _.findIndex(item.units, {_id: item.preferedUnit._id});
              if (pInd >= 0) {
                preferedUnits = angular.copy(item.units[pInd]);
                var preferedconversionFactor = item.units[pInd].conversionFactor;
                console.log("sFactor_Menu" + selectedConversionFactor);
                console.log("pFactor_Menu" + preferedconversionFactor);
                if (preferedconversionFactor == selectedConversionFactor) {
                  preferedUnits = angular.copy(selectedUnit1);
                  preferedUnits.type = "preferedUnit";
                }
                else if (preferedconversionFactor > selectedConversionFactor) {
                  preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                }
                else if (preferedconversionFactor < selectedConversionFactor) {
                  preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                }
              }
            }
            else {
              preferedUnits = angular.copy(selectedUnit1);
              preferedUnits.type = "preferedUnit";
            }
            // if(baseUnit.id==2 || baseUnit.id==3){
            //     var cf=1000;
            //     if(selectedConversionFactor==cf){
            //         baseUnit.baseQty=parseFloat(baseUnit.baseQty)/parseFloat(cf);
            //     }
            //     else if(selectedConversionFactor>cf){
            //         baseUnit.baseQty=parseFloat(baseUnit.baseQty)*parseFloat(cf);
            //     }
            //     else if(cf>selectedConversionFactor) {
            //         baseUnit.baseQty=parseFloat(baseUnit.baseQty)/parseFloat(cf);
            //     };
            // };
            item.calculateInUnits = [];
            item.calculateInUnits.push(baseUnit);
            item.calculateInUnits.push(preferedUnits);
            item.calculateInUnits.push(selectedUnit1);
            console.log(item);
          }
        });
        return items;
        //}
      },
      basePrefferedAnsSelectedUnitsByItem: function (item, selectedUnits, type) {
        //if(item.itemType=="RawMaterial"){
        var conversionFactor = 1;
        var selectedConversionFactor = 1;
        _.forEach(selectedUnits, function (u, ii) {
          if (u != null) {
            if (ii == item._id) {
              selectedConversionFactor = u.conversionFactor;
              if (type == 1) {
                var selectedUnit1 = {
                  "_id": u._id,
                  "unitName": u.unitName,
                  "basePrice": parseFloat(item.price),
                  "baseQty": parseFloat(item.qty),
                  "subTotal": parseFloat(item.subTotal),
                  "addedAmt": parseFloat(item.addedAmt),
                  "totalAmount": parseFloat(item.totalAmount),
                  "type": "selectedUnit",
                  "conversionFactor": parseFloat(u.conversionFactor)
                };
                selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
                selectedUnit1.addedAmt = 0;
                if (item.vatPercent != "" && item.vatPercent != undefined) {
                  selectedUnit1.addedAmt = parseFloat(parseFloat(selectedUnit1.subTotal) * .01 * parseFloat(item.vatPercent));
                }
                selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
                item["selectedUnit"] = selectedUnit1;

                if (item.preferedUnit == undefined) {
                  item["preferedUnits"] = angular.copy(selectedUnit1);
                  item.preferedUnits.type = "preferedUnit";
                }
              }
              else if (type == 2) {
                var selectedUnit1 = {
                  "_id": u._id,
                  "unitName": u.unitName,
                  "basePrice": parseFloat(item.price),
                  "baseQty": parseFloat(item.qty),
                  "subTotal": parseFloat(item.subTotal),
                  "addedAmt": parseFloat(item.addedAmt),
                  "totalAmount": parseFloat(item.totalAmount),
                  "type": "selectedUnit",
                  "conversionFactor": parseFloat(u.conversionFactor)
                };
                selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
                selectedUnit1.addedAmt = 0;
                selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
                item["selectedUnit"] = selectedUnit1;

                if (item.preferedUnit == undefined) {
                  item["preferedUnits"] = angular.copy(selectedUnit1);
                  item.preferedUnits.type = "preferedUnit";
                }
              }
            }
          }
        });

        var baseUnit = {};
        if (item.units[0].baseUnit != undefined) {
          baseUnit = angular.copy(item.units[0].baseUnit);
          baseUnit.unitName = baseUnit.name;
        }
        else {
          if (item.baseUnit != undefined) {
            baseUnit = angular.copy(item.baseUnit);
            baseUnit.unitName = baseUnit.name;
          }
          else {
            baseUnit = angular.copy(item.units[0]);
          }
        }

        baseUnit.type = "baseUnit";
        baseUnit.conversionFactor = conversionFactor;

        baseUnit.baseQty = parseFloat(item.qty * selectedConversionFactor);
        baseUnit.basePrice = parseFloat(item.price / selectedConversionFactor);

        var baseAmt = parseFloat(item.qty) * parseFloat(item.price);
        if (item.vatPercent == "" || item.vatPercent == undefined) {
          baseUnit.totalAmount = baseAmt;
          baseUnit.subTotal = baseAmt;
          baseUnit.addedAmt = 0;
        }
        else {
          var vatamt = (baseAmt * .01 * parseFloat(item.vatPercent));
          baseUnit.addedAmt = vatamt;
          baseUnit.subTotal = baseAmt;
          baseUnit.totalAmount = baseAmt + parseFloat(vatamt);
        }

        item.baseUnit = baseUnit;

        var pInd = _.findIndex(item.units, {_id: item.preferedUnit});

        if (pInd >= 0) {
          var preferedUnits = angular.copy(item.units[pInd]);
          var preferedconversionFactor = item.units[pInd].conversionFactor;
          preferedUnits.addedAmt = 0;

          console.log("selectedConversionFactor" + selectedConversionFactor);
          console.log("preferedconversionFactor" + preferedconversionFactor);
          if (preferedconversionFactor == selectedConversionFactor) {
            // preferedUnits.baseQty=parseFloat(baseUnit.baseQty);
            // preferedUnits.basePrice=parseFloat(baseUnit.basePrice);
            // preferedUnits.addedAmt=parseFloat(baseUnit.addedAmt);
            // preferedUnits.subTotal=parseFloat(baseUnit.subTotal);
            // preferedUnits.totalAmount=parseFloat(preferedUnits.subTotal)+parseFloat(preferedUnits.addedAmt);
            preferedUnits = angular.copy(item.selectedUnit);
          }
          else if (preferedconversionFactor > selectedConversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
            if (type == 1) {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
            }
            else {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
              baseUnit.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
              item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) / parseFloat(preferedconversionFactor);
            }
            preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
            if (item.vatPercent != "" && item.vatPercent != undefined) {
              preferedUnits.addedAmt = parseFloat(parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
            }

            preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
          }
          else if (preferedconversionFactor < selectedConversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
            if (type == 1) {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
            }
            else {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
              baseUnit.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
              item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) * parseFloat(preferedconversionFactor);
            }
            preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
            if (item.vatPercent != "" && item.vatPercent != undefined) {
              preferedUnits.addedAmt = (parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
            }
            preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
          }

          if (type == 2) {
            baseUnit.subTotal = parseFloat(baseUnit.basePrice) * parseFloat(baseUnit.baseQty);
            baseUnit.addedAmt = 0;
            baseUnit.totalAmount = parseFloat(baseUnit.subTotal);

            item.selectedUnit.subTotal = parseFloat(item.selectedUnit.basePrice) * parseFloat(item.selectedUnit.baseQty);
            item.selectedUnit.addedAmt = 0;
            item.selectedUnit.totalAmount = parseFloat(item.selectedUnit.subTotal);
          }
          preferedUnits.type = "preferedUnit";
          delete preferedUnits.baseUnit;
          item.preferedUnits = preferedUnits;
        }
        if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
          var cf = 1000;
          if (selectedConversionFactor >= cf) {
            item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
            item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
          }
          else if (cf > selectedConversionFactor) {
            item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
            item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
          }
          ;
        }
        ;

        item.calculateInUnits = [];
        item.calculateInUnits.push(item.baseUnit);
        item.calculateInUnits.push(item.preferedUnits);
        item.calculateInUnits.push(item.selectedUnit);

        return item.calculateInUnits;
        //}
        // else
        // {
        //    return this.calculateForProcessedAndSemiProcessed(item,selectedUnits);
        // }
      },
      basePrefferedAnsSelectedUnitsByItemStockReturn: function (item, selectedUnits, type) {
        //if(item.itemType=="RawMaterial"){
        var conversionFactor = 1;
        var selectedConversionFactor = 1;
        console.log(selectedUnits);
        _.forEach(selectedUnits, function (u, ii) {
          console.log(ii, u);
          if (u != null) {
            console.log(item.preferedUnits);
            if (ii == item._id) {
              console.log(u.conversionFactor);
              selectedConversionFactor = u.conversionFactor;
              if (type == 1) {
                var selectedUnit1 = {
                  "_id": u._id,
                  "unitName": u.unitName,
                  "basePrice": parseFloat(item.price),
                  "baseQty": parseFloat(item.qty),
                  "subTotal": parseFloat(item.subTotal),
                  "addedAmt": parseFloat(item.addedAmt),
                  "totalAmount": parseFloat(item.totalAmount),
                  "type": "selectedUnit",
                  "conversionFactor": parseFloat(u.conversionFactor)
                };
                selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
                selectedUnit1.addedAmt = 0;
                if (item.vatPercent != "" && item.vatPercent != undefined) {
                  selectedUnit1.addedAmt = parseFloat(parseFloat(selectedUnit1.subTotal) * .01 * parseFloat(item.vatPercent));
                }
                selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
                item["selectedUnit"] = selectedUnit1;
                if (item.preferedUnit == undefined) {
                  item["preferedUnits"] = angular.copy(selectedUnit1);
                  item.preferedUnits.type = "preferedUnit";
                }
              }
              else if (type == 2) {
                var selectedUnit1 = {
                  "_id": u._id,
                  "unitName": u.unitName,
                  "basePrice": parseFloat(item.price),
                  "baseQty": parseFloat(item.qty),
                  "subTotal": parseFloat(item.subTotal),
                  "addedAmt": parseFloat(item.addedAmt),
                  "totalAmount": parseFloat(item.totalAmount),
                  "type": "selectedUnit",
                  "conversionFactor": parseFloat(u.conversionFactor)
                };
                selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
                selectedUnit1.addedAmt = 0;
                selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
                item["selectedUnit"] = selectedUnit1;

                if (item.preferedUnit == undefined) {
                  item["preferedUnits"] = angular.copy(selectedUnit1);
                  item.preferedUnits.type = "preferedUnit";
                }
              }
            }
          }
        });

        var baseUnit = {};
        if (item.units[0].baseUnit != undefined) {
          baseUnit = angular.copy(item.units[0].baseUnit);
          baseUnit.unitName = baseUnit.name;
        }
        else {
          if (item.baseUnit != undefined) {
            baseUnit = angular.copy(item.baseUnit);
            baseUnit.unitName = baseUnit.name;
          }
          else {
            baseUnit = angular.copy(item.units[0]);
          }
        }

        baseUnit.type = "baseUnit";
        baseUnit.conversionFactor = conversionFactor;

        baseUnit.baseQty = parseFloat(item.qty * selectedConversionFactor);
        baseUnit.basePrice = parseFloat(item.price / selectedConversionFactor);

        var baseAmt = parseFloat(item.qty) * parseFloat(item.price);
        if (item.vatPercent == "" || item.vatPercent == undefined) {
          baseUnit.totalAmount = baseAmt;
          baseUnit.subTotal = baseAmt;
          baseUnit.addedAmt = 0;
        }
        else {
          var vatamt = (baseAmt * .01 * parseFloat(item.vatPercent));
          baseUnit.addedAmt = vatamt;
          baseUnit.subTotal = baseAmt;
          baseUnit.totalAmount = baseAmt + parseFloat(vatamt);
        }

        item.baseUnit = baseUnit;

        var pInd = _.findIndex(item.units, {_id: item.preferedUnit});

        if (pInd >= 0) {
          var preferedUnits = angular.copy(item.units[pInd]);
          var preferedconversionFactor = item.units[pInd].conversionFactor;
          preferedUnits.addedAmt = 0;

          console.log("selectedConversionFactor" + selectedConversionFactor);
          console.log("preferedconversionFactor" + preferedconversionFactor);
          if (preferedconversionFactor == selectedConversionFactor) {
            // preferedUnits.baseQty=parseFloat(baseUnit.baseQty);
            // preferedUnits.basePrice=parseFloat(baseUnit.basePrice);
            // preferedUnits.addedAmt=parseFloat(baseUnit.addedAmt);
            // preferedUnits.subTotal=parseFloat(baseUnit.subTotal);
            // preferedUnits.totalAmount=parseFloat(preferedUnits.subTotal)+parseFloat(preferedUnits.addedAmt);
            preferedUnits = angular.copy(item.selectedUnit);
          }
          else if (preferedconversionFactor > selectedConversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
            if (type == 1) {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
            }
            else {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
              baseUnit.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
              item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) / parseFloat(preferedconversionFactor);
            }
            preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
            if (item.vatPercent != "" && item.vatPercent != undefined) {
              preferedUnits.addedAmt = parseFloat(parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
            }

            preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
          }
          else if (preferedconversionFactor < selectedConversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
            if (type == 1) {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
            }
            else {
              preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
              baseUnit.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
              item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) * parseFloat(preferedconversionFactor);
            }
            preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
            if (item.vatPercent != "" && item.vatPercent != undefined) {
              preferedUnits.addedAmt = (parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
            }
            preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
          }

          if (type == 2) {
            baseUnit.subTotal = parseFloat(baseUnit.basePrice) * parseFloat(baseUnit.baseQty);
            baseUnit.addedAmt = 0;
            baseUnit.totalAmount = parseFloat(baseUnit.subTotal);

            item.selectedUnit.subTotal = parseFloat(item.selectedUnit.basePrice) * parseFloat(item.selectedUnit.baseQty);
            item.selectedUnit.addedAmt = 0;
            item.selectedUnit.totalAmount = parseFloat(item.selectedUnit.subTotal);
          }
          preferedUnits.type = "preferedUnit";
          delete preferedUnits.baseUnit;
          item.preferedUnits = preferedUnits;
        }
        if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
          var cf = 1000;
          if (selectedConversionFactor >= cf) {
            item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
            item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
          }
          else if (cf > selectedConversionFactor) {
            item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
            item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
          }
          ;
        };

        item.calculateInUnits = [];
        item.calculateInUnits.push(item.baseUnit);
        item.calculateInUnits.push(item.preferedUnits);
        item.calculateInUnits.push(item.selectedUnit);

        return item.calculateInUnits;
        //}
        // else
        // {
        //    return this.calculateForProcessedAndSemiProcessed(item,selectedUnits);
        // }
      },
      generateBillNumbers: function (transactionType) {
        // var resettime = moment(Utils.getSettingValue('day_sale_cutoff', $rootScope.settings)).format('hh');
        // var currentdate = new Date();
        var bnQuery = "";
        var sDnQuaery = "";
        var snQuery = "";
        var dSNQuery = "";
        // if (currentdate.getHours() < resettime) {
        //   bnQuery = "select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created between  datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime') )>(select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created between  datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime') ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber";
        //   sDnQuaery = "select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created between datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime')";
        //   snQuery = "select (case when max(serialnumber) is null or max(serialnumber)='undefined' then 1 else (max(serialnumber)+1) end) as serialnumber from bills ";
        // }
        // else {
        //   bnQuery = "select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created between  datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime') )>(select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created between  datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime') ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber";
        //   sDnQuaery = "select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created between datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime')";
        //   snQuery = "select (case when max(serialnumber) is null or max(serialnumber)='undefined' then 1 else (max(serialnumber)+1) end) as serialnumber from bills ";
        // }

        bnQuery = "select (case when max(transactionNumber) is null or max(transactionNumber)='undefined' then 1 else max(transactionNumber)+1 end) as transactionNumber from stocktransactions where transactionType='" + transactionType + "' and tenant_id='" + this.tenant_id + "' and deployment_id='" + this.deployment_id + "' and daySerialNumber != 'undefined'";
        dSNQuery = "select * from stocktransactions where daySerialNumber = (Select max(daySerialNumber) from stocktransactions where created between datetime(date()) and daySerialNumber != 'undefined' and deployment_id='" + this.deployment_id + "')";
        console.log(dSNQuery);
        var tQuery = "select * from stocktransactions where transactionType='" + transactionType + "' and tenant_id='" + this.tenant_id + "' and deployment_id='" + this.deployment_id + "' and daySerialNumber != 'undefined'";
        var deferred = $q.defer();
        var snumbers = {};
        $q.all([
            //$rootScope.db.selectDynamic(bnQuery),
            //$rootScope.db.selectDynamic(dSNQuery),
            // $rootScope.db.selectDynamic(snQuery),
            // $rootScope.db.selectDynamic(sDnQuaery)
            $rootScope.db.selectDynamic(tQuery)
          ])
          .then(function (sns) {
              //  console.log(sns);
              //snumbers.billnumber = sns[0].rows.item(0).transactionNumber;
              //var tDate = new Date(new Date().setHours(0, 0, 0, 0));
              //if (sns[1]) {
              //  if (!sns[1].rows.item(0).daySerialNumber) {
              //    snumbers.daySerialNumber = 1;
              //  }
              //  else {
              //    console.log(new Date(sns[1].rows.item(0).created).getDate(), tDate.getDate());
              //    if (new Date(sns[1].rows.item(0).created).getDate() < tDate.getDate()) {
              //      console.log('if');
              //      snumbers.daySerialNumber = 1;
              //    }
              //    else {
              //      console.log('else');
              //      snumbers.daySerialNumber = sns[1].rows.item(0).daySerialNumber + 1;
              //    }
              //    console.log(sns[1].rows.item(0).daySerialNumber, sns[0].rows.item(0).transactionNumber);
              //    // snumbers.serialnumber = sns[1].rows.item(0).serialnumber;
              //    // snumbers.dayserialnumber = sns[2].rows.item(0).dayserialnumber;
              //    //this._transactionNumber=snumbers.billnumber;
              //  }
              //} else {
              //  snumbers.daySerialNumber = 1;
              //}
              //console.log(sns[0].rows);
              _.forEach(sns[0].rows, function (transaction) {
                //console.log(transaction.transactionNumber, transaction.daySerialNumber, new Date(transaction.created))
                if(transaction.transactionNumber && transaction.daySerialNumber && new Date(transaction.created) < new Date()){
                  if(!snumbers.billnumber){
                    snumbers.billnumber = transaction.transactionNumber;
                  } else {
                    if(snumbers.billnumber < transaction.transactionNumber){
                      snumbers.billnumber = transaction.transactionNumber;
                    }
                  }
                  if(!snumbers.maxTransactionDate) {
                    snumbers.maxTransactionDate = new Date(transaction.created);
                    snumbers.daySerialNumber = transaction.daySerialNumber;
                  } else {
                    if(snumbers.maxTransactionDate < new Date(transaction.created)){
                      snumbers.maxTransactionDate = new Date(transaction.created);
                      snumbers.daySerialNumber = transaction.daySerialNumber;
                    }
                  }

                }
              });
              if(!snumbers.billnumber)
                snumbers.billnumber = 1;
              else snumbers.billnumber+=1;
              if(!snumbers.daySerialNumber)
                snumbers.daySerialNumber = 1;
              else {
                if(moment(snumbers.maxTransactionDate).format('L') == moment(new Date()).format('L'))
                  snumbers.daySerialNumber += 1;
                else snumbers.daySerialNumber = 1;
              }
              deferred.resolve(snumbers);
            }
          ).catch(function (e) {
          deferred.reject(e);
        });
        return deferred.promise;
      },
      getOpenTransactions: function (transactionType) {
        var bnQuery = "";
        var sDnQuaery = "";
        var snQuery = "";
        bnQuery = "select transactionData from stocktransactions where  isOpen='true' and transactionType='" + transactionType + "' and tenant_id='" + this.tenant_id + "' and deployment_id='" + this.deployment_id + "'";
        var deferred = $q.defer();
        var snumbers = {};
        $q.all([
            $rootScope.db.selectDynamic(bnQuery)
            // $rootScope.db.selectDynamic(snQuery),
            // $rootScope.db.selectDynamic(sDnQuaery)
          ])
          .then(function (results) {
              var _rows = [];
              for (var i = 0; i < results[0].rows.length; i++) {
                _rows.push(angular.fromJson(results[0].rows[i].transactionData));
              }
              //console.log(_rows);
              deferred.resolve(_rows);
            }
          ).catch(function (e) {
          deferred.reject(e);
        });
        return deferred.promise;
      },
      getTemplates: function (transactionType) {
        var bnQuery = "";
        var sDnQuaery = "";
        var snQuery = "";
        bnQuery = "select transactionData from stocktransactions where transactionType='" + transactionType + "' and tenant_id='" + this.tenant_id + "' and deployment_id='" + this.deployment_id + "'";
        var deferred = $q.defer();
        var snumbers = {};
        $q.all([
            $rootScope.db.selectDynamic(bnQuery)
          ])
          .then(function (results) {
              var _rows = [];
              for (var i = 0; i < results[0].rows.length; i++) {
                _rows.push(angular.fromJson(results[0].rows[i].transactionData));
              }
              deferred.resolve(_rows);
            }
          ).catch(function (e) {
          deferred.reject(e);
        });
        return deferred.promise;
      },
      processTransaction: function (data, transactionType) {
        console.log(this);
        if (transactionType == 1) {
          console.log(data);
        }
      },

      updateTransaction: function (options, operation) {
        //$.extend(this, options);
        var deferred = $q.defer();
        var promise = deferred.promise;
        console.log(angular.copy(this));
        var cdate = Utils.getDateFormatted(new Date());
        var transaction = checkStoresInTransactions(angular.copy(this));
        console.log('transaction service',transaction)
        if (operation === 'insert') {
          //alert("Inserting");
          $rootScope.db.insert('stocktransactions',
            {
              "transactionId": (this._id != null) ? this._id : "null",
              "transactionType": (this._transactionType != null) ? this._transactionType : "null",
              "daySerialNumber": (this.daySerialNumber != null) ? this.daySerialNumber : "null",
              "transactionNumber": (this.transactionNumber != null) ? this.transactionNumber : "null",
              "transactionData": angular.toJson(angular.copy(this)),
              "deployment_id": (this.deployment_id != null) ? this.deployment_id : "null",
              "tenant_id": (this.tenant_id != null) ? this.tenant_id : "null",
              "isSynced": (this.isSynced != null) ? this.isSynced : "null",
              "syncedOn": (this.syncedOn != null) ? this.syncedOn : "null",
              "isOpen": (this.isOpen != null) ? this.isOpen : "null",
              "created": (cdate != null) ? cdate : "null",
            }
          ).then(function (results) {
            console.log(results);
            deferred.resolve(results.insertId)
          }).catch(function (e) {
            console.log(e);
            deferred.reject(e);
          });
 
 
        } else {
          //alert("Updating")
          console.log(this);
          $rootScope.db.update('stocktransactions', {
            "transactionData": angular.toJson(this),
            "isOpen": this.isOpen,
            "isSynced": false
          }, {
            "transactionId": this._id
          }).then(function (results) {
            deferred.resolve("Updated!");
          }).catch(function (e) {
            deferred.reject(e);
          });
 
        }
 
        return promise;
      },
    };

    function checkStoresInTransactions(transaction)
    {
      if(transaction.transactionType == '3' || transaction.transactionType == '4'  || transaction.transactionType == '5'
         || transaction.transactionType == '6'  || transaction.transactionType == '7'  || transaction.transactionType == '8' 
         || transaction.transactionType == '9'  || transaction.transactionType == '10'  || transaction.transactionType == '11'
         || transaction.transactionType == '12'  || transaction.transactionType == '13'  || transaction.transactionType == '14'
         || transaction.transactionType == '15'  || transaction.transactionType == '16')
      {
        delete transaction._store.category; 
        if(!transaction._store.category)
        {
          var stores = transaction._store;
          stores.category = [];
          _.forEach(transaction._items, function (itm, i) {
            var index = _.findIndex(stores.category, {_id: itm.fromCategory._id});
            if (index < 0) {
              stores.category.push(angular.copy(itm.fromCategory));
            }
          });
          _.forEach(stores.category, function (c, i) {
            c.items = [];
            _.forEachRight(transaction._items, function (itm, ii) {
              if (c._id == itm.fromCategory._id) {
                delete itm.fromCategory;
                c.items.push(itm);
                transaction._items.splice(ii, 1);
              }
            });
          });
          transaction._store = angular.copy(stores);
          console.log('transaction in PWS',angular.copy(transaction));
          //return _transaction;
        }
        else
        {
          //return transaction;
        }
      }
      else if(transaction.transactionType == '2')
      {
       //delete transaction._store.receiver.category;
       if(transaction._store.receiver == undefined)
       {
        var stores = transaction._store;
        var rec = transaction._receiver;
        delete rec.pricing;
        stores.receiver = rec;
        stores.receiver.category = [];
        _.forEach(transaction._items, function (itm, i) {
          var index = _.findIndex(stores.receiver.category, {_id: itm.fromCategory._id});
          if (index < 0) {
            stores.receiver.category.push(itm.fromCategory);
          }
        });

        _.forEach(stores.receiver.category, function (c, i) {
          c.items = [];
          _.forEachRight(transaction._items, function (itm, ii) {
            if (c._id == itm.fromCategory._id) {
              delete itm.fromCategory;
              c.items.push(itm);
              transaction._items.splice(ii, 1);
            }
          });
        });
        transaction._store = angular.copy(stores);
        console.log('transaction when receiver is missing',transaction);
       }
       else if(transaction._store.receiver.category == undefined)
       {
        var stores = transaction._store;
        var rec = transaction._receiver;
        delete rec.pricing;
        stores.receiver = rec;
        stores.receiver.category = [];
        _.forEach(transaction._items, function (itm, i) {
          var index = _.findIndex(stores.receiver.category, {_id: itm.fromCategory._id});
          if (index < 0) {
            stores.receiver.category.push(itm.fromCategory);
          }
        });

        _.forEach(stores.receiver.category, function (c, i) {
          c.items = [];
          _.forEachRight(transaction._items, function (itm, ii) {
            if (c._id == itm.fromCategory._id) {
              delete itm.fromCategory;
              c.items.push(itm);
              transaction._items.splice(ii, 1);
            }
          });
        });
        transaction._store = angular.copy(stores);
        console.log('transaction in receiver',angular.copy(transaction));
       }
       else
       {

       } 
      }
      else if(transaction.transactionType == '1')
      {
        //delete transaction._store.vendor 
        /*if(transaction._store.vendor == undefined)
        {
          console.log('when transaction._store.vendor is missing')
          transaction._store.vendor = angular.copy(transaction._vendor);
        }*/
      }
      else
      {

      }
      return transaction;
    }

    function setDaySerialNumberAndTransactionNumber(bills){
      var deferred = $q.defer();

      $rootScope.db.selectDynamic("Select * from stocktransactions where transactionNumber = (Select Max(transactionNumber) from stocktransactions)").then(function (rows) {
        if (rows.rows.length > 0) {
          console.log('from sql', rows.rows[0].transactionNumber, rows.rows[0].daySerialNumber);
          bills.transactionNumber = Utils.roundNumber(rows.rows[0].transactionNumber) + 1;
          console.log(moment(rows.rows[0].created).format('L'), moment(new Date()).format('L'));
          if (moment(rows.rows[0].created).format('L') == moment(new Date()).format('L'))
            bills.daySerialNumber = Utils.roundNumber(rows.rows[0].daySerialNumber) + 1;
          else
            bills.daySerialNumber = 1;
        } else {
          StockResource.getLatestTransactionOfType({
            deployment_id: currentUser.deployment_id,
            tenant_id: currentUser.tenant_id,
            transactionType: bills.transactionType
          }, function (lastTransaction) {
            console.log('from api', lastTransaction.transactionNumber, lastTransaction.daySerialNumber);
            bills.transactionNumber = lastTransaction.transactionNumber;
            console.log(moment(lastTransaction.created).format('L'), moment(new Date()).format('L'));
            if (moment(lastTransaction.created).format('L') == moment(new Date()).format('L'))
              bills.daySerialNumber = Utils.roundNumber(lastTransaction.daySerialNumber) + 1;
            else
              bills.daySerialNumber = 1;
          }).catch (function (err) {
            growl.error("Couldn't Sync transaction", {ttl: 3000});
          });
        }
      });
      return deferred.promise;
    }
    return ( StockTransaction );
  });
