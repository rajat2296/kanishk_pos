'use strict';

describe('Service: stockConsumptionReport', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockConsumptionReport;
  beforeEach(inject(function (_stockConsumptionReport_) {
    stockConsumptionReport = _stockConsumptionReport_;
  }));

  it('should do something', function () {
    expect(!!stockConsumptionReport).toBe(true);
  });

});
