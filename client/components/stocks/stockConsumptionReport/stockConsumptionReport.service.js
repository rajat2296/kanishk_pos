'use strict';

angular.module('posistApp')
  .service('stockConsumptionReport', function ($resource) {

    return $resource('/api/stockConsumptionReport/:controller/:id',
      {
        id: '@_id'
      },
      {
        getRequirementData: {method: 'GET', params: {controller: "getRequirementData"}, isArray: true},
        getStockSummary: {method: 'GET', params: {controller: "getStockSummary"}, isArray: true},
        getConsumptionSummary: {method: 'GET', params: {controller: "getConsumptionSummary"}, isArray: false},
        getConsumptionSummary_IntermediateReport: {method: 'GET', params: {controller: "getConsumptionSummary_IntermediateReport"}, isArray: false},
        getConsumptionSummary_FinishedFoodReport: {method: 'GET', params: {controller: "getConsumptionSummary_FinishedFoodReport"}, isArray: false},
        getClosingQty: {method: 'GET', params: {controller: "getClosingQty"}, isArray: true},
        getClosingQty_SemiProcessed: {method: 'GET', params: {controller: "getClosingQty_SemiProcessed"}, isArray: true},
        getClosingQty_ProcessedFood: {method: 'GET', params: {controller: "getClosingQty_ProcessedFood"}, isArray: true},
        testFunction: {method: 'GET', params: {controller: "testFunction"}, isArray: true},
        getLastPriceOfItem_RawMaterial:{method:'GET',params:{controller:"getLastPriceOfItem_RawMaterial"},isArray:true},
        getLastPriceOfItem_Intermediate:{method:'GET',params:{controller:"getLastPriceOfItem_Intermediate"},isArray:true},
        getLastPriceOfItem_FinishedFood:{method:'GET',params:{controller:"getLastPriceOfItem_FinishedFood"},isArray:true},
        RawMaterialPricing_Receipe: {method: 'GET', params: {controller: "RawMaterialPricing_Receipe"}, isArray: true},
        getConsumptionSummary_ItemReport: {method: 'GET', params: {controller: "getConsumptionSummary_ItemReport"}, isArray: true},
        getRecipeQtyInBase: {method: 'GET', params: {controller: "getRecipeQtyInBase"}, isArray: true},
        getConsumptionSummary_DateWise: {method: 'GET', params: {controller: "getConsumptionSummary_DateWise"}, isArray: false},
        getGrowthReport: {method: 'GET', params: {controller: 'getGrowthReport'}, isArray: false},
        getConsumptionSummaryMailer: {method: 'GET', params: {controller: 'getConsumptionSummaryMailer'}, isArrays: false}
      }
    );
  });
