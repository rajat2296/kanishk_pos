'use strict';

describe('Service: stockTransactions', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockTransactions;
  beforeEach(inject(function (_stockTransactions_) {
    stockTransactions = _stockTransactions_;
  }));

  it('should do something', function () {
    expect(!!stockTransactions).toBe(true);
  });

});
