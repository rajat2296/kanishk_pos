'use strict';

angular.module('posistApp')
  .factory('StockMasterUser', function ($resource) {
    return $resource('/api/stockMasterUsers/:controller/:id',
      {
        id: '@_id'
      },
      {
        getTenantName: {method: 'POST', params: {controller: 'getTenantName'}, isArray: true},
        getDeploymentName: {method: 'GET', params: {controller: 'getDeploymentName'}, isArray: true},
        getTransactions: {method: 'GET', params: {controller: 'getTransactions'}, isArray: true},
        deleteTransaction: {method: 'POST', params: {controller: 'deleteTransaction'}, isArray: false},
        deleteAllTransactions: {method: 'POST', params: {controller: 'deleteAllTransactions'}, isArray: false}
      }
    );
  });
