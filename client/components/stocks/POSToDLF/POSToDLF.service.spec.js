'use strict';

describe('Service: POSToDLF', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var POSToDLF;
  beforeEach(inject(function (_POSToDLF_) {
    POSToDLF = _POSToDLF_;
  }));

  it('should do something', function () {
    expect(!!POSToDLF).toBe(true);
  });

});
