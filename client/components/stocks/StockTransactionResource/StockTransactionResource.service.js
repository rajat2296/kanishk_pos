'use strict';

angular.module('posistApp')
  .factory('StockResource', function ($resource) {
    return $resource('/api/stockTransactions/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray:false},
        get: {method: 'GET', isArray: true},
        find:{method: 'GET'},
        update:{method:'PUT'},
        editEntriesRedirect:{method: 'GET',params: {controller: "editEntriesRedirect"},isArray:false},
        getAllByTranasactionAndDateRange: {method: 'GET', params: {controller: "getAllByTranasactionAndDateRange"}, isArray: true},
        getAllByTranasactionAndDateRangeNew: {method: 'GET', params: {controller: "getAllByTranasactionAndDateRangeNew"}, isArray: true},
        getClosingQty: {method: 'GET', params: {controller: "getClosingQty"}, isArray: true},
        getBasicReports: {method: 'GET', params: {controller: "getBasicReports"}, isArray: true},
        getBasicEntryReports: {method: 'GET', params: {controller: "getBasicEntryReports"}, isArray: true},
        getBasicEntryReportsForPurchaseSummary: {method: 'GET', params: {controller: "getBasicEntryReportsForPurchaseSummary"}, isArray: true},
        getBasicEntryReportsForPurchaseDetail: {method: 'GET', params: {controller: "getBasicEntryReportsForPurchaseDetail"}, isArray: true},
        getBasicEntryReportsForEntries: {method: 'GET', params: {controller: "getBasicEntryReportsForEntries"}, isArray: true},
        getLatestTransaction:{method: 'GET', params: {controller: "getLatestTransaction"}, isArray: true},
        createMultiple:{method: 'POST', params: {controller: "createMultiple"}, isArray: false},
        createWithTransactionId: {method: 'POST', params: {controller: "createWithTransactionId"}, isArray: false},
        getLatestTransactionOfType: {method: 'GET', params: {controller: "getLatestTransactionOfType"}, isArray: false},
        checkIfMenuItemExistsInTransaction: {method: 'GET', params: {controller: "checkIfMenuItemExistsInTransaction"}, isArray: false}
      }
    );
  });
