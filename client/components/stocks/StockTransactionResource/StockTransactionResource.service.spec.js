'use strict';

describe('Service: StockTransactionResource', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var StockTransactionResource;
  beforeEach(inject(function (_StockTransactionResource_) {
    StockTransactionResource = _StockTransactionResource_;
  }));

  it('should do something', function () {
    expect(!!StockTransactionResource).toBe(true);
  });

});
