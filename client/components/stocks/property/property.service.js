'use strict';

angular.module('posistApp')
  .factory('property', function ($resource) {
    return $resource('/api/stockPropertys/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      }
    );
  });
