'use strict';

describe('Service: EditTransactionHistory', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var EditTransactionHistory;
  beforeEach(inject(function (_EditTransactionHistory_) {
    EditTransactionHistory = _EditTransactionHistory_;
  }));

  it('should do something', function () {
    expect(!!EditTransactionHistory).toBe(true);
  });

});
