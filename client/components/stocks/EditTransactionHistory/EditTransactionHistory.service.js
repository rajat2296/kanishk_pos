'use strict';

angular.module('posistApp')
  .factory('EditTransactionHistory', function ($resource) {
    return $resource('/api/editTransactionHistorys/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        getHistoryByMainTransactionId: {method: 'GET', params: {controller: "getHistoryByMainTransactionId"}, isArray: true},
      }
    );
  });
