'use strict';

angular.module('posistApp')
  .service('outletIndent', ['$resource', function ($resource) {
    return $resource('/api/outletIndents/:controller/:id',
      {
        id: '@_id'
      },
      {
        getAll: {method: 'GET', isArray: true},
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        getLastOutletIndentNumber: {method: 'GET', params: {controller: 'getLastOutletIndentNumber'}, isArray: false},
        findOne: {method: 'GET', params: {controller: 'findOne'}, isArray: false},
        update: {method: 'PUT', isArray: false},
        getCount:{method: 'GET', params: {controller: 'getCount'}, isArray: false},
        getRequirements: {method: 'GET', params: {controller: 'getRequirements'}, isArray: true},
        getRequirementsPagination: {method: 'GET', params: {controller: 'getRequirementsPagination'}, isArray: true},
        getRequirementReport: {method: 'GET', params: {controller: "getRequirementReport"}, isArray: true},
        getLastRequirementBillNo: {method: 'GET', params: {controller: "getLastRequirementBillNo"}, isArray: false},
        getSuplyRequirementReport: {method: 'GET', params: {controller: "getSuplyRequirementReport"}, isArray: true},
        getStoreNItems: {method: 'GET', params: {controller: "getStoreNItems"}, isArray: false},
        getItemsInSelectedRequirements: {method: 'GET', params: {controller: 'getItemsInSelectedRequirements'}, isArray: true}
      });
  }]);
