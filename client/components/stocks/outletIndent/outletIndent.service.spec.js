'use strict';

describe('Service: outletIndent', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var outletIndent;
  beforeEach(inject(function (_outletIndent_) {
    outletIndent = _outletIndent_;
  }));

  it('should do something', function () {
    expect(!!outletIndent).toBe(true);
  });

});
