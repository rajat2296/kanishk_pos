'use strict';

angular.module('posistApp')
  .factory('stockRequirement', function ($resource) {
    return $resource('/api/stockRequirements/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        getConsolidatedInventoryReport: {method: 'GET', params: {controller: 'getConsolidatedInventoryReport'}, isArray: true},
        get: {method: 'GET', isArray: true},
        getCount:{method: 'GET', params: {controller: 'getCount'}, isArray: false},
        getRequirements: {method: 'GET', params: {controller: 'getRequirements'}, isArray: true},
        getRequirementsPagination: {method: 'GET', params: {controller: 'getRequirementsPagination'}, isArray: true},
        findOne: {method: 'GET',isArray:true},
        update: {method: 'PUT'},
        getRequirementReport: {method: 'GET', params: {controller: "getRequirementReport"}, isArray: true},
        getLastRequirementBillNo: {method: 'GET', params: {controller: "getLastRequirementBillNo"}, isArray: false},
        getSuplyRequirementReport: {method: 'GET', params: {controller: "getSuplyRequirementReport"}, isArray: true},
        getStoreNItems: {method: 'GET', params: {controller: "getStoreNItems"}, isArray: false},
        checkIfItemExistsIndentExists: {method: 'GET', params: {controller: "checkIfItemExistsIndentExists"}, isArray: false},
        getItemsInSelectedRequirements: {method: 'GET', params: {controller: 'getItemsInSelectedRequirements'}, isArray: true}
      }
    );
  });
