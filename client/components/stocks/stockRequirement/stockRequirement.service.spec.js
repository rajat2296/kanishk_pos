'use strict';

describe('Service: stockRequirement', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockRequirement;
  beforeEach(inject(function (_stockRequirement_) {
    stockRequirement = _stockRequirement_;
  }));

  it('should do something', function () {
    expect(!!stockRequirement).toBe(true);
  });

});
