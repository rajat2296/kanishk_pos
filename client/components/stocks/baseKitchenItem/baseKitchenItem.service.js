'use strict';

angular.module('posistApp')
.factory('baseKitchenItem', function ($resource) {
    return $resource('/api/baseKitchenItems/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        saveAll: {method: 'POST', params: {controller: 'saveAll'}, isArray: true},
        baseKitchenItemsByDeployment: {method: 'GET', params: {controller: 'baseKitchenItemsByDeployment'}, isArray: true},
        updateBaseKitchenItemUnit: {method: 'PUT', params: {controller: 'updateBaseKitchenItemUnit'}, isArray: false},
        deleteBaseKitchenItems: {method: 'DELETE', params: {controller: 'deleteBaseKitchenItems'}, isArray: false},
        getUniqueBaseKitchenItems: {method: 'GET', params: {controller: 'getUniqueBaseKitchenItems'}, isArray: true}
      }
    );
  });
