'use strict';

describe('Service: stockRecipe', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockRecipe;
  beforeEach(inject(function (_stockRecipe_) {
    stockRecipe = _stockRecipe_;
  }));

  it('should do something', function () {
    expect(!!stockRecipe).toBe(true);
  });

});
