'use strict';

angular.module('posistApp')
  .factory('stockRecipe', function ($resource) {
    return $resource('/api/stockRecipes/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        getSemiProcessedRecipes: {method: 'GET', params: {controller: "getSemiProcessedRecipes"}, isArray: true},
        getProcessedRecipes: {method: 'GET', params: {controller: "getProcessedRecipes"}, isArray: true},
        update: {method: 'PUT'},
        findByItemId: {method: 'GET', params: {controller: 'findByItemId'}, isArray: false},
        findByRecipeItemId: {method: 'GET', params: {controller: 'findByRecipeItemId'}, isArray: false},
        checkIfBaseKitchenItemInRecipe: {method: 'GET', params: {controller: 'checkIfBaseKitchenItemInRecipe'}, isArray: false},
      }
    );
  });
