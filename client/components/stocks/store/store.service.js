'use strict';

angular.module('posistApp')
  .factory('store', function ($resource) {
    return $resource('/api/stock/stores/:id',
            {
                id: '@_id'
            },
            {
                saveData: {method: 'POST', isArray: false},
                get: {method: 'GET', isArray: true},
                update: {method: 'PUT'}
            }
        );
  });
