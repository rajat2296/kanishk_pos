'use strict';

angular.module('posistApp')
  .factory('stockItem', function ($resource) {
    return $resource('/api/stockItems/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'},

        updateItemsAsync: {method: 'POST', params: {controller: 'updateItemsAsync'}, isArray: false},
        tranferItemToStore: {method: 'PUT', params: {controller: 'tranferItemToStore'}, isArray: false},
        updateYeild: {method: 'PUT', params: {controller: 'updateYeild'}, isArray: false}
      }
    );
  });
