'use strict';

describe('Service: stockItem', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockItem;
  beforeEach(inject(function (_stockItem_) {
    stockItem = _stockItem_;
  }));

  it('should do something', function () {
    expect(!!stockItem).toBe(true);
  });

});
