'use strict';

angular.module('posistApp')
  .service('StockComment', ['$resource', function ($resource) {
    return $resource('/api/stockComments/:id/:controller', {
      id: '@_id'
    },
    {
      update: {method: 'PUT', isArray: false}
    })
  }]);
