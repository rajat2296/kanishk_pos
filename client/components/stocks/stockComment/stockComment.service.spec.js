'use strict';

describe('Service: stockComment', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockComment;
  beforeEach(inject(function (_stockComment_) {
    stockComment = _stockComment_;
  }));

  it('should do something', function () {
    expect(!!stockComment).toBe(true);
  });

});
