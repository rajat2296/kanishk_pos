'use strict';

angular.module('posistApp')
  .factory('StockReport', function ($resource) {
    return $resource('/api/stockReports/:controller/:id',
      {
        id: '@_id'
      },
      {
        getRequirementData: {method: 'GET', params: {controller: "getRequirementData"}, isArray: true},
        getStockSummary: {method: 'GET', params: {controller: "getStockSummary"}, isArray: true},
        getConsumptionSummary: {method: 'GET', params: {controller: "getConsumptionSummary"}, isArray: false},
        getWastageReport: {method: 'GET', params: {controller: "getWastageReport"}, isArray: true},
        lastPrice: {method: 'GET', params: {controller: "lastPrice"}, isArray: true},
        RawMaterialPricing_Receipe: {method: 'GET', params: {controller: "RawMaterialPricing_Receipe"}, isArray: true},
        RawMaterialPricing_ReceipeBasekitchen: {method: 'GET', params: {controller: "RawMaterialPricing_ReceipeBasekitchen"}, isArray: true},
        RawMaterialPricing_ReceipeBasekitchenConsumptionReport: {method: 'GET', params: {controller: "RawMaterialPricing_ReceipeBasekitchenConsumptionReport"}, isArray: true},
        RawMaterialPricing_ReceipeConsumptionReport: {method: 'GET', params: {controller: "RawMaterialPricing_ReceipeConsumptionReport"}, isArray: true},
        getConsumptionSummary_ItemReport: {method: 'GET', params: {controller: "getConsumptionSummary_ItemReport"}, isArray: true},
        getConsumptionSummary_DateWise: {method: 'GET', params: {controller: "getConsumptionSummary_DateWise"}, isArray: false}
         
      }
    );
  });