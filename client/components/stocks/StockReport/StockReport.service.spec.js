'use strict';

describe('Service: StockReport', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var StockReport;
  beforeEach(inject(function (_StockReport_) {
    StockReport = _StockReport_;
  }));

  it('should do something', function () {
    expect(!!StockReport).toBe(true);
  });

});
