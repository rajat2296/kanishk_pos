'use strict';

describe('Service: stockReportResetTime', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockReportResetTime;
  beforeEach(inject(function (_stockReportResetTime_) {
    stockReportResetTime = _stockReportResetTime_;
  }));

  it('should do something', function () {
    expect(!!stockReportResetTime).toBe(true);
  });

});
