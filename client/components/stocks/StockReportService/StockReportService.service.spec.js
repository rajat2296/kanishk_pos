'use strict';

describe('Service: StockReportService', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var StockReportService;
  beforeEach(inject(function (_StockReportService_) {
    StockReportService = _StockReportService_;
  }));

  it('should do something', function () {
    expect(!!StockReportService).toBe(true);
  });

});
