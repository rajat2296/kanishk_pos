'use strict';

angular.module('posistApp')
    .factory('StockReportService', function ($q, $resource, $timeout, $http, $webSql, $modal, $state, Utils, $rootScope, growl, localStorageService, StockReportNew, stockReportResetTime, stockConsumptionReport) {
      // Define the constructor function.
      function StockReportService(data, fromDate, toDate, items, lastPrice, stockitems, deployment, stockUnits) {
        var self = this;
        self._data = data;
        self._fromDate = fromDate;
        self._toDate = toDate;
        self._items = items;
        self._units = stockUnits;
        self._lastPrice = lastPrice || [];
        self._stockItems = stockitems || [];
        self.deployment = deployment || {};
      };

      var isYieldEnabled = false;

      Date.prototype.addDays = function (days) {
        var dat = new Date(this.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
      };

      var stockAggregateService = $resource('/api/stockAggregate/:controller/:id',
        {
          id: '@_id'
        },
        {
          getFoodCostData: {method: 'GET', params: {controller: 'getFoodCostData'}, isArray: true},
          getData: {method: 'GET', params: {controller: 'getData'}, isArray: false}
        }
      );

      StockReportService.prototype = {
        getDates: function () {
          var dateArray = new Array();
          var currentDate = this._fromDate;
          var toDate = new Date(this._toDate);
          //console.log('28 ', toDate);
          toDate.setDate(toDate.getDate() + 1);
          while (currentDate <= toDate) {
            dateArray.push({date: new Date(currentDate)});
            currentDate = currentDate.addDays(1);
            //console.log(currentDate);
          }
          return dateArray;
        },
        spliceandmovePhysical: function () {
          var self = this;
          _.forEachRight(this._data.betweenDate, function (b, i) {
            if (b.totalBalanceQty != undefined) {
              var d = new Date(self._toDate);
              d.setHours(0, 0, 0, 0);

              var cDate = new Date(b.created);
              cDate.setHours(0, 0, 0, 0);
              if (d.getTime() >= cDate.getTime()) {
                self._data.beforeDate.push(b);
                self._data.betweenDate.splice(i, 1);
              }
            }
          });
          return self._data;
        },
        spliceAllItemsBeforePhysical: function () {
          var self = this;
          //console.log(angular.copy(self._data));
          var data = [];
          var result = self._data;
          var deployment = self.deployment;
          var latestPhysical = null;
          var latestPhysicalBetween = null;
          var i = 0;
          var j = 0;
          _.forEach(result.beforeDate, function(r,i){
            if(r.totalBalanceQty!=undefined){
              var d = new Date(r.created);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              //d.setHours(0, 0, 0, 0);
              r.created = new Date(d);
              r.actualDate=new Date(d);
              data.push(r);
            };
          });
          _.forEach(data,function(d,i){
            _.forEachRight(result.beforeDate,function(itm,ii){
              if(itm.itemId==d.itemId){
                var phyDate=new Date(d.created);
                var compareDate=new Date(itm.created);
                if(phyDate.getTime()>compareDate.getTime()){
                  result.beforeDate.splice(ii,1);
                }
              }
            });
          });

          // var data2=[];
          _.forEachRight(result.betweenDate,function(r,i){
            if(r.totalBalanceQty!=undefined){
              //console.log(r.created);
              r.actualDate = r.created;
              var d=new Date(r.created);
              //var cdate=new Date($scope.purchaseConsumptionForm.toDate);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              //console.log(a)
              //console.log(b)
              //console.log(angular.copy(d))
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              //d.setDate(d.getDate() + 1);
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              r.created = new Date(d);
              r.actualDate=new Date(d);
              //console.log(r.created);
              // if(cdate.getTime()>d.getTime()){
              //   data2.push(r);
              // }
              // else
              // {
              //   result.betweenDate.splice(i,1);
              // }
            };
          });
          // _.forEach(data2,function(d,i){
          //   _.forEachRight(result.betweenDate,function(itm,ii){
          //     if(itm.itemId==d.itemId){
          //       if(itm.totalBalanceQty!=undefined){
          //           var phyDate=new Date(d.created);
          //           var compareDate=new Date(itm.created);
          //           if(phyDate.getTime()>compareDate.getTime()){
          //             result.betweenDate.splice(ii,1);
          //           }
          //         }
          //     }
          //   });
          //});
          return self._data;
        },
        spliceAllItemsBeforePhysicalDateWise: function () {
          var self = this;
          //console.log(angular.copy(self._data));
          var data = [];
          var result = self._data;
          var deployment = self.deployment;
          var latestPhysical = null;
          var latestPhysicalBetween = null;
          var i = 0;
          var j = 0;
          _.forEach(result.beforeDate, function(r,i){
            if(r.totalBalanceQty!=undefined){
              var d = new Date(r.created);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              //d.setHours(0, 0, 0, 0);
              r.created = new Date(d);
              if(!latestPhysical){
                i = 0;
                latestPhysical = new Date(d);
              } else {
                if(latestPhysical.getTime() == d.getTime()){
                  i++;
                } else if(d.getTime() > latestPhysical.getTime()) {
                  i = 0;
                  latestPhysical = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = i;
              data.push(r);
            };
          });
          _.forEach(data,function(d,i){
            _.forEachRight(result.beforeDate,function(itm,ii){
              if(itm.itemId==d.itemId){
                var phyDate=new Date(d.created);
                var compareDate=new Date(itm.created);
                if(phyDate.getTime()>compareDate.getTime()){
                  result.beforeDate.splice(ii,1);
                }
              }
            });
          });

          // var data2=[];
          _.forEachRight(result.betweenDate,function(r,i){
            if(r.totalBalanceQty!=undefined){
              //console.log(r.created);
              r.actualDate = r.created;
              var d=new Date(r.created);
              //var cdate=new Date($scope.purchaseConsumptionForm.toDate);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              //console.log(a)
              //console.log(b)
              //console.log(angular.copy(d))
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              //d.setDate(d.getDate() + 1);
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              r.created = new Date(d);
              if(!latestPhysicalBetween){
                j = 0;
                latestPhysicalBetween = new Date(d);
              } else {
                if(latestPhysicalBetween.getTime() == d.getTime()){
                  j++;
                } else if(d.getTime() > latestPhysicalBetween.getTime()) {
                  j = 0;
                  latestPhysicalBetween = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = j;
              //console.log(r.created);
              // if(cdate.getTime()>d.getTime()){
              //   data2.push(r);
              // }
              // else
              // {
              //   result.betweenDate.splice(i,1);
              // }
            };
          });
          // _.forEach(data2,function(d,i){
          //   _.forEachRight(result.betweenDate,function(itm,ii){
          //     if(itm.itemId==d.itemId){
          //       if(itm.totalBalanceQty!=undefined){
          //           var phyDate=new Date(d.created);
          //           var compareDate=new Date(itm.created);
          //           if(phyDate.getTime()>compareDate.getTime()){
          //             result.betweenDate.splice(ii,1);
          //           }
          //         }
          //     }
          //   });
          //});
          console.log(angular.copy(self._data));
          return self._data;
        },
        spliceForCategoryAndItem: function () {
          var result = this._data;
          var items = this._items;
          _.forEachRight(result.items, function (itm, i) {
            var index = _.findIndex(items, {id: itm.itemId});
            if (index < 0) {
              result.items.splice(i, 1);
            }
            else {
              itm.itemName = items[index].itemName;
            }
          });
          return result;
        },
        // convertItemInPreferredUnit: function () {
         
        //   var result = this._data;
        //   var items = this._stockItems;
        //      console.log("convert");
        //     //console.log(JSON.stringify(result.items))
        //   _.forEach(result.items, function (itm, i) {
    
         
        //     var indeex = _.findIndex(items, {_id: itm.itemId});
        //     if (indeex >= 0) {
        //       var item = items[indeex];
        //      //  console.log("itmmmm",JSON.stringify(itm));
            
        //       if (item.preferedUnit != undefined) {
            
        //         _.forEach(item.units, function (u, i) {
                   
                 
        //           if (u._id == item.preferedUnit) {
        //             var conversionFactor = 1;
        //             var pconFac = parseFloat(u.conversionFactor);
        //             itm.UnitName = u.unitName;
        //             if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
        //               conversionFactor = parseFloat(u.baseConversionFactor);
        //               if (pconFac > conversionFactor) {
        //                 itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 //console.log('wastage', itm.wastageQty);
        //                 itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 if (itm.physicalQty == undefined) {
        //                   itm.physicalQty = parseFloat(0).toFixed(3);
        //                 }
        //                 else {
        //                   itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 }
              
        //                   itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
        //                   itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(1000)).toFixed(3);
        //                   // when liter is preferred
        //               }
        //               else if (pconFac < conversionFactor) {
        //                 itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(conversionFactor)).toFixed(3);
        //                 itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(conversionFactor)).toFixed(3);
        //                 itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(conversionFactor)).toFixed(3);
        //                 itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(conversionFactor)).toFixed(3);
        //                 //console.log('wastage', itm.wastageQty);
        //                 itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
        //                 itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
        //                 if (itm.physicalQty == undefined) {
        //                   itm.physicalQty = parseFloat(0).toFixed(3);
        //                 }
        //                 else {
        //                   itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
        //                 }
        //               //  console.log("var");
        //               console.log('varrrrr');
        //                    itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
        //                    itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(pconFac)).toFixed(3);
        //               }
                   
        //             }
        //             else {
                  
        //               if (pconFac > conversionFactor) {
        //                     // console.log("varrr");

        //                 itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(pconFac)).toFixed(3);
        //                 itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(pconFac)).toFixed(3);
        //                 itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(pconFac)).toFixed(3);
        //                 itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(pconFac)).toFixed(3);
        //                 //console.log('wastage', itm.wastageQty);
        //                 itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
        //                 if (itm.physicalQty == undefined) {
        //                   itm.physicalQty = parseFloat(0).toFixed(3);
        //                 }
        //                 else {
        //                   itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
        //                 }
        //             //    console.log("----------------------");
        //             //    console.log(itm)
        //             //      console.log(pconFac)
        //              //   console.log(u);
        //              //console.log('varrrrr');
        //                 itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
        //                 itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
        //               }
        //               else if (pconFac < conversionFactor) {
        //                  //   console.log("varrr11");
        //                 itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(pconFac)).toFixed(3);
        //                 itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(pconFac)).toFixed(3);
        //                 itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(pconFac)).toFixed(3);
        //                 itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(pconFac)).toFixed(3);
        //                 //console.log('wastage', itm.wastageQty);
        //                 itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
        //                 if (itm.physicalQty == undefined) {
        //                   itm.physicalQty = parseFloat(0).toFixed(3);
        //                 }
        //                 else {
        //                   itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
        //                 }
        //               //  console.log(pconFac)
        //                // console.log(u);
        //                console.log('varrrrr');
        //                 itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
        //                 itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
        //               }
        //             }
        //           }
        //         });
        //       }
        //       else
        //       {
        //         console.log("pre not set");
        //         var count=0
        //         console.log(itm);
        //          // console.log(item);
        //           _.forEach(item.units,function(u){   
        //               if(u.baseUnit.id==2 && count==0)
                        
        //                 {

        //                  count++
        //                   console.log(itm);
                         
        //                  itm.varianceAmt = parseFloat(itm.varianceAmt * 1000 ).toFixed(3);
        //                }
        //           })
        //       } // end of forEach

        //     }
        //   });
        //   return result;
        // },
    convertItemInPreferredUnit: function (){
          var result = this._data;
          var items = this._stockItems;
          var stockUnits = this._units;
            console.log('result cvi',result);
          _.forEach(result.items,function(itm,i){
            var indeex=_.findIndex(items,function(item) {
              return item._id == itm.itemId || item.itemId == itm.itemId;
            });
            if(indeex>=0){
              var item=items[indeex];
              if(!itm.itemCode)
                itm.itemCode = item.itemCode;
              //console.log('item', item);
              if(item.preferedUnit != undefined){
                //console.log(item.itemName, item.preferedUnit);
                if(typeof item.preferedUnit == 'string'){
                  _.forEach(item.units,function(u,i){
                   if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                    {
                      var ind = _.findIndex(stockUnits, {_id: u._id});
                      if(ind >= 0)
                      {
                        u.baseUnit = stockUnits[ind].baseUnit;
                        u.conversionFactor = stockUnits[ind].conversionFactor;
                        u.baseConversionFactor = stockUnits[ind].baseConversionFactor;
                      }  
                    }
                    if(u._id==item.preferedUnit){
                      var conversionFactor=1;
                      var pconFac=parseFloat(u.conversionFactor);
                      itm.UnitName=u.unitName;
                      if(u.baseUnit.id==2 || u.baseUnit.id==3){
                        conversionFactor=parseFloat(u.baseConversionFactor);
                        if(pconFac>conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(1000)).toFixed(3);
                          
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                        //  console.log("var");
                        console.log('varrrrr');
                             itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                             itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(pconFac)).toFixed(3);
                        }
                      }
                      else
                      {
                        if(pconFac>conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                          }
                   
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                          }
                        //  console.log(pconFac)
                         // console.log(u);
                         console.log('varrrrr');
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                    }
                  });
                } else {
                  console.log(item.itemName);
                  _.forEach(item.units,function(u,i){
                    if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                    {
                      var ind = _.findIndex(stockUnits, {_id: u._id});
                      if(ind >= 0)
                      {
                        u.baseUnit = stockUnits[ind].baseUnit;
                        u.conversionFactor = stockUnits[ind].conversionFactor;
                        u.baseConversionFactor = stockUnits[ind].baseConversionFactor;
                      }  
                    }
                    if(u._id==item.preferedUnit._id){
                      var conversionFactor=1;
                      var pconFac=parseFloat(u.conversionFactor);
                      itm.UnitName=u.unitName;
                      if(u.baseUnit.id==2 || u.baseUnit.id==3){
                        conversionFactor=parseFloat(u.baseConversionFactor);
                        //console.log(itm.itemName,"mutton")
                        //console.log("conversionFactor",conversionFactor)
                        //console.log("pconFac",pconFac)
                        if(itm.physicalQty != undefined || itm.physicalQty!="NA")
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(pconFac)).toFixed(3);
                        if(pconFac>conversionFactor){
                          //console.log(itm.itemName,"mutton if")
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(1000)).toFixed(3);
                          
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(pconFac)).toFixed(3);
                        }
                      }
                      else
                      {
                        if(pconFac>conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                          }
                   
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                          }
                          //  console.log(pconFac)
                           // console.log(u);
                           console.log('varrrrr');
                            itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
                            itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                        }
                       
                      }
                    }
                  });
                }
              }
              else
                {
                  //console.log("pre not set");
                  var count=0
                  ///console.log(itm);
                   // console.log(item);
                    _.forEach(item.units,function(u){   
                        if(u.baseUnit.id==2 && count==0)
                          {
                           count++
                           //console.log(itm);
                           itm.varianceAmt = parseFloat(itm.varianceAmt * 1000 ).toFixed(3);
                         }
                    })
                } // end of forEach
            }
          });
         return result;
    },
    convertItemInPreferredUnit_DateWiseNew: function (){
          var result = this._data;
          var items = this._stockItems;
            console.log('result cvi',result);
          _.forEach(result.items,function(itm,i){
            var indeex=_.findIndex(items,function(item) {
              return item._id == itm.itemId || item.itemId == itm.itemId;
            });
            if(indeex>=0){
              var item=items[indeex];
              if(!itm.itemCode)
                itm.itemCode = item.itemCode;
              //console.log('item', item);
              if(item.preferedUnit != undefined){
                //console.log(item.itemName, item.preferedUnit);
                if(typeof item.preferedUnit == 'string'){
                  _.forEach(item.units,function(u,i){
                    if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                    {
                      var ind = _.findIndex(req.stockUnits, {_id: u._id});
                      if(ind >= 0)
                      {
                        u.baseUnit = req.stockUnits[ind].baseUnit;
                        u.conversionFactor = req.stockUnits[ind].conversionFactor;
                        u.baseConversionFactor = $scope.stockUnits[ind].baseConversionFactor;
                      }  
                    }
                    if(u._id==item.preferedUnit){
                      var conversionFactor=1;
                      var pconFac=parseFloat(u.conversionFactor);
                      itm.UnitName=u.unitName;
                      if(u.baseUnit.id==2 || u.baseUnit.id==3){
                        conversionFactor=parseFloat(u.baseConversionFactor);
                        if(pconFac>conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                         
                          
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                        //  console.log("var");
                          console.log('varrrrr');
                             itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                             
                        }
                      }
                      else
                      {
                        if(pconFac>conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                          }
                   
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
                          
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                          }
                        //  console.log(pconFac)
                         // console.log(u);
                         console.log('varrrrr');
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
                          
                        }
                      }
                    }
                  });
                } else {
                  console.log("Base Kitchen Item")
                  console.log(item.itemName);

                  _.forEach(item.units,function(u,i){
                    if(u._id==item.preferedUnit._id){
                      var conversionFactor=1;
                      var pconFac=parseFloat(u.conversionFactor);
                      itm.UnitName=u.unitName;
                      if(u.baseUnit.id==2 || u.baseUnit.id==3){
                         conversionFactor=parseFloat(u.baseConversionFactor);
                        //console.log(itm.itemName,"mutton")
                        //console.log("conversionFactor",conversionFactor)
                        //console.log("pconFac",pconFac)
                        if(itm.physicalQty != undefined || itm.physicalQty!="NA")
                          itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(pconFac)).toFixed(3);
                        if(pconFac>conversionFactor){
                          //console.log(itm.itemName,"mutton if")
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          
                          
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                          //itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          
                        }
                      }
                      else
                      {
                        console.log(item.itemName);
                        console.log("conversionFactor",conversionFactor)
                        console.log("pconFac",pconFac)
                        console.log("physicalQty",itm.physicalQty)
                        console.log("varianceQty",itm.varianceQty)
                        if(pconFac>conversionFactor){
                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                   
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          
                        }
                        else if(pconFac<conversionFactor){
                          itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                          }
                        //  console.log(pconFac)
                         // console.log(u);
                         console.log('varrrrr');
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                          
                        }
                       
                      }
                    }
                  });
                }
              }
              else
                {
                  //console.log("pre not set");
                  var count=0
                  ///console.log(itm);
                   // console.log(item);
                    _.forEach(item.units,function(u){   
                        if(u.baseUnit.id==2 && count==0)
                          {
                           count++
                           //console.log(itm);
                           itm.varianceAmt = parseFloat(itm.varianceAmt * 1000 ).toFixed(3);
                         }
                    })
                } // end of forEach
            }
          });
         return result;
    },
        convertItemInPreferredUnit_DateWise: function () {
          var result = this._data;
          var items = this._stockItems;
          var stockUnits = this._units;
          console.log("preferred");
          console.log(result);
          _.forEach(result.items, function (itm, i) {
            var indeex = _.findIndex(items, {_id: itm.itemId});
            if (indeex >= 0) {
              var item = items[indeex];
              if(!itm.itemCode)
                itm.itemCode = item.itemCode;
              if (item.preferedUnit != undefined) {
                _.forEach(item.units, function (u, i) {
                  if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                    {
                      var ind = _.findIndex(stockUnits, {_id: u._id});
                      if(ind >= 0)
                      {
                        u.baseUnit = stockUnits[ind].baseUnit;
                        u.conversionFactor = stockUnits[ind].conversionFactor;
                        u.baseConversionFactor = stockUnits[ind].baseConversionFactor;
                      }  
                    }
                  if (u._id == item.preferedUnit) {
                    var conversionFactor = 1;
                    var pconFac = parseFloat(u.conversionFactor);
                    itm.UnitName = u.unitName;
                    if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
                      conversionFactor = parseFloat(u.baseConversionFactor);
                      if (pconFac > conversionFactor) {
                        itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(conversionFactor)).toFixed(3);
                        itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(conversionFactor)).toFixed(3);
                        itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(conversionFactor)).toFixed(3);
                        itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(conversionFactor)).toFixed(3);
                        itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                        if (itm.physicalQty != "NA") {
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty= parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                          // itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(1000)).toFixed(3);
                        //  itm.varianceAmt= parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                      else if (pconFac < conversionFactor) {
                        itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(conversionFactor)).toFixed(3);
                        itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(conversionFactor)).toFixed(3);
                        itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(conversionFactor)).toFixed(3);
                        itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(conversionFactor)).toFixed(3);
                        itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                        if (itm.physicalQty != "NA") {
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                        //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(pconFac)).toFixed(3);
                      //    itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                    }
                    else {
                      if (pconFac > conversionFactor) {
                        itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(pconFac)).toFixed(3);
                        itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(pconFac)).toFixed(3);
                        itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(pconFac)).toFixed(3);
                        itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(pconFac)).toFixed(3);
                        itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                        if (itm.physicalQty != "NA") {
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
                       //   itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                        //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(pconFac)).toFixed(3);
                        }
                      }
                      else if (pconFac < conversionFactor) {
                        itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(pconFac)).toFixed(3);
                        itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(pconFac)).toFixed(3);
                        itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(pconFac)).toFixed(3);
                        itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(pconFac)).toFixed(3);
                        itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                        if (itm.physicalQty != "NA") {
                          if (itm.physicalQty == undefined) {
                            itm.physicalQty = parseFloat(0).toFixed(3);
                          }
                          else {
                            itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                          }
                          itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
                        //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                        //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(pconFac)).toFixed(3);
                        }
                      }
                    }
                  }
                });
              }
              //    else
              // {
              //   console.log("pre not set");
              //   var count=0
              //   console.log(itm);
              //    // console.log(item);
              //     _.forEach(item.units,function(u){   
              //         if(u.baseUnit.id==2 && count==0)
              //           {
              //            count++
              //             console.log(itm);
              //            itm.varianceAmt = parseFloat(itm.varianceAmt * 1000 ).toFixed(3);
              //          }
              //     })
              // } // end of forEach

            }
          });
          return result;
        },
         convertItemInPreferredUnit_DateWiseFoodCost: function () {
            var result = this._data;
            var items = this._stockItems;
            console.log("preferred");
            console.log(result);
            _.forEach(result.items, function (itm, i) {
              var indeex = _.findIndex(items, {_id: itm.itemId});
              if (indeex >= 0) {
                var item = items[indeex];
                if (item.preferedUnit != undefined) {
                  _.forEach(item.units, function (u, i) {
                    if (u._id == item.preferedUnit) {
                      var conversionFactor = 1;
                      var pconFac = parseFloat(u.conversionFactor);
                      itm.UnitName = u.unitName;
                      if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
                        conversionFactor = parseFloat(u.baseConversionFactor);
                        if (pconFac > conversionFactor) {
                          itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                          itm.openingAmt = parseFloat(parseFloat(itm.openingAmt) / parseFloat(conversionFactor)).toFixed(3);
                          itm.closingAmount = parseFloat(parseFloat(itm.closingAmount) / parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionAmount = parseFloat(parseFloat(itm.consumptionAmount) / parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseAmount = parseFloat(parseFloat(itm.purchaseAmount) / parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageAmt = parseFloat(parseFloat(itm.wastageAmt) / parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty != "NA") {
                            if (itm.physicalQty == undefined) {
                              itm.physicalQty = parseFloat(0).toFixed(3);
                            }
                            else {
                             itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                              itm.physicalAmt = parseFloat(parseFloat(itm.physicalAmt) * parseFloat(conversionFactor)).toFixed(3);
                            }
                            itm.varianceQty= parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                            // itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(1000)).toFixed(3);
                          //  itm.varianceAmt= parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                          }
                        }
                        else if (pconFac < conversionFactor) {
                          itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(conversionFactor)).toFixed(3);
                          itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                          itm.openingAmt = parseFloat(parseFloat(itm.openingAmt) * parseFloat(conversionFactor)).toFixed(3);
                          itm.closingAmount = parseFloat(parseFloat(itm.closingAmount) * parseFloat(conversionFactor)).toFixed(3);
                          itm.consumptionAmount = parseFloat(parseFloat(itm.consumptionAmount) * parseFloat(conversionFactor)).toFixed(3);
                          itm.purchaseAmount = parseFloat(parseFloat(itm.purchaseAmount) * parseFloat(conversionFactor)).toFixed(3);
                          itm.wastageAmt = parseFloat(parseFloat(itm.wastageAmt) * parseFloat(conversionFactor)).toFixed(3);
                          if (itm.physicalQty != "NA") {
                            if (itm.physicalQty == undefined) {
                              itm.physicalQty = parseFloat(0).toFixed(3);
                            }
                            else {
                              itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                              itm.physicalAmt = parseFloat(parseFloat(itm.physicalAmt) * parseFloat(conversionFactor)).toFixed(3);
                            }
                            itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                          //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(pconFac)).toFixed(3);
                        //    itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(conversionFactor)).toFixed(3);
                          }
                        }
                      }
                      else {
                        if (pconFac > conversionFactor) {
                          itm.openingQty = parseFloat(parseFloat(itm.openingQty) / parseFloat(pconFac)).toFixed(3);
                          itm.closingQty = parseFloat(parseFloat(itm.closingQty) / parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) / parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) / parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                          itm.openingAmt = parseFloat(parseFloat(itm.openingAmt) / parseFloat(pconFac)).toFixed(3);
                          itm.closingAmount = parseFloat(parseFloat(itm.closingAmount) / parseFloat(pconFac)).toFixed(3);
                          itm.consumptionAmount = parseFloat(parseFloat(itm.consumptionAmount) / parseFloat(pconFac)).toFixed(3);
                          itm.purchaseAmount = parseFloat(parseFloat(itm.purchaseAmount) / parseFloat(pconFac)).toFixed(3);
                          itm.wastageAmt = parseFloat(parseFloat(itm.wastageAmt) / parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty != "NA") {
                            if (itm.physicalQty == undefined) {
                              itm.physicalQty = parseFloat(0).toFixed(3);
                            }
                            else {
                              itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                              itm.physicalAmt = parseFloat(parseFloat(itm.physicalAmt) / parseFloat(pconFac)).toFixed(3);
                            }
                            itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
                         //   itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                          //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(pconFac)).toFixed(3);
                          }
                        }
                        else if (pconFac < conversionFactor) {
                          itm.openingQty = parseFloat(parseFloat(itm.openingQty) * parseFloat(pconFac)).toFixed(3);
                          itm.closingQty = parseFloat(parseFloat(itm.closingQty) * parseFloat(pconFac)).toFixed(3);
                          itm.consumptionQty = parseFloat(parseFloat(itm.consumptionQty) * parseFloat(pconFac)).toFixed(3);
                          itm.purchaseQty = parseFloat(parseFloat(itm.purchaseQty) * parseFloat(pconFac)).toFixed(3);
                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(pconFac)).toFixed(3);
                          itm.openingAmt = parseFloat(parseFloat(itm.openingAmt) * parseFloat(pconFac)).toFixed(3);
                          itm.closingAmount = parseFloat(parseFloat(itm.closingAmount) * parseFloat(pconFac)).toFixed(3);
                          itm.consumptionAmount = parseFloat(parseFloat(itm.consumptionAmount) *parseFloat(pconFac)).toFixed(3);
                          itm.purchaseAmount = parseFloat(parseFloat(itm.purchaseAmount) * parseFloat(pconFac)).toFixed(3);
                          itm.wastageAmt = parseFloat(parseFloat(itm.wastageAmt) * parseFloat(pconFac)).toFixed(3);
                          if (itm.physicalQty != "NA") {
                            if (itm.physicalQty == undefined) {
                              itm.physicalQty = parseFloat(0).toFixed(3);
                            }
                            else {
                              itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(pconFac)).toFixed(3);
                              itm.physicalAmt = parseFloat(parseFloat(itm.physicalAmt) * parseFloat(pconFac)).toFixed(3);
                            }
                            itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(pconFac)).toFixed(3);
                          //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) / parseFloat(conversionFactor)).toFixed(3);
                          //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(pconFac)).toFixed(3);
                          }
                        }
                      }
                    }
                  });
                }
                //    else
                // {
                //   console.log("pre not set");
                //   var count=0
                //   console.log(itm);
                //    // console.log(item);
                //     _.forEach(item.units,function(u){   
                //         if(u.baseUnit.id==2 && count==0)
                //           {
                //            count++
                //             console.log(itm);
                //            itm.varianceAmt = parseFloat(itm.varianceAmt * 1000 ).toFixed(3);
                //          }
                //     })
                // } // end of forEach

              }
            });
            return result;
        },
        getClosingQtyPreviousDate: function (items, pdate, item) {
          var cQTy = 0;
          var index = _.findIndex(items, {date: pdate});
          if (index >= 0) {
            var itmIndex = _.findIndex(items[index].items, {itemId: item.itemId});
            if (itmIndex >= 0) {
              if (items[index].items[itmIndex].physicalQty != "NA") {
                cQTy = parseFloat(items[index].items[itmIndex].closingQty).toFixed(3);
              }
              else {
                cQTy = parseFloat(items[index].items[itmIndex].physicalQty).toFixed(3);
              }
            }
          }
          return cQTy;
        },
        formatDataForReport: function () {
          var self= this;
          console.log("last price")

          var startDate = this._fromDate;
          var endDate = this._toDate;
          var result = this._data;
          var deployment = this.deployment;
          result.items = [];
          var endDateToShow = new Date(endDate);
          var startDateToShow = new Date(startDate);
          endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
          _.forEach(result.beforeDate,function(itm,i){
            var lastPriceIndex=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
            if(itm.totalOpeningQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(startDate.getTime() < new Date(itm.created).getTime())
                startDateToShow = new Date(itm.created);
                itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
                itm.openingAmt=parseFloat( parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
              //  console.log("index not found");
              //  console.log("itm",JSON.stringify(itm));
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat( parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt=parseFloat(  parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat( parseFloat(result.items[itmIndex].openingAmt)+   parseFloat(itm.totalBalanceQty)* parseFloat(self._lastPrice[itmIndex].monthAverage) ).toFixed(3);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceQty) *  parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if(itm.totalPurchaseQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                 itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount) ).toFixed(2);
              }
            }

            else if(itm.totalIntermediateQty!=undefined){
              //console.log(itm)
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});

              if(itmIndex<0){
                console.log(itmIndex)
                console.log(self._lastPrice[itmIndex]);
                itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalIntermediateQty) * parseFloat( self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)- parseFloat(itm.totalIntermediateQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-  parseFloat(itm.totalIntermediateQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if(itm.totalSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount) ).toFixed(2);
              }
            }
            else if(itm.totalWastageQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(  parseFloat(result.items[itmIndex].openingAmt)+  parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if(itm.totalTransferQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                //console.log("avg",self._lastPrice[lastPriceIndex].monthAverage)

                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(    parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-   parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if(itm.totalTransferQty_toStore!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt=parseFloat(  parseFloat(itm.totalTransferQty_toStore)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat( parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalTransferQty_toStore)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat( parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalTransferQty_toStore)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
              }
            }
            else if(itm.itemSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                itm.UnitName=itm.baseUnit;
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)  ).toFixed(2);
              }
            }
            else if(itm.totalStockReturnQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)- parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-  parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)   ).toFixed(2);
              }
            }
            else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.openingAmt=parseFloat(  parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
              else if(itm.totalIndentingChallanQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalIndentingChallanAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
              }
            }
          });
          _.forEach(result.betweenDate,function(itm,i){
             var lastPriceIndex=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
            if(itm.totalPurchaseQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                 if(itm.varianceAmt == undefined) {
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
             // console.log('itm', itm);
              var cDate=new Date(itm.actualDate);
              //cDate.setDate(cDate.getDate()-1);
              //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
              //var ed = new Date(endDate);
              //ed = new Date(ed.setDate(ed.getDate()-1));
              //console.log(cDate);
              //console.log(endDate);
              //console.log(ed);
              if(itmIndex<0){
                //itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                //itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                if(endDate.getTime()==cDate.getTime()){
                  itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                  itm.physicalAmt=parseFloat( parseFloat(itm.totalBalanceQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                }
                else{
                  itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  itm.physicalDate = new Date(itm.created);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                if(itm.varianceAmt == undefined) {
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                if(endDate.getTime()==cDate.getTime()){
                  result.items[itmIndex].physicalQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  result.items[itmIndex].physicalAmt=parseFloat(parseFloat( parseFloat(itm.totalBalanceQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )).toFixed(2);
                  result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
                  result.items[itmIndex].varianceAmt=parseFloat( parseFloat(  result.items[itmIndex].varianceQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                  result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);

                  //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                }
                else {
                  //console.log(itm.itemName, " else");
                  //console.log(result.items[itmIndex].closingQty);
                  //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  if(result.items[itmIndex].physicalDate) {
                    //console.log(result.items[itmIndex].physicalDate)
                    //console.log(cDate)
                    if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                      //console.log('if');
                      result.items[itmIndex].closingQty = Utils.roundNumber(parseFloat(itm.totalBalanceQty), 3);
                      //console.log(result.items[itmIndex].closingQty);
                    } else {
                      //console.log('else');
                      result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                      result.items[itmIndex].physicalDate = cDate;
                    }
                  } else {
                    result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                    result.items[itmIndex].physicalDate = cDate;
                  }
                  // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
                  // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
                  // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                }
              }
            }
            else if(itm.totalIntermediateQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(parseFloat(-itm.totalIntermediateQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }

                itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                itm.consumptionAmount=parseFloat(parseFloat(itm.totalIntermediateQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat( parseFloat(itm.totalIntermediateQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat( parseFloat(itm.totalIntermediateQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )).toFixed(2);
              }
            }
            else if(itm.totalSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
            }
            else if(itm.totalWastageQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.totalWastageQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat( parseFloat(-itm.totalWastageQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                  itm.wastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
                  itm.wastageAmt = parseFloat( parseFloat(itm.totalWastageQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                // if(itm.wastageQty == undefined) {
                //   itm.wastageQty=parseFloat(itm.totalWastageQty).toFixed(3);
                //   itm.wastageAmount=parseFloat(itm.totalWastageAmt).toFixed(3);
                // }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].wastageQty=parseFloat(parseFloat(result.items[itmIndex].wastageQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].wastageAmount=parseFloat(parseFloat(  parseFloat(result.items[itmIndex].wastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )+parseFloat( parseFloat(itm.totalWastageQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat( parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )).toFixed(2);
              }
            }
            else if(itm.totalTransferQty_toStore!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat( parseFloat(itm.totalTransferQty_toStore) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.purchaseAmount=parseFloat( parseFloat(itm.totalTransferQty_toStore)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ) .toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat( parseFloat(itm.totalTransferQty_toStore) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat( parseFloat(itm.totalTransferQty_toStore) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )).toFixed(2);
              }
            }
            else if(itm.totalTransferQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat( parseFloat(-itm.totalTransferQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                itm.consumptionAmount=parseFloat( parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat( parseFloat(itm.totalTransferQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) )).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }
            else if(itm.itemSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                itm.UnitName=itm.baseUnit;
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
              }
            }
            else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                //console.log(itmIndex);
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
            }
            else if(itm.totalStockReturnQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalStockReturnAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }
            }
            else if(itm.totalIndentingChallanQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalIndentingChallanAmt).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalIndentingChallanQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
              }
            }

          });
          return result;
        },
        formatDataForReportViaPhysical: function () {
          var startDate = this._fromDate;
          var endDate = this._toDate;
          var result = this._data;
          var deployment = this.deployment;
          result.items = [];
          var endDateToShow = new Date(endDate);
          var startDateToShow = new Date(startDate);
          endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
          _.forEach(result.beforeDate,function(itm,i){
            if(itm.totalOpeningQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(startDate.getTime() < new Date(itm.created).getTime())
                startDateToShow = new Date(itm.created);
                itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                //console.log('total balance', itm.totalBalanceQty);
                result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
              }
            }
            else if(itm.totalPurchaseQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
              }
            }
            /*else if(itm.totalIntermediateQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
              }
            }
            else if(itm.totalSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
            }
            else if(itm.totalWastageQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
              }
            }
            else if(itm.totalTransferQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }*/
            else if(itm.totalTransferQty_toStore!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
              }
            }
            /*else if(itm.itemSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                itm.UnitName=itm.baseUnit;
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
              }
            }*/
            /*else if(itm.totalStockReturnQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalStockReturnAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }
            }*/
            else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
            }

          });
          _.forEach(result.betweenDate,function(itm,i){
            if(itm.totalPurchaseQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              console.log('itm', itm);
              var cDate=new Date(itm.created);
              //cDate.setDate(cDate.getDate()-1);
              cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
              cDate =new Date(cDate.setDate(cDate.getDate()+1));
              //var ed = new Date(endDate);
              //ed = new Date(ed.setDate(ed.getDate()-1));
              console.log('cDate',cDate);
              console.log('endDate',endDate);
              //console.log(ed);
              if(itmIndex<0){
                //itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                //itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(parseFloat(itm.openingQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
                itm.consumptionAmount=parseFloat(parseFloat(itm.openingAmt)-parseFloat(itm.totalBalanceAmt)).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                //itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                if(endDate.getTime()==cDate.getTime()){
                  itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                  itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                  itm.consumptionQty=parseFloat(parseFloat(itm.openingQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);

                }
                else{
                  itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  itm.consumptionQty=parseFloat(parseFloat(itm.openingQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);

                  itm.physicalDate = new Date(itm.created);
                }
                /*
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }*/
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                if(endDate.getTime()==cDate.getTime()){
                  result.items[itmIndex].physicalQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  result.items[itmIndex].physicalAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                  result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(result.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);

                  //result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
                  //result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);

                  result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                }
                else {
                  //console.log(itm.itemName, " else");
                  //console.log(result.items[itmIndex].closingQty);
                  //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  if(result.items[itmIndex].physicalDate) {
                    //console.log(result.items[itmIndex].physicalDate)
                    //console.log(cDate)
                    if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                      //console.log('if');
                      result.items[itmIndex].closingQty = Utils.roundNumber(parseFloat(itm.totalBalanceQty), 3);
                      //console.log(result.items[itmIndex].closingQty);
                    result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(result.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);

                    } else {
                      //console.log('else');
                      result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                      result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(result.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);

                      result.items[itmIndex].physicalDate = cDate;
                    }
                  } else {
                    result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                    result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(result.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);

                    result.items[itmIndex].physicalDate = cDate;
                  }
                  // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
                  // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
                  // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                }
              }
              /*else
              {
                result.items[itmIndex].closingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(itm.totalBalanceAmt).toFixed(3);
                result.items[itmIndex].physicalQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(result.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(result.items[itmIndex].purchaseAmount)-parseFloat(itm.totalBalanceAmt)).toFixed(2)
              }*/
            }
            /*else if(itm.totalIntermediateQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIntermediateAmt)).toFixed(2);
              }
            }
            else if(itm.totalSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
            }
            else if(itm.totalWastageQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.totalWastageQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
                  itm.wastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(itm.totalWastageAmt).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                // if(itm.wastageQty == undefined) {
                //   itm.wastageQty=parseFloat(itm.totalWastageQty).toFixed(3);
                //   itm.wastageAmount=parseFloat(itm.totalWastageAmt).toFixed(3);
                // }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].wastageQty=parseFloat(parseFloat(result.items[itmIndex].wastageQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].wastageAmount=parseFloat(parseFloat(result.items[itmIndex].wastageAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
              }
            }*/
            else if(itm.totalTransferQty_toStore!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
              }
            }
            /*else if(itm.totalTransferQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }
            else if(itm.itemSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                itm.UnitName=itm.baseUnit;
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
              }
            }*/
            else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                //console.log(itmIndex);
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
            }
            /*else if(itm.totalStockReturnQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalStockReturnAmount).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }
            }*/

          });
          return result;
        },
        formatDataForReport_DateWise: function () {
          var self=this
          var startDate = this._fromDate;
          var endDate = this._toDate;
          var endDateToShow = new Date(endDate.setDate(endDate.getDate() - 1));
          var result = this._data;
          result.items = [];
          _.forEach(result.beforeDate, function (itm, i) {
            var lastPriceIndex=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
            if (itm.totalOpeningQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalOpeningQty).toFixed(3);
                itm.openingAmt=parseFloat( parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(3);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat( parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if (itm.totalBalanceQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt=parseFloat(  parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.created = itm.actualDate;
                result.items.push(itm);
              }
              else {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat( parseFloat(result.items[itmIndex].openingAmt)+   parseFloat(itm.totalBalanceQty)* parseFloat(self._lastPrice[itmIndex].monthAverage) ).toFixed(3);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceQty) *  parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if (itm.totalPurchaseQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalPurchaseAmount).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].openingAmt= parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
              }
            }
            else if (itm.totalIntermediateQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalIntermediateQty) * parseFloat( self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                 result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)- parseFloat(itm.totalIntermediateQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-  parseFloat(itm.totalIntermediateQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if (itm.totalSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.totalSaleAmount).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
            }
            else if (itm.totalWastageQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                 result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(  parseFloat(result.items[itmIndex].openingAmt)+  parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if (itm.totalTransferQty != undefined) {
              console.log("itm",itm)
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                 result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(    parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-   parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if (itm.totalTransferQty_toStore != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt=parseFloat(  parseFloat(itm.totalTransferQty_toStore)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat( parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalTransferQty_toStore)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat( parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalTransferQty_toStore)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
              }
            }
            else if (itm.itemSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.itemSaleAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.UnitName = itm.baseUnit;
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.itemSaleAmt)).toFixed(2);
              }
            }
            else if (itm.totalStockReturnQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
                itm.openingAmt=parseFloat(- parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)- parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-  parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)   ).toFixed(2);
              }
            }
            else if (itm.totalStockReceiveQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.openingAmt=parseFloat(  parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              }
            }
            else if(itm.totalIndentingChallanQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalIndentingChallanAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceAmt = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
              }
            }
          });
          _.forEach(result.betweenDate, function (itm, i) {
            var lastPriceIndex=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
            if (itm.totalPurchaseQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalPurchaseAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.purchaseAmount = parseFloat(itm.totalPurchaseAmount).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalBalanceQty != undefined) {
              //console.log('Between item in physical :-  ',JSON.stringify(itm));
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(0).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDate);
                //itm.created = itm.actualDate;
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
              }
              itm.varianceQty = parseFloat(0).toFixed(3);
              itm.varianceAmt = parseFloat(0).toFixed(3);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.physicalAmt = parseFloat( parseFloat(itm.totalBalanceQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalIntermediateQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat( parseFloat(-itm.totalIntermediateQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalIntermediateQty).toFixed(3);
              itm.consumptionAmount = parseFloat( parseFloat(itm.totalIntermediateQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalSaleAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalSaleQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalSaleAmount).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalWastageQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(parseFloat(-itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              itm.wastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
              itm.wastageAmt = parseFloat( parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalTransferQty_toStore != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat( parseFloat(itm.totalTransferQty_toStore) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.purchaseAmount = parseFloat( parseFloat(itm.totalTransferQty_toStore) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalTransferQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(parseFloat(-itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalTransferQty).toFixed(3);
              itm.consumptionAmount = parseFloat( parseFloat(itm.totalTransferQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.itemSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.itemSaleAmt).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.itemSaleQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.itemSaleAmt).toFixed(2);
              itm.UnitName = itm.baseUnit;
              result.items.push(itm);
            }
            else if (itm.totalStockReceiveQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
              itm.purchaseAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalStockReturnQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalStockReturnAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceAmt = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalStockReturnQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalStockReturnAmount).toFixed(2);
              result.items.push(itm);
            }
            else if(itm.totalIndentingChallanQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalIndentingChallanAmt).toFixed(3);
                  itm.wastageQty = parseFloat(0).toFixed(3);
                  itm.wastageAmt = parseFloat(0).toFixed(2);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmt=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                  itm.varianceAmt=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalIndentingChallanQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
              }
            }
          });
         
          //console.log("result date wise",result);
          return result;
        },
        // formatDataForReport_DateWiseFoodCost: function () {
        //   var self=this
        //   var startDate = this._fromDate;
        //   var endDate = this._toDate;
        //   var endDateToShow = new Date(endDate.setDate(endDate.getDate() - 1));
        //   var result = this._data;
        //   result.items = [];
        //   _.forEach(result.beforeDate, function (itm, i) {
        //     var lastPriceIndex=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
        //     if (itm.totalOpeningQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(itm.totalOpeningQty).toFixed(3);
        //         itm.openingAmt=parseFloat( parseFloat(itm.ConversionFactor)* parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(3);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //         result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat( parseFloat(itm.ConversionFactor)* parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);

        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalBalanceQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
        //         itm.openingAmt=parseFloat(  parseFloat(itm.ConversionFactor)* parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.created = itm.actualDate;
        //         result.items.push(itm);
        //       }
        //       else {
        //       result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].openingAmt)+   parseFloat(itm.totalBalanceQty)* parseFloat(self._lastPrice[itmIndex].monthAverage) ).toFixed(3);

        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(itm.totalBalanceQty) *  parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalPurchaseQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
        //         itm.openingAmt = parseFloat(itm.totalPurchaseAmount).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //         result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt= parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);

        //         result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalIntermediateQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
        //         itm.openingAmt=parseFloat(-  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalIntermediateQty) * parseFloat( self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].openingAmt)- parseFloat(itm.totalIntermediateQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount)-  parseFloat(itm.totalIntermediateQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalSaleQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
        //         itm.openingAmt = parseFloat(- parseFloat(itm.ConversionFactor)*itm.totalSaleAmount).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //         result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt = parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalSaleAmount)).toFixed(2);

        //         result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount = parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalSaleAmount)).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalWastageQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
        //         itm.openingAmt=parseFloat(-  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat(  parseFloat(itm.ConversionFactor)* parseFloat(result.items[itmIndex].openingAmt)+  parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalTransferQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
        //         itm.openingAmt=parseFloat(- parseFloat(itm.ConversionFactor)* parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat(   parseFloat(itm.ConversionFactor)*  parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);

        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount)-   parseFloat(itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalTransferQty_toStore != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        //         itm.openingAmt=parseFloat(  parseFloat(itm.ConversionFactor)* parseFloat(itm.totalTransferQty_toStore)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(3);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //         result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat( parseFloat(itm.ConversionFactor)* parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalTransferQty_toStore)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)* parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalTransferQty_toStore)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //       }
        //     }
        //     else if (itm.itemSaleQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
        //         itm.openingAmt = parseFloat(parseFloat(-itm.itemSaleAmt)* parseFloat(itm.ConversionFactor)  ).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.UnitName = itm.baseUnit;
        //         result.items.push(itm);
        //       }
        //       else {
        //         result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt = parseFloat( parseFloat(itm.ConversionFactor)* parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.itemSaleAmt)).toFixed(2);

        //         result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.itemSaleAmt)).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalStockReturnQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
        //         itm.openingAmt=parseFloat(- parseFloat(itm.ConversionFactor)* parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //         result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].openingAmt)- parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount)-  parseFloat(itm.totalStockReturnQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)   ).toFixed(2);
        //       }
        //     }
        //     else if (itm.totalStockReceiveQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itmIndex < 0) {
        //         itm.openingQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
        //         itm.openingAmt=parseFloat(  parseFloat(itm.ConversionFactor)* parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(2);
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(2);
        //         itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(2);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         result.items.push(itm);
        //       }
        //       else {
        //          result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        //         result.items[itmIndex].openingAmt=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].openingAmt)+ parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);

        //         result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
        //         result.items[itmIndex].closingAmount=parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(result.items[itmIndex].closingAmount)+  parseFloat(itm.totalStockReceiveQty)  * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //       }
        //     }
        //   });
        //   _.forEach(result.betweenDate, function (itm, i) {
        //     var lastPriceIndex=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
        //     if (itm.totalPurchaseQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(parseFloat(itm.totalPurchaseAmount) * parseFloat(itm.ConversionFactor) ).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.consumptionQty == undefined) {
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.purchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
        //       itm.purchaseAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalPurchaseAmount ) ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalBalanceQty != undefined) {
        //       //console.log('Between item in physical :-  ',JSON.stringify(itm));
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(0).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(0).toFixed(3);
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDate);
        //         //itm.created = itm.actualDate;
        //       }
        //       if (itm.consumptionQty == undefined) {
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.purchaseQty == undefined) {
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //       }
        //       itm.varianceQty = parseFloat(0).toFixed(3);
        //       itm.varianceAmt = parseFloat(0).toFixed(3);
        //       itm.varianceInPercent = parseFloat(0).toFixed(2);
        //       itm.physicalQty = parseFloat(itm.totalBalanceQty).toFixed(3);
        //       itm.physicalAmt = parseFloat(   parseFloat(itm.ConversionFactor)* parseFloat(itm.totalBalanceQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)  ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalIntermediateQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat( parseFloat(itm.ConversionFactor)* parseFloat(-itm.totalIntermediateQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.purchaseQty == undefined) {
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.consumptionQty = parseFloat(itm.totalIntermediateQty).toFixed(3);
        //       itm.consumptionAmount = parseFloat( parseFloat(itm.ConversionFactor)* parseFloat(itm.totalIntermediateQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalSaleQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(- parseFloat(itm.ConversionFactor)*parseFloat(itm.totalSaleAmount) ).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.purchaseQty == undefined) {
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.consumptionQty = parseFloat(itm.totalSaleQty).toFixed(3);
        //       itm.consumptionAmount = parseFloat( parseFloat(itm.ConversionFactor)*  parseFloat(itm.totalSaleAmount) ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalWastageQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(-itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.purchaseQty == undefined) {
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.consumptionQty == undefined) {
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(3);
        //       }
        //       itm.wastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
        //       itm.wastageAmt = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalWastageQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage)).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalTransferQty_toStore != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalTransferQty_toStore) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.consumptionQty == undefined) {
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.purchaseQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
        //       itm.purchaseAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalTransferQty_toStore) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalTransferQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat( parseFloat(itm.ConversionFactor)*parseFloat(-itm.totalTransferQty)* parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.purchaseQty == undefined) {
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.consumptionQty = parseFloat(itm.totalTransferQty).toFixed(3);
        //       itm.consumptionAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalTransferQty) * parseFloat(self._lastPrice[lastPriceIndex].monthAverage) ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.itemSaleQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(-itm.itemSaleAmt).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.purchaseQty == undefined) {
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.consumptionQty = parseFloat(itm.itemSaleQty).toFixed(3);
        //       itm.consumptionAmount = parseFloat( parseFloat(itm.ConversionFactor)*parseFLoat(itm.itemSaleAmt) ).toFixed(2);
        //       itm.UnitName = itm.baseUnit;
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalStockReceiveQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(itm.totalStockReceiveQty)  ).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.consumptionQty == undefined) {
        //         itm.consumptionQty = parseFloat(0).toFixed(3);
        //         itm.consumptionAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.purchaseQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
        //       itm.purchaseAmount = parseFloat(   parseFloat(itm.ConversionFactor)* parseFloat(itm.totalStockReceiveAmount) ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //     else if (itm.totalStockReturnQty != undefined) {
        //       var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
        //       if (itm.openingQty == undefined) {
        //         itm.openingQty = parseFloat(0).toFixed(3);
        //         itm.closingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
        //         itm.openingAmt = parseFloat(0).toFixed(3);
        //         itm.closingAmount = parseFloat(  parseFloat(itm.ConversionFactor)*parseFloat(-itm.totalStockReturnAmount) ).toFixed(3);
        //         itm.totalBalanceQty = parseFloat(0).toFixed(3);
        //         itm.physicalQty = "NA";
        //         itm.physicalAmt = parseFloat(0).toFixed(2);
        //         itm.varianceQty = "NA";
        //         itm.varianceAmt = "NA";
        //         itm.varianceInPercent = "NA";
        //         itm.openingDate = new Date(startDate);
        //         itm.closingDate = new Date(endDateToShow);
        //       }
        //       if (itm.purchaseQty == undefined) {
        //         itm.purchaseQty = parseFloat(0).toFixed(3);
        //         itm.purchaseAmount = parseFloat(0).toFixed(3);
        //       }
        //       if (itm.wastageQty == undefined) {
        //         itm.wastageQty = parseFloat(0).toFixed(3);
        //         itm.wastageAmt = parseFloat(0).toFixed(3);
        //       }
        //       itm.consumptionQty = parseFloat(itm.totalStockReturnQty).toFixed(3);
        //       itm.consumptionAmount = parseFloat(  parseFloat(itm.ConversionFactor)* parseFloat(itm.totalStockReturnAmount)  ).toFixed(2);
        //       result.items.push(itm);
        //     }
        //   });
        //   console.log("---------------------------------------------");
        //   //console.log("result date wise",result);
        //   return result;
        // },
     formatDataForReport_DateWiseFoodCost : function(resu){
        
        var startDate = this._fromDate;
            var endDate = this._toDate;
            var result = angular.copy(resu) 
            var deployment = this.deployment;
        result.items = [];
        result.dateWiseItems = {};
        var endDateToShow = new Date(endDate);
        var startDateToShow = new Date(startDate)
        endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
         // console.log('in format result', angular.copy(result) );
        _.forEach(result.beforeDate,function(itm,i){
          //console.log("item before")
          // console.log(angular.copy(itm) )
             // console.log("before date",angular.copy(itm) )

           if(itm.totalOpeningQty!=undefined){
                    

               //   console.log("inside opening",angular.copy(itm) )
               var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                if(itmIndex<0){
                 // console.log("index in open")
                //  console.log(angular.copy(itm))
                if(startDate.getTime() < new Date(itm.created).getTime())
                  startDateToShow = new Date(itm.created);
                itm.openingQty=Number(itm.totalOpeningQty)
                itm.openingAmt=Number(itm.totalOpeningAmt)
                itm.purchaseQty=Number(0)
                itm.purchaseAmount=Number(0)
                itm.consumptionQty=Number(0)
                itm.consumptionAmount=Number(0)
                itm.closingQty=Number(itm.openingQty)
                itm.closingAmount=Number(itm.openingAmt)
                itm.wastageQty = Number(0)
                itm.totalBalanceQty = Number(0)
                itm.wastageAmt = Number(0)
                itm.varianceQty = Number(0)
                itm.varianceInPercent = Number(0)
                itm.physicalQty = Number(0)
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              
              }
              else
              {
                //console.log("NOOOOO index in open")
                //console.log("index found opening qty")
                console.log(angular.copy(itm))
                result.items[itmIndex].openingQty=Number(result.items[itmIndex].openingQty+itm.totalOpeningQty)
                result.items[itmIndex].openingAmt=Number(result.items[itmIndex].openingAmt+itm.totalOpeningAmt)

                result.items[itmIndex].closingQty=Number(result.items[itmIndex].closingQty+itm.totalOpeningQty)
                result.items[itmIndex].closingAmount=Number(result.items[itmIndex].closingAmount+itm.totalOpeningAmt)
              }
          }

          else if(itm.totalBalanceQty!=undefined){
          //  console.log("before totalBalanceQty")
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=Number(itm.totalBalanceQty)
              itm.openingAmt=Number(itm.totalBalanceAmt)
              itm.purchaseQty=Number(0)
              itm.purchaseAmount=Number(0)
              itm.consumptionQty=Number(0)
              itm.consumptionAmount=Number(0)
              itm.closingQty=Number(itm.openingQty)
              itm.closingAmount=Number(itm.openingAmt)
              itm.wastageQty = Number(0)
              itm.totalBalanceQty = Number(0)
              itm.wastageAmt = Number(0)
              itm.varianceQty = Number(0)
              itm.varianceInPercent = Number(0)
              itm.physicalQty = Number(0)
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
              result.items.push(itm);
            }
            else
            {
              //console.log('total balance', itm.totalBalanceQty);
              result.items[itmIndex].openingQty=Number(itm.totalBalanceQty)
              result.items[itmIndex].openingAmt=Number(itm.totalBalanceAmt)

              result.items[itmIndex].closingQty=Number(itm.totalBalanceQty)
              result.items[itmIndex].closingAmount=Number(itm.totalBalanceAmt)
            }
          }

           else if(itm.totalPurchaseQty != undefined && itm.totalIntermediateQty != undefined && itm.wastage != undefined
              && itm.totalTransferQty!= undefined && itm.totalTransferQty_toStore!= undefined && itm.itemSaleQty!=undefined
               && itm.totalStockReturnQty!=undefined && itm.totalStockReceiveQty!= undefined && itm.totalSaleQty!=undefined
               && itm.totalIndentingChallanQty!=undefined 

             ) 
           {
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});

              if(itmIndex<0){
                  itm.openingQty=Number(itm.totalPurchaseQty - itm.totalIntermediateQty - itm.wastage - itm.totalSaleQty - itm.totalTransferQty
                 + itm.totalTransferQty_toStore - itm.itemSaleQty - itm.totalStockReturnQty + itm.totalStockReceiveQty - itm.totalIndentingChallanQty
                  );
                  itm.openingAmt=Number(itm.totalPurchaseAmount - itm.totalIntermediateAmt - itm.wastageAmt - itm.totalSaleAmount - itm.totalTransferAmount
                  + itm.totalTransferQtyAmount_toStore - itm.itemSaleAmt - itm.totalStockReturnAmount + itm.totalStockReceiveAmount - itm.totalIndentingChallanAmt
                  );
                itm.purchaseQty=Number(0);
                itm.purchaseAmount=Number(0);
                itm.consumptionQty=Number(0);
                itm.consumptionAmount=Number(0);
                itm.closingQty=Number(itm.openingQty);
                itm.closingAmount=Number(itm.openingAmt);
                itm.wastageQty = Number(0);
                itm.totalBalanceQty = Number(0);
                itm.wastageAmt = Number(0);
                itm.varianceQty = Number(0);
                itm.varianceInPercent = Number(0);
                itm.physicalQty = Number(0);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                //console.log('total balance', result.items[itmIndex].openingQty);
                //console.log(itm.totalPurchaseAmount ,itm.totalIntermediateAmt , itm.wastageAmt , itm.totalSaleAmount , itm.totalTransferAmount
                  //, itm.totalTransferAmount_toStore ,itm.itemSaleAmt , itm.totalStockReturnAmount , itm.totalStockReceiveAmount);
                result.items[itmIndex].openingQty += Number(itm.totalPurchaseQty - itm.totalIntermediateQty - itm.wastage - itm.totalSaleQty - itm.totalTransferQty
                 + itm.totalTransferQty_toStore - itm.itemSaleQty - itm.totalStockReturnQty + itm.totalStockReceiveQty - itm.totalIndentingChallanQty);
               
                result.items[itmIndex].openingAmt += Number(itm.totalPurchaseAmount - itm.totalIntermediateAmt - itm.wastageAmt - itm.totalSaleAmount - itm.totalTransferAmount
                  + itm.totalTransferAmount_toStore - itm.itemSaleAmt - itm.totalStockReturnAmount + itm.totalStockReceiveAmount - itm.totalIndentingChallanAmt);

                result.items[itmIndex].closingQty += Number(  itm.openingQty=itm.totalPurchaseQty - itm.totalIntermediateQty - itm.wastage - itm.totalSaleQty - itm.totalTransferQty
                 + itm.totalTransferQty_toStore - itm.itemSaleQty - itm.totalStockReturnQty + itm.totalStockReceiveQty  -itm.totalIndentingChallanQty );
               
                result.items[itmIndex].closingAmount += Number(itm.totalPurchaseAmount - itm.totalIntermediateAmt - itm.wastageAmt - itm.totalSaleAmount - itm.totalTransferAmount
                  + itm.totalTransferAmount_toStore - itm.itemSaleAmt - itm.totalStockReturnAmount + itm.totalStockReceiveAmount - itm.totalIndentingChallanAmt) ;
                //console.log('total balance',result.items[itmIndex].openingAmt);            
              } 


          } //  end of long else ifffffffffffff





        });
          console.log("before")
          console.log(angular.copy(result))
        _.forEach(result.betweenDate,function(itm,i){
            console.log("-----------------------")
          


        if(itm.totalBalanceQty!=undefined){
          //console.log('totalBalanceQty');
          var cDate=new Date(itm.created);
          var dItm = angular.copy(itm);
          var dDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(new Date(dItm.created.setDate(dItm.created.getDate() - 1))))));
            if(!result.dateWiseItems[dDate])
              result.dateWiseItems[dDate] = [];

          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          var dItmIndex = _.findIndex(result.dateWiseItems[dDate],{itemId:itm.itemId});
          if(dItmIndex < 0) {
            //console.log('dItmIndex < 0');
            if(itmIndex < 0){
             //console.log('itmIndex < 0');
                dItm.openingQty=Number(0);
                dItm.openingAmt=Number(0);
                dItm.closingQty=Number(0);
                dItm.closingAmount=Number(0);
            }
            else {
              dItm.openingQty=Number(result.items[itmIndex].closingQty);
              dItm.openingAmt=Number(result.items[itmIndex].closingAmount); 
              dItm.closingQty=Number(result.items[itmIndex].closingQty);
              dItm.closingAmount=Number(result.items[itmIndex].closingAmount); 
            }
            dItm.purchaseQty=Number(0);
            dItm.purchaseAmount=Number(0);
            dItm.consumptionQty=Number( 0);
            dItm.consumptionAmount=Number(0);
            dItm.physicalQty = Number(dItm.totalBalanceQty);
            dItm.physicalAmt = Number(dItm.totalBalanceAmt);
            dItm.actualConsumption = Number(dItm.closingQty - dItm.physicalQty + dItm.consumptionQty + dItm.wastage);
            dItm.actualConsumptionAmt = Number(dItm.closingAmount - dItm.physicalAmt + dItm.consumptionAmount + dItm.wastageAmt);
            //dItm.closingQty+=Number(dItm.openingQty)
            //dItm.closingAmount+=Number(dItm.openingAmt)
            dItm.wastageQty = Number(0);
            //dItm.totalBalanceQty = Number(dItm.totalBalanceQty);
            dItm.wastageAmt = Number(0);
            dItm.varianceQty = Number();
            dItm.varianceInPercent = Number(0);
            dItm.physicalQty = Number(0);
          } else{
            result.dateWiseItems[dDate][dItmIndex].physicalQty=Number(dItm.totalBalanceQty);
            result.dateWiseItems[dDate][dItmIndex].physicalAmt=Number(dItm.totalBalanceAmt);
            // result.dateWiseItems[dDate][dItmIndex].closingAmount=Number(dItm.totalBalanceAmt);
            // result.dateWiseItems[dDate][dItmIndex].closingQty=Number(dItm.totalBalanceQty);
            result.dateWiseItems[dDate][dItmIndex].actualConsumption=Number(result.dateWiseItems[dDate][dItmIndex].closingQty - dItm.totalBalanceQty + result.dateWiseItems[dDate][dItmIndex].consumptionQty);
            result.dateWiseItems[dDate][dItmIndex].actualConsumptionAmt=Number(result.dateWiseItems[dDate][dItmIndex].closingAmount - dItm.totalBalanceAmt + result.dateWiseItems[dDate][dItmIndex].consumptionAmount);
          }


          //console.log("[hy itm",angular.copy(itm))
          //var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          //console.log('itm', itm);
          //var cDate=new Date(itm.created);
          //alert(cDate)
          //cDate.setDate(cDate.getDate()-1);
          //cDate = new Date(Utils.getDateeFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
          //var ed = new Date(endDate);
          //ed = new Date(ed.setDate(ed.getDate()-1));
          //console.log(cDate);
          //console.log(endDate);
          //console.log(ed);
          if(itmIndex<0){
            //itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            //itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            itm.openingQty=Number(0);
            itm.openingAmt=Number(0);
            itm.purchaseQty=Number(0);
            itm.purchaseAmount=Number(0);
            itm.consumptionQty=Number(0);
            itm.consumptionAmount=Number(0);
            itm.closingQty=Number(itm.openingQty);
            itm.closingAmount=Number(itm.openingAmt);
            itm.wastageQty = Number(0);
            //itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = Number(0);
            itm.varianceQty = Number(0);
            itm.varianceInPercent = Number(0);
            itm.physicalQty = Number(0);
            if(endDate.getTime()==cDate.getTime()){
              //console.log('if', itm.itemName, endDate.getTime(), cDate.getTime());
              itm.physicalQty=Number(itm.totalBalanceQty);
              itm.physicalAmt=Number(itm.totalBalanceAmt);
             // itm.closingQty=Number(itm.totalBalanceQty);
            }
            else{
              //console.log('else', itm.itemName, endDate.getTime(), cDate.getTime());
             // itm.closingQty = Number(itm.totalBalanceQty);
              itm.physicalDate = new Date(itm.created);
              //console.log(itm.closingQty);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=Number(0);
              itm.wastageAmount=Number(0);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            if(endDate.getTime()==cDate.getTime()){
              result.items[itmIndex].physicalQty=Number(itm.totalBalanceQty);
              result.items[itmIndex].physicalAmt=Number(itm.totalBalanceAmt);
              result.items[itmIndex].varianceQty=Number(result.items[itmIndex].closingQty - result.items[itmIndex].physicalQty);
              result.items[itmIndex].varianceInPercent=Number(result.items[itmIndex].varianceQty/result.items[itmIndex].closingQty*100);

              //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
            else {
              //console.log(itm.itemName, " else");
              //console.log(result.items[itmIndex].closingQty);
              //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              if(result.items[itmIndex].physicalDate) {
                //console.log(result.items[itmIndex].physicalDate)
                //console.log(cDate)
                if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                  //console.log('if');
                //  result.items[itmIndex].closingQty = Number(itm.totalBalanceQty);
                  //console.log(result.items[itmIndex].closingQty);
                } else {
                  //console.log('else');
                  //result.items[itmIndex].closingQty = Number(itm.totalBalanceQty);
                  result.items[itmIndex].physicalDate = cDate;
                }
              } else {
                //result.items[itmIndex].closingQty = Number(itm.totalBalanceQty);
                result.items[itmIndex].physicalDate = cDate;
              }
              // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
              // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
              // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
          }
        }
        



          if  (itm.totalPurchaseQty != undefined && itm.totalIntermediateQty != undefined && itm.wastage != undefined
              && itm.totalSaleQty!=undefined && itm.totalTransferQty!= undefined && itm.totalTransferQty_toStore!=undefined
              && itm.itemSaleQty!=undefined && itm.totalStockReturnQty!=undefined && itm.totalStockReceiveQty!=undefined
              && itm.totalIndentingChallanQty!=undefined
              
          )
        {
          //console.log(itm.consumptionQty)
          

          var dItm = angular.copy(itm);
          var dDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(dItm.created))));
            if(!result.dateWiseItems[dDate])
              result.dateWiseItems[dDate] = [];

          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          var dItmIndex = _.findIndex(result.dateWiseItems[dDate],{itemId:itm.itemId});
          if(dItmIndex < 0) {
            if(itmIndex < 0){
                dItm.openingQty=Number(0);
                dItm.openingAmt=Number(0);
                dItm.closingQty=Number(0);
                dItm.closingAmount=Number(0);
            }
            else {
              dItm.openingQty=Number(result.items[itmIndex].closingQty);
              dItm.openingAmt=Number(result.items[itmIndex].closingAmount); 
              dItm.closingQty=Number(result.items[itmIndex].closingQty);
              dItm.closingAmount=Number(result.items[itmIndex].closingAmount); 
            }
            dItm.purchaseQty=Number(dItm.totalPurchaseQty + dItm.totalTransferQty_toStore + dItm.totalStockReceiveQty);
            dItm.purchaseAmount=Number(dItm.totalPurchaseAmount + dItm.totalTransferAmount_toStore + dItm.totalStockReceiveAmount);
            dItm.consumptionQty=Number( dItm.itemSaleQty );
            dItm.consumptionAmount=Number(  dItm.itemSaleAmt);
            dItm.actualConsumption = Number(dItm.consumptionQty);
            dItm.actualConsumptionAmt = Number(dItm.consumptionAmount);
            dItm.closingQty+=Number(dItm.totalIntermediateQty - dItm.totalSaleQty + dItm.totalTransferQty_toStore -dItm.totalTransferQty - dItm.itemSaleQty + dItm.totalStockReceiveQty - dItm.totalStockReturnQty + dItm.totalPurchaseQty - dItm.totalIndentingChallanQty)
            dItm.closingAmount+=Number(dItm.totalIntermediateAmt - dItm.totalSaleAmount + dItm.totalTransferAmount_toStore  - dItm.totalTransferAmount - dItm.itemSaleAmt + dItm.totalStockReceiveAmount - dItm.totalStockReturnAmount + dItm.totalPurchaseAmount - dItm.totalIndentingChallanAmt)
            dItm.wastageQty = Number(dItm.wastage);
            dItm.totalBalanceQty = Number(0);
            dItm.wastageAmt = Number(dItm.wastageAmt);
            dItm.varianceQty = Number(0);
            dItm.varianceInPercent = Number(0);
            dItm.physicalQty = Number(0);
            dItm.openingDate=new Date(startDateToShow);
            dItm.closingDate=new Date(endDateToShow);
            result.dateWiseItems[dDate].push(dItm);
            } 
            else {
            result.dateWiseItems[dDate][dItmIndex].purchaseQty=Number(result.dateWiseItems[dDate][dItmIndex].totalPurchaseQty + dItm.totalTransferQty_toStore + dItm.totalStockReceiveQty);
            result.dateWiseItems[dDate][dItmIndex].purchaseAmount=Number(result.dateWiseItems[dDate][dItmIndex].totalPurchaseAmount + dItm.totalTransferAmount_toStore + dItm.totalStockReceiveAmount);

            result.dateWiseItems[dDate][dItmIndex].consumptionQty=Number(result.dateWiseItems[dDate][dItmIndex].consumptionQty + dItm.wastage + dItm.itemSaleQty );
            result.dateWiseItems[dDate][dItmIndex].consumptionAmount=Number(result.dateWiseItems[dDate][dItmIndex].consumptionAmount  + dItm.wastageAmt + dItm.itemSaleAmt );

            result.dateWiseItems[dDate][dItmIndex].actualConsumption=Number(result.dateWiseItems[dDate][dItmIndex].consumptionQty + dItm.itemSaleQty );
            result.dateWiseItems[dDate][dItmIndex].actualConsumptionAmt=Number(result.dateWiseItems[dDate][dItmIndex].consumptionAmount  + dItm.itemSaleAmt );

            result.dateWiseItems[dDate][dItmIndex].closingQty = Number(result.dateWiseItems[dDate][dItmIndex].closingQty +dItm.totalPurchaseQty - dItm.totalIntermediateQty - dItm.wastage - dItm.totalSaleQty + dItm.totalTransferQty_toStore - dItm.totalTransferQty - dItm.itemSaleQty + dItm.totalStockReceiveQty - dItm.totalStockReturnQty - dItm.totalIndentingChallanQty) ;
            result.dateWiseItems[dDate][dItmIndex].closingAmount = Number(result.dateWiseItems[dDate][dItmIndex].closingAmount + dItm.totalPurchaseAmount  - dItm.totalIntermediateQty - dItm.wastageAmt - dItm.totalSaleAmount + dItm.totalTransferAmount_toStore -dItm.totalTransferAmount - dItm.itemSaleAmt + dItm.totalStockReceiveAmount - dItm.totalStockReturnAmount - dItm.totalIndentingChallanAmt );
            }
            if(itmIndex<0){
            itm.openingQty=Number(0);
            itm.openingAmt=Number(0);
            itm.purchaseQty=Number(itm.totalPurchaseQty + itm.totalTransferQty_toStore + itm.totalStockReceiveQty);
            itm.purchaseAmount=Number(itm.totalPurchaseAmount + itm.totalTransferAmount_toStore + itm.totalStockReceiveAmount);
            itm.consumptionQty=Number( itm.itemSaleQty);
            itm.consumptionAmount=Number(  itm.itemSaleAmt);
            itm.closingQty=Number(itm.openingQty - itm.totalIntermediateQty - itm.totalSaleQty + itm.totalTransferQty_toStore -itm.totalTransferQty - itm.itemSaleQty + itm.totalStockReceiveQty - itm.totalStockReturnQty + itm.totalPurchaseQty - itm.totalIndentingChallanQty)
            itm.closingAmount=Number(itm.openingAmt - itm.totalIntermediateAmt - itm.totalSaleAmount + itm.totalTransferAmount_toStore  - itm.totalTransferAmount - itm.itemSaleAmot + itm.totalStockReceiveAmount - itm.totalStockReturnAmount + itm.totalPurchaseAmount - itm.totalIndentingChallanAmt)
            itm.wastageQty = Number(itm.wastage);
            itm.totalBalanceQty = Number(0);
            itm.wastageAmt = Number(itm.wastageAmt);
            itm.varianceQty = Number(0);
            itm.varianceInPercent = Number(0);
            itm.physicalQty = Number(0);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
              
          }
          else
          {
            // console.log("index found")
            //    console.log("consumptionQty")
            // console.log(result.items[itmIndex].consumptionAmount )
            //   console.log("total transger")
            // console.log(itm.totalTransferAmount)
            //   console.log("wayage")
            // console.log( itm.wastageAmt)
            //   console.log("inter amt")
            // console.log(itm.totalIntermediateAmt)
            //   console.log(" total sale")
            // console.log(itm.totalSaleAmount)
            //   console.log(" item sale")
            // console.log(itm.itemSaleAmt)

            // console.log(itm.totalStockReturnAmount)
            //             console.log("wastage")
            
            // console.log( angular.copy(itm.wastage) )
            // console.log("closing")
            // console.log( angular.copy(itm.closingQty) )
            // console.log("Purchasee")
            // console.log( angular.copy(itm.purchaseQty) )
            // console.log("itemsale")
            // console.log(itm.itemSaleQty)
            // console.log("total ds;r")
            // console.log(itm.totalSaleQty)
            result.items[itmIndex].purchaseQty=Number(result.items[itmIndex].purchaseQty + itm.totalPurchaseQty + itm.totalTransferQty_toStore + itm.totalStockReceiveQty);
            result.items[itmIndex].purchaseAmount=Number(result.items[itmIndex].purchaseAmount + itm.totalPurchaseAmount + itm.totalTransferAmount_toStore + itm.totalStockReceiveAmount);

            result.items[itmIndex].consumptionQty=Number(result.items[itmIndex].consumptionQty + itm.wastage + itm.itemSaleQty );
            result.items[itmIndex].consumptionAmount=Number(result.items[itmIndex].consumptionAmount  + itm.wastageAmt + itm.itemSaleAmt );

            result.items[itmIndex].closingQty = Number(result.items[itmIndex].closingQty +itm.totalPurchaseQty - itm.totalIntermediateQty - itm.wastage - itm.totalSaleQty + itm.totalTransferQty_toStore - itm.totalTransferQty - itm.itemSaleQty + itm.totalStockReceiveQty - itm.totalStockReturnQty - itm.totalIndentingChallanQty);
            result.items[itmIndex].closingAmount = Number(result.items[itmIndex].closingAmount + itm.totalPurchaseAmount  - itm.totalIntermediateQty - itm.wastageAmt - itm.totalSaleAmount + itm.totalTransferAmount_toStore -itm.totalTransferAmount - itm.itemSaleAmt + itm.totalStockReceiveAmount - itm.totalStockReturnAmount - itm.totalIndentingChallanAmt);

          }
        }

        
       // console.log(JSON.stringify(angular.copy(result) ) )




          // if(itm.totalIndentingChallanQty!=undefined){
          //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          //   if(itmIndex<0){
          //     if(itm.openingQty==undefined){
          //       itm.openingQty=parseFloat(0).toFixed(3);
          //       itm.closingQty=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
          //       itm.openingAmt=parseFloat(0).toFixed(3);
          //       itm.closingAmount=parseFloat(-itm.totalIndentingChallanAmt).toFixed(3);
          //       itm.wastageQty = parseFloat(0).toFixed(3);
          //       itm.totalBalanceQty = parseFloat(0).toFixed(3);
          //       itm.wastageAmt = parseFloat(0).toFixed(2);
          //       itm.varianceQty = parseFloat(0).toFixed(2);
          //       itm.varianceInPercent = parseFloat(0).toFixed(2);
          //       itm.physicalQty = parseFloat(0).toFixed(3);
          //       itm.openingDate=new Date(startDateToShow);
          //       itm.closingDate=new Date(endDateToShow);
          //     }
          //     if(itm.purchaseQty==undefined){
          //       itm.purchaseQty=parseFloat(0).toFixed(3);
          //       itm.purchaseAmount=parseFloat(0).toFixed(3);
          //     }
          //     if(itm.wastageQty == undefined) {
          //       itm.wastageQty=parseFloat(0).toFixed(3);
          //       itm.wastageAmount=parseFloat(0).toFixed(3);
          //     }
          //     if(itm.varianceQty == undefined) {
          //       itm.varianceQty=parseFloat(0).toFixed(3);
          //     }
          //     itm.consumptionQty=parseFloat(itm.totalIndentingChallanQty).toFixed(3);
          //     itm.consumptionAmount=parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
          //     result.items.push(itm);
          //   }
          //   else
          //   {
          //     result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
          //     result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

          //     result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
          //     result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
          //   }
          // }

        });
       return result;
    },
    formatDataForReport_DateWiseViaPhysical: function () {
          var startDate = this._fromDate;
          var endDate = this._toDate;
          var endDateToShow = new Date(endDate.setDate(endDate.getDate() - 1));
          var result = this._data;
          result.items = [];
          _.forEach(result.beforeDate, function (itm, i) {
            if (itm.totalOpeningQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalOpeningQty).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalOpeningAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(3);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalOpeningAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalOpeningAmt)).toFixed(2);
              }
            }
            else if (itm.totalBalanceQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = -1 * parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.consumptionAmount = parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.created = itm.actualDate;
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(itm.totalBalanceAmt).toFixed(2);
              }
            }
            else if (itm.totalPurchaseQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalPurchaseAmount).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].getVarianceDateWiseReportpeningAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
              }
            }
            /*else if (itm.totalIntermediateQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.totalIntermediateAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalIntermediateAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalIntermediateAmt)).toFixed(2);
              }
            }*/
            /*else if (itm.totalSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.totalSaleAmount).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
            }*/
            /*else if (itm.totalWastageQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.totalWastageAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalWastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalWastageAmt)).toFixed(2);
              }
            }*/
            /*else if (itm.totalTransferQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.totalTransferAmount).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalTransferAmount)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }*/
            else if (itm.totalTransferQty_toStore != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
              }
            }
            /*else if (itm.itemSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.itemSaleAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.UnitName = itm.baseUnit;
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.itemSaleAmt)).toFixed(2);
              }
            }*/
            /*else if (itm.totalStockReturnQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
                itm.openingAmt = parseFloat(-itm.totalStockReturnAmount).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalStockReturnAmount)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }
            }*/
            else if (itm.totalStockReceiveQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
            }
          });
          _.forEach(result.betweenDate, function (itm, i) {
            if (itm.totalPurchaseQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalPurchaseAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.purchaseAmount = parseFloat(itm.totalPurchaseAmount).toFixed(2);
              result.items.push(itm);
            }
            else if (itm.totalBalanceQty != undefined) {
              //console.log('item in physical :-  ',itm);
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(0).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDate);
                //itm.created = itm.actualDate;
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = -1 * parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              /*else
              {
               itm.consumptionQty = -1 * parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }*/
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
              }
              itm.varianceQty = parseFloat(0).toFixed(3);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.physicalAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
              result.items.push(itm);
            }
            /*else if (itm.totalIntermediateQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalIntermediateAmt).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalIntermediateQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalIntermediateAmt).toFixed(2);
              result.items.push(itm);
            }*/
            /*else if (itm.totalSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalSaleAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalSaleQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalSaleAmount).toFixed(2);
              result.items.push(itm);
            }*/
            /*else if (itm.totalWastageQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalWastageAmt).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              itm.wastageQty = parseFloat(itm.totalWastageQty).toFixed(3);
              itm.wastageAmt = parseFloat(itm.totalWastageAmt).toFixed(2);
              result.items.push(itm);
            }*/
            else if (itm.totalTransferQty_toStore != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.purchaseAmount = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              result.items.push(itm);
            }
            /*else if (itm.totalTransferQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalTransferAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalTransferQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalTransferAmount).toFixed(2);
              result.items.push(itm);
            }*/
            /*else if (itm.itemSaleQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.itemSaleAmt).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.itemSaleQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.itemSaleAmt).toFixed(2);
              itm.UnitName = itm.baseUnit;
              result.items.push(itm);
            }*/
            else if (itm.totalStockReceiveQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalStockReceiveQty).toFixed(3);
              itm.purchaseAmount = parseFloat(itm.totalStockReceiveAmount).toFixed(2);
              result.items.push(itm);
            }
            /*else if (itm.totalStockReturnQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalStockReturnQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalStockReturnAmount).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalStockReturnQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalStockReturnAmount).toFixed(2);
              result.items.push(itm);
            }*/
          });
          return result;
     },
    formatDataForReportAggregated: function () {
          console.log(angular.copy(this._data));
          var startDate = this._fromDate;
          var endDate = this._toDate;
          var result = this._data;
          var deployment = this.deployment;
          result.items = [];
          var endDateToShow = new Date(endDate);
          var startDateToShow = new Date(startDate);
          endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
          _.forEach(result.beforeDate,function(itm,i){
            if(itm.totalOpeningQty!=undefined){
              var itmIndex = _.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex < 0){
                if(startDate.getTime() < new Date(itm.created).getTime())
                  startDateToShow = new Date(itm.created);
                itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                //console.log('total balance', itm.totalBalanceQty);
                result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
              }
            }

            else if(itm.purchase != undefined && itm.consumption != undefined && itm.wastage != undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.purchase - itm.consumption - itm.wastage).toFixed(3);
                itm.openingAmt=parseFloat(itm.purchaseAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                //console.log('total balance', itm.totalBalanceQty);
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.purchase - itm.consumption - itm.wastage)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.purchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.purchase - itm.consumption - itm.wastage)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.purchaseAmount)).toFixed(2);
              }
            }
          });
          _.forEach(result.betweenDate,function(itm,i){
            if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              //console.log('itm', itm);
              var cDate=new Date(itm.created);
              //cDate.setDate(cDate.getDate()-1);
              //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
              //var ed = new Date(endDate);
              //ed = new Date(ed.setDate(ed.getDate()-1));
              //console.log(cDate);
              //console.log(endDate);
              //console.log(ed);
              if(itmIndex<0){
                //itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                //itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                //itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.varianceQty = parseFloat(0).toFixed(2);
                itm.varianceInPercent = parseFloat(0).toFixed(2);
                itm.physicalQty = parseFloat(0).toFixed(3);
                if(endDate.getTime()==cDate.getTime()){
                  itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                  itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                }
                else{
                  itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  itm.physicalDate = new Date(itm.created);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(0).toFixed(3);
                  itm.wastageAmount=parseFloat(0).toFixed(3);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                itm.openingDate=new Date(startDateToShow);
                itm.closingDate=new Date(endDateToShow);
                result.items.push(itm);
              }
              else
              {
                if(endDate.getTime()==cDate.getTime()){
                  result.items[itmIndex].physicalQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  result.items[itmIndex].physicalAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                  result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
                  result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);

                  //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                }
                else {
                  //console.log(itm.itemName, " else");
                  //console.log(result.items[itmIndex].closingQty);
                  //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  if(result.items[itmIndex].physicalDate) {
                    console.log(result.items[itmIndex].physicalDate)
                    console.log(cDate)
                    if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                      console.log('if');
                      result.items[itmIndex].closingQty = Utils.roundNumber(parseFloat(result.items[itmIndex].closingQty), 3) + Utils.roundNumber(parseFloat(itm.totalBalanceQty), 3);
                      console.log(result.items[itmIndex].closingQty);
                    } else {
                      console.log('else');
                      result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                      result.items[itmIndex].physicalDate = cDate;
                    }
                  } else {
                    result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                    result.items[itmIndex].physicalDate = cDate;
                  }
                  // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
                  // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
                  // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                }
              }
            }
            else if(itm.purchase != undefined && itm.consumption != undefined && itm.wastage != undefined) {
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itm.itemName == "Biriyani") {
                console.log(itmIndex, itm)
              }
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.purchase - itm.consumption - itm.wastage).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.purchase).toFixed(3);
                  itm.totalBalanceQty = parseFloat(0).toFixed(3);
                  itm.varianceQty = parseFloat(0).toFixed(2);
                  itm.varianceInPercent = parseFloat(0).toFixed(2);
                  itm.physicalQty = parseFloat(0).toFixed(3);
                  itm.purchaseQty = parseFloat(itm.purchase).toFixed(3);
                  itm.wastageQty=parseFloat(itm.wastage).toFixed(3);
                  itm.wastageAmount=parseFloat(itm.wastageAmt).toFixed(3);
                  itm.consumptionQty=parseFloat(itm.consumption).toFixed(3);
                  itm.consumptionAmount=parseFloat(itm.consumptionAmount).toFixed(2);
                  itm.purchaseAmount=parseFloat(itm.purchaseAmount).toFixed(3);
                  itm.openingDate=new Date(startDateToShow);
                  itm.closingDate=new Date(endDateToShow);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(itm.purchase).toFixed(3);
                  itm.purchaseAmount=parseFloat(itm.purchaseAmount).toFixed(3);
                }
                if(itm.wastageQty == undefined) {
                  itm.wastageQty=parseFloat(itm.wastage).toFixed(3);
                  itm.wastageAmount=parseFloat(itm.wastageAmt).toFixed(3);
                }
                if(itm.consumptionQty == undefined) {
                  itm.consumptionQty=parseFloat(itm.consumption).toFixed(3);
                  itm.consumptionAmount=parseFloat(itm.consumptionAmount).toFixed(2);
                }
                if(itm.varianceQty == undefined) {
                  itm.varianceQty=parseFloat(0).toFixed(3);
                }
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.purchase)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.purchaseAmount)).toFixed(2);

                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.consumption)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.consumptionAmount)).toFixed(2);

                result.items[itmIndex].wastageQty=parseFloat(parseFloat(result.items[itmIndex].wastageQty)+parseFloat(itm.wastage)).toFixed(3);
                result.items[itmIndex].wastageAmt=parseFloat(parseFloat(result.items[itmIndex].wastageAmt)+parseFloat(itm.wastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.purchase - itm.consumption - itm.wastage)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt)).toFixed(2);
              }
            }
          });
          console.log(angular.copy(result.items));
          return result;
     },
    formatDataForReport_DateWiseAggregated: function () {
          var startDate = this._fromDate;
          var endDate = this._toDate;
          var endDateToShow = new Date(endDate.setDate(endDate.getDate() - 1));
          var result = this._data;
          result.items = [];
          _.forEach(result.beforeDate, function (itm, i) {
            if(itm.itemName =='Hot & Sour Chicken Soup(bk)')
            {
              console.log('Hot & Sour Chicken Soup(bk)',itm);
            }
            if (itm.totalOpeningQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalOpeningQty).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalOpeningAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(3);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalOpeningAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalOpeningAmt)).toFixed(2);
              }
            }
            else if (itm.totalBalanceQty != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                itm.created = itm.actualDate;
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].openingAmt = parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(itm.totalBalanceAmt).toFixed(2);
              }
            }
            else if(itm.purchase != undefined && itm.consumption != undefined && itm.wastage != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itmIndex < 0) {
                itm.openingQty = parseFloat(itm.purchase - itm.consumption - itm.wastage).toFixed(3);
                itm.openingAmt = parseFloat(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt).toFixed(2);
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(2);
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(2);
                itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
                itm.wastageQty = parseFloat(0).toFixed(3);
                itm.wastageAmt = parseFloat(0).toFixed(2);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.varianceInPercent = "NA";
                result.items.push(itm);
              }
              else {
                result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.purchase - itm.consumption - itm.wastage)).toFixed(3);
                result.items[itmIndex].getVarianceDateWiseReportpeningAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.purchase - itm.consumption - itm.wastage)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt)).toFixed(2);
              }
            }
          });
          _.forEach(result.betweenDate, function (itm, i) {

            if(itm.itemName =='Hot & Sour Chicken Soup(bk)')
            {
              console.log('Hot & Sour Chicken Soup(bk)',itm);
            }
            if (itm.totalBalanceQty != undefined) {
              //console.log('item in physical :-  ',itm);
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(0).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(0).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDate);
                //itm.created = itm.actualDate;
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              if (itm.wastageQty == undefined) {
                itm.wastageQty = parseFloat(0).toFixed(3);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
              }
              itm.varianceQty = parseFloat(0).toFixed(3);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.physicalAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
              result.items.push(itm);
            }
            else if(itm.purchase != undefined && itm.consumption != undefined && itm.wastage != undefined) {
              var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.purchase - itm.consumption - itm.wastage).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt).toFixed(3);
                itm.totalBalanceQty = parseFloat(0).toFixed(3);
                itm.physicalQty = "NA";
                itm.varianceQty = "NA";
                itm.consumptionQty = parseFloat(itm.consumption).toFixed(3);
                itm.consumptionAmount = parseFloat(itm.consumptionAmount).toFixed(2);
                itm.purchaseQty = parseFloat(itm.purchase).toFixed(3);
                itm.purchaseAmount = parseFloat(itm.purchaseAmount).toFixed(2);
                itm.wastageQty = parseFloat(itm.wastage).toFixed(3);
                itm.wastageAmt = parseFloat(itm.wastageAmt).toFixed(2);
                itm.varianceInPercent = "NA";
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
                result.items.push(itm);
              } else {
                result.items[itmIndex].purchaseQty = parseFloat(parseFloat(result.items[itmIndex].purchaseQty) + parseFloat(itm.purchase)).toFixed(3);
                result.items[itmIndex].purchaseAmount = parseFloat(parseFloat(result.items[itmIndex].purchaseAmount) + parseFloat(itm.purchaseAmount)).toFixed(2);

                result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.consumption)).toFixed(3);
                result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.consumptionAmount)).toFixed(2);

                result.items[itmIndex].wastageQty = parseFloat(parseFloat(result.items[itmIndex].wastageQty) + parseFloat(itm.wastage)).toFixed(3);
                result.items[itmIndex].wastageAmt = parseFloat(parseFloat(result.items[itmIndex].wastageAmt) + parseFloat(itm.wastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.purchase - itm.consumption - itm.wastage)).toFixed(3);
                result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt)).toFixed(2);
              }
            }
          });
          return result;
     },
    generateDateWiseVarianceReport: function (items, days) {
          var self = this;
         // console.log('---------------------------ITEMS----------------------------------');
          //console.log(angular.copy(items));
          //console.log('---------------------------DAYS----------------------------------');
          //console.log(angular.copy(days));
          items.sort(function (a, b) {
            if((new Date(b.created).getTime() - new Date(a.created).getTime()) != 0)
              return new Date(b.created) - new Date(a.created);
            else return a.num - b.num;
          });

         // console.log('items resorted', angular.copy(items));
          _.forEach(days, function (d, i) {
            //d.actualCost=0.0,d.idealCost-0.0
            var lastPriceIndex1=_.findIndex(self._lastPrice,{ itemId:d.itemId } );
            d.items = [];
            var pdate = new Date(d.date);
            pdate.setDate(pdate.getDate() - 1);
            //console.log('pDate :-  ', pdate);
            d.items = self.getPreviousDate_Data(days, pdate);
            //console.log('---------------------------D.ITEMS----------------------------------');
            //console.log(angular.copy(d.items));
            var resetDate = new Date(d.date);
            //console.log('resetDate :-   ',resetDate);
            resetDate = new Date(resetDate.setDate(resetDate.getDate() + 1));
            var ind = -1;
            _.forEachRight(items, function (itm, ii) {
              var lastPriceIndex2=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
              var cDate = new Date(itm.created);
              if(_.has(itm, 'totalBalanceQty')){
                var cDate = new Date(itm.created);
              }
              //console.log('cDate :-   ',cDate);
              //console.log(itm);
              //cDate.setHours(0,0,0,0);
              //var resetDate = new Date(d.date.setDate(d.date.getDate() + 1));
              //console.log('resetDate :-   ',resetDate);
              //console.log('d.date :-   ',d.date);
              //console.log(cDate);
              if (new Date(d.date) > new Date(cDate)) {
                //console.log('if');
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                   // console.log("itmmmmmmmmmmmmmmmmm",itm);
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                  }
                  else {
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                    itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                    itm.closingAmount = parseFloat( parseFloat(itm.closingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)).toFixed(3)


                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
              }
              else if (new Date(d.date) <= new Date(cDate) && new Date(cDate) <= resetDate) {
                //console.log('else');
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                  }
                  else {
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                    //console.log(d.date, itm.openingQty);
                    itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                    itm.closingAmount = parseFloat( parseFloat(itm.closingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)).toFixed(3)

                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
                else {
                  
                  d.items[index].openingQty = parseFloat(parseFloat(d.items[index].openingQty) + parseFloat(itm.openingQty)).toFixed(3);
                  d.items[index].openingAmt = parseFloat( parseFloat( parseFloat(d.items[index].openingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage) ) + parseFloat( parseFloat(itm.openingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  d.items[index].purchaseQty = parseFloat( parseFloat(d.items[index].purchaseQty) + parseFloat(itm.purchaseQty)).toFixed(3);
                  d.items[index].purchaseAmount = parseFloat(  parseFloat(d.items[index].purchaseAmount )   + parseFloat(itm.purchaseAmount) ).toFixed(3);
                  d.items[index].consumptionQty = parseFloat( parseFloat(d.items[index].consumptionQty) + parseFloat(itm.consumptionQty)).toFixed(3);
                  d.items[index].consumptionAmount = parseFloat( parseFloat(d.items[index].consumptionAmount) + parseFloat(itm.consumptionAmount)).toFixed(3);
                  d.items[index].wastageQty = parseFloat( parseFloat(d.items[index].wastageQty) + parseFloat(itm.wastageQty)).toFixed(3);
                  d.items[index].wastageAmt = parseFloat( parseFloat(  parseFloat(d.items[index].wastageQty ) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ) + parseFloat( parseFloat(itm.wastageQty ) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  d.items[index].closingQty = parseFloat(parseFloat(d.items[index].closingQty) + parseFloat(itm.closingQty)).toFixed(3);
                  d.items[index].closingAmount = parseFloat( parseFloat( parseFloat(d.items[index].closingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  ) + parseFloat( parseFloat(itm.closingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  //d.idealCost=parseFloat( parseFloat(d.items[index].consumptionAmount) + parseFloat(d.items[index].wastageAmt) ).toFixed(3);
                 // console.log("ideal",d.idealCost);
                  if (itm.physicalQty != "NA") {
                    if(itm.itemName == "Garlic Chicken(bk)"){
                      //console.log(itm.itemName, itm.physicalQty);
                    }
                    if (d.items[index].physicalQty != "NA") {
                      //console.log('Hello --------------------------------------------')
                      //console.log(itm);
                      d.items[index].physicalQty = parseFloat(itm.physicalQty).toFixed(3);
                    }
                    else {
                      //console.log('Hello 2--------------------------------------------')

                      //console.log(itm, d.date);
                      d.items[index].physicalQty = parseFloat(parseFloat(itm.physicalQty)).toFixed(3);
                    }
                    //d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //if(d.items[index].physicalQty!="NA"){
                    //  var dNO=parseFloat(i+1);
                    //
                    //  //d.items[index].openingQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].closingQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(d.items[index].purchaseQty)-parseFloat(d.items[index].wastageQty)-parseFloat(d.items[index].consumptionQty)).toFixed(3);
                    //  d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].varianceQty=parseFloat(parseFloat(d.items[index].closingQty)-parseFloat(d.items[index].physicalQty)).toFixed(3);
                    //  d.items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(d.items[index].varianceQty)/parseFloat(d.items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //if(dNO>=0){
                    //  days[dNO].items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  days[dNO].items[index].varianceQty=parseFloat(parseFloat(days[dNO].items[index].closingQty)-parseFloat(days[dNO].items[index].physicalQty)).toFixed(3);
                    //  days[dNO].items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(days[dNO].items[index].varianceQty)/parseFloat(days[dNO].items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //}
                    //d.items[index].physicalQty="NA";
                  }
                  items.splice(ii, 1);
                }
              }
            })
           
            //console.log("-----------------------");
            //console.log(angular.copy(d.items) );
            //var count =0;
            _.forEach(d.items, function (itm){
              //console.log("food item",itm);
             // console.log("-----------------------");
              //d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
              var lastPriceIndex3=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
              // if(itm.wastageAmt==undefined)
              // {
              //   itm.wastageAmt=0
              // }
              // if(itm.consumptionAmount==undefined)
              // {
              //   itm.consumptionAmount=0
              // }
            
              
               
              if(itm.physicalQty != "NA"){

                //console.log("itm phy",itm);
                itm.physicalAmt=parseFloat( parseFloat(itm.physicalQty)  * parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  ) ).toFixed(3)
                itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
                itm.closingAmount=parseFloat(parseFloat(itm.closingQty)*parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) ) ).toFixed(3)
               // if(itm.consumptionQty>0 || itm.wastageQty>0)
                  //d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                
                //if(itm.physicalQty === "NA")
                //  itm.varianceAmt=parseFloat().toFixed(3)
                //else
                //console.log("var amt");
                //console.log(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt));
                if(itm.physicalQty>0)
                {
                   if(itm.varianceQty!="NA" || itm.varianceQty>0)
                  {
                   // d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                    //d.actualCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt) ).toFixed(3);
                    itm.varianceAmt=parseFloat(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt)).toFixed(3);    
                   // d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
                    //console.log(d.idealCost,"ideal phy");
                  }
                }
                //   if(itm.physicalQty<=0 || itm.physicalQty==="NA")
                // {
                //   console.log("inside else");
                //   d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                //   d.actualCost=parseFloat( d.idealCost ).toFixed(3);
                // }
                // else
                // {
                //    d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                // }
               
               // itm.varianceAmt=parseFloat(parseFloat( parseFloat(itm.closingQty)  * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  )-parseFloat( parseFloat(itm.physicalQty) * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) )).toFixed(3);
                itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
              }
              // else
              // {
              //   count++;
              //   console.log('count',count);
              //   console.log("else not physical",angular.copy(itm));
              //   //if(itm.physicalQty=0)
              //   d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt) ).toFixed(3);
              // }
              // else
              // {
              //       d.actualCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt) ).toFixed(3);
        
              //       d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
              //       console.log(d.idealCost,"ideal phy");
              // }
              
            //console.log(d.idealCost, "cost")
            });
          });
          return days;
     },
    generateDateWiseFoodCostReport: function (items, days) {
          var self = this;
          console.log('---------------------------ITEMS----------------------------------');
          console.log(angular.copy(items));
          console.log('---------------------------DAYS----------------------------------');
          console.log(angular.copy(days));
          items.sort(function (a, b) {
            if((new Date(b.created).getTime() - new Date(a.created).getTime()) != 0)
              return new Date(b.created) - new Date(a.created);
            else return a.num - b.num;
          });

         // console.log('items resorted', angular.copy(items));
          _.forEach(days, function (d, i) {
           // d.actualCost=0.0,d.idealCost-0.0
            var lastPriceIndex1=_.findIndex(self._lastPrice,{ itemId:d.itemId } );
            d.items = [];
            var pdate = new Date(d.date);
            pdate.setDate(pdate.getDate() - 1);
            //console.log('pDate :-  ', pdate);
            d.items = self.getPreviousDate_Data(days, pdate);
            //console.log('---------------------------D.ITEMS----------------------------------');
            //console.log(angular.copy(d.items));
            var resetDate = new Date(d.date);
            //console.log('resetDate :-   ',resetDate);
            resetDate = new Date(resetDate.setDate(resetDate.getDate() + 1));
            var ind = -1;
            _.forEachRight(items, function (itm, ii) {

              var lastPriceIndex2=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
              var cDate = new Date(itm.created);
              if(_.has(itm, 'totalBalanceQty')){
                var cDate = new Date(itm.created);
              }
              //console.log('cDate :-   ',cDate);
              //console.log(itm);
              //cDate.setHours(0,0,0,0);
              //var resetDate = new Date(d.date.setDate(d.date.getDate() + 1));
              //console.log('resetDate :-   ',resetDate);
              //console.log('d.date :-   ',d.date);
              //console.log(cDate);
              if (new Date(d.date) > new Date(cDate)) {
                //console.log('if');
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                   // console.log("itmmmmmmmmmmmmmmmmm",itm);
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                  }
                  else {
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                    itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                    itm.closingAmount = parseFloat( parseFloat(itm.closingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)).toFixed(3)


                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
              }
              else if (new Date(d.date) <= new Date(cDate) && new Date(cDate) <= resetDate) {
                //console.log('else');
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                  }
                  else {
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                    //console.log(d.date, itm.openingQty);
                    itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                    itm.closingAmount = parseFloat( parseFloat(itm.closingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)).toFixed(3)

                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
                else {
                  //console.log('babababbababababababbababababbabababababbaab');
                  d.items[index].openingQty = parseFloat(parseFloat(d.items[index].openingQty) + parseFloat(itm.openingQty)).toFixed(3);
                  d.items[index].openingAmt = parseFloat( parseFloat( parseFloat(d.items[index].openingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage) ) + parseFloat( parseFloat(itm.openingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  d.items[index].purchaseQty = parseFloat( parseFloat(d.items[index].purchaseQty) + parseFloat(itm.purchaseQty)).toFixed(3);
                  d.items[index].purchaseAmount = parseFloat(  parseFloat(d.items[index].purchaseAmount )   + parseFloat(itm.purchaseAmount) ).toFixed(3);
                  d.items[index].consumptionQty = parseFloat( parseFloat(d.items[index].consumptionQty) + parseFloat(itm.consumptionQty)).toFixed(3);
                  d.items[index].consumptionAmount = parseFloat( parseFloat(d.items[index].consumptionAmount) + parseFloat(itm.consumptionAmount)).toFixed(3);
                  d.items[index].wastageQty = parseFloat( parseFloat(d.items[index].wastageQty) + parseFloat(itm.wastageQty)).toFixed(3);
                  d.items[index].wastageAmt = parseFloat( parseFloat(  parseFloat(d.items[index].wastageQty ) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ) + parseFloat( parseFloat(itm.wastageQty ) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  d.items[index].closingQty = parseFloat(parseFloat(d.items[index].closingQty) + parseFloat(itm.closingQty)).toFixed(3);
                  d.items[index].closingAmount = parseFloat( parseFloat( parseFloat(d.items[index].closingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  ) + parseFloat( parseFloat(itm.closingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  //d.idealCost=parseFloat( parseFloat(d.items[index].consumptionAmount) + parseFloat(d.items[index].wastageAmt) ).toFixed(3);
                 // console.log("ideal",d.idealCost);
                  if (itm.physicalQty != "NA") {
                    if(itm.itemName == "Garlic Chicken(bk)"){
                      //console.log(itm.itemName, itm.physicalQty);
                    }
                    if (d.items[index].physicalQty != "NA") {
                      //console.log('Hello --------------------------------------------')
                      //console.log(itm);
                      d.items[index].physicalQty = parseFloat(itm.physicalQty).toFixed(3);
                    }
                    else {
                      //console.log('Hello 2--------------------------------------------')

                      //console.log(itm, d.date);
                      d.items[index].physicalQty = parseFloat(parseFloat(itm.physicalQty)).toFixed(3);
                    }
                    //d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //if(d.items[index].physicalQty!="NA"){
                    //  var dNO=parseFloat(i+1);
                    //
                    //  //d.items[index].openingQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].closingQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(d.items[index].purchaseQty)-parseFloat(d.items[index].wastageQty)-parseFloat(d.items[index].consumptionQty)).toFixed(3);
                    //  d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].varianceQty=parseFloat(parseFloat(d.items[index].closingQty)-parseFloat(d.items[index].physicalQty)).toFixed(3);
                    //  d.items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(d.items[index].varianceQty)/parseFloat(d.items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //if(dNO>=0){
                    //  days[dNO].items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  days[dNO].items[index].varianceQty=parseFloat(parseFloat(days[dNO].items[index].closingQty)-parseFloat(days[dNO].items[index].physicalQty)).toFixed(3);
                    //  days[dNO].items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(days[dNO].items[index].varianceQty)/parseFloat(days[dNO].items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //}
                    //d.items[index].physicalQty="NA";
                  }
                  items.splice(ii, 1);
                }
              }
            })
           
            console.log("-----------------------");
            console.log(angular.copy(d.items) );
            _.forEach(d.items, function (itm){
               
              var lastPriceIndex3=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
              // if(itm.wastageAmt==undefined)
              // {
              //   itm.wastageAmt=0
              // }
              // if(itm.consumptionAmount==undefined)
              // {
              //   itm.consumptionAmount=0
              // }
                
              
               
              if(itm.physicalQty != "NA"){
                //console.log("itm phy",itm);
                itm.physicalAmt=parseFloat( parseFloat(itm.physicalQty)  * parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  ) ).toFixed(3)
                itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
                itm.closingAmount=parseFloat(parseFloat(itm.closingQty)*parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) ) ).toFixed(3)
               // if(itm.consumptionQty>0 || itm.wastageQty>0)
                  //d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                
                //if(itm.physicalQty === "NA")
                //  itm.varianceAmt=parseFloat().toFixed(3)
                //else
                //console.log("var amt");
                //console.log(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt));
                if(itm.physicalQty>0)
                {
                   if(itm.varianceQty!="NA" || itm.varianceQty>0)
                  {
                   // d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                   // d.actualCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt) ).toFixed(3);
                    itm.varianceAmt=parseFloat(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt)).toFixed(3);    
                   // d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
                   
                  }
                }
                //   if(itm.physicalQty<=0 || itm.physicalQty==="NA")
                // {
                //   console.log("inside else");
                //   d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                //   d.actualCost=parseFloat( d.idealCost ).toFixed(3);
                // }
                // else
                // {
                //    d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                // }
               
               // itm.varianceAmt=parseFloat(parseFloat( parseFloat(itm.closingQty)  * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  )-parseFloat( parseFloat(itm.physicalQty) * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) )).toFixed(3);
                itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
              }
              
            
            });
          });
          return days;
     },
         generateDateWiseFoodCostReportFoodCost: function (items, days) {
          var self = this;
          console.log('---------------------------ITEMS----------------------------------');
          console.log(angular.copy(items));
          console.log('---------------------------DAYS----------------------------------');
          console.log(angular.copy(days));
          items.sort(function (a, b) {
            if((new Date(b.created).getTime() - new Date(a.created).getTime()) != 0)
              return new Date(b.created) - new Date(a.created);
            else return a.num - b.num;
          });

         // console.log('items resorted', angular.copy(items));
          _.forEach(days, function (d, i) {
           // d.actualCost=0.0,d.idealCost-0.0
            var lastPriceIndex1=_.findIndex(self._lastPrice,{ itemId:d.itemId } );
            d.items = [];
            var pdate = new Date(d.date);
            pdate.setDate(pdate.getDate() - 1);
            //console.log('pDate :-  ', pdate);
            d.items = self.getPreviousDate_Data(days, pdate);
            //console.log('---------------------------D.ITEMS----------------------------------');
            //console.log(angular.copy(d.items));
            var resetDate = new Date(d.date);
            //console.log('resetDate :-   ',resetDate);
            resetDate = new Date(resetDate.setDate(resetDate.getDate() + 1));
            var ind = -1;
            _.forEachRight(items, function (itm, ii) {

              var lastPriceIndex2=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
              var cDate = new Date(itm.created);
              if(_.has(itm, 'totalBalanceQty')){
                var cDate = new Date(itm.created);
              }
              //console.log('cDate :-   ',cDate);
              //console.log(itm);
              //cDate.setHours(0,0,0,0);
              //var resetDate = new Date(d.date.setDate(d.date.getDate() + 1));
              //console.log('resetDate :-   ',resetDate);
              //console.log('d.date :-   ',d.date);
              //console.log(cDate);
              if (new Date(d.date) > new Date(cDate)) {
                //console.log('if');
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                   // console.log("itmmmmmmmmmmmmmmmmm",itm);
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                    // itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                  }
                  else {
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    // itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                    itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                    // itm.closingAmount = parseFloat( parseFloat(itm.closingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)).toFixed(3)


                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
              }
              else if (new Date(d.date) <= new Date(cDate) && new Date(cDate) <= resetDate) {
                //console.log('else');
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                    // itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                  }
                  else {
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    // itm.openingAmt = parseFloat( parseFloat(itm.openingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ).toFixed(3);
                    //console.log(d.date, itm.openingQty);
                    itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                    // itm.closingAmount = parseFloat( parseFloat(itm.closingQty) *  parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)).toFixed(3)

                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
                else {
                  //console.log('babababbababababababbababababbabababababbaab');
                  d.items[index].openingQty = parseFloat(parseFloat(d.items[index].openingQty) + parseFloat(itm.openingQty)).toFixed(3);
                  // d.items[index].openingAmt = parseFloat( parseFloat( parseFloat(d.items[index].openingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage) ) + parseFloat( parseFloat(itm.openingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  d.items[index].purchaseQty = parseFloat( parseFloat(d.items[index].purchaseQty) + parseFloat(itm.purchaseQty)).toFixed(3);
                  // d.items[index].purchaseAmount = parseFloat(  parseFloat(d.items[index].purchaseAmount )   + parseFloat(itm.purchaseAmount) ).toFixed(3);
                  d.items[index].consumptionQty = parseFloat( parseFloat(d.items[index].consumptionQty) + parseFloat(itm.consumptionQty)).toFixed(3);
                  // d.items[index].consumptionAmount = parseFloat( parseFloat(d.items[index].consumptionAmount) + parseFloat(itm.consumptionAmount)).toFixed(3);
                  d.items[index].wastageQty = parseFloat( parseFloat(d.items[index].wastageQty) + parseFloat(itm.wastageQty)).toFixed(3);
                  // d.items[index].wastageAmt = parseFloat( parseFloat(  parseFloat(d.items[index].wastageQty ) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)   ) + parseFloat( parseFloat(itm.wastageQty ) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  d.items[index].closingQty = parseFloat(parseFloat(d.items[index].closingQty) + parseFloat(itm.closingQty)).toFixed(3);
                  // d.items[index].closingAmount = parseFloat( parseFloat( parseFloat(d.items[index].closingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  ) + parseFloat( parseFloat(itm.closingQty) * parseFloat(self._lastPrice[lastPriceIndex2].monthAverage)  )).toFixed(3);
                  //d.idealCost=parseFloat( parseFloat(d.items[index].consumptionAmount) + parseFloat(d.items[index].wastageAmt) ).toFixed(3);
                 // console.log("ideal",d.idealCost);
                  if (itm.physicalQty != "NA") {
                    if(itm.itemName == "Garlic Chicken(bk)"){
                      //console.log(itm.itemName, itm.physicalQty);
                    }
                    if (d.items[index].physicalQty != "NA") {
                      //console.log('Hello --------------------------------------------')
                      //console.log(itm);
                      d.items[index].physicalQty = parseFloat(itm.physicalQty).toFixed(3);
                    }
                    else {
                      //console.log('Hello 2--------------------------------------------')

                      //console.log(itm, d.date);
                      d.items[index].physicalQty = parseFloat(parseFloat(itm.physicalQty)).toFixed(3);
                    }
                    //d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //if(d.items[index].physicalQty!="NA"){
                    //  var dNO=parseFloat(i+1);
                    //
                    //  //d.items[index].openingQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].closingQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(d.items[index].purchaseQty)-parseFloat(d.items[index].wastageQty)-parseFloat(d.items[index].consumptionQty)).toFixed(3);
                    //  d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].varianceQty=parseFloat(parseFloat(d.items[index].closingQty)-parseFloat(d.items[index].physicalQty)).toFixed(3);
                    //  d.items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(d.items[index].varianceQty)/parseFloat(d.items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //if(dNO>=0){
                    //  days[dNO].items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  days[dNO].items[index].varianceQty=parseFloat(parseFloat(days[dNO].items[index].closingQty)-parseFloat(days[dNO].items[index].physicalQty)).toFixed(3);
                    //  days[dNO].items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(days[dNO].items[index].varianceQty)/parseFloat(days[dNO].items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //}
                    //d.items[index].physicalQty="NA";
                  }
                  items.splice(ii, 1);
                }
              }
            })
           
            console.log("-----------------------");
            console.log(angular.copy(d.items) );
            _.forEach(d.items, function (itm){
               
              var lastPriceIndex3=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
              // if(itm.wastageAmt==undefined)
              // {
              //   itm.wastageAmt=0
              // }
              // if(itm.consumptionAmount==undefined)
              // {
              //   itm.consumptionAmount=0
              // }
                
              
               
              if(itm.physicalQty != "NA"){
                //console.log("itm phy",itm);
                // itm.physicalAmt=parseFloat( parseFloat(itm.physicalQty)  * parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  ) ).toFixed(3)
                itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
                itm.closingAmount=parseFloat(parseFloat(itm.closingQty)*parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) ) ).toFixed(3)
               // if(itm.consumptionQty>0 || itm.wastageQty>0)
                  //d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                
                //if(itm.physicalQty === "NA")
                //  itm.varianceAmt=parseFloat().toFixed(3)
                //else
                //console.log("var amt");
                //console.log(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt));
                if(itm.physicalQty>0)
                {
                   if(itm.varianceQty!="NA" || itm.varianceQty>0)
                  {
                   // d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                   // d.actualCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt) ).toFixed(3);
                    itm.varianceAmt=parseFloat(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt)).toFixed(3);    
                   // d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
                   
                  }
                }
                //   if(itm.physicalQty<=0 || itm.physicalQty==="NA")
                // {
                //   console.log("inside else");
                //   d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                //   d.actualCost=parseFloat( d.idealCost ).toFixed(3);
                // }
                // else
                // {
                //    d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                // }
               
               // itm.varianceAmt=parseFloat(parseFloat( parseFloat(itm.closingQty)  * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  )-parseFloat( parseFloat(itm.physicalQty) * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) )).toFixed(3);
                itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
              }
              
            
            });
          });
          return days;
        },
        generateDateWiseVarianceReportViaPhysical: function (items, days) {
          var self = this;
          //console.log('---------------------------ITEMS----------------------------------');
          //console.log(angular.copy(items));
          items.sort(function (a, b) {
            if((new Date(b.created).getTime() - new Date(a.created).getTime()) != 0)
              return new Date(b.created) - new Date(a.created);
            else return a.num - b.num;
          });

          console.log('items resorted', angular.copy(items));
          console.log('days',days);
          _.forEach(days, function (d, i) {
            d.items = [];
            var pdate = new Date(d.date);
            pdate.setDate(pdate.getDate() - 1);
            console.log('pDate :-  ', pdate);
            d.items = self.getPreviousDate_Data(days, pdate);
            //console.log('---------------------------D.ITEMS----------------------------------');
            //console.log(angular.copy(d.items));
            var resetDate = new Date(d.date);
            //console.log('resetDate :-   ',resetDate);
            resetDate = new Date(resetDate.setDate(resetDate.getDate() + 1));
            var ind = -1;
            _.forEachRight(items, function (itm, ii) {
              var cDate = new Date(itm.created);
              if(_.has(itm, 'totalBalanceQty')){
                var cDate = new Date(itm.created);
              }
              //console.log('cDate :-   ',cDate);
              //console.log(itm);
              //cDate.setHours(0,0,0,0);
              //var resetDate = new Date(d.date.setDate(d.date.getDate() + 1));
              //console.log('resetDate :-   ',resetDate);
              //console.log('d.date :-   ',d.date);
              //console.log(cDate);
              if (new Date(d.date) > new Date(cDate)) {
                console.log('inside 1',itm);
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                  }
                  else {
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    if(itm.physicalQty)
                    itm.consumptionQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.physicalQty)).toFixed(3);
                    else
                    itm.consumptionQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.closingQty)).toFixed(3);
                    //itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                    itm.closingQty = parseFloat(itm.closingQty).toFixed(3);
                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
              }
              else if (new Date(d.date) <= new Date(cDate) && new Date(cDate) <= resetDate) {
                console.log('inside 2',itm);
                var index = _.findIndex(d.items, {itemId: itm.itemId});
                ind = index;
                if (index < 0) {
                  if (index == -1) {
                    console.log('3',angular.copy(itm));
                    itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                    if(itm.totalBalanceQty)
                    {
                      itm.closingQty = parseFloat(itm.physicalQty).toFixed(3);

                    itm.consumptionQty = parseFloat(itm.physicalQty).toFixed(3);
                    }
                  else
                    itm.consumptionQty = parseFloat(itm.closingQty).toFixed(3);

                  }
                  else {
                    console.log('4',angular.copy(itm));
                    itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                    //console.log(d.date, itm.openingQty);
                    /*itm.closingQty = parseFloat(itm.closingQty).toFixed(3);
                    if(itm.physicalQty)
                    itm.consumptionQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.physicalQty)).toFixed(3);
                  else
                    itm.consumptionQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.closingQty)).toFixed(3);
                    */
                    //itm.consumptionQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.closingQty)).toFixed(3);
                  }
                  d.items.push(itm);
                  items.splice(ii, 1);
                }
                else {
                  //console.log('babababbababababababbababababbabababababbaab');
                  d.items[index].openingQty = parseFloat(parseFloat(d.items[index].openingQty) + parseFloat(itm.openingQty)).toFixed(3);
                  d.items[index].purchaseQty = parseFloat(parseFloat(d.items[index].purchaseQty) + parseFloat(itm.purchaseQty)).toFixed(3);
                  d.items[index].consumptionQty = parseFloat(parseFloat(d.items[index].consumptionQty) + parseFloat(itm.consumptionQty)).toFixed(3);
                  d.items[index].wastageQty = parseFloat(parseFloat(d.items[index].wastageQty) + parseFloat(itm.wastageQty)).toFixed(3);
                  d.items[index].closingQty = parseFloat(parseFloat(d.items[index].closingQty) + parseFloat(itm.closingQty)).toFixed(3);
                  if (itm.physicalQty != "NA") {
                    if(itm.itemName == "Garlic Chicken(bk)"){
                      //console.log(itm.itemName, itm.physicalQty);
                    }
                    if (d.items[index].physicalQty != "NA") {
                      //console.log('Hello --------------------------------------------')
                      //console.log(itm);
                      d.items[index].physicalQty = parseFloat(itm.physicalQty).toFixed(3);
                      d.items[index].closingQty = parseFloat(itm.physicalQty).toFixed(3);
                      d.items[index].consumptionQty = parseFloat(parseFloat(d.items[index].openingQty) + parseFloat(d.items[index].purchaseQty) - parseFloat(itm.physicalQty)).toFixed(3);
                    }
                    else {
                      //console.log('Hello 2--------------------------------------------')

                      //console.log(itm, d.date);
                      d.items[index].closingQty = parseFloat(itm.physicalQty).toFixed(3);
                      d.items[index].physicalQty = parseFloat(parseFloat(itm.physicalQty)).toFixed(3);
                    }
                    //d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //if(d.items[index].physicalQty!="NA"){
                    //  var dNO=parseFloat(i+1);
                    //
                    //  //d.items[index].openingQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].closingQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(d.items[index].purchaseQty)-parseFloat(d.items[index].wastageQty)-parseFloat(d.items[index].consumptionQty)).toFixed(3);
                    //  d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  d.items[index].varianceQty=parseFloat(parseFloat(d.items[index].closingQty)-parseFloat(d.items[index].physicalQty)).toFixed(3);
                    //  d.items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(d.items[index].varianceQty)/parseFloat(d.items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //if(dNO>=0){
                    //  days[dNO].items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    //  days[dNO].items[index].varianceQty=parseFloat(parseFloat(days[dNO].items[index].closingQty)-parseFloat(days[dNO].items[index].physicalQty)).toFixed(3);
                    //  days[dNO].items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(days[dNO].items[index].varianceQty)/parseFloat(days[dNO].items[index].closingQty))*parseFloat(100)).toFixed(2);
                    //}
                    //d.items[index].physicalQty="NA";
                  }
                  items.splice(ii, 1);
                }
              }
            })
           /* _.forEach(d.items, function (itm){
              if(itm.physicalQty != "NA"){
                itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
                itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
              }
            });*/
          });
          return days;
        },
        getPreviousDate_Data: function (items, pdate) {
          var data = [];
          var index = _.findIndex(items, {date: pdate});
          if (index >= 0) {
            var d = angular.copy(items[index].items);
             _.forEach(d, function (itm, i) {
              if(itm.physicalQty != "NA")
                itm.openingQty = parseFloat(itm.physicalQty).toFixed(3);
              else
                itm.openingQty = parseFloat(itm.closingQty).toFixed(3);
              itm.closingQty = itm.openingQty;
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.physicalQty = "NA";
              itm.varianceQty = "NA";
              itm.varianceAmt = "NA";
              itm.varianceInPercent = "NA";
              data.push(itm);
            });
          }
          return data;
        },
        getPurchaseConsumptionReport: function () {

        },
        getStockSummaryReport: function () {

        },
        getItemWiseReport: function () {

        },
        getVarianceConsolidated: function (req) {
          var d = $q.defer();
          var self = this;
          console.log('req',req);
          isYieldEnabled = req.isYieldEnabled;
          if(req.ConsumptionViaPhysical ==false)
          {
          stockReportResetTime.getConsumptionSummary(req, function (result){
          //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {
            if(result.errorMessage){
              d.reject(result.errorMessage);
            }
            else {
              self._data = result;
              //console.log(angular.copy(result));
              //console.log(JSON.stringify(result.beforeDate));
              result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result = self.spliceAllItemsBeforePhysicalDateWise();
              //console.log(angular.copy(result));
              result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
              result=self.calculatePrice(result)
              console.log(angular.copy(result),"price" )
              result = self.formatDataForReport();
              console.log(angular.copy(result));
              result = self.spliceForCategoryAndItem();
              //console.log(angular.copy(result));
              var res = self.convertItemInPreferredUnit();
              //console.log(result);
              d.resolve(result);
            }
          });
          }
          else
          {
           stockReportResetTime.getConsumptionViaPhysical(req, function (result){
          //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {
            if(result.errorMessage){
              d.reject(result.errorMessage);
            }
            else {
              self._data = result;
              //console.log(angular.copy(result));
              //console.log(JSON.stringify(result.beforeDate));
              result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              //result = self.spliceAllItemsBeforePhysicalDateWise();
              //console.log(angular.copy(result));
              /*result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });*/
              result = self.formatDataForReportViaPhysical();
              //console.log(angular.copy(result));
              result = self.spliceForCategoryAndItem();
              //console.log(angular.copy(result));
              var res = self.convertItemInPreferredUnit();
              //console.log(result);
              d.resolve(result);
            }
          });
          }
          return d.promise;
        },
        getVarianceConsolidatedAggregated: function (req) {
          var d = $q.defer();
          var self = this;
          stockAggregateService.getData(req, function (result) {
          //stockReportResetTime.getConsumptionSummary(req, function (result){
          //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {

            if(result.errorMessage){
              d.reject(result.errorMessage);
            }
            else {
              self._data = result;
              result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result = self.spliceAllItemsBeforePhysicalDateWise();
              //console.log(angular.copy(result));
              result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
              console.log('result',result);
              result = self.formatDataForReportAggregated();
              //console.log(angular.copy(result));
              result = self.spliceForCategoryAndItem();
              //console.log(angular.copy(result));
              var res = self.convertItemInPreferredUnit();
              console.log(result);
              d.resolve(result);
            }
          });
          return d.promise;
        },
        getConsumptionViaPhysical: function (req) {
          var d = $q.defer();
          var self = this;
          stockReportResetTime.getConsumptionViaPhysical(req, function (result){
          //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {
            if(result.errorMessage){
              d.reject(result.errorMessage);
            }
            else {
              self._data = result;
              //console.log(angular.copy(result));
              //console.log(JSON.stringify(result.beforeDate));
              result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result = self.spliceAllItemsBeforePhysicalDateWise();
              //console.log(angular.copy(result));
              result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
              result = self.formatDataForReport();
              //console.log(angular.copy(result));
              result = self.spliceForCategoryAndItem();
              //console.log(angular.copy(result));
              var res = self.convertItemInPreferredUnit();
              //console.log(result);
              d.resolve(result);
            }
          });
          return d.promise;
        },
        getVarianceDateWiseReport: function (req) {
          var d = $q.defer();
          var self = this;
          var stockUnits = this._units;
          console.log('req',req);
          isYieldEnabled = req.isYieldEnabled;
          if(req.consumptionViaPhysical == false)
          {
          stockReportResetTime.getConsumptionSummary(req, function (result) {
          //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {
            if(result.errorMessage){
              d.reject(result.errorMessage);
            } else {
              self._data = result;
              //console.log(JSON.stringify(result.beforeDate));
              result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result = self.spliceAllItemsBeforePhysicalDateWise();
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
              console.log("before Price",angular.copy(result))
              result=self.calculatePrice(result)
              result = self.formatDataForReport_DateWise();
              console.log("format data",angular.copy(result));
              result = self.spliceForCategoryAndItem();
              var res = self.convertItemInPreferredUnit_DateWise(result);
              var days = self.getDates();
              var gtR = self.generateDateWiseVarianceReport(res.items, days);
              console.log("-------------------------------");
              console.log(angular.copy(gtR),"dataaaaaaaaaaaaa")
              var index = gtR.length - 1;
              gtR.splice(index, 1);
               var result = self._data;
              var items = self._stockItems;
              
              _.forEach(gtR, function (gt, i) {
              
               // console.log("days")
               // console.log(gt)

               // gt.idealCost=0.0,gt.actualCost=0.0
               //console.log("gtttttttt");
               //console.log(gt)
               //console.log("stock items",items)
                _.forEach(gt.items,function(itm){
                     var indeex=_.findIndex(items,function(item) {
                        return item._id == itm.itemId || item.itemId == itm.itemId;
                      });
                    if (indeex >= 0)
                     {
                        
                        var item = items[indeex];
                           // console.log("--------------------------------------");
                          // console.log("consumptionAmount",itm.consumptionAmount)
                          // console.log("wastageAmount",itm.wastageAmount);
                          // console.log("actualCost",angular.copy(gt.actualCost) );
                          // gt.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmount) +  parseFloat(gt.actualCost) ).toFixed(3);
                          // console.log("actualCost",angular.copy(gt.actualCost) );
                          // if(itm.physicalQty!=="NA" || itm.physicalQty!=0)
                          // {
                          //   gt.actualCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmount) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt)  ).toFixed(3)
                          // }
                          // else
                          //  {
                          //   gt.actualCost=parseFloat(gt.idealCost).toFixed(3)
                          //  } 

                        // console.log(item,"prefered")
                        
                        if (item.preferedUnit != undefined) 
                        {
                          //console.log("preferred")
                         // console.log(item.itemName, item.preferedUnit);
                          if(typeof item.preferedUnit == 'string'){
                            // console.log("Outlet Item")
                            // console.log(item.itemName, item.preferedUnit);
                              _.forEach(item.units, function (u, i) {
                                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                                  {
                                    var ind = _.findIndex(stockUnits, {_id: u._id});
                                    if(ind >= 0)
                                    {
                                      u.baseUnit = stockUnits[ind].baseUnit;
                                      u.conversionFactor = stockUnits[ind].conversionFactor;
                                      u.baseConversionFactor = stockUnits[ind].baseConversionFactor;
                                    }  
                                  }
                                if (u._id == item.preferedUnit) {
                                  var conversionFactor = 1;
                                  var pconFac = parseFloat(u.conversionFactor);
                                  itm.UnitName = u.unitName;
                                  if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
                                    conversionFactor = parseFloat(u.baseConversionFactor);
                                    if (pconFac > conversionFactor) {
                                      if (itm.physicalQty != "NA") {
                                         if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                          itm.varianceAmt= parseFloat(itm.varianceAmt* pconFac).toFixed(3);
                                         
                                       if(itm.varianceAmt==NaN)
                                         itm.varianceAmt="NA"
                                      }
                                    }
                                    else if (pconFac < conversionFactor) {
                                
                                      if (itm.physicalQty != "NA") {
                                        if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                           itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(pconFac)).toFixed(3);
                                      if(itm.varianceAmt==NaN)
                                         itm.varianceAmt="NA"
                                      }
                                    }
                                  }
                                  else {
                                   
                                    if (pconFac > conversionFactor) {
                                       // console.log("pconf",pconFac)
                                       // console.log("conversionFactor",conversionFactor);
                                       // console.log("pcon is greater");
                                       // console.log("kggg",itm.itemName);
                                       // console.log(angular.copy(itm))
                                      if (itm.physicalQty != "NA") {
                                        if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                           itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(pconFac)).toFixed(3);
                                       console.log("var amt",angular.copy(itm.varianceAmt))
                                      if(itm.varianceAmt==NaN)
                                         itm.varianceAmt="NA"
                                      }
                                    }
                                    else if (pconFac < conversionFactor) {
                                        // console.log("pconf",pconFac)
                                        // console.log("conversionFactor",conversionFactor);
                                        // console.log("confactor is greater");
                                        if (itm.physicalQty != "NA") {
                                           if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                              itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(conversionFactor)).toFixed(3);
                                         //console.log("var amt",angular.copy(itm.varianceAmt))
                                         if(itm.varianceAmt==NaN)
                                           itm.varianceAmt="NA"
                                      }
                                    }
                                    else
                                    {
                                       // console.log("else");
                                       // console.log("pconf",pconFac)
                                       // console.log("conversionFactor",conversionFactor);
                                       // console.log("pcon equal to conf");
                                       // console.log("kggg",itm.itemName);
                                       // console.log(angular.copy(itm))
                                       if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                         itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(conversionFactor)).toFixed(3);
                                         if(itm.varianceAmt==NaN)
                                            itm.varianceAmt="NA"
                                    }
                                  }
                                }
                              });
                          }
                           else {
                              //console.log("Base Kitchen Items")
                             // console.log(item.itemName);
                              _.forEach(item.units,function(u,i){
                                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                                  {
                                    var ind = _.findIndex(stockUnits, {_id: u._id});
                                    if(ind >= 0)
                                    {
                                      u.baseUnit = stockUnits[ind].baseUnit;
                                      u.conversionFactor = stockUnits[ind].conversionFactor;
                                      u.baseConversionFactor = stockUnits[ind].baseConversionFactor;
                                    }  
                                  }
                                    if(u._id==item.preferedUnit._id){
                                      var conversionFactor=1;
                                      console.log(itm.varianceAmt)
                                      var pconFac=parseFloat(u.conversionFactor);
                                      itm.UnitName=u.unitName;
                                      if(u.baseUnit.id==2 || u.baseUnit.id==3){
                                        conversionFactor = parseFloat(u.baseConversionFactor);
                                         if(itm.varianceQty!="NA" || itm.varianceQty!=0)
                                              {
                                               // console.log("inside varianceAmt")
                                             //   itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                                                itm.varianceAmt= parseFloat(itm.varianceAmt* 1000).toFixed(3); 
                                              }
                                            if(itm.varianceAmt==NaN || isNaN(itm.varianceAmt))
                                             itm.varianceAmt="NA"
                                           if(itm.physicalQty==NaN || isNaN(itm.physicalQty))
                                              itm.physicalQty="NA"
                                        if (pconFac > conversionFactor) {
                                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                                            if (itm.physicalQty == undefined) {
                                                itm.physicalQty = parseFloat(0).toFixed(3);
                                              }
                                              else {
                                                itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                                              }
                                          if (itm.physicalQty != "NA" ) {
                                            
                                        
                                          
                                          }
                                           if(itm.varianceQty!="NA" || itm.varianceQty!=0)
                                              {
                                              //  console.log("inside varianceAmt")
                                                itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(conversionFactor)).toFixed(3);
                                             //   itm.varianceAmt= parseFloat(itm.varianceAmt* 1000).toFixed(3); 
                                              }
                                           if(itm.varianceAmt==NaN)
                                             itm.varianceAmt="NA"
                                           if(itm.physicalQty==NaN || isNaN(itm.physicalQty))
                                              itm.physicalQty="NA"
                                        }
                                        else if (pconFac < conversionFactor) {
                                          itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                                          itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                                          itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                                          itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                                          itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                                            if (itm.physicalQty == undefined) {
                                                itm.physicalQty = parseFloat(0).toFixed(3);
                                              }
                                              else {
                                                itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                                              }
                                          if (itm.physicalQty != "NA") {
                                            if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                            {
                                              itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                                            //  itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(pconFac)).toFixed(3);

                                            }
                                               
                                          if(itm.varianceAmt==NaN)
                                             itm.varianceAmt="NA"
                                        
                                          }
                                             if(itm.physicalQty==NaN || isNaN(itm.physicalQty))
                                              itm.physicalQty="NA"
                                        }
                                      }
                                      else{

                                         if (pconFac > conversionFactor) {
                                              itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                                              itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                                              itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                                              itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                                              itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(pconFac)).toFixed(3);
                                              if (itm.physicalQty == undefined) {
                                                itm.physicalQty = parseFloat(0).toFixed(3);
                                              }
                                              else {
                                                itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(pconFac)).toFixed(3);
                                              }
                                               // console.log("pconf",pconFac)
                                               // console.log("conversionFactor",conversionFactor);
                                               // console.log("pcon is greater");
                                               // console.log("kggg",itm.itemName);
                                               //  console.log("consumptionQty",itm.consumptionQty)
                                               // console.log("closingQty",itm.closingQty)
                                               // console.log("physicalQty",itm.physicalQty)
                                               // console.log(angular.copy(itm))
                                               if (itm.physicalQty != "NA") {
                                                  if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                                  {
                                                    itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) / parseFloat(pconFac)).toFixed(3);
                                                    itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(conversionFactor)).toFixed(3);
                                                  }
                                                    
                                                // console.log("var amt",angular.copy(itm.varianceAmt))
                                                if(itm.varianceAmt==NaN)
                                                  itm.varianceAmt="NA"
                                               
                                                }
                                                 if(itm.physicalQty==NaN || isNaN(itm.physicalQty))
                                                   itm.physicalQty="NA"
                                           }

                                          else if (pconFac < conversionFactor) {
                                                itm.openingxQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                                                itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                                                itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                                                itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                                                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) / parseFloat(conversionFactor)).toFixed(3);
                                                if (itm.physicalQty == undefined) {
                                                  itm.physicalQty = parseFloat(0).toFixed(3);
                                                 }
                                                else {
                                                  itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) / parseFloat(conversionFactor)).toFixed(3);
                                                 }
                                                  // console.log("pconf",pconFac)
                                                  // console.log("conversionFactor",conversionFactor);
                                                  // console.log("confactor is greater");
                                                  if (itm.physicalQty != "NA") {
                                                     if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                                     {
                                                      itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) /parseFloat(conversionFactor)).toFixed(3);
                                                      itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(conversionFactor)).toFixed(3);
                                                     }
                                                        
                                                       console.log("var amt",angular.copy(itm.varianceAmt))
                                                     if(itm.varianceAmt==NaN)
                                                       itm.varianceAmt="NA"
                                                     
                                                       }
                                                       if(itm.physicalQty==NaN || isNaN(itm.physicalQty))
                                                         itm.physicalQty="NA"
                                                }
                                            else
                                            {
                                                itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                                                itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                                                itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                                                itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                                                itm.wastageQty = parseFloat(parseFloat(itm.wastageQty) * parseFloat(conversionFactor)).toFixed(3);
                                                if (itm.physicalQty == undefined) {
                                                  itm.physicalQty = parseFloat(0).toFixed(3);
                                                }
                                                else {
                                                  itm.physicalQty = parseFloat(parseFloat(itm.physicalQty) * parseFloat(conversionFactor)).toFixed(3);
                                                }
                                               // console.log("else");
                                               // console.log("pconf",pconFac)
                                               // console.log("conversionFactor",conversionFactor);
                                               // console.log("pcon equal to conf");
                                               // console.log("kggg",itm.itemName);
                                               // console.log("consumptionQty",itm.consumptionQty)
                                               // console.log("closingQty",itm.closingQty)
                                               // console.log()
                                               // console.log(angular.copy(itm))
                                               if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                               {
                                                itm.varianceQty = parseFloat(parseFloat(itm.varianceQty) * parseFloat(conversionFactor)).toFixed(3);
                                                itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(conversionFactor)).toFixed(3);
                                               }
                                                 
                                                 if(itm.varianceAmt==NaN)
                                                    itm.varianceAmt="NA"
                                                 if(itm.physicalQty==NaN || isNaN(itm.physicalQty))
                                                   itm.physicalQty="NA"
                                            }
                                       }
                                    }
                              });
                           }
                        }
                        else
                        {
                             console.log("Base Preferred Not Present")
                             var count=0
                             console.log(itm);
                               // console.log(item);
                                _.forEach(item.units,function(u){   
                                    if(u.baseUnit.id==2 && count==0)
                                      {
                                       count++
                                         if (itm.physicalQty != "NA") {
                                           if(itm.varianceQty!="NA" || itm.varianceQty >0)
                                              itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(1000)).toFixed(3);
                                         if(itm.varianceAmt==NaN)
                                            itm.varianceAmt="NA"

                                      }
                                       if(itm.physicalQty==NaN)
                                          itm.physicalQty="NA"
                                     }

                                })
                      } 
                    } //End of if index
                    else
                    {
                        // console.log(itm.itemName,"else index Not Found")
                        // console.log(itm)
                        var indeex = _.findIndex(items, {itemId: itm.itemId});
                        var item = items[indeex];
                        _.forEach(item.units,function(u){   
                            
                          if(u.baseUnit.id==2 || u.baseUnit.id==3){
                            console.log(itm.itemName,"mutton")
                          if(itm.physicalQty != undefined || itm.physicalQty!="NA")
                            itm.varianceAmt = parseFloat(parseFloat(itm.varianceAmt) * parseFloat(u.conversionFactor)).toFixed(3);
                          }
                        })
                    }
                 })  // end of insisde forach
              });
                            //console.log("gtrrrrrrrr",angular.copy(gtR));
           /* _.forEach(gtR, function (d, i) {
            
            
            console.log("-----------------------");
            console.log(angular.copy(d.items) );
            var count =0;
            _.forEach(d.items, function (itm){
              console.log("food item",itm);
              console.log("-----------------------");
              //d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
              //var lastPriceIndex3=_.findIndex(self._lastPrice,{ itemId:itm.itemId } );
              // if(itm.wastageAmt==undefined)
              // {
              //   itm.wastageAmt=0
              // }
              // if(itm.consumptionAmount==undefined)
              // {
              //   itm.consumptionAmount=0
              // }
            
              
               
              if(itm.physicalQty != "NA"){

                //console.log("itm phy",itm);
                //itm.physicalAmt=parseFloat( parseFloat(itm.physicalQty)  * parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  ) ).toFixed(3)
               // itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
               // itm.closingAmount=parseFloat(parseFloat(itm.closingQty)*parseFloat(parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) ) ).toFixed(3)
               // if(itm.consumptionQty>0 || itm.wastageQty>0)
                  //d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                
                //if(itm.physicalQty === "NA")
                //  itm.varianceAmt=parseFloat().toFixed(3)
                //else
                //console.log("var amt");
                //console.log(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt));
                if(itm.physicalQty>0)
                {
                   if(itm.varianceQty!="NA" || itm.varianceQty>0)
                  {
                   // d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                    actualCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt) ).toFixed(3);
                    d.actualCost=actualCost
                    itm.varianceAmt=parseFloat(parseFloat(itm.closingAmount)-parseFloat(itm.physicalAmt)).toFixed(3);    
                    idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
                    
                    console.log(d.idealCost,"ideal phy");
                  }
                }
                //   if(itm.physicalQty<=0 || itm.physicalQty==="NA")
                // {
                //   console.log("inside else");
                //   d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                //   d.actualCost=parseFloat( d.idealCost ).toFixed(3);
                // }
                // else
                // {
                //    d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);
                // }
               
               // itm.varianceAmt=parseFloat(parseFloat( parseFloat(itm.closingQty)  * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage)  )-parseFloat( parseFloat(itm.physicalQty) * parseFloat(self._lastPrice[lastPriceIndex3].monthAverage) )).toFixed(3);
                //itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
              }
              else
              {
                count++;
                console.log('count',count);
                console.log("else not physical")
                if(itm.wastageAmt==undefined)
                  itm.wastageAmt=0
                console.log("consumptionAmount",angular.copy(itm.consumptionAmount));
                console.log("cwastageAmton",angular.copy(itm.wastageAmt));
                if(itm.physicalQty==0 || itm.physicalQty=="NA")
                {
                  idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3);   
                  //d.idealCost=idealCost

                  console.log("cost",angular.copy(d));
                }
                
              }

              d.idealCost=idealCost
              // else
              // {
              //       d.actualCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) + parseFloat(itm.closingAmount) - parseFloat(itm.physicalAmt) ).toFixed(3);
        
              //       d.idealCost=parseFloat( parseFloat(itm.consumptionAmount) + parseFloat(itm.wastageAmt) ).toFixed(3); 
              //       console.log(d.idealCost,"ideal phy");
              // }
              
           // console.log(d.idealCost, "cost")
            });
          });*/


              d.resolve(gtR);
              //d.resolve(result);
            }
          });
          }
          else
          {
           stockReportResetTime.getConsumptionViaPhysical(req, function (result) {
            console.log('result of getConsumptionViaPhysical',result)
          //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {
            if(result.errorMessage){
              d.reject(result.errorMessage);
            } else {
              self._data = result;
              //console.log(JSON.stringify(result.beforeDate));
              result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              /*result = self.spliceAllItemsBeforePhysicalDateWise();
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });*/
              result = self.formatDataForReport_DateWiseViaPhysical();
              console.log('formatDataForReport_DateWiseViaPhysical',angular.copy(result));
              result = self.spliceForCategoryAndItem();
              var res = self.convertItemInPreferredUnit_DateWise(result);
              //var res=self.convertItemInPreferredUnit();
              var days = self.getDates();
              //console.log(days);
              //console.log('items', angular.copy(res.items));
              var gtR = self.generateDateWiseVarianceReportViaPhysical(res.items, days);
              var index = gtR.length - 1;
              gtR.splice(index, 1);
              d.resolve(gtR);
              //d.resolve(result);
            }
          });
          }
          return d.promise;
        },
      getFoodCOstDateWiseReport: function (req) {
          var d = $q.defer();
          var deployment = this.deployment;
          var self = this;
          var resu
          var gtR
          console.log('request Food Cost ',req);
         

          

            stockAggregateService.getFoodCostData(req, function (result) {
            var actualCost=0,idealCost=0,data={},str=[]
            //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {
            if(result.errorMessage){
              d.reject(result.errorMessage);
            } else {
              var count=0
              console.log("result",angular.copy(result) );
              _.forEach(result,function(r){

                  data={}
                  self._data = r.data;
                  //console.log(JSON.stringify(result.beforeDate));
                  r.data.beforeDate.sort(function (a, b) {
                    return new Date(a.created) - new Date(b.created);
                  });
                  r.data.betweenDate.sort(function (a, b) {
                    return new Date(a.created) - new Date(b.created);
                  });
                  console.log("before splice",angular.copy(r));
                  resu = self.spliceAllItemsBeforePhysicalDateWiseFoodCost(r.data);
                  
                  //console.log("result after splice",angular.copy(result))
                  resu.betweenDate.sort(function (a, b) {
                    if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                      return new Date(a.created) - new Date(b.created);
                    else
                      return b.num - a.num;
                  });
                  console.log("before Price",angular.copy(resu))
                  resu=self.calculatePriceFoodCost(angular.copy(resu) )
                  count++
                    //console.log("after  price",angular.copy(resu) )
                    
                   resu = self.formatDataForReport_DateWiseFoodCost(angular.copy(resu) )
                   console.log("format",angular.copy(resu) )
                   var d,createdDate=[]
                    _.forEach(resu.dateWiseItems,function(dateWiseItems){
                     // data[dateWiseItems]=[]
                        console.log("datewise",dateWiseItems)
                        var cDate
                        //if(dateWiseItems!=undefined)
                      _.forEach(dateWiseItems,function(obj){
                          var dDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(obj.created))));
                            if(!data[dDate])
                              data[dDate] = [];   
                              cDate=new Date(obj.created)
                              d=dDate
                              //console.log("d",d)
                               // if(obj.itemSaleQty>0){
                                //console.log("55555555555555555",idealCost, obj.consumptionAmount,obj.wastageAmt)
                                
                                idealCost= Number(idealCost+ obj.consumptionAmount+ obj.wastageAmt)
                                if(obj.actualConsumptionAmt==0)
                                  actualCost=Number(actualCost+ obj.actualConsumptionAmt + obj.consumptionAmount+ obj.wastageAmt)
                                else
                                  actualCost=Number(actualCost+ obj.actualConsumptionAmt)
                            // }
                           //   console.log("idealCost INN",idealCost)
                            //console.log("idealCost",obj.consumptionAmount,obj.wastageAmt)
                      });
                    //  if(cDate!=undefined)
                      createdDate.push({
                        date:cDate,
                        idealCost:idealCost,
                        actualCost:actualCost
                      })
                          //console.log("idealCost out",idealCost)
                     //   dateWise[obj].idealC=Number(idealCost)
                       // if(idealCost)
                       //createdDate({date:dateWiseItems.created})
                        // data[d].push({
                        //   idealCost:idealCost,
                        //   date:cDate
                        // })
                        idealCost=0
                        actualCost=0

                    })
                    //console.log("createdDate",createdDate)
                     //console.log("cost data",angular.copy(data) )
                     
                     // var grp = _.groupBy(createdDate, function(str) {
                        
                     //   return str.date; 
                     // });
                    // console.log("st rrgrppppppp",grp)
                     str.push(createdDate)
                    })  /// End of for each
                    
                    console.log("str",angular.copy(str) )
                    // var grp = _.groupBy(str, function(str) {
                    //     console.log("strrr",str)
                    //    return str.date; 
                    //  });
                    // console.log("grp",grp)
                     // console.log(gtR)


                    d.resolve(str);
                    //d.resolve(result);
                  }
               });// End of Food Cost aggregated data


            
          
        
          return d.promise;
        },
       spliceAllItemsBeforePhysicalDateWiseFoodCost: function (result) {
          var self = this;
          //console.log(angular.copy(self._data));
          var data = [];
          var result = result;
          var deployment = self.deployment;
          // console.log("deployment setting",deployment)
          var latestPhysical = null;
          var latestPhysicalBetween = null;
          var i = 0;
          var j = 0;
          _.forEach(result.beforeDate, function(r,i){
            if(r.totalBalanceQty!=undefined){
              var d = new Date(r.created);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              //d.setHours(0, 0, 0, 0);
              r.created = new Date(d);
              if(!latestPhysical){
                i = 0;
                latestPhysical = new Date(d);
              } else {
                if(latestPhysical.getTime() == d.getTime()){
                  i++;
                } else if(d.getTime() > latestPhysical.getTime()) {
                  i = 0;
                  latestPhysical = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = i;
              data.push(r);
            };
          });
          _.forEach(data,function(d,i){
            _.forEachRight(result.beforeDate,function(itm,ii){
              if(itm.itemId==d.itemId){
                var phyDate=new Date(d.created);
                var compareDate=new Date(itm.created);
                if(phyDate.getTime()>compareDate.getTime()){
                  result.beforeDate.splice(ii,1);
                }
              }
            });
          });

          // var data2=[];
          _.forEachRight(result.betweenDate,function(r,i){
            if(r.totalBalanceQty!=undefined){
              //console.log(r.created);
              r.actualDate = r.created;
              var d=new Date(r.created);
              //var cdate=new Date($scope.purchaseConsumptionForm.toDate);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              //console.log(a)
              //console.log(b)
              //console.log(angular.copy(d))
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              //d.setDate(d.getDate() + 1);
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              r.created = new Date(d);
              if(!latestPhysicalBetween){
                j = 0;
                latestPhysicalBetween = new Date(d);
              } else {
                if(latestPhysicalBetween.getTime() == d.getTime()){
                  j++;
                } else if(d.getTime() > latestPhysicalBetween.getTime()) {
                  j = 0;
                  latestPhysicalBetween = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = j;
              //console.log(r.created);
              // if(cdate.getTime()>d.getTime()){
              //   data2.push(r);
              // }
              // else
              // {
              //   result.betweenDate.splice(i,1);
              // }
            };
          });
          // _.forEach(data2,function(d,i){
          //   _.forEachRight(result.betweenDate,function(itm,ii){
          //     if(itm.itemId==d.itemId){
          //       if(itm.totalBalanceQty!=undefined){
          //           var phyDate=new Date(d.created);
          //           var compareDate=new Date(itm.created);
          //           if(phyDate.getTime()>compareDate.getTime()){
          //             result.betweenDate.splice(ii,1);
          //           }
          //         }
          //     }
          //   });
          //});
          console.log(angular.copy(self._data));
          return self._data;
        },
      
    // calculatePriceFoodCost :function (result){
    //   var self = this;
    //   console.log("price");
    //   console.log(result);
    //                   // if(count==0)
    //                   //  {
    //                   //      _.forEach(self._lastPrice,function(price){
    //                   //             price.monthAverage=parseFloat(price.monthAverage * 1000).toFixed(2)
    //                   //          })
    //                   //      console.log("counttttttttt")
    //                   //  }
    //                 _.forEach(result.beforeDate,function(itm,i){
    //                     //console.log(JSON.stringify(itm));
    //                   //  console.log("price",self._lastPrice)
    //                     var index=_.findIndex(self._lastPrice,{itemId:itm.itemId});
    //                     //if(itm.itemName=="Chicken Burger Tikki")
    //                       //  console.log("itm price",itm)
    //                     //console.log(itm.itemName, itm.totalPurchaseQty, $scope.lastPrice[index].monthAverage, itm.ConversionFactor, itm.UnitName);
    //                     //if(index>=0){
    //                       console.log("conversion factor",itm)
    //                     if(itm.totalOpeningQty!=undefined){

    //                       //console.log("price",self._lastPrice[index].monthAverage)
    //                       if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
    //                         if(index>=0){
    //                           //console.log("opening index found");
    //                           itm.totalOpeningAmt=Number( itm.totalOpeningQty *self._lastPrice[index].monthAverage )
    //                           //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         }
    //                         else
    //                         {
    //                           //console.log("opening index NOT found");
    //                           itm.totalOpeningAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                                  //console.log("opening index found");
    //                           itm.totalOpeningAmt= Number(  itm.totalOpeningQty*self._lastPrice[index].monthAverage )
    //                           //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         }
    //                         else
    //                         {
    //                             //console.log("opening index NOT found");
    //                           itm.totalOpeningAmt=0.00;
    //                         }
    //                       }
    //                     }
                        


    //                     else if(itm.totalBalanceQty!=undefined){
    //                       if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
    //                         if(index>=0){
    //                           itm.totalBalanceAmt=Number( itm.totalBalanceQty*self._lastPrice[index].monthAverage );
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         else
    //                         {
    //                           itm.totalBalanceAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           itm.totalBalanceAmt= Number( itm.totalBalanceQty* self._lastPrice[index].monthAverage );
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         else
    //                         {
    //                           itm.totalBalanceAmt=0.00;
    //                         }
    //                       }
    //                     }

    //                    else if(itm.totalStockReturnQty != undefined && itm.totalStockReceiveQty != undefined 
    //                     && itm.totalTransferQty!=undefined  && itm.totalTransferQty_toStore!=undefined && itm.totalSaleQty!=undefined && itm.totalIntermediateQty!= undefined 
    //                    &&    itm.totalPurchaseQty!=undefined && itm.wastage != undefined 
    //                     ) 
    //                  {

    //                   if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
    //                       if(index>=0){
    //                             //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                             itm.totalPurchaseAmount=Number(  itm.totalPurchaseQty*self._lastPrice[index].monthAverage )
    //                             //console.log("pur amt");
    //                             //console.log(JSON.stringify(itm.totalPurchaseAmount))
    //                           }
    //                           else{
    //                             itm.totalPurchaseAmount=0.00;
    //                           }
    //                      }
    //                    else
    //                         {
    //                          if(index>=0){
    //                             //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                             itm.totalPurchaseAmount=Number(itm.totalPurchaseQty*self._lastPrice[index].monthAverage )
    //                           }
    //                           else{
    //                             itm.totalPurchaseAmount=0.00;
    //                           }
    //                         }
                        

                  

    //                    if(itm.wastageAmt==undefined || itm.wastageAmt==0){
                    
    //                     if(index>=0){
    //                       //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                       itm.wastageAmt=Number( itm.wastage *self._lastPrice[index].monthAverage)
    //                     }
    //                     else{
    //                       itm.wastageAmt=0.00;
    //                     }
    //                   }
    //                   else{
    //                     if(index>=0){
    //                       //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                       itm.wastageAmt=Number( itm.wastage* self._lastPrice[index].monthAverage) 
    //                     }
    //                     else{
    //                       itm.wastageAmt=0.00;
    //                     } 
    //                   }

    //                       if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
    //                         if(index>=0){
    //                           //console.log("transfer");
    //                           //console.log(JSON.stringify(itm));
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         itm.totalTransferAmount= Number( itm.totalTransferQty*self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         itm.totalTransferAmount=Number( itm.totalTransferQty*self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount=0.00;
    //                         }
    //                       }
                        
                        
    //                       if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
    //                         if(index>=0){
    //                           //console.log("transfer to store");
    //                           //console.log(JSON.stringify(itm));
    //                           //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                         //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalTransferAmount_toStore=Number(itm.totalTransferQty_toStore*self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount_toStore=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //console.log("transfer to store");
    //                           //console.log(JSON.stringify(itm));
    //                           //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                         //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalTransferAmount_toStore= Number(itm.totalTransferQty_toStore* self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount_toStore=0.00;
    //                         }
    //                       }


                         
    //                       if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
    //                         if(index>=0){
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

    //                           itm.itemSaleAmt= Number(itm.itemSaleQty* self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.itemSaleAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

    //                           itm.itemSaleAmt= Number( itm.itemSaleQty*self._lastPrice[index].monthAverage);
    //                         }
    //                         else
    //                         {
    //                           itm.itemSaleAmt=0.00;
    //                         }
    //                       }
                        
                         
    //                       if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
    //                         if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalIntermediateAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalIntermediateAmt=0.00;
    //                         }
    //                       }
                        
                         
    //                       //console.log('price', itm.itemName, itm.itemId, index);
    //                       if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
    //                         if(index>=0){
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReturnAmount= Number( itm.totalStockReturnQty*self._lastPrice[index].monthAverage)

    //                           //console.log(itm, $scope.lastPrice[index]);
    //                         }
    //                         else
    //                         {
    //                           itm.totalStockReturnAmount=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReturnAmount= Number(  itm.totalStockReturnQty*self._lastPrice[index].monthAverage)

    //                           //console.log(itm, $scope.lastPrice[index]);
    //                         }
    //                         else
    //                         {
    //                           itm.totalStockReturnAmount=0.00;
    //                         }
    //                       }
                        
                         
    //                       if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
    //                         if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReceiveAmount=Number( itm.totalStockReceiveQty* self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalStockReceiveAmount=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReceiveAmount=Number(  itm.totalStockReceiveQty * self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                          itm.totalStockReceiveAmount=0.00;
    //                         }
    //                       }


    //                 }

                        
                        

    //                   });
    //                   _.forEach(result.betweenDate,function(itm,i){
    //                              var index=_.findIndex(self._lastPrice,{itemId:itm.itemId});
    //                     //if(itm.itemName=="Chicken Burger Tikki")
    //                       //  console.log("itm price",itm)
    //                     //console.log(itm.itemName, itm.totalPurchaseQty, $scope.lastPrice[index].monthAverage, itm.ConversionFactor, itm.UnitName);
    //                     //if(index>=0){
    //                       console.log("conversion factor",itm.ConversionFactor)
    //                     if(itm.totalOpeningQty!=undefined){

    //                       //console.log("price",self._lastPrice[index].monthAverage)
    //                       if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
    //                         if(index>=0){
    //                           //console.log("opening index found");
    //                           itm.totalOpeningAmt=Number( itm.totalOpeningQty *self._lastPrice[index].monthAverage )
    //                           //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         }
    //                         else
    //                         {
    //                           //console.log("opening index NOT found");
    //                           itm.totalOpeningAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                                  //console.log("opening index found");
    //                           itm.totalOpeningAmt= Number(  itm.totalOpeningQty*self._lastPrice[index].monthAverage )
    //                           //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         }
    //                         else
    //                         {
    //                             //console.log("opening index NOT found");
    //                           itm.totalOpeningAmt=0.00;
    //                         }
    //                       }
    //                     }
    //                     else if(itm.totalBalanceQty!=undefined){
    //                       if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
    //                         if(index>=0){
    //                           itm.totalBalanceAmt=Number( itm.totalBalanceQty*self._lastPrice[index].monthAverage );
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         else
    //                         {
    //                           itm.totalBalanceAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           itm.totalBalanceAmt= Number( itm.totalBalanceQty* self._lastPrice[index].monthAverage );
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
    //                           //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
    //                         else
    //                         {
    //                           itm.totalBalanceAmt=0.00;
    //                         }
    //                       }
    //                     }

    //                    else if(itm.totalStockReturnQty != undefined && itm.totalStockReceiveQty != undefined && itm.itemSaleQty != undefined
    //                     && itm.totalTransferQty!=undefined  && itm.totalTransferQty_toStore!=undefined && itm.totalSaleQty!=undefined && itm.totalIntermediateQty!= undefined 
    //                    &&    itm.totalPurchaseQty!=undefined && itm.wastage != undefined 
    //                     ) 
    //                  {

    //                   if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
    //                       if(index>=0){
    //                             //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                             itm.totalPurchaseAmount=Number(  itm.totalPurchaseQty *self._lastPrice[index].monthAverage)
    //                             //console.log("pur amt");
    //                             //console.log(JSON.stringify(itm.totalPurchaseAmount))
    //                           }
    //                           else{
    //                             itm.totalPurchaseAmount=0.00;
    //                           }
    //                      }
    //                    else
    //                         {
    //                          if(index>=0){
    //                             //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                             itm.totalPurchaseAmount=Number(itm.totalPurchaseQty*self._lastPrice[index].monthAverage )
    //                           }
    //                           else{
    //                             itm.totalPurchaseAmount=0.00;
    //                           }
    //                         }
                        

    //                    if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
    //                         if(index>=0){
    //                           //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           //console.log(itm.totalSaleQty,self._lastPrice[index].monthAverage);

    //                           itm.totalSaleAmount=Number( itm.totalSaleQty *self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalSaleAmount=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                         if(index>=0){
    //                           //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                          // console.log(itm.totalSaleQty,self._lastPrice[index].monthAverage);

    //                           itm.totalSaleAmount=Number( itm.totalSaleQty * self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalSaleAmount=0.00;
    //                         }
    //                       }

    //                    if(itm.wastageAmt==undefined || itm.wastageAmt==0){
                    
    //                     if(index>=0){
    //                       //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                       itm.wastageAmt=Number( itm.wastage *self._lastPrice[index].monthAverage)
    //                     }
    //                     else{
    //                       itm.wastageAmt=0.00;
    //                     }
    //                   }
    //                   else{
    //                     if(index>=0){
    //                       //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
    //                       itm.wastageAmt=Number( itm.wastage* self._lastPrice[index].monthAverage) 
    //                     }
    //                     else{
    //                       itm.wastageAmt=0.00;
    //                     } 
    //                   }

    //                       if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
    //                         if(index>=0){
    //                           //console.log("transfer");
    //                           //console.log(JSON.stringify(itm));
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         itm.totalTransferAmount= Number( itm.totalTransferQty*self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                         itm.totalTransferAmount=Number( itm.totalTransferQty*self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount=0.00;
    //                         }
    //                       }
                        
                        
    //                       if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
    //                         if(index>=0){
    //                           //console.log("transfer to store");
    //                           //console.log(JSON.stringify(itm));
    //                           //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                         //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalTransferAmount_toStore=Number(itm.totalTransferQty_toStore*self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount_toStore=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //console.log("transfer to store");
    //                           //console.log(JSON.stringify(itm));
    //                           //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                         //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalTransferAmount_toStore= Number(itm.totalTransferQty_toStore* self._lastPrice[index].monthAverage)

    //                         }
    //                         else
    //                         {
    //                           itm.totalTransferAmount_toStore=0.00;
    //                         }
    //                       }


                         
    //                       if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
    //                         if(index>=0){
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

    //                           itm.itemSaleAmt= Number(itm.itemSaleQty* self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.itemSaleAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

    //                           itm.itemSaleAmt= Number( itm.itemSaleQty*self._lastPrice[index].monthAverage);
    //                         }
    //                         else
    //                         {
    //                           itm.itemSaleAmt=0.00;
    //                         }
    //                       }
                        
                         
    //                       if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
    //                         if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalIntermediateAmt=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalIntermediateAmt=0.00;
    //                         }
    //                       }
                        
                         
    //                       //console.log('price', itm.itemName, itm.itemId, index);
    //                       if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
    //                         if(index>=0){
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReturnAmount= Number( itm.totalStockReturnQty*self._lastPrice[index].monthAverage)

    //                           //console.log(itm, $scope.lastPrice[index]);
    //                         }
    //                         else
    //                         {
    //                           itm.totalStockReturnAmount=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReturnAmount= Number(  itm.totalStockReturnQty*self._lastPrice[index].monthAverage)

    //                           //console.log(itm, $scope.lastPrice[index]);
    //                         }
    //                         else
    //                         {
    //                           itm.totalStockReturnAmount=0.00;
    //                         }
    //                       }
                        
                         
    //                       if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
    //                         if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReceiveAmount=Number( itm.totalStockReceiveQty* self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                           itm.totalStockReceiveAmount=0.00;
    //                         }
    //                       }
    //                       else
    //                       {
    //                        if(index>=0){
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
    //                           //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
    //                           itm.totalStockReceiveAmount=Number(  itm.totalStockReceiveQty * self._lastPrice[index].monthAverage)
    //                         }
    //                         else
    //                         {
    //                          itm.totalStockReceiveAmount=0.00;
    //                         }
    //                       }




    //                 }

    //                   });
    //                   //console.log("price final ");
    //                   //console.log(JSON.stringify(result));
    //                   return result;
    // },


calculatePriceFoodCost :function (result){
      var self = this;
    
    
                      // if(count==0)
                      //  {
                      //      _.forEach(self._lastPrice,function(price){
                      //             price.monthAverage=parseFloat(price.monthAverage * 1000).toFixed(2)
                      //          })
                      //      console.log("counttttttttt")
                      //  }
                    _.forEach(result.beforeDate,function(itm,i){
                        //console.log(JSON.stringify(itm));
                      //  console.log("price",self._lastPrice)
                        var index=_.findIndex(self._lastPrice,{itemId:itm.itemId});
                        //if(itm.itemName=="Chicken Burger Tikki")
                          //  console.log("itm price",itm)
                        //console.log(itm.itemName, itm.totalPurchaseQty, $scope.lastPrice[index].monthAverage, itm.ConversionFactor, itm.UnitName);
                        //if(index>=0){
                          
                        if(itm.totalOpeningQty!=undefined){

                          //console.log("price",self._lastPrice[index].monthAverage)
                          if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
                            if(index>=0){
                              //console.log("opening index found");
                              itm.totalOpeningAmt=Number( itm.totalOpeningQty *self._lastPrice[index].lastPrice )
                              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            }
                            else
                            {
                              //console.log("opening index NOT found");
                              itm.totalOpeningAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                                     //console.log("opening index found");
                              itm.totalOpeningAmt= Number(  itm.totalOpeningQty*self._lastPrice[index].lastPrice )
                              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            }
                            else
                            {
                                //console.log("opening index NOT found");
                              itm.totalOpeningAmt=0.00;
                            }
                          }
                        }
                        


                        else if(itm.totalBalanceQty!=undefined){
                          if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
                            if(index>=0){
                              itm.totalBalanceAmt=Number( itm.totalBalanceQty*self._lastPrice[index].lastPrice );
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            else
                            {
                              itm.totalBalanceAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              itm.totalBalanceAmt= Number( itm.totalBalanceQty* self._lastPrice[index].lastPrice );
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            else
                            {
                              itm.totalBalanceAmt=0.00;
                            }
                          }
                        }

                       else if(itm.totalStockReturnQty != undefined && itm.totalStockReceiveQty != undefined 
                        && itm.totalTransferQty!=undefined  && itm.totalTransferQty_toStore!=undefined && itm.totalSaleQty!=undefined && itm.totalIntermediateQty!= undefined 
                       &&    itm.totalPurchaseQty!=undefined && itm.wastage != undefined 
                        ) 
                     {

                      if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
                          if(index>=0){
                                //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                                itm.totalPurchaseAmount=Number(  itm.totalPurchaseQty*self._lastPrice[index].lastPrice )
                                //console.log("pur amt");
                                //console.log(JSON.stringify(itm.totalPurchaseAmount))
                              }
                              else{
                                itm.totalPurchaseAmount=0.00;
                              }
                         }
                       else
                            {
                             if(index>=0){
                                //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                                itm.totalPurchaseAmount=Number(itm.totalPurchaseQty*self._lastPrice[index].lastPrice )
                              }
                              else{
                                itm.totalPurchaseAmount=0.00;
                              }
                            }
                        

                  

                       if(itm.wastageAmt==undefined || itm.wastageAmt==0){
                    
                        if(index>=0){
                          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                          itm.wastageAmt=Number( itm.wastage *self._lastPrice[index].lastPrice)
                        }
                        else{
                          itm.wastageAmt=0.00;
                        }
                      }
                      else{
                        if(index>=0){
                          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                          itm.wastageAmt=Number( itm.wastage* self._lastPrice[index].lastPrice) 
                        }
                        else{
                          itm.wastageAmt=0.00;
                        } 
                      }

                          if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
                            if(index>=0){
                              //console.log("transfer");
                              //console.log(JSON.stringify(itm));
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            itm.totalTransferAmount= Number( itm.totalTransferQty*self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            itm.totalTransferAmount=Number( itm.totalTransferQty*self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount=0.00;
                            }
                          }
                        
                        
                          if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
                            if(index>=0){
                              //console.log("transfer to store");
                              //console.log(JSON.stringify(itm));
                              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalTransferAmount_toStore=Number(itm.totalTransferQty_toStore*self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount_toStore=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //console.log("transfer to store");
                              //console.log(JSON.stringify(itm));
                              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalTransferAmount_toStore= Number(itm.totalTransferQty_toStore* self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount_toStore=0.00;
                            }
                          }


                         
                          if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
                            if(index>=0){
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

                              itm.itemSaleAmt= Number(itm.itemSaleQty* self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.itemSaleAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

                              itm.itemSaleAmt= Number( itm.itemSaleQty*self._lastPrice[index].lastPrice);
                            }
                            else
                            {
                              itm.itemSaleAmt=0.00;
                            }
                          }
                        
                         
                          if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
                            if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalIntermediateAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalIntermediateAmt=0.00;
                            }
                          }
                        
                         
                          //console.log('price', itm.itemName, itm.itemId, index);
                          if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
                            if(index>=0){
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReturnAmount= Number( itm.totalStockReturnQty*self._lastPrice[index].lastPrice)

                              //console.log(itm, $scope.lastPrice[index]);
                            }
                            else
                            {
                              itm.totalStockReturnAmount=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReturnAmount= Number(  itm.totalStockReturnQty*self._lastPrice[index].lastPrice)

                              //console.log(itm, $scope.lastPrice[index]);
                            }
                            else
                            {
                              itm.totalStockReturnAmount=0.00;
                            }
                          }
                        
                         
                          if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
                            if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReceiveAmount=Number( itm.totalStockReceiveQty* self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalStockReceiveAmount=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReceiveAmount=Number(  itm.totalStockReceiveQty * self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                             itm.totalStockReceiveAmount=0.00;
                            }
                          }


                    }

                        
                        

                      });
                      _.forEach(result.betweenDate,function(itm,i){
                                 var index=_.findIndex(self._lastPrice,{itemId:itm.itemId});
                        //if(itm.itemName=="Chicken Burger Tikki")
                          //  console.log("itm price",itm)
                        //console.log(itm.itemName, itm.totalPurchaseQty, $scope.lastPrice[index].monthAverage, itm.ConversionFactor, itm.UnitName);
                        //if(index>=0){
                         
                        if(itm.totalOpeningQty!=undefined){

                          //console.log("price",self._lastPrice[index].monthAverage)
                          if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
                            if(index>=0){
                              //console.log("opening index found");
                              itm.totalOpeningAmt=Number( itm.totalOpeningQty *self._lastPrice[index].lastPrice )
                              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            }
                            else
                            {
                              //console.log("opening index NOT found");
                              itm.totalOpeningAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                                     //console.log("opening index found");
                              itm.totalOpeningAmt= Number(  itm.totalOpeningQty*self._lastPrice[index].lastPrice )
                              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            }
                            else
                            {
                                //console.log("opening index NOT found");
                              itm.totalOpeningAmt=0.00;
                            }
                          }
                        }
                        else if(itm.totalBalanceQty!=undefined){
                          if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
                            if(index>=0){
                              itm.totalBalanceAmt=Number( itm.totalBalanceQty*self._lastPrice[index].lastPrice );
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            else
                            {
                              itm.totalBalanceAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              itm.totalBalanceAmt= Number( itm.totalBalanceQty* self._lastPrice[index].lastPrice );
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
                              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
                            else
                            {
                              itm.totalBalanceAmt=0.00;
                            }
                          }
                        }

                       else if(itm.totalStockReturnQty != undefined && itm.totalStockReceiveQty != undefined && itm.itemSaleQty != undefined
                        && itm.totalTransferQty!=undefined  && itm.totalTransferQty_toStore!=undefined && itm.totalSaleQty!=undefined && itm.totalIntermediateQty!= undefined 
                       &&    itm.totalPurchaseQty!=undefined && itm.wastage != undefined 
                        ) 
                     {

                      if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
                          if(index>=0){
                                //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                                itm.totalPurchaseAmount=Number(  itm.totalPurchaseQty *self._lastPrice[index].lastPrice)
                                //console.log("pur amt");
                                //console.log(JSON.stringify(itm.totalPurchaseAmount))
                              }
                              else{
                                itm.totalPurchaseAmount=0.00;
                              }
                         }
                       else
                            {
                             if(index>=0){
                                //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                                itm.totalPurchaseAmount=Number(itm.totalPurchaseQty*self._lastPrice[index].lastPrice )
                              }
                              else{
                                itm.totalPurchaseAmount=0.00;
                              }
                            }
                        

                       if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
                            if(index>=0){
                              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              //console.log(itm.totalSaleQty,self._lastPrice[index].monthAverage);

                              itm.totalSaleAmount=Number( itm.totalSaleQty *self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalSaleAmount=0.00;
                            }
                          }
                          else
                          {
                            if(index>=0){
                              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                             // console.log(itm.totalSaleQty,self._lastPrice[index].monthAverage);

                              itm.totalSaleAmount=Number( itm.totalSaleQty * self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalSaleAmount=0.00;
                            }
                          }

                       if(itm.wastageAmt==undefined || itm.wastageAmt==0){
                    
                        if(index>=0){
                          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                          itm.wastageAmt=Number( itm.wastage *self._lastPrice[index].lastPrice)
                        }
                        else{
                          itm.wastageAmt=0.00;
                        }
                      }
                      else{
                        if(index>=0){
                          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
                          itm.wastageAmt=Number( itm.wastage* self._lastPrice[index].lastPrice) 
                        }
                        else{
                          itm.wastageAmt=0.00;
                        } 
                      }

                          if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
                            if(index>=0){
                              //console.log("transfer");
                              //console.log(JSON.stringify(itm));
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            itm.totalTransferAmount= Number( itm.totalTransferQty*self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
                            itm.totalTransferAmount=Number( itm.totalTransferQty*self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount=0.00;
                            }
                          }
                        
                        
                          if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
                            if(index>=0){
                              //console.log("transfer to store");
                              //console.log(JSON.stringify(itm));
                              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalTransferAmount_toStore=Number(itm.totalTransferQty_toStore*self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount_toStore=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //console.log("transfer to store");
                              //console.log(JSON.stringify(itm));
                              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalTransferAmount_toStore= Number(itm.totalTransferQty_toStore* self._lastPrice[index].lastPrice)

                            }
                            else
                            {
                              itm.totalTransferAmount_toStore=0.00;
                            }
                          }


                         
                          if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
                            if(index>=0){
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

                              itm.itemSaleAmt= Number(itm.itemSaleQty* self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.itemSaleAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

                              itm.itemSaleAmt= Number( itm.itemSaleQty*self._lastPrice[index].lastPrice);
                            }
                            else
                            {
                              itm.itemSaleAmt=0.00;
                            }
                          }
                        
                         
                          if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
                            if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalIntermediateAmt=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalIntermediateAmt= Number( itm.totalIntermediateQty*self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalIntermediateAmt=0.00;
                            }
                          }
                        
                         
                          //console.log('price', itm.itemName, itm.itemId, index);
                          if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
                            if(index>=0){
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReturnAmount= Number( itm.totalStockReturnQty*self._lastPrice[index].lastPrice)

                              //console.log(itm, $scope.lastPrice[index]);
                            }
                            else
                            {
                              itm.totalStockReturnAmount=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReturnAmount= Number(  itm.totalStockReturnQty*self._lastPrice[index].lastPrice)

                              //console.log(itm, $scope.lastPrice[index]);
                            }
                            else
                            {
                              itm.totalStockReturnAmount=0.00;
                            }
                          }
                        
                         
                          if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
                            if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReceiveAmount=Number( itm.totalStockReceiveQty* self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                              itm.totalStockReceiveAmount=0.00;
                            }
                          }
                          else
                          {
                           if(index>=0){
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
                              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
                              itm.totalStockReceiveAmount=Number(  itm.totalStockReceiveQty * self._lastPrice[index].lastPrice)
                            }
                            else
                            {
                             itm.totalStockReceiveAmount=0.00;
                            }
                          }




                    }

                      });
                      //console.log("price final ");
                      //console.log(JSON.stringify(result));
                      return result;
    },


      calculatePrice :function (result){
        var self = this;
        //console.log("price");
      //console.log(result);
      _.forEach(result.beforeDate,function(itm,i){
        //console.log(JSON.stringify(itm));
      //  console.log("price",self._lastPrice)
        var index=_.findIndex(self._lastPrice,{itemId:itm.itemId});
        //if(itm.itemName=="Chicken Burger Tikki")
          //  console.log("itm price",itm)
        //console.log(itm.itemName, itm.totalPurchaseQty, $scope.lastPrice[index].monthAverage, itm.ConversionFactor, itm.UnitName);
        //if(index>=0){
        if(itm.totalOpeningQty!=undefined){
          //console.log("price",self._lastPrice[index].monthAverage)
          if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
            if(index>=0){
              //console.log("opening index found");
              itm.totalOpeningAmt=parseFloat( parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[index].monthAverage) ).toFixed(3);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            }
            else
            {
              //console.log("opening index NOT found");
              itm.totalOpeningAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
                     //console.log("opening index found");
              itm.totalOpeningAmt= parseFloat( parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[index].monthAverage) ).toFixed(3);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            }
            else
            {
                //console.log("opening index NOT found");
              itm.totalOpeningAmt=0.00;
            }
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
            if(index>=0){ 
              itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[index].monthAverage);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            else
            {
              itm.totalBalanceAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[index].monthAverage);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            else
            {
              itm.totalBalanceAmt=0.00;
            }
          }
        }
        else if(itm.totalPurchaseQty!=undefined){
          // if(itm.totalPurchaseAmount==undefined){
          //   itm.totalPurchaseAmount=0.00;
          // }
          if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
          //console.log("purchasee before")
          //console.log(itm.stringify());
          if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseAmount) * parseFloat(itm.ConversionFactor)).toFixed(2);
            //console.log("pur amt");
            //console.log(JSON.stringify(itm.totalPurchaseAmount))
          }
          else{
            itm.totalPurchaseAmount=0.00;
          }
        }
        else
        {
         if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseAmount) * parseFloat(itm.ConversionFactor)).toFixed(2);
          }
          else{
            itm.totalPurchaseAmount=0.00;
          }
        }
        }
        else if(itm.totalSaleQty!=undefined){
        if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.totalSaleQty,self._lastPrice[index].monthAverage);

              itm.totalSaleAmount=parseFloat(itm.totalSaleAmount);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            }
          }
          else
          {
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
             // console.log(itm.totalSaleQty,self._lastPrice[index].monthAverage);

              itm.totalSaleAmount=parseFloat(itm.totalSaleAmount);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            }
          }
        }
        else if(itm.totalWastageQty!=undefined){
          if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
            if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalWastageAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalWastageAmt=0.00;
            }
          }
        }
        else if(itm.totalTransferQty!=undefined){
          if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
            if(index>=0){
              //console.log("transfer");
              //console.log(JSON.stringify(itm));
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
            itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalTransferAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
            itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalTransferAmount=0.00;
            }
          }
        }
        else if(itm.totalTransferQty_toStore!=undefined){
          if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
            if(index>=0){
              //console.log("transfer to store");
              //console.log(JSON.stringify(itm));
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            }
          }
          else
          {
           if(index>=0){
              //console.log("transfer to store");
              //console.log(JSON.stringify(itm));
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            }
          }
        }
        else if(itm.itemSaleQty!=undefined){
          if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
            if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage);
              if(!isYieldEnabled)
                itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage)).toFixed(2);
              else {
                  var itemIndex = _.findIndex(self._stockItems, function(item) {
                  return itm.itemId == item._id;
                });
                if(itemIndex > 0)
                {
                  if(self._stockItems[itemIndex].yeild) {
                    var price = 0,yieldAdjustedPrice = 0;;
                    price=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage)).toFixed(2);
                    yieldAdjustedPrice = ((200 - self._stockItems[itemIndex].yeild)  * 0.01 * price);
                    itm.itemSaleAmt=parseFloat(yieldAdjustedPrice).toFixed(2);
                  }
                }
                else
                {
                 itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage)).toFixed(2); 
                }
              }
            }
            else
            {
              itm.itemSaleAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

              itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.itemSaleAmt=0.00;
            }
          }
        }
        else if(itm.totalIntermediateQty!=undefined){
          if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            }
          }
        }
        else if(itm.totalStockReturnQty != undefined) {
          //console.log('price', itm.itemName, itm.itemId, index);
          if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
            if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(itm.totalStockReturnQty)*parseFloat(self._lastPrice[index].monthAverage);

              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(itm.totalStockReturnQty)*parseFloat(self._lastPrice[index].monthAverage);

              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            }
          }
        }
        else if(itm.totalStockReceiveQty!=undefined){
          if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(itm.totalStockReceiveQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalStockReceiveAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(itm.totalStockReceiveQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
             itm.totalStockReceiveAmount=0.00;
            }
          }
        }

      });
      _.forEach(result.betweenDate,function(itm,i){
        //console.log(JSON.stringify(itm));
        var index=_.findIndex(self._lastPrice,{itemId:itm.itemId});

        //if(index>=0){
        if(itm.totalOpeningQty!=undefined){
          if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
            if(index>=0){
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalOpeningAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalOpeningAmt=0.00;
            }
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
            if(index>=0){
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalBalanceAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalBalanceAmt=0.00;
            }
          }
        }
        else if(itm.totalPurchaseQty!=undefined){
         /* if(itm.totalPurchaseAmount==undefined){
            itm.totalPurchaseAmount=0.00;
          }*/
           //console.log("purchasee between")
           //console.log(JSON.stringify(itm));
      if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
          if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseAmount) * parseFloat(itm.ConversionFactor)).toFixed(2);
            //console.log("pur amt");
            //console.log(JSON.stringify(itm.totalPurchaseAmount))
          }
          else{
            itm.totalPurchaseAmount=0.00;
          }
        }
        else
        {
         if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseAmount) * parseFloat(itm.ConversionFactor)).toFixed(2);
            //console.log("pur amt");
            //console.log(JSON.stringify(itm.totalPurchaseAmount))
          }
          else{
            itm.totalPurchaseAmount=0.00;
          }
        }
        }
        else if(itm.totalSaleQty!=undefined){
          if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
            //console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);

              itm.totalSaleAmount=parseFloat(itm.totalSaleAmount);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
           // console.log(itm.totalSaleQty,self._lastPrice[index].monthAverage);

              itm.totalSaleAmount=parseFloat(itm.totalSaleAmount);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            }
          }
        }
        else if(itm.totalWastageQty!=undefined){
          if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
            if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalWastageAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalWastageAmt=0.00;
            }
          }
        }
        else if(itm.totalTransferQty!=undefined){
          if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
            if(index>=0){
              //console.log("transfer");
              //console.log(JSON.stringify(itm));
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalTransferAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
            //console.log("transfer");
              //console.log(JSON.stringify(itm));
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalTransferAmount=0.00;
            }
          }
        }
        else if(itm.totalTransferQty_toStore!=undefined){
          if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
            if(index>=0){
              //console.log("transfer to store");
              //console.log(JSON.stringify(itm));
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            }
          }
          else
          {
           if(index>=0){
              //console.log("transfer to store");
              //console.log(JSON.stringify(itm));
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(self._lastPrice[index].monthAverage);

            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            }
          }
        }
        else if(itm.itemSaleQty!=undefined){
        //  console.log('price', itm.itemName, itm.itemId, index);
          if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
            if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage);
              //console.log(itm, $scope.lastPrice[index]);
              if(!isYieldEnabled)
                itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage)).toFixed(2);
              else {
                  var itemIndex = _.findIndex(self._stockItems, function(item) {
                  return itm.itemId == item._id;
                });
                if(itemIndex > 0)
                {
                  if(self._stockItems[itemIndex].yeild) {
                    var price = 0,yieldAdjustedPrice = 0;;
                    price=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage)).toFixed(2);
                    yieldAdjustedPrice = ((200 - self._stockItems[itemIndex].yeild)  * 0.01 * price);
                    itm.itemSaleAmt=parseFloat(yieldAdjustedPrice).toFixed(2);
                  }
                }
                else
                {
                 itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage)).toFixed(2); 
                }
              }
            }
            else
            {
              itm.itemSaleAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);

              itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(self._lastPrice[index].monthAverage);
              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.itemSaleAmt=0.00;
            }
          }
        }
        else if(itm.totalStockReturnQty != undefined) {
          //console.log('price', itm.itemName, itm.itemId, index);
          if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
            if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(itm.totalStockReturnQty)*parseFloat(self._lastPrice[index].monthAverage);

              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(itm.totalStockReturnQty)*parseFloat(self._lastPrice[index].monthAverage);

              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            }
          }
        }
        else if(itm.totalIntermediateQty!=undefined){
          if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            }
          }
        }
        else if(itm.totalStockReceiveQty!=undefined){
          if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(itm.totalStockReceiveQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalStockReceiveAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(itm.totalStockReceiveQty)*parseFloat(self._lastPrice[index].monthAverage);
            }
            else
            {
              itm.totalStockReceiveAmount=0.00;
            }
          }
        }

      });
      //console.log("price final ");
      //console.log(JSON.stringify(result));
      return result;
    },




      getVarianceDateWiseReportAggregated: function (req) {
          var d = $q.defer();
          var self = this;
          //stockReportResetTime.getConsumptionSummary(req, function (result) {
          //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result) {
          stockAggregateService.getData(req, function (result){
            if(result.errorMessage){
              d.reject(result.errorMessage);
            } else {
              self._data = result;
              //console.log(JSON.stringify(result.beforeDate));
              result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              console.log('result before',result);
              result = self.spliceAllItemsBeforePhysicalDateWise();
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
              // result.beforeDate.sort(function (a, b) {
              //   if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
              //     return new Date(a.created) - new Date(b.created);
              //   else
              //     return b.num - a.num;
              // });
              console.log('result',result);
              result = self.formatDataForReport_DateWiseAggregated();
              result = self.spliceForCategoryAndItem();
              console.log("--------------------------");
              console.log(result);
              var res = self.convertItemInPreferredUnit_DateWise(result);

              //var res=self.convertItemInPreferredUnit();
              var days = self.getDates();
              //console.log(days);
              var gtR = self.generateDateWiseVarianceReport(res.items, days);

              var index = gtR.length - 1;
              gtR.splice(index, 1);
              d.resolve(gtR);

              
              //d.resolve(result);
            }
          });
          return d.promise;
        }
      };

      return ( StockReportService );
    });
