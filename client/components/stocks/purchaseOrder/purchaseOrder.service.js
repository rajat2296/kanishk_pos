'use strict';

angular.module('posistApp')
  .service('purchaseOrder', ['$resource', function ($resource) {
    return $resource('/api/purchaseOrders/:controller/:id',
      {
        id: '@_id'
      },
      {
        getAll: {method: 'GET', isArray: true},
        get: {method: 'GET', isArray: false},
        getLastPurchaseOrderNumber: {method: 'GET', params: {controller: 'getLastPurchaseOrderNumber'}, isArray: false},
        findOne: {method: 'GET', params: {controller: 'findOne'}, isArray: false},
        update: {method: 'PUT', isArray: false}
      }
    );
  }]);
