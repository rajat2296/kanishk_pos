'use strict';

describe('Service: purchaseOrder', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var purchaseOrder;
  beforeEach(inject(function (_purchaseOrder_) {
    purchaseOrder = _purchaseOrder_;
  }));

  it('should do something', function () {
    expect(!!purchaseOrder).toBe(true);
  });

});
