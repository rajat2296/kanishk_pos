'use strict';

describe('Service: VendorPaymentHistory', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var VendorPaymentHistory;
  beforeEach(inject(function (_VendorPaymentHistory_) {
    VendorPaymentHistory = _VendorPaymentHistory_;
  }));

  it('should do something', function () {
    expect(!!VendorPaymentHistory).toBe(true);
  });

});
