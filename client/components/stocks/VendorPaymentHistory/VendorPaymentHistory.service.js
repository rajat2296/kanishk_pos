
'use strict';

angular.module('posistApp')
  .factory('VendorPaymentHistory', function ($resource) {
    return $resource('/api/vendorPaymentHistorys/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray:false},
        get: {method: 'GET', isArray: true},
        find:{method: 'GET'},
        update:{method:'PUT'},
        getByMainTransactionId: {method: 'GET', params: {controller: "getByMainTransactionId"}, isArray: true},
        getTransactionHistoryByTransactionId: {method: 'GET', params: {controller: "getTransactionHistoryByTransactionId"}, isArray: true}
      }
    );
  });
