'use strict';

angular.module('posistApp')
  .factory('receiver', function ($resource) {
    return $resource('/api/receivers/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      }
    );
  });
