'use strict';

describe('Service: receiver', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var receiver;
  beforeEach(inject(function (_receiver_) {
    receiver = _receiver_;
  }));

  it('should do something', function () {
    expect(!!receiver).toBe(true);
  });

});
