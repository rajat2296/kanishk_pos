'use strict';

angular.module('posistApp')
  .service('demand', ['$resource', function ($resource) {
    return $resource('/api/demands/:id/:controller',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        getLastDemandBillNumber: {method: 'GET', params: {controller: 'getLastDemandBillNumber'}, isArray: false}
      }
    );
  }]);
