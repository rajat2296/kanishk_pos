'use strict';

describe('Service: demand', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var demand;
  beforeEach(inject(function (_demand_) {
    demand = _demand_;
  }));

  it('should do something', function () {
    expect(!!demand).toBe(true);
  });

});
