'use strict';

describe('Service: StockReportNew', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var StockReportNew;
  beforeEach(inject(function (_StockReportNew_) {
    StockReportNew = _StockReportNew_;
  }));

  it('should do something', function () {
    expect(!!StockReportNew).toBe(true);
  });

});
