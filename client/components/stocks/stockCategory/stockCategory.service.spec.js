'use strict';

describe('Service: stockCategory', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockCategory;
  beforeEach(inject(function (_stockCategory_) {
    stockCategory = _stockCategory_;
  }));

  it('should do something', function () {
    expect(!!stockCategory).toBe(true);
  });

});
