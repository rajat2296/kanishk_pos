'use strict';

angular.module('posistApp')
  .factory('stockCategory', function ($resource) {
    return $resource('/api/stockCategorys/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      }
    );
  });
