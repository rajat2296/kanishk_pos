'use strict';

angular.module('posistApp')
  .factory('baseKitchenUnit', function ($resource) {
    return $resource('/api/baseKitchenUnits/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      }
    );
  });