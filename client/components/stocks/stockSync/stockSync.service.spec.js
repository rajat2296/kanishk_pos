'use strict';

describe('Service: stockSync', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stockSync;
  beforeEach(inject(function (_stockSync_) {
    stockSync = _stockSync_;
  }));

  it('should do something', function () {
    expect(!!stockSync).toBe(true);
  });

});
