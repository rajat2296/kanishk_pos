'use strict';

angular.module('posistApp')
  .factory('stockSync', function ($q, $rootScope,localStorageService,growl,lzw,store,stockCategory,stockUnit,stockItem,vendor,receiver,Tenant,StockResource,Utils, $webSql) {
    return {
        InsertStores: function (t, count) {
          var d = $q.defer();
          var insertArr = [];
          for (var i = 0; i < t.length; ++i) {
            insertArr.push(
                $rootScope.db.insert('stores',
                    {
                      'storeId': t[i]._id,
                      'storeData': JSON.stringify(t[i])
                    })
                //$rootScope.db.executeQuery('insert into stores(itemId,itemData) values(?,?) ', [t[i]._id, JSON.stringify(t[i])])
            )
          }
          $q.all(insertArr).then(function (result) {
            console.log('inserted Stores');
            d.resolve("done");
          });
          return d.promise;
        },


        InsertReceivers: function (t, count) {
          var d = $q.defer();
          var insertArr = [];
          for (var i = 0; i < t.length; ++i) {
            insertArr.push(
                $rootScope.db.insert('receivers',
                    {
                      'receiverId': t[i]._id,
                      'receiverData': JSON.stringify(t[i])
                    })
            )
          }
          $q.all(insertArr).then(function (result) {
            console.log('inserted Receivers');
            d.resolve("done");
          })
          return d.promise;
        },

        InsertVendors: function (t, count) {
          var d = $q.defer();
          var insertArr = [];
          for (var i = 0; i < t.length; ++i) {
            insertArr.push(
                $rootScope.db.insert('vendors',
                    {
                      'vendorId': t[i]._id,
                      'vendorData': JSON.stringify(t[i])
                    })
            )
          }
          $q.all(insertArr).then(function (result) {
            console.log('inserted Vendors');
            d.resolve("done");
          })
          return d.promise;
        },

        InsertLastTransaction:function(){
            console.log('InsertLastTransaction');
              var d=$q.defer();
              $rootScope.db.selectDynamic('select count(*) as c from stocktransactions',[]).then(function(r){
                  if(r.rows.item(0).c===0){
                      StockResource.getLatestTransaction({tenantId:localStorageService.get('tenant_id') , deploymentId: localStorageService.get('deployment_id')}).$promise.then(function (bill) {
                          console.log(console.log(bill));
                        console.log(bill);
                        if(bill.length>0){
                        var ss=bill;
                        _.forEach(ss,function(s,i){
                          if(s._id!=undefined){
                            // $rootScope.db.insert('stockTransaction',
                            //   {
                            //     "transactionId":s.transactionId,
                            //     "transactionType":s.transactionType,
                            //     "user":"",
                            //     "store":"",
                            //     "toStore":"",
                            //     "category":"",
                            //     "daySerialNumber": s.daySerialNumber,
                            //     "transactionNumber":s.transactionNumber,
                            //     "tempTransactionNumber":s.tempTransactionNumber,
                            //     "vendor":"",
                            //     "receiver":"",
                            //     "cartage":0,
                            //     "discount":0,
                            //     "discountType":"",
                            //     "serviceCharge":0,
                            //     "serviceChargeType":"",
                            //     "vat":0,
                            //     "vatType":"",
                            //     "payment":0,
                            //     "heading":"",
                            //     "wastageItemType":'',
                            //     "wastageEntryType":'',
                            //     "created": new Date(s.created),
                            //     "items": '',
                            //     "templateName":"",
                            //     "deployment_id": (s.deployment_id != null) ? s.deployment_id : "null",
                            //     "tenant_id": s.tenant_id,
                            //     "isSynced" : true,
                            //     "syncedOn" : ''
                            //   }
                            // )
                            $rootScope.db.insert('stocktransactions',
                              {
                                "transactionId":s.transactionId,
                                "transactionType":s.transactionType,
                                "daySerialNumber": s.daySerialNumber,
                                "transactionNumber":s.transactionNumber,
                                "transactionData":angular.toJson(s),
                                "deployment_id": (s.deployment_id != null) ? s.deployment_id : "null",
                                "tenant_id": s.tenant_id,
                                "created": new Date(s.created),
                                "isOpen": false,
                                "isSynced": true,
                                "syncedOn": new Date(s.syncedOn)
                              }
                            ).then(function(){
                              //console.log("inserted transaction");
                                    d.resolve();
                            }, function (err) {
                                console.log(err);
                            });
                          }
                          // else{
                          //   d.resolve();
                          // }
                        });

                        }else
                        {
                          d.resolve();
                        }
                      });
                  }
                  else
                  {d.resolve();}
              });
              return d.promise;
          },
        DeleteDynamic: function (query) {

          var d = $q.defer();
          $rootScope.db.selectDynamic(query, []).then(function () {
            //console.log('deleted tab');
            d.resolve('done');
          });
          d.resolve('done');
          return d.promise;
        },


        getAll: function () {
        if($rootScope.db==undefined){
            $rootScope.db= $webSql.openDatabase('posistApp', '1.0', 'Posist Offline DB', 100000 * 1024 * 1024);
        }
          var d = $q.defer();
          $q.all([
            $rootScope.db.selectAll('stores'),
            $rootScope.db.selectAll('receivers'),
            $rootScope.db.selectAll('vendors')
          ]).then(function (alldata) {
            var storeData = alldata[0];
            var receiverData = alldata[1];
            var vendorData = alldata[2];

            var tempStores=[];
            for (var i = 0; i < storeData.rows.length; i++) {
              var t=(JSON.parse(storeData.rows.item(i).storeData));
              tempStores.push(JSON.parse(storeData.rows.item(i).storeData));
            };

            $rootScope.stores=tempStores;

            for (var i = 0; i < receiverData.rows.length; i++) {
              var itemDataJSON=JSON.parse(receiverData.rows.item(i).receiverData);
              $rootScope.receivers.push(itemDataJSON);
            };

            for (var i = 0; i < vendorData.rows.length; i++) {
              $rootScope.vendors.push(JSON.parse(vendorData.rows.item(i).vendorData));
            };

            var alldata = {stores: $rootScope.stores, receivers: $rootScope.receivers, vendors: $rootScope.vendors}
            d.resolve(alldata);
          });
          return d.promise;
        },

     syncAll: function () {
          var dsa = $q.defer();
          $rootScope.stores = [];
          $rootScope.vendors = [];
          $rootScope.receivers = [];
          $rootScope.users = [];
          var self = this;
          Tenant.getServerTime().$promise.then(function (time) {
            console.log("Server Time :"+time.serverTime);
            var currentSyncTime = time.serverTime;
            $rootScope.serverTime=time
            var params = {
              tenant_id: localStorageService.get('tenant_id'),
              deployment_id: localStorageService.get('deployment_id'),
              lastStoreSynced: localStorageService.get('lastStoreSynced'),
              currentSyncTime: currentSyncTime
            };

            $q.all([
              store.get(params).$promise,
              receiver.get(params).$promise,
              vendor.get(params).$promise
              //User.all(userParams).$promise
            ]).then(function (syncdata) {
              var storedata = syncdata[0];
              var receiverdata = syncdata[1];
              var vendordata = syncdata[2];
              var sids = [];
              var rids = [];
              var vids = [];

              _.forEach(storedata, function (_t) {
                sids.push("'" + _t._id + "'");
              })
              _.forEach(receiverdata, function (_t) {
                rids.push("'" + _t._id + "'");
              })
              _.forEach(vendordata, function (_t) {
                vids.push("'" + _t._id + "'");
              })
              var squery = 'delete from stores';// where storeId in (' + (sids.join(",")) + ');';
              var rquery = 'delete from receivers';// where receiverId in (' + (rids.join(",")) + ');';
              var vquery = 'delete from vendors';// where vendorId in (' + (vids.join(",")) + ');';
              console.log("deleted All");
              $q.all([
                self.DeleteDynamic(squery),
                self.DeleteDynamic(rquery),
                self.DeleteDynamic(vquery)
              ]).then(function () {
                $q.all([
                  self.InsertStores(storedata, 0),
                  self.InsertReceivers(receiverdata, 0),
                  self.InsertVendors(vendordata, 0)
                ]).then(function () {
                    self.InsertLastTransaction().then(function(){
                        //self.updateBillCountItems().then(function(){
                            //console.log("inserted");
                        self.getAll().then(function (data) {
                            //console.log(data);
                            // console.log(time);
                            localStorageService.set('lastStoreSynced', time.serverTime);
                            dsa.resolve(data);
                            });
                        //});
                    }).catch(function (errMessage) {
                        growl.error(errMessage, {ttl: 1000});
                        self.getAll().then(function (data) {
                            dsa.resolve(data);
                            console.log("done");
                        });
                    });
                }).catch(function (errMessage) {
                    growl.error(errMessage, {ttl: 1000});
                    self.getAll().then(function (data) {
                        dsa.resolve(data);
                        console.log("done");
                    });
                });
              }).catch(function (errMessage) {
                growl.error(errMessage, {ttl: 1000});
                self.getAll().then(function (data) {
                  dsa.resolve(data);
                  console.log("done");
                });
              });
            }).catch(function (errMessage) {
              growl.error(errMessage, {ttl: 1000});
              self.getAll().then(function (data) {
                dsa.resolve(data);
                console.log("done");
              });
            });
          }).catch(function (errMessage) {
            growl.error(errMessage, {ttl: 1000});
            self.getAll().then(function (data) {
              dsa.resolve(data);
              console.log("done");
            });
          });
          return dsa.promise;
        },

      syncStores: function (currentUser) {

                var d = $q.defer();
                var _lastSync = "";

                /* Get Partial Items */
                if (localStorageService.get('stores_lastSynced')) {

                    _lastSync = localStorageService.get('stores_lastSynced');
                    localStorageService.set('stores_lastSynced', new Date().toISOString());

                    $rootScope.stores = [];
                    store.get({
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: localStorageService.get('deployment_id'),
                        stores_lastSynced: _lastSync
                    }, function (stores) {

                        if (stores.length > 0) {

                            /* Got Differential Data*/
                            _.forEach(stores, function (i) {
                                $rootScope.db.del("stores", {"storeId": i._id});
                                $rootScope.db.insert('stores',
                                    {
                                        'storeId': i._id,
                                        'storeData': JSON.stringify(i)
                                    }
                                ).then(function (result) {
                                        console.log("Inserted stores");
                                    });

                            });

                            $rootScope.db.selectAll('stores').then(function (results) {
                                for (var i = 0; i < results.rows.length; i++) {
                                    $rootScope.stores.push(JSON.parse(results.rows.item(i).storeData));
                                }
                                growl.success('Stores Synced from Server.', {ttl: 3000});
                                d.resolve($rootScope.stores);
                            });

                        } else {

                            $rootScope.stores = [];
                            $rootScope.db.selectAll('stores').then(function (results) {
                                for (var i = 0; i < results.rows.length; i++) {
                                    $rootScope.stores.push(JSON.parse(results.rows.item(i).storeData));
                                }
                                growl.success('Stores Synced from Local, no differential data.', {ttl: 3000});
                                d.resolve($rootScope.stores);
                            });
                        }


                    }, function (err) {

                        /* Couldn't connect Online just get offline Items */
                        $rootScope.db.selectAll('stores').then(function (results) {
                            for (var i = 0; i < results.rows.length; i++) {
                                $rootScope.stores.push(JSON.parse(results.rows.item(i).storeData));
                            }
                            growl.success('Server Offline, Stores Synced from Local', {ttl: 3000});
                            d.resolve($rootScope.stores);
                        });

                    });


                } else {

                    /* Get All Items since lastSynced was not set */
                    _lastSync = localStorageService.get('stores_lastSynced');

                    store.get({
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: localStorageService.get('deployment_id'),
                        stores_lastSynced: _lastSync
                    }, function (stores) {
                        $rootScope.stores = [];
                        /*console.log(items);*/
                        _.forEach(stores, function (i) {
                            $rootScope.db.insert('stores',
                                {
                                    'storeId': i._id,
                                    'storeData': JSON.stringify(i)
                                }
                            ).then(function (result) {
                                    console.log(result);
                                    $rootScope.stores.push(i);
                                });
                        });

                        localStorageService.set('stores_lastSynced', new Date().toISOString());
                        growl.success('Stores Synced from Server', {ttl: 3000});
                        d.resolve($rootScope.stores);

                    }, function (err) {
                        growl.error('Error getting stores from Server', {ttl: 3000});
                    });

                }
                return d.promise;
      },

      syncReceivers: function (currentUser) {
                var d = $q.defer();
                var _lastSync = "";
                /* Get Partial Receiver */
                if (localStorageService.get('receivers_lastSynced')) {

                    _lastSync = localStorageService.get('receivers_lastSynced');
                    localStorageService.set('receivers_lastSynced', new Date().toISOString());

                    $rootScope.stores = [];
                    receiver.get({
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: localStorageService.get('deployment_id'),
                        receivers_lastSynced: _lastSync
                    }, function (receivers) {

                        if (receivers.length > 0) {

                            /* Got Differential Data*/
                            _.forEach(receivers, function (i) {
                                $rootScope.db.del("receivers", {"receiverId": i._id});
                                $rootScope.db.insert('receivers',
                                    {
                                        'receiverId': i._id,
                                        'receiverData': JSON.stringify(i)
                                    }
                                ).then(function (result) {
                                        console.log("Inserted receivers");
                                    });

                            });

                            $rootScope.db.selectAll('receivers').then(function (results) {
                                for (var i = 0; i < results.rows.length; i++) {
                                    $rootScope.receivers.push(JSON.parse(results.rows.item(i).receiverData));
                                }
                                growl.success('Receivers Synced from Server, got differential data.', {ttl: 3000});
                                d.resolve($rootScope.receivers);
                            });

                        } else {

                            $rootScope.receivers = [];
                            $rootScope.db.selectAll('receivers').then(function (results) {
                                for (var i = 0; i < results.rows.length; i++) {
                                    $rootScope.receivers.push(JSON.parse(results.rows.item(i).receiverData));
                                }
                                growl.success('Receiver Synced from Local, no differential data.', {ttl: 3000});
                                d.resolve($rootScope.receivers);
                            });
                        }


                    }, function (err) {

                        /* Couldn't connect Online just get offline Items */
                        $rootScope.db.selectAll('receivers').then(function (results) {
                            for (var i = 0; i < results.rows.length; i++) {
                                $rootScope.receivers.push(JSON.parse(results.rows.item(i).receiverData));
                            }
                            growl.success('Server Offline, Receivers Synced from Local', {ttl: 3000});
                            d.resolve($rootScope.receivers);
                        });

                    });


                } else {

                    /* Get All Items since lastSynced was not set */
                    _lastSync = localStorageService.get('receivers_lastSynced');

                    receiver.get({
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: localStorageService.get('deployment_id'),
                        receivers_lastSynced: _lastSync
                    }, function (receivers) {
                        $rootScope.receivers = [];
                        /*console.log(items);*/
                        _.forEach(receivers, function (i) {
                            $rootScope.db.insert('receivers',
                                {
                                    'receiverId': i._id,
                                    'receiverData': JSON.stringify(i)
                                }
                            ).then(function (result) {
                                    console.log(result);
                                    $rootScope.receivers.push(i);
                                });
                        });

                        localStorageService.set('receivers_lastSynced', new Date().toISOString());
                        growl.success('Receivers Synced from Server', {ttl: 3000});
                        d.resolve($rootScope.receivers);

                    }, function (err) {
                        growl.error('Error getting receivers from Server', {ttl: 3000});
                    });

                }
                return d.promise;
      },

      syncVendors: function (currentUser) {

                var d = $q.defer();
                var _lastSync = "";

                /* Get Partial Items */
                if (localStorageService.get('vendors_lastSynced')) {

                    _lastSync = localStorageService.get('vendors_lastSynced');
                    localStorageService.set('vendors_lastSynced', new Date().toISOString());

                    $rootScope.vendors = [];
                    vendor.get({
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: localStorageService.get('deployment_id'),
                        vendors_lastSynced: _lastSync
                    }, function (vendors) {

                        if (vendors.length > 0) {

                            /* Got Differential Data*/
                            _.forEach(vendors, function (i) {
                                $rootScope.db.del("vendors", {"vendorId": i._id});
                                $rootScope.db.insert('vendors',
                                    {
                                        'vendorId': i._id,
                                        'vendorData': JSON.stringify(i)
                                    }
                                ).then(function (result) {
                                        console.log("Inserted vendors");
                                    });

                            });

                            $rootScope.db.selectAll('vendors').then(function (results) {
                                for (var i = 0; i < results.rows.length; i++) {
                                    $rootScope.vendors.push(JSON.parse(results.rows.item(i).vendorData));
                                }
                                growl.success('Vendors Synced from Server, got differential data.', {ttl: 3000});
                                d.resolve($rootScope.vendors);
                            });

                        } else {

                            $rootScope.vendors = [];
                            $rootScope.db.selectAll('vendors').then(function (results) {
                                for (var i = 0; i < results.rows.length; i++) {
                                    $rootScope.vendors.push(JSON.parse(results.rows.item(i).vendorData));
                                }
                                growl.success('Vendors Synced from Local, no differential data.', {ttl: 3000});
                                d.resolve($rootScope.vendors);
                            });
                        }


                    }, function (err) {

                        /* Couldn't connect Online just get offline Items */
                        $rootScope.db.selectAll('vendors').then(function (results) {
                            for (var i = 0; i < results.rows.length; i++) {
                                $rootScope.vendors.push(JSON.parse(results.rows.item(i).vendorData));
                            }
                            growl.success('Vendors Offline, Items Synced from Local', {ttl: 3000});
                            d.resolve($rootScope.vendors);
                        });

                    });


                } else {

                    /* Get All Items since lastSynced was not set */
                    _lastSync = localStorageService.get('vendors_lastSynced');

                    vendor.get({
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: localStorageService.get('deployment_id'),
                        vendors_lastSynced: _lastSync
                    }, function (vendors) {
                        $rootScope.vendors = [];
                        /*console.log(items);*/
                        _.forEach(vendors, function (i) {
                            $rootScope.db.insert('vendors',
                                {
                                    'vendorId': i._id,
                                    'vendorData': JSON.stringify(i)
                                }
                            ).then(function (result) {
                                    console.log(result);
                                    $rootScope.vendors.push(i);
                                });
                        });

                        localStorageService.set('vendors_lastSynced', new Date().toISOString());
                        growl.success('Vendors Synced from Server', {ttl: 3000});
                        d.resolve($rootScope.vendors);

                    }, function (err) {
                        growl.error('Error getting stores from Server', {ttl: 3000});
                    });

                }
                return d.promise;
      },

      syncOfflineTransaction: function (currentUser) {
                var d = $q.defer();
                    var dep=localStorageService.get('deployment_id');
                    var ten=localStorageService.get('tenant_id');


                    //$rootScope.db.selectAll('stockTransaction')


                    $rootScope.db.select("stockTransaction", {
                        //"tenant_id": ten,
                        //"deployment_id": dep
                        "created": ""
                    })
                    .then(function (results) {
                        var _rows = [];
                        var offlineBills = [];
                        for (var i = 0; i < results.rows.length; i++) {
                            _rows.push(results.rows.item(i));
                        }
                        _.forEach(_rows, function (ob) {
                            if(ob.transactionType=="1" || ob.transactionType=="2" || ob.transactionType=="3" || ob.transactionType=="4" || ob.transactionType=="5" || ob.transactionType=="6" || ob.transactionType=="7"){
                              var _ob = {
                                transactionId:ob.transactionId,
                                transactionType:ob.transactionType,
                                transactionNumber:ob.transactionNumber,
                                user:JSON.parse(ob.user),
                                store:ob.store==""?"": JSON.parse(ob.store),
                                toStore:ob.toStore==""?"": JSON.parse(ob.toStore),
                                category:ob.category,//JSON.stringify($scope.stockEntryForm.availableCategory),
                                vendor:ob.vendor==""?"": JSON.parse(ob.vendor),
                                receiver:ob.receiver==""?"": JSON.parse(ob.receiver),
                                cartage:ob.cartage,
                                discount:parseFloat(ob.discount),
                                discountType:ob.discountType,
                                serviceCharge:parseFloat(ob.serviceCharge),
                                serviceChargeType:ob.serviceChargeType,
                                vat:parseFloat(ob.vat),
                                vatType:ob.vatType,
                                payment:ob.payment,
                                heading:ob.heading,
                                wastageItemType:ob.wastageItemType,
                                wastageEntryType:ob.wastageEntryType,
                                created:new Date(ob.created),
                                items: JSON.parse(ob.items),// JSON.stringify(this),
                                deployment_id: ob.deployment_id,
                                tenant_id: ob.tenant_id
                            };
                            offlineBills.push(_ob);
                            }
                        });
                        /*console.log("Offline Bills Got");*/

                        d.resolve(offlineBills);
                        growl.success("Offline transaction Synced!", {ttl: 3000});

                    }).catch(function (err) {
                        growl.error("Error Syncing Offline transaction", {ttl: 3000});
                        d.reject();
                    });

                return d.promise;

      },
      syncOfflineTemplates: function (currentUser) {
                var d = $q.defer();
                    $rootScope.db.select("stockTransaction", {
                        "deployment_id": localStorageService.get('deployment_id')
                    }).then(function (results) {
                        var _rows = [];
                        var offlineBills = [];
                        for (var i = 0; i < results.rows.length; i++) {
                            _rows.push(results.rows.item(i));
                        }
                        _.forEach(_rows, function (ob) {
                            if(ob.transactionType=="101" || ob.transactionType=="102" || ob.transactionType=="103" || ob.transactionType=="104" || ob.transactionType=="105"){
                              var _ob = {
                                  transactionId:ob.transactionId,
                                  transactionType:ob.transactionType,
                                  user:JSON.parse(ob.user),
                                  store:ob.store==""?"": JSON.parse(ob.store),
                                  toStore:ob.toStore==""?"": JSON.parse(ob.toStore),
                                  category:ob.category,//JSON.stringify($scope.stockEntryForm.availableCategory),
                                  vendor:ob.vendor==""?"": JSON.parse(ob.vendor),
                                  receiver:ob.receiver==""?"": JSON.parse(ob.receiver),
                                  cartage:ob.cartage,
                                  discount:parseFloat(ob.discount),
                                  discountType:ob.discountType,
                                  serviceCharge:parseFloat(ob.serviceCharge),
                                  serviceChargeType:ob.serviceChargeType,
                                  vat:parseFloat(ob.vat),
                                  vatType:ob.vatType,
                                  payment:ob.payment,
                                  heading:ob.heading,
                                  wastageItemType:ob.wastageItemType,
                                  wastageEntryType:ob.wastageEntryType,
                                  templateName:ob.templateName==""?"": ob.templateName,
                                  created:new Date(ob.created),
                                  items: JSON.parse(ob.items),// JSON.stringify(this),
                                  deployment_id: ob.deployment_id,
                                  tenant_id: ob.tenant_id
                              };
                              offlineBills.push(_ob);
                            }

                        });
                        /*console.log("Offline Bills Got");*/

                        d.resolve(offlineBills);
                        growl.success("templates Synced!", {ttl: 3000});

                    }).catch(function (err) {
                        growl.error("Error Syncing templates", {ttl: 3000});
                        d.reject();
                    });

                return d.promise;

      },
      syncTransactionOnline:function(currentUser){
        var d=$q.defer();
        var _tDetail=[];
        var _qAllTDetail=[];
        $rootScope.db.selectAll('stockTransaction').then(function (results) {
            if(results.isSynced!="" || results.isSynced!=undefined){
                var _rows = [];
                var offlineBills = [];
                        for (var i = 0; i < results.rows.length; i++) {
                            _rows.push(results.rows.item(i));
                        }
                        _.forEach(_rows, function (ob) {
                            var _ob = {

                                transactionId:ob.transactionId,
                                transactionType:ob.transactionType,
                                user:JSON.parse(ob.user),
                                store:ob.store==""?"": JSON.parse(ob.store),
                                toStore:ob.toStore==""?"": JSON.parse(ob.toStore),
                                category:ob.category,//JSON.stringify($scope.stockEntryForm.availableCategory),
                                vendor:ob.vendor==""?"": JSON.parse(ob.vendor),
                                receiver:ob.receiver==""?"": JSON.parse(ob.receiver),
                                cartage:ob.cartage,
                                discount:parseFloat(ob.discount),
                                discountType:ob.discountType,
                                serviceCharge:parseFloat(ob.serviceCharge),
                                serviceChargeType:ob.serviceChargeType,
                                vat:parseFloat(ob.vat),
                                vatType:ob.vatType,
                                payment:ob.payment,
                                heading:ob.heading,
                                wastageItemType:ob.wastageItemType,
                                wastageEntryType:ob.wastageEntryType,
                                templateName:ob.templateName==""?"": ob.templateName,
                                created:new Date(ob.created),
                                items: JSON.parse(ob.items),// JSON.stringify(this),
                                deployment_id: ob.deployment_id,
                                tenant_id: ob.tenant_id
                            };
                            offlineBills.push(_ob);
                        });
                        d.resolve(offlineBills);
                        growl.success("templates Synced!", {ttl: 3000});

                    }
            }).catch(function (err) {
                        growl.error("Error Syncing templates", {ttl: 3000});
                        d.reject();
                    });

                return d.promise;
        }

      }
  });
