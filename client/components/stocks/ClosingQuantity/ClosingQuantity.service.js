'use strict';

angular.module('posistApp')
    .factory('ClosingQuantity', function ($q, $resource, $timeout, $http, $rootScope, growl,localStorageService,StockReportNew,baseKitchenItem,property) {
        // Define the constructor function.
        function ClosingQuantity(data,startDate,endDate,deployment_id,tenant_id) {
            var self = this;
            self.startDate=startDate || null;
            self.endDate=endDate || null;
            self._data=data || null;
            self.deployment_id=deployment_id || localStorageService("deployment_id");
            self.tenant_id=tenant_id||localStorageService("tenant_id");
            //self.isPhysicalStock = false;
            


            
        };

        ClosingQuantity.prototype = {
            mergeData:function(result,resultIntermediate,resultProcessed){
                _.forEach(resultIntermediate,function(inter,i){
                    var index=_.findIndex(result,{storeId:inter.storeId});
                    if(index>=0){
                        _.forEach(inter.beforeDate,function(itm,ii){
                            result[index].beforeDate.push(itm);
                        });
                        _.forEach(inter.betweenDate,function(itm,ii){
                            result[index].betweenDate.push(itm);
                        });
                    }
                });
                _.forEach(resultProcessed,function(inter,i){
                    var index=_.findIndex(result,{storeId:inter.storeId});
                    if(index>=0){
                        _.forEach(inter.beforeDate,function(itm,ii){
                            result[index].beforeDate.push(itm);
                        });
                        _.forEach(inter.betweenDate,function(itm,ii){
                            result[index].betweenDate.push(itm);
                        });
                    }
                });
                return result;
            },
            splicePhysicalGreaterThanStartDate:function(result){
                var self=this;
                _.forEach(result,function(r,i){
                    _.forEachRight(r.beforeDate,function(rb,ii){
                        if(rb.totalBalanceQty!=undefined){
                            var startDate=new Date(self.startDate);
                            var d=new Date(rb.created);
                            d.setDate(d.getDate() + 1);
                            d.setHours(0, 0, 0, 0);
                            rb.created=d;
                            if(d.getTime()>=startDate.getTime()){
                                result[i].beforeDate.splice(ii,1);
                            }
                        }
                    });
                });
                return result;
            },
            spliceAllItemsBeforePhysical:function (result){
                var self=this;
                var data=[];
                var latestPhysical = null;
                var latestPhysicalBetween = null;
                var i = 0;
                var j = 0;

                 _.forEach(result,function(rs,i){
                    rs.beforeDate.sort(function (a, b) {
                    return new Date(a.created) - new Date(b.created);
                    });
                    rs.betweenDate.sort(function (a, b) {
                    return new Date(a.created) - new Date(b.created);
                    });
                    //console.log('------------After Sorting in spliceAllItemsBeforePhysical--------------------------');
                    //console.log(result);
                //     _.forEach(r.beforeDate,function(rb,ii){
                //         if(rb.totalBalanceQty!=undefined){
                //             var d=new Date(rb.created);
                //             d.setDate(d.getDate() + 1);
                //             d.setHours(0, 0, 0, 0);
                //             rb.created=d;
                //             data.push(rb);
                //         };
                //     });
                //     // _.forEach(data,function(d,iii){
                //     //     _.forEachRight(r.beforeDate,function(itm,ii){
                //     //         if(itm.itemId==d.itemId){
                //     //             var phyDate=new Date(d.created);
                //     //             var compareDate=new Date(itm.created);
                //     //             if(phyDate.getTime()>compareDate.getTime()){
                //     //               result[i].beforeDate.splice(ii,1);
                //     //             }
                //     //         }
                //     //     });
                //     // });
                _.forEach(rs.beforeDate, function(r,i){
                    if(r.totalBalanceQty!=undefined){
                      /*if(r.itemName == "Manchow Chicken Soup(bk)"){
                        console.log('beforeDate in SAIBP',r.itemName, new Date(r.created), r.totalBalanceQty);
                      }*/
                      var d = new Date(r.created);
                      r.actualDate=new Date(d);
                      var a = new Date(d);
                      a.setHours(0,0,0,0);
                      a = new Date(a);
                      var b = new Date(d);
                      b = new Date(b.setHours(self.startDate.getHours(), self.startDate.getMinutes(), 0, 0));
                      if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

                      }else {
                        //console.log('splice all', d);
                        d.setDate(d.getDate() + 1);
                      }
                      d = new Date(d.setHours(self.startDate.getHours(), self.startDate.getMinutes(), 0, 0));
                      //d.setHours(0, 0, 0, 0);
                      r.created = new Date(d);
                      //r.created = new Date(d);
              if(!latestPhysical){
                i = 0;
                latestPhysical = new Date(d);
              } else {
                if(latestPhysical.getTime() == d.getTime()){
                  i++;
                } else if(d.getTime() > latestPhysical.getTime()) {
                  i = 0;
                  latestPhysical = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = i;
              data.push(r);
                      //data.push(r);
                    };
                  });
                  _.forEach(data,function(d,i){
                    _.forEachRight(rs.beforeDate,function(itm,ii){
                      if(itm.itemId==d.itemId){
                        var phyDate=new Date(d.created);
                        var compareDate=new Date(itm.created);
                        if(phyDate.getTime()>compareDate.getTime()){
                          rs.beforeDate.splice(ii,1);
                        }
                      }
                    });
                  });

                  // var data2=[];
                  _.forEachRight(rs.betweenDate,function(r,i){
                    if(r.totalBalanceQty!=undefined){
                      //console.log(r.created);
                      /*if(r.itemName == "Manchow Chicken Soup(bk)"){
                        console.log('betweenDate in SAIBP',r.itemName, new Date(r.created), r.totalBalanceQty);
                      }*/
                      r.actualDate = r.created;
                      var d=new Date(r.created);
                      //var cdate=new Date($scope.purchaseConsumptionForm.toDate);
                      var a = new Date(d);
                      a.setHours(0,0,0,0);
                      a = new Date(a);
                      var b = new Date(d);
                      b = new Date(b.setHours(self.startDate.getHours(), self.startDate.getMinutes(), 0, 0));
                      //console.log(a)
                      //console.log(b)
                      //console.log(angular.copy(d))
                      if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

                      }else {
                        //console.log('splice all', d);
                        d.setDate(d.getDate() + 1);
                      }
                      //d.setDate(d.getDate() + 1);
                      d = new Date(d.setHours(self.startDate.getHours(), self.startDate.getMinutes(), 0, 0));
                      r.created = new Date(d);
                      r.actualDate = new Date(d);
                      if(!latestPhysicalBetween){
                j = 0;
                latestPhysicalBetween = new Date(d);
              } else {
                if(latestPhysicalBetween.getTime() == d.getTime()){
                  j++;
                } else if(d.getTime() > latestPhysicalBetween.getTime()) {
                  j = 0;
                  latestPhysicalBetween = new Date(d);
                }
              }
              //r.actualDate=new Date(d);
              r.num = j;
                      //console.log(r.created);
                      // if(cdate.getTime()>d.getTime()){
                      //   data2.push(r);
                      // }
                      // else
                      // {
                      //   result.betweenDate.splice(i,1);
                      // }
                    };
                  });
                });
//console.log(angular.copy(result));
                return result;
            },
            storeWiseItems: function(result){
                
                _.forEach(result,function(r,i){
                    r.items=[];
                    r.items2=[];
                    /*r.beforeDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                    r.betweenDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                    */
                    _.forEach(r.beforeDate,function(b,ii){
                        //if(!b.totalBalanceQty)
                        r.items.push(b);
                        //else
                        //r.items2.push(b);
                    });
                    _.forEach(r.betweenDate,function(b,ii){
                        //if(!b.totalBalanceQty)
                        r.items.push(b);
                        //else
                        //r.items2.push(b);
                    });
                    //console.log(angular.copy(r.items));
                    /*r.beforeDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                    r.betweenDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                    */if(r.items!=undefined){

                        r.items.sort(function(a,b){
                            if(new Date(a.created).getTime() != new Date(b.created).getTime())
                                return new Date(b.created) - new Date(a.created);
                            else return b.num - a.num;
                        });
                        /*console.log('------------After Sorting in storeWiseItems when r.items!=undefined--------------------------');
                        console.log(angular.copy(r.items));*/
                    }
                    /*if(r.items2!=undefined){

                        r.items2.sort(function(a,b){return new Date(b.actualDate) - new Date(a.actualDate);});
                        console.log('------------After Sorting in storeWiseItems when r.items2!=undefined--------------------------');
                        console.log(r.items2);
                        _.merge(r.items,r.items2);
                    }*/
                });
                return result;
            },
            getStorewiseUniqueItems: function(str){
                var self=this;
                var deferred=$q.defer();
                var data=[];
                //baseKitchenItem.get({tenant_id:self.tenant_id,deployment_id:self.deployment_id}).$promise.then(function (items){
                _.forEach(str,function(s,i){
                    _.forEach(s.items,function(itm,ii){
                        var index=_.findIndex(data,{storeId:s.storeId,itemId:itm.itemId});
                        if(index<0){
                            var obj={storeId:s.storeId,storeName:s.storeName,itemId:itm.itemId,itemName:itm.itemName,unitName:itm.UnitName};
                            data.push(obj);
                        }
                    });
                    // _.forEach(items,function(itm,ii){
                    //   var index=_.findIndex(data,{storeId:s.storeId,itemId:itm.itemId});
                    //     if(index<0){
                    //         var obj={storeId:s.storeId,storeName:s.storeName,itemId:itm.itemId,itemName:itm.itemName,unitName:itm.UnitName};
                    //         data.push(obj);
                    //     }
                    // })
                });
                //data.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                deferred.resolve(data);
                //});
                return deferred.promise;
                //return data;
            },
            getClosingQtyStoreWise:function (items,str){
                var self=this;
                //items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                _.forEach(items,function(it,i){
                    _.forEach(str,function(s,ii){
                        _.forEachRight(s.items,function(itm,iii){
                            /*if(itm.itemName == "Rose Syrup (O)"){
                                            console.log('Rose Syrup (O)', itm);
                                        }*/
                            if(s.storeId==it.storeId && itm.itemId==it.itemId){
                                
                                if(itm.totalOpeningQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalOpeningQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalBalanceQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                        var cDate=new Date(itm.created);
                                        cDate = new Date(cDate.setHours(self.startDate.getHours(),self.startDate.getMinutes(),0,0));
                                        /*if(itm.itemName == "Manchow Chicken Soup(bk)"){
                                            console.log('totalBalanceQty in Manchow Chicken Soup(bk)', itm.itemName, cDate, itm.totalBalanceQty);
                                        }*/
                                        cDate.setDate(cDate.getDate() - 1);
                                        var td=new Date();
                                        td = new Date(td.setHours(self.startDate.getHours(),self.startDate.getMinutes(),0,0));
                                        if(td.getTime()==cDate.getTime()){
                                            //console.log('---------------------Inside Match --------------------------------------')
                                        }
                                        else
                                        {
                                            if(it.unitName=="Litre" || it.unitName=="Meter"){
                                                it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
                                            }
                                            else
                                            {
                                                it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        var cDate=new Date(itm.created);
                                        cDate = new Date(cDate.setHours(self.startDate.getHours(),self.startDate.getMinutes(),0,0));
                                        /*if(itm.itemName == "Manchow Chicken Soup(bk)"){
                                            console.log('totalBalanceQty in Manchow Chicken Soup(bk)', itm.itemName, cDate, itm.totalBalanceQty);
                                        }*/
                                        cDate.setDate(cDate.getDate() - 1);
                                        var td=new Date();
                                        td = new Date(td.setHours(self.startDate.getHours(),self.startDate.getMinutes(),0,0));
                                        if(td.getTime()==cDate.getTime()){ 

                                        }
                                        else
                                        {
                                            if(it.unitName=="Litre" || it.unitName=="Meter"){
                                                it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
                                            }
                                            else
                                            {
                                                it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                            }
                                        }
                                    }
                                }
                                else if(itm.totalPurchaseQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                                        }

                                    }
                                }
                                else if(itm.totalReturnAcceptQty!=undefined){
                                  if(it.closingQtyInBase==undefined){
                                    //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      it.closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(itm.totalReturnAcceptQty).toFixed(3);
                                    }
                                  }
                                  else
                                  {
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
                                    }

                                  }
                                }
                                else if(itm.totalSaleQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalSaleQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalSaleQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalSaleQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalWastageQty!=undefined){
                                    /*if(itm.itemName == "Manchow Chicken Soup(bk)"){
                                            console.log('totalWastageQty in Manchow Chicken Soup(bk)', itm.itemName, itm.totalWastageQty);
                                        }*/
                                    if(it.closingQtyInBase==undefined){
                                        
                                        //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalWastageQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalWastageQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalWastageQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalReturnWastageQty!=undefined){
                                  if(it.closingQtyInBase==undefined){
                                    //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      it.closingQtyInBase=parseFloat(parseFloat(-itm.totalReturnWastageQty)*1000).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(-itm.totalReturnWastageQty).toFixed(3);
                                    }
                                  }
                                  else
                                  {
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnWastageQty)*1000).toFixed(3);
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalReturnWastageQty)).toFixed(3);
                                    }
                                  }
                                }
                                else if(itm.totalTransferQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalTransferQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalTransferQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalTransferQty_toStore!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.itemSaleQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.itemSaleQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.itemSaleQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.itemSaleQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalStockReceiveQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalIntermediateQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalIntermediateQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalIntermediateQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalStockReturnQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalStockReturnQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReturnQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalIndentingChallanQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalIndentingChallanQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalIndentingChallanQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                                        }
                                    }
                                }
                            }
                        });
                    });
                });
                return items;
            },
            getClosingQtyStoreWiseViaPhysical:function (items,str){
                var self=this;
                //items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                _.forEach(items,function(it,i){
                    _.forEach(str,function(s,ii){
                        _.forEachRight(s.items,function(itm,iii){
                            /*if(itm.itemName == "Rose Syrup (O)"){
                                            console.log('Rose Syrup (O)', itm);
                                        }*/
                            if(s.storeId==it.storeId && itm.itemId==it.itemId){
                                
                                if(itm.totalOpeningQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalOpeningQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalBalanceQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
          
                                            if(it.unitName=="Litre" || it.unitName=="Meter"){
                                                it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
                                            }
                                            else
                                            {
                                                it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                            }
                                        

                                    }
                                    else
                                    {
                                        
                                            if(it.unitName=="Litre" || it.unitName=="Meter"){
                                                it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
                                            }
                                            else
                                            {
                                                it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                            }
                                        
                                    }
                                }
                                else if(itm.totalPurchaseQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                                        }

                                    }
                                }
                                /*else if(itm.totalReturnAcceptQty!=undefined){
                                  if(it.closingQtyInBase==undefined){
                                    //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      it.closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(itm.totalReturnAcceptQty).toFixed(3);
                                    }
                                  }
                                  else
                                  {
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
                                    }

                                  }
                                }*/
                                /*else if(itm.totalSaleQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalSaleQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalSaleQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalSaleQty)).toFixed(3);
                                        }
                                    }
                                }*/
                                /*else if(itm.totalWastageQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalWastageQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalWastageQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalWastageQty)).toFixed(3);
                                        }
                                    }
                                }*/
                                /*else if(itm.totalReturnWastageQty!=undefined){
                                  if(it.closingQtyInBase==undefined){
                                    //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      it.closingQtyInBase=parseFloat(parseFloat(-itm.totalReturnWastageQty)*1000).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(-itm.totalReturnWastageQty).toFixed(3);
                                    }
                                  }
                                  else
                                  {
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnWastageQty)*1000).toFixed(3);
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalReturnWastageQty)).toFixed(3);
                                    }
                                  }
                                }*/
                                /*else if(itm.totalTransferQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalTransferQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalTransferQty)).toFixed(3);
                                        }
                                    }
                                }*/
                                else if(itm.totalTransferQty_toStore!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                                        }
                                    }
                                }
                                /*else if(itm.itemSaleQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.itemSaleQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.itemSaleQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.itemSaleQty)).toFixed(3);
                                        }
                                    }
                                }*/
                                else if(itm.totalStockReceiveQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                                        }
                                    }
                                }
                                /*else if(itm.totalIntermediateQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalIntermediateQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalIntermediateQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                                        }
                                    }
                                }*/
                                /*else if(itm.totalStockReturnQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalStockReturnQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReturnQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                                        }
                                    }
                                }*/
                            }
                        });
                    });
                });
                return items;
            },
            getClosingQty:function(){
                var self=this;
                var deferred = $q.defer();
                var req={fromDate:self.startDate,toDate:self.endDate,deployment_id:self.deployment_id,tenant_id:self.tenant_id};
                // StockReportNew.getClosingQty(req).$promise.then(function (result) {
                //     StockReportNew.getClosingQty_SemiProcessed(req).$promise.then(function (resultIntermediate) {
                //       StockReportNew.getClosingQty_ProcessedFood(req).$promise.then(function (resultProcessed){
                //         var data=self.mergeData(result,resultIntermediate,resultProcessed)
                //         //data=self.splicePhysicalGreaterThanStartDate(data);
                //         var data1=self.spliceAllItemsBeforePhysical(data);
                //         var res=self.storeWiseItems(data1);
                //         // var uItems=self.getStorewiseUniqueItems(res);
                //         // console.log(uItems);
                //         // var ret=self.getClosingQtyStoreWise(uItems,res);
                //         //console.log(ret);
                //         self.getStorewiseUniqueItems(res).then(function (items) {
                //           var uItems=items;
                //           console.log(uItems);
                //           var ret=self.getClosingQtyStoreWise(uItems,res);
                //           console.log(ret);
                //           deferred.resolve(ret);
                //         });
                //       });
                //     });
                // });
                /*self.getClosingQty_RawItems().then(function(rItems){
                    console.log("RawItems : ",rItems);
                    self.getClosingQty_IntermediateFood().then(function(sItems){
                        console.log("IntermediateItems : ",sItems);
                        self.getClosingQty_ProcessedFood().then(function(pItems){
                            console.log("MenuItems : ",pItems);
                            _.forEach(sItems,function(s){
                                rItems.push(s);
                            });
                            _.forEach(pItems,function(p){
                                rItems.push(p);
                            });

                            deferred.resolve(rItems);
                        });
                    });
                });*/


                $q.all([
                    self.getClosingQty_RawItems(),
                    self.getClosingQty_IntermediateFood(),
                    self.getClosingQty_ProcessedFood()
                    ]).then(function(result) {

                        var array = _.union(result[0], result[1], result[2]);
                        deferred.resolve(array);
                    });
                return deferred.promise;
            },
            getClosingQty_RawItems:function(){
                var self=this;
                var deferred = $q.defer();
               /* console.log('------------getClosingQty_RawItems--------------------------');                   
                console.log('self.startDate :-   ',self.startDate);
                console.log('self.endDate :-   ',self.endDate);

               */
               var isPhysicalStock =false;
               property.get({
               tenant_id: self.tenant_id,
               deployment_id: self.deployment_id
               }, function success(result) {
                if(result == '')
                  isPhysicalStock=false;
                else if(result[0].isPhysicalStock == undefined)
                  isPhysicalStock=false;
                else
                  isPhysicalStock=result[0].isPhysicalStock;
               //console.log('result[0].isPhysicalStock',result[0].isPhysicalStock);
               var req={fromDate:self.startDate,toDate:self.endDate,deployment_id:self.deployment_id,tenant_id:self.tenant_id};
              //var consumptionViaPhysical =false;
              if(isPhysicalStock == false)
              {
                StockReportNew.getClosingQty(req).$promise.then(function (result){
                  console.log('result',result)
                  /*_.forEach(result,function(r){
                    if(r.allOpeningsValid == false)
                      growl.error('Available stock of '+r.storeName+' items may be incorrect as opening is older than 2 months.Please do an opening for '+r.storeName,{ttl:5000})
                  })*/
               /*     console.log('------------getClosingQty Result--------------------------');
                    console.log(result);
               */     //var data=self.splicePhysicalGreaterThanStartDate(resultProcessed);

                    var data1=self.spliceAllItemsBeforePhysical(result);
                 /*   console.log('------------spliceAllItemsBeforePhysical--------------------------');
                    console.log(data1);
                 */   var res=self.storeWiseItems(data1);
                 /*   console.log('------------storeWiseItems--------------------------');
                    console.log(res);
                 */   self.getStorewiseUniqueItems(res).then(function (items) {
                        //res.items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                        //console.log('------------getStorewiseUniqueItems--------------------------');
                        //console.log(items);
                        var uItems=items;
                        var ret=self.getClosingQtyStoreWise(uItems,res);
                        //console.log('------------getClosingQtyStoreWise--------------------------');
                        console.log(ret);
                        deferred.resolve(ret);
                    });
                });
                //return deferred.promise;
              }
              else
              {
                StockReportNew.getClosingQtyViaPhysical(req).$promise.then(function (result){
               /*     console.log('------------getClosingQty Result--------------------------');
                    console.log(result);
               */     //var data=self.splicePhysicalGreaterThanStartDate(resultProcessed);

                    //var data1=self.spliceAllItemsBeforePhysical(result);
                 /*   console.log('------------spliceAllItemsBeforePhysical--------------------------');
                    console.log(data1);
                 */   var res=self.storeWiseItems(result);
                 /*   console.log('------------storeWiseItems--------------------------');
                    console.log(res);
                 */   self.getStorewiseUniqueItems(res).then(function (items) {
                        //res.items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                        //console.log('------------getStorewiseUniqueItems--------------------------');
                        //console.log(items);
                        var uItems=items;
                        var ret=self.getClosingQtyStoreWiseViaPhysical(uItems,res);
                        //console.log('------------getClosingQtyStoreWise--------------------------');
                        //console.log(ret);
                        deferred.resolve(ret);
                    });
                });
                
              }
            //deferred.resolve(result);
            }, function error(err) {
            //deferred.reject(err);
            });
               //console.log('self.isPhysicalStock',self.isPhysicalStock); 
               
              return deferred.promise;
            },
            getClosingQty_IntermediateFood:function(){
                var self=this;
                var deferred = $q.defer();
                var req={fromDate:self.startDate,toDate:self.endDate,deployment_id:self.deployment_id,tenant_id:self.tenant_id};
                StockReportNew.getClosingQty_SemiProcessed(req).$promise.then(function (result){
                    //StockReportNew.getConsumptionSummary_IntermediateReport(req).$promise.then(function (result){
                    //var data=self.splicePhysicalGreaterThanStartDate(resultProcessed);
                    var data1=self.spliceAllItemsBeforePhysical(result);
                    var res=self.storeWiseItems(data1);
                    self.getStorewiseUniqueItems(res).then(function (items) {
                        var uItems=items;
                        var ret=self.getClosingQtyStoreWise(uItems,res);
                        deferred.resolve(ret);
                    });
                });
                return deferred.promise;
            },
            getClosingQty_ProcessedFood:function(){
                var self=this;
                var deferred = $q.defer();
                var req={fromDate:self.startDate,toDate:self.endDate,deployment_id:self.deployment_id,tenant_id:self.tenant_id};
                StockReportNew.getClosingQty_ProcessedFood(req).$promise.then(function (resultProcessed){
                    //StockReportNew.getConsumptionSummary_FinishedFoodReport(req).$promise.then(function (resultProcessed){
                    //var data=self.splicePhysicalGreaterThanStartDate(resultProcessed);
                    var data1=self.spliceAllItemsBeforePhysical(resultProcessed);
                    var res=self.storeWiseItems(data1);
                    self.getStorewiseUniqueItems(res).then(function (items) {
                        var uItems=items;
                        var ret=self.getClosingQtyStoreWise(uItems,res);
                        deferred.resolve(ret);
                    });
                });
                return deferred.promise;
            }

        };

        return ( ClosingQuantity );
    });
