'use strict';

describe('Service: ClosingQuantity', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var ClosingQuantity;
  beforeEach(inject(function (_ClosingQuantity_) {
    ClosingQuantity = _ClosingQuantity_;
  }));

  it('should do something', function () {
    expect(!!ClosingQuantity).toBe(true);
  });

});
