'use strict';

angular.module('posistApp')
  .factory('vendor', function ($resource) {
    return $resource('/api/vendors/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        //modified
        getAll: {method: 'GET', params: {controller: "getAll"}, isArray: true},
        checkSameVendors: {method: 'GET', params: {controller: "checkSameVendors"}, isArray: false},
        getNumVendorsHavingCategory: {method: 'GET', params: {controller: 'getNumVendorsHavingCategory'}, isArray: false},
        updateItemUnitInHOVendor: {method: 'PUT', params: {controller: 'updateItemUnitInHOVendor'}, isArray: false},
        deleteItemFromHOVendor: {method: 'PUT', params: {controller: 'deleteItemFromHOVendor'}, isArray: false},
        findHOVendorWithItem: {method: 'GET', params: {controller: 'findHOVendorWithItem'}, isArray: false},
        getHOVendor: {method: 'GET', params: {controller: "getHOVendor"}, isArray: true},
        getOutletHOVendor: {method: 'GET', params: {controller: "getOutletHOVendor"}, isArray: true}
        //end
      }
    );
  });
