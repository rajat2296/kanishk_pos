'use strict';

angular.module('posistApp').directive('focus', function() {
  return {
    //restrict: 'A',
    link: function($scope,elem,attrs) {

      elem.bind('keydown keypress', function(e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
          e.preventDefault();
          elem.next().focus();
        }
      });
    }
  }
});