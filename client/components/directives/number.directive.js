'use strict';

angular.module('posistApp').directive('numericOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {

                var transformedInput = 0;
                if (inputValue != undefined) {
                    transformedInput = inputValue.replace(/[^\d.]/g,'');
                } else {
                    inputValue = 0;
                }

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});