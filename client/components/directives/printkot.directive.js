'use strict';

angular.module('posistApp').directive('printkot', function () {
    return {
        scope:{
          currentKot: '&'
        },
        restrict:'E',
        transclude: true,
        template:'<div ng-transclude></div>',
        replace:true
    };
});