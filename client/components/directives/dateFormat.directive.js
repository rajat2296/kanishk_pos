'use strict';

angular.module('posistApp')
.directive('dateFormat', function ($filter,dateFilter) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      
      ngModel.$formatters.push(function (value) {
        return ngModel.$isEmpty(value) ? value : dateFilter(value, "dd-MMM-yyyy");
      });
    }
  }
});