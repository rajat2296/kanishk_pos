'use strict';

angular.module('posistApp')
  .factory('Sync', function ($q, $rootScope, Customer, Station, Supercategory, Category, Tab, Tax, Item, User, growl, localStorageService, lzw, Bill, Deployment, Deploymentclone, Tenant,$webSql,billResource,Utils,Offer,$http,ComplementaryHead) {
    return {
      InsertItems: function (t, count) {
        var d = $q.defer();
        var insertArr = [];
        for (var i = 0; i < t.length; ++i) {
          insertArr.push(
            /* $rootScope.db.insert('items',
             {
             'itemId': t[i]._id,
             'itemData': JSON.stringify(t[i])
             })*/
            $rootScope.db.executeQuery('insert into items(itemId,itemData) values(?,?) ', [t[i]._id, JSON.stringify(t[i])])
          )
        }
        console.log('done delete');
        $q.all(insertArr).then(function (result) {
          console.log('inserted tab');
          d.resolve("done");
        });
        return d.promise;
      },
      InsertTabs: function (t, count) {
        var d = $q.defer();
        var insertArr = [];
        for (var i = 0; i < t.length; ++i) {
          insertArr.push(
            $rootScope.db.insert('tabs',
              {
                'tabId': t[i]._id,
                'tabData': JSON.stringify(t[i])
              })
          )
        }
        console.log('done delete');
        $q.all(insertArr).then(function (result) {
          console.log('inserted tab');
          d.resolve("done");
        })
        return d.promise;
      },
      DeleteDynamic: function (query) {
        var d = $q.defer();
        $rootScope.db.selectDynamic(query, []).then(function () {
          console.log('deleted tab');
          d.resolve('done');
        });
        d.resolve('done');
        return d.promise;
      },
      InsertUsers: function (t, count) {
        var d = $q.defer();
        var insertArr = [];
        for (var i = 0; i < t.length; ++i) {
          insertArr.push(
            $rootScope.db.insert('users',
              {
                'userId': t[i]._id,
                'userData': JSON.stringify(t[i])
              })
          )
        }
        $q.all(insertArr).then(function (result) {
          console.log('inserted tab');
          d.resolve("done");
        });
        return d.promise;
      },
      InsertOffers: function (t, count) {
        var d = $q.defer();
        var insertArr = [];
        for (var i = 0; i < t.length; ++i) {
          insertArr.push(
            $rootScope.db.insert('offers',
              {
                'offerId': t[i]._id,
                'offerData': JSON.stringify(t[i])
              })
          )
        }
        $q.all(insertArr).then(function (result) {
          console.log('inserted offers');
          d.resolve("done");
        });
        return d.promise;
      },
      InsertLastBill:function(){
        var d=$q.defer();
        $rootScope.db.selectDynamic('select count(*) as c from bills',[]).then(function(r){
          if(r.rows.item(0).c===0){
            billResource.getLatestBill({tenantId:localStorageService.get('tenant_id') , deploymentId: localStorageService.get('deployment_id')}).$promise.then(function (bill) {
              console.log(bill);
              if(bill.length>0){
                var s=bill[0];
                /*  $rootScope.db.selectDynamic('insert into bills(serialNumber,daySerialNumber,billNumber,billId,billData,tab,isSynced,syncedOn,created,closeTime,kotNumber) values('+bill.serialNumber,bill.daySerialNumber,bill.billNumber,bill.ng_id,angular.toJson( bill),bill.tab,true,new Date(),bill._created,bill.closeTime,bill.kotNumber +')',[]).then(function(r) {
                 console.log('inserted last item');
                 });*/
                $rootScope.db.insert('bills',
                  {
                    "serialNumber": s.serialNumber,
                    "daySerialNumber": s.daySerialNumber,
                    "billNumber": s.billNumber,
                    "splitNumber": 0,
                    "kotNumber": s.kotNumber,
                    "billId": s.ng_id,
                    "created":Utils.getDateFormatted(Utils.convertDate( s._created)),
                    // "created": (d.toDateString() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()),
                    "billData": '',
                    "isVoid": s.isVoid,
                    "isPrinted": s.isPrinted,
                    "isSettled": s.isSettled,
                    "tab": s.tab,
                    "isSynced": true,
                    "syncedOn": '',
                    "closeTime": s._closeTime,
                    "deployment_id": (s.deployment_id != null) ? s.deployment_id : "null",
                    "tenant_id": s.tenant_id,
                    "tabId": s.tabId,
                    "totalBill": 0,
                    "totalTax": 0,
                    "customer": '',
                    "customer_address": '',
                    "delivery_time": new Date()
                  }
                ).then(function(){
                  d.resolve();
                })
              }else
              {
                d.resolve();
              }
            });
          }
          else
          {d.resolve();}
        });
        return d.promise;
      },
      updateBillCountItems:function(){
        var settingbillcount=localStorageService.get('ItemCountSyncTime');
        var d=$q.defer();
        if(settingbillcount==null) {
          this.syncBillCountItem().then(function(){
            d.resolve();
            localStorageService.set('ItemCountSyncTime',Utils.getDateFormattedDate(new Date()));
          }).catch(function(){
            d.resolve();
          })
        }else
        {
          if(Utils.getDateFormattedDate(new Date())!=Utils.getDateFormattedDate(Utils.convertDate( settingbillcount)))
          {
            this.syncBillCountItem().then(function(){
              d.resolve();
              localStorageService.set('ItemCountSyncTime',Utils.getDateFormattedDate(new Date()));
            }).catch(function(){
              d.resolve();
            })
          }else
          {
            d.resolve();
          }
        }
        return  d.promise;
      },
      syncBillCountItem:function(){
        var d=$q.defer();
        var queries = [];
        billResource.billItemsCount({
          tenantId: localStorageService.get('tenant_id'),
          deploymentId: localStorageService.get('deployment_id')
        }).$promise.then(function (bItems) {
          //tx.executeSql("ALTER TABLE Bills ADD COLUMN IsEdit ", [], null, onupdate);
          console.log(bItems);
          $rootScope.db.selectDynamic("ALTER TABLE Items ADD COLUMN billCount ; ", []).then(function () {
            for (var i = 0; i < bItems.length; i++) {
              queries.push($rootScope.db.selectDynamic('update items set billCount=' + bItems[i].quantity + ' where itemId="' + bItems[i]._id + '";', []));
            }
            $q.all(queries).then(function () {
              d.resolve();
            })
          }).catch(function () {
            for (var i = 0; i < bItems.length; i++) {
              queries.push($rootScope.db.selectDynamic('update items set billCount=' + bItems[i].quantity + ' where itemId="' + bItems[i]._id + '";', []));
            }
            $q.all(queries).then(function () {
              d.resolve();
            }).catch(function () {
              d.resolve();
            })
          });
        });
        return d.promise;
      },
      getAll: function () {
        if($rootScope.db==undefined){
          $rootScope.db= $webSql.openDatabase('posistApp', '1.0', 'Posist Offline DB', 100000 * 1024 * 1024);
        }
        var d = $q.defer();
        $q.all([
          $rootScope.db.selectDynamic('select * from items order by billCount desc'),
          $rootScope.db.selectAll('tabs'),
          $rootScope.db.selectAll('users'),
          $rootScope.db.selectAll('offers')
        ]).then(function (alldata) {
          var itemdata = alldata[0];
          var tabdata = alldata[1];
          var userdata = alldata[2];
          var offerdata=alldata[3]
          //****************************************getting all tabs**********************
          var tempTabs=[];
          var tempTableTabs=[];
          for (var i = 0; i < tabdata.rows.length; i++) {
            var t=(JSON.parse(tabdata.rows.item(i).tabData));
            if(t.tabType=='table')
              tempTableTabs.push(JSON.parse(tabdata.rows.item(i).tabData));
            else {
              var tTabData=JSON.parse(tabdata.rows.item(i).tabData);
              tTabData.orderCount=0;
              tempTabs.push(tTabData);
            }
          }
          $rootScope.tabs=angular.copy( tempTableTabs);
          for(var i=0;i<tempTabs.length;i++){
            $rootScope.tabs.push(tempTabs[i]);
          }
          //**************************************************all tabs End Here***********
          /*getting all offers*/
          for(var i=0;i<offerdata.rows.length;i++){
            $rootScope.offers.push(JSON.parse(offerdata.rows.item(i).offerData));
          }
          var rootExplorerItems=[];
          var rootMasterItems=[];
          var rootExplorerItemsT=[];
          var rootMasterItemsS=[];
          var rootMasterAddons=[];
          for(var t=0;t<$rootScope.tabs.length;t++) {
            var tab={tabId:angular.copy($rootScope.tabs[t]._id),Items:[]};
            var tabCategory={tabId:angular.copy($rootScope.tabs[t]._id),Items:[],categories:[],categoriesT:[],superCategories:[]};
            rootExplorerItems.push(angular.copy( tab));
            rootMasterItems.push(angular.copy( tabCategory ));
            rootMasterItemsS.push(angular.copy( tab));
            rootExplorerItemsT.push(angular.copy( tab));
            rootMasterAddons.push(angular.copy(tab));
          }
          for (var i = 0; i < itemdata.rows.length; i++) {

            var itemDataJSON = JSON.parse(itemdata.rows.item(i).itemData);
            // if (!itemDataJSON.category.isAddons) {
            $rootScope.items.push(angular.copy(itemDataJSON));
            /******************************************Slick Hack******************/
            var tempitem = angular.copy(itemDataJSON);
            var tempitemS = {name: tempitem.name, number: tempitem.number};
            delete tempitem.tabs;
            if (_.has(itemDataJSON, 'tabs')) {
              for (var it = 0; it < rootExplorerItems.length; it++) {  //getting tab wise rate tax
                var itemtab = _.filter(itemDataJSON.tabs, {"_id": rootExplorerItems[it].tabId});
                if (itemtab.length > 0) {
                  if (_.has(itemtab[0], 'item')) {
                    tempitem.rate = itemtab[0].item.rate;
                  }
                  if (_.has(itemtab[0], 'taxes')) {
                    tempitem.taxes = itemtab[0].taxes;
                    //console.log(tempitem);
                  }

                  /* if (Utils.hasSetting('inclusive_tax', $scope.settings)) {
                   tempitem.rate = calculateInclusiveTax(tempitem);
                   }*/
                  if (!itemDataJSON.category.isAddons) {
                    rootMasterItems[it].Items.push(angular.copy(tempitem));
                    rootMasterItemsS[it].Items.push(angular.copy(tempitemS));
                    if (_.has(tempitem, 'category')) {
                      rootMasterItems[it].categories.push(tempitem.category);
                    }
                    rootExplorerItems[it].Items.push({
                      _id: tempitem._id,
                      name: tempitem.name,
                      number: tempitem.number,
                      category: tempitem.category
                    });
                  }else{
                    rootMasterAddons[it].Items.push(angular.copy(tempitem));
                  }
                }
              }
            }
            //********************************Slick Hack Ends Here*********************************
            //}
          }
          //  console.log(rootMasterItems);
          /*********************** Unique Categories with Super Categories****************************/
          for(var it=0;it<rootMasterItems.length;it++) {
            var _uCats = _.uniq(rootMasterItems[it].categories, 'categoryName');
            rootMasterItems[it].categories = _uCats;
            rootMasterItems[it].categories = _.sortBy(rootMasterItems[it].categories, ['categoryName']);
            for(var sc=0;sc<rootMasterItems[it].categories.length;sc++){
              if (_.has(rootMasterItems[it].categories[sc], 'superCategory')) {
                rootMasterItems[it].superCategories.push(angular.copy( rootMasterItems[it].categories[sc].superCategory));
              }
            }
            var _uSCats = _.uniq(rootMasterItems[it].superCategories, 'superCategoryName');
            rootMasterItems[it].superCategories = _uSCats;
            rootMasterItems[it].superCategories = _.sortBy(rootMasterItems[it].superCategories, ['superCategoryName']);
          }
          /************************************Category T**********************/
          /*  for(var it=0;it<rootMasterItems.length;it++) {
           var count = 1;
           var catArr = [];
           _.forEach(rootMasterItems[it].categories, function (_cat, i) {
           catArr.push(_cat);
           if (count % 2 == 0) {
           rootMasterItems[it].categoriesT.push(catArr);
           catArr = [];
           } else {
           if (i == rootMasterItems[it].categories.length - 1) {
           rootMasterItems[it].categoriesT.push(catArr);
           }
           }
           count++;
           });
           }*/
          /************************End Unique Categories ***********************/

          /*  for (var it = 0; it < rootExplorerItems.length; it++) {
           if (rootExplorerItems[it].Items.length > 0) {
           var eItems =angular.copy( rootExplorerItems[it].Items);
           var itemarr = [];
           var count = 1;
           for (var i = 0; i < eItems.length; i++) {
           itemarr.push(eItems[i]);
           if (count % 5 == 0) {
           rootExplorerItemsT[it].Items.push(itemarr);
           itemarr = [];
           } else {
           if (i == eItems.length - 1) {
           if (itemarr.length > 0) {
           // $scope.explorerItemsT.push(itemarr);
           rootExplorerItemsT[it].Items.push(itemarr);
           }
           }
           }
           count++;
           // if(count>60){break;}
           }
           }
           }*/


          $rootScope.rootExplorerItems=rootExplorerItems;
          $rootScope.rootMasterItems=  rootMasterItems;
          $rootScope.rootMasterItemsS=  rootMasterItemsS;
          $rootScope.rootExplorerItemsT=rootExplorerItemsT;
          $rootScope.addons=rootMasterAddons;

          /************************************Slick Page End Here*******************************************/

          for (var i = 0; i < userdata.rows.length; i++) {
            $rootScope.users.push(JSON.parse(userdata.rows.item(i).userData));
          }
          var alldata = {items: $rootScope.items, tabs: $rootScope.tabs,offers:$rootScope.offers, users: $rootScope.users,rootExplorerItems:$rootScope.rootExplorerItems,rootExplorerItemsT:$rootScope.rootExplorerItemsT,rootMasterItems:$rootScope.rootMasterItems,rootMasterItemsS:$rootScope.rootMasterItems,addons:$rootScope.addons};
          d.resolve(alldata);
        });
        return d.promise;
      },
       syncMasterStation:function(){
        $http.get('/api/stations/master?deployment_id='+localStorageService.get('deployment_id')).success(function(station){
          console.log('synced master station',station);
          localStorageService.set('masterStation',station);
        }).error(function(err){
          console.log("No master station",err);
        })
      },

      syncAll: function () {
        var dsa = $q.defer();
        $rootScope.customers = [];
        $rootScope.items = [];
        $rootScope.tabs = [];
        $rootScope.users = [];
        $rootScope.offers=[];
        var self = this;
        Tenant.getServerTime().$promise.then(function (time) {
          //  console.log(time);
          var currentSyncTime = time.serverTime;
          var params = {
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            lastSynced: localStorageService.get('lastSynced'),
            currentSyncTime: currentSyncTime
          };

          var userParams = {
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            role: 'user',
            lastSynced: localStorageService.get('lastSynced'),
            currentSyncTime: currentSyncTime
          };
          $q.all([
            Item.get(params).$promise,
            Tab.get(params).$promise,
            User.syncAll(userParams).$promise,
            Offer.getOffersWithIds(params).$promise
          ]).then(function (syncdata) {
            var itemsdata = angular.copy(syncdata[0]);
            var tabsdata = angular.copy(syncdata[1]);
            var usersdata = angular.copy(syncdata[2]);
            var offersdata=angular.copy(syncdata[3]);
            // console.log(offersdata);
            var tbids = [];
            var itemids = [];
            var uids = [];
            var offerids=[];
            var offeridsd=[];
            _.forEach(tabsdata, function (_t) {
              tbids.push("'" + _t._id + "'");
            })
            _.forEach(itemsdata, function (_t) {
              itemids.push("'" + _t._id + "'");
            })
            _.forEach(usersdata, function (_t) {
              uids.push("'" + _t._id + "'");
            })
            _.forEach(offersdata.offers, function (_t) {
              offerids.push("'" + _t._id + "'");
            });
            _.forEach(offersdata.ids, function (id) {
              offeridsd.push("'" + id._id + "'");
            });
            console.log(offeridsd);
            var tquery = 'delete from items where itemId in (' + (itemids.join(",")) + ');';
            var iquery = 'delete from tabs where tabId in (' + (tbids.join(",")) + ');';
            var uquery = 'delete from users where userId in (' + (uids.join(",")) + ');';
            var oquery='delete from offers where offerId in ('+ (offerids.join(","))+');';
            var oqueryd='delete from offers where offerId not in ('+ (offeridsd.join(","))+');';
            $q.all([
              self.DeleteDynamic(iquery),
              self.DeleteDynamic(tquery),
              self.DeleteDynamic(uquery),
              self.DeleteDynamic(oquery),
              self.DeleteDynamic(oqueryd)
            ]).then(function () {
              $q.all([
                self.InsertItems(itemsdata, 0),
                self.InsertTabs(tabsdata, 0),
                self.InsertUsers(usersdata, 0),
                self.InsertOffers(offersdata.offers,0)
              ]).then(function () {
                self.InsertLastBill().then(function(){
                  self.updateBillCountItems().then(function(){
                    console.log("inserted");
                    self.getAll().then(function (data) {
                      console.log(data);
                      // console.log(time);
                      localStorageService.set('lastSynced', time.serverTime);
                      dsa.resolve(data);
                    });
                  })
                }).catch(function (errMessage) {
                  growl.error(errMessage, {ttl: 1000});
                  self.getAll().then(function (data) {
                    dsa.resolve(data);
                    console.log("done");
                  });
                });
              }).catch(function (errMessage) {
                growl.error(errMessage, {ttl: 1000});
                self.getAll().then(function (data) {
                  dsa.resolve(data);
                  console.log("done");
                });
              });
            }).catch(function (errMessage) {
              growl.error(errMessage, {ttl: 1000});
              self.getAll().then(function (data) {
                dsa.resolve(data);
                console.log("done");
              });
            });
          }).catch(function (errMessage) {
            growl.error(errMessage, {ttl: 1000});
            self.getAll().then(function (data) {
              dsa.resolve(data);
              console.log("done");
            });
          });
        }).catch(function (errMessage) {
          growl.error(errMessage, {ttl: 1000});
          self.getAll().then(function (data) {
            dsa.resolve(data);
            console.log("done");
          });
        });
        return dsa.promise;
      },

      syncCustomers: function () {

        var d = $q.defer();
        var _lastSync = "";

        /* Get Partial Customers */
        if (localStorageService.get('customers_lastSynced')) {
          _lastSync = localStorageService.get('customers_lastSynced');
          /*alert("Get Differential Data");*/
          $rootScope.customers = [];
          Customer.get({
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            lastSynced: localStorageService.get('customers_lastSynced')
          }, function (items) {
            /*console.log(items);*/
            if (items.length > 0) {

              /* Got Differential Data*/
              _.forEach(items, function (i) {
                $rootScope.db.del("customers", {"customerId": i._id});
                $rootScope.db.insert('customers',
                  {
                    'customerId': i._id,
                    'customerData': lzw.compress(JSON.stringify(i))
                  }
                ).then(function (result) {
                  console.log("Inserted Customer");
                });

              });

              $rootScope.db.selectAll('customers').then(function (results) {

                for (var i = 0; i < results.rows.length; i++) {
                  /*console.log(results.rows.item(i));*/
                  $rootScope.customers.push(JSON.parse(lzw.decompress(results.rows.item(i).customerData)));
                }
                growl.success('Customers Synced from Server', {ttl: 3000});
                d.resolve($rootScope.customers);
              });

            } else {
              $rootScope.db.selectAll('customers').then(function (results) {
                for (var i = 0; i < results.rows.length; i++) {
                  $rootScope.customers.push(JSON.parse(lzw.decompress(results.rows.item(i).customerData)));
                }
                growl.success('Customers Synced from Local', {ttl: 3000});
                d.resolve($rootScope.customers);
              });
            }


          }, function (err) {

            /* Couldn't connect Online just get offline Items */
            $rootScope.db.selectAll('customers').then(function (results) {
              for (var i = 0; i < results.rows.length; i++) {
                $rootScope.customers.push(JSON.parse(lzw.decompress(results.rows.item(i).customerData)));
              }
              growl.success('Server Offline, Customers Synced from Local', {ttl: 3000});
              d.resolve($rootScope.customers);
            });

          });


        } else {

          /* Get All Customers */
          Customer.get({
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            lastSynced: localStorageService.get('customers_lastSynced')
          }, function (items) {
            $rootScope.customers = [];

            _.forEach(items, function (i) {
              $rootScope.db.insert('customers',
                {
                  'customerId': i._id,
                  'customerData': lzw.compress(JSON.stringify(i))
                }
              ).then(function (result) {
                $rootScope.customers.push(i);
              });
            });

            localStorageService.set('customers_lastSynced', Math.round(+new Date() / 1000));
            growl.success('Customers Synced from Server', {ttl: 3000});
            d.resolve($rootScope.customers);

          }, function (err) {
            growl.error('Error getting Customers from Server', {ttl: 3000});
          });

        }

        return d.promise;

        /*var d = $q.defer();
         $rootScope.db.selectAll("customers").then(function(results) {

         if (results.rows.length > 0) {
         var _id = results.rows.item(0).id;
         */
        /*console.log("First Case");*/
        /*

         Customer.get({tenant_id:currentUser.tenant_id}, function (customers) {

         $rootScope.customers = [];
         _.forEach(customers, function (c) {
         $rootScope.customers.push(c);
         });
         $rootScope.db.update('customers',
         {
         'lastSynced': new Date,
         'customerData': JSON.stringify(customers)
         },
         {
         id: _id
         }
         );
         */
        /*console.log($rootScope.customers);*/
        /*

         d.resolve($rootScope.customers);

         }, function () {

         $rootScope.customers = [];
         $rootScope.db.selectAll('customers').then(function (results) {
         $rootScope.customers = JSON.parse(results.rows.item(0).customerData);
         });

         d.resolve($rootScope.customers);

         });

         } else {

         console.log("Second case");
         $rootScope.db.insert('customers', {
         'lastSynced': 'null',
         'customerData': 'null'
         }).then(function (results){

         var _id = results.insertId;
         Customer.get({tenant_id:currentUser.tenant_id}, function (customers) {

         $rootScope.customers = [];
         _.forEach(customers, function (c) {
         $rootScope.customers.push(c);
         });
         $rootScope.db.update('customers',
         {
         'lastSynced': new Date,
         'customerData': JSON.stringify(customers)
         },
         {
         id: _id
         }
         );

         d.resolve($rootScope.customers);

         }, function () {

         $rootScope.customers = [];
         $rootScope.db.selectAll('customers').then(function (results) {
         $rootScope.customers = JSON.parse(results.rows.item(0).customerData);
         });

         d.resolve($rootScope.customers);

         });

         });
         }

         });
         return d.promise;*/
      },

      syncItems: function (currentUser) {

        // this.syncAll();
        var d = $q.defer();
        var _lastSync = "";

        /* Get Partial Items */
        if (localStorageService.get('items_lastSynced')) {

          _lastSync = localStorageService.get('items_lastSynced');
          localStorageService.set('items_lastSynced', new Date().toISOString());

          $rootScope.items = [];
          Item.get({
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            lastSynced: _lastSync
          }, function (itemob) {
            var items = itemob;
            if (items.length > 0) {

              /* Got Differential Data*/
              _.forEach(items, function (i, index) {
                // console.log(index);
                $rootScope.db.del("items", {"itemId": i._id});
                $rootScope.db.insert('items',
                  {
                    'itemId': i._id,
                    'itemData': JSON.stringify(i)
                  }
                ).then(function (result) {
                  console.log("Inserted Items");
                });

              });

              $rootScope.db.selectAll('items').then(function (results) {
                for (var i = 0; i < results.rows.length; i++) {
                  $rootScope.items.push(JSON.parse(results.rows.item(i).itemData));
                }
                growl.success('Items Synced from Server, got differential data.', {ttl: 3000});
                d.resolve($rootScope.items);
              });

            } else {

              $rootScope.items = [];
              $rootScope.db.selectAll('items').then(function (results) {
                for (var i = 0; i < results.rows.length; i++) {
                  $rootScope.items.push(JSON.parse(results.rows.item(i).itemData));
                }
                growl.success('Items Synced from Local, no differential data.', {ttl: 3000});
                d.resolve($rootScope.items);
              });
            }
          }, function (err) {

            /* Couldn't connect Online just get offline Items */
            $rootScope.db.selectAll('items').then(function (results) {
              for (var i = 0; i < results.rows.length; i++) {
                $rootScope.items.push(JSON.parse(results.rows.item(i).itemData));
              }
              growl.success('Server Offline, Items Synced from Local', {ttl: 3000});
              d.resolve($rootScope.items);
            });

          });


        } else {

          /* Get All Items since lastSynced was not set */
          _lastSync = localStorageService.get('items_lastSynced');

          Item.get({
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            lastSynced: _lastSync
          }, function (items) {
            $rootScope.items = [];
            /*console.log(items);*/
            _.forEach(items, function (i) {
              $rootScope.db.insert('items',
                {
                  'itemId': i._id,
                  'itemData': JSON.stringify(i)
                }
              ).then(function (result) {
                console.log(result);
                $rootScope.items.push(i);
              });
            });

            localStorageService.set('items_lastSynced', new Date().toISOString());
            growl.success('Items Synced from Server', {ttl: 3000});
            d.resolve($rootScope.items);

          }, function (err) {
            growl.error('Error getting Items from Server', {ttl: 3000});
          });

        }


        return d.promise;
      },

      syncUsers: function (currentUser) {

        var d = $q.defer();
        var _lastSync = "";

        /* Get Partial Customers */
        if (localStorageService.get('users_lastSynced')) {
          _lastSync = localStorageService.get('users_lastSynced');
          /*alert("Get Differential Data");*/
          $rootScope.users = [];
          User.all({
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            role: 'user',
            lastSynced: localStorageService.get('customers_lastSynced')
          }, function (items) {
            /*console.log(items);*/
            if (items.length > 0) {

              /* Got Differential Data*/
              _.forEach(items, function (i) {
                $rootScope.db.del("users", {"userId": i._id});
                $rootScope.db.insert('users',
                  {
                    'userId': i._id,
                    'userData': JSON.stringify(i)
                  }
                ).then(function (result) {
                  console.log("Inserted User");
                });

              });

              $rootScope.db.selectAll('users').then(function (results) {

                for (var i = 0; i < results.rows.length; i++) {
                  /*console.log(results.rows.item(i));*/
                  $rootScope.users.push(JSON.parse(results.rows.item(i).userData));
                }
                growl.success('Users Synced from Server', {ttl: 3000});
                d.resolve($rootScope.users);
              });

            } else {
              $rootScope.db.selectAll('users').then(function (results) {
                for (var i = 0; i < results.rows.length; i++) {
                  $rootScope.users.push(JSON.parse(results.rows.item(0).userData));
                }
                growl.success('Users Synced from Local', {ttl: 3000});
                d.resolve($rootScope.users);
              });
            }


          }, function (err) {

            /* Couldn't connect Online just get offline Items */
            $rootScope.db.selectAll('users').then(function (results) {
              for (var i = 0; i < results.rows.length; i++) {
                $rootScope.users.push(JSON.parse(results.rows.item(i).userData));
              }
              growl.success('Server Offline, Users Synced from Local', {ttl: 3000});
              d.resolve($rootScope.users);
            });

          });


        } else {

          /* Get All Customers */
          User.all({
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id'),
            role: 'user',
            lastSynced: localStorageService.get('users_lastSynced')
          }, function (items) {
            $rootScope.users = [];

            _.forEach(items, function (i) {
              $rootScope.db.insert('users',
                {
                  'userId': i._id,
                  'userData': JSON.stringify(i)
                }
              ).then(function (result) {
                $rootScope.users.push(i);
              });
            });

            localStorageService.set('users_lastSynced', Math.round(+new Date() / 1000));
            growl.success('Users Synced from Server', {ttl: 3000});
            d.resolve($rootScope.users);

          }, function (err) {
            growl.error('Error getting Users from Server', {ttl: 3000});
          });

        }

        return d.promise;

      },

      syncOfflineBills: function (tabId,dep_id) {
        var d = $q.defer();

        if (tabId != undefined) {
          if ($rootScope.isInstance) {
            /*fine dine */
            var offlineBills = [];
            $http.get($rootScope.url + '/api/synckots/getBillsByDepTab?deployment_id='+dep_id+'&tabId=' + tabId, {}).then(function (bills) {
              _.forEach(bills.data, function (ob) {
                // console.log(JSON.parse( ob.billData));
                /*  var billdata=ob.billData;
                 try{
                 var tempbilldata=JSON.parse(billdata);
                 billdata=tempbilldata;
                 }catch(ex){
                 console.log(ex);
                 }*/


                //console.log(ob.billData);
                delete ob.billData._kots;
                if(_.has( ob.billData._waiter,'firstname')){
                  var tmp=ob.billData._waiter.firstname;
                  delete ob.billData._waiter;
                  ob.billData._waiter={firstname:tmp};
                }

               // var billData={_isPrinted:ob.billData._isPrinted_splitNumber:ob.billData._splitNumber,complimentary:ob.billData.complimentary,_isDelivery:ob.billData._isDelivery,_isTakeOut:ob.billData._isTakeOut, tab:ob.billData.tab,tabId:ob.billData.tabId,_id:ob.billData._id,_tableId:ob.billData._tableId,_waiter:ob.billData._waiter,_covers:ob.billData._covers,aggregation:{netRoundedAmount:ob.billData.aggregation.netRoundedAmount},_customer:ob.billData._customer,billNumber:ob.billData.billNumber};
              //console.log(ob.billData);
                var _ob = {
                  id: ob.id,
                  tab: ob.tab,
                  tabId: ob.tabId,
                  serialNumber: ob.serialNumber,
                  daySerialNumber: ob.daySerialNumber,
                  billNumber: ob.billNumber,
                  kotNumber: ob.kotNumber,
                  billId: ob.billId,
                  //bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null, null), billData),

                   bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null, null), typeof(ob.billData)=='object'?ob.billData: JSON.parse(ob.billData)),
                  // bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null, null), billdata),

                  totalBill: parseFloat(ob.totalBill),
                  totalTax: parseFloat(ob.totalTax),
                  isSynced: ob.isSynced,
                  syncedOn: ob.syncedOn,
                  customer: ob.customer,
                  customer_address: ob.customer_address,
                  isVoid: ob.isVoid,
                  isSettled: ob.isSettled,
                  isPrinted: ob.isPrinted,
                  created: ob.created,
                  name: _.has(ob,'instance')? ob.instance.name:'',
                  instance:_.has(ob,'instance')? ob.instance:{},
                  isOnlineBill: false,
                  isAdvanceBill: false,
                  lastModified:ob.lastModified
                };
                _.forEach( _ob.bill._kots,function(kot){
                  delete kot.items;
                })
                offlineBills.push(_ob);

              });
              $http.get($rootScope.url + '/api/synckots/getOnlineBills?deployment_id='+dep_id+'&tabId=' + tabId + '&DownloadStatus=synced', {}).then(function (onlineBills) {
                //console.log(onlineBills);
                _.forEach(onlineBills.data, function (ob) {
                  // console.log(JSON.parse( ob.billData));
                  var _ob = {
                    id: ob.OnlineBillId,
                    tab: '',
                    tabId: ob.tabId,
                    serialNumber: null,
                    daySerialNumber: null,
                    billNumber: parseInt(ob.OnlineBillNumber),
                    kotNumber: '',
                    billId: '',
                    bill: JSON.parse(ob.OnlineBillsData),
                    // customer: ob.customer,
                    isOnlineBill: true,
                    isAdvanceBill: false
                  };
                  offlineBills.push(_ob);

                });
                d.resolve(offlineBills);
                growl.success("Tab changed!", {ttl: 1000});

              }, function (err) {
                console.log(err);
                growl.error("Error Syncing Offline Bills", {ttl: 3000});
                d.reject();

              });
            }, function (err) {
              console.log(err);
              growl.error("Error Syncing Offline Bills", {ttl: 3000});
              d.reject();

            });
            /*fine dine end*/
          }
          else {
            $rootScope.db.selectDynamic("select id,serialNumber,daySerialNumber,billNumber,kotNumber,billData,tab,tabId,billId,isVoid,isSynced,syncedOn,created,isSettled,isPrinted,tab,tabId,totalBill,totalTax,customer,customer_address,delivery_time from bills where tabId='" + tabId + "' and closeTime is null order by billnumber,splitnumber ")
              .then(function (results) {
                // console.log(results);

                var _rows = [];
                var offlineBills = [];
                for (var i = 0; i < results.rows.length; i++) {
                  _rows.push(results.rows.item(i));
                }
                _.forEach(_rows, function (ob) {
                  var _ob = {
                    id: ob.id,
                    tab: ob.tab,
                    tabId: ob.tabId,
                    serialNumber: ob.serialNumber,
                    daySerialNumber: ob.daySerialNumber,
                    billNumber: ob.billNumber,
                    kotNumber: ob.kotNumber,
                    billId: ob.billId,
                    bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null, null), JSON.parse(ob.billData)),
                    totalBill: parseFloat(ob.totalBill),
                    totalTax: parseFloat(ob.totalTax),
                    isSynced: ob.isSynced,
                    syncedOn: ob.syncedOn,
                    customer: ob.customer,
                    customer_address: ob.customer_address,
                    isVoid: ob.isVoid,
                    isSettled: ob.isSettled,
                    isPrinted: ob.isPrinted,
                    created: ob.created,
                    isOnlineBill: false,
                    isAdvanceBill: false
                  };
                  offlineBills.push(_ob);
                });
                console.log("Offline Bills Got");
                //Online Bills Rendering
                //  $rootScope.db.selectDynamic("select OnlineBillId,OnlineBillNumber,OnlineBillsData  from OnlineBills where TabId='" + tabId + "' and DownloadStatus='synced' order by OnlineBillNumber ")
                $rootScope.db.selectDynamic("select OnlineBillId,OnlineBillNumber,OnlineBillsData  from OnlineBills where TabId='" + tabId + "' and DownloadStatus='synced' order by orderTime, OnlineBillNumber ")

                  .then(function (results) {
                    //  console.log(results);

                    var _rows = [];
                    //  var offlineBills = [];
                    for (var i = 0; i < results.rows.length; i++) {
                      _rows.push(results.rows.item(i));
                    }
                    _.forEach(_rows, function (ob) {
                      var _ob = {
                        id: ob.OnlineBillId,
                        tab: '',
                        tabId: ob.tabId,
                        serialNumber: null,
                        daySerialNumber: null,
                        billNumber: parseInt(ob.OnlineBillNumber),
                        kotNumber: '',
                        billId: '',
                        bill: JSON.parse(ob.OnlineBillsData),
                        //bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null,null), JSON.parse(ob.billData)),
                        //totalBill: parseFloat(ob.totalBill),
                        //totalTax: parseFloat(ob.totalTax),
                        //isSynced: ob.isSynced,
                        //syncedOn: ob.syncedOn,
                        customer: ob.customer,
                        isOnlineBill: true,
                        isAdvanceBill: false
                        //customer_address: ob.customer_address,
                        //isVoid: ob.isVoid,
                        //isSettled: ob.isSettled,
                        //isPrinted: ob.isPrinted,
                        //created: ob.created
                      };
                      //if(tabId=='557c3f5334e397b40fe90145')
                      offlineBills.push(_ob);
                    });

                    // Online Bills rendering Ends Here
                    // d.resolve(offlineBills);
                    $rootScope.db.selectDynamic("select AdvanceBillId,AdvanceBillsData  from AdvanceBookings where TabId='" + tabId + "' and DownloadStatus='synced' order by orderTime ")

                      .then(function (results) {
                        //  console.log(results);

                        var _rows = [];
                        //  var offlineBills = [];
                        for (var i = 0; i < results.rows.length; i++) {
                          _rows.push(results.rows.item(i));
                        }
                        _.forEach(_rows, function (ob) {
                          var _ob = {
                            id: ob.AdvanceBillId,
                            tab: '',
                            tabId: ob.tabId,
                            serialNumber: null,
                            daySerialNumber: null,
                            billNumber: null,// parseInt( ob.OnlineBillNumber),
                            kotNumber: '',
                            billId: '',
                            bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null, null), JSON.parse(ob.AdvanceBillsData)),
                            //bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null,null), JSON.parse(ob.billData)),
                            //totalBill: parseFloat(ob.totalBill),
                            //totalTax: parseFloat(ob.totalTax),
                            //isSynced: ob.isSynced,
                            //syncedOn: ob.syncedOn,
                            customer: ob._customer == undefined ? null : ob._customer,
                            isOnlineBill: false,
                            isAdvanceBill: true
                            //customer_address: ob.customer_address,
                            //isVoid: ob.isVoid,
                            //isSettled: ob.isSettled,
                            //isPrinted: ob.isPrinted,
                            //created: ob.created
                          };
                          //if(tabId=='557c3f5334e397b40fe90145')
                          offlineBills.push(_ob);
                        });

                        //  Online Bills rendering Ends Here
                        d.resolve(offlineBills);
                        growl.success("Tab changed!", {ttl: 1000});
                      }).catch(function (err) {

                      console.log(err);
                      growl.error("Error Syncing Offline Bills", {ttl: 3000});
                      d.reject();
                    });

                    //growl.success("Tab changed!", {ttl: 1000});
                  }).catch(function (err) {

                  console.log(err);
                  growl.error("Error Syncing Offline Bills", {ttl: 3000});
                  d.reject();
                });

              }).catch(function (err) {

              console.log(err);
              growl.error("Error Syncing Offline Bills", {ttl: 3000});
              d.reject();
            });
          }
        } else {


          // $rootScope.db.selectAll("bills").then(function (results) {
          //     var _rows = [];
          //     var offlineBills = [];
          //     for (var i = 0; i < results.rows.length; i++) {
          //         _rows.push(results.rows.item(i));
          //     }
          //     _.forEach(_rows, function (ob) {
          //         var _ob = {
          //             id: ob.id,
          //             tab: ob.tab,
          //             tabId: ob.tabId,
          //             serialNumber: ob.serialNumber,
          //             daySerialNumber: ob.daySerialNumber,
          //             billNumber: ob.billNumber,
          //             billId: ob.billId,
          //             bill: _.assign(new Bill(null,null,null,null,null,null, $rootScope.db, null, null, null), JSON.parse(ob.billData)),
          //             totalBill: parseFloat(ob.totalBill),
          //             totalTax: parseFloat(ob.totalTax),
          //             isSynced: ob.isSynced,
          //             syncedOn: ob.syncedOn,
          //             customer: ob.customer,
          //             customer_address: ob.customer_address,
          //             isVoid: ob.isVoid,
          //             isSettled: ob.isSettled,
          //             isPrinted: ob.isPrinted,
          //             created: ob.created
          //         };
          //         offlineBills.push(_ob);
          //     });
          //     /*console.log("Offline Bills Got");*/

          //     d.resolve(offlineBills);
          //     growl.success("Offline Bills Synced!", {ttl:3000});

          // }).catch(function (err) {
          //     growl.error("Error Syncing Offline Bills", {ttl:3000});
          //     d.reject();
          // });

        }

        return d.promise;

      },

      syncSettings: function () {
        var d = $q.defer();

        Deployment.findOne({id: localStorageService.get('deployment_id')}, function (deployment) {

          //console.log(deployment);
          growl.success("Synced Deployment Settings!", {ttl: 3000});
          if (deployment) {
            localStorageService.set('settings', deployment.settings);
            if(_.has(deployment,'smsSettings')){
              localStorageService.set('smssettings', deployment.smsSettings);
            }
          }
          d.resolve({settings: localStorageService.get('settings'),smsSettings:localStorageService.get('smssettings')});
        }, function (err) {
          growl.error("Couldn't load Settings!", {ttl: 3000});
          //d.reject();
          d.resolve({settings: localStorageService.get('settings'),smsSettings:localStorageService.get('smssettings')});
        });
        // added column to resolve duplicate ng_id
        $rootScope.db.selectDynamic("ALTER TABLE Bills ADD COLUMN syncCount ; ", []).then(function () {

        });


        return d.promise;
      },
      syncInstancePrinter: function () {
        var d = $q.defer();
        if($rootScope.isInstance) {
          Deployment.getInstancePrint({deployment_id: localStorageService.get('deployment_id')}, function (instance) {
            console.log(instance);
            if (_.has(instance, 'instances')) {
              localStorageService.set('InstancePrinter', instance.instances);
              //growl.success("Synced Deployment Settings!", {ttl: 3000});
            } else {
              localStorageService.set('InstancePrinter', []);
            }
            d.resolve();
          }, function (err) {
            growl.error("Couldn't load Settings!", {ttl: 3000});
            //d.reject();
          });
        }else{
          d.resolve();
        }
        return d.promise;
      },
       syncComplimentaryHead: function () {
        var d = $q.defer();
        
          ComplementaryHead.get({tenant_id:localStorageService.get('tenant_id'), deployment_id: localStorageService.get('deployment_id')}, function (heads) {
            var hString='';
            heads.forEach(function(head){
              hString+=head.name+';';
            })
              localStorageService.set('complimentary_head', hString.replace(/;(\s+)?$/, '')  );
            d.resolve();
          }, function (err) {
            //growl.error("Couldn't load Settings!", {ttl: 3000});
            //d.reject();
          });
        
        return d.promise;
      },
      syncCharges:function(){
        var charges=[];
        $http.get('/api/charges?deployment_id='+ localStorageService.get('deployment_id')).success(function(data){
          //console.log(data);
          data.forEach(function(charge){
            var chargeOb={name:charge.name,type:charge.type,value:charge.value,tabs:charge.tabs};
            charges.push(chargeOb);
          })
          localStorageService.set('charges',charges );
        });
      } ,
      syncInstanceStationPrinter: function () {
        var d = $q.defer();
        if($rootScope.isInstance) {
          Deployment.getInstanceStationsPrint({deployment_id: localStorageService.get('deployment_id')}, function (instance) {
            console.log(instance);
            if (_.has(instance, 'instancesStations')) {
              localStorageService.set('InstanceStationPrinter', instance.instancesStations);
              //growl.success("Synced Deployment Settings!", {ttl: 3000});
            } else {
              localStorageService.set('InstanceStationPrinter', []);
            }
            d.resolve();
          }, function (err) {
            growl.error("Couldn't load instance Station Settings!", {ttl: 3000});
            //d.reject();
          });
        }else{
          d.resolve();
        }
        return d.promise;
      },

      syncCustomersNew: function () {

        var d = $q.defer();
        var _customers = [];
        var _qAllCustomers = [];

        $rootScope.db.selectAll('customers').then(function (results) {
          for (var i = 0; i < results.rows.length; i++) {
            _customers.push(JSON.parse(results.rows.item(i).customerData));
          }

          _.forEach(_customers, function (customer) {
            _qAllCustomers.push(Customer.save({}, customer).$promise);
          });

          /* Delete all Records from WebSQL */
          $rootScope.db.selectDynamic('DELETE from customers; ').then(function () {
            $q.all(_qAllCustomers).then(function () {
              d.resolve(_qAllCustomers.length + ' customers have been Synced Online!');
            }).catch(function (err) {
              d.reject('Customers could not be saved Online!');
            });
          });

        });

        return d.promise;

      },

      importMasterData: function (importToDeployment, masterDeployment) {

        var d = $q.defer();
        Deploymentclone.batchSave({
          toDeploymentId: importToDeployment._id,
          fromMasterId: masterDeployment._id
        }, function (masterData) {

          d.resolve(masterData);

        }, function (err) {
          d.reject('Something went wrong');
        });
        return d.promise;
      },

      saveBatchDeployment: function (masterData) {

        var _cleanData = this.cleanData(masterData).then(function (_cleanData) {

          /* Clone: Stations*/
          var _stationQ = [];
          _.forEach(_cleanData.stations, function (station) {
            _stationQ.push(Station.save({}, station).$promise);
          });

          $q.all(_stationQ).then(function (stations) {

            _cleanData.stations = stations;

            /* Clone: SuperCategories, replace Stations */
            _.forEach(_cleanData.superCategories, function (sc) {
              if (_.has(sc, 'superCategoryStation')) {
                var _f = _.filter(_cleanData.stations, {stationName: sc.superCategoryStation.stationName});
                if (_f.length > 0) {
                  sc.superCategoryStation = _f[0];
                }
              }

            });

            var _scQ = [];
            _.forEach(_cleanData.superCategories, function (sc) {
              _scQ.push(Supercategory.save({}, sc).$promise);
            });

            $q.all(_scQ).then(function (scs) {
              _cleanData.superCategories = scs;

              /* Clone: Categories */
              _.forEach(_cleanData.categories, function (c) {
                var _f = _.filter(_cleanData.superCategories, {superCategoryName: c.superCategory.superCategoryName});
                if (_f.length > 0) {
                  c.superCategory = _f[0];
                }
              });

              var _cQ = [];
              _.forEach(_cleanData.categories, function (c) {
                _cQ.push(Category.save({}, c).$promise);
              });

              $q.all(_cQ).then(function (cats) {

                _cleanData.categories = cats;
                /*console.log(_cleanData.categories);*/
                /* Clone: Tabs */
                var _cCount = 0;
                angular.forEach(_cleanData.tabs, function (tab) {
                  angular.forEach(tab.categories, function (cat, index) {

                    var _f = _.filter(_cleanData.categories, {categoryName: cat.categoryName});
                    if (_f.length > 0) {
                      tab.categories[index] = _f[0];
                      /* tab.categories.splice(index, 1);
                       tab.categories.push(_f[0]);
                       _cCount++;*/
                    }
                  });
                });
                console.log(_cCount);
                var _tabQ = [];
                _.forEach(_cleanData.tabs, function (tab) {
                  _tabQ.push(Tab.save({}, tab).$promise);
                });

                $q.all(_tabQ).then(function (tabs) {
                  _cleanData.tabs = tabs;


                  /* Clone: Taxes */
                  var _taxQ = [];
                  angular.forEach(_cleanData.taxes, function (tax) {
                    angular.forEach(tax.tabs, function (tab, index) {
                      var _ft = _.filter(_cleanData.tabs, {tabName:tab.tabName});
                      if (_ft.length > 0) {
                        tax.tabs.splice(index, 1);
                        tax.tabs.push({
                          _id: _ft[0]._id,
                          tabName: _ft[0].tabName
                        });
                      }
                    });
                    _taxQ.push(Tax.save({}, tax).$promise);
                  });

                  $q.all(_taxQ).then(function (taxes) {
                    _cleanData.taxes = taxes;

                    /* Clone: Items */
                    _.forEach(_cleanData.items, function (item) {
                      console.log(item);
                      // Map Category to Item
                      var _f = _.filter(_cleanData.categories, {categoryName: item.category.categoryName});
                      if (_f.length > 0) {
                        item.category = _f[0];
                      }

                      // Map Item to Tab
                      angular.forEach(item.tabs, function (tab) {
                        var _tab = _.filter(_cleanData.tabs, {tabName:tab.tabName});
                        if (_tab.length > 0) {
                          tab._id = _tab[0]._id;
                          tab.tabName = _tab[0].tabName;
                          tab.tabType = _tab[0].tabType;
                          tab.deployment_id = _tab[0].deployment_id;
                          tab.tenant_id = _tab[0].tenant_id;
                          tab.item._id = item._id;
                          tab.item.name = item.name;
                          tab.item.rate = parseFloat(item.rate);
                          tab.item.number = parseFloat(item.number);
                          tab.item.category.categoryName = item.category.categoryName;
                        }

                        // Map Tax to Item
                        angular.forEach(tab.taxes, function (tax, index) {
                          var _fTax = _.filter(_cleanData.taxes, {name: tax.name});
                          if (_fTax.length > 0) {
                            tab.taxes.splice(index, 1);
                            tab.taxes.push(_fTax[0]);
                          }
                        });

                      });

                    });

                    var _itemsQ = [];
                    /*console.log(_cleanData.items);*/
                    _.forEach(_cleanData.items, function (item) {
                      _itemsQ.push(Item.save({}, item).$promise);
                    });

                    $q.all(_itemsQ).then(function (items) {
                      /* Finished Cloning */
                      _cleanData.items = items;
                      growl.success("Cloning Successful!", {ttl:3000});
                    });

                  });

                });
              });
            });

          });

        });

      },
      cleanData: function (masterData) {

        var defer = $q.defer();

        try {

          /* Clean Stations */
          _.forEach(masterData.stations, function (station) {
            delete station._id;
            delete station.created;
            delete station.updated;
          });


          _.forEach(masterData.superCategories, function (sc) {
            delete sc._id;
            delete sc.created;
            delete sc.updated;
          });


          _.forEach(masterData.categories, function (c) {
            delete c._id;
            delete c.created;
            delete c.updated;
          });


          _.forEach(masterData.tabs, function (t) {
            delete t._id;
            delete t.created;
            delete t.updated;
          });


          _.forEach(masterData.taxes, function (tax) {
            delete tax._id;
            delete tax.created;
            delete tax.updated;
          });


          _.forEach(masterData.items, function (item) {
            delete item._id;
            delete item.created;
            delete item.updated;
          });

          defer.resolve(masterData);

        } catch (e) {
          alert("Something went wrong");
          defer.reject(e);
        }

        return defer.promise;
      }
    };
  });

