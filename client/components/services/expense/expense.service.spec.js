'use strict';

describe('Service: expense', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var expense;
  beforeEach(inject(function (_expense_) {
    expense = _expense_;
  }));

  it('should do something', function () {
    expect(!!expense).toBe(true);
  });

});
