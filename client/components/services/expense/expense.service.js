'use strict';

angular.module('posistApp')
  .factory('expense', function ($resource) {
    return $resource('/api/expenses/:controller/:id', {
      id: '@_id'
    }, {
      saveData: {
        method: 'POST',
        isArray: false
      },
      get: {
        method: 'GET',
        isArray: true
      },
      findOne: {
        method: 'GET',
        isArray: true
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'DELETE',
        isArray: false
      },
      getExpenseBillNo: {
        method: 'GET',
        params: {
          controller: 'getExpenseBillNo'
        },
        isArray: true
      },
      getBillReportBetweenDates: {
        method: 'GET',
        params: {
          controller: 'getBillReportBetweenDates'
        },
        isArray: true
      },
      getCategoryReportBetweenDates: {
        method: 'GET',
        params: {
          controller: 'getCategoryReportBetweenDates'
        },
        isArray: true
      },
      getItemReportBetweenDates: {
        method: 'GET',
        params: {
          controller: 'getItemReportBetweenDates'
        },
        isArray: true
      }
    });
  });
