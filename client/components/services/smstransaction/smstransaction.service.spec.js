'use strict';

describe('Service: smstransaction', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var smstransaction;
  beforeEach(inject(function (_smstransaction_) {
    smstransaction = _smstransaction_;
  }));

  it('should do something', function () {
    expect(!!smstransaction).toBe(true);
  });

});
