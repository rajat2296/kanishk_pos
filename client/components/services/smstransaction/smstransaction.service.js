'use strict';

angular.module('posistApp')
    .factory('Smstansaction', function ($resource) {
      return $resource('/api/smstransactions/:id',
          {
            id: '@_id'
          },
          {
            Create: {method: 'POST', isArray: true},
            get: {method: 'GET', isArray: true},
            findOne: {method: 'GET'},
            update: {method: 'PUT'},
            destroy:{method:'DELETE'}
          }
      );

    });
