'use strict';

describe('Service: dataList', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var dataList;
  beforeEach(inject(function (_dataList_) {
    dataList = _dataList_;
  }));

  it('should do something', function () {
    expect(!!dataList).toBe(true);
  });

});
