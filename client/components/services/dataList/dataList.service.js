'use strict';

angular.module('posistApp')
    .factory('DateList', function ($resource) {
      return $resource('/api/datelists/:controller/:id',
          {
            id:"@_id"
          },
          {
            destroy:{method:'delete',isArray:false},
            update:{method:'PUT',isArray:false},
            save: {method: 'POST', isArray:false,params:{controller:'save'}},
            get: {method: 'GET', isArray:true},
            findByName: {method: 'GET',params:{controller:'findByName'}, isArray:true}
          }
      );
    });
