'use strict';

describe('Service: Nomnom', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var Nomnom;
  beforeEach(inject(function (_Nomnom_) {
    Nomnom = _Nomnom_;
  }));

  it('should do something', function () {
    expect(!!Nomnom).toBe(true);
  });

});
