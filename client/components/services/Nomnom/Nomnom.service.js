'use strict';

angular.module('posistApp')
  .factory('Nomnom', function Nomnom($location, $rootScope, $http, User, $cookieStore, $q, subdomain) {

    return {

      /**
       * Authenticate user and save token
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      getNomnomKeys: function (temp, callback) {
        var cb = callback || angular.noop;
        var deferred = $q.defer();
        var self=this;
        $http.get('/api/nomnom', {params:{'tenantId': temp.tenantId, 'deploymentId': temp.deploymentId}}).success(function (data) {
          deferred.resolve(data);
          return cb();
        }).error(function (err) {
          deferred.reject(err);
          return cb(err);
        }.bind(this));

        return deferred.promise;
      },

      createNomnom: function (temp, callback) {
        console.log(temp)
        var cb = callback || angular.noop;
        return  $http.post('/api/nomnom/', temp).success(function (data) {
          return cb(data);
        }).error(function (err) {
          return cb(err);
        }.bind(this)).$promise;
      },

      deleteNomnom: function (temp, callback) {
        var cb = callback || angular.noop;
        return  $http.post('/api/nomnom/delete', temp).success(function (data) {
          return cb(data);
        }).error(function (err) {
          return cb(err);
        }.bind(this)).$promise;
      },

      activateNomnom: function (temp, callback) {
        var cb = callback || angular.noop;
        return  $http.post('/api/nomnom/update', temp).success(function (data) {
          return cb(data);
        }).error(function (err) {
          return cb(err);
        }.bind(this)).$promise;
      },

      addSelectionToNomnom: function (temp, callback) {
        var postData = {items:[],nomnomKeys:temp.nomnomKeys};
        _.forEach(temp.items, function (_item) {
          if(  _item.editSelected){
            var itemToPush = {};
            itemToPush = {"id": _item._id,
              "deployment_id": _item.deployment_id,
              "name": _item.name,
              "price": _item.rate,
              "description": _item.description,
              "status": "Avaiable",
              "taxes": [],
              "add_ons":[]
            }
            console.log(itemToPush)
            postData.items.push(itemToPush);
          }
        })
        var cb = callback || angular.noop;
        console.log(postData)
        return  $http.post('api/nomnom/postItems',postData).success(function(success){
          console.log(success)
          return cb(success);
        }).error(function(err){
          console.log(err)
          return cb(err);
        }.bind(this)).$promise;
      },

      deleteSelectionFromNomnom: function (temp, callback) {
        var postData = {items:[],nomnomKeys:temp.nomnomKeys};
        _.forEach(temp.items, function (_item) {
          if(  _item.editSelected){
            var itemToPush = {};
            itemToPush = {"id": _item._id,
              "deployment_id": _item.deployment_id,
              "name": _item.name,
              "price": _item.rate,
              "description": _item.description,
              "status": "Removed from menu",
              "taxes": [],
              "add_ons":[]
            }
            console.log(itemToPush)
            postData.items.push(itemToPush);
          }
        })
        var cb = callback || angular.noop;
        console.log(postData)
        return  $http.post('api/nomnom/deleteItems',postData).success(function(success){
          console.log(success)
          return cb(success);
        }).error(function(err){
          console.log(err)
          return cb(err);
        }.bind(this)).$promise;
      },

      updateItemsInNomnom: function (temp, callback) {
        var postData = {items:[],nomnomKeys:temp.nomnomKeys};
        _.forEach(temp.items, function (itm) {
          var deliveryTab = _.filter(itm.tabs, {tabType:'delivery'});
          //console.log(deliveryTab)
          if(deliveryTab.length>0){
            if(itm.nomnomId>0){
              console.log(itm)
              var taxes = []
              if (_.has(deliveryTab[0], 'taxes')){
                _.forEach(deliveryTab[0].taxes, function (tax) {
                  taxes.push({
                    "name": tax.name,
                    "type": tax.type,
                    value: tax.value
                  });
                })
              }
              var tempItem = {}
              tempItem = {"id": itm._id,
                "deployment_id": itm.deployment_id,
                "name": itm.name,
                "price": itm.rate,
                "description": itm.description,
                "status": "Avaiable",
                "taxes": taxes,
                "add_ons":[]
              }
              console.log(taxes,deliveryTab[0].item.name)

              postData.items.push(tempItem);

            }
          }
        });
        var cb = callback || angular.noop;
        console.log(postData)
        return  $http.post('api/nomnom/deleteItems',postData).success(function(success){
          console.log(success)
          return cb(success);
        }).error(function(err){
          console.log(err)
          return cb(err);
        }.bind(this)).$promise;
      },

      addItemToNomnom: function (temp, callback) {
        var postData = {item:{},nomnomKeys:temp.nomnomKeys};
        postData.item = { "id": temp.item._id,
          "deployment_id": temp.item.deployment_id,
          "name": temp.item.name,
          "price": temp.item.rate,
          "description": temp.item.description,
          "status": "Avaiable",
          "add_ons":[],
          "taxes" : []
        }
        var cb = callback || angular.noop;
        console.log("addItemtonomnom")
        return  $http.post('api/nomnom/postItem',postData).success(function(success){
          console.log(success)
          return cb(success);
        }).error(function(err){
          return cb(err);
        }.bind(this)).$promise;
      },

      deleteItemFromNomnom: function (temp, callback) {
        var postData = {item:{},nomnomKeys:temp.nomnomKeys};
        postData.item = { "id": temp.item._id,
          "deployment_id": temp.item.deployment_id,
          "name": temp.item.name,
          "price": temp.item.rate,
          "description": temp.item.description,
          "status": "Removed from menu",
          "add_ons":[],
          "taxes" : []
        }
        var cb = callback || angular.noop;

        return  $http.post('api/nomnom/deleteItem',postData).success(function(success){
          return cb(success);
        }).error(function(err){
          return cb(err);
        }.bind(this)).$promise;
      },

      updateItemToNomnom: function (temp, callback) {
        var postData = {item:{},nomnomKeys:temp.nomnomKeys};
        postData.item = { "id": temp.item._id,
          "deployment_id": temp.item.deployment_id,
          "name": temp.item.name,
          "price": temp.item.rate,
          "description": temp.item.description,
          "status": "Avaiable",
          "add_ons":[],
          "taxes" : []
        }
        var cb = callback || angular.noop;
        console.log(postData)
        return  $http.post('api/nomnom/updateItem',postData).success(function(success){
          return cb(success);
        }).error(function(err){
          return cb(err);
        }.bind(this)).$promise;
      }

    };
  });
