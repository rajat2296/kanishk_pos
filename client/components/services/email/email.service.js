'use strict';

angular.module('posistApp')
    .factory('Email', function ($resource) {

        return $resource('/api/emails',
            {

            },
            {
                send:{method:'POST'}
            }
        );

    });
