'use strict';

angular.module('posistApp')
  .factory('Role', function ($resource) {

    return $resource('/api/roles/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: true},
        get: {method: 'GET', isArray: true},
        findOne: {method: 'GET'},
        update: {method: 'PUT'}
      }
    );

  });
