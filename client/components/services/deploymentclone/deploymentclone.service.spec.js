'use strict';

describe('Service: deploymentclone', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var deploymentclone;
  beforeEach(inject(function (_deploymentclone_) {
    deploymentclone = _deploymentclone_;
  }));

  it('should do something', function () {
    expect(!!deploymentclone).toBe(true);
  });

});
