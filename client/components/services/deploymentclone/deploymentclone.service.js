'use strict';

angular.module('posistApp')
    .factory('Deploymentclone', function ($resource) {

        return $resource('/api/deploymentclones',
            {},
            {
                batchSave: {method: 'GET'}
            }
        );



    });
