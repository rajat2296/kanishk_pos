'use strict';

describe('Service: smstemplate', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var smstemplate;
  beforeEach(inject(function (_smstemplate_) {
    smstemplate = _smstemplate_;
  }));

  it('should do something', function () {
    expect(!!smstemplate).toBe(true);
  });

});
