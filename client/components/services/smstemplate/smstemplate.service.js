'use strict';

angular.module('posistApp')
    .factory('Smstemplates', function ($resource) {

      return $resource('/api/smstemplates/:id',
          {
            id: '@_id'
          },
          {
            Create: {method: 'POST', isArray: true},
            get: {method: 'GET', isArray: true},
            findOne: {method: 'GET'},
            update: {method: 'PUT'},
             destroy:{method:'DELETE'}
          }
      );

    });
