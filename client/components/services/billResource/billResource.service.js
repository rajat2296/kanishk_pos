'use strict';

angular.module('posistApp')
  .factory('billResource', function($resource) {
    return $resource('/api/bills/:controller/:id/:tenantId/:deploymentId',
      {
        id: '@_id'
      },
      {
        get: {method: 'GET', isArray:true},
        getLatestBill:{
          method:'GET',
          params:{
            controller:'getLatestBill'
          },
          isArray:true
        },
        billItemsCount:{
          method:'GET',
          params:{
            controller:'billItemsCount'
          },
          isArray:true
        },
        sendSMS:{
          method:'GET',
          params:{
            controller:'sendSMS'
          },
          isArray:false
        },
        CRMDetailFromBill:{method:'GET',params:{controller:'CRMDetailFromBill'},isArray:false},
        getItemWiseEnterprise:{method:'POST',params:{controller:'getItemWiseEnterprise'},isArray:true},
        createWithId: {method: 'POST', params: {controller: "createWithId"}, isArray: false},
        updateBillSet: {method: 'POST', params: {controller: "updateBillSet"}, isArray: false},
        billsBackup:{method:'GET',params:{controller: "billsBackup"}, isArray: false},
        billsRestore:{method:'POST',params:{controller: "billsRestore"}, isArray: false},
        billsForManipulation:{method:'GET',params:{controller: "billsForManipulation"}, isArray: true},
        billsTotalAmount:{method:'GET',params:{controller: "billsTotalAmount"}, isArray: true},
        updateBill:{method:'POST',params:{controller:"updateBill"}, isArray: false},
        updateById: {method: 'POST',params:{controller:"updateById"}, isArray: false},
        markDeleteBill:{method:'GET',params:{controller:"markDeleteBill"}, isArray: false},
        updateWithId:{method:'PUT',params:{controller:"updateWithId"}, isArray: false},
        getAggregatedNewOtherPaymentsDetail: {method: 'GET', params: {controller: 'getAggregatedNewOtherPaymentsDetail'}, isArray: true},

        getComplimentaryConsolidated: {
          method: 'POST',
          params: {controller: 'getComplimentaryConsolidated'},
          isArray: true
        },
        getComplimentary: {method: 'POST', params: {controller: 'getComplimentary'}, isArray: true},
        getSettlement: {method: 'POST', params: {controller: 'getSettlement'}, isArray: true},
        getConsolidatedOtherPayments: {method: 'POST', params: {controller: 'getConsolidatedOtherPayments'}, isArray: true},
        getInstanceWiseReports: {method: 'POST', params: {controller: 'getInstanceWiseReports'}, isArray: true},
        getDiscountReports: {method: 'POST', params: {controller: 'getDiscountReports'}, isArray: true},
        getReprintReports: {method: 'POST', params: {controller: 'getReprintReports'}, isArray: true},
        getCategoryWiseReports: {method: 'POST', params: {controller: 'getCategoryWiseReports'}, isArray: true},
        getTaxWiseReports: {method: 'POST', params: {controller: 'getTaxWiseReports'}, isArray: true},
        /*Kanishk Report Rssource*/
        getBillsInDateRange: {method: 'POST', params: {controller: 'getBillsInDateRange'}, isArray: true},
        getBillConsolidate: {method: 'POST', params: {controller: 'getBillConsolidate'}, isArray: true},
        getWaiterBillsInDateRange: {method: 'POST', params: {controller: 'getWaiterBillsInDateRange'}, isArray: true},
        getSuperCategories: {method: 'GET', params: {controller: 'getSuperCategories'}, isArray: true},
        getCategories: {method: 'GET', params: {controller: 'getCategories'}, isArray: true},
        getBillsWithPagination: {method: 'GET', params: {controller: 'getBillsWithPagination'}, isArray: true},
        transferBill: {method: 'POST', params: {controller: 'transferBill'}, isArray: false},
        getBillsByCustomer:{method:'GET',params:{controller: "getBillsByCustomer"}, isArray: false},
        getAllBills:{method:'GET',params:{controller: "getAllBills"}, isArray: true},
        getItemWiseConsolidate:{method:'POST',params:{controller:'getItemWiseConsolidate'},isArray:true},
        getItemWiseConsolidateForProfitSharing:{method:'POST',params:{controller:'getItemWiseConsolidateForProfitSharing'},isArray:true},
        getDataRangeBillsForInvoice:{method:'GET',params:{controller:'getDataRangeBillsForInvoice'},isArray:true},
        getDataRangeBillsForKot:{method:'GET',params:{controller:'getDataRangeBillsForKot'},isArray:true},

        getBillConsolidateForHourlySales: {method: 'POST', params: {controller: 'getBillConsolidateForHourlySales'}, isArray: true},
        //getBillConsolidateForKotDelete: {method: 'POST', params: {controller: 'getBillConsolidateForKotDelete'}, isArray: true},
        //getBillConsolidateForKotTracking: {method: 'POST', params: {controller: 'getBillConsolidateForKotTracking'}, isArray: true},
        getBillsInDateRangeForDelivery: {method: 'POST', params: {controller: 'getBillsInDateRangeForDelivery'}, isArray: true},
        getBillsInDateRangeForDeliveryReport: {method: 'POST', params: {controller: 'getBillsInDateRangeForDeliveryReport'}, isArray: true},
        //getBillsInDateRangeForCompareWaiters: {method: 'POST', params: {controller: 'getBillsInDateRangeForCompareWaiters'}, isArray: true},
        getBillsInDateRangeForOtherPayment: {method: 'POST', params: {controller: 'getBillsInDateRangeForOtherPayment'}, isArray: true},
        getBillsInDateRangeForCustomerData: {method: 'POST', params: {controller: 'getBillsInDateRangeForCustomerData'}, isArray: true},
        getBillsInDateRangeForDailySales: {method: 'POST', params: {controller: 'getBillsInDateRangeForDailySales'}, isArray: true},
        getBillsInDateRangeForTenantWise: {method: 'POST', params: {controller: 'getBillsInDateRangeForTenantWise'}, isArray: true},

        getBillConsolidateForKotDelete: {method: 'POST', params: {controller: 'getBillConsolidateForKotDelete'}, isArray: true},
        getBillConsolidateForKotTracking: {method: 'POST', params: {controller: 'getBillConsolidateForKotTracking'}, isArray: true},
        getAggregatedOtherPaymentsDetail: {method: 'GET', params: {controller: 'getAggregatedOtherPaymentsDetail'}, isArray: true},
        getAggregatedOtherPaymentsConsolidated: {method: 'GET', params: {controller: 'getAggregatedOtherPaymentsConsolidated'}, isArray: true},
        getAggregatedCouponDetail: {method: 'POST', params: {controller: 'getAggregatedCouponDetail'}, isArray: true},
        //getAggregatedCouponConsolidated: {method: 'POST', params: {controller: 'getAggregatedOtherPaymentsConsolidated'}, isArray: true},
        getBillsInDateRangeForCompareWaiters: {method: 'POST', params: {controller: 'getBillsInDateRangeForCompareWaiters'}, isArray: true},
        getRemovedTaxes : {method: 'POST', params: {controller: 'getRemovedTaxes'}, isArray:true},
        getComplimentaryHeadwise : {method: 'POST', params: {controller: 'getComplimentaryHeadwise'}, isArray:true},
        getDataRangeBillsForDailySalesInvoice : {method: 'POST', params: {controller: 'getDataRangeBillsForDailySalesInvoice'}, isArray:true},
        getDataRangeBillsForBoh : {method: 'POST', params: {controller: 'getDataRangeBillsForBoh'}, isArray:true},
        getBillsInDateRangeForAdvanceBooking: {method: 'POST', params: {controller: 'getBillsInDateRangeForAdvanceBooking'}, isArray: true},
        getDataForMenuMix: {method: 'POST', params: {controller: 'getDataForMenuMix'}, isArray: true},
        getItemwiseDiscountReports: {method: 'POST', params: {controller: 'getItemwiseDiscountReports'}, isArray: true},
        getOffersReport: {method: 'POST', params: {controller: 'getOffersReport'}, isArray: true},
        getDataForEmployeePerformance: {method: 'POST', params: {controller: 'getDataForEmployeePerformance'}, isArray: true},
        getDataForEnterpriseSettlement: {method: 'POST', params: {controller: 'getDataForEnterpriseSettlement'}, isArray: true}
        /*Kanishk ReportResource End*/
      }
    );
  });
