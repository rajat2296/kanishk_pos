'use strict';

describe('Service: billResource', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var billResource;
  beforeEach(inject(function (_billResource_) {
    billResource = _billResource_;
  }));

  it('should do something', function () {
    expect(!!billResource).toBe(true);
  });

});
