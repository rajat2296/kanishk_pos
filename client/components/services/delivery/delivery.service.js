'use strict';

angular.module('posistApp')
  .factory('delivery', function delivery($location, $rootScope, $http, User, $cookieStore, $q, subdomain, Tab, localStorageService,Utils,Deployment,growl,socket,$modal,ThirdPartyService,$filter) {

    return {
      assignOrder:function(bill,deliveryOrders,partners){
        console.log("in assignn order");
        var settings=localStorageService.get('settings');
        if(bill._delivery && bill._delivery.firstname && !bill._delivery.username){
          var deliveryName=bill._delivery.firstname;
          var index=-1;
          for(var i=0;i<partners.length;i++){
            if(partners[i].partner_name==deliveryName){
              index=i;
              break;
            }
          }
          if(index!=-1 && partners[index].class && partners[index].class.indexOf('partner_delivery')!=-1)
            this.assignOrder_partner_delivery(bill,deliveryOrders,partners,index);
        }

        else if(bill._delivery){

          if(Utils.hasSetting('enable_delivery_boy_app',settings))
            this.assignOrder_tp(bill);
          else if(Utils.hasSetting('zomato_trace_apikey',settings) && Utils.hasSetting('zomato_trace_username',settings))
            this.assignOrder_zomato(bill,deliveryOrders);
          else{
            var index=-1;
            for(var i=0;i<partners.length;i++){
              if(partners[i].class.indexOf('self_delivery')!=-1){
                index=i;break;
              }
            }
            if(index!=-1)
              this.assignOrder_webhook(bill,partners,index,true);
          }
        }

      },

      assignOrder_partner_delivery:function(bill,deliveryOrders,partners,index){

        if(partners[index].partner_name.toLowerCase()=='delhivery')
          this.assignOrder_delhivery(bill,partners,index,false);
        else if(partners[index].partner_name.toLowerCase()=='roadrunnr')
          this.assignOrder_roadrunnr(bill,partners,index,false);
        else if(partners[index].partner_name.toLowerCase()=='grab')
          this.assignOrder_grab(bill,partners,index,false);
        else
          this.assignOrder_webhook(bill,partners,index,false);
        //  this.assignOrder_webhook(bill,false);
      },

      assignOrder_tp:function(bill){
        try {
          console.log("notifying delivery boy");
          if (!bill || !bill._customer || !bill._customer.firstname || !bill._customer.mobile || !bill._delivery || !bill.billNumber) {
            growl.error("Insufficient Details to notify delivery boy", {ttl: 3000});
            return false;
          }
          if(!bill._delivery.username) {
            growl.error("No Delivery Boy Set",{ttl:3000});
            return false;
          }
          if (!bill._customer.address1) {
            growl.error("No Delivery Address", {ttl: 3000});
            return false;
          }

          var request = {
            username: bill._delivery.username,
            restaurantId: localStorageService.get('deployment_id'),
            restaurantName: ' ',
            orderId: bill._id, bill_no: bill.billNumber.toString(),
            amount: (bill.getTotalBill() + bill.getKotsTotalBill()).toFixed(2),
            add1: bill._customer.address1,
            add2: bill._customer.address2,
            phone: bill._customer.mobile, name: bill._customer.firstname,
            landmark: ' ',
            comments: ' ',
            datetime: (Date.parse(new Date()) / 1000) * 1000,
            threstime: 400000,
            mandatoryImage: '0',
            "orderMode": "0",
            "orderEdit": "0",
            "clientId": "1",
            "client": "POSist",
            "reason": "",
            "markerId": "",
            "markerName": "",
            "to_collect": "0",
          }

          $http({
            method: 'POST',
            url: '/api/technopurple/notify',
            data: request
          }).success(function (data) {
            growl.success("Order Notification sent to delivery boy", {ttl: 3000});
            //$scope.tpOrders.push(data);
          }).error(function (err) {
            growl.error("Notification not sent to delivery boy", {ttl: 3000});
          })
        }
        catch(e){
          console.log("Exception",e);
        }

      },

      assignOrder_zomato:function(bill,zomatoOrders){
        try {
          var autoAssign=Utils.hasSetting('enable_auto_rider_assignment',localStorageService.get('settings'));
          console.log('auto assign',autoAssign);

          console.log("notifying delivery boy");
          if (!bill || !bill._customer || !bill._customer.firstname || !bill._customer.mobile ||(!autoAssign&&!bill._delivery)|| !bill.billNumber) {
            growl.error("Insufficient Details to notify delivery boy", {ttl: 3000});
            return false;
          }
          if(autoAssign && bill._delivery && bill._delivery.username){
            growl.error("Delivery boy will be assigned automatically",{ttl:3000});
            return false;
          }
          if (!autoAssign && !bill._delivery.username) {
            growl.error("No Delivery Boy Set", {ttl: 3000});
            return false;
          }
          if (!bill._customer.address1) {
            growl.error("No Delivery Address", {ttl: 3000});
            return false;
          }
          if(!autoAssign && !bill._delivery.mobile){
            grow.error("Delivery Boy Mobile Not Set",{ttl:3000});
            return false;
          }

          var mode='create';

          Deployment.findOne({id: localStorageService.get('deployment_id')}, function (deployment) {
              console.log(deployment);
              var zomato_apiKey=Utils.getSettingValue('zomato_trace_apikey',localStorageService.get('settings'));
              var zomato_username=Utils.getSettingValue('zomato_trace_username',localStorageService.get('settings'));

              var request = {
                mobile:bill._customer.mobile,
                customer_address:bill._customer.address1,
                customer_name:bill._customer.firstname,
                bill_amount:bill.aggregation.netRoundedAmount,
                order_number:bill.billNumber
                //restaurant_address:deployment.address1+" "+deployment.address2
              }

              if(bill._customer.address2) {request.customer_address+=' '+bill._customer.address2;
                request.customer_locality=bill._customer.address2;
              }

              if(deployment.address1 && deployment.address2)
                request.restaurant_address=deployment.address1 +" "+ deployment.address2;
              // if(!autoAssign)
              request.delivery_boy_mobile=bill._delivery.mobile;
              request.payment_mode='COD';
              if(bill.onlineBill && bill.onlineBill.payments && bill.onlineBill.payments.type)
                request.payment_mode=bill.onlineBill.payments.type;
              if(bill.onlineBill&& bill.onlineBill.source && bill.onlineBill.source.name)
                request.source=bill.onlineBill.source.name;

              var extra_info={};
              extra_info.order_items=[];
              _.forEach(bill._kots,function(k){
                _.forEach(k.items,function(i){
                  extra_info.order_items.push({"name":i.name,"qty": i.qty,"price":i.subtotal});
                })
              });
              request.extra_info=JSON.stringify(extra_info);
              request.confirm_time=$filter('date')(new Date(bill._created), 'dd-mm-yyyy HH:mm');
              var date=new Date(bill._created);
              request.confirm_time=date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes();

              for(var i=0;i<zomatoOrders.length;i++){
                if(bill._id==zomatoOrders[i].orderId){
                  mode='edit';
                  request.oid=zomatoOrders[i].zomatoOrderId;
                  request.status=zomatoOrders[i].status;
                  request.trackingUrl=zomatoOrders[i].trackingUrl
                  break;
                }
              }
              console.log('mode',mode,request);
              $http({
                method: 'POST',
                url: '/api/zomatoTrace/dispatch',
                data: request,
                params:{deployment_id:localStorageService.get('deployment_id'),billId:bill._id,apiKey:zomato_apiKey,username:zomato_username,mode:mode}
              }).success(function (data) {
                growl.success("Order Notification sent to delivery boy", {ttl: 3000});
              }).error(function (err) {
                growl.error("Notification not sent to delivery boy", {ttl: 3000});
              })
            },
            function(err) {
              growl.error("Something went wrong", {ttl: 3000});
            })
        }
        catch (e) {
          console.log("Exception", e);
          growl.error("something went wrong",{ttl:3000});
        }
      },

      assignOrder_webhook:function(bill,partners,index,isDeliveryBoySet){
        try {

          console.log("notifying delivery boy");
          if (!bill || !bill._customer || !bill._customer.firstname || !bill._customer.mobile||!bill.billNumber||!bill._delivery) {
            growl.error("Insufficient Details to notify delivery boy", {ttl: 3000});
            return false;
          }

          if (!bill._delivery.username && isDeliveryBoySet) {
            growl.error("No Delivery Boy Set", {ttl: 3000});
            return false;
          }
          if (!bill._customer.address1) {
            growl.error("No Delivery Address", {ttl: 3000});
            return false;
          }
          if(!bill._delivery.mobile && isDeliveryBoySet){
            growl.error("Delivery Boy Mobile Not Set",{ttl:3000});
            return false;
          }

          var mode='create';

          ThirdPartyService.getPartner(localStorageService.get('deployment_id'),partners[index].partner_name).success(function(partner){

            Deployment.findOne({id: localStorageService.get('deployment_id')}, function (deployment) {

              if(!partner.customer_key){
                growl.error("Partner's Customer Key Not Present",{ttl:3000});
                return false;
              }

              var request = {
                customer_mobile:bill._customer.mobile,
                customer_address:bill._customer.address1+" "+bill._customer.address2,
                customer_locality:bill._customer.address2,
                customer_name:bill._customer.firstname,
                order_id:bill._id,
                bill_amount:bill.getTotalBill() + bill.getKotsTotalBill(),
                order_number:bill.billNumber,
                restaurant_name:deployment.name,
                restaurant_address:deployment.address1+" "+deployment.address2,
                delivery_boy_name:bill._delivery.firstname,
                delivery_boy_mobile:bill._delivery.mobile
              }

              if(partner.partner_merchant_id)
                request.merchant_id=partner.partner_merchant_id;

              request.payment_mode='COD';
              if(bill.onlineBill && bill.onlineBill.payments && bill.onlineBill.payments.type)
                request.payment_mode=bill.onlineBill.payments.type;

              request.bill_details={items:[]};
              _.forEach(bill._kots,function(k){
                _.forEach(k.items,function(i){
                  var item={name:i.name,quantity:i.quantity,rate:i.rate,amount:i.subtotal};
                  if(item.discounts && item.discounts.length>0)
                    item.discount=item.discounts[0].discountAmount;

                  if(i.taxes && i.taxes.length>0){
                    var _taxes=[];
                    _.forEach(i.taxes,function(t){
                      _taxes.push({name:t.name,amount:t.tax_amount});
                    });
                    item.taxes=[]; item.taxes=_taxes;
                  }
                  request.bill_details.items.push(item);
                })
              });

              if(bill.charges && bill.charges.detail && bill.charges.detail.length>0){
                var charges=[];
                request.bill_details.charges=[]
                _.forEach(bill.charges.detail,function(d){
                  charges.push({name:d.name,amount:d.amount});
                });
                request.bill_details.charges=charges;
              }


              console.log('mode',request);
              $http({
                method: 'POST',
                url: '/api/delivery/webhook_notify',
                data: {data:request,partners:[{client_id:partners[index].client_id,customer_key:partner.customer_key}]},
                params:{deployment_id:localStorageService.get('deployment_id'),billId:bill._id,merchant_id:partner.partner_merchant_id,clientId:partners[index].client_id}
              }).success(function (data) {
                growl.success("Order Notification sent to delivery boy", {ttl: 3000});
              }).error(function (err) {
                growl.error("Notification not sent to delivery boy", {ttl: 3000});
              });
            })
          }).error(function(err){
            growl.error("Could not get Partner Details",{ttl:3000});
          });
        }
        catch (e) {
          console.log("Exception", e);
          growl.error("something went wrong",{ttl:3000});
        }
      },

      showRiderLocation:function(deliveryBoys){
        try{
          var modalInstance = $modal.open({
            templateUrl: 'app/billing/riderLocation.html',
            resolve:{
              deliveryBoys:function(){
                return deliveryBoys;
              }
            },
            controller: ['$rootScope', '$scope', '$modalInstance','socket','deliveryBoys', function ($rootScope, $scope, $modalInstance,socket,deliveryBoys) {
              $scope.deliveryBoys=deliveryBoys;

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

              console.log($scope.deliveryBoys);

              var marker={};

              _.forEach($scope.deliveryBoys,function(user){
                marker['m_'+user.username]= {
                  lat: parseFloat(user.location.lat),
                  lng: parseFloat(user.location.lon),
                  draggable: false,
                  message: user.firstname+' is here!',
                  focus: true
                }
              })

              console.log($scope.deliveryBoys[0].location.lat,$scope.deliveryBoys[0].location.lon);

              angular.extend($scope, {
                center: {
                  lat: parseFloat($scope.deliveryBoys[0].location.lat),
                  lng: parseFloat($scope.deliveryBoys[0].location.lon),
                  zoom: 12
                },

                markers: marker,
                position: {
                  lat: parseFloat($scope.deliveryBoys[0].location.lat),
                  lng: parseFloat($scope.deliveryBoys[0].location.lon)
                },
                events: {
                  markers: {
                    enable: ['dragend']
                  }
                }
              });

              console.log($scope.markers);

              var subdomain=window.location.hostname.slice(0,window.location.hostname.indexOf('.'));
              if(window.location.hostname=='54.169.245.230') subdomain='flipkart';

              socket.on('userLocation_'+subdomain,function(data){
                console.log('userlocationsocket',data);
                if($scope.markers['m_'+data.username]) {
                  $scope.markers['m_' + data.username].lat = data.lat;
                  $scope.markers['m_' + data.username].lng = data.lon;
                }
              });

            }],
            windowClass: 'app-modal-window'
          });



          modalInstance.result.then(function (phone) {
            console.log("map closed");
          }, function () {
            console.log('Modal dismissed at: ' + new Date());
          });
        }
        catch(e){
          console.log("Exception"+e);
        }
      },

      showDeliveryHistory:function(bill){
        try{
          var modalInstance = $modal.open({
            templateUrl: 'app/billing/_deliveryHistory.html',
            resolve:{
              bill:function(){
                return bill;
              }
            },
            controller: ['$rootScope', '$scope', '$modalInstance','socket','bill', function ($rootScope, $scope, $modalInstance,socket,bill) {
              $scope.bill=bill;

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

              console.log($scope.bill);


            }],
            size: 'lg'
          });

          modalInstance.result.then(function (phone) {
            console.log("map closed");
          }, function () {
            console.log('Modal dismissed at: ' + new Date());
          });
        }
        catch(e){
          console.log("Exception"+e);
        }
      },

      refreshOrderStatus:function(orderId,deliveryOrders){
        var deferred=$q.defer();
        var zomatoOrders=deliveryOrders;
        console.log(zomatoOrders);
        var oid='';
        for(var i=0;i<zomatoOrders.length;i++){
          console.log(orderId,zomatoOrders[i].orderId);
          if(zomatoOrders[i].orderId==orderId){
            oid=zomatoOrders[i].zomatoOrderId;
            break;
          }
        }
        if(oid=='')
          deferred.reject({status:'no order found for this bill'});

        else{
          var zomato_apiKey=Utils.getSettingValue('zomato_trace_apikey',localStorageService.get('settings'));
          var zomato_username=Utils.getSettingValue('zomato_trace_username',localStorageService.get('settings'));

          $http({
            method:'GET',
            url:'/api/zomatoTrace/getStatus',
            params:{deployment_id:localStorageService.get('deployment_id'), billId:orderId,oid:oid,username:zomato_username,apiKey:zomato_apiKey}
          }).success(function(data){
            console.log(data);
            deferred.resolve(data);
          }).error(function(err){
            deferred.reject({status:'Something went wrong'});
          })
        }
        return deferred.promise;
      },

      getPendingOrders:function(tabs){
        var deferred=$q.defer();
        var orders = [];
        if (tabs)
          _.forEach(tabs, function (t) {
            if (t.bill)
              orders.push(t.bill._id);
          });

        console.log(orders);
        if(orders.length<=0) deferred.reject({status:'empty'});
        if(orders.length>0){
          var url='';
          if(Utils.hasSetting('enable_delivery_boy_app',localStorageService.get('settings'))) url='/api/technopurple/getOrders'
          else if(Utils.hasSetting('zomato_trace_apikey',localStorageService.get('settings'))) url='/api/zomatoTrace/orders';
          else url='/api/delivery/orders';
          console.log("url",url);
          $http({
            method: 'GET',
            url: url+'?deployment_id='+localStorageService.get('deployment_id')+'&orders='+orders.toString()
          }).success(function (data) {
            console.log(data);
            deferred.resolve(data);
          }).error(function (data) {
            deferred.reject({status:'error'});
          });
        }
        return deferred.promise;
      },

      assignOrder_delhivery:function(bill,partners,index,isDeliveryBoySet){

        var auth_token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoicG9zaXN0QG9keC5pbyIsImVtYWlsIjoicG9zaXN0QG9keC5pbyIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9kZWxoaXZlcnktb3JnLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ODIyZDE2NGM4Zjk1MWRmNzQ1NGVlNzAiLCJhdWQiOiIwMk5EazJKbEdBNjRqYlAzR3p1ZHRCalJEVVdIRkNGMyIsImV4cCI6MTAxMTg2NzY4MzcsImlhdCI6MTQ3ODY3NjgzN30.KlqsvXzmTabUerCUstTry9YJGlUZZK7TJ2lk0sjYBcU"
        try {
          console.log("notifying delivery boy");
          if (!bill || !bill._customer || !bill._customer.firstname || !bill._customer.mobile || !bill._delivery || !bill.billNumber) {
            growl.error("Insufficient Details to notify delivery boy", {ttl: 3000});
            return false;
          }

          if (!bill._customer.address1) {
            growl.error("No Delivery Address", {ttl: 3000});
            return false;
          }

          ThirdPartyService.getPartner(localStorageService.get('deployment_id'),partners[index].partner_name).success(function(partner){
            Deployment.findOne({id: localStorageService.get('deployment_id')}, function (deployment) {
              console.log(deployment);

              var request = {
                order_id:bill._id,
                merchant:{
                  app_merchant_id:partner.partner_merchant_id,
                  name:deployment.name
                },
                customer:{
                  name:bill._customer.firstname,
                  address:bill._customer.address1+" "+bill._customer.address2,
                  locality:bill._customer.address2,
                  phone:bill._customer.mobile
                },
                amount_bill_value:bill.aggregation.netRoundedAmount,
                amount_collect_customer:bill.aggregation.netRoundedAmount
              }

              if(bill._customer.city) request.customer.city=bill._customer.city;
              if(bill._customer.postCode) request.customer.pincode=bill._customer.postCode
              if(deployment.address1 && deployment.address2){
                request.merchant.address=deployment.address1+" "+deployment.address2;
                request.merchant.locality=request.address2;
              }
              if(deployment.city) request.merchant.city=deployment.city;
              if(deployment.phone) request.merchant.phone=deployment.phone

              request.payment_type='COD';
              if(bill.onlineBill && bill.onlineBill.payments && bill.onlineBill.payments.type && bill.onlineBill.payments.type){
                request.payment_type='Prepaid';
                request.amount_collect_customer=0;
              }


              $http({
                method: 'POST',
                url: '/api/delivery/delhivery_notify',
                data: request,
                params:{deployment_id:localStorageService.get('deployment_id'),billId:bill._id,token:auth_token}
              }).success(function (data) {
                growl.success("Order Notification sent", {ttl: 3000});
              }).error(function (err) {
                growl.error("Notification not sent", {ttl: 3000});
              })

            })
          }).error(function(err){
            growl.error("Could not get partner details",{ttl:3000});
          })

        }
        catch(e){
          console.log("Exception",e);
        }
      },

      assignOrder_roadrunnr:function(bill,orders,partners,index){
        if (!bill || !bill._customer || !bill._customer.firstname || !bill._customer.mobile || !bill._delivery || !bill.billNumber) {
          growl.error("Insufficient Details to notify delivery boy", {ttl: 3000});
          return false;
        }

        if (!bill._customer.address1) {
          growl.error("No Delivery Address", {ttl: 3000});
          return false;
        }
        if(!bill._customer.postCode){
          growl.error("Customer postal code is required",{ttl:3000});
          return false;
        }
        if(!bill._customer.city){
          growl.error("Customer city is required",{ttl:3000});
          return false;
        }

        var settings=localStorageService.get('settings');
        var client_id=Utils.getSettingValue('roadrunnr_client_id',settings)
        var client_secret=Utils.getSettingValue('roadrunnr_client_secret',settings);
        var latitude=Utils.getSettingValue('latitude',settings);
        var longitude=Utils.getSettingValue('longitude',settings);
        if(!latitude || !longitude){
          growl.error("Outlet Latitude and Longitude are mandatory",{ttl:3000});
          return false;
        }

        Deployment.findOne({id: localStorageService.get('deployment_id')}, function (deployment) {

          if(!deployment.address1 || !deployment.mobile || !deployment.city){
            growl.error("Outlet Details missing",{ttl:3000});
            return false;
          }
          var request={
            pickup:{
              user:{
                name:deployment.name,
                phone_no:deployment.mobile,
                type:'merchant',
                full_address:{
                  address:deployment.address1+" "+(deployment.address2?deployment.address2:''),
                  city:{
                    name:deployment.city,
                  },
                  geo:{
                    latitude:latitude,
                    longitude:longitude
                  }
                }
              }
            },
            drop:{
              user:{
                name:bill._customer.firstname,
                phone_no:bill._customer.mobile,
                type:'customer',
                full_address:{
                  address:bill._customer.address1+' '+(bill._customer.address2?bill._customer.address2:''),
                  zip_code:{
                    code:bill._customer.postCode
                  },
                  city:{
                    name:bill._customer.city,
                  }
                }
              }
            },
            order_details:{
              order_id:bill._id,
              order_value:bill.getNetAmount().toString(),
              amount_to_be_collected:(bill.onlineBill && bill.onlineBill.payments && bill.onlineBill.payments.type!='COD')?'0':bill.getNetAmount().toString(),
              amount_to_be_paid:(bill.onlineBill && bill.onlineBill.payments && bill.onlineBill.payments.type!='COD')?'0':bill.getNetAmount().toString(),
              order_type:{
                name:(bill.onlineBill && bill.onlineBill.payments && bill.onlineBill.payments.type!='COD')?'Prepaid':'CashOnDelivery',
              },
              order_items:[]
            },
            created_at:Utils.getDateFormatted(new Date()),
            callback_url:'http://'+window.location.host.toString()+'/api/delivery/roadrunnr/status'
          }

          _.forEach(bill._kots,function(kot){
            _.forEach(kot.items,function(item){
              request.order_details.order_items.push({quantity:item.quantity,price:item.rate,item:{name:item.name}})
            })
          })

          $http({
            method:'POST',
            url:'/api/delivery/roadrunnr/pushOrder',
            data:{data:request,client_id:client_id,client_secret:client_secret},
            params:{billId:bill._id,deployment_id:localStorageService.get('deployment_id')}
        }).then(function(result) {
          var message=result.data.message?result.data.message:'Order sent';
          growl.success(message, {ttl: 3000});
        },function (err) {
          var message=err.data.message?err.data.message:'Notification not sent';
          growl.error(message,{ttl:3000});
        });
      })
  },

assignOrder_grab:function(bill,order,partners,index){
  if (!bill || !bill._customer || !bill._customer.firstname || !bill._customer.mobile || !bill._delivery || !bill.billNumber) {
    growl.error("Insufficient Details to notify delivery boy", {ttl: 3000});
    return false;
  }

  if (!bill._customer.address1) {
    growl.error("No Delivery Address", {ttl: 3000});
    return false;
  }
  var settings=localStorageService.get('settings');
  var clientId=Utils.getSettingValue('grab_client_id',settings);
  var publicKey=Utils.getSettingValue('grab_public_key',settings);
  var privateKey=Utils.getSettingValue('grab_private_key',settings);

  var request={
    clientId:clientId,
    clientOrderId:bill._id,
    dttm:Utils.getDateFormatted(new Date(bill._created)),
    merchantId:"10",
    //merchantId:loalStorageService.get('deployment_id');
    customerPhone:bill._customer.mobile,
    customerName:bill._customer.firstname,
    customerAddesssLine1:bill._customer.address1,
    customerAddressLine2:bill._customer.address2,
    billAmount:bill.getNetAmount(),
    billNo:bill.billNumber,
    orderType:(bill.onlineBill && bill.onlineBill.payments && bill.onlineBill.payments.type!='COD')?'Prepaid':'COD'
  }
  request.amountPaidByClient=(request.orderType=='Prepaid')?bill.getNetAmount():'0',
    request.amountCollectedInCash='0';

  $http({
      method:'POST',
      url:'/api/delivery/grab/pushOrder',
      data:{data:request,clientId:clientId,publicKey:publicKey,privateKey:privateKey},
      params:{billId:bill._id,deployment_id:localStorageService.get('deployment_id')}
  }).then(function(result) {
     growl.success(result.data.message?result.data.message:"Order Notification sent", {ttl: 3000});
   },function (err) {
     growl.error(err.data.status?err.data.status:'Notification not sent',{ttl: 3000});
  });
}

};
});

