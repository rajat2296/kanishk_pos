'use strict';

describe('Service: bills', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var bills;
  beforeEach(inject(function (_bills_) {
    bills = _bills_;
  }));

  it('should do something', function () {
    expect(!!bills).toBe(true);
  });

});
