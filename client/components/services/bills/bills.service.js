'use strict';

angular.module('posistApp')
    .factory('Bills', function () {

        var meaningOfLife = 42;
        var bills = [];
        var taxesAggregated = [];
        var lastSynced = "";

        return {
            addBill: function (bill) {
                bills.push(bill);

                /* Aggregate Taxes */
                var _allTaxes = [];
                angular.forEach(bills, function (bill) {
                    angular.forEach(bill.bill._kots, function (kot){
                        angular.forEach(kot.taxes, function (tax){
                            _allTaxes.push(tax);
                        })
                    });
                });
                _.chain(_allTaxes).groupBy('id').map(function (t) {
                    var _taxAmount = 0;
                    angular.forEach(t, function (gt) {
                        _taxAmount += parseFloat(gt.tax_amount);
                    });
                    taxesAggregated.push({
                        "id": t[0].id,
                        "name": t[0].name,
                        "tax_amount": parseFloat(_taxAmount).toFixed(2)
                    });
                });

            },
            getUnsettled: function () {

            },
            getTotalBill: function () {

            },
            getTotalTax: function () {

            },
            sync: function () {
                alert("Syncing Bills to Server...");
            },
            getTaxesAggregated: function () {
                return taxesAggregated;
            }
        };
    });
