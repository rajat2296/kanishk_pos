'use strict';

describe('Service: supercategory', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var supercategory;
  beforeEach(inject(function (_supercategory_) {
    supercategory = _supercategory_;
  }));

  it('should do something', function () {
    expect(!!supercategory).toBe(true);
  });

});
