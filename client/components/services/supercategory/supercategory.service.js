'use strict';

angular.module('posistApp')
    .factory('Supercategory', function ($resource) {
        return $resource('/api/supercategorys/:id/:controller',
            {
                id: '@_id'
            },
            {
                saveData: {method: 'POST', isArray: true},
                get: {method: 'GET', isArray: true},
                update: {method: 'PUT'},
                DeleteAuth: {
                    method: 'DELETE',
                    params: {
                        controller: 'checkdelete'
                    }
                },
                deleteAuthMulti: {
                    method: 'PUT',
                    params: {
                        controller: 'checkdelete'
                    }
                }
            });
    });
