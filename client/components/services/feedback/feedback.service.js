'use strict';

angular.module('posistApp')
  .factory('Feedback', function ($resource) {
      return $resource('/api/feedbacks/:id/:controller',
          {
            id:'@_id'
          },
          {
            get: {method: 'GET', isArray: true},
            update:{method:'PUT'},
            createBill:{method:'POST',params:{controller:'createBill'},isArray:false} ,
            create:{method:'POST'}
          }
      );
    });
