'use strict';

angular.module('posistApp')
  .factory('Advancebooking', function ($resource) {
    // AngularJS will instantiate a singleton by calling "new" on this function
      return $resource('/api/advancebookings/:id/:controller',
          {
            id:"@_id"
          },
          {
              updateOrderModify:{method:'POST',isArray:false,params:{controller:'updateOrderModify'}},
              getAdvanceByDeployment:{method:'GET',isArray:true,params:{controller:'getAdvanceByDeployment'}},
            save: {method: 'POST', isArray:false},
            get: {method: 'GET', isArray:true},
            update: {method: 'PUT'},
              delete:{method: 'DELETE',isArray:false}
          }
      );
  });
