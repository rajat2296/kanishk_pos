'use strict';

describe('Service: advancebooking', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var advancebooking;
  beforeEach(inject(function (_advancebooking_) {
    advancebooking = _advancebooking_;
  }));

  it('should do something', function () {
    expect(!!advancebooking).toBe(true);
  });

});
