'use strict';

angular.module('posistApp')
  .factory('barExchange', function delivery($location, $rootScope, $http, User, $cookieStore, $q, subdomain, Tab, localStorageService,Utils,Deployment,growl,socket,$modal,ThirdPartyService,$filter) {

    return {      
     getItemPrice:function(item,bill,ip,quantity){
      var deferred=$q.defer();
      try{
       //console.log(item);
        var qty;
        if(!quantity)
          qty=1;
         else  
          qty=parseFloat(quantity); 

           var url=ip+'/techfreshBar/changePrice?PartnerId=1001&orderFrom=pst&tableNumber='+bill._tableId+'&CategoryId='+item.category._id+'&ItemCode='+item._id+'&PricePerItem='+item.rate+'&quantity='+qty;
    
            var xhttp = new XMLHttpRequest();

              xhttp.onreadystatechange = function() {
                 if (this.readyState == 4 && this.status==200){
                    console.log(this.response);
                   try{
                     var data=JSON.parse(this.response.substring(1,this.response.length-1));
                     if(data.BxPricePerItem && data.BxPricePerItem!="")
                       deferred.resolve(parseFloat(data.BxPricePerItem));
                     else
                      deferred.resolve(item.rate)
                   }
                   catch(e){
                     deferred.resolve(item.rate);
                   }
                 }
                 else if(this.readyState==4 && this.status!=200){
                   growl.error("Could not Get Bar Exchange Item Price",{ttl:3000});
                   deferred.resolve(item.rate);
                 }    
              }
              xhttp.ontimeout=function(){
                growl.error("Bar exchange server took too long to respond",{ttl:3000})
                deferred.resolve(item.rate);
              }
              xhttp.timeout=2000;

              xhttp.open("GET",url,true);
              xhttp.send();
           }
        catch(e){
         deferred.resolve(item.rate);
        }
      return deferred.promise;
    }

  };
});
