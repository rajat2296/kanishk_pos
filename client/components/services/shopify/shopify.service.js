'use strict';

angular.module('posistApp')
  .factory('Shopify', function ($resource) {

    return $resource('/api/shopify/:id',
      {
        id: '@_id'
      },
      {
        create: {method: 'POST'},
        get: {method: 'GET', isArray: true},
        findOne: {method: 'GET'},
        update: {method: 'PUT'}
      }
    );

  });
