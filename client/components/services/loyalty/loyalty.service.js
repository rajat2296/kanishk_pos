  'use strict';

angular.module('posistApp')
  .factory('loyalty', function loyalty($location, $rootScope, $http, User, $cookieStore, $q, subdomain, Tab, localStorageService,Utils,Deployment,growl,socket,$modal) {

    return { 
        isLoyaltyPartner:function(partnerName){
          this.getClientId(partnerName).then(function(data){
            return data.clientId;
          },
          function(err){
              return false;
          });
        },

        getClientId:function(partner){
          var deferred=$q.defer();
          $http({
            method:'GET',
            url:'/api/partnerDetails/'+partner
          }).success(function(data){
            deferred.resolve(data);
          }).error(function(err){
            deferred.reject(err);
          })
          return deferred.promise;
        },

        notifyLoyaltyPartner:function(partnerName,otherForm,bill){
           var modalInstance = $modal.open({
           template: '<div class="modal-header text-center">'+ 
           '<h3 style="color:blue">Loyalty</h3><div class="modal-body text-center"><br/><input type="text" ng-if="!mobileNumber" placeholder="Enter Amount" ng-model="phone">' +
           '<div align="center"><input type="text" class="form-control" ng-model="enteredAmount" ng-disabled="!isValidated" placeholder="Enter Amount"/><br/>'+
           '<button type="button" class="btn btn-primary"  ng-click="validatePayment()">Validate</button>&nbsp; &nbsp;<button type="button" class="btn btn-primary" ng-disabled="!isValidated" ng-click="redeem()">Redeem</button></div>' +
           '</div>',
          resolve: {
            mobileNumber: function () {
              return bill._customer.mobile;
            },
            
            billAmount:function(){
              return (bill.getTotalBill() + bill.getKotsTotalBill()).toFixed(2)
            }
          },
          controller: ['$rootScope', '$scope', '$modalInstance','mobileNumber','username','apiKey','billAmount', function ($rootScope, $scope, $modalInstance,mobileNumber,username,apiKey,billAmount) {

            //  $scope.bill=bill;
            $scope.form = {};
            $scope.mobileNumber=mobileNumber;
            $scope.isValidated=false;
            $scope.amount=0;
            $scope.billAmount=billAmount;

            $scope.validatePayment = function () {
              var mobile=$scope.mobileNumber?$scope.mobileNumber:$scope.phone; 
                var ex = new RegExp('^[0-9]+$');
                if ((!(ex.test(mobile))) || mobile.length != 10) {
                  console.log(mobile.length, ex.test(mobile));
                  growl.error("Please enter a valid Mobile Number", {ttl: 3000});
                  return false;
                }
              $scope.mobileNumber=mobile;
              var url='';
              $http({
                method:'GET',
                url:url,
                data:{customer_mobile:mobile}
              }).success(function(data){
                console.log("success",data);
                growl.success("Total Balance in user's account is "+data.total_balance);
                $scope.amount=data.total_balance;
                $scope.isValidated=true;
              }).error(function(err){
                console.log("error",err);
                if(err && err.status)
                  growl.error(err.status,{ttl:3000});
                else
                  growl.error("Could not connect with urban piper",{ttl:3000});
              })
            }

            $scope.redeem=function(){
              console.log($scope.enteredAmount);
              if($scope.mobileNumber) {
                if(isNaN($scope.enteredAmount)||Number($scope.enteredAmount<=0)) growl.error("Invalid Amount",{ttl:3000});

                else if(Number($scope.enteredAmount)>Number($scope.amount)){
                  growl.error("Insufficient Balance in user's account",{ttl:3000});
                }
                else if(Number($scope.enteredAmount)>Number($scope.billAmount)){
                  growl.error("Invalid Amount",{ttl:3000})
                }
                else
                  $modalInstance.close({phone: $scope.mobileNumber, amount: Number($scope.enteredAmount)});
              }
              else
                growl.error("Something went wrong",{ttl:3000});
            }

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }],
          size: 'sm'
        });

        modalInstance.result.then(function (data) {
          console.log("method",data);
          this.redeemLoyaltyCashback(bill,data.phone,data.amount,otherForm);
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      },

      redeemLoyaltyCashback:function(bill,mobileNumber,amount,otherForm){
        var url='';
        var data={};
        data.customer_phone=mobileNumber;
        data.amount=amount;
           $http({
             method:'POST',
             url:url,
             data:data 
          }).success(function(data){
             $rootScope.paymentDone=true;
             bill.addOtherPayment(otherForm);
         }).error(function(data){
            growl.error("Redemption failed",{ttl:3000});
         })
      },

    sendBillDetailsToLoyaltyPartner:function(bill){
      var amount=(bill.getTotalBill() + bill.getKotsTotalBill()).toFixed(2);
      var data={};
      data.customer_mobile=bill._customer.mobile;
      data.bill_amount=amount;
      data.bill_no=bill.billNumber;
      data.order_id=bill._id;
      data.customer_name=data.customer
      var bill_details=[];
      if(bill._kots.length>0 && bill._kots[0].items)
        _.forEach(bill._kots,function(kot){
          _.forEach(kot.items,function(item){
            bill_details.push({"item_number":item.number,
              "item_name":item.name,
              "qty":item.quantity,
              "line_total":item.subtotal,
              "discount": '',
              "discount_type":''
            });
          });
        });
      else if(bill._items){
        _.forEach(bill._items,function(item){
          bill_details.push({"item_number":item.number,
            "item_name":item.name,
            "qty":item.quantity,
            "line_total":item.subtotal,
            "discount": '',
            "discount_type":''
          });
        })
      }
      console.log("bill_details",bill_details);

      $http({
        method: 'POST',
        url: url,
        data: data 
      }).success(function (data) {
        growl.success("Cashback credited to customer's account",{ttl:3000});
      }).error(function (err) {
        console.log(err);
      });
      }
 
    };
  });
