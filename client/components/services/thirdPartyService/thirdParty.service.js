'use strict';

angular.module('posistApp')
    .factory('ThirdPartyService', function ($http) {

    	var factory = {};

      factory.createPartner = function(partner){
        return $http.post('/api/partnerDetails', partner);
      };

      factory.updatePartner = function(partner){
        return $http.put('/api/partnerDetails/', partner);
      };

      factory.deletePartner = function(id){
        return $http.delete('/api/partnerDetails/'+id);
      };

      factory.allPartners = function(){
        return $http.get('/api/partnerDetails');
      };

    	factory.getPartners = function(deployment_id){
    		return $http.get('/api/partners?deployment_id='+deployment_id);
    	};

   		factory.activatePartner = function(partner){
   			return $http.post('/api/partners', partner);
   		};


   		factory.deactivatePartner = function(deployment_id, customer_key){
   			return $http.delete('/api/partners?deployment_id='+deployment_id+'&customer_key='+customer_key);
   		};

      factory.getPartnersByDeployment=function(deployment_id){
        return $http.get('/api/partnerDetails/'+deployment_id);
      }

        factory.updateDeploymentPartner = function(deployment_id, customer_key, data){
        return $http.put('/api/partners?deployment_id='+deployment_id+'&customer_key='+customer_key, data);
      };

      factory.getPartner=function(deployment_id,partner_name){
        return $http.get('/api/partners/'+partner_name+'?deployment_id='+deployment_id);
      }

      factory.execute_event = function(event){
        return $http.post('/api/partners/execute_event?deployment_id='+event.deployment_id+'&event_name='+event.event_name, {partners:event.partners});
      }

      factory.syncZomato = function(event){
        return $http.post('/api/partners/syncZomato?deployment_id='+event.deployment_id+'&event_name='+event.event_name, {partners:event.partners});
      }
      
   		return factory;
    });
