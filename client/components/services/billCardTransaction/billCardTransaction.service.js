/**
 * Created by Ranjeet Sinha on 7/30/2016.
 */
angular.module('posistApp')
    .factory('BillCardTransaction', function ( $resource) {

        return $resource('/api/billCardTransactions/:id/:controller',
            {
                id:"@_id"
            },
            {
                getTransactionsForReport:{method:'POST',isArray:false,params:{controller:'getTransactionsForReport'}},
                save: {method: 'POST', isArray:false},
                get: {method: 'GET', isArray:true},
                update: {method: 'PUT'},
                delete:{method: 'DELETE',isArray:false}
            }
        );
    });