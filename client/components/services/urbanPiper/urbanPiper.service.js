'use strict';

angular.module('posistApp')
  .factory('UrbanPiper', function UrbanPiper($location, $rootScope, $http, User, $cookieStore, $q, subdomain, Tab, localStorageService) {

    return {
      getUrbanPiperKeys: function (temp, callback) {
        var cb = callback || angular.noop;
        var deferred = $q.defer();
        var self=this;
        $http.get('/api/urbanPiper', {params:{'tenantId': temp.tenantId, 'deploymentId': temp.deploymentId}}).success(function (data) {
          deferred.resolve(data);
          return cb();
        }).error(function (err) {
          deferred.reject(err);
          return cb(err);
        }.bind(this));

        return deferred.promise;
      },

      createUrbanPiper: function (temp, callback) {
        console.log("in urban service");
        var cb = callback || angular.noop;
        return  $http.post('/api/urbanPiper/', temp).success(function (data) {
          return cb(data);
        }).error(function (err) {
          return cb(err);
        }.bind(this)).$promise;
      },

      deleteUrbanPiper: function (temp, callback) {
        var cb = callback || angular.noop;
        return  $http.post('/api/urbanPiper/delete', temp).success(function (data) {
          return cb(data);
        }).error(function (err) {
          return cb(err);
        }.bind(this)).$promise;
      },

      activateUrbanPiper: function (temp, callback) {
        var cb = callback || angular.noop;
        return  $http.post('/api/urbanPiper/update', temp).success(function (data) {
          return cb(data);
        }).error(function (err) {
          return cb(err);
        }.bind(this)).$promise;
      },

      addSelectionToUrbanPiper: function (temp, callback) {
        var postData = {items:{associations:[]},urbanPiperKeys:temp.urbanPiperKeys};
        _.forEach(temp.items, function (_item) {
          //console.log("upitem",_item);
          if( _item.editSelected){
            var deliveryTab = _.findLast(_item.tabs, {tabType:'delivery'});
            //console.log(deliveryTab);
            var taxes = [{
              "name": "service tax",
              "type": "percent",
              value: 0
            },{
              "name": "vat",
              "type": "percent",
              value: 0
            }]
            if(deliveryTab){
                //console.log(itm)
                if (_.has(deliveryTab, 'taxes')) {
                  _.forEach(deliveryTab.taxes, function (tax) {
                    if ((tax.name.match(/service/i)|| tax.name.match(/surcharge/i))&& tax.type == 'percentage') {
                      taxes[0].value = tax.value;
                    }
                    else if (tax.name.match(/vat/i) && tax.type == 'percentage' && !tax.name.match(/surcharge/i)) {
                      taxes[1].value = tax.value;
                    }
                  })
                }
            }
            var itemToPush = {};
            itemToPush = {"item_ref_id": _item._id,
              "location_ref_id": _item.deployment_id,
              "title": _item.name,
              "price": (deliveryTab && deliveryTab.item)?deliveryTab.item.rate:_item.rate,
              "description": _item.description,
              "active": true,
              "taxes": taxes
            }
            console.log(itemToPush);
            postData.items.associations.push(itemToPush);
          }
        });
        var cb = callback || angular.noop;

        return  $http.post('api/urbanPiper/postItems',postData).success(function(success){
          console.log(success)
          return cb(success);
        }).error(function(err){
          console.log(err)
          return cb(err);
        }.bind(this)).$promise;
      },
      addItemToUrbanPiper: function (temp, callback) {
        var postData = {item:{},urbanPiperKeys:temp.urbanPiperKeys, itemId:temp.item._id, depId:temp.item.deployment_id};

        var deliveryTab = _.find(temp.item.tabs, {tabType:'delivery'});
        var taxes = [{
          "name": "service tax",
          "type": "percent",
          value: 0
        },{
          "name": "vat",
          "type": "percent",
          value: 0
        }]
        if(deliveryTab){
          if (_.has(deliveryTab, 'taxes')) {
            _.forEach(deliveryTab.taxes, function (tax) {
              if ((tax.name.match(/service/i)||tax.name.match(/surcharge/i)) && tax.type == 'percentage') {
                taxes[0].value = tax.value;
              }
              else if (tax.name.match(/vat/i) && tax.type == 'percentage' && !tax.name.match(/surcharge/i)) {
                taxes[1].value = tax.value;
              }
            })
          }
        }

        postData.item = {"association":{ "title": temp.item.name,
                          "price": (deliveryTab && deliveryTab.item)?deliveryTab.item.rate:temp.item.rate,
                          "description": temp.item.description,
                          "active": true,
                          "taxes" : taxes
                        }}
        var cb = callback || angular.noop;

        return  $http.post('api/urbanPiper/postItem',postData).success(function(success){
          return cb(success);
        }).error(function(err){
          return cb(err);
        }.bind(this)).$promise;
      },
      updateItemInUrbanPiper: function (temp, callback) {
        var postData = {item:{},urbanPiperKeys:temp.urbanPiperKeys, itemId:temp.item._id, depId:temp.item.deployment_id};
        console.log(temp);
        var tabId = _.find(temp.tabs,{tabType:'delivery'});
        var deliveryTab = _.find(temp.item.tabs, {_id:tabId._id});
        console.log(deliveryTab)
        var taxes = [{
                          "name": "service tax",
                          "type": "percent",
                          value: 0
                        },{
                          "name": "vat",
                          "type": "percent",
                          value: 0
                        }]
        if(deliveryTab){
          if(temp.item.urbanPiperId>0){
            //console.log(itm)
            if (_.has(deliveryTab, 'taxes')){
              _.forEach(deliveryTab.taxes, function (tax) {
                if((tax.name.match(/service/i)||tax.name.match(/surcharge/i)) && tax.type == 'percentage'){
                  taxes[0].value = tax.value;
                }
                else if(tax.name.match(/vat/i) && tax.type == 'percentage' && !tax.name.match(/surcharge/i)){
                  taxes[1].value = tax.value;
                }
              })
            }
          }
        }
        postData.item = {"association":{ "title": temp.item.name,
                          "price": temp.item.rate,
                          "description": temp.item.description,
                          "active": true,
                          "taxes" : taxes
                        }}
        var cb = callback || angular.noop;
        return  $http.post('api/urbanPiper/updateItem',postData).success(function(success){
          return cb(success);
        }).error(function(err){
          return cb(err);
        }.bind(this)).$promise;
      },

      deleteItemInUrbanPiper: function (temp, callback) {
        var postData = {item:{},urbanPiperKeys:temp.urbanPiperKeys, itemId:temp.item._id, depId:temp.item.deployment_id};
        postData.item = {"association":{ "title": temp.item.name,
          "price": temp.item.rate,
          "description": temp.item.description,
          "active": false,
          "taxes" : []
        }}
        var cb = callback || angular.noop;

        return  $http.post('api/urbanPiper/updateItem',postData).success(function(success){
          return cb(success);
        }).error(function(err){
          return cb(err);
        }.bind(this)).$promise;
      },

      updateItemLocationAssociation:function(temp,callback){
       var postData = {item:{},urbanPiperKeys:temp.urbanPiperKeys};
        console.log(temp)
        var tabId = _.find(temp.tabs,{tabType:'delivery'});
        var deliveryTab = _.find(temp.item.tabs, {_id:tabId._id});
        console.log(deliveryTab)
        var taxes = [{
                          "name": "service tax",
                          "type": "percent",
                          value: 0
                        },{
                          "name": "vat",
                          "type": "percent",
                          value: 0
                        }]
        if(deliveryTab){
          if(temp.item.urbanPiperId>0){
            //console.log(itm)
            if (_.has(deliveryTab, 'taxes')){
              _.forEach(deliveryTab.taxes, function (tax) {
                if(tax.name.match(/service/i) && tax.type == 'percentage'){
                  taxes[0].value = tax.value;
                }
                else if(tax.name.match(/vat/i) && tax.type == 'percentage'){
                  taxes[1].value = tax.value;
                }
              })
            }
          }
        }
        postData.item = {"associations":[{
                          "item_ref_id":temp.item._id,
                          "location_ref_id":temp.item.deployment_id,
                          "title": (deliveryTab && deliveryTab.item)?deliveryTab.item.name:temp.item.name,
                          "price": (deliveryTab && deliveryTab.item)?parseInt(deliveryTab.item.rate):temp.item.rate,
                          "description": (deliveryTab && deliveryTab.item)?deliveryTab.item.description:temp.item.description,
                          "active": true,
                          "taxes" : taxes
                        }]}
        if(temp.item.outOfStock==true)
         postData.item.associations[0].current_stock=0;
        var cb = callback || angular.noop;
        return  $http.post('api/urbanPiper/updateItemLocationAssociation',postData).success(function(success){
          return cb(success);
        }).error(function(err){
          return cb(err);
        }.bind(this)).$promise;
      },
      updateItemsInUrbanPiper: function (temp, callback) {
        //var d = $q.defer();
        var postData = {items:{associations:[]},urbanPiperKeys:temp.urbanPiperKeys};
        var tabId = _.find(temp.tabs,{tabType:'delivery'});
        console.log(tabId);
          _.forEach(temp.items, function (itm) {

            var deliveryTab = _.find(itm.tabs, {_id:tabId._id});
            console.log(deliveryTab,itm,itm.urbanPiperId)
            if(deliveryTab){
              if(itm.urbanPiperId>0){
                console.log(itm);
                var taxes = [{
                              "name": "service tax",
                              "type": "percent",
                              value: 0
                            },{
                              "name": "vat",
                              "type": "percent",
                              value: 0
                            }]
                if (_.has(deliveryTab, 'taxes')){
                  _.forEach(deliveryTab.taxes, function (tax) {
                    if((tax.name.match(/service/i)||tax.name.match(/surcharge/i)) && tax.type == 'percentage'){
                      taxes[0].value = tax.value;
                    }
                    else if(tax.name.match(/vat/i) && tax.type == 'percentage' && !tax.name.match(/surcharge/i)){
                      taxes[1].value = tax.value;
                    }
                  })
                }
                var tempItem = {}
                tempItem = {  "item_ref_id": itm._id,
                              "location_ref_id": itm.deployment_id,
                              "title": deliveryTab.item?deliveryTab.item.name:itm.name,
                              "price": deliveryTab.item?parseInt(deliveryTab.item.rate):itm.rate,
                              "description": deliveryTab.item?deliveryTab.item.description:itm.description,
                              "active": true,
                              "taxes" : taxes
                            }
                console.log(taxes,deliveryTab.item.taxes)

                postData.items.associations.push(tempItem);

              }
            }
          });
          console.log(postData)

          var cb = callback || angular.noop;


          return  $http.post('api/urbanPiper/updateItems',postData).success(function(success){
                    return cb(success);
                  }).error(function(err){
                    return cb(err);
                  }.bind(this)).$promise;
        // });
      },

      getDeploymentKeys:function(temp){
        var deferred = $q.defer();

       if(!$rootScope.urbanPiperSettings){
        $http.get('/api/urbanPiper', {params:{'tenantId': temp.tenantId, 'deploymentId': temp.deploymentId}}).success(function (data) {
          if(data[0].active==true){
           $rootScope.urbanPiperSettings={username:data[0].username,apiKey:data[0].apiKey,activated:true}
          deferred.resolve($rootScope.urbanPiperSettings);
         }
         else{
          $rootScope.urbanPiperSettings={activated:false};
          deferred.reject($rootScope.urbanPiperSettings);
         }
        }).error(function (err) {
          deferred.reject(err);
        });
       }
       else if($rootScope.urbanPiperSettings && $rootScope.urbanPiperSettings.apiKey)
         deferred.resolve($rootScope.urbanPiperSettings);
       else
         deferred.reject({activated:false});

        return deferred.promise;
      },
    };
  });
