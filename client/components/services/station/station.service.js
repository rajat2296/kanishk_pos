'use strict';

angular.module('posistApp')
    .factory('Station', function ($resource) {

        return $resource('/api/stations/:id',
            {
                id: '@_id'
            },
            {
                saveData: {method: 'POST', isArray: true},
                get: {method: 'GET', isArray: true},
                findOne: {method: 'GET'},
                update: {method: 'PUT'}
            }
        );

    });
