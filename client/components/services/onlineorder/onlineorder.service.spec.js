'use strict';

describe('Service: onlineorder', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var onlineorder;
  beforeEach(inject(function (_onlineorder_) {
    onlineorder = _onlineorder_;
  }));

  it('should do something', function () {
    expect(!!onlineorder).toBe(true);
  });

});
