'use strict';

angular.module('posistApp')
    .factory('OnlineOrder', function ($resource) {
      return $resource('/api/onlineorders/:id:controller',
          {
            id:"@_id"
          },
          {
            executeUrl: {params:{controller:'executeUrl'}, method: 'POST', isArray:false},
              modifyUrl: {params:{controller:'modifyUrl'}, method: 'POST', isArray:false},
            updateOrderModify: {params:{controller:'updateOrderModify'}, method: 'POST', isArray:false},
            get: {method: 'GET', isArray:true},
            update:{method:'PUT',isArray:false},
            delete:{method:'DELETE',isArray:false},
            refundTransaction: {params:{controller:'refundTransaction'}, method: 'POST', isArray:false}
          }
      );
    });
