'use strict';

describe('Service: codeApplied', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var codeApplied;
  beforeEach(inject(function (_codeApplied_) {
    codeApplied = _codeApplied_;
  }));

  it('should do something', function () {
    expect(!!codeApplied).toBe(true);
  });

});
