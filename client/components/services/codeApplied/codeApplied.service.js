'use strict';

angular.module('posistApp')
    .factory('CodeApplied', function ($resource) {
      return $resource('/api/codeApplieds/:controller/:id',
          {
            id:"@_id"
          },
          {
            destroy:{method:'delete',isArray:false},
            update:{method:'PUT',isArray:false},
            create: {method: 'POST', isArray:false},
            get: {method: 'GET', isArray:true},
            countCodes: {method: 'GET',params:{controller:'countCodes'}, isArray:false}
          }
      );
    });
