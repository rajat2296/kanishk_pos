'use strict';

angular.module('posistApp')
    .factory('Item', function ($resource) {
        return $resource('/api/items/:id/:controller',
            {
                id:"@_id"
            },
            {
                saveData: {method: 'POST', isArray: true},
                updateItemsToAddons: { method: 'POST', isArray: false,params:{controller:'updateItemsToAddons'}},
                updateMapEmailOrder: { method: 'POST', isArray: false,params:{controller:'updateMapEmailOrder'}},
                get: {method: 'GET', isArray: true},
                getItemNumber:{method:'GET',isArray:false},
                getItemsName:{method:'GET',params:{controller:'getItemsName'},isArray:true},
                getItemByDepoyment:{method:'GET',params:{controller:'getItemByDepoyment'},isArray:true},
                update: {method: 'PUT'},
                getProcessedItem: {method: 'GET', params: {controller: "getProcessedItem"}, isArray: true},
                getProcessedItemOptimized: {method: 'GET', params: {controller: "getProcessedItemOptimized"}, isArray: true},
                getSemiProcessedItem: {method: 'GET', params: {controller: "getSemiProcessedItem"}, isArray: true},
                getSemiProcessedItemOptimized: {method: 'GET', params: {controller: "getSemiProcessedItemOptimized"}, isArray: true},
                getProcessedItemWithRecipe: {method: 'GET', params: {controller: "getProcessedItemWithRecipe"}, isArray: true},
                deleteFallback: {method: 'DELETE', params: {controller: "deleteFallback"}, isArray: false},
                deleteMultipleItems: {method: 'PUT', params: {controller: "deleteMulti"}, isArray: true},
                deleteMultiFallback: {method: 'PUT', params: {controller: "deleteMultiFallback"}, isArray: true},
                setTabRate: {method: 'PUT', params: {controller: "setTabRate"}, isArray: false},
                mapTaxes: {method: 'PUT', params: {controller: "mapTaxes"}, isArray: false},
                getItemsAndCategoriesName: {method: 'GET', params: {controller: "getItemsAndCategoriesName"}, isArray: true},
                createMulti: {method: 'POST', params: {controller: "multi"}, isArray: false},
                validateItemUnitChange: {method: 'GET', params: {controller: "validateItemUnitChange"}, isArray: false},
                validateItemForDeletion: {method: 'GET', params: {controller: "validateItemForDeletion"}, isArray: false},
                sendMailOnDeletion:{method: 'GET', params: {controller: "sendMailOnDeletion"}, isArray: true}
            }
        );
    });
