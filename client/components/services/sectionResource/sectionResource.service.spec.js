'use strict';

describe('Service: sectionResource', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var stationResource;
  beforeEach(inject(function (_stationResource_) {
    stationResource = _stationResource_;
  }));

  it('should do something', function () {
    expect(!!stationResource).toBe(true);
  });

});
