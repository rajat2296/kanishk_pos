'use strict';

angular.module('posistApp')
  .service('sectionResource', ['$resource', function ($resource) {

    return $resource('/api/sections/:id/:controller',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        delete: {method: 'DELETE'}
      }
    );
  }]);
