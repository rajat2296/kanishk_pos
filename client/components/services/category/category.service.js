'use strict';

angular.module('posistApp')
  .factory('Category', function ($resource) {
        return $resource('/api/categorys/:id',
            {
                id:"@_id"
            },
            {
                saveData: {method: 'POST', isArray:true},
                get: {method: 'GET', isArray:true},
                update: {method: 'PUT'},
                updateMultiple: {method: 'PUT'},
                updateSectionInCategoryAndItems: {method: 'PUT', params: {controller: 'updateSectionInCategoryAndItems'}, isArray: false}
            }
        );
  });
