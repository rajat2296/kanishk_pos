'use strict';

angular.module('posistApp')
    .factory('Tax', function ($resource) {
        return $resource('/api/taxs/:id',
            {
                id:"@_id"
            },
            {
                saveData: {method: 'POST', isArray: true},
                get: {method: 'GET', isArray: true},
                update: {method: 'PUT'},
                cascade: {method: 'PUT'}
            }
        );
    });
