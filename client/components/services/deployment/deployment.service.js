'use strict';

angular.module('posistApp')
  .factory('Deployment', function ($resource) {

        return $resource('/api/deployments/:id/:controller',
            {
                id:'@_id'
            },
            {
                updateField:{method:'PUT',params:{controller:'updateField'},isArray:false},
                updateFieldEmailer:{method:'POST',params:{controller:'updateFieldEmailer'},isArray:false},
                updateFieldInstances:{method:'POST',params:{controller:'updateFieldInstances'},isArray:false},
                updateFieldMapOrder:{method:'POST',params:{controller:'updateFieldMapOrder'},isArray:false},
                createInstancePrint:{method:'POST',params:{controller:'createInstancePrint'},isArray:true},
                getInstancePrint:{method:'GET',params:{controller:'getInstancePrint'},isArray:false},
                createInstanceStationsPrint:{method:'POST',params:{controller:'createInstanceStationsPrint'},isArray:true},
                getInstanceStationsPrint:{method:'GET',params:{controller:'getInstanceStationsPrint'},isArray:false},
                getCurrentDeploymentDetails:{method: 'GET', params: {controller: 'getCurrentDeploymentDetails'}, isArray: false},
                saveData: {method: 'POST', isArray:true},
                getAllDeployments: {method: 'GET',params:{controller:'getAllDeployments'} , isArray: true},
                get: {method: 'GET', isArray: true},
                findOne: {method: 'GET',isArray:false},
                getSingleDeployment: {method: 'GET',params:{controller:'getSingleDeployment'},isArray:false},
                update:{method:'PUT'},

                getMasterDeployment: {
                    method: 'GET',
                    params: {
                        controller: 'getMasterDeployment'
                    }
                   
                },
                 getDeploymentsByTenantName: {
                    method: 'POST',
                    params: {
                        controller: 'getDeploymentsByTenantName'
                      },isArray:true
                    }
            }
        );

  });
