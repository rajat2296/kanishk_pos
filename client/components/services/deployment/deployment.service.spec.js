'use strict';

describe('Service: deployment', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var deployment;
  beforeEach(inject(function (_deployment_) {
    deployment = _deployment_;
  }));

  it('should do something', function () {
    expect(!!deployment).toBe(true);
  });

});
