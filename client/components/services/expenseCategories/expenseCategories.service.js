'use strict';

angular.module('posistApp')
  .factory('expenseCategories', function ($resource) {
    return $resource('/api/expenseCategories/:controller/:id', {
      id: '@_id'
    }, {
      saveData: {
        method: 'POST',
        isArray: false
      },
      get: {
        method: 'GET',
        isArray: true
      },
      findOne: {
        method: 'GET',
        isArray: true
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'DELETE',
        isArray: false
      }
      /*,
      getRequirementReport: { method: 'GET', params: { controller: "getRequirementReport" }, isArray: true },
      getSuplyRequirementReport: { method: 'GET', params: { controller: "getSuplyRequirementReport" }, isArray: true },
      getRequirementBillNumber: { method: 'GET', params: { controller: "getRequirementBillNumber" }, isArray: true }
      */
    });
  });