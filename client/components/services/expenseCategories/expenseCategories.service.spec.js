'use strict';

describe('Service: expenseCategories', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var expenseCategories;
  beforeEach(inject(function (_expenseCategories_) {
    expenseCategories = _expenseCategories_;
  }));

  it('should do something', function () {
    expect(!!expenseCategories).toBe(true);
  });

});
