'use strict';

angular.module('posistApp')
    .factory('Tab', function ($resource) {
        return $resource('/api/tabs/:id/:controller',
            {
                id: '@_id'
            },
            {
                saveData: {method: 'POST', isArray: false},
                get: {method: 'GET', isArray: true},
                update: {method: 'PUT'},
                getTenantTabs: {
                    method: 'GET',
                    params: {
                        controller: 'getTenantTabs'
                    },
                    isArray: true
                },
                mapCategories: {
                    method: 'PUT',
                    params: {
                        controller: 'mapCategories'
                    },
                    isArray: false
                },
                getTabsByType: {
                    method: 'GET',
                    params: {
                        controller: 'getTabsByType'
                    },
                    isArray: true
                }
            }
        );
    });
