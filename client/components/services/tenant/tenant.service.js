'use strict';

angular.module('posistApp')
  .factory('Tenant', function ($resource) {
        return $resource('/api/tenants/:controller:id/', { id: '@_id' },

            {
                get:{
                    method: 'GET',isArray:false
                   },
                getServerTime: {
                    method: 'GET',
                    params: {
                        controller: 'getServerTime'
                    }
                },
                search: {
                    method: 'GET',
                    params: {
                        controller: 'search'
                    },isArray:true
                }
            }
        );
  });
