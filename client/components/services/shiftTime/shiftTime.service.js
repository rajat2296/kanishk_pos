'use strict';

angular.module('posistApp')
  .factory('ShiftTime', function ($resource) {
    // AngularJS will instantiate a singleton by calling "new" on this function
      return $resource('/api/shiftTime/:id/:controller',
          {
            id:"@_id"
          },
          {
            save: {method: 'POST', isArray:false},
            get: {method: 'GET', isArray:true},
            update: {method: 'PUT'},
            delete:{method: 'DELETE',isArray:false}
          }
      );
  });
