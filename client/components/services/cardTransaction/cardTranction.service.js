/**
 * Created by Ranjeet Sinha on 7/3/2016.
 */
'use strict';

angular.module('posistApp')
    .factory('CardTransaction', function ($q, $resource, $timeout, $http, $webSql, $modal, $state, Utils, $rootScope, growl, localStorageService,$ngBootbox, nSocket) {

    // Define the constructor function.
    function CardTransaction() {
        var self = this;
        self.amount= '';
        self.type= '';
        self.user= {};
        self.deployment_id= '',
        self.timeStamp= new Date();
        self.cardId= '';
        self.itemDetail={};
        self.billDetail={};
        self.paymentType='Cash';
    };
    return(CardTransaction);
});