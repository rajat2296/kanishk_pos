'use strict';

angular.module('posistApp')
  .factory('Utils', function () {

    var meaningOfLife = 42;
    return {
      isQueue:function(){
        return true;
      },
      asyncLoop: function (iterations, func, callback) {
        var index = 0;
        var done = false;
        var loop = {
          next: function () {
            if (done) {
              return;
            }

            if (index < iterations) {
              index++;
              func(loop);

            } else {
              done = true;
              callback();
            }
          },

          iteration: function () {
            return index - 1;
          },

          break: function (d) {
            done = true;
            callback(d);
          }
        };
        loop.next();
        return loop;
      },
      arrayObjectIndexOf: function (arraytosearch, key, valuetosearch) {
        for (var i = 0; i < arraytosearch.length; i++) {
          if (arraytosearch[i][key] == valuetosearch) {
            return i;
          }
        }
        return -1;
      },
      getCategoryPages: function (categories) {
        var _o = [];
        var _t = angular.copy(categories);
        var _p = Math.ceil(_t.length / 6);
        for (var i = 0; i < _p; i++) {
          _o[i] = _t.splice(0, 6);
        }
        return _o;
      },
      getItemPages: function (items, pages) {
        var _o = [];
        var _t = angular.copy(items);
        var _p = Math.ceil(_t.length / pages);
        for (var i = 0; i < _p; i++) {
          _o[i] = _t.splice(0, pages);
        }
        return _o;
      },

      guid: function () {
        var s4 = function () {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        };

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4()+ '-' +s4() + s4() + '-' + s4() + '-' + s4()+ s4()+ s4() ;
      },
      getRandomEmail: function () {
        var s4 = function () {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        };
        return s4() + '@' + s4() + s4() + '.com';
      },
      hasSetting: function (settingName, settingsArray, tabtype) {
        // console.log(settingsArray);
        var flag = false;
        var _f = _.filter(settingsArray, {name: settingName});
        if (_f.length > 0) {
          if (_.has(_f[0], 'tabs')) {
            var _ft = _.filter(_f[0].tabs, {tabType: tabtype})
           if(_ft.length>0) {
             flag = _ft[0].selected;
           }
          } else {
            flag = _f[0].selected;
          }
        }
        return flag;
      },
      hasUserPermission:function(permissions,permissionName){
        var flag=false;
        for(var i=0;i<permissions.length;i++){
         /*will activate later for garnular permission*/
         // if(permissions[i].name===permissionName){
           // flag=permissions[i].selected;
          if(permissions[i].group===permissionName){
            flag=true;
            break;
          }
        }
        return flag;
      },
        //modified
      isUserPermitted: function(user, permissionLabel){
        var permissions = user.selectedPermissions;
        console.log(user.role);
        if(user.role === 'user'){
          var flag=false;
          for(var i=0;i<permissions.length;i++){
            /*will activate later for garnular permission*/
            if(permissions[i].label===permissionLabel){
              flag=true;
              break;
            }
          }
          return flag;
        } else return true;
      },
      //end
      isUserInRole:function(roles,roleName){
        var flag=false;
        for(var i=0;i<roles.length;i++){
          if(roles[i].name===roleName){
            flag=true;
          }
        }
        return flag;
      },
      hasPermission:function(permissions,permissionName){
        var flag=false;
        for(var i=0;i<permissions.length;i++){
          /*will activate later for garnular permission*/
           if(permissions[i].name===permissionName){
             flag=true;
            break;
          }
        }
        return flag;
      },

      getSettingValue: function (settingName, settingsArray) {
        var _f = _.filter(settingsArray, {name: settingName});
        if (_f) {
          if(_f[0]==undefined){return undefined}
          else
          return _f[0].value;
        } else {
          return undefined;
        }
      },
      getSMSSettingValue: function (settingName, settingsArray) {
        var _f = _.filter(settingsArray, {type: settingName});
        if (_f) {
          return _f[0];
        } else {
          return {isApplied:false};
        }
      },

      getPassCode: function (passCode, users,roles) {
        //console.log(users);

        var _f = _.filter(users, {passcode: passCode});
        if (_f.length > 0) {
          //console.log(roles);
          if(roles!=undefined){
           var rolesArr=roles.split(',');
            if(rolesArr.length>0) {
              for(var j=0;j<rolesArr.length;j++) {
                for (var i = 0; i < _f.length; i++) {
                  if(_.has( _f[i],'selectedRoles')){
                    for(var k=0;k < _f[i].selectedRoles.length;k++) {
                      if (rolesArr[j] == _f[i].selectedRoles[k].name) {
                        return passCode;
                      }
                    }
                  }
                }
              }
            }
          }else {
            return _f[0].passcode;
          }
        } else {
          return undefined;
        }
      },
      roundNumber: function (number, decimals) {
        var value = number;
        var exp = decimals;
        if (typeof exp === 'undefined' || +exp === 0)
          return Math.round(value);
        value = +value;
        exp = +exp;
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
          return NaN;
        // Shift
        value = value.toString().split('e');
        value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));
        // Shift back
        value = value.toString().split('e');
       // return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
        var rounded= +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
        return parseFloat( rounded.toFixed(decimals));
      },
      parseTime: function (timeStr, dt) {
        if (!dt) {
          dt = new Date();
        }

        var time = timeStr.match(/(\d+)(?::(\d\d))?\s*(p?)/i);
        if (!time) {
          return NaN;
        }
        var hours = parseInt(time[1], 10);
        if (hours == 12 && !time[3]) {
          hours = 0;
        }
        else {
          hours += (hours < 12 && time[3]) ? 12 : 0;
        }

        dt.setHours(hours);
        dt.setMinutes(parseInt(time[2], 10) || 0);
        dt.setSeconds(0, 0);

        return dt;
      },
      getDateFormatted: function (datetime) {
        // if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        //   var datetime = datetime.toString();
        //   var cdate = new Date(datetime.replace(' ', 'T'));
        // }
        // else{
        //   var cdate = datetime;
        // }
        var cdate = typeof(datetime)=='string'?new Date(datetime):datetime;
        var month = (cdate.getMonth() + 1).toString();
        var day = (cdate.getDate()).toString();
        var hrs = cdate.getHours().toString();
        var minutes = cdate.getMinutes().toString();
        var ss = cdate.getSeconds().toString();
        month = ((month.length == 1) ? "0" + month : month);
        day = ((day.length == 1) ? "0" + day : day);
        hrs = ((hrs.length == 1) ? "0" + hrs : hrs);
        minutes = ((minutes.length == 1) ? "0" + minutes : minutes);
        ss = ((ss.length == 1) ? "0" + ss : ss);
        //alert(day);
        //if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)
        //  return cdate.getFullYear() + "/" + month + "/" + day + " " + hrs + ":" + minutes + ":" + ss;
        //else
          return cdate.getFullYear() + "-" + month + "-" + day + " " + hrs + ":" + minutes + ":" + ss;
      },
      getDateFormattedHrs: function (datetime) {
         if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
           var datetime1 = datetime.toString();
           var cdate = new Date(datetime1.replace(/-/g, '/'));
         }
         else{
           var cdate = typeof(datetime)=='string'?new Date(datetime):datetime;
         }
        //var cdate = datetime;
        var month = (cdate.getMonth() + 1).toString();
        var day = (cdate.getDate()).toString();
        var hrs = cdate.getHours().toString();
        var minutes = cdate.getMinutes().toString();
        var ss = cdate.getSeconds().toString();
        month = ((month.length == 1) ? "0" + month : month);
        day = ((day.length == 1) ? "0" + day : day);
        hrs = ((hrs.length == 1) ? "0" + hrs : hrs);
       // minutes = ((minutes.length == 1) ? "0" + minutes : minutes);
       // ss = ((ss.length == 1) ? "0" + ss : ss);
        //alert(day);
        // if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)
        //   return cdate.getFullYear() + "/" + month + "/" + day + " " + hrs + ":" + 0 + ":" + 0;
        // else
        //   return cdate.getFullYear() + "-" + month + "-" + day + " " + hrs + ":" + 0 + ":" + 0;
        return cdate.getFullYear() + "-" + month + "-" + day + " " + hrs
      },
      getDateFormattedDate: function (datetime) {
        // if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        //   var datetime = datetime.toString();
        //   var cdate = new Date(datetime.replace(' ', 'T'));
        // }
        // else{
        //   var cdate = datetime;
        // }
        var cdate = datetime;
        var month = (cdate.getMonth() + 1).toString();
        var day = (cdate.getDate()).toString();
        month = ((month.length == 1) ? "0" + month : month);
        day = ((day.length == 1) ? "0" + day : day);
        //alert(day);
          return cdate.getFullYear() + "-" + month + "-" + day;
      },
      getMonthDate: function (datetime) {
        // if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        //   var datetime = datetime.toString();
        //   var cdate = new Date(datetime.replace(' ', 'T'));
        // }
        // else{
        //   var cdate = datetime;
        // }
        var cdate = datetime;
        var month = (cdate.getMonth() + 1).toString();
        var day = (cdate.getDate()).toString();
        month = ((month.length == 1) ? "0" + month : month);
        day = ((day.length == 1) ? "0" + day : day);
        //alert(day);

        return  month + "-" + day ;
      },
      getDate: function (datetime) {
        // if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        //   var datetime = datetime.toString();
        //   var cdate = new Date(datetime.replace(' ', 'T'));
        // }
        // else{
        //   var cdate = datetime;
        // }
        var cdate = datetime;
        //var month = (cdate.getMonth() + 1).toString();
        var day = (cdate.getDate()).toString();
        //month = ((month.length == 1) ? "0" + month : month);
        day = ((day.length == 1) ? "0" + day : day);
        //alert(day);
        return   day ;
      },
      convertDate: function (d) {

        return (
          d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0], d[1], d[2]) :
              d.constructor === Number ? new Date(d) :
                d.constructor === String ? new Date(d) :
                  typeof d === "object" ? new Date(d.year, d.month, d.date) :
                    NaN
        );
      },
      getResetDate:function(settings,date){
        
       var resetDate=this.convertDate(date);
        var hr=0;
        var min = 0;
        for(var i=0;i<settings.length;i++){
          if(settings[i].name=='day_sale_cutoff'){
            hr=this.convertDate(settings[i].value).getHours();
            min=this.convertDate(settings[i].value).getMinutes();
             //console.log(this.convertDate(settings[i].value),hr)
            break;
          }
        }

        resetDate.setHours(hr,min,0,0);
        return resetDate;
      },
      getResetDateAttendance:function(settings,date){
       var resetDate=this.convertDate(date);
        var hr=0;
        hr=this.convertDate(settings).getHours();

        resetDate.setHours(hr,0,0,0);
        return resetDate;
      },
              /*Offer Part Start here*/
        offerValidated:function(offer){
        var currentDate=this.getDateFormattedDate( new Date());

       /*Validation on date*/
       //console.log(moment(currentDate).isBetween(this.getDateFormattedDate(new Date( offer.valid.date.startDate)),this.getDateFormattedDate(new Date( offer.valid.date.endDate))))
        if(!(offer.valid.date.startDate==null ||offer.valid.date.startDate==''||offer.valid.date.endDate==null || offer.valid.date.endDate=='')){
         //if( (this.getDateFormattedDate(currentDate)>this.getDateFormattedDate(offer.valid.date.startDate)) &&(this.getDateFormattedDate(currentDate)<this.getDateFormattedDate(offer.valid.date.endDate))){
           if(moment(currentDate).isBetween(this.getDateFormattedDate(new Date( offer.valid.date.startDate)),this.getDateFormattedDate(new Date( offer.valid.date.endDate)))){
             console.log('validated date');
         }else {
            return false;
         }
        }else{
          console.log('validated date');
        }
        
        //var arr = "2010-03-15 10:30:00".split(/[- :]/), date = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]); console.log(date);
        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
          var arr2 = this.getDateFormattedDate(new Date())+" "+ moment( new Date(offer.valid.time.startTime)).format("HH:mm");
          var arr = arr2.split(/[- :]/);
          var arr3 = this.getDateFormattedDate(new Date())+" "+ moment(new Date(offer.valid.time.endTime)).add(-1,'seconds').format("HH:mm");
          var arr1 = arr3.split(/[- :]/);
          var startTime= moment(new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4]));
          var endTime= moment(new Date(arr1[0], arr1[1]-1, arr1[2], arr1[3], arr1[4]));
        }
        else{
          var startTime= moment(new Date( this.getDateFormattedDate(new Date())+" "+ moment( new Date(offer.valid.time.startTime)).format("LT")));
          var endTime= moment( new Date( this.getDateFormattedDate(new Date())+" "+ moment(new Date(offer.valid.time.endTime)).add(-1,'seconds').format("LT")));
        }
        var currentTime=moment();
        if((startTime.isBefore(currentTime) && currentTime.isBefore(endTime))){
          console.log('validated time');
        }else{
          return false;
        }

        /*Validation on days*/
        var isDayFound=false;
        for(var i=0;i<offer.valid.days.length;i++){
          if(moment.weekdays(new Date().getDay()).toUpperCase()==offer.valid.days[i].toUpperCase()){
            isDayFound=true;
            break;
          }
        }
        if(isDayFound){console.log('validated day'); }else{return false;}

        /*Validate dateList*/
        var dateLists=offer.valid.dateLists;
        var isMatchedDateList=false;
        for(var i=0;i<dateLists.length;i++ ){
            for(var j=0;j<dateLists[i].dates.length;j++){
              if(dateLists[i].repeat=='daily'){
                if(dateLists[i].dates[j].selected) {
                  if (this.getDateFormattedDate(new Date( dateLists[i].dates[j].date)) == this.getDateFormattedDate(new Date())) {
                    isMatchedDateList=true;
                    break;
                  }
                }
              }
              if(dateLists[i].repeat=='monthly'){
                if(dateLists[i].dates[j].selected) {
                  if (this.getDate(new Date( dateLists[i].dates[j].date)) == this.getDate(new Date())) {
                    isMatchedDateList=true;
                    break;
                  }
                }
              }
              if(dateLists[i].repeat=='yearly'){
                if(dateLists[i].dates[j].selected) {
                  if (this.getMonthDate(new Date( dateLists[i].dates[j].date)) == this.getMonthDate(new Date())) {
                    isMatchedDateList=true;
                    break;
                  }
                }
              }
            }
          }
       // console.log(isMatchedDateList);
        if(dateLists.length>0){
          if(offer.valid.behaviourDateList=='Include'){
            if(isMatchedDateList){console.log('validated dateList');}else{return false;}
          }else{
            if(!isMatchedDateList){console.log('validated dateList');}else{return false;}
          }
        }
        return true;
      },
      validateCode:function(code,offer){
        var currentDate=this.getDateFormattedDate( new Date());
        var flag=false;
      //  console.log(offer);
        if(_.has(offer,'code')) {
          if (_.has(offer.code, 'codeList')) {
            for (var i = 1; i <= offer.code.codeList.noOfCodes; i++) {
              // console.log(offer.code.codeList.codePrefix + i);
              if (code == (offer.code.codeList.codePrefix + i)) {
                if (!(offer.code.codeList.validFrom == null || offer.code.codeList.validFrom == '' || offer.code.codeList.validTo == null || offer.code.codeList.validTo == '')) {
                  //if( (this.getDateFormattedDate(currentDate)>this.getDateFormattedDate(offer.valid.date.startDate)) &&(this.getDateFormattedDate(currentDate)<this.getDateFormattedDate(offer.valid.date.endDate))){
                  if (moment(currentDate).isBetween(this.getDateFormattedDate(new Date(offer.code.codeList.validFrom)), this.getDateFormattedDate(new Date(offer.code.codeList.validTo)))) {
                    console.log('validated date');
                    flag = true;
                  }
                }else{
                  flag = true;
                }
                break;
              }
            }
          }
        }
      return flag;
      }
    }
  });
