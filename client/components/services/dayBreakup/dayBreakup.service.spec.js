'use strict';

describe('Service: dayBreakup', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var dayBreakup;
  beforeEach(inject(function (_dayBreakup_) {
    dayBreakup = _dayBreakup_;
  }));

  it('should do something', function () {
    expect(!!dayBreakup).toBe(true);
  });

});
