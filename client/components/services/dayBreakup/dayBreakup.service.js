'use strict';

angular.module('posistApp')
  .service('dayBreakup', ['$resource', function ($resource) {

    return $resource('/api/dayBreakup/:id/:controller',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        update: {method: 'PUT'},
        delete: {method: 'DELETE'}
      }
    );
  }]);
