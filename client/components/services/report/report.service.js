'use strict';

angular.module('posistApp').factory('Report', function ($q, $timeout, growl, Bill, $resource, $filter, Utils, localStorageService) {

  function Report() {

    var self = this;

    self.isGenerated = false;

    self.bills = []; // Collection of Bill Service Class
    self.allBills = [];
    self.kots = []; // Flatten Bills to KOTs, array of KOT objects
    self.allTaxes = [];
    self.totalSales = 0;
    self.totalTaxes = 0;
    self.totalDiscounts = 0;

    self.rows = [];

    self.rowsBreakDown = {
      daily: [],
      weekly: [],
      monthly: [],
      quarterly: [],
      half_yearly: [],
      yearly: []
    };

    self.filters = {
      fromDate: null,
      toDate: null,
      _fromDate: null,
      _toDate: null,
      tenant_id: null,
      deployment_id: null,
      name: null,
      superCategory_id: null,
      category_id: null,
      includeCutoffTime: true,
      byCutoffTime: 'true',
      type: 'daywise',
      cutOffTimeSetting: '',
      startTime: null,
      endTime: null,
      tabType: '',
      breakDownBy: '',
      period: 'daily',
      byTab: '',
      autoDateRange: 'today',
      deployment_settings: null,
      isComplimentary:false,
      discountValue:false,
      waiter_id: null
    };

    self.resource = $resource('/api/reports/:controller',
      {},
      {
        excelInvoiceDetails: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelInvoiceDetails'
          }
        },
        excelPaymentDetails: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelPaymentDetails'
          }
        },
        excelTaxSummary: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelTaxSummary'
          }
        },
        excelItemCategory: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelItemCategory'
          }
        },
        excelTabBreakDown: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelTabBreakDown'
          }
        },
        excelUserBreakDown: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelUserBreakDown'
          }
        },
        excelWaiterBreakDown: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelWaiterBreakDown'
          }
        },
        excelKotDetail: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelKotDetail'
          }
        },
        excelKotDeleteHistory: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelKotDeleteHistory'
          }
        },
        excelAverageBill: {
          method: 'POST',
          isArray: false,
          params: {
            controller: 'excelAverageBill'
          }
        }
      }
    );

    self.categories = [];
    self.superCategories = [];

    /* Bill Resource */
    self.billResource = $resource('/api/bills/:controller',
      {},
      {
        getDataRangeBillsNew: {
          method: 'GET',
          isArray: true,
          params: {
            controller: 'getDataRangeBillsNew'
          }
        },
        getDataRangeBills: {
          method: 'GET',
          isArray: true,
          params: {
            controller: 'getDataRangeBills'
          }
        },
        getDataRangeBillsCustom: {
          method: 'POST',
          isArray: true,
          params: {
            controller: 'getDataRangeBillsCustom'
          }
        },
        getCustomerCount: {
          method: 'GET',
          isArray: false,
          params: {
            controller: 'getCustomerCount'
          }
        }
      }
    );

  };

  Report.prototype = {
    reset: function () {
      var self = this;

      self.isGenerated = false;

      /* Reset Filters */
      /*self.filters = {
       fromDate: "",
       toDate: "",
       deployment_id: null,
       superCategory_id: null,
       category_id: null,
       includeCutoffTime: true,
       byCutoffTime: 'true',
       type: 'daywise',
       cutOffTimeSetting: '',
       startTime: null,
       endTime: null,
       tabType: '',
       breakDownBy: '',
       period: 'daily',
       byTab: ''
       };*/

      self.bills = []; // Collection of Bill Service Class
      self.kots = []; // Flatten Bills to KOTs, array of KOT objects
      self.allTaxes = [];
      self.totalSales = 0;
      self.totalTaxes = 0;
      self.totalDiscounts = 0;

      self.rows = [];

      self.rowsBreakDown = {
        daily: [],
        weekly: [],
        monthly: [],
        quarterly: [],
        half_yearly: [],
        yearly: []
      };

      self.categories = [];
      self.superCategories = [];
    },
    addBill: function (bill) {
      var self = this;
      var _b = _.extend(new Bill(), bill);
      this.bills.push(_b);
    },
    addBills: function (bills, tabType) {
      var d = $q.defer();
      var self = this;
      if (bills.length > 0) {
        angular.forEach(bills, function (bill) {
          if (tabType != null) {
            if (bill.tabType === tabType) {
              self.bills.push(_.extend(new Bill, bill));
            }
          } else {
            self.bills.push(_.extend(new Bill, bill));
          }
        });
        d.resolve(self.bills);
      } else {
        d.reject("No Bills");
      }
      return d.promise;
    },
    prepareBillsForReport: function () {
      var d = $q.defer();
      var self = this;

      var _pQ = [];
      angular.forEach(self.bills, function (bill, index) {
        //console.log(bill);
        _pQ.push(bill.prepareForReport(self.filters.cutOffTimeSetting, self.filters.deployment_settings));
      });

      $q.all(_pQ).then(function () {
        d.resolve();
      }).catch(function () {
        d.reject();
      });

      return d.promise;
    },
    prepareBillsForReportForKot: function () {
      var d = $q.defer();
      var self = this;

      var _pQ = [];
      //console.log("isnide1");
      angular.forEach(self.bills, function (bill, index) {
        //console.log(bill);
       // console.log("isnide");

        _pQ.push(bill.prepareForReportForKot(self.filters.cutOffTimeSetting, self.filters.deployment_settings));
      });

      $q.all(_pQ).then(function () {
        d.resolve();
      }).catch(function () {
        d.reject();
      });

      return d.promise;
    },
    aggregateEachBill: function () {
      var self = this;

      var d = $q.defer();
      var _allTaxes = [];
      $timeout(function () {

        // Create Global All Taxes Array
        angular.forEach(self.bills, function (bill) {
          angular.forEach(bill._taxes, function (tax) {
            _allTaxes.push({
              id:tax.id,
              name:tax.name
            });
          });
        });

        self.allTaxes = _.uniq(_allTaxes, 'id');
        //console.log(self.allTaxes);


        d.resolve();

      }, 100);


      return d.promise;
    },
    
    flattenAllBills: function () {
      var d = $q.defer();
      var self = this;

      //console.log(self.bills);

      $timeout(function () {

        var _allItems = [];
        //console.log("Flatten Bills: ", self.bills);
        angular.forEach(self.bills, function (bill) {
          //console.log(bill);
          console.log('billcompoi', bill.complimentary);
          var isBillComplementary = false;
          if(bill.complimentary){
            isBillComplementary = true;
            console.log('billcomplimentary', bill.complimentary);
          }
          angular.forEach(bill._items, function (item) {

            if(isBillComplementary){
              console.log('complimentary found');
              item.complementaryItemQty = item.quantity;
              item.nonComplementaryItemQty = 0;
            }
            else {
              item.complementaryItemQty = item.compItems;
              item.nonComplementaryItemQty = item.nonCompItems;
            }
           console.log(moment(bill.closeTime).format('YYYY-MM-DD'));
            item.billDay = moment(bill.closeTime).format('YYYY-MM-DD');
            item.billTime = moment(bill.closeTime).format('HH:mm:ss');
            item.billCloseTime = bill.closeTime;
             console.log(item.billDiscountAmount)
            /*Adding Add Ons*/
            if(_.has(item,'addOns')) {
              _.forEach(item.addOns,function(addon){
                item.subtotal+=addon.subtotal;
                _.forEach(addon.taxes,function(at){
                  item.taxes.push(at);
                })
              })
            }
            /*End Add Ons*/
            _allItems.push(item);
          });
        });

        /* Pipeline: Group by Date */
        var _groupedItems = [];
        _.chain(_allItems).groupBy('billDay').map(function (items) {
          _.chain(items).groupBy('id').map(function (gItemsById) {

            //console.log(gItemsById);

            var _itemTotalInEachTab = {
              _qty: 0,
              _amount: 0,
              discountedAmount:0,
              _tax: 0,
              _discount: 0,
              _taxes: [],
              _complementaryItemQty: 0,
              _nonComplementaryItemQty: 0,
              totalAmount:0
            };

            angular.forEach(gItemsById, function (itemInTab) {
              delete itemInTab['itemNet']; // Remove old Item Net and Recalculate
              //console.log(itemInTab);
              if(itemInTab.billDiscountAmount == undefined || !itemInTab.billDiscountAmount)
                itemInTab.billDiscountAmount = 0;
              _itemTotalInEachTab._qty += parseFloat(itemInTab.quantity);
              _itemTotalInEachTab._complementaryItemQty += parseFloat(itemInTab.complementaryItemQty);
              _itemTotalInEachTab._nonComplementaryItemQty += parseFloat(itemInTab.nonComplementaryItemQty);
              _itemTotalInEachTab.totalAmount += parseFloat(itemInTab.subtotal) ;
             
              _itemTotalInEachTab._amount += parseFloat(itemInTab.subtotal) - (parseFloat(itemInTab.billDiscountAmount) + parseFloat(itemInTab.discount));

              _itemTotalInEachTab._discount += parseFloat(itemInTab.discount) + parseFloat(itemInTab.billDiscountAmount);
              /* if( _itemTotalInEachTab.totalAmount ==  _itemTotalInEachTab._discount)
                { console.log('insideIf')
                  itemInTab.billDiscountAmount = 0;
                 _itemTotalInEachTab._discount += parseFloat(itemInTab.discount) 
                }*/
              _itemTotalInEachTab.discountedAmount += (parseFloat(itemInTab.subtotal) - (parseFloat(itemInTab.discount)+ parseFloat(itemInTab.billDiscountAmount)))

              /* Aggregate Taxes */
              angular.forEach(itemInTab.taxes, function (iTax) {
                //console.log(iTax.tax_amount);
                if (iTax.isConsidered) {
                  _itemTotalInEachTab._tax += parseFloat(iTax.tax_amount);
                }

                /* Tax Subtotal Amount */
                _itemTotalInEachTab._taxes.push({
                  id: iTax.id,
                  name: iTax.name + ' Amount',
                  tax_amount: (!_.isNaN(iTax.baseRate_preTax)) ? parseFloat(iTax.baseRate_preTax) : 0,
                  isConsidered: false
                });
                /* Tax Amount */
                _itemTotalInEachTab._taxes.push({
                  id: iTax.id,
                  name: iTax.name,
                  tax_amount: parseFloat(iTax.tax_amount),
                  isConsidered: true
                });

                if (iTax.length > 0) {
                  angular.forEach(iTax.cascadingTaxes, function (cITax) {
                    _itemTotalInEachTab._tax += parseFloat(cITax.tax_amount);
                    /* Tax Subtotal Amount */
                    _itemTotalInEachTab._taxes.push({
                      id: iTax.id,
                      name: iTax.name + ' Amount',
                      tax_amount: parseFloat(iTax.baseRate_preTax),
                      isConsidered: false
                    });
                    /* Tax Amount */
                    _itemTotalInEachTab._taxes.push({
                      id: iTax.id,
                      name: iTax.name,
                      tax_amount: parseFloat(iTax.tax_amount),
                      isConsidered: true
                    });
                  });
                }

              });

            });

            var allItemTaxes = [];
            angular.forEach(gItemsById, function (_it) {
              angular.forEach(_it.taxes, function (_itax) {
                allItemTaxes.push(_itax);
              });
            });

            //console.log("ALL ITEMS:", gItemsById);
            var _t = [];
            _.chain(allItemTaxes).groupBy('id').map(function (_iTaxes) {
              //console.log("GTA " ,_iTaxes);
              var _tAmt = 0;
              angular.forEach(_iTaxes, function (_t) {
                _tAmt += parseFloat(_t.tax_amount);
              });
              _t.push({
                id: _iTaxes[0].id,
                name: _iTaxes[0].name,
                isConsidered: _iTaxes[0].isConsidered,
                tax_amount: _tAmt
              })
            });


            var _net = 0;
            //_net = (parseFloat(_itemTotalInEachTab._amount) - parseFloat(_itemTotalInEachTab._discount)) + parseFloat(_itemTotalInEachTab._tax);
            /*Changes done by ranjeet to accomodate billwise discount*/
            _net = (parseFloat(_itemTotalInEachTab.totalAmount) - parseFloat(_itemTotalInEachTab._discount)) + parseFloat(_itemTotalInEachTab._tax);
                   console.log(_net,_itemTotalInEachTab._discount,_itemTotalInEachTab._amount)
            _groupedItems.push({
              id: gItemsById[0].id,
              name: gItemsById[0].name,
              tab: gItemsById[0].tab,
              billTime: gItemsById[0].billTime,
              billDay: gItemsById[0].billDay,
              billCloseTime: gItemsById[0].billCloseTime,
              category: gItemsById[0].category,
              superCategory: gItemsById[0].superCategory,
              totalQty: _itemTotalInEachTab._qty,
              totalAmount: _itemTotalInEachTab._amount,
              totalAmountNoDiscount:  _itemTotalInEachTab.totalAmount,
              totalDiscount: _itemTotalInEachTab._discount,
              discountAmount: _itemTotalInEachTab.discountedAmount,
              totalTax: _itemTotalInEachTab._tax,
              net: _net,
              roundOff: self.getRoundOff(_net),
              afterRoundOff: self.getAfterRoundOff(_net),
              taxes: _t,
              complementaryItemQty: _itemTotalInEachTab._complementaryItemQty,
              nonComplementaryItemQty: _itemTotalInEachTab._nonComplementaryItemQty

            });


          });
        });

        /* Create Global Taxes */
        var _gT = [];
        angular.forEach(self.bills, function (bill) {
          angular.forEach(bill._taxes, function (t) {
            _gT.push({
              id: t.id,
              name: t.name
            });
          });
        });
        _.chain(_gT).groupBy('id').map(function (taxes) {
          self.allTaxes.push({
            id: taxes[0].id,
            name: taxes[0].name
          });
        });

        d.resolve(_groupedItems);

      }, 100);

      return d.promise;
    },
    groupByDay: function () {

      var d = $q.defer();
      var self = this;
      var _allTaxes = [];
      $timeout(function () {
        _allTaxes = [];

        angular.forEach(self.bills, function (bill) {
          //console.log(bill._taxes);
          angular.forEach(bill._taxes, function (tax) {
            tax.tab = bill.tab;
            tax.billDay = bill.closeTime;
            //tax.billTime = moment(bill._closeTime).format('HH:mm:ss');
            //tax.billCloseTime = bill._closeTime;
            _allTaxes.push(tax);
          });
        });

        //console.log(_allTaxes);

        var _gtax = [];
        _.chain(_allTaxes).groupBy('billDay').map(function (gTax) {
          _.chain(gTax).groupBy('id').map(function (tTaxes) {
            //console.log(tTaxes);
            var _tTax = 0;
            angular.forEach(tTaxes, function (_tax) {
              _tTax += (!_.isNaN(_tax.tax_amount)) ? parseFloat(_tax.tax_amount) : 0;
            });
            _gtax.push({
              id: tTaxes[0].id,
              name: tTaxes[0].name,
              day: tTaxes[0].billDay,
              isConsidered: tTaxes[0].isConsidered,
              tax_amount: _tTax
            });
          });
        });

        //console.log(_gtax);
        //console.log(_allTaxes);

        /* Agrregate Day by CloseTime */
        var _dayRows = [];
        _.chain(self.bills).groupBy('closeTime').map(function (dayBills) {
          //console.log(dayBills);
          var _bill = {
            totalQty:0,
            totalAmount:0,
            totalDiscount: 0,
            totalTax: 0,
            totalCash: 0,
            totalDebit:0,
            totalCredit:0,
            totalOther:0,
            totalNet: 0,
            taxes: [],
            day: ''
          };

          //console.log(JSON.stringify(_gtax));
          angular.forEach(dayBills, function (b){
            //console.log(b);
            _bill.totalQty += parseFloat(b.totalQty);
            _bill.totalAmount += parseFloat(b.totalAmount);
            _bill.totalDiscount += parseFloat(b.totalDiscount);
            _bill.totalNet += parseFloat(b.totalNet);
            _bill.totalTax += parseFloat(b.totalTax);
            _bill.totalCash += parseFloat(b.totalCash);
            _bill.totalDebit += parseFloat(b.totalDebitCardAmount);
            _bill.totalCredit += parseFloat(b.totalCreditCardAmount);
            _bill.totalOther += parseFloat(b.totalOtherCardAmount);
          });

          /* TODO: After Roundoff */
          _bill.day = dayBills[0].closeTime;
          _bill.taxes = _.filter(_gtax, {day: dayBills[0].closeTime});
          _bill.totalNet = (parseFloat(_bill.totalAmount) + parseFloat(_bill.totalTax)) - parseFloat(_bill.totalDiscount);
          _bill.roundOff = self.getRoundOff(_bill.totalNet);
          _bill.afterRoundOff = self.getAfterRoundOff(_bill.totalNet);
          _bill.noOfBills = dayBills.length;
          _dayRows.push(_bill);
        });

        self.allTaxes = _.uniq(_gtax, 'id');

        self.rows = _dayRows;
        d.resolve(_dayRows);

      },100);
      return d.promise;
    },
    groupByTabType: function () {

      var d = $q.defer();
      var self = this;
      var _allTaxes = [];
      $timeout(function () {
        _allTaxes = [];

        angular.forEach(self.bills, function (bill) {
          //console.log(bill._taxes);
          if (!bill.isVoid) {
            angular.forEach(bill._taxes, function (tax) {
              tax.tab = bill.tab;
              tax.tabType = bill.tabType;
              tax.billDay = bill.closeTime;
              //tax.billTime = moment(bill._closeTime).format('HH:mm:ss');
              //tax.billCloseTime = bill._closeTime;
              _allTaxes.push(tax);
            });
          }

        });

        //console.log(_allTaxes);

        var _gtax = [];
        _.chain(_allTaxes).groupBy('tab').map(function (gTax) {
          _.chain(gTax).groupBy('id').map(function (tTaxes) {

            var _tTax = 0;
            angular.forEach(tTaxes, function (_tax) {
              _tTax += (!_.isNaN(_tax.tax_amount)) ? parseFloat(_tax.tax_amount) : 0;
            });
            _gtax.push({
              id: tTaxes[0].id,
              name: tTaxes[0].name,
              day: tTaxes[0].billDay,
              isConsidered: tTaxes[0].isConsidered,
              tax_amount: _tTax,
              tabType: tTaxes[0].tabType,
              tab: tTaxes[0].tab
            });
          });
        });

        //console.log(_gtax);
        //console.log(_allTaxes);

        /* Agrregate Day by CloseTime */
        var _dayRows = [];
        _.chain(self.bills).groupBy('tabType').map(function (dayBills) {
          //console.log(dayBills);
          var _bill = {
            totalQty:0,
            totalAmount:0,
            totalDiscount: 0,
            totalTax: 0,
            totalCash: 0,
            totalDebitCard:0,
            totalCreditCard:0,
            totalNet: 0,
            taxes: []
            //day: ''
          };

          //console.log(JSON.stringify(_gtax));

          angular.forEach(dayBills, function (b){
            if (!b.isVoid) {
              _bill.totalQty += parseFloat(b.totalQty);
              _bill.totalAmount += parseFloat(b.totalAmount);
              _bill.totalDiscount += parseFloat(b.totalDiscount);
              _bill.totalNet += parseFloat(b.totalNet);
              _bill.totalTax += parseFloat(b.totalTax);
              _bill.totalCash += parseFloat(b.totalCash);
              _bill.totalDebitCard += parseFloat(b.totalDebitCardAmount);
              _bill.totalCreditCard += parseFloat(b.totalCreditCardAmount);
            }
          });

          //_bill.day = dayBills[0].closeTime;
          _bill.tabType = dayBills[0].tabType;
          _bill.tab = dayBills[0].tab;
          _bill.taxes = _.filter(_gtax, {tab: dayBills[0].tab});
          _bill.totalNet = (parseFloat(_bill.totalAmount) + parseFloat(_bill.totalTax)) - parseFloat(_bill.totalDiscount);
          _bill.roundOff = self.getRoundOff(_bill.totalNet);
          _bill.afterRoundOff = self.getAfterRoundOff(_bill.totalNet);
          _bill.noOfBills = dayBills.length;
          _dayRows.push(_bill);


        });

        self.allTaxes = _.uniq(_gtax, 'id');

        self.rows = _dayRows;
        d.resolve(_dayRows);

      },100);

      return d.promise;
    },
    groupByItemItem: function () {
      var d = $q.defer();
      var self = this;

      $timeout(function () {

        var _items = [];

        //console.log(JSON.stringify(self.bills));


        // Cycle through each Bill and get Items from KOTs
        angular.forEach(self.bills, function (bill) {

          var isBillComplementary = false;
          if (!bill.isVoid) {
            if(bill.complimentary)
              isBillComplementary = true;
            angular.forEach(bill._kots, function (kot) {
              if (!kot.isVoid) {
                for(var it in kot.items) {
                  if(_.has(kot.items[it],'addOns')) {
                    _.forEach(kot.items[it].addOns,function(addon){
                      //item.subtotal+=addon.subtotal;
                      //_.forEach(addon.taxes,function(at){
                      //  item.taxes.push(at);
                      //})
                      kot.items.push(addon);
                    })
                  }
                }
                angular.forEach(kot.items, function (item) {

                  if(!item.category){
                    item.category = {categoryName: "NA", superCategory: {superCategoryName: "NA"}};
                  } else {
                    if(!item.category.superCategory){
                      item.category.superCategory = {superCategoryName: "NA"}
                    } else {
                      if(!item.category.superCategory.superCategoryName)
                        item.category.superCategory.superCategoryName = "NA";
                    }
                  }
                  if(isBillComplementary)
                    item.isComplementary = true;
                  else if(item.subtotal == item.discount)
                    item.isComplementary = true;
                  // Add Bill/ KOT info to Item
                  item.billNumber = bill.billNumber;
                  item.serialNumber = bill.serialNumber;
                  item.daySerialNumber = bill.serialNumber;
                  item.closeTime = bill._closeTime;
                  item.superCategory = item.category.superCategory.superCategoryName;
                  item.superCategoryId = item.category.superCategory._id;
                  item.categoryId = item.category._id;
                  item.category = item.category.categoryName;

                  item.tab = bill.tab;
                  item.tabType = bill.tabType;
                  item.tabId = bill.tabId;
                  /*Adding Add Ons*/
                  /*End Add Ons*/

                  // Create Item Tax Amount Column
                  var _tItemTax = 0;
                  var _newTaxAmtCols = [];
                  //console.log("Item Item ", item);
                  angular.forEach(item.taxes, function (tax) {

                    _tItemTax += parseFloat(tax.tax_amount);

                    // Add Tax Amount Col
                    _newTaxAmtCols.push({
                      id: tax._id + '_amt',
                      name: tax.name,
                      isConsidered: false,
                      tax_amount: tax.baseRate_preTax
                    });

                    // Add Tax Col
                    _newTaxAmtCols.push({
                      id: tax._id,
                      name: tax.name,
                      isConsidered: true,
                      tax_amount: tax.tax_amount
                    });

                    // Add Cascading Tax Col
                    angular.forEach(tax.cascadingTaxes, function (cTax) {
                      _tItemTax += parseFloat(cTax.tax_amount);
                      _newTaxAmtCols.push({
                        id: cTax._id,
                        name: cTax.name,
                        isConsidered: true,
                        tax_amount: cTax.tax_amount
                      })
                    });

                    // Add Cascading Tax Amount Col
                    angular.forEach(tax.cascadingTaxes, function (cTaxAmt) {
                      _newTaxAmtCols.push({
                        id: cTaxAmt._id + '_amt',
                        name: cTaxAmt.name,
                        isConsidered: false,
                        tax_amount: cTaxAmt.baseRate_preTax
                      });
                    });
                  });

                  // Map newly created Tax cols to Item

                  item._taxes = _newTaxAmtCols;

                  /* Set Total Tax on Item */
                  item.totalTax = parseFloat(_tItemTax);

                  // Set Total Discount on Item
                  var _iDiscount = 0;
                  if (_.has(item, 'discounts') && item.isDiscounted) {
                    angular.forEach(item.discounts, function (d) {
                      if (d.type === 'fixed') {
                        _iDiscount += parseFloat(d.value);
                      }
                      if (d.type === 'percentage') {
                        _iDiscount += parseFloat(d.discountAmount);
                      }
                    });
                  }
                  item.totalDiscount = _iDiscount;

                  //console.log(angular.toJson(item));
                  // Finally Push to _items after preparation
                  _items.push(item);

                });
              }
            });
          }
        });

        //console.log('Item Items', JSON.stringify(_items));
        self.groupByItemItems(_items).then(function (items) {
          //console.log(JSON.stringify(items));
          d.resolve(items);
        });

      });

      return d.promise;
    },
    groupByItemItems: function (_items) {
      //console.log(angular.copy(_items));
      var d = $q.defer();
      var self = this;
      //console.log(JSON.stringify(_items));
      $timeout(function () {
        var _tItems = [];

        _.chain(_items).groupBy('_id').map(function (items) {
          //console.log(items);
          var _iQty = 0, _iTax = 0, _iDiscount = 0, _iSubtotal = 0, _iTaxes = [], _iDiscount = 0, _net = 0, complementaryItemQty = 0, nonComplementaryItemQty = 0;
          angular.forEach(items, function (_i) {
            _iQty += parseFloat(_i.quantity);
            _iSubtotal += parseFloat(_i.subtotal);
            _iTax += parseFloat(_i.totalTax);
            _iDiscount += parseFloat(_i.totalDiscount);
            _net = parseFloat((parseFloat(_iSubtotal) - parseFloat(_iDiscount)) + parseFloat(_iTax));
            if(_i.isComplementary)
              complementaryItemQty += parseFloat(_i.quantity);
            else
              nonComplementaryItemQty += parseFloat(_i.quantity);
          });

          // GroupBy Taxes
          var _iTaxes = [];
          angular.forEach(items, function (it) {
            angular.forEach(it._taxes, function (iTax) {
              _iTaxes.push(iTax);
            })
          });

          var groupedTaxes = [];
          _.chain(_iTaxes).groupBy('id').map(function (gTaxes) {
            //console.log(JSON.stringify(gTaxes));
            var _tA = 0;
            angular.forEach(gTaxes, function (_t) {
              if (_.has(_t, 'tax_amount')) {
                if (_t.tax_amount != null || _t.tax_amount != undefined) {
                  _tA += parseFloat(_t.tax_amount);
                }
              }
            });

            groupedTaxes.push({
              id: gTaxes[0].id,
              name: gTaxes[0].name,
              isConsidered: gTaxes[0].isConsidered,
              tax_amount: parseFloat(_tA)
            });

          });

          //console.log(items[0].name , JSON.stringify(groupedTaxes));

          if(!items[0].category)
            items[0].category = "NA";
          if(!items[0].superCategory)
            items[0].superCategory = "NA";
          _tItems.push({
            id: items[0]._id,
            name: items[0].name,
            rate: items[0].rate,
            categoryId: items[0].categoryId,
            category: items[0].category,
            superCategoryId: items[0].superCategoryId,
            superCategory: items[0].superCategory,
            quantity: _iQty,
            subtotal: _iSubtotal,
            totalTax: _iTax,
            totalDiscount: _iDiscount,
            taxes: groupedTaxes,
            net: _net,
            tab:items[0].tab,
            tabType: items[0].tabType,
            tabId: items[0].tabId,
            complementaryItemQty: complementaryItemQty,
            nonComplementaryItemQty: nonComplementaryItemQty
          });


        });
        //console.log(JSON.stringify(_tItems));
        d.resolve(_tItems);

      });

      return d.promise;
    },

    groupByTabItem: function (rows) {
      var d = $q.defer();
      var self = this;

      $timeout(function () {

        var _items = [];

        //console.log(JSON.stringify(self.bills));


        /* Cycle through each Bill and get Items from KOTs */
        angular.forEach(self.bills, function (bill) {
          if (!bill.isVoid) {
            angular.forEach(bill._kots, function (kot) {
              if (!kot.isVoid) {
                angular.forEach(kot.items, function (item) {

                  /* Add Bill/ KOT info to Item */
                  item.billNumber = bill.billNumber;
                  item.serialNumber = bill.serialNumber;
                  item.daySerialNumber = bill.serialNumber;
                  item.tab = bill.tab;
                  item.tabType = bill.tabType;
                  item.tabId = bill.tabId;
                  item.username = bill._currentUser.username;
                  item.waitername = (_.has(bill._waiter, 'username')) ? bill._waiter.username : 'no_waiter_assigned';
                  item.deliveryname = (_.has(bill._delivery, 'username')) ? bill._delivery.username : 'no_delivery_assigned';
                  item.closeTime = bill._closeTime;
                  /*Adding Add Ons*/
                  if(_.has(item,'addOns')) {
                    _.forEach(item.addOns,function(addon){
                      item.subtotal+=addon.subtotal;
                      _.forEach(addon.taxes,function(at){
                        item.taxes.push(at);
                      })
                    })
                  }
                  /*End Add Ons*/

                  /* Create Item Tax Amount Column */
                  var _tItemTax = 0;
                  var _newTaxAmtCols = [];
                  angular.forEach(item.taxes, function (tax) {

                    _tItemTax += parseFloat(tax.tax_amount);

                    /* Add Tax Amount Col*/
                    _newTaxAmtCols.push({
                      id: tax._id + '_amt',
                      name: tax.name,
                      isConsidered: false,
                      tax_amount: tax.baseRate_preTax
                    });

                    /* Add Tax Col */
                    _newTaxAmtCols.push({
                      id: tax._id,
                      name: tax.name,
                      isConsidered: true,
                      tax_amount: tax.tax_amount
                    });

                    /* Add Cascading Tax Col */
                    angular.forEach(tax.cascadingTaxes, function (cTax) {
                      _tItemTax += parseFloat(cTax);
                      _newTaxAmtCols.push({
                        id: cTax._id,
                        name: cTax.name,
                        isConsidered: true,
                        tax_amount: cTax.tax_amount
                      })
                    });

                    /* Add Cascading Tax Amount Col */
                    angular.forEach(tax.cascadingTaxes, function (cTaxAmt) {
                      _newTaxAmtCols.push({
                        id: cTaxAmt._id + '_amt',
                        name: cTaxAmt.name,
                        isConsidered: false,
                        tax_amount: cTaxAmt.baseRate_preTax
                      });
                    });
                  });

                  /* Map newly created Tax cols to Item */
                  item._taxes = _newTaxAmtCols;

                  /* Set Total Tax on Item */
                  item.totalTax = parseFloat(_tItemTax);

                  /* Set Total Discount on Item */
                  var _iDiscount = 0;
                  if (_.has(item, 'discounts') && item.isDiscounted) {
                    angular.forEach(item.discounts, function (d) {
                      if (d.type === 'fixed') {
                        _iDiscount += parseFloat(d.value);
                      }
                      if (d.type === 'percentage') {
                        _iDiscount += parseFloat(d.discountAmount);
                      }
                    });
                  }
                  item.totalDiscount = _iDiscount;


                  /* Finally Push to _items after preparation */
                  _items.push(item);


                });
              }
            });
          }
        });

        //console.log(JSON.stringify(_items));
        self.groupByTabItems(_items).then(function (items) {
          //console.log(JSON.stringify(items));
          d.resolve(items);
        });

      });

      return d.promise;
    },
    groupByTabItems: function (_items) {
      var d = $q.defer();
      var self = this;

      $timeout(function () {
        var _tItems = [];

        _.chain(_items).groupBy('tabId').map(function (tabItems) {
          _.chain(tabItems).groupBy('_id').map(function (items) {

            var _iQty = 0, _iTax = 0, _iDiscount = 0, _iSubtotal = 0, _iTaxes = [], _iDiscount = 0, _net = 0;
            angular.forEach(items, function (_i) {
              _iQty += parseFloat(_i.quantity);
              _iSubtotal += parseFloat(_i.subtotal);
              _iTax += parseFloat(_i.totalTax);
              _iDiscount += parseFloat(_i.totalDiscount);
              _net = parseFloat((_iSubtotal - _iDiscount) + _iTax);
            });


            _tItems.push({
              id: items[0]._id,
              name: items[0].name,
              rate: items[0].rate,
              tab: tabItems[0].tab,
              tabType: tabItems[0].tabType,
              tabId: tabItems[0].tabId,
              quantity: _iQty,
              subtotal: _iSubtotal,
              totalTax: _iTax,
              totalDiscount: _iDiscount,
              net: _net
            });


          });
        });

        d.resolve(_tItems);

      });

      return d.promise;
    },
    groupByWaiterItem: function () {

      var d = $q.defer();
      var self = this;

      $timeout(function () {

        var _items = [];

        //console.log(JSON.stringify(self.bills));


        /* Cycle through each Bill and get Items from KOTs */
        angular.forEach(self.bills, function (bill) {
          if (!bill.isVoid) {
            angular.forEach(bill._kots, function (kot) {
              if (!kot.isVoid) {
                for(var it in kot.items) {
                  if(_.has(kot.items[it],'addOns')) {
                    _.forEach(kot.items[it].addOns,function(addon){
                      //item.subtotal+=addon.subtotal;
                      //_.forEach(addon.taxes,function(at){
                      //  item.taxes.push(at);
                      //})
                      kot.items.push(addon);
                    })
                  }
                }
                angular.forEach(kot.items, function (item) {

                  if (!bill.isVoid || !kot.isVoid) {

                    /* Add Bill/ KOT info to Item */
                    item.billNumber = bill.billNumber;
                    item.serialNumber = bill.serialNumber;
                    item.daySerialNumber = bill.serialNumber;
                    item.tab = bill.tab;
                    item.tabType = bill.tabType;
                    item.tabId = bill.tabId;
                    item.waitername = (_.has(bill._waiter, 'username')) ? bill._waiter.username : 'no_waiter_assigned';
                    item.closeTime = bill._closeTime;
                    /*Adding Add Ons*/
                    //if(_.has(item,'addOns')) {
                    //  _.forEach(item.addOns,function(addon){
                    //    item.subtotal+=addon.subtotal;
                    //    _.forEach(addon.taxes,function(at){
                    //      item.taxes.push(at);
                    //    })
                    //  })
                    //}
                    /*End Add Ons*/

                    /* Create Item Tax Amount Column */
                    var _tItemTax = 0;
                    var _newTaxAmtCols = [];
                    angular.forEach(item.taxes, function (tax) {

                      _tItemTax += parseFloat(tax.tax_amount);

                      /* Add Tax Amount Col*/
                      _newTaxAmtCols.push({
                        id: tax._id + '_amt',
                        name: tax.name,
                        isConsidered: false,
                        tax_amount: tax.baseRate_preTax
                      });

                      /* Add Tax Col */
                      _newTaxAmtCols.push({
                        id: tax._id,
                        name: tax.name,
                        isConsidered: true,
                        tax_amount: tax.tax_amount
                      });

                      /* Add Cascading Tax Col */
                      angular.forEach(tax.cascadingTaxes, function (cTax) {
                        _tItemTax += parseFloat(cTax);
                        _newTaxAmtCols.push({
                          id: cTax._id,
                          name: cTax.name,
                          isConsidered: true,
                          tax_amount: cTax.tax_amount
                        })
                      });

                      /* Add Cascading Tax Amount Col */
                      angular.forEach(tax.cascadingTaxes, function (cTaxAmt) {
                        _newTaxAmtCols.push({
                          id: cTaxAmt._id + '_amt',
                          name: cTaxAmt.name,
                          isConsidered: false,
                          tax_amount: cTaxAmt.baseRate_preTax
                        });
                      });
                    });

                    /* Map newly created Tax cols to Item */
                    item._taxes = _newTaxAmtCols;

                    /* Set Total Tax on Item */
                    item.totalTax = parseFloat(_tItemTax);

                    /* Set Total Discount on Item */
                    var _iDiscount = 0;
                    if (_.has(item, 'discounts') && item.isDiscounted) {
                      angular.forEach(item.discounts, function (d) {
                        if (d.type === 'fixed') {
                          _iDiscount += parseFloat(d.value);
                        }
                        if (d.type === 'percentage') {
                          _iDiscount += parseFloat(d.discountAmount);
                        }
                      });
                    }
                    item.totalDiscount = _iDiscount;


                    /* Finally Push to _items after preparation */
                    _items.push(item);

                  }

                });
              }
            });
          }
        });

        //console.log(JSON.stringify(_items));
        self.groupByWaiterItems(_items).then(function (items) {
          //console.log(JSON.stringify(items));
          d.resolve(items);
        });

      });

      return d.promise;
    },
    groupByWaiterItems: function (_items) {

      var d = $q.defer();
      var self = this;

      $timeout(function () {
        var _tItems = [];

        _.chain(_items).groupBy('waitername').map(function (tabItems) {
          _.chain(tabItems).groupBy('_id').map(function (items) {

            var _iQty = 0, _iTax = 0, _iDiscount = 0, _iSubtotal = 0, _iTaxes = [], _iDiscount = 0, _net = 0;
            angular.forEach(items, function (_i) {
              _iQty += parseFloat(_i.quantity);
              _iSubtotal += parseFloat(_i.subtotal);
              _iTax += parseFloat(_i.totalTax);
              _iDiscount += parseFloat(_i.totalDiscount);
              _net = parseFloat((_iSubtotal - _iDiscount) + _iTax);
            });


            _tItems.push({
              id: items[0]._id,
              name: items[0].name,
              rate: items[0].rate,
              waitername: items[0].waitername,
              tab: tabItems[0].tab,
              tabType: tabItems[0].tabType,
              tabId: tabItems[0].tabId,
              quantity: _iQty,
              subtotal: _iSubtotal,
              totalTax: _iTax,
              totalDiscount: _iDiscount,
              net: _net
            });


          });
        });

        d.resolve(_tItems);

      });

      return d.promise;

    },
    groupByUserItem: function () {

      var d = $q.defer();
      var self = this;

      $timeout(function () {

        var _items = [];

        //console.log(JSON.stringify(self.bills));


        /* Cycle through each Bill and get Items from KOTs */
        angular.forEach(self.bills, function (bill) {
          if (!bill.isVoid) {
            angular.forEach(bill._kots, function (kot) {
              if (!kot.isVoid) {
                for(var it in kot.items) {
                  if(_.has(kot.items[it],'addOns')) {
                    _.forEach(kot.items[it].addOns,function(addon){
                      //item.subtotal+=addon.subtotal;
                      //_.forEach(addon.taxes,function(at){
                      //  item.taxes.push(at);
                      //})
                      kot.items.push(addon);
                    })
                  }
                }
                angular.forEach(kot.items, function (item) {

                  /* Add Bill/ KOT info to Item */
                  item.billNumber = bill.billNumber;
                  item.serialNumber = bill.serialNumber;
                  item.daySerialNumber = bill.serialNumber;
                  item.tab = bill.tab;
                  item.tabType = bill.tabType;
                  item.tabId = bill.tabId;
                  item.username = bill._currentUser.username;
                  item.closeTime = bill._closeTime;
                  /*Adding Add Ons*/

                  /*End Add Ons*/

                  /* Create Item Tax Amount Column */
                  var _tItemTax = 0;
                  var _newTaxAmtCols = [];
                  angular.forEach(item.taxes, function (tax) {

                    _tItemTax += parseFloat(tax.tax_amount);

                    /* Add Tax Amount Col*/
                    _newTaxAmtCols.push({
                      id: tax._id + '_amt',
                      name: tax.name,
                      isConsidered: false,
                      tax_amount: tax.baseRate_preTax
                    });

                    /* Add Tax Col */
                    _newTaxAmtCols.push({
                      id: tax._id,
                      name: tax.name,
                      isConsidered: true,
                      tax_amount: tax.tax_amount
                    });

                    /* Add Cascading Tax Col */
                    angular.forEach(tax.cascadingTaxes, function (cTax) {
                      _tItemTax += parseFloat(cTax);
                      _newTaxAmtCols.push({
                        id: cTax._id,
                        name: cTax.name,
                        isConsidered: true,
                        tax_amount: cTax.tax_amount
                      })
                    });

                    /* Add Cascading Tax Amount Col */
                    angular.forEach(tax.cascadingTaxes, function (cTaxAmt) {
                      _newTaxAmtCols.push({
                        id: cTaxAmt._id + '_amt',
                        name: cTaxAmt.name,
                        isConsidered: false,
                        tax_amount: cTaxAmt.baseRate_preTax
                      });
                    });
                  });

                  /* Map newly created Tax cols to Item */
                  item._taxes = _newTaxAmtCols;

                  /* Set Total Tax on Item */
                  item.totalTax = parseFloat(_tItemTax);

                  /* Set Total Discount on Item */
                  var _iDiscount = 0;
                  if (_.has(item, 'discounts') && item.isDiscounted) {
                    angular.forEach(item.discounts, function (d) {
                      if (d.type === 'fixed') {
                        _iDiscount += parseFloat(d.value);
                      }
                      if (d.type === 'percentage') {
                        _iDiscount += parseFloat(d.discountAmount);
                      }
                    });
                  }
                  item.totalDiscount = _iDiscount;


                  /* Finally Push to _items after preparation */
                  _items.push(item);
                });
              }
            });
          }
        });

        //console.log(JSON.stringify(_items));
        self.groupByUserItems(_items).then(function (items) {
          //console.log(JSON.stringify(items));
          d.resolve(items);
        });

      });

      return d.promise;
    },
    groupByUserItems: function (_items) {

      var d = $q.defer();
      var self = this;

      $timeout(function () {
        var _tItems = [];

        _.chain(_items).groupBy('username').map(function (tabItems) {
          _.chain(tabItems).groupBy('_id').map(function (items) {

            var _iQty = 0, _iTax = 0, _iDiscount = 0, _iSubtotal = 0, _iTaxes = [], _iDiscount = 0, _net = 0;
            angular.forEach(items, function (_i) {
              _iQty += parseFloat(_i.quantity);
              _iSubtotal += parseFloat(_i.subtotal);
              _iTax += parseFloat(_i.totalTax);
              _iDiscount += parseFloat(_i.totalDiscount);
              _net = parseFloat((_iSubtotal - _iDiscount) + _iTax);
            });


            _tItems.push({
              id: items[0]._id,
              name: items[0].name,
              rate: items[0].rate,
              username: items[0].username,
              tab: tabItems[0].tab,
              tabType: tabItems[0].tabType,
              tabId: tabItems[0].tabId,
              quantity: _iQty,
              subtotal: _iSubtotal,
              totalTax: _iTax,
              totalDiscount: _iDiscount,
              net: _net
            });


          });
        });

        d.resolve(_tItems);

      });

      return d.promise;

    },
    groupByUser: function () {


      var d = $q.defer();
      var self = this;
      var _allTaxes = [];
      $timeout(function () {
        _allTaxes = [];

        angular.forEach(self.bills, function (bill) {
          //console.log(bill._taxes);
          angular.forEach(bill._taxes, function (tax) {
            tax.tab = bill.tab;
            tax.user = bill._currentUser.username;
            //tax.tabType = bill.tabType;
            tax.billDay = bill.closeTime;
            //tax.billTime = moment(bill._closeTime).format('HH:mm:ss');
            //tax.billCloseTime = bill._closeTime;
            _allTaxes.push(tax);
          });

          /* Add Username to Bill */
          bill.user = bill._currentUser.username;

        });

        //console.log(_allTaxes);

        var _gtax = [];
        _.chain(_allTaxes).groupBy('user').map(function (gTax) {
          _.chain(gTax).groupBy('id').map(function (tTaxes) {

            var _tTax = 0;
            angular.forEach(tTaxes, function (_tax) {
              _tTax += (!_.isNaN(_tax.tax_amount)) ? parseFloat(_tax.tax_amount) : 0;
            });
            _gtax.push({
              id: tTaxes[0].id,
              name: tTaxes[0].name,
              day: tTaxes[0].billDay,
              isConsidered: tTaxes[0].isConsidered,
              tax_amount: _tTax,
              tabType: tTaxes[0].tabType,
              user: tTaxes[0].user
            });
          });
        });

        //console.log(_gtax);
        //console.log(_allTaxes);

        /* Agrregate Day by User */
        var _dayRows = [];
        _.chain(self.bills).groupBy('user').map(function (dayBills) {
          //console.log(dayBills);
          var _bill = {
            totalQty:0,
            totalAmount:0,
            totalDiscount: 0,
            totalTax: 0,
            totalCash: 0,
            totalDebitCard:0,
            totalCreditCard:0,
            totalNet: 0,
            taxes: []
            //day: ''
          };

          //console.log(JSON.stringify(_gtax));
          angular.forEach(dayBills, function (b){
            _bill.totalQty += parseFloat(b.totalQty);
            _bill.totalAmount += parseFloat(b.totalAmount);
            _bill.totalDiscount += parseFloat(b.totalDiscount);
            _bill.totalNet += parseFloat(b.totalNet);
            _bill.totalTax += parseFloat(b.totalTax);
            _bill.totalCash += parseFloat(b.totalCash);
            _bill.totalDebitCard += parseFloat(b.totalDebitCardAmount);
            _bill.totalCreditCard += parseFloat(b.totalCreditCardAmount);
          });
          //_bill.day = dayBills[0].closeTime;
          //_bill.tabType = dayBills[0].tabType;
          _bill.user = dayBills[0].user;
          _bill.taxes = _.filter(_gtax, {user: dayBills[0].user});
          _bill.totalNet = (parseFloat(_bill.totalAmount) + parseFloat(_bill.totalTax)) - parseFloat(_bill.totalDiscount);
          _bill.roundOff = self.getRoundOff(_bill.totalNet);
          _bill.afterRoundOff = self.getAfterRoundOff(_bill.totalNet);
          _bill.noOfBills = dayBills.length;
          _dayRows.push(_bill);
        });

        self.allTaxes = _.uniq(_gtax, 'id');


        self.rows = _dayRows;
        d.resolve(_dayRows);

      },100);

      return d.promise;

    },
    groupByWaiter: function () {



      var d = $q.defer();
      var self = this;
      var _allTaxes = [];
      $timeout(function () {
        _allTaxes = [];

        angular.forEach(self.bills, function (bill) {
          //console.log(bill._taxes);
          if (_.has(bill._waiter, 'username')) {
            angular.forEach(bill._taxes, function (tax) {
              tax.tab = bill.tab;
              tax.waiter = bill._waiter.username;
              //tax.user = bill._currentUser.username;
              //tax.tabType = bill.tabType;
              tax.billDay = bill.closeTime;
              //tax.billTime = moment(bill._closeTime).format('HH:mm:ss');
              //tax.billCloseTime = bill._closeTime;
              _allTaxes.push(tax);
            });
            /* Add Waiter Username to Bill */
            bill.waiter = bill._waiter.username;

          } else {

            /* No Waiter Assigned */
            angular.forEach(bill._taxes, function (tax) {
              tax.tab = bill.tab;
              tax.waiter = 'no_waiter_assigned';
              //tax.user = bill._currentUser.username;
              //tax.tabType = bill.tabType;
              tax.billDay = bill.closeTime;
              //tax.billTime = moment(bill._closeTime).format('HH:mm:ss');
              //tax.billCloseTime = bill._closeTime;
              _allTaxes.push(tax);
            });
            /* Add Waiter Username to Bill */
            bill.waiter = 'no_waiter_assigned';

          }
        });

        //console.log(_allTaxes);

        var _gtax = [];
        _.chain(_allTaxes).groupBy('waiter').map(function (gTax) {
          _.chain(gTax).groupBy('id').map(function (tTaxes) {

            var _tTax = 0;
            angular.forEach(tTaxes, function (_tax) {
              _tTax += (!_.isNaN(_tax.tax_amount)) ? parseFloat(_tax.tax_amount) : 0;
            });
            _gtax.push({
              id: tTaxes[0].id,
              name: tTaxes[0].name,
              day: tTaxes[0].billDay,
              isConsidered: tTaxes[0].isConsidered,
              tax_amount: _tTax,
              tabType: tTaxes[0].tabType,
              waiter: tTaxes[0].waiter
            });
          });
        });

        //console.log(_gtax);
        //console.log(_allTaxes);

        /* Agrregate Day by User */
        var _dayRows = [];
        _.chain(self.bills).groupBy('waiter').map(function (dayBills) {
          //console.log(dayBills);
          var _bill = {
            totalQty:0,
            totalAmount:0,
            totalDiscount: 0,
            totalTax: 0,
            totalCash: 0,
            totalDebitCard:0,
            totalCreditCard:0,
            totalNet: 0,
            taxes: []
            //day: ''
          };

          //console.log(JSON.stringify(_gtax));
          angular.forEach(dayBills, function (b){
            _bill.totalQty += parseFloat(b.totalQty);
            _bill.totalAmount += parseFloat(b.totalAmount);
            _bill.totalDiscount += parseFloat(b.totalDiscount);
            _bill.totalNet += parseFloat(b.totalNet);
            _bill.totalTax += parseFloat(b.totalTax);
            _bill.totalCash += parseFloat(b.totalCash);
            _bill.totalDebitCard += parseFloat(b.totalDebitCardAmount);
            _bill.totalCreditCard += parseFloat(b.totalCreditCardAmount);
          });
          //_bill.day = dayBills[0].closeTime;
          //_bill.tabType = dayBills[0].tabType;
          _bill.waiter = dayBills[0].waiter;
          _bill.taxes = _.filter(_gtax, {user: dayBills[0].user});
          _bill.totalNet = (parseFloat(_bill.totalAmount) + parseFloat(_bill.totalTax)) - parseFloat(_bill.totalDiscount);
          _bill.roundOff = self.getRoundOff(_bill.totalNet);
          _bill.afterRoundOff = self.getAfterRoundOff(_bill.totalNet);
          _bill.noOfBills = dayBills.length;
          _dayRows.push(_bill);
        });

        self.allTaxes = _.uniq(_gtax, 'id');


        self.rows = _dayRows;
        d.resolve(_dayRows);

      },100);

      return d.promise;
    },

    groupByDelivery: function () {



      var d = $q.defer();
      var self = this;
      var _allTaxes = [];
      $timeout(function () {
        _allTaxes = [];

        angular.forEach(self.bills, function (bill) {

          if (bill.tabType != 'table') {
            if (bill.tabType === 'delivery' || bill.tabType === 'takeout') {
              //console.log(bill._taxes);
              //console.log(bill);
              if (_.has(bill._delivery, 'username')) {
                angular.forEach(bill._taxes, function (tax) {
                  tax.tab = bill.tab;
                  tax.delivery = bill._delivery.username;
                  //tax.user = bill._currentUser.username;
                  //tax.tabType = bill.tabType;
                  tax.billDay = bill.closeTime;
                  //tax.billTime = moment(bill._closeTime).format('HH:mm:ss');
                  //tax.billCloseTime = bill._closeTime;
                  _allTaxes.push(tax);
                });
                /* Add Waiter Username to Bill */
                bill.delivery = bill._delivery.username;

              } else {

                /* No Waiter Assigned */
                angular.forEach(bill._taxes, function (tax) {
                  tax.tab = bill.tab;
                  tax.delivery = 'no_delivery_assigned';
                  //tax.user = bill._currentUser.username;
                  //tax.tabType = bill.tabType;
                  tax.billDay = bill.closeTime;
                  //tax.billTime = moment(bill._closeTime).format('HH:mm:ss');
                  //tax.billCloseTime = bill._closeTime;
                  _allTaxes.push(tax);
                });
                /* Add Waiter Username to Bill */
                bill.delivery = 'no_delivery_assigned';

              }

            }
          }
        });

        //console.log(_allTaxes);

        var _gtax = [];
        _.chain(_allTaxes).groupBy('delivery').map(function (gTax) {
          _.chain(gTax).groupBy('id').map(function (tTaxes) {

            var _tTax = 0;
            angular.forEach(tTaxes, function (_tax) {
              _tTax += (!_.isNaN(_tax.tax_amount)) ? parseFloat(_tax.tax_amount) : 0;
            });
            _gtax.push({
              id: tTaxes[0].id,
              name: tTaxes[0].name,
              day: tTaxes[0].billDay,
              isConsidered: tTaxes[0].isConsidered,
              tax_amount: _tTax,
              tabType: tTaxes[0].tabType,
              waiter: tTaxes[0].waiter
            });
          });
        });

        //console.log(_gtax);
        //console.log(_allTaxes);

        /* Agrregate Day by Delivery */
        var _dayRows = [];
        _.chain(self.bills).groupBy('delivery').map(function (dayBills) {

          //console.log(dayBills);
          var _bill = {
            totalQty:0,
            totalAmount:0,
            totalDiscount: 0,
            totalTax: 0,
            totalCash: 0,
            totalDebitCard:0,
            totalCreditCard:0,
            totalNet: 0,
            taxes: []
            //day: ''
          };

          //console.log(JSON.stringify(_gtax));
          angular.forEach(dayBills, function (b){
            _bill.totalQty += parseFloat(b.totalQty);
            _bill.totalAmount += parseFloat(b.totalAmount);
            _bill.totalDiscount += parseFloat(b.totalDiscount);
            _bill.totalNet += parseFloat(b.totalNet);
            _bill.totalTax += parseFloat(b.totalTax);
            _bill.totalCash += parseFloat(b.totalCash);
            _bill.totalDebitCard += parseFloat(b.totalDebitCardAmount);
            _bill.totalCreditCard += parseFloat(b.totalCreditCardAmount);
          });
          //_bill.day = dayBills[0].closeTime;
          //_bill.tabType = dayBills[0].tabType;
          _bill.delivery = dayBills[0].delivery;
          _bill.taxes = _.filter(_gtax, {user: dayBills[0].user});
          _bill.totalNet = (parseFloat(_bill.totalAmount) + parseFloat(_bill.totalTax)) - parseFloat(_bill.totalDiscount);
          _bill.roundOff = self.getRoundOff(_bill.totalNet);
          _bill.afterRoundOff = self.getAfterRoundOff(_bill.totalNet);
          _bill.noOfBills = dayBills.length;

          if (dayBills[0].tabType != 'table') {
            _dayRows.push(_bill);

          }

        });

        self.allTaxes = _.uniq(_gtax, 'id');

        if (_dayRows.length > 0) {
          self.rows = _dayRows;
          d.resolve(_dayRows);
        } else {
          d.reject("No Delivery bills found!")
        }

      },100);

      return d.promise;
    },
    groupByDeliveryItem: function () {

      var d = $q.defer();
      var self = this;

      $timeout(function () {

        var _items = [];

        //console.log(JSON.stringify(self.bills));


        /* Cycle through each Bill and get Items from KOTs */
        angular.forEach(self.bills, function (bill) {
          if (!bill.isVoid) {
            if (bill.tabType === 'delivery' || bill.tabType === 'takeout') {
              angular.forEach(bill._kots, function (kot) {
                if (!kot.isVoid) {
                  for(var it in kot.items) {
                    if (_.has(kot.items[it], 'addOns')) {
                      _.forEach(kot.items[it].addOns, function (addon) {
                        //item.subtotal+=addon.subtotal;
                        //_.forEach(addon.taxes,function(at){
                        //  item.taxes.push(at);
                        //})flattenBillByKot
                        kot.items.push(addon);
                      })
                    }
                  }
                  angular.forEach(kot.items, function (item) {

                    if (!bill.isVoid || !kot.isVoid) {

                      /* Add Bill/ KOT info to Item */
                      item.billNumber = bill.billNumber;
                      item.serialNumber = bill.serialNumber;
                      item.daySerialNumber = bill.serialNumber;
                      item.tab = bill.tab;
                      item.tabType = bill.tabType;
                      item.tabId = bill.tabId;
                      item.deliveryname = (_.has(bill._delivery, 'username')) ? bill._delivery.username : 'no_delivery_assigned';
                      item.closeTime = bill._closeTime;
                      /*Adding Add Ons*/
                      //if(_.has(item,'addOns')) {
                      //  _.forEach(item.addOns,function(addon){
                      //    item.subtotal+=addon.subtotal;
                      //    _.forEach(addon.taxes,function(at){
                      //      item.taxes.push(at);
                      //    })
                      //  })
                      //}
                      /*End Add Ons*/

                      /* Create Item Tax Amount Column */
                      var _tItemTax = 0;
                      var _newTaxAmtCols = [];
                      angular.forEach(item.taxes, function (tax) {

                        _tItemTax += parseFloat(tax.tax_amount);

                        /* Add Tax Amount Col*/
                        _newTaxAmtCols.push({
                          id: tax._id + '_amt',
                          name: tax.name,
                          isConsidered: false,
                          tax_amount: tax.baseRate_preTax
                        });

                        /* Add Tax Col */
                        _newTaxAmtCols.push({
                          id: tax._id,
                          name: tax.name,
                          isConsidered: true,
                          tax_amount: tax.tax_amount
                        });

                        /* Add Cascading Tax Col */
                        angular.forEach(tax.cascadingTaxes, function (cTax) {
                          _tItemTax += parseFloat(cTax);
                          _newTaxAmtCols.push({
                            id: cTax._id,
                            name: cTax.name,
                            isConsidered: true,
                            tax_amount: cTax.tax_amount
                          })
                        });

                        /* Add Cascading Tax Amount Col */
                        angular.forEach(tax.cascadingTaxes, function (cTaxAmt) {
                          _newTaxAmtCols.push({
                            id: cTaxAmt._id + '_amt',
                            name: cTaxAmt.name,
                            isConsidered: false,
                            tax_amount: cTaxAmt.baseRate_preTax
                          });
                        });
                      });

                      /* Map newly created Tax cols to Item */
                      item._taxes = _newTaxAmtCols;

                      /* Set Total Tax on Item */
                      item.totalTax = parseFloat(_tItemTax);

                      /* Set Total Discount on Item */
                      var _iDiscount = 0;
                      if (_.has(item, 'discounts') && item.isDiscounted) {
                        angular.forEach(item.discounts, function (d) {
                          if (d.type === 'fixed') {
                            _iDiscount += parseFloat(d.value);
                          }
                          if (d.type === 'percentage') {
                            _iDiscount += parseFloat(d.discountAmount);
                          }
                        });
                      }
                      item.totalDiscount = _iDiscount;


                      /* Finally Push to _items after preparation */
                      _items.push(item);

                    }

                  });
                }
              });
            }
          }
        });

        //console.log(JSON.stringify(_items));
        self.groupByDeliveryItems(_items).then(function (items) {
          //console.log(JSON.stringify(items));
          d.resolve(items);
        });

      });

      return d.promise;
    },
    groupByDeliveryItems: function (_items) {

      var d = $q.defer();
      var self = this;

      $timeout(function () {
        var _tItems = [];

        _.chain(_items).groupBy('deliveryname').map(function (tabItems) {
          _.chain(tabItems).groupBy('_id').map(function (items) {

            var _iQty = 0, _iTax = 0, _iDiscount = 0, _iSubtotal = 0, _iTaxes = [], _iDiscount = 0, _net = 0;
            angular.forEach(items, function (_i) {
              _iQty += parseFloat(_i.quantity);
              _iSubtotal += parseFloat(_i.subtotal);
              _iTax += parseFloat(_i.totalTax);
              _iDiscount += parseFloat(_i.totalDiscount);
              _net = parseFloat((_iSubtotal - _iDiscount) + _iTax);
            });


            _tItems.push({
              id: items[0]._id,
              name: items[0].name,
              rate: items[0].rate,
              deliveryname: items[0].deliveryname,
              tab: tabItems[0].tab,
              tabType: tabItems[0].tabType,
              tabId: tabItems[0].tabId,
              quantity: _iQty,
              subtotal: _iSubtotal,
              totalTax: _iTax,
              totalDiscount: _iDiscount,
              net: _net
            });


          });
        });

        d.resolve(_tItems);

      });

      return d.promise;
    },

    groupByCategory: function (rows) {
      var self = this;
      var defer = $q.defer();
      var _cats = [];

      $timeout(function () {

        _.chain(rows).groupBy('category').map(function (catItems) {

          var _totalSubtotal = 0;
          var _totalTax = 0;
          var _totalDiscount = 0;
          var _totalQuantity = 0;

          angular.forEach(catItems, function (catItem) {
            _totalSubtotal += catItem.subtotal;
          });

          _cats.push({
            categoryName: catItems[0].category,
            totalAmount: _totalSubtotal
          });

        });

        defer.resolve(_cats);

      });

      return defer.promise;
    },
    getTotalSale: function () {

      var self = this;
      var defer = $q.defer();
      var _totals = {
        totalNet: 0,
        totalAmount: 0,
        totalDiscount: 0,
        totalTax: 0
      };

      $timeout(function () {

        angular.forEach(self.bills, function (bill) {
          _totals.totalNet += parseFloat(bill.totalNet);
          _totals.totalAmount += parseFloat(bill.totalAmount);
          /*Changes done to accomodate billWiseDiscount*/
          _totals.totalDiscount += (parseFloat(bill.totalDiscount)+parseFloat(bill.billDiscountAmount));
          _totals.totalTax += parseFloat(bill.totalTax);

        });

        defer.resolve(_totals);

      });

      return defer.promise;
    },
    flattenBillByKot: function () {
      var d = $q.defer();
      var self = this;
      var _kots = [];
      $timeout(function () {

        /* Add Bill Details to KOT */
        angular.forEach(self.bills, function (bill) {

          /* Don't consider Void Bills */
          if (!bill.isVoid) {
            angular.forEach(bill._kots, function (kot) {
              if (!kot.isVoid) {
                kot.billNumber = bill.billNumber;
                kot.closeTime = bill._closeTime;
                kot.closeDate = moment(bill._closeTime).format('YYYY-MM-DD');
                kot.tab = bill.tab;
                kot.tabType = bill.tabType;
                kot.daySerialNumber = bill.daySerialNumber;
                kot.serialNumber = bill.serialNumber;
                _kots.push(kot);
              }
            });
          }

        });

        /* Aggregate Each KOT with Net, Discount, Tax et al.*/
        angular.forEach(_kots, function (kot) {
          for(var it in kot.items) {
            if(_.has(kot.items[it],'addOns')) {
              _.forEach(kot.items[it].addOns,function(addon){
                //item.subtotal+=addon.subtotal;
                //_.forEach(addon.taxes,function(at){
                //  item.taxes.push(at);
                //})
                kot.items.push(addon);
              })
            }
          }
          angular.forEach(kot.items, function (item) {
            /*Adding Add Ons*/
            //if(_.has(item,'addOns')) {
            //  _.forEach(item.addOns,function(addon){
            //    item.subtotal+=addon.subtotal;
            //    _.forEach(addon.taxes,function(at){
            //      item.taxes.push(at);
            //    })
            //  })
            //}
            /*End Add Ons*/

            /* Aggregate Taxes */
            var _itemTotalTax = 0;
            //console.log(item.taxes);
            angular.forEach(item.taxes, function (tax) {
              console.log("fukgmh");
              _itemTotalTax += parseFloat(tax.tax_amount);
              angular.forEach(tax.cascadingTaxes, function (t) {
                _itemTotalTax += parseFloat(t.tax_amount);
              });
            });


            /* Discounts */
            var _itemTotalDiscount = 0;
            if (_.has(item, 'discounts')) {
              angular.forEach(item.discounts, function (discount) {
                if (discount.type === 'fixed') {
                  _itemTotalDiscount += parseFloat(discount.value);
                }
                if (discount.type === 'percentage') {
                  _itemTotalDiscount += parseFloat(discount.discountAmount);
                }
              });
            }

            item.totalTax = _itemTotalTax;
            item.totalDiscount = parseFloat(_itemTotalDiscount);
            item.totalNet = (parseFloat(item.subtotal) - parseFloat(item.totalDiscount)) + _itemTotalTax;

          });

        });

        d.resolve(_kots);
      }, 100);

      return d.promise;
    },
    flattenBillByKotDeleteHistory: function () {
      var d = $q.defer();
      var self = this;
      var _kotsDeleteHistory = [];
      $timeout(function () {
        /* Include Items from Delete History */
        _.forEach(self.bills, function (bill) {
          console.log(bill)
          _.forEach(bill._kots, function (kot) {
            if (bill.isVoid) {
              /* Get all KOT Items from Void Bill*/
              if(_.has(kot,"deleteHistory")) {
                if(kot.deleteHistory.length > 0){
                  _.forEach(kot.deleteHistory, function (delItem){
                    delItem.billNumber = bill.billNumber;
                    delItem.serialNumber = bill.serialNumber;
                    delItem.daySerialNumber = bill.daySerialNumber;
                    delItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                    delItem._time = moment(bill._closeTime).format('hh:mm:ss a');
                    delItem.kotNumber = kot.kotNumber;
                    delItem.username = bill._currentUser.username;
                    delItem.openTime = kot.created;
                    delItem.deleteTime = delItem.voidItemTime;
                    delItem.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                    delItem.deleteUser = delItem.deleteUser?delItem.deleteUser:(kot.voidUser? kot.voidUser: (!_.isEmpty(bill.voidDetail)? bill.voidDetail.userName: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA"))));
                    _kotsDeleteHistory.push(delItem);
                    /*Adding Add Ons*/
                    if(_.has(delItem,'addOns')) {
                      _.forEach(delItem.addOns,function(addon){
                        addon.billNumber = bill.billNumber;
                        addon.serialNumber = bill.serialNumber;
                        addon.daySerialNumber = bill.daySerialNumber;
                        addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                        addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                        addon.kotNumber = kot.kotNumber;
                        addon.username = bill._currentUser.username;
                        addon.openTime = kot.created;
                        addon.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                        addon.deleteTime = delItem.voidItemTime;
                        addon.deleteUser = delItem.deleteUser?delItem.deleteUser:(kot.voidUser? kot.voidUser: (!_.isEmpty(bill.voidDetail)? bill.voidDetail.userName: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA"))));
                        _kotsDeleteHistory.push(addon);
                      })
                    }
                    /*End Add Ons*/
                  });
                }
              }
              _.forEach(kot.items, function (voidBillItem) {
                voidBillItem.billNumber = bill.billNumber;
                voidBillItem.serialNumber = bill.serialNumber;
                voidBillItem.daySerialNumber = bill.daySerialNumber;
                voidBillItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                voidBillItem._time = moment(bill._closeTime).format('hh:mm:ss a');
                voidBillItem.kotNumber = kot.kotNumber;
                voidBillItem.username = bill._currentUser.username;
                voidBillItem.comment = bill.voidComment;
                voidBillItem.openTime = kot.created;
                voidBillItem.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                voidBillItem.deleteTime = bill.voidBillTime || bill.voidDetail.time;
                voidBillItem.deleteUser = kot.voidUser? kot.voidUser: (!_.isEmpty(bill.voidDetail)? bill.voidDetail.userName: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA")));
                _kotsDeleteHistory.push(voidBillItem);
                /*Adding Add Ons*/
                if(_.has(voidBillItem,'addOns')) {
                  _.forEach(voidBillItem.addOns,function(addon){
                    addon.billNumber = bill.billNumber;
                    addon.serialNumber = bill.serialNumber;
                    addon.daySerialNumber = bill.daySerialNumber;
                    addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                    addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                    addon.kotNumber = kot.kotNumber;
                    addon.username = bill._currentUser.username;
                    addon.comment = bill.voidComment;
                    addon.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                    addon.openTime = kot.created;
                    addon.deleteTime = bill.voidBillTime || bill.voidDetail.time;
                    addon.deleteUser = kot.voidUser? kot.voidUser: (!_.isEmpty(bill.voidDetail)? bill.voidDetail.userName: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA")));
                    _kotsDeleteHistory.push(addon);
                  })
                }
                /*End Add Ons*/
              });
            } else {
              if (kot.isVoid) {
                angular.forEach(kot.deleteHistory, function (delItem){
                  delItem.billNumber = bill.billNumber;
                  delItem.serialNumber = bill.serialNumber;
                  delItem.daySerialNumber = bill.daySerialNumber;
                  delItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                  delItem._time = moment(bill._closeTime).format('hh:mm:ss a');
                  delItem.kotNumber = kot.kotNumber;
                  delItem.username = bill._currentUser.username;
                  delItem.openTime = kot.created;
                  delItem.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                  delItem.deleteTime = delItem.voidItemTime;
                  delItem.deleteUser = delItem.deleteUser?delItem.deleteUser:(kot.voidUser? kot.voidUser: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA")));
                  _kotsDeleteHistory.push(delItem);
                  /*Adding Add Ons*/
                  if(_.has(delItem,'addOns')) {
                    _.forEach(delItem.addOns,function(addon){
                      addon.billNumber = bill.billNumber;
                      addon.serialNumber = bill.serialNumber;
                      addon.daySerialNumber = bill.daySerialNumber;
                      addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                      addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                      addon.kotNumber = kot.kotNumber;
                      addon.username = bill._currentUser.username;
                      addon.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                      addon.openTime = kot.created;
                      addon.deleteTime = delItem.voidItemTime;
                      addon.deleteUser = delItem.deleteUser?delItem.deleteUser:(kot.voidUser? kot.voidUser: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA")));
                      _kotsDeleteHistory.push(addon);
                    })
                  }
                })
                /* Get all Items from Void KOT */
                angular.forEach(kot.items, function (voidKotItem) {
                  voidKotItem.billNumber = bill.billNumber;
                  voidKotItem.serialNumber = bill.serialNumber;
                  voidKotItem.daySerialNumber = bill.daySerialNumber;
                  voidKotItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                  voidKotItem._time = moment(bill._closeTime).format('hh:mm:ss a');
                  voidKotItem.kotNumber = kot.kotNumber;
                  voidKotItem.username = bill._currentUser.username;
                  voidKotItem.comment = kot.comment;
                  voidKotItem.openTime = kot.created;
                  voidKotItem.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                  voidKotItem.deleteTime = kot.voidKotTime;
                  voidKotItem.deleteUser = kot.voidUser? kot.voidUser: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA"));
                  _kotsDeleteHistory.push(voidKotItem);
                  /*Adding Add Ons*/
                  if(_.has(voidKotItem,'addOns')) {
                    _.forEach(voidKotItem.addOns,function(addon){
                      addon.billNumber = bill.billNumber;
                      addon.serialNumber = bill.serialNumber;
                      addon.daySerialNumber = bill.daySerialNumber;
                      addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                      addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                      addon.kotNumber = kot.kotNumber;
                      addon.username = bill._currentUser.username;
                      addon.comment = kot.comment;
                      addon.openTime = kot.created;
                      addon.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                      addon.deleteTime = kot.voidKotTime;
                      addon.deleteUser = kot.voidUser? kot.voidUser: (!_.isEmpty(kot.waiter)? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA"));
                      _kotsDeleteHistory.push(addon);
                    })
                  }
                  /*End Add Ons*/
                });
              } else {
                /* Get Items from Delete History */
                angular.forEach(kot.deleteHistory, function (delItem){
                  delItem.billNumber = bill.billNumber;
                  delItem.serialNumber = bill.serialNumber;
                  delItem.daySerialNumber = bill.daySerialNumber;
                  delItem.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                  delItem._time = moment(bill._closeTime).format('hh:mm:ss a');
                  delItem.kotNumber = kot.kotNumber;
                  delItem.username = bill._currentUser.username;
                  delItem.openTime = kot.created;
                  delItem.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                  delItem.deleteTime = delItem.voidItemTime;
                  delItem.deleteUser = delItem.deleteUser?delItem.deleteUser:(kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA"));
                  _kotsDeleteHistory.push(delItem);
                  /*Adding Add Ons*/
                  if(_.has(delItem,'addOns')) {
                    _.forEach(delItem.addOns,function(addon){
                      addon.billNumber = bill.billNumber;
                      addon.serialNumber = bill.serialNumber;
                      addon.daySerialNumber = bill.daySerialNumber;
                      addon.closeTime = moment(bill._closeTime).format('YYYY-MM-DD');
                      addon._time = moment(bill._closeTime).format('hh:mm:ss a');
                      addon.kotNumber = kot.kotNumber;
                      addon.username = bill._currentUser.username;
                      addon.waiter = (!_.isEmpty(bill._waiter) ? bill._waiter.username : "NA");
                      addon.openTime = kot.created;
                      addon.deleteTime = delItem.voidItemTime;
                      addon.deleteUser = delItem.deleteUser?delItem.deleteUser:(kot.waiter? kot.waiter.username: (bill._currentUser.username? bill._currentUser.username: "NA"));
                      _kotsDeleteHistory.push(addon);
                    })
                  }
                  /*End Add Ons*/
                });
              }
            }
          });
        });
        d.resolve(_kotsDeleteHistory);
      }, 100);
      return d.promise;
    },
    groupBySuperCategory: function (items) {

    },
    getRoundOff: function (netAmount) {
      var _r = parseFloat(netAmount) % 1;
      if (_r >= .50) {
        return (1 - _r);
      } else {
        if (_r < .50) {
          return -(_r);
        }
      }
    },
    getAfterRoundOff: function (netAmount) {
      return Math.round(netAmount);
    },
    getAverageBills: function () {
      var d = $q.defer();
      var self = this;
      var _gBills = [];

      $timeout(function () {

        // Group By Date
        var _filteredByTabType = (self.filters.byTab != '') ? _.filter(self.bills, {tabType: self.filters.byTab}) : self.bills;
        _.chain(_filteredByTabType).groupBy('closeTime').map(function (dateBills) {
          var _totalNet = 0;
          angular.forEach(dateBills, function (bill) {
            _totalNet += parseFloat(bill.totalNet);
            //console.log(moment(bill._closeTime).valueOf());
          });

          /* Get Each Bill Open - Close time */
          var _diffCloseOpen = [];
          angular.forEach(dateBills, function (bill) {
            var _open = moment(bill._created);
            var _close = moment(bill._closeTime);
            var _diff = _close.diff(_open); // in milliseconds
            /*console.log(_open.format('hh:mm:ss:SSS a'));
             console.log(_close.format('hh:mm:ss:SSS a'));
             console.log(_diff);
             console.log("------------")*/
            _diffCloseOpen.push(_diff); // milliseconds
          });

          /* Preparation Time */
          var _diffPrepTime = [];
          angular.forEach(dateBills, function (bill) {
            var _firstKot = moment(bill._kots[0].created);
            var _billPrintTime = moment(bill.billPrintTime);
            var _prepTime = _billPrintTime.diff(_firstKot);
            _diffPrepTime.push(_prepTime);
          });

          var _aggDiffs = 0;
          //console.log(_diffCloseOpen);
          angular.forEach(_diffCloseOpen, function (diff) {
            _aggDiffs += diff;
          });
          var _avgTime = (_aggDiffs / dateBills.length)/1000; // convert to sec

          /* Aggregate Prep Time */
          var _aggPrepTime = 0;
          angular.forEach(_diffPrepTime, function (pDiff) {
            _aggPrepTime += pDiff;
          });
          var _avgPrepTime = (_aggPrepTime/ dateBills.length)/1000; //convert to sec

          //console.log(_avgTime);
          self.rowsBreakDown['daily'].push({
            date: dateBills[0].closeTime,
            totalAmount: _totalNet,
            noOfBills: dateBills.length,
            avg_amount: _totalNet / dateBills.length,
            avg_time_raw: _avgTime,
            avg_time: moment.duration(_avgTime, "seconds").format('hh:mm:ss', { trim: false }),
            avg_prepTime_raw: _avgPrepTime,
            avg_prepTime: moment.duration(_avgPrepTime, "seconds").format('hh:mm:ss', {trim: false}),
            month: moment(dateBills[0]._closeTime).format('MM'),
            year: moment(dateBills[0]._closeTime).format('YYYY'),
            quarter: moment(dateBills[0]._closeTime).format('Q'),
            week: moment(dateBills[0]._closeTime).format('WW'),
            tabType: dateBills[0].tabType
          });

        });

        self.getAverageWeekly().then(function () {
          self.getAverageMonthly().then(function () {
            self.getAverageQuaterly().then(function () {
              self.getAverageHalfYearly().then(function () {
                self.getAverageYearly().then(function () {

                  d.resolve(self.rowsBreakDown);

                });
              });
            });
          });
        });

      }, 100);

      return d.promise;
    },
    getTotalCovers: function () {
      var self = this;
      var defer = $q.defer();
      $timeout(function () {

        var _totalCovers = 0;
        angular.forEach(self.bills, function (bill) {
          if (bill._covers != null) {
            _totalCovers += parseFloat(bill._covers);
          }
        });

        defer.resolve(_totalCovers);

      });
      return defer.promise;
    },
    getAverageWeekly: function () {
      var d = $q.defer();
      var self = this;
      var dateSelectionRange = [];
      var _weekGroup = [];

      $timeout(function () {

        var _from = moment(self.filters.fromDate).toArray();
        var _to = moment(self.filters.toDate).toArray();
        var xDateRange = moment().range(new Date(_from[0],_from[1],_from[2], _from[3], _from[4], _from[5]), new Date(_to[0],_to[1],_to[2], _to[3], _to[4], _to[5]));

        xDateRange.by('days', function (m) {
          dateSelectionRange.push(m.format('YYYY-MM-DD'));
        });

        //console.log(dateSelectionRange);

        var startSlice = 0;
        var endSlice = 7;
        for (var i = 0; i <= Math.floor(dateSelectionRange.length/7); i++) {
          var group = dateSelectionRange.slice(startSlice, endSlice);
          //console.log(group);
          if (group.length > 0) {
            _weekGroup.push({
              from: group[0],
              to: group[group.length - 1],
              amount: 0,
              time: 0
            });
          }
          startSlice += 7;
          endSlice += 7;
          if (endSlice >= dateSelectionRange.length) {
            endSlice = dateSelectionRange.length;
          }
        }

        //console.log(_weekGroup);

        angular.forEach(_weekGroup, function (g) {
          var totalAmt = 0;
          var totalTime = 0;
          var totalBills = 0;
          var totalPrepTime = 0;

          var startDate = moment(g.from).toArray();
          var endDate = moment(g.to).toArray();

          //console.log(startDate);

          var _sDate = new Date(startDate[0], startDate[1], startDate[2]);
          var _eDate = new Date(endDate[0], endDate[1], endDate[2]);

          var grange = moment().range(_sDate, _eDate);
          angular.forEach(self.rowsBreakDown['daily'], function (item) {
            var _iD = moment(item.date).toArray();
            var itemDate = moment(item.date, 'YYYY-MM-DD');
            if (itemDate.within(grange)) {
              totalAmt += item.totalAmount;
              totalTime += item.avg_time_raw;
              totalBills += item.noOfBills;
              totalPrepTime += item.avg_prepTime_raw;
            }

          });
          //console.log(totalAmt + ' / ' + totalBills);
          var avgAmount = totalAmt/totalBills;
          g.amount = (_.isNaN(avgAmount)) ? 0 : avgAmount;
          g.time = moment.duration(totalTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
          g.prepTime = moment.duration(totalPrepTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
        });

        //console.log(_weekGroup);
        self.rowsBreakDown['weekly'] = _weekGroup;

        //console.log(JSON.stringify(self.rowsBreakDown['daily']));



        /*_.chain(self.rowsBreakDown['daily']).groupBy('week').map(function (weekItems) {
         //console.log(monthItems);
         var _mTotals = {
         amount:0,
         time:0
         };
         angular.forEach(weekItems, function (item) {
         _mTotals.amount += parseFloat(item.avg_amount);
         _mTotals.time += item.avg_time_raw;
         });

         self.rowsBreakDown['weekly'].push({
         week: weekItems[0].week,
         avg_amount: _mTotals.amount / 7,
         avg_time_raw: _mTotals.time / 7,
         avg_time: moment.duration(_mTotals.time/7, "seconds").format('hh:mm:ss', { trim: false })
         });

         });*/

        d.resolve();

      }, 100);

      return d.promise;
    },
    getAverageMonthly: function () {
      var deferred = $q.defer();
      var self = this;
      var monthGroup = [];

      $timeout(function () {

        var start = moment(self.filters.fromDate);
        var end = moment(self.filters.toDate);
        var d = moment(start.format('YYYY-MM-DD'));

        do {
          //console.log("Start: " + d.format('YYYY-MM-DD'));
          var _f = d.format('YYYY-MM-DD');
          d.add(1, 'months');
          if (d.isAfter(end)) {
            //console.log("End: " + end.format('YYYY-MM-DD'));
            monthGroup.push({
              from: _f,
              to: end.format('YYYY-MM-DD')
            });
            break;
          }
          //console.log("End: " + d.format('YYYY-MM-DD'));
          var _t = d.format('YYYY-MM-DD');
          monthGroup.push({
            from: _f,
            to: _t
          });
        } while (d.isBefore(end));


        angular.forEach(monthGroup, function (g) {
          var totalAmt = 0;
          var totalTime = 0;
          var totalBills = 0;
          var totalPrepTime = 0;

          var startDate = moment(g.from).toArray();
          var endDate = moment(g.to).toArray();

          var _sDate = new Date(startDate[0], startDate[1], startDate[2]);
          var _eDate = new Date(endDate[0], endDate[1], endDate[2]);

          var grange = moment().range(_sDate, _eDate);
          angular.forEach(self.rowsBreakDown['daily'], function (item) {
            var itemDate = moment(item.date, 'YYYY-MM-DD');
            if (itemDate.within(grange)) {
              //console.log(item);
              totalAmt += item.totalAmount;
              totalTime += item.avg_time_raw;
              totalBills += item.noOfBills;
              totalPrepTime += item.avg_prepTime_raw;
            }

          });
          var avgAmount = totalAmt/totalBills;
          g.amount = (_.isNaN(avgAmount)) ? 0 : avgAmount;
          g.time = moment.duration(totalTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
          g.prepTime = moment.duration(totalPrepTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
        });

        self.rowsBreakDown['monthly'] = monthGroup;

        deferred.resolve();

      }, 100);

      return deferred.promise;
    },
    getAverageQuaterly: function () {
      var deferred = $q.defer();
      var self = this;
      var quarterGroup = [];
      $timeout(function () {

        var start = moment(self.filters.fromDate);
        var end = moment(self.filters.toDate);
        var d = moment(start.format('YYYY-MM-DD'));

        do {
          //console.log("Start: " + d.format('YYYY-MM-DD'));
          var _f = d.format('YYYY-MM-DD');
          d.add(1, 'quarters');
          if (d.isAfter(end)) {
            //console.log("End: " + end.format('YYYY-MM-DD'));
            quarterGroup.push({
              from: _f,
              to: end.format('YYYY-MM-DD')
            });
            break;
          }
          //console.log("End: " + d.format('YYYY-MM-DD'));
          var _t = d.format('YYYY-MM-DD');
          quarterGroup.push({
            from: _f,
            to: _t
          });
        } while (d.isBefore(end));


        angular.forEach(quarterGroup, function (g) {
          var totalAmt = 0;
          var totalTime = 0;
          var totalBills = 0;
          var totalPrepTime = 0;

          var startDate = moment(g.from).toArray();
          var endDate = moment(g.to).toArray();

          var _sDate = new Date(startDate[0], startDate[1], startDate[2]);
          var _eDate = new Date(endDate[0], endDate[1], endDate[2]);

          var grange = moment().range(_sDate, _eDate);
          angular.forEach(self.rowsBreakDown['daily'], function (item) {
            var itemDate = moment(item.date, 'YYYY-MM-DD');
            if (itemDate.within(grange)) {
              //console.log(item);
              totalAmt += item.totalAmount;
              totalTime += item.avg_time_raw;
              totalBills += item.noOfBills;
              totalPrepTime += item.avg_prepTime_raw;
            }

          });
          var avgAmount = totalAmt/totalBills;
          g.amount = (_.isNaN(avgAmount)) ? 0 : avgAmount;
          g.time = moment.duration(totalTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
          g.prepTime = moment.duration(totalPrepTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
        });

        self.rowsBreakDown['quarterly'] = quarterGroup;

        deferred.resolve();
      }, 100);

      return deferred.promise;
    },
    getAverageHalfYearly: function () {
      var deferred = $q.defer();
      var self = this;
      var halfYearlyGroup = [];
      $timeout(function () {

        var start = moment(self.filters.fromDate);
        var end = moment(self.filters.toDate);
        var d = moment(start.format('YYYY-MM-DD'));

        do {
          //console.log("Start: " + d.format('YYYY-MM-DD'));
          var _f = d.format('YYYY-MM-DD');
          d.add(6, 'months');
          if (d.isAfter(end)) {
            //console.log("End: " + end.format('YYYY-MM-DD'));
            halfYearlyGroup.push({
              from: _f,
              to: end.format('YYYY-MM-DD')
            });
            break;
          }
          //console.log("End: " + d.format('YYYY-MM-DD'));
          var _t = d.format('YYYY-MM-DD');
          halfYearlyGroup.push({
            from: _f,
            to: _t
          });
        } while (d.isBefore(end));


        angular.forEach(halfYearlyGroup, function (g) {
          var totalAmt = 0;
          var totalTime = 0;
          var totalBills = 0;
          var totalPrepTime = 0;

          var startDate = moment(g.from).toArray();
          var endDate = moment(g.to).toArray();

          var _sDate = new Date(startDate[0], startDate[1], startDate[2]);
          var _eDate = new Date(endDate[0], endDate[1], endDate[2]);

          var grange = moment().range(_sDate, _eDate);
          angular.forEach(self.rowsBreakDown['daily'], function (item) {
            var itemDate = moment(item.date, 'YYYY-MM-DD');
            if (itemDate.within(grange)) {
              //console.log(item);
              totalAmt += item.totalAmount;
              totalTime += item.avg_time_raw;
              totalBills += item.noOfBills;
              totalPrepTime += item.avg_prepTime_raw;
            }

          });
          var avgAmount = totalAmt/totalBills;
          g.amount = (_.isNaN(avgAmount)) ? 0 : avgAmount;
          g.time = moment.duration(totalTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
          g.prepTime = moment.duration(totalPrepTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
        });

        self.rowsBreakDown['half_yearly'] = halfYearlyGroup;

        deferred.resolve();

      }, 100);

      return deferred.promise;
    },
    getAverageYearly: function () {
      var deferred = $q.defer();
      var self = this;
      var yearlyGroup = [];

      $timeout(function () {

        var start = moment(self.filters.fromDate);
        var end = moment(self.filters.toDate);
        var d = moment(start.format('YYYY-MM-DD'));

        do {
          //console.log("Start: " + d.format('YYYY-MM-DD'));
          var _f = d.format('YYYY-MM-DD');
          d.add(12, 'months');
          if (d.isAfter(end)) {
            //console.log("End: " + end.format('YYYY-MM-DD'));
            yearlyGroup.push({
              from: _f,
              to: end.format('YYYY-MM-DD')
            });
            break;
          }
          //console.log("End: " + d.format('YYYY-MM-DD'));
          var _t = d.format('YYYY-MM-DD');
          yearlyGroup.push({
            from: _f,
            to: _t
          });
        } while (d.isBefore(end));


        angular.forEach(yearlyGroup, function (g) {
          var totalAmt = 0;
          var totalTime = 0;
          var totalBills = 0;
          var totalPrepTime = 0;

          var startDate = moment(g.from).toArray();
          var endDate = moment(g.to).toArray();

          var _sDate = new Date(startDate[0], startDate[1], startDate[2]);
          var _eDate = new Date(endDate[0], endDate[1], endDate[2]);

          var grange = moment().range(_sDate, _eDate);
          angular.forEach(self.rowsBreakDown['daily'], function (item) {
            var itemDate = moment(item.date, 'YYYY-MM-DD');
            if (itemDate.within(grange)) {
              //console.log(item);
              totalAmt += item.totalAmount;
              totalTime += item.avg_time_raw;
              totalBills += item.noOfBills;
              totalPrepTime += item.avg_prepTime_raw;
            }

          });
          var avgAmount = totalAmt/totalBills;
          g.amount = (_.isNaN(avgAmount)) ? 0 : avgAmount;
          g.time = moment.duration(totalTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
          g.prepTime = moment.duration(totalPrepTime/totalBills, "seconds").format('hh:mm:ss', {trim: false});
        });

        self.rowsBreakDown['yearly'] = yearlyGroup;



        deferred.resolve();
      }, 100);

      return deferred.promise;
    },
    excelInvoiceDetails: function () {
      var d = $q.defer();
      var self = this;

      /* Excel: Invoice Report Struc */
      var invoiceStruc = {
        columns: [
          {
            name:'item',
            label: 'Item Name',
            colIndex: 0
          },
          {
            name:'qty',
            label: 'Quantity',
            colIndex: 0
          },
          {
            name: 'amount',
            label: 'Amount',
            colIndex: 0
          },
          {
            name: 'discount',
            label: 'Discount',
            colIndex: 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        /* Tax Columns */
        angular.forEach(self.allTaxes, function (tax) {
          invoiceStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex:0
          })
        });
        /* Net Column */
        invoiceStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        //console.log(self.bills);
        angular.forEach(self.bills, function (bill) {
          //console.log(bill);
          angular.forEach(bill._items, function (item) {
            var _item = {
              name: item.name,
              quantity: item.quantity,
              amount: item.subtotal,
              discount: item.discount,
              taxes: angular.copy(self.allTaxes),
              created: bill.created,
              closeTime: moment(moment(bill.closeTime)).format('YYYY-MM-DD'),
              billNumber: bill.billNumber,
              daySerial : bill.daySerialNumber,
              serial: bill.serialNumber,
              tab: bill.tab
            };
            //console.log(_item);
            angular.forEach(_item.taxes, function (tax) {
              var _t = 0;
              var _f = _.filter(item.taxes, {_id: tax.id});
              if (_f.length > 0) {
                angular.forEach(_f, function (t) {
                  //console.log(t);
                  _t += parseFloat(t.tax_amount);
                });
                tax.amount = _t;
                tax.label = tax.name;
                tax.name = tax.id;

              } else {
                tax.amount = 0;
                tax.label = tax.name;
                tax.name = tax.id;
              }

            });


            invoiceStruc.items.push(_item);

          });
        });

        //console.log(JSON.stringify(invoiceStruc.items));
        d.resolve(invoiceStruc);

      }, 100);

      return d.promise;
    },
    excelPaymentDetails: function (bills,allTaxes) {
      var self = this;
      var d = $q.defer();

      /* Excel: Invoice Report Struc */
      var invoiceStruc = {
        columns: [
          {
            "name": "billNumber",
            "label": "Bill Number",
            "colIndex": 0
          },
          {
            "name": "openTime",
            "label": "Open Time",
            "colIndex": 0
          },
          {
            "name": "tableNumber",
            "label": "Table #",
            "colIndex": 0
          },
          {
            "name": "closeTime",
            "label": "Close Time",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amount",
            "colIndex": 0
          },
          {
            "name": "discount",
            "label": "Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        /* Tax Columns */
        Object.keys(allTaxes).forEach( function (tax) {
          invoiceStruc.columns.push({
            id: tax,
            name: allTaxes[tax],
            label: allTaxes[tax],
            colIndex: 0
          })
        });

        /* Column */
        invoiceStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        invoiceStruc.columns.push({
          "name": "cash",
          "label": "Cash",
          "colIndex": 0
        });
        invoiceStruc.columns.push({
          "name": "creditCard",
          "label": "Credit Card",
          "colIndex": 0
        });
        invoiceStruc.columns.push({
          "name": "debitCard",
          "label": "Debit Card",
          "colIndex": 0
        });
        invoiceStruc.columns.push({
          "name": "otherCard",
          "label": "Other Card",
          "colIndex": 0
        });

        /* Add Bills Items and Totals */
         console.log(bills);
        angular.forEach(bills, function (bill) {
          //console.log(bill);
          var _item = {
            billNumber: bill.billNumber,
            _created: bill._created,
            created: moment(bill._created).format('YYYY-MM-DD'),
            _tableId: (bill._tableId === null) ? '' : bill._tableId,
            _closeTime: bill._closeTime,
            closeTime: bill.closeTime,
            totalAmount: bill.aggregate.amount,
            totalDiscount: bill.aggregate.discount + bill.billDiscountAmount,
            taxes: angular.copy(bill.aggregate.taxes),
            totalNet: bill.aggregate.netAmount,
            totalCash: bill.aggregate.cash,
            totalCreditCard: bill.aggregate.credit,
            totalDebitCard: bill.aggregate.debit,
            totalOtherCard: bill.aggregate.other
          };

          /* Map All Tax Values to Global Taxes*/
          /* angular.forEach(_item.taxes, function (tax) {
            var _t = 0;
            console.log(tax);
            _.forEach(Object.keys(allTaxes),function(taxId){

            })
            var _f = _.filter(allTaxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                _t += Utils.roundNumber(parseFloat(t.tax_amount),2);
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });
          */
          invoiceStruc.items.push(_item);

        });
       console.log(invoiceStruc);
        d.resolve(invoiceStruc);

      }, 100);

      return d.promise;
    },
    excelTaxSummary: function (rows) {

      var self = this;
      var d = $q.defer();

      /* Excel: Invoice Report Struc */
      var invoiceStruc = {
        columns: [
          {
            "name": "day",
            "label": "Day",
            "colIndex": 0
          },
          {
            "name": "totalBills",
            "label": "No of Bills",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amt",
            "colIndex": 0
          },
          {
            "name": "totalDiscount",
            "label": "Total Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        /* Tax Columns */
        angular.forEach(self.allTaxes, function (tax) {
          invoiceStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });

        invoiceStruc.columns.push({
          name:'totalNet',
          label:'Net Amount',
          colIndex: 0
        });
        invoiceStruc.columns.push({
          "name": "roundOff",
          "label": "Round Off",
          "colIndex": 0
        });
        invoiceStruc.columns.push({
          "name": "cash",
          "label": "Cash",
          "colIndex": 0
        });
        invoiceStruc.columns.push({
          "name": "debitCard",
          "label": "Debit Card",
          "colIndex": 0
        });
        invoiceStruc.columns.push({
          "name": "creditCard",
          "label": "Credit Card",
          "colIndex": 0
        });

        //console.log(JSON.stringify(self.rows));

        /* Add Bills Items and Totals */
        angular.forEach(rows, function (row) {
          var _row = {
            day: row.day,
            totalQty: row.totalQty,
            totalBills: row.noOfBills,
            totalAmount: row.totalAmount,
            totalDiscount: row.totalDiscount,
            taxes: angular.copy(self.allTaxes),
            totalNet: row.totalNet,
            roundOff: row.roundOff,
            cash: row.totalCash,
            debitCard: row.totalDebitCard,
            creditCard: row.totalCreditCard
          };

          /* Map All Tax Values to Global Taxes*/
          angular.forEach(_row.taxes, function (tax) {
            var _t = 0;
            var _f = _.filter(row.taxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                _t += parseFloat(t.tax_amount);
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });

          invoiceStruc.items.push(_row);

        });

        d.resolve(invoiceStruc);

      }, 100);

      return d.promise;
    },
    excelItemCategory: function (items) {
      var self = this;
      var d = $q.defer();

      /* Excel: Invoice Report Struc */
      var invoiceStruc = {
        columns: [
          {
            "name": "item",
            "label": "Item Name",
            "colIndex": 0
          },
          {
            "name": "totalQty",
            "label": "Quantity",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Amount",
            "colIndex": 0
          },
          {
            "name": "totalDiscount",
            "label": "Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        /* Tax Columns */
        angular.forEach(self.allTaxes, function (tax) {
          invoiceStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });

        invoiceStruc.columns.push({
          "name": "totalTax",
          "label": "Total Tax",
          "colIndex": 0
        });

        invoiceStruc.columns.push({
          "name": "net",
          "label": "Net Amount",
          "colIndex": 0
        });

        invoiceStruc.items = items;

        d.resolve(invoiceStruc);

      }, 100);


      return d.promise;
    },
    excelTabBreakDown: function (rows) {

      var self = this;
      var d = $q.defer();

      var _workBookStruc = {
        consolidated: {},
        detail: {},
        itemwise: {}
      };

      /* Excel: Consolidated Struc */
      var consolidatedStruc = {
        columns: [
          {
            "name": "tabType",
            "label": "Tab Type",
            "colIndex": 0
          },
          {
            "name": "totalBills",
            "label": "No of Bills",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amt",
            "colIndex": 0
          },
          {
            "name": "totalDiscount",
            "label": "Total Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };
      var itemWiseStruc = {
        columns: [
          {
            name:'item',
            label: 'Item Name',
            colIndex: 0
          },
          {
            name:'qty',
            label: 'Quantity',
            colIndex: 0
          },
          {
            name: 'amount',
            label: 'Amount',
            colIndex: 0
          },
          {
            name: 'discount',
            label: 'Discount',
            colIndex: 0
          }
        ],
        items:[]
      };
      var detailStruc = {
        columns: [
          {
            "name": "_date",
            "label": "Date",
            "colIndex": 0
          },
          {
            "name": "billNumber",
            "label": "Bill Number",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amount",
            "colIndex": 0
          },
          {
            "name": "discount",
            "label": "Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        /* Consolidated */
        angular.forEach(self.allTaxes, function (tax) {
          consolidatedStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });
        consolidatedStruc.columns.push({
          name:'totalNet',
          label:'Net Amount',
          colIndex: 0
        });
        consolidatedStruc.columns.push({
          "name": "roundOff",
          "label": "Round Off",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "cash",
          "label": "Cash",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "debitCard",
          "label": "Debit Card",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "creditCard",
          "label": "Credit Card",
          "colIndex": 0
        });
        angular.forEach(rows, function (row) {
          var _row = {
            tab: row.tab,
            tabType: row.tabType,
            totalQty: row.totalQty,
            totalBills: row.noOfBills,
            totalAmount: row.totalAmount,
            totalDiscount: row.totalDiscount,
            taxes: angular.copy(self.allTaxes),
            totalNet: row.totalNet,
            roundOff: row.roundOff,
            cash: row.totalCash,
            debitCard: row.totalDebitCard,
            creditCard: row.totalCreditCard
          };

          /* Map All Tax Values to Global Taxes*/
          angular.forEach(_row.taxes, function (tax) {
            var _t = 0;
            var _f = _.filter(row.taxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                _t += parseFloat(t.tax_amount);
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });

          consolidatedStruc.items.push(_row);

        });
        /* Consolidated */




        /* Item Wise */
        angular.forEach(self.allTaxes, function (tax) {
          itemWiseStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex:0
          })
        });
        itemWiseStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        angular.forEach(self.bills, function (bill) {
          angular.forEach(bill._items, function (item) {
            var _item = {
              name: item.name,
              quantity: item.quantity,
              amount: item.subtotal,
              discount: item.discount,
              taxes: angular.copy(self.allTaxes),
              created: bill.created,
              closeTime: bill.closeTime,
              billNumber: bill.billNumber,
              daySerial : bill.daySerialNumber,
              serial: bill.serialNumber,
              tab: bill.tab,
              tabType: bill.tabType
            };
            //console.log(_item);
            angular.forEach(_item.taxes, function (tax) {
              if (!tax.isConsidered) {
                /* Tax Amount column */
                var _tID = tax.id.split('_');
                var _t = 0;
                var _f = _.filter(item.taxes, {_id: _tID[0]});
                if (_f.length > 0) {
                  angular.forEach(_f, function (t) {
                    _t += (_.has(t, 'baseRate_preTax')) ? parseFloat(t.baseRate_preTax) : 0;
                  });
                  tax.amount = _t;
                  tax.label = tax.name;
                  tax.name = tax.id;
                } else {
                  tax.amount = 0;
                  tax.label = tax.name;
                  tax.name = tax.id;
                }

              } else {
                var _t = 0;
                var _f = _.filter(item.taxes, {_id: tax.id});
                if (_f.length > 0) {
                  angular.forEach(_f, function (t) {
                    _t += parseFloat(t.tax_amount);
                  });
                  tax.amount = _t;
                  tax.label = tax.name;
                  tax.name = tax.id;

                } else {
                  tax.amount = 0;
                  tax.label = tax.name;
                  tax.name = tax.id;
                }
              }


            });

            itemWiseStruc.items.push(_item);

          });
        });
        /* Item Wise */




        /* Detail */
        angular.forEach(self.allTaxes, function (tax) {
          detailStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });
        detailStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        angular.forEach(self.bills, function (bill) {
          var _item = {
            billNumber: bill.billNumber,
            _created: bill._created,
            created: moment(bill._created).format('YYYY-MM-DD'),
            _tableId: (bill._tableId === null) ? '' : bill._tableId,
            _closeTime: bill._closeTime,
            closeTime: bill.closeTime,
            totalAmount: bill.totalAmount,
            totalDiscount: bill.totalDiscount,
            taxes: angular.copy(self.allTaxes),
            totalNet: bill.totalNet,
            totalCash: bill.totalCash,
            totalCreditCard: 0,
            totalDebitCard: 0,
            tabType: bill.tabType,
            tab: bill.tab
          };

          /* Map All Tax Values to Global Taxes*/
          angular.forEach(_item.taxes, function (tax) {
            var _t = 0;
            var _f = _.filter(bill._taxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                if (!_.isNaN(t.tax_amount)  || t.tax_amount != undefined) {
                  _t += parseFloat(t.tax_amount);
                } else {
                  _t += 0;
                }
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });
          detailStruc.items.push(_item);
        });
        /* Detail */

        _workBookStruc.consolidated = consolidatedStruc;
        _workBookStruc.itemwise = itemWiseStruc;
        _workBookStruc.detail = detailStruc;

        //console.log(JSON.stringify(_workBookStruc));
        d.resolve(_workBookStruc);

      }, 100);

      return d.promise;

    },
    excelUserBreakdown: function (rows) {

      var self = this;
      var d = $q.defer();

      var _workBookStruc = {
        consolidated: {},
        detail: {},
        itemwise: {}
      };

      /* Excel: Consolidated Struc */
      var consolidatedStruc = {
        columns: [
          {
            "name": "user",
            "label": "Username",
            "colIndex": 0
          },
          {
            "name": "totalBills",
            "label": "No of Bills",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amt",
            "colIndex": 0
          },
          {
            "name": "totalDiscount",
            "label": "Total Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };
      var itemWiseStruc = {
        columns: [
          {
            name:'item',
            label: 'Item Name',
            colIndex: 0
          },
          {
            name:'qty',
            label: 'Quantity',
            colIndex: 0
          },
          {
            name: 'amount',
            label: 'Amount',
            colIndex: 0
          },
          {
            name: 'discount',
            label: 'Discount',
            colIndex: 0
          }
        ],
        items:[]
      };
      var detailStruc = {
        columns: [
          {
            "name": "_date",
            "label": "Date",
            "colIndex": 0
          },
          {
            "name": "billNumber",
            "label": "Bill Number",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amount",
            "colIndex": 0
          },
          {
            "name": "discount",
            "label": "Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        /* Consolidated */
        angular.forEach(self.allTaxes, function (tax) {
          consolidatedStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });
        consolidatedStruc.columns.push({
          name:'totalNet',
          label:'Net Amount',
          colIndex: 0
        });
        consolidatedStruc.columns.push({
          "name": "roundOff",
          "label": "Round Off",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "cash",
          "label": "Cash",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "debitCard",
          "label": "Debit Card",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "creditCard",
          "label": "Credit Card",
          "colIndex": 0
        });
        angular.forEach(rows, function (row) {
          var _row = {
            user: row.user,
            tab: row.tab,
            tabType: row.tabType,
            totalQty: row.totalQty,
            totalBills: row.noOfBills,
            totalAmount: row.totalAmount,
            totalDiscount: row.totalDiscount,
            taxes: angular.copy(self.allTaxes),
            totalNet: row.totalNet,
            roundOff: row.roundOff,
            cash: row.totalCash,
            debitCard: row.totalDebitCard,
            creditCard: row.totalCreditCard
          };

          /* Map All Tax Values to Global Taxes*/
          angular.forEach(_row.taxes, function (tax) {
            var _t = 0;
            var _f = _.filter(row.taxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                _t += parseFloat(t.tax_amount);
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });

          consolidatedStruc.items.push(_row);

        });
        /* Consolidated */




        /* Item Wise */
        angular.forEach(self.allTaxes, function (tax) {
          itemWiseStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex:0
          })
        });
        itemWiseStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        angular.forEach(self.bills, function (bill) {
          angular.forEach(bill._items, function (item) {
            var _item = {
              name: item.name,
              quantity: item.quantity,
              amount: item.subtotal,
              discount: item.discount,
              taxes: angular.copy(self.allTaxes),
              created: bill.created,
              closeTime: bill.closeTime,
              billNumber: bill.billNumber,
              daySerial : bill.daySerialNumber,
              serial: bill.serialNumber,
              tab: bill.tab,
              tabType: bill.tabType,
              user: bill.user
            };
            //console.log(_item);
            angular.forEach(_item.taxes, function (tax) {
              if (!tax.isConsidered) {
                /* Tax Amount column */
                var _tID = tax.id.split('_');
                var _t = 0;
                var _f = _.filter(item.taxes, {_id: _tID[0]});
                if (_f.length > 0) {
                  angular.forEach(_f, function (t) {
                    _t += (_.has(t, 'baseRate_preTax')) ? parseFloat(t.baseRate_preTax) : 0;
                  });
                  tax.amount = _t;
                  tax.label = tax.name;
                  tax.name = tax.id;
                } else {
                  tax.amount = 0;
                  tax.label = tax.name;
                  tax.name = tax.id;
                }

              } else {

                var _t = 0;
                var _f = _.filter(item.taxes, {_id: tax.id});
                if (_f.length > 0) {
                  angular.forEach(_f, function (t) {
                    _t += parseFloat(t.tax_amount);
                  });
                  tax.amount = _t;
                  tax.label = tax.name;
                  tax.name = tax.id;

                } else {
                  tax.amount = 0;
                  tax.label = tax.name;
                  tax.name = tax.id;
                }

              }


            });

            itemWiseStruc.items.push(_item);

          });
        });
        /* Item Wise */




        /* Detail */
        angular.forEach(self.allTaxes, function (tax) {
          detailStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });
        detailStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        angular.forEach(self.bills, function (bill) {
          var _item = {
            billNumber: bill.billNumber,
            _created: bill._created,
            created: moment(bill._created).format('YYYY-MM-DD'),
            _tableId: (bill._tableId === null) ? '' : bill._tableId,
            _closeTime: bill._closeTime,
            closeTime: bill.closeTime,
            totalAmount: bill.totalAmount,
            totalDiscount: bill.totalDiscount,
            taxes: angular.copy(self.allTaxes),
            totalNet: bill.totalNet,
            totalCash: bill.totalCash,
            totalCreditCard: 0,
            totalDebitCard: 0,
            tabType: bill.tabType,
            tab: bill.tab,
            user: bill.user
          };

          /* Map All Tax Values to Global Taxes*/
          angular.forEach(_item.taxes, function (tax) {
            var _t = 0;
            var _f = _.filter(bill._taxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                if (!_.isNaN(t.tax_amount)  || t.tax_amount != undefined) {
                  _t += parseFloat(t.tax_amount);
                } else {
                  _t += 0;
                }
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });
          detailStruc.items.push(_item);
        });
        /* Detail */

        _workBookStruc.consolidated = consolidatedStruc;
        _workBookStruc.itemwise = itemWiseStruc;
        _workBookStruc.detail = detailStruc;

        //console.log(JSON.stringify(_workBookStruc));
        d.resolve(_workBookStruc);

      }, 100);

      return d.promise;

    },
    excelWaiterBreakdown: function (rows) {


      var self = this;
      var d = $q.defer();

      var _workBookStruc = {
        consolidated: {},
        detail: {},
        itemwise: {}
      };

      /* Excel: Consolidated Struc */
      var consolidatedStruc = {
        columns: [
          {
            "name": "waiter",
            "label": "Waiter Username",
            "colIndex": 0
          },
          {
            "name": "totalBills",
            "label": "No of Bills",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amt",
            "colIndex": 0
          },
          {
            "name": "totalDiscount",
            "label": "Total Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };
      var itemWiseStruc = {
        columns: [
          {
            name:'item',
            label: 'Item Name',
            colIndex: 0
          },
          {
            name:'qty',
            label: 'Quantity',
            colIndex: 0
          },
          {
            name: 'amount',
            label: 'Amount',
            colIndex: 0
          },
          {
            name: 'discount',
            label: 'Discount',
            colIndex: 0
          }
        ],
        items:[]
      };
      var detailStruc = {
        columns: [
          {
            "name": "_date",
            "label": "Date",
            "colIndex": 0
          },
          {
            "name": "billNumber",
            "label": "Bill Number",
            "colIndex": 0
          },
          {
            "name": "totalAmount",
            "label": "Total Amount",
            "colIndex": 0
          },
          {
            "name": "discount",
            "label": "Discount",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        /* Consolidated */
        angular.forEach(self.allTaxes, function (tax) {
          consolidatedStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });
        consolidatedStruc.columns.push({
          name:'totalNet',
          label:'Net Amount',
          colIndex: 0
        });
        consolidatedStruc.columns.push({
          "name": "roundOff",
          "label": "Round Off",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "cash",
          "label": "Cash",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "debitCard",
          "label": "Debit Card",
          "colIndex": 0
        });
        consolidatedStruc.columns.push({
          "name": "creditCard",
          "label": "Credit Card",
          "colIndex": 0
        });
        angular.forEach(rows, function (row) {
          var _row = {
            waiter: row.waiter,
            tab: row.tab,
            tabType: row.tabType,
            totalQty: row.totalQty,
            totalBills: row.noOfBills,
            totalAmount: row.totalAmount,
            totalDiscount: row.totalDiscount,
            taxes: angular.copy(self.allTaxes),
            totalNet: row.totalNet,
            roundOff: row.roundOff,
            cash: row.totalCash,
            debitCard: row.totalDebitCard,
            creditCard: row.totalCreditCard
          };

          /* Map All Tax Values to Global Taxes*/
          angular.forEach(_row.taxes, function (tax) {
            var _t = 0;
            var _f = _.filter(row.taxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                _t += parseFloat(t.tax_amount);
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });

          consolidatedStruc.items.push(_row);

        });
        /* Consolidated */




        /* Item Wise */
        angular.forEach(self.allTaxes, function (tax) {
          itemWiseStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex:0
          })
        });
        itemWiseStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        angular.forEach(self.bills, function (bill) {
          angular.forEach(bill._items, function (item) {
            var _item = {
              name: item.name,
              quantity: item.quantity,
              amount: item.subtotal,
              discount: item.discount,
              taxes: angular.copy(self.allTaxes),
              created: bill.created,
              closeTime: bill.closeTime,
              billNumber: bill.billNumber,
              daySerial : bill.daySerialNumber,
              serial: bill.serialNumber,
              tab: bill.tab,
              tabType: bill.tabType,
              waiter: bill.waiter
            };
            //console.log(_item);
            angular.forEach(_item.taxes, function (tax) {
              if (!tax.isConsidered) {
                /* Tax Amount column */
                var _tID = tax.id.split('_');
                var _t = 0;
                var _f = _.filter(item.taxes, {_id: _tID[0]});
                if (_f.length > 0) {
                  angular.forEach(_f, function (t) {
                    _t += (_.has(t, 'baseRate_preTax')) ? parseFloat(t.baseRate_preTax) : 0;
                  });
                  tax.amount = _t;
                  tax.label = tax.name;
                  tax.name = tax.id;
                } else {
                  tax.amount = 0;
                  tax.label = tax.name;
                  tax.name = tax.id;
                }

              } else {

                var _t = 0;
                var _f = _.filter(item.taxes, {_id: tax.id});
                if (_f.length > 0) {
                  angular.forEach(_f, function (t) {
                    _t += parseFloat(t.tax_amount);
                  });
                  tax.amount = _t;
                  tax.label = tax.name;
                  tax.name = tax.id;

                } else {
                  tax.amount = 0;
                  tax.label = tax.name;
                  tax.name = tax.id;
                }

              }


            });

            itemWiseStruc.items.push(_item);

          });
        });
        /* Item Wise */




        /* Detail */
        angular.forEach(self.allTaxes, function (tax) {
          detailStruc.columns.push({
            id: tax.id,
            name: tax.id,
            label: tax.name,
            colIndex: 0
          })
        });
        detailStruc.columns.push({
          name:'net',
          label:'Net Amount',
          colIndex: 0
        });
        angular.forEach(self.bills, function (bill) {
          var _item = {
            billNumber: bill.billNumber,
            _created: bill._created,
            created: moment(bill._created).format('YYYY-MM-DD'),
            _tableId: (bill._tableId === null) ? '' : bill._tableId,
            _closeTime: bill._closeTime,
            closeTime: bill.closeTime,
            totalAmount: bill.totalAmount,
            totalDiscount: bill.totalDiscount,
            taxes: angular.copy(self.allTaxes),
            totalNet: bill.totalNet,
            totalCash: bill.totalCash,
            totalCreditCard: 0,
            totalDebitCard: 0,
            tabType: bill.tabType,
            tab: bill.tab,
            waiter: bill.waiter
          };

          /* Map All Tax Values to Global Taxes*/
          angular.forEach(_item.taxes, function (tax) {
            var _t = 0;
            var _f = _.filter(bill._taxes, {id: tax.id});
            if (_f.length > 0) {
              angular.forEach(_f, function (t) {
                if (!_.isNaN(t.tax_amount)  || t.tax_amount != undefined) {
                  _t += parseFloat(t.tax_amount);
                } else {
                  _t += 0;
                }
              });
              tax.amount = _t;
              tax.label = tax.name;
              tax.name = tax.id;

            } else {
              tax.tax_amount = 0;
              tax.label = tax.name;
              tax.name = tax.id;
            }

          });
          detailStruc.items.push(_item);
        });
        /* Detail */

        _workBookStruc.consolidated = consolidatedStruc;
        _workBookStruc.itemwise = itemWiseStruc;
        _workBookStruc.detail = detailStruc;

        //console.log(JSON.stringify(_workBookStruc));
        d.resolve(_workBookStruc);

      }, 100);

      return d.promise;

    },
    excelKotDetail: function (rows) {

      var self = this;
      var d = $q.defer();

      /* Excel: Invoice Report Struc */
      var invoiceStruc = {
        columns: [
          {
            "name": "item",
            "label": "Item Name",
            "colIndex": 0
          },
          {
            "name": "rate",
            "label": "Rate",
            "colIndex": 0
          },
          {
            "name": "qty",
            "label": "Quantity",
            "colIndex": 0
          },
          {
            "name": "net",
            "label": "Net Amount",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        angular.forEach(rows, function (row) {
          invoiceStruc.items.push(row);
        });

        d.resolve(invoiceStruc);

      }, 100);

      return d.promise;

    },
    excelKotDeleteHistory: function (rows) {

      var self = this;
      var d = $q.defer();

      /* Excel: Invoice Report Struc */
      var invoiceStruc = {
        columns: [
          {
            "name": "kotNumber",
            "label": "KOT Number",
            "colIndex": 0
          },
          {
            "name": "billNumber",
            "label": "Bill Number",
            "colIndex": 0
          },
          {
            "name": "date",
            "label": "Date",
            "colIndex": 0
          },
          {
            "name": "item",
            "label": "Item Name",
            "colIndex": 0
          },
          {
            "name": "orderTime",
            "label": "Order Time",
            "colIndex": 0
          },
          {
            "name": "deleteTime",
            "label": "Delete Time",
            "colIndex": 0
          },
          {
            "name": "qty",
            "label": "Quantity",
            "colIndex": 0
          },
          {
            "name": "rate",
            "label": "Rate",
            "colIndex": 0
          },
          {
            "name": "total",
            "label": "Total",
            "colIndex": 0
          },
          {
            "name": "comment",
            "label": "Comment",
            "colIndex": 0
          },
          {
            "name": "username",
            "label": "Username",
            "colIndex": 0
          }
        ],
        items:[]
      };

      $timeout(function () {

        angular.forEach(rows, function (row) {
          invoiceStruc.items.push(row);
        });

        d.resolve(invoiceStruc);

      }, 100);

      return d.promise;

    },
    excelAverageBill: function (rows) {

      var self = this;
      var d = $q.defer();

      /* Excel: Report Struc */
      var struc = {
        daily_Struc: {
          columns: [
            {
              "name": "date",
              "label": "Date",
              "colIndex": 0
            },
            {
              "name": "amount",
              "label": "Amount",
              "colIndex": 0
            },
            {
              "name": "time",
              "label": "Time",
              "colIndex": 0
            }
          ],
          items:[]
        },
        weekly_Struc: {
          columns: [
            {
              "name": "date",
              "label": "Date",
              "colIndex": 0
            },
            {
              "name": "amount",
              "label": "Amount",
              "colIndex": 0
            },
            {
              "name": "time",
              "label": "Time",
              "colIndex": 0
            }
          ],
          items:[]
        },
        monthly_Struc: {
          columns: [
            {
              "name": "date",
              "label": "Date",
              "colIndex": 0
            },
            {
              "name": "amount",
              "label": "Amount",
              "colIndex": 0
            },
            {
              "name": "time",
              "label": "Time",
              "colIndex": 0
            }
          ],
          items:[]
        },
        quarterly_Struc: {
          columns: [
            {
              "name": "date",
              "label": "Date",
              "colIndex": 0
            },
            {
              "name": "amount",
              "label": "Amount",
              "colIndex": 0
            },
            {
              "name": "time",
              "label": "Time",
              "colIndex": 0
            }
          ],
          items:[]
        },
        half_yearly_Struc: {
          columns: [
            {
              "name": "date",
              "label": "Date",
              "colIndex": 0
            },
            {
              "name": "amount",
              "label": "Amount",
              "colIndex": 0
            },
            {
              "name": "time",
              "label": "Time",
              "colIndex": 0
            }
          ],
          items:[]
        },
        yearly_Struc: {
          columns: [
            {
              "name": "date",
              "label": "Date",
              "colIndex": 0
            },
            {
              "name": "amount",
              "label": "Amount",
              "colIndex": 0
            },
            {
              "name": "time",
              "label": "Time",
              "colIndex": 0
            }
          ],
          items:[]
        }
      };

      $timeout(function () {

        /* Daily Items */
        angular.forEach(rows['daily'], function (row) {
          struc.daily_Struc.items.push(row);
        });

        /* Weekly Items */
        angular.forEach(rows['weekly'], function (row) {
          struc.weekly_Struc.items.push(row);
        });

        /* Monthly Items */
        angular.forEach(rows['monthly'], function (row) {
          struc.monthly_Struc.items.push(row);
        });

        /* Quarterly Items */
        angular.forEach(rows['quarterly'], function (row) {
          struc.quarterly_Struc.items.push(row);
        });

        /* Half Yearly Items */
        angular.forEach(rows['half_yearly'], function (row) {
          struc.half_yearly_Struc.items.push(row);
        });

        /* Yearly Items */
        angular.forEach(rows['yearly'], function (row) {
          struc.yearly_Struc.items.push(row);
        });

        d.resolve(struc);

      }, 100);

      return d.promise;

    },
    populateGlobalTax: function () {
      var self = this;
      var d = $q.defer();

      $timeout(function () {

        var _allTaxes = [];
        if (self.bills.length > 0) {
          //console.log(self.bills);
          angular.forEach(self.bills, function (bill) {
            angular.forEach(bill._items, function (item) {


            });
          });

        } else {

        }

      }, 100);

      return d.promise;
    },
    getCustomDates: function () {
      var self = this;
      var d = $q.defer();

      $timeout(function () {
        var _dates = [];

        var start = new Date("2015-03-02");
        var end = new Date("2015-03-15");

        while(start < end){
          //console.log("Start: " + moment(start).format('YYYY-MM-DD'));
          var newDate = start.setDate(start.getDate() + 1);
          start = new Date(newDate);
          //console.log("Next Iteration: " + moment(start).format('YYYY-MM-DD'));
        }

        d.resolve();

      }, 100);

      return d.promise;
    },
    graphItemCategory: function (bills) {

      var tabData = [];
      var drillDownTabData = [];
      var graphData = {
        tabData: [],
        drilldown: []
      };

      _.chain(bills).groupBy('tab').map(function (tabItems) {
        var _tabTotal = 0;
        angular.forEach(tabItems, function (i) {
          _tabTotal += parseFloat(i.totalAmount);
        });

        /* 1st level */
        tabData.push({
          "name": tabItems[0].tab,
          "y": _tabTotal,
          "drilldown": tabItems[0].tab
        });

        /* 2nd Level */
        var _catD = [];
        _.chain(tabItems).groupBy('category').map(function (tabCatItems) {
          var _d = [];
          var _tCat = 0;
          angular.forEach(tabCatItems, function (cat) {
            _tCat += parseFloat(cat.totalAmount);
          });
          _d.push(tabCatItems[0].category);
          _d.push(_tCat);

          _catD.push(_d);

        });

        drillDownTabData.push({
          "name": tabItems[0].tab,
          "id": tabItems[0].tab,
          "data": _catD
        });

      });

      graphData = {
        tabData: tabData,
        drilldown: drillDownTabData
      };

      return graphData;


    },
    getTotalKOTQty: function (rows) {
      var _tQty = 0;
      angular.forEach(rows, function (kot) {
        angular.forEach(kot.items, function (item) {
          _tQty += parseFloat(item.quantity);
        });
      });
      return _tQty;
    },
    getTotalKOTNet: function (rows) {
      var _tNet = 0;
      angular.forEach(rows, function (kot) {
        angular.forEach(kot.items, function (item) {
          _tNet += parseFloat(item.totalNet);
        });
      });
      return _tNet;
    },
    getTotalKOTSubtotal: function (rows) {
      var _tSubtotal = 0;
      angular.forEach(rows, function (kot) {
        angular.forEach(kot.items, function (item) {
          _tSubtotal += parseFloat(item.subtotal);
        });
      });
      return _tSubtotal;
    },
    setDateRange: function () {

      /* Set Date Range as Per Cutoff time */
      var d = $q.defer();
      var self = this;

      $timeout(function () {

        var _resetTime = moment(self.filters.cutOffTimeSetting).toArray();
        var _from = moment(self.filters.fromDate).toArray();

        /* Check Reset Time and adjust ToDate */
        var _meridian = moment(self.filters.cutOffTimeSetting).format('a');
        if (_meridian === 'am') {
          var _t = moment(self.filters.toDate);
          _t.add(1, 'days');
          self.filters.toDate = _t.toISOString();
        } else {
          if (_meridian === 'pm') {
            var _f = moment(self.filters.fromDate);
            _f.subtract(1, 'days');
            self.filters.fromDate = _f.toISOString();
            _from = moment(self.filters.fromDate).toArray();
          }
        }

        var _to = moment(self.filters.toDate).toArray();

        var fromDateWithTime = new Date(_from[0], _from[1], _from[2], _resetTime[3], _resetTime[4]).toISOString();
        var toDateWithTime = new Date(_to[0], _to[1], _to[2], _resetTime[3], _resetTime[4]).toISOString();

        //console.log("FROM - Date: " + moment(fromDateWithTime).format('YYYY-MM-DD hh:mm:ss a'));
        //console.log("TO - Date: " + moment(toDateWithTime).format('YYYY-MM-DD hh:mm:ss a'));

        self.filters.fromDate = fromDateWithTime;
        self.filters.toDate = toDateWithTime;

        d.resolve({
          fromDate: fromDateWithTime,
          toDate: toDateWithTime
        });

      });

      return d.promise;

    },
    groupBillsByHoursTableOccupancy: function (dates) {
      var self = this;
      var d = $q.defer();

      $timeout(function () {

        var _f = moment(moment(dates.fromDate)).toArray()
        var _t = moment(moment(dates.toDate)).toArray();
        var _d = Math.abs(moment([_f[0],_f[1],_f[2]]).diff([_t[0],_t[1],_t[2]], 'days', true));
        //console.log(Math.abs(_d));

        if (_d > 1) {

          console.log("Diff day");
          // Break By Days
          self.breakByday(dates.fromDate, dates.toDate).then(function (dayIntervalGroups) {

            angular.forEach(dayIntervalGroups, function (dayGroup) {

              dayGroup.bills = [];
              var _d = moment(new Date(dayGroup.day[0], dayGroup.day[1], dayGroup.day[2])).format("YYYY-MM-DD");
              angular.forEach(self.bills, function (bill) {
                if (bill.closeTime === _d) {
                  dayGroup.bills.push(bill);
                }
              });

              /* Aggregate Bills for Interval */
              var _totalAmount = 0, _totalNet = 0, _totalTax = 0, _totalDiscount = 0;
              angular.forEach(dayGroup.bills, function (bill) {
                _totalAmount += parseFloat(bill.totalAmount);
                _totalNet += parseFloat(bill.totalNet);
                _totalTax += parseFloat(bill.totalTax);
                _totalDiscount += parseFloat(bill.totalDiscount);
              });
              dayGroup.totalAmount = _totalAmount;
              dayGroup.totalDiscount = _totalDiscount;
              dayGroup.totalTax = _totalTax;
              dayGroup.totalNet = _totalNet;
              dayGroup.totalBills = dayGroup.bills.length;

            });

            d.resolve(dayIntervalGroups);

          });


        } else {

          if (_d === 1 || _d < 1) {

            console.log("Same day");
            // Break By Hours
            self.breakDayInHours(dates.fromDate, dates.toDate, 2).then(function (intervalGroups) {

              //console.log(intervalGroups);
              angular.forEach(intervalGroups, function (intervalGroup) {
                // Create new bills attribute in group to push matching bills
                intervalGroup.bills = [];

                var _intervalFrom = new Date(intervalGroup.from[0], intervalGroup.from[1], intervalGroup.from[2], intervalGroup.from[3], intervalGroup.from[4]);
                var _intervalTo = new Date(intervalGroup.to[0], intervalGroup.to[1], intervalGroup.to[2], intervalGroup.to[3], intervalGroup.to[4]);

                // Cycle through all bills
                angular.forEach(self.bills, function (bill) {

                  var _billOpenTime = moment(bill._created).toArray();
                  var _billCloseTime = moment(bill._closeTime).toArray();
                  var _openTime = new Date(_billOpenTime[0], _billOpenTime[1], _billOpenTime[2], _billOpenTime[3], _billOpenTime[4]);
                  var _closeTime = new Date(_billCloseTime[0], _billCloseTime[1], _billCloseTime[2], _billCloseTime[3], _billCloseTime[4]);

                  /* Push Bill inside Time Interval */



                  if (_openTime > _intervalFrom && _openTime < _intervalTo && _closeTime > _intervalFrom && _closeTime < _intervalTo) {
                    intervalGroup.bills.push(bill);
                    // console.log("1. OI ", _intervalFrom,"CI ", _intervalTo,"OT ", _openTime,"CT ", _closeTime, "billID: ", bill.serialNumber);
                  }

                  if (_openTime > _intervalFrom && _openTime < _intervalTo && _closeTime > _intervalTo) {
                    intervalGroup.bills.push(bill);
                    //console.log("2. OI ", _intervalFrom,"CI ", _intervalTo,"OT ", _openTime,"CT ", _closeTime, "billID: ", bill.serialNumber);
                  }

                  if (_openTime < _intervalFrom && _closeTime > _intervalTo) {
                    intervalGroup.bills.push(bill);
                    // console.log("3. OI ", _intervalFrom,"CI ", _intervalTo,"OT ", _openTime,"CT ", _closeTime, "billID: ", bill.serialNumber);
                  }

                  if (_openTime < _intervalFrom && _closeTime > _intervalFrom && _closeTime < _intervalTo) {
                    intervalGroup.bills.push(bill);
                    // console.log("4. OI ", _intervalFrom,"CI ", _intervalTo,"OT ", _openTime,"CT ", _closeTime, "billID: ", bill.serialNumber);
                  }
                  /*if (_closeTime > _intervalFrom && _closeTime < _intervalTo && _openTime < _intervalFrom) {
                   intervalGroup.bills.push(bill);
                   }*/




                });

                /* Aggregate Bills for Interval */
                var _totalAmount = 0, _totalNet = 0, _totalTax = 0, _totalDiscount = 0;
                angular.forEach(intervalGroup.bills, function (bill) {
                  _totalAmount += parseFloat(bill.totalAmount);
                  _totalNet += parseFloat(bill.totalNet);
                  _totalTax += parseFloat(bill.totalTax);
                  _totalDiscount += parseFloat(bill.totalDiscount);
                });
                intervalGroup.totalAmount = _totalAmount;
                intervalGroup.totalDiscount = _totalDiscount;
                intervalGroup.totalTax = _totalTax;
                intervalGroup.totalNet = _totalNet;
                intervalGroup.totalBills = intervalGroup.bills.length;

              });

              d.resolve(intervalGroups);
            });


          }
        }




      });

      return d.promise;
    },
    groupBillsByHours: function (dates) {
      var self = this;
      var d = $q.defer();

      $timeout(function () {

        var _f = moment(moment(dates.fromDate)).toArray()
        var _t = moment(moment(dates.toDate)).toArray();
        var _d = Math.abs(moment([_f[0],_f[1],_f[2]]).diff([_t[0],_t[1],_t[2]], 'days', true));
        //console.log(Math.abs(_d));

        if (_d > 1) {

          console.log("Diff day");
          // Break By Days
          self.breakByday(dates.fromDate, dates.toDate).then(function (dayIntervalGroups) {

            angular.forEach(dayIntervalGroups, function (dayGroup) {

              dayGroup.bills = [];
              var _d = moment(new Date(dayGroup.day[0], dayGroup.day[1], dayGroup.day[2])).format("YYYY-MM-DD");
              angular.forEach(self.bills, function (bill) {
                if (bill.closeTime === _d) {
                  dayGroup.bills.push(bill);
                }
              });

              /* Aggregate Bills for Interval */
              var _totalAmount = 0, _totalNet = 0, _totalTax = 0, _totalDiscount = 0;
              angular.forEach(dayGroup.bills, function (bill) {
                _totalAmount += parseFloat(bill.totalAmount);
                _totalNet += parseFloat(bill.totalNet);
                _totalTax += parseFloat(bill.totalTax);
                _totalDiscount += parseFloat(bill.totalDiscount);
              });
              dayGroup.totalAmount = _totalAmount;
              dayGroup.totalDiscount = _totalDiscount;
              dayGroup.totalTax = _totalTax;
              dayGroup.totalNet = _totalNet;
              dayGroup.totalBills = dayGroup.bills.length;

            });

            d.resolve(dayIntervalGroups);

          });


        } else {
          if (_d === 1 || _d < 1) {

            console.log("Same day");
            // Break By Hours
            self.breakDayInHours(dates.fromDate, dates.toDate, 2).then(function (intervalGroups) {

              //console.log(intervalGroups);
              angular.forEach(intervalGroups, function (intervalGroup) {
                // Create new bills attribute in group to push matching bills
                intervalGroup.bills = [];

                var _intervalFrom = new Date(intervalGroup.from[0], intervalGroup.from[1], intervalGroup.from[2], intervalGroup.from[3], intervalGroup.from[4]);
                var _intervalTo = new Date(intervalGroup.to[0], intervalGroup.to[1], intervalGroup.to[2], intervalGroup.to[3], intervalGroup.to[4]);

                // Cycle through all bills
                angular.forEach(self.bills, function (bill) {

                  var _billCloseTime = moment(bill._closeTime).toArray();
                  var _billOpenTime = moment(bill._created).toArray();
                  var _openTime = new Date(_billOpenTime[0], _billOpenTime[1], _billOpenTime[2], _billOpenTime[3], _billOpenTime[4], _billOpenTime[5]);
                  var _closeTime = new Date(_billCloseTime[0], _billCloseTime[1], _billCloseTime[2], _billCloseTime[3], _billCloseTime[4], _billCloseTime[5]);

                  /* Push Bill inside Time Interval */
                  if (_closeTime > _intervalFrom && _closeTime < _intervalTo) {
                    intervalGroup.bills.push(bill);
                  }

                });

                /* Aggregate Bills for Interval */
                var _totalAmount = 0, _totalNet = 0, _totalTax = 0, _totalDiscount = 0;
                angular.forEach(intervalGroup.bills, function (bill) {
                  _totalAmount += parseFloat(bill.totalAmount);
                  _totalNet += parseFloat(bill.totalNet);
                  _totalTax += parseFloat(bill.totalTax);
                  _totalDiscount += parseFloat(bill.totalDiscount);
                });
                intervalGroup.totalAmount = _totalAmount;
                intervalGroup.totalDiscount = _totalDiscount;
                intervalGroup.totalTax = _totalTax;
                intervalGroup.totalNet = _totalNet;
                intervalGroup.totalBills = intervalGroup.bills.length;

              });

              d.resolve(intervalGroups);
            });


          }
        }




      });

      return d.promise;
    },
    groupBillsByDay: function () {
      var self = this;
      var defer = $q.defer();

      $timeout(function () {

        self.breakMonthInDays().then(function (days) {
          //console.log(days);

          angular.forEach(days, function (_day) {
            // Create new bills attribute in group to push matching bills
            _day.bills = [];


            var _currentDay = moment(new Date(_day.day[0], _day.day[1], _day.day[2], _day.day[3], _day.day[4])).format('YYYY-MM-DD');

            // Cycle through all bills
            angular.forEach(self.bills, function (bill) {

              /* Push Bill inside Day */
              if (_currentDay === bill.closeTime) {
                _day.bills.push(bill);
              }

            });

            /* Aggregate Bills for Interval */
            var _totalAmount = 0, _totalNet = 0, _totalTax = 0, _totalDiscount = 0;
            angular.forEach(_day.bills, function (bill) {
              _totalAmount += parseFloat(bill.totalAmount);
              _totalNet += parseFloat(bill.totalNet);
              _totalTax += parseFloat(bill.totalTax);
              _totalDiscount += parseFloat(bill.totalDiscount);
            });
            _day.totalAmount = _totalAmount;
            _day.totalDiscount = _totalDiscount;
            _day.totalTax = _totalTax;
            _day.totalNet = _totalNet;
            _day.totalBills = _day.bills.length;

          });

          //console.log('Grouped Bills ', days);
          defer.resolve(days);

        });

      });

      return defer.promise;

    },
    breakByday: function (fromDate, toDate) {
      var self = this;
      var defer = $q.defer();

      $timeout(function () {

        var _daysInMonth = [];
        var _beginMonth = moment(moment(fromDate));
        var _endMonth = moment(moment(toDate));
        var d = moment(moment(_beginMonth));

        do {

          //console.log("Start: " + d.format('YYYY-MM-DD'));
          var _f = moment(moment(d));
          d.add(1, 'days');
          if (d.isAfter(_endMonth)) {
            //console.log("End: " + end.format('YYYY-MM-DD'));
            _daysInMonth.push({
              day: _f.toArray()
            });
            break;
          }
          //console.log("End: " + d.format('YYYY-MM-DD'));
          var _t = moment(moment(d));
          _daysInMonth.push({
            day: _f.toArray()
          });

        } while (d.isBefore(_endMonth));
        //console.log('Days Group ',_daysInMonth);
        defer.resolve(_daysInMonth);

      });

      return defer.promise;
    },
    breakMonthInDays: function (currentDay) {

      var self = this;
      var defer = $q.defer();

      $timeout(function () {
        var _daysInMonth = [];

        var _currentDay = moment(new Date());
        var _beginMonth = moment(moment(self.filters.fromDate));
        var _endMonth = moment(moment(self.filters.toDate));
        var d = moment(moment(_beginMonth));

        //console.log(_beginMonth.format("YYYY-MM-DD"));
        //console.log(_endMonth.format("YYYY-MM-DD"));

        do {

          //console.log("Start: " + d.format('YYYY-MM-DD'));
          var _f = moment(moment(d));
          d.add(1, 'days');
          if (d.isAfter(_endMonth)) {
            //console.log("End: " + end.format('YYYY-MM-DD'));
            _daysInMonth.push({
              day: _f.toArray()
            });
            break;
          }
          //console.log("End: " + d.format('YYYY-MM-DD'));
          var _t = moment(moment(d));
          _daysInMonth.push({
            day: _f.toArray()
          });

        } while (d.isBefore(_endMonth));
        //console.log('Days Group ',_daysInMonth);
        defer.resolve(_daysInMonth);

      });

      return defer.promise;

    },
    breakDayInHours: function (fromDate, toDate, interval) {

      /* Break day in intervals according to reset time cycle */
      var self = this;
      var defer = $q.defer();

      $timeout(function () {

        var start = moment(moment(fromDate));
        var end = moment(moment(toDate));
        var d = moment(moment(start));

        //console.log("Start: ", start.toArray());
        //console.log("End: ", end.toArray());

        var biHourlyGroup = [];
        do {
          //console.log("Start: " + d.format('YYYY-MM-DD'));
          var _f = moment(moment(d));
          d.add(interval, 'hours');
          if (d.isAfter(end)) {
            //console.log("End: " + end.format('YYYY-MM-DD'));
            biHourlyGroup.push({
              from: _f.toArray(),
              to: end.toArray()
            });
            break;
          }
          //console.log("End: " + d.format('YYYY-MM-DD'));
          var _t = moment(moment(d));
          biHourlyGroup.push({
            from: _f.toArray(),
            to: _t.toArray()
          });
        } while (d.isBefore(end));

        //console.log('Ranges ', biHourlyGroup);
        defer.resolve(biHourlyGroup);

      });

      return defer.promise;

    },
    validateDeploymentSelection: function () {
      var self = this;
      var defer = $q.defer();
      $timeout(function () {
        if (self.filters.deployment_id != null) {
          defer.resolve(true); // Has Deployment ID
        } else {
          defer.resolve(false); // Does not have Deployment ID
        }
      });
      return defer.promise;
    },
    validateDateSelection: function () {
      var self = this;
      var defer = $q.defer();

      $timeout(function () {
        if (self.filters.fromDate != null && self.filters.toDate != null) {
          if (self.filters.fromDate > self.filters.toDate) {
            defer.resolve({error: true, message: "From Date cannot be greater than To Date."});
          } else {
            defer.resolve({error: false, message:""});
          }
        } else {
          defer.resolve({error:true, message: "From Date/ To Date is required."});
        }
      });

      return defer.promise;
    },
    getCompareData: function () {
      var self = this;
      var defer = $q.defer();
      $timeout(function () {
        //console.log("Before Bill Fetch");
        self.setDateRange().then(function (dates) {

          self.billResource.getDataRangeBills(
            {
              fromDate: dates.fromDate,
              toDate: dates.toDate,
              tenant_id: self.filters.tenant_id,
              deployment_id: self.filters.deployment_id,
              cutOffTimeSetting: self.filters.cutOffTimeSetting
            }).$promise.then(function (bills) {
            if (bills.length > 0) {
              self.addBills(bills).then(function () {

                self.prepareBillsForReport().then(function () {
                  self.getTotalSale().then(function (saleData) {
                    self.getAverageBills().then(function (avgBillRows) {
                      self.getTotalCovers().then(function (totalCovers) {

                        if (self.filters.autoDateRange === 'today' || self.filters.autoDateRange === 'yesterday') {

                          self.groupBillsByHours(dates).then(function (groupedIntervalBills) {
                            defer.resolve({
                              name: self.filters.name,
                              from: self.filters.fromDate,
                              to: self.filters.toDate,
                              autoDate: self.filters.autoDateRange,
                              deployment_id: self.filters.deployment_id,
                              saleData: saleData,
                              avgBills: avgBillRows,
                              hourIntervalBills: groupedIntervalBills,
                              covers: totalCovers
                            });
                          });

                        } else {

                          /* TODO: Need to Write logic for Date Ranges so it breaks by day */
                        }


                      }).catch(function () {

                      });

                    }).catch(function () {

                    });

                  }).catch(function () {

                  });

                }).catch(function (err) {

                });

              }).catch(function (err) {

              });
            } else {
              defer.resolve({
                error:true,
                message:"Error fetching Bills!",
                deployment_id: self.filters.deployment_id,
                name: self.filters.name
              });
            }
            //console.log(bills);
          }).catch(function (err) {

            defer.resolve({
              error:true,
              message:"No Data!",
              deployment_id: self.filters.deployment_id,
              name: self.filters.name
            });

          });

        });



      });
      return defer.promise;
    },
    getTotalCustomers: function (groupedBills) {

      var self = this;
      var defer = $q.defer();

      $timeout(function () {

        angular.forEach(groupedBills, function (gBill) {
          var _customers = [];
          var _totalCustomers = 0;
          angular.forEach(gBill.bills, function (_bill) {

            if (_.has(_bill._customer, 'customerId')) {
              _totalCustomers++;
              _customers.push(_bill._customer.customerId);
            }

          });

          gBill.totalNewCustomers = 0;
          gBill.totalRepeatCustomers = 0;
          gBill.newCustomers = [];
          gBill.repeatCustomers = [];
          gBill.customers = _.uniq(_customers);
          gBill.totalCustomers = gBill.customers.length;

        });


        defer.resolve(groupedBills);

      });

      return defer.promise;

    },
    getNewCustomers: function (groupedBills) {

      var self = this;
      var defer = $q.defer();
      $timeout(function () {
        //console.log('Groups', groupedBills);
        var _customerCountQ = [];
        angular.forEach(groupedBills, function (gBill, gBillIndex) {
          angular.forEach(gBill.customers, function (customerId) {
            _customerCountQ.push(self.billResource.getCustomerCount({
              tenant_id: localStorageService.get('tenant_id'),
              deployment_id: self.filters.deployment_id,
              customer_id: customerId,
              beforeDate: new Date(gBill.day[0], gBill.day[1], gBill.day[2]),
              gBillIndex: gBillIndex
            }).$promise);
          });
        });

        $q.all(_customerCountQ).then(function (customerCounts) {
          //console.log('Counts ', customerCounts);
          //console.log("GroupedBills ", groupedBills);

          var _new = 0;
          var _repeat = 0;
          angular.forEach(customerCounts, function (customerCount) {

            if (customerCount === 0) {
              _new++;
            } else {
              if (customerCount > 0) {
                _repeat++;
              }
            }

          });

          _.chain(customerCounts).groupBy('gBillIndex').map(function (dayCounts) {
            _.chain(dayCounts).groupBy('customerId').map(function (groupedCustomerId) {
              //console.log('Grouped Cust', groupedCustomerId)
              if (groupedCustomerId[0].count === 0) {
                groupedBills[groupedCustomerId[0].gBillIndex].totalNewCustomers += 1;
                groupedBills[groupedCustomerId[0].gBillIndex].newCustomers.push(groupedCustomerId[0].customerId);

              } else {
                if (groupedCustomerId[0].count > 0) {
                  groupedBills[groupedCustomerId[0].gBillIndex].totalRepeatCustomers += 1;
                  groupedBills[groupedCustomerId[0].gBillIndex].repeatCustomers.push(groupedCustomerId[0].customerId);
                }
              }

            });
          });

          defer.resolve(groupedBills);

        });


      });
      return defer.promise;

    },
    getRepeatCustomers: function (groupedBills) {

    }
  };

  return ( Report );
});