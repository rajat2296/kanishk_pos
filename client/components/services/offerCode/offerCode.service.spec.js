'use strict';

describe('Service: offerCode', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var offerCode;
  beforeEach(inject(function (_offerCode_) {
    offerCode = _offerCode_;
  }));

  it('should do something', function () {
    expect(!!offerCode).toBe(true);
  });

});
