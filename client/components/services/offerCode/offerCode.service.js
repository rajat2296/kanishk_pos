'use strict';

angular.module('posistApp')
    .factory('OfferCode', function ($resource) {
      return $resource('/api/offercodes/:controller/:id',
          {
            id:"@_id"
          },
          {
            destroy:{method:'delete',isArray:false},
            update:{method:'PUT',isArray:false},
            save: {method: 'POST', isArray:false,params:{controller:'save'}},
            create: {method: 'POST', isArray:false},
            get: {method: 'GET', isArray:true},
            findByName: {method: 'GET',params:{controller:'findByName'}, isArray:true},
              findByPrefix: {method: 'GET',params:{controller:'findByPrefix'}, isArray:true}

          }
      );
    });
