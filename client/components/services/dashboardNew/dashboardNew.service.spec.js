'use strict';

describe('Service: dashboardNew', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var dashboardNew;
  beforeEach(inject(function (_dashboardNew_) {
    dashboardNew = _dashboardNew_;
  }));

  it('should do something', function () {
    expect(!!dashboardNew).toBe(true);
  });

});
