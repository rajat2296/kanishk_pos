'use strict';

angular.module('posistApp')
  .factory('DashboardNew', function ($resource) {
    return $resource('/api/dashboards/:controller/:id',
      {
        id: '@_id'
      },
      {
        // saveData: {method: 'POST', isArray: false},
        // get: {method: 'GET', isArray: true},
        // findOne: {method: 'GET',isArray:true},
        // update: {method: 'PUT'},
        getDashboardDataByTime: {method: 'GET', params: {controller: "getDashboardDataByTime"}, isArray: true},
        getDailyReport: {method: 'GET', params: {controller: "getDailyReport"}, isArray: false},
        getMonthlyReport: {method: 'GET', params: {controller: "getMonthlyReport"}, isArray: false},
        getItemWiseReport: {method: 'GET', params: {controller: "getItemWiseReport"}, isArray: true},
        getTabWiseReport: {method: 'GET', params: {controller: "getTabWiseReport"}, isArray: true},
        getStaffWaiterWiseReport: {method: 'GET', params: {controller: "getStaffWaiterWiseReport"}, isArray: true},
        getTableOccupacyWiseReport: {method: 'GET', params: {controller: "getTableOccupacyWiseReport"}, isArray: true},
        getCustomerWiseReport: {method: 'GET', params: {controller: "getCustomerWiseReport"}, isArray: true}
      }
    );
  });
