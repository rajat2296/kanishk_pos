'use strict';

angular.module('posistApp')
  .factory('Bill', function ($q, $resource, $timeout, $http, $webSql, $modal, $state, Utils, $rootScope, growl, localStorageService,$ngBootbox, nSocket) {

    // Define the constructor function.
    function Bill(tableId, covers, items, isTakeOut, isDelivery, currentUser, db, tab, tabId, tabType, serialNumber, daySerialNumber, billNumber, isPrinted, splitNumber) {
      var self = this;
      // console.log(items);
      self.tab = tab || "";
      self.tabId = tabId || "";
      self.tabType = tabType || "";
      /*self.mode = 'new';*/
      self.db = db || angular.noop;
      self._id = guid();
      self._localId = undefined; // WebSQL ID
      self._waiter = {};
      self._delivery = {};
      self._currentUser = currentUser || "";
      self._meaningOfLife = true;
      self._isTakeOut = isTakeOut || false;
      self._isDelivery = isDelivery || false;
      self._covers = covers || null;
      self._tableId = tableId || null;
      self._tabDetails = {};
      self._customer = {};
      self._items = items || [];
      self._taxes = [];
      self._kots = [];
      self._kotsTaxes = [];
      self._cash = [];
      self._offers=[];
      self._debitCards = [];
      self._creditCards = [];
      self._otherCards = [];
      self._onlineCards = [];
      self._coupons = [];        //added for coupon settlement option
      self.complimentary = false;
      self.complimentaryComment = "";
      self.complimentaryHead = "";
      self.charges = {};
      /* generate serial numbers for each bill */


      self._serialNumber = serialNumber || "";
      self._daySerialNumber = daySerialNumber || "";
      self._billNumber = billNumber || "";
      self._splitNumber = splitNumber || "";

      self.instance_id = (localStorageService.get('instanceId')==undefined || localStorageService.get('instanceId')==undefined)?null:( localStorageService.get('instanceId').id==undefined?null:localStorageService.get('instanceId').id);
      self.prefix=null;
      self.deployment_id = localStorageService.get('deployment_id');
      self.tenant_id = localStorageService.get('tenant_id');

      self.printItems = [];
      self.printTaxes = [];
      self._coupons=[];        //added for coupon settlement option

      //Bil wise discount amount
      self.billDiscountAmount=0;
      //Offer amount and count validation on item wise
      self.offerItemValidation=[];
      self.onlineBill={};
      // Delivery
      self._deliveryTime;

      // Flags
      self.isPrinted = self.isPrinted || false;
      self.billPrintTime;

      self.isSettled = false;
      self.isVoid = false;
      self.voidComment = "";
      self.voidBillTime;
      self.voidDetail={};

      // Synced
      self.isSynced = false;
      self.syncedOn = new Date();
      // Data Structures
      self.creditCardForm = {};
      self.debitCardForm = {};
      self.otherForm = {};
      self.onlineForm = {};
      self.couponForm={};       // added for coupon settlement option
      // Settlement
      self.couponForm={};       // added for coupon settlement option

      self.payments = {cash: [], cards: []};
      self._created = new Date(); // Local creation or Bill Open Time
      self._closeTime = null;
      self.lastModified=null;
      self.billNumber = null;
      self.daySerialNumber = null;
      self.serialNumber = null;
      self.kotNumber = null;
      self.splitNumber = null;

      // Server Attributes
      self.ng_id = null;
      self.created = null;

      // Close Time Hack
      self.fauxCloseTime = null;

      // For Report Purpose Only
      self.totalQty = 0;
      self.totalAmount = 0;
      self.totalDiscount = 0;
      self.totalNet = 0;
      self.totalTax = 0;
      self.roundOff = 0;
      self.afterRoundOff = 0;

      // For Advance Booking
      self.advance={};
      self.aggregation={};
      // Reprint
      self.rePrint=[];
      self.transfer={};
      self.cardTransaction={};
      self.removedTaxes=[];
      self.denominations={2000:"",1000:"",500:"",100:"",50:"",20:"",10:"",5:"",2:"",1:""};

      //PassCode User
      self._currentPassCodeUser='';
    };

    var guid = (function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }

      return function () {
        /*return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();*/
        return  s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4()+ '-' +s4() + s4() + '-' + s4() + '-' + s4() + s4() + s4() ;
      };
    })();


    Bill.prototype = {
      addItem: function (item,updatedPrice) {
        item = angular.copy(item);
        if(!(updatedPrice==null||updatedPrice==undefined)){
          item.rate=updatedPrice;
        }
        var f_Items=   _.filter(this._items, function(fitem) {
          return !_.has(fitem,'offer');
        });
        //  var _item = _.find(this._items, {'_id': item._id});
        var _item = _.find(f_Items, {'_id': item._id});
        if (_item != undefined) {
          if(_.has(item,'discounts')||_.has(item,'offer') || (updatedPrice!=null)||(updatedPrice!=undefined)){
            item.quantity = 1;
            delete item.searchField;
            this._items.push(item);
          }else {
            if(_.has(_item,'addOns')){
              if(_item.addOns.length>0){
                item.quantity = 1;
                delete item.searchField;
                this._items.push(item);
              }else{
                _item.quantity=Utils.roundNumber( _item.quantity+ 1,3);
              }
            } else{
              _item.quantity =Utils.roundNumber( _item.quantity+ 1,3);
            }
            //_item.quantity += 1;
          }
        } else {
          item.quantity = 1;
          delete item.searchField;
          this._items.push(item);
        }
        this.processBill();
      },
      addItemWithQuantity: function (item, quantity,updatedPrice) {
        item = angular.copy(item);
        if(!(updatedPrice==null||updatedPrice==undefined)){
          item.rate=updatedPrice;
        }
        var _item = _.find(this._items, {'_id': item._id});
        if (_item != undefined && (updatedPrice==null || updatedPrice==undefined)) {
          if(_.has(_item,'addOns')){
            if(_item.addOns.length>0){
              item.quantity = quantity;
              delete item.searchField;
              this._items.push(item);
            }else{
              _item.quantity += quantity;
            }
          }else{
            _item.quantity += quantity;
          }
        } else {
          item.quantity = quantity;
          delete item.searchField;
          this._items.push(item);
        }
        this.processBill();
      },
      addItems: function (items) {
        this._items = [];
        angular.forEach(items, function (item) {
          self._items.push(item);
        });
        this.processBill();
      },
      getItemSubtotal: function () {
        var self = this;
        angular.forEach(this._items, function (item) {
          self.calculateItemDiscount(item);
        });

        /* Calculate Taxes */
        // this.getTaxes();
      },
      getKotDeleteHistorySubtotal: function () {
        var self = this;
        angular.forEach(this._kots, function (kot) {
          angular.forEach(kot.deleteHistory, function (item) {
            self.calculateItemDiscount(item);
          });
        });
      },
      getKotSubtotal: function () {
        var self = this;
        angular.forEach(this._kots, function (kot) {
          angular.forEach(kot.items, function (item) {
            self.calculateItemDiscount(item);
          });
        });
      },
      calculateAllTaxes:function(){
        var self = this;
        angular.forEach(this._items, function (item) {
          self.calculateTaxOnItem(item);
        });
        /* Calculate Taxes */
        this.getTaxes();

        angular.forEach(this._kots, function (kot) {
          angular.forEach(kot.deleteHistory, function (item) {
            self.calculateTaxOnItem(item);
          });
        });
        angular.forEach(this._kots, function (kot) {
          angular.forEach(kot.items, function (item) {
            // console.log(item);
            self.calculateTaxOnItem(item);
          });
        });
      },
      calculateAllBillWiseDiscountOnItem:function() {
        var self = this;
        var tDiscount = parseFloat(this.billDiscountAmount);
        var tAmount = 0;
        var billwiseOffer=null;
        var isExclude = false;
        if (this._offers.length > 0) {
          var _filerBillWise = _.filter(this._offers, {'isBillWise': true});
          if (_filerBillWise.length > 0) {
            billwiseOffer = _filerBillWise[0].offer;
          }
        }

        if(billwiseOffer!=null) {
          if (_.has(billwiseOffer, 'applicable')) {
            if (_.has(billwiseOffer.applicable, 'on')) {
              if (_.has(billwiseOffer.applicable, 'isExclude')) {
                if (billwiseOffer.applicable.on == 'itemCriteria' && billwiseOffer.applicable.isExclude) {
                  isExclude = true;
                }
              }
            }
          }
        }


        angular.forEach(this._items, function (item) {
          var isExcludeItem=false;
          if(isExclude) {
            if (self.isCategoryOrItemInOffer(item, billwiseOffer)) {
              isExcludeItem=true;
            }
          }
          if(!isExcludeItem) {
            var discountedAmount = ((self.getTotalItemWiseDiscount(item) >= item.subtotal) ? 0 : (item.subtotal - self.getTotalItemWiseDiscount(item)));
            tAmount +=parseFloat( discountedAmount);
          }
        });

        angular.forEach(this._kots, function (kot) {
          if (!kot.isVoid) {
          angular.forEach(kot.items, function (item) {
            var isExcludeItem=false;
            if(isExclude) {
              if (self.isCategoryOrItemInOffer(item, billwiseOffer)) {
                isExcludeItem=true;
              }
            }

            if(!isExcludeItem) {
              var discountedAmount = ((self.getTotalItemWiseDiscount(item) >= item.subtotal) ? 0 : (item.subtotal - self.getTotalItemWiseDiscount(item)));
              tAmount += parseFloat(discountedAmount);
            }
          });
         }
        });

        angular.forEach(this._items, function (item) {
          var isExcludeItem=false;
          if(isExclude) {
            if (self.isCategoryOrItemInOffer(item, billwiseOffer)) {
              isExcludeItem=true;
            }
          }
          if(!isExcludeItem) {
            var discountedAmount = ((self.getTotalItemWiseDiscount(item) >= item.subtotal) ? 0 : (item.subtotal - self.getTotalItemWiseDiscount(item)));
            var itemWiseBillDiscount = (tAmount==0 ? 0: Utils.roundNumber((  discountedAmount / tAmount * tDiscount), ($rootScope.disable_roundoff?2:2)));
            // console.log(itemWiseBillDiscount);
            item.billDiscountAmount = itemWiseBillDiscount;
          }else{
            item.billDiscountAmount=0;
          }
        });
        /* angular.forEach(this._kots, function (kot) {
         angular.forEach(kot.deleteHistory, function (item) {
         self.calculateTaxOnItem(item);
         });
         });*/
        angular.forEach(this._kots, function (kot) {
          angular.forEach(kot.items, function (item) {
            var isExcludeItem=false;
            if(isExclude) {
              if (self.isCategoryOrItemInOffer(item, billwiseOffer)) {
                isExcludeItem=true;
              }
            }
            if(!isExcludeItem) {
              var tItemDiscount = self.getTotalItemWiseDiscount(item);
              //console.log(item.subtotal);
              var discountedAmount = ((tItemDiscount >= item.subtotal) ? 0 : (item.subtotal - tItemDiscount));
              //console.log(discountedAmount);
              var itemWiseBillDiscount =(tAmount== 0 ? 0 : Utils.roundNumber((   discountedAmount / tAmount * tDiscount), ($rootScope.disable_roundoff?2:2)));
              item.billDiscountAmount = itemWiseBillDiscount;
            }else{
              item.billDiscountAmount=0;
            }
            //console.log(tAmount);
          });
        });
      },
      getTotalItemWiseDiscount:function(item){
        var _totalDiscount=0;
        if (_.has(item, 'discounts') && item.discounts.length > 0 && item.isDiscounted) {
          _.forEach(item.discounts, function (discount, index) {
            if (discount.type === 'percentage') {
              _totalDiscount += parseFloat(discount.discountAmount);
            }
            if (discount.type === 'fixed') {
              _totalDiscount += parseFloat(discount.value);
            }
          });
        }
        return parseFloat(_totalDiscount);
      },
      calculateItemDiscount: function (item) {
        var self=this;
        //console.log(item);
        /*if (!item.manualOverride) {*/
        var baseRate = parseFloat(item.rate);
        var priceQuantity = Utils.roundNumber(( parseFloat(item.rate) * parseFloat(item.quantity)), 2);

        /* Calculate Discounts */
        if (_.has(item, 'discounts') && item.discounts.length > 0) {
          item.rateBeforeDiscount = baseRate;
          item.isDiscounted = true;
          _.forEach(item.discounts, function (discount, index) {
            var _d = 0;
            if (discount.type === 'percentage') {
              _d = Utils.roundNumber((  (parseFloat(discount.value) / 100) * (item.rate * item.quantity)), ($rootScope.disable_roundoff?2:2));
              discount.discountAmount = (!_.isNaN(_d)) ? _d : 0;
            }else{
              discount.discountAmount =Utils.roundNumber( parseFloat(discount.value),2);
            }
          });
        }

        /* Calculate Taxes */
        /*
         var _discountedBaseRate = 0;
         angular.forEach(item.taxes, function (tax) {
         *//* Check if Item is Discounted *//*
         var _itemTotalDiscount = 0;
         if (item.isDiscounted && item.discounts.length > 0) {
         _.forEach(item.discounts, function (discount, index) {
         if (discount.type === 'percentage') {
         _itemTotalDiscount += Utils.roundNumber(parseFloat(discount.discountAmount), 0);
         }
         if (discount.type === 'fixed') {
         _itemTotalDiscount += parseFloat(discount.value);
         }
         });
         }

         var _t;
         var taxableAmount=((parseFloat(item.rate) * parseFloat(item.quantity))-_itemTotalDiscount);
         if(self.complimentary){taxableAmount=0;}
         if (tax.type === "percentage") {
         _t = (parseFloat(tax.value) / 100) * taxableAmount
         // Add New Tax Amount Attribute
         tax.tax_amount = _t;
         tax.baseRate_preTax = taxableAmount;
         *//*baseRate += _t;*//*
         tax.baseRate_postTax = (_discountedBaseRate * parseFloat(item.quantity)) + _t;
         _t = 0;
         }
         if (tax.type === "fixed") {
         _t = parseFloat(tax.value);
         // Add New Tax Amount Attribute
         tax.tax_amount = _t;
         tax.baseRate_preTax = taxableAmount;
         *//*baseRate += _t;*//*
         tax.baseRate_postTax = _discountedBaseRate * parseFloat(item.quantity) + _t;
         _t = 0;
         }

         *//* TODO: Calculate Sub Taxes*//*

         angular.forEach(tax.cascadingTaxes, function (subtax) {
         var _subT;
         if(subtax.selected) {
         var _tfilter = _.filter(item.taxes, {"_id": subtax._id});
         if (_tfilter.length > 0) {
         if (subtax.type === "percentage") {
         if (subtax.isTaxableOnBaseRate) {
         // Changes has been made to fix the calculation bug

         // _subT = (parseFloat(subtax.value) / 100) * tax.baseRate_preTax;
         _subT = (parseFloat(subtax.value) / 100) * tax.tax_amount;
         } else {
         //_subT = (parseFloat(subtax.value) / 100) * tax.baseRate_postTax;
         _subT = (parseFloat(subtax.value) / 100) * tax.tax_amount;
         }
         subtax.baseRate_preTax = tax.tax_amount;
         subtax.tax_amount = Utils.roundNumber(_subT, 2);
         _subT = 0;
         }
         }
         else {
         subtax.tax_amount = 0;
         }
         }else
         {
         subtax.tax_amount = 0;
         }
         });

         });*/
        /*End Calculation Taxes on Items */

        item.subtotal = Utils.roundNumber((  parseFloat(item.rate) * parseFloat(item.quantity)), 2);
        item.priceQuantity = parseFloat(item.rate) * parseFloat(item.quantity);
        this.calculateTaxesAddOns(item);
        // return item;
        /*}*/
        //console.log(item);
      },

      calculateTaxesAddOns:function(item){
        if(_.has(item,'addOns')){
          _.forEach(item.addOns,function(addon){
            addon.taxes=angular.copy( item.taxes);
            angular.forEach(addon.taxes, function (tax) {
              var _t;
              var taxableAmount=((parseFloat(addon.rate) * parseFloat(addon.quantity)));
              if(self.complimentary){taxableAmount=0;}
              if (tax.type === "percentage") {
                _t = (parseFloat(tax.value) / 100) * taxableAmount
                // Add New Tax Amount Attribute
                tax.tax_amount = _t;
                tax.baseRate_preTax = taxableAmount;
                /*baseRate += _t;*/
                tax.baseRate_postTax = ( parseFloat(addon.quantity)) + _t;
                _t = 0;
              }
              if (tax.type === "fixed") {
                _t = parseFloat(tax.value);
                // Add New Tax Amount Attribute
                tax.tax_amount = _t;
                tax.baseRate_preTax = taxableAmount;
                /*baseRate += _t;*/
                tax.baseRate_postTax = addon.rate * parseFloat(addon.quantity) + _t;
                _t = 0;
              }

              /* TODO: Calculate Sub Taxes*/

              angular.forEach(tax.cascadingTaxes, function (subtax) {
                var _subT;
                if(subtax.selected) {
                  var _tfilter = _.filter(addon.taxes, {"_id": subtax._id});
                  if (_tfilter.length > 0) {
                    if (subtax.type === "percentage") {
                      if (subtax.isTaxableOnBaseRate) {
                        // Changes has been made to fix the calculation bug

                        // _subT = (parseFloat(subtax.value) / 100) * tax.baseRate_preTax;
                        _subT = (parseFloat(subtax.value) / 100) * tax.tax_amount;
                      } else {
                        //_subT = (parseFloat(subtax.value) / 100) * tax.baseRate_postTax;
                        _subT = (parseFloat(subtax.value) / 100) * tax.tax_amount;
                      }
                      subtax.baseRate_preTax = tax.tax_amount;
                      subtax.tax_amount = Utils.roundNumber(_subT, 2);
                      _subT = 0;
                    }
                  }
                  else {
                    subtax.tax_amount = 0;
                  }
                }else
                {
                  subtax.tax_amount = 0;
                }
              });

            });
            addon.subtotal = Utils.roundNumber((  parseFloat(addon.rate) * parseFloat(addon.quantity)), 2);
          })
        }
        // console.log(item.addOns);
      },
      calculateTaxOnItem:function(item){
        var self=this;

         if(self.removedTaxes.length>0){
         var taxes=[];
        angular.forEach(item.taxes,function(tax){
          if(!self.isRemoved(tax))
           taxes.push(tax);
        })
        item.taxes=taxes;

        angular.forEach(item.taxes,function(tax){
          if(tax.cascadingTaxes){
            var ctaxes=[];
            angular.forEach(tax.cascadingTaxes,function(cTax){
                if(!self.isRemoved(cTax))
                  ctaxes.push(cTax);
            });
            tax.cascadingTaxes=angular.copy(ctaxes);
          }
        })

       if(item.addOns){
        angular.forEach(item.addOns,function(addOn){
          var addOnTaxes=[];
          angular.forEach(addOn.taxes,function(tax){
             if(!self.isRemoved(tax))
              addOnTaxes.push(tax);
          })
          addOn.taxes=angular.copy(addOnTaxes);
        })
      }

      if(item.addOns){
        angular.forEach(item.addOns,function(addOn){
          angular.forEach(addOn.taxes,function(tax){
            if(tax.cascadingTaxes){
              var ctaxes=[];
              angular.forEach(tax.cascadingTaxes,function(ctax){
                if(!self.isRemoved(ctax))
                  ctaxes.push(ctax);
              })
              tax.cascadingTaxes=angular.copy(ctaxes);
            }
          })
        })
      }
    }
        var _discountedBaseRate = 0;
        angular.forEach(item.taxes, function (tax) {
          /* Check if Item is Discounted */
          var _itemTotalDiscount = 0;
          if(_.has(item,'discounts') && _.has(item,'isDiscounted')){
            if (item.isDiscounted && item.discounts.length > 0) {
              _.forEach(item.discounts, function (discount, index) {
                if (discount.type === 'percentage') {
                  _itemTotalDiscount += Utils.roundNumber(parseFloat(discount.discountAmount), ($rootScope.disable_roundoff?2:2));
                }
                if (discount.type === 'fixed') {
                  _itemTotalDiscount += parseFloat(discount.value);
                }
              });
            }
          }
          var billWiseItemDiscount=0;
          if(_.has(item,'billDiscountAmount')){
            billWiseItemDiscount=item.billDiscountAmount;
          }
          // console.log(billWiseItemDiscount);
          var _t;
          var taxableAmount=((parseFloat(item.rate) * parseFloat(item.quantity))-(_itemTotalDiscount+billWiseItemDiscount));
          if(self.complimentary){taxableAmount=0;}
          if (tax.type === "percentage") {

            _t = (parseFloat(tax.value) / 100) * taxableAmount
            // Add New Tax Amount Attribute
            //console.log(_t);
            tax.tax_amount = Utils.roundNumber( _t,2);
            tax.baseRate_preTax =  taxableAmount;
            /*baseRate += _t;*/
            tax.baseRate_postTax = (_discountedBaseRate * parseFloat(item.quantity)) + _t;
            _t = 0;
          }
          if (tax.type === "fixed") {
            _t = parseFloat(tax.value);
            // Add New Tax Amount Attribute
            tax.tax_amount = _t;
            tax.baseRate_preTax = taxableAmount;
            /*baseRate += _t;*/
            tax.baseRate_postTax = _discountedBaseRate * parseFloat(item.quantity) + _t;
            _t = 0;
          }

          /* TODO: Calculate Sub Taxes*/

          angular.forEach(tax.cascadingTaxes, function (subtax) {
            var _subT;
            if(subtax.selected) {
              var _tfilter = _.filter(item.taxes, {"_id": subtax._id});
              if (_tfilter.length > 0) {
                if (subtax.type === "percentage") {
                  if (subtax.isTaxableOnBaseRate) {
                    // Changes has been made to fix the calculation bug
                    // _subT = (parseFloat(subtax.value) / 100) * tax.baseRate_preTax;
                    _subT = (parseFloat(subtax.value) / 100) * tax.tax_amount;
                  } else {
                    //_subT = (parseFloat(subtax.value) / 100) * tax.baseRate_postTax;
                    _subT = (parseFloat(subtax.value) / 100) * tax.tax_amount;
                  }
                  subtax.baseRate_preTax = tax.tax_amount;
                  subtax.tax_amount = Utils.roundNumber(_subT, 2);
                  _subT = 0;
                }
              }
              else {
                subtax.tax_amount = 0;
              }
            }else
            {
              subtax.tax_amount = 0;
            }
          });

        });
        //console.log(item);
      },

      getKotTaxTotal: function (kotIndex) {
        var _kotTax = 0;
        angular.forEach(this._kots[kotIndex].taxes, function (tax) {
          _kotTax += parseFloat(tax.tax_amount);
        });
        return _kotTax;
      },
      getKotTaxes: function () {

        angular.forEach(this._kots, function (kot) {
          var _allTaxes = [];

          angular.forEach(kot.items, function (item) {
            angular.forEach(item.taxes, function (tax) {
              _allTaxes.push(angular.copy(tax));
              angular.forEach(tax.cascadingTaxes, function (st) {
                _allTaxes.push(angular.copy(st));
              });
            });
            /*integration of addon taxation*/
            if(_.has(item,'addOns')){
              _.forEach(item.addOns,function(addon){
                angular.forEach(addon.taxes, function (tax) {
                  _allTaxes.push(angular.copy(tax));
                  angular.forEach(tax.cascadingTaxes, function (st) {
                    _allTaxes.push(angular.copy(st));
                  });
                });
              })
            }
            /*End addon*/
          });

          var _gTax = [];
          var _groupedTaxes = _.chain(_allTaxes).groupBy('name').map(function (t) {

            var _taxAmount = 0;
            angular.forEach(t, function (gt) {
              _taxAmount += parseFloat(gt.tax_amount);
            });
            if(_taxAmount>0) {
              _gTax.push({
                "id": t[0]._id,
                "name": t[0].name,
                "tax_amount": parseFloat(_taxAmount).toFixed(2)
              });
            }
          });

          kot.taxes = [];
          kot.taxes = _gTax;

          var _kTax = 0;
          angular.forEach(kot.taxes, function (t) {
            _kTax += parseFloat(t.tax_amount);
          });
          kot.taxAmount = _kTax;
        });
      },
      aggregateKotTaxes: function () {
        var _allKotsTaxes = [];
        var _flattenedKotsTaxes = [];

        if (this._kots.length > 0) {

          angular.forEach(this._kots, function (kot) {
            var _allTaxes = [];
            if(!kot.isVoid) {
              angular.forEach(kot.items, function (item) {
                angular.forEach(item.taxes, function (tax) {
                  _allTaxes.push(angular.copy(tax));
                  angular.forEach(tax.cascadingTaxes, function (st) {
                    _allTaxes.push(angular.copy(st));

                  });
                });
                /*integration of addon taxation*/
                if(_.has(item,'addOns')){
                  _.forEach(item.addOns,function(addon){
                    angular.forEach(addon.taxes, function (tax) {
                      _allTaxes.push(angular.copy(tax));
                      angular.forEach(tax.cascadingTaxes, function (st) {
                        _allTaxes.push(angular.copy(st));
                      });
                    });
                  })
                }
                /*End addon*/
              });
            }
            // console.log(_allTaxes);
            var _groupedTaxes = _.chain(_allTaxes).groupBy('name').map(function (t) {
              var _taxAmount = 0;
              var _baseAmount=0;

              angular.forEach(t, function (gt) {
                _taxAmount += parseFloat(gt.tax_amount);
                if(_.has(gt,'baseRate_preTax')){
                  if(!(gt.baseRate_preTax==null||gt.baseRate_preTax==undefined)){
                    _baseAmount+=parseFloat(gt.baseRate_preTax);
                  }
                }
              });

              _allKotsTaxes.push({
                "id": t[0]._id,
                "name": t[0].name,
                "tax_amount": parseFloat(_taxAmount).toFixed(2),
                "baseAmount":parseFloat(_baseAmount).toFixed(2)
              });
            });

          });

        }


        /* Add _items Taxes and then aggregate */
        if (this._items.length > 0) {
          angular.forEach(this._items, function (item) {
            angular.forEach(item.taxes, function (tax) {
              _allKotsTaxes.push(angular.copy(tax));
              angular.forEach(tax.cascadingTaxes, function (st) {
                _allKotsTaxes.push(angular.copy(st));

              });
            });
            /*integration of addon taxation*/
            if(_.has(item,'addOns')){
              _.forEach(item.addOns,function(addon){
                angular.forEach(addon.taxes, function (tax) {
                  _allKotsTaxes.push(angular.copy(tax));
                  angular.forEach(tax.cascadingTaxes, function (st) {
                    _allKotsTaxes.push(angular.copy(st));
                  });
                });
              })
            }
            /*End addon*/
          });
        }

        var _groupByAllKots = _.chain(_allKotsTaxes).groupBy('name').map(function (t) {
          var _taxAmount = 0;
          var _baseAmount=0;
          angular.forEach(t, function (gt) {
            _taxAmount += parseFloat(gt.tax_amount);
            _baseAmount+=parseFloat(gt.baseAmount);
          });

          // if (t[0].tax_amount > 0) {
          _flattenedKotsTaxes.push({
            "id": t[0]._id,
            "name": t[0].name,
            "tax_amount": parseFloat(_taxAmount).toFixed(2),
            "baseAmount":parseFloat(_baseAmount).toFixed(2)
          });
          //  }
        });
        this._kotsTaxes = _flattenedKotsTaxes;
      },
      getTaxes: function () {
        var _allTaxes = [];
        angular.forEach(this._items, function (item) {
          angular.forEach(item.taxes, function (tax) {
            _allTaxes.push(angular.copy(tax));
            angular.forEach(tax.cascadingTaxes, function (st) {
              _allTaxes.push(angular.copy(st));

            });
          });
          /*integration of addon taxation*/
          if(_.has(item,'addOns')){
            _.forEach(item.addOns,function(addon){
              angular.forEach(addon.taxes, function (tax) {
                _allTaxes.push(angular.copy(tax));
                angular.forEach(tax.cascadingTaxes, function (st) {
                  _allTaxes.push(angular.copy(st));
                });
              });
            })
          }
          /*End addon*/
        });

        var _gTax = [];
        var _groupedTaxes = _.chain(_allTaxes).groupBy('name').map(function (t) {
          //console.log(t);
          if (t.length > 0) {
            var _taxAmount = 0;
            angular.forEach(t, function (gt) {
              _taxAmount += parseFloat(gt.tax_amount);
            });
            if (_taxAmount > 0) {
              _gTax.push({
                "id": t[0]._id,
                "name": t[0].name,
                "tax_amount": parseFloat(_taxAmount).toFixed(2)
              });
            }
          }

        });

        this._taxes = [];
        this._taxes = _gTax;
      },
      getTotalDiscount: function () {
        // console.log(this);
        var _totalDiscount = 0;

        _.forEach(this._items, function (item, index) {
          if(_.has(item,'discounts') && _.has(item,'isDiscounted')){
            if (_.has(item, 'discounts') && item.discounts.length > 0 && item.isDiscounted) {
              _.forEach(item.discounts, function (discount, index) {
                if (discount.type === 'percentage') {
                  _totalDiscount += parseFloat(discount.discountAmount);
                }
                if (discount.type === 'fixed') {
                  _totalDiscount += parseFloat(discount.value);
                }
              });
            }
          }
        });
        //  this.totalDiscount=Utils.roundNumber(parseFloat(_totalDiscount));

        return Utils.roundNumber(parseFloat(_totalDiscount), ($rootScope.disable_roundoff?2:2))+ Utils.roundNumber(parseFloat(this.billDiscountAmount), ($rootScope.disable_roundoff?2:2) );
      },
      getBillWiseOfferDiscount:function(){
        // console.log(this._items);
        /*calculate billwise discount*/
        if(this._offers.length==0){return 0;}
        var _filerBillWise= _.filter( this._offers,{'isBillWise':true});
        var discountAmount=0;
        var appliedBillWiseOffer={};
        if(_filerBillWise.length>0){
          appliedBillWiseOffer=_filerBillWise[0].offer;
          // console.log(_filerBillWise[0]);
        }else{return 0; }
        if(this.validateBillWiseOffer(appliedBillWiseOffer)){
          if(appliedBillWiseOffer.type.name=='percent') {
            discountAmount = Utils.roundNumber(parseFloat(this.getTotalAmountWithoutOffer(appliedBillWiseOffer) * .01 * appliedBillWiseOffer.type.value),($rootScope.disable_roundoff?2:2));
          }else {discountAmount= parseFloat( appliedBillWiseOffer.type.value);}
        }
        this.billDiscountAmount=discountAmount;
        _filerBillWise[0].offerAmount=discountAmount;
        // console.log(discountAmount);
        //return discountAmount;
      },
      getTotalAmountWithoutOffer:function(offer){
        var self=this;
        // console.log(offer);
        var isExclude=false;
        if(_.has(offer,'applicable')){
          if(_.has(offer.applicable,'on')){
            if(_.has(offer.applicable,'isExclude')){
              if(offer.applicable.on=='itemCriteria' && offer.applicable.isExclude){
                isExclude=true;
              }
            }
          }
        }
        var amount = 0;//(this.getSubtotal() + this.getKotsTotalAmount());
        // console.log(this._items);
        if(!isExclude) {
          _.forEach(this._items, function (item) {

            if (!((_.has(item, 'discounts') && item.discounts.length > 0 ))) {
              //  console.log(item);
              amount += parseFloat(item.subtotal);
            } else {
              amount += parseFloat((item.subtotal - item.discounts[0].discountAmount));
            }
          });
          _.forEach(this._kots, function (kot) {
            if (!kot.isVoid) {
              _.forEach(kot.items, function (item) {
                if (!((_.has(item, 'discounts') && item.discounts.length > 0))) {
                  amount += parseFloat(item.subtotal);
                } else {
                  amount += parseFloat((item.subtotal - item.discounts[0].discountAmount));
                }
              })
            }
          });
        }else{
          //alert();
          _.forEach(this._items, function (item) {
            if(!self.isCategoryOrItemInOffer(item,offer)) {
              if (!((_.has(item, 'discounts') && item.discounts.length > 0 ))) {
                //  console.log(item);
                amount += parseFloat(item.subtotal);
              } else {
                amount += parseFloat((item.subtotal - item.discounts[0].discountAmount));
              }
            }
          });
          _.forEach(this._kots, function (kot) {
            if (!kot.isVoid) {
              _.forEach(kot.items, function (item) {
                if(!self.isCategoryOrItemInOffer(item,offer)) {
                  if (!((_.has(item, 'discounts') && item.discounts.length > 0))) {
                    amount += parseFloat(item.subtotal);
                  } else {
                    amount += parseFloat((item.subtotal - item.discounts[0].discountAmount));
                  }
                }
              })
            }
          });
        }
        return amount;
      },
      isCategoryOrItemInOffer:function(item,offer){
        var flag=false;
        if(_.has( offer.applicable,'items')){
          var offerItems=offer.applicable.items;
          for(var i=0;i<offerItems.length;i++){
            if(offerItems[i].item!=""){
              if(item._id==offerItems[i].item._id){
                flag =true;
                break;
              }
            }else{
              if(item.category._id==offerItems[i].category._id){
                flag=true;
                break;
              }
            }
          }
        }
        return flag;
      },
      validateBillWiseOffer:function(offer){
        // console.log((this.getSubtotal()+this.getKotsTotalAmount()));
        var flag=false;
        var validateItemQty=false;
        var validateBillAmount=false;
        if(!(offer.minItemCount==null||offer.minItemCount=="")){
          if( parseInt( offer.minItemCount)<=this.getGTotalQuantity()){
            validateItemQty=true;
          }else
          {return false;}
        }else{validateItemQty=true;}
        if(!(offer.minBillAmount==null||offer.minBillAmount=="")){
          if( parseFloat( offer.minBillAmount)<=this.getTotalAmountWithoutOffer(offer)){
            validateBillAmount=true;
          }else
          {return false;}
        }else{ validateBillAmount=true;}
        if(validateItemQty && validateBillAmount){
          flag = true;
        }
        return flag;
      },
      getSubtotal: function () {
        var subtotal = 0;
        angular.forEach(this._items, function (item) {
          subtotal += parseFloat(item.subtotal);
          /*addon item integartion*/
          if(_.has(item,'addOns')){
            _.forEach(item.addOns,function(addon){
              subtotal += parseFloat(addon.subtotal);
              //quantity += parseFloat(addon.quantity);
            })
          }
          /*end addon*/
        });
        return Utils.roundNumber(parseFloat(subtotal),($rootScope.disable_roundoff?2:2));
      },
      getKotIndexSubtotal: function (index) {
        var subtotal = 0;
        angular.forEach(this._kots[index].items, function (item) {
          subtotal += parseFloat(item.subtotal);
          /*addon item integartion*/
          if(_.has(item,'addOns')){
            _.forEach(item.addOns,function(addon){
              subtotal += parseFloat(addon.subtotal);
              // quantity += parseFloat(addon.quantity);
            })
          }
          /*end addon*/
        });
        return Utils.roundNumber(parseFloat(subtotal), 2);
      },
      getKotItemSubtotal: function () {

        angular.forEach(this._kots, function (kot) {
          var subtotal = 0;
          var quantity = 0;
          angular.forEach(kot.items, function (item) {
            subtotal += parseFloat(item.subtotal);
            quantity += parseFloat(item.quantity);
            /*addon item integartion*/
            if(_.has(item,'addOns')){
              _.forEach(item.addOns,function(addon){
                subtotal += parseFloat(addon.subtotal);
                quantity += parseFloat(addon.quantity);
              })
            }
            /*end addon*/
          });
          kot.totalAmount = parseFloat(subtotal);
          kot.totalItems = parseFloat(quantity);
        });

        /* Calculate KOT Items Total Discount */
        this.calculateKotTotalDiscount();
      },
      calculateKotTotalDiscount: function () {
        angular.forEach(this._kots, function (kot) {
          var _d = 0;
          if(!kot.isVoid) {
            angular.forEach(kot.items, function (item) {
              if(_.has(item,'discounts') ){
              angular.forEach(item.discounts, function (discount) {
                if (discount.type === 'percentage') {
                  _d += Utils.roundNumber(parseFloat(discount.discountAmount), ($rootScope.disable_roundoff?2:2));
                }
                if (discount.type === 'fixed') {
                  _d += parseFloat(discount.value);
                }
              });
             }
            });
          }
          kot.totalDiscount = parseFloat(_d);
        });
      },
      getKotTotalDiscount: function () {
        var _t = 0;
        angular.forEach(this._kots, function (kot) {
          _t += kot.totalDiscount;
        });
        /*  if (this._items.length > 0) {
         _t += parseFloat(this.getTotalDiscount());
         }*/
        return Utils.roundNumber(parseFloat(_t),  ($rootScope.disable_roundoff?2:2));
      },
      getTotalTax: function () {
        var _taxTotal = 0;
        angular.forEach(this._taxes, function (tax) {
          _taxTotal += parseFloat(tax.tax_amount);
        });
        return Utils.roundNumber(_taxTotal, 2);
      },
      getGTotalQuantity: function () {
        var tQty = 0;
        angular.forEach(this._items, function (_item) {
          tQty += _item.quantity;
          /*addon item integartion*/
          if(_.has(_item,'addOns')){
            _.forEach(_item.addOns,function(addon){
              //subtotal += parseFloat(addon.subtotal);
              tQty += parseFloat(addon.quantity);
            })
          }
          /*end addon*/

        });
        angular.forEach(this._kots, function (_kot) {
          angular.forEach(_kot.items, function (item) {
            tQty += item.quantity;
            /*addon item integartion*/
            if(_.has(item,'addOns')){
              _.forEach(item.addOns,function(addon){
                //subtotal += parseFloat(addon.subtotal);
                tQty += parseFloat(addon.quantity);
              })
            }
            /*end addon*/

          });
        })
        return Utils.roundNumber( tQty,3);
      },
      getTotalBill: function () {
        var a=((parseFloat(this.getSubtotal()) - parseFloat(this.getTotalDiscount())) + parseFloat(this.getTotalTax()));
        if(Utils.hasSetting('disable_roundoff', localStorageService.get('settings'))) {
          return Utils.roundNumber(a, 2);
        }
        return Utils.roundNumber(a, 2);
      },
      getTotalBillR2: function () {
        return Utils.roundNumber(((parseFloat(this.getSubtotal()) - parseFloat(this.getTotalDiscount())) + parseFloat(this.getTotalTax())), 2);
      },

      getSumKotsAndTotalBill:function(){
        //return  ( this.getKotsTotalBill()+this.getTotalBill());
       // return  ( this.getKotsTotalBill()+this.getTotalBill());
       return this.getNetAmount();
      },
      isCustomerSelected: function () {
        return _.has(this._customer, 'firstname');
      },
      removeCustomer: function () {
        this._customer = {};
      },
      incrementQuantity: function (itemIndex) {
        var _q = parseFloat(this._items[itemIndex].quantity);
        _q += 1;
        // console.log(_q);
        this._items[itemIndex].quantity =Utils.roundNumber(  _q,3);
        this.processBill();
      },
      decrementQuantity: function (itemIndex) {
        var _q = parseFloat(this._items[itemIndex].quantity);
        _q -= 1;
        if (_q < 1) {
          this._items.splice(itemIndex, 1);
        } else {
          this._items[itemIndex].quantity = Utils.roundNumber( _q,3);
        }
        this.processBill();
      },
      decrementKotQuantity: function (index) {

      },
      calculateCharges:function(){
        var tChargeAmount=0;
        if(_.has(this.charges,'detail')){
          _.forEach(this.charges.detail,function(c){
             /*if(c.type=='percentage'){
              }*/
             c.amount=parseFloat( c.value);
             tChargeAmount+=parseFloat( c.amount);
          });
          this.charges.amount=tChargeAmount;
        }
        return tChargeAmount;
      },
      getCharges:function(){
        if(_.has(this.charges,'amount')){
          return parseFloat( this.charges.amount);
        }
      },
      processBill: function (options) {
        _.merge(this, options);
        var billwisetaxfix=Utils.hasSetting('bill_wise_offer_tax',localStorageService.get('settings'));
        this.calculateCharges();
        this.getItemSubtotal();
        this.getKotSubtotal();
        this.getBillWiseOfferDiscount();
        this.getKotItemSubtotal();
        this.getKotDeleteHistorySubtotal();
        if(billwisetaxfix){
          this.calculateAllBillWiseDiscountOnItem();
        }
        this.calculateAllTaxes();
        this.getKotTaxes();
        this.aggregateKotTaxes();
      },
       addCashWithoutDenominations:function(){
        //console.log(this.denominations);
        var billValue=parseFloat(this.getSumKotsAndTotalBill());
        billValue-=parseFloat(this.getTotalCouponPayment())+ parseFloat(this.getTotalCardPayments())+parseFloat(this.getAdvanceBooking());
        this._cash=[];
        var tAmount=0;
        var flag=true;
        this.payments.cash=[];
        for(var key in this.denominations){
          for(var i=0;i<parseInt(this.denominations[key]);i++) {
            var amount = parseInt(key);
            tAmount += amount;
            this._cash.push(parseInt(key));
            if (billValue > 0) {
              if ((tAmount <= billValue)) {
                this.payments.cash.push(amount);
              } else {
                if (flag)
                  this.payments.cash.push((amount >= billValue) ? (billValue - (tAmount - amount)) : (billValue - (tAmount - amount)));
                flag = false;
              }
            }

          }
        }
        //console.log(this._cash);
        console.log(this.payments.cash);
        //return false;
      },


      addCash: function (amount) {
        //if (parseFloat(this.getTotalCash()) <= parseFloat(this.getKotsTotalBill())) {
        //this._cash.push(amount);
        /* If Settled by only Cash and denomination given larger than total bill */
        //alert('(0) Amount: ' + amount + ' Remaining: ' + this.getAmountRemaining());
        //  if (parseFloat(this.getAmountRemaining()) != 0) {
        this._cash.push(amount);
        //  }

        //  if (parseFloat(this.getAmountRemaining()) != 0) {

        if (parseFloat(amount) >= parseFloat(this.getSumKotsAndTotalBill())) {
          //alert('(1) Amount: ' + amount + ' Remaining: ' + this.getAmountRemaining());
          //this._cash.push(amount);
          this.payments.cash.push(parseFloat(this.getSumKotsAndTotalBill()));
        }
        /*If Settlement done by one of; Cash, DC, CC */
        if (parseFloat(amount) <= parseFloat(this.getAmountRemaining())) {
          if (parseFloat(amount) >= parseFloat(this.getAmountRemaining())) {
            //alert('(2) Amount: ' + amount + ' Remaining: ' + this.getAmountRemaining());
            //this._cash.push(amount);
            this.payments.cash.push(parseFloat(this.getAmountRemaining()));
          } else {
            //alert('(3) Amount: ' + amount + ' Remaining: ' + this.getAmountRemaining());
            //this._cash.push(amount);
            this.payments.cash.push(amount);
          }
        } else {
          if (parseFloat(amount) > parseFloat(this.getAmountRemaining())) {
            //alert('(4) Amount: ' + amount + ' Remaining: ' + this.getAmountRemaining());
            //this._cash.push(amount);
            this.payments.cash.push(parseFloat(this.getAmountRemaining()));
          }
        }
        console.log(this.denominations[amount]);
        this.denominations[amount]= (this.denominations[amount]==""?1:parseInt( this.denominations[amount])+1);
      },
      getTotalCash: function () {
        var _tCash = 0;
        _.forEach(this._cash, function (d) {
          _tCash += d;
        });
        return (parseFloat(_tCash)).toFixed(2);
      },
      getTotalCashPayments: function () {
       // console.log(this.payments.cash);
        var _tCash = 0;
        _.forEach(this.payments.cash, function (d) {
          _tCash += d;
        });
        return (parseFloat(_tCash)).toFixed(2);
      },
      clearCash: function () {
        this.denominations={2000:"",1000:"",500:"",100:"",50:"",20:"",10:"",5:"",2:"",1:""};
        this._cash = [];
        this.payments.cash = [];
      },
      getAdvanceBooking:function(){
        var advanceAmt=0;
        if(_.has( this,'advance')){
          if(_.has(this.advance,'amount')){
            if(this.advance.amount>0){
              advanceAmt=this.advance.amount;
            }
          }
        }
        return advanceAmt;
      },
      getAmountRemaining: function () {                   //edited by rajat
        /*console.log(this.getTotalCardPayments());*/

        var _totalPayment = parseFloat(this.getTotalCashPayments()) + parseFloat(this.getTotalCouponPayment())+ parseFloat(this.getTotalCardPayments())+parseFloat(this.getAdvanceBooking());
        //console.log(_totalPayment);
        return (parseFloat(this.getSumKotsAndTotalBill()) - parseFloat(_totalPayment)).toFixed(2);
      },
      getTotalCardPayments: function () {
        var _tCards = 0;

        /* Get Total From CC*/
        _.forEach(this._creditCards, function (cc) {
          _tCards += parseFloat(cc.amount);
        });
        /* Get Total From DB */
        _.forEach(this._debitCards, function (db) {
          _tCards += parseFloat(db.amount);
        });
        _.forEach(this._otherCards, function (db) {
          // console.log(db);
          _tCards += parseFloat(db.amount);
        });
       _.forEach(this._onlineCards, function (db) {
          // console.log(db);
          _tCards += parseFloat(db.amount);
        });

        return parseFloat(_tCards);
      },
      getDBillbunber:function(){
        var billNumber= Utils.hasSetting('reset_serial_daily',localStorageService.get('settings') )?this.daySerialNumber:this.serialNumber;
        billNumber = billNumber+(parseInt( this.splitNumber)?'-'+this.splitNumber:'');
        if(!(this.prefix==undefined||this.prefix==null))
          billNumber=this.prefix+billNumber;
        return billNumber
      },
      addCreditCardPayment: function (ccForm) {

        if (ccForm.$valid) {
          if (parseFloat(this.creditCardForm.amount) <= this.getAmountRemaining()){

               this.creditCardForm.bankName = ccForm.bankName.$modelValue.name;
                this.creditCardForm.bankType = ccForm.bankType.$modelValue;
                this.creditCardForm.amount=parseFloat(this.creditCardForm.amount);
                this._creditCards.push(angular.copy(this.creditCardForm));
           //console.log(this.creditCardForm);
          }
          this.assignCardAmount();
          ccForm.$setPristine();
        }
      },
      addDebitCardPayment: function (dbForm) {
        //  console.log(dbForm);
        if (dbForm.$valid) {
          if (parseFloat(this.debitCardForm.amount) == 0) {
            return false;
          }
          if (parseFloat(this.debitCardForm.amount) <= this.getAmountRemaining()) {
              this.debitCardForm.bankName = dbForm.bankName.$modelValue.name;
                this.debitCardForm.bankType = dbForm.bankType.$modelValue;
                this.debitCardForm.amount=parseFloat(this.debitCardForm.amount);
            this._debitCards.push(angular.copy(this.debitCardForm));
            dbForm.$setPristine();
          }
          else {
            alert('more amount');
            return false;
          }
          this.assignCardAmount();

        }
      },
      addOthersPayment: function (otherForm) {
       /* if (this._otherCards.length > 0) {
          growl.error('Cant add more entry into other settlement !!', {ttl: 2000});
          return false;
        }*/
        if (otherForm.$valid) {
          if (parseFloat(this.otherForm.amount) <= 0) {
            return false;
          }

          //ADDED BY RAJAT
          if (parseFloat(this.otherForm.amount) <= this.getAmountRemaining()) {
            var index = -1;
            var integrated = ['mobikwik', 'ruplee', 'paytm', 'binge', 'urbanpiper','ikaaz'];
            for (var i = 0; i < integrated.length; i++) {
              if (otherForm.otherName.$modelValue.name.replace(/\s/g, '').toLowerCase() == integrated[i]) {
                index = i;
                break;
              }
            }

            if (index != -1)
              this.sendBillToOthers(otherForm.otherName.$modelValue.name.replace(/\s/g, '').toLowerCase(), otherForm);

            else {
              if (parseFloat(this.otherForm.amount) <= this.getAmountRemaining()) {
                console.log(otherForm);
                this.otherForm.otherName = otherForm.otherName.$modelValue.name;
               // this.otherForm.otherType = otherForm.otherType.$modelValue;
                this._otherCards.push(angular.copy(this.otherForm));
                this.otherForm.amount=parseFloat(this.otherForm.amount);
                otherForm.$setPristine();
              }
              else {
                alert('more amount');
                return false;
              }
              this.assignCardAmount();
            }

          }
          else{ alert('more amount'); return false;}
        }
      },

      addOnlinePayment: function (onlineForm) {
        console.log(onlineForm);
        if (this._onlineCards.length > 0) {
          growl.error('Cant add more entry into online bill settlement !!', {ttl: 2000});
          return false;
        }
        if (onlineForm.$valid) {
          if (parseFloat(this.onlineForm.amount) <= 0) {
            return false;
          }

          //ADDED BY RAJAT
              if (parseFloat(this.onlineForm.amount) <= this.getAmountRemaining()) {

                this.onlineForm.onlineName = onlineForm.onlineName.$modelValue.name;
                this.onlineForm.onlineType = onlineForm.onlineType.$modelValue;
                this._onlineCards.push(angular.copy(this.onlineForm));
                this.onlineForm.amount=parseFloat(this.onlineForm.amount);
                onlineForm.$setPristine();
              }
              else {
                alert('more amount');
                return false;
              }
              this.assignCardAmount();
            }
          else{ alert('more amount'); return false;}

      },

      refreshAmount: function (isOther) {
        if(isOther){
          return false;
        }
        this.assignCardAmount();
      },
      removeDebitCard: function (idx) {
        this._debitCards.splice(idx, 1)
        this.assignCardAmount();
      },
      removeCreditCard: function (idx) {
        this._creditCards.splice(idx, 1);
        this.assignCardAmount();
      },
      removeOther: function (idx) {
        this._otherCards.splice(idx, 1);
        this.assignCardAmount();
      },
      removeOnline: function (idx) {
        this._onlineCards.splice(idx, 1);
        this.assignCardAmount();
      },
      assignCardAmount:function(){        // edited by rajat
        this.debitCardForm = {
          amount: (parseFloat(this.getAmountRemaining()) < 0) ? 0 : this.getAmountRemaining()
        };
        this.creditCardForm = {
          amount: (parseFloat(this.getAmountRemaining()) < 0) ? 0 : this.getAmountRemaining()
        },
          this.otherForm = {
            amount: (parseFloat(this.getAmountRemaining()) < 0) ? 0 : this.getAmountRemaining()
          },
           this.couponForm = {
            amount: (parseFloat(this.getAmountRemaining()) < 0) ? 0 : this.getAmountRemaining()
          },
          this.onlineForm={
            amount: (parseFloat(this.getAmountRemaining()) < 0) ? 0 : this.getAmountRemaining()
          }
      },
      getDenominationCount: function () {
        var _c = _.groupBy(this._cash, function (num) {
          return Math.floor(num);
        });
        return _c;
      },
      getReturnToCustomer: function () {           //edited by rajat
        //console.log(this.getKotsTotalBill());
        // return (parseFloat((parseFloat(this.getTotalCash()) + parseFloat(this.getTotalCardPayments())) - this.getKotsTotalBill())).toFixed(2);
        return (parseFloat((parseFloat(this.getTotalCash())+ parseFloat(this.getTotalCouponPayment()) + parseFloat(this.getTotalCardPayments())) - this.getSumKotsAndTotalBill()+this.getAdvanceBooking())).toFixed(2);
      },
      flattenCardItems: function (cardItems) {
        var self=this;
        var _cardItems = [];
        var _cardItemTaxes = [];
        var _allItems = [];
        var _allTaxes = [];
        var tDiscount=0;
        var tTotalAmount=0;
        angular.forEach(cardItems, function (item) {
          /*Adding Add Ons*/
          if (_.has(item, 'addOns')) {
            _.forEach(item.addOns, function (addon) {
              item.subtotal += addon.subtotal;
              _.forEach(addon.taxes, function (at) {
                item.taxes.push(at);
              })
            })
          }
          /*End Add Ons*/

          _allItems.push(item);
          angular.forEach(item.taxes, function (tax) {
            //console.log(tax.baseRate_preTax);
            /*Add Tax Subtotal Amount */
            /* Add Tax */
            _allTaxes.push({
              id: tax._id,
              name: tax.name,
              tax_amount: tax.tax_amount ? parseFloat(tax.tax_amount) : 0,
              isConsidered: true
            });

            angular.forEach(tax.cascadingTaxes, function (casTax) {
              //console.log(casTax);
              /*Add Tax Subtotal Amount */

              /* Add Tax */
              _allTaxes.push({
                id: casTax._id,
                name: casTax.name,
                tax_amount: tax.tax_amount ? parseFloat(casTax.tax_amount) : 0,
                isConsidered: true
              });

            });


          });
        });


        /* Flatten Items */
        _.chain(_allItems).groupBy('_id').map(function (i) {
          var _itemAmount = 0,
            _itemQ = 0,
            _itemD = 0,
            _compItems = 0,
            _nonCompItems = 0,
            _compDiscount = 0,
            _nonCompDiscount = 0;

          angular.forEach(i, function (it) {
            _itemAmount += parseFloat(it.subtotal);
            _itemQ += parseFloat(it.quantity);

            //console.log(i);
            /* Discounts */
            var d = 0;
            if (_.has(it, 'isDiscounted') && it.isDiscounted === true) {
              angular.forEach(it.discounts, function (discount) {
                if (discount.type === 'fixed') {
                  _itemD += parseFloat(discount.value);
                  d +=parseFloat( discount.discountAmount);
                }
                if (discount.type === 'percentage') {
                  _itemD += parseFloat(discount.discountAmount);
                  d +=parseFloat( discount.discountAmount);
                }
              });
            }
            if(Utils.roundNumber(d) === Utils.roundNumber(it.subtotal)){
              _compItems += it.quantity;
              _compDiscount += d;
            } else {
              _nonCompItems += it.quantity;
              _nonCompDiscount += d;
            }
          });

          /* Flatten Tax */
          var _t = [];
          var _itemTotalTax = 0;
          var _allItemTaxes = [];
          angular.forEach(i, function (_ite) {
            angular.forEach(_ite.taxes, function (tax) {
              //console.log(tax);


              //console.log('tax', parseFloat(tax.tax_amount));

              _allItemTaxes.push({
                id: tax._id + '_amt',
                name: tax.name,
                tax_amount: tax.baseRate_preTax ? parseFloat(tax.baseRate_preTax) : 0,
                isConsidered: false
              });

              _allItemTaxes.push({
                id: tax._id,
                name: tax.name,
                tax_amount: tax.tax_amount ? parseFloat(tax.tax_amount) : 0,
                isConsidered: true
              });

              angular.forEach(tax.cascadingTaxes, function (_cTax) {
                if (_cTax.selected) {

                  _allItemTaxes.push({
                    id: _cTax._id + "_amt",
                    name: _cTax.name,
                    tax_amount: (_.has(_cTax, 'baseRate_preTax')) ? parseFloat(_cTax.baseRate_preTax) : 0,
                    isConsidered: false
                  });

                  console.log('ctax', _cTax.tax_amount);

                  _allItemTaxes.push({
                    id: _cTax._id,
                    name: _cTax.name,
                    tax_amount: _cTax.tax_amount ? parseFloat(_cTax.tax_amount) : 0,
                    isConsidered: true
                  });
                }
              });
            });
          });

          console.log('allItemTaxes', _allItemTaxes);

          /* Aggregate Taxes */
          var _groupedItemTaxes = [];
          _.chain(_allItemTaxes).groupBy('id').map(function (_ts) {
            var _tsTotal = 0;
            angular.forEach(_ts, function (_tswa) {
              _tsTotal += parseFloat(_tswa.tax_amount);
            });
            _groupedItemTaxes.push({
              id: _ts[0].id,
              name: _ts[0].name,
              isConsidered: _ts[0].isConsidered,
              tax_amount: parseFloat(_tsTotal)
            });
          });

          /* Calc Total Tax */
          angular.forEach(_groupedItemTaxes, function (_taxwa) {
            if (_taxwa.isConsidered) {
              _itemTotalTax += parseFloat(_taxwa.tax_amount);
            }
          });

          /* Flattened KOT Items */
          console.log(_itemAmount, _itemD, _itemTotalTax);
          var _i = {
            "id": i[0]._id,
            "name": i[0].name,
            "rate": i[0].rate,
            "quantity": _itemQ,
            "subtotal": parseFloat(_itemAmount),
            // "taxes": _groupedItemTaxes,
            "itemNet": (parseFloat(_itemAmount) - parseFloat(_itemD)) + parseFloat(_itemTotalTax),
            "discount": parseFloat(_itemD)
          };
          //console.log(_i);
          _cardItems.push(_i);
          tDiscount+=parseFloat(_itemD);
          tTotalAmount+=_itemAmount;
        });
        var tTax=0;
        /* Flatten Taxes */
        _.chain(_allTaxes).groupBy('id').map(function (t) {
          var _taxAmount = 0;
          angular.forEach(t, function (tt) {
            //console.log(tt);
            _taxAmount += parseFloat(tt.tax_amount);
          });

          if (parseFloat(_taxAmount) > 0) {
            _cardItemTaxes.push({
              "id": t[0].id,
              "name": t[0].name,
              "tax_amount":Utils.roundNumber( parseFloat(_taxAmount),2),
              "isConsidered": t[0].isConsidered
            });
            tTax+=Utils.roundNumber( parseFloat(_taxAmount),2);
          }
        });


         /*Bill wise discount integration with card*/
       /*  var billDiscountAmount=0;
          var tDiscount = parseFloat(this.billDiscountAmount);
        var tAmount = 0;
        var billwiseOffer=null;
        var isExclude = false;
        if (this._offers.length > 0) {
          var _filerBillWise = _.filter(this._offers, {'isBillWise': true});
          if (_filerBillWise.length > 0) {
            billwiseOffer = _filerBillWise[0].offer;
          }
        }

        if(billwiseOffer!=null) {
          if (_.has(billwiseOffer, 'applicable')) {
            if (_.has(billwiseOffer.applicable, 'on')) {
              if (_.has(billwiseOffer.applicable, 'isExclude')) {
                if (billwiseOffer.applicable.on == 'itemCriteria' && billwiseOffer.applicable.isExclude) {
                  isExclude = true;
                }
              }
            }
          }
        }


        angular.forEach(cardItems, function (item) {
          var isExcludeItem=false;
          if(isExclude) {
            if (self.isCategoryOrItemInOffer(item, billwiseOffer)) {
              isExcludeItem=true;
            }
          }
          if(!isExcludeItem) {
            var discountedAmount = ((self.getTotalItemWiseDiscount(item) >= item.subtotal) ? 0 : (item.subtotal - self.getTotalItemWiseDiscount(item)));
            tAmount +=parseFloat( discountedAmount);
          }
        });

        angular.forEach(this._kots, function (kot) {
          if (!kot.isVoid) {
          angular.forEach(kot.items, function (item) {
            var isExcludeItem=false;
            if(isExclude) {
              if (self.isCategoryOrItemInOffer(item, billwiseOffer)) {
                isExcludeItem=true;
              }
            }

            if(!isExcludeItem) {
              var discountedAmount = ((self.getTotalItemWiseDiscount(item) >= item.subtotal) ? 0 : (item.subtotal - self.getTotalItemWiseDiscount(item)));
              tAmount += parseFloat(discountedAmount);
            }
          });
         }
        });
       var tdAmount=0;
        angular.forEach(cardItems, function (item) {
          var isExcludeItem=false;
          if(isExclude) {
            if (self.isCategoryOrItemInOffer(item, billwiseOffer)) {
              isExcludeItem=true;
            }
          }
          if(!isExcludeItem) {
            var discountedAmount = ((self.getTotalItemWiseDiscount(item) >= item.subtotal) ? 0 : (item.subtotal - self.getTotalItemWiseDiscount(item)));
            var itemWiseBillDiscount = (tAmount==0 ? 0: Utils.roundNumber((  discountedAmount / tAmount * tDiscount), ($rootScope.disable_roundoff?2:2)));
            // console.log(itemWiseBillDiscount);
            item.billDiscountAmount = itemWiseBillDiscount;
            tdAmount+=itemWiseBillDiscount;
          }else{
            item.billDiscountAmount=0;
          }
        });*/
         /*End ehere bill wise discount Amunt in card*/


         // var gSumTotal=Utils.roundNumber(tTotalAmount-tDiscount+tTax -tAmount,($rootScope.disable_roundoff?2:0));

          var gSumTotal=Utils.roundNumber(tTotalAmount-tDiscount+tTax - 0,($rootScope.disable_roundoff?2:0));
          var charges=gSumTotal<=0?0:( (_.has(this.charges,'amount'))?parseFloat( this.charges.amount):0);
          charges=(gSumTotal>0 && this._kots.length>0)?0:charges;
        return {items: _cardItems, taxes: _cardItemTaxes,discount:tDiscount,netAmount:gSumTotal+charges}
      },
      flattenCardItemsWithOffer:function(cardItems){
        console.log(this._id);
        var d = $q.defer();
        var self=this;
        $http.post($rootScope.url + '/api/synckots/cardTransactionsAmountByBillId', {
            deployment_id: localStorageService.get('deployment_id'),
            billId:self._id
          }).then(function (result) {
            console.log('card balance',result);
            var billItemsDetail=  self.flattenCardItems(cardItems);
            billItemsDetail.netAmount=self.getNetAmount()+result.data.Amount;
            d.resolve(billItemsDetail);
        },function(err){
          console.log(err.data);
          if(_.has(err,'data')){
            growl.error(err.data.error,{ttl:2000});
          }else{
            growl.error(err,{ttl:2000});
          }
          d.reject(err);
        })
          return d.promise;
      },
      printBill: function () {
        /*console.log(JSON.stringify(this));
         sconsole.log($rootScope.db);*/
        /*$rootScope.db.select("bills", {
         "billId": {
         "value": this._id
         }
         }).then(function (results) {
         console.log(results);

         });*/
        /*$rootScope.db.insert('bills', {"billId": this._id, "billData": JSON.stringify(this)}).then(function (results) {
         console.log("WebSQL - " + results.insertId);
         });*/
      },
      cardItemsCalculation:function(){
        var self=this;
        var cardItems=angular.copy(this._items);
        _.forEach(cardItems,function(item){
          self.calculateTaxOnItem(item);
          self.calculateTaxesAddOns(item);
        });
        return this.flattenCardItems(cardItems);
      },
       cardItemsCalculationWithOffer:function(){
        var d=$q.defer();
        var self=this;
        var cardItems=angular.copy(this._items);
        _.forEach(cardItems,function(item){
          self.calculateTaxOnItem(item);
          self.calculateTaxesAddOns(item);
        });
        this.flattenCardItemsWithOffer(cardItems).then(function(cardTrans){
          //setTimeout(function() {d.resolve(cardTrans);}, 1000);
          d.resolve(cardTrans);
        },function(err){
          console.log(err);
          //growl.error(err,{ttl:2000});
          d.reject();
        });
        return d.promise;
      },
      cardItemsDeleteCalculation:function(item){
        var cardItems=[];
        cardItems.push(item);
        return this.flattenCardItems(cardItems);
      },
      printKot: function (comment,waiter) {

        /*this.processBill();*/

        var _totalItems = 0;
        var _totalAmount = 0;
        var _totalTax = 0;
        // alert(this.kotNumber);
        // console.log(waiter);
        if(_.has( this.cardTransaction,'billDetail')){
          //this.cardTransaction.billDetail={kotNumber:this.kotNumber,serialNumber:this.serialNumber,daySerialNumber:this.daySerialNumber,billId:this._id}
          this.cardTransaction.billDetail.kotNumber=this.kotNumber;
        }
        var _k = {
          isVoid: false,
          voidUser:null,
          items: angular.copy(this._items),
          taxes: angular.copy(this._taxes),
          deleteHistory: [],
          comment: comment,
          isPrinted: true,
          created: new Date(),
          kotNumber: this.kotNumber,
          waiter:waiter,
          cardTransaction:this.cardTransaction,
          user:angular.copy(((this._currentPassCodeUser==null||this._currentPassCodeUser==''||this._currentPassCodeUser==undefined)? (_.has(this._currentUser,'username')?this._currentUser.username:this._currentPassCodeUser ):this._currentPassCodeUser))
        };

        /*KOT Total Item Count */
        _.forEach(_k.items, function (item) {
          _totalItems += parseFloat(item.quantity);
        });
        _k.totalItems = _totalItems;


        this._kots.push(_k);
        this._items = [];
        this._taxes = [];
        this.processBill();
        return (this._kots.length - 1);
      },
      aggregateBill:function(){
        var tempTax=[];
        var aggregateBillObj={
          totalAmount:0,
          totalTax:0,
          totalItems:0,
          totalItemDiscount:0,
          billDiscount:0,
          netAmount:0,
          roundOff:0,
          netRoundedAmount:0,
          taxDetail:[]
        }
        _.forEach(this._kots,function(kot){
          if(!kot.isVoid) {
            aggregateBillObj.totalItems += kot.totalItems;
            aggregateBillObj.totalAmount += kot.totalAmount;
            aggregateBillObj.totalTax += Utils.roundNumber(kot.taxAmount, 2);
            aggregateBillObj.totalItemDiscount += kot.totalDiscount;
            _.forEach(kot.taxes,function(ttx){
              tempTax.push(ttx);
            })
          }
        })
        aggregateBillObj.billDiscount=this.billDiscountAmount;
        var gSumTotal=aggregateBillObj.totalAmount+aggregateBillObj.totalTax-aggregateBillObj.totalItemDiscount-aggregateBillObj.billDiscount;
        var charges=gSumTotal<=0?0:( (_.has(this.charges,'amount'))?parseFloat( this.charges.amount):0);
        aggregateBillObj.netAmount=gSumTotal +charges ;
        aggregateBillObj.netRoundedAmount=Utils.roundNumber(aggregateBillObj.netAmount, ($rootScope.disable_roundoff?2:0));
        aggregateBillObj.roundOff=Utils.roundNumber( aggregateBillObj.netRoundedAmount-aggregateBillObj.netAmount,2);
        var uniqTax= _.uniq(tempTax,'id');
        // console.log(uniqTax);
        var taxes=[];
        _.forEach(uniqTax,function(tax){
          var tTax={};
          var amount=0;
          _.forEach(tempTax,function(tx){
            if(tx.id==tax.id){
              amount+=Utils.roundNumber( parseFloat( tx.tax_amount),2);
            }
          })
          /*var _f= _.filter(tempTax,{id:tax})
           if(_f.length>0) {*/
          tTax.id = tax.id;
          tTax.name = tax.name;
          tTax.amount = Utils.roundNumber( amount,2);
          taxes.push(tTax);
          //}
        })
        aggregateBillObj.taxDetail=taxes;
        this.aggregation=aggregateBillObj;
      },
      voidKot: function (index, comment,user) {
       // alert(user);
        var defer = $q.defer();
        this._kots[index].isVoid = true;
        this._kots[index].comment = comment;
        this._kots[index].voidKotTime = new Date();
        this._kots[index].voidUser = angular.copy(((this._currentPassCodeUser==null||this._currentPassCodeUser==''||this._currentPassCodeUser==undefined)? user:this._currentPassCodeUser));
        // this.getKotSubtotal();
        this.processBill();
        this.updateBill({}, 'update').then(function () {
          growl.info("KOT Voided", {ttl: 2000});
          defer.resolve("done");
        });
        return defer.promise;
        // this.processBill();
      },
      setWeight: function (w, index) {
        var qty = this._items[index].quantity;
        this._items[index].quantity = parseFloat(w);
        this._items[index].manualOverrideBy = 'weight';
        this.getItemSubtotal();
      },
      setRate: function (rate, index) {
        var _r = parseFloat(this._items[index].rate);
        var _q = parseFloat(this._items[index].quantity);
        var _newQ = parseFloat(((_q / _r) * rate).toFixed(2));
        this._items[index].rate = parseFloat(rate);
        this._items[index].quantity = _newQ;
        this._items[index].subtotal = parseFloat(rate);
        this._items[index].priceQuantity = parseFloat(rate);
        this._items[index].manualOverride = true;
        this._items[index].manualOverrideBy = 'rate';

      },
      removeItem: function (index) {
        this._items.splice(index, 1);
      },
      getCustomerFirstName: function () {
        return (!_.isEmpty(this._customer)) ? this._customer.firstname : "No Customer";
      },
      setDeliveryTime: function (deliveredTime) {
        var _deliveryTime = new Date;
        if(deliveredTime!=null)
          _deliveryTime=deliveredTime;             // changed by rajat for technopurple integration
        this._delivery.deliveryTime = _deliveryTime;
        this._deliveryTime = _deliveryTime;
        this.updateBill({}, 'update').then(function () {
          growl.info("Delivery Time Set!", {ttl: 2000});
        });
        return this._delivery.deliveryTime;
      },
      getDelivery: function () {
        var delivery='No Delivery Boy';
        if(this._delivery){
          if(this._delivery.firstname) delivery=this._delivery.firstname;
          if(this._delivery.lastname) delivery+=' '+this._delivery.lastname;
        }
        return delivery;
      },

      checkBillExists: function (billId) {
        var deferred = $q.defer();
        if(!$rootScope.isInstance) {
          $rootScope.db.select("bills", {
            "billId": {
              "value": this._id
            }
          }).then(function (results) {
            deferred.resolve(results.rows.length > 0);
          }).catch(function (e) {
            deferred.reject(e);
          });
        }else {
          /*fine dine*/
          $http.get($rootScope.url + '/api/synckots/getByBillId?billId=' + this._id, {billId: this._id}, {billId: this._id}).then(function (result) {
            //console.log(result);
            deferred.resolve(true);
          }).catch(function (err) {
            //  console.log(err);
            deferred.resolve(false);
          })
          /*fine dine end*/
        }
        return deferred.promise;
      },
      // generateKotNumber: function () {

      //   /* TODO: Check with Ranjeet if we consider only getHours(), what if reset time is 02:30 AM */

      //   var deferred = $q.defer();

      //   var currentdate = new Date();
      //   // var resetbilltime = moment((Utils.getSettingValue('day_sale_cutoff', $rootScope.settings))).format('hh');
      //   var resetbilltime = new Date(Utils.getSettingValue('day_sale_cutoff', localStorageService.get('settings'))).getHours();
      //   var kotnumberquery = "";
      //   console.log(resetbilltime,currentdate.getHours());
      //   if (currentdate.getHours() < resetbilltime) {
      //     kotnumberquery = "select (case when max(kotnumber) is null or max(kotnumber)='undefined' then 1 else (max(kotnumber)+1) end) as kotnumber from bills";
      //     console.log("", kotnumberquery);
      //   }
      //   else {
      //     kotnumberquery = "select (case when max(kotnumber) is null or max(kotnumber)='undefined' then 1 else (max(kotnumber)+1) end) as kotnumber from bills where created between datetime('now','localtime','" + resetbilltime + " hours') and datetime('now','localtime')";
      //     console.log(currentdate.getHours());
      //   }
      //   console.log(kotnumberquery)
      //   if(!$rootScope.isInstance) {
      //     $rootScope.db.selectDynamic(kotnumberquery).then(function (k) {
      //       //console.log(k.rows)
      //       deferred.resolve(k.rows.item(0).kotnumber);
      //     }).catch(function (e) {
      //       deferred.reject(e);
      //     });
      //   }else {
      //     var startDate = new Date();
      //     startDate = new Date( startDate.setHours(resetbilltime, 0, 0, 0));
      //     if (currentdate.getHours() < resetbilltime) {
      //       startDate = new Date(startDate.setDate(startDate.getDate()-1));
      //     }
      //     var endDate =new Date();
      //     var query="deployment_id="+this.deployment_id+"&instance_id="+this.instance_id+"&startDate="+moment(startDate).subtract(moment.duration('05:30:00')).toDate() +"&endDate="+endDate.toString();
      //     $http.get($rootScope.url + '/api/synckots/getKotNumber?'+query ).then(function (result) {
      //       deferred.resolve(result.data.kotNumber+1);
      //     },function(err){
      //       deferred.reject(err);
      //     });
      //   }
      //   return deferred.promise;
      // },

generateKotNumber: function () {

/* TODO: Check with Ranjeet if we consider only getHours(), what if reset time is 02:30 AM */

var deferred = $q.defer();

var currentdate = new Date();
// var resetbilltime = moment((Utils.getSettingValue('day_sale_cutoff', $rootScope.settings))).format('hh');
var resetbilltime = new Date(Utils.getSettingValue('day_sale_cutoff', localStorageService.get('settings'))).getHours();
var kotnumberquery = "";
//console.log(resetbilltime);
if (currentdate.getHours() < resetbilltime) {
kotnumberquery = "select (case when max(kotnumber) is null or max(kotnumber)='undefined' then 1 else (max(kotnumber)+1) end) as kotnumber from bills where created between datetime( datetime(date('now','localtime'),'" + resetbilltime + " hours'),'-1 days') and datetime('now','localtime')";
//console.log("", kotnumberquery);
}
else {
kotnumberquery = "select (case when max(kotnumber) is null or max(kotnumber)='undefined' then 1 else (max(kotnumber)+1) end) as kotnumber from bills where created between datetime(date('now','localtime'),'" + resetbilltime + " hours') and datetime('now','localtime')";
console.log(kotnumberquery);
}
if(!$rootScope.isInstance) {
$rootScope.db.selectDynamic(kotnumberquery).then(function (k) {
deferred.resolve(k.rows.item(0).kotnumber);
}).catch(function (e) {
deferred.reject(e);
});
}else {
var startDate = new Date();
startDate = new Date( startDate.setHours(resetbilltime, 0, 0, 0));
if (currentdate.getHours() < resetbilltime) {
startDate = new Date(startDate.setDate(startDate.getDate()-1));
}
var endDate =new Date();
var query="deployment_id="+this.deployment_id+"&instance_id="+this.instance_id+"&startDate="+moment(startDate).subtract(moment.duration('05:30:00')).toDate() +"&endDate="+endDate.toString();
$http.get($rootScope.url + '/api/synckots/getKotNumber?'+query ).then(function (result) {
deferred.resolve(result.data.kotNumber+1);
},function(err){
deferred.reject(err);
});
}
return deferred.promise;
},
      generateBillNumbers: function () {

        /* TODO: Check with Ranjeet if we consider only getHours(), what if reset time is 02:30 AM */

        //var resettime = moment(Utils.getSettingValue('day_sale_cutoff', $rootScope.settings)).format('hh');
        var resettime = new Date(Utils.getSettingValue('day_sale_cutoff', localStorageService.get('settings'))).getHours();
        //  console.log('resettime:'+resettime);
        var currentdate = new Date();
        var bnQuery = "";
        var sDnQuaery = "";
        var snQuery = "";
        var q="";
        if (currentdate.getHours() <= resettime) {
          //bnQuery = "select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created between  datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime') )>(select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created between  datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime') ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber";
          //sDnQuaery = "select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created between datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime')";
          //snQuery = "select (case when max(serialnumber) is null or max(serialnumber)='undefined' then 1 else (max(serialnumber)+1) end) as serialnumber from bills ";
          //  q="select (select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created between  datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime') )>(select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created between  datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime') ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber) as billnumber,(select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created between datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') and datetime('now','localtime')) as dayserialnumber,(select (case when max(serialnumber) is null or max(serialnumber)='undefined' then 1 else (max(serialnumber)+1) end) as serialnumber from bills) as serialnumber";
          q="select (select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created > datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days')  )>(select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created >  datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days')  ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber) as billnumber,(select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created > datetime(datetime(date('now','localtime'),'" + resettime + " hours'),'-1 days') ) as dayserialnumber,(select (case when max(serialnumber) is null or max(serialnumber)='undefined' then 1 else (max(serialnumber)+1) end) as serialnumber from bills) as serialnumber";

        }
        else {
          //bnQuery = "select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created between  datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime') )>(select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created between  datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime') ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber";
          //sDnQuaery = "select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created between datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime')";
          //snQuery = "select (case when max(serialnumber) is null or max(serialnumber)='undefined' then 1 else (max(serialnumber)+1) end) as serialnumber from bills ";
          //  q="select (select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created between  datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime') )>(select case when max(billNumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created between  datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime') ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber) as billnumber,(select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created between datetime(date('now','localtime'),'" + resettime + " hours') and datetime('now','localtime')) as dayserialnumber,(select (case when max(serialNumber) is null or max(serialNumber)='undefined' then 1 else (max(serialNumber)+1) end) as serialnumber from bills ) as serialnumber";
          q="select (select (case when (select case when  max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end   from bills where  created > datetime(date('now','localtime'),'" + resettime + " hours')  )>(select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else max(billnumber)+1 end from bills where (closetime is null or closetime='undefined')  ) then (select case when  (max(billnumber)) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end   from bills where   created >  datetime(date('now','localtime'),'" + resettime + " hours')  ) else (select case when max(billnumber) is null or max(billnumber)='undefined' then 1 else (max(billnumber)+1) end from bills where (closetime is null or closetime='undefined') ) end) as billnumber) as billnumber,(select (case when max(daySerialNumber) is null or max(daySerialNumber)='undefined' then 1 else (max(daySerialNumber)+1) end) as dayserialnumber from bills where  created > datetime(date('now','localtime'),'" + resettime + " hours') ) as dayserialnumber,(select (case when max(serialnumber) is null or max(serialnumber)='undefined' then 1 else (max(serialnumber)+1) end) as serialnumber from bills ) as serialnumber";

        }
        // console.log(q);
        var deferred = $q.defer();
        var snumbers = {};
        /*$q.all([
         $rootScope.db.selectDynamic(bnQuery),
         $rootScope.db.selectDynamic(snQuery),
         $rootScope.db.selectDynamic(sDnQuaery)
         ])
         .then(function (sns) {
         //  console.log(sns);
         snumbers.billnumber = sns[0].rows.item(0).billnumber;
         snumbers.serialnumber = sns[1].rows.item(0).serialnumber;
         snumbers.dayserialnumber = sns[2].rows.item(0).dayserialnumber;
         deferred.resolve(snumbers);
         }
         ).catch(function (e) {
         deferred.reject(e);
         });*/
        // console.log(q);
        if(!$rootScope.isInstance) {
          $rootScope.db.selectDynamic(q).then(function (sns) {
            snumbers.billnumber = sns.rows.item(0).billnumber;
            snumbers.serialnumber = sns.rows.item(0).serialnumber;
            snumbers.dayserialnumber = sns.rows.item(0).dayserialnumber;
            // console.log(snumbers);
            deferred.resolve(snumbers);
          })
        }else{
          currentdate= new Date();
          resettime = new Date(Utils.getSettingValue('day_sale_cutoff', localStorageService.get('settings'))).getHours();
          snumbers={};
          var startDate = new Date();
          startDate = new Date( startDate.setHours(resettime, 0, 0, 0));
          // console.log();
          if (currentdate.getHours() < resettime) {
            startDate = new Date(startDate.setDate(startDate.getDate()-1));
          }
          var endDate =new Date();
          // var query="deployment_id="+this.deployment_id+"&instance_id="+this.instance_id+"&startDate="+startDate +"&endDate="+endDate.toString();
          var query="deployment_id="+this.deployment_id+"&instance_id="+this.instance_id+"&startDate="+moment(startDate).subtract(moment.duration('05:30:00')).toDate()+"&endDate="+endDate.toString();

          $http.get($rootScope.url + '/api/synckots/getDaySerialNumber?'+query ).then(function (result) {
            snumbers.billnumber = result.data.billNumber+1;
            snumbers.serialnumber = result.data.serialNumber+1;
            snumbers.dayserialnumber = result.data.daySerialNumber+1;

            deferred.resolve(snumbers);
          },function(err){
            deferred.reject(err);
          });
        }
        return deferred.promise;
      },
      updateBill: function (options, operation) {

        $.extend(this, options);

        var deferred = $q.defer();
        var promise = deferred.promise;
        console.log(operation)
        this.aggregateBill();
        if (operation === 'insert') {
          /*alert("Inserting");*/
          var self = this;
          //if($rootScope.isInstance){
            var instance=localStorageService.get('instanceId');
            if(!(instance==null||instance==undefined)){
            if(!(instance.preFix == null||instance.preFix==undefined)){
              self.prefix=instance.preFix;
          /*Changed for online instance*/
              self.instance=instance;
            }
          }
          //}


          this.generateBillNumbers().then(function (snumbers) {
            //console.log(self);
            //return false;
            var fdate = Utils.getDateFormatted(new Date());
            console.log(fdate);
            //changed here for split bill
            /*  self.billNumber = ((self.billNumber == null) ? snumbers.billnumber : self.billNumber);
             self.serialNumber = ((self.serialNumber == null) ? snumbers.serialnumber : self.serialNumber);
             self.daySerialNumber = ((self.daySerialNumber == null) ? snumbers.dayserialnumber : self.daySerialNumber);*/
            self.billNumber = ((self.splitNumber == null) ? snumbers.billnumber : self.billNumber);
            self.serialNumber = ((self.splitNumber == null) ? snumbers.serialnumber : self.serialNumber);
            self.daySerialNumber = ((self.splitNumber == null) ? snumbers.dayserialnumber : self.daySerialNumber);
            //for making id the most unique
            self._id+='-'+self.serialNumber;
            if(_.has(self.cardTransaction,'billDetail')){
              self.cardTransaction.billDetail.billId=self._id;
              self.cardTransaction.billDetail.serialNumber=self.serialNumber;
              self.cardTransaction.billDetail.daySerialNumber=self.daySerialNumber;
              //self.cardTransaction.billDetail.instance=localStorageService.get('instanceId');
              if(_.has(self.cardTransaction.billDetail,'instance')){
                if(self.cardTransaction.billDetail.instance.preFix==null||self.cardTransaction.billDetail.instance.preFix==undefined){
                  self.cardTransaction.billDetail.instance.preFix=self.prefix;
                }
              }
            }
            var billobj={
              "serialNumber": self.serialNumber,  //snumbers.serialnumber,
              "daySerialNumber": self.daySerialNumber, //snumbers.dayserialnumber,
              "billNumber": self.billNumber, //snumbers.billnumber,
              "splitNumber": self.splitNumber,
              "kotNumber": self.kotNumber,
              "billId": self._id,
              "created": fdate,
              // "created": (d.toDateString() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()),
              "billData": angular.toJson(angular.copy( self)),
              "isVoid": self.isVoid,
              "isPrinted": self.isPrinted,
              "isSettled": self.isSettled,
              "tab": self.tabType,
              "tabId": self.tabId,
              //"totalBill": self.getKotsTotalAmount(),
              totalBill:self.getNetAmount(),
              "totalTax": self.getKotsTotalTax(),
              "deployment_id": (self.deployment_id != null) ? self.deployment_id : "null",
              "tenant_id": self.tenant_id,
              "isSynced": self.isSynced,
              "syncedOn": self.syncedOn,
              "customer": self.getCustomerFirstName(),
              "customer_address": self.getCustomerAddress(),
              "delivery_time": (_.has(self._delivery, 'deliveryTime')) ? self._delivery.deliveryTime : new Date(),
              "closeTime": self._closeTime

            }
            if(!$rootScope.isInstance) {
              console.log("instance")
             /* if(self.instance_id){
                billobj.instance_id=self.instance_id;
                billobj.instance=localStorageService.get('instanceId');
              }*/
              $rootScope.db.insert('bills',
                billobj
              ).then(function (results) {
                deferred.resolve(results.insertId)
              }).catch(function (e) {
                deferred.reject(e);
              });
            }else {
              /*fine dine */
              // console.log(billobj.splitNumber);

              if(billobj.splitNumber!=undefined){
                billobj.isSplit=true;
              }
              billobj.instance_id=self.instance_id;
              billobj.instance=localStorageService.get('instanceId');
              //alert(self._tableId);
              billobj.tableNumber=self._tableId;
              billobj.isCard=Utils.hasSetting('enable_card', localStorageService.get('settings'));
              /*if(!(billobj.instance.preFix == null||billobj.instance.preFix==undefined)){
               billobj.billData.prefix=billobj.instance.preFix;
               }*/
              // console.log(billobj)
              $http.post($rootScope.url + '/api/synckots', billobj).then(function (results) {
                //  console.log(results);
                deferred.resolve({_id: results.data._id,lastModified:results.data.lastModified})
              }, function (e) {
                console.log(e)
                deferred.reject(e);
              })
              /*fine dine*/
            }
          });

        } else {
         // console.log(this.lastModified);
          var billForUpdate = {
            //"serialNumber": "",
            //"daySerialNumber": "",
            //"billNumber": "",
            "kotNumber": this.kotNumber,
            "billData": angular.toJson(angular.copy(this)),
            "isVoid": this.isVoid,
            "isPrinted": this.isPrinted,
            "isSettled": this.isSettled,
            "tab": this.tabType,
            "tabId": this.tabId,
            "totalBill": this.getNetAmount(),
            "totalTax": this.getKotsTotalTax(),
            "deployment_id": (this.deployment_id != null) ? this.deployment_id : "null",
            "tenant_id": this.tenant_id,
            "isSynced": this.isSynced,
            "syncedOn": this.syncedOn,
            "customer": this.getCustomerFirstName(),
            "customer_address": this.getCustomerAddress(),
            "delivery_time": (_.has(this._delivery, 'deliveryTime')) ? this._delivery.deliveryTime : new Date(),
            "closeTime": this._closeTime
          };
          /*alert("Updating");*/
          if (!$rootScope.isInstance) {
            $rootScope.db.update('bills', billForUpdate, {
              "billId": this._id
            }).then(function () {

              deferred.resolve("Updated!");
            }).catch(function (e) {
              deferred.reject(e);
            });
          } else {
            /*fine dine */
            //console.log(bill);
            billForUpdate.lastModified=this.lastModified;
            billForUpdate.id = this._id;
            billForUpdate.tableNumber=this._tableId;
            billForUpdate.isCard=Utils.hasSetting('enable_card', localStorageService.get('settings'));
            $http.post($rootScope.url + '/api/synckots/billUpdate', billForUpdate).then(function (result) {
              // console.log(result);
              deferred.resolve(result.data.lastModified);
            },function(e){
              console.log(e)
              deferred.reject(e);
            })
            /*fine dine end*/
          }
        }
        $rootScope.offlineBill.lastCreated=new Date();
        return promise;

      },
      setCustomerDeliverTo: function (address) {
        this._customer.deliverTo = address;
      },
      getCustomerAddress: function () {
        // return (this._customer.deliverTo != undefined) ? this._customer.deliverTo.line1 + ', ' + this._customer.deliverTo.line2 : "No Delivery Address";
        var address='No Delivery Address';
        if(this._customer && _.has(this._customer,'address1')) {
          address = this._customer.address1;
         if(this._customer.address2)
          address+=','+this._customer.address2;
        }
        return address;
      },

      getKotsTotalAmount: function () {
        var _total = 0;
        angular.forEach(this._kots, function (kot) {
          if (!kot.isVoid) {
            _total += parseFloat(kot.totalAmount);
          }
        });
        /*Repeated Task Delete this Ranjeet*/
        /* if (this._items.length > 0) {
         _total += parseFloat(this.getSubtotal());
         }*/
        return Utils.roundNumber(parseFloat(_total), 2);
      },
      getKotsTotalTax: function () {
        var _total = 0;
        angular.forEach(this._kots, function (kot) {
          if (!kot.isVoid) {
            _total += parseFloat(kot.taxAmount);
          }
        });
        /*Repeated Task Delete this Ranjeet*/
        /*   if (this._items.length > 0) {
         _total += parseFloat(this.getTotalTax());
         }*/
        return Utils.roundNumber((parseFloat(_total)), 2);
      },
      getKotsTotalBill: function () {
        //console.log("tamount:"+this.getKotsTotalAmount()+"disc:"+this.getKotTotalDiscount()+"tax:"+this.getKotsTotalTax());
        var a = ((parseFloat(this.getKotsTotalAmount()) - parseFloat(this.getKotTotalDiscount())) + parseFloat(this.getKotsTotalTax()));
        if(Utils.hasSetting('disable_roundoff', localStorageService.get('settings'))) {
          return Utils.roundNumber(a, 2);
        }
        return Utils.roundNumber(a, 2);
      },
      getKotsTotalBillR2: function () {
        var a = ((parseFloat(this.getKotsTotalAmount()) - parseFloat(this.getKotTotalDiscount())) + parseFloat(this.getKotsTotalTax()));
        return Utils.roundNumber(a, 2);
      },
      getNetAmount:function(){
        if(parseFloat( this.getTotalBill()+this.getKotsTotalBill())<=0){
         var tChargeAmount=0;
           if(_.has(this.charges,'detail')){
          _.forEach(this.charges.detail,function(c){
             c.amount=parseFloat( 0);
             tChargeAmount+=parseFloat( c.amount);
          });
          this.charges.amount=tChargeAmount;
          this.charges.amount=0;
        }

        }else{
          this.calculateCharges();
        }
        var tmpCharges=0;
        if(_.has(this.charges,'amount')){
          tmpCharges=this.charges.amount;
        }
        return (Utils.roundNumber( this.getTotalBill()+this.getKotsTotalBill()+ tmpCharges,($rootScope.disable_roundoff?2:0)));
      },
      validateSettlement: function () {

        var d = $q.defer();

        try {
          if (this.getAmountRemaining() > 0) {
            //alert('Greater than 0 ' + parseFloat(this.getAmountRemaining()));
            d.reject(false);
          } else if (this.getAmountRemaining() === 0 ) {
            //alert('Equal than 0 ' + parseFloat(this.getAmountRemaining()));
            d.resolve(true);
          } else if (this.getAmountRemaining() <= 0) {
            //alert('Less than 0 ' + parseFloat(this.getAmountRemaining()));
            d.resolve(true);
          }
        } catch (e) {
          console.log(e);
        }

        return d.promise;

      },
      prepareAggregation: function (settings, tabtype) {

        var deferred = $q.defer();
        var addOns=0;
        var _f = [], _t = [];
        var _allItems = [], _allTaxes = [];

        angular.forEach(this._kots, function (kot) {
          if (!kot.isVoid) {
            angular.forEach(kot.items, function (item) {
              _allItems.push(item);
              if(_.has(item,'addOns')){
                _.forEach(item.addOns,function(addon){
                  addOns+=addon.subtotal;
                })
              }
            });
            angular.forEach(kot.taxes, function (tax) {
              _allTaxes.push(tax);
            });
          }
        });

        /*Calculate amount for each items of bill*/
        angular.forEach(this._items, function (item) {
          _allItems.push(item);
          if(_.has(item,'addOns')){
            _.forEach(item.addOns,function(addon){
              addOns+=addon.subtotal;
            })
          }
        });
        angular.forEach(this._taxes, function (tax) {
          _allTaxes.push(tax);
        });

        //console.log(_allItems);
        // Aggregate Items
        _.chain(_allItems).groupBy('_id').map(function (i) {
          // console.log(i);
          var _itemAmount = 0, _itemQ = 0;
          angular.forEach(i, function (it) {
            _itemAmount += parseFloat(it.subtotal);
            _itemQ += parseFloat(it.quantity);
          });
          //console.log(_itemAmount);
      if (Utils.hasSetting('inclusive_tax', settings)){

          _f.push({
            "id": i[0]._id,
            "name": i[0].name,
            "quantity": _itemQ,
            "subtotal": parseFloat(_itemAmount).toFixed(2),
            "rate":i[0].rate,
            "originalRate":i[0].originalRate
          });
        }else{
          _f.push({
            "id": i[0]._id,
            "name": i[0].name,
            "quantity": _itemQ,
            "subtotal": parseFloat(_itemAmount).toFixed(2),
            "rate":i[0].rate,
          });

        }
        });

        // Aggregate Taxes
        _.chain(_allTaxes).groupBy('_id').map(function (t) {
          var _taxAmount = 0;
          angular.forEach(t, function (tt) {
            _taxAmount += parseFloat(tt.tax_amount);
          });

          _t.push({
            "id": t[0]._id,
            "name": t[0].name,
            "tax_amount": parseFloat(_taxAmount).toFixed(2)

          });

        });

        var _tA = 0, _tT = 0, _tD = 0;
        angular.forEach(_f, function (i) {
          _tA += parseFloat(i.subtotal);
        });
        angular.forEach(_t, function (t) {
          _tT += parseFloat(t.tax_amount);
        });
        // console.log(this._kots);
        angular.forEach(this._kots, function (kot) {
          if (!kot.isVoid) {
            _tD += parseFloat(kot.totalDiscount);
          }
        });


        //bill wise discount
        _tD+=parseFloat(this.billDiscountAmount);
        var _errors = [];
        var isAdvance=false;
        if(_.has( this.advance,'amount')){
          if(_.has(this.advance.amount>0)){
            if(this._kots.length==0){
              isAdvance=true;
            }
          }
        }

        // Check for Waiter/Delivery Boy Selection
        if (Utils.hasSetting('delivery_boy_selection', settings)) {
          if (this.tabType == 'delivery') {
            if (_.isEmpty(this._delivery)) {
              _errors.push('Delivery Boy Selection required.');
            }
          }
        }

        if (Utils.hasSetting('waiter_selection', settings)) {
          if (this.tabType == 'table') {
            if (_.isEmpty(this._waiter)) {
              _errors.push('Waiter Selection required.');
            }
          }
        }

       /* if (Utils.hasSetting('enable_crm_mandatory', settings, tabtype)) {
          if (_.isEmpty(this._customer)) {
            _errors.push('Customer not selected.');
          }
        }*/
        // alert(_tA+":"+_tT+":"+_tD);

        if(this.tabType!='table'){
        if(!(Utils.getSettingValue('minimum_order_delivery', settings)==''||Utils.getSettingValue('minimum_order_delivery', settings)==null)) {
        //alert(this.tabType);
          if ((_tA + _tT - _tD + addOns) < Utils.getSettingValue('minimum_order_delivery', settings)) {
            _errors.push("Minimum Order is " + Utils.getSettingValue('minimum_order_delivery', settings));
          }
        }
      }

        if (_errors.length > 0 && (!isAdvance)) {
          deferred.reject(_errors);
        } else {
          deferred.resolve(
            {
              currentUser: this._currentUser,
              items: _f,
              taxes: _t,
              totalAmount: _tA,
              totalTax: _tT,
              totalDiscount: _tD,
              header: Utils.getSettingValue('header_bill', settings),
              footer: Utils.getSettingValue('footer_bill', settings),
              created: new Date(),
              bill: this
            }
          );
        }
        return deferred.promise;
      },
      calculateAdvanceAmount:function(booking){
        // console.log(booking);
        var tAmount=0;
        _.forEach(booking._items,function(item){
          tAmount+=item.subtotal;
          if(_.has(item,'discounts')){
            _.forEach(item.discounts,function(dis){
              if(_.has(dis,'discountAmount')){
                tAmount-=dis.discountAmount;
              }
            })
          }
        })
        _.forEach(booking._taxes,function(tax){
          console.log(tax);
          tAmount+=parseFloat( tax.tax_amount);
        })
        if(_.has(booking,'billDiscountAmount')){
          tAmount-=booking.billDiscountAmount;
        }
        return Utils.roundNumber( tAmount, ($rootScope.disable_roundoff?2:0));
      },
      prepareForReport: function (cutoffTimeSetting, deploymentSettings) {
        var self = this;
        var defer = $q.defer();
        $timeout(function () {

          var _cutOffTime = moment(moment(cutoffTimeSetting));
          var _cutOffTimeMeridian = moment(moment(cutoffTimeSetting)).format('a');
          var _cutOffTimeHour = parseInt(moment(moment(_cutOffTime)).format('H'));
          var _cutOffTimeMin = parseInt(moment(moment(_cutOffTime)).format('mm'));

          var _billOpenTime = moment(moment(self._created));
          var _billCloseTime = moment(moment(self._closeTime));

          var _billOpenTimeStartOfDay = moment(moment(self._created)).startOf('day');
          var _billOpenTimeEnfOfDay = moment(moment(self._created)).endOf('day');
          var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour,'hours').add(_cutOffTimeMin,'minutes');
          //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

          if (_cutOffTimeMeridian === 'am') {
            if (_billOpenTime.isAfter(_billOpenTimeOffsetCutOff) && (_billOpenTime.isBefore(_billOpenTimeEnfOfDay))) {
              self.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
            }

            if (_billOpenTime.isAfter(_billOpenTimeStartOfDay) && _billOpenTime.isBefore(_billOpenTimeOffsetCutOff)) {
              self.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
            }
          }



          //var _calcCutoffTime = _cutOffTime.toArray();
          //var _calcOpenTime = moment(_billOpenTime).toArray();
          //var _billCutoffTime = moment(new Date(_calcOpenTime[0], _calcOpenTime[1], _calcOpenTime[2], _calcCutoffTime[3], _calcCutoffTime[4], _calcCutoffTime[5]));
          //_billCutoffTime.subtract(1, 'days');

          //console.log('Bill# ', bill.serialNumber, 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"), ' Close: ', _billCloseTime.format("YYYY-MM-DD hh:mm:ss a"), ' Cutoff: ', _cutOffTime.format('hh:mm:ss a'));
          //console.log('Bill Open time end of Day: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'));

          /*if (_billCloseTime.isAfter(_billOpenTimeEnfOfDay) && _billCloseTime.isBefore(_billCutoffTime)) {
           self.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
           console.log("Case 1 Bill#", self.serialNumber, ' CloseTime: ', self.fauxCloseTime);
           }*/


          /* Changing for groupBy to work */
          self.created = moment(self._created).format('YYYY-MM-DD');
          //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

          /* Create Faux Date Hack */
          if (self.fauxCloseTime != null) {
            self.closeTime = self.fauxCloseTime;
          } else {
            if (_cutOffTimeMeridian != 'am') {
              self.closeTime = self.created;
            }
          }

          /* Replacing serialNumber with daySerialNumber */
          if (Utils.hasSetting('reset_serial_daily', deploymentSettings)) {
            //console.log("Entering reset_serial_daily");
            self.serialNumber =self.daySerialNumber;
          }
          /*fine dine fix*/
          if(self.prefix!=null){
            self.serialNumber=self.prefix + self.serialNumber;
          }

          /* Get Items out of _kots and push them into _items */
          self.flattenKots().then(function () {
            /* Start aggregating Bill under _items */
            self.aggregate().then(function () {
              defer.resolve(self);
            })
          });

        }, 100);

        return defer.promise;
      },
       prepareForReportForKot: function (cutoffTimeSetting, deploymentSettings) {
        var self = this;
        var defer = $q.defer();
        $timeout(function () {

          var _cutOffTime = moment(moment(cutoffTimeSetting));
          var _cutOffTimeMeridian = moment(moment(cutoffTimeSetting)).format('a');
          var _cutOffTimeHour = parseInt(moment(moment(_cutOffTime)).format('H'));
          var _cutOffTimeMin = parseInt(moment(moment(_cutOffTime)).format('mm'));

          var _billOpenTime = moment(moment(self._created));
          var _billCloseTime = moment(moment(self._closeTime));

          var _billOpenTimeStartOfDay = moment(moment(self._created)).startOf('day');
          var _billOpenTimeEnfOfDay = moment(moment(self._created)).endOf('day');
          var _billOpenTimeOffsetCutOff = moment(moment(_billOpenTimeStartOfDay)).add(_cutOffTimeHour,'hours').add(_cutOffTimeMin,'minutes');
          //console.log('Bill Cutoff: ', self.serialNumber, 'Start: ', _billOpenTimeStartOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' End: ', _billOpenTimeEnfOfDay.format('YYYY-MM-DD hh:mm:ss a'), ' Offeset:' , _billOpenTimeOffsetCutOff.format('YYYY-MM-DD hh:mm:ss'), 'Open: ', _billOpenTime.format("YYYY-MM-DD hh:mm:ss a"));

          if (_cutOffTimeMeridian === 'am') {
            if (_billOpenTime.isAfter(_billOpenTimeOffsetCutOff) && (_billOpenTime.isBefore(_billOpenTimeEnfOfDay))) {
              self.fauxCloseTime = _billOpenTime.format('YYYY-MM-DD');
            }

            if (_billOpenTime.isAfter(_billOpenTimeStartOfDay) && _billOpenTime.isBefore(_billOpenTimeOffsetCutOff)) {
              self.fauxCloseTime = moment(_billOpenTime).subtract(1, 'days').format('YYYY-MM-DD');
            }
          }
          //*!* Changing for groupBy to work *!/*/
          self.created = moment(self._created).format('YYYY-MM-DD');
          //self.closeTime = moment(self._closeTime).format('YYYY-MM-DD');

          ///!* Create Faux Date Hack *!/
          if (self.fauxCloseTime != null) {
            self.closeTime = self.fauxCloseTime;
          } else {
            if (_cutOffTimeMeridian != 'am') {
              self.closeTime = self.created;
            }
          }

         /* /!* Replacing serialNumber with daySerialNumber *!/*/
          if (Utils.hasSetting('reset_serial_daily', deploymentSettings)) {
            //console.log("Entering reset_serial_daily");
            self.serialNumber =self.daySerialNumber;
          }
          /*/!*fine dine fix*!/*/
          if(self.prefix!=null){
            self.serialNumber=self.prefix + self.serialNumber;
          }


          defer.resolve(self);


        }, 100);

        return defer.promise;
      },
      aggregate: function () {
        var defer = $q.defer();
        var self = this;
        var _tQty = 0;
        var _tAmount = 0;
        var _tDiscount = 0;
        var _tNetAmount = 0;

        $timeout(function () {

          angular.forEach(self._items, function (item) {
            _tQty += parseFloat(item.quantity);
            _tAmount += Utils.roundNumber(parseFloat(item.subtotal),2);
            _tDiscount += Utils.roundNumber(parseFloat(item.discount),2);
            _tNetAmount += Utils.roundNumber(parseFloat(item.itemNet),2);
            console.log('itemnet', item.itemNet);
          });

          self.totalQty = _tQty;
          self.totalAmount = _tAmount;
          self.totalDiscount = _tDiscount +self.billDiscountAmount; //changes might required here
          if((self.charges == undefined || self.charges == null || self.isVoid || !_.has(self.charges, 'amount')))
           {
            console.log(self.isVoid);
            console.log("hello")
            self.charges = {};
            self.charges.amount = 0;
            self.charges.detail=[];
          }

          //self.charges.amount
          self.totalNet = Utils.roundNumber((_tNetAmount + Utils.roundNumber(self.charges.amount) -Utils.roundNumber(self.billDiscountAmount,2)),2);


          /* Total Cash */
          self.totalCash = 0;
          angular.forEach(self.payments.cash, function (c) {
            self.totalCash += parseFloat(c);
          });

          /* Total Cards: Debit & Credit Card, scale here for more Card Types */
          self.totalDebitCardAmount = 0;
          self.totalCreditCardAmount = 0;
          self.totalOtherCardAmount = 0;
          self.totalCouponAmount = 0;
          angular.forEach(self.payments.cards, function (card) {
            if (card.cardType === 'DebitCard') {
              self.totalDebitCardAmount += parseFloat(card.totalAmount);
            }
            if (card.cardType === 'CreditCard') {
              self.totalCreditCardAmount += parseFloat(card.totalAmount);
            }
            if (card.cardType === 'Other') {
              self.totalOtherCardAmount += parseFloat(card.totalAmount);
            }
            if (card.cardType === 'Coupon') {
              self.totalCouponAmount += parseFloat(card.totalAmount);
            }
          });

          /* Total Tax */
          angular.forEach(self._taxes, function (t) {
            //console.log(t);
            if (t.isConsidered) {
              self.totalTax += Utils.roundNumber(parseFloat(t.tax_amount),2);
            }
          });

          /* Rounding Off & G. Total */
          self.roundOff = self.getRoundOff(self.totalNet);
          self.afterRoundOff = self.getAfterRoundOff(self.totalNet);

          //console.log(self.roundOff, self.afterRoundOff);

          defer.resolve();
        });
        return defer.promise;
      },
    flattenKots: function () {

        var self = this;
        var d = $q.defer();
        var _allItems = [];
        var _allTaxes = [];

        $timeout(function () {

          if (self.isVoid != true) {

            /* Flatten KOTs */
            angular.forEach(self._kots, function (kot) {
              if (!kot.isVoid) {

                for(var it in kot.items){
                  if(_.has(kot.items[it],'addOns')) {
                    _.forEach(kot.items[it].addOns,function(addon){

                      _allItems.push(addon);
                      _.forEach(addon.taxes, function (tax) {
                        _allTaxes.push({
                          id: tax._id + "_amt",
                          name: tax.name + ' Amount',
                          tax_amount: tax.baseRate_preTax? parseFloat(tax.baseRate_preTax): 0,
                          isConsidered: false
                        });

                        /* Add Tax */
                        _allTaxes.push({
                          id: tax._id,
                          name: tax.name,
                          tax_amount: tax.tax_amount? parseFloat(tax.tax_amount): 0,
                          isConsidered: true
                        });

                        angular.forEach(tax.cascadingTaxes, function (casTax) {
                          //console.log(casTax);
                          /*Add Tax Subtotal Amount */

                          _allTaxes.push({
                            id: casTax._id + "_amt",
                            name: casTax.name + ' Amount',
                            tax_amount: (_.has(casTax, 'baseRate_preTax')) ? parseFloat(casTax.baseRate_preTax) : 0,
                            isConsidered: false
                          });
                          /* Add Tax */
                          _allTaxes.push({
                            id: casTax._id,
                            name: casTax.name,
                            tax_amount: tax.tax_amount? parseFloat(casTax.tax_amount): 0,
                            isConsidered: true
                          });

                        });
                      });
                    })
                  }
                }
                angular.forEach(kot.items, function (item) {
                  /*Adding Add Ons*/

                  /*End Add Ons*/

                  _allItems.push(item);
                  angular.forEach(item.taxes, function (tax) {
                    //console.log(tax.baseRate_preTax);
                    /*Add Tax Subtotal Amount */
                    _allTaxes.push({
                      id: tax._id + "_amt",
                      name: tax.name + ' Amount',
                      tax_amount: tax.baseRate_preTax? parseFloat(tax.baseRate_preTax): 0,
                      isConsidered: false
                    });
                    //console.log(tax.tax_amount);
                    /* Add Tax */
                    _allTaxes.push({
                      id: tax._id,
                      name: tax.name,
                      tax_amount: tax.tax_amount? Utils.roundNumber(parseFloat(tax.tax_amount),2): 0,
                      isConsidered: true
                    });

                    angular.forEach(tax.cascadingTaxes, function (casTax) {
                      //console.log(casTax);
                      /*Add Tax Subtotal Amount */

                      _allTaxes.push({
                        id: casTax._id + "_amt",
                        name: casTax.name + ' Amount',
                        tax_amount: (_.has(casTax, 'baseRate_preTax')) ? parseFloat(casTax.baseRate_preTax) : 0,
                        isConsidered: false
                      });
                      /* Add Tax */
                      _allTaxes.push({
                        id: casTax._id,
                        name: casTax.name,
                        tax_amount: tax.tax_amount? Utils.roundNumber(parseFloat(casTax.tax_amount),2): 0,
                        isConsidered: true
                      });

                    });
                  });
                });
                /*angular.forEach(kot.taxes, function (tax) {
                 _allTaxes.push(tax);
                 });*/
              }
            });
            /* Flatten Items */
            _.chain(_allItems).groupBy('_id').map(function (i) {
              var _itemAmount = 0,
                _itemQ = 0,
                _itemD = 0,
                _compItems = 0,
                _nonCompItems = 0,
                _compDiscount = 0,
                _nonCompDiscount = 0;
              var _itemBillDiscount=0;
              angular.forEach(i, function (it) {
                _itemAmount += Utils.roundNumber(parseFloat(it.subtotal),2);
                _itemQ += parseFloat(it.quantity);

                //console.log(i);
                /* Discounts */
                var d = 0;
                if (_.has(it, 'isDiscounted') && it.isDiscounted === true) {
                  angular.forEach(it.discounts, function (discount) {
                    if (discount.type === 'fixed') {
                      console.log(discount);
                      _itemD += parseFloat(discount.discountAmount);
                      d += discount.discountAmount;
                    }
                    if (discount.type === 'percentage') {
                      _itemD += parseFloat(discount.discountAmount);
                      d += discount.discountAmount;
                    }
                  });
                }
                 _itemBillDiscount+=Utils.roundNumber(it.billDiscountAmount,2);
                if(Utils.roundNumber(d) === Utils.roundNumber(it.subtotal)){
                  _compItems += it.quantity;
                  _compDiscount += Utils.roundNumber(d,2);
                } else {
                  _nonCompItems += it.quantity;
                  _nonCompDiscount += d;
                }
              });

              /* Flatten Tax */
              var _t = [];
              var _itemTotalTax = 0;
              var _allItemTaxes = [];
              angular.forEach(i, function (_ite) {
                angular.forEach(_ite.taxes, function (tax) {
                  //console.log(tax);

                  //console.log('tax', parseFloat(tax.tax_amount));

                  _allItemTaxes.push({
                    id: tax._id + '_amt',
                    name: tax.name,
                    tax_amount: tax.baseRate_preTax? parseFloat(tax.baseRate_preTax): 0,
                    isConsidered: false
                  });

                  _allItemTaxes.push({
                    id: tax._id,
                    name: tax.name,
                    tax_amount: tax.tax_amount? parseFloat(tax.tax_amount): 0,
                    isConsidered: true
                  });

                  angular.forEach(tax.cascadingTaxes, function (_cTax) {
                    if (_cTax.selected) {

                      _allItemTaxes.push({
                        id:_cTax._id + "_amt",
                        name: _cTax.name,
                        tax_amount: (_.has(_cTax, 'baseRate_preTax')) ? Utils.roundNumber(parseFloat(_cTax.baseRate_preTax),2) : 0,
                        isConsidered: false
                      });

                      //console.log('ctax', _cTax.tax_amount);

                      _allItemTaxes.push({
                        id: _cTax._id,
                        name: _cTax.name,
                        tax_amount: _cTax.tax_amount? Utils.roundNumber(parseFloat(_cTax.tax_amount),2): 0,
                        isConsidered: true
                      });



                    }
                  });


                });
              });

              //console.log('allItemTaxes', _allItemTaxes);

              /* Aggregate Taxes */
              var _groupedItemTaxes = [];
              _.chain(_allItemTaxes).groupBy('id').map(function (_ts) {
                var _tsTotal = 0;
                angular.forEach(_ts, function (_tswa) {
                  _tsTotal += Utils.roundNumber(parseFloat(_tswa.tax_amount),2);
                });
                _groupedItemTaxes.push({
                  id: _ts[0].id,
                  name: _ts[0].name,
                  isConsidered: _ts[0].isConsidered,
                  tax_amount: parseFloat(_tsTotal)
                });
              });
              /* Calc Total Tax */
              angular.forEach(_groupedItemTaxes, function (_taxwa) {
                if (_taxwa.isConsidered) {
                  _itemTotalTax += Utils.roundNumber(parseFloat(_taxwa.tax_amount),2);
                }
              });
              if(i[0].category){
                if((i[0].category.superCategory== undefined||i[0].category.superCategory==null)) {
                  i[0].category.superCategory = {};
                  i[0].category.superCategory.superCategoryName = 'NA';
                  i[0].category.superCategory._id = null;
                } else if(!i[0].category.superCategory.superCategoryName)
                  i[0].category.superCategory.superCategoryName = "NA";
              } else {
                i[0].category = {};
                i[0].category._id = null;
                i[0].category.categoryName = "NA";
                i[0].category.superCategory = {};
                i[0].category.superCategory.superCategoryName = 'NA';
                i[0].category.superCategory._id = null;
              }


              /* Flattened KOT Items */
              //console.log(_itemAmount, _itemD, _itemTotalTax);
              if(!i[0].category){
                i[0].category = {
                  categoryName: "NA",
                  superCategory: {
                    superCategoryName: "NA"
                  }
                };
              } else {
                if(!i[0].category.superCategory){
                  i[0].category.superCategory = {
                    superCategoryName: "NA"
                  }
                } else {
                  if(!i[0].category.superCategory)
                    i[0].category.superCategory.superCategoryName = "NA";
                }
              }
              var _i = {
                "id": i[0]._id,
                "name": i[0].name,
                "rate": i[0].rate,
                "quantity": _itemQ,
                "subtotal": parseFloat(_itemAmount),
                "taxes": _groupedItemTaxes,
                "itemNet": (parseFloat(_itemAmount) - parseFloat(_itemD)) + parseFloat(_itemTotalTax),
                "discount": _itemD,
                "tab": self.tab,
                "billDiscountAmount": _itemBillDiscount,
                "category": i[0].category.categoryName,
                "superCategory":i[0].category.superCategory.superCategoryName,
                "compItems": _compItems,
                "_compDiscount": _compDiscount,
                "nonCompItems": _nonCompItems,
                "_nonCompDiscount": _nonCompDiscount
              };
              //console.log(_i);
              self._items.push(_i);
            });

            /* Flatten Taxes */
            _.chain(_allTaxes).groupBy('id').map(function (t) {
              var _taxAmount = 0;
              //console.log(t);
              angular.forEach(t, function (tt) {
                _taxAmount += Utils.roundNumber(parseFloat(tt.tax_amount),2);
              });

              self._taxes.push({
                "id": t[0].id,
                "name": t[0].name,
                "tax_amount": parseFloat(_taxAmount),
                "isConsidered": t[0].isConsidered
              });
            });

          }

          d.resolve();

        }, 100);


        return d.promise;
      },

      getRoundOff: function (netAmount) {
        var _r = Utils.roundNumber(parseFloat(parseFloat(netAmount) % 1),2);
        if (_r >= .5) {
          return Utils.roundNumber(parseFloat(1-_r),2);
        } else {
          if (_r < .5) {
            return -Utils.roundNumber((_r),2);
          }
        }
      },
      getAfterRoundOff: function (netAmount) {
        //return Math.round(netAmount);
        return parseFloat(Utils.roundNumber(netAmount), ($rootScope.disable_roundoff?2:0));
      },
      // function added by RAJAT to add ruplee payment
      // function added by RAJAT for payments //

      checkRupleeBill: function (otherForm) {
        var self = this;
        var isActive = false;

        var ruplee_settings = _.filter(localStorageService.get('settings'), {
          "name": "Ruplee MerchantId",
          "selected": true
        });
        if (ruplee_settings.length > 0)
          var merchant_id = ruplee_settings[0].value;

        var min = 0;
        var minimum = _.filter(localStorageService.get('settings'), {
          "name": "minimum_order_delivery",
          "selected": true
        });
        if (minimum.length > 0)
          min = Number(minimum[0].value);

        if (!(angular.isDefined(merchant_id))) {
          growl.error("No Ruplee MerchantId Set", {ttl: 3000});
        }
        else if (this.aggregation.netRoundedAmount < min) {
          growl.error("Minimum order is" + min, {ttl: 3000});
        }
        else if (isActive) {
          console.log("bill paid");
          this.addOtherPayment(otherForm);
        }

        else if (!(self._customer.mobile)) {
          var modalInstance = $modal.open({
            templateUrl: 'app/billing/customerPhoneRuplee.html',
            controller: ['$rootScope', '$scope', '$modalInstance', function ($rootScope, $scope, $modalInstance) {

              //  $scope.bill=bill;
              $scope.form = {};

              $scope.ok = function (phone) {
                var ex = new RegExp('^[0-9]+$');
                if ((!(ex.test(phone))) || phone.length != 10) {
                  console.log(phone.length, ex.test(phone));
                  growl.error("Please enter a valid Mobile Number", {ttl: 3000});
                }
                else {
                  $modalInstance.close(phone);
                }
              };
              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
            }],
            size: 'sm'
          });

          modalInstance.result.then(function (phone) {
            self._customer.mobile = phone;
            self.sendBillToRuplee(otherForm);
          }, function () {
            console.log('Modal dismissed at: ' + new Date());
          });
        }
        else
          this.sendBillToRuplee(otherForm);
      },

      sendBillToRuplee: function (otherForm) {


        var self=this;
        console.log("add ruplee");
        if (parseFloat(this.otherForm.amount) <= this.getAmountRemaining()) {

          var merchant_id = _.filter(localStorageService.get('settings'), {
            "name": "Ruplee MerchantId",
            "selected": true
          })[0].value;

          var rest_id = merchant_id;
          var bill = angular.copy(this);
          console.log("test", this);

          var discount = bill.aggregation.billDiscount;
          if (bill.aggregation.totalItemDiscount)
            discount += bill.aggregation.totalItemDiscount;

          var service = 0;
          var vat = 0;
          var local = 0;
          for (var i = 0; i < bill.aggregation.taxDetail.length; i++) {
            if (bill.aggregation.taxDetail[i].name.replace(/\s/g, '').toLowerCase().indexOf("vat") != -1)
              vat += bill.aggregation.taxDetail[i].amount;
            else if (bill.aggregation.taxDetail[i].name.replace(/\s/g, '').toLowerCase().indexOf("servicecharge") != -1)
              service += bill.aggregation.taxDetail[i].amount;
            else
              local += bill.aggregation.taxDetail[i].amount;
          }

          var _bill = {};
          _bill.customer_mobile = bill._customer.mobile;
          _bill.merchant_id = merchant_id;
          _bill.bill_no = bill.serialNumber.toString() + bill._created.toString();
          _bill.service_tax = service.toFixed(2);
          _bill.vat = vat.toFixed(2);
          _bill.local_tax = local.toFixed(2);
          _bill.bill_subtotal = (bill.aggregation.totalAmount).toFixed(2);
          _bill.items = [];
          _bill.round_off = bill.aggregation.roundOff.toFixed(2);
          _bill.grand_total = bill.otherForm.amount;
          _bill.discount = discount.toFixed(2);
          // _bill.table_no=bill._tableId;

          for (var i = 0; i < bill._kots.length; i++) {
            if (bill._kots[i].items) {
              for (var j = 0; j < bill._kots[i].items.length; j++) {
                var item = {};
                item.item_details = bill._kots[i].items[j].name;
                item.qty = bill._kots[i].items[j].quantity;
                item.pricePerItem = bill._kots[i].items[j].priceQuantity;
                item.totalAmountForItem = bill._kots[i].items[j].subtotal;
                _bill.items.push(item);
                if (bill._kots[i].items[j].addOns) {
                  for (var k = 0; k < bill._kots[i].items[j].addOns.length; i++) {
                    var addOn = {};
                    addOn.item_details = bill._kots[i].items[j].addOns[k].name;
                    addOn.qty = bill._kots[i].items[j].addOns[k].quantity;
                    addOn.pricePerItem = bill._kots[i].items[j].addOns[k].rate;
                    addOn.totalAmountForItem = bill._kots[i].items[j].addOns[k].subtotal;
                    _bill.items.push(addOn);
                  }
                }
              }
            }
          }
          _bill.call_back_url = 'http://'+window.location.hostname+'/api/payments/ruplee/callback';
          // _bill.call_back_url = 'http://requestb.in/1l5lt961';
          //  _bill.call_back_url="http://requestb.in/11m29x11";
          _bill.special_key = localStorageService.get('deployment_id');
          console.log(_bill);

          $http({
            method: 'POST',
            url: '/api/payments/ruplee/',
            data: {
              bill: _bill,
              tenant_id: localStorageService.get('tenant_id'),
              deployment_id: localStorageService.get('deployment_id'),
              billId: bill._id,
              posBillId:bill._id
            }
          }).success(function (data) {
            console.log("ruplee", data);
            //growl.success("bill sent to ruplee user", {ttl: 3000});
            //$rootScope.settleModalClose = true;
            if (!data.method) {


              self.addPaymentToBill(data,otherForm);
            }
          }).error(function (data) {
            growl.error("Bill not sent to ruplee user", {ttl: 3000});
            console.log("bill not sent to ruplee");
          })
        }

        else {
          alert('more amount');
          return false;
        }
      },
      //---ADDED BY RAJAT END---//

      //---ADDED BY RAJAT FOR MOBIKWIK START---//

      checkMobikwikSettings: function (otherForm) {
        var self = this;

        $http({
          method: 'GET',
          url: '/api/mobikwikSettings/',
          params: {
            deployment_id: localStorageService.get('deployment_id'),
            tenant_id: localStorageService.get('tenant_id')
          }
        }).success(function (settings) {
          if (settings.length > 0) {

            $rootScope.mobikwikSettings = settings[0];
            self.checkBillForMobikwik(otherForm);
          }
          else
            growl.error("No Mobikwik Settings Found", {ttl: 3000})
        }).error(function (err) {
          growl.error("No Mobikwik Settings Found Or Error in Connection")
        })
      },

      checkBillForMobikwik: function (otherForm) {
        var self = this;
        var isActive = false;


        var min = 0;
        var minimum = _.filter(localStorageService.get('settings'), {
          "name": "minimum_order_delivery",
          "selected": true
        });
        if (minimum.length > 0)
          min = Number(minimum[0].value);

        if (this.aggregation.netRoundedAmount < min) {
          growl.error("Minimum order is" + min, {ttl: 3000});
        }
        else if (isActive) {
          console.log("present");
          this.addOtherPayment(otherForm);
        }
        else if (!(self._customer.mobile)) {
          var modalInstance = $modal.open({
            templateUrl: 'app/billing/customerPhoneRuplee.html',
            controller: ['$rootScope', '$scope', '$modalInstance', function ($rootScope, $scope, $modalInstance) {

              //  $scope.bill=bill;
              $scope.form = {};

              $scope.ok = function (phone) {
                var ex = new RegExp('^[0-9]+$');
                if ((!(ex.test(phone))) || phone.length != 10) {
                  console.log(phone.length, ex.test(phone));
                  growl.error("Please enter a valid Mobile Number", {ttl: 3000});
                }
                else {
                  $modalInstance.close(phone);
                }
              };
              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
            }],
            size: 'sm'
          });

          modalInstance.result.then(function (phone) {
            self._customer.mobile = phone;
            self.sendBillToMobikwik(otherForm);
          }, function () {
            console.log('Modal dismissed at: ' + new Date());
          });
        }
        else
          this.sendBillToMobikwik(otherForm);

      },

      sendBillToMobikwik: function (otherForm) {
        if (parseFloat(this.otherForm.amount) <= this.getAmountRemaining()) {
          var self=this;
          if(self.onlineTransaction && (_.filter(self.onlineTransaction,{paymentType:'mobikwik'}).length>0)){
            growl.error("Bill already sent",{ttl:3000});
            return false;
          }

          var merchantid = $rootScope.mobikwikSettings.merchant_id;
          var merchantname = $rootScope.mobikwikSettings.merchant_name;
          var secretKey = $rootScope.mobikwikSettings.secret_key;
          //changed
          var storeid = this.deployment_id;
          var posid='none';
          if(this.instance_id)
            posid=this.instance_id;
          //changed
          var bill = this;
          var discount = bill.aggregation.billDiscount;
          if (bill.aggregation.totalItemDiscount)
            discount += bill.aggregation.totalItemDiscount;

          var _bill = {};

          _bill.merchantid = merchantid;
          _bill.merchantname = merchantname;
          _bill.storeid = storeid;
          _bill.posid = posid;                                 //changed for changes from mobikwik's side
          _bill.membercell = bill._customer.mobile;
          _bill.orderid = bill.serialNumber + new Date(bill._created).toISOString().replace(/:/g,'').replace(/-/g,'').replace('.','');
          _bill.totalamount = (bill.aggregation.netAmount).toFixed(2).toString();
          _bill.totaldiscount = discount.toString();
         // _bill.txnamount = bill.aggregation.netRoundedAmount.toString();
          _bill.txnamount=bill.otherForm.amount;
          _bill.roundoffamount = bill.aggregation.roundOff.toString();
          _bill.responseurl ='http://'+ window.location.hostname + '/api/payments/mobikwik/callback';
          // _bill.responseurl = 'http://requestb.in/sgmsdysg';
          var particulars = [];
          _.forEach(bill._kots, function (kot) {
            _.forEach(kot.items, function (item) {
              var _item = {};
              _item.name = item.name
              _item.quantity = item.quantity.toString();
              _item.rate = item.rate.toString();
              _item.amount = item.priceQuantity.toFixed(2).toString();
              _item.discount = '0'
              particulars.push(_item);
              if (item.addOns) {
                _.forEach(item.addOns, function (addOn) {
                  var addon = {};
                  addon.name = addOn.name;
                  addon.quantity = addOn.quantity.toString();
                  addon.rate = addOn.rate.toString();
                  addon.amount = addOn.subtotal.toString();
                  addon.discount = '0';
                  particulars.push(addOn);
                })
              }
            })
          })
          _bill.particulars = particulars;
          var taxes = [];
          _.forEach(bill.aggregation.taxDetail, function (tax) {
            var _tax = {name: tax.name, rate: '', amount: tax.amount.toString()};
            taxes.push(_tax);
          });
          _bill.taxes = taxes;

          console.log('mobikwik bill', _bill);
          //var tabData = {tab: bill.tab, tabType: bill.tabType, billNumber: bill.billNumber, _tableId: bill._tableId}

          $http({
            method: 'POST',
            data: {
              bill: _bill,
              deployment_id: localStorageService.get('deployment_id'),
              tenant_id: localStorageService.get('tenant_id'),
              secretKey: secretKey,
              billId:bill._id,
              posBillId:bill._id
            },
            url: '/api/payments/mobikwik/createInvoice'
          }).success(function (data) {
            self.addPaymentToBill(data,otherForm);
          }).error(function (err) {
            if (err.txnStatusCode)
              growl.error(err.txnStatusMsg, {ttl: 3000});
            else
              growl.error("Could not connect with Mobikwik!", {ttl: 3000});
            console.log(err);
          })
        }
        else {
          alert("More Amount");
          return false;
        }
      },
      //--ADDED BY RAJAT FOR MOBIKWIK--//

       //--ADDED BY RAJAT FOR PAYTM---//
      checkPaytmBill: function (otherForm) {
        var self = this;
        var min = 0;
        var merchantGuid,secretKey;
        var industryId='Retail';
        if(self.onlineTransaction && _.filter(self.onlineTransaction,{paymentType:'paytm'}).length>0){
          growl.error("Payment already added",{ttl:3000});
          return false;
        }

        if(!(merchantGuid=Utils.getSettingValue('Paytm Merchant Guid',localStorageService.get('settings')))){
          growl.error("Paytm Merchant Id Not Set",{ttl:3000});
          return false;
         }

          if(!(secretKey=Utils.getSettingValue('Paytm Secret Key',localStorageService.get('settings')))){
          growl.error("Paytm Secret Key Not Set",{ttl:3000});
          return false;
         }

          var mobileNumber = self._customer.mobile;
          var modalInstance = $modal.open({
            template: '<div class="modal-header text-center">' +
            '<h3 style="color:blue">Paytm TOTP</h3><div class="modal-body text-center"><input type="text" class="form-control" ng-model="phone" placeholder="Customer Mobile" ng-show="!mobileNumber"/><br/>'+
            '<input type="text" class="form-control" ng-model="totp" placeholder="TOTP"/><br/>' +
            '<div align="center"><button type="button" class="btn btn-primary"  ng-click="withdraw()">Pay</button>&nbsp;'+
            '&nbsp;<button type="button" class="btn btn-primary"  ng-click="checkStatus()">Status</button>&nbsp;'+
            '&nbsp;<button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>'+
            '</div></div>',
            backdrop: 'static',
            keyboard: false,
            resolve: {
              mobileNumber: function () {
                return self._customer.mobile;
              },
              bill:function(){
                return self;
              },
              merchantGuid: function(){
                return merchantGuid;
              },
              secretKey:function(){
                return secretKey;
              }
            },

            controller: ['$rootScope', '$scope', '$modalInstance', 'mobileNumber', 'bill','merchantGuid','secretKey',function ($rootScope, $scope, $modalInstance, mobileNumber, bill,merchantGuid,secretKey) {

              $scope.mobileNumber = mobileNumber;
              console.log($scope.mobileNumber);

              $scope.withdraw = function () {
                var phone=$scope.mobileNumber?$scope.mobileNumber:$scope.phone;
                  var ex = new RegExp('^[0-9]+$');
                  if ((!(ex.test(phone))) || phone.length != 10) {
                    console.log(phone.length, ex.test(phone));
                    growl.error("Please enter a valid Mobile Number", {ttl: 3000});
                    return false;
                   }
                  $modalInstance.close({totp: $scope.totp, phone: phone,action:"withdraw"});
               };

               $scope.checkStatus=function(){
                var body={
                  "request":
                  {"requestType":"merchanttxnid",
                  "txnType":"withdraw",
                  "txnId":bill._id },
                  "ipAddress": "127.0.0.1",
                  "platformName": "PayTM",
                   "operationType": "CHECK_TXN_STATUS",
                    "channel":"POS",
                    "version":"1.0"}
                 $http.post('/api/payments/paytm/checkStatus',{bill:body,mid:merchantGuid,secret_key:secretKey}).success(function(data){
                  growl.success("Payment Successful",{ttl:3000});
                  $modalInstance.close({action:'addPayment',amount:data.amount});
                }).error(function(err){
                  growl.error("Transaction not successful or Not Found",{ttl:3000});
                })
               }


              $scope.cancel=function(){
                $modalInstance.dismiss('cancel');
              }

            }],
            size: 'sm'
          });

          modalInstance.result.then(function (data) {
            console.log(data);
            if (data.action=='withdraw') {
              self._customer.mobile = data.phone;
              self.sendBillToPaytm(otherForm, data, merchantGuid, secretKey,industryId);
            }
            if(data.action=='addPayment'){
              otherForm.amount.$modelValue=data.amount;
              self.addPaymentToBill({paymentType:'paytm',billData:{posBillId:self._id,status:'paid'}},otherForm)
            }
          }, function () {
            console.log('Modal dismissed at:' + new Date());
          });
      },

      sendBillToPaytm: function (otherForm, data, merchantGuid, secret_key,industryId) {
        var self = this;

        if (parseFloat(this.otherForm.amount) <= this.getAmountRemaining()) {
          var posId = localStorageService.get('deployment_id');
          var self = this;
          var bill = {
            "request": {
              "totalAmount":self.otherForm.amount,
              "currencyCode": "INR",
              "merchantGuid": merchantGuid,
              "merchantOrderId": self._id,
              "industryType": industryId,
              "posId": posId,
              "callbackUrl": 'http://'+window.location.hostname+'/api/payments/paytm/callback',
              "totp":data.totp
            },
            "platformName": "PayTM",
            "ipAddress": "139.162.28.200", "operationType": "WITHDRAW_MONEY",
            "channel": "POS",
            "version": "1.0"
          }

          var invoiceDetails={};
          invoiceDetails.headerName=["ItemName","Rate","Quantity","Amount"];
          invoiceDetails.itemDetails=[];
          invoiceDetails.taxDetails=[];
          _.forEach(self._kots,function(kot){
            _.forEach(kot.items,function(item){
              invoiceDetails.itemDetails.push([item.name,item.rate,item.quantity,item.subtotal]);
           });
          })

          _.forEach(self._items,function(item){
            invoiceDetails.itemDetails.push([item.name,item.rate,item.quantity,item.subtotal]);
          });

          _.forEach(self._kotsTaxes,function(tax){
              invoiceDetails.taxDetails.push({rowName:tax.name,rowValue:tax.tax_amount})
          });

          if(self.charges && self.charges.detail){
            _.forEach(self.charges.detail,function(ch){
                invoiceDetails.taxDetails.push({rowName:ch.name,rowValue:ch.amount});
            })
          }

          invoiceDetails.taxDetails.push({rowName:'Discount',rowValue:(self.getTotalDiscount()+self.getKotTotalDiscount()).toFixed(2)});
          bill.request.invoiceDetails=invoiceDetails;

          $http({
            method: 'POST',
            url: '/api/payments/paytm/withdraw',
            data: {
              bill: bill,
              secret_key: secret_key,
              deployment_id: localStorageService.get('deployment_id'),
              tenant_id: localStorageService.get('tenant_id'),
              mid: merchantGuid,
              mobile: self._customer.mobile,
              posBillId:self._id
            }
          }).success(function (data) {
            growl.success("Paytm Payment Successful",{ttl:3000});
            self.addPaymentToBill({paymentType:'paytm',billData:{posBillId:self._id,status:'paid'}},otherForm);
          }).error(function (err) {
            if (err && err.status)
              growl.error(err.status, {ttl: 3000});
            else
              growl.error("Could not connect with Paytm", {ttl: 3000});
            self.checkPaytmBill(otherForm);
          });
        }
        else {
          alert('More Amount');
          return false;
        }
      },

      addPartnerPayment: function (otherForm) {
        this.otherForm.otherName = otherForm.otherName.$modelValue.name;
         this.otherForm.amount=parseFloat(otherForm.amount.$modelValue);
        this._otherCards.push(angular.copy(this.otherForm));
        otherForm.$setPristine();
        this.assignCardAmount();
      },

      checkBingeBill: function (otherForm) {
        var self = this;
        var isActive = false;


        var min = 0;
        var minimum = _.filter(localStorageService.get('settings'), {
          "name": "minimum_order_delivery",
          "selected": true
        });
        if (minimum.length > 0)
          min = Number(minimum[0].value);

        var binge_username = _.filter(localStorageService.get('settings'), {
          "name": "binge_username",
          "selected": true
        });
        var binge_password = _.filter(localStorageService.get('settings'), {
          "name": "binge_password",
          "selected": true
        });

        if (binge_username.length == 0 || binge_password.length == 0) {
          growl.error("Binge Credentials not set", {ttl: 3000});
        }
        else if (isActive) {
          console.log("bill paid");
          this.addOtherPayment(otherForm);
        }

        else if (this.aggregation.netRoundedAmount < min) {
          growl.error("Minimum order is" + min, {ttl: 3000});
        }
        else {
          var modalInstance = $modal.open({
            template: '<div class="modal-header text-center">' +
            '<h3 style="color:blue">User BingeId</h3></div> <div class="modal-body text-center"> <p class="text-center"><b><i>Please Enter Customer Binge Id</i></b></p>' +
            '<br/> <input type="text" class="form-control" ng-model="bingeId" name="bingeId" /> </div>' +
            ' <div class="modal-footer">' +
            '<button class="btn btn-primary" type="button"  ng-click="ok(bingeId)">OK</button>' +
            '<button class="btn btn-primary" type="button" ng-click="cancel()">Cancel</button>' +
            '</div><footer> </footer> </div>',

            controller: ['$rootScope', '$scope', '$modalInstance', function ($rootScope, $scope, $modalInstance) {

              //  $scope.bill=bill;
              $scope.form = {};

              $scope.ok = function (bingeId) {
                if (bingeId && bingeId.length > 0)
                  $modalInstance.close(bingeId);
                else
                  growl.error("Please Enter a valid bingeId", {ttl: 3000});
              };
              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
            }],
            size: 'sm'
          });

          modalInstance.result.then(function (bingeId) {
            self.sendBillToBinge(otherForm, bingeId);
          }, function () {
            console.log('Modal dismissed at: ' + new Date());
          });
        }
      },

      sendBillToBinge: function (otherForm, bingeId) {
        var self=this;
        var username = "";
        var password = "";
        var binge_username = _.filter(localStorageService.get('settings'), {
          "name": "binge_username",
          "selected": true
        });
        var binge_password = _.filter(localStorageService.get('settings'), {
          "name": "binge_password",
          "selected": true
        });
        if (binge_username.length > 0 && binge_password.length > 0) {
          username = binge_username[0].value;
          password = binge_password[0].value;
        }
        if (username != "" && password != "") {
          console.log("id", bingeId);

          var discount = this.aggregation.billDiscount;
          if (this.aggregation.totalItemDiscount)
            discount += this.aggregation.totalItemDiscount;

          var request = {};
          request.client = {};
          var order = {
            bingeid: bingeId, tableid: this._tableId,
            bills: {
              bill: {
                billno: this.serialNumber,
                totalamount: (this.aggregation.netAmount).toFixed(2),
                roundoff: this.aggregation.roundOff,
                totalamountbeforetax: this.aggregation.totalAmount,
                discountamount: discount,
                paymentmode: 1
              },
              grandtotal: this.otherForm.amount, totalamounttopay: this.otherForm.amount,
              unpaidamount: this.otherForm.amount, paidamount: 0,
              paymentcallbackurl:'http://'+ window.location.hostname + '/api/payments/binge/callback'
            }
          };
          request.client.order = order;
          var item = [];
          for (var i = 0; i < this._kots.length; i++) {
            for (var j = 0; j < this._kots[i].items.length; j++) {
              item.push({
                name: this._kots[i].items[j].name,
                quantity: this._kots[i].items[j].quantity,
                rate: this._kots[i].items[j].priceQuantity,
                amount: this._kots[i].items[j].subtotal
              });
              if (this._kots[i].items[j].addOns) {
                for (var k = 0; k < this._kots[i].items[j].addOns.length; k++) {
                  var addOn = {};
                  addOn.name = this._kots[i].items[j].addOns[k].name;
                  addOn.quantity = this._kots[i].items[j].addOns[k].quantity;
                  addOn.rate = this._kots[i].items[j].addOns[k].rate;
                  addOn.amount = this._kots[i].items[j].addOns[k].subtotal;
                  item.push(addOn);
                }
              }
            }

          }
          request.client.order.bills.bill.items = {};
          request.client.order.bills.bill.items.item = [];
          request.client.order.bills.bill.items.item = item;

          var tax = [];
          for (var i = 0; i < this.aggregation.taxDetail.length; i++)
            tax.push({
              '@value': this.aggregation.taxDetail[i].amount,
              '@attributes': {'title': this.aggregation.taxDetail[i].name}
            });

          request.client.order.bills.bill.taxes = {};
          if (tax.length > 0) {
            request.client.order.bills.bill.taxes.tax = [];
            request.client.order.bills.bill.taxes.tax = tax;
          }
          else request.client.order.bills.bill.taxes.tax = "";

          request.client.datetime = (new Date(this._created)).toISOString().substring(0, 19).replace('T', ' ');

          console.log(request);
          //var tabData = {tab: this.tab, tabType: this.tabType, billNumber: this.billNumber, _tableId: this._tableId}
          $http({
            method: 'POST',
            url: '/api/payments/binge/sendBill',
            data: {
              request: request,
              auth: btoa(username + ':' + password),
              deployment_id: localStorageService.get('deployment_id'),
              tenant_id: localStorageService.get('tenant_id'),
              billId: this._id,
              username: username,
              password: password,
              posBillId:this._id
            }
          }).success(function (data) {
            console.log(data);
            self.addPaymentToBill(data,otherForm);
          }).error(function (err) {
            console.log(err);
            if (err && err.status) growl.error(err.status, {ttl: 3000});
            else
              growl.error("Could not connect with Binge", {ttl: 3000})
          });
        } else growl.error("Incorrect binge Credentials", {ttl: 3000});
      },

      addOtherPayment: function (otherForm) {
        console.log('otherForm', otherForm);
        this.otherForm.otherName = otherForm.name;
        this.otherForm.amount=parseFloat(otherForm.amount);
        //this.otherForm.otherType = otherForm.type;
        this._otherCards.push(angular.copy(this.otherForm));
        this.assignCardAmount();
      },
     /* addOnlinePayment: function (otherForm) {
        console.log('otherForm', otherForm);
        this.otherForm.otherName = otherForm.name;
        this.otherForm.otherType = otherForm.type;
        this._otherCards.push(angular.copy(this.otherForm));
        this.assignCardAmount();
      },
*/
      addPaymentToBill:function(d,otherForm){
        var self=this;
           delete d.deployment_id;
          delete d.tenant_id;
          var val=otherForm.otherName.$modelValue.name.toLowerCase();
          if(val=='urbanpiper')
            d.billData.amount=self.otherForm.amount;
          else if(val!='urbanpiper')
           d.billData.amount=parseFloat(otherForm.amount.$modelValue);
          var data=[];
        if(self.onlineTransaction)
          data=self.onlineTransaction;
          data.push(d);
          self.updateBill({
          onlineTransaction:data
        }, 'update').then(function (resultData) {
            console.log(resultData);
            self.lastModified = (!(resultData == undefined || resultData == null)) ? resultData : null;
          var val=otherForm.otherName.$modelValue.name.toLowerCase();
          if(val!='paytm' && val!='ikaaz' && val!='urbanpiper'){
           growl.success("Bill Sent To User",{ttl:3000});
           $rootScope.settleModalClose=true;
          }
         if(val=='paytm'||val=='ikaaz')
          self.addPartnerPayment(otherForm);
         if(val=='urbanpiper')
          self.addUrbanPiperPayment(otherForm);
        },function(err){
          //growl.error("Error in updating bill",{ttl:3000});
           var val=otherForm.otherName.$modelValue.name.toLowerCase();
           if(val=='paytm'||val=='ikaaz')
           self.addPartnerPayment(otherForm);
           if(val=='urbanpiper')
           self.addUrbanPiperPayment(otherForm);
        });
      },

      checkUrbanPiperSettings:function(otherForm){
        var self=this;
        if(_.filter(self.onlineTransaction && self.onlineTransaction,{paymentType:'urbanpiper'}).length>0){
          growl.error("Payment already added",{ttl:3000});
          return false;
        }
        var apiKey='';
        var username='';
        var self=this;
        if(!this._customer.mobile) growl.error("Please enter Customer's Phone number",{ttl:3000});

        else if($rootScope.urbanPiperSetting && $rootScope.urbanPiperSetting.apiKey ){
          apiKey=$rootScope.urbanPiperSetting.apiKey;
          username=$rootScope.urbanPiperSetting.username;
          self.openUrbanPiperModal(otherForm,apiKey,username);
        }
        else {
          $http({
            method: 'GET',
            url: '/api/urbanPiper',
            params: {
              deploymentId: localStorageService.get('deployment_id'),
              tenantId: localStorageService.get('tenant_id')
            }
          }).success(function(data){
            console.log(data);
            if(data && data.length>0 && data[0].apiKey && data[0].username){
              if(data[0].active==true){
                apiKey=data[0].apiKey; username=data[0].username;
                $rootScope.urbanPiperSetting = {};
                $rootScope.urbanPiperSetting.apiKey = data[0].apiKey;
                $rootScope.urbanPiperSetting.username = data[0].username;
                self.openUrbanPiperModal(otherForm,apiKey,username);
              }
              else growl.error("Please Activate UrbanPiper settings first",{ttl:3000});
            }
            else growl.error("No UrbanPiper Settings found",{ttl:3000});
          }).error(function(data){
            growl.error("Could not fetch UrbanPiper Settings",{ttl:3000});
          })
        }

      },

      openUrbanPiperModal: function (otherForm,apiKey,username) {
        var self = this;
        var modalInstance = $modal.open({
          template: '<div class="modal-header text-center">' +
          '<h3 style="color:blue">Urban Piper</h3><div class="modal-body text-center"><input type="text" class="form-control" ng-model="cardNumber" placeholder="Card Number" ng-show="false"/><br/>' +
          '<div align="center"><input type="text" class="form-control" ng-model="enteredAmount" ng-disabled="!isValidated" placeholder="Enter Amount"/><br/>'+
          '<button type="button" class="btn btn-primary"  ng-click="validatePayment(cardNumber)">Validate</button>'+
          '&nbsp;<button type="button" class="btn btn-primary" ng-disabled="!isValidated" ng-click="sendOtp(cardNumber)">Send Otp</button>' +
          '&nbsp;<button type="button" class="btn btn-primary" ng-click="checkStatus()">Status</button></div>'+
          '</div>',
          resolve: {
            mobileNumber: function () {
              return self._customer.mobile;
            },
            apiKey:function(){
              return apiKey;
            },
            username:function(){
              return username
            },
            billAmount:function(){
              return parseFloat(otherForm.amount.$modelValue);
            },
            bill:function(){
              return self;
            }
          },
          controller: ['$rootScope', '$scope', '$modalInstance','mobileNumber','username','apiKey','billAmount','bill', function ($rootScope, $scope, $modalInstance,mobileNumber,username,apiKey,billAmount,bill) {

            //  $scope.bill=bill;
            $scope.form = {};
            $scope.mobileNumber=mobileNumber;
            $scope.isValidated=false;
            $scope.username=username;
            $scope.apiKey=apiKey;
            $scope.amount=0;
            $scope.billAmount=billAmount;

            $scope.checkStatus=function(){
              var body={deployment_id:localStorageService.get('deployment_id'),posBillId:bill._id}
              $http.post('/api/payments/urbanpiper/status',body).success(function(data){
                growl.success("Payment Successful",{ttl:3000});
                  $modalInstance.close({action:'addPayment',amount:data.amount});
                }).error(function(err){
                  growl.error("No Transaction found for this bill",{ttl:3000});
                })
            }

            $scope.validatePayment = function (cardNumber) {

              if(cardNumber) fetchUrbanPiperAccountDetails({method:'card',cardNumber:cardNumber});
              else fetchUrbanPiperAccountDetails({method:'phone',phone:$scope.mobileNumber});
            };

            function fetchUrbanPiperAccountDetails(data){
              $http({
                method:'POST',
                url:'/api/payments/urbanpiper/validateAccount',
                data:{method:data.method,cardNumber:data.cardNumber,phone:data.phone,username:$scope.username,apiKey:$scope.apiKey}
              }).success(function(data){
                console.log("success",data);
                growl.success("Total Balance in user's account is "+data.total_balance);
                $scope.amount=data.total_balance;
                $scope.isValidated=true;
              }).error(function(err){
                console.log("error",err);
                if(err && err.status)
                  growl.error(err.status,{ttl:3000});
                else
                  growl.error("Could not connect with urban piper",{ttl:3000});
              })
            }

            $scope.sendOtp=function(cardNumber){
              console.log($scope.enteredAmount);
              if($scope.mobileNumber) {
                if(isNaN($scope.enteredAmount)||Number($scope.enteredAmount<=0)) growl.error("Invalid Amount",{ttl:3000});

                else if(parseFloat($scope.enteredAmount)>parseFloat($scope.amount)){
                  growl.error("Insufficient Balance in user's account",{ttl:3000});
                }
                else if(parseFloat($scope.enteredAmount)>parseFloat($scope.billAmount)){
                  growl.error("Invalid Amount",{ttl:3000})
                }
                else
                  $modalInstance.close({method: 'phone', phone: $scope.mobileNumber, amount: parseFloat($scope.enteredAmount)});
              }
              else
                $modalInstance.close({method:'card',cardNumber:cardNumber,amount:$scope.amount})
            }

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }],
          size: 'sm'
        });

        modalInstance.result.then(function (data) {
          console.log("method",data);
          if(data.action=='addPayment'){
             otherForm.amount.$modelValue=data.amount;
            self.addPaymentToBill({paymentType:'urbanpiper',billData:{posBillId:self._id,status:'paid'}},otherForm);
            }
          else
           self.sendBillToUrbanPiper(otherForm, apiKey, username,data);
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      },

      sendBillToUrbanPiper:function(otherForm,apiKey,username,data){
        console.log('otherform',otherForm);
        var self=this;
        if (parseFloat(this.otherForm.amount) <= this.getAmountRemaining()) {
          var billAmount = (this.getTotalBill() + this.getKotsTotalBill()).toFixed(2);
          var amount = Number(data.amount);
          //  if(amount>data.amount)
          // amount=data.amount;

          // this.otherForm.amount=amount;

          /* if (data.amount < amount ) {
           growl.error("Balance in user's account is insufficient", {ttl: 3000});
           }
           else if (amount > billAmount)
           growl.error("Invalid Amount", {ttl: 3000});
           else {*/
          $http({
            method: 'POST',
            url: '/api/payments/urbanpiper/sendBill',
            data: {
              username: username,
              apiKey: apiKey,
              phone: self._customer.mobile,
              cardNumber: data.cardNumber,
              method: data.method,
              amount: amount
            }
          }).success(function (data) {
            console.log("sucess", data);
            if (data.method == 'phone')
              self.confirmUrbanPiperPayment(otherForm, apiKey, username, amount);
            else if (data.method == 'card') {
              growl.success("Payment Successful", {ttl: 3000});
              otherForm.amount.$modelValue=amount;
              self.addPaymentToBill({paymentType:'urbanpiper',billData:{posBillId:self._id,status:'paid'}},otherForm);
              //self.addUrbanPiperPayment(otherForm);
            }
          }).error(function (err) {
            if (err && err.status) growl.error(err.status, {ttl: 3000});
            else
              growl.error("Something went wrong", {ttl: 3000});
          })
        }
        else growl.error("Invalid Amount",{ttl:3000});
      },

      confirmUrbanPiperPayment: function (otherForm,apiKey,username,amount) {
        var self = this;
        var modalInstance = $modal.open({
          template: '<div class="modal-header text-center">' +
          '<h3 style="color:blue">Please Enter OTP</h3></div><div class="modal-body text-center"><input type="text" class="form-control" ng-model="otp" placeholder="OTP" required/><br/>' +
          '<div align="center"><button type="button" class="btn btn-primary" ng-click="ok(otp)">OK</button>&nbsp;&nbsp;' +
          '<button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>'+
          '</div>'+
          '</div>',
          backdrop:'static',
          keyboard:false,
          controller: ['$rootScope', '$scope', '$modalInstance', function ($rootScope, $scope, $modalInstance) {

            $scope.ok = function (otp) {
              if (!otp) {
                growl.error("Please enter a valid otp");
              }
              else $modalInstance.close(otp);
            };

            $scope.cancel=function(){
              $modalInstance.dismiss('cancel');
            }

          }],
          size: 'sm'
        });

        modalInstance.result.then(function (otp) {
          console.log(otp);

          $http({
            method: 'POST',
            url: '/api/payments/urbanpiper/validatePayment',
            data: {username:username,apiKey:apiKey,phone:self._customer.mobile,amount:amount,pin:otp},
            params:{deployment_id:localStorageService.get('deployment_id'),tenant_id:localStorageService.get('tenant_id'),posBillId:self._id}
          }).success(function (data){
            growl.success("Payment Successful", {ttl: 3000});
               self.otherForm.amount=amount;
             // var otherform={otherName:{$modelValue:{name:otherForm.otherName.$modelValue.name}},amount:{$modelValue:amount}}
              self.addPaymentToBill({paymentType:'urbanpiper',billData:{posBillId:self._id,status:'paid'}},otherForm);
            //$rootScope.paymentDone=true;
          }).error(function (err) {
            if (err && err.status)growl.error(err.status, {ttl: 5000}); else growl.error("Payment could not be processed!,Please try again", {ttl: 5000});
            self.confirmUrbanPiperPayment(otherForm,apiKey,username,amount);
          })
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      },


      addUrbanPiperPayment:function(otherForm,amount) {
        var self=this;
        self.otherForm.otherName = otherForm.otherName.$modelValue.name;
        //self.otherForm.otherType = otherForm.otherType.$modelValue;
        self._otherCards.push(angular.copy(this.otherForm));
        otherForm.$setPristine();
        self.assignCardAmount();
      },

       initiateIkaazPayment:function(otherForm){
         var self=this;

        if(self.onlineTransaction && _.filter(self.onlineTransaction,{paymentType:'ikaaz'}).length>0){
          growl.error("Payment already added",{ttl:3000});
          return false;
        }

        var modalInstance = $modal.open({
            template: '<div class="modal-header text-center">' +
            '<h4 style="color:blue">Click on Pay and Tap your Card</h4><br/>'+
            '<div align="center"><button type="button" class="btn btn-primary"  ng-click="getTxnUid()">Pay</button>&nbsp;'+
            '&nbsp;<button type="button" class="btn btn-primary"  ng-click="checkStatus()">Status</button>&nbsp;'+
            '&nbsp; <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>'+
            '</div></div>',
            backdrop: 'static',
            keyboard: false,
            resolve: {
              bill:function(){
                return self;
              }
            },

            controller: ['$rootScope', '$scope', '$modalInstance','bill', function ($rootScope, $scope, $modalInstance,bill) {


              $scope.getTxnUid = function () {
               $rootScope.showLoaderOverlay=true;

                $modal.open({
                   template: '<h4 style="margin:20px;">Processing your card !</h4>',
                   scope: $scope,
                   controller: ['$rootScope', '$scope', '$modalInstance', function ($rootScope, $scope, $modalInstance) {

                 var xhttp = new XMLHttpRequest();

                xhttp.onreadystatechange = function() {
                  $rootScope.showLoaderOverlay=false;
                 if (this.readyState == 4 && this.status==200){
                  var data=JSON.parse(this.response);
                  if(data.status=="0" && data.errorMessage)
                    $modalInstance.close({tagUid:data.errorMessage,action:"withdraw"});
                   else{
                    growl.error(data.errorMessage,{ttl:3000});
                    $modalInstance.dismiss('cancel');
                  }
                 }
                 else if(this.readyState==4 && this.status!=200){
                  growl.error("Could not process card.Please try again",{ttl:3000});
                  $modalInstance.dismiss('cancel');
                 }
              };
              xhttp.open("GET", "http://localhost:8181/TapPay/Execute", true);
              xhttp.send();
             }],

             size:'sm'
             }).result.then(function(data){
               $modalInstance.close(data);
             },function(err){
              console.log("cancelled");
             });

            }


               $scope.checkStatus=function(){
                var body={deployment_id:localStorageService.get('deployment_id'),posBillId:bill._id}
                 $http.post('/api/payments/ikaaz/checkStatus',body).success(function(data){
                  growl.success("Payment Successful",{ttl:3000});
                  $modalInstance.close({action:'addPayment',amount:data.amount});
                }).error(function(err){
                  growl.error("No Transaction found for this bill",{ttl:3000});
                })
               }

              $scope.cancel=function(){
                $modalInstance.dismiss('cancel');
              }

            }],
            size: 'sm'
          });

          modalInstance.result.then(function (data) {
            console.log(data);
            $rootScope.showLoaderOverlay=false;
            if (data.action=='withdraw') {
              self.sendBillToIkaaz(otherForm,data.tagUid);
            }
            if(data.action=='addPayment'){
              otherForm.amount.$modelValue=data.amount;
              self.addPaymentToBill({paymentType:'ikaaz',billData:{posBillId:self._id,status:'paid'}},otherForm);
            }
          }, function () {
            console.log('Modal dismissed at: ' + new Date());
          });
      },

      sendBillToIkaaz: function(otherForm,tagUid){
        var self=this;
        console.log("tagUd",tagUid);
        //tagUid='B87CBAB6303030303030303030303030303030303030303030303030303030304D58312D41582D3030302D41473230303030303030303030D2';
        var _bill={"tagUid":tagUid,"txnType":"SALE","channel":'MPOS',"amount":self.otherForm.amount,"txnNumber":'989898'}
        var data={deployment_id:localStorageService.get('deployment_id'),
                  tenant_id:localStorageService.get('tenant_id'),
                  bill:_bill,
                  posBillId:self._id
                  }
         $http.post('/api/payments/ikaaz/sendBill',data).success(function(data){
            growl.success("Payment Successful.Balance left : "+data.balance, {ttl: 4000});
            self.addPaymentToBill({paymentType:'ikaaz',billData:{posBillId:self._id,status:'paid'}},otherForm);
         }).error(function(err){
            growl.error("Payment Failed",{ttl:3000});
         }) ;
      },

      sendBillToOthers:function(option,otherForm){
        if (option == "ruplee") {
          console.log(option);
          this.checkRupleeBill(otherForm);
        }
        else if (option == 'mobikwik') {
          console.log('mobikwik');
          this.checkMobikwikSettings(otherForm);
        }
        else if (option == 'paytm') {
          console.log('paytm');
          this.checkPaytmBill(otherForm);
        }
        else if (option == 'binge') {
          console.log('binge');
          this.checkBingeBill(otherForm);
        }
        else if (option == 'urbanpiper') {
          console.log('urban piper');
          this.checkUrbanPiperSettings(otherForm);
        }
        else if(option=='ikaaz'){
          console.log("ikaaz");
          this.initiateIkaazPayment(otherForm);
        }
      },
      // --- payments end---  //
//--coupon settlement option start---//

         addCouponPayment:function(couponForm){
          /*if (this._coupons.length > 0) {
          growl.error('Cant add more entry into coupon settlement !!', {ttl: 2000});
          return false;
        }*/
        if (couponForm.$valid) {
          if (parseFloat(this.couponForm.amount) <= 0) {
            growl.error("Invalid Amount",{ttl:3000});
           couponForm.$setPristine();
           this.assignCardAmount();
            return false;
          }
          if(parseFloat(this.couponForm.amount)>this.getAmountRemaining()){
            growl.error("Entered Amount exceeds Bill Amount",{ttl:3000});
            couponForm.$setPristine();
            this.assignCardAmount();
            return false;
          }


          if (parseFloat(this.couponForm.amount) <= this.getAmountRemaining()) {
               // console.log(couponForm);
                this.couponForm.couponName = couponForm.couponName.$modelValue.name;
                this.couponForm.amount=parseFloat(this.couponForm.amount)
                this._coupons.push(angular.copy(this.couponForm));
               // console.log(this._coupons);
                couponForm.$setPristine();
              }
              else {
                alert('more amount');
                return false;
              }
              this.assignCardAmount();
            }

          else{ growl.error('more amount'); return false;}
         },
          removeCoupon: function (idx) {
          this._coupons.splice(idx, 1);
          this.assignCardAmount();
         },

         getTotalCouponPayment: function () {
         var _tCards = 0;
        _.forEach(this._coupons, function (db) {
          // console.log(db);
          _tCards += parseFloat(db.amount);
        });

        return parseFloat(_tCards);
      },

      removeTax:function(tax){
        this.removedTaxes.push(tax);
        this.processBill();
        this.updateBill({},'update').then(function(){
          console.log("bill updated");
        })
        console.log("after removing tax",this);
      },

      isRemoved:function(tax){
        var self=this;
        console.log(_.filter(self.removedTaxes,{id:tax._id}).length>0);
       if(tax._id)
        return _.filter(self.removedTaxes,{id:tax._id}).length>0;
      else if(tax.id)
       return _.filter(self.removedTaxes,{id:tax.id}).length>0;
      }

    }

    return (Bill);


  });
