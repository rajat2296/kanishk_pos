'use strict';

angular.module('posistApp')
    .factory('BillPrintService', function($http, localStorageService, $rootScope, printer, Utils) {


        return {
            setString: function(string, length) {
                if (string == undefined) {
                    return '';
                }
                var str = "";
                if (string.length > length) {
                    str = string.substring(0, length);
                } else {
                    var blankchar = "";
                    for (var i = 0; i < (length - string.length); i++) {
                        blankchar += " ";
                    }
                    str = string + blankchar;
                }
                return str;
            },

            setAmount: function(string, length) {
                if (string == undefined) {
                    return '';
                }
                var str = "";
                var blankchar = "";
                for (var i = 0; i < (length - string.length); i++) {
                    blankchar += " ";
                    str = blankchar + string;
                }
                return str;
            },

            wordWrapString: function(string, length) {
                if (string == undefined) {
                    return '';
                }
                var str = "";
                var finalString = "";
                if (string.length > length) {
                    str = string;
                    while (string.length > length) {
                        str = string.substring(0, length);
                        console.log(str);
                        finalString += str + '\r\n';
                        string = string.slice(length, string.length);
                    }
                    var blankchar = "";
                    for (var i = 0; i < (length - string.length); i++) {
                        blankchar += " ";
                    }
                    finalString += string + blankchar;
                } else {
                    var blankchar = "";
                    for (var i = 0; i < (length - string.length); i++) {
                        blankchar += " ";
                    }
                    finalString = string + blankchar;
                }
                return finalString;
            },

            wordWrapString1: function(string, length) {
                if (string == undefined) {
                    return '';
                }
                var str = "";
                var finalString = "";
                if (string.length > length) {
                    str = string;
                    var count = 0;
                    while (string.length > length) {
                        str = string.substring(0, length);
                        console.log(str);
                        if (count > 0)
                            finalString += "        " + str + '\r\n';
                        else
                            finalString += str + '\r\n';
                        string = string.slice(length, string.length);
                        count++;
                    }
                    var blankchar = "";
                    for (var i = 0; i < (length - string.length); i++) {
                        blankchar += " ";
                    }
                    finalString += "        " + string + blankchar;
                } else {
                    var blankchar = "";
                    for (var i = 0; i < (length - string.length); i++) {
                        blankchar += " ";
                    }
                    finalString = string + blankchar;
                }
                return finalString;
            },

            templateBillPrintWithTaxBaseAmount: function(printdata) {
                console.log(printdata);
                var settings = (localStorageService.get('settings'));
                var bill = printdata.bill;
                var index = printdata.index;
                var billNumber = Utils.hasSetting('reset_serial_daily', localStorageService.get('settings')) ? bill.daySerialNumber : bill.serialNumber;
                var disableRoundoff = Utils.hasSetting('disable_roundoff', localStorageService.get('settings'));
                var items = [];
                items = printdata.items;

                var totalAmount = printdata.totalAmount;
                var totalDiscount = printdata.totalDiscount;
                var totalTax = printdata.totalTax;
                var taxes = printdata.taxes;
                console.log(totalAmount, totalDiscount, totalTax);
                _.forEach(bill._kots, function(kot) {
                        _.forEach(kot.items, function(item) {
                            _.forEach(item.addOns, function(addOn) {
                              console.log(addOn);
                                items.push({ name: addOn.name, quantity: addOn.quantity, subtotal: addOn.subtotal,rate:addOn.rate,isContainAddOn:true });
                            })
                        })
                    })


                //console.log(paymentMethod);
                var header = Utils.getSettingValue('header_bill', localStorageService.get('settings'));
                var footer = Utils.getSettingValue('footer_bill', localStorageService.get('settings'));
                var tqty = 0;
                var subtotal = 0;
                var char = 0;
                var billHtml = '';
                if (Utils.hasSetting('settle_bill_on_print', localStorageService.get('settings'))) {

                    if (bill.rePrint && bill.rePrint.length > 2) {
                        billHtml += 'Duplicate Print';
                        billHtml += '\r\n--------------------------------------\r\n';
                    }
                } else {

                    if (bill.rePrint && bill.rePrint.length > 1) {
                        billHtml += 'Duplicate Print';
                        billHtml += '\r\n--------------------------------------\r\n';
                    }
                }

                billHtml += header;
                billHtml += '\r\n--------------------------------------\r\n';
                billHtml += '             ' + bill.tab + '\r\n';
                billHtml += '           RETAIL INVOICE';
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill.prefix != null)
                    billHtml += "Bill Number:" + bill.prefix + "-" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                else
                    billHtml += "Bill Number:" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                if (bill.tabType == 'table')
                    billHtml += 'Staff: ' + (bill._waiter.firstname == undefined ? '' : bill._waiter.firstname) + ' ' + (bill._waiter.lastname == undefined ? '' : bill._waiter.lastname) + '\r\n';
                else
                    billHtml += 'Delivery Boy:' + (bill._delivery.firstname == undefined ? '' : (bill._delivery.firstname + ' ' + bill._delivery.lastname)) + '\r\n';
                billHtml += 'Date:' + Utils.getDateFormatted(bill.rePrint[0].printTime);
                if (bill.rePrint && bill.rePrint.length > 1)
                    billHtml += '\r\nRePrint Time: ' + Utils.getDateFormatted(bill.rePrint[bill.rePrint.length - 1].printTime);
                if (bill._tableId != undefined) {
                    billHtml += '\r\nTable: ' + bill._tableId;
                } else if (bill.tableNumber != undefined) {
                    billHtml += '\r\nTable: ' + bill.tableNumber;
                }
                billHtml += '\r\n--------------------------------------\r\n';
               if (bill._customer && bill._customer.mobile != undefined) {
                   billHtml += 'Customer Detail';
                   billHtml += '\r\n===============\r\n';
                   billHtml += 'Name: ' + bill._customer.firstname + '\r\n';
                   if(bill._customer.address1 || bill._customer.address2 || bill._customer.city || bill._customer.state || bill._customer.postCode)
                   {
                   billHtml +='Add:';
                   if(bill._customer.address1)
                   {
                   billHtml += bill._customer.address1  + ',' + '\r\n';
                   }
                   if(bill._customer.address2)
                   {
                   billHtml += bill._customer.address2+ ',' + '\r\n';
                   }
                   if(bill._customer.city)
                   {
                   billHtml += bill._customer.city;
                   }
                   if(bill._customer.state)
                   {
                   billHtml += ',' + bill._customer.state;
                   }
                   if(bill._customer.postCode)
                   {
                   billHtml +=',' + bill._customer.postCode;
                   }
                   billHtml +='\r\n'
                 }
                 
                   billHtml += 'Mobile: ' + bill._customer.mobile;
                   billHtml += '\r\n--------------------------------------\r\n';
               }



                billHtml += this.setString('Description', 15) + this.setString('Qty', 3) + this.setAmount('Price', 8) + this.setAmount('Amount', 8);
                billHtml += '\r\n--------------------------------------\r\n';
                for (var i = 0; i < items.length; i++) {
                    tqty += items[i].quantity;
                    subtotal += parseFloat(items[i].subtotal);
                    billHtml += this.wordWrapString(items[i].name, 15) + ' ' + this.setString(items[i].quantity.toString(), 3) + ' ' + this.setAmount((parseFloat(items[i].rate).toFixed(0)).toString(), 6) + ' ' + this.setAmount((parseFloat(items[i].subtotal).toFixed(2)).toString(), 9) + '\r\n';
                }
                //console.log(parseFloat(subtotal));
                billHtml += '--------------------------------------\r\n';
                // billHtml += this.setString('Total Qty:', 18) + this.setAmount(tqty.toString(), 9) + '\r\n';
                billHtml += this.setString('SubTotal:', 25) + this.setAmount((parseFloat(subtotal)).toFixed(2), 10) + '\r\n';
                billHtml += this.setString('Discount:', 25) + this.setAmount(totalDiscount.toFixed(2), 10) + '\r\n';
                billHtml += '--------------------------------------\r\n';
                billHtml += this.setString('Tax', 19) + this.setString('Base Amt', 9) + this.setString('Tax Amt', 9) + '\r\n';
                billHtml += '--------------------------------------\r\n';
                for (var i = 0; i < bill._kotsTaxes.length; i++) {
                    billHtml += this.setString(bill._kotsTaxes[i].name, 17) + this.setAmount(bill._kotsTaxes[i].baseAmount.toString(), 9) + this.setAmount(bill._kotsTaxes[i].tax_amount.toString(), 9) + '\r\n';
                }
                billHtml += '--------------------------------------\r\n';
                for (var j = 0; j < bill.charges.detail.length > 0; j++) {
                    char += bill.charges.detail[j].amount;
                    billHtml += this.setString(bill.charges.detail[j].name, 26) + this.setAmount(bill.charges.detail[j].amount.toString(), 12) + '\r\n';
                }
                if (bill.charges.detail.length > 0)
                    billHtml += '--------------------------------------\r\n';
                if (disableRoundoff)
                    billHtml += this.setString('Grand Total:', 16) + this.setAmount(((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(2)).toString(), 19) + '\r\n';
                else {
                    billHtml += this.setString('Round Off:', 16) + this.setAmount(((((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0)) - (parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(2)).toFixed(2)).toString(), 19) + '\r\n';
                    billHtml += this.setString('Grand Total:', 16) + this.setAmount(((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0)).toString(), 19) + '\r\n';
                }
                billHtml += '--------------------------------------\r\n';
                billHtml += footer;
                billHtml += '\r\n\r\n';
                billHtml += '\r\n\r\n';
                console.log(billHtml);
                printer.print('dynamicTemplate', billHtml, 'dynamicPrint');
            },
            templateBillPrintInclusiveTaxes: function(printdata) {
                var settings = (localStorageService.get('settings'));
                var bill = printdata.bill;
                var index = printdata.index;
                var billNumber = Utils.hasSetting('reset_serial_daily', localStorageService.get('settings')) ? bill.daySerialNumber : bill.serialNumber;
                var disableRoundoff = Utils.hasSetting('disable_roundoff', localStorageService.get('settings'));
                var items = [];
                items = printdata.items;

                var totalAmount = printdata.totalAmount;
                var totalDiscount = printdata.totalDiscount;
                var totalTax = printdata.totalTax;
                var taxes = printdata.taxes;
                console.log(totalAmount, totalDiscount, totalTax);
                _.forEach(bill._kots, function(kot) {
                        _.forEach(kot.items, function(item) {
                            _.forEach(item.addOns, function(addOn) {
                              console.log(addOn);
                                items.push({ name: addOn.name, quantity: addOn.quantity, subtotal: addOn.subtotal,rate:addOn.rate,isContainAddOn:true });
                            })
                        })
                    })
                    //console.log(items);




                //console.log(paymentMethod);
                var header = Utils.getSettingValue('header_bill', localStorageService.get('settings'));
                var footer = Utils.getSettingValue('footer_bill', localStorageService.get('settings'));
                var tqty = 0;
                var subtotal = 0;
                var char = 0;
                var billHtml = '';
                if (Utils.hasSetting('settle_bill_on_print', localStorageService.get('settings'))) {

                    if (bill.rePrint && bill.rePrint.length > 2) {
                        billHtml += 'Duplicate Print';
                        billHtml += '\r\n--------------------------------------\r\n';
                    }
                } else {

                    if (bill.rePrint && bill.rePrint.length > 1) {
                        billHtml += 'Duplicate Print';
                        billHtml += '\r\n--------------------------------------\r\n';
                    }
                }

                billHtml += header;
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill.tabType == 'table' && bill._tableId != undefined) {
                    billHtml += 'Type:' + bill.tab + '   Table Number:' + bill._tableId;
                } else if (bill.tabType == 'table' && bill.tableNumber != undefined) {
                    billHtml += 'Type:' + bill.tab + 'Table Number:' + bill.tableNumber;
                } else {
                    billHtml += 'Type:' + bill.tab;
                }
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill.prefix != null)
                    billHtml += "Serial Number:" + bill.prefix + "-" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                else
                    billHtml += "Serial Number:" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                billHtml += 'Date:' + Utils.getDateFormatted(bill.rePrint[0].printTime);
                if (bill.rePrint && bill.rePrint.length > 1)
                    billHtml += '\r\nRePrint Time: ' + Utils.getDateFormatted(bill.rePrint[bill.rePrint.length - 1].printTime);
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill._customer && bill._customer.mobile != undefined) {
                   billHtml += 'Customer Detail';
                   billHtml += '\r\n--------------------------------------\r\n';
                   billHtml += 'Name: ' + bill._customer.firstname + '\r\n';
                   if(bill._customer.address1 || bill._customer.address2 || bill._customer.city || bill._customer.state || bill._customer.postCode)
                   {
                   billHtml +='Add:';
                   if(bill._customer.address1)
                   {
                   billHtml += bill._customer.address1  + ',' + '\r\n';
                   }
                   if(bill._customer.address2)
                   {
                   billHtml += bill._customer.address2+ ',' + '\r\n';
                   }
                   if(bill._customer.city)
                   {
                   billHtml += bill._customer.city;
                   }
                   if(bill._customer.state)
                   {
                   billHtml += ',' + bill._customer.state;
                   }
                   if(bill._customer.postCode)
                   {
                   billHtml +=',' + bill._customer.postCode;
                   }
                   billHtml +='\r\n'
                 }
                 
                   billHtml += 'Mobile: ' + bill._customer.mobile;
                   billHtml += '\r\n--------------------------------------\r\n';
               }


                billHtml += this.setString('@', 9) + this.setString('ITEM', 14) + this.setString('Qty', 3) + this.setAmount('Amount', 9);
                billHtml += '\r\n--------------------------------------\r\n';
                for (var i = 0; i < items.length; i++) {
                  if(items[i].isContainAddOn==true)
                  {
                    tqty += items[i].quantity;
                    subtotal += parseFloat(items[i].subtotal);
                    billHtml += this.setAmount((parseFloat(items[i].rate).toFixed(0)).toString(), 7) + ' ' + this.wordWrapString1(items[i].name, 15) + ' ' + this.setString(items[i].quantity.toString(), 2) + ' ' + this.setAmount((parseFloat(items[i].subtotal).toFixed(2)).toString(), 8) + '\r\n';
                
                  }
                  else
                  {
                     tqty += items[i].quantity;
                    subtotal += parseFloat(items[i].subtotal);
                    billHtml += this.setAmount((parseFloat(items[i].originalRate).toFixed(0)).toString(), 7) + ' ' + this.wordWrapString1(items[i].name, 15) + ' ' + this.setString(items[i].quantity.toString(), 2) + ' ' + this.setAmount((parseFloat(parseFloat(items[i].originalRate) * parseFloat(items[i].quantity)).toFixed(2)).toString(), 8) + '\r\n';
                
                  }
                }
                //console.log(parseFloat(subtotal));
                billHtml += '--------------------------------------\r\n';
                billHtml += this.setString('Discount:', 26) + this.setAmount(totalDiscount.toFixed(2), 9) + '\r\n';
                billHtml += this.setString('Total in Rs:', 13) + this.setAmount(((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0)).toString(), 22) + '\r\n';
                billHtml += '(Inclusive of All Taxes)    ========'
                billHtml += '\r\n--------------------------------------\r\n';
                billHtml += this.setString('TaxableAmount:', 26) + this.setAmount((parseFloat(subtotal)).toFixed(2), 9) + '\r\n';
                for (var i = 0; i < bill._kotsTaxes.length; i++) {
                    billHtml += this.setString(bill._kotsTaxes[i].name, 23) + this.setAmount(bill._kotsTaxes[i].tax_amount.toString(), 12) + '\r\n';
                }
                for (var j = 0; j < bill.charges.detail.length > 0; j++) {
                    char += bill.charges.detail[j].amount;
                    billHtml += this.setString(bill.charges.detail[j].name, 23) + this.setAmount(bill.charges.detail[j].amount.toString(), 12) + '\r\n';
                }
                billHtml += this.setString('Round Off:', 13) + this.setAmount(((((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0)) - (parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(2)).toFixed(2)).toString(), 22) + '\r\n';

                //billHtml += '--------------------------------------\r\n';
                // billHtml += this.setString('Grand Total:', 16) + this.setAmount(((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0)).toString(), 22) + '\r\n';

                billHtml += '--------------------------------------\r\n';
                billHtml += footer;
                billHtml += '\r\n\r\n';
                billHtml += '\r\n\r\n';
                //console.log(totalAmount, subtotal);
                //console.log(billHtml);
                printer.print('dynamicTemplate', billHtml, 'dynamicPrint');

            }

        };
    });
