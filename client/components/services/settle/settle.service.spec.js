'use strict';

describe('Service: settle', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var settle;
  beforeEach(inject(function (_settle_) {
    settle = _settle_;
  }));

  it('should do something', function () {
    expect(!!settle).toBe(true);
  });

});
