'use strict';

angular.module('posistApp')
    .factory('Settles', function ($resource) {
    // AngularJS will instantiate a singleton by calling "new" on this function
      return $resource('/api/settles/:id/:controller',
          {
            id: '@_id'
          },
          {
            Create: {method: 'POST', isArray: true},
            
            updateBank: { method: 'POST', isArray: false,params:{controller:'updateBank'}},
            destroyBank: { method: 'POST', isArray: false,params:{controller:'destroyBank'}},
            createBank: { method: 'POST', isArray: false,params:{controller:'createBank'}},
            get: {method: 'GET', isArray: true},
            getBank: { method: 'GET',params: {controller:'getBank'}, isArray: true },
            findOne: {method: 'GET'},
            update: {method: 'PUT'},
            destroy:{method:'DELETE'}
          }
      );

    });
