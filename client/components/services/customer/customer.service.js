'use strict';

angular.module('posistApp')
    .factory('Customer', function ($resource) {
        return $resource('/api/customers/:id/:controller',
            {
                id:"@_id",
                query:"@query"
            },
            {
                saveData: {method: 'POST', isArray:true},
                get: {method: 'GET', isArray:true},
                getUniqueCustomer: {method: 'GET', isArray:true,params:{controller:"getUniqueCustomer"}},
                createWithCheck:{method:'POST',  params:{
                    controller: "createWithCheck"
                },isArray:false},
                createCustomerGroupWithCheck:{method:'POST',  params:{
                    controller: "createCustomerGroupWithCheck"
                },isArray:false},
                getCustomerGroupByTenant:{method:'POST',  params:{
                    controller: "getCustomerGroupByTenant"
                },isArray:true},
                getCustomerGroups:{method:'POST',  params:{
                    controller: "getCustomerGroups"
                },isArray:true},
                
                update: {method: 'PUT'},
                search:{
                    method:"GET",
                    params:{
                        controller: "search"
                    },
                    isArray: true
                }
            }
        );
    });
