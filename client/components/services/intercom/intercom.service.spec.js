'use strict';

describe('Service: intercom', function () {

  // load the service's module
  beforeEach(module('posistApp'));

  // instantiate service
  var intercom;
  beforeEach(inject(function (_intercom_) {
    intercom = _intercom_;
  }));

  it('should do something', function () {
    expect(!!intercom).toBe(true);
  });

});
