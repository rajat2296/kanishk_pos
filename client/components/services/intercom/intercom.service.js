'use strict';

angular.module('posistApp')
  .factory('intercom', function ($http,localStorageService,$rootScope,$interval,Deployment) {
    // Service logic
    // Public API here
    if(!$rootScope.intercomEvents)
      $rootScope.intercomEvents=[];
 

    return {
      createUser: function () {
        try{
        var self = this;
        Deployment.findOne({id: localStorageService.get('deployment_id')}, function (deployment) {
            var user={
                  user_id:deployment._id,
                  name:deployment.name,
                  update_last_request_at:true,
                  new_session:true
                };
        $http.post('/api/intercom',user).success(function(data){
           var event={ event_name: 'Login',
                       user_id:localStorageService.get('deployment_id'),
                       created: Date.parse(new Date())/1000
                      }
          self.addEvent(event);
          $rootScope.isLoggedIntoIntercom=true;
        }).error(function(err){
          console.log(err);
        })
       });
      }
       catch(e){
        console.log("iException",e);
      }
    },

      sendEvent:function(event){
        /*try{
        var self=this;
        if(!$rootScope.intercomEvents||$rootScope.intercomEvents.length==0)
          return false;
        if(!$rootScope.isLoggedIntoIntercom)
          {self.createUser();
            return false;
          }
        $http.post('/api/intercom/event',{data:$rootScope.intercomEvents}).success(function(failed){
           $rootScope.intercomEvents=[];
           $rootScope.intercomevents=failed;
        }).error(function(err){
         // console.log("intercom error",err);
        })
        console.log("send event",$rootScope.intercomEvents);
      }
      catch(e){
        console.log("iException",e);
      }*/
    },

      addEvent:function(event){
       /* try{
        $rootScope.intercomEvents.push(event);
        if($rootScope.intercomEvents.length>100)
          $rootScope.intercomEvents.splice(0,1);
        console.log("events",$rootScope.intercomEvents)
        if(!$rootScope.intercomTimer){
          this.startTimer();
          $rootScope.intercomTimer=false;
        }
       }
        catch(e){
        console.log("iException",e);
       }*/
      },

      startTimer:function(){
       /* try{
         var self=this;
         $interval(function(){
           self.sendEvent();

         },10*60*1000);
       }
        catch(e){
        console.log("iException",e);
       }*/
      },

      registerEvent:function(eventName){
      /*  try{
       var event={ event_name: eventName,
          user_id:localStorageService.get('deployment_id'),
          created: Date.parse(new Date())/1000
        }
        this.addEvent(event);
       }
        catch(e){
        console.log("iException",e);
      }*/
     }
      
    };
  });
