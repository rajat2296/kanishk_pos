'use strict';

angular.module('posistApp')
  .factory('Offer', function ($resource) {
        return $resource('/api/offers/:id:controller',
            {
                id:"@_id"
            },
            {
                saveData: {method: 'POST', isArray:false},
                get: {method: 'GET', isArray:true},
                update:{method:'PUT',isArray:false},
                delete:{method:'DELETE',isArray:false},
                findByName:{method:'GET',params:{controller:'findByName'},isArray:true},
                countOffer:{method:'GET',params:{controller:'countOffer'},isArray:false},
                getOffersWithIds:{method:'GET',params:{controller:'getOffersWithIds'},isArray:false}
            }
        );
  });
