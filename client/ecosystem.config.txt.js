module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : "BrandUpdate",
      script    : "new_workers/brandUpdate.js",
    },

    // Second application
    {
      name      : "SubBrandSync",
      script    : "new_workers/subBrandSync.js"
    },
	{
		name	: "SubBrandUpdate",
		script	: "new_workers/subBrandUpdate.js"
	},
	{
		name	: "ClusterSync",
		script	: "new_workers/clusterSync.js"
	},
	{
		name	: "ClusterUpdate",
		script	: "new_workers/clusterUpdate.js"
	}
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
  deploy : {
    production : {
      user : "node",
      host : "212.83.163.1",
      ref  : "origin/master",
      repo : "git@github.com:repo.git",
      path : "/var/www/production",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.json --env production"
    },
    dev : {
      user : "node",
      host : "212.83.163.1",
      ref  : "origin/master",
      repo : "git@github.com:repo.git",
      path : "/var/www/development",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.json --env dev",
      env  : {
        NODE_ENV: "dev"
      }
    }
  }*/
}
