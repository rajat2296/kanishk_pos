'use strict';

angular.module('posistApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('dashboardNew', {
                url: '/dashboardNew',
                templateUrl: 'app/dashboardNew/dashboardNew.html',
                controller: 'DashboardCtrl',
                resolve: {
                    currentUser: ['$state', '$stateParams', 'Auth',
                        function($state, $stateParams, Auth) {
                            return Auth.getCurrentUser().$promise.then(function(user) {
                                return user;
                            });
                        }
                    ],
                    settings: ['$q', 'localStorageService', '$timeout', function($q, localStorageService, $timeout) {

                        var d = $q.defer();
                        var _settings = {};
                        $timeout(function() {
                            _settings = localStorageService.get('settings');
                            d.resolve(_settings);
                        });

                        return d.promise;

                    }],
                    superCategories: ['$q', 'Supercategory', 'localStorageService', function($q, Supercategory, localStorageService) {
                        var d = $q.defer();
                        Supercategory.get({
                            tenant_id: localStorageService.get('tenant_id')
                        }, function(scs) {
                            //console.log(scs);
                            d.resolve(scs);
                        }, function(err) {
                            growl.error('Could not fetch Super Categories!', {
                                ttl: 3000
                            });
                            d.reject();
                        });
                        return d.promise;
                    }],
                    categories: ['$q', 'Category', 'localStorageService', '$timeout', function($q, Category, localStorageService, $timeout) {
                        var d = $q.defer();
                        Category.get({
                            tenant_id: localStorageService.get('tenant_id')
                        }, function(categories) {
                            //console.log(categories);
                            d.resolve(categories);
                        }, function(err) {
                            growl.error('Could not fetch Categories!', {
                                ttl: 3000
                            });
                            d.reject();
                        });
                        return d.promise;
                    }],
                    deployments: ['$q', 'Deployment', 'localStorageService', function($q, Deployment, localStorageService) {
                        var d = $q.defer();
                        Deployment.get({
                            tenant_id: localStorageService.get('tenant_id'),
                            isMaster: false
                        }, function(deployments) {
                            //console.log(deployments);
                            d.resolve(deployments);
                        }, function(err) {
                            d.reject();
                        });
                        return d.promise;
                    }],
                    tabs: ['$q', 'Tab', 'localStorageService', function($q, Tab, localStorageService) {
                        var d = $q.defer();

                        /*Tab.getTenantTabs({tenant_id: localStorageService.get('tenant_id')}, function (tabs) {
                          //console.log(tabs);
                          d.resolve(tabs);
                        }, function (err) {
                          d.reject();
                        });*/
                        d.resolve();

                        return d.promise;
                    }]
                }
            })
            .state('dashboardNew.dash_time', {
                url: '/time',
                templateUrl: 'app/dashboardNew/dash/_time.html',
                controller: 'DashboardNewCtrl',
                parent: 'dashboardNew'
            })
            .state('dashboardNew.dash_foodCategories', {
                url: '/foodcategories',
                templateUrl: 'app/dashboardNew/dash/_foodCategories.html',
                controller: 'DashboardNewCtrl',
                parent: 'dashboardNew'
            })
            .state('dashboardNew.dash_menuItems', {
                url: '/menuitems',
                templateUrl: 'app/dashboardNew/dash/_menuItems.html',
                controller: 'DashboardNewCtrl',
                parent: 'dashboardNew'
            })
            .state('dashboardNew.dash_serviceType', {
                url: '/servicetype',
                templateUrl: 'app/dashboardNew/dash/_serviceType.html',
                controller: 'DashboardNewCtrl',
                parent: 'dashboardNew'
            })
            .state('dashboardNew.dash_staffWaiters', {
                url: '/staffwaiters',
                templateUrl: 'app/dashboardNew/dash/_staffWaiters.html',
                controller: 'DashboardNewCtrl',
                parent: 'dashboardNew'
            })
            .state('dashboardNew.dash_firstTimeCustomer', {
                url: '/customers',
                templateUrl: 'app/dashboardNew/dash/_firstTimeCustomers.html',
                controller: 'DashboardNewCtrl',
                parent: 'dashboardNew'
            })
            .state('dashboardNew.dash_tableOccupancy', {
                url: '/tableoccupancy',
                templateUrl: 'app/dashboardNew/dash/_tableOccupancy.html',
                controller: 'DashboardNewCtrl',
                parent: 'dashboardNew'
            });

    });
