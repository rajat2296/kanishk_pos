'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('partnerDetails', {
        url: '/partnerDetails',
        templateUrl: 'app/partnerDetails/partnerDetails.html',
        controller: 'partnerDetailsCtrl'
      });
  });