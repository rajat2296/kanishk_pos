'use strict';

angular.module('posistApp')
  .controller('partnerDetailsCtrl', ['$scope', '$modal', 'ThirdPartyService', 'growl', function ($scope, $modal, ThirdPartyService, growl) {
      $scope.partner={};
      $scope.create = function(partner){
        if(partner.partner_name.length>0){

          var classes=partner.class.split(',');
          partner.class=classes;

          ThirdPartyService.createPartner(partner).success(function(data){
            $scope.partner.partner_name = '';
            $scope.partner.client_id = '';
            $scope.partners.push(data);
            growl.success('Successfully created partner.', {ttl:3000});
          }).error(function(err){
            growl.error('Could not create partner.', {ttl: 3000});  
          });
        }else{
          growl.error('Please enter partner name.', {ttl: 3000});
        }
      };

      $scope.updatePartner = function(partner, index){
       
        if(partner.classes){
        var classes=partner.class;
        partner.class=classes.join(); 
       }

        var partner = angular.copy(partner);
        $modal.open({
          templateUrl: 'app/partnerDetails/partnerUpdate_modal.html',
          controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {
            $scope.partner = partner;
            $scope.cancel = function () {
              $modalInstance.dismiss('')
            };
            $scope.ok = function (partner) {
              $modalInstance.close(partner);
            }
          }],
          size: 'md'
        }).result.then(function(partner){

          if(partner.partner_name.length>0 && partner.client_id.length>0){
            console.log(partner);
            var classes=typeof partner.class=='string'?partner.class.split(','):partner.class;
            partner.class=classes;

            ThirdPartyService.updatePartner(partner).success(function(data){
              $scope.partners[index]=data;
              growl.success('Successfully updated partner.', {ttl:3000});
            }).error(function(err){
              growl.error('Could not update partner.', {ttl: 3000});  
            });
          }else{
            growl.error('Please enter partner name and client ID.', {ttl: 3000});
          }
        });
      };

      $scope.allPartners = function(){
        ThirdPartyService.allPartners().success(function(data){
          $scope.partners = data;
        }).error(function(err){
          growl.error('Could not get partners.', {ttl:3000});
        });
      };

      $scope.deletePartner = function(id, index){
        $modal.open({
          templateUrl: 'app/partnerDetails/partnerDelete_modal.html',
          controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {
            $scope.cancel = function () {
              $modalInstance.dismiss('')
            };
            $scope.ok = function () {
              $modalInstance.close('ok');
            }
          }],
          size: 'md'
        }).result.then(function(decision){
          if(decision=='ok'){
            ThirdPartyService.deletePartner(id).success(function(data){
              $scope.partners.splice(index, 1);
            }).error(function(err){
              growl.error('Could not delete this partner.', {ttl:3000});
            });
          }
        });
      };
  }]);