'use strict';

describe('Controller: TransferBillsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var TransferBillsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TransferBillsCtrl = $controller('TransferBillsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
