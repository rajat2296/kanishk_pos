'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('transferBills', {
        url: '/transferBills',
        templateUrl: 'app/transferBills/transferBills.html',
        controller: 'TransferBillsCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions,'view_past_bills')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view past bill !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          deployments: ['$q', 'Deployment', 'localStorageService', function ($q, Deployment, localStorageService) {
            var d = $q.defer();
            Deployment.get({tenant_id: localStorageService.get('tenant_id'), isMaster: false}, function (deployments) {
              //console.log(deployments);
              d.resolve(deployments);
            }, function (err) {
              d.reject();
            });
            return d.promise;
          }]
        }
      });
  });
