'use strict';

angular.module('posistApp')
  .controller('TransferBillsCtrl', ['$scope', 'Utils', 'currentUser', 'deployments', 'billResource', 'growl', '$modal', function ($scope, Utils, currentUser, deployments, billResource, growl, $modal) {

    $scope.selectedDate = {
      from: Utils.getDateFormattedDate(new Date()),
      to: Utils.getDateFormattedDate(new Date())
    };
    $scope.server = {};
    $scope.open = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };
    $scope.openTo = function ($event) {
      console.log($scope.openedTo);
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedTo = true;
    };

    $scope.limit = 10;
    $scope.skip = 0;
    $scope.deployments = deployments;
    $scope.loadMore = false;
    $scope.bills = [];
    $scope.fetchBills = function (skip, limit) {

      if ($scope.deployment_id && $scope.selectedDate.from && $scope.selectedDate.to) {
        if ((skip || skip == 0) && limit) {
          $scope.bills = [];
          $scope.skip = skip;
          $scope.limit = limit;
        }
        var query = {
          startDate: $scope.selectedDate.from,
          endDate: $scope.selectedDate.to,
          deployment_id: $scope.deployment_id
        }
        query.skip = skip == 0 || skip ? skip : $scope.skip;
        query.limit = limit ? limit : $scope.limit;

        var _deployment = _.filter($scope.deployments, {'_id': $scope.deployment_id});
        if (_deployment.length == 0) {
          growl.error('Select Deployment Date.', {ttl: 2000});
          return false;
        }
        billResource.getBillsWithPagination(query, function (bills) {
          var tempBills = angular.copy(bills);
          $scope.allBills = tempBills;
          _.forEach(tempBills, function (bill) {
            var gTotal = 0;
            var gTotalItems = 0;
            if (!bill.isVoid) {
              _.forEach(bill._kots, function (kot) {
                if (!kot.isVoid) {
                  gTotal += Utils.roundNumber((kot.totalAmount + kot.taxAmount - kot.totalDiscount), 2);
                  gTotalItems += kot.totalItems;
                }

              });
            }
            bill.gTotalAmount = Utils.roundNumber(gTotal - bill.billDiscountAmount, 0);
            bill.gTotalItems = gTotalItems;
            var billNumber = Utils.hasSetting('reset_serial_daily', _deployment[0].settings) ? bill.daySerialNumber : bill.serialNumber;
            bill.dBillNumber = billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '');
            bill.dBillNumber = ((_.has(bill, 'instance')) ? (bill.instance.preFix == undefined ? '' : bill.instance.preFix) : '') + bill.dBillNumber;
            if (bill.tabType == 'table') {
              bill.waiterDelivery = (_.has(bill._waiter, 'firstname')) ? bill._waiter.firstname : '';
            } else {
              bill.waiterDelivery = (_.has(bill._delivery, 'firstname')) ? bill._delivery.firstname : '';
            }
            $scope.bills.push(bill);
          });

          if (bills.length > 0 && bills.length == 10)
            $scope.loadMore = true;
          else $scope.loadMore = false;
          $scope.skip += $scope.limit;
        });
      } else if (!$scope.deployment_id) {
        growl.error("Please select a deployment.", {ttl: 3000});
      } else if (!$scope.selectedDate.from || !$scope.selectedDate.to) {
        growl.error("Please select a valid date range.", {ttl: 3000});
      }
    }

    $scope.transferBill = function (bill) {

      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'myModalContent.html',
        controller: 'ModalInstanceCtrl',
        size: 'md',
        resolve: {
          bill: function () {
            return bill;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        growl.success("Bill successfully transferred", {ttl: 3000});
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }
  }]).controller('ModalInstanceCtrl', function ($scope, $modalInstance, bill, billResource) {

  console.log(bill);
  $scope.bill = bill;
  $scope.server = {};

  $scope.ok = function () {
    delete bill._id;
    billResource.transferBill({url: $scope.server.to, bill: bill}, function (nBill) {
      $modalInstance.close(nBill);
    }, function (err){
      growl.error("Error transferring bill!", {ttl: 3000});
      $modalInstance.dismiss(err);
    })
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
