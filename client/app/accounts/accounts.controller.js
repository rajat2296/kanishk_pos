'use strict';

angular.module('posistApp')
    .controller('AccountsCtrl',['Auth','$location','$q','$scope','$http','$timeout','$modal','$log','growl','intercom','Utils',
    'localStorageService' ,function (Auth,$location,$q,$scope, $http,$timeout,$modal,$log,growl,intercom,Utils,localStorageService) {


      $scope.flag=0;
      $scope.created = 0;
      $scope.connectflag = 0;
      $scope.progressflag = false;
      $scope.progressvalue = 0;
      $scope.progressV = 0;
      $scope.isSale=true;
      $scope.tallypushEmpty=false;
      $scope.dateformat='dd-MMMM-yyyy';
      $scope.maxdate=new Date();
      $scope.stockbillsEmpty=false;


      $scope.form = {};
      // console.log(Auth.getCurrentUser().role);
      //console.log(Auth.getCurrentUser().deployment_id);\
      $scope.form.voucherOption="Billing";
      $scope.form.option="all";

      $scope.stockbills=[];


      $scope.tallypush=[];



      $scope.init=function(){
        console.log('init');
        if($scope.form.voucherOption=='Billing') {
          $scope.getBills();
        }
        if($scope.form.voucherOption=='Stock'){
          $scope.getStock();
        }
      }

      function getLedgers(){
        var d=$q.defer();
        $http.get('/api/ledgers?deployment_id='+Auth.getCurrentUser().deployment_id).then(function(result){
          $scope.ledgerMap=result.data.ledgerMap;
          d.resolve(true);
        },function(err){
          d.reject(false);
        });
        return d.promise;
      }

      function getLedgerName(entity){
        var ledger= _.find($scope.ledgerMap,{name:entity});
        return ((ledger && ledger.ledgerName)?ledger.ledgerName:entity);
      }

      $scope.getBills=function(){
       getLedgers().then(function(){
         console.log($scope.form.startDate);
         console.log($scope.form.endDate);

         if ($scope.form.option == 'all')         // if not filtered according to invoice no.
         {
           $scope.form.invoicefrom = -1;
           $scope.form.invoiceto = -1;
         }
         $http({
           method: 'GET',
           url: 'api/accounts/',
           params: {
             deployment_id: Auth.getCurrentUser().deployment_id,
             startDate: $scope.form.startDate,
             endDate: $scope.form.endDate,
             startBillNo: $scope.form.invoicefrom,
             endBillNo: $scope.form.invoiceto
           }
         }).success(function (data) {
           $scope.tallypush = data;
           console.log($scope.tallypush);
           if (Utils.hasSetting('split_sales_in_tally', localStorageService.get('settings'))) {
             if (data.length == 0) {
               growl.error("No Data found in your selection", {ttl: 3000});
               $scope.form.invoicefrom = 0;
               $scope.form.invoiceto = 0;
             }
             else {
               $http.get('/api/deployments/' + Auth.getCurrentUser().deployment_id).success(function (deployment) {
                 $scope.outletName = deployment.name;
                 addLedgerDaywise(data)
               }).error(function (err) {
                 growl.error("Could not get Deployment", {ttl: 3000});
                 return false;
               })
             }
           }
           else {
             $scope.addLedger();
           }
         }).error(function (data) {
           growl.error("Could not get bills", {ttl: 4000});
           console.log(data);
         });
       },function(err){
         growl.error("Please check your internet connection",{ttl:3000});
       });
      };

      $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
      };

      $scope.open2 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.endDateOpened = true;
      };


      $scope.connect = function () {

        var message = {
          data: "<ENVELOPE></ENVELOPE>",
          url: $scope.hosturl
        };


        var url= "http://localhost:65505/service1.svc/ExcuteTallyCommand";

        $http({
          method:'POST',
          url:url,
          data:{xml:"<ENVELOPE><ENVELOPE>",url:$scope.hosturl}
        }).success(function(data){
          if((data.d).toString().indexOf('<RESPONSE>Unknown Request, cannot be processed</RESPONSE>')==0)
            $scope.connectflag=1;
          else
            $scope.connectflag=-1;
        }).error(function(error){
          console.log(error);
          $scope.connectflag=-1;
        });

        //   makeCorsRequest();
      };

      $scope.disconnect=function(){
        $scope.connectflag=0;
      };

      $scope.getStock = function () {              //used to get stock from stock transaction schema
        $http({
          method: 'GET',
          url: 'api/accounts/getStock',
          params:{
            deployment_id:Auth.getCurrentUser().deployment_id,
            startDate:$scope.form.startDate,
            endDate:$scope.form.endDate
          }
        }).success(function (data) {
          $scope.stockbills = data;
          $scope.pushStockBills();
          console.log($scope.stockbills);
        }).error(function (data) {
          console.log(data);
        });
      };

      $scope.addLedger = function () {

        try {

          $scope.tallypushEmpty = false;                     //setting tallypushEnmpty  flag to false
          // resetting variables
          $scope.created = 0;

          console.log("out");

          var ledgerxml = "";
          var voucherxml = "";
          var vouchercount = 0;

          // if tallypush is empty
          console.log($scope.tallypush.length);

          if ($scope.tallypush.length == 0) {

            $scope.flag = 0;                                 //to avoid NAN error

            var modalInstance = $modal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'tallypushEmptyModal.html',
              controller: 'tallypushEmptyModalCtrl',
              size: 'lg'
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
            }, function () {
              $log.info('Modal dismissed at: ' + new Date());
            });

            $scope.toggleAnimation = function () {
              $scope.animationsEnabled = !$scope.animationsEnabled;
            };
            $scope.tallypushEmpty = true;
            $scope.form.invoicefrom = 0;
            $scope.form.invoiceto = 0;

          }

          if ($scope.tallypushEmpty == false) {

            $scope.created = 0;
            $scope.flag = 0;
            $scope.progressflag = true;
            //    pushVouchers($scope.tallypush.length);

            for (var i = 0; i < $scope.tallypush.length;) {

              ledgerxml = "";
              voucherxml = "";
              vouchercount = 0;

              while (vouchercount < 1 && i < $scope.tallypush.length) {

                try {
                  console.log("bakk");

                  vouchercount++;

                  ledgerxml += "<ENVELOPE>" +
                    "<HEADER>" +
                    "<VERSION>1</VERSION>" +
                    "<TALLYREQUEST>Import</TALLYREQUEST>" +
                    "<TYPE>Data</TYPE>" +
                    "<ID>All Masters</ID>" +
                    "</HEADER>" +
                    "<BODY>" +
                    "<DESC>" +
                    "<STATICVARIABLES>" +
                    "<IMPORTDUPS>@@DUPCOMBINE</IMPORTDUPS>" +
                    "</STATICVARIABLES>" +
                    "</DESC>" +
                    "<DATA>" +
                    "<TALLYMESSAGE>";

                  ledgerxml += "<LEDGER NAME='Sales' Action = 'Create'>" +
                    " <NAME>Sales</NAME>" +
                    " <PARENT>Sales Accounts</PARENT>" +
                    "</LEDGER>";

                  ledgerxml += "<LEDGER NAME='Credit Card' Action = 'Create'>" +
                    " <NAME>Credit Card</NAME>" +
                    " <PARENT>Bank Accounts</PARENT>" +
                    " </LEDGER>";

                  ledgerxml += "<LEDGER NAME='Debit Card' Action = 'Create'>" +
                    " <NAME>Debit Card</NAME>" +
                    " <PARENT>Bank Accounts</PARENT>" +
                    "</LEDGER>";

                  ledgerxml += "<LEDGER NAME='Other' Action = 'Create'>" +
                    " <NAME>Other</NAME>" +
                    " <PARENT>Bank Accounts</PARENT>" +
                    "</LEDGER>";

                  ledgerxml += "<LEDGER NAME='Online' Action = 'Create'>" +
                    " <NAME>Online</NAME>" +
                    " <PARENT>Bank Accounts</PARENT>" +
                    "</LEDGER>";

                  ledgerxml += "<LEDGER NAME='Coupon' Action = 'Create'>" +
                    " <NAME>Coupon</NAME>" +
                    " <PARENT>Bank Accounts</PARENT>" +
                    "</LEDGER>";



                  var date = "";                                           //converting date to required string
                  var created = new Date($scope.tallypush[i].created);
                  date += created.getFullYear();
                  var month = created.getMonth() + 1;
                  if (month < 10) {
                    date += 0;
                    date += month;
                  }
                  else {
                    date += month;
                  }
                  var day = created.getDate();
                  if (day < 10) {
                    date += 0;
                    date += day;
                  }
                  else {
                    date += day;
                  }

                  //console.log("Sales",Utils.roundNumber(Utils.roundNumber($scope.tallypush[i].aggregation.netAmount,2)-Utils.roundNumber($scope.tallypush[i].aggregation.totalTax,2),2));

                  voucherxml += "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Import</TALLYREQUEST>   <TYPE>Data</TYPE>   <ID>Vouchers</ID> </HEADER> <BODY><DESC></DESC><DATA><TALLYMESSAGE>";
                  voucherxml += "<VOUCHER><DATE>" +date+ "</DATE><VOUCHERTYPENAME>Receipt</VOUCHERTYPENAME>" + "<NARRATION>BILL NO." + $scope.tallypush[i].serialNumber + "</NARRATION>" + "<VOUCHERNUMBER>"+$scope.tallypush[i].serialNumber+"</VOUCHERNUMBER>";
                  var categories={};
                  var sales=0;
                  var allTaxes={};
                  var charges={};
                  var totalTax=0;
                  var totalCharge=0;
                  var discount=0;

                  if(Utils.hasSetting('split_sales_in_tally',localStorageService.get('settings')))
                  discount=$scope.tallypush[i].billDiscountAmount?parseFloat($scope.tallypush[i].billDiscountAmount):0;

                 if(Utils.hasSetting('split_sales_in_tally',localStorageService.get('settings'))){
                  _.forEach($scope.tallypush[i]._kots,function(kot){
                    _.forEach(kot.items,function(item){
                      if(item.category.section){
                      var cat=item.category.section.name;
                      if(!categories[cat])
                        categories[cat]=0;
                      categories[cat]+=item.subtotal;
                      sales+=item.subtotal;
                     }
                    });

                     if(kot.totalDiscount) discount+=kot.totalDiscount;
                  });
                 }
                 else{
                  _.forEach($scope.tallypush[i]._kots,function(kot){
                    sales+=kot.totalAmount-kot.totalDiscount;
                  })
                  if($scope.tallypush[i].billDiscountAmount)
                    sales-=$scope.tallypush[i].billDiscountAmount;
                 }

                  _.forEach($scope.tallypush[i]._kots,function(kot){
                   // sales+=kot.totalAmount-kot.totalDiscount;
                    _.forEach(kot.taxes,function(tax){
                      if(!allTaxes[tax.name])
                       allTaxes[tax.name]=0;
                       allTaxes[tax.name]+=parseFloat(tax.tax_amount);
                       //totalTax+=parseFloat(tax.tax_amount);
                    })
                  });

                 /* if($scope.tallypush[i].billDiscountAmount)
                   sales-=$scope.tallypush[i].billDiscountAmount;*/

                   if($scope.tallypush[i].charges && $scope.tallypush[i].charges.detail){
                  _.forEach($scope.tallypush[i].charges.detail,function(ch){
                    charges[ch.name]=parseFloat(ch.amount);
                    //totalCharge+=parseFloat(ch.amount);
                   });
                  }

                  for(key in allTaxes){
                    if(allTaxes.hasOwnProperty(key)){
                      allTaxes[key]=Utils.roundNumber(allTaxes[key],2);
                      totalTax+=allTaxes[key];
                    }
                  }

                  for(key in charges){
                    if(charges.hasOwnProperty(key)){
                      charges[key]=Utils.roundNumber(charges[key],2);
                      totalCharge+=charges[key];
                    }
                  }

                  sales=Utils.roundNumber(sales,2);

                  var debit=0;
                  var credit=0;
                  var other=0;
                  var coupon=0;
                  var online=0;
                  var cash=0;

                  _.forEach($scope.tallypush[i].payments.cash,function(c){
                    cash+=parseFloat(c);
                  });
                  _.forEach($scope.tallypush[i].payments.cards,function(card){
                    //console.log("cardtype",card.cardType);
                    if(card.cardType=='DebitCard')
                      debit+=parseFloat(card.totalAmount);
                    if(card.cardType=='CreditCard')
                      credit+=parseFloat(card.totalAmount);
                    if(card.cardType=='Other')
                      other+=parseFloat(card.totalAmount);
                    if(card.cardType=='Online')
                      online+=parseFloat(card.totalAmount);
                    if(card.cardType=='Coupon')
                      coupon+=parseFloat(card.totalAmount);
                  })

                  var billAmount=sales+totalCharge+totalTax-discount;
                  var payments=(cash+debit+credit+online+coupon+other);
                  var roundOff=Utils.roundNumber(payments-billAmount,2);

                  console.log("details",sales,allTaxes,charges,totalTax,totalCharge,roundOff,payments,discount);

                  /*voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                    "<LEDGERNAME>Sales</LEDGERNAME>" +
                    "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                    "<AMOUNT>" +sales+ "</AMOUNT>"+
                   /* "<CATEGORYALLOCATIONS.LIST>" +
                    "<CATEGORY>Outlets</CATEGORY>"+
                    "<ISDEEMEDPOSITIVE>No</ISDEEMEDPOSITIVE>"+
                    "<COSTCENTREALLOCATIONS.LIST>"+
                     "<NAME>KIT NAGAR</NAME>"+
                     "<AMOUNT>"+sales+"</AMOUNT>"+
                     "</COSTCENTREALLOCATIONS.LIST></CATEGORYALLOCATIONS.LIST>"+
                    "</ALLLEDGERENTRIES.LIST>";*/
                 if(Utils.hasSetting('split_sales_in_tally',localStorageService.get('settings'))){
                  for(var key in categories) {
                    if (categories.hasOwnProperty(key)) {
                      ledgerxml += "<LEDGER NAME=" + "\'" + key + "\'" + " Action = 'Create'>" +
                        " <NAME>" + key + "</NAME>" +
                        " <PARENT>Sales</PARENT>" +
                        "</LEDGER>";

                      voucherxml+="<ALLLEDGERENTRIES.LIST>" +
                        "<LEDGERNAME>" + key + "</LEDGERNAME>"+
                        "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                        "<AMOUNT>" + categories[key] + "</AMOUNT>" +
                        "</ALLLEDGERENTRIES.LIST>";
                    }
                  }
                 }
                 else{
                  voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                    "<LEDGERNAME>Sales</LEDGERNAME>" +
                    "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                    "<AMOUNT>" +sales+ "</AMOUNT></ALLLEDGERENTRIES.LIST>";
                 }

                  for(var key in allTaxes) {
                    if(allTaxes.hasOwnProperty(key)){
                      ledgerxml += "<LEDGER NAME=" + "\'" +key+ "\'" + " Action = 'Create'>" +
                        " <NAME>" + key + "</NAME>" +
                        " <PARENT>Duties &amp; Taxes</PARENT>" +
                        "</LEDGER>";

                      voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                        "<LEDGERNAME>" + key + "</LEDGERNAME>";

                      voucherxml += "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                        "<AMOUNT>" + allTaxes[key] + "</AMOUNT>" +
                        "</ALLLEDGERENTRIES.LIST>";
                    }
                  }

                 for(var key in charges) {
                   if (charges.hasOwnProperty(key)) {
                     ledgerxml += "<LEDGER NAME=" + "\'" + key + "\'" + " Action = 'Create'>" +
                       " <NAME>" + key + "</NAME>" +
                       " <PARENT>Duties &amp; Taxes</PARENT>" +
                       "</LEDGER>";

                     voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                       "<LEDGERNAME>" + key + "</LEDGERNAME>";

                     voucherxml += "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                       "<AMOUNT>" + charges[key] + "</AMOUNT>" +
                       "</ALLLEDGERENTRIES.LIST>";
                   }
                 }

                  ledgerxml += "<LEDGER name='roundOff' action='create'><NAME>roundOff</NAME><PARENT>Misc. Expenses (ASSET)</PARENT></LEDGER>"+
                                "<LEDGER name='Discount' action='create'><NAME>Discount</NAME><PARENT>Misc. Expenses (ASSET)</PARENT></LEDGER>"

                  ledgerxml += "</TALLYMESSAGE>" +
                    "</DATA>" +
                    "</BODY>" +
                    "</ENVELOPE>";


                  voucherxml += "<ALLLEDGERENTRIES.LIST><LEDGERNAME>roundOff</LEDGERNAME>"+
                                "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>"+
                                "<AMOUNT>" + roundOff + "</AMOUNT>"+
                                "</ALLLEDGERENTRIES.LIST>";

                  voucherxml+="<ALLLEDGERENTRIES.LIST><LEDGERNAME>Discount</LEDGERNAME>"+
                              "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>"+
                              "<AMOUNT>-" + discount + "</AMOUNT>" +
                              "</ALLLEDGERENTRIES.LIST>";


                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                      "<LEDGERNAME>Debit Card</LEDGERNAME>";
                    voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + debit + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                      "<LEDGERNAME>Credit Card</LEDGERNAME>";
                    voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + credit + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                      "<LEDGERNAME>Other</LEDGERNAME>";
                    voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + other + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                      "<LEDGERNAME>Cash</LEDGERNAME>";
                    voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + cash + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                        voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                      "<LEDGERNAME>Online</LEDGERNAME>";
                    voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + online + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                        voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                      "<LEDGERNAME>Coupon</LEDGERNAME>";
                    voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + coupon + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>"

                  voucherxml +=
                    "</VOUCHER>" +
                    "</TALLYMESSAGE>" +
                    "</DATA>" +
                    "</BODY>" +
                    "</ENVELOPE>";

                  //console.log("ledger");
                  //console.log(ledgerxml+voucherxml);
                  i++;                              //increment
                }
                catch (e) {
                  console.log("Exception", e);
                  i++;
                }
              }

              var message = {
                url: $scope.hosturl,
                data: ledgerxml+voucherxml
              };

              $scope.created += vouchercount;

             // console.log('vouchercount:' + vouchercount);
              var proxyurl = "//localhost:65505/service1.svc/ExcuteTallyCommand";
             // console.log(message.data);
              $http({
                method: 'POST',
                url: proxyurl,
                data: {xml: message.data, url: $scope.hosturl}
              })
                .success(function (data) {
                  console.log(data);
                 /* if(data.d.indexOf("<STATUS>0</STATUS>")!=-1)
                   console.log("error",data,message.data);
                  if(data.d.indexOf("<STATUS>1</STATUS>")!=-1)
                  console.log("statusNumber");*/
                  $scope.progressvalue = Math.min(((($scope.progressV += 1) * 100) / $scope.tallypush.length), 100);
                  if ($scope.progressvalue >= 100) {
                    $timeout(function () {
                      $scope.progressflag = false;
                      $scope.flag = 1;
                      $scope.progressvalue = 0;
                      $scope.progressV = 0;
                      $scope.form.invoicefrom = 0;
                      $scope.form.invoiceto = 0;
                      console.log("hiiiiii");
                    }, 1000);
                  }
                })
                .error(function (data) {
                  console.log(data);
                  $scope.progressflag = false;
                  console.log("error in connection with Tally");
                  growl.error('Lost Connection with Tally! Try Again', {ttl: 4000});
                  $scope.form.invoicefrom = 0;
                  $scope.form.invoiceto = 0;
                  $scope.progressV = 0;
                  $scope.connectflag = -1;
                });
            }
          }
        }
        catch(e){
          $scope.progressflag = false;
          console.log("Exception",e);
          growl.error("An unexpected error has occurred", {ttl: 4000});
          $scope.form.invoicefrom = 0;
          $scope.form.invoiceto = 0;
          $scope.progressV = 0;
          $scope.connectflag = -1;
        }
      };


      $scope.pushStockBills=function() {

        try {

          $scope.stockbillsEmpty = false;
          $scope.created = 0;
          $scope.flag = 0;


          var ledgerxml = "";
          var voucherxml = "";
          var vouchercount = 0;


          if ($scope.stockbills.length == 0) {

            $scope.flag = 0;                                 //to avoid NAN error

            var modalInstance = $modal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'tallypushEmptyModal.html',
              controller: 'tallypushEmptyModalCtrl',
              size: 'lg'
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
            }, function () {
              $log.info('Modal dismissed at: ' + new Date());
            });

            $scope.toggleAnimation = function () {
              $scope.animationsEnabled = !$scope.animationsEnabled;
            };
            $scope.stockbillsEmpty = true;

          }

          if ($scope.stockbillsEmpty == false) {

            $scope.progressflag = true;

            for (var i = 0; i < $scope.stockbills.length;) {

              ledgerxml = "";
              voucherxml = "";
              vouchercount = 0;
              //  $scope.isSale = true;

              while (vouchercount < 1 && i < $scope.stockbills.length) {

                try {
                  console.log("bakk");
                  vouchercount++;

                  ledgerxml += "<ENVELOPE>" +
                    "<HEADER>" +
                    "<VERSION>1</VERSION>" +
                    "<TALLYREQUEST>Import</TALLYREQUEST>" +
                    "<TYPE>Data</TYPE>" +
                    "<ID>All Masters</ID>" +
                    "</HEADER>" +
                    "<BODY>" +
                    "<DESC>" +
                    "<STATICVARIABLES>" +
                    "<IMPORTDUPS>@@DUPCOMBINE</IMPORTDUPS>" +
                    "</STATICVARIABLES>" +
                    "</DESC>" +
                    "<DATA>" +
                    "<TALLYMESSAGE>";

                  //purchase voucher
                  ledgerxml += "<LEDGER NAME='Purchase' Action = 'Create'>" +
                    " <NAME>Purchase</NAME>" +
                    " <PARENT>Purchase Accounts</PARENT>" +
                    "</LEDGER>";


                  ledgerxml += "<LEDGER NAME='service charge' Action = 'Create'>" +
                    " <NAME>service charge</NAME>" +
                    " <PARENT>Duties &amp; Taxes</PARENT>" +
                    "</LEDGER>";

                  ledgerxml += "<LEDGER NAME='Vat' Action = 'Create'>" +
                    " <NAME>Vat</NAME>" +
                    " <PARENT>Duties &amp; Taxes</PARENT>" +
                    "</LEDGER>";

                  ledgerxml += "<LEDGER NAME='cartage' Action = 'Create'>" +
                    " <NAME>cartage</NAME>" +
                    " <PARENT>Indirect Expenses</PARENT>" +
                    "</LEDGER>";

                  var date = "";
                  var created = new Date($scope.stockbills[i].created);
                  date += created.getFullYear();
                  var month = created.getMonth() + 1;
                  if (month < 10) {
                    date += 0;
                    date += month;
                  }
                  else {
                    date += month;
                  }
                  var day = created.getDate();
                  if (day < 10) {
                    date += 0;
                    date += day;
                  }
                  else {
                    date += day;
                  }

                  ledgerxml += "</TALLYMESSAGE>" +
                    "</DATA>" +
                    "</BODY>" +
                    "</ENVELOPE>";

                  if ($scope.stockbills[i].transactionType == "1")                    //if purchase stock voucher
                  {
                    var narration = "Stock Purchase of ";
                    for (var j = 0; j < $scope.stockbills[i]._items.length; j++)
                      narration += $scope.stockbills[i]._items[j].itemName + " @" + $scope.stockbills[i]._items[j].price + " quantity= " + $scope.stockbills[i]._items[j].qty + " , ";

                    voucherxml += "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Import</TALLYREQUEST>   <TYPE>Data</TYPE>   <ID>Vouchers</ID> </HEADER> <BODY><DESC></DESC><DATA><TALLYMESSAGE>";
                    voucherxml += "<VOUCHER><DATE>" + date + "</DATE><VOUCHERTYPENAME>Payment</VOUCHERTYPENAME>" + "<NARRATION>BILL NO. SE" + $scope.stockbills[i].transactionNumber + "</NARRATION>" + "<VOUCHERNUMBER>1</VOUCHERNUMBER>";

                    var subTotal = 0;
                    var totalAmount = 0;

                    for (var j = 0; j < $scope.stockbills[i]._items.length; j++) {              //calculation for  multiple items

                      subTotal += $scope.stockbills[i]._items[j].subTotal;
                      totalAmount += $scope.stockbills[i]._items[j].totalAmount;

                    }

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                //Purchase amount
                      "<LEDGERNAME>Purchase</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>";

                    var disc = 0;
                    if ($scope.stockbills[i].discountType == "percent")
                      disc = ($scope.stockbills[i].discount * subTotal) / 100;
                    else
                      disc = $scope.stockbills[i].discount;

                    voucherxml += "<AMOUNT>-" + (subTotal - disc) + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                         //vat calculation
                      "<LEDGERNAME>vat</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + (totalAmount - subTotal) + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                           //cartage
                      "<LEDGERNAME>cartage</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + $scope.stockbills[i].cartage + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                             //Cash
                      "<LEDGERNAME>Cash</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>" + (totalAmount - disc + $scope.stockbills[i].cartage) + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";
                  }


                  if ($scope.stockbills[i].transactionType == "2")                         //if stock sales voucher
                  {
                    var narration = " Stock Sale of ";
                    for (var j = 0; j < $scope.stockbills[i]._items.length; j++)
                      narration += $scope.stockbills[i]._items[j].itemName + " @" + $scope.stockbills[i]._items[j].price + "  quantity= " + $scope.stockbills[i]._items[j].qty + " , ";

                    voucherxml += "<ENVELOPE> <HEADER><VERSION>1</VERSION>    <TALLYREQUEST>Import</TALLYREQUEST>   <TYPE>Data</TYPE>   <ID>Vouchers</ID> </HEADER> <BODY><DESC></DESC><DATA><TALLYMESSAGE>";
                    voucherxml += "<VOUCHER><DATE>" + date + "</DATE><VOUCHERTYPENAME>Receipt</VOUCHERTYPENAME>" + "<NARRATION>BILL NO. SS" + $scope.stockbills[i].transactionNumber + "</NARRATION>" + "<VOUCHERNUMBER>1</VOUCHERNUMBER>";

                    var total = 0;

                    for (var j = 0; j < $scope.stockbills[i]._items.length; j++) {              //calculation for  multiple items
                      total += $scope.stockbills[i]._items[j].Total;
                    }

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                //Purchase amount
                      "<LEDGERNAME>Sales</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>";

                    var disc = 0;
                    if ($scope.stockbills[i].discountType == "percent")
                      disc = ($scope.stockbills[i].discount * total) / 100;
                    else
                      disc = $scope.stockbills[i].discount;

                    voucherxml += "<AMOUNT>" + (total - disc + $scope.stockbills[i].payment) + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                         //vat calculation
                      "<LEDGERNAME>vat</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>";
                    var vat = 0;
                    if ($scope.stockbills[i].vatType = "percent")
                      vat = (total * $scope.stockbills[i].vat) / 100;
                    else
                      vat = $scope.stockbills[i].vat;

                    voucherxml += "<AMOUNT>" + vat + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";


                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                         //service charge calculation
                      "<LEDGERNAME>service charge</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>";
                    var service = 0;
                    if ($scope.stockbills[i].serviceChargeType = "percent")
                      service = (total * $scope.stockbills[i].serviceCharge) / 100;
                    else
                      vat = $scope.stockbills[i].serviceCharge;

                    voucherxml += "<AMOUNT>" + service + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";


                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                           //cartage
                      "<LEDGERNAME>cartage</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>" + $scope.stockbills[i].cartage + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";

                    voucherxml += "<ALLLEDGERENTRIES.LIST>" +                             //Cash
                      "<LEDGERNAME>Cash</LEDGERNAME>" +
                      "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
                      "<AMOUNT>-" + (total - disc + service + vat + $scope.stockbills[i].payment + $scope.stockbills[i].cartage) + "</AMOUNT>" +
                      "</ALLLEDGERENTRIES.LIST>";
                  }

                  voucherxml += "</VOUCHER>" +          //end of voucher
                    "</TALLYMESSAGE>" +
                    "</DATA>" +
                    "</BODY>" +
                    "</ENVELOPE>";

                  console.log("ledger");
                  //console.log(ledgerxml);
                  i++;                              //increment
                }
                catch (e) {
                  console.log("exception", e);
                  i++;
                }
              }


              var message = {
                url: $scope.hosturl,
                data: ledgerxml + voucherxml
              };

              $scope.created += vouchercount;

              console.log('vouchercount:' + vouchercount);
              var proxyurl = "http://localhost:65505/service1.svc/ExcuteTallyCommand";

              /*  $http({
               method:'POST',
               url:'api/accounts/',
               data:message
               })*/

              $http({
                method: 'POST',
                url: proxyurl,
                data: {xml: message.data, url: $scope.hosturl}
              })
                .success(function (data) {
                  console.log(data);
                  $scope.progressvalue = Math.min(((($scope.progressV += 1) * 100) / $scope.stockbills.length), 100);
                  if ($scope.progressvalue >= 100) {
                    $timeout(function () {
                      $scope.progressflag = false;
                      $scope.flag = 1;
                      $scope.progressvalue = 0;
                      $scope.progressV = 0;
                      console.log("hiiiiii");
                      $scope.form.invoicefrom = 0;
                      $scope.form.invoiceto = 0;
                    }, 1000);
                  }
                })
                .error(function (data) {
                  console.log(data);
                  $scope.progressflag = false;
                  console.log("error in connection with Tally");
                  growl.error('Lost Connection with Tally! Try Again', {ttl: 4000});
                  $scope.form.invoicefrom = 0;
                  $scope.form.invoiceto = 0;
                  $scope.progressV = 0;
                  $scope.connectflag = -1;
                });
            }
          }
        }
        catch (e) {
          $scope.progressflag = false;
          console.log("Exception", e);
          growl.error('An unexpected error has occurred', {ttl: 4000});
          $scope.form.invoicefrom = 0;
          $scope.form.invoiceto = 0;
          $scope.progressV = 0;
          $scope.connectflag = -1;
        }
      }

      $scope.$watch('form.voucherOption', function () {
        $scope.flag = 0;
        $scope.created = 0;
        //   $scope.form.option='all';
      });
      function getDayStart(date){
        var settings=localStorageService.get('settings');
        if(!date)
          var result=new Date();
        else
          var result=new Date(date);
        var hrs=result.getHours();
        var min=result.getMinutes();
        var cutOffHour=new Date(Utils.getSettingValue('day_sale_cutoff',settings)).getHours();
        var cutOffMin=new Date(Utils.getSettingValue('day_sale_cutoff',settings)).getMinutes();
        result.setHours(cutOffHour);
        result.setMinutes(cutOffMin);
        result.setSeconds(0);
        if((hrs*60+min)<(cutOffHour*60+cutOffMin))
          result.setDate(result.getDate()-1);
        return new Date(result);
      }

      function addLedgerDaywise(bills){
        $scope.progressflag=true;

        _.forEach(bills,function(bill){
           bill.cutOffDate=Utils.getDateFormatted(getDayStart(new Date(bill._created)));
         })
        var tallypush= _.groupBy(bills,'cutOffDate');
        $scope.tallypush={length:Object.keys(tallypush).length};
        console.log("tallypush",$scope.tallypush.length);
         var vouchercount=0;
        $scope.created=0;
        _.forEach(tallypush,function(dateBills,key){
           var ledgerxml='',voucherxml='';
           vouchercount++;
            var categories={};
            var sales=0;
            var allTaxes={};
            var charges={};
            var totalTax=0;
            var totalCharge=0;
            var discount=0;
            var debit=0;
            var credit=0;
            var other=0;
            var coupon=0;
            var online=0;
            var cash=0;

          ledgerxml += "<ENVELOPE>" +
            "<HEADER>" +
            "<VERSION>1</VERSION>" +
            "<TALLYREQUEST>Import</TALLYREQUEST>" +
            "<TYPE>Data</TYPE>" +
            "<ID>All Masters</ID>" +
            "</HEADER>" +
            "<BODY>" +
            "<DESC>" +
            "<STATICVARIABLES>" +
            "<IMPORTDUPS>@@DUPCOMBINE</IMPORTDUPS>" +
            "</STATICVARIABLES>" +
            "</DESC>" +
            "<DATA>" +
            "<TALLYMESSAGE>";

          ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName('sales') + "\'" + " Action = 'Create'>" +
            " <NAME>"+getLedgerName('sales')+"</NAME>" +
            " <PARENT>Sales Accounts</PARENT>" +
            "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
            "</LEDGER>";

          ledgerxml+="<LEDGER NAME=" + "\'" + getLedgerName('cash') + "\'" + " Action = 'Create'>" +
            "<NAME>"+getLedgerName('cash')+"</NAME>" +
            " <PARENT>Cash-in-hand</PARENT>" +
            "</LEDGER>";

          ledgerxml+="<GROUP NAME=" + "\'" + getLedgerName('sales') + "\'" + " Action = 'Create'>" +
            "<NAME>"+getLedgerName('sales')+"</NAME>" +
            " <PARENT>Sales Accounts</PARENT>" +
            "</GROUP>";

          ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName('creditcard') + "\'" + " Action = 'Create'>" +
            " <NAME>"+getLedgerName('creditcard')+"</NAME>" +
            " <PARENT>Bank Accounts</PARENT>" +
            "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
            " </LEDGER>";

          ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName('debitcard') + "\'" + " Action = 'Create'>" +
            " <NAME>"+getLedgerName('debitcard')+"</NAME>" +
            " <PARENT>Bank Accounts</PARENT>" +
            "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
            " </LEDGER>";

          ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName('other') + "\'" + " Action = 'Create'>" +
            " <NAME>"+getLedgerName('other')+"</NAME>" +
            " <PARENT>Bank Accounts</PARENT>" +
            "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
            " </LEDGER>";

          ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName('online') + "\'" + " Action = 'Create'>" +
            " <NAME>"+getLedgerName('online')+"</NAME>" +
            " <PARENT>Bank Accounts</PARENT>" +
            "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
            " </LEDGER>";

          ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName('coupon') + "\'" + " Action = 'Create'>" +
            " <NAME>"+getLedgerName('coupon')+"</NAME>" +
            " <PARENT>Bank Accounts</PARENT>" +
            "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
            " </LEDGER>";

            var date = "";                                           //converting date to required string
            var created = new Date(key);
            date += created.getFullYear();
            var month = created.getMonth() + 1;
            if (month < 10){
             date += 0;
             date += month;
            }
            else {
             date += month;
            }
            var day = created.getDate();
            if (day < 10) {
              date += 0;
              date += day;
            }
            else {
              date += day;
            }


          voucherxml += "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Import</TALLYREQUEST>   <TYPE>Data</TYPE>   <ID>Vouchers</ID> </HEADER> <BODY><DESC></DESC><DATA><TALLYMESSAGE>";
          voucherxml += "<VOUCHER><DATE>" +'20170131'+ "</DATE><VOUCHERTYPENAME>Receipt</VOUCHERTYPENAME>" + "<NARRATION>Voucher Date "+ key+"</NARRATION>" + "<VOUCHERNUMBER>1</VOUCHERNUMBER>";
          voucherxml+= "<GUID>"+key+"</GUID>";

          _.forEach(dateBills,function(bill){


            //console.log("Sales",Utils.roundNumber(Utils.roundNumber($scope.tallypush[i].aggregation.netAmount,2)-Utils.roundNumber($scope.tallypush[i].aggregation.totalTax,2),2));
            discount+=bill.billDiscountAmount?parseFloat(Utils.roundNumber(bill.billDiscountAmount,2)):0;

              _.forEach(bill._kots,function(kot){
                _.forEach(kot.items,function(item){
                    var cat=(item.category.section && item.category.section.name)?item.category.section.name:'NA';
                    console.log("category",cat,item.subtotal);
                    if(!categories[cat])
                      categories[cat]=0;
                    categories[cat]+=item.subtotal;
                    sales+=item.subtotal;
                    if(item.discounts && item.discounts.length>0) {
                      categories[cat]-=item.discounts[0].discountAmount;
                      sales-=item.discounts[0].discountAmount;
                  }
                  categories[cat]=Utils.roundNumber(categories[cat],2);
                  sales=Utils.roundNumber(sales,2);
                });

                _.forEach(kot.taxes,function(tax){
                  if(!allTaxes[tax.name])
                    allTaxes[tax.name]=0;
                  allTaxes[tax.name]+=parseFloat(tax.tax_amount)
                  totalTax+=parseFloat(tax.tax_amount);
                  allTaxes[tax.name]=Utils.roundNumber(allTaxes[tax.name],2);
                  totalTax=Utils.roundNumber(totalTax,2);
                });
              });

            if(bill.charges && bill.charges.detail){
              _.forEach(bill.charges.detail,function(ch){
                if(!charges[ch.name])
                  charges[ch.name]=0;
                charges[ch.name]+=parseFloat(ch.amount);
                totalCharge+=parseFloat(ch.amount);
                charges[ch.name]=Utils.roundNumber(charges[ch.name],2);
                totalCharge=Utils.roundNumber(totalCharge,2);
              });
            }

            //sales=Utils.roundNumber(sales,2);

            _.forEach(bill.payments.cash,function(c){
              cash+=parseFloat(c);
            });
            _.forEach(bill.payments.cards,function(card){
              //console.log("cardtype",card.cardType);
              if(card.cardType=='DebitCard')
                debit+=parseFloat(card.totalAmount);
              if(card.cardType=='CreditCard')
                credit+=parseFloat(card.totalAmount);
              if(card.cardType=='Other')
                other+=parseFloat(card.totalAmount);
              if(card.cardType=='Online')
                online+=parseFloat(card.totalAmount);
              if(card.cardType=='Coupon')
                coupon+=parseFloat(card.totalAmount);
            })
          })

         // totalTax=Utils.roundNumber(totalTax,2);


          var billAmount=sales+totalCharge+totalTax-discount;
          var payments=(cash+debit+credit+online+coupon+other);
          var roundOff=Utils.roundNumber(payments-billAmount,2);
          console.log("details",sales,allTaxes,charges,totalTax,totalCharge,roundOff,payments,discount);

          for(var key in categories) {
            if (categories.hasOwnProperty(key)) {
              ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName(key) + "\'" + " Action = 'Create'>" +
                " <NAME>" + getLedgerName(key) + "</NAME>" +
                " <PARENT>Sales Accounts</PARENT>" +
                "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
                "</LEDGER>";


              voucherxml+="<ALLLEDGERENTRIES.LIST>" +
                "<LEDGERNAME>" + getLedgerName(key)+ "</LEDGERNAME>"+
                "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                "<AMOUNT>" + categories[key] + "</AMOUNT>" +
                addCostCentreAllocation(categories[key],'NO')+
                "</ALLLEDGERENTRIES.LIST>";

            }
          }

          for(var key in allTaxes) {
            if(allTaxes.hasOwnProperty(key)){
              ledgerxml += "<LEDGER NAME=" + "\'" +getLedgerName(key)+ "\'" + " Action = 'Create'>" +
                " <NAME>" + getLedgerName(key) + "</NAME>" +
                " <PARENT>Duties &amp; Taxes</PARENT>" +
                "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
                "</LEDGER>";

              voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                "<LEDGERNAME>" + getLedgerName(key) + "</LEDGERNAME>";

              voucherxml += "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                "<AMOUNT>" + allTaxes[key] + "</AMOUNT>" +
                addCostCentreAllocation(allTaxes[key],'NO')+
                "</ALLLEDGERENTRIES.LIST>";

            }
          }

          for(var key in charges) {
            if (charges.hasOwnProperty(key)) {
              ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName(key) + "\'" + " Action = 'Create'>" +
                " <NAME>" + getLedgerName(key) + "</NAME>" +
                " <PARENT>Duties &amp; Taxes</PARENT>" +
                "<ISCOSTCENTRESON>Yes</ISCOSTCENTRESON>"+
                "</LEDGER>";

              voucherxml += "<ALLLEDGERENTRIES.LIST>" +
                "<LEDGERNAME>" + getLedgerName(key) + "</LEDGERNAME>";

              voucherxml += "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>" +
                "<AMOUNT>" + charges[key] + "</AMOUNT>" +
                addCostCentreAllocation(charges[key],'NO')+
                "</ALLLEDGERENTRIES.LIST>";
            }
          }

          ledgerxml += "<LEDGER NAME=" + "\'" + getLedgerName('roundoff') + "\'" + " Action = 'Create'>"+"<NAME>"+getLedgerName('roundoff')+"</NAME><PARENT>Misc. Expenses (ASSET)</PARENT><ISCOSTCENTRESON>Yes</ISCOSTCENTRESON></LEDGER>"+
            "<LEDGER NAME=" + "\'" + getLedgerName('discount') + "\'" + " Action = 'Create'>"+"<NAME>"+getLedgerName('discount')+"</NAME><PARENT>Misc. Expenses (ASSET)</PARENT><ISCOSTCENTRESON>Yes</ISCOSTCENTRESON></LEDGER>"

          ledgerxml += "</TALLYMESSAGE>" +
            "</DATA>" +
            "</BODY>" +
            "</ENVELOPE>";


          voucherxml += "<ALLLEDGERENTRIES.LIST><LEDGERNAME>"+getLedgerName('roundoff')+"</LEDGERNAME>"+
            "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>"+
            "<AMOUNT>" + roundOff + "</AMOUNT>"+
            addCostCentreAllocation(roundOff,'NO')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml+="<ALLLEDGERENTRIES.LIST><LEDGERNAME>"+getLedgerName('discount')+"</LEDGERNAME>"+
            "<ISDEEMEDPOSITIVE>NO</ISDEEMEDPOSITIVE>"+
            "<AMOUNT>-" + discount + "</AMOUNT>" +
            addCostCentreAllocation(-discount,'NO')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml += "<ALLLEDGERENTRIES.LIST>" +
            "<LEDGERNAME>"+getLedgerName('debitcard')+"</LEDGERNAME>";
          voucherxml+= "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
            "<AMOUNT>-"+debit+"</AMOUNT>" +
            addCostCentreAllocation(-debit,'YES')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml+= "<ALLLEDGERENTRIES.LIST>" +
            "<LEDGERNAME>"+getLedgerName('creditcard')+"</LEDGERNAME>";
          voucherxml+= "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
            "<AMOUNT>-" + credit + "</AMOUNT>" +
            addCostCentreAllocation(-credit,'YES')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml += "<ALLLEDGERENTRIES.LIST>" +
            "<LEDGERNAME>"+getLedgerName('other')+"</LEDGERNAME>";
          voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
            "<AMOUNT>-" + other + "</AMOUNT>" +
            addCostCentreAllocation(-other,'YES')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml += "<ALLLEDGERENTRIES.LIST>" +
            "<LEDGERNAME>"+getLedgerName('cash')+"</LEDGERNAME>";
          voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
            "<AMOUNT>-" + cash + "</AMOUNT>" +
            addCostCentreAllocation(-cash,'YES')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml += "<ALLLEDGERENTRIES.LIST>" +
            "<LEDGERNAME>"+getLedgerName('online')+"</LEDGERNAME>";
          voucherxml+= "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
            "<AMOUNT>-" + online + "</AMOUNT>" +
            addCostCentreAllocation(-online,'YES')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml += "<ALLLEDGERENTRIES.LIST>" +
            "<LEDGERNAME>"+getLedgerName('coupon')+"</LEDGERNAME>";
          voucherxml += "<ISDEEMEDPOSITIVE>YES</ISDEEMEDPOSITIVE>" +
            "<AMOUNT>-" + coupon + "</AMOUNT>" +
            addCostCentreAllocation(-coupon,'YES')+
            "</ALLLEDGERENTRIES.LIST>";

          voucherxml +=
            "</VOUCHER>" +
            "</TALLYMESSAGE>" +
            "</DATA>" +
            "</BODY>" +
            "</ENVELOPE>";
          console.log(ledgerxml+voucherxml);
          var message = {
            url: $scope.hosturl,
            data: ledgerxml+voucherxml
          };

          // console.log('vouchercount:' + vouchercount);
          var proxyurl = "//localhost:65505/service1.svc/ExcuteTallyCommand";
          // console.log(message.data);
          $http({
            method: 'POST',
            url: proxyurl,
            data: {xml: message.data, url: $scope.hosturl}
          })
            .success(function (data) {
              console.log(data);

              $scope.progressvalue = Math.min(((($scope.progressV += 1) * 100) / $scope.tallypush.length), 100);
              $scope.created++;
              console.log("prg",$scope.progressvalue);
              if ($scope.progressvalue >= 100) {
                $timeout(function () {
                  $scope.progressflag = false;
                  $scope.flag = 1;
                  $scope.progressvalue = 0;
                  $scope.progressV = 0;
                  $scope.form.invoicefrom = 0;
                  $scope.form.invoiceto = 0;
                  console.log("hiiiiii");
                }, 1000);
              }
            })
            .error(function (data) {
              console.log(data);
              $scope.progressflag = false;
              console.log("error in connection with Tally");
              growl.error('Lost Connection with Tally! Try Again', {ttl: 4000});
              $scope.form.invoicefrom = 0;
              $scope.form.invoiceto = 0;
              $scope.progressV = 0;
              $scope.connectflag = -1;
            });
        })
      }

      function addCostCentreAllocation(amount,isDeemedPositive){
        var res = '<CATEGORYALLOCATIONS.LIST>'+
          '<CATEGORY>Outlets</CATEGORY>'+
          '<ISDEEMEDPOSITIVE>'+isDeemedPositive+'</ISDEEMEDPOSITIVE>'+
          '<COSTCENTREALLOCATIONS.LIST>'+
          '<NAME>'+$scope.outletName+'</NAME>'+
          '<AMOUNT>'+(amount)+'</AMOUNT>'+
          '</COSTCENTREALLOCATIONS.LIST>'+
          '</CATEGORYALLOCATIONS.LIST>';
        return res;
      }

      intercom.registerEvent('accounts');

    }]);


angular.module('posistApp').controller('tallypushEmptyModalCtrl',['$scope','$modal','$modalInstance', function ($scope, $modal, $modalInstance) {

  $scope.ok = function () {
    $modalInstance.dismiss('cancel');
    $scope.tallypushEmpty = true;
  };

  $scope.toggleAnimation = function () {
    $scope.animationsEnabled = !$scope.animationsEnabled;
  }

}]);
