'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('accounts', {
          url: '/accounts',
          templateUrl: 'app/accounts/accounts.html',
          controller: 'AccountsCtrl',
          resolve:{
            goat:
              function(Auth,$location,growl) {
                console.log("auth");
                if (Auth.getCurrentUser().$promise) {
                  Auth.getCurrentUser().$promise.then(function (user) {
                      console.log("accounts resolve:" + user.deployment_id);
                      if (user.deployment_id == null) {
                        console.log("accounts page resolve msg:access denied coz of null deployment id");
                        growl.error('No Deployment id set Cannot access accounts', {ttl: 3000});
                        $location.path("/login");
                      }
                    }, function (err) {
                      console.log("hello");
                      console.log(err);
                      $location.path('/login');
                    }
                  );
                }
                else
                {
                  console.log('error');
                  $location.path('/login');
                }
              }

             }
        }
      );
  });
