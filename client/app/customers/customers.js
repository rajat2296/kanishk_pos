'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('customers', {
        url: '/customers',
        templateUrl: 'app/customers/customers.html',
        controller: 'CustomersCtrl',
          resolve: {
            currentUser: ['$state', '$stateParams', 'Auth',
              function ($state, $stateParams, Auth) {
                return Auth.getCurrentUser().$promise.then(function (user) {
                  return user;
                });
              }]
          }
          })
      .state('CRM', {
        url: '/CRM',
        templateUrl: 'app/customers/CRM.html',
        controller: 'CustomersCtrl',
          resolve: {
            currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
              function ($state, $stateParams, Auth,Utils,growl,$location) {
                return Auth.getCurrentUser().$promise.then(function (user) {
                  var flag=false;
                  if (user.role === 'user') {
                    if (Utils.hasPermission(user.selectedPermissions,'view_customer_details')) {
                      flag = true;
                    }
                    if(!flag){
                      growl.error('No permission to view CRM !!',{ttl:3000});
                      Auth.logout();
                      $location.path('/login');
                      return false;
                    }
                  }
                  return user;
                });
              }]
          }
          });
  });