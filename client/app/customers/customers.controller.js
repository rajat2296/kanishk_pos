'use strict';

angular.module('posistApp')
  .controller('CustomersCtrl', function ($scope,Customer,Utils,localStorageService,$q,$timeout,growl,$ngBootbox,$modal, Item,billResource,$location) {
    $scope.isResetDaily = _.find(localStorageService.get('settings'), {name: 'reset_serial_daily'});
    $scope.crmForm = {
      disable: false,
      exportDisable: false
    };
    var pageNo = 0;
    var billRecords = true;
    var totalBillCustomers = 0;
    var totalNonBillCustomers = 0;
    if ($scope.currentUser.role != 'superadmin') {
      /*  if (!Utils.hasUserPermission($scope.currentUser.selectedPermissions, 'Reports')) {
       growl.error('00ps !! have permission ?', {ttl: 3000});
       $location.path('/login');
       }*/

    }
    $scope.csv = {
      content: null,
      header: true,
      separator: ',',
      result: null
    };

    $scope.pCount=0;
    $scope.tCount=0;
    $scope.loading=true;
    var params = {
      tenant_id: localStorageService.get('tenant_id'),
      deployment_id: localStorageService.get('deployment_id')
    };
    $scope.categories = [{id:1,name:'All'}];
    $scope.items = [];
    $scope.selectedItems = [];
    Item.getItemByDepoyment(params,function(result){
      $scope.items = angular.copy(result);
      console.log(result)
      _.forEach(result, function(item){

        if($scope.categories.length>0){
          var temp = _.find($scope.categories, {id : item.category._id});
        }

        if(!temp){
          $scope.categories.push({id:item.category._id,name:item.category.categoryName})
        }
      })
    })

    $scope.addSelected = function(categories){
      $scope.itemsFilter = [];
      $scope.selectedItems = [];
      if(categories.length==1 && categories[0]==1){
        _.forEach($scope.items, function(item){
          $scope.selectedItems.push({id:item._id,name:item.name});
        })
      }
      else{
        _.forEach(categories, function(category){
          var items = _.filter($scope.items, function(item){
            if(_.has(item,'category'))
            return item.category._id === category;
          });
          _.forEach(items, function(item){
            $scope.selectedItems.push({id:item._id,name:item.name});
          })
        })
      }
      //console.log($scope.selectedItems)
    }
    $scope.addSelectedItems = function(items){
      $scope.itemsFilter = [];
      _.forEach(items, function(item){
        $scope.itemsFilter.push(item);
      })
      console.log(items, $scope.itemsFilter)
    }



    $scope.onFinishImport=function(data){
      //console.log(data);
      if((localStorageService.get('tenant_id')==null||localStorageService.get('deployment_id')==null)){
        growl.error('Deployment not selected',{ttl:3000});
        return false;
      }
      $scope.loading=false;
      $scope.pCount=0;
      $scope.tCount=0;
      if(data.length>0){
        if(!(_.has(data[0],'name')&&_.has(data[0],'mobile')&&_.has(data[0],'flat')&&_.has(data[0],'street')&&_.has(data[0],'city')&& _.has(data[0],'state'))){
          growl.error('File format may not compatible',{ttl:3000});
          $scope.loading=true;
          return false;
        }
      }
      $scope.customers=data;
      /* for(var i= 0;i<10;i++){
       $scope.customers.push(data[i]);
       }*/

      var _customerAll=[];

      _.forEach($scope.customers,function(cust){
        var customerObject={customerId:Utils.guid(),addresses:[], firstname:cust.name,mobile:cust.mobile,addType:'Residence',phone:'',address1:cust.flat,address2:cust.street,city:cust.city,state:cust.state,postCode:'',DOB:null,MA:null,email:'',tenant_id:'',deployment_id:''};
        if(_.has(cust,'phone')){
          customerObject.phone=cust.phone;
        }else{
          //delete customerObject.phone;
        }
        if(_.has(cust,'postcode')){
          customerObject.postCode=cust.postcode;
        }else{
          //delete customerObject.postCode;
        }
        if(_.has(cust,'dob')){
          var dob= Utils.convertDate(cust.dob);
          customerObject.DOB=dob;
        }else{
          // delete customerObject.DOB;
        }
        if(_.has(cust,'ma')){
          var ma= Utils.convertDate(cust.ma);
          customerObject.MA=ma;
        }else{
          //delete customerObject.MA;
        }
        if(_.has(cust,'email')){
          customerObject.email=cust.email;
        }else{
          //delete customerObject.email;
        }
        customerObject.tenant_id=localStorageService.get('tenant_id');
        customerObject.deployment_id=localStorageService.get('deployment_id');
        // console.log(customerObject);
        // _customerAll.push(Customer.createWithCheck(JSON.stringify( customerObject)).$promise);
        _customerAll.push(JSON.stringify( customerObject));
      })
      $scope.tCount=_customerAll.length;
      autoCall(_customerAll,0);
      /*  $q.all(_customerAll).then(function(res){
       console.log(res);
       })*/

    }
    function autoCall(custs,count){
      Customer.createWithCheck(( custs[count])).$promise.then(function(){
        if(count<custs.length-1){
          count+=1;
          $timeout(function(){autoCall(custs,count)},5);
          $scope.pCount=count;
        }else
        {
          $scope.loading=true;
          console.log('done:'+count);
          growl.success('uploading customer data done successfully',{ttl:2000});
        }
      }).catch(function(err){
        $scope.loading=true;
        growl.error('Some problem in uploading data.',{ttl:2000});
      })

    }
    //////////////////////////////CRM////////////////////////////////////////////////
    $scope.selectedDate={
      from: "",//Utils.getDateFormattedDate(new Date()),
      to:""//Utils.getDateFormattedDate(new Date())
    }
    $scope.customerFilter={
      mobile:"",
      name:"",
      amountType:"=",
      frequencyType:"="
    }
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };
    $scope.openTo = function($event) {
      console.log($scope.openedTo);
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedToo = true;
    };
    $scope.openDOB = function($event) {
      console.log($scope.openedTo);
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedTo = true;
    };
    $scope.openMA = function($event) {
      console.log($scope.openedTo);
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedT = true;
    };
    $scope.customersBillData=[];
    $scope.searchCustomer=function(){
      $scope.crmForm.disable = true;
      $scope.customersBillData=[];
      var filter={};
      var query={};
      if(!($scope.selectedDate.from==""||$scope.selectedDate.from==null)){
        if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)){
          filter.includeDate=true;
        }else{
          $ngBootbox.alert("Feed in to date.");
          $scope.crmForm.disable = false;
          return false;
        }
      }else{
        if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)) {
          $ngBootbox.alert("Feed in start date.");
          $scope.crmForm.disable = false;;
          return false;
        }

        filter.includeDate=false;
      }

      if(filter.includeDate){
        if(Utils.getDateFormattedDate($scope.selectedDate.from)>Utils.getDateFormattedDate($scope.selectedDate.to)){
          $ngBootbox.alert("Hey!!  Is from date greater ?");
          $scope.crmForm.disable = false;
          return false;
        }
        query.startDate= Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.from));
        query.endDate=Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.to));
        query.endDate.setDate(query.endDate.getDate()+1);
        console.log(query.startDate,query.endDate);
      }
      if($scope.customerFilter.mobile!=""){
        query.mobile=$scope.customerFilter.mobile;
      }
      if($scope.customerFilter.name!=""){
        query.name=$scope.customerFilter.name;
      }
      if($scope.customerFilter.address!=""){
        query.address=$scope.customerFilter.address;
      }
      if($scope.customerFilter.dob){
        query.dob=$scope.customerFilter.dob;
      }
      if($scope.customerFilter.anniversary){
        query.anniversary=$scope.customerFilter.anniversary;
      }
      if($scope.customerFilter.amountValue){
        query.amountValue=$scope.customerFilter.amountValue;
        query.amountType=$scope.customerFilter.amountType;
      }
      if($scope.customerFilter.frequencyValue){
        query.frequencyValue=$scope.customerFilter.frequencyValue;
        query.frequencyType=$scope.customerFilter.frequencyType;
      }
      if($scope.customerFilter.total){
        query.total=$scope.customerFilter.total;
      }
      if($scope.itemsFilter && $scope.itemsFilter.length>0){
        query.itemsFilter = [];
        _.forEach($scope.itemsFilter, function(temp){
          if(temp != 1)
            query.itemsFilter.push(temp);
        })
      }
      else if($scope.categoryFilter){
        query.categoryFilter = [];
        _.forEach($scope.categoryFilter, function(temp){
          if(temp != 1)
            query.categoryFilter.push(temp);
        });
      }
      query.deployment_id=localStorageService.get('deployment_id');
      query.skip=0;
      if($scope.customerFilter.total && $scope.customerFilter.total<15)
        query.limit = $scope.customerFilter.total;
      else
        query.limit = 15;
      pageNo=0;
      $scope.busy=true;
      billRecords=true;
      Customer.getUniqueCustomer(query,function(customers){
        console.log("length=",customers)
        $scope.crmForm.disable = false;
        var customerObject =  customers;
        for(var i=0;i<customerObject.length;i++){
          if(customerObject[i].customer)
            $scope.customersBillData.push(customerObject[i]);
        }
        if(!$scope.customerFilter.total || $scope.customerFilter.total>15){
          $scope.busy = false;
        }
        else{
          billRecords=false;
        }
      })
    }
    $scope.nextPage = function(){
      // console.log("helooooo")
      // console.log($scope.busy);
        if ($scope.busy)
          return;
        $scope.busy = true;
        var filter = {};
        var query = {};
        if (!($scope.selectedDate.from == "" || $scope.selectedDate.from == null)) {
          if (!($scope.selectedDate.to == "" || $scope.selectedDate.to == null)) {
            filter.includeDate = true;
          } else {
            $ngBootbox.alert("Feed in to date.");
            return false;
          }
        } else {
          if (!($scope.selectedDate.to == "" || $scope.selectedDate.to == null)) {
            $ngBootbox.alert("Feed in start date.");
            return false;
          }

          filter.includeDate = false;
        }

        if (filter.includeDate) {
          if (Utils.getDateFormattedDate($scope.selectedDate.from) > Utils.getDateFormattedDate($scope.selectedDate.to)) {
            $ngBootbox.alert("Hey!!  Is from date greater ?");
            return false;
          }
          query.startDate = Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.from));
          query.endDate = Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.to));
          query.endDate.setDate(query.endDate.getDate() + 1);
        }
        if ($scope.customerFilter.mobile != "") {
          query.mobile = $scope.customerFilter.mobile;
        }
        if ($scope.customerFilter.name != "") {
          query.name = $scope.customerFilter.name;
        }
        if ($scope.customerFilter.address != "") {
          query.address = $scope.customerFilter.address;
        }
        if ($scope.customerFilter.dob) {
          query.dob = $scope.customerFilter.dob;
        }
        if ($scope.customerFilter.anniversary) {
          query.anniversary = $scope.customerFilter.anniversary;
        }
        if ($scope.customerFilter.amountValue != undefined) {
          query.amountValue = parseInt($scope.customerFilter.amountValue);
          query.amountType = $scope.customerFilter.amountType;
        }
        if ($scope.customerFilter.frequencyValue != undefined) {
          query.frequencyValue = parseInt($scope.customerFilter.frequencyValue);
          query.frequencyType = $scope.customerFilter.frequencyType;
        }
        if ($scope.customerFilter.total != undefined) {
          query.total = $scope.customerFilter.total;
        }
        if ($scope.itemsFilter && $scope.itemsFilter.length > 0) {
          query.itemsFilter = $scope.itemsFilter;
        }
        else if ($scope.categoryFilter) {
          query.categoryFilter = [];
          _.forEach($scope.categoryFilter, function (temp) {
            if (temp != 1)
              query.categoryFilter.push(temp);
          });
        }
        query.limit = 15;
        query.deployment_id = localStorageService.get('deployment_id');
        if (billRecords)
          query.skip = ++pageNo;
        else {
          if(!(query.startDate || query.endDate || (query.itemsFilter && query.itemsFilter.length > 0) || (query.categoryFilter && query.categoryFilter.length > 0))) {
            query.skipCust = pageNo++;
          }
        }
        if ($scope.customerFilter.total && ($scope.customerFilter.total - $scope.customersBillData.length) < 15) {
          query.limit = $scope.customerFilter.total - $scope.customersBillData.length
        }
        if(query.skip >= 0 || query.skipCust >= 0) {
          Customer.getUniqueCustomer(query, function (customers) {
            console.log(customers)
            var customerObject = customers;
            for (var i = 0; i < customerObject.length; i++) {
              if (customerObject[i].customer)
                $scope.customersBillData.push(customerObject[i]);
            }
            if(!$scope.customerFilter.total || $scope.customerFilter.total>15*(pageNo + 1)){
              if(customers.length > 0 && billRecords==true){
                billRecords=true;
                $scope.busy = false;
              }
              else if(customers.length == 0 && billRecords==true){
                pageNo=0;
                billRecords=false;
                $scope.busy = false;
              }
              else if(customers.length > 0 && billRecords==false){
                billRecords=false;
                $scope.busy = false;
              }
              else{
                $scope.busy = true;
              }
            }
            else{
              $scope.busy = true;
            }
          })
        }
      //}
    }

    $scope.deleteCustomer = function(index){
      $scope.customersBillData.splice(index,1);
    };

    $scope.showCustomerDetail=function(customer,index){
      var modalInstance = $modal.open({
        templateUrl: 'app/billing/customer/_newCustomer.html',
        controller: 'CustomerCtrl',
        size: "md",
        resolve: {
          number: function() {
            return customer;
          }

        }
      });
      modalInstance.result.then(function (updatedCustomer) {
        $scope.customersBillData[index].customer = updatedCustomer;
      });
    }
    $scope.customersBill = [];
    var billRecordsExcel = true;
    var pageNoExcel = 0;

    $scope.getAllData = function(divId){
      $scope.crmForm.exportDisable = true;
      var filter={};
      var query={};
      console.log($scope.selectedDate);
      if(!($scope.selectedDate.from==""||$scope.selectedDate.from==null)){
        if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)){
          // if(Utils.convertDate($scope.selectedDate.from) && Utils.convertDate($scope.selectedDate.to)){
          filter.includeDate=true;
          /*}else {
           $ngBootbox.alert("Error in date format.");
           return false;
           }*/
        }else{
          $ngBootbox.alert("Feed in to date.");
          return false;
        }
      }else{
        if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)) {
          $ngBootbox.alert("Feed in start date.");
          return false;
        }

        filter.includeDate=false;
      }

      if(filter.includeDate){
        if(Utils.getDateFormattedDate($scope.selectedDate.from)>Utils.getDateFormattedDate($scope.selectedDate.to)){
          $ngBootbox.alert("Hey!!  Is from date greater ?");
          return false;
        }
        query.startDate= Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.from));
        query.endDate=Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.to));
        query.endDate.setDate(query.endDate.getDate()+1);
      }
      if($scope.customerFilter.mobile!=""){
        query.mobile=$scope.customerFilter.mobile;
      }
      if($scope.customerFilter.name!=""){
        query.name=$scope.customerFilter.name;
      }
      if($scope.customerFilter.address!=""){
        query.address=$scope.customerFilter.address;
      }
      if($scope.customerFilter.dob){
        query.dob=$scope.customerFilter.dob;
      }
      if($scope.customerFilter.anniversary){
        query.anniversary=$scope.customerFilter.anniversary;
      }
      if($scope.customerFilter.amountValue){
        query.amountValue=parseInt($scope.customerFilter.amountValue);
        query.amountType=$scope.customerFilter.amountType;
      }
      if($scope.customerFilter.frequencyValue){
        query.frequencyValue=parseInt($scope.customerFilter.frequencyValue);
        query.frequencyType=$scope.customerFilter.frequencyType;
      }
      if($scope.customerFilter.total){
        query.total=$scope.customerFilter.total;
      }
      if($scope.itemsFilter && $scope.itemsFilter.length>0){
        query.itemsFilter=$scope.itemsFilter;
      }
      else if($scope.categoryFilter){
        query.categoryFilter = [];
        _.forEach($scope.categoryFilter, function(temp){
          if(temp != 1)
            query.categoryFilter.push(temp);
        })
      }

      //var query = {};
      fetchCust(query).then(function () {
        console.log($scope.customersBill);
        $scope.crmForm.exportDisable = false;
        $timeout(function() {
          window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById(divId).innerHTML));
        }, 10);

      });
    }

    function fetchCust(query) {

      var deferred = $q.defer();
      query.limit = 50;
      query.deployment_id=localStorageService.get('deployment_id');
      if(billRecordsExcel)
        query.skip= pageNoExcel++;
      else
        query.skipCust = pageNoExcel++;
      if($scope.customerFilter.total && ($scope.customerFilter.total - $scope.customersBill.length)<50){
        query.limit = $scope.customerFilter.total - $scope.customersBill.length
      }
      console.log(query)
      Customer.getUniqueCustomer(query,function(customers){
        var customerObject =  customers;
        for(var i=0;i<customerObject.length;i++){
          if(customerObject[i].customer)
            $scope.customersBill.push(customerObject[i]);
        }
        console.log($scope.customersBill.length)
        if(!$scope.customerFilter.total || $scope.customerFilter.total>$scope.customersBill.length){
          if(customers.length > 0 && billRecordsExcel==true){
            billRecordsExcel=true;
            fetchCust(query).then(function (result) {
              deferred.resolve(customers);
            });
          }
          else if(customers.length == 0 && billRecordsExcel==true){
            pageNoExcel=0;
            billRecordsExcel=false;
            fetchCust(query).then(function (result) {
              deferred.resolve(customers);
            });
          }
          else if(customers.length > 0 && billRecordsExcel==false){
            billRecords=false;
            fetchCust(query).then(function (result) {
              deferred.resolve(customers);
            });
          }
          else{
            deferred.resolve(customers);
          }
        }
        else{
          deferred.resolve(customers);
        }
      });
      return deferred.promise;
    }

    $scope.exportToExcel = function(divId){
      $scope.customersBill = [];
      billRecordsExcel = true;
      pageNoExcel = 0;
      if($scope.customersBill.length == 0)
        $scope.getAllData(divId);
    };

    $scope.exportToExcelForSelected = function(divId){
      $scope.crmForm.exportDisableForSelected = false;
      $timeout(function() {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById(divId).innerHTML));
      }, 10);
    };

    $scope.getOrderHistory=function(customerId, totalBills,avgCovers,avgAmount,totalAmount){
      var modalInstance = $modal.open({
        templateUrl: 'app/customers/_orderHistory.html',
        controller: function($scope,localStorageService,billResource,$rootScope,$modalInstance,Utils){
          console.log(localStorageService.get('settings'))
          $scope.isResetDaily = _.find(localStorageService.get('settings'), {name: 'reset_serial_daily'});
          console.log($scope.isResetDaily)
          var query = {
            deployment_id:localStorageService.get('deployment_id'),
            customerId:customerId,
            limit:4,
            skip:0
          }
          $scope.bill = {totalBills:totalBills,covers:avgCovers,avgAmount:avgAmount,totalAmount:totalAmount}
          $scope.orderHistory = [];
          $rootScope.showLoaderOverlay = true;
          billResource.getBillsByCustomer(query).$promise.then(function (res) {
            $rootScope.showLoaderOverlay = false;
            console.log(res);
              $scope.orderHistory = res.bills;
              $scope.topItems = [] ;
              _.forEach(res.topItems,function(item){
                $scope.topItems.push([item.name,item.count]);
              })
              $scope.topCategories = [];
              _.forEach(res.topCategories,function(item){
                $scope.topCategories.push([item.name,item.count]);
              })
              var chart = new Highcharts.Chart({
                chart: {
                  renderTo: 'container1',
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false
                },
                title: {
                  text: 'Top Categories'
                },
                plotOptions: {
                  pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                      style: {
                        width: '100px'
                      },
                      enabled: true,
                      color: '#000000',
                      connectorColor: '#000000',
                      formatter: function () {
                        return '<b style="word-wrap:break-word;">' + this.point.name + '</b>: ' + (isNaN(this.percentage) ? this.percentage : this.percentage.toFixed(2)) + ' %';
                      }
                    }
                  }
                },
                series: [{
                  type: 'pie',
                  name: 'Top Categories',
                  data: $scope.topCategories
                }]
              });
              var chart1 = new Highcharts.Chart({
                chart: {
                  renderTo: 'container',
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false
                },
                title: {
                  text: 'Top Most Ordered Items'
                },
                plotOptions: {
                  pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                      style: {
                        width: '100px'
                      },
                      enabled: true,
                      color: '#000000',
                      connectorColor: '#000000',
                      formatter: function () {
                        return '<b>' + this.point.name + '</b>: ' + (isNaN(this.percentage) ? this.percentage : this.percentage.toFixed(2)) + ' %';
                      }
                    }
                  }
                },
                series: [{
                  type: 'pie',
                  name: 'Top Items',
                  data: $scope.topItems
                }]
              });
          })

          $scope.getTotal = function(cash){
              var total = 0;
              for(var i = 0; i < cash.length; i++){
                  total += cash[i];
              }
              return total;
          }
          $scope.getAllData = function(divId){
            $scope.crmForm = true;
            var query={};

            fetchCust(query).then(function () {
              console.log($scope.customersBillHistory,divId);
              $scope.crmForm = false;
              $timeout(function() {
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById(divId).innerHTML));
              }, 10);

            });
          }

          function fetchCust(query) {

            var deferred = $q.defer();
            query.limit = 100;
            query.deployment_id=localStorageService.get('deployment_id');
            query.customerId = customerId;
            query.skip= pageNoExcel++;

            billResource.getAllBills(query).$promise.then(function (res) {
              if(res.length>0){
                for(var i=0;i<res.length;i++){
                  if(res[i].addOns && res[i].addOns.length>0){
                    for(var j=0;j<res[i].addOns.length;j++){
                      var temp = {
                        name:res[i].name,mobile:res[i].mobile,phone:res[i].phone,address1:res[i].address1,address2:res[i].address2,
                        city:res[i].city,state:res[i].state,postCode:res[i].postCode,email:res[i].email,
                        daySerialNumber:res[i].daySerialNumber,serialNumber:res[i].serialNumber,covers:res[i].covers,item:res[i].addOns[j].name,qty:res[i].addOns[j].quantity,
                        rate:res[i].addOns[j].rate,priceQuantity:res[i].addOns[j].subtotal,catName:res[i].addOns[j].category.categoryName,
                        created:res[i].created,tab:res[i].tab
                      }
                      $scope.customersBillHistory.push(temp);
                    }
                  }
                    $scope.customersBillHistory.push(res[i]);
                }
                fetchCust(query).then(function (result) {
                  deferred.resolve(result);
                });
              }
              else{
                deferred.resolve(res);
              }
            });
            return deferred.promise;
          }

          $scope.exportToExcel = function(divId){
            $scope.customersBillHistory = [];
            billRecordsExcel = true;
            pageNoExcel = 0;
            if($scope.customersBillHistory.length == 0)
              $scope.getAllData(divId);
          };
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
        },
        size: "lg",
        backdrop  : 'static',
        resolve: {
          number: function() {
            return "success";
          }

        }
      });
      modalInstance.result.then(function (updatedCustomer) {
        $scope.customersBillData[index].customer = updatedCustomer;
      });
    }
    $scope.exportToExcelForOrderHistory = function(divId){
      $scope.customersBill = [];
      billRecordsExcel = true;
      pageNoExcel = 0;
      if($scope.customersBill.length == 0)
        $scope.getAllDataForOrderHistory(divId);
    }
    $scope.getAllDataForOrderHistory = function(divId){
      $scope.crmForm.exportDisableForOrderHistory = true;
      var filter={};
      var query={};
      console.log($scope.selectedDate);
      if(!($scope.selectedDate.from==""||$scope.selectedDate.from==null)){
        if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)){
          filter.includeDate=true;
        }else{
          $ngBootbox.alert("Feed in to date.");
          return false;
        }
      }else{
        if(!($scope.selectedDate.to==""||$scope.selectedDate.to==null)) {
          $ngBootbox.alert("Feed in start date.");
          return false;
        }

        filter.includeDate=false;
      }

      if(filter.includeDate){
        if(Utils.getDateFormattedDate($scope.selectedDate.from)>Utils.getDateFormattedDate($scope.selectedDate.to)){
          $ngBootbox.alert("Hey!!  Is from date greater ?");
          return false;
        }
        query.startDate= Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.from));
        query.endDate=Utils.getResetDate(localStorageService.get('settings'), angular.copy($scope.selectedDate.to));
        query.endDate.setDate(query.endDate.getDate()+1);
      }
      if($scope.customerFilter.mobile!=""){
        query.mobile=$scope.customerFilter.mobile;
      }
      if($scope.customerFilter.name!=""){
        query.name=$scope.customerFilter.name;
      }
      if($scope.customerFilter.address!=""){
        query.address=$scope.customerFilter.address;
      }
      if($scope.customerFilter.dob){
        query.dob=$scope.customerFilter.dob;
      }
      if($scope.customerFilter.anniversary){
        query.anniversary=$scope.customerFilter.anniversary;
      }
      if($scope.customerFilter.amountValue){
        query.amountValue=parseInt($scope.customerFilter.amountValue);
        query.amountType=$scope.customerFilter.amountType;
      }
      if($scope.customerFilter.frequencyValue){
        query.frequencyValue=parseInt($scope.customerFilter.frequencyValue);
        query.frequencyType=$scope.customerFilter.frequencyType;
      }
      if($scope.customerFilter.total){
        query.total=$scope.customerFilter.total;
      }
      if($scope.itemsFilter && $scope.itemsFilter.length>0){
        query.itemsFilter=$scope.itemsFilter;
      }
      else if($scope.categoryFilter){
        query.categoryFilter = [];
        _.forEach($scope.categoryFilter, function(temp){
          if(temp != 1)
            query.categoryFilter.push(temp);
        })
      }

      //var query = {};
      fetchCustForOrderHistory(query).then(function (result) {
        //$scope.customersBill=result;
        console.log($scope.customersBill);
        $scope.crmForm.exportDisableForOrderHistory = false;
        $timeout(function() {
          window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById(divId).innerHTML));
        }, 10);

      });
    }

    function fetchCustForOrderHistory(query) {

      var deferred = $q.defer();
      query.limit = 100;
      query.skip = pageNoExcel++;
      query.deployment_id=localStorageService.get('deployment_id');
      if($scope.customerFilter.total && ($scope.customerFilter.total - $scope.customersBill.length)<50){
        query.limit = $scope.customerFilter.total - $scope.customersBill.length
      }
      console.log(query)
      billResource.getAllBills(query).$promise.then(function (res) {
        if(res.length>0){
          console.log(res)
          for(var i=0;i<res.length;i++){
              //if(customerObject[i].customer)<td>{{bill.name}}</td>
              if(res[i].addOns && res[i].addOns.length>0){
                for(var j=0;j<res[i].addOns.length;j++){
                  var temp = {
                    name:res[i].name,mobile:res[i].mobile,phone:res[i].phone,address1:res[i].address1,address2:res[i].address2,
                    city:res[i].city,state:res[i].state,postCode:res[i].postCode,email:res[i].email,
                    BillNumber:res[i].BillNumber,covers:res[i].covers,item:res[i].addOns[j].name,qty:res[i].addOns[j].quantity,
                    rate:res[i].addOns[j].rate,daySerialNumber:res[i].daySerialNumber,serialNumber:res[i].serialNumber,priceQuantity:res[i].addOns[j].subtotal,catName:res[i].addOns[j].category.categoryName,
                    created:res[i].created,tab:res[i].tab
                  }
                  $scope.customersBill.push(temp);
                }
              }
              $scope.customersBill.push(res[i]);
          }
          fetchCustForOrderHistory(query).then(function (result) {
            deferred.resolve(result);
          });
        }
        else{
          deferred.resolve(res);
        }
      });
      return deferred.promise;
    }
  });
