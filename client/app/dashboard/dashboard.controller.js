'use strict';

angular.module('posistApp')
    .controller('DashboardCtrl', ['$scope', '$rootScope', '$resource', 'deployments', 'Report', 'localStorageService', '$state', '$filter', '$q', '$timeout', 'growl', '$modal', 'currentUser', 'Utils', '$location', function($scope, $rootScope, $resource, deployments, Report, localStorageService, $state, $filter, $q, $timeout, growl, $modal, currentUser, Utils, $location) {

        $scope.currentUser = currentUser;

        if ($scope.currentUser.role != 'superadmin') {
            if (!Utils.hasUserPermission($scope.currentUser.selectedPermissions, 'Reports')) {
                growl.error('00ps !! have permission ?', {
                    ttl: 3000
                });
                $location.path('/login');
            }
        }

        $scope.selectedDate = {
            startDate: new Date(),
            endDate: new Date()
        };

        if ($state.current.name === 'dashboard.dash_firstTimeCustomer') {
            console.log("Customers State");
            var _f = moment().subtract(6, 'days').toArray();
            var _t = moment().toArray();
            $scope.selectedDate.startDate =  new Date(_f[0], _f[1], _f[2]);
            $scope.selectedDate.endDate = new Date(_t[0], _t[1], _t[2])
        }


        $scope.datePickerOptions = {
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };


        $scope.expectedSales = {
            day: 60000,
            timeRange: 60000 * 30
        };

        /* Navigation State Flags */
        $scope.isSalesNavCollapsed = false;
        $scope.isUtilizationNavCollapsed = true;
        $scope.isInventoryNavCollapsed = true;
        $scope.isCustomerNavCollapsed = true;

        /* Masters */
        var _deployments = [];
        if ($scope.currentUser.role != 'superadmin') {
            if (!Utils.hasUserPermission($scope.currentUser.selectedPermissions, 'Reports')) {
                growl.error('00ps !! have permission ?', {
                    ttl: 3000
                });
                $location.path('/login');
            }
            _deployments = _.filter(deployments, {
                _id: $scope.currentUser.deployment_id
            });
        } else {
            _deployments = deployments;
        }

        $scope.deployments = _deployments;

        /* Report Service */
        $scope.report = new Report();

        /* Compare View Flag */
        $scope.showCompareView = false;
        $scope.compareViewCols;
        $scope.hasData = false;
        $scope.hasChartData = true;


        /* Graph Data Object */
        $scope.tabData = [];
        $scope.drillDownTabData = [];
        $scope.graphData = {
            tabData: $scope.tabData,
            drilldown: $scope.drillDownTabData
        };

        $scope._dashes = ['dashboard.dash_time', 'dashboard.dash_menuItems', 'dashboard.dash_serviceType', 'dashboard.dash_staffWaiters'];
        $scope.gotoNextDash = function() {
            //console.log("Infinite Scroll, goto Next Dash!");
            var _currentStateIndex = $scope._dashes.indexOf($state.current.name);
            //console.log('Current State ', _currentStateIndex , $state.$current.name);
            /*$timeout(function () {
                var _nextStateIndex = _currentStateIndex + 1;
                $state.transitionTo($scope._dashes[_nextStateIndex]);
            }, 1000);*/
        };

        $scope.clearCompare = function() {

            $scope.showCompareView = false;
        };

        $scope.generateDashboard = function() {

            $rootScope.showLoaderOverlay = true;

            $scope.report.isGenerated = false;

            $scope.report.setDateRange().then(function(dates) {

                $scope.billResource.getDataRangeBills({
                    fromDate: dates.fromDate,
                    toDate: dates.toDate,
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: $scope.report.filters.deployment_id,
                    cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting
                }).$promise.then(function(bills) {
                    //console.log("Today Bills", bills);
                    $scope.report.addBills(bills).then(function() {
                        $scope.report.prepareBillsForReport().then(function() {
                            $scope.report.getTotalSale().then(function(saleData) {

                                //$scope.saleToday = saleData.totalNet;
                                //console.log(saleData);

                                $scope.report.getAverageBills().then(function(rows) {

                                    $scope.averageBills = rows['daily'][0].avg_amount;
                                    $scope.report.groupBillsByHours(dates).then(function(groupedInterval) {
                                        console.log('Today Bills Group',groupedInterval);    
                                        $scope.report.getTotalCovers().then(function(totalCovers) {
                                            //console.log("Today", totalCovers);
                                            $scope.totalCovers = totalCovers;

                                            $scope.getTotalSaleBeginMonth().then(function(saleFromMonthBegin) {
                                                //console.log('Beging from month', saleFromMonthBegin);
                                                $scope.saleFromBeginMonth = saleFromMonthBegin.totalSale;
                                                $scope.saleAvgFromBeginMonth = parseFloat(saleFromMonthBegin.totalSale/saleFromMonthBegin.totalBills);
                                                $scope.coversFromBeginMonth = saleFromMonthBegin.totalCovers;
                                                $scope.billsFromBeginMonth = saleFromMonthBegin.totalBills;

                                                $scope.progressExpectedTimeRangeSale = (($scope.saleFromBeginMonth / $scope.expectedSales.timeRange) * 100)

                                            });

                                            $scope.getTotalSaleToday().then(function (todaySaleData) {
                                                //console.log('Today', todaySaleData);
                                                $scope.todaySaleData = todaySaleData;
                                                $scope.progressExpectedDaySale = (($scope.todaySaleData.totalSale / $scope.expectedSales.day) * 100);
                                            });

                                            //$scope.progressExpectedDaySale = (($scope.saleToday / $scope.expectedSales.day) * 100);


                                            /* Prepare Sales Chart By Interval */
                                            var _xTimeCategories = [];
                                            var _series = {
                                                name: 'Bi-Hourly Sales',
                                                type: 'column',
                                                yAxis: 1,
                                                data: [],
                                                tooltip: {
                                                    valueSuffix: ''
                                                }
                                            };
                                            var _cumulativeAmount = 0;
                                            var _seriesSpline = {
                                                name: 'Total Sales',
                                                type: 'spline',
                                                data: [],
                                                tooltip: {
                                                    valueSuffix: ''
                                                }
                                            };
                                            angular.forEach(groupedInterval, function(interval) {
                                                if (_.has(interval, 'day')) {

                                                    var _d = moment(new Date(interval.day[0], interval.day[1], interval.day[2])).format('YYYY-MM-DD');
                                                    _xTimeCategories.push(_d);
                                                    _series.data.push(interval.totalNet);
                                                    _cumulativeAmount += interval.totalNet;
                                                    _seriesSpline.data.push(_cumulativeAmount);

                                                } else {
                                                    var _iFrom = moment(interval.from).format('hh:mm a');
                                                    var _iTo = moment(interval.to).format('hh:mm a');
                                                    _xTimeCategories.push(_iFrom + ' - ' + _iTo);
                                                    _series.data.push(interval.totalNet);
                                                    _cumulativeAmount += interval.totalNet;
                                                    _seriesSpline.data.push(_cumulativeAmount);
                                                }

                                            });

                                            /* Sale Amount */
                                            $scope.graphDataSalesHourly.xAxis = [];
                                            $scope.graphDataSalesHourly.xAxis.push({
                                                categories: _xTimeCategories,
                                                crosshair: true
                                            });

                                            $scope.graphDataSalesHourly.series = [];
                                            $scope.graphDataSalesHourly.series.push(_series);
                                            $scope.graphDataSalesHourly.series.push(_seriesSpline);
                                            /* Sale Amount */


                                            /* Prepare No of Bills By Interval */
                                            var _xNoOfBillsCategores = [];
                                            var _seriesOrders = {
                                                name: 'Bi-Hourly Orders(Bills)',
                                                type: 'column',
                                                yAxis: 1,
                                                data: [],
                                                tooltip: {
                                                    valueSuffix: ''
                                                }
                                            };
                                            var _cumulativeOrders = 0;
                                            var _seriesSplineOrders = {
                                                name: 'Total Orders',
                                                type: 'spline',
                                                data: [],
                                                tooltip: {
                                                    valueSuffix: ''
                                                }
                                            };
                                            angular.forEach(groupedInterval, function(interval) {
                                                if (_.has(interval, 'day')) {
                                                    var _d = moment(new Date(interval.day[0], interval.day[1], interval.day[2])).format('YYYY-MM-DD');
                                                    _xNoOfBillsCategores.push(_d);
                                                    _seriesOrders.data.push(interval.totalBills);
                                                    _cumulativeOrders += interval.totalBills;
                                                    _seriesSplineOrders.data.push(_cumulativeOrders);

                                                } else {
                                                    var _iFrom = moment(interval.from).format('hh:mm a');
                                                    var _iTo = moment(interval.to).format('hh:mm a');
                                                    _xNoOfBillsCategores.push(_iFrom + ' - ' + _iTo);
                                                    _seriesOrders.data.push(interval.totalBills);
                                                    _cumulativeOrders += interval.totalBills;
                                                    _seriesSplineOrders.data.push(_cumulativeOrders);
                                                }

                                            });

                                            $scope.graphDataOrdersHourly.xAxis = [];
                                            $scope.graphDataOrdersHourly.xAxis.push({
                                                categories: _xNoOfBillsCategores,
                                                crosshair: true
                                            });

                                            $scope.graphDataOrdersHourly.series = [];
                                            $scope.graphDataOrdersHourly.series.push(_seriesOrders);
                                            $scope.graphDataOrdersHourly.series.push(_seriesSplineOrders);

                                            console.log(_seriesOrders);
                                            console.log("_seriesSplineOrders:"+ _seriesSplineOrders);
                                            /* Prepare No of Bills By Interval */

                                            $scope.report.isGenerated = true;
                                            $scope.hasData = true;
                                            $rootScope.showLoaderOverlay = false;

                                        });




                                    });

                                });


                            });
                        });

                    }).catch(function(err) {
                        growl.error("No Data!", {
                            ttl: 3000
                        });
                        $rootScope.showLoaderOverlay = false;
                        $scope.hasData = false;
                    });

                }).catch(function(err) {
                    growl.error("No Data found!", {
                        ttl: 3000
                    });
                    $rootScope.showLoaderOverlay = false;
                    $scope.hasData = false;
                });


            });
        };

        $scope.getTotalSaleBeginMonth = function() {
            /* Get Bills from begining of the Month: Using new Report() */
            var self = this;
            var defer = $q.defer();

            $timeout(function() {

                $scope.newReport = new Report();
                var cdate = new Date();
                var y = cdate.getFullYear();
                var m = cdate.getMonth();
                var firstDay = new Date(y, m, 1).toISOString();

                $scope.newReport.filters.fromDate = firstDay;
                $scope.newReport.filters.toDate = new Date().toISOString();
                $scope.newReport.filters.deployment_id = $scope.report.filters.deployment_id;
                $scope.newReport.filters.cutOffTimeSetting = $scope.report.filters.cutOffTimeSetting;


                $scope.newReport.setDateRange().then(function(dates) {
                    $scope.billResource.getDataRangeBills({
                        fromDate: dates.fromDate,
                        toDate: dates.toDate,
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: $scope.report.filters.deployment_id,
                        cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting
                    }).$promise.then(function(bills) {
                        $scope.newReport.addBills(bills).then(function() {
                            $scope.newReport.prepareBillsForReport().then(function() {
                                $scope.newReport.getTotalSale().then(function(fromFirstDaySaleAmount) {
                                    $scope.newReport.getAverageBills().then(function(avgBills) {
                                        $scope.newReport.getTotalCovers().then(function(covers) {
                                            //console.log(fromFirstDaySaleAmount);
                                            defer.resolve({
                                                totalSale: fromFirstDaySaleAmount.totalNet,
                                                avg: avgBills['daily'][0].avg_amount,
                                                totalCovers: covers,
                                                totalBills: bills.length
                                            });
                                        });
                                    });
                                    //defer.resolve(fromFirstDaySaleAmount);
                                });
                            });
                        });

                    }).catch(function() {
                        growl.error("No Data found!", {
                            ttl: 3000
                        });
                    });
                });

            });

            return defer.promise;
        };

        $scope.getTotalSaleToday = function() {
            /* Get Bills from begining of the Month: Using new Report() */
            var self = this;
            var defer = $q.defer();

            $timeout(function() {

                $scope.todayNewReport = new Report();
                var cdate = new Date();
                $scope.todaysDate = cdate;
                /*var y = cdate.getFullYear();
                var m = cdate.getMonth();
                var firstDay = new Date(y, m, 1).toISOString();*/

                $scope.todayNewReport.filters.fromDate = cdate.toISOString();
                $scope.todayNewReport.filters.toDate = cdate.toISOString();
                $scope.todayNewReport.filters.deployment_id = $scope.report.filters.deployment_id;
                $scope.todayNewReport.filters.cutOffTimeSetting = $scope.report.filters.cutOffTimeSetting;


                $scope.todayNewReport.setDateRange().then(function(dates) {
                    $scope.billResource.getDataRangeBills({
                        fromDate: dates.fromDate,
                        toDate: dates.toDate,
                        tenant_id: localStorageService.get('tenant_id'),
                        deployment_id: $scope.report.filters.deployment_id,
                        cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting
                    }).$promise.then(function(bills) {
                        $scope.todayNewReport.addBills(bills).then(function() {
                            $scope.todayNewReport.prepareBillsForReport().then(function() {
                                $scope.todayNewReport.getTotalSale().then(function(fromFirstDaySaleAmount) {
                                    $scope.todayNewReport.getAverageBills().then(function(avgBills) {
                                        $scope.todayNewReport.getTotalCovers().then(function(covers) {
                                            console.log('Today Yo',fromFirstDaySaleAmount);
                                            defer.resolve({
                                                totalSale: fromFirstDaySaleAmount.totalNet,
                                                avg: avgBills['daily'][0].avg_amount,
                                                totalCovers: covers,
                                                totalBills: bills.length
                                            });
                                        });
                                    });
                                    //defer.resolve(fromFirstDaySaleAmount);
                                });
                            });
                        });

                    }).catch(function() {
                        growl.error("No Data found!", {
                            ttl: 3000
                        });
                    });
                });

            });

            return defer.promise;
        };

        $scope.generateDashServiceType = function() {

            $scope.report.isGenerated = false;
            $rootScope.showLoaderOverlay = true;

            $scope.report.setDateRange().then(function(dates) {

                $scope.billResource.getDataRangeBills({
                    fromDate: dates.fromDate,
                    toDate: dates.toDate,
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: $scope.report.filters.deployment_id,
                    cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting
                }).$promise.then(function(bills) {

                    //console.log(bills);
                    $scope.report.addBills(bills).then(function() {
                        $scope.report.prepareBillsForReport().then(function() {
                            $scope.report.groupByTabType().then(function(rows) {

                                //console.log(rows);
                                var _rows = $filter('orderBy')(rows, '-subtotal', false);

                                /* Prepare Chart */
                                var _series = {
                                    type: 'pie',
                                    name: 'Tab Share',
                                    data: []
                                };
                                angular.forEach(_rows, function(tab) {
                                    console.log(tab);
                                    _series.data.push([tab.tab, tab.totalNet]);
                                });

                                $scope.graphDataSalesServiceType.series = [];
                                $scope.graphDataSalesServiceType.series.push(_series);

                                $scope.report.isGenerated = true;
                                $scope.hasData = true;
                                $rootScope.showLoaderOverlay = false;

                            }).catch(function() {
                                $rootScope.showLoaderOverlay = false;
                            });
                        }).catch(function() {
                            $rootScope.showLoaderOverlay = false;
                        });
                    }).catch(function() {
                        $rootScope.showLoaderOverlay = false;
                    });

                }).catch(function() {
                    growl.error("No Data found!", {
                        ttl: 3000
                    });
                    $rootScope.showLoaderOverlay = false;
                    $scope.hasData = false;
                });
            }).catch(function(err) {
                growl.error("Something went wrong!", {
                    ttl: 3000
                });
                $rootScope.showLoaderOverlay = false;
                $scope.hasData = false;
            });
        };

        $scope.generateDashMenuItems = function() {

            $scope.report.isGenerated = false;
            $rootScope.showLoaderOverlay = true;

            $scope.report.setDateRange().then(function(dates) {

                $scope.billResource.getDataRangeBills({

                    fromDate: dates.fromDate,
                    toDate: dates.toDate,
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: $scope.report.filters.deployment_id,
                    cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting

                }).$promise.then(function(bills) {


                    $scope.report.addBills(bills).then(function() {
                        $scope.report.prepareBillsForReport().then(function() {
                            //console.log($scope.report.bills);
                            $scope.report.groupByItemItem().then(function(rows) {

                                $scope.report.groupByCategory(rows).then(function(catRows) {

                                    /* Top 15 Items */
                                    var _rows = $filter('orderBy')(rows, '-subtotal', false);
                                    var _topFifteenItems = _rows.slice(0, 15);

                                    /* Top 15 Categories */
                                    var _catRows = $filter('orderBy')(catRows, '-totalSubtotal', false);
                                    var _topFifteenCategories = _catRows.slice(0, 15);

                                    //console.log(_topFifteenCategories);

                                    /* Prepare Chart: Items */
                                    var _xItemCategories = [];
                                    var _seriesItems = {
                                        name: "Menu Items",
                                        data: []
                                    };
                                    angular.forEach(_topFifteenItems, function(item) {
                                        _xItemCategories.push(item.name);
                                        _seriesItems.data.push(item.subtotal);
                                    });

                                    /* Prepare Chart: Categories */
                                    var _xCatCategories = [];
                                    var _seriesCategories = {
                                        name: "Categories",
                                        data: []
                                    };
                                    angular.forEach(_topFifteenCategories, function(cat) {
                                        _xCatCategories.push(cat.categoryName);
                                        _seriesCategories.data.push(cat.totalAmount);
                                    });

                                    // Item Graph Data
                                    $scope.graphDataTopMenuItems.xAxis.categories = [];
                                    $scope.graphDataTopMenuItems.series = [];
                                    $scope.graphDataTopMenuItems.xAxis.categories = _xItemCategories;
                                    $scope.graphDataTopMenuItems.series.push(_seriesItems);

                                    console.log(_seriesItems);
                                    // Category Graph Data
                                    $scope.graphDataTopCategoryItems.xAxis.categories = [];
                                    $scope.graphDataTopCategoryItems.series = [];
                                    $scope.graphDataTopCategoryItems.xAxis.categories = _xCatCategories;
                                    $scope.graphDataTopCategoryItems.series.push(_seriesCategories);

                                    console.log(_seriesCategories);
                                    $scope.report.isGenerated = true;
                                    $rootScope.showLoaderOverlay = false;
                                    $scope.hasData = true;

                                });



                            });
                        }).catch(function() {
                            $rootScope.showLoaderOverlay = false;
                        });
                    }).catch(function() {
                        $rootScope.showLoaderOverlay = false;
                    });

                }).catch(function() {
                    growl.error("No Data Found!", {
                        ttl: 3000
                    });
                    $rootScope.showLoaderOverlay = false;
                    $scope.hasData = false;
                });
            }).catch(function(err) {
                growl.error("Something went wrong!", {
                    ttl: 3000
                });
                $rootScope.showLoaderOverlay = false;
                $scope.hasData = false;
            });
        };

        $scope.generateDashWaiters = function() {

            $scope.report.isGenerated = false;
            $rootScope.showLoaderOverlay = true;

            $scope.report.setDateRange().then(function(dates) {

                $scope.billResource.getDataRangeBills({
                    fromDate: dates.fromDate,
                    toDate: dates.toDate,
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: $scope.report.filters.deployment_id,
                    cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting
                }).$promise.then(function(bills) {

                    //console.log(bills);
                    $scope.report.addBills(bills).then(function() {
                        $scope.report.prepareBillsForReport().then(function() {
                            $scope.report.groupByWaiter().then(function(rows) {

                                var _rows = $filter('orderBy')(rows, '-totalAmount', false);
                                var _xTopWaiterCategories = [];
                                var _series = {
                                    name: 'Staff',
                                    data: []
                                }

                                /* Prepare Chart */
                                angular.forEach(_rows, function(row) {
                                    _xTopWaiterCategories.push(row.waiter);
                                    _series.data.push(row.totalAmount);
                                });

                                $scope.graphDataTopStaff.xAxis.categories = [];
                                $scope.graphDataTopStaff.series = [];
                                $scope.graphDataTopStaff.xAxis.categories = _xTopWaiterCategories;
                                $scope.graphDataTopStaff.series.push(_series);

                                $scope.hasData = true;
                                $scope.report.isGenerated = true;
                                $rootScope.showLoaderOverlay = false;

                            }).catch(function() {
                                $rootScope.showLoaderOverlay = false;
                            });
                        }).catch(function() {
                            $rootScope.showLoaderOverlay = false;
                        });
                    }).catch(function() {
                        $rootScope.showLoaderOverlay = false;
                    });

                }).catch(function() {
                    growl.error("No Data found!", {
                        ttl: 3000
                    });
                    $rootScope.showLoaderOverlay = false;
                    $scope.hasData = false;
                });
            }).catch(function(err) {
                growl.error("Something went wrong!", {
                    ttl: 3000
                });
                $rootScope.showLoaderOverlay = false;
                $scope.hasData = false;
            });
        };

        $scope.generateDashboardCustomers = function() {
            $scope.groupedCustomerData = [];
            $scope.report.isGenerated = false;
            //$rootScope.showLoaderOverlay = true;

            $scope.report.setDateRange().then(function(dates) {
                //console.log(dates);

                $scope.billResource.getDataRangeBills({
                    fromDate: dates.fromDate,
                    toDate: dates.toDate,
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: $scope.report.filters.deployment_id,
                    cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting
                }).$promise.then(function(bills) {

                    //console.log(bills);

                    $scope.report.addBills(bills).then(function() {
                        $scope.report.prepareBillsForReport().then(function() {

                            /* Group Bills By Each Day*/
                            $scope.report.groupBillsByDay().then(function(groupedByDayBills) {

                                //console.log(groupedByDayBills);
                                $scope.report.getTotalCustomers(groupedByDayBills).then(function(billsWithTotalCustomers) {
                                    //console.log(billsWithTotalCustomers);
                                    $scope.report.getNewCustomers(billsWithTotalCustomers).then(function(billsWithNewCustomers) {
                                        //console.log(JSON.stringify(billsWithNewCustomers));
                                        $scope.groupedCustomerData = billsWithNewCustomers;

                                        /* Start: Prepare Chart */

                                        // New Customers Series
                                        var _splineNewCust = {
                                            name: 'New Customers',
                                            type: 'spline',
                                            data: [],
                                            tooltip: {
                                                valueSuffix: ''
                                            }
                                        };
                                        // Repeat Customers Series
                                        var _splineRepeatCust = {
                                            name: 'Repeat Customers',
                                            type: 'spline',
                                            data: [],
                                            tooltip: {
                                                valueSuffix: ''
                                            }
                                        };

                                        // Build Categories & Series
                                        var _xCategories = [];
                                        angular.forEach($scope.groupedCustomerData, function(day) {
                                            var _day = moment(new Date(day.day[0], day.day[1], day.day[2])).format('YYYY-MM-DD');
                                            //console.log(_day);
                                            _xCategories.push(_day);
                                            _splineNewCust.data.push(parseInt(day.totalNewCustomers));
                                            _splineRepeatCust.data.push(parseInt(day.totalRepeatCustomers));
                                        });

                                        $scope.graphDataCusomtersNewRepeatDaily.xAxis[0].categories = [];
                                        $scope.graphDataCusomtersNewRepeatDaily.xAxis[0].categories = _xCategories;

                                        $scope.graphDataCusomtersNewRepeatDaily.series = [];
                                        $scope.graphDataCusomtersNewRepeatDaily.series.push(_splineNewCust);
                                        $scope.graphDataCusomtersNewRepeatDaily.series.push(_splineRepeatCust);

                                        /* End:   Prepare Chart */

                                        $scope.generateNewCustomerSaleNumbers($scope.groupedCustomerData);


                                        $scope.report.isGenerated = true;
                                        /*$scope.report.getRepeatCustomers(billsWithNewCustomers).then(function (billsWithRepeatCustomers) {
                                            console.log(billsWithRepeatCustomers);
                                        });*/
                                    });
                                });

                            });

                        });
                    });


                }).catch(function() {

                    // Bill Resource Error

                });

            }).catch(function() {

                // Set Date Range Error

            });
        };

        $scope.generateNewCustomerSaleNumbers = function(groupedCustomerData) {

            var _allNewCustBills = [],
                _allRepeatCustBills = [],
                _allNoCustomer = [];
            angular.forEach(groupedCustomerData, function(day) {
                // New Customers 
                var _newCustBills = [];
                angular.forEach(day.newCustomers, function(custId) {
                    var _custBills = _.filter(day.bills, {
                        _customer: {
                            customerId: custId
                        }
                    });
                    if (_custBills.length > 0) {
                        _newCustBills.push({
                            customerId: custId,
                            bills: _custBills,
                        });
                    }
                });

                // Repeat Customers
                var _repeatCustBills = [];
                angular.forEach(day.repeatCustomers, function(custId) {
                    var _custBills = _.filter(day.bills, {
                        _customer: {
                            customerId: custId
                        }
                    });
                    if (_custBills.length > 0) {
                        _repeatCustBills.push({
                            customerId: custId,
                            bills: _custBills,
                        });
                    }
                });



                var _totalNet = 0;
                angular.forEach(_newCustBills, function(newCust) {
                    angular.forEach(newCust.bills, function(bill) {
                        _totalNet += parseFloat(bill.totalNet);
                    });
                });

                var _totalNetRepeat = 0;
                angular.forEach(_repeatCustBills, function(rCust) {
                    angular.forEach(rCust.bills, function(bill) {
                        _totalNetRepeat += parseFloat(bill.totalNet);
                    });
                });

                var _totalNetNoCustomer = 0;
                angular.forEach(day.bills, function(bill) {
                    if (!_.has(bill._customer, 'customerId')) {
                        _totalNetNoCustomer += parseFloat(bill.totalNet);
                    }
                });

                _allNewCustBills.push({
                    day: day.day,
                    totalNet: _totalNet,
                    newCustomers: day.newCustomers.length
                });

                _allRepeatCustBills.push({
                    day: day.day,
                    totalNet: _totalNetRepeat,
                    repeatCustomers: day.repeatCustomers.length
                });

                _allNoCustomer.push({
                    day: day.day,
                    totalNet: _totalNetNoCustomer,
                });

            });

            //console.log(_allRepeatCustBills);

            // New Customers Series
            var _splineCustNo = {
                name: 'Numbers',
                type: 'spline',
                data: [],
                tooltip: {
                    valueSuffix: ''
                }
            };
            var _splineSaleCust = {
                name: 'Sales',
                type: 'column',
                yAxis: 1,
                data: [],
                tooltip: {
                    valueSuffix: ''
                }
            };
            // New Customers Series 

            // New Customers:  Build Categories & Series
            var _xCategories = [];
            angular.forEach(_allNewCustBills, function(day) {
                var _day = moment(new Date(day.day[0], day.day[1], day.day[2])).format('YYYY-MM-DD');
                //console.log(_day);
                _xCategories.push(_day);
                _splineCustNo.data.push(parseInt(day.newCustomers));
                _splineSaleCust.data.push(parseInt(day.totalNet));
            });

            $scope.graphDataCustomersNew.xAxis[0].categories = [];
            $scope.graphDataCustomersNew.xAxis[0].categories = _xCategories;

            $scope.graphDataCustomersNew.series = [];
            $scope.graphDataCustomersNew.series.push(_splineCustNo);
            $scope.graphDataCustomersNew.series.push(_splineSaleCust);
            // New Customers:  Build Categories & Series


            // Repeat Customers Series
            var _splineRepeatCustNo = {
                name: 'Numbers',
                type: 'spline',
                data: [],
                tooltip: {
                    valueSuffix: ''
                }
            };
            var _splineRepeatSaleCust = {
                name: 'Sales',
                type: 'column',
                yAxis: 1,
                data: [],
                tooltip: {
                    valueSuffix: ''
                }
            };
            // Repeat Customers Seriess
            // Repeat Customers: Build Categories & Series
            var _xRepeatCategories = [];
            angular.forEach(_allRepeatCustBills, function (day) {
                var _day = moment(new Date(day.day[0], day.day[1], day.day[2])).format('YYYY-MM-DD');
                _xRepeatCategories.push(_day);
                _splineRepeatCustNo.data.push(day.repeatCustomers);
                _splineRepeatSaleCust.data.push(day.totalNet);
            });

            $scope.graphDataCustomersRepeat.xAxis[0].categories = [];
            $scope.graphDataCustomersRepeat.xAxis[0].categories = _xCategories;

            $scope.graphDataCustomersRepeat.series = [];
            $scope.graphDataCustomersRepeat.series.push(_splineRepeatCustNo);
            $scope.graphDataCustomersRepeat.series.push(_splineRepeatSaleCust);
            // Repeat Customers: Build Categories & Series


            /* Build Stacked New & Repeat */
            var _newStack = {
                name: 'New',
                data: []
            };
            var _repeatStack = {
                name: 'Repeat',
                data: []
            };
            var _noCustStack = {
                name: 'No Customer',
                data: []
            };
            var _xStackedCategories = [];
            angular.forEach(_allNewCustBills, function(nday) {
                var _day = moment(new Date(nday.day[0], nday.day[1], nday.day[2])).format('YYYY-MM-DD');
                _xStackedCategories.push(_day);
                _newStack.data.push(nday.totalNet);
            });
            angular.forEach(_allRepeatCustBills, function(rday) {
                _repeatStack.data.push(rday.totalNet);
            });
            angular.forEach(_allNoCustomer, function(nday) {
                _noCustStack.data.push(nday.totalNet);
            });

            $scope.graphDataStackNewRepeatCustomers.xAxis.categories = [];
            $scope.graphDataStackNewRepeatCustomers.xAxis.categories = _xStackedCategories;
            $scope.graphDataStackNewRepeatCustomers.series = [];
            $scope.graphDataStackNewRepeatCustomers.series.push(_newStack);
            $scope.graphDataStackNewRepeatCustomers.series.push(_repeatStack);
            $scope.graphDataStackNewRepeatCustomers.series.push(_noCustStack);

            /* Build Stacked New & Repeat */
        };

        $scope.generateTableOccupancy = function () {

            $rootScope.showLoaderOverlay = true;

            $scope.report.isGenerated = false;

            $scope.report.setDateRange().then(function(dates) {

                $scope.billResource.getDataRangeBills({
                    fromDate: dates.fromDate,
                    toDate: dates.toDate,
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: $scope.report.filters.deployment_id,
                    cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting
                }).$promise.then(function(bills) {


                    $scope.report.addBills(bills, 'table').then(function() {
                        $scope.report.prepareBillsForReport().then(function() {

                            $scope.report.groupBillsByHoursTableOccupancy(dates).then(function(groupedInterval) {


                                /* Table Occupancy */
                                angular.forEach(groupedInterval, function (gBills) {
                                    var _occupied = 0;
                                    _.chain(gBills.bills).groupBy('_tableId').map(function (tableBills) {
                                        _occupied += 1;
                                    });
                                    gBills.occupied = _occupied;
                                });

                                var _xTableOccupancyCategories = [];
                                var _tableOccupancySales = {
                                    name: 'Sales',
                                    type: 'column',
                                    data: [],
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                }
                                var _tableOccupancy = {
                                    name: 'Occupied',
                                    type: 'spline',
                                    yAxis: 1,
                                    data: [],
                                    tooltip: {
                                        valueSuffix: ''
                                    }
                                };
                                angular.forEach(groupedInterval, function (interval) {
                                    if (_.has(interval, 'day')) {
                                        var _d = moment(new Date(interval.day[0], interval.day[1], interval.day[2])).format('YYYY-MM-DD');
                                        _xTableOccupancyCategories.push(_d);
                                        _tableOccupancy.data.push(interval.occupied);
                                        _tableOccupancySales.data.push(interval.totalNet);
                                    } else {
                                        var _iFrom = moment(interval.from).format('hh:mm a');
                                        var _iTo = moment(interval.to).format('hh:mm a');
                                        _xTableOccupancyCategories.push(_iFrom + ' - ' + _iTo);
                                        _tableOccupancy.data.push(interval.occupied);
                                        _tableOccupancySales.data.push(interval.totalNet);
                                    }
                                });

                                $scope.graphDataTableOccupancy.xAxis[0].categories = [];
                                $scope.graphDataTableOccupancy.xAxis[0].categories = _xTableOccupancyCategories;

                                $scope.graphDataTableOccupancy.series = [];
                                $scope.graphDataTableOccupancy.series.push(_tableOccupancy);
                                $scope.graphDataTableOccupancy.series.push(_tableOccupancySales);

                                /* Table Occupancy */



                                /* Table Number Net Sales */
                                var _tables = [];
                                for(var i = 0; i < 30; i++) {
                                    _tables[i] = {
                                        tableNumber: i+1,
                                        totalNet: 0,
                                        totalDiscount: 0,
                                        totalTax: 0,
                                        totalAmount: 0
                                    }; 
                                }
                                _.chain($scope.report.bills).groupBy('_tableId').map(function (tableNumberBills) {
                                    var _totalNet = 0;
                                    angular.forEach(tableNumberBills, function (bill) {
                                        _totalNet += bill.totalNet;
                                    });
                                    var _f = _.filter(_tables, {tableNumber: parseInt(tableNumberBills[0]._tableId)});
                                    _f[0].totalNet = _totalNet;
                                });
                                    
                                var _xTableNumberCategories = [];
                                var _tableNumberSaleSeries = {
                                    name: 'Net Sales',
                                    type: 'column',
                                    data: [],
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                };
                                angular.forEach(_tables, function(table) {
                                    _xTableNumberCategories.push(table.tableNumber);
                                    _tableNumberSaleSeries.data.push(table.totalNet);
                                });

                                $scope.graphDataTableNumberNetSales.xAxis[0].categories = [];
                                $scope.graphDataTableNumberNetSales.xAxis[0].categories = _xTableNumberCategories;

                                $scope.graphDataTableNumberNetSales.series = [];
                                $scope.graphDataTableNumberNetSales.series.push(_tableNumberSaleSeries);

                                /* Table Number Net Sales */


                                if (bills.length > 0) {
                                    $scope.hasData = true;
                                    $rootScope.showLoaderOverlay = false;
                                } else {
                                    $scope.hasData = false;
                                    $rootScope.showLoaderOverlay = false;
                                }

                                


                            });

                        });
                    }).catch(function (err) {
                        $rootScope.showLoaderOverlay = false;
                        $scope.hasData = false;
                    });


                }).catch(function (err) {

                });

            }).catch(function (err) {

            });
        };


        /* Compare Modals */

        $scope.compareSalesModal = function() {

            $modal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'app/dashboard/tmplt_compareOutlets.html',
                controller: function($scope, $modalInstance, $resource, deployments, tenant_id, $q, growl) {

                    $scope.showLoader = false;

                    $scope.selectedDate = {
                        startDate: new Date(),
                        endDate: new Date()
                    };

                    $scope.datePickerOptions = {
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        }
                    };

                    $scope.outlets = [{
                        bills: [],
                        report: new Report()
                    }, {
                        bills: [],
                        report: new Report()
                    }, {
                        bills: [],
                        report: new Report()
                    }];


                    angular.forEach($scope.outlets, function(outlet) {
                        //outlet.report.filters.autoDateRange = 'today';
                        outlet.report.filters.fromDate = new Date().toISOString();
                        outlet.report.filters.toDate = new Date().toISOString();
                    });

                    //console.log($scope.outlets);

                    $scope.deployments = deployments;

                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.compareWithSelected = function() {

                        $scope.showLoader = true;

                        /* Validate: Deployment Selection */
                        var _validateDeployments = [];
                        angular.forEach($scope.outlets, function(outlet) {
                            _validateDeployments.push(
                                outlet.report.validateDeploymentSelection()
                            );
                        });
                        $q.all(_validateDeployments).then(function(values) {
                            angular.forEach(values, function(val, index) {
                                $scope.outlets[index].isValidDeployment = val;
                            });

                            /* Validate: Date Selection */
                            var _validateDeploymentDates = [];
                            angular.forEach($scope.outlets, function(outlet) {
                                _validateDeploymentDates.push(
                                    outlet.report.validateDateSelection()
                                );
                            });

                            $q.all(_validateDeploymentDates).then(function(values) {
                                angular.forEach(values, function(val, index) {
                                    $scope.outlets[index].isValidDates = val;
                                });

                                var dCount = 0;
                                angular.forEach($scope.outlets, function(outlet) {
                                    if (outlet.report.filters.deployment_id != null) {
                                        dCount++;
                                    }
                                });


                                /* Start: Preparing Compare */

                                // Validation: Check for min 2 or more outlets
                                if (dCount > 1) {

                                    // Validate: Check if Duplicate Outlet selected
                                    var _d = [],
                                        hasDuplicateDeploymentId = false;
                                    angular.forEach($scope.outlets, function(outlet) {
                                        if (_d.indexOf(outlet.report.filters.deployment_id) === -1) {
                                            _d.push(outlet.report.filters.deployment_id);
                                        } else {
                                            hasDuplicateDeploymentId = true;
                                        }
                                    });

                                    if (!hasDuplicateDeploymentId) {

                                        var _compareCols = [];
                                        angular.forEach($scope.outlets, function(outlet) {
                                            if (outlet.isValidDeployment) {
                                                _compareCols.push(outlet.report.getCompareData());
                                            }
                                        });
                                        $q.all(_compareCols).then(function(compareCols) {

                                            //console.log(compareCols);
                                            $scope.showLoader = false;
                                            $modalInstance.close({
                                                selectedTimeSlot: $scope.selectedDate,
                                                columns: compareCols
                                            });

                                        }).catch(function(err) {
                                            growl.error("Could not fetch Outlet Data!", {
                                                ttl: 3000
                                            });
                                        });

                                    } else {

                                        $scope.showLoader = false;
                                        growl.error("Duplicate Outlet found, please select another.", {
                                            ttl: 3000
                                        });
                                    }



                                } else {

                                    growl.error("Minimum 2 Outlets need to be selected!", {
                                        ttl: 3000
                                    });
                                    $scope.showLoader = false;

                                }
                                /* End: Preparing Compare */


                            });


                        });
                    };

                    $scope.validateDuplicateDeploymentSelection = function() {
                        var defer = $q.defer();
                        var _d = [];
                        $timeout(function() {
                            for (var i = 0; i < $scope.outlets.length; i++) {
                                if ($scope.outlets[i].report.filters.deployment_id != null) {
                                    _d.push({
                                        deployment_id: $scope.outlets[i].report.filters.deployment_id
                                    });
                                    var _f = _.filter(_d, {
                                        deployment_id: $scope.outlets[i].report.filters.deployment_id
                                    });
                                    if (_f.length > 1) {
                                        $scope.outlets[i].report.filters.deployment_id = null;
                                        defer.resolve({
                                            error: true,
                                            message: "Duplicate Deployment ID!",
                                            outletIndex: i
                                        });
                                        break;
                                    }
                                }
                            }
                            defer.resolve({
                                error: false,
                                message: ""
                            });
                        });
                        return defer.promise;
                    };

                    $scope.onCompareDeploymentChange = function(index) {
                        /* Get CutOff Time */
                        if ($scope.outlets[index]['report']['filters']['deployment_id'] != null) {
                            var _d = _.filter($scope.deployments, {
                                _id: $scope.outlets[index]['report']['filters']['deployment_id']
                            });
                            var _setting = _.filter(_d[0].settings, {
                                name: 'day_sale_cutoff'
                            });
                            $scope.outlets[index]['report']['filters']['cutOffTimeSetting'] = _setting[0].value;
                            $scope.outlets[index]['report']['filters']['tenant_id'] = localStorageService.get('tenant_id');
                            $scope.outlets[index]['report']['filters']['name'] = _d[0].name;
                        }
                    };

                    $scope.changeDateRange = function (selectedDate) {
                        console.log('Date Range Changed', selectedDate);

                        var _s = moment(selectedDate.startDate).toArray();
                        var _e = moment(selectedDate.endDate).toArray();

                        var _fromDate = new Date(_s[0], _s[1], _s[2]).toISOString();
                        var _toDate = new Date(_e[0], _e[1], _e[2]).toISOString();

                        $scope.selectedDate.startDate = _fromDate;
                        $scope.selectedDate.endDate = _toDate;

                        angular.forEach($scope.outlets, function(outlet) {
                            //outlet.report.filters.autoDateRange = 'today';
                            outlet.report.filters.fromDate = null;
                            outlet.report.filters.toDate = null;
                        });

                        angular.forEach($scope.outlets, function(outlet) {
                            //outlet.report.filters.autoDateRange = 'today';
                            outlet.report.filters.fromDate = _fromDate;
                            outlet.report.filters.toDate = _toDate;
                        });

                    };

                },
                size: 'lg',
                resolve: {
                    deployments: function() {
                        return $scope.deployments;
                    },
                    tenant_id: function() {
                        return localStorageService.get('tenant_id');
                    }
                }
            }).result.then(function(compareCols) {

                $scope.compareViewCols = compareCols.columns;
                $scope.compareSlot = compareCols.selectedTimeSlot;
                //console.log('Cols', $scope.compareViewCols);

                /* Start: Prepare Charts */

                /*=== Sales Spline Multi Deployments ===*/
                var _salesCompareXCategoriesHourly = [];
                angular.forEach($scope.compareViewCols[0].hourIntervalBills, function(hourBreakUp) {
                    if (_.has(hourBreakUp, 'day')) {
                        var _day = moment(new Date(hourBreakUp.day[0], hourBreakUp.day[1], hourBreakUp.day[2]));
                        var _cFT = _day.format('YYYY-MM-DD');
                    } else {
                        var _f = moment(new Date(hourBreakUp.from[0], hourBreakUp.from[1], hourBreakUp.from[2], hourBreakUp.from[3], hourBreakUp.from[4]));
                        var _t = moment(new Date(hourBreakUp.to[0], hourBreakUp.to[1], hourBreakUp.to[2], hourBreakUp.to[3], hourBreakUp.to[4]));
                        var _cFT = _f.format("hh:mm a") + ' - ' + _t.format("hh:mm a");
                    }
                    _salesCompareXCategoriesHourly.push(_cFT);
                });

                $scope.graphCompareOutletSaleHourly.xAxis[0].categories = [];
                $scope.graphCompareOutletSaleHourly.xAxis[0].categories = _salesCompareXCategoriesHourly;

                var _salesCompareSplines = [];
                angular.forEach($scope.compareViewCols, function(outlet) {
                    var _s = {
                        name: outlet.name,
                        type: 'spline',
                        data: [],
                        tooltip: {
                            valueSuffix: ''
                        }
                    };
                    angular.forEach(outlet.hourIntervalBills, function(hourVal) {
                        //console.log($filter('number')(hourVal.totalNet, 2));
                        _s.data.push(hourVal.totalNet);
                    });
                    _salesCompareSplines.push(_s);
                });

                $scope.graphCompareOutletSaleHourly.series = [];
                $scope.graphCompareOutletSaleHourly.series = _salesCompareSplines;

                /*=== Sales Spline Multi Deployments ===*/


                /*=== Orders Spline Multi Deployments ===*/
                var _ordersCompareXCategoriesHourly = [];
                angular.forEach($scope.compareViewCols[0].hourIntervalBills, function(hourBreakUp) {
                    if (_.has(hourBreakUp, 'day')) {
                        var _day = moment(new Date(hourBreakUp.day[0], hourBreakUp.day[1], hourBreakUp.day[2]));
                        var _cFT = _day.format('YYYY-MM-DD');
                    } else {
                        var _f = moment(new Date(hourBreakUp.from[0], hourBreakUp.from[1], hourBreakUp.from[2], hourBreakUp.from[3], hourBreakUp.from[4]));
                        var _t = moment(new Date(hourBreakUp.to[0], hourBreakUp.to[1], hourBreakUp.to[2], hourBreakUp.to[3], hourBreakUp.to[4]));
                        var _cFT = _f.format("hh:mm a") + ' - ' + _t.format("hh:mm a");
                    }
                    _ordersCompareXCategoriesHourly.push(_cFT);
                });

                $scope.graphCompareOutletOrders.xAxis[0].categories = [];
                $scope.graphCompareOutletOrders.xAxis[0].categories = _ordersCompareXCategoriesHourly;

                var _orderCompareSplines = [];
                angular.forEach($scope.compareViewCols, function(outlet) {
                    var _s = {
                        name: outlet.name,
                        type: 'spline',
                        data: [],
                        tooltip: {
                            valueSuffix: ''
                        }
                    };
                    angular.forEach(outlet.hourIntervalBills, function(hourVal) {
                        //console.log(hourVal.totalBills);
                        _s.data.push(hourVal.totalBills);
                    });
                    _orderCompareSplines.push(_s);
                });

                $scope.graphCompareOutletOrders.series = [];
                $scope.graphCompareOutletOrders.series = _orderCompareSplines;
                /*=== Orders Spline Multi Deployments ===*/

                /* End:   Prepare Charts */

                /*angular.forEach($scope.compareViewCols, function(col) {
                    if (col.error === true) {
                        $scope.hasChartData = false;
                    }
                });*/

                $scope.showCompareView = true;
                if ($scope.compareViewCols[0].error) {
                    $scope.hasData = false;
                } else {
                    $scope.hasData = true;
                }
                //console.log('hasChartData', $scope.hasChartData);
                //console.log($scope.compareViewCols);

            });
        };

        /* Compare Modals */


        $scope.onDeploymentChange = function() {

            /* Get CutOff Time */
            if ($scope.report.filters.deployment_id != null) {
                var _d = _.filter($scope.deployments, {
                    _id: $scope.report.filters.deployment_id
                });
                var _setting = _.filter(_d[0].settings, {
                    name: 'day_sale_cutoff'
                });
                $scope.report.filters.cutOffTimeSetting = _setting[0].value;
            }

            $scope.onDateRangeChange();
        };

        $scope.onDateRangeChange = function() {
            
            $scope.showCompareView = false;
            
            if ($scope.report.filters.fromDate === null && $scope.report.filters.toDate === null) {
                $scope.report.filters.fromDate = $scope.selectedDate.startDate.toISOString();
                $scope.report.filters.toDate = $scope.selectedDate.endDate.toISOString();
            };

            switch ($state.current.name) {
                case 'dashboard.dash_time':
                    $scope.report.reset();
                    $scope.generateDashboard();
                    break;
                case 'dashboard.dash_menuItems':
                    $scope.report.reset();
                    $scope.generateDashMenuItems();
                    break;
                case 'dashboard.dash_serviceType':
                    $scope.report.reset();
                    $scope.generateDashServiceType();
                    break;
                case 'dashboard.dash_staffWaiters':
                    $scope.report.reset();
                    $scope.generateDashWaiters();
                    break;
                case 'dashboard.dash_firstTimeCustomer':
                    $scope.report.reset();
                    $scope.generateDashboardCustomers();
                    break;
                case 'dashboard.dash_tableOccupancy':
                    $scope.report.reset();
                    $scope.generateTableOccupancy();
                    break;
            };
        };

        $scope.changeDateRange = function (selectedDate) {

            var _s = moment(selectedDate.startDate).toArray();
            var _e = moment(selectedDate.endDate).toArray();

            $scope.report.filters.fromDate = null;
            $scope.report.filters.toDate = null;

            $scope.report.filters.fromDate = new Date(_s[0], _s[1], _s[2]).toISOString();
            $scope.report.filters.toDate = new Date(_e[0], _e[1], _e[2]).toISOString();

            $scope.selectedDate.startDate = $scope.report.filters.fromDate;
            $scope.selectedDate.endDate = $scope.report.filters.toDate;

            $scope.onDateRangeChange();
        }; 


        /* Bill Resource */
        $scope.billResource = $resource('/api/bills/:controller', {}, {
            getDataRangeBills: {
                method: 'GET',
                isArray: true,
                params: {
                    controller: 'getDataRangeBills'
                }
            },
            getDataRangeBillsCustom: {
                method: 'POST',
                isArray: true,
                params: {
                    controller: 'getDataRangeBillsCustom'
                }
            }
        });

        $scope.graphDataSalesHourly = {
            chart: {
                renderTo: 'container-graph-SalesHourly',
                zoomType: 'xy'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Total Sales (Hourly)'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                min:0,
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'Sales Amount - Hourly (Rs)',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: true

            }, { // Secondary yAxis
                min:0,
                gridLineWidth: 0,
                title: {
                    text: 'Sales Amount - Total',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Bi-Hourly Sales',
                type: 'column',
                yAxis: 1,
                data: [310, 3625, 1920, 5280, 26250, 13750, 14252],
                tooltip: {
                    valueSuffix: ''
                }

            }, {
                name: 'Total Sales',
                type: 'spline',
                data: [310, 3935, 5855, 11135, 37385, 51135, 65387],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };

        $scope.graphDataOrdersHourly = {
            chart: {
                renderTo: 'container-graph-OrdersHourly',
                zoomType: 'xy'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Number of Bills (Hourly)'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                min:0,
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'No. of Orders (Total)',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: true

            }, { // Secondary yAxis
                min:0,
                gridLineWidth: 0,
                title: {
                    text: 'No. of Orders (Hourly)',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Bi-Hourly Order',
                type: 'column',
                yAxis: 1,
                data: [1, 8, 4, 13, 45, 31, 13],
                tooltip: {
                    valueSuffix: ''
                }

            }, {
                name: 'Total Order',
                type: 'spline',
                data: [1, 9, 13, 26, 71, 102, 115],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };

        $scope.graphDataSalesServiceType = {
            chart: {
                renderTo: 'container-graph-SalesByTabs',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Sales by Service Type'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y:.2f}/-</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{series.name}: {point.y:.2f}/-'
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Tab Share',
                data: [
                    ['Rooftop Table', 5000],
                    ['Table', 2500],
                    ['Take Out', 3300]
                ]
            }]
        };

        $scope.graphDataSalesByWaiters = {
            chart: {
                renderTo: 'container-graph-SalesByWaiters',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Sales by Waiters'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y:.2f}/-</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{series.name}: {point.y}/-'
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Waiter Share',
                data: [
                    ['Rooftop Table', 5000],
                    ['Table', 2500],
                    ['Take Out', 3300]
                ]
            }]
        }

        $scope.graphDataTopMenuItems = {
            chart: {
                renderTo: 'container-graph-TopSellingMenuItems',
                type: 'column'
            },
            title: {
                text: 'Top Selling Menu Items'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Mutton Biryani',
                    'Chicken 65',
                    'Chicken Biryani',
                    'Mutton Biryani (Student Pack)',
                    'Double ka Meetha'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Sales Amount (Rs)'
                }
            },
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            scrollbar: {
                enabled: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Menu Items',
                data: [5000, 3210, 3000, 2800, 1500]

            }]
        };

        $scope.graphDataTopCategoryItems = {
            chart: {
                renderTo: 'container-graph-TopSellingCategoryItems',
                type: 'column'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Top Selling Categories'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Biryani',
                    'Sides',
                    'Deserts'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Sales Amount (Rs)'
                }
            },
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            scrollbar: {
                enabled: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Category',
                data: [5000, 3210, 3000]

            }]
        };

        $scope.graphDataTopStaff = {
            chart: {
                renderTo: 'container-graph-TopSellingStaff',
                type: 'column'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Sales By Staff'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Ajeet',
                    'Pushpinder',
                    'Agam',
                    'Choudhari',
                    'Thapa',
                    'Dheeraj',
                    'Sandeep',
                    'Bhagwansingh',
                    'Raju',
                    'Subhash'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Sales Amount (Rs)'
                }
            },
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            scrollbar: {
                enabled: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Staff',
                data: [10000, 9500, 8100, 8050, 7500, 7000, 6500, 6000, 5500, 5000]
            }]
        };

        $scope.graphDataCusomtersNewRepeatDaily = {
            chart: {
                renderTo: 'container-graph-NewRepeatCustomersDaily',
                zoomType: 'xy'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'First Time Customers Vs Repeat Customers'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Secondary yAxis
                min: 0,
                gridLineWidth: 0,
                title: {
                    text: 'No. of Customers',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'New Customers',
                type: 'spline',
                data: [1, 9, 13, 26, 71, 102, 115],
                tooltip: {
                    valueSuffix: ''
                }
            }, {
                name: 'Repeat Customers',
                type: 'spline',
                data: [5, 100, 200, 50, 60, 25, 30],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };

        $scope.graphDataCustomersNew = {
            chart: {
                renderTo: 'container-graph-NewCustomers',
                zoomType: 'xy'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'First Time Customers - Sale, Numbers'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                min: 0,
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'Sale Amount',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: true

            }, { // Secondary yAxis
                min:0,
                gridLineWidth: 0,
                title: {
                    text: 'No. of Customers',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Sales',
                type: 'column',
                yAxis: 1,
                data: [1, 8, 4, 13, 45, 31, 13],
                tooltip: {
                    valueSuffix: ''
                }

            }, {
                name: 'Numbers',
                type: 'spline',
                data: [1, 9, 13, 26, 71, 102, 115],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };

        $scope.graphDataCustomersRepeat = {
            chart: {
                renderTo: 'container-graph-RepeatCustomers',
                zoomType: 'xy'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Repeat Customers - Sale, Numbers'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                min: 0,
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'Sale Amount',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: true

            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'No. of Customers',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Sales',
                type: 'column',
                yAxis: 1,
                data: [1, 8, 4, 13, 45, 31, 13],
                tooltip: {
                    valueSuffix: ''
                }

            }, {
                name: 'Numbers',
                type: 'spline',
                data: [1, 9, 13, 26, 71, 102, 115],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };

        $scope.graphDataStackNewRepeatCustomers = {
            chart: {
                renderTo: 'container-graph-StackedNewRepeatCustomers',
                type: 'column'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Sales - New & Repeat Customers'
            },
            xAxis: {
                categories: ['2015-05-20', '2015-05-21', '2015-05-22', '2015-05-23', '2015-05-24']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Sales Amount'
                },
                stackLabels: {
                    enabled: true,
                    pointFormat: "{point.y:.2f}",
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'center',
                x: 0,
                verticalAlign: 'top',
                y: 20,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'New',
                data: [5, 3, 4, 7, 2]
            }, {
                name: 'Repeat',
                data: [2, 2, 3, 2, 1]
            }]
        };

        $scope.graphDataTableOccupancy = {
            chart: {
                renderTo: 'container-graph-TableOccupancy',
                zoomType: 'xy'
            },
            title: {
                text: 'Table Occupancy vs Sales'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                min: 0,
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'Sales Amount',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: true

            }, { // Secondary yAxis
                gridLineWidth: 0,
                min:0,
                title: {
                    text: 'No. of Tables',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Sales',
                type: 'column',
                data: [1, 8, 4, 13, 45, 31, 13],
                tooltip: {
                    valueSuffix: ''
                }

            }, {
                name: 'Occupied',
                type: 'spline',
                yAxis: 1,
                data: [1, 9, 13, 26, 71, 102, 115],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };

        $scope.graphDataTableNumberNetSales = {
            chart: {
                renderTo: 'container-graph-TableNumberNetSales',
                zoomType: 'xy'
            },
            exporting: false,
            title: {
                text: 'Net Sales By Table Number'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Net Sales',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value:.2f}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Net Sales',
                type: 'column',
                data: [1, 8, 4, 13, 45, 31, 13],
                tooltip: {
                    valueSuffix: ''
                }

            }]
        };


        /* Compare Graphs */
        $scope.graphCompareOutletSaleHourly = {
            chart: {
                renderTo: 'container-graph-CompareSalesHourly',
                zoomType: 'xy'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Total Sale Across Outlets'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                min: 0,
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'Total Sale',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: false

            }],
            tooltip: {
                shared: true,
                pointFormat: "{series.name} - <strong>{point.y:.2f}</strong> <br/>"
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Hauz Khas',
                type: 'spline',
                data: [0, 0, 0, 3000, 2500, 5002, 1250],
                tooltip: {
                    valueSuffix: ''
                }
            }, {
                name: 'Green Park',
                type: 'spline',
                data: [150, 2400, 1000, 5000, 3500, 6000, 2300],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };

        $scope.graphCompareOutletOrders = {
            chart: {
                renderTo: 'container-graph-CompareOrders',
                zoomType: 'xy'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: 'Orders Across Outlets'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['12:00 PM - 02:00 PM', '02:00 PM - 04:00 PM', '04:00 PM - 06:00 PM', '06:00 PM - 08:00 PM', '08:00 PM - 10:00 PM', '10:00 PM - 12:00 PM', '12:00 AM - 02:00 AM'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                min:0,
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'Total Sale',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: false

            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Hauz Khas',
                type: 'spline',
                data: [0, 0, 0, 3000, 2500, 5002, 1250],
                tooltip: {
                    valueSuffix: ''
                }
            }, {
                name: 'Green Park',
                type: 'spline',
                data: [150, 2400, 1000, 5000, 3500, 6000, 2300],
                tooltip: {
                    valueSuffix: ''
                }
            }]
        };
        /* Compare Graphs */


        /* Auto Generate Today Dash */
        $scope.report.filters.deployment_id = $scope.deployments[0]._id;
        var _setting = _.filter($scope.deployments[0].settings, {
            name: 'day_sale_cutoff'
        });
        $scope.report.filters.cutOffTimeSetting = _setting[0].value;
        //$scope.report.filters.autoDateRange = 'today';
        //$scope.report.filters.fromDate = new Date();
        //$scope.report.filters.endDate = new Date();
        $scope.onDateRangeChange();
        /* Auto Generate Today Dash */


    }]);
