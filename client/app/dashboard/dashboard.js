'use strict';

angular.module('posistApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'app/dashboard/dashboard.html',
                controller: 'DashboardCtrl',
                resolve: {
                    currentUser: ['$state', '$stateParams', 'Auth',
                        function($state, $stateParams, Auth) {
                            return Auth.getCurrentUser().$promise.then(function(user) {
                                return user;
                            });
                        }
                    ],
                    settings: ['$q', 'localStorageService', '$timeout', function($q, localStorageService, $timeout) {

                        var d = $q.defer();
                        var _settings = {};
                        $timeout(function() {
                            _settings = localStorageService.get('settings');
                            d.resolve(_settings);
                        });

                        return d.promise;

                    }],
                    superCategories: ['$q', 'Supercategory', 'localStorageService', function($q, Supercategory, localStorageService) {
                        var d = $q.defer();
                        Supercategory.get({
                            tenant_id: localStorageService.get('tenant_id')
                        }, function(scs) {
                            //console.log(scs);
                            d.resolve(scs);
                        }, function(err) {
                            growl.error('Could not fetch Super Categories!', {
                                ttl: 3000
                            });
                            d.reject();
                        });
                        return d.promise;
                    }],
                    categories: ['$q', 'Category', 'localStorageService', '$timeout', function($q, Category, localStorageService, $timeout) {
                        var d = $q.defer();
                        Category.get({
                            tenant_id: localStorageService.get('tenant_id')
                        }, function(categories) {
                            //console.log(categories);
                            d.resolve(categories);
                        }, function(err) {
                            growl.error('Could not fetch Categories!', {
                                ttl: 3000
                            });
                            d.reject();
                        });
                        return d.promise;
                    }],
                    deployments: ['$q', 'Deployment', 'localStorageService', function($q, Deployment, localStorageService) {
                        var d = $q.defer();
                        Deployment.get({
                            tenant_id: localStorageService.get('tenant_id'),
                            isMaster: false
                        }, function(deployments) {
                            //console.log(deployments);
                            d.resolve(deployments);
                        }, function(err) {
                            d.reject();
                        });
                        return d.promise;
                    }],
                    tabs: ['$q', 'Tab', 'localStorageService', function($q, Tab, localStorageService) {
                        var d = $q.defer();

                        /*Tab.getTenantTabs({tenant_id: localStorageService.get('tenant_id')}, function (tabs) {
                          //console.log(tabs);
                          d.resolve(tabs);
                        }, function (err) {
                          d.reject();
                        });*/
                        d.resolve();

                        return d.promise;
                    }]
                }
            })
            .state('dashboard.dash_time', {
                url: '/time',
                templateUrl: 'app/dashboard/dash/_time.html',
                controller: 'DashboardCtrl',
                parent: 'dashboard'
            })
            .state('dashboard.dash_foodCategories', {
                url: '/foodcategories',
                templateUrl: 'app/dashboard/dash/_foodCategories.html',
                controller: 'DashboardCtrl',
                parent: 'dashboard'
            })
            .state('dashboard.dash_menuItems', {
                url: '/menuitems',
                templateUrl: 'app/dashboard/dash/_menuItems.html',
                controller: 'DashboardCtrl',
                parent: 'dashboard'
            })
            .state('dashboard.dash_serviceType', {
                url: '/servicetype',
                templateUrl: 'app/dashboard/dash/_serviceType.html',
                controller: 'DashboardCtrl',
                parent: 'dashboard'
            })
            .state('dashboard.dash_staffWaiters', {
                url: '/staffwaiters',
                templateUrl: 'app/dashboard/dash/_staffWaiters.html',
                controller: 'DashboardCtrl',
                parent: 'dashboard'
            })
            .state('dashboard.dash_firstTimeCustomer', {
                url: '/customers',
                templateUrl: 'app/dashboard/dash/_firstTimeCustomers.html',
                controller: 'DashboardCtrl',
                parent: 'dashboard'
            })
            .state('dashboard.dash_tableOccupancy', {
                url: '/tableoccupancy',
                templateUrl: 'app/dashboard/dash/_tableOccupancy.html',
                controller: 'DashboardCtrl',
                parent: 'dashboard'
            });

    });
