'use strict';

angular.module('posistApp')
  .controller('CustomerCtrl', function ($scope, $modalInstance, number, growl, Customer) {

    /* $scope.selected = {
     item: $scope.items[0]
     };*/
    console.log(number);

    $scope.addressTypes = [{id: 0, type: "Residence"}, {id: 1, type: "Office"}];
    //  $scope.address.type="Residence";
    $scope.address = {
      type: "Residence",
      line1: "",
      line2: "",
      city: "",
      state: "",
      postcode: "",
      country: "India"
    };
    /* $scope.newCustomer = {
     mobile: number,
     addresses: []
     };*/
    console.log(number)
    $scope.newCustomer = angular.copy(number);
    if (!_.has($scope.newCustomer, 'addresses')) $scope.newCustomer.addresses = [];

    /*$scope.newCustomer.addresses.push(angular.copy($scope.address));*/

    $scope.addAddress = function () {
      $scope.newCustomer.addresses.push(angular.copy($scope.address));
    };


    $scope.createNewCustomer = function (form) {
      var newAddress = [];
      if (form.$valid) {
        _.forEach($scope.newCustomer.addresses, function (_add) {
          if (!(_add.line1 == '' && _add.line2 == '' && _add.city == '' && _add.state == '' && _add.postcode == '')) {
            newAddress.push(_add);
          }
        });
        $scope.newCustomer.addresses = newAddress;
        // if ($scope.newCustomer.addresses.length > 0) {
        //     if ($scope.newCustomer)
        $modalInstance.close($scope.newCustomer);
        /* } else {
         growl.success("Please add Customer Address", {ttl: 3000});
         }*/
      } else {
        growl.error("Something went wrong while filling the form", {ttl: 3000});
      }
    };
    $scope.updateCustomer = function (form) {
      var newAddress = [];
      if (form.$valid) {
        _.forEach($scope.newCustomer.addresses, function (_add) {
          if (!(_add.line1 == '' && _add.line2 == '' && _add.city == '' && _add.state == '' && _add.postcode == '')) {
            newAddress.push(_add);
          }
        });
        $scope.newCustomer.addresses = newAddress;
        Customer.update({id: $scope.newCustomer._id}, $scope.newCustomer, function (result) {
          console.log(result)
          $modalInstance.close(result);
        })

      } else {
        growl.error("Something went wrong while filling the form", {ttl: 3000});
      }
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    $scope.open = function ($event) {
      console.log($event);
      $event.preventDefault();
      $event.stopPropagation();

    }

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    $scope.open = function ($event) {
      console.log($event);
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };
    $scope.openMA = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened2 = true;
    };
    $scope.deleteAddress = function (index) {
      console.log(index);
      $scope.newCustomer.addresses.splice(index, 1);
    };
    $scope.maxDate = new Date();
    $scope.maxDateMA = new Date();

    $scope.getFocused = function (event) {
      $location.hash(event);
      $anchorScroll();
    }
  })
;
