/**
 * Created by Ranjeet Sinha on 4/6/2015.
 */
'use strict';
angular.module('posistApp')
.controller('customerAddressCtrl',function($scope,$modalInstance,customerAddresses,growl){
       console.log(customerAddresses);
        $scope.addresses=customerAddresses;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    $scope.selectedAddress=function(address){
        console.log(address);
        $modalInstance.close(address);
        }
    });