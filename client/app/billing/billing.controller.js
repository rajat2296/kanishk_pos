'use strict';

angular.module('posistApp')
  .controller('BillingCtrl', ['$modalStack','$scope', '$rootScope', '$state', '$modal', '$timeout', 'currentUser', '$webSql', 'Utils', 'Bill', 'Bills', 'socket', 'growl', 'lzw', 'printer', '$location', 'Auth', 'Deployment', 'Category', 'offlineBills', 'localStorageService', 'User', 'Customer', 'Sync', 'Tab', 'Pagination', 'settings', 'syncAll', '$q', '$interval', 'dataTables', '$templateCache', '$compile', '$window', '$resource','billResource','appcache','$aside','$ngBootbox','CodeApplied','mySocket','OnlineOrder','Advancebooking','Feedback','nSocket','$http','intercom','Item','Fullscreen','CardTransaction','delivery','loyalty','ThirdPartyService','barExchange','BillPrintService','UrbanPiper',function ($modalStack, $scope, $rootScope, $state, $modal, $timeout, currentUser, $webSql, Utils, Bill, Bills, socket, growl, lzw, printer, $location, Auth, Deployment, Category, offlineBills, localStorageService, User, Customer, Sync, Tab, Pagination, settings, syncAll, $q, $interval, dataTables, $templateCache, $compile, $window, $resource,billResource,appcache,$aside,$ngBootbox,CodeApplied,mySocket,OnlineOrder,Advancebooking,Feedback,nSocket,$http,intercom,Item,Fullscreen,CardTransaction,delivery,loyalty,ThirdPartyService,barExchange,BillPrintService,UrbanPiper) {
    var audio = document.getElementById('xyz');
    var myAudio;
    //console.log(dataTables);
    $scope.windowWidth = $window.innerWidth;
    $rootScope.winWidth = $window.innerWidth;
    $scope.isCollapsed = true;
    $scope.modalInstance = {};

    var setting= _.filter(localStorageService.get('settings'), {'name': 'enable_delivery_boy_app','selected': true});
    $scope.tp_integrated=(setting.length>0)?true:false;   //flag for tp integration
    $scope.zomato_integrated=(Utils.hasSetting('zomato_trace_username',localStorageService.get('settings')) && Utils.hasSetting('zomato_trace_apikey',localStorageService.get('settings')))?true:false;
    var zomato_apiKey=Utils.getSettingValue('zomato_trace_apikey',localStorageService.get('settings'));
    var zomato_username=Utils.getSettingValue('zomato_trace_username',localStorageService.get('settings'));
      console.log("zomato",$scope.zomato_integrated);
    var deliveryOrders=[];
    var deploymentPartners=[];
    var deliveryPartners=[];


    $scope.goFullscreen = function () {
      if (Fullscreen.isEnabled())
        Fullscreen.cancel();
      else
        Fullscreen.all();
    }
    $scope.isRound = function () {
      var flag = true;
      if (Utils.hasSetting('disable_roundoff', $scope.settings)) {
        flag = false;
      }
      return flag;
    }
    var modalInstance;
    $scope.forCustSearch = false;
    if ($window.innerWidth <= 1020) {
      $scope.posLogo = '/assets/images/posist-glificon-logo.jpg'
      if (navigator.userAgent.match(/(Android|BlackBerry|IEMobile)/)) {
        if ($window.innerWidth < 1040 && $window.innerWidth > 800)
        //console.log("Rendering: 700 - 1024");
          $scope.modalInstance = $modal.open({
            template: '<h4 style="margin:20px;">Rotate Your Screen !</h4>',
            backdrop: 'static',
            keyboard: 'false',
            size: 'sm',
            scope: $scope
          });
      }
    }
    else {
      $scope.posLogo = '/assets/images/posist-logo.jpg'
    }

    //var _anglrEle = angular.element($window).bind('resize.doResize', function() {
    $(window).resize(function(){
      $scope.windowWidth = $window.innerWidth;
      $rootScope.winWidth = $window.innerWidth;
      if ($state.current.name === 'billing') {

        if ($window.innerWidth > 1020 && $window.innerWidth < 1100) {
          //$modalStack.dismissAll()
          $scope.posLogo = '/assets/images/posist-logo.jpg'
          console.log("Rendering:  1024 - 1100");
          // alert("hhh")
          // $modal.open({
          //       template: '<h4 style="margin:20px;">Rotate Your Screen !</h4>',
          //       backdrop: 'static',
          //       keyboard: 'false',
          //       size: 'sm',
          //       scope: $scope
          //     });
          $scope.$apply(function() {

            $scope.itemPagination.perPage = $scope.selectedOffer==null?24: 18;
            $scope.itemPagination.numPages = Math.ceil($scope.filteredTabItems.length / $scope.itemPagination.perPage);

            /* Categories: Reset Pagination */
            $scope.categoryPagination.perPage = 10;
            $scope.categoryPagination.numPages = Math.ceil($scope.filteredCategoryItems.length / $scope.categoryPagination.perPage);

            /* SCategory: Reset Pagination */
            $scope.sCategoryPagination.perPage = 5;
            $scope.sCategoryPagination.numPages = Math.ceil($scope.sCategoryItems.length / $scope.sCategoryPagination.perPage);

          });

        } else if ($window.innerWidth > 1100) {
          $scope.posLogo = '/assets/images/posist-logo.jpg'
          console.log("Rendering > 1100");
          //   console.log($scope.itemPagination);
          $scope.$apply(function() {
            //if($scope.selectedOffer)
            $scope.itemPagination.perPage =$scope.selectedOffer==null?30: 24;
            $scope.itemPagination.numPages = Math.ceil($scope.tabItems.length / $scope.itemPagination.perPage);

            /* Categories: Reset Pagination */
            $scope.categoryPagination.perPage = 12;
            $scope.categoryPagination.numPages = Math.ceil($scope.categoryItems.length / $scope.categoryPagination.perPage);

            /* SCategory: Reset Pagination */
            $scope.sCategoryPagination.perPage = 6;
            $scope.sCategoryPagination.numPages = Math.ceil($scope.sCategoryItems.length / $scope.sCategoryPagination.perPage);

          });

        } else if ($window.innerWidth <= 1020 && $window.innerWidth >= 800) {
          //alert("hello")
          //if (navigator.userAgent.match(/(Android|BlackBerry|IEMobile)/)){
              // $modal.open({
              //   template: '<h4 style="margin:20px;">Rotate Your Screen !</h4>',
              //   backdrop: 'static',
              //   keyboard: 'false',
              //   size: 'sm',
              //   scope: $scope
              // });
          //}
          $scope.posLogo = '/assets/images/posist-glificon-logo.jpg';
          $scope.$apply(function() {
            /* Items: Reset Pagination*/
            $scope.itemPagination.perPage = $scope.selectedOffer == null?18: 15;
            $scope.itemPagination.numPages = Math.ceil($scope.tabItems.length / $scope.itemPagination.perPage);

            /* Categories: Reset Pagination */
            $scope.categoryPagination.perPage = 6;
            $scope.categoryPagination.numPages = Math.ceil($scope.categoryItems.length / $scope.categoryPagination.perPage);

            /* SCategory: Reset Pagination */
            $scope.sCategoryPagination.perPage = 3;
            $scope.sCategoryPagination.numPages = Math.ceil($scope.sCategoryItems.length / $scope.sCategoryPagination.perPage);
          });


        }
        else if ($window.innerWidth < 800) {
          //console.log("Rendering: < 700");
          $scope.posLogo = '/assets/images/posist-glificon-logo.jpg'
          $scope.$apply(function() {
            /* Items: Reset Pagination*/
            $scope.itemPagination.perPage = $scope.selectedOffer == null?18: 15;
            $scope.itemPagination.numPages = Math.ceil($scope.tabItems.length / $scope.itemPagination.perPage);

            /* Categories: Reset Pagination */
            $scope.categoryPagination.perPage = 6;
            $scope.categoryPagination.numPages = Math.ceil($scope.categoryItems.length / $scope.categoryPagination.perPage);

            /* SCategory: Reset Pagination */
            $scope.sCategoryPagination.perPage = 3;
            $scope.sCategoryPagination.numPages = Math.ceil($scope.sCategoryItems.length / $scope.sCategoryPagination.perPage);
          });
          // $scope.$apply(function() {
          //   /* Items: Reset Pagination*/
          //   $scope.itemPagination.perPage = 4;
          //   $scope.itemPagination.numPages = Math.ceil($scope.tabItems.length / $scope.itemPagination.perPage);

          //   /* Categories: Reset Pagination */
          //   $scope.categoryPagination.perPage = 2;
          //   $scope.categoryPagination.numPages = Math.ceil($scope.categoryItems.length / $scope.categoryPagination.perPage);

          //   /* SCategory: Reset Pagination */
          //   $scope.sCategoryPagination.perPage = 2;
          //   $scope.sCategoryPagination.numPages = Math.ceil($scope.sCategoryItems.length / $scope.sCategoryPagination.perPage);
          // });

        }
      }
    });
    $scope.passCodeContextUser='';
    $scope.assignPassUser = function(user){
      $scope.bill._currentPassCodeUser=user.username;
      $scope.passCodeContextUser=angular.copy( user.username);
    }
    // alert($rootScope.offlineBill.lastCreated);
    $scope.appCacheCall = function () {
      console.log('appcache Called');
     // appcache.checkUpdate().then(function (status) {
     //   console.log(status);
     // });
    }
    appcache.addEventListener('downloading', function () {
      console.log('downloading started');
    });
    appcache.addEventListener('updateready', function () {
      console.log('downloading done');
      appcache.swapCache().then(function () {
        growl.success('Your software has been updated with the new version successfully.')
        window.location.reload();
      });
    });

    // $scope.$on("$destroy", function () {
    //   _anglrEle.off("resize.doResize"); //remove the handler added earlier
    // });
    /* $scope.$on("editBill", function(bill) {
     console.log(bill);
     alert(); //remove the handler added earlier
     });*/

    //$scope.tabWiseWaiterDelivery = $rootScope.users;

    $scope.explorerTemplate = 'null';
    $scope.explorerCategoryTemplate = 'null';
    $scope.explorerSuperCategoryTemplate = 'null';

    // console.log($rootScope.editBill);


    /* Offline Bills */
    $scope.bills = offlineBills;

    $scope.tableTemplate = 'null';

    $scope.showBillButton = true;

    $scope.ActivateKeyboard = function () {
      var keyBoardSetting = Utils.hasSetting('activate_keyboard', localStorageService.get('settings'));
      // alert(keyBoardSetting);
      if (keyBoardSetting == true) {
        $rootScope.alphaKeyboard.customClass = 'showKeyboard';
        $rootScope.alphaKeyboardTop.customClass = 'showKeyboard';
        $rootScope.numKeyboard.customClass = 'showKeyboard';
      }
    }
    $scope.ActivateKeyboard();
    $scope.isKeyBoard=function(){
      var flag=false;
      if(Utils.hasSetting('activate_keyboard', localStorageService.get('settings'))){
        flag=true;
      }
      return flag;
    }
    // console.log($scope.addons);

    /*var template = $templateCache.put('itemExplorer.html', '<div class="col-md-2" style="width: 126px;" ng-repeat="items in data"><div ng-repeat="item in items"><div class="s-class-cat"><a ng-click="addItemFromExplorer(item)" class="btn btn-default_p ng-binding">{{item.name}}</a></div></div></div>');
     var slideScope = $scope.$new();
     angular.extend(slideScope, angular.copy($scope.fauxItems));
     //console.log(template);
     var _t = $compile($('<div id="explorer">' + template + '</div>'))(slideScope);
     $scope.explorerTemplate = _t;

     console.log("Compiled.")
     console.log($scope.explorerTemplate);*/

    $scope.isUser = function(permissionName) {
      if (currentUser.role === 'superadmin') {
        return true;
      } else {
        return Utils.hasUserPermission(currentUser.selectedPermissions, permissionName);
      }
    };

    $scope.isPermission=function(permissions){
      // console.log(currentUser);
      var flag = false;
      if (currentUser.role === 'user') {
        // flag= true;
        //   } else {
        var permissionArr = permissions.split(',');
        // console.log(permissionArr);
        _.forEach(permissionArr, function (permname) {
          if (Utils.hasPermission(currentUser.selectedPermissions, permname)) {
            flag = true;
          }
        })
      }else{
        flag=true;
      }
      return flag;
    }

    $scope.isGroupPermission=function(permissions){
      // console.log(currentUser);
      var flag = false;
      if (currentUser.role === 'user') {
        // flag= true;
        //   } else {
        var permissionArr = permissions.split(',');
        // console.log(permissionArr);
        _.forEach(permissionArr, function (permname) {
          if (Utils.hasUserPermission(currentUser.selectedPermissions, permname)) {
            flag = true;
          }
        })
      }else{
        flag=true;
      }
      return flag;
    }
    /* Current Tab */
    $scope.tab = "";
    $scope.tabId = "";
    $scope.tabType = "";

    /* Table List */
    $scope.tables = [];
    $scope.gotoTableNumber = 1;


    /* Table Details */
    $scope.table;

    /* Master Items */
    $scope.masterItems = [];
    $scope.explorerItems = [];

    /* Categories */
    $scope.categories = [];
    $scope.categoriesT = [];

    /* Bill */
    $scope.bill = {};
    $scope.billableItems = $rootScope.masterItems;

    /* Settings - Billing */
    //console.log(settings);
    $scope.settings = settings.settings;
    $scope.settleSettings=settings.settings[settings.settings.length-2].value;
    $scope.bankSettings=settings.settings[settings.settings.length-1].value;
    // console.log($scope.settleSettings);
    //$scope.settleValue={selected:$scope.settleSettings[0].name};
    $scope.smsSettings=settings.smsSettings;
    //$scope.showKot=(Utils.hasSetting('dis_kot', $scope.settings))?false:(Utils.hasSetting('print_kot_bill', $scope.settings))?false:true;

    /* Users */
    $scope.users = $rootScope.users;
    if(Utils.hasSetting('bar_exchange_ip',$scope.settings)){
      $scope.bar_exchange_enabled= Utils.getSettingValue('bar_exchange_ip',$scope.settings);
    }//console.log("users",$scope.users);


    $scope.itemCurrentIndex = 0;
    $scope.selectedTab = {};
    // $scope.tabWiseWaiterDelivery = $scope.filterWaiterDelivery($scope.selectedTab.tabType=='table'?'Waiter':'');
    $scope.showAdvance=false;
    $scope.isEditAdvance=false;
    function getItemById(itemId) {
      var itemob = {};
      // var filterMasterTab = _.filter($scope.masterItems)
      for (var i = 0; i < $scope.masterItems.length; i++) {
        if ($scope.masterItems[i]._id == itemId) {
          itemob = angular.copy($scope.masterItems[i]);
          if(Utils.hasSetting('inclusive_tax',$scope.settings)){
            itemob.originalRate=itemob.rate;
            itemob.rate=calculateInclusiveTax(itemob);
          }
          break;
        }
      };
      return itemob;
    };

    var tempEvent = 'syncKot_save_'+localStorageService.get('deployment_id');
    var tableLockEvent='table_event_'+localStorageService.get('deployment_id');
    var tableSplitEvent='table_split_event_'+localStorageService.get('deployment_id');
    var daysaleEvent='syncKot_daySale_'+localStorageService.get('deployment_id');
    var instanceStart='instance_start_'+localStorageService.get('deployment_id');
    $scope.tableLock = [];
    $scope.nonTableLock = [];

    if($rootScope.isInstance) {
      nSocket.forward(tempEvent, $scope);
      $scope.$on('socket:' + tempEvent, function (msg, ob) {
        var offlineBill = $scope.mapFineDineObject(ob);
        //console.log(offlineBill);
        if (offlineBill.tab == 'table') {
          if (offlineBill == undefined) {
            growl.error('Open bill not found on server,may be settled from other client!!', {ttl: 6000});
            $scope.renderTableWithBill();
            $scope.showAdvance = false;
            return false;
          }
          if(offlineBill.splitNumber==null)
            $scope.renderFineDineTable(offlineBill);
        } else {
          $scope.renderFineDineTakeOutDelivery(offlineBill);
        }
        //alert();
        // alert(data.tab);
      });
      nSocket.forward(tableLockEvent, $scope);
      $scope.$on('socket:' + tableLockEvent, function (msg, ob) {
        if (ob.type == 'table') {
          //console.log($scope.tableLock);
          if (ob.event == 'add') {
            if ($scope.tableLock.indexOf(ob.number) == -1)
              $scope.tableLock.push(ob.number);
          } else {
            //   console.log(ob.number);
            console.log( $scope.tableLock.indexOf(ob.number));
            if ($scope.tableLock.indexOf(ob.number) != -1)
              $scope.tableLock.splice($scope.tableLock.indexOf(ob.number), 1);
          }
          // console.log($scope.tableLock);

          /* if (ob.event == 'add') {
           if ($scope.tableLock.indexOf($scope.selectedTab.tabId+ob.number) == -1)
           $scope.tableLock.push($scope.selectedTab.tabId+ob.number);
           } else {
           //   console.log(ob.number);
           console.log( $scope.tableLock.indexOf($scope.selectedTab.tabId+ob.number));
           if ($scope.tableLock.indexOf($scope.selectedTab.tabId+ob.number) != -1)
           $scope.tableLock.splice($scope.tableLock.indexOf($scope.selectedTab.tabId+ ob.number), 1);
           }*/
        } else {
          if (ob.event == 'add') {
            if ($scope.nonTableLock.indexOf(ob.number) == -1) {
              $scope.nonTableLock.push(ob.number);
            }
          } else {
            if ($scope.nonTableLock.indexOf(ob.number) != -1) {
              $scope.nonTableLock.splice($scope.nonTableLock.indexOf(ob.number), 1);
            }
          }
          // console.log($scope.nonTableLock);
        }
      });
      nSocket.forward(tableSplitEvent, $scope);
      $scope.$on('socket:' + tableSplitEvent, function (msg, ob) {
        console.log('tableSplitRender');
        // console.log($scope.tableStarted);
        if ($scope.isBillOn == false)
          $scope.renderTableWithBill();
      });
      nSocket.forward(daysaleEvent, $scope);
      $scope.$on('socket:' + daysaleEvent, function (msg, ob) {
        // console.log(ob);
        $scope.offlineSale = ob;
         $scope.unsettledUpdateToServer(ob.unsettle);
      });

      /* nSocket.forward(instanceStart,$scope);
       $scope.$on('socket:'+ instanceStart , function (msg,ob) {
       console.log(ob);
       $scope.tableLock=ob.tables;
       })*/

      $scope.instanceStartN = function () {
        // alert();
        if ($rootScope.isInstance) {
          /*nSocket.emit('instance_start', {
           deployment_id: localStorageService.get('deployment_id')
           }, function (data) {
           console.log(data);
           })*/
          $http.get($rootScope.url + '/api/synckots/getBillLockData?deployment_id=' + localStorageService.get('deployment_id')).then(function (syncLock) {
            var billlock = syncLock.data;
            if (_.has(billlock, 'tables')) {
              $scope.tableLock = billlock.tables;
              $scope.nonTableLock = billlock.nonTables;
            }
          })
          $http.get($rootScope.url + '/api/synckots/getUnsettledSale?deployment_id=' + localStorageService.get('deployment_id')).then(function (syncData) {
            $scope.offlineSale=syncData.data;
            $scope.unsettledUpdateToServer(syncData.data.unsettle);
          })
        }
      }
      $scope.instanceStartN();
    }

    $scope.unsettledUpdateToServer=function(amount){
       var unsettledAmount = {
                'tenant_id': localStorageService.get('tenant_id'),
                'deployment_id': localStorageService.get('deployment_id'),
                'unsettledAmount': amount
            };
      $http.post('/api/unsettledAmount', unsettledAmount).then(function(success) {
            console.log(success);
        }, function(err) {
            console.log(err);
        })
    }

    $scope.getSocketStatus = function() {
      socket.getStatus(function(data) {
        console.log(data);
      });
    };

    $scope.renderFineDineTable=function(bill){
      var offlineBill=angular.copy(bill);
      // console.log( $state.current.data.tables);
      if(offlineBill.bill._closeTime!=null){
        for(var i=0;i<$state.current.data.tables.length;i++){
          // if(offlineBill.bill.splitNumber==null) {
          if (parseInt($state.current.data.tables[i].id) == offlineBill.bill._tableId  ) {

            delete $state.current.data.tables[i].bill;
            delete $state.current.data.tables[i]._covers;
            if(offlineBill.bill.splitNumber!=null){
              if($scope.isBillOn==false) {
                $scope.renderTableWithBill();
                return false;
              }
            }
            //   $scope.$scope.renderTableWithBill();
            // console.log(i)
            break;
          }
          /*}else{

           }*/
        }
        return false;
      }
      _.forEach($state.current.data.tables, function (table) {
        if (offlineBill.isSettled == "false" || offlineBill.isSettled == false) {
          var tableStr = table.id.toString();
          var splittedTable = tableStr.split("-");

          if (splittedTable.length == 1) {
            if (parseInt(offlineBill.bill._tableId) == parseInt(table.id) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
              //if(offlineBill.bill._closeTime!=null){
              //alert(offlineBill.bill._closeTime);
              //}
              table.bill = offlineBill.bill;
              table.covers = offlineBill.bill._covers;
            }
          } else {
            if (parseInt(offlineBill.bill._tableId) == parseInt(splittedTable[0]) && parseInt(offlineBill.bill.splitNumber) == parseInt(splittedTable[1]) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
              //alert(offlineBill.bill._closeTime);
              table.bill = offlineBill.bill;
              table.covers = offlineBill.bill._covers;
            }

          }
        }
        //});
      });
    }

    $scope.renderFineDineTakeOutDelivery=function(offlineBill) {

      // $state.current.data.openedBills.splice( Utils.arrayObjectIndexOf($state.current.data.openedBills,'billId',offlineBill.billId),1);
      var index=-1;
      index= Utils.arrayObjectIndexOf($state.current.data.openedBills,'billId',offlineBill.billId);
      //console.log(index);
      if(offlineBill.bill._closeTime!=null){
        $state.current.data.openedBills.splice( Utils.arrayObjectIndexOf($state.current.data.openedBills,'billId',offlineBill.billId),1);
        return false;
      }
      if (offlineBill == undefined) {
        growl.error('Bill not found on server,may be settled', {ttl: 6000});
      }
      if (!(offlineBill.isOnlineBill || offlineBill.isAdvanceBill)) {
        var table = {
          "id": (offlineBill.bill.billNumber + (offlineBill.bill.splitNumber > 0 ? '-' + offlineBill.bill.splitNumber : ''))
        };
        table.bill = offlineBill.bill;
        table.covers = offlineBill.bill._covers;
        table.billId = offlineBill.bill._id;
        table.name = offlineBill.name;
        table.isOnlineBill = false;
        table.isAdvanceBill = false;

        //$state.current.data.openedBills.push(table);
        if(index==-1){
          if($scope.selectedTab.tabId==offlineBill.tabId)
            $state.current.data.openedBills.push(table);
        }else {
          $state.current.data.openedBills[index] = table;
        }
      } else {
        //$state.current.data.openedBills.push(offlineBill);
      }
      // $state.current.data.openedBills.push(_ob);
      //_filterdT[0]=_ob;
      //     $scope.startNewTakeOutDeliveryBillPost(billNumber,billId);


    }

    var sEvent='onlineOrder_save'+localStorageService.get('deployment_id');
    var aEvent='advanceBooking_save'+localStorageService.get('deployment_id');
    var isSocketConnected=false;
    //  socket.forward('sEvent', $scope);
    $scope.getLiveOnlineOrder=function(){
      mySocket.forward(sEvent,$scope);
      $scope.$on('socket:'+ sEvent, function (ev, data) {
        growl.success("new order synced", {ttl: 2000})
        console.log(data);
        $scope.saveOnlineBills(data);
        myAudio = setInterval(function(){
            audio.play()
          },
          6000);
      });
      mySocket.forward(aEvent,$scope);
      $scope.$on('socket:'+ aEvent, function (ev, data) {
        growl.success("new order synced", {ttl: 2000})
        console.log(data);
        $scope.saveAdvanceBills(data);
      });
      isSocketConnected=true;
    }
    $scope.getLiveOnlineOrder();
    socket.on("connect",function(res){
      console.log('connected');
    })
    socket.on("disconnect",function(res){
      console.log('disconnected');
    })
    $scope.saveOnlineBills=function(billData) {
      //var query="insert into onlineBills(OnlineBillId,OnlineBillNumber,TimeStamp,OnlineBillsData,OrderTime,DownloadStatus)" +
      var orderOb={
        "OnlineBillId": billData._id,
        "OnlineBillNumber": billData.billNumber,//number to be generated
        "TimeStamp": Utils.getDateFormatted(new Date()),
        "OnlineBillsData":angular.toJson( billData),
        "OrderTime": Utils.getDateFormatted(new Date( billData.created)),
        "DownloadStatus": "saved",
        "TabId":billData.tabId
      };
      if(!$rootScope.isInstance) {
        $rootScope.db.insert('OnlineBills', orderOb
          , function (err) {
            //console.log(err);
          }).then(function (result) {
          //console.log(result);
          OnlineOrder.updateOrderModify({id: billData._id}, function (res) {
            // console.log(res);
            $rootScope.db.update('OnlineBills',
              {
                "DownloadStatus": "synced"
              }, {"OnlineBillId": billData._id}).then(function (resl) {
              var tabId = billData.tabId;
              var _f = _.filter($scope.tabs, {_id: tabId});
              if (_f.length > 0) {
                var t = _f[0];
                t.orderCount += 1;
              }

            });
          })
        });
      }else {
        /*fine dine */
        orderOb.deployment_id = localStorageService.get('deployment_id');
        $http.post($rootScope.url + '/api/synckots/createOnlineBill', orderOb).then(function (result) {
          OnlineOrder.updateOrderModify({id: billData._id}, function (res) {
            $http.post($rootScope.url + '/api/synckots/updateOnlineBill', {
              "OnlineBillId": billData._id,
              "DownloadStatus": "synced"
            }).then(function (updatestatus) {
              var tabId = billData.tabId;
              var _f = _.filter($scope.tabs, {_id: tabId});
              if (_f.length > 0) {
                var t = _f[0];
                t.orderCount += 1;
              }
            });
          });
        });

        /*fine dine end*/
      }
      var customerData=billData.customer;
      var custId=Utils.guid();
      if(customerData.firstname!=undefined){
        customerData.deployment_id=localStorageService.get('deployment_id');
        customerData.customerId=custId;
        if(!_.has(customerData,'addresses')){
          customerData.addresses=[];
        }
        // console.log(customerData);
        $rootScope.db.insert('customers', {
          'customerId': custId,
          'customerData': JSON.stringify(customerData)
        })
      }
    }
    $scope.updateOnlineBillStatus=function(status,bill) {
      //  alert(status);
      //console.log($scope.bill._kots[0]);
      if (_.has($scope.bill, 'onlineBill')) {
        if ($scope.bill.onlineBill._id != undefined) {
          OnlineOrder.updateOrderModify({
            id: $scope.bill.onlineBill._id,
            deployment_id: $scope.bill.onlineBill.deployment_id,
            status: status
          }, function (res) {
          });
          if (status == 'kot') {
            if (_.has($scope.bill.onlineBill, 'kotPrintUrl'))
              $scope.executeOnlineBillStatusUrl($scope.bill.onlineBill.kotPrintUrl, $scope.bill.onlineBill._id);
          }
          if (status == 'bill') {
            if (_.has($scope.bill.onlineBill, 'billPrintUrl')){
              var data = {};
              if ($scope.bill._delivery && $scope.bill._delivery.firstname && $scope.bill._delivery.lastname){
                console.log("delivery boy", $scope.bill._delivery.firstname);
                data = {firstname: $scope.bill._delivery.firstname, lastname: $scope.bill._delivery.lastname};
                OnlineOrder.executeUrl({
                  url: $scope.bill.onlineBill.billPrintUrl,
                  deliveryInfo: data,
                  id: $scope.bill.onlineBill._id
                }).$promise.then(function (resp) {
                });
              }else{
                console.log("No delivery info");
                $scope.executeOnlineBillStatusUrl($scope.bill.onlineBill.billPrintUrl, $scope.bill.onlineBill._id);
              }
            }
          }
          if (status == 'settle') {
            if (_.has($scope.bill.onlineBill, 'settleUrl'))
              $scope.executeOnlineBillStatusUrl($scope.bill.onlineBill.settleUrl, $scope.bill.onlineBill._id);
          }
        }
      }
      if (status == 'dispatch') {
        if (_.has(bill, 'onlineBill')) {
          if (bill.onlineBill._id != undefined) {
            OnlineOrder.updateOrderModify({
              id: bill.onlineBill._id,
              deployment_id: bill.onlineBill.deployment_id,
              status: status
            }, function (res) {
            });
            if (_.has(bill.onlineBill, 'dispatchUrl')) {
              //$scope.executeOnlineBillStatusUrl(bill.onlineBill.dispatchUrl);
              var data = {};
              if (bill._delivery && bill._delivery.firstname && bill._delivery.lastname)
                data = {firstname: bill._delivery.firstname, lastname: bill._delivery.lastname}
              OnlineOrder.executeUrl({
                url: bill.onlineBill.dispatchUrl,
                deliveryInfo: data,
                id: bill.onlineBill._id
              }).$promise.then(function (resp) {
              });
            }
          }
        }
      }
    }
    $scope.executeOnlineBillStatusUrl=function(url, id) {
      OnlineOrder.executeUrl({
        url: url,
        id: id
      }).$promise.then(function (resp) {
      });
    }
    $scope.saveAdvanceBills=function(billData) {
      //var query="insert into onlineBills(OnlineBillId,OnlineBillNumber,TimeStamp,OnlineBillsData,OrderTime,DownloadStatus)" +
      $rootScope.db.insert('AdvanceBookings',
        {
          "AdvanceBillId": billData._id,
          //"OnlineBillNumber": billData.billNumber,//number to be generated
          "TimeStamp": Utils.getDateFormatted(new Date()),
          "AdvanceBillsData": angular.toJson(billData),
          "OrderTime": Utils.getDateFormatted(new Date(billData.created)),
          "DownloadStatus": "saved",
          "TabId": billData.tabId
        }, function (err) {
          //console.log(err);
        }).then(function (result) {
        //console.log(result);
        Advancebooking.updateOrderModify({id: billData._id}, function (res) {
          // console.log(res);
          $rootScope.db.update('AdvanceBookings',
            {
              "DownloadStatus": "synced"
            }, {"AdvanceBillId": billData._id}).then(function (resl) {
            var tabId = billData.tabId;
            var _f = _.filter($scope.tabs, {_id: tabId});
            if (_f.length > 0) {
              var t = _f[0];
              t.orderCount += 1;
            }

          });
        })
      });
    }
    /*socket.on("error",function(res){
     console.log('error in connection');
     })
     socket.emit('init',localStorageService.get('deployment_id'),function(data){
     console.log(data);

     })
     */
    /* socket.getStatus(function(data) {
     console.log(data);
     });*/

    /*  socket.on('bill:save'+localStorageService.get('deployment_id'),function(data){
     console.log(data);

     })
     socket.on("connect",function(res){
     console.log('connected');
     })
     socket.on("disconnect",function(res){
     console.log('disconnected');
     })
     socket.on("error",function(res){
     console.log('error in connection');
     })
     socket.emit('init',localStorageService.get('deployment_id'),function(data){
     console.log(data);

     })
     */
    /* socket.getStatus(function(data) {
     console.log(data);
     });*/

    /*  socket.on("onlineOrder_update"+localStorageService.get('deployment_id'),function(res){
     alert();
     console.log(res);
     })*/

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
      //console.info("TODO: Add Passcode Authentication to Table!");
    });

    $scope.tableDClass = function(bill) {
      var tableClass = 'table-main-grey';
      if (bill) {
        if (bill.isPrinted == true) {
          tableClass = 'table-main-green';
        } else {
          tableClass = 'table-main-violet';
        }
      }
      return tableClass;
    };

    $scope.seconds = 0;
    $scope.elapsedMinutes = 0;

    /*$interval(function () {
     getTime()
     }, 1000 * 60);*/

    if ($state.current.name === 'billing') {
      $interval(function() {
        $scope.syncCustomers();
        //$scope.syncOfflineBills();
        $scope.autoSyncOfflineBills();
       // $scope.appCacheCall();
        if(!isSocketConnected){$scope.getLiveOnlineOrder();}
      }, 1000 * 60);
    }


    function getTime() {
      $scope.elapsedMinutes++;
      // $scope.seconds++;
    }

    $scope.currentDate = new Date();
    $scope.getLapsedTime = function(bill, index, length) {
      if (!bill) {
        return 0;
      }
      if ($scope.elapsedMinutes == 0) {
        var miliseconds = $scope.currentDate - (Utils.convertDate(angular.copy(bill._created)));
        var dminutes = Math.round((miliseconds / (60 * 1000)), 0);
        var dhours = Math.round((dminutes / (60)), 0);
        var ddays = Math.round(dhours / 24, 0);
        bill.calulatedTime = {
          isCalculated: true,
          hr: dhours,
          mins: dminutes,
          sec: 20
        };
        $scope.seconds++;
      }
      var nbill = angular.copy(bill);
      var elapsedMins = (nbill.calulatedTime.mins + $scope.elapsedMinutes);
      var hr = nbill.calulatedTime.hr + (Math.round(($scope.elapsedMinutes / 60), 0));
      var mins = (nbill.calulatedTime.mins + $scope.elapsedMinutes) % 60;
      return (hr + ":" + (mins < 0 ? 0 : mins));
    };

    $scope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {

      /* Redirect to Admin if User is SuperAdmin */
      /* if (currentUser.role === 'superadmin') {
       $location.path('/admin');
       }*/
      $scope.initializeTab();
      if (toState.name === 'billing') {

      }
    });
    $scope.lockTable=function(tableId,copied){
      if($rootScope.isInstance) {
        $scope.tableStarted = {isTable: true, index: tableId};
        nSocket.emit('table_event', {
          event: 'add',
          type: 'table',
          number:$scope.selectedTab.tabId+ tableId,
          deployment_id: localStorageService.get('deployment_id')
        }, function (data) {
          //console.log(data);
        })
      }
      if(!(copied==null||copied==undefined)){
        $scope.tableStarted = {};
      }
    }
    $scope.lockNonTable=function(billNumbe) {
      // console.log(billNumber);
      if ($rootScope.isInstance) {
        if(billNumbe==null || billNumbe== undefined){
          return false;
        }
        var billNumber=  billNumbe.toString();
        $scope.tableStarted = {isTable: false, index: billNumber};
        nSocket.emit('table_event', {
          event: 'add',
          type: 'nonTable',
          number: billNumber,
          deployment_id: localStorageService.get('deployment_id')
        }, function (data) {
          //console.log(data);
        })
      }
    }
    $scope.unlockTable=function() {
      var d=$q.defer();
      if($rootScope.isInstance) {
        if ($scope.tableStarted != null) {
          if ($scope.tableStarted.isTable) {
            nSocket.emit('table_event', {
              event: 'remove',
              type: 'table',
              number:$scope.selectedTab.tabId+ $scope.tableStarted.index,
              deployment_id: localStorageService.get('deployment_id')
            }, function (data) {
              $scope.tableStarted = null;
              d.resolve('success');
              //console.log(data);
            })
          }
          else {
            nSocket.emit('table_event', {
              event: 'remove',
              type: 'nonTable',
              number: $scope.tableStarted.index,
              deployment_id: localStorageService.get('deployment_id')
            }, function (data) {
              $scope.tableStarted = null;
              d.resolve('success');
              //console.log(data);
            })
          }
        }
      }
      return d.promise;
    }
    $scope.initializeTab = function() {
      var d= $q.defer();
      $scope.unlockTable().then(function(res){
        // alert();
        d.resolve('success');
      });
      $scope.isBillOn=false;
      $scope.isEditAdvance=false;
      var index=0;

      /*if($rootScope.editBill!=undefined) {
       var tabtype=$rootScope.editBill.tabType=='takeout'?'take_out':$rootScope.editBill.tabType;
       for(var i=0;i<$rootScope.tabs.length;i++){
       if($rootScope.tabs[i].tabType==tabtype){
       index=i;
       }
       }
       }*/
      // console.log($scope.selectedTab.tabName);
      // console.log($rootScope.tabs);
      if($scope.selectedTab.tabType == undefined) {
        if ($rootScope.editBill != undefined) {
          var tabtype = $rootScope.editBill.tabType == 'takeout' ? 'take_out' : $rootScope.editBill.tabType;
          for (var i = 0; i < $rootScope.tabs.length; i++) {
            if ($rootScope.tabs[i].tabType == tabtype) {
              index = i;
            }
          }
        }
      }else {
        for (var i = 0; i < $rootScope.tabs.length; i++) {
          if ($rootScope.tabs[i]._id.toString() == $scope.selectedTab.tabId) {
            //alert(index);
            index = i;
          }
        }
      }


      $scope.selectedTab = {
        tabId: $rootScope.tabs[index].tabId,
        tabName: $rootScope.tabs[index].tabName,
        tabType: $rootScope.tabs[index].tabType
      }
      console.log("Initialize Tab Start");
      console.log(index);
      $scope.startTabRender($rootScope.tabs[index]);
      //$scope.startTabRender($scope.selectedTab);
      //  $scope.lastFilteredSCategory='';
      return d.promise;
    };

    //$scope.itemPagination;
    $scope.renderItemsTabWise=function(tab,rItems){
      $scope.lastFilteredSCategory = '';
      $scope.lastFilteredCategory = '';
      //console.log($rootScope);

      /* Filter Items, Categories, SuperCategories by Tab */
      var _tabData = _.filter(rItems, {
        tabId: tab._id
      });
      /* Responsive on Load */
      var _windowInnerWidth = $window.innerWidth;
      console.log(_windowInnerWidth);
      var _renderCounts = {
        items:$scope.selectedOffer==null?30: 24 ,
        categories: 12,
        sCategories: 6
      }
      if (_windowInnerWidth < 700) {
        _renderCounts.items = 18;
        _renderCounts.categories = 6;
        _renderCounts.sCategories = 3;
      }
      if (_windowInnerWidth > 750 && _windowInnerWidth < 1024) {
        _renderCounts.items = 18;
        _renderCounts.categories = 6;
        _renderCounts.sCategories = 3;
      }
      /* Responsive on Load */



      /* Item Explorer */
      $scope.tabItems = [];

      // Sort According to Setting
      if (Utils.hasSetting('orderby_itemnumber', $scope.settings)) {
        $scope.tabItems = _.sortBy(_tabData[0].Items, ['number']);
      } else {
        //  $scope.tabItems = _.sortBy(_tabData[0].Items, ['name']);
        $scope.tabItems = (_tabData[0].Items);
      };

      $scope.filteredTabItems = $scope.tabItems;
      $scope.itemPagination = Pagination.getNew(_renderCounts.items);
      $scope.itemPagination.numPages = Math.ceil($scope.tabItems.length / $scope.itemPagination.perPage);
      $scope.itemPagination.totalItems = $scope.tabItems.length;


      /* Item Explorer */


      /* Category Explorer */
      $scope.categoryItems = [];
      $scope.categoryItems = _tabData[0].categories;
      $scope.filteredCategoryItems = _tabData[0].categories;
      $scope.categoryPagination = Pagination.getNew(_renderCounts.categories);
      $scope.categoryPagination.numPages = Math.ceil($scope.categoryItems.length / $scope.categoryPagination.perPage);
      $scope.categoryPagination.totalItems = $scope.categoryItems.length;
      /* Category Explorer */


      /* Super Category Explorer */
      $scope.sCategoryItems = [];
      $scope.sCategoryItems = _tabData[0].superCategories;
      $scope.sCategoryPagination = Pagination.getNew(_renderCounts.sCategories);
      $scope.sCategoryPagination.numPages = Math.ceil($scope.sCategoryItems.length / $scope.sCategoryPagination.perPage);
      $scope.sCategoryPagination.totalItems = $scope.sCategoryItems.length;
      /* Super Category Explorer */


    }
    $scope.startTabRender = function(tab) {
      $scope.offerPrint=false;
      $scope.unlockTable();
      $scope.isBillOn=false;
      $scope.isEditAdvance=false;
      tab.orderCount=0;
      $scope.isBillingOn=false;
      console.time("TAB RENDER:");
       audio.pause();
      clearInterval(myAudio);
      $scope.selectedOffer=null;
      $scope.renderItemsTabWise(tab,$rootScope.rootMasterItems);
      //if($scope.selectedOffer!=null){return false;}
      $scope.showBillPage = false;

      /* if (toState.name === 'billing.table') {*/
      $scope.elapsedMinutes = 0;
      //console.log(tab);
      $scope.selectedTab = {
        tabId: tab._id,
        tabName: tab.tabName,
        tabType: tab.tabType == 'take_out' ? 'takeout' : tab.tabType
      };

      /* Table State */
      if ($scope.selectedTab.tabType === 'table') {

        $scope.getOfflineBillsByTab().then(function(offlineBills) {
          $scope.elapsedMinutes = 0;
          $state.current.data.tables = [];
          var splittedTable = [];
          for (var i = 0; i < offlineBills.length; i++) {
            if (offlineBills[i].bill.splitNumber != null) {
              var tempOb = {
                tableId: offlineBills[i].bill._tableId,
                splitNumber: offlineBills[i].bill.splitNumber
              };
              splittedTable.push(tempOb);
            }
          }
          if (splittedTable.length > 0) {
            _.forEach(dataTables.tables, function(table) {
              var splitNos = [];
              for (var j = 0; j < splittedTable.length; j++) {
               // console.log();
                if (table.id == splittedTable[j].tableId) {
                  splitNos.push(splittedTable[j].splitNumber);
                }
              }
              if (splitNos.length > 0) {
                for (var k = 0; k < splitNos.length; k++) {
                  var tableTemp = {
                    id: (table.id + '-' + splitNos[k]),
                    name: table.name
                  };
                  $state.current.data.tables.push(angular.copy(tableTemp));
                }
              } else {
                $state.current.data.tables.push(angular.copy(table));
              }
            })
          } else {
            // console.log(dataTables.tables);
            $state.current.data.tables = angular.copy(dataTables.tables);
          }


          $scope.bill = {};

          /* Bind with Unsettled Table Bills */

          _.forEach($state.current.data.tables, function(table) {
            _.forEach(offlineBills, function(offlineBill) {
              if (offlineBill.isSettled == "false"||offlineBill.isSettled == false) {
                var tableStr = table.id.toString();
                var splittedTable = tableStr.split("-");

                if (splittedTable.length == 1) {
                  if (parseInt(offlineBill.bill._tableId) == parseInt(table.id) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
                    table.bill = offlineBill.bill;
                    table.covers = offlineBill.bill._covers;
                  }
                } else {

                  if (parseInt(offlineBill.bill._tableId) == parseInt(splittedTable[0]) && parseInt(offlineBill.bill.splitNumber) == parseInt(splittedTable[1]) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
                    table.bill = offlineBill.bill;
                    table.covers = offlineBill.bill._covers;
                  }
                }
              }

            });
          });

          $scope.tablePagination = Pagination.getNew(24);
          $scope.tablePagination.numPages = Math.ceil($state.current.data.tables.length / $scope.tablePagination.perPage);
          $scope.tablePagination.totalItems = $state.current.data.tables.length;

          if ($rootScope.editBill != undefined){
            $scope.startEdit();
          }
          //  loadPendingBills();
          renderTableBookings();
        });
        $scope.showAdvance=false;
      }

      /* Take/Delivery State */
      if ($scope.selectedTab.tabType != 'table') {
        $scope.getOfflineBillsByTab().then(function(offlineBills) {
          //console.log(offlineBills);
          $state.current.data.openedBills = [];
          $state.current.data.openedBills.push({
            "id": "New",
            isOnlineBill:false,
            isAdvanceBill:false
          });
          $scope.bill = {};
          /* Bind with Unsettled Table Bills */
          _.forEach(offlineBills, function(offlineBill) {
            if(!(offlineBill.isOnlineBill||offlineBill.isAdvanceBill)) {
              var table = {
                "id": (offlineBill.bill.billNumber + (offlineBill.bill.splitNumber > 0 ? '-' + offlineBill.bill.splitNumber : ''))
              };
              table.bill = offlineBill.bill;
              table.covers = offlineBill.bill._covers;
              table.name=offlineBill.name;
              table.isOnlineBill=false;
              table.isAdvanceBill=false;
              table.billId=offlineBill.bill._id;
              // console.log(offlineBill.instance);
              /*  if(_.has(offlineBill,'instance')){
               if(!(offlineBill.instance.preFix==null||offlineBill.instance.preFix==undefined) )
               {
               // alert(offlineBill.instance.preFix);
               //delete table.bill.prefix;
               table.bill.prefix = angular.copy( offlineBill.instance.preFix);
               }
               }*/
              $state.current.data.openedBills.push(table);
            }else{
              $state.current.data.openedBills.push(offlineBill);
            }
          });
          if ($rootScope.editBill != undefined){
            $scope.startEdit();
          }
          console.log($state.current.data.openedBills);
          $scope.lockOnlineBill();
          if($scope.selectedTab.tabType == "delivery" && ($scope.tp_integrated||$scope.zomato_integrated || $scope.delivery_integrated))
            loadDeliveryInfo();
        });
        //renderTableBookings();
        //-----ADDED BY RAJAT END-----//
      };

      console.timeEnd("TAB RENDER:");

      $scope.getDaySale();
      intercom.registerEvent(tab.tabType);

    };


    $scope.showBillPage = false;
    $scope.tableStarted=null;
    $scope.isBillOn=false;
    $scope.selectedWaiter=null;
    $scope.selectedManager=null;
    $scope.selectedCashier=null;
    $scope.selectedManagerDetail=null;
    $scope.tempUserName=null;
    $scope.startNewTableBill = function(tableId) {
      /* if ($rootScope.isInstance) {
       $scope.tableStarted={isTable:true,index:tableId};
       nSocket.emit('table_event', {event:'add', type:'table',number:tableId,deployment_id:localStorageService.get('deployment_id')}, function (data) {
       //console.log(data);
       })
       }*/

      $scope.lockTable(tableId);
      $scope.isBillOn = true;
      if ($rootScope.isInstance) {
        if (Utils.hasSetting('passcode_table', $scope.settings)) {
          $scope.selectedWaiter = null;
          $scope.selectedCashier=null;
          $scope.selectedManager=null;
          $scope.selectedManagerDetail=null;
          $scope.tempUserName=null;
          $scope.passCodeModal().then(function (res) {
            console.log(res);
            if(res=='cancel'){
              $scope.unlockTable();
              return false;
            }
            //console.log( res);
            var _f = _.filter($rootScope.users, {passcode: res});
            if (_f.length > 0) {
              delete _f[0].hashedPassword;
              delete _f[0].salt;
              delete _f[0].selectedPermissions;
              // delete _f[0].passcode;
              var flagWaiter=false;
              var flagManager=false;
              var flagCashier=false;
              //console.log(_f[0]);
              _.forEach(_f[0].selectedRoles,function(role){
                if(role.name == 'Waiter'){
                  flagWaiter=true;
                }
                if(role.name == 'Manager'){
                  flagManager=true;
                  $scope.selectedManager=true;
                  $scope.selectedManagerDetail = _f[0];
                }
                if(role.name == 'Cashier'){
                  flagCashier=true;
                  $scope.selectedCashier = _f[0];
                }
              })
              if(flagWaiter)
                $scope.selectedWaiter = _f[0];
              //$scope.assignPassUser=_f[0];

              if(flagWaiter==true || flagManager==true)
                $scope.startNewTableWithPasscode(tableId);
              else{
                growl.error('Role assigned is not permitted.',{ttl:3000});
                $scope.unlockTable();
                return false;
              }
            }
            else {
              $scope.unlockTable();
              growl.error('Wrong password !', {ttl: 2000});
            }
          })
        } else {
          $scope.startNewTableWithPasscode(tableId);
        }
        //return false;
      }else{
        $scope.startNewTableWithPasscode(tableId);
      }
    }

    $scope.startNewTableWithPasscode=function(tableId){

      var _t = {};
      var _tempt = _.filter($state.current.data.tables, {
        id: tableId
      });
      _t = _tempt[0];
      //}
      if (!_t['bill']) {
        $scope.startNewTableBillPost(tableId);
      } else {
        if (!$rootScope.isInstance) {
          $scope.startNewTableBillPost(tableId);
        } else {
          //$scope.bill = _t.bill;
          $scope.getFineBillById(_t.bill._id).then(function (offlineBill) {
            //$state.current.data.tables.splice(Utils.arrayObjectIndexOf($state.current.data.tables, 'id', tableId), 1);
            if (offlineBill == undefined) {
              growl.error('Open bill not found on server,may be settled from other client!!', {ttl: 6000});
              $scope.renderTableWithBill();
              $scope.showAdvance=false;
              return false;
            }
            _.forEach($state.current.data.tables, function(table) {
              if (offlineBill.isSettled == "false"||offlineBill.isSettled == false) {
                var tableStr = table.id.toString();
                var splittedTable = tableStr.split("-");

                if (splittedTable.length == 1) {
                  if (parseInt(offlineBill.bill._tableId) == parseInt(table.id) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
                    table.bill = offlineBill.bill;
                    table.covers = offlineBill.bill._covers;
                  }
                } else {
                  if (parseInt(offlineBill.bill._tableId) == parseInt(splittedTable[0]) && parseInt(offlineBill.bill.splitNumber) == parseInt(splittedTable[1]) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
                    table.bill = offlineBill.bill;
                    table.covers = offlineBill.bill._covers;
                  }

                }
              }

              //});
            });

            /* $scope.tablePagination = Pagination.getNew(24);
             $scope.tablePagination.numPages = Math.ceil($state.current.data.tables.length / $scope.tablePagination.perPage);
             $scope.tablePagination.totalItems = $state.current.data.tables.length;*/

            $scope.startNewTableBillPost(tableId);
          });
        }
      }
    }

    $scope.filterChargesByTab=function(){
      var tempCharges = [];
      var charges=localStorageService.get('charges');
      if(charges!=null){
        _.forEach(charges,function(charge){
           var _f=_.filter(charge.tabs,{_id:$scope.selectedTab.tabId});
            if(_f.length>0){
              tempCharges.push({name:charge.name,type:charge.type,value:charge.value});
            }
        })
      }
      return tempCharges;
    }

    $scope.startNewTableBillPost = function(tableId) {
      /*  nSocket.emit('table:start',1,function(data){
       console.log(data);
       })
       mySocket.emit('table:start',1,function(data){
       console.log(data);
       })*/
      $scope.isOnlineBill=false;
      $scope.showBillPage = true;
      $scope.categories = [];
      GetMasterItems();
      /* Build Categories Filter */
      _.forEach($scope.masterItems, function(i) {
        $scope.categories.push(i.category);
      });
      var _uCats = _.uniq($scope.categories, 'categoryName');
      $scope.categories = _uCats;
      $scope.categories = _.sortBy($scope.categories, ['categoryName']);
      renderCategoriesT();
      var _t = {};
      var _tempt = _.filter($state.current.data.tables, {
        id: tableId
      });
      _t = _tempt[0];
      //}
      if (!_t['bill']) {
        $scope.renderRefreshNewBill();
        _t.bill = new Bill(tableId, null, null, null, null, currentUser, $rootScope.db, $scope.selectedTab.tabName, $scope.selectedTab.tabId, $scope.selectedTab.tabType);
        $scope.bill = _t.bill;
        $scope.bill.charges.detail=$scope.filterChargesByTab();
        //console.log($scope.bill);
        if($scope.selectedWaiter!=null)
          $scope.bill._waiter=$scope.selectedWaiter;
      } else {
        // _t.bill._covers = toParams.covers;

        $scope.bill = _t.bill;
        if(_.has($scope.copiedTableBill,'bill')){
          if(_.has($scope.copiedTableBill.bill,'_id')){
            if($scope.copiedTableBill.bill._id===$scope.bill._id){
               $scope.copiedTableBill={};
            }
          }
        }
        if($scope.bill._customer.firstname!=undefined){
          $scope.billCustomer=$scope.bill._customer;
          // $scope.selectedCustomer=$scope.bill._customer;
        }
      }
      addInrestoCustomerToBill(tableId);
      $scope.handleBill();
    };
    $scope.renderTableWithBill=function(){
      $scope.getOfflineBillsByTab().then(function(offlineBills) {

        $scope.elapsedMinutes = 0;
        $state.current.data.tables = [];
        var splittedTable = [];
        for (var i = 0; i < offlineBills.length; i++) {
          if (offlineBills[i].bill.splitNumber != null) {
            var tempOb = {
              tableId: offlineBills[i].bill._tableId,
              splitNumber: offlineBills[i].bill.splitNumber
            };
            splittedTable.push(tempOb);
          }
        }
        if (splittedTable.length > 0) {
          _.forEach(dataTables.tables, function(table) {
            var splitNos = [];
            for (var j = 0; j < splittedTable.length; j++) {
              console.log();
              if (table.id == splittedTable[j].tableId) {
                splitNos.push(splittedTable[j].splitNumber);
              }
            }
            if (splitNos.length > 0) {
              for (var k = 0; k < splitNos.length; k++) {
                var tableTemp = {
                  id: (table.id + '-' + splitNos[k]),
                  name: table.name
                };
                $state.current.data.tables.push(angular.copy(tableTemp));
              }
            } else {
              $state.current.data.tables.push(angular.copy(table));
            }
          })
        } else {
          $state.current.data.tables = angular.copy(dataTables.tables);
        }


        $scope.bill = {};

        /* Bind with Unsettled Table Bills */
        //console.log(offlineBills);
        _.forEach($state.current.data.tables, function(table) {
          _.forEach(offlineBills, function(offlineBill) {
            if (offlineBill.isSettled == "false"||offlineBill.isSettled == false) {
              var tableStr = table.id.toString();
              var splittedTable = tableStr.split("-");

              if (splittedTable.length == 1) {
                if (parseInt(offlineBill.bill._tableId) == parseInt(table.id) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
                  table.bill = offlineBill.bill;
                  table.covers = offlineBill.bill._covers;
                }
              } else {

                if (parseInt(offlineBill.bill._tableId) == parseInt(splittedTable[0]) && parseInt(offlineBill.bill.splitNumber) == parseInt(splittedTable[1]) && offlineBill.bill.tabId == $scope.selectedTab.tabId) {
                  table.bill = offlineBill.bill;
                  table.covers = offlineBill.bill._covers;
                }
              }
            }

          });
        });

        $scope.tablePagination = Pagination.getNew(24);
        $scope.tablePagination.numPages = Math.ceil($state.current.data.tables.length / $scope.tablePagination.perPage);
        $scope.tablePagination.totalItems = $state.current.data.tables.length;
        if ($rootScope.editBill != undefined){
          $scope.startEdit();
        }
      });
    }
    $scope.startNewTakeOutDeliveryBill = function(billNumber,billId) {
      /*if ($rootScope.isInstance) {
       $scope.tableStarted={isTable:false,index:billNumber};
       nSocket.emit('table_event', {event:'add', type:'nonTable',number:billNumber,deployment_id:localStorageService.get('deployment_id')}, function (data) {
       //console.log(data);
       })
       }*/
      $scope.selectedManager=null;
      $scope.lockNonTable(billNumber);
      $scope.isBillOn=true;
      //console.log(billId);
      /*var _filterdT = _.filter($state.current.data.openedBills, {
       "id": billNumber
       })*/
      var _filterdT = _.filter($state.current.data.openedBills, {
        "billId": billId
      })
      var _t = {};
      if (_filterdT[0].id === "New") {
        $scope.startNewTakeOutDeliveryBillPost(billNumber,billId);
      }else{
        if(!$rootScope.isInstance){
          $scope.startNewTakeOutDeliveryBillPost(billNumber,billId);
        }else{
          /*fine dine*/
          // console.log(_filterdT[0]);
          $scope.getFineBillById(_filterdT[0].bill._id).then(function(offlineBill){

            $state.current.data.openedBills.splice( Utils.arrayObjectIndexOf($state.current.data.openedBills,'id',billNumber),1);
            if(offlineBill==undefined){
              growl.error('Bill not found on server,may be settled',{ttl:6000});
            }
            if(!(offlineBill.isOnlineBill||offlineBill.isAdvanceBill)) {
              var table = {
                "id": (offlineBill.bill.billNumber + (offlineBill.bill.splitNumber > 0 ? '-' + offlineBill.bill.splitNumber : ''))
              };
              table.bill = offlineBill.bill;
              table.covers = offlineBill.bill._covers;
              table.billId=offlineBill.bill._id;
              table.name=offlineBill.name;
              table.isOnlineBill=false;
              table.isAdvanceBill=false;
              /* if(_.has(offlineBill,'instance')){
               if(!(offlineBill.instance.preFix==null||offlineBill.instance.preFix==undefined) )
               {
               // alert(offlineBill.instance.preFix);
               //delete table.bill.prefix;
               table.bill.prefix = angular.copy( offlineBill.instance.preFix);
               }
               }*/
              $state.current.data.openedBills.push(table);
            }else{
              $state.current.data.openedBills.push(offlineBill);
            }
            // $state.current.data.openedBills.push(_ob);
            //_filterdT[0]=_ob;

            $scope.startNewTakeOutDeliveryBillPost(billNumber,billId);
          },function(){
            $scope.startNewTakeOutDeliveryBillPost(billNumber,billId);
          });
        }
      }

    };

    $scope.startNewTakeOutDeliveryBillPost=function(billNumber,billId){
      console.log(billId);
      $scope.isOnlineBill=false;
      $scope.showBillPage = true;
      $scope.categories = [];
      GetMasterItems();
      /* Build Categories Filter */
      _.forEach($scope.masterItems, function(i) {
        $scope.categories.push(i.category);
      });
      var _uCats = _.uniq($scope.categories, 'categoryName');
      $scope.categories = _uCats;
      $scope.categories = _.sortBy($scope.categories, ['categoryName']);
      renderCategoriesT();
      /*console.log($state.current.data.tables[parseInt(toParams.tableId)]);*/
      /* var _filterdT = _.filter($state.current.data.openedBills, {
       "id": billNumber
       })*/
      var _filterdT = _.filter($state.current.data.openedBills, {
        "billId": billId
      })
      var _t = {};
      if (_filterdT[0].id === "New") {
        $scope.renderRefreshNewBill();
        $scope.bill = new Bill(null, null, null, (($scope.selectedTab.tabType === 'takeout') ? true : null), (($scope.selectedTab.tabType === 'delivery') ? true : null), currentUser, $rootScope.db, $scope.selectedTab.tabName, $scope.selectedTab.tabId, $scope.selectedTab.tabType); //_t.bill;
        $scope.bill.charges.detail=$scope.filterChargesByTab();
        //console.log($scope.bill);
      } else {
        $scope.bill = _filterdT[0].bill;
        if($scope.bill._customer.firstname!=undefined){
          $scope.billCustomer=$scope.bill._customer;
          // $scope.selectedCustomer=$scope.bill._customer;
        }
      }
      $scope.handleBill();
      if(Utils.hasSetting('enable_advance_booking', $scope.settings)){
        var isAdvance=false;
        console.log($scope.bill);
        if(_.has( $scope.bill,'advance')){
          if(_.has($scope.bill.advance,'amount')){
            isAdvance=true;
          }
        }
        if(isAdvance)
          $scope.showAdvance=false;
        else
        { if($scope.bill._kots.length==0) $scope.showAdvance=true;else $scope.showAdvance=false;}
      }
    }
    $scope.startNewTakeOutDeliveryBillAdvance = function(bill) {
      // console.log(bill);
      $scope.isOnlineBill=false;
      $scope.showBillPage = true;
      $scope.categories = [];
      GetMasterItems();
      /* Build Categories Filter */
      _.forEach($scope.masterItems, function(i) {
        $scope.categories.push(i.category);
      });
      var _uCats = _.uniq($scope.categories, 'categoryName');
      $scope.categories = _uCats;
      $scope.categories = _.sortBy($scope.categories, ['categoryName']);
      renderCategoriesT();
      /*console.log($state.current.data.tables[parseInt(toParams.tableId)]);*/
      var _filterdT = _.filter($state.current.data.openedBills, {
        "id": bill.bill._id
      })
      var _t = {};
      $scope.bill = _filterdT[0].bill;
      $scope.handleBill();
      if(Utils.hasSetting('enable_advance_booking', $scope.settings)){
        var isAdvance=false;
        // console.log($scope.bill);
        if(_.has( $scope.bill,'advance')){
          if(_.has($scope.bill.advance,'amount')){
            isAdvance=true;
          }
        }
        if(isAdvance )
          $scope.showAdvance=false;
        else
        { if($scope.bill._kots.length==0) $scope.showAdvance=true;else $scope.showAdvance=false;}
      }
    };

    $scope.isOnlineBill=false;

    $scope.filterOnlineAddOns=function(itemId){
      console.log($scope.addons);
       var filteredAddons=[];
      var f_t_addons= _.filter($scope.addons,{tabId:$scope.selectedTab.tabId});
      if(f_t_addons.length>0){
        _.forEach( f_t_addons[0].Items,function(aitem){
          var f_i= _.filter(aitem.mapItems,{_id:itemId})
          if(f_i.length>0){
           // console.log(aitem);
            var newItem = {
          _id : aitem._id,
          name : aitem.name,
          unit : aitem.unit,
          stockQuantity : aitem.stockQuantity,
          rate:aitem.rate,
          category : {
            categoryName : aitem.category.categoryName,
            superCategory:{superCategoryName:aitem.category.superCategory.superCategoryName}
          }
        }
            filteredAddons.push(newItem);
          }
        })
      }
      console.log(filteredAddons);
      return filteredAddons;
    }
    $scope.getPassCodeUserPre=function(){
     return (($scope.bill._currentPassCodeUser==''||$scope.bill._currentPassCodeUser==null||$scope.bill._currentPassCodeUser==undefined)? currentUser.username:$scope.bill._currentPassCodeUser);
    }

    $scope.startNewTakeOutDeliveryBillOnline=function(onlineBill){
      $scope.showBillPage = true;
      $scope.categories = [];
      GetMasterItems();
      /* Build Categories Filter */
      _.forEach($scope.masterItems, function(i) {
        $scope.categories.push(i.category);
      });
      var _uCats = _.uniq($scope.categories, 'categoryName');
      $scope.categories = _uCats;
      $scope.categories = _.sortBy($scope.categories, ['categoryName']);
      renderCategoriesT();
      $scope.renderRefreshNewBill();
      $scope.bill = new Bill(null, null, null, (($scope.selectedTab.tabType === 'takeout') ? true : null), (($scope.selectedTab.tabType === 'delivery') ? true : null), currentUser, $rootScope.db, $scope.selectedTab.tabName, $scope.selectedTab.tabId, $scope.selectedTab.tabType); //_t.bill;
      $scope.bill.onlineBill=onlineBill.bill;
      if(_.has(onlineBill.bill,'charges')){
        var tempcharges=[];
        _.forEach(onlineBill.bill.charges,function(charge){
          if(_.has(charge,'name') && _.has(charge,'value')){
             var chargeOb={name:charge.name,type:'amount',value:charge.value};
             tempcharges.push(chargeOb);
          }
        })
        $scope.bill.charges.detail=tempcharges;
      }
      // Changes to accomodate bill wise offer//
      //console.log(typeof( onlineBill.bill.discount));
      if(_.has(onlineBill.bill,'discount')){
        if(typeof( onlineBill.bill.discount)=='object'){
       /* var billWiseOffer={
                            isBillWise:true,
                            time:new Date(),
                            user:currentUser.username,
                            offer:{
                              tabs:[],
                              type: {name: (onlineBill.bill.discount.type=="percentage"?"percent":onlineBill.bill.discount.type),value:parseFloat( onlineBill.bill.discount.value)},
                              name:'Express Bill Offer'
                            }
                          }
*/
       var isComplimentary=false;
         if(onlineBill.bill.discount.type=="percentage" && parseFloat( onlineBill.bill.discount.value)==100){
          isComplimentary=true;
         }
         var billWiseOffer={
                            isBillWise:true,
                            time:new Date(),
                            user:$scope.getPassCodeUserPre(),
                            offer:{
                              tabs:[],
                              type: {name: (onlineBill.bill.discount.type=="percentage"?"percent":onlineBill.bill.discount.type),value:parseFloat( onlineBill.bill.discount.value)},
                              name:'Express Offer('+ (onlineBill.bill.discount.type=="percentage"?'':'Rs.')+ parseFloat( onlineBill.bill.discount.value) + (onlineBill.bill.discount.type=="percentage"? '%':'' )+')',
                              complimentary:isComplimentary,
                              deployment_id:localStorageService.get('deployment_id'),
                              tenant_id:localStorageService.get('tenant_id')
                            },
                            isApplicableItem:true
                          }

        $scope.bill._offers.push(billWiseOffer);
      }
    }

      ////////////////////////////////////////
      $scope.isOnlineBill=true;
      var tempCustomer=angular.copy(onlineBill.bill.customer);
      tempCustomer.customerId = Utils.guid();
      tempCustomer.deployment_id=localStorageService.get('deployment_id');
      tempCustomer.tenant_id=localStorageService.get('tenant_id');
      if(!_.has(tempCustomer,'addType')){tempCustomer.addType='Residence';};
      if(!_.has(tempCustomer,'addresses')){tempCustomer.addresses=[];};
      $scope.billCustomer=tempCustomer;
      $scope.bill._customer=tempCustomer;
      /* $rootScope.db.insert('customers',{
       customerId:$scope.billCustomer.customerId,
       customerData:angular.toJson( $scope.billCustomer)
       });*/
      var flag=false;
      var mapOnlineItem=false;
      var nonMatchedItem=[];
      if(onlineBill.bill.items.length==0){growl.error('No items to render.',{ttl:2000});}
      _.forEach(onlineBill.bill.items,function(item){
        var mitem=angular.copy( getItemById(item.id));
        if(_.has(item,'discounts')) {
          if(item.discounts.length>0)
            mitem.discounts =angular.copy(  item.discounts); // [{type: 'percentage', value: 10, comment: 'jai ho', offerName: 'Express'}];
        }
        if(_.has(item,'comment')){
          mitem.comment=angular.copy( item.comment);
        }
        if(_.has(item,'addOns')){
          ///////////////////// Add on changes for online Order//
         // mitem.addOns=angular.copy( item.addOns);
         //console.log(item);
          var addonitems= $scope.filterOnlineAddOns(item.id);
          var tempAddon=[];
          var flag=false;
          _.forEach(item.addOns,function(addon){
            var _addonf=_.filter(addonitems,{_id:addon._id});
            if(_addonf.length>0){
              var tAddOn=angular.copy(_addonf[0]);
              if(_.has(addon,'quantity')){
              tAddOn.quantity=parseFloat( addon.quantity);
              tempAddon.push(tAddOn);
            }else{
              flag=true;
            }
            }else{
              flag=true;
            }
          })
          mitem.addOns=angular.copy(tempAddon);
          if(flag){growl.error('Some mapped addons are not available!',{ttl:3000})};
          //////////////////////////////////////////////////////
        }
          console.log(mitem);
        if(_.has(mitem,'_id')){
          if(item.quantity>0)
            $scope.bill.addItemWithQuantity(mitem,item.quantity);
        }
        else{
          nonMatchedItem.push(item);
          mapOnlineItem=true;
        }
      })
      if(mapOnlineItem) {
        //console.log(onlineBill.bill.source.id>10);
        growl.error('Some of the items not found in database', {ttl: 2000});
        if (onlineBill.bill.source.id >= 10) {
          $ngBootbox.confirm('Some or All items are not mapped .Do you like to map ?').then(function () {
            $modal.open({
              templateUrl: 'app/billing/item/_mapMailOrderItems.html',
              controller: ['$scope', '$modalInstance', 'items', 'unMappedItems', function ($scope, $modalInstance, items, unMappedItems) {
                $scope.masterItemsS = angular.copy(items);
                $scope.umpItems = angular.copy(unMappedItems);
                $scope.getSearchItemSelect = function (item, model, label) {
                  //$scope.searchItem.qty=1;
                  //$scope.goToQuantity();
                }
                $scope.label = function (item, number) {
                  return number + " " + item;
                }
                $scope.cancel = function () {
                  $modalInstance.dismiss('')
                };
                $scope.submit = function () {
                  $modalInstance.close(angular.copy($scope.umpItems));
                }
              }],
              size: 'md',
              resolve: {
                unMappedItems: function () {
                  return nonMatchedItem;
                },
                items: function () {
                  return $scope.masterItemsS;
                }
              }
            }).result.then(function(mapitems) {
              _.forEach(mapitems,function(titem){
                //console.log(titem);
                $scope.addItemNameWithQuantity( titem.mapName,titem.quantity);
                if(onlineBill.bill.source.id==10){
                  var _f= _.filter($scope.masterItems,{name: titem.mapName});
                  if(_f.length>0){
                    Item.updateMapEmailOrder({type:'swiggy',itemName:titem.name,item_id:_f[0]._id},function(res){
                      //console.log(res);
                      $scope.handleBill();
                    })
                    // console.log(_f);
                  }
                }
                if(onlineBill.bill.source.id==11){
                  var _f= _.filter($scope.masterItems,{name: titem.mapName});
                  if(_f.length>0){
                    Item.updateMapEmailOrder({type:'zomato',itemName:titem.name,item_id:_f[0]._id},function(res){
                      //console.log(res);
                      $scope.handleBill();
                    })
                    // console.log(_f);
                  }
                }
                if(onlineBill.bill.source.id == 12){
                  var _f= _.filter($scope.masterItems,{name: titem.mapName});
                  if(_f.length>0){
                    Item.updateMapEmailOrder({type:'foodpanda',itemName:titem.name,item_id:_f[0]._id},function(res){
                      $scope.handleBill();
                      //console.log(res);
                    })
                    // console.log(_f);
                  }
                }
              })
              //console.log(mapitems);
            });
          })
        }
      }

      //console.log(onlineBill);
      if(_.has( onlineBill.bill,'billOfferDiscount')){
        //console.log(onlineBill.bill.billOfferDiscount);
        if(onlineBill.bill.billOfferDiscount.length>0){
          $scope.offerIntegrationOnline(onlineBill.bill.billOfferDiscount[0]);
        }
      }
      $scope.handleBill();
      $scope.showAdvance=false;
    }
    $scope.getFineBillById=function(billId) {
      var d= $q.defer();
      $http.get($rootScope.url + '/api/synckots/getByBillIdOpen?billId=' + billId).then(function (results) {
        console.log(results);
        var ob = results.data;
        var offlineBill=$scope.mapFineDineObject(ob);
        d.resolve(offlineBill);
      },function(err){
        console.log(err);
        if(err.status==404)
          d.resolve();
        else
        {
          growl.error('It appear that the connection with server not established !.',{ttl:3000});
        }
      });
      return d.promise;
    }
    $scope.mapFineDineObject=function(ob){
      /*var billdata=ob.billData;
       try{
       var tempbilldata=JSON.parse(billdata);
       billdata=angular.copy( tempbilldata);
       console.log(billdata);
       }catch(ex){
       console.log(ex);
       }*/
      console.log(ob.lastModified);

      var offlineBill = {
        id: ob.id,
        tab: ob.tab,
        tabId: ob.tabId,
        serialNumber: ob.serialNumber,
        daySerialNumber: ob.daySerialNumber,
        billNumber: ob.billNumber,
        kotNumber: ob.kotNumber,
        billId: ob.billId,
        bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null, null), typeof(ob.billData)=='object'?ob.billData: JSON.parse(ob.billData)),
        // bill: _.assign(new Bill(null, null, null, null, null, null, $rootScope.db, null, null, null, null),billdata),

        totalBill: parseFloat(ob.totalBill),
        totalTax: parseFloat(ob.totalTax),
        isSynced: ob.isSynced,
        syncedOn: ob.syncedOn,
        customer: ob.customer,
        customer_address: ob.customer_address,
        isVoid: ob.isVoid,
        isSettled: ob.isSettled,
        isPrinted: ob.isPrinted,
        name:ob.instance.name ,
        created: ob.created,
        isOnlineBill: false,
        isAdvanceBill: false
      };
      offlineBill.bill.lastModified=ob.lastModified;
      return offlineBill;
    }
    $scope.offerIntegrationOnline=function(onlineOffer) {
      //  console.log(onlineOffer);
      if (_.has(onlineOffer, 'codeApplied')) {
        if (_.has(onlineOffer.offer.code, 'uses')) {
          if (onlineOffer.offer.code.uses != null) {
            $scope.code = {offerCode: onlineOffer.codeApplied, offer: onlineOffer.offer};
          }
        }
      }
      //if(offer.isBillWise) {
      for (var i = 0; i < $scope.bill._offers.length; i++) {
        if ($scope.bill._offers[i].isBillWise) {
          //$scope.bill._offers.splice(i,1);
          $scope.scrapOfferFromBill($scope.bill._offers[i].offer._id);
          break;
        }
      }
      //   }


      var billOfferApplied = {
        offer: onlineOffer.offer,
        user: $scope.getPassCodeUserPre(),
        time: new Date(),
        offerAmount: 0,
        isBillWise: true,
        isApplicableItem: true
      };
      $scope.bill._offers.push(billOfferApplied);
      $scope.bill.complimentary = _.has(onlineOffer.offer, 'complimentary') ? onlineOffer.offer.complimentary : false;
      $scope.bill.complimentaryComment=($scope.bill.complimentary?$scope.bill.complimentaryComment:"");
      $scope.bill.processBill();

      $scope.selectedOffer = null;
      /*var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
       if(_fTab.length>0){
       $scope.renderItemsTabWise(_fTab[0],$rootScope.rootMasterItems);
       }*/
      growl.success(onlineOffer.offer.name + ' offer applied', {ttl: 2000});
      console.log($scope.billStatus);
      /*if ($scope.billStatus == 'Settle') {
       $scope.billStatus = 'PrintBill';
       $scope.bill.isPrinted = false;
       }*/
    }

/*$scope.onlineBillTrigger=function(isRejected){

      if(_.has( $scope.bill.onlineBill,'trigger')) {
        if(!isRejected) {
          if(_.has($scope.bill.onlineBill.trigger, 'acceptUrl')) {
            OnlineOrder.executeUrl({url: $scope.bill.onlineBill.trigger.acceptUrl, isRejected: isRejected,id:$scope.bill.onlineBill._id}).$promise.then( function (resp) {
              growl.success('status updated successfully on server.', {ttl: 2000});
              $scope.onlineChangeLocal(isRejected);
            },function(){
              growl.error('problem connecting server,try after some time.', {ttl: 2000});
            })

          }
        }else{
          console.log($scope.bill)
          if (_.has($scope.bill.onlineBill.trigger, 'rejectUrl')) {
             OnlineOrder.executeUrl({url: $scope.bill.onlineBill.trigger.rejectUrl, isRejected: isRejected,id:$scope.bill.onlineBill._id}).$promise.then( function (resp) {
             growl.success('status updated successfully on server.', {ttl: 2000});
             $scope.onlineChangeLocal(isRejected);
             },function(){
             growl.error('problem connecting server,try after some time.', {ttl: 2000});
             })
            $scope.rejectOrderTrigger();
          }
        }
      }else{
        if(isRejected){
          if($scope.bill.onlineBill && $scope.bill.onlineBill.onlineTransactions && $scope.bill.onlineBill.onlineTransactions[0].transactionId){
            OnlineOrder.refundTransaction({tenant_id:$scope.bill.tenant_id,amount:$scope.bill.getNetAmount().toFixed(2),onlineTransaction:$scope.bill.onlineBill.onlineTransactions[0]}).$promise.then( function (resp) {
              $scope.onlineChangeLocal(isRejected);
            },function(){
              growl.error('problem connecting server,try after some time.', {ttl: 2000});
              return;
            });
          }
          else{
            $scope.onlineChangeLocal(isRejected);
          }
        }
        else{
          $scope.onlineChangeLocal(isRejected);
        }
      }
    }
*/
 $scope.onlineBillTrigger=function(isRejected){

      if(_.has( $scope.bill.onlineBill,'trigger')) {
        if(!isRejected) {
            OnlineOrder.executeUrl({url: $scope.bill.onlineBill.trigger.acceptUrl||'', isRejected: isRejected,id:$scope.bill.onlineBill._id}).$promise.then( function (resp) {
              growl.success('status updated successfully on server.', {ttl: 2000});
              $scope.onlineChangeLocal(isRejected);
            },function(){
              growl.error('problem connecting server,try after some time.', {ttl: 2000});
            })
        }else{
          console.log($scope.bill)
            /* OnlineOrder.executeUrl({url: $scope.bill.onlineBill.trigger.rejectUrl, isRejected: isRejected,id:$scope.bill.onlineBill._id}).$promise.then( function (resp) {
             growl.success('status updated successfully on server.', {ttl: 2000});
             $scope.onlineChangeLocal(isRejected);
             },function(){
             growl.error('problem connecting server,try after some time.', {ttl: 2000});
             })*/
            $scope.rejectOrderTrigger();
        }
      }else{
       /* if(isRejected){
          if($scope.bill.onlineBill && $scope.bill.onlineBill.onlineTransactions && $scope.bill.onlineBill.onlineTransactions[0].transactionId){
            OnlineOrder.refundTransaction({tenant_id:$scope.bill.tenant_id,amount:$scope.bill.getNetAmount().toFixed(2),onlineTransaction:$scope.bill.onlineBill.onlineTransactions[0]}).$promise.then( function (resp) {
              $scope.onlineChangeLocal(isRejected);
            },function(){
              growl.error('problem connecting server,try after some time.', {ttl: 2000});
              return;
            });
          }
          else{
            $scope.onlineChangeLocal(isRejected);
          }
        }
        else{
          $scope.onlineChangeLocal(isRejected);
        }*/
        $scope.onlineChangeLocal(isRejected);
      }
    }

    function refundOnilineTransactionMoney(onlineBill){
      if(onlineBill && onlineBill.onlineTransactions && onlineBill.onlineTransactions[0].transactionId){

      }
    }

    $scope.rejectOrderTrigger = function() {
      console.log("rejected order")
      var modalInstance = $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/item/_reject_reason.html',
        controller: ['$scope', '$modalInstance', 'growl', function($scope, $modalInstance, growl) {
          $scope.selectedReason = "Delivery boy not available";
          $scope.reasons = ["Delivery boy not available", "Out of delivery radius", "Items Out of Stock", "others"];
          $scope.submit = function(){
            console.log($scope.selectedReason)
            $modalInstance.close($scope.selectedReason);
          }
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };

        }]
      });

      modalInstance.result.then(function(reason) {
        OnlineOrder.executeUrl({url: $scope.bill.onlineBill.trigger.rejectUrl||'', isRejected: true,id:$scope.bill.onlineBill._id,reason:reason}).$promise.then( function (resp) {
          growl.success('status updated successfully on server.', {ttl: 2000});
          $scope.onlineChangeLocal(true);
        },function(){
          growl.error('problem connecting server,try after some time.', {ttl: 2000});
        })

      }, function() {
        console.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.onlineChangeLocal=function(isRejected) {
      //console.log($scope.bill.onlineBill);
      if(!$rootScope.isInstance) {
        $rootScope.db.update('OnlineBills', {
          'DownloadStatus': isRejected ? 'rejected' : 'accepted'
        }, {
          'OnlineBillId': $scope.bill.onlineBill._id
        }).then(function () {
          if (!isRejected) {
            if(_.has($scope.bill.onlineBill, 'customer')) {
              isCustomerFound($scope.bill.onlineBill.customer).then(function(flag) {
                console.log(flag);
                $scope.newCustomer = $scope.bill.onlineBill.customer;
                if (!flag) {
                  $scope.newCustomer.customerId = Utils.guid();
                  $scope.newCustomer.tenant_id = localStorageService.get('tenant_id'); //currentUser.tenant_id;
                  $scope.newCustomer.deployment_id = localStorageService.get('deployment_id'); //currentUser.deployment_id;
                }
                $rootScope.db.insert('customers', {
                  'customerId': $scope.newCustomer.customerId,
                  'customerData': JSON.stringify($scope.newCustomer)
                }).then(function(result) {
                  console.log("Inserted Customer to WebSQL");
                  var tempcust = angular.copy($scope.newCustomer);
                  $scope.billCustomer = angular.copy(tempcust);
                  delete tempcust.addresses;
                  //$scope.bill._customer = angular.copy($scope.newCustomer);
                  $scope.bill._customer = tempcust;
                  $scope.selectedCustomer = '';
                });
              });
            }
            $scope.doBilling();
          }
          else {
            refreshState();
          }
        })
      }else {
        /*fine dine*/
        $http.post($rootScope.url + '/api/synckots/updateOnlineBill', {
          "OnlineBillId": $scope.bill.onlineBill._id,
          "DownloadStatus": isRejected ? 'rejected' : 'accepted'
        }).then(function (updatestatus) {
          if (!isRejected) {
            $scope.doBilling();
          }
          else {
            refreshState();
          }
        });
        /*fine dine end*/
      }
    }


    $scope.onlineChangeLocalMenu=function(isRejected,bill) {
      console.log(bill);
      if (_.has(bill, 'trigger')) {
        if (_.has(bill.trigger, 'rejectUrl')) {
          $scope.cancelOrder(true,bill);
        }else{

          $rootScope.db.update('OnlineBills', {
            'DownloadStatus': "deleted"
          }, {
            'OnlineBillId': bill._id
          }).then(function () {
            refreshState();
          })
        }
      }else{
        $rootScope.db.update('OnlineBills', {
          'DownloadStatus': "deleted"
        }, {
          'OnlineBillId': bill._id
        }).then(function () {
          refreshState();
        })
      }
    }
    $scope.cancelOrder=function(isRejected,bill){
      OnlineOrder.executeUrl({url: '', isRejected: isRejected,id:bill._id}).$promise.then( function (resp) {
        growl.success('status updated successfully on server.', {ttl: 2000});
        $rootScope.db.update('OnlineBills', {
          'DownloadStatus': "deleted"
        }, {
          'OnlineBillId': bill._id
        }).then(function () {
          refreshState();
        })
      },function(){
        growl.error('problem connecting server,try after some time.', {ttl: 2000});
      })
    }

    $scope.filterWaiterDelivery = function(role) {

      if(role=='Delivery' && $scope.deliveryBoys && $scope.tp_integrated)   //---added by rajat for technopurple---//
      {
        var result=[];
        _.forEach($scope.deliveryBoys,function(user){
          if(user.location && user.location.action=='check_in')
            result.push(user);
        });
        return result;
      }

      var filteredUser = [];
      for (var i = 0; i < $scope.users.length; i++) {
        for (var j = 0; j < $scope.users[i].selectedRoles.length; j++) {
          if ($scope.users[i].selectedRoles[j].name == role) {
            filteredUser.push($scope.users[i]);
            break;
          }
        }
      }

      if(role=='Delivery' && deliveryPartners.length>0){
        _.forEach(deliveryPartners,function(p){
          filteredUser.push(p);
        })
      }
      return filteredUser;
    };

    function bindBillDataToTabBill(tabtype) {
      var _getTabName = _.filter($scope.tabs, {
        _id: $scope.selectedTab.tabId
      });
      /* $scope.tab = _getTabName[0].tabName;
       $scope.tabId = toParams.tabId;
       $scope.tabType = tabtype;*/
      GetMasterItems();
      /* Build Categories Filter */
      _.forEach($scope.masterItems, function(i) {
        $scope.categories.push(i.category);
      });
      var _uCats = _.uniq($scope.categories, 'categoryName');
      $scope.categories = _uCats;
      renderCategoriesT();
      var billId = toParams.billId;
      var _f = _.filter($scope.bills, {
        billId: billId
      });
      if (_f.length > 0) {
        $scope.bill = _f[0].bill;
      }
      $scope.handleBill();
    };

    function renderCategoriesT() {
      $scope.categoriesT = [];
      $scope.dataLoadedCat = false;
      var count = 1;
      var catArr = [];
      _.forEach($scope.categories, function(_cat, i) {
        catArr.push(_cat);
        if (count % 2 == 0) {
          $scope.categoriesT.push(catArr);
          catArr = [];
        } else {
          if (i == $scope.categories.length - 1) {
            $scope.categoriesT.push(catArr);
          }
        }
        count++;
      });
      // $scope.dataLoadedCat = true;
      //console.log(JSON.stringify($scope.categoriesT));
      $timeout(function() {
        $scope.dataLoadedCat = true;
      });
    };

    function calculateInclusiveTax(item) {
      var rate = item.rate;
      var mtax = 0;
      var ttax = 0;
      _.forEach(item.taxes, function(_tax) {
        rate = item.rate;
        if (_tax.type === 'percentage')
          mtax = (0.01 * _tax.value);
        if (_.has(_tax, 'cascadingTaxes')) {
          if (_tax.cascadingTaxes.length > 0) {
            var _ftax = _.filter(item.taxes, {
              _id: _tax.cascadingTaxes[0]._id
            });
            if (_ftax.length > 0) {
              if (_tax.cascadingTaxes[0].type === 'percentage')
                mtax += ((mtax * 0.01 * _tax.cascadingTaxes[0].value));
            }
          }
        }
        /*  if(item._id=="54edd2f94321914015bdce33"){
         console.log(item.rate-rate);
         }*/
        ttax += (mtax);
        mtax = 0;
      });
      return Utils.roundNumber((item.rate / (1 + ttax)), 2);
    };
    $scope.masterItemsS=[];
    function GetMasterItems() {
      //console.log($scope.selectedTab.tabId);
      var e_filter = _.filter($rootScope.rootExplorerItems, {
        tabId: $scope.selectedTab.tabId
      });
      var et_filter = _.filter($rootScope.rootExplorerItemsT, {
        tabId: $scope.selectedTab.tabId
      });
      var m_filter = _.filter($rootScope.rootMasterItems, {
        tabId: $scope.selectedTab.tabId
      });
      var m_filterS = _.filter($rootScope.rootMasterItemsS, {
        tabId: $scope.selectedTab.tabId
      });
      if (m_filter.length > 0) $scope.masterItems =  m_filter[0].Items,['number'];
      if (m_filterS.length > 0) $scope.masterItemsS =_.sortBy( m_filterS[0].Items,['number','name']);
      if (e_filter.length > 0) $scope.explorerItems = e_filter[0].Items;


      /*if (Utils.hasSetting('orderby_itemnumber', $scope.settings)) {
       $scope.explorerItems = _.sortBy($scope.explorerItems, ['number']);
       }
       else {
       $scope.explorerItems = _.sortBy($scope.explorerItems, ['name']);
       }*/
      $scope.dataLoaded = false;
      var tempExplorerItemT = [];
      if (et_filter.length > 0) {
        tempExplorerItemT = et_filter[0].Items;
      }
      $timeout(function() {
        $scope.explorerItemsT = tempExplorerItemT;
        $scope.dataLoaded = true;
      });


      //  $scope.getExplorerItemsT(null);

      // $scope.explorerItems=_.sortByAll($scope.explorerItems, ['name']);
      // console.log(_.sortByAll($scope.masterItems, ['name']));
    };


    //$scope.explorerItemsT = [];
    //  $scope.dataLoaded = false;
    $scope.getExplorerItemsT = function(cat) {

      $scope.dataLoaded = false;
      // console.log( cat);
      var tempExplorerItemT = [];
      // $scope.explorerItemsT=[];

      var explorerItems = $scope.explorerItems;
      var expItems = [];
      if (cat) {
        _.forEach($scope.explorerItems, function(_item) {
          if (_item.category.categoryName == cat.categoryName) {
            expItems.push(_item);
          }
        });
        explorerItems = expItems;
      }
      // console.log(explorerItems);
      var itemarr = [];
      var count = 1;
      for (var i = 0; i < explorerItems.length; i++) {
        itemarr.push(explorerItems[i]);

        if (count % 5 == 0) {
          // $scope.explorerItemsT.push(itemarr);
          tempExplorerItemT.push(itemarr);
          itemarr = [];
        } else {
          if (i == explorerItems.length - 1) {
            if (itemarr.length > 0) {
              // $scope.explorerItemsT.push(itemarr);
              tempExplorerItemT.push(itemarr);
            }
          }
        }
        count++;
        // if(count>60){break;}
      };
      // $scope.explorerItemsT=Utils.getItemPages(explorerItems,5)
      $timeout(function() {
        $scope.explorerItemsT = tempExplorerItemT;
        $scope.dataLoaded = true;
      });
      //$scope.dataLoaded = true;
      console.log($scope.explorerItemsT);
    };


    /*  function getItemWiseKot(mBill, itemName) {
     //console.log(mBill);
     var kot;
     var kotWithBill = {};
     for (var i = 0; i < mBill._kots.length; i++) {
     for (var j = 0; j < mBill._kots[i].items.length; j++) {
     if (mBill._kots[i].items[j].name === itemName) {
     kot = angular.copy(mBill._kots[i]);
     var item = [];
     item.push(angular.copy(mBill._kots[i].items[j]));
     item[0].quantity = 1;
     kot.items = item;
     if (mBill._kots[i].items[j].quantity === 1) {
     mBill._kots[i].items.splice(j, 1);
     if (mBill._kots[i].items.length === 0) {
     mBill._kots.splice(i, 1);
     }
     kotWithBill = {
     kotOb: kot,
     bill: angular.copy(mBill)
     };
     return kotWithBill;
     } else {
     mBill._kots[i].items[j].quantity -= 1
     kotWithBill = {
     kotOb: kot,
     bill: angular.copy(mBill)
     };
     return kotWithBill;
     }
     kotWithBill = {
     kotOb: kot,
     bill: mBill
     };
     return kotWithBill;
     break;
     }
     }
     }
     return kotWithBill;
     };*/

    function renderTakeOutDelivery(tabType) {
      var _getTabName = _.filter($scope.tabs, {
        _id: $scope.selectedTab.tabId
      });
      /*console.log(_getTabName);
       $scope.tab = _getTabName[0].tabName;
       $scope.tabId = toParams.tabId;
       $scope.tabType = tabType;*/
      GetMasterItems();

      /* Build Categories Filter */
      _.forEach($scope.masterItems, function(i) {
        $scope.categories.push(i.category);
      });
      var _uCats = _.uniq($scope.categories, 'categoryName');
      $scope.categories = _uCats;
      renderCategoriesT();
      $scope.bill = new Bill(null, null, null, (($scope.selectedTab.tabType === 'takeout') ? true : null), (($scope.selectedTab.tabType === 'delivery') ? true : null), currentUser, $rootScope.db, $scope.selectedTab.tabName, $scope.selectedTab.tabId, $scope.selectedTab.tabType);
    };


    $scope.renderRefreshNewBill = function(cover, bill) {
      if ($scope.bill.serialNumber == null) {
        if (Utils.hasSetting('enable_covers', $scope.settings, $scope.selectedTab.tabType) || Utils.hasSetting('cover_mandatory',$scope.settings,$scope.selectedTab.tabType)) {
          $modal.open({
            templateUrl: 'app/billing/item/_cover.html',
            controller: ['$scope', '$modalInstance', function($scope, $modalInstance, Cover) {
              var tempCovers = '';
              if (bill) {
                if (_.has(bill, '_covers'))
                  tempCovers = parseInt(bill._covers);
              }
              $scope.covers = tempCovers > 0 ? tempCovers : '';
              $scope.bill = bill;
              $scope.cancel = function() {
                 if($scope.bill)
                $modalInstance.dismiss($scope.bill._covers);
               else
                $modalInstance.dismiss(null);
                //$modalInstance.dismiss('')
              };
              $scope.addCover = function() {
                if(!parseInt($scope.covers) ){
                  growl.error('Too many Covers, slow down homie!', {
                    ttl: 2000
                  });
                  return false;
                }
                if (parseInt($scope.covers) > 9999) {
                  growl.error('Too many Covers, slow down homie!', {
                    ttl: 2000
                  });
                } else {
                  $scope.cancel = $modalInstance.close({
                    cover: $scope.covers,
                    bill: $scope.bill
                  });
                }
                //$scope.cancel = $modalInstance.close({cover: $scope.covers, bill: $scope.bill});
              }
            }],
            size: 'sm'
          }).result.then(function(covers) {
            if (parseInt(covers.cover)) {
              console.log(covers.bill);
              //  $scope.bill._covers = covers;
              var _parsedCovers = covers.cover;
              if (bill) {
                covers.bill._covers = _parsedCovers;
                covers.bill.updateBill({
                  _covers: _parsedCovers
                }, 'update');
              } else {
                $scope.bill._covers = _parsedCovers;
              }
            } else {
              growl.error('covers should be in the integer.', {
                ttl: 2000
              })
              console.log(covers);
            }
          },function(prevCovers){
            console.log(prevCovers);
            if(Utils.hasSetting('cover_mandatory',$scope.settings,$scope.selectedTab.tabType) && (!prevCovers || !parseInt(prevCovers)))
              $scope.initializeTab();
          });

          // console.log($scope.tabType);
        }
      }
    };

    $scope.tableBreakSlides = function(tables, byRows) {
      var d = $q.defer();
      var itemarr = [];
      var count = 1;
      var tempExplorerItemT = [];
      console.log("Table TAB Render inside BreakSlides");
      $timeout(function() {
        for (var i = 0; i < tables.length; i++) {
          itemarr.push(tables[i]);
          if (count % byRows == 0) {
            tempExplorerItemT.push(itemarr);
            itemarr = [];
          } else {
            if (i === tables.length - 1) {
              if (itemarr.length > 0) {
                tempExplorerItemT.push(itemarr);
              }
            }
          }
          count++;
        };

        $scope.compileTableState(tempExplorerItemT).then(function(_view) {
          d.resolve(_view);
        });
        //console.log(JSON.stringify(tempExplorerItemT));

      });

      return d.promise;

    };

    $scope.compileTableState = function(tables) {
      var d = $q.defer();
      $timeout(function() {

        /* Start Prepare: Explorer Items Compile */
        console.log('Starting Compile: TabeSlides');
        //var template = $templateCache.put('itemExplorer.html', '<div class="col-md-2" style="width: 126px;" ng-repeat="items in data"><div ng-repeat="item in items"><div class="s-class-cat"><a ng-click="addItemFromExplorer(item)" class="btn btn-default_p ng-binding">{{item.name}}</a></div></div></div>');
        var template = $templateCache.get('tableSlides.html');
        var slideScope = $scope.$new();
        angular.extend(slideScope, {
          data: tables
        });
        //console.log(template);
        var _t = $compile($('<div id="explorer">' + template + '</div>'))(slideScope);
        console.log('End Compile: TableSlides');
        //console.log(_t);
        d.resolve(_t);
        /* End Prepare: Explorer Items Compile */

      });
      return d.promise;
    };

    $scope.transposeTable = function(arr, arrlen) {
      var arr = arr;
      var arrlen = arrlen;
      var d = $q.defer();
      $timeout(function() {
        for (var i = 0; i < arrlen; i++) {
          for (var j = 0; j < i; j++) {
            //swap element[i,j] and element[j,i]
            var temp = arr[i][j];
            arr[i][j] = arr[j][i];
            arr[j][i] = temp;
          }
        }
        d.resolve(arr);
      });
      return d.promise;
    };

    // $scope.changedWaiter=null;
    $scope.changeWaiterDelivery = function(bill) {
      if (bill._items.length == 0) {
        if (bill._kots.length > 0) {
          bill.updateBill({}, 'update').then(function(resultData) {
            //console.log(resultData);
             $scope.bill.lastModified = (!(resultData == undefined || resultData == null)) ? resultData : null;

            growl.success('Change updated.', {
              ttl: 1000
            });
           // bill.lastModified = (!(resultData == undefined || resultData == null)) ? resultData.lastModified : null;
            delivery.assignOrder(bill,deliveryOrders,deploymentPartners);
           },function(err){
            growl.error('There is problem updating the transaction.',{ttl:3000});
            if(err.data.error=='inValid'){
              $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

              })
            }
          });
        }
      }
    };

    /*$scope.getOfflineBillsByTab = function() {
     var d = $q.defer();
     Sync.syncOfflineBills($scope.selectedTab.tabId).then(function(bills) {
     //   console.log(bills);
     d.resolve(bills);
     });
     return d.promise;
     };*/
    $scope.billStruc = {
      items: [],
      postSubtotalTaxes: []
    };

    $scope.getOfflineBillsByTab = function() {
      var d = $q.defer();
      Sync.syncOfflineBills($scope.selectedTab.tabId,localStorageService.get('deployment_id')).then(function(bills) {
        // console.log(bills);
        d.resolve(bills);
      });
      return d.promise;
    }
    $scope.billStruc = {
      items: [],
      postSubtotalTaxes: []
    };

    /* Tabs */
    $scope.tabs = $rootScope.tabs;

    $scope.onlineOrderCountTabs=function(){

    }

    $scope.gotoTablePage = function() {
      /*var _tNo = $scope.gotoTableNumber;
       console.log($scope.pagination);*/
      $scope.pagination.toPageId($scope.gotoTableNumber - 1);
    };

    $scope.active = function(route) {
      // return $state.is(route);
    };

    $scope.isTableFiltered = false;
    $scope.tempTables = [];
    $scope.filterUnsettledTableBills = function() {
      $scope.tempTables = angular.copy($state.current.data.tables);
      $state.current.data.tables = _.filter($state.current.data.tables, function(table) {
        return _.has(table, 'bill');
      });
      $scope.isTableFiltered = true;
    };
    $scope.clearFilterUnsettledTableBills = function() {
      $scope.isTableFiltered = false;
      $state.current.data.tables = $scope.tempTables;
      $scope.tempTables = [];
    };


    $scope.getWaiterName = function(waiter) {
      return waiter.firstname + ' ' + waiter.lastname;
    };
    $scope.getDelivererName = function(deliverer) {
      return deliverer.firstname + ' ' + deliverer.lastname;
    }
    $scope.setWaiter = function(waiter) {
      console.log(waiter);
      $scope.bill._waiter;
    };

    $scope.subcategories = [];
    $scope.selectedSuperCategory = {};
    $scope.clickCategory = function(sc) {
      $scope.selectedSuperCategory = sc;
      $scope.categories = sc.child;
    };

    $scope.showCustomerPanel = function() {
      return _.isEmpty($scope.selectedCustomer);
    };

    /*$scope.selectedCustomer = {};*/
    $scope.billCustomer;
    $scope.selectedCustomer;
    $scope.getSelectedCustomer = function(item, model, label) {
      // console.log(item);
      // $scope.selectedCustomer=angular.copy(item);
      var tempCustomer = angular.copy(item);
      $scope.billCustomer = angular.copy(tempCustomer);
      console.log($scope.billCustomer);
      delete tempCustomer.addresses;
      /*$scope.bill._customer = angular.copy(item);*/
      $scope.bill._customer = tempCustomer;
      billResource.CRMDetailFromBill({customerId: tempCustomer.customerId}).$promise.then(function(custDetail){
        $scope.bill._customer.detail=custDetail;
      })
      intercom.registerEvent('CRM');
    };
    $scope.customerSearchEnter = function(customer) {
      //  console.log( $scope.selectedCustomer);
      $scope.selectedCustomer = customer;
      if (!angular.isUndefined($scope.selectedCustomer)) {
        delete $scope.bill._customer;
        if ($scope.selectedCustomer.length > 7) {
          if (parseInt($scope.selectedCustomer)) {
            $scope.openCustomerModal('', {
              mobile: $scope.selectedCustomer,
              addType: 'Residence'
            });
          } else {
            growl.error('Feed in the right mobile/phone number.', {
              ttl: 2000
            });
          }
        } else {
          growl.error('Feed in the right mobile/phone number.', {
            ttl: 2000
          });
        }
      }
      /*$timeout(function(){
       console.log($scope.customerForm.mobileSearch.$modelValue);
       return false;
       },100)*/

    };
    $scope.customerModal = function() {
      $scope.openCustomerModal('', $scope.billCustomer);
    };
    $scope.addDynamicComment=function(item){
      console.log(item);
      var f_Items=   _.filter($scope.bill._items, function(item) {
        return !_.has(item,'offer');
      });
      var _item = _.find(f_Items, {'_id': item._id});
      if(item.comments && item.comments.length!=0 && (_item == undefined || _item.comment==undefined)  )
      {
        $modal.open({
          backdrop : 'static',
          keyboard :false,
          templateUrl: 'app/billing/_dynamic_comment.html',
          size: 'md',
          scope:$scope,
          resolve: {
            item:function(){
              return item;
            }
          },
          controller: ['$scope', '$modalInstance','item', function ($scope, $modalInstance,item) {
            $timeout(function(){
              $scope.$broadcast('SetFocus');
            },100)

            $scope.item=item;
            $scope.selected={};
            $scope.errorMessage="";
            // $scope.selected = { value: $scope.itemArray[0] };
            $scope.cancel=function(){
              $modalInstance.dismiss();
            }
            $scope.setFocus={};
            $scope.commentChange=function(){
              $scope.setFocus.focusButton=true;
            }
            $scope.addComment=function(){
              var index= _.findIndex($scope.bill._items, function(i) {
                return i._id == item._id;
              });
              console.log(index);
              if($scope.selected.comment!=undefined)
              {
                $scope.bill._items[index].comment=$scope.selected.comment.name;
                $scope.setFocus.focusItem=true;
                $modalInstance.dismiss();
              }
              else
              {
                console.log('Empty');
                $scope.errorMessage="Select Atleast One Comment";
              }
            }

          }]
        }).result.then(function (ob)
        {
          console.log(ob);
        })
      }
    }

    $scope.searchItem = {
      name: "",
      qty: ""
    };
    if($scope.windowWidth<=1040)
      $scope.setFocus = {
        focusItem: false,
        focusInput: false
      }
    else{
      $scope.setFocus = {
        focusItem: true,
        focusInput: false
      }
    }

    Number.prototype.countDecimals = function () {
      if(Math.floor(this.valueOf()) === this.valueOf()) return 0;
      return this.toString().split(".")[1].length || 0;
    }

    $scope.getSelectedItem = function() {
      if($scope.checkBeforePunchItem()){return false;}
      var enteredQty=parseFloat( $scope.searchItem.qty);
      var countDecimal= enteredQty.countDecimals();
      if(countDecimal>3){
        growl.error('Decimal more that 3 is not allowed',{ttl:3000});
        return false;
      }
      if(! Utils.hasSetting('enable_sweet_shop', $scope.settings)) {
        if(countDecimal>0){
          growl.error('Decimal quantity is not allowed',{ttl:3000});
          return false;
        }
      }
      /*if( Utils.hasSetting('enable_sweet_shop', $scope.settings))
       {

       var enteredQty=$scope.searchItem.qty;
       if (enteredQty != Utils.roundNumber(enteredQty)) {
       growl.error('Decimal quantity is not allowed',{ttl:3000});
       return false;
       };
       }*/
      $scope.addSearchedItemWithQuantity();
      $scope.setFocus.focusInput = false;
      $scope.setFocus.focusInput = false;
      //$scope.setFocus.focusItem = true;
      if($scope.windowWidth<=1040)
        $scope.setFocus.focusItem = false;
      else
        $scope.setFocus.focusItem = true;
      $timeout(function() {
        $scope.setFocus.focusItem = false;
      }, 100);
      $scope.handleBill();
    };

    $scope.addSearchedItemWithQuantity = function(updatedPrice) {
      $scope.isBillingOn=false;
      var tempItem;
      var _f_item = _.filter($scope.masterItems, {
        name: $scope.searchItem.name
      })
       if(parseFloat($scope.searchItem.qty)>0 && parseFloat($scope.searchItem.qty)%1==0){
      if($scope.bar_exchange_enabled && _f_item.category.isliquor && (updatedPrice==null||updatedPrice==undefined) && $scope.selectedTab.tabType=='table'){
           $scope.isBillingOn=true;                  //added for bar exchange
           barExchange.getItemPrice(_f_item[0],$scope.bill,$scope.bar_exchange_enabled,$scope.searchItem.qty).then(function(updatedPrice){
           $scope.isBillingOn=false;
           $scope.addSearchedItemWithQuantity(updatedPrice);
        });
        return false;
       }
      }
      //console.log(_f_item);
      if (_f_item.length > 0) {
        tempItem= angular.copy(_f_item[0]);
        if(Utils.hasSetting('inclusive_tax',$scope.settings)){
          tempItem.rate=calculateInclusiveTax(tempItem);
        }
        if(Utils.hasSetting('dynamic_comment',$scope.settings)){
          console.log("dynamic comment activated");
          $scope.addDynamicComment(tempItem);
          $scope.setFocus.focusItem = true;
          console.log($scope.setFocus.focusItem);
          }
        if (parseFloat($scope.searchItem.qty)) {
          if ($scope.searchItem.qty < 0) {
            growl.error('Negative Quantity not allowed!', {
              ttl: 2000
            });
          } else {
            $scope.bill.addItemWithQuantity(tempItem, parseFloat($scope.searchItem.qty),updatedPrice);
             /*Changes to implement addon compulsory*/
            if (Utils.hasSetting('addon_compulsory', $scope.settings)) {
              console.log("addon Compulsory activated");
              var addons = $scope.filterAddons($scope.bill._items[$scope.bill._items.length - 1]._id);
              if (addons.length != 0) {
                $scope.expressDiscountApply('lg', tempItem, ($scope.bill._items.length - 1), false, false, false, true);
              }
            }
          }
        } else {
          growl.error('Quantity should be in decimal only.', {
            ttl: 2000
          });
        }
      } else {
        growl.error('Item not found with this name.', {
          ttl: 2000
        });
      }
      $scope.searchItem.name = "";
      $scope.searchItem.qty = "";
    };

    $scope.addItemNameWithQuantity = function(name,qty) {
      var _f_item = _.filter($scope.masterItems, {
        name: name
      })
      //console.log(_f_item);
      if (_f_item.length > 0) {
        if(Utils.hasSetting('inclusive_tax',$scope.settings)){
          _f_item[0].rate=calculateInclusiveTax(_f_item[0]);
        }
        if (parseFloat(qty)) {
          if (qty < 0) {
            growl.error('Negative Quantity not allowed!', {
              ttl: 2000
            });
          } else {
            $scope.bill.addItemWithQuantity(_f_item[0], parseFloat(qty));
          }
        } else {
          growl.error('Quantity should be in decimal only.', {
            ttl: 2000
          });
        }
      } else {
        growl.error('Item not found with this name.', {
          ttl: 2000
        });
      }
    };

    $scope.GoToNextElement = function(form) {
      if($scope.checkBeforePunchItem()){return false;}
      var enteredQty=parseFloat( $scope.searchItem.qty);
      var countDecimal= enteredQty.countDecimals();
      if(countDecimal>3){
        growl.error('Decimal more that 3 is not allowed',{ttl:3000});
        return false;
      }
      if(! Utils.hasSetting('enable_sweet_shop', $scope.settings)) {
        if(countDecimal>0){
          growl.error('Decimal quantity is not allowed',{ttl:3000});
          return false;
        }
      }
      //console.log(form);
      if (parseFloat($scope.searchItem.qty)) {
        $scope.setFocus.focusItem = true;
        $scope.setFocus.focusInput = false;
        $scope.addSearchedItemWithQuantity();
        $timeout(function() {
          $scope.setFocus.focusItem = false;
        }, 100);
      } else {

        if ($scope.searchItem.qty != '')
          growl.error('Item quantity should be in decimal only.', {
            ttl: 2000
          });
        $scope.searchItem.qty = "";
      }
      $scope.handleBill();
    };

    $scope.goToQuantity = function() {
      $scope.setFocus.focusItem = false;
      $scope.setFocus.focusInput = true;
      $timeout(function() {
        $scope.setFocus.focusInput = false;
      }, 100);
     // console.log($scope.searchItem.name);
     // if($scope.searchItem.name!=''){ $scope.searchItem.qty=1;}
    }

    $scope.getSearchItemSelect = function(item, model, label) {
      $scope.goToQuantity();
    }

    $scope.label=function(item,number){
      return number+" "+item;
    }

    $scope.getCustomersQuery = function(q) {
      return Customer.search({
        q: q,
        deployment_id: localStorageService.get('deployment_id'),
        tenant_id:localStorageService.get('tenant_id')
      }).$promise.then(function(customers) {
        var _c = [];
        _.forEach(customers, function(c) {
          _c.push(c);
        });
        //  console.log(_c);
        // $scope.selectedCustomer='';
        return _c;
      });
    };

    $scope.getFullname = function(customer) {
      if (_.isObject(customer)) {
        return customer.firstname + '(' + customer.mobile + ')';
      }
    };


    $scope.explorerItemsSwipeLeft = function(event) {
      console.log("Swiped Left");
      $scope.itemPagination.nextPage();
    };

    $scope.explorerItemsSwipeRight = function(event) {
      console.log("Swiped Right");
      $scope.itemPagination.prevPage();
    };

    $scope.billingContentsSwipeUp = function(event) {
      console.log("Swipe Up");
    };

    $scope.billingContentsSwipeDown = function(event) {
      console.log("Swipe Down");
    };

    $scope.addItemFromExplorer = function(item,updatedPrice) {
      //console.log(item);
      if($scope.checkBeforePunchItem()){return false;}
      if(item.outOfStock){
         growl.error("Item is out of stock",{ttl:3000});
         return false;
      }

      if($scope.bar_exchange_enabled  && item.category.isliquor && (updatedPrice==null||updatedPrice==undefined) && $scope.selectedTab.tabType=='table'){
           $scope.isBillingOn=true;                  //added for bar exchange
           barExchange.getItemPrice(item,$scope.bill,$scope.bar_exchange_enabled).then(function(updatedPrice){
           $scope.addItemFromExplorer(item,updatedPrice);
          });
         return false;
       }

       //console.log($scope.bill);
      $scope.isBillingOn=false;
      if(Utils.hasSetting('dynamic_comment',$scope.settings)){
        console.log("dynamic comment activated");
        $scope.addDynamicComment(item);
      }

      if($scope.selectedOffer!=null){
        var offerItem=getItemById(item._id);
        if(_.has(item,'offer')){
          offerItem.offer=item.offer;
        }
        if(_.has(item,'discounts')){
          offerItem.discounts=item.discounts;
          //  offerItem.discounts[0].discountAmount= Utils.roundNumber( ((offerItem.discounts[0].type=='percent')?(offerItem.rate*offerItem.discounts[0].value*.01):offerItem.discounts[0].value))
        }
        /*if(_.has(item,'isDiscounted')){
         offerItem.isDiscounted=item.isDiscounted;
         }*/
        // console.log(offerItem);
        if($scope.selectedOffer.offer.offer.type.name=='comboOffer'){
        for(var i=0;i<$scope.bill._items.length;i++){
          if($scope.bill._items[i].isReplaceable==true){
            if(_.has( $scope.bill._items[i],'isDiscounted')){
               offerItem.isDiscounted=$scope.bill._items[i].isDiscounted;
            }

            if(_.has($scope.bill._items[i],'discounts')){
              if($scope.bill._items[i].discounts.length>0){
                var tempdiscount=$scope.bill._items[i].discounts[0];
                tempdiscount.value=offerItem.rate -($scope.bill._items[i].rate-tempdiscount.value);
               /* var tempOfferNewItems=angular.copy($scope.selectedOffer.offerItems.applicable[0].Items);
                tempOfferNewItems.splice(Utils.arrayObjectIndexOf($scope.selectedOffer.offerItems.applicable[0].Items,'_id',$scope.bill._items[i]._id),1);
                tempOfferNewItems.push(offerItem);
                tempdiscount.value=$scope.calculateComboOfferAmount(tempOfferNewItems);*/
                offerItem.discounts=[];
                offerItem.discounts.push(tempdiscount);
              }
            }
            $scope.bill._items.splice(i,1);
            $scope.bill.addItem(offerItem,updatedPrice);

            $scope.bill.processBill();
            $scope.selectedOffer=null;
            $scope.resetBillItems();

            return false;
          }
        }
      }
        $scope.bill.addItem(offerItem,updatedPrice);
        $scope.handleOffer(item);
      }else{
        $scope.bill.addItem(getItemById(item._id),updatedPrice);
        /*Changes to implement compulsory addon*/
        if (Utils.hasSetting('addon_compulsory', $scope.settings)) {
          console.log("addon Compulsory activated");
          var addons = $scope.filterAddons($scope.bill._items[$scope.bill._items.length - 1]._id);
          if (addons.length != 0) {
            $scope.expressDiscountApply('lg', item, ($scope.bill._items.length - 1), false, false, false, true);
          }
        }
      }
      $scope.handleBill();
      //console.log($scope.bill);
    };

    $scope.handleOffer=function(item) {
      console.log($scope.selectedOffer);
      /* if(!_.has( $scope.selectedOffer.offer[0],'billedItems')){
       $scope.selectedOffer.offer[0].billedItems={};
       $scope.selectedOffer.offer[0].billedItems={applicable:[],applied:[]};
       }*/


      if (!$scope.selectedOffer.validated.applicable) {
        // console.log($scope.selectedOffer);

       /*Least value Item Offer Integration*/
       if($scope.selectedOffer.offer.offer.getLeastValueItem){
        //alert();
        if(!_.has($scope.selectedOffer.offer.offer.leastItemValueCount,'billedBuyQuantity')){
          $scope.selectedOffer.offer.offer.leastItemValueCount.billedBuyQuantity=1;
        }else{
          $scope.selectedOffer.offer.offer.leastItemValueCount.billedBuyQuantity += 1;
        }

        var factor=0;
        var billedQty=parseInt( $scope.selectedOffer.offer.offer.leastItemValueCount.billedBuyQuantity);
        var buyqty=parseInt( $scope.selectedOffer.offer.offer.leastItemValueCount.buyQuantity);
        var divqty= billedQty/buyqty;
        var flag=false;
        if(divqty==Math.ceil(divqty)){
          flag=true;
          factor=Math.ceil(divqty);
          $scope.selectedOffer.isLeastOfferApplied=true;
        }
        //if(parseInt( $scope.selectedOffer.offer.offer.leastItemValueCount.billedBuyQuantity) == parseInt( $scope.selectedOffer.offer.offer.leastItemValueCount.buyQuantity)){
        //  growl.success("Offer apllied successfully",{ttl:2000});
          if(flag){
          var tempItems=angular.copy( $scope.bill._items);
          var sortedItems=_.sortBy(tempItems,['rate']);
          //console.log(sortedItems);
          for(var i=0;i< parseInt(parseInt( $scope.selectedOffer.offer.offer.leastItemValueCount.getQuantity)* factor) ;i++){
            var tmpItem=sortedItems[i];
             var offerDiscount={type:"percentage",value:100};
             if(!_.has( sortedItems[i],'discounts')){
              sortedItems[i].discounts=[];
              sortedItems[i].discounts.push(offerDiscount);
             }
          }
          //console.log(sortedItems);
          $scope.bill._items=[];
          _.forEach( sortedItems,function(item){
            $scope.bill.addItem(item);
          });
          console.log($scope.bill._items);
         /* $scope.selectedOffer.validated.applicable=true;
          growl.success('Your offer items completed',{ttl:2000});
          $scope.showBootBox('Your offer applied on the bill. ').then(function(as){})
          //console.log($scope.selectedOffer.offer.offer);
          $scope.offerAppliedNotification(angular.copy($scope.selectedOffer.offer.offer),true);
          $scope.selectedOffer=null;
          $scope.resetBillItems();*/
        }
        return false;
       }
        /*Least Offer Item Integration*/

        for (var i = 0; i < $scope.selectedOffer.offer.offer.applicable.items.length; i++) {
          if ($scope.selectedOffer.offer.offer.applicable.items[i].item == null || $scope.selectedOffer.offer.offer.applicable.items[i].item == "") {
            if ($scope.selectedOffer.offer.offer.applicable.items[i].category._id == item.category._id) {
              if (_.has($scope.selectedOffer.offer.offer.applicable.items[i], "billedQuantity")) {
                $scope.selectedOffer.offer.offer.applicable.items[i].billedQuantity += 1;
              } else {
                $scope.selectedOffer.offer.offer.applicable.items[i].billedQuantity = 1;
              }
            }
          } else {
            if ($scope.selectedOffer.offer.offer.applicable.items[i].item._id == item._id) {
              if (_.has($scope.selectedOffer.offer.offer.applicable.items[i], "billedQuantity")) {
                $scope.selectedOffer.offer.offer.applicable.items[i].billedQuantity += 1;
              } else {
                $scope.selectedOffer.offer.offer.applicable.items[i].billedQuantity = 1;
              }
            }
          }
        }
        $scope.selectedOffer.billedItems.applicable.push(item);
        $scope.checkOfferIsCompleted(true, item);
      }
      else {
        $scope.renderOfferItems(item);
      }
    }

    $scope.renderOfferItems=function(item){
      if (!$scope.selectedOffer.validated.offerItems) {
        for (var i = 0; i < $scope.selectedOffer.offer.offer.offerApplied.items.length; i++) {
          if ($scope.selectedOffer.offer.offer.offerApplied.items[i].item == null || $scope.selectedOffer.offer.offer.offerApplied.items[i].item == "") {
            if ($scope.selectedOffer.offer.offer.offerApplied.items[i].category._id == item.category._id) {
              if (_.has($scope.selectedOffer.offer.offer.offerApplied.items[i], "billedQuantity")) {

                $scope.selectedOffer.offer.offer.offerApplied.items[i].billedQuantity += 1;
              } else {
                $scope.selectedOffer.offer.offer.offerApplied.items[i].billedQuantity = 1;
              }
            }
          } else {
            if ($scope.selectedOffer.offer.offer.offerApplied.items[i].item._id == item._id) {
              if(_.has($scope.selectedOffer.offer.offer.offerApplied.items[i], "billedQuantity")) {
                $scope.selectedOffer.offer.offer.offerApplied.items[i].billedQuantity += 1;
              } else {
                $scope.selectedOffer.offer.offer.offerApplied.items[i].billedQuantity = 1;
              }
            }
          }
        }
        $scope.selectedOffer.billedItems.applied.push(item);
        $scope.checkOfferIsCompleted(false,item);
      }
    }

    $scope.checkOfferIsCompleted=function(isApplicable,item){
      if(isApplicable){
        var flag=true;
        var isLimitMin=false;
        var isLimitUpto=false;
        for(var i=0;i< $scope.selectedOffer.offer.offer.applicable.items.length;i++){
          if(_.has($scope.selectedOffer.offer.offer.applicable.items[i],'billedQuantity')) {
            if(parseInt( $scope.selectedOffer.offer.offer.applicable.items[i].quantity) != $scope.selectedOffer.offer.offer.applicable.items[i].billedQuantity) {
              flag=false;
            }else{
              /*Remove applicable items*/
              $scope.removeApplicableOfferItem(i,true);
            }
            if(_.has($scope.selectedOffer.offer.offer.applicable.items[i],'limit')) {
              isLimitMin = $scope.selectedOffer.offer.offer.applicable.items[i].limit == 'min';
            }
            if(_.has($scope.selectedOffer.offer.offer.applicable.items[i],'limit')) {
              isLimitUpto = $scope.selectedOffer.offer.offer.applicable.items[i].limit == 'upto';
            }
            /*Close Offer if min 1 item copleted*/
            if($scope.selectedOffer.offer.offer.applicable.on=='item' && ($scope.selectedOffer.offer.offer.type.name!='item')) {
              if(isLimitUpto) {
                $scope.IsOfferComplete = true;
              } else {
                $scope.IsOfferComplete = false;
              }
            }
            /*Close Offer if min 1 item copleted End Here*/
          }else{
            if($scope.selectedOffer.offer.offer.applicable.items[0].relation=='or'){
              /*Remove applicable items*/
              $scope.removeApplicableOfferItem(i,true);
            }else{
              flag=false;
            }
          }
        }

        if(flag){
          /* if($scope.selectedOffer.offer.offer.applicable.on=='item') {
           for (var i = 0; i < $scope.selectedOffer.offer.offer.applicable.items.length; i++) {
           delete $scope.selectedOffer.offer.offer.applicable.items[i].billedQuantity;
           }
           $scope.selectedOffer.offer.offer.offerApplied = angular.copy($scope.selectedOffer.offer.offer.applicable);
           }*/

          $scope.selectedOffer.validated.applicable=true;
          growl.success('Your offer selection items completed',{ttl:2000});
          if($scope.selectedOffer.offer.offer.applicable.on=='itemCriteria' && ($scope.selectedOffer.offer.offer.type.name!='item')){
            var billOfferApplied={offer:$scope.selectedOffer.offer.offer,user:$scope.getPassCodeUserPre(),time:new Date(),offerAmount:0,isBillWise:true,isApplicableItem:true};
            $scope.bill._offers=[];
            $scope.bill._offers.push(billOfferApplied);
            $scope.bill.processBill();
            $scope.selectedOffer=null;
            $scope.resetBillItems();
            return false;
          }
          if($scope.selectedOffer.offer.offer.applicable.on=='item' && ($scope.selectedOffer.offer.offer.type.name!='item')){
            if(isLimitMin){
              $scope.applyOfferApplicable(item,true);
              $scope.selectedOffer=null;
              $scope.resetBillItems();
              return false;
            }
            if(isLimitUpto){
              $scope.applyOfferApplicable(item,false);
              $scope.selectedOffer=null;
              $scope.resetBillItems();
              return false;
            }
          }

          /*Load offer items*/
          if($scope.selectedOffer.offer.offer.sameOrLess){
            $scope.sameOrLessFiltering();
            var isBilledItem=false;
            if($scope.selectedOffer.offerItems.appliedItems[0].Items.length==0){
              _.forEach($scope.selectedOffer.offer.offer.offerApplied.items,function(item){
                if(_.has(item,'billedQuantity')){
                  isBilledItem=true;
                }
              });
              if(!isBilledItem){
                growl.success('No offer items to select removing offer.',{ttl:2000});
                _.forEach($scope.bill._items,function(bItem){
                  if(_.has(bItem,'offer')){
                    if(bItem.offer.type='applicable'){
                      delete bItem.offer;
                    }
                  }
                })
              }
              growl.success('No offer items to select.',{ttl:2000});
              $scope.selectedOffer=null;
              $scope.resetBillItems();
              return false;
            }
          }
          var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
          if(_fTab.length>0){
            $scope.renderItemsTabWise(_fTab[0],$scope.selectedOffer.offerItems.appliedItems);
          }
        }else{
          if($scope.selectedOffer.offer.offer.applicable.on=='item' && ($scope.selectedOffer.offer.offer.type.name!='item')){
            if(isLimitUpto){
              $scope.applyOfferApplicable(item,false);
            }
          }
        }
      }else {
        /*For OfferApplied */
        var flag=true;
        /*  if($scope.selectedOffer.offer.offer.applicable.on=='item'){
         for (var i = 0; i < $scope.selectedOffer.offer.offer.applicable.items.length; i++) {
         if (_.has($scope.selectedOffer.offer.offer.applicable.items[i], 'billedQuantity')) {
         if (parseInt($scope.selectedOffer.offer.offer.applicable.items[i].quantity) != $scope.selectedOffer.offer.offer.applicable.items[i].billedQuantity) {
         flag = false;
         } else {
         *//*Remove applicable items*//*
         $scope.removeApplicableOfferItem(i, false);
         }
         } else {
         if ($scope.selectedOffer.offer.offer.applicable.items[0].relation == 'or') {
         *//*Remove applicable items*//*
         $scope.removeApplicableOfferItem(i, false);
         } else {
         flag = false;
         }
         }
         }
         }else{*/
        for (var i = 0; i < $scope.selectedOffer.offer.offer.offerApplied.items.length; i++) {
          if (_.has($scope.selectedOffer.offer.offer.offerApplied.items[i], 'billedQuantity')) {
            if (parseInt($scope.selectedOffer.offer.offer.offerApplied.items[i].quantity) != $scope.selectedOffer.offer.offer.offerApplied.items[i].billedQuantity) {
              flag = false;
            } else {
              /*Remove applicable items*/
              $scope.removeApplicableOfferItem(i, false);
            }
          } else {
            if ($scope.selectedOffer.offer.offer.offerApplied.items[0].relation == 'or') {
              /*Remove applicable items*/
              $scope.removeApplicableOfferItem(i, false);
            } else {
              flag = false;
            }
          }
        }
        /*}*/
        if(flag){
          $scope.selectedOffer.validated.applicable=true;
          /*Load offer items*/
          growl.success('Your offer items completed',{ttl:2000});
          $scope.showBootBox('Your offer applied on the bill. ').then(function(as){

            /*  $scope.selectedOffer=null;
             $scope.resetBillItems();*/

          })
          //console.log($scope.selectedOffer.offer.offer);
          $scope.offerAppliedNotification(angular.copy($scope.selectedOffer.offer.offer),true);
          $scope.selectedOffer=null;
          $scope.resetBillItems();
        }
      }
    }
    $scope.getUserNameForNotification=function(){
      //console.log($scope.selectedManagerDetail);
      var username='';
      if($scope.selectedWaiter!=null){
        username = $scope.selectedWaiter.firstname;
      }
      if($scope.selectedManager!=null){
        // console.log($scope.currentUser.username);
        var name=$scope.currentUser.username;
        if(_.has($scope.selectedManagerDetail,'firstname')){
          name=$scope.selectedManagerDetail.firstname;
        }
        username = "Manager("+name+")";
      }
      if(username == ''){
        username = $scope.currentUser.username;
      }
      if($scope.tempUserName!=null){
        username=$scope.tempUserName;
      }
      //console.log(username);
      return username;
    }
    $scope.removeOfferNotification=function(offer,isItemWise){
      //console.log(offer);
      var username=$scope.getUserNameForNotification();
      var str= "Removed "+(isItemWise?"Item Wise":"Bill Wise")+ " Offer "+offer.name +" by "+ username+ " to customer "+ ($scope.bill._customer.firstname==undefined?" none ":$scope.bill._customer.firstname);
      $scope.offerNotification('Removed Offer',str);
      console.log(str);
    }
    $scope.offerAppliedNotification=function(offer,isItemWise){
      var username=$scope.getUserNameForNotification();
      var str=(isItemWise?"Item Wise":"Bill Wise")+ " Offer "+offer.name +" applied by "+ username+ " to customer "+ ($scope.bill._customer.firstname==undefined?" none ":$scope.bill._customer.firstname);
      //console.log(str);
      $scope.offerNotification('Offer Applied',str);
    }
    $scope.sameOrLessFiltering=function(){
      var applicableAmount=0;
      var appliedAmount=0;
      /* _.forEach($scope.bill._items,function(item){
       console.log(item);
       if(_.has(item,'offer')) {
       if (item.offer._id == $scope.selectedOffer.offer.offer._id) {
       if (item.offer.type == 'applicable') {
       applicableAmount += item.rate * item.quantity;
       }
       if (item.offer.type == 'applied') {
       appliedAmount += item.rate * item.quantity;
       }
       }
       }
       });*/
      _.forEach($scope.selectedOffer.billedItems.applicable,function(appItem){
        console.log(appItem);
        applicableAmount += appItem.rate ;//* appItem.billedQuantity;
      });
      _.forEach($scope.selectedOffer.billedItems.applied,function(offItem){
        appliedAmount += offItem.rate ;//* offItem.billedQuantity;
      });
      var diffAmount=applicableAmount-appliedAmount;
      var filteredData={items:[],categories:[],superCategories:[]};
      _.forEach($scope.selectedOffer.offerItems.appliedItems[0].Items,function(offerItem){
        if(offerItem.rate<=diffAmount){
          filteredData.items.push(offerItem);
          filteredData.categories.push(offerItem.category);
          filteredData.superCategories.push(offerItem.category.superCategory);
        }
      });
      $scope.selectedOffer.offerItems.appliedItems[0].Items=filteredData.items;
      var uCat= _.uniq(filteredData.categories,'categoryName');
      var uSCat= _.uniq(filteredData.superCategories,'superCategoryName');
      $scope.selectedOffer.offerItems.appliedItems[0].categories=uCat;
      $scope.selectedOffer.offerItems.appliedItems[0].superCategories=uSCat;
    }

    $scope.applyOfferApplicable=function(item,isMin){
      var discountOb = {
        type: $scope.selectedOffer.offer.offer.type.name == 'percent' ? 'percentage' : 'fixed',
        value: $scope.selectedOffer.offer.offer.type.value,
        comment: null,
        offerName: $scope.selectedOffer.offer.offer.name,
        itemComment: null//Utils.roundNumber( ((item.discountType=='percent')?(tempItem.rate*item.value*.01):item.value))
      }
      var discounts=[];
      discounts.push(discountOb);
      _.forEach($scope.bill._items,function(billItem){
        if(_.has(billItem,'offer')) {
          if(!isMin) {
            if (billItem._id == item._id && billItem.offer._id == $scope.selectedOffer.offer.offer._id) {
              if (!_.has(billItem, 'discounts')) {
                billItem.discounts = [];
                billItem.discounts=angular.copy(discounts);
                billItem.isDiscounted=true;
              }
            }
          }else{
            if ( billItem.offer._id == $scope.selectedOffer.offer.offer._id) {
              if (!_.has(billItem, 'discounts')) {
                billItem.discounts = [];
                billItem.discounts=angular.copy(discounts);
                billItem.isDiscounted=true;
              }
            }
          }
        }
      });
      $scope.bill.processBill();
    }
    $scope.resetOffer=function(){
      growl.success('Your offer items completed',{ttl:2000});
      $scope.selectedOffer=null;
      $scope.resetBillItems();
    }
    $scope.cancelOffer=function(){
      $scope.discardOfferItem($scope.selectedOffer.offer.offer._id);
      if($scope.code!=null) {
        if ($scope.selectedOffer.offer.codeApplied == $scope.code.offerCode) {
          $scope.code = null;
        }
      }
      $scope.selectedOffer=null;
      $scope.resetBillItems();
      growl.success('Your selected offer cancelled successfully.',{ttl:2000});
    }
    $scope.resetBillItems=function(){
      var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
      if(_fTab.length>0){
        $scope.renderItemsTabWise(_fTab[0],$rootScope.rootMasterItems);
      }
    }
    $scope.removeApplicableOfferItem=function(index,isApplicable){
      var tabData={tabId:$scope.selectedTab.tabId, Items:[],categories:[],superCategories:[]};
      /*For Applicable Items*/
      if(isApplicable) {
        if ($scope.selectedOffer.offer.offer.applicable.items[index].item == null || $scope.selectedOffer.offer.offer.applicable.items[index].item == "") {
          _.forEach($scope.selectedOffer.offerItems.applicable[0].Items, function (item, i) {
            // console.log(item);
            if (item.category._id != $scope.selectedOffer.offer.offer.applicable.items[index].category._id) {
              //$scope.selectedOffer.offerItems.applicable[0].Items.splice(i,1);
              tabData.Items.push(item);
              tabData.categories.push(item.category);
              tabData.superCategories.push(item.category.superCategory);
            }
          })
        } else {
          _.forEach($scope.selectedOffer.offerItems.applicable[0].Items, function (item, i) {
            if (item._id != $scope.selectedOffer.offer.offer.applicable.items[index].item._id) {
              //$scope.selectedOffer.offerItems.applicable[0].Items.splice(i,1);
              tabData.Items.push(item);
              tabData.categories.push(item.category);
              tabData.superCategories.push(item.category.superCategory);
            }
          })
        }
      }else {
        /*For Offer Items*/
        /* if ($scope.selectedOffer.offer.offer.applicable.on == 'item') {
         if ($scope.selectedOffer.offer.offer.applicable.items[index].item == null || $scope.selectedOffer.offer.offer.applicable.items[index].item == "") {
         _.forEach($scope.selectedOffer.offerItems.appliedItems[0].Items, function (item, i) {
         // console.log(item);
         if (item.category._id != $scope.selectedOffer.offer.offer.applicable.items[index].category._id) {
         //$scope.selectedOffer.offerItems.applicable[0].Items.splice(i,1);
         tabData.Items.push(item);
         tabData.categories.push(item.category);
         tabData.superCategories.push(item.category.superCategory);
         }
         })
         } else {
         _.forEach($scope.selectedOffer.offerItems.appliedItems[0].Items, function (item, i) {
         if (item._id != $scope.selectedOffer.offer.offer.applicable.items[index].item._id) {
         //$scope.selectedOffer.offerItems.applicable[0].Items.splice(i,1);
         tabData.Items.push(item);
         tabData.categories.push(item.category);
         tabData.superCategories.push(item.category.superCategory);
         }
         })
         }
         }
         else {*/
        if ($scope.selectedOffer.offer.offer.offerApplied.items[index].item == null || $scope.selectedOffer.offer.offer.offerApplied.items[index].item == "") {
          _.forEach($scope.selectedOffer.offerItems.appliedItems[0].Items, function (item, i) {
            // console.log(item);
            if (item.category._id != $scope.selectedOffer.offer.offer.offerApplied.items[index].category._id) {
              //$scope.selectedOffer.offerItems.applicable[0].Items.splice(i,1);
              tabData.Items.push(item);
              tabData.categories.push(item.category);
              tabData.superCategories.push(item.category.superCategory);
            }
          })
        } else {
          _.forEach($scope.selectedOffer.offerItems.appliedItems[0].Items, function (item, i) {
            if (item._id != $scope.selectedOffer.offer.offer.offerApplied.items[index].item._id) {
              //$scope.selectedOffer.offerItems.applicable[0].Items.splice(i,1);
              tabData.Items.push(item);
              tabData.categories.push(item.category);
              tabData.superCategories.push(item.category.superCategory);
            }
          })
        }
      }
      /*}*/
      /*Done Offer Items*/

      var _uCats = _.uniq(tabData.categories, 'categoryName');
      tabData.categories = _uCats;
      tabData.categories = _.sortBy(tabData.categories, ['categoryName']);
      var _uSCats = _.uniq(tabData.superCategories, 'superCategoryName');
      tabData.superCategories = _uSCats;
      tabData.superCategories = _.sortBy(tabData.superCategories, ['superCategoryName']);
      if(isApplicable) {
        $scope.selectedOffer.offerItems.applicable[0]=tabData;
      }else{
        /* if ($scope.selectedOffer.offer.offer.applicable.on == 'item') {
         $scope.selectedOffer.offerItems.applicable[0]=tabData;
         }else {*/
        $scope.selectedOffer.offerItems.appliedItems[0] = tabData;
        /*}*/
      }
      var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
      if(_fTab.length>0){
        if(isApplicable) {
          $scope.renderItemsTabWise(_fTab[0], $scope.selectedOffer.offerItems.applicable);
        }else{
          $scope.renderItemsTabWise(_fTab[0], $scope.selectedOffer.offerItems.appliedItems);
        }
      }
      // $scope.renderItemsTabWise(_fTab[0],$scope.selectedOffer.offerItems.applicable);
    }
    $scope.clearCategoryFilter = function() {
      $scope.categoryNameToFilter = '';
    };

    // $scope.categoryNameToFilter = '';
    $scope.lastFilteredSCategory = '';
    $scope.lastFilteredCategory = '';
    $scope.filterItemsFromCategory = function(cat) {
      // $scope.categoryNameToFilter = cat.categoryName;
      $scope.categoryNameToFilter(cat.categoryName);
    };


    // $scope.$watch('categoryNameToFilter', function(catName) {
    $scope.categoryNameToFilter= function(catName) {

      console.log('Cat Filter called', catName);
      if (catName === '') {
        $scope.filteredTabItems = $scope.tabItems;
      } else {
        $scope.lastFilteredCategory=catName;
        $scope.filteredTabItems = _.filter($scope.tabItems, {category: {categoryName: catName}});

        // Sort According to Setting
        if (Utils.hasSetting('orderby_itemnumber', $scope.settings)) {
          $scope.filteredTabItems = _.sortBy(angular.copy($scope.filteredTabItems), ['number']);
        } else {
          //$scope.filteredTabItems = _.sortBy(angular.copy($scope.filteredTabItems), ['name']);
          $scope.filteredTabItems = angular.copy($scope.filteredTabItems);
        };

        $scope.itemPagination.numPages = Math.ceil($scope.filteredTabItems.length / $scope.itemPagination.perPage);
        $scope.itemPagination.totalItems = $scope.filteredTabItems.length;
        $scope.itemPagination.page = 0;

      }
    };
    //, true);

    //$scope.superCategoryNameToFilter = '';
    $scope.filterCatFromSCategory = function(scat) {
      //  $scope.superCategoryNameToFilter = scat.superCategoryName;
      $scope.superCategoryNameToFilter(scat.superCategoryName);
    };
    //  $scope.$watch('superCategoryNameToFilter', function(scatName) {
    $scope.superCategoryNameToFilter= function(scatName) {
      if(scatName===$scope.lastFilteredSCategory){scatName='';}
      if (scatName === '') {
        $scope.filteredCategoryItems = $scope.categoryItems;
        $scope.filteredTabItems = $scope.tabItems;
        $scope.itemPagination.numPages = Math.ceil($scope.filteredTabItems.length / $scope.itemPagination.perPage);
        $scope.itemPagination.totalItems = $scope.filteredTabItems.length;

        $scope.itemPagination.page = 0;
      } else {
        //  $scope.filteredCategoryItems = _.filter($scope.categoryItems, {superCategory: {superCategoryName: $scope.superCategoryNameToFilter}});
        $scope.filteredCategoryItems = _.filter($scope.categoryItems, {superCategory: {superCategoryName: scatName}});

        /* Re-filter all items according to available categories */
        var _fTabItems = [];
        angular.forEach($scope.filteredCategoryItems, function (cat) {
          var _f = _.filter($scope.tabItems, {category: {categoryName: cat.categoryName}});
          angular.forEach(_f, function (item) {
            _fTabItems.push(item);
          });
        });
        $scope.filteredTabItems = _fTabItems;
        // Sort According to Setting
        if (Utils.hasSetting('orderby_itemnumber', $scope.settings)) {
          $scope.filteredTabItems = _.sortBy(angular.copy(_fTabItems), ['number']);
        } else {
          // $scope.filteredTabItems = _.sortBy(angular.copy(_fTabItems), ['name']);
          $scope.filteredTabItems = (angular.copy(_fTabItems));
        };

        $scope.itemPagination.numPages = Math.ceil($scope.filteredTabItems.length / $scope.itemPagination.perPage);
        $scope.itemPagination.totalItems = $scope.filteredTabItems.length;
        $scope.itemPagination.toPageId(0);
        //console.log("SuperCategory:", scatName, 'Categories:', $scope.filteredCategoryItems, $scope.filteredTabItems);

        $scope.categoryPagination.numPages = Math.ceil($scope.filteredCategoryItems.length / $scope.categoryPagination.perPage);
        $scope.categoryPagination.totalItems = $scope.filteredCategoryItems.length;
        $scope.categoryPagination.toPageId(0);
      }
      $scope.lastFilteredSCategory=scatName;
      $scope.lastFilteredCategory='';
    };

    /* Copy Table Bill */
    $scope.copiedTableBill = {};
    $scope.copyTableBill = function(index,bill) {
      console.log(bill);
      $scope.copiedTableBill.tableIndex = index;
      $scope.copiedTableBill.bill = bill;
      growl.success("Copied Bill in Table#" + (index), {
        ttl: 3000
      });
      /* nSocket.emit('table_event', {event:'add', type:'table',number:index,deployment_id:localStorageService.get('deployment_id')}, function (data) {
       //console.log(data);
       })
       $scope.tableStarted ={isTable:true,index: index};*/
      $scope.unlockTable();
      $scope.lockTable(index,index);
    };

    /* Enable Comment on KOT */
    $scope.enableCommentOnKot = function() {
      var showKotCommentButton = Utils.hasSetting('enable_comment_kot', $scope.settings);
      //console.log('KOT Comment Setting', showKotCommentButton);
      return showKotCommentButton;
    };


    /* Paste Table Bill */
    $scope.pasteTableBill = function(index) {
      console.log($scope.copiedTableBill.bill);
      if(!_.has($scope.copiedTableBill,'bill')){
        growl.error('No bill to paste',{ttl:2000});
        return false;
      }
      if(_.has($scope.copiedTableBill.bill,'cardTransaction'))
          $scope.copiedTableBill.bill.cardTransaction={};
     // $state.current.data.tables[index].bill.cardTransaction={};
      /*if($scope.copiedTableBill.bill._offers.length>0){
        growl.error('Offer card bill can not shift.',{ttl:2000});
        return false;
      }*/

      if($state.current.data.tables[index].bill!=undefined){
        //   growl.error('Ohh.. Table is occupied !!',{ttl:2000});
      }
      //$state.current.data.tables[index].covers = parseInt($scope.copiedTableBill.covers);
      /* var filterTb = _.filter($state.current.data.tables, {
       'id': $scope.copiedTableBill.tableIndex
       });
       //console.log(filterTb);
       $state.current.data.tables[index].bill = angular.copy(filterTb[0].bill);*/
      var tempBill= angular.copy($scope.copiedTableBill.bill);
      //console.log(tempBill);
      tempBill.tab=$scope.selectedTab.tabName;
      tempBill.tabId=$scope.selectedTab.tabId;
      tempBill.tabType=$scope.selectedTab.tabType;
      $state.current.data.tables[index].bill = angular.copy(tempBill);
      $state.current.data.tables[index].bill._tableId = index + 1;
      //$state.current.data.tables[index].bill.splitNumber = null;
      $state.current.data.tables[index].bill.checkBillExists($state.current.data.tables[index].bill._id).then(function(len) {
        $state.current.data.tables[index].bill.updateBill({}, 'update').then(function() {

          growl.success("Pasted Bill in Table#" + (index + 1), {
            ttl: 3000
          });
          $scope.initializeTab().then(function(res){});
          if($rootScope.isInstance) {
            nSocket.emit('table_split_event', {
              event: 'add',
              type: 'table',
              number: 0,
              deployment_id: localStorageService.get('deployment_id')
            }, function (data) {
              //console.log(data);
            })
            nSocket.emit('table_event', {
              event: 'remove',
              type: 'table',
              number: $scope.copiedTableBill.bill.tabId + $scope.copiedTableBill.tableIndex,
              deployment_id: localStorageService.get('deployment_id')
            }, function (data) {
            });
          }
          printer.print('_template_Bill.html', $scope.copiedTableBill.bill,{'from':$scope.copiedTableBill.bill._tableId,to:index+1,fromTab:$scope.copiedTableBill.bill.tab,toTab:tempBill.tab,type:'table'});
          $scope.copiedTableBill = {};
        });
      });
    };

    $scope.splitBill = function(billFromTable, index) {
      // $scope.lockTable(billFromTable._tableId);
      if(billFromTable.tabType=='table')
        $scope.lockTable(billFromTable._tableId);
      else
        $scope.lockNonTable(billFromTable.billNumber.toString());

      // console.log(billFromTable);
      $scope.splitBillModal(billFromTable).then(function(splitedBill) {
        if(splitedBill=='done'){
          $scope.unlockTable();
          return false;
        }
        //  var bill=$state.current.data.tables[index].bill;
        // console.log(billFromTable);
        var mainBill = angular.copy(billFromTable);
        // console.log(mainBill);
        var tempBill = [];
        for (var i = 0; i < splitedBill.length; i++) {
          tempBill.push(angular.copy(billFromTable));
          tempBill[i]._kots = [];
          for (var j = 0; j < splitedBill[i].length; j++) {
            // console.log(mainBill);
            var tkotob = getItemWiseKot(mainBill, splitedBill[i][j].name);
            mainBill = angular.copy(tkotob.bill);
            var kot = tkotob.kotOb;
            var flagIsKot = false;
            for (var k = 0; k < tempBill[i]._kots.length; k++) {
              if (tempBill[i]._kots[k].kotNumber === kot.kotNumber) {
                flagIsKot = true;
                var flagIsItem = false;
                if (_.has(tempBill[i]._kots[k], 'items')) {
                  for (var l = 0; l < tempBill[i]._kots[k].items.length; l++) {
                    if (tempBill[i]._kots[k].items[l].name === kot.items[0].name) {
                      tempBill[i]._kots[k].items[l].quantity += 1;
                      flagIsItem = true;
                      break;
                    }
                  }
                }
                if (!flagIsItem) {
                  tempBill[i]._kots[k].items.push(kot.items[0]);
                }
              }
            }
            if (!flagIsKot)
              tempBill[i]._kots.push(kot);
          }

          // tempBill.processBill({});
        }
        console.log(mainBill);

        _.forEach(mainBill._kots,function(kot){
          if(kot.isVoid==true){
            tempBill[0]._kots.push(kot);
          }
        })

        var promiseInsert = [];
        _.forEach(tempBill, function(bill, i) {
          bill.processBill({});
          bill._id = Utils.guid();
          bill.splitNumber = i + 1;
          bill.isSplit=true;
          promiseInsert.push(bill.updateBill({}, 'insert'));
          //console.log(bill);
        });
        $q.all(promiseInsert).then(function() {
          if(!$rootScope.isInstance) {
            $rootScope.db.selectDynamic('delete from bills where billId="' + mainBill._id + '"', []).then(function () {
              growl.success('Split bill completed.', {
                ttl: 2000
              });
              refreshState();
            })
          }else{
            /*fine dine*/
            $http.delete($rootScope.url + '/api/synckots/destroyBill?billId='+mainBill._id).then(function(result){
              growl.success('Split bill completed.', {
                ttl: 2000
              });
              refreshState();
              nSocket.emit('table_split_event', {event:'add', type:'table',number:0,deployment_id:localStorageService.get('deployment_id')}, function (data) {
                //console.log(data);
              })
              nSocket.emit('table_split_remove_event',{deployment_id:localStorageService.get('deployment_id'),billId:mainBill._id});
            });
            /*fine dine ends*/
          }
        });
        //console.log(tempBill);
      });
    };


    function getItemWiseKot(mBill, itemName) {
      //console.log(mBill);
      var kot;
      var kotWithBill = {};
      for (var i = 0; i < mBill._kots.length; i++) {
        if(mBill._kots[i].isVoid==false){
          for (var j = 0; j < mBill._kots[i].items.length; j++) {
            if (mBill._kots[i].items[j].name === itemName) {
              kot = angular.copy(mBill._kots[i]);
              var item = [];
              item.push(angular.copy(mBill._kots[i].items[j]));
              item[0].quantity = 1;
              kot.items = item;
              if (mBill._kots[i].items[j].quantity === 1) {
                mBill._kots[i].items.splice(j, 1);
                if (mBill._kots[i].items.length === 0) {
                  mBill._kots.splice(i, 1);
                }
                kotWithBill = {
                  kotOb: kot,
                  bill: angular.copy(mBill)
                };
                return kotWithBill;
              } else {
                mBill._kots[i].items[j].quantity -= 1
                kotWithBill = {
                  kotOb: kot,
                  bill: angular.copy(mBill)
                };
                return kotWithBill;
              }
              /*To look into it*/
              /*   kotWithBill = {
               kotOb: kot,
               bill: mBill
               };
               return kotWithBill;
               break;*/
            }
          }
        }
      }
      return kotWithBill;
    };

    $scope.subcategories = [];
    $scope.superCategories = [];
    $scope.selectedSuperCategory = {};
    $scope.clickCategory = function(sc) {
      $scope.selectedSuperCategory = sc;
      $scope.categories = sc.child;
    };

    $scope.splitBillModal = function(billFromTable) {
      var defer = $q.defer();

      var _allItems = [];
      var Items = [];
      angular.forEach(billFromTable._kots, function(kot) {
        if (!kot.isVoid) {
          angular.forEach(kot.items, function(item) {
            _allItems.push(item);
          });
        }
      });
      // Aggregate Items
      _.chain(_allItems).groupBy('_id').map(function(i) {
        var count = 0;
        console.log(i)
        for (var j = 0; j < i.length; j++) {
          for (var k = 0; k < i[j].quantity; k++) {
            count++;
            Items.push({
              _id: i[0]._id + count,
              name: i[0].name,
              rate:parseFloat(i[0].rate),
              category:{
                _id : i[0].category._id,
                isliquor : i[0].category.isliquor
              }
            });
          }
        }
      });

      $modal.open({
        backdrop : $scope.windowWidth<1030?false:true,
        backdropClass : 'modal-backdrop',
        templateUrl: 'app/billing/item/_split.html',
        controller: ['$scope', '$rootScope', 'growl', 'bill', '$modalInstance', function($scope, $rootScope, growl, bill, $modalInstance) {
          $scope.rateCalculate=function(){
          _.forEach($scope.sItems, function(sItems, index) {
              var i = index;
               $scope.rate[i] = 0;
               _.forEach(sItems, function(item) {
               $scope.rate[i] = $scope.rate[i] + item.rate;
              })
           })

          }
          $scope.sItems = [];
          $scope.rate=[];
          $scope.rate[0]=0;
          var splitType = 0;
          $scope.sItems.push(angular.copy(Items));
          _.forEach($scope.sItems, function(sItems) {
             _.forEach(sItems, function(item) {
                $scope.rate[0] = $scope.rate[0] + item.rate;
               })
           })
          $scope.splitByCategory = function(data){
            $scope.sItems = [];
            if(data==0){
              $scope.sItems.push(angular.copy(Items));
              $scope.rateCalculate();
            }
            else if(data==1){
              var temp = _.groupBy(Items,function(item){
                return item.category._id;
              });
              console.log(temp);
              _.forEach(temp, function(value){
                  $scope.sItems.push(value);
              })
              $scope.rateCalculate();
            }
            else if(data==2){
              $scope.sItems = [[],[]];
              _.forEach(Items,function(item){
                console.log(item.category.isliquor)
                if(item.category.isliquor){
                  $scope.sItems[0].push(item)
                }
                else{
                  $scope.sItems[1].push(item)
                }
                $scope.rateCalculate();
              })
            }
          }
          $scope.windowWidth = $window.innerWidth;
          // var temp = _.groupBy(Items,'category');
          // _.forEach(temp, function(value){
          //     $scope.cItems.push(value);
          // })
          // _.forEach(Items,function(item){
          //   console.log(item.category.isliquor)
          //   if(item.category.isliquor){
          //     $scope.fLItems[0].push(item)
          //   }
          //   else{
          //     $scope.fLItems[1].push(item)
          //   }
          // })
          //console.log($scope.fLItems)
          $scope.onDragComplete=function(data,evt){
            console.log("drag success, data:", data);
          }
          $scope.onDropComplete=function(data,evt){
            console.log("drop success, data:", data);
          }
          $scope.$watch(function(){
            return $window.innerWidth;
          }, function(){
            $scope.windowWidth = $window.innerWidth;
          }, true)
          $scope.split = function() {
            console.log($scope.sItems);
            $modalInstance.close($scope.sItems);
          };
          $scope.addSelected = function(index, selectedItems) {
            $scope.sItems[index].Selected = selectedItems;
            for (var i = 0; i < $scope.sItems.length; i++) {
              if (i != index) {
                $scope.sItems[i].Selected = [];
              }
            }
          };
          $scope.shiftLeft = function(index) {
            if (!_.has($scope.sItems[index + 1], 'Selected')) {
              return false;
            }
            _.forEach($scope.sItems[index + 1].Selected, function(id) {
              for (var i = 0; i < $scope.sItems[index + 1].length; i++) {
                if ($scope.sItems[index + 1][i]._id === id) {
                  $scope.sItems[index].push($scope.sItems[index + 1][i]);
                  $scope.sItems[index + 1].splice(i, 1);
                  if ($scope.sItems[index + 1].length == 0) {
                    $scope.sItems.splice(index + 1, 1);
                  }
                  break;
                }
              }
            });
            $scope.rateCalculate();
          };
          $scope.shiftLeftTab = function(index) {
            // if (!_.has($scope.sItems[index + 1], 'Selected')) {
            //     return false;
            // }
            var items = angular.copy($scope.sItems[index])
            _.forEach(items, function(item) {

              var id = item._id
              if(item.ticked)
                for (var i = 0; i < $scope.sItems[index].length; i++) {
                  $scope.sItems[index][i].ticked = false
                  if ($scope.sItems[index][i]._id === id) {
                    $scope.sItems[index-1].push($scope.sItems[index][i]);
                    $scope.sItems[index].splice(i, 1);
                    if ($scope.sItems[index].length == 0) {
                      $scope.sItems.splice(index, 1);
                    }
                    break;
                  }
                }
            });
            $scope.rateCalculate();
          };

          $scope.shiftRightAll = function(index, selected) {
            _.forEach($scope.sItems[index].Selected, function(id) {
              for (var i = 0; i < $scope.sItems[index].length; i++) {
                if ($scope.sItems[index][i]._id === id) {
                  if ($scope.sItems.length === index + 1) {
                    var titem = [];
                    titem.push($scope.sItems[index][i]);
                    $scope.sItems[index + 1] = titem;

                  } else {
                    $scope.sItems[index + 1].push($scope.sItems[index][i]);
                  }
                  $scope.sItems[index].splice(i, 1);
                  if ($scope.sItems[index].length == 0) {
                    $scope.sItems.splice(index, 1);
                  }
                  break;
                }
              }
            });
            $scope.rateCalculate();
          };
          $scope.shiftRightAllTab = function(index,selectedItems) {
            console.log(selectedItems, index)
            var items = angular.copy(selectedItems)
            _.forEach(items, function(item) {
              console.log(item)
              var id = item._id
              if(item.ticked)
                for (var i = 0; i < $scope.sItems[index].length; i++) {
                  $scope.sItems[index][i].ticked = false
                  if ($scope.sItems[index][i]._id === id) {
                    console.log(id)
                    if ($scope.sItems.length === index + 1) {
                      var titem = [];
                      titem.push($scope.sItems[index][i]);
                      $scope.sItems[index + 1] = titem;
                      //console.log(titem)

                    } else {
                      $scope.sItems[index + 1].push($scope.sItems[index][i]);
                    }
                    $scope.sItems[index].splice(i, 1);
                    if ($scope.sItems[index].length == 0) {
                      $scope.sItems.splice(index, 1);
                    }
                    break;
                  }
                }
              //console.log($scope.sItems)
            });
            $scope.rateCalculate();
          };

          $scope.shiftRight = function(index, selected) {
            if ($scope.sItems[index].length === 1) {
              growl.error('Cant shift whole the item', {
                ttl: 2000
              });
              return false;
            }
            //   _.forEach(selected,function(id){
            var id = selected[0];
            for (var i = 0; i < $scope.sItems[index].length; i++) {
              if ($scope.sItems[index][i]._id === id) {
                if ($scope.sItems.length === index + 1) {
                  var titem = [];
                  titem.push($scope.sItems[index][i]);
                  $scope.sItems.push(titem);
                  //  console.log($scope.sItems);
                } else {
                  var titem = $scope.sItems[index + 1];
                  titem.push($scope.sItems[index][i]);
                  $scope.sItems[index + 1] = titem;
                }
                $scope.sItems[index].splice(i, 1);
                break;
              }
            }
            $scope.rateCalculate();
          };
          $scope.split = function() {
            if($scope.sItems.length<2){
              growl.error('Min. Two set of items required! ',{ttl:2000});
              return false;
            }
            var flag=false;
            _.forEach($scope.sItems,function(sItem){
                if(sItem.length<1){
                  flag=true;
                }
            })
            if(flag){
              growl.error('Minimum 1 item in each split is required!',{ttl:2000});
              return false;
            }
            //console.log($scope.sItems);
            $modalInstance.close($scope.sItems);
              // if($splitType=='0')
              //     $modalInstance.close($scope.sItems);
              // else if($scope.splitType=='1')
              //     $modalInstance.close($scope.cItems);
              // else if($scope.splitType=='2')
              //     $modalInstance.close($scope.fLItems);
          };
          $scope.cancel = function() {
            $modalInstance.close('done');
          }
        }],
        size: $scope.windowWidth<1030?'md':'lg',
        resolve: {
          itemIndex: function() {
            return undefined;
          },
          taxes: function() {
            return $scope.taxes;
          },
          bill: function() {
            return (billFromTable) ? billFromTable : $scope.bill;
          }
        }

      }).result.then(function(splitedBill) {
        defer.resolve(splitedBill);
      });
      return defer.promise;
    };

    /* Open Customer Modal */
    $scope.openCustomerModal = function(size, value) {
      var modalInstance = $modal.open({
        templateUrl: 'app/billing/customer/_new.html',
        controller: 'CustomerCtrl',
        size: size,
        resolve: {
          number: function() {
            return value;
          }
        }
      });

      modalInstance.result.then(function(newCustomer) {
        console.log(newCustomer);
        //newCustomer.addType='Residence';
        //newCustomer.customerId='9028c6ec-fb9d-18f3-1f3b-3950f00564df1';
        isCustomerFound(newCustomer).then(function(flag) {
          console.log(flag);
          $scope.newCustomer = newCustomer;
          if (!flag) {
            $scope.newCustomer.customerId = Utils.guid();
            $scope.newCustomer.tenant_id = localStorageService.get('tenant_id'); //currentUser.tenant_id;
            $scope.newCustomer.deployment_id = localStorageService.get('deployment_id'); //currentUser.deployment_id;
          }
          $rootScope.db.insert('customers', {
            'customerId': $scope.newCustomer.customerId,
            'customerData': JSON.stringify($scope.newCustomer)
          }).then(function(result) {
            console.log("Inserted Customer to WebSQL");
            var tempcust = angular.copy($scope.newCustomer);
            $scope.billCustomer = angular.copy(tempcust);
            delete tempcust.addresses;
            //$scope.bill._customer = angular.copy($scope.newCustomer);
            $scope.bill._customer = tempcust;
            $scope.selectedCustomer = '';
          });
        });
        /*Customer.save({tenant_id:currentUser.tenant_id}, $scope.newCustomer, function (customer){
         Sync.syncCustomers($rootScope.currentUser).then(function (customers) {
         $rootScope.customers = customers;
         });
         });*/
      }, function() {
        console.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.selectAddresses = function(value) {
      var addresses = angular.copy(value);
      var tempCustomer = angular.copy($scope.billCustomer);
      var defaultAddress = {
        type: tempCustomer.addType,
        line1: tempCustomer.address1,
        line2: tempCustomer.address2,
        city: tempCustomer.city,
        state: tempCustomer.state,
        postcode: tempCustomer.postCode
      };
      addresses.push(defaultAddress);
      $scope.openModalAddresses('sm', addresses);
    };

    $scope.openModalAddresses = function(size, value) {
      console.log(value);
      var modalinstance = $modal.open({
        templateUrl: 'app/billing/customer/_addresses.html',
        controller: 'customerAddressCtrl',
        size: 'md',
        resolve: {
          customerAddresses: function() {
            return value;
          }
        }
      });
      modalinstance.result.then(function(selectedAddress) {
        $scope.bill._customer.addType = selectedAddress.type;
        $scope.bill._customer.address1 = selectedAddress.line1;
        $scope.bill._customer.address2 = selectedAddress.line2;
        $scope.bill._customer.state = selectedAddress.state;
        $scope.bill._customer.city = selectedAddress.city;
        $scope.bill._customer.postCode = selectedAddress.postcode;
      })
    };

    $scope.showCustomerMore = function(value) {
      var modalinstance = $modal.open({
        templateUrl: 'app/billing/customer/_more.html',
        controller: 'moreCtrl',
        size: 'md',
        resolve: {
          customer: function() {
            return value;
          }
        }
      });
    };

    function isCustomerFound(newCustomer) {
      var d = $q.defer();
      if (!_.has(newCustomer, 'customerId')) {
        d.resolve(false);
      } else {
        $rootScope.db.selectDynamic("select count(*) as cnt from customers where customerId='" + newCustomer.customerId + "'")
          .then(function(custcount) {
            if (custcount.rows.item(0).cnt == 0) {
              $rootScope.db.selectDynamic("delete from customers where customerId='" + newCustomer.customerId + "'")
                .then(function(res) {
                  d.resolve(true);
                });
            } else {
              d.resolve(false);
            }
          });
      }
      return d.promise;
    };

    /* Open Item Quantity Modal */
    $scope.openItemQuantityModal = function(size, item, index) {
      //weightT=0.0;
      if (!Utils.hasSetting('enable_sweet_shop', $scope.settings)) {
        return false;
      }
      var modalInstance = $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/item/_weight.html',
        controller: ['$scope', '$modalInstance', 'itemIndex', 'taxes', 'bill', 'Utils', 'growl', function($scope, $modalInstance, itemIndex, taxes, bill, Utils, growl) {
          $scope.weightT = 0;
          $scope.setWeight = function(weight, index) {
            //console.log($scope.weightT);
            $scope.weightT = ($scope.weightT == 'undefined' || $scope.weightT == NaN) ? 0 : $scope.weightT;
            $scope.weightT += parseFloat(weight);
            $scope.weightT = Utils.roundNumber($scope.weightT, 3);
            //bill.getItemSubtotal();
          }
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
          $scope.applyOnBill = function() {
            item.quantity = $scope.weightT;
            //bill.getItemSubtotal();
            bill.processBill();
            $modalInstance.dismiss('cancel');
          };
          $scope.reset = function() {
            $scope.weightT = 0;
          }
        }],
        size: size,
        resolve: {
          itemIndex: function() {
            return index;
          },
          taxes: function() {
            return $scope.taxes;
          },
          bill: function() {
            return $scope.bill;
          }
        }
      });

      modalInstance.result.then(function(bill) {
        $scope.bill = bill;

      }, function() {
        console.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.openItemAmountModal = function(size, item, index) {

      //weightT=0.0;
      if (!Utils.hasSetting('enable_sweet_shop', $scope.settings)) {
        return false;
      }
      var modalInstance = $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/item/_amount.html',
        controller: ['$scope', '$modalInstance', 'itemIndex', 'taxes', 'bill', 'Utils', 'growl', function($scope, $modalInstance, itemIndex, taxes, bill, Utils, growl) {
          $scope.amountT = "";
          $scope.setAmount = function(num, index) {
            $scope.amountT += num;
            $scope.amountT = Utils.roundNumber($scope.amountT, 2);
          }
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
          $scope.applyOnBill = function() {
            item.quantity = Utils.roundNumber(($scope.amountT / item.rate), 3);
            // bill.getItemSubtotal();
            bill.processBill();
            $modalInstance.dismiss('cancel');
          };

          $scope.reset = function() {
            $scope.amountT = "";
          }

          $scope.delete = function() {
            $scope.amountT = $scope.amountT.substring(0, $scope.amountT.length - 1);
          }
        }],
        size: size,
        resolve: {
          itemIndex: function() {
            return index;
          },
          taxes: function() {
            return $scope.taxes;
          },
          bill: function() {
            return $scope.bill;
          }
        }
      });

      modalInstance.result.then(function(bill) {
        $scope.bill = bill;

      }, function() {
        console.info('Modal dismissed at: ' + new Date());
      });

    };


    /* Open Item Quantity Modal */
    $scope.openItemRateTaxDiscModal = function (size, item, index, fromKot, kIndex) {
      /* Authentication Required */
      if(Utils.hasSetting('addon_compulsory', $scope.settings))
      {
      if (Utils.hasSetting('enable_passcode_apply_discount', $scope.settings)) {
        if (!$scope.selectedManager) {
          $scope.passCodeModal().then(function (passCode) {
            if (passCode === Utils.getPassCode(passCode, $rootScope.users, 'Manager,Admin')) {
              $scope.expressDiscountApply(size, item, index, fromKot, kIndex, true, true);
              $scope.setTempManagerName(passCode);
            } else {
              growl.error("Wrong Passcode!", {
                ttl: 2000
              });
              $scope.expressDiscountApply(size, item, index, fromKot, kIndex, false, true);
              //return;
            }

          });
        } else {
          $scope.expressDiscountApply(size, item, index, fromKot, kIndex, true, true);
        }

      } else {
        /* No Authentication Required */
        $scope.expressDiscountApply(size, item, index, fromKot, kIndex, true, true);
      }
    }
      else
      {
        if (Utils.hasSetting('enable_passcode_apply_discount', $scope.settings)) {
          if (!$scope.selectedManager) {
            $scope.passCodeModal().then(function (passCode) {
              if (passCode === Utils.getPassCode(passCode, $rootScope.users, 'Manager,Admin')) {
                $scope.expressDiscountApply(size, item, index, fromKot, kIndex, true, false);
                $scope.setTempManagerName(passCode);
              } else {
                growl.error("Wrong Passcode!", {
                  ttl: 2000
                });
                $scope.expressDiscountApply(size, item, index, fromKot, kIndex, false, false);
                //return;
              }

            });
          } else {
            $scope.expressDiscountApply(size, item, index, fromKot, kIndex, true, false);
          }

        } else {
          /* No Authentication Required */
          $scope.expressDiscountApply(size, item, index, fromKot, kIndex, true, false);
        }
      }

    };




    $scope.expressDiscountApply = function(size, item, index, fromKot, kIndex,isDiscount,isAddonCompulsory) {
      $scope.isBillingOn=false;
      $modal.open({
        backdrop : 'static',
        keyboard :false,
        /* templateUrl: 'app/billing/item/_rateTaxDisc.html',*/
        templateUrl: 'app/billing/item/_comment_discount.html',
        controller: 'ItemquantityCtrl',
        size: 'lg',
        resolve: {
          itemIndex: function() {
            return index;
          },
          taxes: function() {
            return $scope.taxes;
          },
          bill: function() {
            return $scope.bill;
          },
          isKot: function() {
            return fromKot;
          },
          kotIndex: function() {
            return kIndex;
          },
          addons:function(){
            if(fromKot)
              return $scope.filterAddons($scope.bill._kots[kIndex].items[index]._id);
            else
              return $scope.filterAddons($scope.bill._items[index]._id);
          },
          discountStatus:function(){
            return isDiscount;
          },
           isAddonCompulsory: function () {
            return isAddonCompulsory;
          },
          settings:function(){
            return $scope.settings;
          },
          user:function(){
            return $scope.getPassCodeUserPre();
          }
        }
      }).result.then(function (ob) {

        $scope.bill = ob.bill;
        $scope.bill.processBill();
        //console.log(ob);
        /*notification string*/
        var username= $scope.getPassCodeUserPre(); // $scope.getUserNameForNotification();

        var nstr='';
        if(!(ob.notification==null||ob.notification==undefined)) {
          var n=ob.notification;
          if(n.value!="") {
            nstr = "Express Discount of " + (n.type == "percentage" ? (n.value + "%") : ("Rs." + n.value + "/-")) + " applied on " + ob.item + " by " + username + " for customer " + ($scope.bill._customer.firstname == undefined ? "none" : $scope.bill._customer.firstname);
            $scope.offerNotification('Offer Applied',nstr);
          }
        }
        if(!(ob.offerRemoved==null||ob.offerRemoved==undefined)){
          console.log(ob.offerRemoved);
          if(ob.offerRemoved.length>0){
            var n=ob.offerRemoved[0];
            nstr = "Removed Express Discount of " + (n.type == "percentage" ? (n.value + "%") : ("Rs." + n.value + "/-")) + " applied on " + ob.item + " by " + username + " for customer " + ($scope.bill._customer.firstname == undefined ? "none" : $scope.bill._customer.firstname);
            $scope.offerNotification('Removed Offer',nstr);
          }
        }
        console.log(nstr);
        if (fromKot) {
          $scope.bill.updateBill({}, 'update').then(function () {
            $scope.bill.cardTransaction={};
            if($rootScope.isInstance){
              growl.success('Offer applied on the selected kot.',{ttl:3000});
              $scope.initializeTab();
                $scope.isBillingOn=false;
                $scope.initializeTab();
                $scope.getDaySale();

            }
            // console.log($scope.bill);
            //$modalInstance.close($scope.bill);
          },function(err){
            growl.error('There is problem updating the transaction.',{ttl:3000});
            if(err.data.error=='inValid'){
              $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

              })
            }
          });
        }

        /*notification string ends here*/
      });
    };
    $scope.removeOfferFromKot=function(kotItem){
      var d=$q.defer();
      if(_.has(kotItem,'offer')){
        $ngBootbox.confirm('Applied offer on this item will be removed. Sure ?')
          .then(function() {
            var offer_id=kotItem.offer._id;
            _.forEach($scope.bill._kots,function(kot) {
              _.forEach(kot.items, function (item) {
                if (_.has(item, 'offer')) {
                  if (item.offer._id == offer_id) {
                    $scope.removeOfferNotification(angular.copy(item.offer));
                    delete item.offer;
                    delete item.discounts;
                    delete item.isDiscounted;
                  }
                }
              })
            });
            if($scope.bill._offers.length>0){
              if($scope.bill._offers[0].offer._id==offer_id){
                $scope.removeOfferNotification(angular.copy($scope.bill._offers[0].offer));
                $scope.bill._offers=[];
                $scope.bill.complimentary=false;
                $scope.bill.complimentaryComment="";
                $scope.bill.billDiscountAmount=0;
                $scope.bill.processBill();
              }
            }
            d.resolve(true);
          }, function() {
            console.log('Confirm dismissed!');
            d.resolve(false);
          });
      }
      return d.promise;
    }

    $scope.removeOfferFromKotByOffer=function(offerId){
      $scope.checkAndDiscardCode(offerId);
      var d=$q.defer();
      //  if(_.has(kotItem,'offer')){
      /*$ngBootbox.confirm('Applied offer on this item will be removed. Sure ?')
       .then(function() {*/
      var offer_id=offerId;
      _.forEach($scope.bill._kots,function(kot) {
        _.forEach(kot.items, function (item) {
          if (_.has(item, 'offer')) {
            if (item.offer._id == offer_id) {
              $scope.removeOfferNotification(angular.copy(item.offer));
              delete item.offer;
              delete item.discounts;
              delete item.isDiscounted;
            }
          }
        })
      });
      if($scope.bill._offers.length>0){
        if($scope.bill._offers[0].offer._id==offer_id){
          $scope.removeOfferNotification(angular.copy($scope.bill._offers[0].offer));
          $scope.bill._offers=[];
          $scope.bill.complimentary=false;
          $scope.bill.complimentaryComment="";
          $scope.bill.billDiscountAmount=0;
          $scope.bill.processBill();
        }
      }
      d.resolve(true);
      /* }, function() {
       console.log('Confirm dismissed!');
       d.resolve(false);
       });*/
      // }
      return d.promise;
    }


    $scope.showBootBox=function(message) {
      var d = $q.defer();
      $ngBootbox.confirm(message)
        .then(function () {
          d.resolve(true);
        }, function () {
          d.resolve(false);
        });
      return d.promise;
    }

    $scope.getManagerPermission=function(){
      var d=$q.defer();
      $scope.passCodeModal().then(function(passCode) {
        if (passCode === Utils.getPassCode(passCode, $rootScope.users,'Manager,Admin')) {
          $scope.setTempManagerName(passCode);
          d.resolve(true);
        } else {
          growl.error("Wrong Passcode!", {
            ttl: 2000
          });
          d.reject(false);
        }
      });
      return d.promise;
    }
    /* Open Change KOT Item Quantity */
    $scope.deletedItems = {};
    $scope.openChangeKotQuantity = function(size, kotIndex, kotItemIndex) {

      if (Utils.hasSetting('enable_card', $scope.settings)) {
        if(!_.has($scope.bill._kots[kotIndex],'cardTransaction')){
          growl.error('Kot item should be part of card transaction.',{ttl:3000} );
          return false;
        }
      }
      if ($scope.bill._kots[kotIndex].isVoid) {
        return false;
      }

     // alert($scope.bill.isPrinted);
      if ((!Utils.hasSetting('cancel_item_after_print_bill', $scope.settings)) ) {
        var flag=false;
        if( $scope.bill.isPrinted){flag=true;}
        if(($scope.offerPrint && $scope.billStatus=='PrintBill')||($scope.offerPrint && ($scope.bill.rePrint.length>0))){
          flag=true;
        }

        if(flag){
            growl.error('Printed bill items can not be modified', { ttl: 2000});
            return false;
        }

      }
      if(Utils.hasSetting('c_m_p',$scope.settings,'vItem')) {
        $scope.getManagerPermission().then(function (result) {
          $scope.openChangeKotQuantityPass(size, kotIndex, kotItemIndex);
        })
      }else{
        $scope.openChangeKotQuantityPass(size, kotIndex, kotItemIndex);
      }
    };
    $scope.openChangeKotQuantityPass=function(size, kotIndex, kotItemIndex){
      var kotItem=$scope.bill._kots[kotIndex].items[kotItemIndex];
      if(_.has(kotItem,'offer')){
        $scope.removeOfferFromKot(kotItem).then(function(status){
          if(status){
            $scope.modalChangeKotQuantity(size, kotIndex, kotItemIndex);
          }
        })
      }else{
        if($scope.bill.offerItemValidation.length>0){
          $ngBootbox.confirm('Applied offer related to bill amount and count will be removed ?')
            .then(function() {
              _.forEach($scope.bill.offerItemValidation,function(offer){
                $scope.discardOfferItem(offer.offerId);
                $scope.removeOfferFromKotByOffer(offer.offerId);
              })
              $scope.bill.offerItemValidation=[];
              $scope.modalChangeKotQuantity(size, kotIndex, kotItemIndex);

            })
        }else {
          $scope.modalChangeKotQuantity(size, kotIndex, kotItemIndex);

        }
      }
    }
    $scope.modalChangeKotQuantity=function(size, kotIndex, kotItemIndex){
       if (Utils.hasSetting('enable_card', $scope.settings)) {
          //$scope.bill = bill;
          if($scope.bill._offers.length>0){
            growl.error('Bill wise offer applied,can\'t delete!!',{ttl:2000});
            return false;
          }
        }
      $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/item/_changeKotQty.html',
        size: size,
        resolve: {
          kotIndex: function() {
            return kotIndex;
          },
          kotItemIndex: function() {
            return kotItemIndex;
          },
          bill: function() {
            return $scope.bill;
          },
          isEdit:function(){
            return $scope.isEditBill;
          },
          user:function(){
            return $scope.getPassCodeUserPre();
          }
        },
        controller: ['$scope', 'kotIndex', 'kotItemIndex', 'bill', '$modalInstance','isEdit','user', function($scope, kotIndex, kotItemIndex, bill, $modalInstance,isEdit,user) {
          $scope.itemname= angular.copy(bill._kots[kotIndex].items[kotItemIndex].name);
          $scope.kotNumber=angular.copy(bill._kots[kotIndex].kotNumber);
          $scope.kotIndex = kotIndex;
          $scope.isDeleted=false;
          $scope.kotItemIndex = kotItemIndex;
          $scope.errorMessage = "";
          $scope.bill = angular.copy(bill);
          $scope.reducedQty = 0;
          $scope.comment = "";
          $scope.isEditBill=isEdit;
          $scope.changeQty = function() {
            if ($scope.comment.length > 0) {
              var _i = angular.copy($scope.bill._kots[kotIndex].items[kotItemIndex]);
              _i.quantity = ($scope.reducedQty);
              _i.comment = $scope.comment;
              _i.deleteUser=user;
              $scope.deletedItems = _i;
              _i.voidItemTime = new Date();
              $scope.bill._kots[kotIndex].deleteHistory.push(_i);
              var indexOfDeleteItem = $scope.bill._kots[kotIndex].deleteHistory.indexOf(_i);
              // alert(indexOfDeleteItem);
              $scope.reducedQty = 0;
              if (($scope.bill._kots[kotIndex].items[kotItemIndex].quantity) == 0) {
                $scope.bill._kots[kotIndex].items.splice(kotItemIndex, 1);
                $scope.isDeleted=true;
              }
              $scope.bill._items = [];
              $scope.bill.processBill();
               if(!$scope.isEditBill)
              updateInsertDeletedItem(null, kotIndex, indexOfDeleteItem,$scope.bill);
              $modalInstance.close({bill: $scope.bill,itemname:$scope.itemname,kotnumber:$scope.kotNumber,isDeleted:$scope.isDeleted});
            } else {
              $scope.errorMessage = "Comment is mandatory!";
            }
          };
          $scope.changeQtyRemoveAll = function() {
            if ($scope.comment.length > 0) {
              var _i = angular.copy($scope.bill._kots[kotIndex].items[kotItemIndex]);
              // _i.quantity = ($scope.reducedQty);
              _i.comment = $scope.comment;
              _i.voidItemTime = new Date();
              _i.deleteUser=user;
              $scope.deletedItems = _i;
              $scope.bill._kots[kotIndex].deleteHistory.push(_i);
              var indexOfDeleteItem = $scope.bill._kots[kotIndex].deleteHistory.indexOf(_i);
              // alert(indexOfDeleteItem);
              /*  $scope.reducedQty = 0;
               if( ($scope.bill._kots[kotIndex].items[kotItemIndex].quantity )==0){*/
              $scope.bill._kots[kotIndex].items.splice(kotItemIndex, 1);
              //}
              $scope.bill._items = [];
              $scope.bill.processBill();
               if(!$scope.isEditBill)
              updateInsertDeletedItem(null, kotIndex, indexOfDeleteItem,$scope.bill);
              $modalInstance.close({bill: $scope.bill,itemname:$scope.itemname,kotnumber:$scope.kotNumber,isDeleted:true});
            } else {
              $scope.errorMessage = "Comment is mandatory!";
            }

          };
          $scope.decrementQty = function() {
            // $scope.errorMessage=null;
            if (parseFloat($scope.bill._kots[kotIndex].items[kotItemIndex].quantity) != 0) {
              $scope.reducedQty += 1;
              $scope.bill._kots[kotIndex].items[kotItemIndex].quantity -= 1;

            }
          };

          $scope.incrementQty = function() {
            //  $scope.errorMessage=null;
            if (parseFloat($scope.bill._kots[kotIndex].items[kotItemIndex].quantity) < (parseFloat($scope.bill._kots[kotIndex].items[kotItemIndex].quantity) + $scope.reducedQty)) {
              $scope.reducedQty -= 1;
              $scope.bill._kots[kotIndex].items[kotItemIndex].quantity += 1;
            }
          };
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
        }]
      }).result.then(function(bill) {
         if($scope.isEditBill)
          $scope.bill = bill.bill;
        if(bill.isDeleted){
          var username=$scope.getUserNameForNotification();
          var str="Deleted item "+bill.itemname+" from Kot Number:"+bill.kotnumber+" by "+ username +" for customer "+($scope.bill._customer.firstname==undefined?"none":$scope.bill._customer.firstname);
          $scope.offerNotification('Deleted Item',str);
        }
      });
    }
    $scope.decrementBillQty = function(itemIndex) {
      console.log(itemIndex);
      $scope.bill.decrementQuantity(itemIndex);
      $scope.handleBill();
    };

    $scope.showSettlementButton = function() {
      return (Utils.hasSetting('settle_bill_on_print', $scope.settings, $scope.selectedTab.tabType)) ? false : Utils.hasSetting('enable_settlement', $scope.settings, $scope.selectedTab.tabType);
    };
    $scope.managerSettle = function() {
      return  Utils.hasSetting('settle_by_manager', $scope.settings, $scope.selectedTab.tabType);
    };
    $scope.cashierSettle = function() {
      return  Utils.hasSetting('settle_by_cashier', $scope.settings, $scope.selectedTab.tabType);
    };
    $scope.settleForEditBill=function(size,billFromTable) {
    var tbill=(billFromTable) ? billFromTable : $scope.bill;
      $scope.isBillingOn=true;
       if(_.has( tbill,'complimentary')){
            if(tbill.complimentary){
            $scope.complimentarySettleEditHead(tbill);
            return false;
          }
      }


      $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/_settleBill.html',
        controller: 'settleBillCtrl',
        size: 'lg',
        resolve: {
          itemIndex: function () {
            return undefined;
          },
          taxes: function () {
            return $scope.taxes;
          },
          bill: function () {
            return (billFromTable) ? billFromTable : $scope.bill;
          },
          isKot: function () {
            return false;
          },
          kotIndex: function () {
            return 0;
          },
          settleSettings: function () {
            return $scope.settleSettings;
          },
           bankSettings: function () {
            return $scope.bankSettings;
          }
        }

      }).result.then(function (bill) {
        // console.log(bill);
        $scope.saveEditBillSettle(bill);
      })
    }

    $scope.complimentarySettleEditHead=function(bill){
      var compHeads=localStorageService.get('complimentary_head');
      if(compHeads==null){
         $ngBootbox.prompt('Please enter the settle head ?').then(function(data){
            $scope.copmlimentarySettleSaveEdit(data,bill);
          });
         return false;
      }
      var compHeadArr=compHeads.split(';');
      if(compHeadArr.length>0){
           $modal.open({
              templateUrl: 'app/billing/item/_complimentary_head.html',
              size: 'sm',
              resolve: {
                heads:function(){
                  return compHeadArr;
                }
              },
              controller: ['$scope', '$modalInstance','heads', function ($scope, $modalInstance,heads) {
                $scope.heads=heads;
                $scope.head=heads[0];
                $scope.cancel=function(){
                  $modalInstance.dismiss();
                }
                $scope.clickEvent=function(){
                  console.log($scope.head);
                  $modalInstance.close($scope.head);
                }

              }]
            }).result.then(function(head){
              $scope.copmlimentarySettleSaveEdit(head,bill);
         });
      }else{
         $ngBootbox.prompt('Please enter the settle head ?').then(function(data){
            $scope.copmlimentarySettleSaveEdit(data,bill);
        })
      }
    }

   $scope.copmlimentarySettleSaveEdit=function(data,bill){
       bill.complimentaryHead=data;
      var dCard={cardType:'DebitCard',totalAmount:0,detail:[]};
        bill.payments.cards.push(dCard);

        var cCard={cardType:'CreditCard',totalAmount:0,detail:[]};
        bill.payments.cards.push(cCard);

        var oCard={cardType:'Other',totalAmount:0,detail:[]};
        bill.payments.cards.push(oCard);

        var sCard={cardType:'SmartCard',totalAmount:0,detail:[]};
        bill.payments.cards.push(sCard);
        bill.payments.cash.push(0);
        $scope.saveEditBillSettle(bill);
   }

    $scope.saveEditBillSettle=function(bill){
      $scope.bill = bill;
        // $scope.doBilling(true);
        var flag=false;
        if($scope.bill.billPrintTime!=null){
          flag=true;
        }
        var ptime=new Date();
        $scope.bill.billPrintTime = ($scope.bill.billPrintTime==null ? ptime:$scope.bill.billPrintTime);
        $scope.bill.rePrint.push({user:$scope.getPassCodeUserPre(),printTime: ptime,comment:'Edited bill reprint'});
        var comment = "";
        $scope.bill.generateKotNumber().then(function (kotnumber) {
          $scope.bill.kotNumber = $scope.bill == null ? kotnumber : kotnumber - 1;
          if($scope.bill._items.length>0) {
            $scope.currentKotIndex = $scope.bill.printKot('', $scope.getKotWaiter());
          }
          if ($scope.bill._items.length === 0) {
            // Prepare KOT Aggregation
            $scope.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function (preparedBill) {
              $scope.billToPrint = preparedBill;
              //printer.print('_template_Bill.html', $scope.billToPrint);
              if (Utils.hasSetting('bill_print_template', $scope.settings)) {
                    var bill_print_template = parseInt(Utils.getSettingValue('bill_print_template', $scope.settings))
                    if(bill_print_template==1)
                    BillPrintService.templateBillPrintWithTaxBaseAmount(angular.copy( $scope.billToPrint));
                    else if(bill_print_template==2 && Utils.hasSetting('inclusive_tax', $scope.settings))
                    BillPrintService.templateBillPrintInclusiveTaxes(angular.copy($scope.billToPrint));
                    else
                    printer.print('_template_Bill.html', $scope.billToPrint);
                }
            else{
                  printer.print('_template_Bill.html', $scope.billToPrint);
              }
              //  $scope.bill.id=$scope.bill._id;
              // $scope.bill._id=$scope.bill.ng_id;

              // $scope.bill.ng_id=Utils.guid();
              delete  $scope.bill.ng_id;
              delete $scope.bill._id;
              console.log($scope.bill);
              //Saving edited billl to server
              billResource.updateWithId($scope.bill).$promise.then(function(status){
                console.log(status);
                $scope.isEditBill=false;
                $scope.editBill={};
                $scope.isBillingOn=false;
                $scope.initializeTab();
                $scope.getDaySale();
                growl.success('Edited bill synced to server',{ttl:3000});
              },function(){
                growl.success('Error while contacting to server',{ttl:3000});
              })
            }, function (errors) {
              _.forEach(errors, function (e) {
                growl.error(e, {
                  ttl: 3000
                });
              });
            });
          }
          return false;

        }, function (error) {
          $scope.isEditBill=false;
          $scope.editBill={};
          $scope.isBillingOn = false;
          $scope.isOnlineBill = false;
          $scope.getDaySale();
          //console.log(error);
        });
        if(flag){
          $scope.rePrintNotification($scope.bill,'Edited bill reprint');
        }
    }
    $scope.getKotWaiter=function(){
      if($rootScope.isInstance){
        var waiter=null;
        if($scope.selectedWaiter==null){
          if($scope.bill._waiter!=null){
            waiter=$scope.bill._waiter;
          }
        }else{
          waiter=$scope.selectedWaiter;
        }
      }else{
        waiter=$scope.bill._waiter;
      }
      return waiter;
    }
    $scope.cancelEdit=function(){
      $scope.isEditBill=false;
      $scope.editBill={};
      $scope.isBillingOn=false;
      $scope.initializeTab();
      $scope.getDaySale();
      growl.success('Bill editing cancelled!!',{ttl:3000});
    }
    /* Settle Bill */

    $scope.openSettleBill = function(size, billFromTable,isMenu) {
      //alert( $scope.isUser('settle_bill'));

      var tbill = (billFromTable) ? billFromTable : $scope.bill;
      var tabtype = (billFromTable) ? billFromTable.tabType : $scope.selectedTab.tabType;
      if(tbill.tabType=='table')
        $scope.lockTable(tbill._tableId);
      else
        $scope.lockNonTable(tbill.billNumber);

      //  console.log(tbill);
      if (Utils.hasSetting('enable_settlement', $scope.settings, tabtype)) {
        if (!Utils.hasSetting('settle_bill_on_print', $scope.settings, tabtype)) {
          if (!tbill.isPrinted) {
            growl.error('Bill need to be printed before settle.', {
              ttl: 2000
            })
            return false;
          }
        }
        // console.log(tbill);
        if($scope.managerSettle()) {
          console.log($scope.tableStarted);
          if((($scope.selectedManager!=null  )&&(!isMenu))){
            //console.log(currentUser);
            /*  if($scope.isPermission('settle_bill')){
             $scope.openSettleBillExt(size, billFromTable);
             }else{
             growl.error("You don't have permission to perform this action!", {
             ttl: 3000
             });
             $scope.initializeTab();
             }*/
            $scope.openSettleBillExt(size, billFromTable);
            return false;
          }
           if((($scope.selectedCashier!=null   )&&(!isMenu) &&($scope.cashierSettle()))){
            $scope.openSettleBillExt(size, billFromTable);
            return false;
          }
          var flag=false;
          $scope.passCodeModal().then(function (res) {
            if (res == 'cancel') {
              $scope.initializeTab();
              return false;
            }
            //console.log( res);
            var _f = _.filter($rootScope.users, {passcode: res});
            if (_f.length > 0) {
              _.forEach(_f[0].selectedRoles, function (role) {
                if (role.name == 'Manager') {
                  flag = true;
                }
                 if (role.name == 'Cashier' && $scope.cashierSettle()) {
                  flag = true;
                }
              })
            }
            if(flag){
              /* if($scope.isPermission('settle_bill')){
               $scope.openSettleBillExt(size, billFromTable);
               }else{
               growl.error("You don't have permission to perform this action!", {
               ttl: 3000
               });
               $scope.initializeTab();
               }*/
              $scope.openSettleBillExt(size, billFromTable);
            }else{
              growl.error("You are not authorized to perform this action!", {
                ttl: 3000
              });
              $scope.initializeTab();
            }
          });
        }else{
          if($scope.cashierSettle()) {
          if((($scope.selectedCashier!=null)&&(!isMenu))){
            $scope.openSettleBillExt(size, billFromTable);
            return false;
          }

          var flag=false;
          $scope.passCodeModal().then(function (res) {
            if (res == 'cancel') {
              $scope.initializeTab();
              return false;
            }
            //console.log( res);
            var _f = _.filter($rootScope.users, {passcode: res});
            if (_f.length > 0) {
              _.forEach(_f[0].selectedRoles, function (role) {
                if (role.name == 'Cashier') {
                  flag = true;
                }
              })
            }
            if(flag){
              $scope.openSettleBillExt(size, billFromTable);
            }else{
              growl.error("You are not authorized to perform this action!", {
                ttl: 3000
              });
              $scope.initializeTab();
            }
          });
        }else{
            $scope.openSettleBillExt(size, billFromTable);
          }
        }

      } else {
        $scope.isBillingOn=false;
        $scope.isOnlineBill=false;
        $scope.getDaySale();
        growl.error("You are not authorized to perform this action!", {
          ttl: 3000
        });
      }

    };
    $scope.cardSettle=function(){

      $ngBootbox.confirm('Are you sure to settle this bill?').then(function(){
        var dCard={cardType:'DebitCard',totalAmount:0,detail:[]};
        $scope.bill.payments.cards.push(dCard);

        var cCard={cardType:'CreditCard',totalAmount:0,detail:[]};
        $scope.bill.payments.cards.push(cCard);

        var oCard={cardType:'Other',totalAmount:0,detail:[]};
        $scope.bill.payments.cards.push(oCard);

        var sCard={cardType:'SmartCard',totalAmount:0,detail:[]};
        sCard.totalAmount=$scope.bill.getKotsTotalBill();
        $scope.bill.payments.cards.push(sCard);
        $scope.bill.checkBillExists().then(function (len) {
          //$scope.bill.closeTime=new Date();
          if (!len) {

            $scope.bill.updateBill({
              isSettled: true,
              _closeTime: new Date()
            }, 'insert').then(function () {
              var _to = $scope.bill.tab;
              if ($scope.bill.tab === "table") {
                delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
                $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
              }
              // $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
              // $state.go('billing');
              $scope.bill.cardTransaction={};
              $scope.isBillingOn = false;
              $scope.isOnlineBill = false;
              $scope.initializeTab();
              $scope.getDaySale();
              $scope.updateOnlineBillStatus('settle');
            });
          } else {
            $scope.bill.updateBill({
              isSettled: true,
              _closeTime: new Date()
            }, 'update').then(function () {
              var _to = $scope.bill.tab;
              if ($scope.bill.tab === "table") {
                delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
                $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
              }
              //  $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
              //$state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
              // $state.go('billing');
              $scope.bill.cardTransaction={};
              $scope.isBillingOn = false;
              $scope.isOnlineBill = false;
              $scope.initializeTab();
              $scope.getDaySale();
              $scope.updateOnlineBillStatus('settle');
            },function(err){
              growl.error('There is problem updating the transaction.',{ttl:3000});
              if(err.data.error=='inValid'){
                $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){
                })
              }
            });
          }
          /*Added by Rajat for integration*/
          closeBills($scope.bill);
          if($scope.bill._tableId){
            sendBillToInresto(bill);
            removeTableBooking(bill);
          }
        });
      })
    }

    $scope.complimentarySettle=function(bill){
      var compHeads=localStorageService.get('complimentary_head');
      if(compHeads==null){
         $ngBootbox.prompt('Please enter the settle head ?').then(function(data){
            $scope.copmlimentarySettleSave(data,bill);
          });
         return false;
      }
      var compHeadArr=compHeads.split(';');
      if(compHeadArr.length>0){
           $modal.open({
              templateUrl: 'app/billing/item/_complimentary_head.html',
              size: 'sm',
              resolve: {
                heads:function(){
                  return compHeadArr;
                }
              },
              controller: ['$scope', '$modalInstance','heads', function ($scope, $modalInstance,heads) {
                $scope.heads=heads;
                $scope.head=heads[0];
                $scope.cancel=function(){
                  $modalInstance.dismiss();
                }
                $scope.clickEvent=function(){
                  console.log($scope.head);
                  $modalInstance.close($scope.head);
                }

              }]
            }).result.then(function(head){
              $scope.copmlimentarySettleSave(head,bill);
         });
      }else{
         $ngBootbox.prompt('Please enter the settle head ?').then(function(data){
            $scope.copmlimentarySettleSave(data,bill);
        })
      }
    }

    $scope.copmlimentarySettleSave=function(data,bill){
      $scope.bill=bill;
      $scope.bill.complimentaryHead=data;
      var dCard={cardType:'DebitCard',totalAmount:0,detail:[]};
        $scope.bill.payments.cards.push(dCard);

        var cCard={cardType:'CreditCard',totalAmount:0,detail:[]};
        $scope.bill.payments.cards.push(cCard);

        var oCard={cardType:'Other',totalAmount:0,detail:[]};
        $scope.bill.payments.cards.push(oCard);

        var sCard={cardType:'SmartCard',totalAmount:0,detail:[]};
        $scope.bill.payments.cards.push(sCard);

        $scope.bill.payments.cash.push(0);


       if (Utils.hasSetting('settle_bill_on_print', $scope.settings, $scope.selectedTab.tabType)) {
           // $scope.bill = bill;
           $scope.bill.billPrintTime=null;
            $scope.printBill(true);
            return false;
          }
        // sCard.totalAmount=$scope.bill.getKotsTotalBill();

        $scope.bill.checkBillExists().then(function (len) {
          //$scope.bill.closeTime=new Date();
          if (!len) {

            $scope.bill.updateBill({
              isSettled: true,
              _closeTime: new Date()
            }, 'insert').then(function () {
              var _to = $scope.bill.tab;
              if ($scope.bill.tab === "table") {
                delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
                $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
              }
              // $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
              // $state.go('billing');
              $scope.bill.cardTransaction={};
              $scope.isBillingOn = false;
              $scope.isOnlineBill = false;
              $scope.initializeTab();
              $scope.getDaySale();
              $scope.updateOnlineBillStatus('settle');
            });
          } else {
            $scope.bill.updateBill({
              isSettled: true,
              _closeTime: new Date()
            }, 'update').then(function () {
              var _to = $scope.bill.tab;
              if ($scope.bill.tab === "table") {
                delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
                $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
              }
              //  $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
              //$state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
              // $state.go('billing');
              $scope.bill.cardTransaction={};
              $scope.isBillingOn = false;
              $scope.isOnlineBill = false;
              $scope.initializeTab();
              $scope.getDaySale();
              $scope.updateOnlineBillStatus('settle');
            },function(err){
              growl.error('There is problem updating the transaction.',{ttl:3000});
              if(err.data.error=='inValid'){
                $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){
                })
              }
            });
          }
          /*Added by Rajat for integration*/
          closeBills($scope.bill);

        });
    }

    $scope.openSettleBillExt=function(size, billFromTable) {
      //alert();
      if (Utils.hasSetting('enable_card', $scope.settings)) {
        $scope.cardSettle();
        return false;
      }
      var tbill = (billFromTable) ? billFromTable : $scope.bill;
      var tabtype = (billFromTable) ? billFromTable.tabType : $scope.selectedTab.tabType;
      //alert(tbill.complimentary);
    if(_.has( tbill,'complimentary')){
            if(tbill.complimentary){
            $scope.complimentarySettle(tbill);
            return false;
          }
      }


      $modal.open({
        backdrop: 'static',
        keyboard: false,
        templateUrl: 'app/billing/_settleBill.html',
        controller: 'settleBillCtrl',
        size: 'lg',
        resolve: {
          itemIndex: function () {
            return undefined;
          },
          taxes: function () {
            return $scope.taxes;
          },
          bill: function () {
            return (billFromTable) ? billFromTable : $scope.bill;
          },
          isKot: function () {
            return false;
          },
          kotIndex: function () {
            return 0;
          },
          settleSettings: function () {
            return $scope.settleSettings;
          },
           bankSettings: function () {
            return $scope.bankSettings;
          }
        }

      }).result.then(function (bill) {
        console.log(bill);

        if (bill == 'cancel') {
          $scope.isBillingOn=false;
          $scope.unlockTable();
        }
        else {
          if (Utils.hasSetting('settle_bill_on_print', $scope.settings, $scope.selectedTab.tabType)) {
            $scope.bill = bill;
            $scope.printBill(true);
            return false;
          }
          $scope.bill = bill;
          $scope.bill.checkBillExists().then(function (len) {
            //$scope.bill.closeTime=new Date();
            if (!len) {

              $scope.bill.updateBill({
                isSettled: true,
                _closeTime: new Date()
              }, 'insert').then(function () {

                var _to = $scope.bill.tab;
                if ($scope.bill.tab === "table") {
                  delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
                  $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
                }
                // $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
                // $state.go('billing');
                $scope.bill.cardTransaction={};
                $scope.isBillingOn = false;
                $scope.isOnlineBill = false;
                 if (Utils.hasSetting('print_after_settle', $scope.settings)) {
                                    $scope.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function(preparedBill) {
                                        $scope.settleBillPrint = angular.copy(preparedBill);
                                        $scope.settleBillPrint.bill = angular.copy(bill);
                                        printAfterSettle(angular.copy( $scope.settleBillPrint));
                                    }, function(errors) {
                                        // alert();
                                        _.forEach(errors, function(e) {
                                            //$scope.isBillingOn=false;
                                            growl.error(e, {
                                                ttl: 3000
                                            });
                                        });
                                    });
                                }
                checkUrbanPiperSettings(angular.copy(bill));   //added for urban piper
                $scope.initializeTab();
                $scope.getDaySale();
                $scope.updateOnlineBillStatus('settle');
              });
            } else {
              $scope.bill.updateBill({
                isSettled: true,
                _closeTime: new Date()
              }, 'update').then(function () {
                var _to = $scope.bill.tab;
                if ($scope.bill.tab === "table") {
                  delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
                  $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
                }
                //  $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
                //$state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
                // $state.go('billing');
                $scope.bill.cardTransaction={};
                $scope.isBillingOn = false;
                $scope.isOnlineBill = false;
               if (Utils.hasSetting('print_after_settle', $scope.settings)) {
                                    $scope.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function(preparedBill) {
                                        $scope.settleBillPrint = angular.copy(preparedBill);
                                        $scope.settleBillPrint.bill = angular.copy(bill);
                                        printAfterSettle(angular.copy( $scope.settleBillPrint));
                                    }, function(errors) {
                                        // alert();
                                        _.forEach(errors, function(e) {
                                            //$scope.isBillingOn=false;
                                            growl.error(e, {
                                                ttl: 3000
                                            });
                                        });
                                    });
                                }
                checkUrbanPiperSettings(angular.copy(bill));   //added for urban piper
                $scope.initializeTab();
                $scope.getDaySale();
                $scope.updateOnlineBillStatus('settle');
              },function(err){
                growl.error('There is problem updating the transaction.',{ttl:3000});
                if(err.data.error=='inValid'){
                  $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

                  })
                }
              });
            }
            /*Added by Rajat for integration*/
            var bill=angular.copy($scope.bill);
            closeBills(bill);

             if($scope.bill._tableId){
            sendBillToInresto(bill);
            removeTableBooking(bill);
           }

          });
        }
      }, function (error) {
        $scope.isBillingOn = false;
        $scope.isOnlineBill = false;
        $scope.getDaySale();
        //console.log(error);
      });
    }
    $scope.checkAndDiscardCode=function(offerId) {
      var codeName = '';
      if ($scope.code != null) {
        var _fOffer = _.filter($scope.offers, {"_id": offerId});
        if (_fOffer.length > 0) {
          if (_.has(_fOffer[0].code, 'codeList')) {
            codeName = _fOffer[0].code.codeList.name;
          }
        }
        console.log(_fOffer);
        if (Utils.validateCode($scope.code.offerCode, _fOffer[0])) {
          // if (codeName == $scope.code.offerCode) {
          $scope.code = null;
        }
      }
    }
    $scope.discardOfferItem=function(offerId){
      $scope.checkAndDiscardCode(offerId);
      _.forEach($scope.bill._items,function(item){
        if(_.has( item,'offer' )){
          if(item.offer._id==offerId){
            delete item.offer;
            delete item.discounts;
            delete item.isDiscounted;
          }
        }
      })
      if($scope.bill._offers.length>0){
        if($scope.bill._offers[0].offer._id==offerId){
          $scope.bill._offers=[];
          $scope.bill.complimentary=false;
          $scope.bill.complimentaryComment="";
          $scope.bill.billDiscountAmount=0;
          $scope.bill.processBill();
        }
      }
      if($scope.bill.offerItemValidation.length>0){
        var _offer= _.filter($scope.bill.offerItemValidation,{offerId:offerId});
        if(_offer.length>0){
          $scope.removeOfferFromKotByOffer(offerId);
        }
        $scope.bill.offerItemValidation.splice(Utils.arrayObjectIndexOf($scope.bill.offerItemValidation,'offerId',offerId),1 );
      }
    }

    $scope.removeItem = function(index) {
      if(_.has( $scope.bill._items[index],'offer')){
        var offer_id=$scope.bill._items[index].offer._id;
        var tmpOffer=$scope.bill._items[index].offer;
        $ngBootbox.confirm('Applied offer on this item will be removed. Sure ?')
          .then(function() {
            $scope.discardOfferItem(offer_id);
            $scope.bill.removeItem(index);
            $scope.bill.processBill();
            $scope.handleBill();
            $scope.selectedOffer=null;
            $scope.resetBillItems();
            $scope.removeOfferNotification(tmpOffer,true);
          }, function() {
            console.log('Confirm dismissed!');
          });
      }else {
        $scope.bill.removeItem(index);
        $scope.bill.processBill();
        $scope.handleBill();
      }
    };
    $scope.code=null;
    $scope.showOfferComment=function(){
      /*  $modal.open({
       templateUrl: 'app/billing/item/_comment.html',
       controller: function ($scope, $modalInstance) {
       $scope.comment = "";
       $scope.addComment = function () {
       $modalInstance.close($scope.comment);
       };
       $scope.cancel = function () {
       $modalInstance.dismiss('cancel');
       };
       },
       size: 'sm'
       }).result.then(function (comment) {
       console.info(comment);
       $scope.bill.removeItem(index);
       });*/
      $scope.selectedOffer=null;
      $scope.IsOfferComplete=false;
      $modal.open({
        templateUrl: 'app/billing/item/_comment.html',
        /*  placement: 'bottom',
         backdrop: true,*/
        size:'lg',
        controller: function($scope, $modalInstance,offers,bill,Utils,CodeApplied,appliedCode,growl,isExpressOfferApplicable) {
          //alert(isExpressOfferApplicable);
          $scope.isExpressOfferApplicable = isExpressOfferApplicable;
          $scope.bill=bill;
          $scope.appliedOffers=[];
          $scope.offerAppliedOnBillItem=false;
          _.forEach($scope.bill._items,function(bItem){
            if(_.has(bItem,'offer')){
              var offerOb={_id:bItem.offer._id,name:bItem.offer.name};
              $scope.appliedOffers.push(offerOb);
              $scope.offerAppliedOnBillItem=true;
            }
          });
          _.forEach($scope.bill._kots,function(kot){
            _.forEach(kot.items,function(bItem){
              if(_.has(bItem,'offer')){
                var offerOb={_id:bItem.offer._id,name:bItem.offer.name};
                $scope.appliedOffers.push(offerOb);
              }
            })
          });
          if($scope.bill._offers.length>0){
            var offerOb={_id:$scope.bill._offers[0].offer._id,name:$scope.bill._offers[0].offer.name};
            $scope.appliedOffers.push(offerOb);
          }
          $scope.appliedOffers= _.uniq($scope.appliedOffers,'name');
          $scope.offers=[];
          for(var i=0;i<offers.length;i++) {
           // console.log(offers[i]);
            if(!_.has( offers[i].offer.code,'codeList')){
              $scope.offers.push(offers[i]);
            }
          }
          $scope.code={offerCode:'',isAppliedOnBill:false};
          if(appliedCode!=null){
            $scope.code.offerCode=appliedCode.offerCode;
            $scope.code.isAppliedOnBill=true;
          }else{
            $scope.code.offerCode='';
            $scope.code.isAppliedOnBill=false;
          }
          $scope.applyExpressOffer=function(){
            $modalInstance.close({isExpressOffer:true});
          }
          $scope.ok = function(offer) {
            if(offer.offer.getLeastValueItem){
              if($scope.offerAppliedOnBillItem){
                growl.error("Print kot first and then apply this offer.",{ttl:2000})
                return false;
              }
            }
            if(!($scope.code.offerCode==null||$scope.code.offerCode=='')){
              offer.codeApplied=$scope.code.offerCode;
            }
            $modalInstance.close(offer);
          };
          $scope.cancel = function() {
            $modalInstance.dismiss();
          };
          $scope.scrapOfferFromBill=function(offer){
            offer.isDelete=true;
            $modalInstance.close(offer);
          }
          $scope.codeValidatedOffer=[];
          $scope.searchOfferByCode=function(){
            var tempCodeOffers=[];
            $scope.codeValidatedOffer=[];
            for(var i=0;i<offers.length;i++){
              if(Utils.validateCode($scope.code.offerCode,offers[i].offer)){
                tempCodeOffers.push(offers[i]);
              }
            }
            if(tempCodeOffers.length>0){
              if(_.has(tempCodeOffers[0].offer.code,'uses')) {
                if (!(tempCodeOffers[0].offer.code.uses == null ||tempCodeOffers[0].offer.code.uses == '')) {
                  CodeApplied.countCodes({deployment_id:tempCodeOffers[0].offer.deployment_id,code:$scope.code.offerCode}).$promise.then(function(res){
                    if(res.count>=tempCodeOffers[0].offer.code.uses){
                      growl.error('This code is already used up.',{ttl:3000});

                    }else{
                      $scope.codeValidatedOffer=tempCodeOffers;
                    }
                  })
                }else{
                  $scope.codeValidatedOffer=tempCodeOffers;
                }
              }
            }
           // console.log($scope.codeValidatedOffer);
          }

        },
        resolve: {
          offers: function () {
            $scope.validatedOffers=[];
            _.forEach($scope.offers,function(offer){
              var isApplicable=false;
              var isBillwise=$scope.isBillWiseOffer(offer);
              if(Utils.offerValidated(offer)){
                for(var i=0;i<offer.tabs.length;i++){
                  var tab=$scope.selectedTab.tabType=='table'?'Table':$scope.selectedTab.tabType=='takeout'?'TakeOut':'Delivery';
                  //console.log($scope.selectedTab);
                  if(offer.tabs[i]==tab){
                    isApplicable=true;
                    //validatedOffers.push(offer);
                    break;
                  }
                }
              }
              var tempOffer={offer:angular.copy(offer) ,isValidated:isApplicable,appliedCount:0,isBillWise:isBillwise,reqApplicableItem:[]};
              var count=0;
              for(var i=0;i<$scope.bill._offers.length;i++){
                if($scope.bill._offers[i].offer._id==offer._id){
                  count++;
                }
              }
              tempOffer.appliedCount=count;
              if(!tempOffer.isBillWise){
                if(offer.applicable.items.length>0){
                  var relation=offer.applicable.items[0].relation;
                  var isFound=false;
                  for(var i=0;i<offer.applicable.items.length;i++){
                    var leftQty=offer.applicable.items[i].quantity;
                    /*Searching items in bill items*/
                    for(var j=0;j<$scope.bill._items.length;j++){
                      if(!((_.has($scope.bill._items[j], 'discounts') && $scope.bill._items[j].discounts.length > 0 ))){
                        if(offer.applicable.items[i].item==null||offer.applicable.items[i].item=="") {
                          if ($scope.bill._items[j].category._id == offer.applicable.items[i].category._id) {
                            if(leftQty<=$scope.bill._items[j].quantity) {
                              leftQty=0;
                              isFound = true;
                              break;
                            }else{
                              leftQty-=$scope.bill._items[j].quantity;
                            }
                          }
                        }else{
                          if ($scope.bill._items[j]._id == offer.applicable.items[i].item._id) {
                            if(leftQty<=$scope.bill._items[j].quantity) {
                              leftQty=0;
                              isFound = true;
                              break;
                            }else{
                              leftQty-=$scope.bill._items[j].quantity;
                            }

                          }
                        }
                      }
                    }
                    /*Searching items in bill items*/
                    /*Searching items in kots items*/
                    for(var k=0;k<$scope.bill._kots.length;k++){
                      for(var j=0;j<$scope.bill._kots[k].items.length;j++){
                        if(!((_.has($scope.bill._kots[k].items[j], 'discounts') && $scope.bill._kots[k].items[j].discounts.length > 0 ))){
                          if(offer.applicable.items[i].item==null||offer.applicable.items[i].item=="") {
                            if ($scope.bill._kots[k].items[j].category._id == offer.applicable.items[i].category._id) {
                              if(leftQty<=$scope.bill._kots[k].items[j].quantity) {
                                leftQty=0;
                                isFound = true;
                                break;
                              }else{
                                leftQty-=$scope.bill._kots[k].items[j].quantity;
                              }
                            }
                          }else{
                            if ($scope.bill._kots[k].items[j]._id == offer.applicable.items[i].item._id) {
                              if(leftQty<=$scope.bill._kots[k].items[j].quantity) {
                                leftQty=0;
                                isFound = true;
                                break;
                              }else{
                                leftQty-=$scope.bill._kots[k].items[j].quantity;
                              }
                            }
                          }
                        }
                      }
                    }
                    /*Searching items in kots items*/
                    var searchedItem={applicableItem:offer.applicable.items[i],leftQuantity:leftQty};
                    tempOffer.reqApplicableItem.push(searchedItem);
                  }
                }
              }

              $scope.validatedOffers.push(tempOffer);
              $scope.validatedOffers= _.filter($scope.validatedOffers,{isValidated:true});
             // console.log($scope.validatedOffers);
            });
            return _.sortBy( $scope.validatedOffers,['isValidated','created'],['asc']);
          },
          bill:function(){return $scope.bill ;},
          appliedCode:function(){
            return $scope.code;
          },
          isExpressOfferApplicable:function(){return Utils.hasSetting('enable_express_offer', $scope.settings)}
        }
      }).result.then(function(offer){
      //console.log(offer);
      //console.log($scope.bill);
      if(Utils.hasSetting('enable_card', $scope.settings)){
            $http.post($rootScope.url + '/api/synckots/CardTransactionsCountByBill', {
                billId: $scope.bill._id
               }).then(function (result) {
                //console.log(result.data);
                if(result.data.cards.length>1){
                  growl.error('More than 1 card transaction !! ',{ttl:2000})
                  return false;
                }else{
                  $scope.reDeemOffer(offer);
                }
            })
         }else{
            $scope.reDeemOffer(offer);
         }
      })
    }

    $scope.offerPrint=false;

    $scope.reDeemOffer=function(offer){
        if(offer.isExpressOffer){
          $scope.expressOffer();
          return false;
        }

        if(_.has(offer,'isDelete')){
          $scope.showBootBox('Do you really want to scrap '+offer.name+' offer?').then(function(status){
            if(status){
              $scope.scrapOfferFromBill(offer._id);
              $scope.removeOfferNotification(offer,false);
              //$scope.bill.complimentary=false;
            }
          })
          return false;
        }
        if(_.has(offer,'codeApplied')){
          if(_.has( offer.offer.code,'uses')) {
            if(offer.offer.code.uses!=null) {
              $scope.code = {offerCode: offer.codeApplied, offer: offer.offer};
            }
          }
        }
        if(offer.isBillWise) {
          for(var i=0;i<$scope.bill._offers.length;i++){
            if($scope.bill._offers[i].isBillWise){
              //$scope.bill._offers.splice(i,1);
              $scope.removeOfferNotification($scope.bill._offers[i].offer,false);

              $scope.scrapOfferFromBill($scope.bill._offers[i].offer._id);
              //break;
            }
          }


          var billOfferApplied={offer:offer.offer,user:$scope.getPassCodeUserPre(),time:new Date(),offerAmount:0,isBillWise:offer.isBillWise,isApplicableItem:true};
          $scope.bill.complimentary= _.has( offer.offer,'complimentary')?offer.offer.complimentary:false;
          if($scope.bill.complimentary){
             $ngBootbox.prompt('Comment for the complimentary offer.').then(function(com){
              //alert(com);
              if(!(com==null||com=='')){
                $scope.bill.complimentaryComment=com;
              }else{
                $scope.bill.complimentary=false;
                growl.error('Invalid comment !!',{ttl:3000});
                return false;
              }
              $scope.bill._offers.push(billOfferApplied);
              //$scope.bill.complimentaryComment=($scope.bill.complimentary?$scope.bill.complimentaryComment:"");

              $scope.bill.processBill();

              $scope.selectedOffer=null;
              var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
              if(_fTab.length>0){
                $scope.renderItemsTabWise(_fTab[0],$rootScope.rootMasterItems);
              }
              growl.success( offer.offer.name +' offer applied',{ttl:2000});
             // console.log($scope.billStatus);
              if($scope.billStatus=='Settle'){$scope.billStatus='PrintBill';$scope.bill.isPrinted=false;$scope.offerPrint=true;}
              $scope.offerAppliedNotification(offer.offer,false);
            },function(){
                $scope.bill.complimentary=false;
                growl.error('Invalid comment !!',{ttl:3000});
                return false;
            })
          }else{
            $scope.bill._offers.push(billOfferApplied);
            //$scope.bill.complimentaryComment=($scope.bill.complimentary?$scope.bill.complimentaryComment:"");

            $scope.bill.processBill();

            $scope.selectedOffer=null;
            var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
            if(_fTab.length>0){
              $scope.renderItemsTabWise(_fTab[0],$rootScope.rootMasterItems);
            }
            growl.success( offer.offer.name +' offer applied',{ttl:2000});
           // console.log($scope.billStatus);
            if($scope.billStatus=='Settle'){$scope.billStatus='PrintBill';$scope.bill.isPrinted=false;$scope.offerPrint=true;}
            $scope.offerAppliedNotification(offer.offer,false);
          }
        }else {

          var offerItems={applicable:[],appliedItems:[]};
          var tabData= {tabId:$scope.selectedTab.tabId , Items:[],categories:[],superCategories:[] };
          /*   var tabOfferData= {tabId:$scope.selectedTab.tabId , Items:[],categories:[],superCategories:[] };*/
          var rootMasterItems=[];
          var _fMasterItem= _.filter($rootScope.rootMasterItems,{'tabId':$scope.selectedTab.tabId});
          if(_fMasterItem.length>0){
            rootMasterItems= _fMasterItem[0].Items;
          }
          _.forEach(offer.offer.applicable.items,function(item){
            if(item.item==null||item.item==""){
              _.forEach(rootMasterItems,function(mItem){
                if((mItem.category._id==item.category._id)||(item.category==null||item.category=='')){
                  var tempItem=angular.copy(mItem);
                  // var tempOfferItem=angular.copy(mItem);
                  /*To Apply Direct Offer On Applicable Item*/
                  tempItem.offer={
                    type:'applicable',
                    _id:offer.offer._id,
                    name:offer.offer.name
                  }

                  // tabData.Items.push(mItem);

                  tabData.Items.push(tempItem);
                  tabData.categories.push(mItem.category);
                  tabData.superCategories.push(mItem.category.superCategory);
                  /* if(_.has( item,'limit')) {
                   tempOfferItem.offer={
                   type:'applied',
                   _id:offer.offer._id,
                   name:offer.offer.name
                   }
                   tempOfferItem.discounts = [];
                   var discountOb = {
                   type: offer.offer.type.name == 'percent' ? 'percentage' : offer.offer.type.name,
                   value: offer.offer.type.value,
                   comment: null,
                   offerName: offer.offer.name,
                   itemComment: null//Utils.roundNumber( ((item.discountType=='percent')?(tempItem.rate*item.value*.01):item.value))
                   }
                   tempOfferItem.isDiscounted = true;
                   tempOfferItem.discounts.push(discountOb);
                   tabOfferData.Items.push(angular.copy(tempOfferItem));
                   tabOfferData.categories.push(angular.copy( mItem.category));
                   tabOfferData.superCategories.push(angular.copy( mItem.category.superCategory));
                   }*/

                }
              });
            }else{
              //tabData.Items.push(item.item);
              var _fSCat= _.filter(rootMasterItems,{'_id':item.item._id});
              if(_fSCat.length>0){
                tabData.superCategories.push(_fSCat[0].category.superCategory);
                tabData.categories.push(item.item.category);
                item.item.category.superCategory=_fSCat[0].category.superCategory;
                var tempItem=angular.copy(item.item);
                // var tempOfferItem=angular.copy(item.item);
                /*To Apply Direct Offer On Applicable Item*/
                tempItem.offer={
                  type:'applicable',
                  _id:offer.offer._id,
                  name:offer.offer.name
                }
                //tabData.Items.push(item.item);
                tabData.Items.push(tempItem);
                console.log(item);

                /*   if(_.has( item,'limit')) {
                 tempOfferItem.offer={
                 type:'applied',
                 _id:offer.offer._id,
                 name:offer.offer.name
                 }
                 tempOfferItem.discounts = [];
                 var discountOb = {
                 type: offer.offer.type.name == 'percent' ? 'percentage' : offer.offer.type.name,
                 value: offer.offer.type.value,
                 comment: null,
                 offerName: offer.offer.name,
                 itemComment: null//Utils.roundNumber( ((item.discountType=='percent')?(tempItem.rate*item.value*.01):item.value))
                 }
                 tempOfferItem.isDiscounted = true;
                 tempOfferItem.discounts.push(discountOb);
                 tabOfferData.Items.push(angular.copy(tempOfferItem));
                 tabOfferData.superCategories.push(_fSCat[0].category.superCategory);
                 tabOfferData.categories.push(item.item.category);
                 }*/
              }else{
                //tabData.Items.push(item.item);
              }
            }
          });
          var _uCats = _.uniq(tabData.categories, 'categoryName');
          tabData.categories = _uCats;
          tabData.categories = _.sortBy(tabData.categories, ['categoryName']);
          if(tabData.superCategories.length>0) {
            var _uSCats = _.uniq(tabData.superCategories, 'superCategoryName');
            tabData.superCategories = _uSCats;
            tabData.superCategories = _.sortBy(tabData.superCategories, ['superCategoryName']);
          }
          tabData.Items=_.sortBy(tabData.Items, ['name']);
          offerItems.applicable.push(tabData);

          /*Applied items data push*/
          var tabData= {tabId:$scope.selectedTab.tabId , Items:[],categories:[],superCategories:[] };
          _.forEach(offer.offer.offerApplied.items,function(item){
            //console.log($rootScope.rootMasterItems);
            if(item.item==null||item.item==""){
              _.forEach(rootMasterItems,function(mItem){
                if((mItem.category._id==item.category._id)||(item.category==null||item.category=='')) {
                  var tempItem=angular.copy(mItem);
                  tempItem.offer={
                    type:'applied',
                    _id:offer.offer._id,
                    name:offer.offer.name
                  }
                  tempItem.discounts=[];
                  var discountOb={
                    type:item.discountType=='percent'?'percentage':'fixed',
                    value:item.value,
                    comment:null,
                    offerName:offer.offer.name,
                    itemComment:null//Utils.roundNumber( ((item.discountType=='percent')?(tempItem.rate*item.value*.01):item.value))
                  }
                  tempItem.isDiscounted=true;
                  tempItem.discounts.push(discountOb);
                  tabData.Items.push(tempItem);
                  tabData.categories.push(mItem.category);
                  tabData.superCategories.push(mItem.category.superCategory);
                }
              });
            }else{
              //tabData.Items.push(item.item);

              var _fSCat= _.filter(rootMasterItems,{'_id':item.item._id});
              if(_fSCat.length>0){
                var tempItem=angular.copy(_fSCat[0]);
                tempItem.offer={
                  type:'applied',
                  _id:offer.offer._id,
                  name:offer.offer.name
                }

                tempItem.discounts=[];
                var discountOb={
                  type:item.discountType=='percent'?'percentage':'fixed',
                  value:item.value,
                  comment:null,
                  offerName:offer.offer.name,
                  itemComment:null
                  //Utils.roundNumber( ((item.discountType=='percent')?(_fSCat[0].rate*item.value*.01):item.value))
                }
                tempItem.isDiscounted=true;
                tempItem.discounts.push(discountOb);
                tabData.Items.push(tempItem);

                tabData.superCategories.push(_fSCat[0].category.superCategory);
                item.item.category.superCategory=_fSCat[0].category.superCategory;
                tabData.categories.push(item.item.category);

              }else{
                // tabData.Items.push(item.item);
              }
            }
          });
          var _uCats = _.uniq(tabData.categories, 'categoryName');
          tabData.categories = _uCats;
          tabData.categories = _.sortBy(tabData.categories, ['categoryName']);
          if(tabData.superCategories.length>0) {
            var _uSCats = _.uniq(tabData.superCategories, 'superCategoryName');
            tabData.superCategories = _uSCats;
            tabData.superCategories = _.sortBy(tabData.superCategories, ['superCategoryName']);
          }
          tabData.Items=_.sortBy(tabData.Items, ['name']);
          if(tabData.Items.length>0) {
            offerItems.appliedItems.push(tabData);
          }
          /* if(tabOfferData.Items.length>0){
           var _uOCats = _.uniq(tabOfferData.categories, 'categoryName');
           tabOfferData.categories = _uOCats;
           tabOfferData.categories = _.sortBy(tabOfferData.categories, ['categoryName']);
           if(tabOfferData.superCategories.length>0) {
           var _uOSCats = _.uniq(tabOfferData.superCategories, 'superCategoryName');
           tabOfferData.superCategories = _uOSCats;
           tabOfferData.superCategories = _.sortBy(tabOfferData.superCategories, ['superCategoryName']);
           }
           tabOfferData.Items=_.sortBy(tabOfferData.Items, ['name']);
           offerItems.appliedItems.push(tabOfferData);
           }*/

          //console.log(offerItems);
          /*End Applied Data Push*/
          var selectedOfferObject={offer:offer,offerItems:offerItems,validateOffer:{applicableItems:[],appliedItems:[]},validated:{applicable:false,offerItems:false},billedItems:{applicable:[],applied:[]}};
          $scope.selectedOffer = selectedOfferObject;
          //  console.log($scope.selectedOffer);

          if(!($scope.selectedOffer.offer.offer.minBillAmount==null||$scope.selectedOffer.offer.offer.minBillAmount=='')){
            var offerValidation={offerId:null,minBillAmount:null,minItemCount:null};
            if($scope.bill.offerItemValidation.length==0){
              offerValidation.offerId=$scope.selectedOffer.offer.offer._id;
              offerValidation.minBillAmount=$scope.selectedOffer.offer.offer.minBillAmount;
              $scope.bill.offerItemValidation.push(offerValidation);
            }
            for(var i=0;i<$scope.bill.offerItemValidation.length;i++ ){
              if($scope.bill.offerItemValidation[i].offerId==$scope.selectedOffer.offer.offer._id){
                $scope.bill.offerItemValidation[i].minBillAmount=$scope.selectedOffer.offer.offer.minBillAmount;
              }
            }
          }
          if(!($scope.selectedOffer.offer.offer.minItemCount==null||$scope.selectedOffer.offer.offer.minItemCount=='')){
            var offerValidation={offerId:null,minBillAmount:null,minItemCount:null};
            if($scope.bill.offerItemValidation.length==0){
              offerValidation.offerId=$scope.selectedOffer.offer.offer._id;
              offerValidation.minItemCount=$scope.selectedOffer.offer.offer.minItemCount;
              $scope.bill.offerItemValidation.push(offerValidation);
            }
            for(var i=0;i<$scope.bill.offerItemValidation.length;i++ ){
              if($scope.bill.offerItemValidation[i].offerId==$scope.selectedOffer.offer.offer._id){
                $scope.bill.offerItemValidation[i].minItemCount=$scope.selectedOffer.offer.offer.minItemCount;
              }
            }
          }
          var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
          var showGetItem=false;
          if(_.has($scope.selectedOffer.offer.offer,'getItemOnly')){
            showGetItem=$scope.selectedOffer.offer.offer.getItemOnly;
          }
          if(_fTab.length>0){
            // $scope.renderItemsTabWise(_fTab[0],$rootScope.rootMasterItems);
            if((showGetItem)||($scope.selectedOffer.offer.offer.type.name=='comboOffer') ){
              $scope.selectedOffer.validated.applicable=true;
              if($scope.selectedOffer.offerItems.appliedItems.length>0)
              $scope.renderItemsTabWise(_fTab[0],$scope.selectedOffer.offerItems.appliedItems);
              if($scope.selectedOffer.offer.offer.type.name=='comboOffer'){
                $scope.addItemForComboOffer();
              }
            }else {
              $scope.renderItemsTabWise(_fTab[0], $scope.selectedOffer.offerItems.applicable);
            }
            // $scope.renderItemsTabWise(_fTab[0],offerItems.appliedItems);
          }
        }
    }

    $scope.addItemForComboOffer=function(){
      //console.log($scope.selectedOffer.offerItems.applicable);
      $scope.selectedOffer.offerItems.applicable[0].Items= _.sortBy($scope.selectedOffer.offerItems.applicable[0].Items,['rate']);
      $scope.selectedOffer.offerItems.applicable[0].Items=($scope.selectedOffer.offerItems.applicable[0].Items).reverse();

      var repleableItem=null;
      _.forEach($scope.selectedOffer.offerItems.applicable[0].Items,function(item){
         if(_.has(item,'isReplaceable')){
          repleableItem=item;
         }
      })
      /*if(repleableItem==null){
        var _fOfferItem=_.sortBy($scope.selectedOffer.offerItems.applicable[0].Items,['rate']);
      if(_fOfferItem.length>0){
        repleableItem=angular.copy( _fOfferItem[_fOfferItem.length-1]);
      }

      }*/
        var calculatedComboAmount=$scope.calculateComboOfferAmount($scope.selectedOffer.offerItems.applicable[0].Items);
        var balanceAmount=calculatedComboAmount;
      _.forEach($scope.selectedOffer.offerItems.applicable[0].Items,function(item){
        var appItem=getItemById(item._id);
          var amountdeduct=Utils.roundNumber( balanceAmount>item.rate?(item.rate-1):balanceAmount,2);
          appItem.offer=item.offer;
         if(repleableItem==null || repleableItem._id!=appItem._id){
          if(balanceAmount>0){
            appItem.offer.type='applied';
            appItem.discounts=[];
            var offerob={comment:null,itemComment:null,offerName:$scope.selectedOffer.offer.offer.name,type:'fixed',value:amountdeduct};
            appItem.discounts.push(offerob);
            appItem.isDiscounted=true;
            balanceAmount-=amountdeduct;
          }
          $scope.bill.addItem(appItem);
         }
      })

      if(repleableItem!=null){
          var appItem=angular.copy( getItemById(repleableItem._id));
          appItem.isReplaceable=true;
          var amountdeduct= balanceAmount>repleableItem.rate?repleableItem.rate:balanceAmount;
          appItem.offer=repleableItem.offer;
        // if(repleableItem._id!=appItem._id){
          if(balanceAmount>0){
            appItem.offer.type='applied';
            appItem.discounts=[];
            var offerob={comment:null,itemComment:null,offerName:$scope.selectedOffer.offer.offer.name,type:'fixed',value:amountdeduct};
            appItem.discounts.push(offerob);
            appItem.isDiscounted=true;
            balanceAmount-=amountdeduct;
          }
          console.log(appItem);
          $scope.bill.addItem(appItem);
        // }
       }
      $scope.bill.processBill();
      if($scope.selectedOffer.offer.offer.offerApplied.items.length==0){
        $scope.selectedOffer=null;
        $scope.resetBillItems();

      }
       $scope.handleBill();
    }

    $scope.calculateComboOfferAmount=function(items){
      var tAmount=0;
      _.forEach(items,function(item){
        tAmount+=item.rate;
      })
      console.log($scope.selectedOffer);
      return tAmount-parseFloat( $scope.selectedOffer.offer.offer.comboAmount);
    }

    $scope.expressOffer=function(){
        if (Utils.hasSetting('enable_password_express',$scope.settings) ) {
          $scope.passCodeModal().then(function(res){
            if (res != Utils.getPassCode(res, $rootScope.users,'Manager,Admin')) {
              growl.error('Wrong Passcode!', {
                ttl: 2000
              });
              return false;
            }else{
              $scope.expressOfferOpen();
            }
        })
      }else{
        $scope.expressOfferOpen();
      }
    }

    $scope.expressOfferOpen=function(){
      $modal.open({
        templateUrl: 'app/billing/item/_expressOffer.html',
        /*  placement: 'bottom',
         backdrop: true,*/
        size:'md',
        controller: function($scope,$modalInstance,growl) {
          $scope.discountForm={
            type:'percentage',
            value:'',
            comment:''
          }
           $scope.apply=function(){
             if($scope.discountForm.comment==''){
                growl.error('Comment is mandatory !',{ttl:2000});
                return false;
              }

            if($scope.discountForm.type=='percentage'){
               if($scope.discountForm.value > 100){
                growl.error('Percentage geater than 100 !',{ttl:2000});
                return false;
              }
              if($scope.discountForm.value==''){
                growl.error('Amount/percentage is blank !',{ttl:2000});
                return false;
              }
             }
            //console.log($scope.discountForm);
             if(!parseFloat( $scope.discountForm.value)){
              growl.error('Amount/percentage is blank or not in number !',{ttl:2000});
             }else{
              $modalInstance.close($scope.discountForm);
            }
           }
           $scope.cancel=function(){
            $modalInstance.dismiss();
           }
        }
      }).result.then(function(discount){
        console.log(discount);
        for(var i=0;i< $scope.bill._offers.length;i++){
           if( $scope.bill._offers[i].isBillWise){
            $scope.bill._offers.splice(i,1);
           }
          }

         var isComplimentary=false;
         if(discount.type=="percentage" && parseInt( discount.value)==100){
          isComplimentary=true;
         }
         var billWiseOffer={
                            isBillWise:true,
                            time:new Date(),
                            user:$scope.getPassCodeUserPre(),
                            offer:{
                              tabs:[],
                              type: {name: (discount.type=="percentage"?"percent":discount.type),value:parseFloat( discount.value)},
                              name:'Express Offer('+ (discount.type=="percentage"?'':'Rs.')+ discount.value + (discount.type=="percentage"? '%':'' )+')',
                              complimentary:isComplimentary,
                              deployment_id:localStorageService.get('deployment_id'),
                              tenant_id:localStorageService.get('tenant_id'),
                              comment:discount.comment
                            },
                            isApplicableItem:true
                          }
                       //   alert("isComplimentary:"+isComplimentary);
        if(isComplimentary){
          $scope.bill.complimentary=true;
          $scope.bill.complimentaryComment=discount.comment;
        }else{
          $scope.bill.complimentary=false;
          $scope.bill.complimentaryComment='';
        }

        $scope.bill._offers.push(billWiseOffer);
        $scope.bill.processBill();
      })

    }

    /* Set Delivery */
    $scope.setDeliveryTime = function(bill) {
      /* var _f = _.filter($scope.bills, {
       billId: billId
       });
       if (_f.length > 0) {
       var _time = _f[0].bill.setDeliveryTime();
       // $scope.openSettleBill('lg', _f[0].bill);
       }*/
      if($rootScope.isInstance) {
        $scope.getFineBillById(bill._id).then(function (offlineBill) {
          bill = angular.copy(offlineBill.bill);
          bill.setDeliveryTime();
          $scope.updateOnlineBillStatus('dispatch',bill);
          $scope.triggerDeliverySMS(bill);
        })
      }else {
        bill.setDeliveryTime();
        $scope.updateOnlineBillStatus('dispatch', bill);
        $scope.triggerDeliverySMS(bill);
      }

    };

    /* Settle Take Out Bill */
    $scope.settleTakeOut = function(billId) {
      var _f = _.filter($scope.bills, {
        billId: billId
      });
      if (_f.length > 0) {
        $scope.openSettleBill('lg', _f[0].bill);
      }
    };
    $scope.openBill = function(billId) {
      var _f = _.filter($scope.bills, {
        billId: billId
      });
      if (_f.length > 0) {

      }
    };

    /* Void KOT */
    $scope.currentKot = {};
    $scope.currentKotIndex = 0;
    $scope.kotSettings=function(index,bill) {
      $modal.open({
        backdrop: 'static',
        keyboard: false,
        templateUrl: 'app/billing/item/_kotSettings.html',
        size: 'md',
        resolve: {
          kotIndex: function () {
            return index;
          },
          isCard:function(){
            return $scope.isCardSystem()
          }
        },
        controller: ['$scope', '$modalInstance','isCard', function ($scope, $modalInstance,isCard) {
          $scope.isCard=isCard;
          $scope.cancel=function(){
            $modalInstance.dismiss();
          }
          $scope.clickEvent=function(event){
            $modalInstance.close(event);
          }

        }]
      }).result.then(function(event){
        if(event=='void'){
          if(Utils.hasSetting('c_m_p',$scope.settings,'vItem')) {
            $scope.getManagerPermission().then(function (result) {
              $scope.voidKot(index);
            });
          }else{
            $scope.voidKot(index);
          }
        }
        if(event=='print'){
          $scope.printKotIndex(index, true)
        }
        if(event=='copy'){
          $scope.transferKot(bill,index)
        }
      })
    }
    $scope.voidKot = function(index) {
      if (Utils.hasSetting('enable_card', $scope.settings)) {
          //$scope.bill = bill;
          if($scope.bill._offers.length>0){
            growl.error('Bill wise offer applied,can\'t be voided!!',{ttl:2000});
            return false;
          }
        }
      if($scope.bill._kots[index].isVoid){
        growl.error('Already voided kot.',{ttl:2000});
        return false;
      }
      $modal.open({
        backdrop: 'static',
        keyboard: false,
        templateUrl: 'app/billing/item/_comment1.html',
        controller: function ($scope, $modalInstance) {
          $scope.comment = "";
          $scope.addComment = function () {
            $modalInstance.close($scope.comment);
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        },
        size: 'sm'
      }).result.then(function (comment) {
        if (Utils.hasSetting('enable_card', $scope.settings)) {
          //$scope.bill = bill;
          if($scope.bill._offers.length>0){
            growl.error('Bill wise offer applied can\'t be voided',{ttl:2000});
            return false;
          }
          $modal.open({
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'app/billing/_cardBillDeleteItem.html',
            controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'bill', 'localStorageService', '$http', 'CardTransaction', '$timeout', 'cardId', function ($rootScope, $scope, $resource, $modalInstance, bill, localStorageService, $http, CardTransaction, $timeout, cardId) {
              console.log(cardId);
              $scope.setFocus = {focusInput: false};
              $timeout(function () {
                $scope.setFocus.focusInput = true;
              }, 200)
              $scope.cardBill = bill;
              $scope.selectedCardId = '';
              $scope.cardDetail = {};
              $scope.cardEntered = function () {
                console.log($scope.cardId);
                if ($scope.cardId != cardId) {
                  growl.error('Card id not matched !!', {ttl: 2000});
                  $scope.cardId = '';
                  return false;
                }
                if ($scope.inProcess == true) {
                  growl.error('Let us finish the last work', {ttl: 1000});
                  return false
                }
                $scope.inProcess = true;
                $http.post($rootScope.url + '/api/synckots/checkCardIfExist', {
                  cardId: $scope.cardId,
                  deployment_id: localStorageService.get('deployment_id')
                }).then(function (result) {
                  console.log(result);
                  $scope.selectedCardId = angular.copy($scope.cardId);
                  $scope.cardDetail.balance = result.data.balance;
                  $scope.cardDetail.cardId = $scope.selectedCardId;
                  $scope.inProcess = false;
                }, function (err) {
                  if (err.data.err == 'na') {
                    growl.error('CardId does not exist !!', {ttl: 3000});
                  } else {
                    growl.error('There is problem accessing the server !!', {ttl: 3000});
                  }
                  $scope.inProcess = false;
                })
              }
              $scope.changeCard = function () {
                $scope.selectedCardId = '';
                $scope.cardId = '';
                $scope.cardDetail = {};
                $scope.setFocus.focusInput = false;
                $timeout(function () {
                  $scope.setFocus.focusInput = true;
                }, 200)
              }
              $scope.cancel = function () {
                $modalInstance.close();
              }
              $scope.submit = function () {

                var cardTransaction = new CardTransaction();
                cardTransaction.cardId = $scope.selectedCardId;
                cardTransaction.type = 'voidKotTransaction';
                cardTransaction.amount = (bill.netAmount);
                cardTransaction.itemDetail = [];
                $modalInstance.close(cardTransaction);
              }
            }],
            size: 'md',
            resolve: {
              bill: function () {
                return {netAmount: Utils.roundNumber( ($scope.bill._kots[index].totalAmount + $scope.bill._kots[index].taxAmount - $scope.bill._kots[index].totalDiscount),($rootScope.disable_roundoff?2:0))};
              },
              cardId: function () {
                return ((_.has($scope.bill._kots[index],'cardTransaction'))? $scope.bill._kots[index].cardTransaction.cardId:'')
              }
            }
          }).result.then(function (ct) {
            console.log(new CardTransaction());
            if (ct) {
              ct.deployment_id = localStorageService.get('deployment_id');
              ct.user = {name: $scope.getPassCodeUserPre(), userId: currentUser._id};
              ct.billDetail = {
                kotNumber: $scope.bill._kots[index].kotNumber,
                billId: $scope.bill._id,
                serialNumber: $scope.bill.serialNumber,
                daySerialNumber: $scope.bill.daySerialNumber,
                instance: {id:$scope.bill.instance_id,preFix:$scope.bill.prefix}

              };
              ct.itemDetail=$scope.bill.flattenCardItems(angular.copy( $scope.bill._kots[index].items));
              $scope.bill.cardTransaction = ct;
              $scope.bill._kots[index].cardTransactionVoid = ct;
              //////////////////////////////////////////////Oringinal Code//////////////////////////////////////////////////
              $scope.bill._items = [];
               var username = $rootScope.isInstance ?$scope.getUserNameForNotification() : $scope.currentUser.username;
              //alert(username);
              $scope.bill.voidKot(index, comment,username).then(function (status) {
                //console.log(status);
                $scope.printCardTransaction(angular.copy($scope.bill));
                $scope.bill.cardTransaction={};
                $scope.printKotIndex(index, false, true);
                if (!$scope.isEditBill)
                  $scope.initializeTab();
              });
              var username = $scope.getUserNameForNotification();
              var str = "Voided Kot Number:" + $scope.bill._kots[index].kotNumber + " by " + username + " for customer " + ($scope.bill._customer.firstname == undefined ? "none" : $scope.bill._customer.firstname);
              $scope.offerNotification('Voided Kot', str);
              ////////////////////////////////////////////////////////////////////////////////////////////////////
            } else {
              $scope.isBillingOn = false;
              $scope.bill.cardTransaction = {};
            }
          });
        } else {
          $scope.bill._items = [];
           var username = $rootScope.isInstance ?$scope.getUserNameForNotification() : $scope.currentUser.username;

          $scope.bill.voidKot(index, comment,username).then(function (status) {
            $scope.printKotIndex(index, false, true);
            if (!$scope.isEditBill)
              $scope.initializeTab();
          });
          var username = $scope.getUserNameForNotification();
          var str = "Voided Kot Number:" + $scope.bill._kots[index].kotNumber + " by " + username + " for customer " + ($scope.bill._customer.firstname == undefined ? "none" : $scope.bill._customer.firstname);
          $scope.offerNotification('Voided Kot', str);
        }
      });
    };
      $scope.billDetailOb=function() {
        // console.log($scope.bill);
        var instanceId={id:$scope.bill.instance_id,preFix:$scope.bill.prefix};
        return {kotNumber:$scope.bill.kotNumber,serialNumber:$scope.bill.serialNumber,daySerialNumber:$scope.bill.daySerialNumber,billId:$scope.bill._id,instance:instanceId,billOffer:$scope.bill._offers };
      }

     $scope.printMasterKot = function(bill, kotIndex, isReprint, isVoid, isDeleted, dIndex) {
            console.log("masterKot");
            if (isDeleted) {


                var billNumber = Utils.hasSetting('reset_serial_daily', localStorageService.get('settings')) ? bill.daySerialNumber : bill.serialNumber;
                //var header = Utils.getSettingValue('header_bill', settings);
                var billHtml = 'MASTER KOT';
                billHtml += '\r\n--------------------------------------\r\n';
                billHtml += 'Void KOT Item';
                billHtml += '\r\n--------------------------------------\r\n';
                console.log(bill._kots[kotIndex]);
                if (bill._tableId != null) {
                    billHtml += 'Type:' + bill.tab + '    Table Number:' + bill._tableId;
                } else {
                    billHtml += 'Type:' + bill.tab;
                }
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill.prefix != null)
                    billHtml += "Bill Number:" + bill.prefix + "-" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                else
                    billHtml += "Bill Number:" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                if (bill.tabType == 'table')
                    billHtml += 'Steward: ' + (bill._waiter.firstname == undefined ? '' : bill._waiter.firstname) + ' ' + (bill._waiter.lastname == undefined ? '' : bill._waiter.lastname) + '\r\n';
                else
                    billHtml += 'Steward:' + (bill._delivery.firstname == undefined ? '' : (bill._delivery.firstname + ' ' + bill._delivery.lastname)) + '\r\n';
                billHtml += 'Date:' + Utils.getDateFormatted(new Date());
                /* if(bill.rePrint && bill.rePrint.length>1)
                 billHtml+='\r\nRePrint Time: '+Utils.getDateFormatted(bill.rePrint[bill.rePrint.length-1].printTime);*/
                billHtml += '\r\nKot Number:' + bill.kotNumber;
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill._customer.mobile != undefined) {
                    billHtml += 'Customer Detail';
                    billHtml += '\r\n--------------------------------------\r\n';
                    billHtml += 'Name: ' + bill._customer.firstname + '\r\n';
                    billHtml += 'Add: ' + bill._customer.address1 + ',' + '\r\n';
                    billHtml += bill._customer.address2 + ',' + '\r\n';
                    billHtml += bill._customer.city + ',' + bill._customer.state + ',' + bill._customer.postCode + '\r\n';
                    billHtml += 'Mobile: ' + bill._customer.mobile;
                    billHtml += '\r\n--------------------------------------\r\n';
                }
                billHtml += $scope.setString('Item', 30) + $scope.setString('Qty', 8);
                billHtml += '\r\n--------------------------------------\r\n';
                var tqty = 0;
                var sTotal = 0;
                //for (var i = 0; i < bill._kots[kotIndex].items.length; i++) {
                billHtml += $scope.setString(bill._kots[kotIndex].deleteHistory[dIndex].name, 31) + '-' + $scope.setAmount(bill._kots[kotIndex].deleteHistory[dIndex].quantity.toString(), 2) + '\r\n';
                /*   if(bill._kots[kotIndex].items[i].addOns.length>0)
                 {
                 billHtml +='(';
                 for (var j = 0; j < bill._kots[kotIndex].items[i].addOns.length; j++)
                 {
                 billHtml += setString(bill._kots[kotIndex].items[i].addOns[j].name, 31) + ' ' + setString(bill._kots[kotIndex].items[i].addOns[j].quantity.toString(), 7);
                 if(j!=bill._kots[kotIndex].items[i].addOns.length-1)
                 billHtml+='\r\n';
                 else
                 billHtml+=')\r\n';
                 }
                 }*/
                /*if(bill._kots[kotIndex].items[i].comment!="")
                 billHtml+='('+setString(bill._kots[kotIndex].items[i].comment, 30)+')';*/
                //tqty += bill._kots[kotIndex].items[i].quantity;
                //  }
                billHtml += '--------------------------------------\r\n';
                billHtml += $scope.setString('Total Qty:', 31) + '-' + $scope.setAmount(bill._kots[kotIndex].deleteHistory[dIndex].quantity.toString(), 2) + '\r\n';
                billHtml += '--------------------------------------\r\n';
                billHtml += '\r\n\r\n';

                console.log(billHtml);


            } else {


                var billNumber = Utils.hasSetting('reset_serial_daily', localStorageService.get('settings')) ? bill.daySerialNumber : bill.serialNumber;
                //var header = Utils.getSettingValue('header_bill', settings);
                var billHtml = ' ';
                // var kotIndex = printdata.index;
                console.log(bill._kots[kotIndex]);
                billHtml += 'MASTER KOT\r\n';

                if (isVoid == true) {
                    billHtml += '--------------------------------------\r\n';
                    billHtml += 'VOID KOT\r\n';
                }
                if (isReprint == true) {
                    billHtml += '--------------------------------------\r\n';
                    billHtml += 'DUPLICATE KOT\r\n';
                }
                billHtml += '--------------------------------------\r\n';
                if (bill._tableId != null) {
                    billHtml += 'Type:' + bill.tab + '    Table Number:' + bill._tableId;
                } else {
                    billHtml += 'Type:' + bill.tab;
                }
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill.prefix != null)
                    billHtml += "Bill Number:" + bill.prefix + "-" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                else
                    billHtml += "Bill Number:" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '') + '\r\n';
                if (bill.tabType == 'table')
                    billHtml += 'Steward: ' + (bill._waiter.firstname == undefined ? '' : bill._waiter.firstname) + ' ' + (bill._waiter.lastname == undefined ? '' : bill._waiter.lastname) + '\r\n';
                else
                    billHtml += 'Steward:' + (bill._delivery.firstname == undefined ? '' : (bill._delivery.firstname + ' ' + bill._delivery.lastname)) + '\r\n';
                billHtml += 'Date:' + Utils.getDateFormatted(bill._kots[kotIndex].created)
                    /*   if(bill.rePrint && bill.rePrint.length>1)
                     billHtml+='\r\nRePrint Time: '+Utils.getDateFormatted(bill.rePrint[bill.rePrint.length-1].printTime);*/
                billHtml += '\r\nKot Number:' + bill.kotNumber;
                billHtml += '\r\n--------------------------------------\r\n';
                if (bill._customer.mobile != undefined) {
                    billHtml += 'Customer Detail';
                    billHtml += '\r\n--------------------------------------\r\n';
                    billHtml += 'Name: ' + bill._customer.firstname + '\r\n';
                    billHtml += 'Add: ' + bill._customer.address1 + ',' + '\r\n';
                    billHtml += bill._customer.address2 + ',' + '\r\n';
                    billHtml += bill._customer.city + ',' + bill._customer.state + ',' + bill._customer.postCode + '\r\n';
                    billHtml += 'Mobile: ' + bill._customer.mobile;
                    billHtml += '\r\n--------------------------------------\r\n';
                }
                billHtml += $scope.setString('Item', 30) + $scope.setString('Qty', 8);
                billHtml += '\r\n--------------------------------------\r\n';
                var tqty = 0;
                var sTotal = 0;
                for (var i = 0; i < bill._kots[kotIndex].items.length; i++) {
                    if (bill._kots[kotIndex].isVoid != true)
                        billHtml += $scope.setString(bill._kots[kotIndex].items[i].name, 32) + ' ' + $scope.setString(bill._kots[kotIndex].items[i].quantity.toString(), 7) + '\r\n';
                    else
                        billHtml += $scope.setString(bill._kots[kotIndex].items[i].name, 32) + ' -' + $scope.setString(bill._kots[kotIndex].items[i].quantity.toString(), 7) + '\r\n';
                    if (bill._kots[kotIndex].items[i].addOns != undefined && bill._kots[kotIndex].items[i].addOns.length > 0) {
                        billHtml += '(';
                        for (var j = 0; j < bill._kots[kotIndex].items[i].addOns.length; j++) {
                            if (bill._kots[kotIndex].isVoid != true)
                                billHtml += bill._kots[kotIndex].items[i].addOns[j].name + ':' + bill._kots[kotIndex].items[i].addOns[j].quantity + ' ';
                            //tqty += bill._kots[kotIndex].items[i].addOns[j].quantity;
                        }
                        billHtml += ')\r\n'
                    }
                    if (bill._kots[kotIndex].items[i].mapComboItems != undefined && bill._kots[kotIndex].items[i].mapComboItems.length > 0) {
                        billHtml += '(';
                        for (var j = 0; j < bill._kots[kotIndex].items[i].mapComboItems.length; j++) {
                            billHtml += bill._kots[kotIndex].items[i].mapComboItems[j].name + ' , ';
                            //tqty += bill._kots[kotIndex].items[i].addOns[j].quantity;
                        }
                        billHtml += ')\r\n'
                    }
                    if (bill._kots[kotIndex].items[i].comment != undefined && bill._kots[kotIndex].items[i].comment != "") {
                        billHtml += '(' + bill._kots[kotIndex].items[i].comment + ')';
                        billHtml += '\r\n';
                    }
                    tqty += bill._kots[kotIndex].items[i].quantity;
                }
                billHtml += '--------------------------------------\r\n';
                if (bill._kots[kotIndex].isVoid != true)
                    billHtml += $scope.setString('Total Qty:', 31) + $scope.setAmount(tqty.toString(), 3) + '\r\n';
                else
                    billHtml += $scope.setString('Total Qty:', 31) + '-' + $scope.setAmount(tqty.toString(), 2) + '\r\n';
                billHtml += '--------------------------------------\r\n';
                if (bill._kots[kotIndex].comment) {
                    billHtml += 'Kot Comment:' + bill._kots[kotIndex].comment + '\r\n';
                }
                billHtml += '\r\n\r\n';
            }
            //console.log(billHtml);

            printer.print('dynamicTemplateMasterKot', billHtml, 'dynamicPrint');
            //console.log(bill);


        }



    /* Print KOT Index */
    $scope.printKotIndex = function(index, isReprint, isVoid) {
      if(!$scope.isPermission('allow_print_kot')){
        growl.error("You don't have permission !!",{ttl:3000});
        return false;
      }

      if($scope.isEditBill){
        return false;
      }
      if (Utils.hasSetting('enable_passcode',$scope.settings,'reprintkot') && (isReprint)) {
        $scope.passCodeModal().then(function(res){
          if (res != Utils.getPassCode(res, $rootScope.users,'Manager,Admin')) {
            growl.error('Wrong Passcode!', {
              ttl: 2000
            });
            return false;
          }else{
            $scope.setTempManagerName(res);
          }
          $scope.currentKotIndex = index;
          printer.print('_template_KOT.html', {
            bill:angular.copy( $scope.bill),
            index: $scope.currentKotIndex,
            isReprint: isReprint,
            isVoid: isVoid
          });

          $scope.checkAndPrintMasterPrintForKot(angular.copy( $scope.bill),angular.copy($scope.currentKotIndex),isReprint,isVoid,false,0);
          var username=$scope.getUserNameForNotification();
          var str="Re printed kot number:#"+ ($scope.bill._kots[index].kotNumber )+" by "+ username +" for customer "+($scope.bill._customer.firstname==undefined?"none":$scope.bill._customer.firstname) ;
         // alert(str);
          $scope.offerNotification('Re-Printed Kot',str);
        },function(err){
          //alert(err);
        })
      }else {
        //alert(Utils.hasSetting('enable_passcode',$scope.settings,'reprintbill'));
        //return false;
        $scope.currentKotIndex = index;
        printer.print('_template_KOT.html', {
          bill: angular.copy( $scope.bill),
          index: $scope.currentKotIndex,
          isReprint: isReprint,
          isVoid: isVoid
        });

        $scope.checkAndPrintMasterPrintForKot(angular.copy( $scope.bill),angular.copy($scope.currentKotIndex),isReprint,isVoid,false,0);

      }
    };

    $scope.checkAndPrintMasterPrintForKot = function(bill, kotIndex, isReprint, isVoid, isDeleted, dIndex) {
        if (!Utils.hasSetting('station_wise_print', $scope.settings)) {
            return false; }
        var mset = localStorageService.get('masterStation');
        var flag = false;
        console.log(mset);
        if (!(mset == null || mset == undefined)) {
            if (mset.length > 0) {
                flag = true;
            }
        }
        //console.log(flag);
        if (flag)
            $scope.printMasterKot(bill, kotIndex, isReprint, isVoid, isDeleted, dIndex);

    }


    /* Add Item Comment */
    $scope.openItemComment = function(itemIndex) {
      $modal.open({
        templateUrl: 'app/billing/item/_comment1.html',
        resolve: {
          comment: function() {
            return $scope.bill._items[itemIndex].comment;
          }
        },
        controller: function($scope, $modalInstance, comment) {
          $scope.comment = comment;
          $scope.addComment = function() {
            $modalInstance.close($scope.comment);
          };
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
        },
        size: 'sm'
      }).result.then(function(comment) {
        $scope.bill._items[itemIndex].comment = comment;
      });
    };

    function sendKotToPrinter() {
      // if(isnewitems){
      if ((Utils.hasSetting('print_kot_bill', $scope.settings))) {
        printer.print('_template_KOT.html', {
          bill: angular.copy( $scope.bill),
          index: $scope.currentKotIndex
        });
        $scope.checkAndPrintMasterPrintForKot(angular.copy( $scope.bill),angular.copy($scope.currentKotIndex),false,false,false,0);

      }
      //}
    };

    function updateInsertBillPrint(comment, isnewitems) {
      var isreprint = true;
      var copiedBill=(angular.copy( $scope.bill));
      var isAdvance= $scope.identifyAdvance(copiedBill);
      var id=copiedBill._id;
      if(isAdvance){$scope.bill.advance.id=id,$scope.bill._id=Utils.guid();};

      if (!Utils.hasSetting('reprint_bill', $scope.settings)) {
        // console.log($scope.bill);
        // return false;
        isreprint = ($scope.bill.isPrinted) ? false : true;
      }

      $scope.bill.checkBillExists($scope.bill._id).then(function(len) {
        if (!len) {
          $scope.bill.updateBill({
            isPrinted: true
          }, 'insert').then(function() {

            if (isnewitems)
              sendKotToPrinter();
            //printer.print('_template_Bill.html', $scope.billToPrint);
               if (Utils.hasSetting('bill_print_template', $scope.settings)) {
                    var bill_print_template = parseInt(Utils.getSettingValue('bill_print_template', $scope.settings))
                    if(bill_print_template==1)
                    BillPrintService.templateBillPrintWithTaxBaseAmount(angular.copy($scope.billToPrint));
                    else if(bill_print_template==2 && Utils.hasSetting('inclusive_tax', $scope.settings))
                    BillPrintService.templateBillPrintInclusiveTaxes(angular.copy($scope.billToPrint));
                    else
                    printer.print('_template_Bill.html', $scope.billToPrint);
                }
            else{
                  printer.print('_template_Bill.html', $scope.billToPrint);
              }
            $scope.triggerDispatchSMS($scope.billToPrint);
            $scope.updateOnlineBillStatus('bill');
          }).then(function() {
            $scope.isBillingOn=false;
            $scope.isOnlineBill=false;
            refreshState();
            $scope.getDaySale();
            if(isAdvance){$scope.deleteAdvanceBill(id);};
            /*Feedback.createBill({billId:copiedBill.id,billDate:copiedBill}).$promise.then(function(){

             })*/
            $scope.bill.cardTransaction={};
            $scope.aggregateDataForFeedback($scope.bill);
          });
        } else {
          $scope.bill.updateBill({
            isPrinted: true
          }, 'update').then(function() {
            if (isnewitems)
              sendKotToPrinter();
            //    console.log($scope.billToPrint);
            if (!isreprint) {
              growl.error("Reprinting bill is not allowed!", {
                ttl: 2000
              });
              $scope.isBillingOn=false;
              $scope.isOnlineBill=false;
              refreshState();
              return false;
            }
            //console.log($scope.billToPrint);
            //printer.print('_template_Bill.html', $scope.billToPrint);
               if (Utils.hasSetting('bill_print_template', $scope.settings)) {
                    var bill_print_template = parseInt(Utils.getSettingValue('bill_print_template', $scope.settings))
                    if(bill_print_template==1)
                    BillPrintService.templateBillPrintWithTaxBaseAmount(angular.copy($scope.billToPrint));
                    else if(bill_print_template==2 && Utils.hasSetting('inclusive_tax', $scope.settings))
                    BillPrintService.templateBillPrintInclusiveTaxes(angular.copy($scope.billToPrint));
                    else
                    printer.print('_template_Bill.html', $scope.billToPrint);
                }
            else{
                 printer.print('_template_Bill.html', $scope.billToPrint);
              }
            $scope.triggerDispatchSMS($scope.billToPrint);
            $scope.updateOnlineBillStatus('bill');
          }).then(function() {
            $scope.bill.cardTransaction={};
            $scope.isBillingOn=false;
            $scope.isOnlineBill=false;
            refreshState();
            $scope.getDaySale();
            if(isAdvance){$scope.deleteAdvanceBill(id);};
            $scope.aggregateDataForFeedback($scope.bill);
          });
        }
      });
    };



    $scope.aggregateDataForFeedback=function(bill){
      if(!Utils.hasSetting('enable_feedback',$scope.settings)){
        return false;
      }
      var items=[];
      _.forEach(bill._kots,function(kot){
          if(kot.isVoid==false){
          _.forEach(kot.items,function(item){
            items.push({name:item.name,rate:item.rate,quantity:item.quantity});
          })
        }
      });
      var code = (Math.random().toString(36) + '00000000000000000').slice(2, 6 + 2);
      var feedBackData = {
        billId:bill._id,
        items:items,
        serialNumber:bill.serialNumber,
        daySerialNumber:bill.daySerialNumber,
        splitBillNumber:bill.splitNumber,
        billDate:bill._created,
        deployment_id:bill.deployment_id,
        customer:bill._customer,
        code:{name:code,created:new Date()}
      };

      //added by rajat for tracking url
       //if($scope.tp_integrated && bill.tabType=='delivery')
       //feedBackData.trackingUrl='http://192.168.1.19/'+code;
       if($scope.zomato_integrated && bill._delivery && bill._delivery.trackingUrl && bil.tabtype=='delivery')
       feedBackData.trackingUrl=bill._delivery.trackingUrl;

      Feedback.createBill({},feedBackData).$promise.then(function(res){
        growl.success('Feedback bill submitted',{ttl:3000});
      },function(){
        growl.error('There are problem submitting feedback bill. ',{ttl:3000});
      });
      //console.log(bill);
    }
    /*Consolidated function to route state after print*/
    function refreshState() {
      /* var _toState = '';
       switch ($scope.selectedTab.tabType) {
       case 'table':
       _toState = 'billing.table';
       break;
       case 'takeout':
       _toState = 'billing.takeout';
       break;
       case 'delivery':
       _toState = 'billing.delivery';
       break;
       }
       if ($scope.bill.tabType === "table") {
       delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
       $state.current.data.tables[parseInt($scope.bill._tableId) - 1]['covers'] = null;
       }
       //  if (_toState === 'billing.takeout' || _toState === 'billing.delivery') {
       $state.transitionTo('billing');
       //} else {
       //    $state.transitionTo(_toState, {tabId: $scope.tabId});
       //}*/
      $scope.initializeTab();
    };

    $scope.getOfflineBills = function() {
      Sync.syncOfflineBills($scope.selectedTab.tabId).then(function(bills) {
        $scope.bills = bills;
      });
    };
    /*Consolidated function with and without comment*/
    function updateInsertBill(comment,isCardOffer) {
      var copiedBill=(angular.copy( $scope.bill));
      var isAdvance= $scope.identifyAdvance(copiedBill);
      var id=copiedBill._id;

      if(isAdvance){$scope.bill.advance.id=id,$scope.bill._id=Utils.guid();};
      $scope.currentKotIndex = $scope.bill.printKot(comment,$scope.getKotWaiter());
      $scope.bill.checkBillExists($scope.bill._id).then(function(len) {
        if (!len) {
            $scope.bill.updateBill(null, 'insert').then(function (resultData) {
            $scope.updateOnlineBillStatus('kot');

            printer.print('_template_KOT.html', {
              bill: angular.copy($scope.bill),
              index: $scope.currentKotIndex
            });
            $scope.printCardTransaction(angular.copy($scope.bill));
            $scope.bill.cardTransaction = {};
            $scope.checkAndPrintMasterPrintForKot(angular.copy( $scope.bill),angular.copy($scope.currentKotIndex),false,false,false,0);

            $scope.handleBill();
            //////////////Offer Card ////////
            if(isCardOffer){
                  $scope.printBill();
                }
            //////////////////End Offer Card//
            /* TODO: Send SMS if Delivery Tab */
            $scope.bill.lastModified = (!(resultData == undefined || resultData == null)) ? resultData.lastModified : null;
            $scope.triggerKotSMS($scope.currentKotIndex, $scope.bill, $scope._customer);
            $scope.isBillingOn = false;
            $scope.isOnlineBill = false;
            $scope.getDaySale();
            if (isAdvance) {
              $scope.deleteAdvanceBill(id);
            }
            ;
            $scope.showAdvance = false;
            var modifiedJson = {};
            if ($scope.bill._kots.length > 0)
              modifiedJson = angular.copy($scope.bill._kots[0]);
            if (_.has($scope.bill.onlineBill.trigger, 'modifyUrl')) {
              var temp = angular.copy($scope.bill)
              console.log(temp)
              OnlineOrder.modifyUrl({
                url: $scope.bill.onlineBill.trigger.modifyUrl,
                order: modifiedJson
              }).$promise.then(function (resp) {
                //console.log(modifiedJson);
                growl.success('status updated successfully on server.', {ttl: 2000});
              }, function () {
                //growl.error('problem connecting server,try after some time.', {ttl: 2000});
              })
            }

          },function(err){
            growl.error('There is problem updating the transaction.',{ttl:3000});
            if(err.data.error=='inValid'){
              $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){
                $scope.initializeTab();
              })
            }
          });
        } else {
          $scope.bill.updateBill(null, 'update').then(function(lastModified) {
            //console.log(lastModified);
            $scope.printCardTransaction(angular.copy($scope.bill));
            $scope.bill.cardTransaction={};
            $scope.bill.lastModified=lastModified;
            $scope.updateOnlineBillStatus('kot');
            printer.print('_template_KOT.html', {
              bill:angular.copy( $scope.bill),
              index: $scope.currentKotIndex
            });
            $scope.checkAndPrintMasterPrintForKot(angular.copy( $scope.bill),angular.copy($scope.currentKotIndex),false,false,false,0);

            $scope.handleBill();
            //////////////Offer Card ////////
            if(isCardOffer){
                  $scope.printBill();
                }
            //////////////////End Offer Card//

            /* TODO: Send SMS if Delivery Tab */
            $scope.triggerKotSMS($scope.currentKotIndex,$scope.bill,$scope.customer);
            $scope.isBillingOn=false;
            $scope.isOnlineBill=false;
            $scope.getDaySale();
            if(isAdvance){$scope.deleteAdvanceBill(id);};
            $scope.showAdvance=false;
          },function(err){
            growl.error('There is problem updating the transaction.',{ttl:3000});
            if(err.data.error=='inValid'){
              $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){
                $scope.initializeTab();
              })
            }
          });
        }
      });
    };

    $scope.identifyAdvance=function(bill){
      var isAdvance=false;
      if(_.has(bill,'advance')){
        if(_.has(bill.advance,'amount')){
          if (Utils.hasSetting('print_kot_bill', $scope.settings) || Utils.hasSetting('dis_kot', $scope.settings)) {
            isAdvance=true;
          } else if(bill._kots.length==0){
            isAdvance=true;
          }
        }
      }
      return isAdvance;
    }
    $scope.deleteAdvanceBill=function(id){
      console.log(id);
      //$rootScope.selectDynamic();
      $rootScope.db.selectDynamic('delete from AdvanceBookings where AdvanceBillId="' + id + '"', []).then(function() {
        growl.success('local advance bill deleted .', {
          ttl: 2000
        });
      });
    }
    $scope.triggerKotSMS=function(kotIndex,bill,customer){
      if($scope.selectedTab.tabType=='delivery') {
        var smsSetting = Utils.getSMSSettingValue('Kot', $scope.smsSettings);
        if (_.has(smsSetting, 'isApplied')) {

          if (Utils.getSMSSettingValue('Kot', $scope.smsSettings).isApplied) {
            var items = '';
            _.forEach(bill._kots[kotIndex].items, function (item) {
              items += item.name + ':' + item.quantity + ',';
            })
            if ((_.has(bill._customer, 'mobile'))) {
              var smsObject = {
                name: (_.has(bill._customer, 'firstname')) ? bill._customer.firstname : '',
                billnumber: bill.serialNumber,
                content: items,
                templateNumber: Utils.getSMSSettingValue('Kot', $scope.smsSettings).templateNumber,
                mobile: bill._customer.mobile,
                phone: '',
                type: 'Kot',
                deployment_id: localStorageService.get('deployment_id')
              };
              console.log(smsObject);
              billResource.sendSMS(smsObject).$promise.then(function (res) {
                console.log(res);
              })
            }
          }
        }
      }
    }
    $scope.triggerDispatchSMS=function(billToPrint) {
      if ($scope.selectedTab.tabType == 'delivery') {
        var smsSetting = Utils.getSMSSettingValue('Dispatch', $scope.smsSettings);
        if (_.has(smsSetting, 'isApplied')) {
          if (Utils.getSMSSettingValue('Dispatch', $scope.smsSettings).isApplied) {
            if (_.has(billToPrint.bill._customer, 'mobile')) {
              var smsObject = {
                name: (_.has(billToPrint.bill._customer, 'firstname')) ? billToPrint.bill._customer.firstname : '',
                billnumber: billToPrint.bill.serialNumber,
                // content: bill._kots[kotIndex].items,
                templateNumber: Utils.getSMSSettingValue('Dispatch', $scope.smsSettings).templateNumber,
                deliveryBoy: (_.has(billToPrint.bill._delivery, 'firstname')) ? billToPrint.bill._delivery.firstname : '',
                amount: Utils.roundNumber((billToPrint.totalAmount + billToPrint.totalTax - billToPrint.totalDiscount), ($rootScope.disable_roundoff?2:0)),
                mobile: billToPrint.bill._customer.mobile,
                type: 'Dispatch',
                deployment_id: localStorageService.get('deployment_id')
              };
              console.log(smsObject);
              billResource.sendSMS(smsObject).$promise.then(function (res) {
                // console.log(res);
              })
            }
          }
        }
      }
    }

    $scope.triggerDeliverySMS=function(bill){
      if($scope.selectedTab.tabType=='delivery') {
        var smsSetting = Utils.getSMSSettingValue('Delivery', $scope.smsSettings);
        if (_.has(smsSetting, 'isApplied')) {
          if (Utils.getSMSSettingValue('Delivery', $scope.smsSettings).isApplied) {
            if (_.has(bill._customer, 'mobile')) {
              var smsObject = {
                name: (_.has(bill._customer, 'firstname')) ? bill._customer.firstname : '',
                billnumber: bill.serialNumber,
                // content: bill._kots[kotIndex].items,
                templateNumber: Utils.getSMSSettingValue('Delivery', $scope.smsSettings).templateNumber,
                amount: Utils.roundNumber((bill.getTotalBill() + bill.getKotsTotalBill()), 0),
                mobile: bill._customer.mobile,
                type: 'Delivery',
                deployment_id: localStorageService.get('deployment_id')
              };
              console.log(smsObject);
              billResource.sendSMS(smsObject).$promise.then(function (res) {
                // console.log(res);
              })
            }
          }
        }
      }
    }

    function updateInsertDeletedItem(comment, index, indexOfDeletedItem,bill) {
      /*   $scope.currentKotIndex = $scope.bill.printKot(comment);*/

      if (Utils.hasSetting('enable_card', $scope.settings)) {
        //$scope.bill = bill;
        console.log(bill);
        $modal.open({
          backdrop: 'static',
          keyboard: false,
          templateUrl: 'app/billing/_cardBillDeleteItem.html',
          controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'bill', 'localStorageService', '$http','CardTransaction','$timeout','cardId', function ($rootScope, $scope, $resource, $modalInstance, bill, localStorageService, $http,CardTransaction,$timeout,cardId) {
            console.log(cardId);
            $scope.setFocus={focusInput:false};
            $timeout(function(){
              $scope.setFocus.focusInput=true;
            },200)
            $scope.cardBill = bill;
            $scope.selectedCardId = '';
            $scope.cardDetail = {};
            $scope.cardEntered = function () {
              console.log($scope.cardId);
              if($scope.cardId!=cardId){
                growl.error('Card id not matched !!',{ttl:2000});
                $scope.cardId='';
                return false;
              }
              if ($scope.inProcess == true) {
                growl.error('Let us finish the last work', {ttl: 1000});
                return false
              }
              $scope.inProcess = true;
              $http.post($rootScope.url + '/api/synckots/checkCardIfExist', {
                cardId: $scope.cardId,
                deployment_id: localStorageService.get('deployment_id')
              }).then(function (result) {
                console.log(result);

                $scope.selectedCardId = angular.copy($scope.cardId);
                $scope.cardDetail.balance = result.data.balance;
                $scope.cardDetail.cardId = $scope.selectedCardId;
                $scope.inProcess = false;
              }, function (err) {
                if (err.data.err == 'na') {
                  growl.error('CardId does not exist !!', {ttl: 3000});
                } else {
                  growl.error('There is problem accessing the server !!', {ttl: 3000});
                }
                $scope.inProcess = false;
              })
            }
            $scope.changeCard = function () {
              $scope.selectedCardId = '';
              $scope.cardId = '';
              $scope.cardDetail = {};
              $scope.setFocus.focusInput=false;
              $timeout(function(){
                $scope.setFocus.focusInput=true;
              },200)
            }
            $scope.cancel = function () {
              $modalInstance.close();
            }
            $scope.submit = function () {

              var cardTransaction=new CardTransaction();
              cardTransaction.cardId=$scope.selectedCardId;
              cardTransaction.type='itemDeleteTransaction';
              cardTransaction.amount=(bill.netAmount);
              cardTransaction.itemDetail=bill;
              //  cardTransaction.billDetail=$scope.billDetailOb();
              $modalInstance.close(cardTransaction);
            }
          }],
          size: 'md',
          resolve: {
            bill: function () {
              return $scope.bill.cardItemsDeleteCalculation(bill._kots[index].deleteHistory[indexOfDeletedItem]);
            },
            cardId:function(){
              return  ((_.has(bill._kots[index],'cardTransaction'))? bill._kots[index].cardTransaction.cardId:'')
            }
          }
        }).result.then(function (ct) {
          console.log(new CardTransaction());

          if (ct) {
            $scope.bill = bill;
            ct.deployment_id=localStorageService.get('deployment_id');
            ct.user={name: $scope.getPassCodeUserPre(), userId: currentUser._id};
            ct.billDetail={kotNumber:bill._kots[index].kotNumber,billId:bill._id,serialNumber:bill.serialNumber,daySerialNumber:bill.daySerialNumber,instance:{id:bill.instance_id,preFix:bill.prefix}};
            $scope.bill.cardTransaction = ct;
            $scope.bill._kots[index].deleteHistory[indexOfDeletedItem].cardTransaction=ct;
            $scope.updateInsertDeletedItemCard(comment, index, indexOfDeletedItem,$scope.bill);
          } else {
            $scope.isBillingOn=false;
            $scope.bill.cardTransaction = {};
          }
        });

      }
      else {
        $scope.bill = bill;
        $scope.updateInsertDeletedItemCard(comment, index, indexOfDeletedItem,$scope.bill);
      }
    };

    $scope.updateInsertDeletedItemCard=function(comment, index, indexOfDeletedItem,bill) {
      //$scope.bill = bill;
      $scope.bill.isPrinted = false;
      $scope.bill.checkBillExists($scope.bill._id).then(function (len) {
        if (!len) {
          $scope.bill.updateBill(null, 'insert').then(function (result) {
            console.log(result);
            printer.print('_template_KOT_Deleted.html', {
              bill: angular.copy($scope.bill),
              index: index,
              dIndex: indexOfDeletedItem
            });
            $scope.printCardTransaction(angular.copy($scope.bill));
            $scope.bill.cardTransaction={};
            $scope.checkAndPrintMasterPrintForKot(angular.copy($scope.bill), angular.copy(index), false, false, true, indexOfDeletedItem);
            $scope.handleBill();
            /* TODO: Send SMS if Delivery Tab */
          });
        } else {
          $scope.bill.updateBill({
            isPrinted: false
          }, 'update').then(function (result) {
            console.log(result);
            $scope.printCardTransaction(angular.copy($scope.bill));
            $scope.bill.cardTransaction={};
            $scope.bill.lastModified=result;
            printer.print('_template_KOT_Deleted.html', {
              /* bill: $scope.bill,
               index: $scope.currentKotIndex*/
              bill: angular.copy($scope.bill),
              index: index,
              dIndex: indexOfDeletedItem
            });
            $scope.checkAndPrintMasterPrintForKot(angular.copy($scope.bill), angular.copy(index), false, false, true,  indexOfDeletedItem);

            $scope.handleBill();
            /* TODO: Send SMS if Delivery Tab */

          }, function (err) {
            growl.error('There is problem updating the transaction.', {ttl: 3000});
            if (err.data.error == 'inValid') {
              $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function () {
                $scope.initializeTab();
              })
            }
          })
        }
      });
    }
    /* Billing process handle*/
    $scope.billStatus = '';
    $scope.isBillingOn=false;
    $scope.doBilling = function() {

      /*Need to refres copied bill, item,kot*/
      // var cdate = new Date($rootScope.offlineBill.lastCreated);
      // var cdate1 = new Date();
      if(!($rootScope.offlineBill.lastCreated==null||$rootScope.offlineBill.lastCreated=='undefined')) {
        if (Utils.getDateFormattedHrs($rootScope.offlineBill.lastCreated) > Utils.getDateFormattedHrs(new Date())) {
          //if(!navigator.userAgent.match(/(iPad|iPhone|iPod)/)) {
            $ngBootbox.alert('It appears that the system date has been changed. Please set the system date first.');
            return false;
          //}
        }
      }

      // console.log($scope.selectedOffer);
      var offerItemValidation={minBillAmount:0,minItemCount:0};

      if($scope.bill.offerItemValidation.length>0){
        var _minAmount=_.max($scope.bill.offerItemValidation,'minBillAmount');
        offerItemValidation.minBillAmount = _minAmount.minBillAmount==null?0: _minAmount.minBillAmount;
        var _minCount=_.max($scope.bill.offerItemValidation,'minItemCount');
        offerItemValidation.minItemCount=_minCount.minItemCount==null?0: _minCount.minItemCount;
      }
      // console.log(offerItemValidation);
      if(offerItemValidation.minBillAmount>0){
        if(offerItemValidation.minBillAmount>($scope.bill.getSubtotal()+ $scope.bill.getKotsTotalAmount())){
          growl.error('Offer min amount should be more than '+offerItemValidation.minBillAmount,{ttl:2000});
          return false;
        }
      }
      if(offerItemValidation.minItemCount>0){
        if(offerItemValidation.minItemCount>($scope.bill.getGTotalQuantity())){
          growl.error('Offer min item count should be more than '+offerItemValidation.minBillAmount,{ttl:2000});
          return false;
        }
      }
      var flagOfferSelected=false;
      if($scope.selectedOffer!=null){
        if(!$scope.selectedOffer.isLeastOfferApplied){
          flagOfferSelected=true;
        }else{
          $scope.offerAppliedNotification(angular.copy($scope.selectedOffer.offer.offer),true);
           growl.success('Your offer applied successfully.',{ttl:2000});
           $scope.selectedOffer=null;
            $scope.resetBillItems();
        }
      }
      //if($scope.selectedOffer!=null){
        if(flagOfferSelected){
        $scope.showBootBox('Offer not completed .Discard Offer before print?').then(function(status){
          if(status){
            $scope.discardOfferItem($scope.selectedOffer.offer.offer._id);
            $scope.selectedOffer=null;
            $scope.resetBillItems();
            return false;
          }
        })
      }else {

        $scope.isBillingOn=true;
        $scope.getDaySale();
        if ($scope.code != null) {
          var codeOb = {
            offerName: $scope.code.offer.name,
            offer_id: $scope.code.offer._id,
            deployment_id: localStorageService.get('deployment_id'),
            offerCode: $scope.code.offerCode,
            userName: $scope.getPassCodeUserPre()
          };
          CodeApplied.create(codeOb).$promise.then(function (code) {
              console.log(code);
              $scope.code=null;
              if ($scope.billStatus == 'PrintKot') {
                $scope.bill.isPrinted = false;
                $scope.printKot();
              }
              if ($scope.billStatus == 'PrintBill') {
                $scope.printBill();
              }
              if ($scope.billStatus == 'Settle') {
                $scope.openSettleBill('lg', $scope.bill)
              }
            }
          )
        } else {
          //alert('ranjeet',$scope.billStatus);
          //console.log($scope.billStatus);
          //$scope.showPrintButton = false;
          if ($scope.billStatus == 'PrintKot') {
            $scope.bill.isPrinted = false;
            $scope.printKot();
          }
          if ($scope.billStatus == 'PrintBill') {
            $scope.printBill();
          }
          if ($scope.billStatus == 'Settle') {
            $scope.openSettleBill('lg', $scope.bill)
          }
        }
      }

    };


    $scope.handleBill = function() {
      var showKot = (Utils.hasSetting('dis_kot', $scope.settings)) ? false : (Utils.hasSetting('print_kot_bill', $scope.settings)) ? false : true;
      //console.log(showKot);
      if (($scope.bill._items.length > 0) && (showKot)) {
       // alert($scope.bill._items.length);
        $scope.billStatus = 'PrintKot';
      } else {
        if (!$scope.bill.isPrinted) {
          if (($scope.bill._kots.length > 0) || ($scope.bill._items.length > 0)) {
            $scope.billStatus = 'PrintBill';
          } else {
            $scope.billStatus = '';
          }
        } else {
          if (($scope.bill._items.length > 0) && ((Utils.hasSetting('dis_kot', $scope.settings)) || ((Utils.hasSetting('print_kot_bill', $scope.settings))))) {
            $scope.billStatus = 'PrintBill';
          } else {
            $scope.billStatus = 'Settle';
          }
        }
      }
      // $scope.isOnlineBill=false;
    };


    /* Print KOT */
    $scope.printKot = function(isCardOffer,scrapOffer) {
      if(!$scope.isPermission('allow_print_kot')){
        growl.error("You don't have permission !!",{ttl:3000});
        return false;
      }

      if (($scope.bill._items.length > 0)|| (isCardOffer)) {

        if (!Utils.hasSetting('enable_card', $scope.settings)) {
          $scope.bill.generateKotNumber().then(function (kotnumber) {
            //alert(kotnumber);
            $scope.bill.kotNumber = kotnumber;
            updateInsertBill($scope.kotComment);
            $scope.kotComment = '';
            //  alert(Utils.hasSetting('enable_comment_kot', $scope.settings));
            /* if (Utils.hasSetting('enable_comment_kot', $scope.settings)) {
             */
            /* to do validation if mandatory */
            /*
             updateInsertBill($scope.kotComment);
             $scope.kotComment = '';
             } else {
             */
            /* Don't show Comment Modal */
            /*
             updateInsertBill($scope.kotComment);
             $scope.kotComment = '';
             }*/
          });
        } else {
          $modal.open({
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'app/billing/_cardBill.html',
            controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'bill', 'localStorageService', '$http','CardTransaction','$timeout','cardId', function ($rootScope, $scope, $resource, $modalInstance, bill, localStorageService, $http,CardTransaction,$timeout,cardId) {
             console.log(cardId);
             $scope.offercardId='';
             if(!(cardId ==null|| cardId==false)){
              $scope.offercardId=cardId[0].cardId;
             }
              $scope.setFocus={focusInput:false};
              $timeout(function(){
                $scope.setFocus.focusInput=true;
              },200)
              $scope.cardBill = bill;
              $scope.selectedCardId = '';
              $scope.cardDetail = {};
              $scope.cardEntered = function () {
                console.log($scope.cardId);
                if ($scope.inProcess == true) {
                  growl.error('Let us finish the last work', {ttl: 1000});
                  return false
                }
                if($scope.offercardId!=''){
                  if($scope.offercardId!=$scope.cardId){
                    growl.error('Wrong card !!', {ttl: 1000});
                    return false;
                  }
                }
                $scope.inProcess = true;
                $http.post($rootScope.url + '/api/synckots/checkCardIfExist', {
                  cardId: $scope.cardId,
                  deployment_id: localStorageService.get('deployment_id')
                }).then(function (result) {
                  console.log(result);

                  $scope.selectedCardId = angular.copy($scope.cardId);
                  $scope.cardDetail.balance = result.data.balance;
                  $scope.cardDetail.cardId = $scope.selectedCardId;
                  $scope.inProcess = false;
                }, function (err) {
                  if (err.data.err == 'na') {
                    growl.error('CardId does not exist !!', {ttl: 3000});
                  } else {
                    growl.error('There is problem accessing the server !!', {ttl: 3000});
                  }
                  $scope.inProcess = false;
                })
              }
              $scope.changeCard = function () {
                $scope.selectedCardId = '';
                $scope.cardId = '';
                $scope.cardDetail = {};
                $scope.setFocus.focusInput=false;
                $timeout(function(){
                  $scope.setFocus.focusInput=true;
                },200)
              }
              $scope.cancel = function () {
                $modalInstance.close();
              }
              $scope.submit = function () {
                if(!_.has($scope.cardDetail,'balance')){
                  growl.error('card not found.',{ttl:2000});
                  return false;
                }else{
                  if($scope.cardDetail.balance<0){
                    growl.error('card has insufficient amount.',{ttl:2000});
                    return false;
                  }
                }
                $scope.cardDetail.balance
                var cardTransaction=new CardTransaction();
                cardTransaction.cardId=$scope.selectedCardId;
                cardTransaction.type='billTransaction';
                cardTransaction.amount=-(bill.netAmount);
                cardTransaction.itemDetail=bill;
                $modalInstance.close(cardTransaction);
              }
            }],
            size: 'md',
            resolve: {
              bill: function () {
                /*Card Transaction with offer*/
                if($scope.bill._offers.length>0 || scrapOffer){
                 return  $scope.bill.cardItemsCalculationWithOffer().then(function(response){
                    console.log(response);
                    return response;
                  });
               }else{
                return $scope.bill.cardItemsCalculation();
               }
                // return $scope.bill.cardItemsCalculation();
              },
              cardId:function(){

                 /*Card Transaction with offer*/
                if($scope.bill._offers.length>0 || scrapOffer){
                return $http.post($rootScope.url + '/api/synckots/CardTransactionsCountByBill', {
                billId: $scope.bill._id
               }).then(function (result) {
                console.log(result.data);
                if(result.data.cards.length>1){
                  growl.error('More than 1 card transaction !! ',{ttl:2000})
                  return false;
                }else{
                  return result.data.cards;
                }
               })
              }else{
                return null;
              }
            }
          }
          }).result.then(function (ct) {
            console.log(new CardTransaction());
            if (ct) {
              ct.deployment_id=localStorageService.get('deployment_id');
              ct.user={name: $scope.getPassCodeUserPre(), userId: currentUser._id};
              ct.billDetail=$scope.billDetailOb();

              $scope.bill.cardTransaction = ct;
              console.log($scope.bill.cardTransaction);
              $scope.bill.generateKotNumber().then(function (kotnumber) {
                //alert(kotnumber);
                $scope.bill.kotNumber = kotnumber;
               // alert(isCardOffer);
                updateInsertBill($scope.kotComment,isCardOffer);
                $scope.kotComment = '';

              });
            } else {
              $scope.isBillingOn=false;
              $scope.bill.cardTransaction = {};
            }
          });
        }
      } else {
        growl.error("Please add at least one Item to generate KOT!", {
          ttl: 3000
        });
      }

      if ($scope.bill._tableId) {
        removeTableBooking($scope.bill);
        sendTableUpdateToInresto($scope.bill, 3);
      }
    };

    /* Print Template */
    $scope.printDiv = function(divName) {

      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;

      if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        var popupWin = window.open('Print', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWin.window.focus();
        popupWin.document.write('<!DOCTYPE html><html><head>' +
          '<link rel="stylesheet" type="text/css" href="assets/print.css" />' +
          '</head><body onload="window.print()" onfocus="windows.close()"><div class="reward-body">' + printContents + '</div></html>');

        /*popupWin.window.onbeforeunload = function (event) {
         popupWin.close();
         return '.\n';
         };*/
        popupWin.window.onabort = function(event) {
          popupWin.document.close();
          popupWin.close();
        };
      } else {
        var popupWin = window.open('Print', '_blank', 'width=800,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
        popupWin.document.close();
      }
      popupWin.document.close();
      return true;
    };

    $scope.printCardTransaction=function(bill) {
      // return false;
      if (_.has(bill, 'cardTransaction')) {
        if (_.has(bill.cardTransaction, 'billDetail')) {
          $http.post($rootScope.url + '/api/synckots/checkCardIfExist', {
            cardId: bill.cardTransaction.cardId,
            deployment_id: localStorageService.get('deployment_id')
          }).then(function (result) {
            //console.log(result);
            /* console.log(result);
             $scope.selectedCardId = angular.copy($scope.cardId);
             $scope.cardDetail.balance = result.data.balance;*/
            //if (bill.cardTransaction.type == 'billTransaction') {
            var billNumber = Utils.hasSetting('reset_serial_daily', settings) ? bill.daySerialNumber : bill.serialNumber;
            var header = Utils.getSettingValue('header_bill', $scope.settings);
            var footer = Utils.getSettingValue('footer_bill', $scope.settings);
            var billHtml = header;
            billHtml += '\r\n--------------------------------------\r\n';
            if (bill.tableNumber != null) {
              billHtml += 'Type:Table    Table Number:' + bill.tableNumber;
            } else {
              billHtml += 'Type:' + bill.tabType;
            }
            billHtml += '\r\n--------------------------------------\r\n';
            billHtml += "Bill Number:" + bill.cardTransaction.billDetail.instance.preFix + billNumber + '\r\n';
            if (bill.tabType == 'table') {
              billHtml += 'Steward: ' + (bill._waiter.firstname == undefined ? '' : (bill._waiter.firstname + ' ' + bill._waiter.lastname)) + '\r\n';
            } else {
              billHtml += 'Delivery Boy:' + (bill._delivery.firstname == undefined ? '' : (bill._delivery.firstname + ' ' + bill._delivery.lastname)) + '\r\n';
            }
            billHtml += 'Print Time:' + Utils.getDateFormatted(new Date())
            billHtml += '\r\nKot Number:' + bill.cardTransaction.billDetail.kotNumber;
            billHtml += '\r\n--------------------------------------\r\n';
            billHtml += $scope.setString('Item', 26) + $scope.setString('Qty', 8) + $scope.setString('Amt', 4);
            billHtml += '\r\n--------------------------------------\r\n';
            var tqty = 0;
            var sTotal = 0;
            for (var i = 0; i < bill.cardTransaction.itemDetail.items.length; i++) {
              billHtml += $scope.setString(bill.cardTransaction.itemDetail.items[i].name, 25) + ' ' + $scope.setString(bill.cardTransaction.itemDetail.items[i].quantity.toString(), 3) + $scope.setAmount(bill.cardTransaction.itemDetail.items[i].subtotal.toString(), 9) + '\r\n';
              tqty += bill.cardTransaction.itemDetail.items[i].quantity;
              sTotal += bill.cardTransaction.itemDetail.items[i].subtotal;
            }
            billHtml += '--------------------------------------\r\n';
            billHtml += $scope.setString('Total Qty:', 18) + $scope.setAmount(tqty.toString(), 9) + '\r\n';
            billHtml += $scope.setString('Sub Total:', 29) + $scope.setAmount(sTotal.toString(), 9) + '\r\n';
            billHtml += $scope.setString('Discount:', 29) + $scope.setAmount(bill.cardTransaction.itemDetail.discount.toFixed(2), 9) + '\r\n';
            for (var j = 0; j < bill.cardTransaction.itemDetail.taxes.length; j++) {
              billHtml += $scope.setString(bill.cardTransaction.itemDetail.taxes[j].name, 26) + $scope.setAmount(bill.cardTransaction.itemDetail.taxes[j].tax_amount.toString(), 12) + '\r\n';
              sTotal += bill.cardTransaction.itemDetail.taxes[j].tax_amount;
            }
            billHtml += '--------------------------------------\r\n';
            var roundOff =0;
            var billDiscount=0;
            if(bill.cardTransaction.type == 'billTransaction') {
              roundOff = Utils.roundNumber((-( bill.cardTransaction.amount)) - sTotal, 2);
            }
            else {
              roundOff = Utils.roundNumber((( bill.cardTransaction.amount)) - sTotal, 2);
            }
            billDiscount=  Math.floor(roundOff);
            if(billDiscount!=0){
            billHtml += $scope.setString('Disc Adjustment:', 16) + $scope.setAmount( (billDiscount+bill.cardTransaction.itemDetail.discount).toFixed(2), 22) + ' \r\n';
             roundOff=roundOff-billDiscount;
            }
            billHtml += $scope.setString('Round Off:', 16) + $scope.setAmount(roundOff.toFixed(2), 22) + ' \r\n';
            billHtml += $scope.setString('Grand Amount:', 16) + $scope.setAmount((-(bill.cardTransaction.amount)).toFixed(2), 22) + ' \r\n';
            billHtml += '--------------------------------------\r\n';
            billHtml += 'Transaction Detail:\r\n';
            billHtml += '===================\r\n';
            billHtml += $scope.setString('Trans. Type:', 15) + bill.cardTransaction.type +'\r\n';
            billHtml += $scope.setString('Card Id:', 15) + bill.cardTransaction.cardId + '\r\n';
            billHtml += $scope.setString('Name:', 15) + result.data.detail.name + '\r\n';
            billHtml += $scope.setString('Mobile:', 15) + result.data.detail.mobile + '\r\n';
            billHtml += $scope.setString('Previous Balance:', 20) + (result.data.balance- bill.cardTransaction.amount)+ '\r\n';
            billHtml += $scope.setString('Trans. Amount:', 20) + bill.cardTransaction.amount + '\r\n';
            billHtml += $scope.setString('Current Balance:', 20) + (result.data.balance ) + '\r\n';
            billHtml += '--------------------------------------\r\n';
            console.log(billHtml);
            //}

            printer.print('dynamicTemplate',billHtml,'dynamicPrint');
            //console.log(bill);

          });
        }
      }
    }
    $scope.setString=function(string,length){
      if(string==undefined){return '';}
      var str="";
      if(string.length>length){
        str=string.substring(0,length);
      }else{
        var blankchar="";
        for(var i=0;i<(length-string.length);i++){
          blankchar+=" ";
        }
        str=string+blankchar;
      }
      return str;
    }
    $scope.setAmount=function(string,length){
      if(string==undefined){return '';}
      var str="";
      var blankchar="";
      for(var i=0;i<(length-string.length);i++){
        blankchar+=" ";
        str=blankchar+string;
      }
      return str;
    }

 var printAfterSettle=function(printdata)
  {
  //console.log("print after settle");
  //console.log(printdata);
      var settings = (localStorageService.get('settings'));
      var bill = printdata.bill;
          var index=printdata.index;
          var billNumber = Utils.hasSetting('reset_serial_daily', localStorageService.get('settings')) ? bill.daySerialNumber : bill.serialNumber;
            var disableRoundoff = Utils.hasSetting('disable_roundoff', localStorageService.get('settings'));
            var items = [];
            items = printdata.items;

            var totalAmount = printdata.totalAmount;
            var totalDiscount = printdata.totalDiscount;
            var totalTax = printdata.totalTax;
            var taxes = printdata.taxes;
            console.log(totalAmount, totalDiscount, totalTax);
            _.forEach(bill._kots, function (kot) {
              _.forEach(kot.items, function (item) {
                _.forEach(item.addOns, function (addOn) {
                  items.push({name: addOn.name, quantity: addOn.quantity, subtotal: addOn.subtotal});
                })
              })
            })
            //console.log(items);



          //--------------------- CALCULATION FOR PAYMENT DETAIL  ------------------------------

          var paymentMethod=[];
          var payment={};
          //console.log(bill.payments);

              payment.key="Cash";
              payment.value=0;
              var change=0;
            _.forEach(bill.payments.cash,function(cash){
              console.log(cash);
              payment.value+=cash

               /* if(cash<0)
                  change=cash;*/

            })
            if(payment.value>0)
          paymentMethod.push(payment);
          payment={};
            _.forEach(bill.payments.cards,function(card){
            //  console.log(card);
              if(card.totalAmount!=0)
              {
                payment.key=card.cardType;
                payment.value=card.totalAmount;
                paymentMethod.push(payment);
                payment={};
              }

            })



          //console.log(paymentMethod);
            var header = Utils.getSettingValue('header_bill', localStorageService.get('settings'));
            var footer = Utils.getSettingValue('footer_bill', localStorageService.get('settings'));
            var tqty = 0;
            var subtotal = 0;
            var char=0;
            var billHtml='';
            if (Utils.hasSetting('settle_bill_on_print', localStorageService.get('settings')))
            {

              if(bill.rePrint && bill.rePrint.length>2)
              {
                billHtml +='Duplicate Print';
                billHtml += '\r\n--------------------------------------\r\n';
              }
            }
            else
            {

              if(bill.rePrint && bill.rePrint.length>1)
              {
                billHtml +='Duplicate Print';
                billHtml += '\r\n--------------------------------------\r\n';
              }
            }

            billHtml += header;
            billHtml += '\r\n--------------------------------------\r\n';
            if (bill.tabType=='table' && bill._tableId!=undefined) {
              billHtml += 'Type:'+bill.tab+'   Table Number:' + bill._tableId;
            } else if(bill.tabType=='table' && bill.tableNumber!=undefined) {
              billHtml += 'Type:'+bill.tab+'Table Number:' + bill.tableNumber;
            }
            else {
              billHtml += 'Type:' + bill.tab;
            }
            billHtml += '\r\n--------------------------------------\r\n';
            if(bill.prefix!=null)
              billHtml += "Bill Number:" + bill.prefix + "-" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '')+'\r\n';
            else
              billHtml += "Bill Number:" + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '')+ '\r\n';
            if(bill.tabType=='table')
              billHtml += 'Steward: ' + (bill._waiter.firstname == undefined ? '' : bill._waiter.firstname) + ' ' + (bill._waiter.lastname == undefined ? '' : bill._waiter.lastname) + '\r\n';
            else
              billHtml += 'Delivery Boy:' + (bill._delivery.firstname == undefined ? '' : (bill._delivery.firstname + ' ' + bill._delivery.lastname)) + '\r\n';
            billHtml += 'Date:' + Utils.getDateFormatted(bill.rePrint[0].printTime);
            if(bill.rePrint && bill.rePrint.length>1)
              billHtml+='\r\nRePrint Time: '+Utils.getDateFormatted(bill.rePrint[bill.rePrint.length-1].printTime);
            billHtml += '\r\nKots:';
            for (var i = 0; i < bill._kots.length; i++) {
              billHtml += bill._kots[i].kotNumber;
              if (i < bill._kots.length - 1)
                billHtml += ',';
            }
            billHtml += '\r\n--------------------------------------\r\n';
            if(bill._customer.mobile!=undefined)
            {
              billHtml +='Customer Detail';
              billHtml += '\r\n--------------------------------------\r\n';
              billHtml +='Name: ' +bill._customer.firstname+'\r\n';
              billHtml +='Add: '+bill._customer.address1+','+'\r\n';
              billHtml +=bill._customer.address2+','+'\r\n';
              billHtml +=bill._customer.city+','+bill._customer.state+','+bill._customer.postCode+'\r\n';
              billHtml+='Mobile: '+bill._customer.mobile;
              billHtml += '\r\n--------------------------------------\r\n';
            }

            billHtml += $scope.setString('Item', 26) + $scope.setString('Qty', 8) + $scope.setString('Amt', 4);
            billHtml += '\r\n--------------------------------------\r\n';
            for (var i = 0; i < items.length; i++) {
              tqty += items[i].quantity;
              subtotal += parseFloat(items[i].subtotal);
              billHtml += $scope.setString(items[i].name, 25) + ' ' + $scope.setString(items[i].quantity.toString(), 3) + ' ' + $scope.setAmount((parseFloat(items[i].subtotal).toFixed(2)).toString(), 8) + '\r\n';
            }
            //console.log(parseFloat(subtotal));
            billHtml += '--------------------------------------\r\n';
            billHtml += $scope.setString('Total Qty:', 18) + $scope.setAmount(tqty.toString(), 9) + '\r\n';
            billHtml += $scope.setString('SubTotal:', 29) + $scope.setAmount((parseFloat(subtotal)).toFixed(2), 9) + '\r\n';
            billHtml += $scope.setString('Discount:', 29) + $scope.setAmount(totalDiscount.toFixed(2), 9) + '\r\n';
            for (var i = 0; i < bill._kotsTaxes.length; i++) {
              billHtml += $scope.setString(bill._kotsTaxes[i].name, 26) + $scope.setAmount(bill._kotsTaxes[i].tax_amount.toString(), 12) + '\r\n';
            }
            for(var j=0;j<bill.charges.detail.length>0;j++)
            {
              char+=bill.charges.detail[j].amount;
              billHtml+=$scope.setString(bill.charges.detail[j].name,26) + $scope.setAmount(bill.charges.detail[j].amount.toString(), 12) + '\r\n';
            }
            billHtml += '--------------------------------------\r\n';
            if(disableRoundoff)
              billHtml += $scope.setString('Grand Total:', 16) + $scope.setAmount(((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(2)).toString(), 22) + '\r\n';
            else
            {
              /*  if((((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0))-(parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(2))>0)
               billHtml += setString('Round Off:', 16) +setAmount((((((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0))-(parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(2))).toFixed(2)).toString(), 22) + '\r\n';
               else*/
              billHtml += $scope.setString('Round Off:', 16) +$scope.setAmount(((((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0))-(parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(2)).toFixed(2)).toString(), 22) + '\r\n';
              billHtml += $scope.setString('Grand Total:', 16) +$scope.setAmount(((parseFloat(subtotal) + totalTax + char - totalDiscount).toFixed(0)).toString(), 22) + '\r\n';
            }
            billHtml += '--------------------------------------\r\n';
            billHtml += 'Payment Detail:\r\n';
            billHtml += '===================\r\n';
            //console.log(paymentMethod)
            _.forEach(paymentMethod,function(payment)
            {
              console.log(payment);
              billHtml += $scope.setString(payment.key, 16) + $scope.setAmount((parseFloat(payment.value).toFixed(2)).toString(),22) +'\r\n';
            })
            //billHtml += $scope.setString('Cash:', 16) + $scope.setAmount(cash.toString(),22) +'\r\n';
            //billHtml += $scope.setString('Credit Card:', 16) + $scope.setAmount(credit.toString(), 22) + '\r\n';
            //billHtml += $scope.setString('Debit Card:', 16) + $scope.setAmount(bill.payments.toString(), 22) + '\r\n';
            //billHtml += $scope.setString('Coupons:', 16) + $scope.setAmount(coupons.toString(), 22) + '\r\n';
            //billHtml += $scope.setString('Others:', 16) + $scope.setAmount(other.toString(), 22)+ '\r\n';
            //billHtml += $scope.setString('Total Amount:', 16) + $scope.setAmount(totalAmount.toString(), 22) + '\r\n';
            //billHtml += $scope.setString('Change:', 16) + $scope.setAmount((parseFloat(change).toFixed(2)).toString(), 22) + '\r\n';
            billHtml += '--------------------------------------\r\n';
            //billHtml += $scope.setString('Paid Amount:', 16) + $scope.setAmount(paidAmount.toString(), 22) + '\r\n';
            //billHtml += '--------------------------------------\r\n';
            billHtml += footer;
            billHtml += '\r\n\r\n';
            billHtml += '\r\n\r\n';
            //console.log(totalAmount, subtotal);
            //console.log(billHtml);
            printer.print('dynamicTemplate',billHtml,'dynamicPrint');

    }


    $scope.passCodeModal = function() {
      var d = $q.defer();
      $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/_passCode.html',
        controller: ['$scope', '$modalInstance', 'growl', function($scope, $modalInstance, growl) {
          $scope.passCode = "";
          $scope.setDigit = function(digit) {
            $scope.passCode += digit;
            if ($scope.passCode.length == 6) {
              $scope.applyPassCode();
            }
          };
          $scope.applyPassCode = function() {
            //d.resolve($scope.passCode);
            $modalInstance.close($scope.passCode);

          };
          $scope.setKeyboardEvent=function(){
            //alert($scope.passCode);
            if ($scope.passCode.length == 6) {
              $scope.applyPassCode();
            }
          }
          $scope.cancelPassCode = function() {
            d.resolve('cancel');
            $modalInstance.close('cancel');
          };
        }],
        size: 'sm'
      }).result.then(function(passCode){
        //console.log(user);
         var _f = _.filter($rootScope.users, {passcode: passCode});
            if (_f.length > 0) {
              $scope.assignPassUser( angular.copy( _f[0] ));
              d.resolve(passCode);
           }else{
            growl.error('Wrong Passcode',{ttl:1000});
            d.resolve('cancel');
           }
      })
      return d.promise;
    };


    $scope.kotComment;
    $scope.getKotComment = function() {
      $scope.commentModal().then(function(comment) {
        $scope.kotComment = comment;
      })
    };

    $scope.commentModal = function() {
      var d = $q.defer();
      $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/item/_comment1.html',
        controller: function($scope, $modalInstance) {
          $scope.comment = "";
          $scope.addComment = function() {
            d.resolve($scope.comment);
            $modalInstance.close($scope.comment);
          };
          $scope.cancel = function() {
            d.resolve('cancel');
            $modalInstance.dismiss('cancel');
          };
        },
        size: 'sm'
      });
      return d.promise;
    };

    $scope.processVoidBill = function(bill) {

      $scope.bill = bill ? bill : $scope.bill;
      var tmpbill= angular.copy( $scope.bill);
      $scope.bill._closeTime = new Date();
      $scope.bill.voidBillTime = new Date();
      var username = $rootScope.isInstance ?$scope.getUserNameForNotification() : $scope.currentUser.username;
      //alert($scope.passCodeContextUser);
      $scope.bill.voidDetail={time:new Date(),userName:angular.copy( ($scope.passCodeContextUser=='')?$scope.currentUser.username:angular.copy( $scope.passCodeContextUser))};
      $scope.passCodeContextUser='';
      $scope.bill.checkBillExists($scope.bill._id).then(function(len) {
        if (!len) {
          $scope.bill.updateBill({
            isVoid: true
          }, 'insert').then(function() {
            var _to = $scope.bill.tab;
            if ($scope.bill.tab === "table") {
              delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
              $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
            }
            // $state.transitionTo('billing.table');
            //$state.transitionTo('billing');
            $scope.initializeTab();

            var username=$scope.getUserNameForNotification();
            var str="Voided bill Number:#"+ $scope.getBillNumber() +" by "+ username +" for customer "+($scope.bill._customer.firstname==undefined?"none":bill._customer.firstname);
            $scope.offerNotification('Voided Bill',str);
          });
        } else {
          $scope.bill.updateBill({
            isVoid: true
          }, 'update').then(function() {
            var _to = $scope.bill.tab;
            if ($scope.bill.tab === "table") {
              delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
              $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
            }
            //  $state.transitionTo('billing.table');
            // $state.transitionTo('billing');
            $scope.initializeTab();
            var username=$scope.getUserNameForNotification();
            var str="Voided bill Number:#"+ $scope.getBillNumber() +" by "+ username +" for customer "+($scope.bill._customer.firstname==undefined?"none":bill._customer.firstname);
            $scope.offerNotification('Voided Bill',str);
          },function(err){
            growl.error('There is problem updating the transaction.',{ttl:3000});
            if(err.data.error=='inValid'){
              $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

              })
            }
          });
        }
      });
    };


    /* Void Bill */
    $scope.voidBill = function(bill) {
      console.log(bill);
      if(bill.tabType=='table'){
        $scope.lockTable(bill._tableId);
      }else{
        $scope.lockNonTable(angular.copy( bill.billNumber.toString()));
      }
      if (Utils.hasSetting('enable_passcode_void_bill', $scope.settings)) {
        $scope.passCodeModal().then(function(res) {
          if (res != Utils.getPassCode(res, $rootScope.users,'Manager,Admin')) {
            growl.error('Wrong Passcode!', {
              ttl: 2000
            });
            $scope.unlockTable();
            return false;
          } else {
            $scope.setTempManagerName(res);
            var _f = _.filter($rootScope.users, {passcode:res});
              if(_f.length>0){
                  $scope.passCodeContextUser=angular.copy( (_.has( _f[0],'username')?_f[0].username:''));
              }
            //console.log($scope.passCodeContextUser);
            //$scope.passCodeContextUser=res.username;
            if (Utils.hasSetting('enable_comment_void_bill', $scope.settings)) {
              $scope.commentModal().then(function(comment) {
                if (comment === 'cancel' || comment.length === 0) {
                  growl.error('Please Enter Valid Comment', {
                    ttl: 2000
                  });
                  $scope.unlockTable();
                  return false;
                } else {
                  //$scope.bill.voidComment = comment;
                  bill.voidComment = comment;
                  $scope.processVoidBill(bill);
                }
              });
            } else {
              $scope.processVoidBill(bill);
            }
          }
        });
      } else {
        if (Utils.hasSetting('enable_comment_void_bill', $scope.settings)) {
          $scope.commentModal().then(function(comment) {
            if (comment === 'cancel' || comment.length === 0) {
              growl.error('Please Enter Valid Comment', {
                ttl: 2000
              });
              $scope.unlockTable();
              return false;
            } else {
              //$scope.bill.voidComment = comment;
              bill.voidComment = comment;
              $scope.processVoidBill(bill);
            }
          });
        } else {
          //$scope.bill.voidComment = comment;
          // bill.voidComment = comment;
          $scope.processVoidBill(bill);
        }
      }
      //console.log($scope.bill);
      // return false;
    };

    $scope.getBillNumber=function(){
      var prefix='';
      var billnumber='';
      var instance=localStorageService.get("instanceId");
      if(!(instance==null|| instance==undefined)){
        if(instance.preFix!=undefined){
          prefix=instance.preFix;
        }
      }
      if(Utils.hasSetting('reset_serial_daily',$scope.settings)){
        billnumber=prefix + $scope.bill.daySerialNumber.toString();
      }else{
        billnumber=prefix + $scope.bill.serialNumber.toString();
      }
      return billnumber;
    }
    $scope.getBillNumberByBill=function(bill){
      console.log(bill);
      var prefix='';
      var billnumber='';
      var instance=localStorageService.get("instanceId");
      if(!(instance==null|| instance==undefined)){
        if(instance.preFix!=undefined){
          prefix=instance.preFix;
        }
      }
      if(Utils.hasSetting('reset_serial_daily',$scope.settings)){
        billnumber=prefix + bill.daySerialNumber.toString();
      }else{
        billnumber=prefix + bill.serialNumber.toString();
      }
      return billnumber;
    }

    $scope.rePrintNotification=function(bill,cmt){
      var tempbill = bill==null?$scope.bill:bill;
      var username=$scope.getUserNameForNotification();
      var str="Re printed bill Number:#"+ (bill==null? $scope.getBillNumber():$scope.getBillNumberByBill(bill) )+" by "+ username +" for customer "+(tempbill._customer.firstname==undefined?"none":tempbill._customer.firstname) + ((!(cmt==null||cmt==undefined||cmt==""))?" Comment:-"+cmt:'');
       // alert(str);
      $scope.offerNotification('Re-Printed Bill',str);
    }
    /* Print Bill */
    $scope.billToPrint = {};

    $scope.checkBeforePunchItem=function(){
      var flag=false;
        if(!($scope.bill==null||$scope.bill==undefined)){
          if(!($scope.bill.isPrinted==null||$scope.bill.isPrinted==undefined)){
            if($scope.bill.isPrinted){
              if(Utils.hasSetting('no_punch_item_print', $scope.settings)) {
                growl.error('Puching item not allowed after print.',{ttl:3000});
                flag=true;
              }
            }
          }
        }
        return flag;
    }

    $scope.checkForComment=function(bill){
      //console.log(bill.billPrintTime);
       var flag=false;
        if(!(bill.billPrintTime==null||bill.billPrintTime==undefined)){
          if(Utils.hasSetting('comment_re_print', $scope.settings)){
            flag=true;
         }
      }
      return flag;
    }

    $scope.printBill=function(isSettled){
       if (Utils.hasSetting('enable_crm_mandatory', $scope.settings, $scope.selectedTab.tabType)) {
          if (_.isEmpty($scope.bill._customer)) {
            $scope.isBillingOn=false;
            growl.error('Customer not selected!',{ttl:2000});
            return false;
          }
        }
        if($scope.checkForComment(angular.copy( $scope.bill))){
        $ngBootbox.prompt('Feed in comment for reprint').then(function(cmt){
          if(cmt==null||cmt==''||cmt==undefined){
            growl.error('Comment is mandatory for reprint',{ttl:3000});
            $scope.isBillingOn=false;
            return false ;
          }
          $scope.printBillPost(isSettled,cmt);
        },function(err){
          //alert();
          $scope.isBillingOn=false;
          growl.error('Comment is mandatory for reprint',{ttl:3000});
        })
      }else{
          $scope.printBillPost(isSettled);
        }
    }

    $scope.printBillPost = function(isSettled,cmt) {
       if (Utils.hasSetting('enable_card', $scope.settings)) {
          if($scope.bill._offers.length>0){
            $scope.bill.cardItemsCalculationWithOffer().then(function(response){
              console.log(response);
              if(response.netAmount==0){
                $scope.printBillPostCard(isSettled,cmt);
              }else{
                $scope.printKot(true);
              }
            })
          }else{
           $scope.printBillPostCard(isSettled,cmt);
          }
        }else{
          $scope.printBillPostCard(isSettled,cmt);
        }

    };

    $scope.printBillPostCard=function(isSettled,cmt){
      if(Utils.hasSetting('print_bill_cashier', $scope.settings)) {
          if($scope.selectedCashier==null){
            growl.error('Only cashier can print the bill',{ttl:3000});
            $scope.isBillingOn=false;
            return false;
          }
        }


        if ($scope.bill._tableId) {
          if(Utils.hasSetting('settle_bill_on_print', $scope.settings, $scope.selectedTab.tabType))
            sendBillToInresto($scope.bill);
           else
            sendTableUpdateToInresto($scope.bill, 4);
          removeTableBooking($scope.bill);
        }

      $scope.bill.cardTransaction={};
      var flag =false;
      // console.log($scope.bill);
      // return false;
      //  alert(Utils.hasSetting('dis_kot', $scope.settings));
      var ptime=new Date();
      if($scope.bill.billPrintTime !=null){
        flag=true;
      }
      $scope.bill.billPrintTime =($scope.bill.billPrintTime==null? ptime:$scope.bill.billPrintTime);
      $scope.bill.rePrint.push({user:$scope.getPassCodeUserPre(),printTime: ptime,comment:cmt});
      var issettle = Utils.hasSetting('enable_settlement', $scope.settings, $scope.selectedTab.tabType);
      var isSettleOnPrint = Utils.hasSetting('settle_bill_on_print', $scope.settings, $scope.selectedTab.tabType);

      if (!isSettled) {
        if (isSettleOnPrint && issettle) {
          $scope.openSettleBill('lg',$scope.bill);
          return false;
        }
      } else {
        console.log('Settle Bill on Print');
        $scope.bill.isSettled = true, $scope.bill._closeTime = new Date();
        checkUrbanPiperSettings(angular.copy($scope.bill));
      }

      if (!issettle) {
        $scope.bill._closeTime = new Date();
        /* Add Total Bill Amount as Cash Received */
        $scope.bill.payments.cash.push($scope.bill.getSumKotsAndTotalBill());
      }

      var comment = "";
      $scope.bill.generateKotNumber().then(function(kotnumber) {
        $scope.bill.kotNumber = $scope.bill == null ? kotnumber : kotnumber - 1;
        if ($scope.bill._items.length === 0) {
          // Prepare KOT Aggregation
          $scope.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function(preparedBill) {
            //console.log(preparedBill);
            $scope.billToPrint = preparedBill;

            updateInsertBillPrint(comment, false);
          }, function(errors) {
           // alert();
            _.forEach(errors, function(e) {
              $scope.isBillingOn=false;
              growl.error(e, {
                ttl: 3000
              });
            });
          });
        } else {
          if ((Utils.hasSetting('kot_mandatory', $scope.settings))) {
            alert("KOT is mandatory, please print kot before printing bill.");
            return false;
          }

          $scope.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function (preparedBill) {

            $scope.currentKotIndex = $scope.bill.printKot(comment,$scope.getKotWaiter());
            $scope.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function (preparedBill) {
              $scope.billToPrint = preparedBill;
              // console.log($scope.billToPrint)
              updateInsertBillPrint(comment, true);
            },function (errors) {
              _.forEach(errors, function (e) {
                growl.error(e, {ttl: 3000});
              });
            });

          }, function(errors) {
            _.forEach(errors, function(e) {
              growl.error(e, {
                ttl: 3000
              });
            });
          });
        }
      });
      if(flag){$scope.rePrintNotification($scope.bill,cmt)};
    }
    /* Show Edit Bill */
    $scope.showEditBill = function() {
      $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/_editBill.html',
        controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'offlineBills', '$state', 'settings', 'Utils', 'Bills', 'Bill', function($rootScope, $scope, $resource, $modalInstance, offlineBills, $state, settings, Utils, Bills, Bill) {
          $scope.bills = offlineBills;


          $scope.gotoEditBill = function(bill) {
            $modalInstance.close(bill);
          };
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
          $scope.getUnsettled = function() {
            var _uAmount = 0;
            _.forEach($scope.bills, function(bill) {
              if (!bill.isSettled || bill.isSettled === 'false') {
                _uAmount += bill.totalBill;
              }
            });
            return (_uAmount).toFixed(2);
          };
          $scope.getDaySale = function() {
            var _daySale = 0;
            angular.forEach($scope.bills, function(bill) {
              _daySale += bill.totalBill;
            });
            return _daySale;
          };
          $scope.getTotalTax = function() {
            var _tT = 0;
            _.forEach($scope.bills, function(bill) {
              _tT += bill.totalTax;
            });
            return (_tT).toFixed(2);
          };

        }],
        size: 'lg',
        resolve: {
          offlineBills: ['$rootScope', 'Bill', 'Bills', function($rootScope, Bill, Bills) {
            return $rootScope.db.selectAll("bills").then(function(results) {
              var _rows = [];
              var offlineBills = [];
              for (var i = 0; i < results.rows.length; i++) {
                _rows.push(results.rows.item(i));
              }

              _.forEach(_rows, function(ob) {
                var _ob = {
                  id: ob.id,
                  billId: ob.billId,
                  bill: _.assign(new Bill, JSON.parse(ob.billData)),
                  totalBill: parseFloat(ob.totalBill),
                  totalTax: parseFloat(ob.totalTax),
                  isSynced: ob.isSynced,
                  syncedOn: ob.syncedOn,
                  customer: ob.customer,
                  customer_address: ob.customer_address,
                  isVoid: ob.isVoid,
                  isSettled: ob.isSettled,
                  isPrinted: ob.isPrinted,
                  created: ob.created
                };
                //  console.log(_ob.isPrinted);
                offlineBills.push(_ob);
                Bills.addBill(_ob);
              });

              return offlineBills;
            });
          }],
          settings: function() {
            return $scope.settings;
          }
        }
      }).result.then(function(bill) {
        $state.go('billing.edit', {
          id: bill.billId
        });
      });
    };


    $scope.syncCustomers = function() {
      var _customerSave = [];
      var _customers=[];
      $rootScope.db.selectDynamic('Select customerData from Customers').then(function(offlineCustomers) {
        if (offlineCustomers.rows.length > 0) {
          for (var i = 0; i < offlineCustomers.rows.length; i++) {
            //   _customerSave.push(Customer.createWithCheck(offlineCustomers.rows.item(i).customerData).$promise);
            _customers.push(offlineCustomers.rows.item(i).customerData);
          }
          /*$q.all(_customerSave).then(function(customers) {
           console.log(customers);
           $rootScope.db.selectDynamic('delete from customers').then(function(result) {
           console.log(result);
           })
           });*/
        }
        if(_customers.length>0)
          $scope.autoSyncCustomer(_customers,0);
      });
    };

    $scope.autoSyncCustomer=function(customers,count){
      Customer.createWithCheck(customers[count]).$promise.then(function(cust){
        $rootScope.db.selectDynamic('delete from customers where customerId="'+cust.customerId+'"').then(function(result) {
          if(count<customers.length-1){
            count=count+1;
            $timeout(function() {
              $scope.autoSyncCustomer(customers,count);
            }, 10);
          }
        })
      },function(){
        $rootScope.db.selectDynamic('delete from customers where customerId="'+customers[count].customerId+'"').then(function(result) {
          if(count<customers.length-1){
            count=count+1;
            $timeout(function() {
              $scope.autoSyncCustomer(customers,count);
            }, 10);
          }
        })
      })
    }

    /* Sync Offline Bills */
    $scope.autoSyncOfflineBills = function () {
      var billPromises=[];
      var allBillsData=[];
      $rootScope.db.selectDynamic("select billData from bills where closetime is not null and isSynced='false' order by created limit 100").then(function(result){
        //console.log(result.rows.length);
        if(result.rows.length>0){
          for(var i=0;i<result.rows.length;i++){
            var data=(result.rows.item(i).billData);
            allBillsData.push(angular.copy( ( angular.toJson(JSON.parse(data)))));
          }
          if(allBillsData.length>0)
            $scope.autoCallSaveBill(( allBillsData),0);
        }
      });
      //console.log(billPromise);
      /*$q.all(billPromises).then(function(ntbills){
       $rootScope.db.selectDynamic('update bills set isSynced="false"',[]);
       });*/
    };

    $scope.autoCallSaveBill=function(allBills,count){
      //console.log(count);
      //billResource.createWithId( angular.copy(allBills[count])).$promise.then(function(resid){
         ///////////////////////
        // console.log($location);
        $http.defaults.headers.post["Content-Type"] = "application/json";
        $http({
        url:'http://'+ $location.host()+':'+$location.port()+'/api/bills/createWithId',
        method: "POST",
        data: angular.copy(allBills[count])
    })
    .then(function(result) {
            // success
         //resid=body;
         /////////////////////
       // console.log(result.data);
        var resid=result.data;
        //        $rootScope.db.update('bills',{"isSynced" : "true", "syncedOn" : new Date().toISOString()},{"billId": id.billId}).then(function(success){
        $rootScope.db.selectDynamic('update bills set isSynced="true",syncedOn="'+new Date().toISOString()+'" where billId="'+resid.billId+'"').then(function(success){
          if(count<allBills.length-1){
            count=count+1;
            $timeout(function() {
              $scope.autoCallSaveBill(allBills,count);
            }, 10);

          }else
          {
            $rootScope.db.selectDynamic("delete from bills where billid not in (select billid from bills where isSynced is null or isSynced='false')and billid not in (select billid from bills order by created desc limit 250)",[]);
          }
        });
      },function(err){
        console.log(err);
        if(_.has( err.data.errors),'ng_id') {
          var newBillData= JSON.parse(allBills[count]);
          var oldId = angular.copy(newBillData._id);
          $rootScope.db.selectDynamic('select syncCount from bills where  billId="' +oldId + '"').then(function (res) {
            console.log(res.rows.item(0).syncCount);
            if(res.rows.item(0).syncCount==null){
              $rootScope.db.selectDynamic('update  bills set syncCount=0 where  billId="' +oldId + '"').then(function (r) {});
            }else {
              if (res.rows.item(0).syncCount < 2) {
                $rootScope.db.selectDynamic('update  bills set syncCount=syncCount+1 where  billId="' + oldId + '"').then(function (r) {
                });
              } else {

                var newId=Utils.guid();
                //   console.log(allBills[count]);
                newBillData._id=newId;

                //  $rootScope.db.selectDynamic('update bills set billData=\'' + JSON.stringify(newBillData) + '\',billId="' + newId + '" where billId="' + oldId + '"').then(function (success) {});
              }
            }
            if (count < allBills.length - 1) {
              count = count + 1;
              // $scope.autoCallSaveBill(allBills,count);
              $timeout(function () {
                $scope.autoCallSaveBill(allBills, count);
              }, 10);

            } else {
              $rootScope.db.selectDynamic("delete from bills where billid not in (select billid from bills where isSynced is null or isSynced='false')and billid not in (select billid from bills order by created desc limit 200)", []);
            }
          }); // count retry duplicate
        }else{
          if (count < allBills.length - 1) {
            count = count + 1;
            // $scope.autoCallSaveBill(allBills,count);
            $timeout(function () {
              $scope.autoCallSaveBill(allBills, count);
            }, 10);

          } else {
            $rootScope.db.selectDynamic("delete from bills where billid not in (select billid from bills where isSynced is null or isSynced='false')and billid not in (select billid from bills order by created desc limit 200)", []);
          }
        }
      })
    };


    $scope.slickHandleItems = {};

    $scope.slickHandleCategories = {};

    $scope.slickHandleTable = {
      startNewTable: function(id) {
        //alert('Hello');
        //console.log("Table Clicked" + id);
      }
    };

    $scope.filterHandle = {};

    $scope.setCustomerDeliverTo = function(address) {
      $scope.bill.setCustomerDeliverTo(address);
    };
    $scope.updateBill = function() {
      $scope.bill.updateBill(null, 'update').then(function() {
        $state.go('^');
      });
    };

    $scope.menuOptionsBill = [
      ['Copy', function ($itemScope) {
        $scope.copyTableBill($itemScope.t.id,$itemScope.t.bill);
      }],
      ['Settle Bill', function ($itemScope) {
        $scope.openSettleBill('lg', $itemScope.t.bill, true);
      }],
      ['Void Bill', function ($itemScope) {
        $scope.voidBill($itemScope.t.bill)
      }],
      ['Split Bill', function ($itemScope) {
        $scope.splitBill($itemScope.t.bill, $itemScope.t.id - 1)
      }]
    ];

    $scope.getMenuTypesOption=function(){
      var menu =   [
        ['Copy', function ($itemScope) {
          $scope.copyTableBill($itemScope.t.id,$itemScope.t.bill);
        }],
        ['Settle Bill', function ($itemScope) {
          $scope.openSettleBill('lg', $itemScope.t.bill, true);
        }],
        ['Void Bill', function ($itemScope) {
          $scope.voidBill($itemScope.t.bill)
        }],
        ['Split Bill', function ($itemScope) {
          $scope.splitBill($itemScope.t.bill, $itemScope.t.id - 1)
        }]
      ];
      return menu;
    }

    $scope.menuOptionsWOBill = [
      ['Paste', function($itemScope) {
        // $scope.player.gold += $itemScope.item.cost;
        $scope.pasteTableBill($itemScope.t.id - 1);
      }]
    ];
    $scope.menuOptionsOnline=function(bill){
      if(bill){
        var allfeture=[];
        console.log(bill);
        var cancel = ['Cancel Order', function() {
          $scope.onlineChangeLocalMenu(true,bill.bill);
        }];
        allfeture.push(cancel);
        return allfeture
      }
    }

    $scope.menuOptionsAdvance=function(bill){
      if(bill){
        var allfeture=[];
        console.log(bill);
        var cancel = ['Cancel Advance', function() {
          $ngBootbox.confirm('Are you sure to delete the selected booking?').then(function(){
            Advancebooking.delete({},{_id:bill.id},function(status){
              //$scope.advanceBookings.splice((Utils.arrayObjectIndexOf($scope.advanceBookings,'id',bill.id) ),1);
              $rootScope.db.selectDynamic('delete from AdvanceBookings where AdvanceBillId="'+bill.id+'"',[]).then(function(){
                growl.success('Selected booking delete from server.',{ttl:2000});
                $scope.initializeTab();
              })
            },function(){
              $rootScope.db.selectDynamic('delete from AdvanceBookings where AdvanceBillId="'+bill.id+'"',[]).then(function(){
                growl.success('Selected booking deleted from server.',{ttl:2000});
                $scope.initializeTab();
              })
            })
          })
        }];
        allfeture.push(cancel);
        return allfeture
      }
    }

    $scope.menuOptions = function(bill) {
      if (bill) {
        var allValidFeature = [];
        var copy = ['Copy', function($itemScope) {
          if($rootScope.isInstance) {
            $scope.getFineBillById($itemScope.t.bill._id).then(function (offlineBill) {
              $itemScope.t.bill = angular.copy(offlineBill.bill);
              $scope.copyTableBill($itemScope.t.id,$itemScope.t.bill);
            })
          }else{
            $scope.copyTableBill($itemScope.t.id,$itemScope.t.bill);
          }
          // $scope.copyTableBill($itemScope.t.id,$itemScope.t.bill);
        }];
        var settle = ['Settle Bill', function($itemScope) {
          if($rootScope.isInstance) {
            $scope.getFineBillById($itemScope.t.bill._id).then(function (offlineBill) {
              $itemScope.t.bill = angular.copy(offlineBill.bill);
              $scope.openSettleBill('lg', $itemScope.t.bill,true);
            })
          }else{
            $scope.openSettleBill('lg', $itemScope.t.bill,true);
          }
          // $scope.openSettleBill('lg', $itemScope.t.bill,true);
        }];
        var voidbill = ['Void Bill', function($itemScope) {
          if($rootScope.isInstance) {
            $scope.getFineBillById($itemScope.t.bill._id).then(function (offlineBill) {
              $itemScope.t.bill = angular.copy(offlineBill.bill);
              $scope.voidBill($itemScope.t.bill);
            })
          }else{
            $scope.voidBill($itemScope.t.bill);
          }
          //$scope.voidBill($itemScope.t.bill)
        }];
        var split = ['Split Bill', function($itemScope) {
          if($rootScope.isInstance) {
            $scope.getFineBillById($itemScope.t.bill._id).then(function (offlineBill) {
              $itemScope.t.bill = angular.copy(offlineBill.bill);
              $scope.splitBill($itemScope.t.bill, $itemScope.t.id - 1)
            })
          }else{
            $scope.splitBill($itemScope.t.bill, $itemScope.t.id - 1)
          }
        }];
        var rePrint = ['RePrint Bill', function($itemScope) {
          // console.log($itemScope.t.bill);
          if($rootScope.isInstance) {
            $scope.getFineBillById($itemScope.t.bill._id).then(function (offlineBill) {
              $itemScope.t.bill = angular.copy(offlineBill.bill);
              $scope.rePrintBillFunction($itemScope);

              /*$itemScope.t.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function(preparedBill) {
               var billToPrint = preparedBill;
               printer.print('_template_Bill.html', billToPrint);
               });*/
            });
          }else{
            $scope.rePrintBillFunction($itemScope);
          }

        }];
        /* var tabWiseTransferTable = ['Tab Tansfer', function($itemScope) {
         $scope.transferTableByTab($itemScope.t.bill, $itemScope.t.id - 1)
         }];*/


        if (Utils.hasSetting('enable_covers', $scope.settings, $scope.selectedTab.tabType) || Utils.hasSetting('cover_mandatory',$scope.settings,$scope.selectedTab.tabType)) {
          var cover = ['Cover', function($itemScope) {
            // $scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)
            if($rootScope.isInstance) {
              $scope.getFineBillById($itemScope.t.bill._id).then(function (offlineBill) {
                $itemScope.t.bill = angular.copy(offlineBill.bill);
                $scope.renderRefreshNewBill(22, $itemScope.t.bill);
              })
            }else{
              $scope.renderRefreshNewBill(22, $itemScope.t.bill);
            }
          }];
          allValidFeature.push(cover);
        }

        if ($scope.selectedTab.tabType === 'table') allValidFeature.push(copy);
        if (bill.isPrinted) {
          if(!(Utils.hasSetting('enable_card', $scope.settings))) {
            allValidFeature.push(settle);
          }
          allValidFeature.push(rePrint);
        }
        if(!(Utils.hasSetting('enable_card', $scope.settings)))
          allValidFeature.push(voidbill);
        if ((bill.splitNumber == null) && ($scope.selectedTab.tabType === 'table'))
        {
          if(!Utils.hasSetting('enable_card', $scope.settings))
            allValidFeature.push(split);
        }

        /*if ($scope.selectedTab.tabType === 'table')
         {
         if($rootScope.isInstance){
         allValidFeature.push(tabWiseTransferTable);
         }
         }*/
        /* var transferBill = ['Tranfer Bill', function($itemScope) {
         // $scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)
         $scope.transferBill( $itemScope.t.bill);
         }];
         allValidFeature.push(transferBill);*/
        var pasteBill = ['Paste Bill', function($itemScope) {
          // $scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)
          $scope.pasteBill($itemScope.t.id - 1, $itemScope.t.bill);
        }];
        var pasteKot = ['Paste Kot', function($itemScope) {
          // $scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)
          $scope.pasteKot( $itemScope.t.bill);
        }];
        var pasteItem = ['Paste Item', function($itemScope) {
          // $scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)
          $scope.pasteItem( $itemScope.t.bill);
        }];
        if($scope.copiedTableBill.tableIndex!=undefined) {
          if(!($scope.copiedTableBill.bill._tableId==bill._tableId && $scope.copiedTableBill.bill.tabId==bill.tabId)) {
            if (!Utils.hasSetting('disable_transfer', $scope.settings, 'tranbill')) {
               if(!Utils.hasSetting('enable_card', $scope.settings))
                 allValidFeature.push(pasteBill);
            }
          }
        }

        if($scope.copiedTableBill.kotIndex!=undefined) {
          if(!($scope.copiedTableBill.bill._tableId==bill._tableId && $scope.copiedTableBill.bill.tabId==bill.tabId)) {
            allValidFeature.push(pasteKot);
          }
        }
        if($scope.copiedTableBill.itemIndex!=undefined) {
          if(!($scope.copiedTableBill.bill._tableId==bill._tableId && $scope.copiedTableBill.bill.tabId==bill.tabId)) {
            allValidFeature.push(pasteItem);
          }
        }
        //added by rajat for kds
        var kdsInfo = ['Kds Update', function($itemScope) {
          $scope.showKdsInfo( $itemScope.t.bill);
        }];
        if($rootScope.isInstance)
        allValidFeature.push(kdsInfo);
        // -- added by rajat end--//
        return allValidFeature;
      } else {
        if ($scope.selectedTab.tabType === 'table')
          return $scope.menuOptionsWOBill;
        else return false;
      }
    };
    $scope.isCardSystem=function(){
      return Utils.hasSetting('enable_card', $scope.settings)
    }
    $scope.transferKot=function(bill,kotIndex){
      if (Utils.hasSetting('disable_transfer', $scope.settings, 'trankot')) {
        growl.error('Transfer kot is disabled.',{ttl:3000});
        return false;
      }
      $scope.copiedTableBill={};
      $scope.copiedTableBill.bill=angular.copy( bill);
      $scope.copiedTableBill.kotIndex=kotIndex;
      growl.success('Copied selected kot',{ttl:2000});
    }
    $scope.transferItem=function(bill,kotIndex,itemIndex){
      if (Utils.hasSetting('disable_transfer', $scope.settings, 'tranitem')) {
        growl.error('Transfer item is disabled.',{ttl:3000});
        return false;
      }
      $scope.copiedTableBill={};
      $scope.copiedTableBill.bill=angular.copy( bill);
      $scope.copiedTableBill.KIndex=kotIndex;
      $scope.copiedTableBill.itemIndex=itemIndex;
      growl.success('Copied selected item #.',{ttl:2000});
    }
    $scope.setTempManagerName=function(passCode){
      var _f = _.filter($rootScope.users, {passcode:passCode});
        if(_f.length>0){
          $scope.tempUserName=(_.has( _f[0],'firstname')?_f[0].firstname:'');
        }
    }
    $scope.pasteBill=function(index,bill){

      growl.error('This feature has been temporarly disabled!',{ttl:2000});
      return false;

      if(Utils.hasSetting('c_m_p',$scope.settings,'bTran')) {
        $scope.passCodeModal().then(function (passCode) {
          if (passCode === Utils.getPassCode(passCode, $rootScope.users, 'Manager,Admin')) {
            $scope.pasteBillPass(index, bill);
               $scope.setTempManagerName(passCode);
            return false;
          } else {
            growl.error("Wrong Passcode!", {
              ttl: 2000
            });
            return false;
          }
        });
      }else{
        $scope.pasteBillPass(index,bill);
      }
      /* $state.current.data.tables[index].bill=angular.copy( bill);
       $state.current.data.tables[index].bill.processBill();
       $state.current.data.tables[index].bill.updateBill({}, 'update').then(function () {
       growl.info("updated", {ttl: 2000});
       });*/
    };
    $scope.pasteBillPass=function(index,bill){
      if (Utils.hasSetting('disable_transfer', $scope.settings, 'tranbill')) {
        growl.error('Transfer bill is disabled.',{ttl:3000});
        return false;
      }
      var tempKot=[];
      $ngBootbox.confirm('Merging bill is irreversible process,confirm before proceed?').then(function(){
        if($rootScope.isInstance) {

          $rootScope.showLoaderOverlay = true;
          $scope.getFineBillById(bill._id).then(function (offlineBill) {
            console.log(offlineBill.bill.lastModified);
            bill._kots = angular.copy(offlineBill.bill._kots);

            // bill.lastModifed=offlineBill.lastModified;
            _.forEach($scope.copiedTableBill.bill._kots, function (kot) {
              _.extend(kot,{transfer: {status:'transferred',billId:$scope.copiedTableBill._id }});
              tempKot.push(angular.copy(kot));
            })
            _.forEach(bill._kots, function (kot) {
              tempKot.push(angular.copy(kot));
            })
            bill._kots = angular.copy(tempKot);
            bill.processBill();
            _.extend(bill,{lastModified:offlineBill.bill.lastModified});
            bill.updateBill({}, 'update').then(function () {
              //$scope.copiedTableBill.transfer={type:'bill',to:bill._id,_kots:angular.copy($scope.copiedTableBill.bill._kots),_items:[]}
              var transferOb={type:'bill',to:bill._id,_kots:angular.copy($scope.copiedTableBill.bill._kots),_items:[]};
              $scope.copiedTableBill.bill._kots=[];
              $scope.copiedTableBill.bill.processBill();
              $scope.copiedTableBill.bill.updateBill({_closeTime:new Date(),transfer:transferOb},'update').then(function(){
                growl.info("Transferred successfully.", {ttl: 2000});

                //refreshState();
                /*nSocket.emit('table_split_event', {event:'add', type:'table',number:0,deployment_id:localStorageService.get('deployment_id')}, function (data) {
                 //console.log(data);
                 })*/
                //$scope.unlockTable();
                var printText=
                  nSocket.emit('table_event', {
                    event: 'remove',
                    type: 'table',
                    number: $scope.copiedTableBill.bill.tabId + $scope.copiedTableBill.tableIndex,
                    deployment_id: localStorageService.get('deployment_id')
                  }, function (data) {
                  });
                var tempBill=angular.copy($scope.copiedTableBill.bill);
                printer.print('_template_Bill.html', tempBill,{'from':tempBill._tableId,to:bill._tableId,fromTab:tempBill.tab,toTab:bill.tab,type:'bill'});
                $scope.copiedTableBill={};
                $rootScope.showLoaderOverlay = false;
              },function(err){
                growl.error('There is problem updating the transaction.',{ttl:3000});
                if(err.data.error=='inValid'){
                  $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

                  })
                }
              })
            },function(err){
              growl.error('There is problem updating the transaction.',{ttl:3000});
              if(err.data.error=='inValid'){
                $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

                })
              }
            })
          })
        }else{

        }
      })
    }
    $scope.pasteKot=function(bill){
      growl.error('This feature has been temporarly disabled!',{ttl:2000});
      return false;

      //console.log(bill);
      if(Utils.hasSetting('c_m_p',$scope.settings,'kTran')) {
        $scope.passCodeModal().then(function (passCode) {
          if (passCode === Utils.getPassCode(passCode, $rootScope.users, 'Manager,Admin')) {
            $scope.pasteKotPass(bill);
            $scope.setTempManagerName(passCode);
            return false;
          } else {
            growl.error("Wrong Passcode!", {
              ttl: 2000
            });
            return false;
          }
        });
      }
      else{
        $scope.pasteKotPass(bill);
      }
      /* $state.current.data.tables[index].bill=angular.copy( bill);
       $state.current.data.tables[index].bill.processBill();
       $state.current.data.tables[index].bill.updateBill({}, 'update').then(function () {
       growl.info("updated", {ttl: 2000});
       });*/
    };
    $scope.pasteKotPass=function(bill){
      var index=$scope.copiedTableBill.kotIndex;
      var tempKot=[];
      $ngBootbox.confirm('Merging Kot is irreversible process,confirm before proceed?').then(function(){
        if($rootScope.isInstance) {
          $rootScope.showLoaderOverlay = true;
          $scope.getFineBillById(bill._id).then(function (offlineBill) {
            // console.log(offlineBill);
            bill._kots = angular.copy(offlineBill.bill._kots);
            // _.forEach($scope.copiedTableBill.bill._kots, function (kot) {
            var kot=$scope.copiedTableBill.bill._kots[index];
            _.extend(kot,{transfer: {status:'transfer',billId:angular.copy( $scope.copiedTableBill.bill._id) }});
            tempKot.push(angular.copy(kot));
            // })
            _.forEach(bill._kots, function (kot) {
              tempKot.push(angular.copy(kot));
            })
            bill._kots = angular.copy(tempKot);
            _.extend(bill,{"lastModified":offlineBill.bill.lastModified});
            bill.processBill();
            bill.updateBill({}, 'update').then(function () {

              //$scope.copiedTableBill.transfer={type:'bill',to:bill._id,_kots:angular.copy($scope.copiedTableBill.bill._kots),_items:[]}
              var copiedKot=[];
              copiedKot.push($scope.copiedTableBill.bill._kots[index]);
              var transferOb={type:'kot',to:bill._id,_kots:angular.copy(copiedKot),_items:[]};
              $scope.copiedTableBill.bill._kots.splice(index,1);
              $scope.copiedTableBill.bill.processBill();
              $scope.copiedTableBill.bill.updateBill({transfer:transferOb},'update').then(function(){
                growl.info("Transferred successfully.", {ttl: 2000});
                nSocket.emit('table_event', {
                  event: 'remove',
                  type: 'table',
                  number: $scope.copiedTableBill.bill.tabId + $scope.copiedTableBill.bill._tableId,
                  deployment_id: localStorageService.get('deployment_id')
                }, function (data) {
                });
                var tempBill=angular.copy($scope.copiedTableBill.bill);
                tempBill._kots=[];
                tempBill._kots.push(kot);
                var printText="Kot Merge Status\r\n==============================\r\n Kot:"+kot.kotNumber+" of Table :  " + tempBill._tableId + " On " + tempBill.tab + "\r\n Merged to\r\n Table: " + bill._tableId + " On " + bill.tab + "\r\n===============================";
                // printer.print('_template_Bill.html', tempBill,{'from':tempBill._tableId,to:bill._tableId,fromTab:tempBill.tab,toTab:bill.tab,type:'kot'});
                printer.print('_template_Bill.html', tempBill,{text:printText,type:'kot'});
                $scope.copiedTableBill={};
                $rootScope.showLoaderOverlay = false;
              },function(err){
                growl.error('There is problem updating the transaction.',{ttl:3000});
                if(err.data.error=='inValid'){
                  $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

                  })
                }
              })
            })
          })
        }else{

        }
      })

    }
    $scope.pasteItem=function(bill){
      growl.error('This feature has been temporarly disabled!',{ttl:2000});
      return false;

      //console.log(bill);
      if(Utils.hasSetting('c_m_p',$scope.settings,'iTran')) {
        $scope.passCodeModal().then(function (passCode) {
          if (passCode === Utils.getPassCode(passCode, $rootScope.users, 'Manager,Admin')) {
              $scope.pasteItemPass(bill);
              $scope.setTempManagerName(passCode);
              return false;
          } else {
            growl.error("Wrong Passcode!", {
              ttl: 2000
            });
            return false;
          }
        });
      }else{
        $scope.pasteItemPass(bill);
      }
    };

    $scope.pasteItemPass=function(bill){
      var kIndex=$scope.copiedTableBill.KIndex;
      var itemIndex=$scope.copiedTableBill.itemIndex;
      var tempKot=[];
      $ngBootbox.confirm('Merging bill is irreversible process,confirm before proceed?').then(function(){
        if($rootScope.isInstance) {
          $rootScope.showLoaderOverlay = true;
          $scope.getFineBillById(bill._id).then(function (offlineBill) {
            var tempBill=angular.copy($scope.copiedTableBill.bill);
            bill._kots = angular.copy(offlineBill.bill._kots);
            // _.forEach($scope.copiedTableBill.bill._kots, function (kot) {
            var item=$scope.copiedTableBill.bill._kots[kIndex].items[itemIndex];
            _.extend(item,{transfer: {status:'transfer',billId:$scope.copiedTableBill.bill._id }});
            tempKot.push(angular.copy(item));
            // })
            bill._items.push(item);
            bill.generateKotNumber().then(function(kotnumber) {
              //alert(kotnumber);
              bill.kotNumber = kotnumber;
              //  alert(Utils.hasSetting('enable_comment_kot', $scope.settings));
              /* to do validation if mandatory */
              // bill.processBill();
              $scope.currentKotIndex = bill.printKot('transferred item',$scope.getKotWaiter());
              var printText="Item Merge Status\r\n==============================\r\n Item from Kot:"+tempBill._kots[kIndex].kotNumber+" of \r\n Table Number:  " + tempBill._tableId + " On " + tempBill.tab + "\r\n Merged to\r\n Table Number: " + bill._tableId + " On " + bill.tab + "\r\n===============================\r\n";
              _.extend(bill,{"lastModified":offlineBill.bill.lastModified});
              bill.updateBill(null, 'update').then(function() {
                printer.print('_template_KOT.html', {
                  bill: angular.copy(bill),
                  index: $scope.currentKotIndex
                },{kotTText:printText,type:'item'});
                //$scope.copiedTableBill.transfer={type:'bill',to:bill._id,_kots:angular.copy($scope.copiedTableBill.bill._kots),_items:[]}
                var copiedItems = [];
                copiedItems.push($scope.copiedTableBill.bill._kots[kIndex].items[itemIndex]);
                var transferOb = {
                  type: 'item',
                  to: bill._id,
                  _kots: [],
                  _items: angular.copy(copiedItems)
                };
                $scope.copiedTableBill.bill._kots[kIndex].items.splice(itemIndex, 1);
                $scope.copiedTableBill.bill.processBill();
                $scope.copiedTableBill.bill.updateBill({transfer: transferOb}, 'update').then(function () {
                  growl.info("Item transferred successfully.", {ttl: 2000});

                  //refreshState();
                  /*nSocket.emit('table_split_event', {event:'add', type:'table',number:0,deployment_id:localStorageService.get('deployment_id')}, function (data) {
                   //console.log(data);
                   })*/
                  //$scope.unlockTable();
                  nSocket.emit('table_event', {
                    event: 'remove',
                    type: 'table',
                    number: $scope.copiedTableBill.bill.tabId + $scope.copiedTableBill.bill._tableId,
                    deployment_id: localStorageService.get('deployment_id')
                  }, function (data) {
                  });
                  $scope.copiedTableBill = {};
                  $rootScope.showLoaderOverlay = false;
                },function(err){
                  growl.error('There is problem updating the transaction.',{ttl:3000});
                  if(err.data.error=='inValid'){
                    $ngBootbox.alert('Transaction you are trying to update has already been updated !!').then(function(){

                    })
                  }
                });
              })
            })
          })
        }else{

        }
      })
    }




    /*  $scope.transferTableByTab=function(bill,tableId){
     console.log(tableId);
     }*/
    /*  $scope.transferBill=function(bill){
     //alert(bill._tableId);
     $modal.open({
     templateUrl: 'app/billing/item/_billTransferAcrossTable.html',
     controller:['$scope', '$modalInstance', 'bill','tabs' ,function($scope, $modalInstance, bill,tabs) {
     $scope.bill = bill;
     $scope.tabs=tabs;
     $scope.cancel = function() {
     $modalInstance.dismiss('cancel');
     };
     }],
     resolve: {
     bill: function() {
     return bill;
     },
     tabs:function(){
     return $scope.tabs;
     }
     },
     size: 'md'
     });

     }*/
    $scope.rePrintBillFunction=function($itemScope){
      if($scope.checkForComment(angular.copy( $itemScope.t.bill))){
        $ngBootbox.prompt('Feed in comment for reprint').then(function(cmt){
          if(cmt==null||cmt==''||cmt==undefined){
            growl.error('Comment is mandatory for reprint',{ttl:3000});
            $scope.isBillingOn=false;
            return false ;
          }
          $scope.rePrintBillFunctionPost($itemScope,cmt);
        },function(err){
          //alert();
          $scope.isBillingOn=false;
          growl.error('Comment is mandatory for reprint',{ttl:3000});
        })
      }else{
          $scope.rePrintBillFunctionPost($itemScope);
        }
    }

    $scope.rePrintBillFunctionPost=function($itemScope,cmt){
      if (Utils.hasSetting('enable_passcode', $scope.settings, 'reprintbill')) {
        $scope.passCodeModal().then(function (res) {
          if (res != Utils.getPassCode(res, $rootScope.users, 'Manager,Admin')) {
            growl.error('Wrong Passcode!', {
              ttl: 2000
            });
            return false;
          }else{
            $scope.setTempManagerName(res);
          }
          var _f = _.filter($rootScope.users, {passcode:res});
              if(_f.length>0){
                  $scope.passCodeContextUser=angular.copy((_.has( _f[0],'username')?_f[0].username:''));
              }
          var ptime = new Date();
          // $scope.bill.billPrintTime = ($scope.bill.billPrintTime==null ? ptime:$scope.bill.billPrintTime);
          $itemScope.t.bill.rePrint.push({
            user: angular.copy( ($scope.passCodeContextUser == ''?currentUser.username:angular.copy($scope.passCodeContextUser))),
            printTime: ptime,
            comment:cmt
          });
          //alert($scope.passCodeContextUser);
          $scope.passCodeContextUser='';
          $itemScope.t.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function (preparedBill) {
            var billToPrint = preparedBill;
            //$scope.getPrefixByBill(  $scope.bill._id);
            //printer.print('_template_Bill.html', billToPrint);
             if (Utils.hasSetting('bill_print_template', $scope.settings)) {
                    var bill_print_template = parseInt(Utils.getSettingValue('bill_print_template', $scope.settings))
                    if(bill_print_template==1)
                    BillPrintService.templateBillPrintWithTaxBaseAmount(billToPrint);
                    else if(bill_print_template==2 && Utils.hasSetting('inclusive_tax', $scope.settings))
                    BillPrintService.templateBillPrintInclusiveTaxes(billToPrint);
                    else
                    printer.print('_template_Bill.html', billToPrint);
                }
            else{
                  printer.print('_template_Bill.html', billToPrint);
              }

          });
          $scope.rePrintNotification($itemScope.t.bill,cmt);
        }, function (err) {
          alert(err);
        })

      } else {
        //alert(Utils.hasSetting('enable_passcode',$scope.settings,'reprintbill'));
        //return false;
        var ptime = new Date();
        // $scope.bill.billPrintTime = ($scope.bill.billPrintTime==null ? ptime:$scope.bill.billPrintTime);
        $itemScope.t.bill.rePrint.push({user: $scope.getPassCodeUserPre(), printTime: ptime,comment:cmt});

        $itemScope.t.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function (preparedBill) {
          var billToPrint = preparedBill;
          //printer.print('_template_Bill.html', billToPrint);
           if (Utils.hasSetting('bill_print_template', $scope.settings)) {
                    var bill_print_template = parseInt(Utils.getSettingValue('bill_print_template', $scope.settings))
                    if(bill_print_template==1)
                    BillPrintService.templateBillPrintWithTaxBaseAmount(billToPrint);
                    else if(bill_print_template==2 && Utils.hasSetting('inclusive_tax', $scope.settings))
                    BillPrintService.templateBillPrintInclusiveTaxes(billToPrint);
                    else
                    printer.print('_template_Bill.html', billToPrint);
                }
            else{
                  printer.print('_template_Bill.html', billToPrint);
              }

        });
        $scope.rePrintNotification($itemScope.t.bill,cmt);
      }
    }

    $scope.getTaxBifurcation=function(){
      var manager='';
          if (Utils.hasSetting('remove_tax_manually', $scope.settings)) {
        if( !$scope.selectedManager) {
          $scope.passCodeModal().then(function (passCode) {
            if (passCode === Utils.getPassCode(passCode, $rootScope.users, 'Manager,Admin')) {
              //$scope.expressDiscountApply(size, item, index, fromKot, kIndex, true);
              //$scope.setTempManagerName(passCode);
              var _f = _.filter($rootScope.users, {passcode:passCode});

                if(_f.length>0){
                  manager=(_.has( _f[0],'firstname')?_f[0].firstname:'');
                }
                $scope.getTaxBifurcationPass($scope.bill,manager);
            } else {
              growl.error("Wrong Passcode!", {
                ttl: 2000
              });
              $scope.getTaxBifurcationPass($scope.bill,manager);
              //return;
            }

          });
        }else{
          $scope.getTaxBifurcationPass($scope.bill.manager);
        }
      }else{
          $scope.getTaxBifurcationPass($scope.bill.manager);
      }
    }

    $scope.getTaxBifurcationPass = function(bill,manager) {
     var modalInstance=$modal.open({
        templateUrl: 'app/billing/item/_taxBifurcation.html',
        controller: function($scope, $modalInstance, bill,removeTaxFlag,managerName) {
          $scope.bill = bill;
          var allTaxes = [];
          $scope.removeTaxFlag=removeTaxFlag;
         // console.log($scope.removeTaxFlag);
          //console.log($scope.bill);
          /*  _.forEach($scope.bill._taxes,function(tax){
           //allTaxes.push(angular.copy(tax));
           });*/
          _.forEach($scope.bill._kotsTaxes, function(tax) {
            allTaxes.push(angular.copy(tax));
          });
          console.log(_.groupBy(allTaxes, 'name'));
          $scope.consolidatedTax = [];
          _.chain(allTaxes).groupBy('name').map(function(_t) {
            var t_Tax_Amount = 0;
            _.forEach(_t, function(tx) {
              t_Tax_Amount += parseFloat(tx.tax_amount);
            });
            var taxOb = {
              name: _t[0].name,
              tax_amount: t_Tax_Amount
            };
            $scope.consolidatedTax.push(angular.copy(taxOb));
          });
          console.log($scope.consolidatedTax);

          $scope.removeTax=function(index){

            var tempTaxes=[];
            for(var i=0;i<$scope.bill._kots.length;i++){
              for(var j=0;j<$scope.bill._kots[i].taxes.length;j++){
                if($scope.consolidatedTax[index].name==$scope.bill._kots[i].taxes[j].name){
                  tempTaxes.push({name:$scope.bill._kots[i].taxes[j].name,
                                  id:$scope.bill._kots[i].taxes[j].id,userName:managerName
                                 });
                  break;
                }
              }
              if(tempTaxes.length>0) break;
            }
            console.log(tempTaxes);
           if(tempTaxes.length>0){
            $scope.bill.removeTax(tempTaxes[0]);
            $scope.consolidatedTax.splice(index,1);
            $modalInstance.close({'reopen':true});
           }
          }

          $scope.removeCharge=function(index){
           if(!_.has($scope.bill.charges,'deleted')){
              $scope.bill.charges.deleted=[];
              $scope.bill.charges.deleted.push(angular.copy($scope.bill.charges.detail[index]));
            }else{
              $scope.bill.charges.deleted.push(angular.copy($scope.bill.charges.detail[index]));
            }
            $scope.bill.charges.detail.splice(index,1);
            $modalInstance.close('close');
          }
           $scope.addCharge=function(index){
           if(!_.has($scope.bill.charges,'detail')){
              $scope.bill.charges.detail=[];
              $scope.bill.charges.detail.push(angular.copy($scope.bill.charges.deleted[index]));
            }else{
              $scope.bill.charges.detail.push(angular.copy($scope.bill.charges.deleted[index]));
            }
            $scope.bill.charges.deleted.splice(index,1);
            $modalInstance.close('close');
          }

          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
        },
        resolve: {
          bill: function() {
            return $scope.bill
          },
          removeTaxFlag:function(){
            //console.log('remove',Utils.hasSetting('remove_tax_manually',localStorageService.get('settings'))&& $scope.bill._kots.length>0);
            var flag=false;
            if(manager==''){
              return false;
            }else
              return Utils.hasSetting('remove_tax_manually',localStorageService.get('settings'))&& $scope.bill._kots.length>0;
          },
          managerName:function(){
            return manager
          }
        },
        size: 'sm'
      });

      modalInstance.result.then(function (status) {
       if(status.reopen==true)
         $scope.getTaxBifurcation();
      });

    };


    /*Apply Offer  start here*/
    $scope.validatedOffers=[];
    $scope.selectedOffer=null;
    $scope.allAppliedOffers=[];
    $scope.openOfferPage=function(){
      if (Utils.hasSetting('enable_passcode_apply_discount', $scope.settings)) {
        if(!$scope.selectedManager) {
          $scope.passCodeModal().then(function (passCode) {
            if (passCode === Utils.getPassCode(passCode, $rootScope.users, 'Manager,Admin')) {
              $scope.openOfferPagePass();
              $scope.setTempManagerName(passCode);
            } else {
              growl.error("Wrong Passcode!", {
                ttl: 2000
              });
              return;
            }

          });
        }else{
          $scope.openOfferPagePass();
        }
      } else {
        /* No Authentication Required */
        $scope.openOfferPagePass();
      }
    }
    $scope.openOfferPagePass=function() {
      if ($scope.selectedOffer != null) {
        $scope.showBootBox('Selected offer not completed yet! want to scrap? ').then(function (status) {
          if (status) {
            _.forEach($scope.bill._items, function (bItem) {
              if (_.has(bItem, 'offer')) {
                if (bItem.offer._id == $scope.selectedOffer.offer.offer._id) {
                  if($scope.code!=null){
                    if($scope.selectedOffer.offer.offer._id==$scope.code.offer._id){
                      $scope.code=null;
                    }
                  }
                  delete bItem.offer;
                  delete bItem.discounts;
                  delete bItem.isDiscounted;
                }
              }
            });
            $scope.selectedOffer=null;
            var _fTab= _.filter($rootScope.tabs,{_id:$scope.selectedTab.tabId});
            if(_fTab.length>0){
              $scope.renderItemsTabWise(_fTab[0],$rootScope.rootMasterItems);
            }
            $scope.showOfferComment();
          }
          else {
            return false;
          }
        })
      }else{
        $scope.showOfferComment();
      }
      intercom.registerEvent('Offer');
    }

    $scope.scrapOfferFromBill=function(offerId){
      if($scope.isCardSystem()){
        if($scope.billStatus=='Settle'){
          $scope.billStatus='Settle' ;
          growl.error('Can not scrap offer after print.',{ttl:2000});
          return false;
        }
      }

      _.forEach($scope.bill._items,function(bItem){
        if(_.has(bItem,'offer')){
          if(bItem.offer._id==offerId){

            delete bItem.offer;
            delete bItem.discounts;
            delete bItem.isDiscounted;
            if($scope.code!=null){
              if(offerId==$scope.code.offer._id){
                $scope.code=null;
              }
            }
          }
        }
      });
      _.forEach($scope.bill._kots,function(kot){
        _.forEach(kot.items,function(bItem){
          if(_.has(bItem,'offer')){
            if(bItem.offer._id==offerId){
              delete bItem.offer;
              delete bItem.discounts;
              delete bItem.isDiscounted;
              if($scope.code!=null){
                if(offerId==$scope.code.offer._id){
                  $scope.code=null;
                }
              }
            }
          }
        })
      });
      // console.log( $scope.bill);
      if(_.has($scope.bill,'_offers')) {
        if ($scope.bill._offers.length > 0) {
          for (var i = 0; i < $scope.bill._offers.length; i++) {
            if ($scope.bill._offers[i].offer._id == offerId) {
              $scope.bill._offers.splice(i,1);
              $scope.bill.complimentary=false;
              $scope.bill.complimentaryComment="";
              $scope.bill.billDiscountAmount = 0;
              if($scope.code!=null){
                if(offerId==$scope.code.offer._id){
                  $scope.code=null;
                }
              }
              break;
            }
          }
        }
      }
      $scope.bill.processBill();
      if (Utils.hasSetting('enable_card', $scope.settings)) {
            $scope.bill.cardItemsCalculationWithOffer().then(function(response){
              console.log(response);
              if(response.netAmount==0){
               // $scope.printBillPostCard(isSettled,cmt);
              }else{
                $scope.printKot(true,true);
              }
            })

        }

    }
    $scope.isBillWiseOffer=function(offer){
      var flag=true;
      if(offer.type.name=='item'|| offer.type.name=='comboOffer'){flag=false;}
      if(offer.applicable.on=='item'||offer.applicable.on=='itemCriteria') {
        if (!(offer.applicable.on == 'itemCriteria' && offer.applicable.isExclude)) {
          flag = false;
        }
      }
      return flag;
    }
    $scope.offlineSale={daySale:0,unsettle:0};

    $scope.getDaySale=function(){
      if(!$rootScope.isInstance) {
        var saleAmount = 0;
        //  var d=$q.defer();
        var resettime = new Date(Utils.getSettingValue('day_sale_cutoff', localStorageService.get('settings'))).getHours();
        ;
        // console.log('resettime:'+resettime);
        var fdayb = 0;
        var tdayb = 1;
        if (resettime > 0) {
          var currentdate = new Date();
          if (currentdate.getHours() > 0 && currentdate.getHours() < resettime) {
            fdayb = -1;
            tdayb = 0;
          }
        }
        var query = "select sum(totalBill) as amount from bills where isVoid <>'true' and  created between (select datetime( datetime( date(datetime('now','localtime'),'" + fdayb + " day')),'+" + resettime + " hour') ) and (select datetime( datetime( date(datetime('now','localtime'),'" + tdayb + " day')),'+" + resettime + " hour')) and closetime is not null";
        $rootScope.db.selectDynamic(query).then(function (result) {
          var saleAmount = 0;
          if (result.rows.length > 0) {
            saleAmount = result.rows.item(0).amount;
          }
          $scope.offlineSale.daySale = saleAmount;
          //    d.resolve( saleAmount);
          var uquery = "select sum(totalBill) as amount from bills where  closetime is  null";
          $rootScope.db.selectDynamic(uquery).then(function (uresult) {
            var usaleAmount = 0;
            if (uresult.rows.length > 0) {
              usaleAmount = uresult.rows.item(0).amount;
            }
            $scope.offlineSale.unsettle = usaleAmount;
             $scope.unsettledUpdateToServer(usaleAmount);
            //    d.resolve( saleAmount);
          })
        })
      }
      //return d.promise;

    }
    $scope.getDaySale()
    ///////////////////Edit Bill Start Here/////////////////////
    $scope.startEdit=function() {
      $scope.isEditBill=true;
      //console.log($rootScope.editBill);

      $scope.editBill=angular.copy( $rootScope.editBill);
      delete $rootScope.editBill;
      $scope.isOnlineBill=false;
      $scope.showBillPage = true;
      $scope.categories = [];
      GetMasterItems();
      /* Build Categories Filter */
      _.forEach($scope.masterItems, function(i) {
        $scope.categories.push(i.category);
      });
      var _uCats = _.uniq($scope.categories, 'categoryName');
      $scope.categories = _uCats;
      $scope.categories = _.sortBy($scope.categories, ['categoryName']);
      renderCategoriesT();
      /*console.log($state.current.data.tables[parseInt(toParams.tableId)]);*/
      /* var _filterdT = _.filter($state.current.data.openedBills, {
       "id": billNumber
       })
       var _t = {};
       if (_filterdT[0].id === "New") {
       */
      // $scope.renderRefreshNewBill();
      $scope.bill = new Bill(null, null, null, (($scope.selectedTab.tabType === 'takeout') ? true : null), (($scope.selectedTab.tabType === 'delivery') ? true : null), currentUser, $rootScope.db, $scope.selectedTab.tabName, $scope.selectedTab.tabId, $scope.selectedTab.tabType); //_t.bill;
      /*    //console.log($scope.bill);
       } else {
       $scope.bill = _filterdT[0].bill;
       }*/
      /*delete $scope.editBill.creditCardForm ;
       delete $scope.editBill.debitCardForm ;
       delete $scope.editBill.otherForm ;
       delete $scope.editBill.payments ;
       delete $scope.editBill._cash ;*/
      $scope.editBill.creditCardForm = {};
      $scope.editBill.debitCardForm = {};
      $scope.editBill.otherForm = {};
      // Settlement
      $scope.editBill.payments = {cash: [], cards: []};
      $scope.editBill._debitCards=[];
      $scope.editBill._creditCards=[];
      $scope.editBill._cash=[];
      _.extend($scope.bill,$scope.editBill);
      $scope.bill.isPrinted=false;
      $scope.bill.id=angular.copy( $scope.bill._id);
      $scope.bill.aggregateKotTaxes();
      if($scope.bill._customer.firstname!=undefined){
        $scope.billCustomer=$scope.bill._customer;
      }
      if($scope.bill._offers.length>0){
        $ngBootbox.alert('Offer applied on this will work on sole discretion of the available items of offer.');
      }
      $scope.handleBill();

    }
    ///////////////////Edit Bill End Here///////////////////////

    /*Advance booking starts here*/
    $scope.submitAdvance=function(){
      $modal.open({
        templateUrl: 'app/billing/item/advanceBooking.html',
        controller: ['$scope', '$modalInstance','advance','growl','isEditAdvance','amount', function ($scope, $modalInstance, advance ,growl,isEditAdvance,amount) {
          console.log(isEditAdvance);
          $scope.isEditAdvance=isEditAdvance;
          $scope.advance={amount:'',datetime:''};
          if(advance.amount!=undefined){$scope.advance.amount=advance.amount};
          if(advance.datetime!=undefined){$scope.advance.datetime=advance.datetime};
          // console.log(advance);
          $scope.openDatePicker = function($event, opened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[opened] = true;
          };

          $scope.cancel=function(){
            $modalInstance.dismiss('cancel');
          }
          $scope.submit=function(){
            try{
              if(!Utils.convertDate($scope.advance.datetime)){
                growl.error('date time error.',{ttl:3000});
                return false;
              }else{
                if(Utils.getDateFormatted($scope.advance.datetime)<Utils.getDateFormatted(new Date())){
                  growl.error('Booking in back date is not allowed.',{ttl:3000});
                  return false;
                }
              }
            }
            catch(err){
              growl.error('date time error.',{ttl:3000});
              return false;

            }
            if(parseInt($scope.advance.amount)==NaN||$scope.advance.amount<0){        // accept zero as advance amount
              growl.error('amount error.',{ttl:3000});
              return false;
            }else{
              if($scope.advance.amount>amount){
                growl.error('advance amount exceeding the bill amount.',{ttl:3000});
                return false;
              }
            }
            var bookingOb={advance:$scope.advance,isEdit:$scope.isEditAdvance};
            $modalInstance.close( bookingOb);
          }

        }],
        resolve: {
          advance: function () {
            return $scope.bill.advance;
          },
          isEditAdvance:function(){
            return $scope.isEditAdvance;
          },
          amount:function(){
            return $scope.bill.calculateAdvanceAmount($scope.bill);
          }
        }
      }).result.then(function(advanceOb) {
        console.log(advanceOb.isEdit);
        $scope.bill.advance = advanceOb.advance;
        if (advanceOb.isEdit) {
          $scope.bill.id = $scope.bill._id
        } else {
          delete $scope.bill._id;
        }
        /*  _.forEach($scope.bill.items,function(){

         })*/
        if(advanceOb.isEdit){
          Advancebooking.update($scope.bill, function (advBookings) {
            growl.success("Advance booking updated.", {ttl: 3000});
            $scope.initializeTab();
          })
        }else {
          Advancebooking.save($scope.bill, function (advBookings) {
            $scope.bill.prepareAggregation($scope.settings, $scope.selectedTab.tabType).then(function(preparedBill) {
              $scope.billToPrint = preparedBill;
              printer.print('_template_Bill.html', $scope.billToPrint);
            });
            growl.success("Advance booking saved.", {ttl: 3000});
            $scope.initializeTab();
          })
        }
      })
      //  console.log($scope.bill);
    }
    $scope.advanceBookings=[];
    $scope.showBooking=function() {
      if ($scope.isConnected) {
        Advancebooking.getAdvanceByDeployment({deployment_id: localStorageService.get('deployment_id')}, function (advs) {
          //console.log(advs);
          $scope.advanceBookings = advs;
          $scope.showModalAdvance();
        })
      }
    }

        $scope.showModalAdvance = function () {
          var dates = [];
          _.forEach($scope.advanceBookings, function (adv) {
            if (_.has(adv, 'advance')) {
              if (_.has(adv.advance, 'datetime')) {
                dates.push(new Date(adv.advance.datetime));
              }
            }
          })
          console.log(dates);
          $modal.open({
            templateUrl: 'app/billing/item/advance_booling_calender.html',
            controller: ['$scope', '$modalInstance', 'growl', 'dates', function ($scope, $modalInstance, growl, dates) {
              $scope.selectedDays = dates;  //= [new Date(2015,5,2), new Date(2015,5,10)];  //selectedDays;
              //  $scope.selectedDays.push(new Date());
              /* $scope.highlightDays = [
               {date: new Date(), css: 'holiday', selectable: false,title: 'Holiday time !'},
               {date: moment().date(14).valueOf(), css: 'off', selectable: false,title: 'We don\'t work today'},
               {date: moment().date(25).valueOf(), css: 'birthday', selectable: true,title: 'I\'m thir... i\'m 28, seriously, I mean ...'}
               ];*/
              $scope.logInfos = function (event, date) {
                event.preventDefault(); // prevent the date.hover to be toggled
                // console.log(date._d);
                $modalInstance.close(date._d);
                if (event.type == 'mouseover') {

                  //we're on it!
                }
              };

              $scope.cancel = function () {
                $modalInstance.dismiss();
              }
            }],
            size: 'md',
            resolve: {
              dates: function () {
                return dates;
              }
            }
          }).result.then(function (selectedDay) {
            //console.log(selectedDay);
            $scope.showBookingDateWise(selectedDay);
          });
        }

    $scope.showBookingDateWise=function(date) {
      var bookings = [];
      _.forEach($scope.advanceBookings, function (adv) {
        if (_.has(adv, 'advance')) {
          if (_.has(adv.advance, 'datetime')) {
            if (Utils.getDateFormattedDate(new Date(adv.advance.datetime)) == Utils.getDateFormattedDate(new Date(date))) {
              bookings.push(adv);
            }
          }
        }
      })

      $modal.open({
        templateUrl: 'app/billing/item/advance_billing_datewise.html',
        controller: ['$scope', '$modalInstance', 'growl', 'bookings', function ($scope, $modalInstance, growl, bookings) {
          $scope.bookings = bookings;
          /*$scope.calculateAdvanceAmount=function(booking){
           // console.log(booking);
           var tAmount=0;
           _.forEach(booking._items,function(item){
           tAmount+=item.subtotal;
           if(_.has(item,'discounts')){
           _.forEach(item.discounts,function(dis){
           if(_.has(dis,'discountAmount')){
           tAmount-=dis.discountAmount;
           }
           })
           }
           })
           _.forEach(booking._taxes,function(tax){
           console.log(tax);
           tAmount+=parseFloat( tax.tax_amount);
           })
           if(_.has(booking,'billDiscountAmount')){
           tAmount-=booking.billDiscountAmount;
           }
           return Utils.roundNumber( tAmount,0);
           }*/
          $scope.calculateAdvanceAmount = function (booking) {
            var bill = new Bill();
            return bill.calculateAdvanceAmount(booking);
          };
          $scope.editBooking = function (booking) {
            var tempOb = {eventType: 'edit', booking: booking};
            $modalInstance.close(tempOb);
          }
          $scope.deleteBooking = function (booking) {
            var tempOb = {eventType: 'delete', booking: booking};
            $modalInstance.close(tempOb);
          }
          $scope.cancel = function () {
            $modalInstance.dismiss();
          }
        }],
        size: 'md',
        resolve: {
          bookings: function () {
            return bookings;
          }
        }
      }).result.then(function (selectedBooking) {
        if (selectedBooking.eventType == 'edit') {
          //$scope.adv="Edit Advance";
          $scope.selectedTab.tabId = selectedBooking.booking.tabId;
          $scope.selectedTab.tabName = selectedBooking.booking.tab;
          $scope.selectedTab.tabType = selectedBooking.booking.tabType;
          var tab = {
            _id: $scope.selectedTab.tabId,
            tabName: $scope.selectedTab.tabName,
            tabType: $scope.selectedTab.tabType
          };
          $scope.bill = new Bill(null, null, null, (($scope.selectedTab.tabType === 'takeout') ? true : null), (($scope.selectedTab.tabType === 'delivery') ? true : null), currentUser, $rootScope.db, $scope.selectedTab.tabName, $scope.selectedTab.tabId, $scope.selectedTab.tabType); //_t.bill;

          GetMasterItems();
          $scope.renderItemsTabWise(tab, $rootScope.rootMasterItems);
          /* Build Categories Filter */
          _.forEach($scope.masterItems, function (i) {
            $scope.categories.push(i.category);
          });

          var _uCats = _.uniq($scope.categories, 'categoryName');
          $scope.categories = _uCats;
          $scope.categories = _.sortBy($scope.categories, ['categoryName']);
          renderCategoriesT();
          _.extend($scope.bill, selectedBooking.booking);
          //$scope.bill=selectedBooking.booking;
          if ($scope.bill._offers.length > 0) {
            $ngBootbox.alert('Offer applied on this will work on sole discretion of the available items of offer.');
          }
          $scope.showBillPage = true;
          $scope.showAdvance = true;
          $scope.isEditAdvance = true;
          //$scope.showBillButton=false;
        }
        if (selectedBooking.eventType == 'delete') {
          $ngBootbox.confirm('Are you sure to delete the selected booking?').then(function () {
            Advancebooking.delete({}, {_id: selectedBooking.booking._id}, function (status) {
              growl.success('Selected booking delete from server.', {ttl: 2000});
            })
          })

        }
      });
    }
    $scope.setShowBooking=function(){
      return (Utils.hasSetting('enable_advance_booking', $scope.settings))

    }

   $scope.filterAddons=function(itemId){
      var filteredAddons=[];
      var f_t_addons= _.filter($scope.addons,{tabId:$scope.selectedTab.tabId});
      if(f_t_addons.length>0){
        _.forEach( f_t_addons[0].Items,function(aitem){
          var f_i= _.filter(aitem.mapItems,{_id:itemId})
          if(f_i.length>0){
           // console.log(aitem);
            var newItem = {
          _id : aitem._id,
          name : aitem.name,
          unit : aitem.unit,
          stockQuantity : aitem.stockQuantity,
          rate:aitem.rate,
          category : {
            categoryName : aitem.category.categoryName,
            superCategory:{superCategoryName:aitem.category.superCategory.superCategoryName}
          }
        }
            filteredAddons.push(newItem);
          }
        })
      }
      return filteredAddons;
    }

   $scope.removeAddon = function (itemIndex, addonIndex) {
      //console.log(itemIndex,index);
      if (Utils.hasSetting('addon_compulsory', $scope.settings))
      {
        if( $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns.length>1)
        {
          $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns.splice(addonIndex, 1);
          $scope.bill.processBill();
        }
        else
        {
          growl.error("Atleast 1 Addon Compulsory",{ttl:3000});
        }

      }
      else
      {
        $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns.splice(addonIndex, 1);
        $scope.bill.processBill();
      }

    }



    $scope.incrementAddOnQuantity = function (itemIndex, addonIndex) {
      if (Utils.hasSetting('max_q_addon', $scope.settings)) {
        var maxAddon = parseInt(Utils.getSettingValue('max_q_addon', $scope.settings))
        if($scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns[addonIndex].quantity<maxAddon)
        {
          $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns[addonIndex].quantity += 1;
          $scope.bill.processBill();
        }
        else
        {
          growl.error("Maximum Quantity of each AddOn Exceed", {ttl: 3000})
        }

      }
      else
      {
        $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns[addonIndex].quantity += 1;
        $scope.bill.processBill();
      }
    }

       $scope.decrementAddOnQty = function (itemIndex, addonIndex) {
      if (Utils.hasSetting('addon_compulsory', $scope.settings)) {
        if ($scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns[addonIndex].quantity > 1)
          $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns[addonIndex].quantity -= 1;
        else
        {
          if($scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns.length>1)
          {
            $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns.splice(addonIndex, 1);
          }
          else
          {
            growl.error("Atleast 1 Addon Compulsory",{ttl:3000});
          }
        }

        $scope.bill.processBill();
      }
      else {
        if ($scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns[addonIndex].quantity > 1)
          $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns[addonIndex].quantity -= 1;
        else
          $scope.bill._items[($scope.bill._items.length - 1) - itemIndex].addOns.splice(addonIndex, 1);

        $scope.bill.processBill();
      }
    }

    $scope.showLockedTables=function() {
      if($scope.isConnected) {
        $modal.open({
          keyboard: false,
          templateUrl: 'app/billing/item/unlockTables.html',
          controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {
            $scope.cancel = function () {
              $modalInstance.dismiss('')
            };
            $scope.unlock = function () {
              $scope.cancel = $modalInstance.close('done');
            }
          }],
          size: 'sm'
        }).result.then(function (bills) {
          if (bills == 'done') {
            $http.get($rootScope.url + '/api/synckots/updateLockBill?tables=[]&nonTables=[]&deployment_id=' + localStorageService.get('deployment_id')).then(function (syncLock) {
              //window.reload();
              $scope.tableLock = [];
              $scope.nonTableLock = [];
            })
          }
        })
      }
    }

    $scope.offerNotification=function(type,message){
      //  alert();
      if(Utils.hasSetting('enable_notification',$scope.settings)) {
        var body = {deployment_id: localStorageService.get('deployment_id'), message: message, type: type};
        // var body={deployment_id:'555baa76034de05173e9fe3b',message:message,type:type};

        $http.post('/api/notifications/createNMessage', body).then(function (result) {
          console.log(result);
        });
        /* $http.post('/api/notifications/createNMessageForIOS', body).then(function (result) {
         console.log(result);
         });*/
      }
      $scope.tempUserName=null;
    }
    /* var externalLinks = document.querySelectorAll('a[href^="/"]');
     for (var i = 0; i < externalLinks.length; ++i) {
     var link = externalLinks[i];
     link.addEventListener('click', function(e) {
     alert();
     });
     }*/
    $scope.$on('$locationChangeStart', function (event, nextPage, currentPage) {
      //console.log(nextPage);
      // alert();
      if($rootScope.isInstance){
        $scope.unlockTable();
      }
    });
    //---ADDED By RAJAT for intercom
   intercom.registerEvent('Billing');
    //---ADDED BY RAJAT END---//
//---added by rajat for kds---//
  nSocket.on('syncKot_update'+localStorageService.get('deployment_id'),function(billData){
      var flag=0;
      console.log("socket",billData);
      if($scope.bill && $scope.bill._id==billData.update.billId){
        _.forEach(billData.update.items,function(item){
          for(var i=0;i<$scope.bill._kots.length;i++) {
          if($scope.bill._kots[i].kotNumber==item.kotNumber){
            for(var j=0;j<$scope.bill._kots[i].items.length;j++){
              if($scope.bill._kots[i].items[j]._id==item.itemId){
                $scope.bill._kots[i].items[j].status=item.status;
                flag=1;
                break;
              }
            }
            if(flag==1)break;
          }
         }
      })
        console.log("new bill",$scope.bill);
    }

      growl.info('Item updated in '+ billData.update.tabName+' - '+billData.update.tableNumber ,{ttl:4000});
    });

    function showNotificationFromKds(update){
           try{
        var modalInstance = $modal.open({
          templateUrl: 'app/billing/kdsUpdate.html',
          resolve:{
            update:function(){
              return update;
            }
          },
          controller: ['$rootScope', '$scope', '$modalInstance','update', function ($rootScope, $scope, $modalInstance,update) {
            $scope.update=update;

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

          }],
          size: 'lg'
        });

        modalInstance.result.then(function () {
          console.log("map closed");
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      }
      catch(e){
        console.log("Exception"+e);
      }
    }

     $scope.showKdsInfo=function(bill){
       $scope.getFineBillById(bill._id).then(function (offlineBill) {
            //$state.current.data.tables.splice(Utils.arrayObjectIndexOf($state.current.data.tables, 'id', tableId), 1);
            if (offlineBill == undefined) {
              growl.error('Could not find info for this bill!', {ttl: 6000});
              return false;
            }

        try{
        var modalInstance = $modal.open({
          templateUrl: 'app/billing/kdsInfo.html',
          resolve:{
            offlineBill:function(){
              return offlineBill.bill;
            }
          },
          controller: ['$rootScope', '$scope', '$modalInstance','offlineBill', function ($rootScope, $scope, $modalInstance,offlineBill){
            $scope.bill=offlineBill;
            console.log("kds bill",$scope.bill);
            $scope.items=[];
            _.forEach($scope.bill._kots,function(kot){
              $scope.items.push({kotNumber:'Kot Number : '+kot.kotNumber.toString()});
              _.forEach(kot.items,function(item){
                 var data={};
                 data.itemName=item.name;
                 if(!item.status || item.status.name=='rejected')
                  data.qty='NA';
                 else if(item.status)
                  data.qty=item.status.quantity;
                else data.qty='NA';

                data.punchedQty=item.quantity;

               data.status=item.status?item.status.name:'NA';
               if(data.status=='closed')
                data.status='Done';
               if(item.status && item.status.rejectionReason)
                data.status+=' due to '+item.status.rejectionReason;
              if(item.addOns)
                data.addOns=item.addOns;
              $scope.items.push(data);
              })
            })
            console.log($scope.items) ;

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

          }],
          size: 'lg'
        });

        modalInstance.result.then(function () {
          console.log("map closed");
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      }
      catch(e){
        console.log("Exception"+e);
      }
      });

     }
    //---added by rajat for kds---//
    //---ADDED BY RAJAT  for payments---//
    /*Payment Integration Starts here*/
    //---new payment integrations---//


    function closeBills(bill){
      if(bill.onlineTransaction && bill.onlineTransaction.billData) {
        var paymentType = bill.onlineTransaction.paymentType, paymentData = bill.onlineTransaction.billData;
        if (paymentType == 'mobikwik') closeMobikwikBill(paymentData);
        else if (paymentType == 'ruplee') closeRupleeBill(paymentData);
        else if (paymentType == 'binge') closeBingeBill(paymentData, bill._id);
        else if (paymentType == 'paytm') closePaytmBill(paymentData);
      }
    }

    function closeRupleeBill(rupleeBill){
      var present=false;
      var ruplee_settings = _.filter(localStorageService.get('settings'), {"name": "Ruplee MerchantId","selected": true});
      if(ruplee_settings.length>0) {

        $http({
          method: 'GET',
          url: '/api/payments/ruplee/delete',
          params: {deployment_id: localStorageService.get('deployment_id'), billId: rupleeBill.billId}
        }).success(function (data) {
          //updateBillAfterClose();
          console.log("successfully deleted ruplee bill");
        }).error(function (err) {
          console.log(err);
        })
      }
    }

    function closeMobikwikBill(mobikwikBill){

      var data = {
        merchantid: mobikwikBill.merchant_id,
        storeid: mobikwikBill.store_id,
        posid: mobikwikBill.pos_id,
        orderid: mobikwikBill.order_id,
        filter: {
          deployment_id: localStorageService.get('deployment_id'),
          tenant_id: localStorageService.get('tenant_id')
        },
        secretKey: mobikwikBill.secret_key
      }

      $http({
        method: 'POST',
        url: '/api/payments/mobikwik/delete',
        data: data
      }).success(function (data) {
        console.log(data);
        if (data.txnstatuscode == 0) {
          console.log("deleted");
          //updateBillAfterClose();
        }
      }).error(function (err) {
        console.log("error in deleting mobikwikBill bill", err)
      })
    }

    function closeBingeBill(bingeBill,billId){
      console.log("bingeBill",bingeBill);
      var binge_username = _.filter(localStorageService.get('settings'), {"name":"binge_username" ,"selected":true});
      var binge_password = _.filter(localStorageService.get('settings'), {"name":"binge_password" ,"selected":true});
      if(binge_username.length>0 && binge_password.length>0) {
        var username=binge_username[0].value;
        var password=binge_password[0].value;

        //  request.client.datetime = (new Date(_bill._created)).toISOString().substring(0, 19).replace('T', ' ');

        bingeBill.bill.client.order.orderno=bingeBill.bingeOrderNo;
        $http({
          method: 'POST',
          url: '/api/payments/binge/delete',
          data: {
            request: bingeBill.bill,
            auth: btoa(username + ':' + password),
            deployment_id: localStorageService.get('deployment_id'),
            tenant_id: localStorageService.get('tenant_id'),
            billId: billId
          }
        }).success(function (data) {
          //updateBillAfterClose();
          console.log(data);
        }).error(function (err) {
          console.log(err);
        });
      }
    }

    $rootScope.$watch('settleModalClose',function(){
      if($rootScope.settleModalClose)
        $scope.initializeTab();
    });


    function closePaytmBill(paytmBill){
      console.log("order_id",paytmBill.order_id);
      $http({
        method:'POST',
        url:'/api/payments/paytm/delete',
        data:{deployment_id:localStorageService.get('deployment_id'),tenant_id:localStorageService.get('tenant_id'),order_id:paytmBill.order_id}
      }).success(function(data){
        //updateBillAfterClose();
        console.log('paytm Bill Closed');
      }).error(function(error){
        console.log('paytm bill not closed');
      })
    }


    //ruplee socket
    socket.on('ruplee'+localStorageService.get('deployment_id'),function(data) {
      console.log('ruplee socket',data);
      var number,found=false;
      if(data.billData.status=='paid') {
        searchForPaidBill(data,'Ruplee');
      }

    });

    //Mobikwik socket
    socket.on('mobikwik_bill_paid_'+localStorageService.get('deployment_id'),function(data){
      console.log('socket',data);
      var number,found=false;
      searchForPaidBill(data,'Mobikwik');

    });



    socket.on('binge'+localStorageService.get('deployment_id'),function(data){
      console.log('binge socket',data);
      var number,found=false;
      searchForPaidBill(data,'binge');
    });


    socket.on('paytm'+localStorageService.get('deployment_id'),function(data){
      console.log('paytm socket',data);
      var number;
      searchForPaidBill(data,'Paytm');
    });

   /* function searchForPaidBill(data,type){
      var number;
      var found=false;
      var _bill;
      if($scope.selectedTab.tabType=='table' && $state.current.data.tables){
        for(var i=0;i<$state.current.data.tables.length;i++){
          if($state.current.data.tables[i].bill && $state.current.data.tables[i].bill.onlineTransaction){
            if($state.current.data.tables[i].bill.onlineTransaction.billData.posBillId==data.billData.posBillId){
              $state.current.data.tables[i].bill.onlineTransaction.billData.status='paid';
              $state.current.data.tables[i].bill.onlineTransaction.billData.transactionStatus='';
              if($state.current.data.tables[i].bill.tabType=='table') number= $state.current.data.tables[i].bill._tableId;
              else number=$state.current.data.tables[i].billNumber;
              growl.success(type+' payment successful for '+$state.current.data.tables[i].bill.tab+''+number);
              _bill=$state.current.data.tables[i].bill;
              found=true;
              break;
            }
          }
        }
      }
      else {
        if ($state.current.data.openedBills) {
          for (var i = 0; i < $state.current.data.openedBills.length; i++) {
            if ($state.current.data.openedBills[i].bill && $state.current.data.openedBills[i].bill.onlineTransaction && $state.current.data.openedBills[i].bill.onlineTransaction.billData) {
              if ($state.current.data.openedBills[i].bill.onlineTransaction.billData.posBillId == data.billData.posBillId) {
                $state.current.data.openedBills[i].bill.onlineTransaction.billData.status = 'paid';
                $state.current.data.openedBills[i].bill.onlineTransaction.billData.transactionStatus = '';
                if ($state.current.data.openedBills[i].bill.tabType == 'table') number = $state.current.data.openedBills[i].bill._tableId;
                else number = $state.current.data.openedBills[i].billNumber;
                growl.success(type + ' payment successful for ' + $state.current.data.openedBills[i].bill.tab + '' + number);
                _bill = $state.current.data.openedBills[i].bill;
                found = true;
                break;
              }
            }
          }
        }
      }
      if(found){
        var data=_bill.onlineTransaction;
        console.log(data);
        if($rootScope.isInstance)
          updateBillAfterPaidTransaction(_bill,data);
      }
      if(!found)
        growl.success(type+' payment successful');
    }*/

    function searchForPaidBill(data,type){
      var list=[],res=null;
      if($state.current.data.tables) list=$state.current.data.tables;
      else if($state.current.data.openedBills) list=$state.current.data.openedBills;
      for(var i=0;i<list.length;i++){
        if(list[i].bill && list[i].bill.onlineTransaction && list[i].bill._id==data.billData.posBillId){
          for(var j=0;j<list[i].bill.onlineTransaction.length;j++){
            if((list[i].bill.onlineTransaction[j].paymentType).toLowerCase()==type.toLowerCase()){
              list[i].bill.onlineTransaction[j].billData.status='paid';
              res=list[i].bill.onlineTransaction;
              break;
            }
          }
          if(res) break;
        }
      }
      if(res){
      var _bill=list[i].bill;
      console.log("_bill",_bill);
      if(_bill.tabType=='table')
         var number=_bill._tableId;
       else
        var number=_bill.billNumber;
       _bill.updateBill({
          onlineTransaction:res
        }, 'update').then(function () {
          console.log("updated bill");
          growl.success(type+" Payment Successful for "+_bill.tab+' -'+number,{ttl:4000});
        },function(err){
          console.log("Error in updating bill");
           growl.success(type+" Payment Successful for "+_bill.tab+' -'+number,{ttl:4000});
       });
      }
      else
        growl.success(type +" payment successful",{ttl:4000});
    }

    function updateBillAfterPaidTransaction(_bill,data){
      $http({
        method:'POST',
        url:$rootScope.url +'/api/synckots/updateBillAfterPaidTransaction',
        data:{deployment_id:_bill.deployment_id,billId:_bill._id,onlineTransaction:data}
      }).success(function(data){
        console.log("bill updated");
      }).error(function(data){
        console.log("error in updating bill");
      })
    }

    //--new payment integrations--//




    $scope.getSocketStatus();

    /*Knowlarity Notification*/

    /* // var socket =null;
     if( ($rootScope.Notification==false||$rootScope.Notification==null)){
     // alert(socket);
     activatenotif();
     }

     var Notification = window.Notification || window.mozNotification || window.webkitNotification;
     Notification.requestPermission(function (permission) {
     console.log(permission);
     });

     //var customername="ranjeet";
     function activatenotif() {
     // alert();
     //var getcloud = localStorageService.get('Knowlarity');
     //console.log(getcloud);
     //if (getcloud.isActivated) {
     $rootScope.Notification = true;
     var ksocket = io.connect('http://api.knowlarity.com:8088/');
     // socket.emit('feedapp', getcloud.api);
     ksocket.emit('feedapp', '6bf2b27f-cb23-456d-b581-84e112992bf7');
     ksocket.on('message', function (msg) {
     //alert(msg);
     msg = msg.replace(/\'/g, '\"');
     var jsonn = JSON.parse(msg);
     shownotification(jsonn.caller_id);
     });
     //}
     }

     function shownotification(m) {
     var number = m.substring(m.length - 10, m.length);
     if (number.substring(0, 2) == '11') { number = number.substring(number.length - 8, number.length); }
     Customer.search({mobile:number}).then(function (results) {
     show(results, number);
     } );
     }



     function show(customer,number) {
     // $scope.searchNumber=number;
     var instance = new Notification(
     (customer.firstname == null ? '' : customer.firstname) + ((customer.address1 == null) ? '' : '(' + customer.address1 + ')'), {
     body: 'Calling from :' + number
     }
     );

     instance.onclick = function (e) {
     //alert(number);
     //$scope.searchNumber = number;//(instance.body);
     //$scope.gnext();
     };

     }
     */
    /*Notification Ends Here*/

//--new payment integrations--//

    /*Payment integration ends here*/
    /*Knowlarity Notification*/

    /* // var socket =null;
     if( ($rootScope.Notification==false||$rootScope.Notification==null)){
     // alert(socket);
     activatenotif();
     }

     var Notification = window.Notification || window.mozNotification || window.webkitNotification;
     Notification.requestPermission(function (permission) {
     console.log(permission);
     });

     //var customername="ranjeet";
     function activatenotif() {
     // alert();
     //var getcloud = localStorageService.get('Knowlarity');
     //console.log(getcloud);
     //if (getcloud.isActivated) {
     $rootScope.Notification = true;
     var ksocket = io.connect('http://api.knowlarity.com:8088/');
     // socket.emit('feedapp', getcloud.api);
     ksocket.emit('feedapp', '6bf2b27f-cb23-456d-b581-84e112992bf7');
     ksocket.on('message', function (msg) {
     //alert(msg);
     msg = msg.replace(/\'/g, '\"');
     var jsonn = JSON.parse(msg);
     shownotification(jsonn.caller_id);
     });
     //}
     }

     function shownotification(m) {
     var number = m.substring(m.length - 10, m.length);
     if (number.substring(0, 2) == '11') { number = number.substring(number.length - 8, number.length); }
     Customer.search({mobile:number}).then(function (results) {
     show(results, number);
     } );
     }



     function show(customer,number) {
     // $scope.searchNumber=number;
     var instance = new Notification(
     (customer.firstname == null ? '' : customer.firstname) + ((customer.address1 == null) ? '' : '(' + customer.address1 + ')'), {
     body: 'Calling from :' + number
     }
     );

     instance.onclick = function (e) {
     //alert(number);
     //$scope.searchNumber = number;//(instance.body);
     //$scope.gnext();
     };

     }
     */
    /*Notification Ends Here*/
    $scope.tableDClassNew = function(table) {
      var tableClass = 'table-main-grey';
      var bill=table.bill;
      var booking=table.tableBookings;
      if (bill) {
        if (bill.isPrinted == true) {
          tableClass = 'table-main-green';
        } else {
          tableClass = 'table-main-violet';
        }
      }
      else if(booking){
        tableClass='table-main-seagreen';
      }

      return tableClass;
    };

    //--added by rajat for offline mode---//
    $scope.isConnected=true;
    $scope.findBillAddress='/findbill';
    $scope.reportsAddress='/reports';
    $scope.dashboardAddress='/dashboard';
    $scope.administrationAddress='/admin/deployments';
    $scope.offerAddress='/offer/show';
    $scope.crmAddress='/CRM';
    $scope.expensesAddress='/expenses';
    $scope.stockAddress='/stock';
    $scope.accountsAddress='/accounts';
    $scope.offlineModeTooltip='online';
    console.log("isConnected",$scope.isConnected);
    socket.on('disconnect',function(){
      onDisconnect();
    });

    socket.on('connect',function(){
      onConnect();
    });

    function onDisconnect(){
      console.log("disconnect");
      if($scope.isConnected){
        $scope.isConnected=false;
        $scope.findBillAddress='';
        $scope.reportsAddress='';
        $scope.dashboardAddress='';
        $scope.administrationAddress='';
        $scope.offerAddress='';
        $scope.crmAddress='';
        $scope.expensesAddress='';
        $scope.stockAddress='';
        $scope.accountsAddress='';
        $scope.offlineModeTooltip='offline';
        growl.error("You have gone offline",{ttl:3000});
      }
    }


    function onConnect() {
      console.log("connected");
      growl.success("You are  online", {ttl: 3000});
      $scope.isConnected = true;
      $scope.findBillAddress = '/findbill';
      $scope.reportsAddress = '/reports';
      $scope.dashboardAddress='/dashboard';
      $scope.administrationAddress='/admin/deployments';
      $scope.offerAddress='/offer/show';
      $scope.crmAddress='/CRM';
      $scope.expensesAddress='/expenses';
      $scope.stockAddress='/stock';
      $scope.accountsAddress='';
      $scope.offlineModeTooltip='online';
    }

    $scope.onlineIndicatorClass=function() {
      if ($scope.isConnected)
        return 'onlineCircle';
      else
        return 'offlineCircle';
    }





    //---added by rajat for offline mode---//

    //---added by rajat for urban piper---//

    function checkUrbanPiperSettings(bill){
      try{
      if(bill && bill._customer && bill._customer.mobile) {
        if ($rootScope.urbanPiperSetting && $rootScope.urbanPiperSetting.apiKey) {
          sendPointsToUrbanPiper(bill, $rootScope.urbanPiperSetting.apiKey, $rootScope.urbanPiperSetting.username);
        }
        else if(!$rootScope.urbanPiperSetting) {
          console.log("fetching urbanpiper settings");
          var apiKey = '';
          var username = '';
          $http({
            method: 'GET',
            url: '/api/urbanPiper',
            params: {
              deploymentId: localStorageService.get('deployment_id'),
              tenantId: localStorageService.get('tenant_id')
            }
          }).success(function (data) {
            console.log("urban piper settings", data);
            $rootScope.urbanPiperSetting={};
            if (data && data.length>0 && data[0].apiKey && data[0].username) {
              if (data[0].active == true) {
                $rootScope.urbanPiperSetting.apiKey = data[0].apiKey;
                $rootScope.urbanPiperSetting.username = data[0].username;
                $rootScope.urbanPiperSetting.activated=true;
                sendPointsToUrbanPiper(bill, $rootScope.urbanPiperSetting.apiKey, $rootScope.urbanPiperSetting.username);
              }
              else {console.log("UrbanPiper settings not activated");  $rootScope.urbanPiperSetting.activated=false;}
            }
            else {console.log("No UrbanPiper Settings found");  $rootScope.urbanPiperSetting.activated=false;}
          }).error(function (data) {
            console.log("Could not fetch UrbanPiper Settings");
          })
        }
      }
    }
    catch(e){
      console.log(e);
    }
    }

    function sendPointsToUrbanPiper(bill,apiKey,username){
      var amount=(bill.getTotalBill() + bill.getKotsTotalBill()).toFixed(2);
      var sku_data=[];
      if(bill._kots.length>0 && bill._kots[0].items)
        _.forEach(bill._kots,function(kot){
          _.forEach(kot.items,function(item){
            sku_data.push({"sku_code":item.number,
              "sku_desc":item.name,
              "qty":item.quantity,
              "line_total":item.subtotal,
              "discount": '',
              "discount_type":''
            });
          });
        });
      else if(bill._items){
        _.forEach(bill._items,function(item){
          sku_data.push({"sku_code":item.number,
            "sku_desc":item.name,
            "qty":item.quantity,
            "line_total":item.subtotal,
            "discount": '',
            "discount_type":''
          });
        })
      }
      console.log("sku_data",sku_data);

      $http({
        method: 'POST',
        url: '/api/payments/urbanpiper/addPoints',
        data: {
          username: username,
          apiKey: apiKey,
          name:bill._customer.firstname,
          phone: bill._customer.mobile,
          amount: amount,
          bill_id:bill._id,
          sku_data:sku_data
        }
      }).success(function (data) {
        growl.success(data.status,{ttl:5000});
      }).error(function (err) {
        growl.error("Could not add points to User's Urban Piper Account",{ttl:3000});
      });
     }



    function loadDeliveryInfo(){
      try {
        console.log("load delivery info");
        if($scope.tp_integrated)
        fetchNearestDeliveryBoys();
        loadPendingOrdersInfo();
      }
      catch(e){
        console.log("Exception",e);
      }
    }


    function loadPendingOrdersInfo(){
      try {

        delivery.getPendingOrders($state.current.data.openedBills).then(function(data){
          deliveryOrders=angular.copy(data);
          console.log(deliveryOrders);
          mapOrdersToBills(data);
        },
        function(err){
         console.log(err);
        })
      }
      catch(e){
        console.log("Exception",e);
      }
    }


     function updateDeliveryOrderStatus(orders,openedBills){
      try{
        _.forEach(openedBills,function(tab){
          if(tab.bill && tab.bill._id==orders.orderId){
            tab.bill._delivery.orderStatus = orders.status;
                tab.bill._delivery.updateTime = orders.datetime;
                tab.bill._delivery.deliveryHistory = {acceptedTime:orders.acceptedTime,pickedUpTime:orders.pickedUpTime,deliveredTime:orders.deliveredTime,paidTime:orders.paidTime,rejection:orders.rejection,dispatchTime:orders.dispatchTime};
                //tab.bill._delivery.deliveryHistory=orders;
                tab.bill._delivery.deliveryBoyName=orders.deliveryBoyName;
                tab.bill._delivery.deliveryBoyMobile=orders.deliveryBoyMobile;
                tab.bill._delivery.isLogistics=orders.isLogistics;

                if(orders.zomatoOrderId) tab.bill._delivery.zomatoOrderId=orders.zomatoOrderId;
                if(orders.location) tab.bill.location=orders.location;
                if(orders.trackingUrl) tab.bill._delivery.trackingUrl=orders.trackingUrl;

                if((orders.status=='Delivered'||tab.bill._deliveryTime)&&(orders.dispatchTime||orders.acceptedTime)){

                  if(orders.deliveredTime) var d1=new Date(orders.deliveredTime);
                  else if(tab.bill._deliveryTime) var d1=new Date(tab.bill._deliveryTime);

                  if(orders.acceptedTime)
                  var diff =  d1.getTime() - new Date(orders.acceptedTime).getTime();
                  else if(orders.dispatchTime)
                  var diff =  d1.getTime() - new Date(orders.dispatchTime).getTime();

                  var milliseconds = parseInt((diff % 1000) / 100)
                    , seconds = parseInt((diff/1000)%60)
                    , minutes = parseInt((diff / (1000 * 60)) % 60)
                    , hours = parseInt((diff / (1000 * 60 * 60)))
                   tab.bill._delivery.avgDeliveryTime = hours + ':' + minutes+':'+seconds+' hrs';
                }
                 if (tab.bill._delivery.orderStatus.startsWith('Rejected'))
                   growl.error("Bill Number " + tab.bill.billNumber + ' ' + tab.bill._delivery.orderStatus, {ttl: 3000});

                if (tab.bill._delivery.orderStatus == 'Delivered' && !tab.bill._deliveryTime) {
                  $scope.setDeliveryTime(tab.bill, orders.deliveredTime);
                }
            console.log(tab.bill._delivery);
          }
        });
      }
      catch(e){
        console.log("an exception ocurred",e);
      }

    }


   function mapOrdersToBills(tpOrders){

      _.forEach(tpOrders,function(order){
           updateDeliveryOrderStatus(order,$state.current.data.openedBills);
      });
    }

    socket.on('tpOrderStatus'+localStorageService.get('deployment_id'),function(data){
     updateDeliveryOrderStatus(data,$state.current.data.openedBills);
    });

     socket.on('zomatoOrderStatus'+localStorageService.get('deployment_id'),function(data){
     updateDeliveryOrderStatus(data,$state.current.data.openedBills);
    });
    socket.on('delivery_order_update'+localStorageService.get('deployment_id'),function(data){
       updateDeliveryOrderStatus(data,$state.current.data.openedBills);
    });
     socket.on('tpUserLocation'+localStorageService.get('deployment_id'),function(data){
      try{
        console.log("tp socket",data);
        for(var i=0;i<$scope.deliveryBoys.length;i++){
          if($scope.deliveryBoys[i].username==data.username) {
            $scope.deliveryBoys[i].location = data.location;
            break;
          }
        }
        if(i==$scope.deliveryBoys.length && data.location.action=='check_in')
        {
          for(var i=0;i<$scope.users.length;i++)
            if($scope.users[i].username==data.username)
               $scope.deliveryBoys.push($scope.users[i]);
        }
      }
      catch(e){
        console.log("exception",e);
      }
    });

    var subdomain=window.location.hostname.slice(0,window.location.hostname.indexOf('.'));
    if(window.location.hostname=='54.169.245.230') subdomain='flipkart';
    console.log(subdomain);

    socket.on('userLocation_'+subdomain,function(data){
      try {
        console.log('userlocationsocket', data);
        for (var i = 0; i < $scope.deliveryBoys.length; i++) {
          if ($scope.deliveryBoys[i].username == data.username) {
            $scope.deliveryBoys[i].location.lat = data.lat;
            $scope.deliveryBoys[i].location.lon = data.lon;
          }
        }
      }
      catch(e){
        console.log("Exception"+e);
      }
    });


    function fetchNearestDeliveryBoys(latitude,longitude){
      try {
        console.log("fetching delivery boys");
        $http({
          method: 'GET',
          url: '/api/technopurple/getDeliveryBoys',
          params: {deployment_id: localStorageService.get('deployment_id')}
        }).success(function (data) {
          console.log(data);
          var deliveryBoys = [];
          _.forEach(data, function (user) {
            if (user.location && user.location.action == 'check_in') deliveryBoys.push(user);
          });

          if(!$scope.deliveryBoys)
          $scope.deliveryBoys = deliveryBoys;

         else{
          var temp=[];
          for(var i=0;i<deliveryBoys.length;i++){
             var user=deliveryBoys[i].username;
             var flag=false;
            for(var j=0;j<$scope.deliveryBoys.length;j++){
               if($scope.deliveryBoys[j].username==user)
                { temp.push($scope.deliveryBoys[j]);
                  flag=true; break;
                }
              }
              if(!flag)
                temp.push(deliveryBoys[i]);
            }
            $scope.deliveryBoys=angular.copy(temp);
          }

          console.log($scope.deliveryBoys);

        }).error(function (err) {
          console.log("error", err);
        })
      }
      catch(e){
        console.log("Exception",e);
      }
    }


    $scope.showDeliveryHistory=function(bill){
      delivery.showDeliveryHistory(bill);
    }

    $scope.showRiderLocation=function() {
      delivery.showRiderLocation($scope.deliveryBoys);
    }

    $scope.showOrderLocation=function(url){
      if(!url||url=='') {
        growl.error("Order Location not available yet",{ttl:3000});
        return false;
      }
      $window.open(url);
    }

    $scope.refreshOrderStatus=function(orderId){
      console.log(deliveryOrders);
      delivery.refreshOrderStatus(orderId,deliveryOrders).then(function(res){
        growl.success("Order refreshed",{ttl:3000});
        updateDeliveryOrderStatus(res,$state.current.data.openedBills);
      },function(err){
         console.log(err);
         growl.error(err.status,{ttl:3000});
      });
    }

  $scope.lockOnlineBill = function(){
      if($scope.selectedTab.tabType!='table'){
        _.forEach($state.current.data.openedBills, function(onlineBill){
          if(onlineBill.isOnlineBill && onlineBill.bill.orderScheduleTime){
            var timeOfOrder = new Date(onlineBill.bill.orderScheduleTime);
            console.log(timeOfOrder);
            var timeToUnlock = moment(timeOfOrder).subtract(40,'minutes').toDate();
            console.log(timeToUnlock);
            var currentTime = new Date();
            console.log(currentTime);
            console.log(timeToUnlock.getTime()<=currentTime.getTime());
            onlineBill.isLocked = timeToUnlock.getTime()>=currentTime.getTime()
            var duration = moment.duration(moment(timeToUnlock).diff(currentTime))//.format('H.m');
            console.log(duration);
            var totalMinutes = parseInt(duration.get('hours'))*60+parseInt(duration.get('minutes'))
            onlineBill.timeLeft = totalMinutes;
            // if(timeToUnlock.getTime()<=currentTime.getTime()){
            //   return false;
            // }
            // else{
            //   return true;
            // }
          }
        })

      }
    }

    // $scope.timeLeftToUnlockBill = function(orderTime){
    //   if($scope.selectedTab.tabType!='table' && orderTime){
    //     var timeOfOrder = new Date(orderTime);
    //     var timeToUnlock = moment(timeOfOrder).subtract(40,'minutes').toDate();
    //     var currentTime = new Date();
    //     var duration = moment.duration(moment(timeToUnlock).diff(currentTime));
    //     console.log(duration.get('minutes'));
    //     return duration.get('minutes')+ ' minutes left';
    //   }
    // }
    $interval($scope.lockOnlineBill,10000);
   /* var deliveryPartners=[{name:'Delhivery'}];
    $scope.deliveryPartners=[];
    _.forEach(deliveryPartners,function(d){
      $scope.deliveryPartners.push({firstname:d.name,lastname:'',_id:'123'});
      console.log("delivery partners",$scope.deliveryPartners);
    });*/

    ThirdPartyService.getPartnersByDeployment(localStorageService.get('deployment_id')).success(function(partners){
      console.log("partners",partners);
      deploymentPartners=partners;
      _.forEach(deploymentPartners,function(p){
          if(p.class &&(p.class.indexOf('partner_delivery')||p.class.indexOf('self_delivery')))
            $scope.delivery_integrated=true;
         if(p.class && p.class.indexOf('partner_delivery')!=-1)
          deliveryPartners.push({firstname:p.partner_name,lastname:'',_id:p._id});
      });
      console.log("delivery partners",deliveryPartners,deploymentPartners);
    }).error(function(err){
      console.log(err);
    })



    // added by rajat for knowlarity
       /*  $scope.showCustomerCalling=function(data){
          var number=JSON.parse(data).caller_id.substring(3);
          console.log("number",number);
           Customer.search({
             q: number,
             deployment_id: localStorageService.get('deployment_id')
           }).$promise.then(function (customers) {
             console.log('new test',JSON.parse(angular.toJson(customers)));
             var temp=JSON.parse(angular.toJson(customers));
             if(temp.length>0)
             //customerName=JSON.parse(angular.toJson(customers[0].firstname));
             notifyOnCustomerCall(temp[0],true);
             else
             notifyOnCustomerCall(number,false)
           })
         }

        function notifyOnCustomerCall(customer,customerFound) {
          console.log("function called from iframe", customer);
          var name = customerFound ? customer.firstname : customer;

          $ngBootbox.alert("Hi! " + name + " is calling").then(function (res) {
              console.log("closed");
              var index = -1;
              for (var i = 0; i < $rootScope.tabs.length; i++) {
                if ($rootScope.tabs[i].tabType == 'delivery') {
                  index = i;
                  break;
                }
              }
              if (index != -1)
                renderDeliveryTab($rootScope.tabs[index], customer);
            },
            function (err) {
              console.log("closed");
            });
        }

        function renderDeliveryTab(tab,customer) {
          $scope.isBillOn = false;
          $scope.isEditAdvance = false;
          tab.orderCount = 0;
          $scope.isBillingOn = false;
          console.time("TAB RENDER:");
          $scope.selectedOffer = null;
          $scope.renderItemsTabWise(tab, $rootScope.rootMasterItems);
          //if($scope.selectedOffer!=null){return false;}
          $scope.showBillPage = false;

          /* if (toState.name === 'billing.table') {*/
         // $scope.elapsedMinutes = 0;
          //console.log(tab);
         /* $scope.selectedTab = {
            tabId: tab._id,
            tabName: tab.tabName,
            tabType: tab.tabType == 'take_out' ? 'takeout' : tab.tabType
          };

          /* Take/Delivery State */
        /*  if ($scope.selectedTab.tabType != 'table') {
            $scope.getOfflineBillsByTab().then(function (offlineBills) {
              //console.log(offlineBills);
              $state.current.data.openedBills = [];
              $state.current.data.openedBills.push({
                "id": "New",
                isOnlineBill: false,
                isAdvanceBill: false
              });
              $scope.bill = {};
              /* Bind with Unsettled Table Bills */
           /*   _.forEach(offlineBills, function (offlineBill) {
                if (!(offlineBill.isOnlineBill || offlineBill.isAdvanceBill)) {
                  var table = {
                    "id": (offlineBill.bill.billNumber + (offlineBill.bill.splitNumber > 0 ? '-' + offlineBill.bill.splitNumber : ''))
                  };
                  table.bill = offlineBill.bill;
                  table.covers = offlineBill.bill._covers;
                  table.name = offlineBill.name;
                  table.isOnlineBill = false;
                  table.isAdvanceBill = false;
                  table.billId = offlineBill.bill._id;

                  $state.current.data.openedBills.push(table);
                } else {
                  $state.current.data.openedBills.push(offlineBill);
                }
              });
              if ($rootScope.editBill != undefined) {
                $scope.startEdit();
              }
              console.log($state.current.data.openedBills);
              if ($scope.selectedTab.tabType == 'delivery' && ($scope.tp_integrated || $scope.zomato_integrated))
                loadDeliveryInfo();     //---added for delivery integrations

              $scope.startNewTakeOutDeliveryBill('New');
              if (customerFound)
                $scope.getSelectedCustomer(customer);
              else
                $scope.customerSearchEnter(customer);

            });
          }

          console.timeEnd("TAB RENDER:");

          $scope.getDaySale();
        }*/


    //---added by rajat for inresto integration---//
    /* var bookingEvent='inresto';
     nSocket.forward(bookingEvent, $scope);*/
    if(Utils.hasSetting('Inresto MerchantId',localStorageService.get('settings')))
      var merchantId=Utils.getSettingValue('Inresto MerchantId',localStorageService.get('settings'));
    console.log(merchantId);

    nSocket.on('inresto'+merchantId, function (data) {
      console.log(data);
      saveInrestoData(data);
    });


    function saveInrestoData(merchantData) {

      $state.current.data.tables[Number(merchantData.tableNumber) - 1].tableBookings = {};
      $state.current.data.tables[Number(merchantData.tableNumber) - 1].tableBookings.tableNumber = Number(merchantData.tableNumber);
      $state.current.data.tables[Number(merchantData.tableNumber) - 1].tableBookings.tableData = merchantData.tableData;
      //console.log($state.current.data.tables);
      var customerPhone = merchantData.tableData.customerPhone;
      console.log(customerPhone);
    }


    function sendBillToInresto(bill) {
      try {
        console.log("send bill");
        var merchantId=Utils.getSettingValue('Inresto MerchantId',localStorageService.get('settings'));
        if(!merchantId || !$rootScope.isInstance)
          return false;

        console.log(" send bill", merchantId);
        var _bill = angular.copy(bill);
        var merchantKey = 'password';
        var billToSend = {};
        billToSend.merchantId = merchantId;
        billToSend.merchantKey = merchantKey;
        billToSend.customerPhone = '91' + _bill._customer.mobile;
        billToSend.totalAmount = _bill.aggregation.netRoundedAmount;

        $http({
          method: 'POST',
          url: $rootScope.url + '/api/synckots/seatcustomer/updatebillstatus',
          data: billToSend
        }).success(function (data) {
          console.log("sent bill to inresto", data);
          sendTableUpdateToInresto(_bill, 2);
        }).error(function (err) {
          console.log("error", err);
          sendTableUpdateToInresto(_bill, 2);
        })
      }
      catch(e){
        console.log("ex",e);
      }
    }


    function sendTableUpdateToInresto(bill, status) {

      try{
        var restId=Utils.getSettingValue('Inresto MerchantId',localStorageService.get('settings'));
        if(!restId || !$rootScope.isInstance)
          return false;

        console.log("send table update");
        var statusToSend = {
          tablename: 'T' + bill._tableId,
          status: status,
          restID: restId
        };

        $http({
          method: 'POST',
          url: $rootScope.url + '/api/synckots/seatcustomer/updatetablestatus',
          data: statusToSend
        }).success(function (data) {
          console.log("sent table update", data);
        }).error(function (err) {
          console.log('error', err);
        })
      }
      catch(e){
        console.log("ex",e);
      }
    }

    function removeTableBooking(bill) {
      try{
        if ($state.current.data.tables[bill._tableId - 1].tableBookings) {
          console.log("remove table booking");
          var merchant_id=Utils.getSettingValue('Inresto MerchantId',localStorageService.get('settings'));
          if(!merchant_id || !$rootScope.isInstance)
            return false;

          console.log("in remove table booking", bill._tableId);
          delete $state.current.data.tables[bill._tableId - 1].tableBookings;
          $http({
            method: 'GET',
            url: $rootScope.url + '/api/synckots/seatcustomer/delete',
            params: {merchant_id: merchant_id, tableNumber: bill._tableId}
          }).success(function (data) {
            console.log("successfully deleted online table booking");
          }).error(function (err) {
            console.log("error");
          })
        }
      }
      catch(e){
        console.log("ex"+e);
      }
    }

    function addInrestoCustomerToBill(tableId) {
      try {
        var t = $state.current.data.tables[tableId - 1];
        if (t.tableBookings && t.tableBookings.tableData) {
          if(t.bill && !t.bill._covers && t.tableBookings.tableData.pax){
            console.log("add covers");
            t.bill._covers= t.tableBookings.tableData.pax;
          }

          var mobile= t.tableBookings.tableData.customerPhone;
          Customer.search({
            q: mobile,
            deployment_id: localStorageService.get('deployment_id')
          }).$promise.then(function (customers) {
            console.log('new test', JSON.parse(angular.toJson(customers)));
            var temp = JSON.parse(angular.toJson(customers));
            if (temp.length > 0)
              $scope.getSelectedCustomer(temp[0]);
            else
              $scope.customerSearchEnter(mobile);

          });
        }
      }
      catch(e){
        console.log("Exception",e);
      }
    };

    $scope.reserveTableBill=function(tableId){
      $state.current.data.tables[tableId].tableBookings={};
      saveTableBookingFromPos(tableId+1);
      sendTableUpdateToInresto({'_tableId':tableId+1},3);
    }

    function saveTableBookingFromPos(tableId){
      try{
        console.log("Seat customer");
        if(!$rootScope.isInstance) {
          growl.error("Only allowed on instances", {ttl: 3000});
          return false;
        }
        var merchantId=Utils.getSettingValue('Inresto MerchantId',localStorageService.get('settings'));
        if(!merchantId || !$rootScope.isInstance)
          return false;

        console.log("in");
        var bookingData={};
        bookingData.merchant_id=merchantId;
        bookingData.tableNumber=tableId;
        bookingData.tableData={};
        $http({
          method:'POST',
          url:$rootScope.url+'/api/synckots/seatcustomer/savebookingfrompos',
          data:bookingData
        }).success(function(data){
          console.log("success",data);
        }).error(function(err){
          console.log(err);
        })
      }
      catch(e){
        console.log("ex",e);
      }
    };


    function renderTableBookings() {

      console.log("instance",$rootScope.isInstance,$rootScope.url);

      var merchantId=Utils.getSettingValue('Inresto MerchantId',localStorageService.get('settings'));
      if(!merchantId || !$rootScope.isInstance)
        return false;
      $http({
        method: 'GET',
        url: $rootScope.url + '/api/synckots/seatcustomer/',
        params: {merchant_id: merchantId}
      }).success(function (data) {
        console.log("table bookings", data);
        assignBookingsToTables(data);
      }).error(function (err) {
        console.log(err);
      })
    }

    function assignBookingsToTables(data) {

      console.log("assign bookings to tables");
      for (var i = 0; i < data.length; i++) {
        var tableNumber = Number(data[i].tableNumber);
        console.log("table", $state.current.data.tables[tableNumber - 1].bill);
        $state.current.data.tables[tableNumber - 1].tableBookings = {};
        $state.current.data.tables[tableNumber - 1].tableBookings.tableNumber = tableNumber;
        $state.current.data.tables[tableNumber - 1].tableBookings.tableData = data[i].tableData;
        if ($state.current.data.tables[tableNumber - 1].bill && !$state.current.data.tables[tableNumber - 1].bill._covers)
          $state.current.data.tables[tableNumber-1].bill._covers=Number(data[i].tableData.pax);
      }
    }

//---inresto integration----//
 $scope.stockOptions=function(item){
     var allValidFeature=[];
     console.log(item);
      var inStock = ['In Stock', function($itemScope) {
              $scope.updateItemStock(item,false);
        }];
        var outOfStock = ['Out of Stock', function($itemScope) {
               $scope.updateItemStock (item,true);
         }];
       if(item.outOfStock)
         allValidFeature.push(inStock);
       else
        allValidFeature.push(outOfStock);
       return allValidFeature;
     }

     $scope.updateItemStock=function(item,outOfStock){

        //var selectQuery='select'
        $rootScope.db.selectDynamic("SELECT itemData from items where itemId='"+item._id+"'").then(function(items) {
        if (items.rows.length > 0) {
          var _item=JSON.parse(items.rows.item(0).itemData);
          console.log("_item",_item);
          _item.outOfStock=outOfStock;
          var updateQuery="update items SET itemData='"+JSON.stringify(_item)+"' where itemId='"+item._id+"'";
           $rootScope.db.selectDynamic(updateQuery).then(function(result){
            growl.success("Item Stock updated",{ttl:3000});
             item.outOfStock=outOfStock;
            _.forEach($rootScope.rootMasterItems,function(tab){
               for(var i=0;i<tab.Items.length;i++){
                 if(tab.Items[i]._id==item._id){
                   tab.Items[i].outOfStock=outOfStock;
                   break;
                  }
                }
              });
             if(item.urbanPiperId && Number(item.urbanPiperId)!=0)
             updateItemInUrbanPiper(_item);
           },function (e) {
             console.log("error",e);
             growl.error("Item Stock not updated.Please try again",{ttl:3000});
          });
        }
      });
     }

     function updateItemInUrbanPiper(item){
        UrbanPiper.getDeploymentKeys({tenantId:localStorageService.get('tenant_id'),deploymentId:localStorageService.get('deployment_id')}).then(function(keys){
            console.log(keys);
            var postData = {item: item, urbanPiperKeys:keys, tabs: $scope.tabs};
            UrbanPiper.updateItemLocationAssociation(postData,function(result){
              console.log(result)
            if(result.status!=200)
              growl.error('Item Not Updated In UrbanPiper!', {ttl: 3000});
          });
        });
      }


     $scope.getItemClass=function(item){
      if(item.outOfStock)
        return 'item-class-red';
      else
        return '';
     }



  }]);
