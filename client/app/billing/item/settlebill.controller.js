/**
 * Created by Ranjeet Sinha on 4/24/2015.
 */
'use strict';

angular.module('posistApp')
  .controller('settleBillCtrl', ['$http','localStorageService','$modal','$scope','$rootScope', '$modalInstance','itemIndex','taxes', 'bill', 'Utils','growl','isKot','kotIndex','settleSettings', 'nSocket','bankSettings','loyalty','intercom', function ($http,localStorageService,$modal,$scope,$rootScope, $modalInstance, itemIndex, taxes, bill, Utils, growl,isKot,kotIndex,settleSettings,nSocket,bankSettings,loyalty,intercom) {
    // alert(kotIndex);
    $scope.isSettlePressed=false;
    $scope.isOtherDisable=false;
    //console.log(settleSettings);
    $scope.settleSettings=[];
    $scope.settleSettingsOther=[];
    $scope.remove_card_details=Utils.hasSetting('remove_credit_card_details',localStorageService.get('settings'));

    var _fs=_.filter(settleSettings,{isOnlineOrder:true});
    if(_fs.length>0){
      $scope.settleSettings=angular.copy( _fs);
    }
    var _fo=_.filter(settleSettings,{isOnlineOrder:false});
    if(_fo.length>0){
      $scope.settleSettingsOther=angular.copy( _fo);
    }

    //$scope.settleSettings=settleSettings;
    $scope.bankSettings=angular.copy( bankSettings);
    $scope.bankDSettings= angular.copy( bankSettings);

    $scope.settleValueOnline=$scope.settleSettings.length>0?  $scope.settleSettings[0]:{name:'--select--'};
    $scope.settleValue=$scope.settleSettingsOther.length>0?  $scope.settleSettingsOther[0]:{name:'--select--'};
    $scope.settleBankValue=bankSettings.length>0? $scope.bankSettings[0]:{name:'--select--'};
    $scope.settleBankDValue=bankSettings.length>0? $scope.bankDSettings[0]:{name:'--select--'};

    $scope.settleType={name:'COD'};
    $scope.settleBankType={name:'VISA'};
    $scope.settleBankDType={name:'VISA'};
    $scope.itemIndex = itemIndex;
    $scope.taxes = taxes;

    $scope.bill = angular.copy(bill);
    // console.log(bill);
    /* Autofill Card Amounts */
    $scope.bill.debitCardForm.amount = $scope.bill.getAmountRemaining();
    $scope.bill.creditCardForm.amount = $scope.bill.getAmountRemaining();
    $scope.bill.otherForm.amount=$scope.bill.getAmountRemaining();
    $scope.bill.onlineForm.amount=$scope.bill.getAmountRemaining();
      $scope.block={};
      $scope.partialPayment=false;
    $scope.applyOnBill = function (options) {
      $scope.bill.validateSettlement().then(function (proSeed) {

        //console.log('Settle Bill');
        if (proSeed) {
          $scope.isSettlePressed=true;
          var dCard={cardType:'DebitCard',totalAmount:0,detail:[]};
          _.forEach($scope.bill._debitCards,function(card){
            dCard.totalAmount+=parseFloat( angular.copy(card.amount));
          });
          dCard.detail=angular.copy($scope.bill._debitCards)
          $scope.bill.payments.cards.push(dCard);

          var cCard={cardType:'CreditCard',totalAmount:0,detail:[]};
          _.forEach($scope.bill._creditCards,function(card){
            cCard.totalAmount+= parseFloat(angular.copy(card.amount));
          });
          cCard.detail=angular.copy($scope.bill._creditCards)
          $scope.bill.payments.cards.push(cCard);

          var oCard={cardType:'Other',totalAmount:0,detail:[]};
          _.forEach($scope.bill._otherCards,function(card){
            oCard.totalAmount+=parseFloat(angular.copy(card.amount));
          });
          oCard.detail=angular.copy($scope.bill._otherCards)
          $scope.bill.payments.cards.push(oCard);

          var onlineCard={cardType:'Online',totalAmount:0,detail:[]};
          _.forEach($scope.bill._onlineCards,function(card){
            onlineCard.totalAmount+=parseFloat(angular.copy(card.amount));
          });
          onlineCard.detail=angular.copy($scope.bill._onlineCards)
          $scope.bill.payments.cards.push(onlineCard);
        //---added for coupon settlement--//
           var couponCard={cardType:'Coupon',totalAmount:0,detail:[]};
          _.forEach($scope.bill._coupons,function(card){
            couponCard.totalAmount+=parseFloat(angular.copy(card.amount));
          });
          couponCard.detail=angular.copy($scope.bill._coupons)
          $scope.bill.payments.cards.push(couponCard);
        //--added for coupon settlement---//

          /* Cash */
          //$scope.bill.payments.cash = angular.copy($scope.bill._cash);


          /*_.forEach($scope.bill._creditCards,function(card){
           var ccard=angular.copy(card);
           ccard.cardType='CreditCard';
           $scope.bill.payments.cards.push(ccard);
           });*/
          // $scope.bill.payments.push(angular.copy($scope.bill._debitCards));
          // $scope.bill.payments.push(angular.copy($scope.bill._creditCards));
          $modalInstance.close($scope.bill);
        }

      }).catch(function (err) {
        console.log(err);
        growl.error('Remaining amount needs to be Zero "0". ', {ttl:3000});
      });
    };

    $scope.updateQuantity = function () {
      $modalInstance.close($scope.item);
    };

    $scope.incrementQuantity = function () {
      var q = parseInt($scope.item.quantity);
      q += 1;
      $scope.item.quantity = q;
    };

    $scope.decrementQuantity = function () {
      var q = parseInt($scope.item.quantity);
      q -= 1;
      $scope.item.quantity = q;
    };

    $scope.cancel = function () {
      $modalInstance.close('cancel');
    };


    $scope.updateSelectedTax = function (action, tax) {
      if (action == 'add' && Utils.arrayObjectIndexOf($scope.bill._items[$scope.itemIndex].taxes, 'name', tax.name) == -1) {
        $scope.bill._items[$scope.itemIndex]._taxes.push(tax);
      }
      if (action == 'remove' && Utils.arrayObjectIndexOf($scope.bill._items[$scope.itemIndex].taxes, 'name', tax.name) != -1) {
        $scope.bill._items[$scope.itemIndex]._taxes.splice(Utils.arrayObjectIndexOf($scope.bill._items[$scope.itemIndex].taxes, 'name', tax.name), 1);
      }
    };

    $scope.setSelectedTax = function (event, tax) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      $scope.updateSelectedTax(action, tax);
    };

    $scope.getOthersType=function(){
      console.log($scope.settleType);
    }

    //----ADDED BY RAJAT---//
    //----ADDED BY RAJAT----//
      $scope.checkPaymentOption = function(settleValue){

        $scope.billSent=false;
        $scope.transactionActive=false;
        $scope.transactionStatus="";
        $scope.enableCheckStatus=false;
        $scope.onlinePayment=false;
        $scope.partialPayment=false;
       $scope.bill._otherCards=[];
        $scope.bill.otherForm.amount=$scope.bill.getAmountRemaining();
        console.log($scope.bill.onlineTransaction);

        if($scope.bill.onlineTransaction){
          $scope.billSent=true;
          $scope.onlinePayment=true;
          $scope.partialPayment=false;
          $scope.settleType.name='Online-Payment';
          _.forEach($scope.bill.onlineTransaction,function(t){
          for(var i=0;i<$scope.settleSettingsOther.length;i++){
            if($scope.settleSettingsOther[i].name.toLowerCase()==t.paymentType){
              $scope.settleValue=$scope.settleSettingsOther[i];
              break;
            }
          }
          $scope.bill.addOtherPayment({name:$scope.settleValue.name,type:$scope.settleType.name,amount:t.billData.amount});
        });
     }
          /*if($scope.bill.onlineTransaction.billData.status=='active'){
            console.log("active");
            $scope.transactionActive=true;
            $scope.transactionStatus='In Progress';
            $scope.enableCheckStatus=true;
          }
          else if($scope.bill.onlineTransaction.billData.status=='paid') {
            console.log('paid');
            $scope.transactionStatus = 'successful';
          }*
        }

      /*  var value=settleValue.toLowerCase().replace(/\s/g, '');
        if(value=='paytm'||value=='ruplee'||value=='binge'||value=='mobikwik'||value=='urbanpiper')
        {
          $scope.onlinePayment=true;
          $scope.settleType.name='Online-Payment';
          if(value=='urbanpiper')
            $scope.partialPayment=true;
        }*/
      //  if(loyalty.isLoyaltyPartner(value))          //added for loyalty
       //    $scope.partialPayment=true;
      }


      $scope.getTransactionStatus=function(value){
       var val=value.toLowerCase();
       if(val=='paytm')
        return 'Successful';
       var res='';
       if(!$scope.bill.onlineTransaction){
        return '';
       }

        for(var i=0;i<$scope.bill.onlineTransaction.length;i++){
         if($scope.bill.onlineTransaction[i].paymentType==val){
            res=$scope.bill.onlineTransaction[i].billData.status;
            break;
          }
        }
        if(res=='active') return 'In Progress..';
        else if(res=='paid') return 'Successful';
        else return '';
      }

      $scope.isSettlementDisabled=function(){
        var res=false;
        _.forEach($scope.bill.onlineTransaction,function(t){
          if(t.billData.status!='paid')
            res=true;
        });
        return res;
      }



      $scope.checkStatus=function(settleValue){
        if(!$scope.bill.onlineTransaction) {
          growl.info("Payment added",{ttl:3000});
          return false;
        }
        /*var paymentType=settleValue.toLowerCase(),paymentData;
         for(var i=0;i<$scope.bill.onlineTransaction.length;i++){
          if($scope.bill.onlineTransaction[i].paymentType==paymentType)
            paymentData=$scope.onlineTransaction[i].billData;
         }*/
        var i=getPaymentPartner(settleValue);
        if(i==-1){growl.success("Payment Successful",{ttl:3000}); return false;}
        var paymentType=$scope.bill.onlineTransaction[i].paymentType,paymentData=$scope.bill.onlineTransaction[i].billData;
        if(paymentType=='mobikwik') checkMobikwikStatus(paymentData);
        else if(paymentType=='ruplee') checkRupleeStatus(paymentData);
        else if(paymentType=='binge') checkBingeStatus(paymentData);
        else if(paymentType=='paytm'||paymentType=='ikaaz'||paymentType=="urbanpiper"){growl.success("Payment Successful",{ttl:3000});}
      }

      $scope.cancelPayment=function(index){
        if(!$scope.bill.onlineTransaction){
          $scope.bill.removeOther(index);
          return false;
        }
        var settleValue=$scope.bill._otherCards[index].otherName;
        var i=getPaymentPartner(settleValue);

        if(i==-1){
           $scope.bill.removeOther(index);
          return false;
        }

        var paymentType=$scope.bill.onlineTransaction[i].paymentType,paymentData=$scope.bill.onlineTransaction[i].billData;
        if(paymentData.status=='paid'){growl.error("Payment already done. Can't be cancelled"); return false;}
        if(paymentType=='mobikwik') cancelMobikwikBill(paymentData);
        else if(paymentType=='ruplee') cancelRupleeBill(paymentData);
        else if(paymentType=='binge') cancelBingeBill(paymentData);
        //else if(paymentType=='paytm'||paymentType=='ikaaz') {growl.error("Payment Cannot be cancelled",{ttl:3000});}
      }

      function checkMobikwikStatus(mobikwikBill){
        console.log(mobikwikBill);
        var data={merchantid:mobikwikBill.merchant_id,storeid:mobikwikBill.store_id,posid:mobikwikBill.pos_id,orderid:mobikwikBill.order_id,secretKey:mobikwikBill.secret_key}
        $http({
          method: 'POST',
          url: '/api/payments/mobikwik/checkStatus',
          data: data
        }).success(function (data) {
          console.log(data);
          if (data.statuscode == "0") {
            updateAfterCheckStatus('mobikwik');
            // $scope.bill.addOthersPayment($scope.otherForm);
          }
          else
            growl.error("payment Still Active",{ttl:3000});
        }).error(function (err) {
          console.log("error", err);
          if(err.statusmessage)
            growl.error(err.statusmessage,{ttl:3000});
          else growl.error("Could not connect with mobikwik",{ttl:3000});
        })
      }

      function checkRupleeStatus(rupleeBill){
        $http({
          method:'POST',
          url:'/api/payments/ruplee/status',
          data:{deployment_id:localStorageService.get('deployment_id'),'tenant_id':localStorageService.get('tenant_id'),billId:rupleeBill.billId}
        }).success(function(data){
          updateAfterCheckStatus('ruplee');
        }).error(function(err){
          if(err.status)
            growl.error(err.status,{ttl:3000});
          else growl.error('Something went wrong',{ttl:3000});
        })
      }

      function checkBingeStatus(bingeBill){
        console.log(bingeBill);

        var binge_username = _.filter(localStorageService.get('settings'), {"name":"binge_username" ,"selected":true});
        var binge_password = _.filter(localStorageService.get('settings'), {"name":"binge_password" ,"selected":true});
        if(binge_username.length>0 && binge_password.length>0) {
          var username = binge_username[0].value;
          var password = binge_password[0].value;
          var bill = {
            client: {
              datetime: new Date().toISOString().substring(0, 19).replace('T', ' '),
              orderno: bingeBill.bingeOrderNo
            }
          };
          console.log(username,password);
          $http({
            method: 'POST',
            url: '/api/payments/binge/status',
            data: {request: bill, auth: btoa(username + ':' + password)}
          }).success(function (data) {
            console.log(data);
            updateAfterCheckStatus('binge');
            // $scope.bill.addOthersPayment($scope.otherForm);
          }).error(function (err) {
            console.log(err);
            if(err.status) growl.error(err.status,{ttl:3000});
            else growl.error("Could not connect with Binge",{ttl:3000});
          })
        }
      }

      function checkPaytmStatus(paytmBill){
        var merchantGuid = "", secret_key = "";
        var paytm_merchant_guid = _.filter(localStorageService.get('settings'), {
          "label": "paytm_merchant_guid",
          "selected": true
        });
        var paytm_secret_key = _.filter(localStorageService.get('settings'), {
          "label": "paytm_secret_key",
          "selected": true
        });
        if (paytm_merchant_guid.length > 0 && paytm_secret_key.length > 0) {
          merchantGuid = paytm_merchant_guid[0].value;
          secret_key = paytm_secret_key[0].value;
        }
        if(merchantGuid!="" && secret_key!="") {
          var modalInstance = $modal.open({
            template: '<div class="modal-header text-center">' +
            '<h3 style="color:blue">Txn Id</h3><div class="modal-body text-center"><input type="text" class="form-control" ng-model="txnId" placeholder="TransactionId"/><br/>' +
            '<div align="center"><button type="button" class="btn btn-primary"  ng-click="check(txnId)">Check Status</button>&nbsp; &nbsp;</div>' +
            '</div>',

            controller: ['$rootScope', '$scope', '$modalInstance', function ($rootScope, $scope, $modalInstance) {


              $scope.check = function (txnId) {
                if (!txnId) growl.error("Please enter Transaction Id", {tt: 3000});
                $modalInstance.close(txnId)
              }

            }],
            size: 'sm'
          });

          modalInstance.result.then(function (txnId) {
            console.log(txnId);

            var data = {
              "request": {
                "requestType": "wallettxnid",
                "txnType": "withdraw",
                "txnId": txnId,
                "merchantGuid": merchantGuid
              },
              "platformName": "PayTM",
              "operationType": "CHECK_TXN_STATUS"
            }
            $http({
              method: 'POST',
              url: '/api/payments/paytm/checkStatus',
              data: {body: data, mid: merchantGuid, secret_key: secret_key}
            }).success(function (data) {
              updateAfterCheckStatus('paytm');
            }).error(function (err) {
              if(err && err.status)
                growl.error(err.status, {ttl: 3000})
              else growl.error("Could not connect with Paytm",{ttl:3000});
            });


          }, function () {
            console.log('Modal dismissed at: ' + new Date());
          });
        }
        else growl.error("No Paytm Credentials Set",{ttl:3000});
      }


      function cancelMobikwikBill(mobikwikBill,index){
        var data = {
          merchantid: mobikwikBill.merchant_id,
          storeid: mobikwikBill.store_id,
          posid: mobikwikBill.pos_id,
          orderid: mobikwikBill.order_id,
          filter: {
            deployment_id: localStorageService.get('deployment_id'),
            tenant_id: localStorageService.get('tenant_id')
          },
          secretKey: mobikwikBill.secret_key
        }

        $http({
          method: 'POST',
          url: '/api/payments/mobikwik/cancel',
          data: data
        }).success(function (data) {
          console.log(data);
          if (data.txnstatuscode == 0)
            updateAfterCancelBill('mobikwik');
        }).error(function (err) {
          growl.error("Bill Could not be Cancelled", {ttl:3000});
        })
      }

      function cancelBingeBill(bingeBill){
        var binge_username = _.filter(localStorageService.get('settings'), {"name":"binge_username" ,"selected":true});
        var binge_password = _.filter(localStorageService.get('settings'), {"name":"binge_password" ,"selected":true});
        if(binge_username.length>0 && binge_password.length>0) {
          var username = binge_username[0].value;
          var password = binge_password[0].value;

          //  request.client.datetime = (new Date(_bill._created)).toISOString().substring(0, 19).replace('T', ' ');
          var bill = {
            client: {
              datetime: new Date().toISOString().substring(0, 19).replace('T', ' '),
              orderno: bingeBill.bingeOrderNo
            }
          };

          $http({
            method: 'POST',
            url: '/api/payments/binge/cancel',
            data: {
              request: bill,
              auth: btoa(username + ':' + password),
              deployment_id: localStorageService.get('deployment_id'),
              tenant_id: localStorageService.get('tenant_id'),
              billId: $scope.bill._id
            }
          }).success(function (data) {
            updateAfterCancelBill('binge');
          }).error(function (err) {
            console.log(err);
            growl.error("Bill could not be cancelled",{ttl:3000});
          });
        }
      }

      function cancelRupleeBill(rupleeBill){
        var present=false;
        var ruplee_settings = _.filter(localStorageService.get('settings'), {"name": "Ruplee MerchantId","selected": true});
        if(ruplee_settings.length>0) {


          $http({
            method: 'GET',
            url: '/api/payments/ruplee/cancel',
            params: {deployment_id: localStorageService.get('deployment_id'), billId: rupleeBill.billId}
          }).success(function (data) {
            updateAfterCancelBill('ruplee');
          }).error(function (err) {
            console.log(err);
          })
        }
      }

      function cancelPaytmBill(paytmBill,index){
        var data={
          "request":
          {
            "txnAmount": $scope.bill.aggregation.netRoundedAmount.toString(),
            "currencyCode": "INR",
            "merchantGuid":paytmBill.merchant_guid,
            "merchantOrderId":paytmBill.order_id,
            // "industryType": "Retail108",
            "stateId":paytmBill.state_id,
            "action":"cancel",
            "comment":"Cancel Bill"
          },
          "platformName": "PayTM",
          "ipAddress": "139.162.28.200",
          "channel": "POS",
          "version": "1.0",
          "operationType": "WITHDRAW_MONEY"
        }

        $http({
          method:'POST',
          url:'/api/payments/paytm/cancel',
          data:{body:data,mobile:paytmBill.mobile,mid:paytmBill.merchant_guid,deployment_id:localStorageService.get('deployment_id'),tenant_id:localStorageService.get('tenant_id'),secret_key:paytmBill.secret_key}
        }).success(function(data){
          updateAfterCancelBill('paytm');
        }).error(function(err){
          console.log(err);
          growl.error("Bill Could not be cancelled",{ttl:3000});
        })
      }

      function updateAfterCheckStatus(settleValue){
        var i=getPaymentPartner(settleValue);
        $scope.bill.onlineTransaction[i].billData.status='paid';
        var data=$scope.bill.onlineTransaction;
        console.log(data);
         $scope.bill.updateBill({
          onlineTransaction:data
        }, 'update').then(function () {
          console.log("updated bill",$scope.bill);
          growl.success("Payment Sucessful",{ttl:3000});
        },function(err){
          console.log("Error in updating bill");
          growl.error("Payment Successful",{ttl:3000});
        });
      }

      function updateAfterCancelBill(settleValue){

        var index=-1;
        for(var i=0;i<$scope.bill._otherCards.length;i++){
          if($scope.bill._otherCards[i].otherName.toLowerCase()=='mobikwik'){
            index=i;
            break;
          }
        }
        $scope.bill._otherCards.splice(index,1);
        $scope.bill.otherForm.amount=$scope.bill.getAmountRemaining();

        var i=getPaymentPartner(settleValue);
        var data=$scope.bill.onlineTransaction;
        data.splice(i,1);

        $scope.bill.updateBill({
          onlineTransaction:data
        }, 'update').then(function () {
          console.log("updated bill",$scope.bill);
          growl.success("Bill Cancelled",{ttl:3000});
        },function(err){
          console.log("Error in updating bill");
          growl.error("Could not cancel bill",{ttl:3000});
        });
      }

      function getPaymentPartner(partner){
        var index=-1;
        for(var i=0;i<$scope.bill.onlineTransaction.length;i++){
          if($scope.bill.onlineTransaction[i].paymentType==partner.toLowerCase()){
            index=i;
            break;
          }
        }
        return index;
      }

      if($scope.settleSettingsOther.length>0 && $scope.settleSettingsOther[0].name){
        var name=$scope.settleSettingsOther[0].name;
        $scope.checkPaymentOption(name);
      }

      $rootScope.settleModalClose=false;
     $scope.paymentDone=false;
     $rootScope.paymentDone=false;
    $rootScope.$watch('paymentDone',function() {
      if($rootScope.paymentDone==true)
      $scope.paymentDone=true;
    });

      $rootScope.$watch('settleModalClose',function() {
        if($rootScope.settleModalClose) {
          $modalInstance.close('cancel');
        }
      });
      //---added for coupon settlement--//
      $scope.couponSettings=[{name:'Sodexo'},{name:'Ticket'},{name:'Accor'},{name:'Meal Vouchers'}];
       $scope.couponValue=$scope.couponSettings.length>0?$scope.couponSettings[0]:{name:'Select'};

       $scope.tabClassOther=function(tab){
        var clas='';
        if(($scope.bill._cash.length+$scope.bill._creditCards.length+$scope.bill._debitCards.length+$scope.bill._coupons.length)>0 && !$scope.partialPayment)
        clas='disabledButton';
        return clas;
        }

        $scope.tabClass=function(tab){
          if($scope.bill._onlineCards.length>0 && !$scope.partialPayment)
            return 'disabledButton';
          return '';
        }
      //--added for coupon settlement---//
      $scope.addManualCash=function(key,value){
        $scope.bill.addCashWithoutDenominations();
        //console.log(value)
      }

      intercom.registerEvent('settlement');

  }]);
