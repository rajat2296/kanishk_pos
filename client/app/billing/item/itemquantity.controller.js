'use strict';

angular.module('posistApp')
  .controller('ItemquantityCtrl', ['$scope', '$modalInstance', 'Pagination','itemIndex', 'taxes', 'bill', 'Utils', 'growl', 'isKot', 'kotIndex', 'addons', 'discountStatus', 'isAddonCompulsory', 'settings','user', function ($scope, $modalInstance, Pagination,itemIndex, taxes, bill, Utils, growl, isKot, kotIndex, addons, discountStatus, isAddonCompulsory, settings,user) {
    // alert(kotIndex);
    $scope.discountStatus = discountStatus;
    $scope.offerDetailForNotification = null;
    $scope.offerRemoved = null;
    //console.log(addons);
    $scope.addons = angular.copy(addons);
    $scope.itemIndex = itemIndex;
    $scope.taxes = taxes;
    $scope.bill = angular.copy(bill);
    $scope.isKot = isKot;
    $scope.settings = settings;

   
    var _renderCounts = {
      items: 12,
      categories:6
    }
    $scope.filteredAddOn = $scope.addons;
    $scope.addonPagination = Pagination.getNew(_renderCounts.items);
    $scope.addonPagination.totalItems = $scope.filteredAddOn.length;
    $scope.addonPagination.perPage = 12;
    $scope.addonPagination.numPages = Math.ceil($scope.filteredAddOn.length / $scope.addonPagination.perPage);
    console.log($scope.addonPagination.numPages);

$scope.addoncategoryItems = [];
var filter=_.uniq($scope.filteredAddOn, function(addon){
  return addon.category.categoryName;
});

_.forEach(filter,function(f){
      $scope.addoncategoryItems.push(f.category);
})

   
      //$scope.addoncategoryItems = _tabData[0].addoncategories;
      $scope.addoncategoryPagination = Pagination.getNew(_renderCounts.categories);
      $scope.addoncategoryPagination.totalItems = $scope.addoncategoryItems.length;
      $scope.addoncategoryPagination.perPage = 6;
      $scope.addoncategoryPagination.numPages = Math.ceil($scope.addoncategoryItems.length / $scope.addoncategoryPagination.perPage);
   

 $scope.addonFilter= function(catName) {

      console.log('Cat Filter called', catName);
      if (catName === '' || catName == $scope.lastFilteredCategory) {
        $scope.filteredAddOn = $scope.addons;
        $scope.lastFilteredCategory="";
      } else {
        $scope.lastFilteredCategory=catName;
        $scope.filteredAddOn = _.filter($scope.addons, {category: {categoryName: catName}});


        $scope.addonPagination.numPages = Math.ceil($scope.filteredAddOn.length / $scope.addonPagination.perPage);
        $scope.addonPagination.totalItems = $scope.filteredAddOn.length;
        $scope.addonPagination.page = 0;

      }
    };

    $scope.itemOffer = (isKot) ? $scope.bill._kots[kotIndex].items[itemIndex] : $scope.bill._items[itemIndex];

    if (_.has($scope.itemOffer, 'addOns')) {
      _.forEach($scope.itemOffer.addOns, function (appliedAddon) {
        if (_.has(appliedAddon, 'quantity')) {
          var _f = _.filter($scope.addons, {_id: appliedAddon._id});
          if (_f.length > 0) {
            _f[0].quantity = appliedAddon.quantity;
          }
        }
      })
    }
    // console.log($scope.itemOffer);

    $scope.discountForm = {
      type: "percentage",
      value: "",
      comment: "",
      offerName: 'Express',
      itemComment: '',
      user:user
    };
    var _discount
    if (!isKot) {
      _discount = _.filter($scope.bill._items[itemIndex].discounts, {offerName: 'Express'});
      if (_.has($scope.bill._items[itemIndex], 'comment'))
        $scope.discountForm.itemComment = $scope.bill._items[itemIndex].comment;
    }
    else {
      _discount = _.filter($scope.bill._kots[kotIndex].items[itemIndex].discounts, {offerName: 'Express'});
      if (_.has($scope.bill._kots[kotIndex].items[itemIndex], 'comment'))
        $scope.discountForm.itemComment = $scope.bill._kots[kotIndex].items[itemIndex].comment;

    }
    if (_discount.length > 0) {
      var d = _discount[0];
      // $scope.discountForm.type=_discount[0].name;
      $scope.discountForm.type = d.type;
      $scope.discountForm.value = d.value;
      $scope.discountForm.comment = d.comment;
      $scope.discountForm.offerName = d.offerName;
    }

    /* Autofill Card Amounts */
    $scope.bill.debitCardForm.amount = $scope.bill.getAmountRemaining();
    $scope.bill.creditCardForm.amount = $scope.bill.getAmountRemaining();

    $scope.addDiscount = function (form) {
      if (_.has($scope.itemOffer, 'offer')) {
        $scope.itemOffer.comment = $scope.discountForm.itemComment;
        $scope.applyOnBill();
        return false;
      }
      $scope.submittedDiscForm = true;
      if (isKot == false) {
        if (!_.has($scope.bill._items[itemIndex], 'discounts')) {
          $scope.bill._items[itemIndex].discounts = [];
        }
        if (form.$valid) {
          if ($scope.discountForm.type === 'fixed') {
            var _pQ = $scope.bill._items[itemIndex].rate * $scope.bill._items[itemIndex].quantity;
            if ($scope.discountForm.value > _pQ) {
              growl.error('Fixed Amount discount is greater than item amount.', {ttl: 1000});
              return false;
            }
          }
          if ($scope.discountForm.type === 'percentage' && $scope.discountForm.value > 100) {
            growl.error("Cannot take a value greater than 100!", {ttl: 3000});
            return false;
          } else {
            if ($scope.discountForm.value > 0) {
              if ($scope.discountForm.comment == '') {
                growl.error("Comment for discount is mandatory.", {ttl: 3000});
                return false;
              }
            }
            $scope.offerRemoved = angular.copy($scope.bill._items[itemIndex].discounts);

            $scope.bill._items[itemIndex].discounts.splice(Utils.arrayObjectIndexOf($scope.bill._items[itemIndex].discounts, 'offerName', 'Express'));
            if ($scope.discountForm.value > 0)
              $scope.bill._items[itemIndex].discounts.push($scope.discountForm);
            $scope.bill._items[itemIndex].comment = $scope.discountForm.itemComment;
            form.$setPristine();
            $scope.offerDetailForNotification = angular.copy($scope.discountForm);
            $scope.discountForm = {
              type: "percentage",
              value: "",
              comment: "",
              offerName: 'Express',
              itemComment: '',
              user:user
            };
          }
        }
        else {
          growl.error('There are some error.', {ttl: 1000});
          return false;
        }
      } else {

        if (!_.has($scope.bill._kots[kotIndex].items[itemIndex], 'discounts')) {
          $scope.bill._kots[kotIndex].items[itemIndex].discounts = [];
        }
        if (form.$valid) {
          if ($scope.discountForm.type === 'fixed') {
            var _pQ = $scope.bill._kots[kotIndex].items[itemIndex].rate * $scope.bill._kots[kotIndex].items[itemIndex].quantity;
            if ($scope.discountForm.value > _pQ) {
              growl.error('Fixed Amount discount is greater than item amount.', {ttl: 1000});
              return false;
            }
          }
          if ($scope.discountForm.type === 'percentage' && $scope.discountForm.value > 100) {
            growl.error("Cannot take a value greater than 100!", {ttl: 3000});
            return false;
          } else {
            if ($scope.discountForm.value > 0) {
              if ($scope.discountForm.comment == '') {
                growl.error("Comment for discount is mandatory.", {ttl: 3000});
                return false;
              }
            }
            $scope.offerRemoved = angular.copy($scope.bill._kots[kotIndex].items[itemIndex].discounts);
            $scope.bill._kots[kotIndex].items[itemIndex].discounts.splice(Utils.arrayObjectIndexOf($scope.bill._kots[kotIndex].items[itemIndex].discounts, 'offerName', 'Express'));
            if ($scope.discountForm.value > 0)
              $scope.bill._kots[kotIndex].items[itemIndex].discounts.push($scope.discountForm);
            $scope.bill.calculateKotTotalDiscount();

            /* angular.forEach($scope.bill._kots, function (kot) {
             var _d = 0;
             angular.forEach(kot.items, function (item){
             angular.forEach(item.discounts, function (discount) {
             if (discount.type === 'percentage') {
             _d += parseFloat(discount.discountAmount);
             }
             if (discount.type === 'fixed') {
             _d += parseFloat(discount.value);
             }

             });
             });
             kot.totalDiscount = parseFloat(_d);
             });*/

            $scope.bill._kots[kotIndex].items[itemIndex].comment = $scope.discountForm.itemComment;
            form.$setPristine();
            $scope.offerDetailForNotification = angular.copy($scope.discountForm);
            $scope.discountForm = {
              type: "percentage",
              value: "",
              comment: "",
              offerName: 'Express',
              itemComment: '',
              user:user
            };
          }
        }
        else {
          growl.error('There are some error.', {ttl: 1000});
          return false;
        }
      }
      $scope.applyOnBill();
    };
    $scope.removeDiscount = function (index) {
      $scope.offerRemoved = angular.copy($scope.bill._items[itemIndex].discounts);
      $scope.bill._items[itemIndex].discounts.splice(index, 1);
      if ($scope.bill._items[itemIndex].discounts.length === 0) {
        if (_.has($scope.bill._items[itemIndex], 'isDiscounted')) {
          delete $scope.bill._items[itemIndex].isDiscounted;
        }
        if (_.has($scope.bill._items[itemIndex], 'rateBeforeDiscount')) {
          delete $scope.bill._items[itemIndex].rateBeforeDiscount;
        }

        delete $scope.bill._items[itemIndex].discounts;
      }
    };

    $scope.updateQuantity = function () {
      $modalInstance.close($scope.item);
    };

    $scope.incrementQuantity = function () {
      var q = parseInt($scope.item.quantity);
      q += 1;
      $scope.item.quantity = q;
    };

    $scope.decrementQuantity = function () {
      var q = parseInt($scope.item.quantity);
      q -= 1;
      $scope.item.quantity = q;
    };

    $scope.cancel = function () {

      if (isAddonCompulsory) {
        console.log("cancel called");
        console.log(isAddonCompulsory);
        var flag = false;
        if (isKot) {
          $modalInstance.dismiss('cancel');
          }
         else {
          if ($scope.bill._items[itemIndex].addOns == undefined || $scope.bill._items[itemIndex].addOns.length == 0) {
            flag = true;
          }
        }
        if (flag) {
          $scope.bill.removeItem($scope.bill._items.length - 1);
          $scope.bill.processBill();
        }

        $modalInstance.close({bill: $scope.bill});
      }
      else {
        $modalInstance.dismiss('cancel');
      }

    };

    $scope.applyOnBill = function (options) {

      var f_addons = [];
      _.forEach($scope.addons, function (tAddon) {
        if (_.has(tAddon, 'quantity')) {
          if (tAddon.quantity > 0) {
            f_addons.push(tAddon);
          }
        }
      })
      if (isAddonCompulsory) {
        if($scope.addons.length==0)
        {
            $modalInstance.close({
              bill: $scope.bill,
              notification: $scope.offerDetailForNotification,
              item: $scope.itemOffer.name,
              offerRemoved: $scope.offerRemoved
            });
        }
        else if ($scope.tempAddOn.length > 0) {
          if (isKot) {
            $scope.bill._kots[kotIndex].items[itemIndex].addOns = f_addons;
          } else {
            $scope.bill._items[itemIndex].addOns = f_addons;
          }
          $modalInstance.close({
            bill: $scope.bill,
            notification: $scope.offerDetailForNotification,
            item: $scope.itemOffer.name,
            offerRemoved: $scope.offerRemoved
          });
        }
        else {
          growl.error("AddOn Selection is Compulsory. Add atleast 1 AddOn", {ttl: 4000});
        }
      }
      else {
        if (isKot) {
          $scope.bill._kots[kotIndex].items[itemIndex].addOns = f_addons;
        } else {
          $scope.bill._items[itemIndex].addOns = f_addons;
        }
        $modalInstance.close({
          bill: $scope.bill,
          notification: $scope.offerDetailForNotification,
          item: $scope.itemOffer.name,
          offerRemoved: $scope.offerRemoved
        });
      }

    };

    $scope.updateSelectedTax = function (action, tax) {
      if (action == 'add' && Utils.arrayObjectIndexOf($scope.bill._items[$scope.itemIndex].taxes, 'name', tax.name) == -1) {
        $scope.bill._items[$scope.itemIndex]._taxes.push(tax);
      }
      if (action == 'remove' && Utils.arrayObjectIndexOf($scope.bill._items[$scope.itemIndex].taxes, 'name', tax.name) != -1) {
        $scope.bill._items[$scope.itemIndex]._taxes.splice(Utils.arrayObjectIndexOf($scope.bill._items[$scope.itemIndex].taxes, 'name', tax.name), 1);
      }
    };
    $scope.setSelectedTax = function (event, tax) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      $scope.updateSelectedTax(action, tax);
    };
    $scope.tempAddOn = [];
    if (!isKot) {
      if($scope.bill._items[itemIndex].addOns)
      $scope.tempAddOn= $scope.bill._items[itemIndex].addOns;
    }
    else {
      if($scope.bill._kots[kotIndex].items[itemIndex].addOns)
      $scope.tempAddOn= $scope.bill._kots[kotIndex].items[itemIndex].addOns;
    }
    $scope.addAddonItem = function (item) {

      var match = _.find($scope.tempAddOn, {_id: item._id});
      if (match) {
        var index = _.indexOf($scope.tempAddOn, match);
        $scope.tempAddOn.splice(index, 1, item);
      } else {
        $scope.tempAddOn.push(item);
      }

      if (Utils.hasSetting('max_n_addon', $scope.settings)) {
        var maxNumberAddon = parseInt(Utils.getSettingValue('max_n_addon', $scope.settings))
        var numberOfAddon = $scope.tempAddOn.length;
        console.log(numberOfAddon);
        if (numberOfAddon <= maxNumberAddon) {
          var f_item = _.filter($scope.addons, {_id: item._id});
          // console.log(f_item);
          if (f_item.length > 0) {
            if (_.has(f_item[0], 'quantity')) {
              if (Utils.hasSetting('max_q_addon', $scope.settings)) {
                var maxAddon = parseInt(Utils.getSettingValue('max_q_addon', $scope.settings))
                //console.log(maxAddon);
                if (f_item[0].quantity == maxAddon) {
                  growl.error("Maximum Quantity of each AddOn Exceed", {ttl: 3000})
                }
                else {
                  f_item[0].quantity += 1;
                }
              }
              else {
                f_item[0].quantity += 1;
              }

            } else {
              f_item[0].quantity = 1;
            }

            var match = _.find($scope.tempAddOn, {_id: f_item[0]._id});
            if (match) {
              var index = _.indexOf($scope.tempAddOn, match);
              $scope.tempAddOn.splice(index, 1, f_item[0]);
            } else {
              $scope.tempAddOn.push(f_item[0]);
            }

          }
        }
        else {
          $scope.tempAddOn.splice($scope.tempAddOn.length - 1, 1);
          growl.error("Maximum Number of AddOn Exceed", {ttl: 3000})
        }

      }
      else {
        var f_item = _.filter($scope.addons, {_id: item._id});
        // console.log(f_item);
        if (f_item.length > 0) {
          if (_.has(f_item[0], 'quantity')) {
            if (Utils.hasSetting('max_q_addon', $scope.settings)) {
              var maxAddon = parseInt(Utils.getSettingValue('max_q_addon', $scope.settings))
              console.log(maxAddon);
              if (f_item[0].quantity == maxAddon) {
                growl.error("Maximum Quantity of each AddOn Exceed", {ttl: 3000})
              }
              else {
                f_item[0].quantity += 1;
              }
            }
            else {
              f_item[0].quantity += 1;

            }

          } else {
            f_item[0].quantity = 1;

          }


        }


      }


    }
    $scope.deleteAddon = function (item) {
      var match = _.find($scope.tempAddOn, {_id: item._id});
      if (match) {
        var index = _.indexOf($scope.tempAddOn, match);
        $scope.tempAddOn.splice(index, 1);
      }
      var f_item = _.filter($scope.addons, {_id: item._id});
      console.log(f_item);
      if (f_item.length > 0) {
        delete f_item[0].quantity;
      }
    }
    Array.prototype.chunk = function (chunkSize) {
      console.log(chunkSize);
      var array = this;
      return [].concat.apply([],
        array.map(function (elem, i) {
          return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
        })
      );
    }


  }]);