//
//angular.module('posistApp')
//    .config(['$stateProvider','$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
//
//        $stateProvider
//            .state('billing', {
//                url: '/billing',
//                templateUrl: 'app/billing/billing.html',
//                controller: 'BillingCtrl',
//                resolve: {
//                    currentUser: ['$rootScope', '$state', '$stateParams', 'Auth', 'Tab', 'growl', 'localStorageService', '$location',
//                        function ($rootScope, $state, $stateParams, Auth, Tab, growl, localStorageService, $location) {
//                            console.log("Billing State");
//                            return Auth.getCurrentUser().$promise.then(function (user) {
//                                $rootScope.currentUser = user;
//
//                                if (!localStorageService.get('deployment_id')) {
//                                    growl.error('No Deployment ID set, cannot start billing!', {ttl:3000});
//                                    $location.path('/');
//                                } else {
//
//                                    /* Getting Tabs */
//                                    /* Tab.get({tenant_id:$rootScope.currentUser.tenant_id, deployment_id:$rootScope.currentUser.deployment_id}, function (tabs) {
//                                     $rootScope.tabs = tabs;
//                                     *//*console.log($rootScope.tabs);*//*
//                                     });*/
//
//                                    return user;
//                                }
//
//                            });
//                        }],
//                    //customers: ['$rootScope', 'Sync', 'growl', function ($rootScope, Sync, growl) {
//                    //    console.log("Get Customers Billing State");
//                    //   /* Customers Table *//**//**//**//*
//                    //    *//**//**//**//*$rootScope.customers = [];
//                    //    return Sync.syncCustomers().then(function (customers) {
//                    //        return $rootScope.customers = customers;
//                    //    });*/
//                    //    return Sync.syncCustomersNew().then(function (message) {
//                    //        growl.success(message, {ttl:3000});
//                    //    }).catch(function (errMessage) {
//                    //        growl.error(errMessage, {ttl:3000});
//                    //    });
//                    //
//                    //}],
//                    //items: ['$rootScope', 'Sync', function ($rootScope, Sync) {
//                    //
//                    //   /* Items Table*/
//                    //    $rootScope.items = [];
//                    //    return Sync.syncItems($rootScope.currentUser).then(function (items) {
//                    //        return $rootScope.items = items;
//                    //    });
//                    //
//                    //}],
//                    offlineBills: ['$q', '$rootScope', 'Sync', '$stateParams', function ($q, $rootScope, Sync, $stateParams) {
//
//                        var d = $q.defer();
//                        /*Sync.syncOfflineBills().then(function (bills) {
//                         d.resolve(bills);
//                         });*/
//                        d.resolve([]);
//                        return d.promise;
//
//                    }],
//
//                    syncAll:['$rootScope','Sync', '$modal', '$q',function($rootScope,Sync,$modal, $q){
//                        var d = $q.defer();
//                        $modal.open({
//                            template: '<h4 style="margin:20px;"><i class="fa fa-spinner fa-spin"></i> Syncing, please wait...</h4>',
//                            controller: ['$scope', 'Sync', '$modalInstance', '$timeout', function ($scope, Sync, $modalInstance, $timeout) {
//
//                                $timeout(function () {
//                                    Sync.syncAll().then(function(data){
//                                        console.log($rootScope.tabs);
//                                        $modalInstance.close("Sync Complete");
//                                        // return data;
//                                    });
//                                }, 10);
//                            }],
//                            size: 'sm'
//                        }).result.then(function (message) {
//                                d.resolve(message);
//                                console.log(message);
//                            });
//
//                        return d.promise;
//                        /*return Sync.syncAll().then(function(data){
//                         console.log($rootScope.tabs);
//                         // return data;
//                         })*/
//
//                    }],
//                    settings: ['Sync', function (Sync){
//
//                        return Sync.syncSettings().then(function (settings) {
//                            /*console.log(settings);*/
//                            return settings;
//                        });
//                        //  return [];
//                    }],
//                    dataTables:[function(){return {tables:[],selectedTables:{}}}]
//                },
//                data:{tables:[],selectedTable: {}
//                }
//                /* data:
//
//                 {
//                 tables:function() {
//                 var tablestemp=[];
//                 for(var i=0;i<100;i++){
//                 var tableOb={"id":(i+1),name:("Table "+(i+1))};
//                 tablestemp.push(tableOb);
//                 }
//                 console.log(tablestemp);
//                 return {tablestemp,};
//                 //   $rootScope.tables
//                 }
//                 *//*  [
//                 {"id": 1, "name": "Table 1"},
//                 {"id": 2, "name": "Table 2"},
//                 {"id": 3, "name": "Table 3"},
//                 {"id": 4, "name": "Table 4"},
//                 {"id": 5, "name": "Table 5"},
//                 {"id": 6, "name": "Table 6"},
//                 {"id": 7, "name": "Table 7"},
//                 {"id": 8, "name": "Table 8"},
//                 {"id": 9, "name": "Table 9"},
//                 {"id": 10, "name": "Table 10"},
//                 {"id": 11, "name": "Table 11"},
//                 {"id": 12, "name": "Table 12"},
//                 {"id": 13, "name": "Table 13"},
//                 {"id": 14, "name": "Table 14"},
//                 {"id": 15, "name": "Table 15"},
//                 {"id": 16, "name": "Table 16"}
//                 ,
//                 {"id": 17, "name": "Table 17"},
//                 {"id": 18, "name": "Table 18"},
//                 {"id": 19, "name": "Table 19"},
//                 {"id": 20, "name": "Table 20"},
//                 {"id": 21, "name": "Table 21"},
//                 {"id": 22, "name": "Table 22"},
//                 {"id": 23, "name": "Table 23"},
//                 {"id": 24, "name": "Table 24"},
//                 {"id": 25, "name": "Table 25"},
//                 {"id": 26, "name": "Table 26"},
//                 {"id": 27, "name": "Table 27"},
//                 {"id": 28, "name": "Table 28"},
//                 {"id": 29, "name": "Table 29"},
//                 {"id": 30, "name": "Table 30"},
//                 {"id": 31, "name": "Table 31"},
//                 {"id": 32, "name": "Table 32"},
//                 {"id": 33, "name": "Table 33"},
//                 {"id": 34, "name": "Table 34"},
//                 {"id": 35, "name": "Table 35"},
//                 {"id": 36, "name": "Table 36"},
//                 {"id": 37, "name": "Table 37"},
//                 {"id": 38, "name": "Table 38"},
//                 {"id": 39, "name": "Table 39"},
//                 {"id": 40, "name": "Table 40"},
//                 {"id": 41, "name": "Table 41"},
//                 {"id": 42, "name": "Table 42"},
//                 {"id": 43, "name": "Table 43"},
//                 {"id": 44, "name": "Table 44"},
//                 {"id": 45, "name": "Table 45"},
//                 {"id": 46, "name": "Table 46"},
//                 {"id": 47, "name": "Table 47"},
//                 {"id": 48, "name": "Table 48"},
//                 {"id": 49, "name": "Table 49"},
//                 {"id": 50, "name": "Table 50"},
//                 {"id": 51, "name": "Table 51"},
//                 {"id": 52, "name": "Table 52"},
//                 {"id": 53, "name": "Table 53"},
//                 {"id": 54, "name": "Table 54"},
//                 {"id": 55, "name": "Table 55"},
//                 {"id": 56, "name": "Table 56"},
//                 {"id": 57, "name": "Table 57"},
//                 {"id": 58, "name": "Table 58"},
//                 {"id": 59, "name": "Table 59"},
//                 {"id": 60, "name": "Table 60"},
//                 {"id": 61, "name": "Table 61"},
//                 {"id": 62, "name": "Table 62"},
//                 {"id": 63, "name": "Table 63"},
//                 {"id": 64, "name": "Table 64"},
//                 {"id": 65, "name": "Table 65"},
//                 {"id": 66, "name": "Table 66"},
//                 {"id": 67, "name": "Table 67"},
//                 {"id": 68, "name": "Table 68"},
//                 {"id": 69, "name": "Table 69"},
//                 {"id": 70, "name": "Table 70"},
//                 {"id": 71, "name": "Table 71"},
//                 {"id": 72, "name": "Table 72"},
//                 {"id": 73, "name": "Table 73"},
//                 {"id": 74, "name": "Table 74"},
//                 {"id": 75, "name": "Table 75"},
//                 {"id": 76, "name": "Table 76"},
//                 {"id": 77, "name": "Table 77"},
//                 {"id": 78, "name": "Table 78"},
//                 {"id": 79, "name": "Table 79"},
//                 {"id": 80, "name": "Table 80"},
//                 {"id": 81, "name": "Table 81"},
//                 {"id": 82, "name": "Table 82"},
//                 {"id": 83, "name": "Table 83"},
//                 {"id": 84, "name": "Table 84"},
//                 {"id": 85, "name": "Table 85"},
//                 {"id": 86, "name": "Table 86"},
//                 {"id": 87, "name": "Table 87"},
//                 {"id": 88, "name": "Table 88"},
//                 {"id": 89, "name": "Table 89"},
//                 {"id": 90, "name": "Table 90"},
//                 {"id": 91, "name": "Table 91"},
//                 {"id": 92, "name": "Table 92"},
//                 {"id": 93, "name": "Table 93"},
//                 {"id": 94, "name": "Table 94"},
//                 {"id": 95, "name": "Table 95"},
//                 {"id": 96, "name": "Table 96"},
//                 {"id": 97, "name": "Table 97"},
//                 {"id": 98, "name": "Table 98"},
//                 {"id": 99, "name": "Table 99"},
//                 {"id": 100, "name": "Table 100"}
//                 ]
//                 ,
//                 selectedTable: {}*//*
//                 }*/
//            })
//            .state('billing.table', {
//                url: '/table/:tabId',
//                templateUrl: 'app/billing/_table.html',
//                controller: 'BillingCtrl',
//                resolve: {
//                    offlineBills: ['$q', 'Sync', '$stateParams', function ($q, Sync, $stateParams) {
//
//                        var d = $q.defer();
//                        Sync.syncOfflineBills($stateParams.tabId).then(function (bills) {
//                            // console.log(bills);
//                            d.resolve(bills);
//                        });
//                        return d.promise;
//                    }],
//                    dataTables:['$q', 'Sync',function($q,Sync){
//                        var tablesTemp=[];
//                        for(var i=0; i<100; i++){
//                            var tableOb={"id":i+1,name:"Table "+(i+1)};
//                            tablesTemp.push(tableOb);
//                        }
//                        var dataOb={};
//                        dataOb.tables=tablesTemp;
//                        dataOb.selectedTable={};
//                        return  dataOb;
//                    }]
//                }
//            })
//            .state('billing.table.bill', {
//                url: '/bill/:tableId/:covers/:tabId',
//                templateUrl: 'app/billing/_billing.html',
//                controller: 'BillingCtrl',
//                parent:'billing'
//            })
//            .state('billing.takeout', {
//                url: '/takeout/:tabId',
//                templateUrl: 'app/billing/_billing.html',
//                controller: 'BillingCtrl',
//                resolve: {
//                    offlineBills: ['$q', '$rootScope', 'Sync', '$stateParams', function ($q, $rootScope, Sync, $stateParams) {
//                        /* var d = $q.defer();
//                         Sync.syncOfflineBills($stateParams.tabId).then(function (bills) {
//                         //console.log(bills);
//                         d.resolve(bills);
//                         });
//                         return d.promise;*/
//                        return [];
//                    }],
//                    dataTables:[function(){return {tables:[],selectedTables:{}}}]
//                }
//            })
//            .state('billing.delivery', {
//                url: '/delivery/:tabId',
//                templateUrl: 'app/billing/_billing.html',
//                controller: 'BillingCtrl',
//                resolve: {
//                    offlineBills: ['$q', '$rootScope', 'Sync', '$stateParams', function ($q, $rootScope, Sync, $stateParams) {
//
//                        var d = $q.defer();
//                        Sync.syncOfflineBills($stateParams.tabId).then(function (bills) {
//                            d.resolve(bills);
//                        });
//                        return d.promise;
//                    }],
//                    dataTables:[function(){return {tables:[],selectedTables:{}}}]
//                }
//            })
//            .state('billing.delivery.bill', {
//                url: '/delivery/bill/:tabId/:billId',
//                templateUrl: 'app/billing/_billing.html',
//                controller: 'BillingCtrl',
//                parent:'billing',
//                resolve: {
//                    offlineBills: ['$q', '$rootScope', 'Sync', '$stateParams', function ($q, $rootScope, Sync, $stateParams) {
//                        var d = $q.defer();
//                        Sync.syncOfflineBills($stateParams.tabId).then(function (bills) {
//                            d.resolve(bills);
//                        });
//                        return d.promise;
//                    }],
//                    dataTables:[function(){return {tables:[],selectedTables:{}}}]
//                }
//            })
//            .state('billing.takeout.bill', {
//                url: '/delivery/bill/:tabId/:billId',
//                templateUrl: 'app/billing/_billing.html',
//                controller: 'BillingCtrl',
//                parent:'billing',
//                resolve: {
//                    offlineBills: ['$q', '$rootScope', 'Sync', '$stateParams', function ($q, $rootScope, Sync, $stateParams) {
//                        var d = $q.defer();
//                        Sync.syncOfflineBills($stateParams.tabId).then(function (bills) {
//                            d.resolve(bills);
//                        });
//                        return d.promise;
//                    }],
//                    dataTables:[function(){return {tables:[],selectedTables:{}}}]
//                }
//            })
//
//            .state('billing.edit', {
//                url: '/edit/:tabId/:billId',
//                templateUrl:'app/billing/_billing.html',
//                controller:'BillingCtrl',
//                parent:'billing',
//                resolve: {
//                    offlineBills: ['$q', '$rootScope', 'Sync', '$stateParams', function ($q, $rootScope, Sync, $stateParams) {
//                        var d = $q.defer();
//                        Sync.syncOfflineBills($stateParams.tabId).then(function (bills) {
//                            d.resolve(bills);
//                        });
//                        return d.promise;
//                    }],
//                    dataTables:[function(){return {tables:[],selectedTables:{}}}]
//                }
//            });
//
//    }]);
