'use strict';

angular.module('posistApp')
  .config(['$stateProvider','$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('billing', {
        url: '/billing',
        templateUrl: 'app/billing/_billing.html',
        controller: 'BillingCtrl',
        resolve: {
          currentUser: ['$rootScope', '$state', '$stateParams', 'Auth', 'Tab', 'growl', 'localStorageService', '$location', 'Deployment',
            function ($rootScope, $state, $stateParams, Auth, Tab, growl, localStorageService, $location, Deployment) {
              if (!localStorageService.get('deployment_id')) {
                growl.error('No Deployment ID set, cannot start billing!', {ttl:3000});
                $location.path('/');
              }else {
                Deployment.findOne({id: localStorageService.get('deployment_id')}, function(deployment){
                  if (deployment.isBaseKitchen) {
                    growl.error('Cannot start billing from Base Kitchen!', {ttl:3000});
                    window.location = "/";
                  }
                  return localStorageService.get('currentUser');
                });
              }
              return localStorageService.get('currentUser');
            }],
          offlineBills: ['$q', '$rootScope', 'Sync', '$stateParams','localStorageService', function ($q, $rootScope, Sync, $stateParams,localStorageService) {
            $rootScope.db.selectDynamic('select max(created) as date  from bills').then(function(result){
              $rootScope.offlineBill={lastCreated:new Date()};
              if(result.rows.length>0){
                $rootScope.offlineBill.lastCreated = result.rows.item(0).date;
              }
            })
          }],

          syncAll:['$rootScope','Sync', '$modal', '$q','localStorageService','growl',function($rootScope,Sync,$modal, $q,localStorageService,growl){
            var d = $q.defer();
            if (!localStorageService.get('deployment_id')){
              growl.error('No Deployment ID set, cannot start billing!', {ttl:3000});
              d.reject();
            }
            else {
              //var d = $q.defer();
              $modal.open({
                template: '<h4 style="margin:20px;"><i class="fa fa-spinner fa-spin"></i> Syncing, please wait...</h4>',
                backdrop: 'static',
                keyboard: 'false',
                controller: ['$scope', 'Sync', '$modalInstance', '$timeout', function ($scope, Sync, $modalInstance, $timeout) {
                  $timeout(function () {
                    Sync.syncAll().then(function (data) {
                      $modalInstance.close("Sync Complete");
                      Sync.syncInstancePrinter();
                      Sync.syncInstanceStationPrinter();
                      Sync.syncComplimentaryHead();
                      Sync.syncCharges();
                      Sync.syncMasterStation();
                    }).catch(function () {
                      $modalInstance.close("Sync error... loading from offline data.");
                      d.resolve();
                    });
                  }, 10);
                }],
                size: 'sm'
              }).result.then(function (message) {
                d.resolve(message);
              });
            }
            return d.promise;
            /*return Sync.syncAll().then(function(data){
             console.log($rootScope.tabs);
             // return data;
             })*/

          }],
          settings: ['Sync', function (Sync){

            return Sync.syncSettings().then(function (settings) {
              //console.log(settings);
              return settings;
            });
            //  return [];
          }],
          dataTables:['$q', 'Sync','localStorageService','Utils',function($q,Sync,localStorageService,Utils){
            var ntables= Utils.getSettingValue('n_o_tables', localStorageService.get('settings'));
            var noOfTables=100;
            if(parseInt(ntables)){
              noOfTables=parseInt(ntables);
            }
            var tablesTemp=[];
            for(var i=0; i<noOfTables; i++){
              var tableOb={"id":i+1,name:"Table "+(i+1)};
              tablesTemp.push(tableOb);
            }
            var dataOb={};
            dataOb.tables=tablesTemp;
            dataOb.selectedTable={};
            dataOb.openedBills=[];
            return  dataOb;
          }]
        },
        data:{tables:[],selectedTable: {}
        }

      })


  }]);
