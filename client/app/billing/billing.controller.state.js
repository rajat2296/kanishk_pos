///**
// * Created by Ranjeet Sinha on 4/18/2015.
// */
//'use strict';
//
//angular.module('posistApp')
//    //.controller('BillingCtrl', ['$scope', '$rootScope', '$state', '$modal', '$timeout', 'currentUser', '$webSql', 'Utils', 'Bill', 'Bills', 'socket', 'growl', 'lzw', 'printer', 'customers', '$location', 'Auth', 'items', 'Deployment', 'Category', 'offlineBills', 'localStorageService', 'User', 'Customer', 'Sync', 'Tab', 'Pagination','settings', function ($scope, $rootScope, $state, $modal, $timeout, currentUser, $webSql, Utils, Bill, Bills, socket, growl, lzw, printer, customers, $location, Auth, items, Deployment, Category, offlineBills, localStorageService, User, Customer, Sync, Tab, Pagination, settings) {
//    .controller('BillingCtrl', ['$scope', '$rootScope', '$state', '$modal', '$timeout', 'currentUser', '$webSql', 'Utils', 'Bill', 'Bills', 'socket', 'growl', 'lzw', 'printer', '$location', 'Auth', 'Deployment', 'Category', 'offlineBills', 'localStorageService', 'User', 'Customer', 'Sync', 'Tab', 'Pagination','settings','syncAll','$q','$interval','dataTables', function ($scope, $rootScope, $state, $modal, $timeout, currentUser, $webSql, Utils, Bill, Bills, socket, growl, lzw, printer, $location, Auth, Deployment, Category, offlineBills, localStorageService, User, Customer, Sync, Tab, Pagination, settings, syncAll,$q,$interval,dataTables ) {
//
//        //  console.log(syncAll.items);
//        /* Offline Bills */
//        $scope.bills = offlineBills;
//
//        /* Current Tab */
//        $scope.tab = "";
//        $scope.tabId = "";
//        $scope.tabType = "";
//
//        /* Table List */
//        $scope.tables = [];
//        $scope.gotoTableNumber = 1;
//
//
//        /* Table Details */
//        $scope.table;
//
//        /* Master Items */
//        $scope.masterItems = [];
//        $scope.explorerItems = [];
//
//        /* Categories */
//        $scope.categories = [];
//        $scope.categoriesT=[];
//
//        /* Bill */
//        $scope.bill = {};
//        $scope.billableItems = $rootScope.masterItems;
//
//        /* Settings - Billing */
//        $scope.settings = settings;
////        $scope.showKot=(Utils.hasSetting('dis_kot', $scope.settings))?false:(Utils.hasSetting('print_kot_bill', $scope.settings))?false:true;
//
//        /* Users */
//        $scope.users = $rootScope.users;
//
//        $scope.itemCurrentIndex=0;
//
//
//        function getItemById(itemId){
//            var itemob={};
//            angular.forEach($scope.masterItems,function(item){
//                if(item._id==itemId){
//                    itemob=angular.copy(item);
//                    angular.break;
//                }
//            });
//            return itemob;
//        }
//        $scope.getSocketStatus = function () {
//            socket.getStatus(function (data) {
//                console.log(data);
//            });
//        };
//
//        $scope.logout = function () {
//            Auth.logout();
//            $location.path('/login');
//        };
//
//        $scope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
//            console.info("TODO: Add Passcode Authentication to Table!");
//
//        });
//        $scope.tableDClass=function(bill){
//            var tableClass='table-main-grey';
//            if(bill){
//                if(bill.isPrinted==true){
//                    tableClass='table-main-green';
//                }else{
//                    tableClass= 'table-main-violet';
//                }
//            }
//            return tableClass;
//        };
//
//
//        $scope.seconds=0;
//        $scope.elapsedMinutes=0;
//
//        $interval(function(){getTime()},1000*60);
//        function getTime(){
//            $scope.elapsedMinutes++;
//            // $scope.seconds++;
//        }
//        $scope.currentDate=new Date();
//        $scope.getLapsedTime=function(bill,index,length){
//            if(!bill){return 0;}
//            if($scope.elapsedMinutes==0){
//                var miliseconds=$scope.currentDate-(Utils.convertDate( angular.copy( bill._created)));
//                var dminutes = Math.round( (miliseconds/(60*1000)),0);
//                var dhours =Math.round( (dminutes/(60)),0);
//                var ddays = Math.round(dhours/24,0);
//                bill.calulatedTime = {
//                    isCalculated: true,
//                    hr: dhours,
//                    mins: dminutes,
//                    sec: 20
//                };
//                $scope.seconds++;
//            }
//            var nbill=angular.copy(bill);
//            var elapsedMins=(nbill.calulatedTime.mins+$scope.elapsedMinutes);
//            var hr=nbill.calulatedTime.hr+(Math.round( ($scope.elapsedMinutes /60),0));
//            var mins=(nbill.calulatedTime.mins+$scope.elapsedMinutes)%60;
//            return (hr+":"+mins);
//        }
//        $scope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
//
//            /* Redirect to Admin if User is SuperAdmin */
//            /* if (currentUser.role === 'superadmin') {
//             $location.path('/admin');
//             }*/
//            if (toState.name === 'billing') {
//
//                // console.log('Select first available tab');
//                //console.log($rootScope.tabs);
//                if ($rootScope.tabs.length > 0) {
//                    console.log($rootScope.tabs[0]);
//                    $state.go($rootScope.tabs[0].route, {tabId:$rootScope.tabs[0]._id});
//                    //$scope.tab = $rootScope.tabs[0].tabName;
//                    //$scope.tabId = $rootScope.tabs[0].tabId;
//                    //$scope.tabType = $rootScope.tabs[0].tabType;
//                }
//            }
//
//            /* Table State */
//            if (toState.name === 'billing.table') {
//                // alert();
//                //console.log(dataTables);
//                $state.current.data.tables=angular.copy( dataTables.tables);
//                $scope.bill={};
//
//                if (fromState.name === 'billing.table.bill') {
//                    $rootScope.$broadcast('event:table:released', fromParams);
//                    //  $state.current.data.tables=angular.copy( dataTables.tables);
//
//                }
//                console.log($state.current.data);
//                $rootScope.$on('event:table:released', function (event, data) {
//                    socket.emit('table:toBeReleased', data);
//                });
//
//                socket.on('table:freed', function (data) {
//                    /*console.log(data);*/
//                    $state.current.data.tables[parseInt(data.tableId) - 1].isLocked = false;
//                });
//
//                $rootScope.$on('event:table:started', function (event, data) {
//                    socket.emit('table:start', data);
//                });
//
//                socket.on('table:started', function (data) {
//                    /*console.log(data);*/
//                    $state.current.data.tables[data.tableNumber - 1].isLocked = true;
//                    /*console.log($state.current.data.tables);*/
//                });
//
//                var _getTabName = _.filter($scope.tabs, {_id:toParams.tabId});
//                $scope.tab = _getTabName[0].tabName;
//                $scope.tabId = toParams.tabId;
//                $scope.tabType = 'table';
//
//                /* Reset Tables */
//                _.forEach($state.current.data.tables, function (table) {
//                    delete table.bill;
//                    table.covers = 0;
//                });
//
//                /* Bind with Unsettled Table Bills */
//                console.log($state.current.data.tables);
//                _.forEach($state.current.data.tables, function (table) {
//                    _.forEach(offlineBills, function (offlineBill) {
//                        if (offlineBill.isSettled === "false") {
//
//                            if (parseInt(offlineBill.bill._tableId) === parseInt(table.id) && offlineBill.bill.tabId === toParams.tabId) {
//                                table.bill = offlineBill.bill;
//                                table.covers = offlineBill.bill._covers;
//                            }
//                        }
//                    });
//                });
//                //console.log($state.current.data);
//                $scope.pagination = Pagination.getNew(8);
//                $scope.pagination.numPages = Math.ceil($state.current.data.tables.length/$scope.pagination.perPage);
//                $scope.pagination.totalTables = $state.current.data.tables.length;
//
//            };
//
//            function bindBillDataToTabBill(tabtype){
//                var _getTabName = _.filter($scope.tabs, {_id:toParams.tabId});
//                $scope.tab = _getTabName[0].tabName;
//                $scope.tabId = toParams.tabId;
//                $scope.tabType = tabtype;
//                GetMasterItems();
//                /* Build Categories Filter */
//                _.forEach($scope.masterItems, function (i) {
//                    $scope.categories.push(i.category);
//                });
//                var _uCats = _.uniq($scope.categories, 'categoryName');
//                $scope.categories = _uCats;
//                renderCategoriesT();
//                var billId=toParams.billId ;
//                var _f = _.filter($scope.bills, {billId:billId});
//                if(_f.length > 0) {
//                    $scope.bill=_f[0].bill;
//                }
//                $scope.handleBill();
//            }
//
//            function renderCategoriesT(){
//                $scope.dataLoadedCat = false;
//                var count=1;
//                var catArr=[];
//                _.forEach($scope.categories,function(_cat,i){
//                    catArr.push(_cat);
//                    if(count%2==0){
//                        $scope.categoriesT.push(catArr);
//                        catArr=[];
//                    }else
//                    {
//                        if(i==$scope.categories.length-1){
//                            $scope.categoriesT.push(catArr);
//                        }
//                    }
//                    count++;
//                });
//                $scope.dataLoadedCat = true;
//            }
//
//            function calculateInclusiveTax(item){
//                var rate=item.rate;
//                var mtax=0;
//                var ttax=0;
//                _.forEach(item.taxes,function(_tax){
//                    rate=item.rate;
//                    if(_tax.type==='percentage')
//                        mtax =(0.01*_tax.value);
//                    if(_.has( _tax,'cascadingTaxes')){
//                        if(_tax.cascadingTaxes.length>0) {
//                            var _ftax = _.filter(item.taxes, {_id: _tax.cascadingTaxes[0]._id});
//                            if (_ftax.length > 0) {
//                                if (_tax.cascadingTaxes[0].type === 'percentage')
//                                    mtax += ( (mtax*0.01 * _tax.cascadingTaxes[0].value));
//                            }
//                        }
//                    }
//                    /*  if(item._id=="54edd2f94321914015bdce33"){
//                     console.log(item.rate-rate);
//                     }*/
//                    ttax+=(mtax);
//                    mtax=0;
//                });
//                return Utils.roundNumber( (item.rate/(1+ttax)),2);
//            }
//
//            function GetMasterItems() {
//                $scope.masterItems = [];
//                $scope.explorerItems = [];
//                _.forEach($scope.items, function (item) {
//                    var itemtab = _.filter(item.tabs, {"_id": $scope.tabId});
//                    if (itemtab.length > 0) {
//                        var tempitem = angular.copy(item);
//                        delete tempitem.tabs;
//                        if (_.has(item, 'tabs')) {
//                            if (_.has(itemtab[0], 'item')) {
//                                tempitem.rate = itemtab[0].item.rate;
//                            }
//                            if (_.has(itemtab[0], 'taxes')) {
//                                tempitem.taxes = itemtab[0].taxes;
//                            }
//
//                        }
//                        if(Utils.hasSetting('inclusive_tax',$scope.settings)){
//                            tempitem.rate=calculateInclusiveTax(tempitem);
//                        }
//                        //console.log(tempitem);
//                        $scope.masterItems.push(tempitem);
//                        $scope.explorerItems.push({_id: tempitem._id, name: tempitem.name, number: tempitem.number, category: tempitem.category});
//                    }
//                });
//                if(Utils.hasSetting( 'orderby_itemnumber',$scope.settings)){
//                    $scope.explorerItems=_.sortBy($scope.explorerItems, ['number']);
//                }
//                else
//                {
//                    $scope.explorerItems=_.sortBy($scope.explorerItems, ['name']);
//                }
//
//                $scope.getExplorerItemsT(null);
//
//                // $scope.explorerItems=_.sortByAll($scope.explorerItems, ['name']);
//                // console.log(_.sortByAll($scope.masterItems, ['name']));
//            };
//            $scope.explorerItemsT=[];
//            //  $scope.dataLoaded = false;
//
//
//            $scope.getExplorerItemsT=function(cat){
//                $scope.dataLoaded = false;
//                // console.log( cat);
//                $scope.explorerItemsT=[];
//                var explorerItems=$scope.explorerItems;
//                var expItems=[];
//                if(cat){
//                    _.forEach($scope.explorerItems,function(_item){
//                        if(_item.category.categoryName==cat.categoryName){
//                            expItems.push(_item);
//                        }
//                    });
//                    explorerItems=expItems;
//                }
//                // console.log(explorerItems);
//                var itemarr=[];
//                var count=1;
//                _.forEach(explorerItems,function(tItem,i){
//                    itemarr.push(tItem);
//                    if(count%5==0 ){
//                        $scope.explorerItemsT.push(itemarr);
//                        itemarr=[];
//                    }else{
//                        if(i==explorerItems.length-1){
//                            if(itemarr.length>0){
//                                $scope.explorerItemsT.push(itemarr);
//                            }
//                        }
//                    }
//                    count++;
//                    // if(count>30){return false;}
//                });
//                // $scope.explorerItemsT=Utils.getItemPages(explorerItems,5)
//                $timeout(function(){
//                    $scope.dataLoaded = true;
//                });
//                console.log( $scope.explorerItemsT);
//            }
//
//            /* Create new Bill for Table */
//            if (toState.name === "billing.table.bill") {
//                var _getTabName = _.filter($scope.tabs, {_id:toParams.tabId});
//                $scope.tab = _getTabName[0].tabName;
//                $scope.tabId = toParams.tabId;
//                $scope.tabType = 'table';
//                GetMasterItems();
//                /* Build Categories Filter */
//                _.forEach($scope.masterItems, function (i) {
//                    $scope.categories.push(i.category);
//                });
//                var _uCats = _.uniq($scope.categories, 'categoryName');
//                $scope.categories = _uCats;
//                $scope.categories=_.sortBy($scope.categories, ['categoryName']);
//                renderCategoriesT();
//                /*console.log($state.current.data.tables[parseInt(toParams.tableId)]);*/
//                var _t = $state.current.data.tables[parseInt(toParams.tableId - 1)];
//
//                if (!_t['bill']) {
//                    $scope.renderRefreshNewBill();
//                    _t.bill = new Bill(toParams.tableId, toParams.covers, null, null, null, currentUser, $rootScope.db, $scope.tab, toParams.tabId, 'table');
//                    $scope.bill = _t.bill;
//
//                } else {
//                    _t.bill._covers = toParams.covers;
//                    $scope.bill = _t.bill;
//                }
//
//
//                $rootScope.$broadcast('event:table:started',
//                    {
//                        tabId:toParams.tabId,
//                        tabType:'table',
//                        tableNumber: parseInt(toParams.tableId),
//                        instanceId: localStorageService.get('instanceId')
//                    }
//                );
//                $scope.handleBill();
//            }
//
//            function renderTakeOutDelivery(tabType){
//                var _getTabName = _.filter($scope.tabs, {_id:toParams.tabId});
//                console.log(_getTabName);
//                $scope.tab = _getTabName[0].tabName;
//                $scope.tabId = toParams.tabId;
//                $scope.tabType = tabType;
//                GetMasterItems();
//
//                /* Build Categories Filter */
//                _.forEach($scope.masterItems, function (i) {
//                    $scope.categories.push(i.category);
//                });
//                var _uCats = _.uniq($scope.categories, 'categoryName');
//                $scope.categories = _uCats;
//                renderCategoriesT();
//                $scope.bill = new Bill(null, null, null, ((tabType==='takeout')?true:null), ((tabType==='delivery')?true:null), currentUser, $rootScope.db,  $scope.tab, toParams.tabId, tabType);
//            }
//            $scope.renderRefreshNewBill=function(cover,bill) {
//                // if ($scope.bill.serialNumber == null && ($scope.tabType!='table') ) {
//                if ($scope.bill.serialNumber == null  ) {
//                    if (Utils.hasSetting('enable_covers', $scope.settings, $scope.tabType)) {
//                        $modal.open({
//                            templateUrl: 'app/billing/item/_cover.html',
//                            controller: ['$scope', '$modalInstance', function ($scope, $modalInstance,Cover) {
//                                var tempCovers='';
//                                if(bill) {
//                                    if(_.has(bill,'_covers'))
//                                        tempCovers = parseInt(bill._covers);
//                                }
//                                $scope.covers= tempCovers>0?tempCovers:'';
//                                $scope.bill=bill;
//                                $scope.cancel = function () {
//                                    $modalInstance.dismiss('')
//                                };
//                                $scope.addCover = function () {
//                                    $scope.cancel = $modalInstance.close({cover: $scope.covers,bill:$scope.bill});
//                                }
//                            }],
//                            size: 'sm'
//                        }).result.then(function (covers) {
//                                if (parseInt(covers.cover)) {
//                                    console.log(covers.bill);
//                                    //  $scope.bill._covers = covers;
//                                    var _parsedCovers = covers.cover;
//                                    if(bill){
//                                        covers.bill._covers=_parsedCovers;
//                                        covers.bill.updateBill({_covers:_parsedCovers}, 'update');
//                                    }else{
//                                        $scope.bill._covers = _parsedCovers;
//                                    }
//                                } else {
//                                    growl.error('covers should be in the integer.', {ttl: 2000})
//                                    console.log(covers);
//                                }
//                            });
//
//                        // console.log($scope.tabType);
//                    }
//                }
//            }
//
//            /* TakeOut State */
//            if (toState.name === 'billing.takeout') {
//                renderTakeOutDelivery('takeout');
//            }
//
//            /* Delivery State */
//            if (toState.name === 'billing.delivery') {
//                renderTakeOutDelivery('delivery');
//                /*
//                 var _getTabName = _.filter($scope.tabs, {_id:toParams.tabId});
//                 $scope.tab = _getTabName[0].tabName;
//                 $scope.tabId = toParams.tabId;
//                 $scope.tabType = 'delivery';
//
//                 GetMasterItems();
//                 *//* Build Categories Filter *//*
//                 _.forEach($scope.masterItems, function (i) {
//                 $scope.categories.push(i.category);
//                 });
//                 var _uCats = _.uniq($scope.categories, 'categoryName');
//                 $scope.categories = _uCats;
//
//                 $scope.bill = new Bill(null, null, null, null, true, currentUser, $rootScope.db,  $scope.tab, toParams.tabId, 'delivery');
//                 */
//            }
//
//            if (toState.name === "billing.delivery.bill") {
//                bindBillDataToTabBill('delivery');
//            }
//
//            if (toState.name === "billing.takeout.bill") {
//                bindBillDataToTabBill('takeout');
//            }
//
//            /* Edit State */
//            if (toState.name === 'billing.edit') {
//                bindBillDataToTabBill('edit');
//                // $modalInstance.close();
//                /* $rootScope.db.select("bills", {
//                 "billId": {
//                 "value": toParams.id
//                 }
//                 }).then(function (result) {
//                 $scope.bill = _.assign(new Bill, JSON.parse(result.rows.item(0).billData));
//                 });*/
//            }
//
//        });
//
//        $scope.billStruc = {
//            items: [],
//            postSubtotalTaxes: []
//        };
//
//        /* Tabs */
//        $scope.tabs = $rootScope.tabs;
//
//        $scope.gotoTablePage = function () {
//            /*var _tNo = $scope.gotoTableNumber;
//             console.log($scope.pagination);*/
//            $scope.pagination.toPageId($scope.gotoTableNumber - 1);
//        };
//
//        $scope.active = function (route) {
//            // return $state.is(route);
//        };
//
//        $scope.isTableFiltered = false;
//        $scope.tempTables = [];
//        $scope.filterUnsettledTableBills = function () {
//            $scope.tempTables = angular.copy($state.current.data.tables);
//            $state.current.data.tables = _.filter($state.current.data.tables, function (table) {
//                return _.has(table, 'bill');
//            });
//            $scope.isTableFiltered = true;
//        };
//        $scope.clearFilterUnsettledTableBills = function () {
//            $scope.isTableFiltered = false;
//            $state.current.data.tables =  $scope.tempTables;
//            $scope.tempTables = [];
//        };
//
//
//        $scope.getWaiterName = function (waiter) {
//            return waiter.firstname + ' ' + waiter.lastname;
//        };
//        $scope.getDelivererName = function (deliverer) {
//            return deliverer.firstname + ' ' + deliverer.lastname;
//        }
//        $scope.setWaiter = function (waiter) {
//            console.log(waiter);
//            $scope.bill._waiter;
//        };
//
//        $scope.subcategories = [];
//        $scope.selectedSuperCategory = {};
//        $scope.clickCategory = function (sc) {
//            $scope.selectedSuperCategory = sc;
//            $scope.categories = sc.child;
//        };
//
//        $scope.showCustomerPanel = function () {
//            return _.isEmpty($scope.selectedCustomer);
//        };
//
//        /*$scope.selectedCustomer = {};*/
//        $scope.billCustomer;
//        $scope.selectedCustomer;
//        $scope.getSelectedCustomer = function (item, model, label) {
//            // console.log(item);
//            // $scope.selectedCustomer=angular.copy(item);
//            var tempCustomer=angular.copy(item);
//            $scope.billCustomer=angular.copy(tempCustomer);
//            delete tempCustomer.addresses;
//            /*$scope.bill._customer = angular.copy(item);*/
//            $scope.bill._customer=tempCustomer;
//        };
//        $scope.customerSearchEnter=function(){
//            //  console.log( $scope.selectedCustomer);
//
//            if( !angular.isUndefined($scope.selectedCustomer)){
//                delete   $scope.bill._customer;
//                if($scope.selectedCustomer.length===10) {
//                    if(parseInt($scope.selectedCustomer))
//                    {
//                        $scope.openCustomerModal('', {mobile: $scope.selectedCustomer,addType:'Residence'});
//                    }
//                    else{
//                        growl.error('Feed in the right mobile/phone number.',{ttl:2000});
//                    }
//                }
//                else{
//                    growl.error('Feed in the right mobile/phone number.',{ttl:2000});
//                }
//            }
//            /*$timeout(function(){
//             console.log($scope.customerForm.mobileSearch.$modelValue);
//             return false;
//             },100)*/
//
//        }
//        $scope.customerModal=function(){
//            $scope.openCustomerModal('', $scope.billCustomer);
//        }
//        $scope.searchItem={name:"",qty:""};
//        $scope.setFocus={focusItem:true,focusInput:false}
//        $scope.getSelectedItem = function () {
//            $scope.addSearchedItemWithQuantity(); $scope.setFocus.focusInput=false;
//            $scope.setFocus.focusInput=false;
//            $scope.setFocus.focusItem=true;
//            $timeout(function(){
//                $scope.setFocus.focusItem=false;
//            },100)
//
//        };
//        $scope.addSearchedItemWithQuantity=function(){
//            var _f_item= _.filter($scope.masterItems,{name: $scope.searchItem.name})
//            if(_f_item.length>0){
//                if(parseFloat($scope.searchItem.qty)) {
//                    $scope.bill.addItemWithQuantity(_f_item[0],parseFloat($scope.searchItem.qty));
//                }else{
//                    growl.error('Quantity should be in decimal only.',{ttl:2000});
//                }
//            }
//            else {
//                growl.error('Item not found with this name.',{ttl:2000});
//            }
//            $scope.searchItem.name="";
//            $scope.searchItem.qty ="";
//        }
//        $scope.GoToNextElement=function(form){
//            //console.log(form);
//            if(parseFloat($scope.searchItem.qty)){
//                $scope.setFocus.focusItem=true;
//                $scope.setFocus.focusInput=false;
//                $scope.addSearchedItemWithQuantity();
//                $timeout(function(){
//                    $scope.setFocus.focusItem=false;
//                },100);
//            }else {
//
//                if($scope.searchItem.qty!='')
//                    growl.error('Item quantity should be in decimal only.',{ttl:2000});
//                $scope.searchItem.qty ="";
//            }
//            $scope.handleBill();
//        }
//
//        $scope.goToQuantity=function(){
//            $scope.setFocus.focusItem=false;
//            $scope.setFocus.focusInput=true;
//            $timeout(function(){
//                $scope.setFocus.focusInput=false;
//            },100)
//        }
//
//        $scope.getSearchItemSelect=function(item, model, label){
//            $scope.goToQuantity();
//        }
//        $scope.getCustomersQuery = function (q) {
//            return Customer.search({q:q}).$promise.then(function (customers) {
//                var _c = [];
//                _.forEach(customers, function (c) {
//                    _c.push(c);
//                });
//                console.log(_c);
//                // $scope.selectedCustomer='';
//                return _c;
//            });
//        };
//
//        $scope.getFullname = function (customer) {
//            if (_.isObject(customer)) {
//                return customer.firstname + '(' + customer.mobile+')';
//            }
//        };
//
//
//        $scope.addItemFromExplorer = function (item) {
//            $scope.bill.addItem(getItemById(item._id));
//            $scope.handleBill();
//        };
//
//        $scope.clearCategoryFilter = function () {
//            $scope.categoryNameToFilter = '';
//        };
//
//        $scope.categoryNameToFilter = '';
//        $scope.filterItemsFromCategory = function (cat) {
//            $scope.categoryNameToFilter = cat.categoryName;
//            $scope.getExplorerItemsT(cat);
//        };
//
//        /* Copy Table Bill */
//        $scope.copiedTableBill = {};
//        $scope.copyTableBill = function (index) {
//            $scope.copiedTableBill = $state.current.data.tables[index-1];
//            $scope.copiedTableBill.tableIndex = index-1;
//            /*console.log("Copy:" + index);
//             console.log($scope.copiedTableBill);*/
//            growl.success("Copied Bill in Table#" + (index ), {ttl:3000});
//        };
//        /* Paste Table Bill */
//        $scope.pasteTableBill = function (index) {
//
//            /*console.log("Paste:" + index);*/
//
//            $state.current.data.tables[index].covers = parseInt($scope.copiedTableBill.covers);
//            $state.current.data.tables[index].bill = angular.copy($scope.copiedTableBill.bill);
//            $state.current.data.tables[index].bill._tableId = index + 1;
//
//            /*console.log($state.current.data.tables[index]);*/
//
//            delete $state.current.data.tables[$scope.copiedTableBill.tableIndex].bill;
//            $state.current.data.tables[$scope.copiedTableBill.tableIndex].covers = 0;
//
//            $state.current.data.tables[index].bill.checkBillExists($state.current.data.tables[index].bill._id).then(function (len) {
//                if (!len) {
//                    $state.current.data.tables[index].bill.updateBill({}, 'insert');
//                } else {
//                    $state.current.data.tables[index].bill.updateBill({}, 'update');
//                }
//
//                $scope.copiedTableBill = {};
//                growl.success("Pasted Bill in Table#" + (index + 1), {ttl:3000});
//            });
//
//        };
//
//        $scope.splitBill=function(billFromTable,index){
//
//            // console.log(billFromTable);
//            $scope.splitBillModal(billFromTable).then(function(splitedBill){
//                //  var bill=$state.current.data.tables[index].bill;
//                // console.log(billFromTable);
//                var mainBill= angular.copy(billFromTable);
//                // console.log(mainBill);
//                var tempBill=[];
//                for(var i=0;i<splitedBill.length;i++) {
//                    tempBill.push( angular.copy(billFromTable));
//                    tempBill[i]._kots = [];
//                    for(var j=0;j<splitedBill[i].length;j++) {
//                        // console.log(mainBill);
//                        var tkotob = getItemWiseKot(mainBill,splitedBill[i][j].name);
//                        mainBill=angular.copy( tkotob.bill);
//                        var kot=tkotob.kotOb;
//                        var flagIsKot=false;
//                        for(var k=0;k<tempBill[i]._kots.length;k++){
//                            if(tempBill[i]._kots[k].kotNumber===kot.kotNumber){
//                                flagIsKot=true;
//                                var flagIsItem=false;
//                                if(_.has(tempBill[i]._kots[k],'items')) {
//                                    for (var l = 0; l < tempBill[i]._kots[k].items.length; l++) {
//                                        if (tempBill[i]._kots[k].items[l].name === kot.items[0].name) {
//                                            tempBill[i]._kots[k].items[l].quantity += 1;
//                                            flagIsItem = true;
//                                            break;
//                                        }
//                                    }
//                                }
//                                if(!flagIsItem){
//                                    tempBill[i]._kots[k].items.push(kot.items[0]);
//                                }
//                            }
//                        }
//                        if(!flagIsKot)
//                            tempBill[i]._kots.push(kot);
//                    }
//
//                    // tempBill.processBill({});
//                }
//                console.log(mainBill);
//                var promiseInsert=[];
//                _.forEach(tempBill,function(bill,i){
//                    bill.processBill({});
//                    bill._id=Utils.guid();
//                    bill.splitNumber=i+1;
//                    promiseInsert.push(bill.updateBill({},'insert'));
//                    //console.log(bill);
//                });
//                $q.all(promiseInsert).then(function(){
//                    $rootScope.db.selectDynamic('delete from bills where billId="'+mainBill._id+'"',[]).then(function(){
//                        growl.success('Split bill bill completed.',{ttl:2000});
//                        refreshState();
//                    })
//                });
//                //console.log(tempBill);
//            });
//        }
//
//        function getItemWiseKot(mBill,itemName){
//            //console.log(mBill);
//            var kot;
//            var kotWithBill={};
//            for(var i=0;i<mBill._kots.length;i++){
//                for(var j=0;j<mBill._kots[i].items.length;j++){
//                    if(mBill._kots[i].items[j].name===itemName){
//                        kot=angular.copy( mBill._kots[i]);
//                        var item=[];
//                        item.push(angular.copy( mBill._kots[i].items[j]));
//                        item[0].quantity=1;
//                        kot.items=item;
//                        if(mBill._kots[i].items[j].quantity===1){
//                            mBill._kots[i].items.splice(j,1);
//                            if(mBill._kots[i].items.length===0){
//                                mBill._kots.splice(i,1);
//                            }
//                            kotWithBill={kotOb:kot,bill:angular.copy( mBill)};
//                            return  kotWithBill;
//                        }else
//                        {
//                            mBill._kots[i].items[j].quantity-=1
//                            kotWithBill={kotOb:kot,bill:angular.copy( mBill)};
//                            return  kotWithBill;
//                        }
//                        kotWithBill={kotOb:kot,bill:mBill};
//                        return  kotWithBill;
//                        break;
//                    }
//                }
//            }
//            return kotWithBill;
//        }
//
//        $scope.splitBillModal=function(billFromTable){
//            var defer=$q.defer();
//
//            var _allItems=[];
//            var Items=[];
//            angular.forEach(billFromTable._kots, function (kot) {
//                if (!kot.isVoid) {
//                    angular.forEach(kot.items, function (item){
//                        _allItems.push(item);
//                    });
//                }
//            });
//            // Aggregate Items
//            _.chain(_allItems).groupBy('_id').map(function (i) {
//                var count=0;
//                for(var j=0;j< i.length;j++) {
//                    for(var k=0;k<i[j].quantity;k++) {
//                        count++;
//                        Items.push({_id: i[0]._id+count, name: i[0].name});
//                    }
//                }
//            });
//
//            $modal.open({
//                templateUrl: 'app/billing/item/_split.html',
//                controller: ['$scope','$rootScope','growl','bill','$modalInstance',function($scope,$rootScope,growl,bill,$modalInstance){
//                    $scope.sItems=[];
//                    $scope.sItems.push(angular.copy( Items));
//                    $scope.split=function(){
//                        $modalInstance.close($scope.sItems);
//                    };
//                    $scope.addSelected=function(index,selectedItems){
//                        $scope.sItems[index].Selected=selectedItems;
//                        for(var i=0;i<$scope.sItems.length;i++){
//                            if(i!=index){
//                                $scope.sItems[i].Selected=[];
//                            }
//                        }
//                    };
//                    $scope.shiftLeft=function(index){
//                        if(!_.has($scope.sItems[index+1],'Selected')){
//                            return false;
//                        }
//                        _.forEach($scope.sItems[index+1].Selected,function(id){
//                            for(var i=0;i<$scope.sItems[index+1].length;i++){
//                                if($scope.sItems[index+1][i]._id===id) {
//                                    $scope.sItems[index].push($scope.sItems[index+1][i]);
//                                    $scope.sItems[index+1].splice(i,1);
//                                    if($scope.sItems[index+1].length==0){
//                                        $scope.sItems.splice(index+1,1);
//                                    }
//                                    break;
//                                }
//                            }
//                        });
//                    };
//
//                    $scope.shiftRightAll=function(index,selected){
//
//                        _.forEach($scope.sItems[index].Selected,function(id){
//                            for(var i=0;i<$scope.sItems[index].length;i++){
//                                if($scope.sItems[index][i]._id===id) {
//                                    if ($scope.sItems.length === index + 1) {
//                                        var titem=[];
//                                        titem.push($scope.sItems[index][i]);
//                                        $scope.sItems[index+1]=titem;
//
//                                    }else {
//                                        $scope.sItems[index+1].push($scope.sItems[index][i]);
//                                    }
//                                    $scope.sItems[index].splice(i,1);
//                                    if($scope.sItems[index].length==0){
//                                        $scope.sItems.splice(index,1);
//                                    }
//                                    break;
//                                }
//                            }
//                        });
//                        // $scope.value.Selected=[];
//
//                        /*var id=selected[0];
//                         var countitem=0;
//                         var itemName='';
//                         for(var i=0;i<$scope.sItems[index].length;i++){
//                         if($scope.sItems[index][i]._id===id){
//                         itemName=$scope.sItems[index][i].name;
//
//                         for(var j=0;j<$scope.sItems[index].length;j++){
//                         if($scope.sItems[index][j].name===itemName){
//                         countitem++;
//                         }
//                         }
//                         break;
//                         }
//                         }
//                         if(countitem>1) {
//                         var flag = confirm('Shift all of this item.');
//                         // BootstrapDialog.confirm('Hi Apple, are you sure?');
//                         }
//
//                         if(flag){
//                         var itemids=[];
//                         for(var i=0;i<$scope.sItems[index].length;i++){
//                         if($scope.sItems[index][i].name===itemName){
//                         if ($scope.sItems.length === index + 1) {
//                         var titem=[];
//                         titem.push($scope.sItems[index][i]);
//                         $scope.sItems.push(titem);
//                         console.log($scope.sItems);
//                         }
//                         else {
//                         var titem=$scope.sItems[index+1];
//                         titem.push($scope.sItems[index][i]);
//                         $scope.sItems[index+1]=titem;
//                         }
//                         //  $scope.sItems[index].splice(i,1);
//                         itemids.push($scope.sItems[index][i]._id);
//                         }
//                         }
//                         for(var i=0;i<itemids.length;i++) {
//                         for (var j = 0; j < $scope.sItems[index].length; j++) {
//                         if ($scope.sItems[index][j]._id === itemids[i]) {
//                         $scope.sItems[index].splice(j,1);
//                         break;
//                         }
//                         }
//                         }
//
//                         return false;
//                         }*/
//                    };
//
//                    $scope.shiftRight=function(index,selected){
//                        if($scope.sItems[index].length===1){
//                            growl.error('Cant shift whole the item',{ttl:2000});
//                            return false;
//                        }
//                        //   _.forEach(selected,function(id){
//                        var id=selected[0];
//                        for(var i=0;i< $scope.sItems[index].length;i++){
//                            if($scope.sItems[index][i]._id===id) {
//                                if ($scope.sItems.length === index + 1) {
//                                    var titem=[];
//                                    titem.push($scope.sItems[index][i]);
//                                    $scope.sItems.push(titem);
//                                    //  console.log($scope.sItems);
//                                }
//                                else {
//                                    var titem=$scope.sItems[index+1];
//                                    titem.push($scope.sItems[index][i]);
//                                    $scope.sItems[index+1]=titem;
//                                }
//                                $scope.sItems[index].splice(i,1);
//                                break;
//                            }
//                        }
//                    };
//                    $scope.cancel=function(){
//                        $modalInstance.close('done');
//                    }
//                }],
//                size: 'lg',
//                resolve: {
//                    itemIndex: function () {
//                        return undefined;
//                    },
//                    taxes: function () {
//                        return $scope.taxes;
//                    },
//                    bill: function () {
//                        return (billFromTable) ? billFromTable : $scope.bill;
//                    }
//                }
//
//            }).result.then(function (splitedBill) {
//                    defer.resolve( splitedBill);
//                });
//            return defer.promise;
//        }
//        /* Open Customer Modal */
//        $scope.openCustomerModal = function (size, value) {
//
//            var modalInstance = $modal.open({
//                templateUrl: 'app/billing/customer/_new.html',
//                controller: 'CustomerCtrl',
//                size: size,
//                resolve: {
//                    number: function () {
//                        return value;
//                    }
//                }
//            });
//
//            modalInstance.result.then(function (newCustomer) {
//                console.log(newCustomer);
//                //newCustomer.addType='Residence';
//                //newCustomer.customerId='9028c6ec-fb9d-18f3-1f3b-3950f00564df1';
//                isCustomerFound(newCustomer).then(function(flag){
//                    console.log(flag);
//                    $scope.newCustomer = newCustomer;
//                    if(!flag) {
//                        $scope.newCustomer.customerId = Utils.guid();
//                        $scope.newCustomer.tenant_id = currentUser.tenant_id;
//                        $scope.newCustomer.deployment_id = currentUser.deployment_id;
//                    }
//                    $rootScope.db.insert('customers',
//                        {
//                            'customerId': $scope.newCustomer.customerId,
//                            'customerData': JSON.stringify($scope.newCustomer)
//                        }
//                    ).then(function (result) {
//                            console.log("Inserted Customer to WebSQL");
//                            var tempcust=angular.copy($scope.newCustomer);
//                            $scope.billCustomer=angular.copy( tempcust);
//                            delete tempcust.addresses;
//                            //$scope.bill._customer = angular.copy($scope.newCustomer);
//                            $scope.bill._customer = tempcust;
//                            $scope.selectedCustomer='';
//                        });
//                });
//                /*Customer.save({tenant_id:currentUser.tenant_id}, $scope.newCustomer, function (customer){
//                 Sync.syncCustomers($rootScope.currentUser).then(function (customers) {
//                 $rootScope.customers = customers;
//                 });
//                 });*/
//            }, function () {
//                console.info('Modal dismissed at: ' + new Date());
//            });
//        };
//        $scope.selectAddresses=function(value) {
//            var addresses=angular.copy(value);
//            var tempCustomer=angular.copy($scope.billCustomer);
//            var defaultAddress={type:tempCustomer.addType,line1:tempCustomer.address1,line2:tempCustomer.address2,city:tempCustomer.city,state:tempCustomer.state,postcode:tempCustomer.postCode};
//            addresses.push(defaultAddress);
//            $scope.openModalAddresses('sm',addresses);
//        }
//        $scope.openModalAddresses=function(size, value){
//            console.log(value);
//
//            var modalinstance=$modal.open({
//                templateUrl:'app/billing/customer/_addresses.html',
//                controller:'customerAddressCtrl',
//                size:'md',
//                resolve:{
//                    customerAddresses:function() {
//                        return value;
//                    }
//                }
//            });
//            modalinstance.result.then(function(selectedAddress){
//                $scope.bill._customer.addType=selectedAddress.type;
//                $scope.bill._customer.address1=selectedAddress.line1;
//                $scope.bill._customer.address2=selectedAddress.line2;
//                $scope.bill._customer.state=selectedAddress.state;
//                $scope.bill._customer.city=selectedAddress.city;
//                $scope.bill._customer.postCode=selectedAddress.postcode;
//            })
//        }
//        $scope.showCustomerMore=function(value){
//            var modalinstance=$modal.open({
//                templateUrl:'app/billing/customer/_more.html',
//                controller:'moreCtrl',
//                size:'md',
//                resolve:{
//                    customer:function() {
//                        return value;
//                    }
//                }
//            });
//        }
//        function  isCustomerFound(newCustomer) {
//            var d=$q.defer();
//            if(!_.has(newCustomer,'customerId')){
//                d.resolve(false);
//            }
//            else {
//                $rootScope.db.selectDynamic("select count(*) as cnt from customers where customerId='" + newCustomer.customerId + "'")
//                    .then(function (custcount) {
//                        if (custcount.rows.item(0).cnt == 0) {
//                            $rootScope.db.selectDynamic("delete from customers where customerId='" + newCustomer.customerId + "'")
//                                .then(function (res) {
//                                    d.resolve(true);
//                                });
//                        }
//                        else {
//                            d.resolve(false);
//                        }
//                    });
//            }
//            return  d.promise;
//        }
//        /* Open Item Quantity Modal */
//        $scope.openItemQuantityModal = function (size, item, index) {
//            //weightT=0.0;
//            if(!Utils.hasSetting('enable_sweet_shop',$scope.settings)){return false;}
//            var modalInstance = $modal.open({
//                templateUrl: 'app/billing/item/_weight.html',
//                controller:['$scope', '$modalInstance','itemIndex','taxes', 'bill', 'Utils','growl',function($scope, $modalInstance,itemIndex,taxes, bill, Utils,growl){
//                    $scope.weightT=0;
//                    $scope.setWeight=function(weight,index){
//                        //console.log($scope.weightT);
//                        $scope.weightT =  ($scope.weightT=='undefined'||$scope.weightT==NaN)?0:$scope.weightT;
//                        $scope.weightT+=parseFloat( weight);
//                        $scope.weightT=Utils.roundNumber($scope.weightT,3);
//                        //bill.getItemSubtotal();
//                    }
//                    $scope.cancel = function () {
//                        $modalInstance.dismiss('cancel');
//                    };
//                    $scope.applyOnBill = function () {
//                        item.quantity=$scope.weightT;
//                        bill.getItemSubtotal();
//                        $modalInstance.dismiss('cancel');
//                    };
//                    $scope.reset=function(){
//                        $scope.weightT=0;
//                    }
//                }],
//                size: size,
//                resolve: {
//                    itemIndex: function () {
//                        return index;
//                    },
//                    taxes: function () {
//                        return $scope.taxes;
//                    },
//                    bill: function () {
//                        return $scope.bill;
//                    }
//                }
//            });
//
//            modalInstance.result.then(function (bill) {
//                $scope.bill = bill;
//
//            }, function () {
//                console.info('Modal dismissed at: ' + new Date());
//            });
//
//        };
//
//        $scope.openItemAmountModal = function (size, item, index) {
//
//            //weightT=0.0;
//            if(!Utils.hasSetting('enable_sweet_shop',$scope.settings)){return false;}
//            var modalInstance = $modal.open({
//                templateUrl: 'app/billing/item/_amount.html',
//                controller:['$scope', '$modalInstance','itemIndex','taxes', 'bill', 'Utils','growl',function($scope, $modalInstance,itemIndex,taxes, bill, Utils,growl){
//                    $scope.amountT="";
//                    $scope.setAmount=function(num,index){
//                        $scope.amountT+=num;
//                        $scope.amountT=Utils.roundNumber($scope.amountT,2);
//                    }
//                    $scope.cancel = function () {
//                        $modalInstance.dismiss('cancel');
//                    };
//                    $scope.applyOnBill = function () {
//                        item.quantity=Utils.roundNumber( ($scope.amountT/item.rate),3);
//                        bill.getItemSubtotal();
//                        $modalInstance.dismiss('cancel');
//                    };
//
//                    $scope.reset=function(){
//                        $scope.amountT="";
//                    }
//
//                    $scope.delete=function(){
//                        $scope.amountT=$scope.amountT.substring(0, $scope.amountT.length-1);
//                    }
//                }],
//                size: size,
//                resolve: {
//                    itemIndex: function () {
//                        return index;
//                    },
//                    taxes: function () {
//                        return $scope.taxes;
//                    },
//                    bill: function () {
//                        return $scope.bill;
//                    }
//                }
//            });
//
//            modalInstance.result.then(function (bill) {
//                $scope.bill = bill;
//
//            }, function () {
//                console.info('Modal dismissed at: ' + new Date());
//            });
//
//        };
//
//
//        /* Open Item Quantity Modal */
//        $scope.openItemRateTaxDiscModal = function (size, item, index, fromKot) {
//
//            /* Authentication Required */
//            if (Utils.hasSetting('enable_passcode_apply_discount', $scope.settings)) {
//
//                $modal.open({
//                    templateUrl: 'app/billing/_passCode.html',
//                    controller: ['$scope', '$modalInstance', 'growl', function ($scope, $modalInstance, growl) {
//                        $scope.passCode = "";
//
//                        $scope.setDigit = function (digit) {
//                            $scope.passCode += digit;
//                        };
//                        $scope.applyPassCode = function () {
//                            if ($scope.passCode != "") {
//                                $modalInstance.close($scope.passCode);
//                            } else {
//                                growl.error("No Passcode entered!", {
//                                    ttl: 2000
//                                });
//                            }
//                        };
//                        $scope.cancelPassCode = function () {
//                            $modalInstance.dismiss('cancel');
//                        };
//
//                        /*  $scope.$watch(function(scope) { return scope.passCode },
//                         function(newValue, oldValue) {
//                         if (newValue.length === 6) {
//                         $scope.applyPassCode();
//                         }
//                         }
//                         );
//                         */
//
//                    }],
//                    size: 'sm',
//                    resolve: {
//                        itemIndex: function () {
//                            return index;
//                        },
//                        taxes: function () {
//                            return $scope.taxes;
//                        },
//                        bill: function () {
//                            return $scope.bill;
//                        }
//                    }
//                }).result.then(function (passCode) {
//
//                        if (passCode === Utils.getPassCode(passCode, $rootScope.users)) {
//                            $modal.open({
//                                templateUrl: 'app/billing/item/_rateTaxDisc.html',
//                                controller: 'ItemquantityCtrl',
//                                size: size,
//                                resolve: {
//                                    itemIndex: function () {
//                                        return index;
//                                    },
//                                    taxes: function () {
//                                        return $scope.taxes;
//                                    },
//                                    bill: function () {
//                                        return $scope.bill;
//                                    }
//                                }
//                            }).result.then(function (bill) {
//                                    $scope.bill = bill;
//                                    $scope.bill.processBill();
//                                });
//                        } else {
//                            growl.error("Wrong Passcode!", {ttl: 2000});
//                            return;
//                        }
//
//                    });
//
//            } else {
//
//                /* No Authentication Required */
//                $modal.open({
//                    templateUrl: 'app/billing/item/_rateTaxDisc.html',
//                    controller: 'ItemquantityCtrl',
//                    size: size,
//                    resolve: {
//                        itemIndex: function () {
//                            return index;
//                        },
//                        taxes: function () {
//                            return $scope.taxes;
//                        },
//                        bill: function () {
//                            return $scope.bill;
//                        }
//                    }
//                }).result.then(function (bill) {
//                        $scope.bill = bill;
//                        $scope.bill.processBill();
//                    });
//
//            }
//
//
//        };
//
//        /* Open Change KOT Item Quantity */
//        $scope.deletedItems={};
//        $scope.openChangeKotQuantity = function (size, kotIndex, kotItemIndex) {
//            if($scope.bill._kots[kotIndex].isVoid){return false;}
//            //alert($scope.bill.isPrinted);
//            if((!Utils.hasSetting('cancel_item_after_print_bill',$scope.settings)) && $scope.bill.isPrinted){
//                growl.error('Printed bill items can not be modified',{ttl:2000});
//                return false;
//            }
//            $modal.open({
//                templateUrl: 'app/billing/item/_changeKotQty.html',
//                size: size,
//                resolve: {
//                    kotIndex: function () {
//                        return kotIndex;
//                    },
//                    kotItemIndex: function () {
//                        return kotItemIndex;
//                    },
//                    bill: function () {
//                        return $scope.bill;
//                    }
//                },
//                controller: ['$scope', 'kotIndex', 'kotItemIndex', 'bill', '$modalInstance', function ($scope, kotIndex, kotItemIndex, bill, $modalInstance) {
//                    $scope.kotIndex = kotIndex;
//                    $scope.kotItemIndex = kotItemIndex;
//                    $scope.errorMessage = "";
//                    $scope.bill = angular.copy(bill);
//                    $scope.reducedQty = 0;
//                    $scope.comment = "";
//                    $scope.changeQty = function () {
//                        if ($scope.comment.length > 0) {
//                            var _i = angular.copy($scope.bill._kots[kotIndex].items[kotItemIndex]);
//                            _i.quantity = ($scope.reducedQty);
//                            _i.comment = $scope.comment;
//                            $scope.deletedItems =_i;
//                            $scope.bill._kots[kotIndex].deleteHistory.push(_i);
//                            var indexOfDeleteItem=$scope.bill._kots[kotIndex].deleteHistory.indexOf(_i);
//                            // alert(indexOfDeleteItem);
//                            $scope.reducedQty = 0;
//                            if( ($scope.bill._kots[kotIndex].items[kotItemIndex].quantity )==0){
//                                $scope.bill._kots[kotIndex].items.splice(kotItemIndex,1);
//                            }
//                            $scope.bill.processBill();
//                            updateInsertDeletedItem(null,kotIndex,indexOfDeleteItem);
//                            $modalInstance.close($scope.bill);
//                        } else {
//                            $scope.errorMessage = "Comment is mandatory!";
//                        }
//                    };
//                    $scope.changeQtyRemoveAll = function () {
//                        if ($scope.comment.length > 0) {
//                            var _i = angular.copy($scope.bill._kots[kotIndex].items[kotItemIndex]);
//                            // _i.quantity = ($scope.reducedQty);
//                            _i.comment = $scope.comment;
//                            $scope.deletedItems =_i;
//                            $scope.bill._kots[kotIndex].deleteHistory.push(_i);
//                            var indexOfDeleteItem=$scope.bill._kots[kotIndex].deleteHistory.indexOf(_i);
//                            // alert(indexOfDeleteItem);
//                            /*  $scope.reducedQty = 0;
//                             if( ($scope.bill._kots[kotIndex].items[kotItemIndex].quantity )==0){*/
//                            $scope.bill._kots[kotIndex].items.splice(kotItemIndex,1);
//                            //}
//                            $scope.bill.processBill();
//                            updateInsertDeletedItem(null,kotIndex,indexOfDeleteItem);
//                            $modalInstance.close($scope.bill);
//                        } else {
//                            $scope.errorMessage = "Comment is mandatory!";
//                        }
//
//                    };
//                    $scope.decrementQty = function () {
//                        // $scope.errorMessage=null;
//                        if (parseFloat($scope.bill._kots[kotIndex].items[kotItemIndex].quantity) != 0) {
//                            $scope.reducedQty += 1;
//                            $scope.bill._kots[kotIndex].items[kotItemIndex].quantity -= 1;
//
//                        }
//                    };
//
//                    $scope.incrementQty = function () {
//                        //  $scope.errorMessage=null;
//                        if (parseFloat($scope.bill._kots[kotIndex].items[kotItemIndex].quantity) <(parseFloat($scope.bill._kots[kotIndex].items[kotItemIndex].quantity)+$scope.reducedQty)) {
//                            $scope.reducedQty -= 1;
//                            $scope.bill._kots[kotIndex].items[kotItemIndex].quantity += 1;
//                        }
//                    };
//                    $scope.cancel = function () {
//                        $modalInstance.dismiss('cancel');
//                    };
//
//                }]
//            }).result.then(function (bill) {
//                    $scope.bill = bill;
//                });
//        };
//        $scope.decrementBillQty=function(itemIndex){
//            console.log(itemIndex);
//            $scope.bill.decrementQuantity(itemIndex);
//            $scope.handleBill();
//        }
//        $scope.showSettlementButton = function () {
//
//            return ( Utils.hasSetting('settle_bill_on_print', $scope.settings))? false: Utils.hasSetting('enable_settlement', $scope.settings,$scope.tabType);
//        };
//
//        /* Settle Bill */
//        $scope.openSettleBill = function (size, billFromTable) {
//            var tbill= (billFromTable)?billFromTable:$scope.bill;
//            var tabtype=(billFromTable)?billFromTable.tabType:$scope.tabType;
//            if (Utils.hasSetting('enable_settlement', $scope.settings,tabtype)) {
//                if(! Utils.hasSetting('settle_bill_on_print',$scope.settings)) {
//                    if(!tbill.isPrinted){
//                        growl.error('Bill need to be printed before settle.',{ttl:2000})
//                        return false;
//                    }
//                }
//
//                $modal.open({
//                    templateUrl: 'app/billing/_settleBill.html',
//                    controller: 'ItemquantityCtrl',
//                    size: size,
//                    resolve: {
//                        itemIndex: function () {
//                            return undefined;
//                        },
//                        taxes: function () {
//                            return $scope.taxes;
//                        },
//                        bill: function () {
//                            return (billFromTable) ? billFromTable : $scope.bill;
//                        }
//                    }
//
//                }).result.then(function (bill) {
//                        if( Utils.hasSetting('settle_bill_on_print',$scope.settings)){
//                            $scope.bill = bill;
//                            $scope.printBill(true);
//                            return false;
//                        }
//                        $scope.bill = bill;
//                        $scope.bill.checkBillExists().then(function (len) {
//                            //$scope.bill.closeTime=new Date();
//                            if (!len) {
//
//                                $scope.bill.updateBill({isSettled: true,_closeTime:new Date()}, 'insert').then(function () {
//                                    var _to = $scope.bill.tab;
//                                    if ($scope.bill.tab === "table") {
//                                        delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                                        $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                                    }
//                                    // $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
//                                    $state.go('billing');
//                                });
//                            } else {
//                                $scope.bill.updateBill({isSettled: true,_closeTime:new Date()}, 'update').then(function () {
//                                    var _to = $scope.bill.tab;
//                                    if ($scope.bill.tab === "table") {
//                                        delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                                        $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                                    }
//                                    //  $state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
//                                    //$state.go($rootScope.tabs[0].route, {tabId: $rootScope.tabs[0]._id});
//                                    $state.go('billing');
//                                });
//                            }
//                        });
//                    });
//            } else {
//                growl.error("You are not authorized to perform this action!", {ttl:3000});
//            }
//        };
//
//        $scope.removeItem = function (index) {
//
//            $scope.bill.removeItem(index);
//            $scope.bill.processBill();
//            $scope.handleBill();
//            /* $modal.open({
//             templateUrl: 'app/billing/item/_comment.html',
//             controller: function ($scope, $modalInstance) {
//             $scope.comment = "";
//             $scope.addComment = function () {
//             $modalInstance.close($scope.comment);
//             };
//             $scope.cancel = function () {
//             $modalInstance.dismiss('cancel');
//             };
//             },
//             size: 'sm'
//             }).result.then(function (comment) {
//             console.info(comment);
//             $scope.bill.removeItem(index);
//             });*/
//        };
//
//        /* Set Delivery */
//        $scope.setDeliveryTime = function (billId) {
//            var _f = _.filter($scope.bills, {billId:billId});
//            if(_f.length > 0) {
//                var _time = _f[0].bill.setDeliveryTime();
//                $scope.openSettleBill('lg', _f[0].bill);
//            }
//        };
//
//        /* Settle Take Out Bill */
//        $scope.settleTakeOut = function (billId) {
//            var _f = _.filter($scope.bills, {billId:billId});
//            if (_f.length > 0) {
//                $scope.openSettleBill('lg',_f[0].bill);
//            }
//        };
//        $scope.openBill = function (billId) {
//            var _f = _.filter($scope.bills, {billId:billId});
//            if(_f.length > 0) {
//
//            }
//        };
//
//        /* Void KOT */
//        $scope.currentKot = {};
//        $scope.currentKotIndex = 0;
//        $scope.voidKot = function (index) {
//            $modal.open({
//                templateUrl: 'app/billing/item/_comment.html',
//                controller: function ($scope, $modalInstance) {
//                    $scope.comment = "";
//                    $scope.addComment = function () {
//                        $modalInstance.close($scope.comment);
//                    };
//                    $scope.cancel = function () {
//                        $modalInstance.dismiss('cancel');
//                    };
//                },
//                size: 'sm'
//            }).result.then(function (comment) {
//                    $scope.bill.voidKot(index, comment);
//                });
//        };
//
//        /* Print KOT Index */
//        $scope.printKotIndex = function (index, isReprint) {
//            $scope.currentKotIndex = index;
//            printer.print('_template_KOT.html', {
//                bill: $scope.bill,
//                index: $scope.currentKotIndex,
//                isReprint: isReprint
//            });
//        };
//
//        /* Add Item Comment */
//        $scope.openItemComment = function (itemIndex) {
//            $modal.open({
//                templateUrl: 'app/billing/item/_comment.html',
//                resolve: {
//                    comment: function () {
//                        return $scope.bill._items[itemIndex].comment;
//                    }
//                },
//                controller: function ($scope, $modalInstance, comment) {
//                    $scope.comment = comment;
//                    $scope.addComment = function () {
//                        $modalInstance.close($scope.comment);
//                    };
//                    $scope.cancel = function () {
//                        $modalInstance.dismiss('cancel');
//                    };
//                },
//                size: 'sm'
//            }).result.then(function (comment) {
//                    $scope.bill._items[itemIndex].comment = comment;
//                });
//        }
//        function sendKotToPrinter(){
//            // if(isnewitems){
//            if((Utils.hasSetting('print_kot_bill', $scope.settings))) {
//                printer.print('_template_KOT.html', {
//                    bill: $scope.bill,
//                    index: $scope.currentKotIndex
//                });
//            }
//            //}
//        }
//        function updateInsertBillPrint(comment,isnewitems){
//            var isreprint=true;
//            if(!Utils.hasSetting('reprint_bill',$scope.settings)){
//                console.log($scope.bill);
//                // return false;
//                isreprint= ($scope.bill.isPrinted)?false:true;
//            }
//            $scope.bill.checkBillExists($scope.bill._id).then(function (len) {
//                if (!len) {
//                    $scope.bill.updateBill({isPrinted: true}, 'insert').then(function () {
//                        if(isnewitems)
//                            sendKotToPrinter();
//                        printer.print('_template_Bill.html', $scope.billToPrint);
//                    }).then(function () {
//                        refreshState();
//                    });
//                } else {
//                    $scope.bill.updateBill({isPrinted: true}, 'update').then(function () {
//                        if(isnewitems)
//                            sendKotToPrinter();
//                        //    console.log($scope.billToPrint);
//                        if(!isreprint){
//                            growl.error("Reprinting bill is not allowed!", {ttl: 2000});
//                            refreshState();
//                            return false;
//                        }
//                        printer.print('_template_Bill.html', $scope.billToPrint);
//
//                    }).then(function () {
//                        refreshState();
//                    });
//                }
//            });
//        }
//        /*Consolidated function to route state after print*/
//        function refreshState(){
//            var _toState = '';
//            switch ($scope.tabType) {
//                case 'table':
//                    _toState = 'billing.table';
//                    break;
//                case 'takeout':
//                    _toState = 'billing.takeout';
//                    break;
//                case 'delivery':
//                    _toState = 'billing.delivery';
//                    break;
//            }
//            if ($scope.bill.tabType === "table") {
//                delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                $state.current.data.tables[parseInt($scope.bill._tableId) - 1]['covers'] = null;
//            }
//            //  if (_toState === 'billing.takeout' || _toState === 'billing.delivery') {
//            $state.transitionTo('billing');
//            //} else {
//            //    $state.transitionTo(_toState, {tabId: $scope.tabId});
//            //}
//        }
//
//        $scope.getOfflineBills=function(){
//            Sync.syncOfflineBills($scope.tabId).then(function(bills){
//                $scope.bills=bills;
//            });
//        }
//        /*Consolidated function with and without comment*/
//        function updateInsertBill(comment){
//            //  console.log($scope.bill);
//            $scope.currentKotIndex = $scope.bill.printKot(comment);
//            $scope.bill.checkBillExists($scope.bill._id).then(function (len) {
//                if (!len) {
//                    $scope.bill.updateBill(null, 'insert').then(function () {
//
//                        printer.print('_template_KOT.html', {
//                            bill: $scope.bill,
//                            index: $scope.currentKotIndex
//                        });
//                        $scope.handleBill();
//                        /* TODO: Send SMS if Delivery Tab */
//                    });
//                } else {
//                    $scope.bill.updateBill(null, 'update').then(function () {
//                        printer.print('_template_KOT.html', {
//                            bill: $scope.bill,
//                            index: $scope.currentKotIndex
//                        });
//                        $scope.handleBill();
//                        /* TODO: Send SMS if Delivery Tab */
//
//                    });
//                }
//            });
//
//        }
//
//        function updateInsertDeletedItem(comment,index,indexOfDeletedItem){
//            /*   $scope.currentKotIndex = $scope.bill.printKot(comment);*/
//            $scope.bill.isPrinted=false;
//            $scope.bill.checkBillExists($scope.bill._id).then(function (len) {
//                if (!len) {
//                    $scope.bill.updateBill(null, 'insert').then(function () {
//
//                        printer.print('_template_KOT_Deleted.html', {
//                            bill: $scope.bill,
//                            index:index,
//                            dIndex:indexOfDeletedItem
//                        });
//                        $scope.handleBill();
//                        /* TODO: Send SMS if Delivery Tab */
//                    });
//                } else {
//                    $scope.bill.updateBill({isPrinted:false}, 'update').then(function () {
//
//                        printer.print('_template_KOT_Deleted.html', {
//                            /* bill: $scope.bill,
//                             index: $scope.currentKotIndex*/
//                            bill: $scope.bill,
//                            index:index,
//                            dIndex:indexOfDeletedItem
//                        });
//                        $scope.handleBill();
//                        /* TODO: Send SMS if Delivery Tab */
//
//                    });
//                }
//            });
//        }
//        /*Billing process handle*/
//        $scope.billStatus='';
//        $scope.doBilling=function(){
//            if($scope.billStatus=='PrintKot'){
//                $scope.bill.isPrinted=false;
//                $scope.printKot();
//            }
//            if($scope.billStatus=='PrintBill'){
//                $scope.printBill();
//            }
//            if($scope.billStatus=='Settle'){
//                $scope.openSettleBill('lg')
//            }
//        };
//        $scope.handleBill=function(){
//            var showKot=  (Utils.hasSetting('dis_kot', $scope.settings))?false:(Utils.hasSetting('print_kot_bill', $scope.settings))?false:true;
//            console.log(showKot);
//            if(($scope.bill._items.length>0)&&(showKot)){
//                $scope.billStatus='PrintKot';
//            }else {
//                if( !$scope.bill.isPrinted){
//                    if(($scope.bill._kots.length>0)||($scope.bill._items.length>0))
//                    {$scope.billStatus='PrintBill';}
//                    else
//                    {$scope.billStatus='';}
//                }else {
//                    $scope.billStatus='Settle';
//                }
//            }
//        };
//        /* Print KOT */
//        $scope.printKot = function () {
//
//            if ($scope.bill._items.length > 0) {
//                $scope.bill.generateKotNumber().then(function(kotnumber){
//                    // alert(kotnumber);
//                    $scope.bill.kotNumber=kotnumber;
//                    //  alert(Utils.hasSetting('enable_comment_kot', $scope.settings));
//                    if (Utils.hasSetting('enable_comment_kot', $scope.settings)) {
//
//                        /* Show Comment Modal */
//                        $modal.open({
//                            templateUrl: 'app/billing/item/_comment.html',
//                            controller: function ($scope, $modalInstance) {
//                                $scope.comment = "";
//                                $scope.addComment = function () {
//                                    $modalInstance.close($scope.comment);
//                                };
//                                $scope.cancel = function () {
//                                    $modalInstance.dismiss('cancel');
//                                };
//                            },
//                            size: 'sm'
//                        }).result.then(function (comment) {
//                                updateInsertBill(comment);
//                            });
//                    } else {
//
//                        /* Don't show Comment Modal */
//                        updateInsertBill(null);
//                    }
//                });
//
//
//            } else {
//                growl.error("Please add at least one Item to generate KOT!", {ttl:3000});
//            }
//        };
//
//        /* Print Template */
//        $scope.printDiv = function (divName) {
//
//            var printContents = document.getElementById(divName).innerHTML;
//            var originalContents = document.body.innerHTML;
//
//            if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
//                var popupWin = window.open('Print', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
//                popupWin.window.focus();
//                popupWin.document.write('<!DOCTYPE html><html><head>' +
//                '<link rel="stylesheet" type="text/css" href="assets/print.css" />' +
//                '</head><body onload="window.print()" onfocus="windows.close()"><div class="reward-body">' + printContents + '</div></html>');
//
//                /*popupWin.window.onbeforeunload = function (event) {
//                 popupWin.close();
//                 return '.\n';
//                 };*/
//                popupWin.window.onabort = function (event) {
//                    popupWin.document.close();
//                    popupWin.close();
//                };
//            } else {
//                var popupWin = window.open('Print', '_blank', 'width=800,height=600');
//                popupWin.document.open();
//                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
//                popupWin.document.close();
//            }
//            popupWin.document.close();
//            return true;
//        };
//
//        $scope.passCodeModal=function(){
//            var d=$q.defer();
//            $modal.open({
//                templateUrl: 'app/billing/_passCode.html',
//                controller: ['$scope', '$modalInstance', 'growl', function ($scope, $modalInstance, growl) {
//                    $scope.passCode = "";
//                    $scope.setDigit = function (digit) {
//                        $scope.passCode += digit;
//                        if($scope.passCode.length==6){
//                            $scope.applyPassCode();
//                        }
//                    };
//                    $scope.applyPassCode = function () {
//                        d.resolve($scope.passCode);
//                        $modalInstance.close($scope.passCode);
//                    };
//                    $scope.cancelPassCode = function () {
//                        d.resolve('cancel');
//                        $modalInstance.close('cancel');
//                    };
//                }],
//                size: 'sm'
//            })
//            return d.promise;
//        };
//
//        $scope.commentModal=function(){
//            var d=$q.defer();
//            $modal.open({
//                templateUrl: 'app/billing/item/_comment.html',
//                controller: function ($scope, $modalInstance) {
//                    $scope.comment = "";
//                    $scope.addComment = function () {
//                        d.resolve($scope.comment);
//                        $modalInstance.close($scope.comment);
//                    };
//                    $scope.cancel = function () {
//                        d.resolve('cancel');
//                        $modalInstance.dismiss('cancel');
//                    };
//                },
//                size: 'sm'
//            });
//            return d.promise;
//        }
//
//        $scope.processVoidBill=function(bill){
//
//            $scope.bill=bill?bill:$scope.bill;
//            $scope.bill._closeTime=new Date();
//            $scope.bill.checkBillExists($scope.bill._id).then(function (len) {
//                if (!len) {
//                    $scope.bill.updateBill({isVoid: true}, 'insert').then(function () {
//                        var _to = $scope.bill.tab;
//                        if ($scope.bill.tab === "table") {
//                            delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                            $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                        }
//                        // $state.transitionTo('billing.table');
//                        $state.transitionTo('billing');
//                    });
//                } else {
//                    $scope.bill.updateBill({isVoid: true}, 'update').then(function () {
//                        var _to = $scope.bill.tab;
//                        if ($scope.bill.tab === "table") {
//                            delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                            $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                        }
//                        //  $state.transitionTo('billing.table');
//                        $state.transitionTo('billing');
//                    });
//                }
//            });
//        }
//        /* Void Bill */
//        $scope.voidBill = function (bill) {
//
//            if (Utils.hasSetting('enable_passcode_void_bill', $scope.settings)) {
//                $scope.passCodeModal().then(function (res) {
//                    if (res != Utils.getPassCode(res, $rootScope.users)) {
//                        growl.error('Wrong Passcode!', {ttl: 2000});
//                        return false;
//                    }else
//                    {
//                        if (Utils.hasSetting('enable_comment_void_bill', $scope.settings)) {
//                            $scope.commentModal().then(function(comment){
//                                if(comment==='cancel'||comment.length===0){
//                                    growl.error('Please Enter Valid Comment', {ttl: 2000});
//                                    return false;
//                                }else
//                                {
//                                    $scope.bill.voidComment=comment;
//                                    $scope.processVoidBill(bill);
//                                }
//                            });
//                        }
//                        else
//                        {
//                            $scope.processVoidBill(bill);
//                        }
//                    }
//                });
//            }else
//            {
//                if (Utils.hasSetting('enable_comment_void_bill', $scope.settings)) {
//                    $scope.commentModal().then(function(comment){
//                        if(comment==='cancel'||comment.length===0){
//                            growl.error('Please Enter Valid Comment', {ttl: 2000});
//                            return false;
//                        }
//                        else
//                        {
//                            $scope.bill.voidComment=comment;
//                            $scope.processVoidBill(bill);
//                        }
//                    });
//                }
//                else
//                {
//                    $scope.processVoidBill(bill);
//                }
//            }
//            return false;
//
//            if (Utils.hasSetting('enable_passcode_void_bill', $scope.settings)) {
//
//                /* Authentication Required */
//                $modal.open({
//                    templateUrl: 'app/billing/_passCode.html',
//                    controller: ['$scope', '$modalInstance', 'growl', 'bill', function ($scope, $modalInstance, growl, bill) {
//                        $scope.passCode = "";
//                        $scope.setDigit = function (digit) {
//                            $scope.passCode += digit;
//                            if($scope.passCode.length==6){
//                                $scope.applyPassCode();
//                            }
//
//                        };
//                        $scope.applyPassCode = function () {
//                            if ($scope.passCode != "") {
//                                console.log(bill);
//                                $modalInstance.close({passCode:$scope.passCode,bill:bill});
//                            } else {
//                                growl.error("No Passcode entered!", {
//                                    ttl: 2000
//                                });
//                            }
//                        };
//                        $scope.cancelPassCode = function () {
//                            $modalInstance.dismiss('cancel');
//                        };
//                    }],
//                    size: 'sm',
//                    resolve: {
//                        itemIndex: function () {
//                            return undefined;
//                        },
//                        taxes: function () {
//                            return $scope.taxes;
//                        },
//                        bill: function () {
//                            return (bill) ? bill : $scope.bill;
//                        }
//                    }
//                }).result.then(function (res) {
//
//                        if (res.passCode === Utils.getPassCode(res.passCode, $rootScope.users)) {
//                            $scope.bill = res.bill;
//                            $modal.open({
//                                templateUrl: 'app/billing/item/_comment.html',
//                                controller: function ($scope, $modalInstance) {
//                                    $scope.comment = "";
//                                    $scope.addComment = function () {
//                                        $modalInstance.close($scope.comment);
//                                    };
//                                    $scope.cancel = function () {
//                                        $modalInstance.dismiss('cancel');
//                                    };
//                                },
//                                size: 'sm'
//                            }).result.then(function (comment) {
//
//                                    $scope.bill.checkBillExists($scope.bill._id).then(function (len) {
//
//                                        if (!len) {
//                                            $scope.bill.updateBill({isVoid: true, voidComment:comment}, 'insert').then(function () {
//                                                var _to = $scope.bill.tab;
//                                                if ($scope.bill.tab === "table") {
//                                                    delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                                                    $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                                                }
//                                                //  $state.transitionTo('billing.table');
//                                                $state.transitionTo('billing');
//                                            });
//                                        } else {
//                                            $scope.bill.updateBill({isVoid: true, voidComment:comment}, 'update').then(function () {
//                                                var _to = $scope.bill.tab;
//                                                if ($scope.bill.tab === "table") {
//                                                    delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                                                    $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                                                }
//                                                //  $state.transitionTo('billing.table');
//                                                $state.transitionTo('billing');
//                                            });
//                                        }
//                                    });
//
//                                });
//
//                        } else {
//                            growl.error('Wrong Passcode!', {ttl: 2000});
//                            return;
//                        }
//
//                    });
//
//            } else {
//
//                $scope.bill.checkBillExists($scope.bill._id).then(function (len) {
//
//                    if (!len) {
//                        $scope.bill.updateBill({isVoid: true}, 'insert').then(function () {
//                            var _to = $scope.bill.tab;
//                            if ($scope.bill.tab === "table") {
//                                delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                                $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                            }
//                            // $state.transitionTo('billing.table');
//                            $state.transitionTo('billing');
//                        });
//                    } else {
//                        $scope.bill.updateBill({isVoid: true}, 'update').then(function () {
//                            var _to = $scope.bill.tab;
//                            if ($scope.bill.tab === "table") {
//                                delete $state.current.data.tables[parseInt($scope.bill._tableId)]['bill'];
//                                $state.current.data.tables[parseInt($scope.bill._tableId)]['covers'] = null;
//                            }
//                            //  $state.transitionTo('billing.table');
//                            $state.transitionTo('billing');
//                        });
//                    }
//                });
//
//            }
//        };
//
//        /* Print Bill */
//        $scope.billToPrint = {};
//        $scope.printBill = function (isSettled) {
//            // console.log($scope.bill);
//            // return false;
//            //  alert(Utils.hasSetting('dis_kot', $scope.settings));
//            var issettle= Utils.hasSetting('enable_settlement', $scope.settings,$scope.tabType);
//            var isSettleOnPrint=Utils.hasSetting('settle_bill_on_print', $scope.settings);
//            if(!isSettled){
//                if(isSettleOnPrint && issettle){
//                    $scope.openSettleBill('lg');
//                    return false;
//                }
//            }else{
//                $scope.bill.isSettled= true,$scope.bill._closeTime=new Date();
//            }
//            if(!issettle){$scope.bill._closeTime=new Date();}
//            var comment = "";
//            $scope.bill.generateKotNumber().then(function(kotnumber) {
//                $scope.bill.kotNumber = kotnumber;
//                if ($scope.bill._items.length === 0) {
//                    // Prepare KOT Aggregation
//                    $scope.bill.prepareAggregation($scope.settings,$scope.tabType).then(function (preparedBill) {
//                        $scope.billToPrint = preparedBill;
//                        updateInsertBillPrint(comment,false);
//                    }, function (errors) {
//                        _.forEach(errors, function (e) {
//                            growl.error(e, {ttl: 3000});
//                        });
//                    });
//                } else {
//                    if((Utils.hasSetting('kot_mandatory', $scope.settings))){alert("Kot is mandatory ,please print kot before printing bill."); return false;}
//                    $scope.bill.prepareAggregation($scope.settings,$scope.tabType).then(function (preparedBill) {
//                        $scope.currentKotIndex = $scope.bill.printKot(comment);
//                        $scope.billToPrint = preparedBill;
//                        updateInsertBillPrint(comment,true);
//
//                    }, function (errors) {
//                        _.forEach(errors, function (e) {
//                            growl.error(e, {ttl: 3000});
//                        });
//                    });
//                }
//            });
//        };
//
//        /* Show Edit Bill */
//        $scope.showEditBill = function () {
//            $modal.open({
//                templateUrl: 'app/billing/_editBill.html',
//                controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'offlineBills', '$state', 'settings', 'Utils', 'Bills', 'Bill', function ($rootScope, $scope, $resource, $modalInstance, offlineBills, $state, settings, Utils, Bills, Bill) {
//
//                    $scope.bills = offlineBills;
//
//                    $scope.resource = $resource('/api/bills/:id',
//                        {
//                            id: '@_id'
//                        },
//                        {
//                            saveData: {method: 'POST', isArray:true},
//                            get: {method: 'GET', isArray: true},
//                            findOne: {method: 'GET'},
//                            update:{method:'PUT'}
//                        }
//                    );
//
//                    /* Sync Offline Bills */
//                    $scope.syncOfflineBills = function () {
//                        /*console.log($scope.bills);*/
//                        _.forEach($scope.bills, function (bill) {
//
//                            /*alert("Delete offline bills after syncing?");*/
//                            $scope.resource.save({}, bill.bill, function (bill) {
//                                console.log(bill);
//
//                                $rootScope.db.update('bills',{
//                                    "isSynced" : "true",
//                                    "syncedOn" : new Date().toISOString()
//                                },{
//                                    "billId": bill.ng_id
//                                }).then(function () {
//                                    growl.success("Synced Bill!", {ttl:3000});
//                                });
//                            }, function (err) {
//                                growl.error("Couldn't Sync Bill", {ttl:3000});
//                            });
//
//                        });
//                    };
//
//                    $scope.gotoEditBill = function (bill) {
//                        $modalInstance.close(bill);
//                    };
//                    $scope.cancel = function () {
//                        $modalInstance.dismiss('cancel');
//                    };
//                    $scope.getUnsettled = function () {
//                        var _uAmount = 0;
//                        _.forEach($scope.bills, function (bill) {
//                            if (!bill.isSettled || bill.isSettled === 'false') {
//                                _uAmount += bill.totalBill;
//                            }
//                        });
//                        return (_uAmount).toFixed(2);
//                    };
//                    $scope.getDaySale = function () {
//                        var _daySale = 0;
//                        angular.forEach($scope.bills, function (bill) {
//                            _daySale += bill.totalBill;
//                        });
//                        return _daySale;
//                    };
//                    $scope.getTotalTax = function () {
//                        var _tT = 0;
//                        _.forEach($scope.bills, function (bill) {
//                            _tT += bill.totalTax;
//                        });
//                        return (_tT).toFixed(2);
//                    };
//
//                }],
//                size: 'lg',
//                resolve: {
//                    offlineBills: ['$rootScope', 'Bill', 'Bills', function ($rootScope, Bill, Bills) {
//                        return $rootScope.db.selectAll("bills").then(function (results) {
//                            var _rows = [];
//                            var offlineBills = [];
//                            for (var i = 0; i < results.rows.length; i++) {
//                                _rows.push(results.rows.item(i));
//                            }
//
//                            _.forEach(_rows, function (ob) {
//                                var _ob = {
//                                    id: ob.id,
//                                    billId: ob.billId,
//                                    bill: _.assign(new Bill, JSON.parse(ob.billData)),
//                                    totalBill: parseFloat(ob.totalBill),
//                                    totalTax: parseFloat(ob.totalTax),
//                                    isSynced: ob.isSynced,
//                                    syncedOn: ob.syncedOn,
//                                    customer: ob.customer,
//                                    customer_address: ob.customer_address,
//                                    isVoid: ob.isVoid,
//                                    isSettled: ob.isSettled,
//                                    isPrinted: ob.isPrinted,
//                                    created: ob.created
//                                };
//                                //  console.log(_ob.isPrinted);
//                                offlineBills.push(_ob);
//                                Bills.addBill(_ob);
//                            });
//
//                            return offlineBills;
//                        });
//                    }],
//                    settings: function () {
//                        return $scope.settings;
//                    }
//                }
//            }).result.then(function (bill) {
//                    $state.go('billing.edit', {id: bill.billId});
//                });
//        };
//
//
//        /* $scope.isMinimumOrderDelivery = function () {
//
//         if ($scope.bill.tab === "delivery" && Utils.hasSetting('minimum_order_delivery', $scope.settings)) {
//         if (parseFloat($scope.bill.getSubtotal()) <= parseFloat(Utils.getSettingValue('minimum_order_delivery', $scope.settings))) {
//         return false;
//         } else {
//         return true;
//         }
//         }
//
//         };
//         */
//
//        $scope.slickHandle = {
//        };
//        $scope.setCustomerDeliverTo = function (address) {
//            $scope.bill.setCustomerDeliverTo(address);
//        };
//        $scope.updateBill = function () {
//            $scope.bill.updateBill(null, 'update').then(function () {
//                $state.go('^');
//            });
//        };
//
//        $scope.menuOptionsBill = [
//            ['Copy', function ($itemScope) {$scope.copyTableBill($itemScope.t.id);}],
//            ['Settle Bill', function ($itemScope) {$scope.openSettleBill('lg', $itemScope.t.bill); }],
//            ['Void Bill', function ($itemScope) {$scope.voidBill($itemScope.t.bill) }],
//            ['Split Bill', function ($itemScope) {$scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)}]
//        ];
//
//        $scope.menuOptionsWOBill = [
//            ['Paste', function ($itemScope) {
//                // $scope.player.gold += $itemScope.item.cost;
//                $scope.pasteTableBill($itemScope.t.id-1);
//            }]
//        ];
//        $scope.menuOptions=function(bill){
//            if(bill){
//                var allValidFeature=[];
//                var copy= ['Copy', function ($itemScope) {$scope.copyTableBill($itemScope.t.id);}];
//                var settle= ['Settle Bill', function ($itemScope) {$scope.openSettleBill('lg', $itemScope.t.bill); }];
//                var voidbill=    ['Void Bill', function ($itemScope) {$scope.voidBill($itemScope.t.bill) }];
//                var split=   ['Split Bill', function ($itemScope) {$scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)}];
//                if (Utils.hasSetting('enable_covers', $scope.settings, $scope.tabType)) {
//                    var cover=   ['Cover', function ($itemScope) {
//                        // $scope.splitBill($itemScope.t.bill,$itemScope.t.id-1)
//                        $scope.renderRefreshNewBill(22,$itemScope.t.bill);
//                    }];
//                    allValidFeature.push(cover);
//                }
//
//                allValidFeature.push(copy);
//                if(bill.isPrinted)  allValidFeature.push(settle);
//                allValidFeature.push(voidbill);
//                allValidFeature.push(split);
//                return allValidFeature;
//            }else
//            {
//                return $scope.menuOptionsWOBill;
//            }
//        }
//    }]);
