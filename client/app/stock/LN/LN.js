'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('LN', {
        url: '/demand',
        templateUrl: 'app/stock/LN/LN.html',
        controller: 'LNCtrl'
      });
  });