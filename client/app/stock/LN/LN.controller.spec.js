'use strict';

describe('Controller: LNCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var LNCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LNCtrl = $controller('LNCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
