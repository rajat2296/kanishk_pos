'use strict';

angular.module('posistApp')
  .controller('LNCtrl', ['$scope', 'LastDemandBillNumber', 'store', 'localStorageService', '$q', 'growl', 'vendor', 'demand', 'currentUser', 'stockUnit', 'property', 'currentDeployment', function ($scope, LastDemandBillNumber, store, localStorageService, $q, growl, vendor, demand, currentUser, stockUnit, property, currentDeployment) {

    $scope.lnStores = [];
    var vendors = [];
    $scope.lnVendors = [];
    var units = [];
    $scope.setting = {};
    $scope.currentUser = currentUser;
    $scope.currentDeployment = currentDeployment;

    $scope.showPricing = true;

    function fetchLNStores() {
      var deferred = $q.defer();
      store.get({
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id'),
        isLNStore: true
      }, function (sts) {
        deferred.resolve(sts);
      }, function (err) {
        console.log(err);
        deferred.reject(err);
      });
      return deferred.promise;
    }

    function fetchLNVendors() {
      var deferred = $q.defer();
      vendor.get({
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id'),
        isERPLN: true
      }, function (vendors) {
        deferred.resolve(vendors);
      }, function (err) {
        console.log(err);
        deferred.reject(err);
      });
      return deferred.promise;
    }

    stockUnit.get({
      deployment_id: localStorageService.get('deployment_id'),
      tenant_id: localStorageService.get('tenant_id')
    }, function (uts) {
      units = uts;
    }, function (err) {
      console.log(err);
    });

    property.get({
      deployment_id: localStorageService.get('deployment_id'),
      tenant_id: localStorageService.get('tenant_id')
    }, function (props) {
      $scope.setting = props[0] ? props[0] : {};
    }, function (err) {
      console.log(err);
    });

    function print_Entry(table1) {
      //console.log("TABLE", table1);
      var printer = window.open('', '', 'width=600,height=600');
      //console.log("PRINTER", printer);
      printer.document.open("text/html");
      printer.document.write(table1);
      printer.document.close();
      printer.focus();
      printer.print();
    };

    $scope.onStoreChange = function (store) {
      $scope.lnVendors = _.filter(vendors, function (vendor) {
        console.log(store._id, vendor.pricing._id);
        if (vendor.pricing) {
          return vendor.pricing._id == store._id;
        }
        return false;
      });

    };

    $scope.onVendorChange = function (vendor) {
      $scope.lnDemandForm.items = [];
      $scope.availableItems = [];
      var category = _.find(vendor.pricing.category, function (cat) {
        return cat.isERPLNCategory = true
      });
      console.log(category);
      $scope.lnDemandForm.items = _.filter(category.item, function (item) {
        item.category = {
          _id: category._id,
          categoryName: category.categoryName
        }
        item.selectedUnit = _.find(item.units, function (unit) {
          return item.selectedUnitId._id == unit._id;
        });
        item.preUnit = angular.copy(item.selectedUnit);
        return _.has(item, 'code');
      })
      if ($scope.lnDemandForm.items.length == 0)
        growl.error("No Items available from LN in this store!", {ttl: 3000});
    }

    $scope.adjustPriceWithUnit = function (item) {
      //console.log(units);
      console.log(item.selectedUnit);
      console.log(item.preUnit);
      var ut = _.find(units, function (ut) {
        return item.preUnit._id == ut._id;
      });

      if (ut) {
        item.price = item.price * ut.baseConversionFactor / item.selectedUnit.conversionFactor;
        item.preUnit = angular.copy(item.selectedUnit);
      } else {
        item.price = item.price * 1 / item.selectedUnit.conversionFactor;
        item.preUnit = angular.copy(item.selectedUnit);
      }
    }

    $scope.removeItem = function (item, index) {
      $scope.availableItems.push(item);
      $scope.lnDemandForm.items.splice(index, 1);
      //console.log($scope.availableItems);
      //console.log($scope.lnDemandForm.items);
    }

    $scope.addToDemandItems = function (item) {
      $scope.lnDemandForm.items.push(item);
      var index = _.findIndex($scope.availableItems, function (it) {
        return item._id == it._id;
      });
      $scope.availableItems.splice(index, 1);
      $scope.lnDemandForm.tempItem = "";
    }

    var getValidRows = function (items) {
      var validItems = [];
      _.forEach(items, function (item) {
        if (item.qty && item.qty != 0 && !isNaN(item.qty) && item.price && item.price != 0 && !isNaN(item.price) && item.selectedUnit)
          validItems.push(item);
      });
      return validItems;
    }

    var allP = $q.all([
      fetchLNStores(),
      fetchLNVendors()
    ]).then(function (values) {
      $scope.lnStores = values[0];
      vendors = values[1];
      console.log(vendors);
    }).catch(function (err) {
      console.log(err);
      growl.error("Some error occured", {ttl: 3000});
    });

    //--------------------------------------------------Generate Demand------------------------------------------------
    $scope.lnDemandForm = {};
    $scope.lnDemandForm.billDate = new Date();
    $scope.lnDemandForm.maxDate = new Date();
    $scope.lnDemandForm.billNumber = 'LN-' + LastDemandBillNumber.demandBillNumber;

    $scope.resetLNDemandForm = function () {
      $scope.lnVendors = [];
      $scope.lnDemandForm = {};
      $scope.lnDemandForm.billNumber = 'LN-' + LastDemandBillNumber.demandBillNumber;
      $scope.lnDemandForm.billDate = new Date();
      $scope.lnDemandForm.maxDate = new Date();
      $scope.lnDemandForm.store = {};
      $scope.lnDemandForm.vendor = {};
      $scope.lnDemandForm.items = [];
      $scope.lnDemandForm.availableItems = [];
      $scope.allowSubmit = false;
    }

    var transformRequest = function(obj) {
      var str = [];
      for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      return str.join("&");
    }

    $scope.submitDemand = function (form) {

      if (form.$valid) {
        var validRows = getValidRows($scope.lnDemandForm.items);
        $scope.allowSubmit = false;
        if (validRows.length == 0) {
          growl.error("Please provide sufficient information to proceed!", {ttl: 3000});
        }
        else if (validRows.length < $scope.lnDemandForm.items.length) {
          if (confirm("Some items will be excluded. Are you sure you want to submit"))
            $scope.allowSubmit = true;
        } else $scope.allowSubmit = true;
        if ($scope.allowSubmit) {
          $scope.lnDemandForm.items = validRows;
          console.log($scope.lnDemandForm);
          $scope.lnDemandForm.demandBillNumber = LastDemandBillNumber.demandBillNumber;
          $scope.lnDemandForm.demandNumber = LastDemandBillNumber.demandBillNumber;
          $scope.lnDemandForm.daySerialNumber = LastDemandBillNumber.daySerialNumber;
          $scope.lnDemandForm.demandNumber = LastDemandBillNumber.demandBillNumber;
          $scope.lnDemandForm.deployment_id = localStorageService.get('deployment_id');
          $scope.lnDemandForm.tenant_id = localStorageService.get('tenant_id');
          $scope.lnDemandForm.created = new Date();
          $scope.lnDemandForm.user = currentUser;
          var xml = "<DocumentElement>";
          _.forEach($scope.lnDemandForm.items, function (item) {
            xml += "<Demand>";
            xml +=    "<LnItemCode>" + item.code + "</LnItemCode>";
            xml +=    "<UOM>" + item.selectedUnit.unitName + "</UOM>";
            xml +=    "<DemandQty>" + item.qty + "</DemandQty>";
            xml +=  "</Demand>";
          });
          xml += "</DocumentElement>";
          var lnRequest = {
            DemandNo: $scope.lnDemandForm.demandNumber,
            DemandFrom: $scope.lnDemandForm.store.store_id,
            DemandDate: moment(new Date($scope.lnDemandForm.created)).format('D-MMM-YYYY'),
            DemandLineXML: xml,
            APIKey: "123$$4tg!dt&_tb#uhy"
          }
          console.log('xml Request', lnRequest);
          var vendor = {
            _id: $scope.lnDemandForm.vendor._id,
            type: $scope.lnDemandForm.vendor.type,
            vendorName: $scope.lnDemandForm.vendor.vendorName,
            isERPLN: $scope.lnDemandForm.vendor.isERPLN
          }
          $scope.lnDemandForm.vendor = vendor;
          $scope.lnDemandForm.xmlRequest = transformRequest(lnRequest);
          console.log('encoded request', $scope.lnDemandForm.xmlRequest);
          console.log($scope.lnDemandForm.xmlRequest);
          console.log(moment(new Date($scope.lnDemandForm.created)).format('D-MMM-YYYY'));
          var dmd = new demand($scope.lnDemandForm);
          dmd.$saveData({}, function (response) {
            var table = document.getElementById('lnDemand').innerHTML;
            print_Entry(table);
            $scope.resetLNDemandForm();
            LastDemandBillNumber.demandBillNumber++;
            LastDemandBillNumber.daySerialNumber++;
            $scope.lnDemandForm.billNumber = 'LN-' + LastDemandBillNumber.demandBillNumber;
            growl.success("Demand generated and sent to LN successfully", {ttl: 3000});
          }, function (err) {
            console.log(err);
            growl.error("Some error occured!", {ttl: 3000});
          });
        }
      }
    }

    //-----------------------------------------------Receive Demand-----------------------------------------------------

    $scope.suppliedDemands = [];
    $scope.receiveLNDemandForm = {};
    demand.get({
      deployment_id: localStorageService.get('deployment_id'),
      tenant_id: localStorageService.get('tenant_id'),
      isSupplied: true,
      projection: {created: 1, user: 1, store: 1, items: 1}
    }, function (suppliedDemands) {
      $scope.suppliedDemands = suppliedDemands;
    }, function (err) {
      console.log(err);
    });

    $scope.openSuppliedDemand = function (demand) {
      $scope.receiveLNDemandForm = demand;
    }

    var validateRows = function (items) {
      var flag = true;
      _.forEach(items, function (item) {
        console.log('supplu qty', item.suppliedQty, (item.receivedQty >= item.suppliedQty));
        if (!_.has(item, 'receivedQty') || isNaN(item.receivedQty) || item.receivedQty > item.suppliedQty || item.receivedQty < 0)
          flag = false;
      });
      $scope.allowReceiptSubmit = flag;
    }

    $scope.submitDemandReceipt = function () {
      validateRows($scope.receiveLNDemandForm.items);
      if ($scope.allowReceiptSubmit) {
        $scope.receiveLNDemandForm.isReceived = true;
        $scope.receiveLNDemandForm.receiveDate = new Date();
        $scope.receiveLNDemandForm.xmlRequest = "";
        $scope.receiveLNDemandForm.$update(function (response) {
          $scope.receiveLNDemandForm = {
            items: []
          };
          growl.success("Material Receipt Successfully Generated", {ttl: 3000});
        }, function (err) {
          console.log(err);
          growl.error("Some error occured!", {ttl: 3000});
        });
      } else {
        growl.error("Please provide valid data!", {ttl: 3000});
      }
    }
  }]);
