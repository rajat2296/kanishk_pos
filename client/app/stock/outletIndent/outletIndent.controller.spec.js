'use strict';

describe('Controller: OutletIndentCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var OutletIndentCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OutletIndentCtrl = $controller('OutletIndentCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
