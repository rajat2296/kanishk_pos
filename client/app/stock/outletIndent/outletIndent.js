'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('outletIndent', {
        url: '/outletIndent',
        templateUrl: 'app/stock/outletIndent/outletIndent.html',
        controller: 'OutletIndentCtrl'
      });
  });
