'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('stockMasterUser', {
        url: '/stockMasterUser',
        templateUrl: 'app/stock/stockMasterUser/stockMasterUser.html',
        controller: 'StockMasterUserCtrl',
        resolve: {
                    currentUser: ['$state', '$stateParams', 'Auth',
                        function ($state, $stateParams, Auth) {
                            return Auth.getCurrentUser().$promise.then(function (user) {
                                return user;
                            });
                        }]
                }
      });
  });