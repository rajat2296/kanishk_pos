'use strict';

angular.module('posistApp')
  .controller('StockMasterUserCtrl',['$scope', 'Utils', 'StockMasterUser', 'deployment', '$http', 'growl' , '$modal' , function ($scope, Utils, StockMasterUser, deployment, $http, growl, $modal) {
  	
  	$scope.transactionsTab = {
  		stockTransactionForm: {},
  		transactions : [],
  		tenantNotSelected : true,
  		disableTenantAndDeployment : false,
  		showResetButton : false,
  		deployments: [],
  		transactionsToDelete : []	
  	}

  	$scope.resetTransactionsTab = function () {
  		$scope.transactionsTab = {
  		stockTransactionForm: {},
  		transactions : [],
  		tenantNotSelected : true,
  		showResetButton : false,
  		deployments : [],
  		transactionsToDelete : []
  		}	
  	}

  	$scope.disableTenant = function() {
  		$scope.transactionsTab.disableTenantAndDeployment = true;
  		$scope.transactionsTab.showResetButton = true;
  		$scope.transactionsTab.tenantNotSelected = false;
  	}

	$scope.dateCal={modFrom:false,modTo:false,delDateFrom:false,delDateTo:false,delItemFrom:false,delItemTo:false,delBillFrom:false,delBillTo:false,historyFromDate:false,historyToDate:false};

    $scope.openCalender = function($event,type) {
      //  console.log($scope.dateCal);
        $event.preventDefault();
        $event.stopPropagation();
        if(type=='modFrom'){$scope.dateCal.modFrom= true;$scope.dateCal.modTo= false;}
        if(type=='modTo'){$scope.dateCal.modTo= true;$scope.dateCal.modFrom= false;}
        if(type=='delDateFrom')$scope.dateCal.delDateFrom= true;
        if(type=='delDateTo')$scope.dateCal.delDateTo= true;
        if(type=='delItemFrom')$scope.dateCal.delItemFrom= true;
        if(type=='delItemTo')$scope.dateCal.delItemTo= true;
        if(type=='delBillFrom')$scope.dateCal.delBillFrom= true;
        if(type=='historyFrom')$scope.dateCal.historyFromDate= true;
        if(type=='historyTo')$scope.dateCal.historyToDate= true;
      };

      $scope.stockTransactionType = [
	      {
	      	type : '1',
	      	transactionName : 'Stock Entry'
	      },
	      {
	      	type : '2',
	      	transactionName : 'Stock Sale'
	      },
	      {
	      	type : '3',
	      	transactionName : 'Physical Stock'
	      },
	      {
	      	type : '4',
	      	transactionName : 'Wastage Entry'
	      },
	      {
	      	type : '5',
	      	transactionName : 'Stock Transfer'
	      },
	      {
	      	type : '6',
	      	transactionName : 'Opening Stock'
	      },
	      {
	      	type : '7',
	      	transactionName : 'Intermediate Entry'
	      },
	      {
	      	type : '9',
	      	transactionName : 'Stock Return'
	      },
	      {
	      	type : '10',
	      	transactionName : 'Intermediate Opening'
	      },
	      {
	      	type : '11',
	      	transactionName : 'Finished Food Entry'
	      },
	      {
	      	type : '12',
	      	transactionName : 'Stock Receive'
	      },
	      {
	      	type : '13',
	      	transactionName : 'Finished Food Opening'
	      },
	      {
	      	type : '14',
	      	transactionName : 'Indent Delivery Challan'
	      },
	      {
	      	type : '15',
	      	transactionName : 'Indent Delivery Challan'
	      },
	      {
	      	type : '16',
	      	transactionName : 'Outlet Indent'
	      }
      ];

      function formatTransaction(transaction) {
      	var tr = transaction;
      	var count = 0;
      	transaction.transactionTime = moment(new Date(transaction.created)).format('DD-MM-YYYY hh:mm:ss a');
      	
      	_.forEach($scope.stockTransactionType, function(type) {
      		if(type.type == tr.transactionType)
      			tr.transactionName = type.transactionName;
      	});

      	if(transaction.transactionType == '1')
      	{
      		_.forEach(transaction._store.vendor.category, function(category) {
      			_.forEach(category.items, function (item) {
      				count++;
      			});
      		});
      		tr.numberOfItems = count;
      	}
      	else if(transaction.transactionType == '2')
      	{
      		_.forEach(transaction._store.receiver.category, function(category) {
      			_.forEach(category.items, function (item) {
      				count++;
      			});
      		});
      		tr.numberOfItems = count;
      	}
      	else
      	{
      		_.forEach(transaction._store.category, function(category) {
      			_.forEach(category.items, function (item) {
      				count++;
      			});
      		});
      		tr.numberOfItems = count;
      	}
      	
      	return tr;
      }

      $scope.getTenantName = function(value) {
            var query = {
                search: value
            }
            return $http({
                method: 'POST',
                url: '/api/stockMasterUsers/getTenantName',
                data: query
            }).then(function(result) {
            	console.log('data', result);
                return result.data;
            });
        };

        $scope.getDeploymentName = function(tenantId) {
        	console.log('tenantId', $scope.transactionsTab.tenant);
        	StockMasterUser.getDeploymentName({tenant_id: tenantId}, function(deployment) {
        		console.log('deployment', deployment);
        		$scope.transactionsTab.deployments = deployment;	
        	}, function(err) {
                if (err.message)
                    growl.error(err.message, { ttl: 3000 });
                else
                    growl.error("Some error occured. Please try again", { ttl: 3000 });
            });
        }

      $scope.getStockTransactions = function(form) {
      	$scope.transactionsTab.transactions = [];
      	var req = {
      		fromDate : new Date($scope.transactionsTab.stockTransactionForm.fromDate),
      		toDate : new Date($scope.transactionsTab.stockTransactionForm.toDate),
      		transactionType : $scope.transactionsTab.stockTransactionForm.transactionType,
      		deployment_id: $scope.transactionsTab.stockTransactionForm.deployment_id
      	}
      	console.log('req', req);
      	StockMasterUser.getTransactions(req, function(transactions) {
      		_.forEach(transactions, function(transaction) {
      		var tr = formatTransaction(transaction);	
      		$scope.transactionsTab.transactions.push(tr);
      		})
      		
      		console.log('transactions', angular.copy(transactions));
      		//$scope.transactionsTab.transactions = tr;
      	});
      }


    $scope.deleteTransactionModal = function(transaction , deletionType , index) {
      	console.log(transaction, index);
      	var deployment_id = $scope.transactionsTab.stockTransactionForm.deployment_id;
      	if(deletionType == 'Single') {
      	$scope.modalInstance = $modal.open({
                templateUrl: 'deleteTransaction.html',
                controller: 'DeleteTransactionCtrl',
                size: 'sm',
                backdrop: 'static',
                resolve: {
                    transaction: function() {
                        return transaction;
                    },
                    deletionType : function() {
                    	return deletionType;
                    },
                    deployment_id : function() {
                    	return deployment_id;
                    }
                }
                
            }).result.then(function (res) {
            	if(res.result == 'success')
            		$scope.transactionsTab.transactions.splice(index, 1);
            	else
            		growl.success('Some error occured. Please try again.', {ttl:3000});
            });
        }
        else if(deletionType == 'Complete') {
        	if(confirm('Think Twice!')) 
        		if(confirm('Galti kari paap chadega!')) {
	        		$scope.modalInstance = $modal.open({
	                templateUrl: 'deleteTransaction.html',
	                controller: 'DeleteTransactionCtrl',
	                size: 'sm',
	                backdrop: 'static',
	                resolve: {
	                    transaction: function() {
	                        return transaction;
	                    },
	                    deletionType : function() {
	                    	return deletionType;
	                    },
	                    deployment_id : function() {
	                    	return deployment_id;
	                    }
	                }       
		            }).result.then(function (res) {
		            	if(res.result == 'success')
		            		$scope.transactionsTab.transactions = [];
		            	else
		            		growl.success('Some error occured. Please try again.', {ttl:3000});
		            });	
        		}
        	}
        else
        {
        	growl.error('Some error occured', {ttl:3000});
        }    
    }

  }])

.controller('DeleteTransactionCtrl', function($scope, $modalInstance, transaction, deletionType, deployment_id, growl, StockMasterUser) {
	$scope.deletionForm = {};
	console.log(transaction, deletionType, deployment_id);
	$scope.deletionType = deletionType;
	$scope.removeTransaction = function() {
		console.log('inside remove transaction', $scope.deletionForm);
		if($scope.deletionForm.comment)
		{
			if($scope.deletionForm.password && $scope.deletionForm.password == 'stock@123')
			{
				if(deletionType == 'Single')
				{
					var req = {
						transaction: transaction,
						comment: $scope.deletionForm.comment,
						deployment_id : deployment_id,
						deletionType : deletionType
					}
					StockMasterUser.deleteAllTransactions(req ,function(result) {
			      		if(result.message)
			      		{
			      			growl.success(result.message, {ttl:3000});
			      			$modalInstance.close({result: 'success'});
			      		}
			      		else
			      		{
			      			console.log('error while deleting', result);
			      			$modalInstance.close({result: 'failure'});
			      		}
				    });
				}
				else if(deletionType == 'Complete')
				{
					var req = {
						comment : $scope.deletionForm.comment,
						deployment_id : deployment_id,
						deletionType : deletionType
					}
					StockMasterUser.deleteAllTransactions(req ,function(result) {
			      		if(result.message)
			      		{
			      			growl.success(result.message, {ttl:3000});
			      			$modalInstance.close({result: 'success'});
			      		}
			      		else
			      		{
			      			console.log('error while deleting', result);
			      			$modalInstance.close({result: 'failure'});
			      		}
				    });
				}
				else
				{
					growl.error('Some error occured', {ttl :3000})
				}
			}
			else
			{
				growl.error('Please provide correct password.', {ttl:3000})
			}    
		}
		else
		{
			growl.error('Please provide correct comment.', {ttl:3000})
		}
	}
    
    $scope.cancel = function() {
        $modalInstance.close();
    };

});
