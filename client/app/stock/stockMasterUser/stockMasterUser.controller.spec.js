'use strict';

describe('Controller: StockMasterUserCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StockMasterUserCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StockMasterUserCtrl = $controller('StockMasterUserCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
