'use strict';

describe('Controller: StockReturnCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StockReturnCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StockReturnCtrl = $controller('StockReturnCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
