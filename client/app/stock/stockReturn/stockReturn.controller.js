/*
 'use strict';

 angular.module('posistApp')
 .controller('StockReturnCtrl', ['$q', '$scope', '$filter', '$resource', 'growl', 'currentUser', 'store', 'stockUnit', 'vendor', 'receiver', 'stockRequirement', 'localStorageService', 'StockTransaction', '$rootScope', 'stockRecipe', function ($q, $scope, $filter, $resource, growl, currentUser, store, stockUnit, vendor, receiver, stockRequirement, localStorageService, StockTransaction, $rootScope, stockRecipe) {
 $scope.stores = [];
 $scope.stockUnits = [];
 $scope.vendors = [];
 $scope.receivers = [];
 $scope.deliveryChallans = [];
 $scope.receiveGRN = [];
 $scope.requirements = [];
 $scope.requirementOrder = [];
 $scope.requirementChallan = [];
 $scope.requirementReceive = [];
 $scope.requirementPOs = [];
 $scope.allStockReturns = [];
 $scope.POTransactionTypes = [
 {id: '1', Name: 'indentGRNOrder', transactionType: '1'},
 {id: '2', Name: 'indentGRNDeliveryChallan', transactionType: '2'},
 {id: '3', Name: 'indentGRNReceive', transactionType: '3'}
 ];

 $scope.enableReturn = true;
 $scope.activeAccept = false;
 disableEntryForVendor('Vendor');
 function disableEntryForVendor(name) {

 var vendor = _.find(currentUser.selectedRoles, function (role) {
 return role.name == name;
 });
 if (vendor) {
 $scope.enableReturn = false;
 $scope.activeAccept = true;
 }
 }

 currentUser.deployment_id = localStorageService.get('deployment_id');

 $scope.stockReturnAccept = [];
 function getStores_ByDeployment() {
 return store.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id});
 };

 function getStockUnits_ByDeployment() {
 return stockUnit.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id});
 };

 function getVendors_ByDeployment() {
 return vendor.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id, type: "HO Vendor"});
 };

 function getStockRecipies() {
 var projection = {itemId: 1};
 stockRecipe.get({
 tenant_id: currentUser.tenant_id,
 deployment_id: $scope.currentUser.deployment_id,
 projection: projection
 }, function (recipies) {
 $scope.recipies = recipies;
 console.log($scope.recipies);
 });
 }

 function getReceivers_ByDeployment() {
 return receiver.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id});
 };

 function getRequirements_ByDeployment() {
 return stockRequirement.getRequirements({
 tenant_id: currentUser.tenant_id,
 deployment_id: $scope.currentUser.deployment_id
 });
 };

 function getStockReturnTransaction_ByDeployment() {
 return $scope.resource.get({
 transactionType: 9,
 tenant_id: currentUser.tenant_id,
 deployment_id: $scope.currentUser.deployment_id
 });
 };

 getStockRecipies();

 stockRequirement.getRequirements({
 tenant_id: currentUser.tenant_id,
 deployment_id: $scope.currentUser.deployment_id
 }).$promise.then(function (result) {
 $scope.requirements = result;
 GetAllRequirements(result);
 });
 $scope.resource = $resource('/api/stockTransactions/:id',
 {
 id: '@_id'
 },
 {
 saveData: {method: 'POST', isArray: true},
 get: {method: 'GET', isArray: true},
 findOne: {method: 'GET', isArray: true},
 update: {method: 'PUT'}
 }
 );

 $scope.resource.get({
 transactionType: "9",
 tenant_id: currentUser.tenant_id,
 deployment_id: $scope.currentUser.deployment_id
 }).$promise.then(function (result) {
 //$scope.stockReturnAccept=result;
 console.log(result);
 $scope.allStockReturns = result;
 //getAllReturnAccept(result);
 });

 $scope.resource.get({
 transactionType: "9",
 tenant_id: currentUser.tenant_id,
 toDeployment_id: $scope.currentUser.deployment_id
 }).$promise.then(function (result) {
 //$scope.stockReturnAccept=result;
 getAllReturnAccept(result);
 });

 function fetchRequiements(store_id, vendor_id, receiveDate) {
 var defered = $q.defer();
 stockRequirement.getRequirements({
 tenant_id: currentUser.tenant_id,
 deployment_id: $scope.currentUser.deployment_id,
 ReceiveDate: new Date(receiveDate),
 vendor_id: vendor_id,
 store_id: store_id
 }, function (result) {
 $scope.requirements = result;
 GetAllRequirements(result);
 defered.resolve(result);
 }, function (err) {
 defered.reject(err);
 });
 return defered.promise;
 }

 $scope.onAnyChange = function () {
 if ($scope.stockReturnForm.store && $scope.stockReturnForm.vendor && $scope.stockReturnForm.returnBillDate) {
 $rootScope.showLoaderOverlay = true;
 fetchRequiements($scope.stockReturnForm.store._id, $scope.stockReturnForm.vendor._id, new Date($scope.stockReturnForm.returnBillDate)).then(function (requirements) {
 console.log(requirements);
 $scope.bindPos_returnForm($scope.stockReturnForm.vendor, $scope.stockReturnForm.store)

 $scope.bindAcceptedPOs($scope.stockReturnForm.returnBillDate);
 $rootScope.showLoaderOverlay = false;
 }).catch(function (err) {
 growl.error("Some error occured!", {ttl: 3000});
 $rootScope.showLoaderOverlay = false;
 })
 }
 }

 var allPromise = $q.all([
 getStores_ByDeployment(), getStockUnits_ByDeployment(),
 getVendors_ByDeployment()
 //,getStockReturnTransaction_ByDeployment()
 ]);

 allPromise.then(function (value) {
 $scope.stores = value[0];
 $scope.stockUnits = value[1];
 $scope.vendors = value[2];
 //$scope.stockReturnAccept=value[5];
 });


 var guid = (function () {
 function s4() {
 return Math.floor((1 + Math.random()) * 0x10000)
 .toString(16)
 .substring(1);
 }

 return function () {
 return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
 s4() + '-' + s4() + s4() + s4();
 };
 })();

 function getAllReturnAccept(result) {
 _.forEach(result, function (a, i) {
 if (a.transactionType == "9" && a.isStockReturnAccepted == false) {
 $scope.stockReturnAccept.push(a);
 }
 });
 console.log($scope.stockReturnAccept);
 };
 //-----------------------------------------------------------------stock Return ------------------------------------//
 $scope.stockReturnForm = {
 isNew: false,
 enableSubmit: true,
 hasItems: false,
 discountType: 'percent',
 isTemplateDelete: true,
 isTemplateSave: true,
 billDate: new Date(),
 maxDate: new Date()
 };
 $scope.stockReturnForm.tempBillNo = "temp/" + $filter('date')(new Date(), 'dd-MMM-yyyy');

 $scope.clearReturnForm = function () {
 $scope.stockReturnForm = {
 isNew: false,
 enableSubmit: true,
 hasItems: false,
 discountType: 'percent',
 isTemplateDelete: true,
 isTemplateSave: true,
 billDate: new Date(),
 maxDate: new Date()
 };
 $scope.stockReturnForm.tempBillNo = "temp/" + $filter('date')(new Date(), 'dd-MMM-yyyy');
 };

 function GetAllRequirements(req) {
 $scope.requirementPOs = [];
 _.forEach(req, function (r, i) {
 if (r.supplyDate == null && r.ReceiveDate == null) {
 $scope.requirementChallan.push(r);
 }
 else if (r.supplyDate != null && r.ReceiveDate == null) {
 $scope.requirementReceive.push(r);
 }
 else {
 $scope.requirementPOs.push(r);
 }
 });
 };

 $scope.resetStockReturnTab = function () {
 if (!$scope.stockReturnForm.isEdit) {
 $scope.stockReturnForm = {
 isNew: false,
 enableSubmit: true,
 discountType: 'percent',
 isTemplateDelete: true,
 isTemplateSave: true,
 billDate: new Date(),
 maxDate: new Date()
 };
 $scope.tempBill_stockReturnForm($scope.stockReturnForm.billDate);
 }
 else {
 $scope.stockReturnForm.isSaved = false;
 $scope.stockReturnForm.isEdit = false;
 $scope.stockReturnForm.isPrint = false;
 }
 }
 $scope.tempBill_stockReturnForm = function (billDate) {
 $scope.stockReturnForm.showBill = true;
 $scope.stockReturnForm.showOpenBill = false;
 $scope.stockReturnForm.tempBillNo = "temp/" + $filter('date')(billDate, 'dd-MMM-yyyy');
 };

 $scope.bindVendor_returnForm = function (store) {
 $scope.stockReturnForm.availableCategory = [];
 $scope.stockReturnForm.availableItems = [];
 $scope.stockReturnForm.availableVendor = [];
 $scope.stockReturnForm.vendor = {};
 if (store != null) {
 _.forEach(angular.copy($scope.vendors), function (v, i) {
 if (v.pricing != undefined) {
 if (v.pricing._id == store._id) {
 if (v.type == "HO Vendor") {
 $scope.stockReturnForm.availableVendor.push(v);
 }
 }
 }
 });
 }
 };

 $scope.bindPos_returnForm = function (vendor, stores) {
 $scope.availablePosForReturn = [];
 //$scope.availablePosForReturn=[];
 var store = stores;
 var ven = vendor;
 $scope.availablePosForReturn = $scope.requirementPOs;
 };


 $scope.bindAcceptedPOs = function (returnBillDate) {
 console.log('Hello');
 $scope.stockReturnForm.availablePosForReturnDateWise = [];
 var bD = new Date(returnBillDate);
 //bD.setHours(0, 0, 0, 0);
 console.log(bD);
 _.forEach($scope.availablePosForReturn, function (pr, i) {

 var cD=new Date(pr.ReceiveDate);
 cD.setHours(0, 0, 0, 0);
 var timeDiff = Math.abs(cD.getTime() - bD.getTime());
 var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

 if(diffDays==0) {
 pr.billNo = "PO" + pr.daySerialNumber;
 var ind = _.findIndex($scope.allStockReturns, {stockReturnId: pr._id});
 console.log(ind);
 if (ind < 0) {
 $scope.stockReturnForm.availablePosForReturnDateWise.push(pr);
 }
 }
 });
 //console.log($scope.stockReturnForm.availablePosForReturnDateWise);
 //console.log($scope.allStockReturns);
 };

 $scope.EnableSubmitReturn = function (items) {
 var flag = false;
 _.forEach(items, function (item, i) {
 if (flag == false) {
 if (item.returnQty != undefined && item.returnQty != "") {
 if (!isNaN(parseFloat(item.returnQty))) {
 if (item.returnQty <= item.receiveQty) {
 flag = false;
 }
 else {
 growl.error('Return Quntity must be less than Receive quantity', {ttl: 3000});
 flag = true;
 }
 }
 else {
 item.returnQty = "";
 }
 }
 else {
 flag = true;
 }
 }
 });
 if (flag == false) {
 $scope.stockReturnForm.enableSubmit = false;
 }
 else {
 $scope.stockReturnForm.enableSubmit = true;
 }
 };

 $scope.goNextReturn = function (nextIdx) {
 var f = angular.element(document.querySelector('#r_' + nextIdx));
 if (f.length != 0) {
 f.focus();
 }
 else {
 $scope.stockReturnForm.enableSubmit = false;
 angular.element(document.querySelector('#submitReturn')).focus();
 }
 };
 $scope.goNextAccept = function (nextIdx) {
 var f = angular.element(document.querySelector('#pr_' + nextIdx));
 if (f.length != 0) {
 f.focus();
 }
 else {
 $scope.stockReturnAcceptForm.enableSubmit = false;
 angular.element(document.querySelector('#submitReturn')).focus();
 }
 };
 $scope.bindItemsReturn = function (returnBillNo) {
 $scope.stockReturnForm.availableItems = [];
 $scope.stockReturnForm.selectedUnits = {};
 var arr = $scope.stockReturnForm.availablePosForReturnDateWise;
 _.forEach(arr, function (a, i) {
 if (a.billNo == returnBillNo.billNo) {
 $scope.stockReturnForm.billToReturn = a;
 _.forEach(a.items, function (itm, ii) {
 $scope.stockReturnForm.selectedUnits[itm._id] = itm.preferedUnits;
 $scope.stockReturnForm.availableItems.push(itm);
 $scope.stockReturnForm.hasItems = true;
 });
 }
 ;
 });
 };


 function formatDataForStockEntry(rr) {
 var r = angular.copy(rr);
 for (var s in r.store) {
 if (s != "_id" && s != "storeName" && s != "StoreUID") {
 delete r.store[s];
 }
 }
 ;

 for (var v in r.vendor) {
 if (v != "_id" && v != "vendorName" && v != "type") {
 delete r.vendor[v];
 }
 }
 ;
 r.store.vendor = r.vendor;
 r.store.vendor.category = [];

 _.forEach(r.availableItems, function (itm, i) {
 var index = _.findIndex(r.store.vendor.category, {_id: itm.fromCategory._id});
 if (index < 0) {
 r.store.vendor.category.push(itm.fromCategory);
 }
 });

 _.forEach(r.store.vendor.category, function (c, i) {
 c.items = [];
 _.forEachRight(r.availableItems, function (itm, ii) {
 if (c._id == itm.fromCategory._id) {
 delete itm.fromCategory;
 delete itm.store;
 var reqQty = angular.copy(itm.qty);
 itm.qty = itm.returnQty;
 //console.log(itm)
 var ss = $scope.transaction.basePrefferedAnsSelectedUnitsByItemStockReturn(itm, $scope.stockReturnForm.selectedUnits, 1);
 itm.calculateInUnits = ss;
 itm.qty = reqQty;
 console.log(itm);
 c.items.push(itm);
 r.availableItems.splice(ii, 1);
 }
 });
 });
 r._store = r.store;
 r._vendor = r.vendor;
 r._items = r.availableItems;
 r.transactionType = 9;

 return r;
 };


 $scope.submitStockReturn = function (formStockReturn) {
 if (formStockReturn.$valid) {
 $scope.stockReturnForm.enableSubmit = true;
 $scope.transaction = new StockTransaction(currentUser, null, null, null, null, null, null, 9, null, null);
 var data = formatDataForStockEntry($scope.stockReturnForm);
 var bills = data;//$scope.stockReturnForm;
 bills.transactionId = guid();
 bills._id = bills.transactionId;
 bills.deployment_id = currentUser.deployment_id;
 bills.fromDeployment_id = currentUser.deployment_id;
 ;
 bills.toDeployment_id = $scope.stockReturnForm.billToReturn['toDeployment_id'];
 bills.tenant_id = currentUser.tenant_id;
 bills.user = currentUser;
 bills.stockReturnId = $scope.stockReturnForm.returnBillNo._id;
 bills.stockReturnBillNo = $scope.stockReturnForm.returnBillNo.billNo;
 bills.isStockReturn = true;
 bills.isStockReturnAccepted = false;
 bills.created = $scope.stockReturnForm.billDate;
 var timestamp = Date.parse(bills.created);
 bills.timeStamp = timestamp;
 console.log(bills);
 // One Stock Entry
 $scope.resource.save({}, bills, function (bill) {
 growl.success("Stock Entry success!", {ttl: 3000});
 $scope.stockReturnAccept.push(bill);
 $scope.allStockReturns.push(bill);
 $scope.resetStockReturnTab();
 });
 }
 };

 $scope.deleteSelectedItemReturnForm = function (item) {
 var index = _.findIndex($scope.stockReturnForm.availableItems, {_id: item._id});
 if (index >= 0) {
 $scope.stockReturnForm.availableItems.splice(index, 1);
 }
 }
 //-------------------------------------------------Return Accept ----------------------------------
 $scope.stockReturnAcceptForm = {isNew: true};

 $scope.clearAccept = function () {
 $scope.stockReturnAcceptForm = {isNew: true};
 };
 $scope.stockReturnAcceptFormData = function (r) {
 $scope.stockReturnAcceptForm = r;
 $scope.stockReturnAcceptForm.isNew = false;
 //$scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 15, null, null);
 //$scope.transaction.generateBillNumbers(15).then(function (snumbers) {
 //  $scope.transaction._transactionNumber = snumbers.billnumber;
 //  $scope.menuItemStockForm.tempBillNo = "Temp/PE -" + snumbers.billnumber;
 //});
 bindItemsAcceptForm(r._store);
 };
 function bindItemsAcceptForm(store) {
 $scope.stockReturnAcceptForm._items = [];
 $scope.stockReturnAcceptForm.selectedUnits = {};
 _.forEach(store.vendor.category, function (c, i) {
 _.forEach(c.items, function (itm, ii) {
 console.log(itm);
 var r = _.find($scope.recipies, function (recipe) {
 return recipe.itemId == itm._id;
 });
 if (r)
 itm.recipeId = r._id;
 $scope.stockReturnAcceptForm.selectedUnits[itm._id] = itm.preferedUnits;
 $scope.stockReturnAcceptForm._items.push(itm);
 });
 });
 //console.log($scope.stockReturnAcceptForm.selectedUnits);
 //console.log($scope.stockReturnAcceptForm._items);
 }

 function mapToStores(stockReturnAcceptForm) {

 var ch = stockReturnAcceptForm;
 var stores = [];
 _.forEach($scope.stores, function (store) {
 if (store.isKitchen) {
 var st = {
 _id: store._id,
 storeName: store.storeName,
 category: []
 };
 _.forEach(store.processedFoodCategory, function (cat) {
 var ct = {
 _id: cat._id,
 categoryName: cat.categoryName,
 items: []
 };
 _.forEach(ch._items, function (it) {
 var item = _.find(cat.item, function (itm) {
 return itm._id == it._id;
 });
 if (item)
 ct.items.push(it);
 });
 if (ct.items.length > 0)
 st.category.push(ct);
 });
 stores.push(st);
 }
 });
 return stores;
 }

 $scope.submitAcceptReturn = function (stockReturnAcceptForm) {
 var transactions = [];
 console.log(stockReturnAcceptForm);
 var ch = angular.copy(stockReturnAcceptForm);
 ch.isStockReturnAccepted = true;
 var stores = mapToStores(ch);
 _.forEach(stores, function (store) {
 var transaction = new StockTransaction(currentUser, null, null, null, null, null, null, 15, null, null);
 transaction._store = store;
 _.forEach(transaction._store.category, function (cat) {
 _.forEach(cat.items, function (itm) {
 //console.log(itm);
 var reqQty = angular.copy(itm.qty);
 itm.qty = itm.returnAcceptQty;
 //console.log(itm)
 var ss = basePrefferedAnsSelectedUnitsByItemStockReturn(itm, $scope.stockReturnAcceptForm.selectedUnits, 1);
 itm.calculateInUnits = ss;
 itm.qty = reqQty;
 //console.log(itm)
 });
 });
 transaction.transactionId = guid();
 transaction._id = transaction.transactionId;
 transaction.deployment_id = currentUser.deployment_id;
 transaction.tenant_id = currentUser.tenant_id;
 transaction.user = currentUser;
 transaction.isStockReturn = true;
 transaction.isStockReturnAccepted = false;
 transaction.created = $scope.stockReturnForm.billDate;
 var timestamp = Date.parse(transaction.created);
 transaction.timeStamp = timestamp;
 //console.log(transaction);
 $scope.resource.save({}, transaction, function (tr) {

 });
 });
 /!*$q.all(transactions, function (){
 console.log('all');
 growl.success("Stock Entry Successful", {ttl: 3000});
 }).catch(function (err) {
 growl.err("Some error occured", {ttl: 3000});
 });*!/
 $scope.resource.update({}, ch, function (bill) {
 growl.success("Stock Entry success!", {ttl: 3000});
 var index = _.findIndex($scope.stockReturnAccept, {_id: bill._id});
 if (index >= 0) {
 $scope.stockReturnAccept.splice(index, 1);
 $scope.clearAccept();
 }

 });
 }

 $scope.changeWastage = function (item) {
 if (item.returnQty < item.returnAcceptQty) {
 growl.error("Accepted quantity cannot be greater than returned quantity", {ttl: 3000});
 item.returnAcceptQty = "";
 item.returnWastageQty = "";
 }
 else
 item.returnWastageQty = parseFloat(item.returnQty) - parseFloat(item.returnAcceptQty);
 };

 $scope.changeAccept = function (item) {
 if (item.returnQty < item.returnWastageQty) {
 growl.error("Waastage quantity cannot be greater than returned quantity", {ttl: 3000});
 item.returnAcceptQty = "";
 item.returnWastageQty = "";
 }
 else
 item.returnAcceptQty = parseFloat(item.returnQty) - parseFloat(item.returnWastageQty);
 };

 function basePrefferedAnsSelectedUnitsByItemStockReturn(item, selectedUnits, type) {
 var conversionFactor = 1;
 var selectedConversionFactor = 1;
 console.log(selectedUnits);
 _.forEach(selectedUnits, function (u, ii) {
 console.log(ii, u);
 if (u != null) {
 console.log(item.preferedUnits);
 if (ii == item._id) {
 console.log(u.conversionFactor);
 selectedConversionFactor = u.conversionFactor;
 if (type == 1) {
 var selectedUnit1 = {
 "_id": u._id,
 "unitName": u.unitName,
 "basePrice": parseFloat(item.price),
 "baseQty": parseFloat(item.qty),
 "subTotal": parseFloat(item.subTotal),
 "addedAmt": parseFloat(item.addedAmt),
 "totalAmount": parseFloat(item.totalAmount),
 "type": "selectedUnit",
 "conversionFactor": parseFloat(u.conversionFactor)
 };
 selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
 selectedUnit1.addedAmt = 0;
 if (item.vatPercent != "" && item.vatPercent != undefined) {
 selectedUnit1.addedAmt = parseFloat(parseFloat(selectedUnit1.subTotal) * .01 * parseFloat(item.vatPercent));
 }
 selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
 item["selectedUnit"] = selectedUnit1;
 if (item.preferedUnit == undefined) {
 item["preferedUnits"] = angular.copy(selectedUnit1);
 item.preferedUnits.type = "preferedUnit";
 }
 }
 else if (type == 2) {
 var selectedUnit1 = {
 "_id": u._id,
 "unitName": u.unitName,
 "basePrice": parseFloat(item.price),
 "baseWastagePrice": parseFloat(item.price),
 "baseQty": parseFloat(item.qty),
 "baseWastageQty": parseFloat(item.returnWastageQty),
 "subTotalWastage": parseFloat(item.subTotalWastage),
 "addedAmt": parseFloat(item.addedAmt),
 "addedAmtWastage": parseFloat(item.addedAmtWastage),
 "totalAmount": parseFloat(item.totalAmount),
 "totalWastageAmount": parseFloat(item.totalAmountWastage),
 "type": "selectedUnit",
 "conversionFactor": parseFloat(u.conversionFactor)
 };
 selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
 selectedUnit1.subTotalWastage = parseFloat(item.price) * parseFloat(item.returnWastageQty);
 selectedUnit1.addedAmt = 0;
 selectedUnit1.addedAmtWastage = 0;
 selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
 selectedUnit1.totalWastageAmount = parseFloat(selectedUnit1.subTotalWastage) + parseFloat(selectedUnit1.addedAmtWastage);
 item["selectedUnit"] = selectedUnit1;

 if (item.preferedUnit == undefined) {
 item["preferedUnits"] = angular.copy(selectedUnit1);
 item.preferedUnits.type = "preferedUnit";
 }
 }
 }
 }
 });

 var baseUnit = {};
 if (item.units[0].baseUnit != undefined) {
 baseUnit = angular.copy(item.units[0].baseUnit);
 baseUnit.unitName = baseUnit.name;
 }
 else {
 if (item.baseUnit != undefined) {
 baseUnit = angular.copy(item.baseUnit);
 baseUnit.unitName = baseUnit.name;
 }
 else {
 baseUnit = angular.copy(item.units[0]);
 }
 }

 baseUnit.type = "baseUnit";
 baseUnit.conversionFactor = conversionFactor;

 baseUnit.baseQty = parseFloat(item.qty * selectedConversionFactor);
 baseUnit.basePrice = parseFloat(item.price / selectedConversionFactor);
 baseUnit.baseWastageQty = parseFloat(item.returnWastageQty * selectedConversionFactor);
 baseUnit.baseWastagePrice = parseFloat(item.price / selectedConversionFactor);

 var baseAmt = parseFloat(item.qty) * parseFloat(item.price);
 var baseWastageAmt = parseFloat(item.returnWastageQty) * parseFloat(item.price);
 if (item.vatPercent == "" || item.vatPercent == undefined) {
 baseUnit.totalAmount = baseAmt;
 baseUnit.subTotal = baseAmt;
 baseUnit.addedAmt = 0;

 baseUnit.totalWastageAmount = baseWastageAmt;
 baseUnit.subTotalWastage = baseWastageAmt;
 baseUnit.addedAmtWastage = 0;
 }
 else {
 var vatamt = (baseAmt * .01 * parseFloat(item.vatPercent));
 var vatamtWastage = (baseWastageAmt * .01 * parseFloat(item.vatPercent));
 baseUnit.addedAmt = vatamt;
 baseUnit.subTotal = baseAmt;
 baseUnit.totalAmount = baseAmt + parseFloat(vatamt);

 baseUnit.addedAmtWastage = vatamtWastage;
 baseUnit.subTotalWastage = baseWastageAmt;
 baseUnit.totalWastageAmount = baseWastageAmt + parseFloat(vatamtWastage);
 }

 item.baseUnit = baseUnit;

 var pInd = _.findIndex(item.units, {_id: item.preferedUnit});

 if (pInd >= 0) {
 var preferedUnits = angular.copy(item.units[pInd]);
 var preferedconversionFactor = item.units[pInd].conversionFactor;
 preferedUnits.addedAmt = 0;
 preferedUnits.addedAmtWastage = 0;

 console.log("selectedConversionFactor" + selectedConversionFactor);
 console.log("preferedconversionFactor" + preferedconversionFactor);
 if (preferedconversionFactor == selectedConversionFactor) {
 // preferedUnits.baseQty=parseFloat(baseUnit.baseQty);
 // preferedUnits.basePrice=parseFloat(baseUnit.basePrice);
 // preferedUnits.addedAmt=parseFloat(baseUnit.addedAmt);
 // preferedUnits.subTotal=parseFloat(baseUnit.subTotal);
 // preferedUnits.totalAmount=parseFloat(preferedUnits.subTotal)+parseFloat(preferedUnits.addedAmt);
 preferedUnits = angular.copy(item.selectedUnit);
 }
 else if (preferedconversionFactor > selectedConversionFactor) {
 preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
 preferedUnits.baseWastageQty = parseFloat(baseUnit.baseWastageQty) / parseFloat(preferedconversionFactor);
 if (type == 1) {
 preferedUnits.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
 preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) * parseFloat(preferedconversionFactor);
 }
 else {
 preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
 preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice);
 baseUnit.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
 baseUnit.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) / parseFloat(preferedconversionFactor);
 item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) / parseFloat(preferedconversionFactor);
 item.selectedUnit.baseWastagePrice = parseFloat(item.selectedUnit.baseWastagePrice) / parseFloat(preferedconversionFactor);
 }
 preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
 preferedUnits.subTotalWastage = parseFloat(preferedUnits.baseWastageQty) * parseFloat(preferedUnits.baseWastagePrice);
 if (item.vatPercent != "" && item.vatPercent != undefined) {
 preferedUnits.addedAmt = parseFloat(parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
 preferedUnits.addedAmtWastage = parseFloat(parseFloat(preferedUnits.subTotalWastage) * .01 * parseFloat(item.vatPercent));
 }

 preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
 preferedUnits.totalWastageAmount = parseFloat(preferedUnits.subTotalWastage) + parseFloat(preferedUnits.addedAmtWastage);
 }
 else if (preferedconversionFactor < selectedConversionFactor) {
 preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
 preferedUnits.baseWastageQty = parseFloat(baseUnit.baseWastageQty) * parseFloat(preferedconversionFactor);
 if (type == 1) {
 preferedUnits.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
 preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) / parseFloat(preferedconversionFactor);
 }
 else {
 preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
 preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice);
 baseUnit.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
 baseUnit.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) * parseFloat(preferedconversionFactor);
 item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) * parseFloat(preferedconversionFactor);
 item.selectedUnit.baseWastagePrice = parseFloat(item.selectedUnit.baseWastagePrice) * parseFloat(preferedconversionFactor);
 }
 preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
 preferedUnits.subTotalWastage = parseFloat(preferedUnits.baseWastageQty) * parseFloat(preferedUnits.baseWastagePrice);
 if (item.vatPercent != "" && item.vatPercent != undefined) {
 preferedUnits.addedAmtWastage = (parseFloat(preferedUnits.subTotalWastage) * .01 * parseFloat(item.vatPercent));
 }
 preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
 preferedUnits.totalWastageAmount = parseFloat(preferedUnits.subTotalWastage) + parseFloat(preferedUnits.addedAmtWastage);
 }

 if (type == 2) {
 baseUnit.subTotal = parseFloat(baseUnit.basePrice) * parseFloat(baseUnit.baseQty);
 baseUnit.subTotalWastage = parseFloat(baseUnit.baseWastagePrice) * parseFloat(baseUnit.baseWastageQty);
 baseUnit.addedAmt = 0;
 baseUnit.addedAmtWastage = 0;
 baseUnit.totalAmount = parseFloat(baseUnit.subTotal);
 baseUnit.totalWastageAmount = parseFloat(baseUnit.subTotalWastage);

 item.selectedUnit.subTotal = parseFloat(item.selectedUnit.basePrice) * parseFloat(item.selectedUnit.baseQty);
 item.selectedUnit.subTotalWastage = parseFloat(item.selectedUnit.baseWastagePrice) * parseFloat(item.selectedUnit.baseWastageQty);
 item.selectedUnit.addedAmt = 0;
 item.selectedUnit.addedAmtWastage = 0;
 item.selectedUnit.totalAmount = parseFloat(item.selectedUnit.subTotal);
 item.selectedUnit.totalWastageAmount = parseFloat(item.selectedUnit.subTotalWastage);
 }
 preferedUnits.type = "preferedUnit";
 delete preferedUnits.baseUnit;
 item.preferedUnits = preferedUnits;
 }
 if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
 var cf = 1000;
 if (selectedConversionFactor >= cf) {
 item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
 item.baseUnit.baseWastagePrice = parseFloat(item.baseUnit.baseWastagePrice) * parseFloat(cf);
 item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
 item.baseUnit.baseWastageQty = parseFloat(item.baseUnit.baseWastageQty) / parseFloat(cf);
 }
 else if (cf > selectedConversionFactor) {
 item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
 item.baseUnit.baseWastagePrice = parseFloat(item.baseUnit.baseWastagePrice) * parseFloat(cf);
 item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
 item.baseUnit.baseWastageQty = parseFloat(item.baseUnit.baseWastageQty) / parseFloat(cf);
 }
 ;
 }
 ;

 item.calculateInUnits = [];
 item.calculateInUnits.push(item.baseUnit);
 item.calculateInUnits.push(item.preferedUnits);
 item.calculateInUnits.push(item.selectedUnit);

 return item.calculateInUnits;
 }
 }]);
 */


//---------------------------------------------------------new test code--------------------------------------------------------//

'use strict';

angular.module('posistApp')
  .controller('StockReturnCtrl', ['$q', '$scope', '$filter', '$resource', 'growl', 'currentUser', 'store', 'stockUnit', 'vendor', 'receiver', 'stockRequirement', 'localStorageService', 'StockTransaction', '$rootScope', 'stockRecipe', 'ClosingQuantity', function ($q, $scope, $filter, $resource, growl, currentUser, store, stockUnit, vendor, receiver, stockRequirement, localStorageService, StockTransaction, $rootScope, stockRecipe, ClosingQuantity) {
    $scope.stores = [];
    $scope.stockUnits = [];
    $scope.vendors = [];
    $scope.receivers = [];
    $scope.deliveryChallans = [];
    $scope.receiveGRN = [];
    $scope.requirements = [];
    $scope.requirementOrder = [];
    $scope.requirementChallan = [];
    $scope.requirementReceive = [];
    $scope.requirementPOs = [];
    $scope.allStockReturns = [];
    $scope.POTransactionTypes = [
      {id: '1', Name: 'indentGRNOrder', transactionType: '1'},
      {id: '2', Name: 'indentGRNDeliveryChallan', transactionType: '2'},
      {id: '3', Name: 'indentGRNReceive', transactionType: '3'}
    ];

    $scope.pagination = {};
    $scope.pagination.currentPage = 1;
    $scope.pagination.itemsPerPage = 20; 

    $scope.enableReturn = true;
    $scope.activeAccept = false;
    disableEntryForVendor('Vendor');
    function disableEntryForVendor(name) {

      var vendor = _.find(currentUser.selectedRoles, function (role) {
        return role.name == name;
      });
      if (vendor) {
        $scope.enableReturn = false;
        $scope.activeAccept = true;
      }
    }

    var startDate = new Date();
    startDate.setHours(0, 0, 0, 0);
    var endDate = new Date();
    endDate.setHours(0, 0, 0, 0);
    endDate.setDate(endDate.getDate() + 1);

    var cls = new ClosingQuantity(null, startDate, endDate, localStorageService.get('deployment_id'), localStorageService.get('tenant_id'));
    cls.getClosingQty().then(function (data) {
      $scope.closingQty = data;
      //console.log("CLOSING QUANTITY", $scope.closingQty);
    });

    currentUser.deployment_id = localStorageService.get('deployment_id');

    $scope.stockReturnAccept = [];
    function getStores_ByDeployment() {
      return store.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id});
    };

    function getStockUnits_ByDeployment() {
      return stockUnit.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id});
    };

    function getVendors_ByDeployment() {
      return vendor.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: $scope.currentUser.deployment_id,
        type: "HO Vendor"
      });
    };

    function getStockRecipies() {
      var projection = {itemId: 1};
      stockRecipe.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: $scope.currentUser.deployment_id,
        projection: projection
      }, function (recipies) {
        $scope.recipies = recipies;
        //console.log($scope.recipies);
      });
    }

    function getReceivers_ByDeployment() {
      return receiver.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id});
    };

    function getRequirements_ByDeployment() {
      return stockRequirement.getRequirements({
        tenant_id: currentUser.tenant_id,
        deployment_id: $scope.currentUser.deployment_id
      });
    };

    function getStockReturnTransaction_ByDeployment() {
      return $scope.resource.get({
        transactionType: 9,
        tenant_id: currentUser.tenant_id,
        deployment_id: $scope.currentUser.deployment_id
      });
    };

    getStockRecipies();

    //stockRequirement.getRequirements({
    //  tenant_id: currentUser.tenant_id,
    //  deployment_id: $scope.currentUser.deployment_id
    //}).$promise.then(function (result) {
    //  $scope.requirements = result;
    //  GetAllRequirements(result);
    //});
    $scope.resource = $resource('/api/stockTransactions/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: true},
        get: {method: 'GET', isArray: true},
        getPagination: {method: 'GET', params: {controller: "getPagination"}, isArray: true},
        findOne: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      }
    );

    $scope.resource.get({
      transactionType: "9",
      tenant_id: currentUser.tenant_id,
      deployment_id: $scope.currentUser.deployment_id,
      //isStockReturnAccepted: false
    }).$promise.then(function (result) {
      //$scope.stockReturnAccept=result;
      //console.log(result);
      $scope.allStockReturns = result;
      console.log($scope.allStockReturns);
      //getAllReturnAccept(result);
    });
    //modified 19/7/16
    $scope.limitReturn = 20;
    $scope.skipReturn = 0;
    $scope.loadMore = false;
    $scope.req={};

    //$scope.fetchReturn = function (skipReturn, limitReturn) {
    //  console.log('return');
    //  if ((skipReturn || skipReturn == 0) && limitReturn) {
    //    $scope.req.return = [];
    //    $scope.skipReturn = skipReturn;
    //    $scope.limitReturn = limitReturn;
    //  }
    //  var query = {
    //    transactionType: "9",
    //    tenant_id: currentUser.tenant_id,
    //    toDeployment_id: $scope.currentUser.deployment_id,
    //    isStockReturnAccepted: true
    //  }
    //  query.skip = skipReturn == 0 || skipReturn ? skipReturn : $scope.skipReturn;
    //  query.limit = limitReturn ? limitReturn : $scope.limitReturn;
    //  console.log(query);
    //  $scope.resource.getPagination(query, function (stockReturn) {
    //    console.log(stockReturn);
    //    _.forEach(stockReturn, function (req) {
    //      $scope.req.return.push(req);
    //    })
    //    getAllReturnAccept($scope.req.return);
    //    //$scope.req.indents = stockreq;
    //    $scope.loadMore = true;
    //
    //    if( $scope.req.return.length > 0 &&  $scope.req.return.length == 20)
    //      $scope.loadMore = true;
    //    else $scope.loadMore = false;
    //    console.log($scope.skipReturn);
    //    $scope.skipReturn += $scope.limitReturn;
    //  })
    //
    //
    //};
    //$scope.fetchReturn(0,20);

    $scope.resource.getPagination({
      transactionType: "9",
      tenant_id: currentUser.tenant_id,
      toDeploymentId: $scope.currentUser.deployment_id,
      isStockReturnAccepted: false
    }).$promise.then(function (result) {
      //$scope.stockReturnAccept=result;
      getAllReturnAccept(result);
      console.log(result);
    });

    function fetchRequiements(store_id, vendor_id, receiveDate) {
      var defered = $q.defer();
      stockRequirement.getRequirements({
        tenant_id: currentUser.tenant_id,
        deployment_id: $scope.currentUser.deployment_id,
        ReceiveDate: new Date(receiveDate),
        vendor_id: vendor_id,
        store_id: store_id
      }, function (result) {
        $scope.requirements = result;
        GetAllRequirements(result);
        defered.resolve(result);
      }, function (err) {
        defered.reject(err);
      });
      return defered.promise;
    }

    $scope.onAnyChange = function () {
      if ($scope.stockReturnForm.store && $scope.stockReturnForm.vendor && $scope.stockReturnForm.returnBillDate) {
        $rootScope.showLoaderOverlay = true;
        fetchRequiements($scope.stockReturnForm.store._id, $scope.stockReturnForm.vendor._id, new Date($scope.stockReturnForm.returnBillDate)).then(function (requirements) {
          //console.log(requirements);
          $scope.bindPos_returnForm($scope.stockReturnForm.vendor, $scope.stockReturnForm.store)

          $scope.bindAcceptedPOs($scope.stockReturnForm.returnBillDate);
          $rootScope.showLoaderOverlay = false;
        }).catch(function (err) {
          growl.error("Some error occured!", {ttl: 3000});
          $rootScope.showLoaderOverlay = false;
        })
      }
    }

    var allPromise = $q.all([
      getStores_ByDeployment(), getStockUnits_ByDeployment(),
      getVendors_ByDeployment()
      //,getStockReturnTransaction_ByDeployment()
    ]);

    allPromise.then(function (value) {
      $scope.stores = value[0];
      $scope.stockUnits = value[1];
      $scope.vendors = value[2];
      //$scope.stockReturnAccept=value[5];
    });


    var guid = (function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }

      return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
      };
    })();

    function getAllReturnAccept(result) {
      _.forEach(result, function (a, i) {
      	console.log('a',a);
      	
        if (a.transactionType == "9" && a.isStockReturnAccepted == false) {
          $scope.stockReturnAccept.push(a);
          $scope.stockReturnAccept.sort(function(b,c)
      	{
      	return new Date(b.created) - new Date(c.created);
      	});
        }
      });
      //console.log($scope.stockReturnAccept);
    };
    //-----------------------------------------------------------------stock Return ------------------------------------//
    $scope.stockReturnForm = {
      isNew: false,
      enableSubmit: true,
      hasItems: false,
      discountType: 'percent',
      isTemplateDelete: true,
      isTemplateSave: true,
      billDate: new Date(),
      maxDate: new Date()
    };
    $scope.stockReturnForm.tempBillNo = "temp/" + $filter('date')(new Date(), 'dd-MMM-yyyy');

    $scope.clearReturnForm = function () {
      $scope.stockReturnForm = {
        isNew: false,
        enableSubmit: true,
        hasItems: false,
        discountType: 'percent',
        isTemplateDelete: true,
        isTemplateSave: true,
        billDate: new Date(),
        maxDate: new Date()
      };
      $scope.stockReturnForm.tempBillNo = "temp/" + $filter('date')(new Date(), 'dd-MMM-yyyy');
    };

    function GetAllRequirements(req) {
      $scope.requirementPOs = [];
      _.forEach(req, function (r, i) {
        if (r.supplyDate == null && r.ReceiveDate == null) {
          $scope.requirementChallan.push(r);
        }
        else if (r.supplyDate != null && r.ReceiveDate == null) {
          $scope.requirementReceive.push(r);
        }
        else {
          $scope.requirementPOs.push(r);
        }
      });
    };

    $scope.resetStockReturnTab = function () {
      if (!$scope.stockReturnForm.isEdit) {
        $scope.stockReturnForm = {
          isNew: false,
          enableSubmit: true,
          discountType: 'percent',
          isTemplateDelete: true,
          isTemplateSave: true,
          billDate: new Date(),
          maxDate: new Date()
        };
        $scope.tempBill_stockReturnForm($scope.stockReturnForm.billDate);
      }
      else {
        $scope.stockReturnForm.isSaved = false;
        $scope.stockReturnForm.isEdit = false;
        $scope.stockReturnForm.isPrint = false;
      }
    }
    $scope.tempBill_stockReturnForm = function (billDate) {
      $scope.stockReturnForm.showBill = true;
      $scope.stockReturnForm.showOpenBill = false;
      $scope.stockReturnForm.tempBillNo = "temp/" + $filter('date')(billDate, 'dd-MMM-yyyy');
    };

    $scope.bindVendor_returnForm = function (store) {
      $scope.stockReturnForm.availableCategory = [];
      $scope.stockReturnForm.availableItems = [];
      $scope.stockReturnForm.availableVendor = [];
      $scope.stockReturnForm.vendor = {};
      if (store != null) {
        _.forEach(angular.copy($scope.vendors), function (v, i) {
          if (v.pricing != undefined) {
            if (v.pricing._id == store._id) {
              if (v.type == "HO Vendor") {
                $scope.stockReturnForm.availableVendor.push(v);
              }
            }
          }
        });
      }
    };

    $scope.bindPos_returnForm = function (vendor, stores) {
      $scope.availablePosForReturn = [];
      //$scope.availablePosForReturn=[];
      var store = stores;
      var ven = vendor;
      $scope.availablePosForReturn = $scope.requirementPOs;
    };


    $scope.bindAcceptedPOs = function (returnBillDate) {
      //console.log('Hello');
      $scope.stockReturnForm.availablePosForReturnDateWise = [];
      var bD = new Date(returnBillDate);
      //bD.setHours(0, 0, 0, 0);
      //console.log(bD);
      _.forEach($scope.availablePosForReturn, function (pr, i) {

        var cD = new Date(pr.ReceiveDate);
        cD.setHours(0, 0, 0, 0);
        var timeDiff = Math.abs(cD.getTime() - bD.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if (diffDays == 0) {
          pr.billNo = "PO" + pr.daySerialNumber;
          var ind = _.findIndex($scope.allStockReturns, {stockReturnId: pr._id});
          //console.log(ind);
          if (ind < 0) {
            $scope.stockReturnForm.availablePosForReturnDateWise.push(pr);
          }
        }
      });
      //console.log($scope.stockReturnForm.availablePosForReturnDateWise);
      //console.log($scope.allStockReturns);
    };

    $scope.EnableSubmitReturn = function (items) {
      var flag = false;
      _.forEach(items, function (item, i) {
        if (flag == false) {
          if (item.returnQty != undefined && item.returnQty != "") {
            if (!isNaN(parseFloat(item.returnQty))) {
              if (item.returnQty <= item.receiveQty) {
                flag = false;
              }
              else {
                growl.error('Return Quntity must be less than Receive quantity', {ttl: 3000});
                flag = true;
              }
            }
            else {
              item.returnQty = "";
            }
          }
          else {
            flag = true;
          }
        }
      });
      if (flag == false) {
        $scope.stockReturnForm.enableSubmit = false;
      }
      else {
        $scope.stockReturnForm.enableSubmit = true;
      }
    };

    $scope.goNextReturn = function (nextIdx) {
      var f = angular.element(document.querySelector('#r_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
      else {
        $scope.stockReturnForm.enableSubmit = false;
        angular.element(document.querySelector('#submitReturn')).focus();
      }
    };
    $scope.goNextAccept = function (nextIdx) {
      var f = angular.element(document.querySelector('#pr_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
      else {
        $scope.stockReturnAcceptForm.enableSubmit = false;
        angular.element(document.querySelector('#submitReturn')).focus();
      }
    };
    $scope.bindItemsReturn = function (returnBillNo) {
      $scope.stockReturnForm.availableItems = [];
      $scope.stockReturnForm.selectedUnits = {};
      var arr = $scope.stockReturnForm.availablePosForReturnDateWise;
      _.forEach(arr, function (a, i) {
        if (a.billNo == returnBillNo.billNo) {
          $scope.stockReturnForm.billToReturn = a;
          _.forEach(a.items, function (itm, ii) {
            var index = _.findIndex($scope.closingQty, function (cl) {
              return cl.storeId == $scope.stockReturnForm.store._id && cl.itemId == itm._id;
            });
            itm.availableQty = $scope.closingQty[index].closingQtyInBase / itm.selectedUnit.conversionFactor;
            itm.availableQtyUnit = $scope.closingQty[index].unitName;
            $scope.stockReturnForm.selectedUnits[itm._id] = itm.preferedUnits;
            $scope.stockReturnForm.availableItems.push(itm);
            $scope.stockReturnForm.hasItems = true;
          });
        }
        ;
      });
    };


    function formatDataForStockEntry(rr) {
      var r = angular.copy(rr);
      for (var s in r.store) {
        if (s != "_id" && s != "storeName" && s != "StoreUID") {
          delete r.store[s];
        }
      }
      ;

      for (var v in r.vendor) {
        if (v != "_id" && v != "vendorName" && v != "type") {
          delete r.vendor[v];
        }
      }
      ;
      r.store.vendor = r.vendor;
      r.store.vendor.category = [];

      _.forEach(r.availableItems, function (itm, i) {
        var index = _.findIndex(r.store.vendor.category, {_id: itm.fromCategory._id});
        if (index < 0) {
          r.store.vendor.category.push(itm.fromCategory);
        }
      });

      _.forEach(r.store.vendor.category, function (c, i) {
        c.items = [];
        _.forEachRight(r.availableItems, function (itm, ii) {
          if (c._id == itm.fromCategory._id) {
            delete itm.fromCategory;
            delete itm.store;
            var reqQty = angular.copy(itm.qty);
            itm.qty = itm.returnQty;
            //console.log(itm)
            var ss = $scope.transaction.basePrefferedAnsSelectedUnitsByItemStockReturn(itm, $scope.stockReturnForm.selectedUnits, 1);
            itm.calculateInUnits = ss;
            itm.qty = reqQty;
            //console.log(itm);
            c.items.push(itm);
            r.availableItems.splice(ii, 1);
          }
        });
      });
      r._store = r.store;
      r._vendor = r.vendor;
      r._items = r.availableItems;
      r.transactionType = 9;

      return r;
    };


    $scope.submitStockReturn = function (formStockReturn) {
      if (formStockReturn.$valid) {
        $scope.stockReturnForm.enableSubmit = true;
        $scope.transaction = new StockTransaction(currentUser, null, null, null, null, null, null, 9, null, null);
        var data = formatDataForStockEntry($scope.stockReturnForm);
        var bills = data;//$scope.stockReturnForm;
        bills.transactionId = guid();
        bills._id = bills.transactionId;
        bills.deployment_id = currentUser.deployment_id;
        bills.fromDeployment_id = currentUser.deployment_id;
        bills.toDeployment_id = $scope.stockReturnForm.billToReturn['toDeployment_id'];
        bills.tenant_id = currentUser.tenant_id;
        bills.user = currentUser;
        bills.stockReturnId = $scope.stockReturnForm.returnBillNo._id;
        bills.stockReturnBillNo = $scope.stockReturnForm.returnBillNo.billNo;
        bills.isStockReturn = true;
        bills.isStockReturnAccepted = false;
        bills.created = $scope.stockReturnForm.billDate;
        var timestamp = Date.parse(bills.created);
        bills.timeStamp = timestamp;
        //console.log(bills);
        // One Stock Entry
        _.forEach($scope.stockReturnForm.availableItems, function (item) {
          var index = _.findIndex($scope.closingQty, function (cl) {
            return cl.storeId == $scope.stockReturnForm.store._id && cl.itemId == item._id;
          });
          //console.log(index);
          $scope.closingQty[index].closingQtyInBase = $scope.closingQty[index].closingQtyInBase - (item.returnQty * item.selectedUnit.conversionFactor);
        });
        $scope.resource.save({}, bills, function (bill) {
          growl.success("Stock Entry success!", {ttl: 3000});
          $scope.stockReturnAccept.push(bill);
          $scope.allStockReturns.push(bill);
          $scope.resetStockReturnTab();
        });
      }
    };

    $scope.deleteSelectedItemReturnForm = function (item) {
      var index = _.findIndex($scope.stockReturnForm.availableItems, {_id: item._id});
      if (index >= 0) {
        $scope.stockReturnForm.availableItems.splice(index, 1);
      }
    }
    //-------------------------------------------------Return Accept ----------------------------------
    $scope.stockReturnAcceptForm = {isNew: true};

    $scope.clearAccept = function () {
      $scope.stockReturnAcceptForm = {isNew: true};
    };
    $scope.stockReturnAcceptFormData = function (r) {
      $scope.stockReturnAcceptForm = r;
      $scope.stockReturnAcceptForm.isNew = false;
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 15, null, null);
      $scope.transaction.generateBillNumbers(15).then(function (snumbers) {
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        $scope.stockReturnAcceptForm.tempBillNo = "Temp/PE -" + snumbers.billnumber;
      });
      bindItemsAcceptForm(r._store);
    };
    function bindItemsAcceptForm(store) {
      $scope.stockReturnAcceptForm._items = [];
      $scope.stockReturnAcceptForm.selectedUnits = {};
      _.forEach(store.vendor.category, function (c, i) {
        _.forEach(c.items, function (itm, ii) {
          //console.log(itm);
          var r = _.find($scope.recipies, function (recipe) {
            return recipe.itemId == itm._id;
          });
          if (r)
            itm.recipeId = r._id;
          $scope.stockReturnAcceptForm.selectedUnits[itm._id] = itm.preferedUnits;
          $scope.stockReturnAcceptForm._items.push(itm);
        });
      });
      //console.log($scope.stockReturnAcceptForm.selectedUnits);
      //console.log($scope.stockReturnAcceptForm._items);
    }

    function mapToStores(stockReturnAcceptForm) {

      var ch = stockReturnAcceptForm;
      var stores = [];
      _.forEach($scope.stores, function (store) {
        if (store.isKitchen) {
          var st = {
            _id: store._id,
            storeName: store.storeName,
            category: []
          };
          _.forEach(store.processedFoodCategory, function (cat) {
            var ct = {
              _id: cat._id,
              categoryName: cat.categoryName,
              items: []
            };
            _.forEach(ch._items, function (it) {
              var item = _.find(cat.item, function (itm) {
                return itm._id == it._id;
              });
              if (item)
                ct.items.push(it);
            });
            if (ct.items.length > 0)
              st.category.push(ct);
          });
          stores.push(st);
        }
      });
      return stores;
    }

    $scope.submitAcceptReturn = function (stockReturnAcceptForm, returnAcceptItemsForm) {
      var transactions = [];
      console.log(returnAcceptItemsForm);
      console.log(stockReturnAcceptForm);
      if (returnAcceptItemsForm.$valid) {
        stockReturnAcceptForm.submitting = true;
        var ch = angular.copy(stockReturnAcceptForm);
        ch.isStockReturnAccepted = true;
        var stores = mapToStores(ch);
        _.forEach(stores, function (store) {
          var transaction = new StockTransaction(currentUser, null, null, null, null, null, null, 15, null, null);
          transaction._store = store;
          _.forEach(transaction._store.category, function (cat) {
            _.forEach(cat.items, function (itm) {
              //console.log(itm);
              var reqQty = angular.copy(itm.qty);
              itm.qty = itm.returnAcceptQty;
              //console.log(itm)
              var ss = basePrefferedAnsSelectedUnitsByItemStockReturn(itm, $scope.stockReturnAcceptForm.selectedUnits, 1);
              itm.calculateInUnits = ss;
              itm.qty = reqQty;
              //console.log(itm)
            });
          });
          transaction.transactionId = guid();
          transaction._id = transaction.transactionId;
          transaction.deployment_id = currentUser.deployment_id;
          transaction.tenant_id = currentUser.tenant_id;
          transaction.user = currentUser;
          transaction.isStockReturn = true;
          transaction.isStockReturnAccepted = false;
          transaction.created = $scope.stockReturnForm.billDate;
          var timestamp = Date.parse(transaction.created);
          transaction.timeStamp = timestamp;
          //console.log(transaction);
          $scope.resource.save({}, transaction, function (tr) {

          });
        });
        /*$q.all(transactions, function (){
         console.log('all');
         growl.success("Stock Entry Successful", {ttl: 3000});
         }).catch(function (err) {
         growl.err("Some error occured", {ttl: 3000});
         });*/
        stockReturnAcceptForm.isStockReturnAccepted = true;
        stockReturnAcceptForm.$update(function (){
          growl.success("Stock Entry success!", {ttl: 3000});
          var index = _.findIndex($scope.stockReturnAccept, {_id: ch._id});
          console.log(index);
          if (index >= 0) {
            $scope.stockReturnAccept.splice(index, 1);
          }
          var indexr = _.findIndex($scope.req.return, {_id: ch._id});
          console.log(indexr);
          if (indexr >= 0) {
            $scope.req.return.splice(index, 1);;
          }
          $scope.clearAccept();
          stockReturnAcceptForm.submitting = false;
        });
      }
    }

    $scope.changeWastage = function (item) {
      if (item.returnQty < item.returnAcceptQty) {
        growl.error("Accepted quantity cannot be greater than returned quantity", {ttl: 3000});
        item.returnAcceptQty = "";
        item.returnWastageQty = "";
      }
      else
        item.returnWastageQty = parseFloat(item.returnQty) - parseFloat(item.returnAcceptQty);
    };

    $scope.changeAccept = function (item) {
      if (item.returnQty < item.returnWastageQty) {
        growl.error("Waastage quantity cannot be greater than returned quantity", {ttl: 3000});
        item.returnAcceptQty = "";
        item.returnWastageQty = "";
      }
      else
        item.returnAcceptQty = parseFloat(item.returnQty) - parseFloat(item.returnWastageQty);
    };

    function basePrefferedAnsSelectedUnitsByItemStockReturn(item, selectedUnits, type) {
      var conversionFactor = 1;
      var selectedConversionFactor = 1;
      //console.log(selectedUnits);
      _.forEach(selectedUnits, function (u, ii) {
        //console.log(ii, u);
        if (u != null) {
          //console.log(item.preferedUnits);
          if (ii == item._id) {
            //console.log(u.conversionFactor);
            selectedConversionFactor = u.conversionFactor;
            if (type == 1) {
              var selectedUnit1 = {
                "_id": u._id,
                "unitName": u.unitName,
                "basePrice": parseFloat(item.price),
                "baseQty": parseFloat(item.qty),
                "subTotal": parseFloat(item.subTotal),
                "addedAmt": parseFloat(item.addedAmt),
                "totalAmount": parseFloat(item.totalAmount),
                "type": "selectedUnit",
                "conversionFactor": parseFloat(u.conversionFactor)
              };
              selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
              selectedUnit1.addedAmt = 0;
              if (item.vatPercent != "" && item.vatPercent != undefined) {
                selectedUnit1.addedAmt = parseFloat(parseFloat(selectedUnit1.subTotal) * .01 * parseFloat(item.vatPercent));
              }
              selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
              item["selectedUnit"] = selectedUnit1;
              if (item.preferedUnit == undefined) {
                item["preferedUnits"] = angular.copy(selectedUnit1);
                item.preferedUnits.type = "preferedUnit";
              }
            }
            else if (type == 2) {
              var selectedUnit1 = {
                "_id": u._id,
                "unitName": u.unitName,
                "basePrice": parseFloat(item.price),
                "baseWastagePrice": parseFloat(item.price),
                "baseQty": parseFloat(item.qty),
                "baseWastageQty": parseFloat(item.returnWastageQty),
                "subTotalWastage": parseFloat(item.subTotalWastage),
                "addedAmt": parseFloat(item.addedAmt),
                "addedAmtWastage": parseFloat(item.addedAmtWastage),
                "totalAmount": parseFloat(item.totalAmount),
                "totalWastageAmount": parseFloat(item.totalAmountWastage),
                "type": "selectedUnit",
                "conversionFactor": parseFloat(u.conversionFactor)
              };
              selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
              selectedUnit1.subTotalWastage = parseFloat(item.price) * parseFloat(item.returnWastageQty);
              selectedUnit1.addedAmt = 0;
              selectedUnit1.addedAmtWastage = 0;
              selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
              selectedUnit1.totalWastageAmount = parseFloat(selectedUnit1.subTotalWastage) + parseFloat(selectedUnit1.addedAmtWastage);
              item["selectedUnit"] = selectedUnit1;

              if (item.preferedUnit == undefined) {
                item["preferedUnits"] = angular.copy(selectedUnit1);
                item.preferedUnits.type = "preferedUnit";
              }
            }
          }
        }
      });

      var baseUnit = {};
      if (item.units[0].baseUnit != undefined) {
        baseUnit = angular.copy(item.units[0].baseUnit);
        baseUnit.unitName = baseUnit.name;
      }
      else {
        if (item.baseUnit != undefined) {
          baseUnit = angular.copy(item.baseUnit);
          baseUnit.unitName = baseUnit.name;
        }
        else {
          baseUnit = angular.copy(item.units[0]);
        }
      }

      baseUnit.type = "baseUnit";
      baseUnit.conversionFactor = conversionFactor;

      baseUnit.baseQty = parseFloat(item.qty * selectedConversionFactor);
      baseUnit.basePrice = parseFloat(item.price / selectedConversionFactor);
      baseUnit.baseWastageQty = parseFloat(item.returnWastageQty * selectedConversionFactor);
      baseUnit.baseWastagePrice = parseFloat(item.price / selectedConversionFactor);

      var baseAmt = parseFloat(item.qty) * parseFloat(item.price);
      var baseWastageAmt = parseFloat(item.returnWastageQty) * parseFloat(item.price);
      if (item.vatPercent == "" || item.vatPercent == undefined) {
        baseUnit.totalAmount = baseAmt;
        baseUnit.subTotal = baseAmt;
        baseUnit.addedAmt = 0;

        baseUnit.totalWastageAmount = baseWastageAmt;
        baseUnit.subTotalWastage = baseWastageAmt;
        baseUnit.addedAmtWastage = 0;
      }
      else {
        var vatamt = (baseAmt * .01 * parseFloat(item.vatPercent));
        var vatamtWastage = (baseWastageAmt * .01 * parseFloat(item.vatPercent));
        baseUnit.addedAmt = vatamt;
        baseUnit.subTotal = baseAmt;
        baseUnit.totalAmount = baseAmt + parseFloat(vatamt);

        baseUnit.addedAmtWastage = vatamtWastage;
        baseUnit.subTotalWastage = baseWastageAmt;
        baseUnit.totalWastageAmount = baseWastageAmt + parseFloat(vatamtWastage);
      }

      item.baseUnit = baseUnit;

      var pInd = _.findIndex(item.units, {_id: item.preferedUnit});

      if (pInd >= 0) {
        var preferedUnits = angular.copy(item.units[pInd]);
        var preferedconversionFactor = item.units[pInd].conversionFactor;
        preferedUnits.addedAmt = 0;
        preferedUnits.addedAmtWastage = 0;

        //console.log("selectedConversionFactor" + selectedConversionFactor);
        //console.log("preferedconversionFactor" + preferedconversionFactor);
        if (preferedconversionFactor == selectedConversionFactor) {
          // preferedUnits.baseQty=parseFloat(baseUnit.baseQty);
          // preferedUnits.basePrice=parseFloat(baseUnit.basePrice);
          // preferedUnits.addedAmt=parseFloat(baseUnit.addedAmt);
          // preferedUnits.subTotal=parseFloat(baseUnit.subTotal);
          // preferedUnits.totalAmount=parseFloat(preferedUnits.subTotal)+parseFloat(preferedUnits.addedAmt);
          preferedUnits = angular.copy(item.selectedUnit);
        }
        else if (preferedconversionFactor > selectedConversionFactor) {
          preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
          preferedUnits.baseWastageQty = parseFloat(baseUnit.baseWastageQty) / parseFloat(preferedconversionFactor);
          if (type == 1) {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
            preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) * parseFloat(preferedconversionFactor);
          }
          else {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
            preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice);
            baseUnit.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
            baseUnit.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) / parseFloat(preferedconversionFactor);
            item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) / parseFloat(preferedconversionFactor);
            item.selectedUnit.baseWastagePrice = parseFloat(item.selectedUnit.baseWastagePrice) / parseFloat(preferedconversionFactor);
          }
          preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
          preferedUnits.subTotalWastage = parseFloat(preferedUnits.baseWastageQty) * parseFloat(preferedUnits.baseWastagePrice);
          if (item.vatPercent != "" && item.vatPercent != undefined) {
            preferedUnits.addedAmt = parseFloat(parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
            preferedUnits.addedAmtWastage = parseFloat(parseFloat(preferedUnits.subTotalWastage) * .01 * parseFloat(item.vatPercent));
          }

          preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
          preferedUnits.totalWastageAmount = parseFloat(preferedUnits.subTotalWastage) + parseFloat(preferedUnits.addedAmtWastage);
        }
        else if (preferedconversionFactor < selectedConversionFactor) {
          preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
          preferedUnits.baseWastageQty = parseFloat(baseUnit.baseWastageQty) * parseFloat(preferedconversionFactor);
          if (type == 1) {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
            preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) / parseFloat(preferedconversionFactor);
          }
          else {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
            preferedUnits.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice);
            baseUnit.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
            baseUnit.baseWastagePrice = parseFloat(baseUnit.baseWastagePrice) * parseFloat(preferedconversionFactor);
            item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) * parseFloat(preferedconversionFactor);
            item.selectedUnit.baseWastagePrice = parseFloat(item.selectedUnit.baseWastagePrice) * parseFloat(preferedconversionFactor);
          }
          preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
          preferedUnits.subTotalWastage = parseFloat(preferedUnits.baseWastageQty) * parseFloat(preferedUnits.baseWastagePrice);
          if (item.vatPercent != "" && item.vatPercent != undefined) {
            preferedUnits.addedAmtWastage = (parseFloat(preferedUnits.subTotalWastage) * .01 * parseFloat(item.vatPercent));
          }
          preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
          preferedUnits.totalWastageAmount = parseFloat(preferedUnits.subTotalWastage) + parseFloat(preferedUnits.addedAmtWastage);
        }

        if (type == 2) {
          baseUnit.subTotal = parseFloat(baseUnit.basePrice) * parseFloat(baseUnit.baseQty);
          baseUnit.subTotalWastage = parseFloat(baseUnit.baseWastagePrice) * parseFloat(baseUnit.baseWastageQty);
          baseUnit.addedAmt = 0;
          baseUnit.addedAmtWastage = 0;
          baseUnit.totalAmount = parseFloat(baseUnit.subTotal);
          baseUnit.totalWastageAmount = parseFloat(baseUnit.subTotalWastage);

          item.selectedUnit.subTotal = parseFloat(item.selectedUnit.basePrice) * parseFloat(item.selectedUnit.baseQty);
          item.selectedUnit.subTotalWastage = parseFloat(item.selectedUnit.baseWastagePrice) * parseFloat(item.selectedUnit.baseWastageQty);
          item.selectedUnit.addedAmt = 0;
          item.selectedUnit.addedAmtWastage = 0;
          item.selectedUnit.totalAmount = parseFloat(item.selectedUnit.subTotal);
          item.selectedUnit.totalWastageAmount = parseFloat(item.selectedUnit.subTotalWastage);
        }
        preferedUnits.type = "preferedUnit";
        delete preferedUnits.baseUnit;
        item.preferedUnits = preferedUnits;
      }
      if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
        var cf = 1000;
        if (selectedConversionFactor >= cf) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
          item.baseUnit.baseWastagePrice = parseFloat(item.baseUnit.baseWastagePrice) * parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
          item.baseUnit.baseWastageQty = parseFloat(item.baseUnit.baseWastageQty) / parseFloat(cf);
        }
        else if (cf > selectedConversionFactor) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
          item.baseUnit.baseWastagePrice = parseFloat(item.baseUnit.baseWastagePrice) * parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
          item.baseUnit.baseWastageQty = parseFloat(item.baseUnit.baseWastageQty) / parseFloat(cf);
        }
      }

      item.calculateInUnits = [];
      item.calculateInUnits.push(item.baseUnit);
      item.calculateInUnits.push(item.preferedUnits);
      item.calculateInUnits.push(item.selectedUnit);

      return item.calculateInUnits;
    }
  }]);