'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('stockReturn', {
        url: '/stockReturn',
        templateUrl: 'app/stock/stockReturn/stockReturn.html',
        controller: 'StockReturnCtrl',
        resolve: {
           //modified
                    deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      }) 
      
  });