'use strict';

angular.module('posistApp')
  .controller('StockImportExportCtrl',['$q','$scope','$modal','$timeout','store','Deployment','currentUser','stockCategory','stockUnit','stockItem','vendor','receiver','Category','Item','stockRecipe','$filter','$rootScope','$resource','growl','Pagination','$state','StockResource','localStorageService', function ($q,$scope,$modal,$timeout,store ,Deployment,currentUser,stockCategory,stockUnit,stockItem,vendor,receiver,Category,Item,stockRecipe,$filter,$rootScope,$resource,growl,Pagination,$state,StockResource,localStorageService) {
    $scope.stores=[];
    $scope.stockCategories = [];
    $scope.stockUnits=[];
    $scope.stockItems=[];
    $scope.storesToBeCreated=[];
    $scope.stockCategoriesToBeCreated = [];
    $scope.stockUnitsToBeCreated=[];
    $scope.stockItemsToBeCreated=[];
    $scope.recipeTobeCreated=[];
    $scope.recipeTobeCreated.disable=true;
    $scope.recipeDetailsToBeCreated=[];
    $scope.recipeDetailsToBeCreated.disable=true;
    $scope.errorStore=[];
    $scope.errorCategory=[];
    $scope.errorUnits=[];
    $scope.errorItems=[];
    $scope.deployment={};
    $scope.showProgress=false;
    $scope.totalCount=0;
    $scope.NumberOFRecorsInserted=0;
    currentUser.deployment_id=localStorageService.get('deployment_id');
    $scope.masterImport=true;
    $scope.recipeImport=true;
    $scope.baseUnits=[
      {id:1,name:"Gram",value:1},
      {id:2,name:"Litre",value:0.001},
      {id:3,name:"Meter",value:0.001},
      {id:4,name:"Pc",value:1}
    ];

     function getStores_ByDeployment(){
      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getStockCategories_ByDeployment(){
      return stockCategory.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getStockUnits_ByDeployment(){
      return stockUnit.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getStockItems_ByDeployment(){
      return stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getCategories(){
    	return Category.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };
    function getItems(){
    	return Item.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };
    function getstockRecies(){
    	return stockRecipe.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

   	if(currentUser.deployment_id!=undefined){
   		var allPromise=$q.all([
		        getStores_ByDeployment(), getStockCategories_ByDeployment(),getStockUnits_ByDeployment(), 
		        getStockItems_ByDeployment(),getCategories(),getItems(),getstockRecies()
		    ]);

            allPromise.then(function (value){
             $scope.stores=value[0];
             $scope.stockCategories = value[1];
             $scope.stockUnits=value[2];
             $scope.stockItems=value[3];
             $scope.categorys=value[4];
             $scope.items=value[5];
             $scope.stockRecipes=value[6];
             console.log(value);
           });
   	}


    $scope.csvstore = {
      content: null,
      header: true,
      separator: ',',
      separatorVisible: false,
      result: null,
      accept:".csv" 
    };
    $scope.csvcategory={
      content: null,
      header: true,
      separator: ',',
      separatorVisible: false,
      result: null
    };
    $scope.csvunits={
      content: null,
      header: true,
      separator: ',',
      separatorVisible: false,
      result: null
    };
    $scope.csvitems={
      content: null,
      header: true,
      separator: ',',
      separatorVisible: false,
      result: null
    };
    $scope.csvRecipes={
      content: null,
      header: true,
      separator: ',',
      separatorVisible: false,
      result: null  
    };
    $scope.csvRecipesDetails={
      content: null,
      header: true,
      separator: ',',
      separatorVisible: false,
      result: null    
    };
    function getProgressBarStatus(){
    	$scope.currentProgress=parseInt(parseFloat(parseFloat($scope.NumberOFRecorsInserted)*100)/parseFloat($scope.totalCount));
    };

    $scope.clearImportTab= function(){
        $scope.masterImport=true;
        $scope.recipeImport=true;
        $scope.csvstore = {
          content: null,
          header: true,
          separator: ',',
          separatorVisible: false,
          result: null,
          accept:".csv" 
        };
        $scope.csvcategory={
          content: null,
          header: true,
          separator: ',',
          separatorVisible: false,
          result: null
        };
        $scope.csvunits={
          content: null,
          header: true,
          separator: ',',
          separatorVisible: false,
          result: null
        };
        $scope.csvitems={
          content: null,
          header: true,
          separator: ',',
          separatorVisible: false,
          result: null
        };
        $scope.csvRecipes={
          content: null,
          header: true,
          separator: ',',
          separatorVisible: false,
          result: null  
        };
        $scope.csvRecipesDetails={
          content: null,
          header: true,
          separator: ',',
          separatorVisible: false,
          result: null    
        };
    };
    $scope.clearExportTab= function(){

    };

    $scope.showMasterExport=function(){
        $scope.masterImport=false;
    };
    $scope.showRecipeExport=function(){
        $scope.recipeImport=false;
    };

    function createStore(){
    	var dd=$q.defer();
    	_.forEach($scope.storesToBeCreated,function(s,i){
    		s.deployment_id=currentUser.deployment_id;
    		s.tenant_id=currentUser.tenant_id;
    		s.isKitchen=false;
    		s.isMain=false;
		   	if(s.storeName!=""){
		    	store.saveData({},s, function (result){
		    		$scope.stores.push(result);
		    		$scope.NumberOFRecorsInserted=parseFloat($scope.NumberOFRecorsInserted)+1;
		    		getProgressBarStatus();
			    	console.log(result);
			    	if($scope.storesToBeCreated.length==i){
			    		dd.resolve();
			    	}
			    });
		    }
		    else
		    {
		    	if($scope.storesToBeCreated.length==i){
			    	dd.resolve();
			    }
		    }
		});
    	return dd.promise;
    };
    function createCategory(){
      console.log('in createCategory()');
      console.log('$scope.stockCategoriesToBeCreated',$scope.stockCategoriesToBeCreated);
    	var dd=$q.defer();
    	_.forEach($scope.stockCategoriesToBeCreated,function(s,i){
    		s.deployment_id=currentUser.deployment_id;
    		s.tenant_id=currentUser.tenant_id;
    		var sName=s.storeName;
    		var index=_.findIndex($scope.stores,{storeName:sName});
    		s.store=$scope.stores[index];
        console.log('s',s);
		   	if(s.categoryName!=""){
		    	stockCategory.saveData({},s, function (result){
		    		$scope.NumberOFRecorsInserted=parseFloat($scope.NumberOFRecorsInserted)+1;
		    		getProgressBarStatus();
			    	console.log(result);
			    	if($scope.stores[index].category==undefined){
			    		$scope.stores[index].category=[];
			    	}
			    	$scope.stores[index].category.push(result);
			    	store.update({},$scope.stores[index], function (result){

			    	});
			    	if($scope.stockCategoriesToBeCreated.length==(i+1)){
			    		dd.resolve();
			    	}
			    });
		    }
		    else
		    {
		    	if($scope.stockCategoriesToBeCreated.length==(i+1)){
		    		dd.resolve();
		    	}
		    }
		});
		return dd.promise;
    };
    function createUnits(){
    	var dd=$q.defer();
    	_.forEach($scope.stockUnitsToBeCreated,function(s,i){
    		s.deployment_id=currentUser.deployment_id;
    		s.tenant_id=currentUser.tenant_id;
		   	if(s.unitName!=""){
		   		var index=_.findIndex($scope.baseUnits,{name:s.baseUnit});
		   		s.conversionFactor=parseFloat(s.baseConversionFactor)/parseFloat($scope.baseUnits[index].value);
	    		s.baseUnit=$scope.baseUnits[index];
		    	stockUnit.saveData({},s, function (result){
		    		$scope.NumberOFRecorsInserted=parseFloat($scope.NumberOFRecorsInserted)+1;
		    		getProgressBarStatus();
			    	console.log(result);
			    	if($scope.stockUnitsToBeCreated.length==(i+1)){
			    		dd.resolve();
			    	}
			    });
		    }
		    else
		    {
		    	if($scope.stockUnitsToBeCreated.length==(i+1)){
		    		dd.resolve();
		    	}
		    }
		});
		return dd.promise;
    };

    function createItems(){
    	var dd=$q.defer();
    	_.forEach($scope.stockItemsToBeCreated,function(s,i){
    		s.deployment_id=currentUser.deployment_id;
    		s.tenant_id=currentUser.tenant_id;
		   	if(s.itemName!=""){
		    	stockItem.saveData({},s, function (result){
		    		$scope.NumberOFRecorsInserted=parseFloat($scope.NumberOFRecorsInserted)+1;
		    		getProgressBarStatus();
			    	console.log(result);
			    	if($scope.stockItemsToBeCreated.length==(i+1)){
			    		dd.resolve();
			    	}
			    });
		    }
		    else
		    {
		    	if($scope.stockItemsToBeCreated.length==(i+1)){
		    		dd.resolve();
		    	}
		    }
		});
		return dd.promise;
    };
    $scope.onFinishWizardRecipe=function(){
        console.log("Recipe Import Wizard Finished");
        $scope.recipeDetailsToBeCreated.disable=true;
        if($scope.errorItemRecipes.length!=0 || $scope.errorItemRecipesDetails.length!=0){
            growl.error('Could Not Import before All previous error are resolved',{ttl:3000});
        }
        else
        {
            var data=[];
            _.forEach($scope.recipeTobeCreated,function(r,i){
                if(r.categoryName!="" && r.recipeName!="" && r.quantity!="" && r.unitName!=""){
                    var name=r.recipeName.toLowerCase();
                    var mItemIndex=_.findIndex($scope.items,{itemNameInLowercase:name});
                    if(mItemIndex>=0){
                        r.deployment_id=currentUser.deployment_id;
                        r.tenant_id=currentUser.tenant_id;    
                        r.itemId=$scope.items[mItemIndex]._id;
                        r.categoryId=$scope.items[mItemIndex].category._id;
                        var uname=r.unitName.toLowerCase();
                        var ind=_.findIndex($scope.stockUnits,{unitNameInLowerCase:uname});
                        r.selectedUnitId=$scope.stockUnits[ind];
                        r.unit=$scope.stockUnits[ind];
                        r.menuQty=parseFloat(parseFloat($scope.items[mItemIndex].stockQuantity)*parseFloat(r.unit.conversionFactor)).toFixed(3);
                        r.recipeQty=parseFloat(parseFloat(r.quantity)*parseFloat(r.unit.conversionFactor)).toFixed(3);                        
                        r.unitName=$scope.stockUnits[ind].unitName;
                        r.units=getAllUNitsAcToBaseUnit($scope.stockUnits[ind].baseUnit.name);
                        r.isSemiProcessed=$scope.items[mItemIndex].category.isSemiProcessed;
                        data.push(r);
                    }
                }
            });
            data.sort(function(a,b){return b.isSemiProcessed-a.isSemiProcessed});
            async.eachSeries(data,function(r,callback){
                var name=r.recipeName.toLowerCase();
                r.receipeDetails=getRecipeDetails(name);
                r.rawItems=getRawItems(name);
                console.log(r);
                if(r.isSemiProcessed==true){
                    r.recipeNameInLowerCase=r.recipeName.toLowerCase();
                    $scope.stockRecipes.push(r);
                }
                stockRecipe.saveData({},r, function(result){
                    if(result.isSemiProcessed==true){
                        var index=_.findIndex($scope.stockRecipes,{itemId:r.itemId});    
                        $scope.stockRecipes[index]._id=result._id;
                        //$scope.stockRecipes.push(result);
                    }
                    callback();
                });
            },function (err) {
                if (err) {
                    callback(err);
                } else {
                    //$rootScope.$state.go('stock.indentGrn');
                    $state.go('stock.recipeManagement');
                    console.log('Done');
                }
            });
        }
    };

    $scope.onFinishWizard=function(){
    	console.log("Import Wizard Finished");
    	if($scope.errorStore.length!=0 || $scope.errorCategory.length!=0 || $scope.errorUnits.length!=0 || $scope.errorItems.length!=0){
    		growl.error('Could Not Import before All previous error are resolved', {ttl:3000});
    	}
    	else
    	{
        console.log('in here');
    		$scope.totalCount=parseFloat(parseFloat($scope.storesToBeCreated.length)+parseFloat($scope.stockCategoriesToBeCreated.length)+parseFloat($scope.stockUnitsToBeCreated.length)+parseFloat($scope.stockItemsToBeCreated.length));
    		console.log('$scope.totalCount', $scope.totalCount);
        growl.success('Please wait Import Started', {ttl:3000});
    		$scope.showProgress=true;
    		var isStoreCreated=false;
    		var isCategoryCreated=false;
    		var isUnitCreated=false;


    		 async.eachSeries($scope.storesToBeCreated, function (item, callback){
    		 	if(item.storeName!=""){
	            		item.deployment_id=currentUser.deployment_id;
			    		item.tenant_id=currentUser.tenant_id;
			    		item.isKitchen=false;
			    		item.isMain=false;
		            	store.saveData({},item, function (result){
				    		$scope.stores.push(result);
				    		$scope.NumberOFRecorsInserted=parseFloat($scope.NumberOFRecorsInserted)+1;
				    		getProgressBarStatus();
					    	console.log(result);
					    	 callback();
					    });
	            }
    		 },function (err) {
	          	if (err) {
	            	callback(err);
	          	} else {
                console.log('else createCategory()')
	            	createCategory();
	          	}
	        });		
	        //});
    		 // createStore().then(function(){
    		 // 	createCategory().then(function(){
    		 // 		createUnits().then(function(){
    		 // 			createItems.then(function(){

    		 // 			});
    		 // 		});
    		 // 	});
    		 // });
    		createUnits();
    		createItems();
    	}
    };
    function checkStore(data){
        var flag=true;
        var length=_.keys(data[0]).length;
        if(length!=1){
            flag=false;
        }
        return flag;    
    };
    function checkCategory(data){
        var flag=true;
        var length=_.keys(data[0]).length;
        if(length!=3){
            flag=false;
        }
        return flag;    
    };
    function checkUnit(data){
        var flag=true;
        var length=_.keys(data[0]).length;
        if(length!=3){
            flag=false;
        }
        return flag;    
    };
    function checkItem(data){
        var flag=true;
        var length=_.keys(data[0]).length;
        if(length!=1){
            flag=false;
        }
        return flag;    
    };
    $scope.onFinishImport_Store = function (data) {
    	//console.log(data)
        $scope.storesToBeCreated=[];
    	$scope.errorStore=[];
    	if($scope.csvstore.separator!=","){
    		$scope.errorStore=[{error:"Please don't change seperator. It must be ,"}];
    	}
    	else
    	{
    		if(data.length>0){
                var chkDoc=checkStore(data);
                if(chkDoc==true){
    	    		checkDublicateStore(data).then(function(strs){
    		    		if(strs.length<=0){
    		    			//get all store for Create
    		    			console.log("Store Ok");
    		    			_.forEach(data,function(d,i){
    		    				if(d.storeName!=""){
    		    					$scope.storesToBeCreated.push(d);
    		    				}
    		    			});
    		    		}
    		    		else
    		    		{
    		    			//error comes stop working
    		    			$scope.errorStore=strs;
    		    			console.log(strs);
    		    			$scope.csvstore = {
    					      content: null,
    					      header: true,
    					      separator: ',',
    					      separatorVisible: false,
    					      result: null
    					    };
    					    console.log($scope.csvstore);
    		    		}
    		    	});	
                }
                else{
                    $scope.errorStore=[{error:"Wrong file provided please contact support to get format of store in csv"}];
                }
	    	}
	    	else
	    	{
	    		$scope.errorStore=[{error:"Documents Does Not contain any row"}];
	    	}	
    	}
    };
    $scope.onFinishImport_Category=function(data){
    	console.log('data', data);
        $scope.stockCategoriesToBeCreated=[];
    	$scope.errorCategory=[];
    	if($scope.csvcategory.separator!=","){  
    		$scope.errorCategory=[{error:"Please don't change seperator. It must be ,"}];
    	}
    	else
    	{
    		if(data.length>0){
                var chkDoc=checkCategory(data);
                console.log('chkDoc',chkDoc);
                if(chkDoc==true){
    	    		checkDublicateCategory(data).then(function(strs){
                console.log('strs',strs);
    		    		if(strs.length<=0){
    		    			console.log("Category Ok");
    		    			_.forEach(data,function(d,i){
    		    				if(d.categoryName!="" || d.storeName!=""){
    		    					$scope.stockCategoriesToBeCreated.push(d);
    		    				}
    		    			});
    		    		}
    		    		else
    		    		{
    		    			//error comes stop working
    		    			$scope.errorCategory=strs;
    		    			console.log(strs);
    		    			$scope.csvcategory = {
    					      content: null,
    					      header: true,
    					      separator: ',',
    					      separatorVisible: false,
    					      result: null
    					    };
    					    console.log($scope.csvcategory);
    		    		}
    		    	});	
                }
                else
                {
                  console.log('Wrong file provided please contact support to get format of Category in csv')
                    $scope.errorCategory=[{error:"Wrong file provided please contact support to get format of Category in csv"}];       
                }
	    	}
	    	else
	    	{
	    		$scope.errorCategory=[{error:"Documents Does Not contain any row"}];
	    	}	
    	}
    };
    $scope.onFinishImport_Units=function(data){
    	//console.log(data);
        $scope.stockUnitsToBeCreated=[];
    	$scope.errorUnits=[];
    	if($scope.csvunits.separator!=","){
    		$scope.errorCategory=[{error:"Please don't change seperator. It must be ,"}];
    	}
    	else
    	{
    		if(data.length>0){
                var chkDoc=checkUnit(data);
                if(chkDoc==true){
        			checkDublicateUnits(data).then(function(strs){
    		    		if(strs.length<=0){
    		    			//get all Units for Create
    		    			console.log("Units Ok");
    		    			_.forEach(data,function(d,i){
    		    				if(d.unitName!="" || d.baseUnit!="" || d.baseConversionFactor!=""){
    		    					$scope.stockUnitsToBeCreated.push(d);
    		    				}
    		    			});
    		    		}
    		    		else
    		    		{
    		    			//error comes stop working
    		    			console.log(strs);
    		    			$scope.errorUnits=strs;
    		    		}
    		    	});
                }
                else{
                    $scope.errorUnits=[{error:"Wrong file provided please contact support to get format of Unit in csv"}];
                }
    		}
    		else
    		{
    			$scope.errorUnits=[{error:"Documents Does Not contain any row"}];
    		}
    	}
    };
    $scope.onFinishImport_Items=function(data){
    	//console.log(data);
        $scope.stockItemsToBeCreated=[];
    	$scope.errorItems=[];
    	if($scope.csvitems.separator!=","){
    		$scope.errorItems=[{error:"Please don't change seperator. It must be ,"}];
    	}
    	else
    	{
    		if(data.length>0){
                var chkDoc=checkItem(data);
                if(chkDoc==true){
        			checkDublicateItems(data).then(function(strs){
    		    		if(strs.length<=0){
    		    			//get all Items for Create
    		    			console.log("Items Ok");
    		    			_.forEach(data,function(d,i){
    		    				if(d.itemName!=""){
    		    					$scope.stockItemsToBeCreated.push(d);
    		    				}
    		    			});
    		    		}
    		    		else
    		    		{
    		    			//error comes stop working
    		    			console.log(strs);
    		    			$scope.errorItems=strs;
    		    		}
    		    	});
                }
                else{
                    $scope.errorItems=[{error:"Wrong file provided please contact support to get format of Stock Item in csv"}];
                }
    		}
    		else
    		{
    			$scope.errorItems=[{error:"Documents Does Not contain any row"}];
    		}
    	}
    };
    $scope.onFinishImport_RecipesDetails=function(data){
        $scope.recipeDetailsToBeCreated.disable=true;
        $scope.recipeDetailsToBeCreated=[];
        $scope.errorItemRecipesDetails=[];
        if($scope.csvRecipesDetails.separator!=","){
            $scope.errorItemRecipesDetails=[{error:"Please don't change seperator. It must be ,"}];
        }
        else
        {
            if(data.length>0){
                validateMenuItemNamePresent(data,'recipeDetails').then(function(strs){
                    if(strs.length<=0){
                        checkQunatityAndUnitPresent(data).then(function(chkUnit){
                            if(chkUnit.length<=0){
                                checkStockItemPresentForRecipeDetails(data).then(function(chkItem){
                                    if(chkItem.length<=0){
                                        //get all Items for Create
                                        _.forEach(data,function(d,i){
                                            if(d.itemName!="" || d.recipeName!="" || d.quantity!="" || d.unitName!=""){
                                                $scope.recipeDetailsToBeCreated.push(d);
                                            }
                                        });
                                        $scope.recipeDetailsToBeCreated.disable=false;
                                    }
                                    else
                                    {
                                        $scope.errorItemRecipesDetails=chkItem;
                                    }
                                });
                            }
                            else
                            {
                                $scope.errorItemRecipesDetails=chkUnit;
                            }
                        });
                    }
                    else
                    {
                        //error comes stop working
                        console.log(strs);
                        $scope.errorItemRecipesDetails=strs;
                    }
                });
            }
            else
            {
                $scope.errorItems=[{error:"Documents Does Not contain any row"}];
            }
        }
    };
    $scope.onFinishImport_Recipes=function(data){
        $scope.recipeTobeCreated.disable=true;
        $scope.recipeTobeCreated=[];
        $scope.errorItemRecipes=[];
        if($scope.csvRecipes.separator!=","){
            $scope.errorItemRecipes=[{error:"Please don't change seperator. It must be ,"}];
        }
        else
        {
            if(data.length>0){
                validateMenuCategory(data).then(function(strs){
                    if(strs.length<=0){
                        validateMenuItemNamePresent(data,'recipes').then(function(valMenuItem){
                            if(valMenuItem.length<=0){
                                checkRecipePresent(data).then(function(chkRec){
                                    if(chkRec.length<=0){
                                        checkQunatityAndUnitPresent(data).then(function(chkUnit){
                                            if(chkUnit.length<=0){
                                                //get all Items for Create
                                                _.forEach(data,function(d,i){
                                                    if(d.categoryName!="" || d.recipeName!="" || d.quantity!="" || d.unitName!=""){
                                                        $scope.recipeTobeCreated.push(d);
                                                    }
                                                });
                                                $scope.recipeTobeCreated.disable=false;
                                            }
                                            else
                                            {
                                                $scope.errorItemRecipes=chkUnit;
                                            }
                                        });
                                    }
                                    else
                                    {
                                        $scope.errorItemRecipes=chkRec;
                                    }
                                });
                            }
                            else
                            {
                                $scope.errorItemRecipes=valMenuItem;
                            }
                        });
                    }
                    else
                    {
                        //error comes stop working
                        console.log(strs);
                        $scope.errorItemRecipes=strs;
                    }
                });
            }
            else
            {
                $scope.errorItems=[{error:"Documents Does Not contain any row"}];
            }
        }
    };
    function getAllUNitsAcToBaseUnit(name){
      var uu=[];
      _.forEach($scope.stockUnits, function(u,i){
        if(u.baseUnit.name==name){
          uu.push(u);
        }
      });
      return uu;
    };
    function getAllStoreNames(){
    	var str=[];
    	_.forEach($scope.stores,function(s,i){
    		var ob={storeNameInLowerCase:s.storeName.toLowerCase(),storeName:s.storeName,lineNumber:"Already exists."};
    		str.push(ob);
    	});
    	return str;
    };
    function checkDublicateStore(data){
    	var dd=$q.defer();
    	var str=getAllStoreNames();
    	var r=false;
    	var lineNumbers=[];
    	try{
    		if(data[0].storeName==undefined){
    			lineNumbers.push({error:"Wrong file provided please contact support to get format of store in csv"});
    		}
    		else
    		{
    			_.forEach(data,function(d,i){
	    			if(d.storeName!=undefined){
	    				var name=d.storeName.toLowerCase();
			    		var index=_.findIndex(str,{storeNameInLowerCase:name});
			    		if(index<0){
			    			var ss=d;
			    			ss.storeNameInLowerCase=d.storeName.toLowerCase();
			    			ss.lineNumber=i;
			    			str.push(ss);
			    		}
			    		else{
			    			r=true;
			    			lineNumbers.push({error:"Duplicate Store Name : ( "+ d.storeName +" ) At Line Number : "+(i+2)+" with Line Number : "+str[index].lineNumber});
			    		}
	    			}
	    			else
	    			{
	    				lineNumbers.push({error:"Wrong file provided please contact support to get format of store csv"});
	    			}
		    	});	
    		}
    		$scope.AllStoreToCheck=str;
	    	console.log(lineNumbers);
	    	dd.resolve(lineNumbers);
    	}catch(e){
    		dd.reject(e);
    	}    	
    	return dd.promise;
    };
    function getAllCategoryNames(){
    	var str=[];
    	_.forEach($scope.stockCategories,function(s,i){
    		var ob={categoryNameInLowerCase:s.categoryName.toLowerCase(),lineNumber:"Already exists."};
    		str.push(ob);
    	});
    	return str;
    };
    function getAllMenuCategoryName(){
        var str=[];
        _.forEach($scope.categorys,function(s,i){
            var ob={categoryName:s.categoryName.toLowerCase(),lineNumber:"Already exists."};
            str.push(ob);
        });
        return str;
    };
    function getAllMenuItemsName(){
        var strs=[];
        _.forEach($scope.items,function(itm,i){
            itm.itemName=itm.name;
            itm.itemNameInLowercase=itm.name.toLowerCase();
            var ob={itemName:itm.itemName,itemNameInLowercase:itm.name.toLowerCase(),categoryName:itm.category.categoryName.toLowerCase(),stockQuantity:itm.stockQuantity,unit:itm.unit,lineNumber:"Already exists"};
            strs.push(ob);
        });
        return strs;
    };
    function checkIsCategoryAssignedToStore(categoryName,storeName){
    	var flag=false;
    	if($scope.AllStoreToCheck==undefined){
    		$scope.AllStoreToCheck=$scope.stores;
    		_.forEach($scope.AllStoreToCheck,function(ac,i){
    			var aa=ac.storeName.toLowerCase();
    			if(aa==storeName){
    				if(aa.category!=undefined){
    					_.forEach(aa.category,function(c,ii){
    						var cName=c.categoryName.toLowerCase();
    						if(cName=categoryName){
    							flag=true;
    						}
    					});
    				}
    			}
    		});
    	}
    	else
    	{
    		_.forEach($scope.AllStoreToCheck,function(a,i){
	    		var na=a.storeName.toLowerCase();
	    		if(storeName==na){
	    			if(a.category!=undefined){
		    			_.forEach(a.category,function(c,ii){
	    					var cName=c.categoryName.toLowerCase();
	    					if(cName=categoryName){
	    						flag=true;
	    					}
	    				});    		
		    		}	
	    		}    		
	    	});
    	}
    	return flag;
    };
    function isStorePresent(storeName){
    	var flag=false;
    	if($scope.AllStoreToCheck==undefined){
    		$scope.AllStoreToCheck=$scope.stores;
    		_.forEach($scope.AllStoreToCheck,function(ac,i){
    			var aa=ac.storeName.toLowerCase();
    			if(aa==storeName){
    				flag=true;
    			}
    		});
    	}
    	else
    	{
    		_.forEach($scope.AllStoreToCheck,function(a,i){
	    		var na=a.storeName.toLowerCase();
	    		if(storeName==na){
	    			flag=true;	
	    		}    		
	    	});
    	}
    	return flag;
    };
    function getStoreName(storeName){
        var flag=storeName;
        if($scope.AllStoreToCheck==undefined){
            $scope.AllStoreToCheck=$scope.stores;
            _.forEach($scope.AllStoreToCheck,function(ac,i){
                var aa=ac.storeName.toLowerCase();
                if(aa==storeName){
                    flag=ac.storeName;
                }
            });
        }
        else
        {
            _.forEach($scope.AllStoreToCheck,function(a,i){
                var na=a.storeName.toLowerCase();
                if(storeName==na){
                    flag=a.storeName;
                }           
            });
        }
        return flag;
    };
    function checkDublicateCategory(data){
    	var dd=$q.defer();
    	var str=getAllCategoryNames();
    	var r=false;
    	var lineNumbers=[];
    	try{
    		if(data[0].categoryName==undefined || data[0].storeName==undefined){
    			lineNumbers.push({error:"Wrong file provided please contact support to get format of category in csv"});
    		}
    		else
    		{
    			_.forEach(data,function(d,i){
	    			var name=d.categoryName.toLowerCase();
		    		var index=_.findIndex(str,{categoryNameInLowerCase:name});
		    		var sName=d.storeName.toLowerCase();
		    		var chk=checkIsCategoryAssignedToStore(name,sName);
		    		var chkStorePresent=isStorePresent(sName);
		    		if(chkStorePresent==true){
                        d.storeName=getStoreName(sName);
		    			if(chk==false){
			    			if(index<0){
				    			var ss=d;
				    			ss.lineNumber=i;
				    			str.push(ss);
				    		}
				    		else{
				    			r=true;
				    			lineNumbers.push({error:"Duplicate Category Name : ( "+ d.categoryName +" ) At Line Number : "+(i+2)+" with Line Number : "+str[index].lineNumber});
				    		}	
			    		}
			    		else
			    		{		    			
				    		r=true;
				    		lineNumbers.push({error:"Duplicate Category Name : ( "+ d.categoryName +" ) At Line Number : "+(i+2)+" with Line Number : "+str[index].lineNumber});	
			    		}
		    		}
		    		else
		    		{
						lineNumbers.push({error:"Store Name : ( "+ d.storeName +" ) At Line Number : "+(i+2)+" does not present in db as well as in store csv. Firstly create one"});			    			
		    		}
		    	});
    		}
	    	console.log(lineNumbers);
	    	dd.resolve(lineNumbers);
    	}catch(e){
    		dd.reject(e);
    	}    	
    	return dd.promise;
    };

    function getAllUnits(){
    	var str=[];
    	_.forEach($scope.stockUnits,function(s,i){
            s.unitNameInLowerCase=s.unitName.toLowerCase();
    		var ob={unitName:s.unitName,unitNameInLowerCase:s.unitNameInLowerCase,lineNumber:"Already exists."};
    		str.push(ob);
    	});
    	return str;
    };

    function getAllStockItemsWithSemiProcessed(){
        var strs=[];
        _.forEach($scope.stockItems,function(it,i){
            it.itemNameInLowercase=it.itemName.toLowerCase();
            var ob={itemName:it.itemName,itemNameInLowercase:it.itemNameInLowercase,units:it.units,lineNumber:"Already exists."};
            strs.push(ob);
        });
        _.forEach($scope.items,function(it,i){
            if(it.category.isSemiProcessed!=undefined){
                if(it.category.isSemiProcessed==true){
                    var units=getAllUNitsAcToBaseUnit(it.unit.baseUnit.name);
                    var ob={itemName:it.name,itemNameInLowercase:it.name.toLowerCase(),units:units,lineNumber:"Already exists."};
                    strs.push(ob);
                }
            }
        });
        return strs;
    };

    function validateBaseUnit(name){
    	var index=_.findIndex($scope.baseUnits,{name:name});
    	if(index<0){
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    };

    function getBaseUnitName(name){
        var index=_.findIndex($scope.baseUnits,{name:name});
        if(index<0){
            return name;
        }
        else
        {
            return $scope.baseUnits[index].name;
        }
    };

    function checkDublicateUnits(data){
    	var dd=$q.defer();
    	var str=getAllUnits();
    	var r=false;
    	var lineNumbers=[];
    	try{
    		if(data[0].unitName==undefined || data[0].baseUnit==undefined || data[0].baseConversionFactor==undefined){
    			lineNumbers.push({error:"Wrong file provided please contact support to get format of units in csv"});
    		}
    		else
    		{
	    		_.forEach(data,function(d,i){
	    			var name=d.unitName.toLowerCase();
	    			var chkBaseUnit=validateBaseUnit(d.baseUnit);
	    			if(chkBaseUnit==false){
	    				lineNumbers.push({error:"Wrong baseUnit  : ( "+ d.baseUnit +" ) At Line Number : "+(i+2)});
	    			}
	    			if(isNaN(d.baseConversionFactor)){
	    				lineNumbers.push({error:"Wrong Conversion Factor : ( "+ d.baseConversionFactor +" ) At Line Number : "+(i+2)});
	    			}
		    		var index=_.findIndex(str,{unitNameInLowerCase:name});
		    		if(index<0){
		    			var ss=d;
		    			ss.unitName=d.unitName;
                        ss.unitNameInLowerCase=d.unitName.toLowerCase();
		    			ss.lineNumber=i;
		    			str.push(ss);
		    		}
		    		else{
		    			r=true;
		    			lineNumbers.push({error:"Duplicate Unit Name : ( "+ d.unitName +" ) At Line Number : "+(i+2)+" with Line Number : "+str[index].lineNumber});
		    		}
		    	});    			
    		}
	    	console.log(lineNumbers);
	    	dd.resolve(lineNumbers);
    	}catch(e){
    		dd.reject(e);
    	}    	
    	return dd.promise;
    };
    function getAllItemNames(){
    	var str=[];
    	_.forEach($scope.stockItems,function(s,i){
    		var ob={itemName:s.itemName,itemNameInLowercase:s.itemName.toLowerCase(),lineNumber:"Already exists."};
    		str.push(ob);
    	});
    	return str;
    };

    function getAllRecipesName(){
        var strs=[];
        _.forEach($scope.stockRecipes,function(r,i){
            r.recipeNameInLowerCase=r.recipeName.toLowerCase();
            var ob={recipeName:r.recipeName.toLowerCase(),lineNumber:"Already exists."};
            strs.push(ob);
        });
        return strs;
    };

    function checkDublicateItems(data){
    	var dd=$q.defer();
    	var str=getAllItemNames();
    	var r=false;
    	var lineNumbers=[];
    	try{
    		if(data[0].itemName==undefined){
    			lineNumbers.push({error:"Wrong file provided please contact support to get format of Stock Items in csv"});
    		}
    		else
    		{
    			_.forEach(data,function(d,i){
	    			var name=d.itemName.toLowerCase();
		    		var index=_.findIndex(str,{itemName:name});
		    		if(index<0){
		    			var ss=d;
		    			ss.itemName=d.itemName;
                        ss.itemNameInLowercase=d.itemName.toLowerCase();
		    			ss.lineNumber=i;
		    			str.push(ss);
		    		}
		    		else{
		    			r=true;
		    			lineNumbers.push({error:"Duplicate Item Name : ( "+ d.itemName +" ) At Line Number : "+(i+2)+" with Line Number : "+str[index].lineNumber});
		    		}
		    	});
    		}
	    	console.log(lineNumbers);
	    	dd.resolve(lineNumbers);
    	}catch(e){
    		dd.reject(e);
    	}    	
    	return dd.promise;
    };

    function validateMenuCategory(data){
        var dd=$q.defer();
        var str=getAllMenuCategoryName();
        var r=false;
        var lineNumbers=[];
        try{
            if(data[0].categoryName==undefined){
                lineNumbers.push({error:"Wrong file provided please contact support to get format of Recipe in csv"});
            }
            else
            {
                _.forEach(data,function(d,i){
                    var name=d.categoryName.toLowerCase();
                    var index=_.findIndex(str,{categoryName:name});
                    if(index<0){
                        lineNumbers.push({error:"Menu Category Name : ( "+ d.categoryName +" ) At Line Number : "+(i+2)+" not present please create first."});
                    }
                });
            }
            console.log(lineNumbers);
            dd.resolve(lineNumbers);
        }catch(e){
            dd.reject(e);
        }       
        return dd.promise;
    };

    function validateMenuItemNamePresent(data,type){
        var dd=$q.defer();
        var str=getAllMenuItemsName();
        var r=false;
        var lineNumbers=[];
        try{
            if(data[0].recipeName==undefined){
                lineNumbers.push({error:"Wrong file provided please contact support to get format of Recipe in csv"});
            }
            else
            {
                _.forEach(data,function(d,i){
                    var name=d.recipeName.toLowerCase();
                    var index=_.findIndex(str,{itemNameInLowercase:name});
                    if(index<0){
                        lineNumbers.push({error:"Menu Item Name : ( "+ d.recipeName +" ) At Line Number : "+(i+2)+" not present please create first."});
                    }
                    else
                    {
                        if(str[index].stockQuantity==undefined|| str[index].stockQuantity==null || str[index].unit==undefined){
                            lineNumbers.push({error:"Menu Item Name : ( "+ d.recipeName +" ) At Line Number : "+(i+2)+" Does not defined stockQuantity or unit"});
                        }
                        d.recipeName=str[index].itemName;
                    }
                    if(type!="recipeDetails" && index>=0){
                        var cname=d.categoryName.toLowerCase();
                        if(str[index].categoryName!=cname){
                            lineNumbers.push({error:"Wrong Menu Category Name : ( "+ str[index].categoryName +" ) At Line Number : "+(i+2)+" provided for Item Name : ( "+d.recipeName+" )."});
                        }
                    }

                });
            }
            dd.resolve(lineNumbers);
        }catch(e){
            dd.reject(e);
        }       
        return dd.promise;
    };

    function checkRecipePresent(data){
        var dd=$q.defer();
        var str=getAllRecipesName();
        var r=false;
        var lineNumbers=[];
        try{
            if(data[0].recipeName==undefined){
                lineNumbers.push({error:"Wrong file provided please contact support to get format of Recipe in csv"});
            }
            else
            {
                _.forEach(data,function(d,i){
                    var name=d.recipeName.toLowerCase();
                    var index=_.findIndex(str,{recipeNameInLowerCase:name});
                    if(index<0){
                        var ss=d;
                        ss.recipeName=d.recipeName;
                        ss.recipeNameInLowerCase=d.recipeName.toLowerCase();
                        ss.lineNumber=i+2;
                        str.push(ss);
                    }
                    else{
                        lineNumbers.push({error:"Duplicate Item Name : ( "+ d.recipeName +" ) At Line Number : "+(i+2)+" with Line Number : "+str[index].lineNumber});
                    }
                });
            }
            dd.resolve(lineNumbers);
        }catch(e){
            dd.reject(e);
        }       
        return dd.promise;
    };

    function checkQunatityAndUnitPresent(data){
        var dd=$q.defer();
        var str=getAllUnits();
        var pitems=getAllStockItemsWithSemiProcessed();
        var r=false;
        var lineNumbers=[];
        try{
            if(data[0].unitName==undefined || data[0].quantity==undefined){
                lineNumbers.push({error:"Wrong file provided please contact support to get format of Recipe in csv"});
            }
            else
            {
                _.forEach(data,function(d,i){
                    var name=d.unitName.toLowerCase();
                    var index=_.findIndex(str,{unitNameInLowerCase:name});
                    if(index<0){
                        lineNumbers.push({error:"Unit Name : ( "+ d.unitName +" ) At Line Number : "+(i+2)+" not present create first. "});
                    }
                    if(isNaN(d.quantity)){
                        lineNumbers.push({error:"quantity  : ( "+ d.quantity +" ) At Line Number : "+(i+2)+" is not a valid number please provide a valid number. "});   
                    }
                });
            }
            dd.resolve(lineNumbers);
        }catch(e){
            dd.reject(e);
        }       
        return dd.promise;
    };

    function checkStockItemPresentForRecipeDetails(data){
        var dd=$q.defer();
        var str=getAllStockItemsWithSemiProcessed();
        var r=false;
        var lineNumbers=[];
        try{
            if(data[0].itemName==undefined){
                lineNumbers.push({error:"Wrong file provided please contact support to get format of Recipe in csv"});
            }
            else
            {
                _.forEach(data,function(d,i){
                    var name=d.itemName.toLowerCase();
                    var index=_.findIndex(str,{itemNameInLowercase:name});
                    if(index<0){
                        lineNumbers.push({error:"Item Name: ( "+ d.itemName +" ) At Line Number : "+(i+2)+" not present create first. "});
                    }
                    else
                    {
                        var chkUnit=false;
                        var uname=d.unitName.toLowerCase();
                        _.forEach(str[index].units,function(u,i){
                            if(u.unitName.toLowerCase()==uname){
                                chkUnit=true;
                            }
                        });
                        if(chkUnit==false){
                            lineNumbers.push({error:"Wrong Unit Provided  : ( "+ d.unitName +" ) for Item ( "+d.itemName+" ) At Line Number : "+(i+2)+" ."});          
                        }
                    }
                });
            }
            dd.resolve(lineNumbers);
        }catch(e){
            dd.reject(e);
        }       
        return dd.promise;
    };
    function getRawItems(recipeName) {
      var details=[];
      _.forEachRight($scope.recipeDetailsToBeCreated,function(r,i){
        var name=r.recipeName.toLowerCase();
        var iname=r.itemName.toLowerCase();
        var uname=r.unitName.toLowerCase();
        if(name==recipeName){
            var ind=_.findIndex($scope.stockItems,{itemNameInLowercase:iname});
            if(ind>=0){
                var uIndex=_.findIndex($scope.stockUnits,{unitNameInLowerCase:uname});
                $scope.stockItems[ind].selectedUnitId=$scope.stockUnits[uIndex];
                $scope.stockItems[ind].selectedUnitName=$scope.stockUnits[uIndex].unitName;
                $scope.stockItems[ind].quantity=parseFloat(r.quantity).toFixed(3);
                $scope.stockItems[ind].baseQuantity=parseFloat(r.quantity)*parseFloat($scope.stockUnits[uIndex].conversionFactor);
                // if($scope.stockUnits[uIndex].baseUnit.id==2 || $scope.stockUnits[uIndex].baseUnit.id==3){
                //   $scope.stockItems[ind].baseQuantity=parseFloat(items.baseQuantity)/1000;
                // }
                var indDetails=_.findIndex(details,{_id:$scope.stockItems[ind]._id});
                if(indDetails>=0){
                    details[indDetails].baseQuantity=parseFloat(details[indDetails].baseQuantity)+parseFloat($scope.stockItems[ind].baseQuantity);   
                }
                else
                {
                    details.push(angular.copy($scope.stockItems[ind]));
                }
            }
            else
            {
                //find in semi processed
                var sIndex=_.findIndex($scope.stockRecipes,{recipeNameInLowerCase:iname});
                if(sIndex>=0){
                    _.forEach($scope.stockRecipes[sIndex].rawItems,function(itm,i){
                        var indDetails=_.findIndex(details,{_id:itm._id});
                        if(indDetails>=0){
                            details[indDetails].baseQuantity=parseFloat(details[indDetails].baseQuantity)+parseFloat(itm.baseQuantity);
                        }
                        else
                        {
                            details.push(itm);
                        }
                    });
                    //details.push($scope.stockRecipes[sIndex]);
                }
            }
        }
      });  
      return details;
    };
    function getRecipeDetails(recipeName) {
      var details=[];
      _.forEachRight($scope.recipeDetailsToBeCreated,function(r,i){
        var name=r.recipeName.toLowerCase();
        var iname=r.itemName.toLowerCase();
        var uname=r.unitName.toLowerCase();
        if(name==recipeName){
            var ind=_.findIndex($scope.stockItems,{itemNameInLowercase:iname});
            if(ind>=0){
                var uIndex=_.findIndex($scope.stockUnits,{unitNameInLowerCase:uname});
                $scope.stockItems[ind].selectedUnitId=$scope.stockUnits[uIndex];
                $scope.stockItems[ind].selectedUnitName=$scope.stockUnits[uIndex].unitName;
                $scope.stockItems[ind].quantity=parseFloat(r.quantity).toFixed(3);
                $scope.stockItems[ind].baseQuantity=parseFloat(r.quantity)*parseFloat($scope.stockUnits[uIndex].conversionFactor);
                // if($scope.stockUnits[uIndex].baseUnit.id==2 || $scope.stockUnits[uIndex].baseUnit.id==3){
                //   $scope.stockItems[ind].baseQuantity=parseFloat(items.baseQuantity)/1000;
                // }
                details.push($scope.stockItems[ind]);
            }
            else
            {
                //find in semi processed
                var sIndex=_.findIndex($scope.stockRecipes,{recipeNameInLowerCase:iname});
                if(sIndex>=0){
                    $scope.stockRecipes[sIndex].quantity=parseFloat(r.quantity).toFixed(3);
                    $scope.stockRecipes[sIndex].itemName=$scope.stockRecipes[sIndex].recipeName+" (Semi-Processed)";
                    $scope.stockRecipes[sIndex].selectedUnitName=$scope.stockRecipes[sIndex].unitName;
                    details.push($scope.stockRecipes[sIndex]);
                }
            }
        }
      });  
      return details;
    };
    //-------------------------------------------Stock Export-----------------------------------------------------//
    $scope.btnExport=true;
    $scope.bindCategory=function(){
    	$scope.availableCategory=[];
    	_.forEach($scope.stores,function(s,i){
    		if(s.selected==true){
    			_.forEach(s.category,function(c,ii){
    				$scope.availableCategory.push(c);
    			});
    		}
    	});
    };
    $scope.bindItems=function(){
    	$scope.availableItems=[];
    	_.forEach($scope.stores,function(s,i){
    		if(s.selected==true){
    			_.forEach(s.category,function(c,ii){
    				if(c.selected==true){
    					_.forEach(c.item,function(itm,iii){
    						itm.category={_id:c._id,categoryName:c.categoryName};
    						itm.store={_id:s._id,storeName:s.storeName};
    						$scope.availableItems.push(itm);
    					});
    				}
    			});
    		}
    	});	
    };
    $scope.checkAllStore= function(){
	      if(!$scope.storeSelectAll){
	        $scope.storeSelectAll=true;
	      }
	      else
	      {
	        $scope.storeSelectAll=false;
	      }
	      angular.forEach($scope.stores, function (s) {
	            s.selected = $scope.storeSelectAll;
	      });
	};
    $scope.checkAllCategory=function(){
	    	if(!$scope.storeSelectAllCAtegory){
	        $scope.storeSelectAllCAtegory=true;
	      }
	      else
	      {
	        $scope.storeSelectAllCAtegory=false;
	      }
	      angular.forEach($scope.availableCategory, function (s) {
	            s.selected = $scope.storeSelectAllCAtegory;
	      });
	};
	$scope.checkAllUnits=function(){
	    	if(!$scope.selectAllUnits){
		        $scope.selectAllUnits=true;
		      }
		      else
		      {
		        $scope.selectAllUnits=true;
		      }
		      angular.forEach($scope.stockUnits, function (s) {
		            s.selected = $scope.selectAllUnits;
		      });
	};
	$scope.checkAllItems=function(){
	  	if(!$scope.selectAllItems){
		    $scope.selectAllItems=true;
		}
		else{
		    $scope.selectAllItems=false;
		}
		angular.forEach($scope.availableItems, function (s) {
		    s.selected = $scope.selectAllItems;
		});
	};
    $scope.bindRecipes=function(){
        $scope.reportDataRecipe=[];
        _.forEach($scope.stockRecipes,function(s,i){
            if(s.selected==true){
                var cINdex=_.findIndex($scope.categorys,{_id:s.categoryId});
                s.category=$scope.categorys[cINdex];
                _.forEach(s.receipeDetails,function(c,ii){
                    var obj={recipeName:s.recipeName,categoryName:s.category.categoryName,itemName:c.itemName,units:c.units};
                    $scope.reportDataRecipe.push(obj);
                });
            }
        }); 
    };
    $scope.checkAllRecipes=function(){
        if(!$scope.selectAllRecipes){
            $scope.selectAllRecipes=true;
        }
        else
        {
            $scope.selectAllRecipes=false;
        }
        angular.forEach($scope.stockRecipes,function(s){
            s.selected=$scope.selectAllRecipes;
        });
    };
    function getItemsForExport(){
        $scope.reportData=[];
        angular.forEach($scope.availableItems,function(s){
            if(s.selected==true){
                $scope.reportData.push(s);
            }
        });
    }
	$scope.onfinishedWizardExport=function(){
        $scope.bindRecipes();
        getItemsForExport();
        $timeout(function() {
          $scope.exportData();
        }, 0);
        
      	$scope.btnExport=false;
	};
	$scope.exportData=function(){
	  	window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('exportTable').innerHTML));
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('exportTableRecipe').innerHTML));
	};
}]);
