'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('stockImportExport', {
        url: '/stockImportExport',
        templateUrl: 'app/stock/stockImportExport/stockImportExport.html',
        controller: 'StockImportExportCtrl'
      });
  });