'use strict';

describe('Controller: StockImportExportCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StockImportExportCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StockImportExportCtrl = $controller('StockImportExportCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
