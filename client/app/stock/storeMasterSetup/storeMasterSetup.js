'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('storeMasterSetup', {
        url: '/storeMasterSetup',
        templateUrl: 'app/stock/storeMasterSetup/storeMasterSetup.html',
        controller: 'StoreMasterSetupCtrl'
      });
  });