'use strict';

describe('Controller: StoreMasterSetupCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StoreMasterSetupCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StoreMasterSetupCtrl = $controller('StoreMasterSetupCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
