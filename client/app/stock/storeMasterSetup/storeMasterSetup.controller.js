'use strict';

angular.module('posistApp')
  .controller('StoreMasterSetupCtrl',['$q','$scope','$modal','store','Deployment','currentUser','stockCategory','stockUnit','stockItem','vendor','receiver','Category','Item','stockRecipe','$filter','$rootScope','$resource','growl','Pagination','$state','StockResource','localStorageService', function ($q,$scope,$modal,store ,Deployment,currentUser,stockCategory,stockUnit,stockItem,vendor,receiver,Category,Item,stockRecipe,$filter,$rootScope,$resource,growl,Pagination,$state,StockResource,localStorageService) {
    $scope.stores=[];
    $scope.deployment={};
    currentUser.deployment_id=localStorageService.get('deployment_id');
   $scope.categories=[];
   $scope.spCategories=[];
   $scope.recipeCategory=[];
   $scope.items=[];
   $scope.stockRecipes=[];

    function getStores_ByDeployment(){
      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getStockCategories_ByDeployment(){
      return stockCategory.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getStockUnits_ByDeployment(){
      return stockUnit.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getStockItems_ByDeployment(){
      return stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getVendors_ByDeployment(){
      return vendor.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getReceivers_ByDeployment(){
        return receiver.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getCategories(){
    	return Category.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
   };
   function getItems(){
    	return Item.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
   };
   function getstockRecies(){
    	return stockRecipe.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
   };
     var getDeployment=Deployment.get({tenant_id:currentUser.tenant_id}).$promise.then( function (dep){
           $scope.deployment=dep;

           var allPromise=$q.all([
		        getStores_ByDeployment(), getStockCategories_ByDeployment(),getStockUnits_ByDeployment(), 
		        getStockItems_ByDeployment() ,getVendors_ByDeployment(),getReceivers_ByDeployment(),
		        getItems() ,getstockRecies()
            //getCategories() ,
		    ]);          
		           
            allPromise.then(function (value){
             $scope.stores=value[0];
             hideKitchenAndMainStore();
             $scope.stockCategories = value[1];
             $scope.stockUnits=value[2];
             //$scope.totalUnits = $scope.stockUnits.length;           
             $scope.stockItems=value[3];
             enableItemGroups();
             $scope.vendors=value[4];
             $scope.receivers=value[5];
             //$scope.categories = value[6];
             $scope.items=value[7];
             $scope.stockRecipes=value[8];
             //$scope.recipeReportForm.items=value[8];
           });
           
           Category.get({tenant_id:currentUser.tenant_id, deployment_id:currentUser.deployment_id}).$promise.then(function (categories) {
            CatWithSemiProcessed(categories); 
          });
            // store.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then(function (result){
            //   $scope.stores=result;
              
            // });

            // stockCategory.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
            //   $scope.stockCategories=result;
            // });

            // stockUnit.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
            //   $scope.stockUnits=result;
            //   $scope.totalUnits = $scope.stockUnits.length;
            // });

            // stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
            //   $scope.stockItems=result;
            //   enableItemGroups();
            //   //selectSelectedPreferredUnit();
            // });

            // vendor.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
            //   $scope.vendors=result;
            // });

            // receiver.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
            //   $scope.receivers=result;
            //   console.log($scope.receivers);
            // });


     });



    
  	$scope.storeForm={
  		isKitchen: false,
  		isMain: false,
  		isEdit:false
  	};

    $scope.clearStoreTab= function(){
      $scope.storeForm={
        isKitchen: false,
        isMain: false,
        isEdit:false
      } ;      
    }

    $scope.storePagination = Pagination.getNew(10);
    function storePagination(){
      $scope.storePagination.numPages = Math.ceil($scope.stores.length / $scope.storePagination.perPage);
      $scope.storePagination.totalItems = $scope.stores.length;
    };

    $scope.hidekitchenMain= function(){
      hideKitchenAndMainStore();
      storePagination();
    };

    function hideKitchenAndMainStore(){
      _.forEach($scope.stores, function(s){
        if(s.isKitchen==true){
          $scope.storeForm.hideKitchen=true;
        }
        if(s.isMain==true)
        {
          $scope.storeForm.hideMainStore=true;
        }
      });
    };
    function checkIsKitchenAndMainStorePresentForUpdate(sa){
      _.forEach($scope.stores, function(s){
        if(s.isKitchen==true){
          $scope.storeForm.hideKitchen=true;
          if(sa.isKitchen==true){
            $scope.storeForm.hideKitchen=false;
          }
        }
        if(s.isMain==true)
        {
          $scope.storeForm.hideMainStore=true;
          if(sa.isMain==true){
            $scope.storeForm.hideMainStore=false;
          }
        }
      });
    };

  	$scope.createStore=function( formStore){
  		if(formStore.$valid && !$scope.storeForm.isEdit){
        $scope.storeForm.tenant_id=currentUser.tenant_id;
        $scope.storeForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
  			store.saveData({},$scope.storeForm, function (result){
          // Remove all error markers
          clearMongooseError(formStore);  
          if (result.error) {
            onMongooseError(result,formStore);
          } 
          else
          {
            $scope.stores.push(result);
            console.log($scope.deployment);
            $scope.storeForm={
              isKitchen: false,
              isMain: false,
              isEdit:false
            };
            hideKitchenAndMainStore();
            growl.success('Successfully Created', {ttl: 3000});
            //alert("Successfully Created");
            formStore.$setPristine();
            storePagination();
          }          
  		  });
      }
  		else
  		{
  			store.update({},$scope.storeForm, function (result){
          clearMongooseError(formStore);  
          if (result.error) {
            onMongooseError(result,formStore);
          } 
          else
          {
  				var id=_.findIndex($scope.stores,{_id:result._id});
  				$scope.stores.splice(id,1);
  				$scope.stores.push(result);
  				$scope.storeForm={
  					isKitchen: false,
  					isMain: false,
  					isEdit:false
  				};
          hideKitchenAndMainStore();
          UpdateCategoryOnStoreChange(result);
          growl.success('Successfully Updated', {ttl: 3000});
          //alert("Successfully Updated");
  				formStore.$setPristine();
        }
  			});
  		}
  	};

  	$scope.editStore=function (s){
  		$scope.storeForm=angular.copy(s); 	
  		$scope.storeForm.isEdit=true;
      checkIsKitchenAndMainStorePresentForUpdate(s);	
      //hideKitchenAndMainStore();
  	};

    function UpdateCategoryOnStoreChange(s){
      _.forEach($scope.stockCategories,function(c){
        if(c.store._id==s._id)
        {
          c.store=s;
          stockCategory.update({},c, function (result){
            console.log(result);
          });
        }
      });
    }

    function checkIsStoreAssigned(storeId){
      var flag=false;
      _.forEach($scope.stockCategories, function(cat,i){
        if(cat.store!=undefined){
          if(cat.store._id==storeId){
              flag=true;
          }
        }
      });
      return flag;
    };

  	$scope.removeStore= function (s){
      var check=checkIsStoreAssigned(s._id);
      if(check==false){
    		store.remove({},s, function (result){
    			var index=_.findIndex($scope.stores,{_id:result._id});
          $scope.stores.splice(index,1);
          storePagination();
    		});
      }
      else
      {
        growl.success('Used store can not be deleted', {ttl: 3000});
        //alert("Used Store can not be deleted");
        //$scope.storeForm.isMessage=true;
        //$scope.storeForm.message="Used Store can not be deleted";
      }
  	};
    $scope.clearStoreForm=function(){
      $scope.storeForm={
            isKitchen: false,
            isMain: false,
            isEdit:false
          };
        hideKitchenAndMainStore();
      formStore.$setPristine();
    };
    $scope.checkAllStore= function()
    {
      if(!$scope.storeSelectAll){
        $scope.storeSelectAll=true;
      }
      else
      {
        $scope.storeSelectAll=false;
      }
      angular.forEach($scope.stores, function (s) {
            s.selected = $scope.storeSelectAll;
      });
    };
    $scope.removeSelectedStore= function (){
      //var s=angular.copy();
      _.forEach($scope.stores, function(s,i){
        if(s.selected==true)
        {
          store.remove({},s,function (result){            
            var index=_.findIndex($scope.stores,{_id:result._id});
            $scope.stores.splice(index,1); 
            console.log(result);
            //console.log(s.selected);
            //console.log(i);
          });
        }
        
      });
    };
    //-------------------------------------------------------------Stock Category-----------------------------------------
    $scope.stockCategories=[];
    $scope.stockCategoryForm={
      isEdit:false
    };

    $scope.categoryPagination = Pagination.getNew(10);


    function categoryPagination(){
      $scope.categoryPagination.numPages = Math.ceil($scope.stockCategories.length / $scope.categoryPagination.perPage);
      $scope.categoryPagination.totalItems = $scope.stockCategories.length;
    };

    $scope.clearCategoryTab=function(){
        $scope.stockCategoryForm={
          isEdit:false
        };
        categoryPagination();
    };
    function clearMongooseError(formname){
      // Remove all error markers
      for (var key in formname) {
        if(formname[key]!=undefined){
        if (formname[key].$error) {
          formname[key].$error.mongoose = null;
        }
      }
      }
    };
    function onMongooseError(result,formname)
    {
      // We got some errors, put them into angular
      for (var key in result.error.errors) {
        formname[key].$error.mongoose = result.error.errors[key].message;
          console.log(formname[key].$error.mongoose);
      }
    };

    function updateReceiveOnCreateCategory(cat){
      _.forEach($scope.receivers, function (r,i){
        if(r.pricing.store!=null){
          _.forEach(r.pricing.store, function(str,ii){
            if(str._id==cat.store._id){
              str.category.push(cat);
            };
          });
        }
        var recToUpdate=angular.copy(r);
        receiver.update({},recToUpdate,function(result){
          $scope.receivers[i]=result;
        });
      });
    }

    $scope.createStockCategory= function(formStockCategory){
      if(formStockCategory.$valid && !$scope.stockCategoryForm.isEdit){
        $scope.stockCategoryForm.tenant_id=currentUser.tenant_id;
        $scope.stockCategoryForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        stockCategory.saveData({},$scope.stockCategoryForm, function (result){
          clearMongooseError(formStockCategory);
          if (result.error) {
            onMongooseError(result,formStockCategory);
          } 
          else
          {
            $scope.stockCategories.push(result);
            $scope.stockCategoryForm={
              isEdit:false
            };
            var index=_.findIndex($scope.stores,{_id:result.store._id});
            var tempStore=angular.copy($scope.stores[index]);
            var tempresult=angular.copy(result);
            delete tempresult.store;
            tempStore.category.push(tempresult);
            store.update({},tempStore, function(s){
              $scope.stores[index]=angular.copy(s);
              $scope.stockCategoryForm.store=$scope.stores[index];
            });
            updateReceiveOnCreateCategory(result);
            console.log($scope.deployment);
            
            growl.success('Successfully Created', {ttl: 3000});
            //alert("Successfully Created");
            formStockCategory.$setPristine();
            categoryPagination();
          }
        });
      }
      else
      {
        stockCategory.update({},$scope.stockCategoryForm, function (result){
          clearMongooseError(formStockCategory);
          if (result.error) {
            onMongooseError(result,formStockCategory);
          } 
          else
          {
            var id=_.findIndex($scope.stockCategories,{_id:result._id});
            $scope.stockCategories.splice(id,1);
            $scope.stockCategories.push(result);

            $scope.stockCategoryForm={
              isEdit:false
            };
            growl.success('Successfully Updated', {ttl: 3000});
            //alert("Successfully Updated");
            formStore.$setPristine();
          }
        });
      }
    };
    $scope.editStockCategory= function(s){
      $scope.stockCategoryForm=angular.copy(s);
      var index = _.findIndex($scope.stores, {"storeName":s.store.storeName});
      $scope.stockCategoryForm.store=$scope.stores[index];
      $scope.stockCategoryForm.isEdit=true;
    };

    function updateVendorCategories(cat){
      _.forEach($scope.vendors,function(v,i){
        var flag=false;
        var vendorForUpdate=null;
        _.forEachRight(v.pricing.category, function(c,ii){
          if(c._id==cat._id)
          {
            v.pricing.category.splice(ii,1);
            flag=true;
            vendorForUpdate=angular.copy($scope.vendors[i]);
          }
        });
        if(flag==true){
          vendor.update({},vendorForUpdate,function(result){
            $scope.vendors[i]=result;
          });
        }
      });
    }

    function updateStoreOnCategoryRemove(cat){
      _.forEach($scope.stores, function(str,i){
        var flag=false;
        var storeToUpdate=null;
        if(str._id==cat.store._id){
          _.forEachRight(str.category, function(c,ii){
            if(c._id==cat._id){
              str.category.splice(ii,1);
              flag=true;
              storeToUpdate=angular.copy($scope.stores[i]);
            }
          });
        }
        if(flag==true){
          store.update({},storeToUpdate, function(result){
            $scope.stores[i]=result;
            //str=result;
          });
        }
      });
    };
    function checkCategoryAssigned(cat){
      if(cat.item.length==0){
        return false;
      }
      else
      {
        return true;
      }
    };

    $scope.removeStockCategory= function(s){
      var check=checkCategoryAssigned(s);
      if(check==false)
      {
        stockCategory.remove({},s, function (result){
          var index=_.findIndex($scope.stockCategories,{_id:result._id});
          $scope.stockCategories.splice(index,1);
          updateStoreOnCategoryRemove(result);
          updateVendorCategories(result);
          categoryPagination();
        });
      }
      else
      {
        //item assigned in category
        growl.error('Used Category can not be deleted', {ttl: 3000});
        //alert("Used Category can not be deleted");
      }
    };
    $scope.clearStockCategoryForm= function(){
      $scope.stockCategoryForm={
        isEdit:false
      };
      formStockCategory.$setPristine();
    };

    //--------------------------------------------------------------------Stock Units ----------------------------------------
    
    $scope.baseUnits=[
      {id:1,name:"Gram",value:1},
      {id:2,name:"Litre",value:0.001},
      {id:3,name:"Meter",value:0.001},
      {id:4,name:"Pc",value:1}
    ];
    $scope.stockUnits=[];
    $scope.stockUnitsForm={
      isEdit:false
    };

    $scope.unitPagination = Pagination.getNew(10);
    function unitPagination(){
      $scope.unitPagination.numPages = Math.ceil($scope.stockUnits.length / $scope.unitPagination.perPage);
      $scope.unitPagination.totalItems = $scope.stockUnits.length;
    };

    $scope.clearUnitsTab= function(){
      $scope.stockUnitsForm={
        isEdit:false
      };      
      unitPagination();
    };
    //--------Pagination
    $scope.itemsPerPage = 1;
    $scope.currentPage = 1;
    //$scope.totalUnits = 64;
    //$scope.currentPage = 4;
    //$scope.maxSizeUnits = 5;
    //$scope.bigTotalItems = 175;
    //$scope.bigCurrentPageUnit = 1;

    //$scope.pageChanged = function() {
      //$scope.currentPage=+$scope.currentPage;      
    //};

     // getDeployment.$promise.then( function(dep){
       // stockUnit.get({deployment_id:dep[0]._id}, function (result){
         //   $scope.stockUnits=result;
           // $scope.totalUnits = $scope.stockUnits.length;
            //$scope.$watch('currentPage + itemsPerPage', function() {
              //var begin = (($scope.currentPage - 1) * $scope.itemsPerPage),
              //end = begin + $scope.itemsPerPage;
              //$scope.stockUnits1 = $scope.stockUnits.slice(begin, end);
            //});
          //});
      //})
      

    //---------End Pagination

    $scope.submitStockUnits=function (formStockUnits){
      if(formStockUnits.$valid && !$scope.stockUnitsForm.isEdit){
        $scope.stockUnitsForm.tenant_id=currentUser.tenant_id;
        $scope.stockUnitsForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        stockUnit.saveData({},$scope.stockUnitsForm, function (result){
          clearMongooseError(formStockUnits);  
          if (result.error) {
            onMongooseError(result,formStockUnits);
          } 
          else
          {
            $scope.stockUnits.push(result);
            $scope.stockUnitsForm={
              isEdit:false
            };
            growl.success('Successfully Created', {ttl: 3000});
            //alert("Successfully Created");
            formStockUnits.$setPristine();
            unitPagination();
          }
        });
      }
      else
      {
        stockUnit.update({},$scope.stockUnitsForm, function (result){
          clearMongooseError(formStockUnits);  
          if (result.error) {
            onMongooseError(result,formStockUnits);
          } 
          else
          {
            var id=_.findIndex($scope.stockUnits,{_id:result._id});
            $scope.stockUnits.splice(id,1);
            $scope.stockUnits.push(result);
            $scope.stockUnitsForm={
              isEdit:false
            };
            UpdateItemsUnitOnUnitChange(result);
            growl.success('Successfully Updated', {ttl: 3000});
            //alert("Successfully Updated");
            formStockUnits.$setPristine();
          }
        });
      }
    };
    $scope.editStockUnits= function(s){
      $scope.stockUnitsForm=angular.copy(s);
      var index = _.findIndex($scope.baseUnits,s.baseUnit);
      $scope.stockUnitsForm.baseUnit=$scope.baseUnits[index];
      $scope.stockUnitsForm.isEdit=true;
    };

    function checkIsUnitAssigned(unitId){
      var flag=false;
      _.forEach($scope.stockItems, function(itm,i){
        _.forEach(itm.units, function(u,ii){
          if(u._id==unitId)
          {
            flag=true;
          }
        });
      });

      return flag;
    };
    $scope.removeStockUnits= function(s){
      var check=checkIsUnitAssigned(s._id);
      if(check==false)
      {
        stockUnit.remove({},s, function (result){
          var index=_.findIndex($scope.stockUnits,{_id:result._id});
          $scope.stockUnits.splice(index,1);
          unitPagination();
        });
      }
      else
      {
        growl.success('Used unit can not be deleted', {ttl: 3000});
        //alert("Used Unit can not be delete");
      }
    };
    $scope.clearStockUnitForm= function(){
      $scope.stockUnitsForm={
        isEdit:false
      };
      stockUnitsForm.$setPristine();
    };

    function UpdateItemsUnitOnUnitChange(u){
      _.forEach($scope.stockItems,function(t){
          _.forEach(t.units, function(ui){
            if(ui._id==u._id){
              var index=_.findIndex(t.units,{_id:u._id});
              t.units.splice(index,1);
              t.units.push(u);
              stockItem.update({},t, function(result){
                console.log(t);
              });
            }
          });
      });
    };

    //---------------------------------------------------Stock Items----------------------------------------------------------
    $scope.stockItemsForm={ isEdit:false};
    $scope.stockItems=[];
    $scope.stockItemsPUnit={};

    $scope.itemPagination = Pagination.getNew(10);

    function itemPagination(){
      $scope.itemPagination.numPages = Math.ceil($scope.stockItems.length / $scope.itemPagination.perPage);
      $scope.itemPagination.totalItems = $scope.stockItems.length;
    };

    $scope.clearItemsTab= function(){
      $scope.stockItemsForm={ isEdit:false};
      enableItemGroups();
      itemPagination();
    };
    $scope.submitStockItems=function(formStockItems){
      if(formStockItems.$valid && !$scope.stockItemsForm.isEdit){
        if($scope.stockItemsForm.Unit!=null || $scope.stockItemsForm.Unit!=undefined){
          var count=0;
          $scope.stockItemsForm.units=[];
          for ( var prop in $scope.stockItemsForm.Unit ) {
            var value = $scope.stockItemsForm.Unit[prop];
            if(value==true){
              count=count+1;              
              console.log(value);
              var index=_.findIndex($scope.stockUnits,{_id:prop});
              delete $scope.stockItemsForm.Unit[prop];

              $scope.stockItemsForm.units.push($scope.stockUnits[index]);
            }
            else
            {
              growl.success('Please select at least one unit to add Item', {ttl: 3000});
              //alert("PLease select at least one unit to add Item");
            }
          }
          console.log($scope.stockItemsForm.units);
          $scope.stockItemsForm.tenant_id=currentUser.tenant_id;
          $scope.stockItemsForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
          if(count>0)
          {
            stockItem.saveData({},$scope.stockItemsForm, function (result){
              clearMongooseError(formStockItems);  
              if (result.error) {
                onMongooseError(result,formStockItems);
              } 
              else
              {
                $scope.stockItems.push(result);
                $scope.stockItemsForm={
                  isEdit:false
                };
                growl.success('Successfully Created', {ttl: 3000});
                //alert("Successfully Created");
                formStockItems.$setPristine();
                enableItemGroups();
                itemPagination();
              }
            });
          }
          else
          {
            growl.success('Please select at least one unit to add Item', {ttl: 3000});
            //alert("PLease select at least one unit to add Item");
          }
        }
        else
        {
          growl.success('Please select at least one unit to add Item', {ttl: 3000});
          //alert("PLease select at least one unit to add Item");   
        }
      }
      else
      {
        if($scope.stockItemsForm.Unit!=null || $scope.stockItemsForm.Unit!=undefined){
          var count=0;
          $scope.stockItemsForm.units=[];
          for ( var prop in $scope.stockItemsForm.Unit ) {
            var value = $scope.stockItemsForm.Unit[prop];
            if(value==true){
              count=count+1;              
              console.log(value);
              var index=_.findIndex($scope.stockUnits,{_id:prop});
              delete $scope.stockItemsForm.Unit[prop];

              $scope.stockItemsForm.units.push($scope.stockUnits[index]);
            }
          }
          if(count>0)
          {
            stockItem.update({},$scope.stockItemsForm, function (result){
              clearMongooseError(formStockItems);  
              if (result.error) {
                onMongooseError(result,formStockItems);
              } 
              else
              {
                var id=_.findIndex($scope.stockItems,{_id:result._id});
                $scope.stockItems.splice(id,1);
                $scope.stockItems.push(result);
                $scope.stockItemsForm={
                  isEdit:false
                };
                enableItemGroups();
                growl.success('Successfully updated', {ttl: 3000});
                //alert("Successfully Updated");
                formStockItems.$setPristine();
              }
            });
          }
          else
          {
            growl.success('Please select at least one unit to add Item', {ttl: 3000});
            //alert("PLease select at least one unit to add Item");
          }
        }
        else
        {
          growl.success('Please select at least one unit to add Item', {ttl: 3000});
          //alert("PLease select at least one unit to add Item");   
        }        
      }
    };


     function enableItemGroups(){
      _.forEach($scope.baseUnits, function(b){
          var n=b.name;
          $scope.stockItemsForm[n]=true;
      });
      delete $scope.stockItemsForm.Unit;
    };

    function selectSelectedPreferredUnit(){
      _.forEach($scope.stockItems, function(st){
        $scope.stockItems.units=st.units;
        var index = _.findIndex($scope.stockItems.units,{unitName:st.preferedUnit});
        $scope.stockItemsPUnit.preferedUnit={};
         $scope.stockItemsPUnit.preferedUnit=$scope.stockItems.units[index];
        console.log($scope.stockItemsPUnit.preferedUnit);
      });
      
    };
    $scope.clearStockItemForm=function(){
      $scope.stockItemsForm={ isEdit:false};
      enableItemGroups();
    };
    $scope.disableOthersGroup= function(key, id){
      $scope.stockItemsForm.key=true;
      //$scope.selecteddiv=key;
      console.log(key);
      var count=0;
      _.forEach($scope.baseUnits, function(b){
        if(key!=b.name){
          var n=b.name;
          $scope.stockItemsForm[n]=false;
        }
        else
        {
          var tf=$scope.stockItemsForm.Unit[id];

        }
      });
      for ( var prop in $scope.stockItemsForm.Unit ) {
            var value = $scope.stockItemsForm.Unit[prop];
            if(value==true){
              count=count+1;
            }
          };
          if(count<1)
          {
           enableItemGroups();
          }
    };
    function disableGroupForEditItems(key){
      $scope.stockItemsForm.key=true;
      //$scope.selecteddiv=key;
      console.log(key);
      var count=0;
      _.forEach($scope.baseUnits, function(b){
        if(key!=b.name){
          var n=b.name;
          $scope.stockItemsForm[n]=false;
        }
      });
    };
    $scope.editStockItems=function(s){
      $scope.stockItemsForm=angular.copy(s);
      $scope.stockItemsForm.isEdit=true;
      var key=s.units[0].baseUnit.name;
      enableItemGroups();
      disableGroupForEditItems(key);
      $scope.stockItemsForm.Unit={};
      _.forEach(s.units, function(u){
        $scope.stockItemsForm.Unit[u._id]=true;
      });
    };

    function checkIsItemAssigned(item){
      if(item.assignedToStore.length==0){
        return false;
      }
      else
      {
        return true;
      }
    };

    $scope.removeStockItems=function(s){
      var check=checkIsItemAssigned(s);
      if(check==false)
      {
        stockItem.remove({},s, function (result){
          var index=_.findIndex($scope.stockItems,{_id:result._id});
          $scope.stockItems.splice(index,1);
          itemPagination();
        });
      }
      else
      {
        growl.success('Assign Item can not be deleted', {ttl: 3000});
        //alert("Assign Item can not delete");
      }
    };

    $scope.updatePreferedUnit=function(punit,s){
      //s.preferedUnit=angular.copy($scope.stockItemsPUnit.preferedUnit);
      //var index=_.findIndex($scope.stockItems.units,{_id:s});
      //s["preferedUnit"]=punit;
      //s.preferedUnit["selected"]=true;
      //var temp=$scope.stockItems.units[index];
      //temp["selected"]=true;
      //$scope.stockItems.units[index]=temp;
      //console.log(s.preferedUnit);
      stockItem.update({},s, function (result){
        console.log(result);
      });
    };


    //---------------------------------------------------Add Item to STore ---------------------------
    $scope.AssignItemToStoreForm ={
      
    };
    $scope.siPagination = Pagination.getNew(10);


    function storeItemsPagination(){
      $scope.siPagination.numPages = Math.ceil($scope.AssignItemToStoreForm.selectedCategory.item.length / $scope.siPagination.perPage);
      $scope.siPagination.totalItems = $scope.AssignItemToStoreForm.selectedCategory.item.length;
    };

    $scope.clearItemsInStoreTab= function(){
      $scope.AssignItemToStoreForm ={};
    };

    $scope.OnStoreChange= function(st){
      $scope.AssignItemToStoreForm.category={};
      $scope.AssignItemToStoreForm.selectedCategory={};
      $scope.AssignItemToStoreForm.category=st.category;
    };
    function ItemsNotInStore(str){
      var itemsforTRansfer=[];
      _.forEach($scope.stockItems,function(i){        
        if(i.assignedToStore.length==0){
          itemsforTRansfer.push(i);
        }
        else
        {
          var flag=false;
          _.forEach(i.assignedToStore, function(s){
            if(s==str){
              flag=true;
            }
          });

          if(flag==false)
          {            
              itemsforTRansfer.push(i);
          }
        }
      });
      return itemsforTRansfer;
    };
    $scope.OnCategoryChange =function(cat){
      $scope.AssignItemToStoreForm.items=ItemsNotInStore($scope.AssignItemToStoreForm.store.storeName);
      $scope.siPagination.page=0;
      storeItemsPagination();
    };

    function assignItemToVendors_trasnferToStore(item,cat_id,store_id){      
      _.forEach($scope.vendors, function(v,i){
        var isItemAddedtoVendor=false;
        if(v.pricing._id==store_id){
          _.forEach(v.pricing.category, function(cat, ii){
            if(cat._id==cat_id){
              var flag=false;
              _.forEach(cat.item, function(itm,iii){
                if(itm._id==item._id){
                  flag=true;
                }
              });

              if(flag==false){
                //add item to category
                var it=angular.copy(item);
                for(var p in it){
                  if(p!="_id" && p!="itemName" && p!="preferedUnit"){
                    _.forEach(it.units, function(u){
                      for(var p in u){
                        if(p!="_id" && p!="unitName" && p!="conversionFactor"){
                          delete u[p];
                        }
                      }
                    });
                  }
                }
                it["price"]="";
                it["fromDate"]="";
                it["toDate"]="";
                cat.item.push(it);
                isItemAddedtoVendor=true;
              }
            }
          });
        }
        if(isItemAddedtoVendor==true){
          //Update vendor 
          vendor.update({}, v , function(result){
            var index=_.findIndex($scope.vendors,{_id:result._id});
            $scope.vendors.splice(index,1);
            $scope.vendors.push(result);
          });
        }
      });
    };
    function assignItemToREceivers_trasnferToStore(item,cat_id,store_id){
      _.forEach($scope.receivers, function(v,i){
        var isItemAddedtoVendor=false;
        _.forEach(v.pricing.store, function(str,j){
          if(str._id==store_id){
          _.forEach(str.category, function(cat, ii){
            if(cat._id==cat_id){
              var flag=false;
              _.forEach(cat.item, function(itm,iii){
                if(itm._id==item._id){
                  flag=true;
                }
              });
              if(flag==false){
                //add item to category
                var it=angular.copy(item);
                for(var p in it){
                  if(p!="_id" && p!="itemName" && p!="preferedUnit"){
                    _.forEach(it.units, function(u){
                      for(var p in u){
                        if(p!="_id" && p!="unitName" && p!="conversionFactor"){
                          delete u[p];
                        }
                      }
                    });
                  }
                }
                it["price"]="";
                //it["fromDate"]="";
                //it["toDate"]="";
                cat.item.push(it);
                isItemAddedtoVendor=true;
              }
            }
          });
          }
        });        
        if(isItemAddedtoVendor==true){
          //Update receiver 
          receiver.update({}, v , function(result){
            var index=_.findIndex($scope.receivers,{_id:result._id});
            $scope.receivers.splice(index,1);
            $scope.receivers.push(result);
          });
        }
      });
    };

    $scope.transferToStore = function(formAssignItemToStore){
      var item_Index=_.findIndex($scope.stockItems,{_id:$scope.AssignItemToStoreForm.selectedItem._id});
      var temp_Item=angular.copy($scope.stockItems[item_Index]);
      temp_Item.assignedToStore.push($scope.AssignItemToStoreForm.store.storeName);
      stockItem.update({},temp_Item, function (result_item){        
        $scope.stockItems[item_Index]=result_item;
        $scope.AssignItemToStoreForm.selectedCategory.item.push(result_item);

        var cat_index=_.findIndex($scope.stockCategories,{_id:$scope.AssignItemToStoreForm.selectedCategory._id});
        var temp_cat=angular.copy($scope.stockCategories[cat_index]);
        temp_cat.item.push(result_item);
        stockCategory.update({},temp_cat, function (result_Cat){
          $scope.stockCategories[cat_index]=result_Cat;
          $scope.OnCategoryChange(result_Cat);

          var store_index=_.findIndex($scope.stores,{_id:$scope.AssignItemToStoreForm.store._id});
          var temp_store=angular.copy($scope.stores[store_index]);
          var cIndex=_.findIndex(temp_store.category,{_id:$scope.AssignItemToStoreForm.selectedCategory._id});
          //splice category from store first
          temp_store.category.splice(cIndex,1);
          //now push the new updated category to store
          var rCat=angular.copy(result_Cat);
          delete rCat.store;
          temp_store.category.push(rCat);
          //update the store with category and  item
          store.update({},temp_store, function (result_store){
            $scope.stores[store_index]=result_store;
             $scope.AssignItemToStoreForm.store=$scope.stores[store_index];
             //$scope.AssignItemToStoreForm ={};

             //add item to vendor and receiver
             assignItemToVendors_trasnferToStore(result_item,result_Cat._id,result_store._id);
             assignItemToREceivers_trasnferToStore(result_item,result_Cat._id,result_store._id);
             console.log(result_item);
             console.log(result_Cat._id);
             console.log(result_store._id);
             growl.success('Successfully Transfered', {ttl: 3000});
             //alert("Successfully Transfered");
          });
        });
      });
    };


    function removeTransferedStockItems_fromVendors(item,cat_id,store_id){
      _.forEach($scope.vendors, function(v,i){
        var isItemAddedtoVendor=false;
        if(v.pricing._id==store_id){
          _.forEach(v.pricing.category, function(cat, ii){
            if(cat._id==cat_id){
              var flag=false;
              _.forEach(cat.item, function(itm,iii){
                if(itm._id==item._id){
                  flag=true;
                  cat.item.splice(iii,1);
                }
              });
              if(flag==true){
                isItemAddedtoVendor=true;
              }
            }
          });
        }
        if(isItemAddedtoVendor==true){
          //Update vendor 
          vendor.update({}, v , function(result){
            var index=_.findIndex($scope.vendors,{_id:result._id});
            $scope.vendors.splice(index,1);
            $scope.vendors.push(result);
          });
        }
      });
    };
    function removeTransferedStockItems_fromReceivers(item,cat_id,store_id){
      _.forEach($scope.receivers, function(v,i){
        var isItemAddedtoVendor=false;
        _.forEach(v.pricing.store, function(str,j){
          if(str._id==store_id){
            _.forEach(str.category, function(cat, ii){
              if(cat._id==cat_id){
                var flag=false;
                _.forEach(cat.item, function(itm,iii){
                  if(itm._id==item._id){
                    flag=true;
                    cat.item.splice(iii,1);
                  }
                });
                if(flag==true){
                  isItemAddedtoVendor=true;
                }
              }
            });
          }
        });
        
        if(isItemAddedtoVendor==true){
          //Update receiver 
          receiver.update({}, v , function(result){
            var index=_.findIndex($scope.receivers,{_id:result._id});
            $scope.receivers.splice(index,1);
            $scope.receivers.push(result);
          });
        }
      });
    };

    $scope.removeTransferedStockItems = function(s,str){
      var item_Index=_.findIndex($scope.stockItems,{_id:s._id});
      var temp_Item=angular.copy($scope.stockItems[item_Index]);
      var index_Str=0;//_.findIndex(temp_Item.assignedToStore,str.storeName);
      for(var i=0;i<=temp_Item.assignedToStore.length;i++){
        if(temp_Item.assignedToStore[i]==str.storeName){
          index_Str=i;
          break;
        }
      }
      temp_Item.assignedToStore.splice(index_Str,1);
      var se_index=_.findIndex($scope.AssignItemToStoreForm.selectedCategory.item,{_id:temp_Item._id});
      stockItem.update({},temp_Item, function(result_item){
        $scope.stockItems[item_Index]=result_item;
        $scope.AssignItemToStoreForm.selectedCategory.item.splice(se_index,1);
      });

      var cat_index=_.findIndex($scope.stockCategories,{_id:$scope.AssignItemToStoreForm.selectedCategory._id});
      var temp_cat=angular.copy($scope.stockCategories[cat_index]);
      var Cat_item_Index=_.findIndex(temp_cat.item,{_id:s._id});
      temp_cat.item.splice(Cat_item_Index,1);


      stockCategory.update({},temp_cat, function(result_Cat){
        $scope.stockCategories[cat_index]=result_Cat;
        $scope.OnCategoryChange(result_Cat);

        var store_index=_.findIndex($scope.stores,{_id:result_Cat.store._id});
        var temp_store=angular.copy($scope.stores[store_index]);
        var cIndex=_.findIndex(temp_store.category,{_id:result_Cat._id});
        var temp_cat1=angular.copy(result_Cat);
        delete temp_cat1.store;
        temp_store.category.splice(cIndex,1);
        temp_store.category.push(temp_cat1);
        store.update({},temp_store, function(result_store){
          $scope.stores[store_index]=result_store;
          $scope.AssignItemToStoreForm.store=$scope.stores[store_index];
          //$scope.AssignItemToStoreForm ={};
          removeTransferedStockItems_fromVendors(s,result_Cat._id,result_store._id);
          removeTransferedStockItems_fromReceivers(s,result_Cat._id,result_store._id);
          growl.success('Successfully Removed', {ttl: 3000});
          //alert("Successfully Removed");
        });

      });
    };

    //--------------------------------------------------Create Vendors ----------------------------------------
    $scope.vendors=[];
    $scope.createVendorForm={
      isEdit:false,
      type:"Local Vendor",
      availableVendor:[]
    };

    $scope.venPagination = Pagination.getNew(10);


    function vendorPagination(){
      $scope.venPagination.numPages = Math.ceil($scope.createVendorForm.availableVendor.length / $scope.venPagination.perPage);
      $scope.venPagination.totalItems = $scope.createVendorForm.availableVendor.length;
    };

    $scope.clearVendorTab= function(){
      $scope.createVendorForm={
        isEdit:false,
        type:"Local Vendor",
        availableVendor:[]
      };
      $scope.clearVendorPricingTab();
    };
    $scope.OnStoreChange_VendorForm= function(st){
      $scope.createVendorForm.selectedCategory={};
      $scope.createVendorForm.category=st.category;
      availableVendorsByStore(st._id);
      vendorPagination();
    };

    function availableVendorsByStore(storeId){
      if($scope.createVendorForm.isEdit==true)
      {
        $scope.createVendorForm.availableVendor=[];
      }
      _.forEach($scope.vendors, function(v,i){
        if(v.pricing._id==storeId){
          $scope.createVendorForm.availableVendor.push($scope.vendors[i]);
        }
      });
    };

    $scope.vendorChk_Changed = function(id){
      var val=$scope.createVendorForm.selectedCategory[id];
      if(val==false)
      {
        delete $scope.createVendorForm.selectedCategory[id];  
      }      
    };

    $scope.clearCreateVendorForm = function(){
      $scope.createVendorForm.isEdit=false;
      $scope.createVendorForm.type="Local Vendor";
      $scope.createVendorForm.vendorName="";
      $scope.createVendorForm.contactPerson="";
      $scope.createVendorForm.contactNumber="";
      $scope.createVendorForm.address="";
      $scope.createVendorForm.tinNumber="";
      $scope.createVendorForm.serviceTaxNumber="";
      $scope.createVendorForm.email="";
      $scope.createVendorForm.selectedCategory={};
      //$scope.createVendorForm.store={};
    };
    $scope.depChange_CreateVendor = function(d){
      if(d!=null)
      {
        $scope.createVendorForm.isHO=true;
        $scope.createVendorForm.vendorName=d.name;
        $scope.createVendorForm.type="HO Vendor";
      }
      else
      {
        delete $scope.createVendorForm.isHO;
        delete $scope.createVendorForm.deployments;
        $scope.createVendorForm.type="Local Vendor";
      }
    };

    function formatPricing_beforeCreate(data){
      var pricing=null;

      for( var p in data){
        if(p!="_id" && p!="storeName" && p!="category"){
          delete data[p];                
        }
      }

      _.forEachRight(data.category, function(s,i){
        var flag=false;
        var sid=s._id;
        for(var prop in $scope.createVendorForm.selectedCategory){
          if(prop==sid){
            flag=true;
          }
        }
        if(flag==false){
          data.category.splice(i,1);
        }
      });

      _.forEach(data.category, function(str){
        for( var p in str){
          if(p!="_id" && p!="categoryName" && p!="item"){
            delete str[p];                
          }
        } 

        _.forEach(str.item, function(itm){
          for( var p in itm){
            if(p!="_id" && p!="itemName" && p!="preferedUnit" && p!="units"){
              delete itm[p];                
            }
          }

          _.forEach(itm.units, function(u){
            for(var p in u){
              if(p!="_id" && p!="unitName" && p!="conversionFactor"){
                delete u[p];
              }
            }
          });
          itm["price"]="";
          itm["fromDate"]="";
          itm["toDate"]="";
        });
      });


      $scope.createVendorForm.pricing=data;
      console.log(data);
    };

    function hasCategorySelected(data) {
      var flag=false;
        for (var k in data) {
            if (data.hasOwnProperty(k)) {
               if(data[k]==true){
                flag=true;
               }
            }
        }
        return flag;
    };

    $scope.createVendor = function(formCreateVendor){
      var hCat=hasCategorySelected($scope.createVendorForm.selectedCategory);
      if(hCat==true){
        if(formCreateVendor.$valid && !$scope.createVendorForm.isEdit){
          formatPricing_beforeCreate(angular.copy($scope.createVendorForm.store));
          //format before save
          $scope.createVendorForm.tenant_id=currentUser.tenant_id;
          $scope.createVendorForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
          vendor.saveData({},$scope.createVendorForm, function(result){
            clearMongooseError(formCreateVendor);  
             if (result.error) {
               onMongooseError(result,formCreateVendor);
             } 
             else
             {
              console.log(result);
              $scope.vendors.push(result);
               $scope.createVendorForm.availableVendor.push(result);
               growl.success('Successfully Created', {ttl: 3000});
               //alert("Successfully Created");
               $scope.clearCreateVendorForm();
               formCreateVendor.$setPristine();
             }
          });
        }
        else{
          formatPricing_beforeCreate(angular.copy($scope.createVendorForm.store));
          vendor.update({},$scope.createVendorForm, function(result){
            clearMongooseError(formCreateVendor);  
             if (result.error) {
               onMongooseError(result,formCreateVendor);
             } 
             else
             {
              console.log(result);
              var index=_.findIndex($scope.vendors,{_id:result._id});
              $scope.vendors.splice(index,1);
              $scope.vendors.push(result);
              var venIndex=_.findIndex($scope.createVendorForm.availableVendor,{_id:result._id});
              $scope.createVendorForm.availableVendor.splice(venIndex,1);
              $scope.createVendorForm.availableVendor.push(result);
              growl.success('Successfully updated', {ttl: 3000});
              //alert("Successfully Updated");
              $scope.clearCreateVendorForm();
              $scope.createVendorForm.isEdit=false;
               formCreateVendor.$setPristine();
              //$scope.clearCreateVendorForm();
             }
          });
        }
      }
      else{
        growl.error('Select a category first.', {ttl: 3000});
      }
    };

    $scope.removeVendors = function(s){
      vendor.remove({},s, function (result){
        var index=_.findIndex($scope.createVendorForm.availableVendor,{_id:result._id});
        $scope.createVendorForm.availableVendor.splice(index,1);
      });
    };

    function selectedCatgory_VendorEdit(){
      _.forEach($scope.createVendorForm.pricing.category, function(v){
        $scope.createVendorForm.selectedCategory[v._id]=true;
      });
    };

    $scope.editVendors = function(s){
      $scope.createVendorForm=angular.copy(s);
      var index=_.findIndex($scope.stores, {_id:s.pricing._id});
      $scope.createVendorForm.store=$scope.stores[index];
      $scope.createVendorForm.isEdit=true;
      $scope.createVendorForm.disableStore=true;
      $scope.OnStoreChange_VendorForm($scope.createVendorForm.store);
      selectedCatgory_VendorEdit();
    };








    //--------------------------------------------Create Receivers-----------------------------------
    $scope.receivers=[];

    $scope.createReceiverForm={
      isEdit:false
    };

    $scope.clearReceiverTab= function(){
      $scope.createReceiverForm={
        isEdit:false
      };
      $scope.receiverPricingform={
        disableSubmit:true
      };
    };

    $scope.clearCreateReceiverForm= function(){
      $scope.createReceiverForm={
        isEdit:false
      }
    };

    $scope.createReceiver = function(formCreateReceiver){
      if(formCreateReceiver.$valid && !$scope.createReceiverForm.isEdit)
      {
        $scope.createReceiverForm.tenant_id=currentUser.tenant_id;
        $scope.createReceiverForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        receiver.saveData({},$scope.createReceiverForm, function(result){
          clearMongooseError(formCreateReceiver);  
           if (result.error) {
             onMongooseError(result,formCreateReceiver);
           } 
           else
           {
            console.log(result);
             $scope.receivers.push(result);
             $scope.createReceiverForm={
              isEdit:false
             };
             growl.success('Successfully Created', {ttl: 3000});
             //alert("Successfully Created");
             formCreateReceiver.$setPristine();
           }
        });
      }
      else
      {
        receiver.update({},$scope.createReceiverForm, function(result){
          clearMongooseError(formCreateReceiver);  
           if (result.error) {
             onMongooseError(result,formCreateReceiver);
           } 
           else
           {
            console.log(result);
            var index=_.findIndex($scope.receivers,{_id:result._id});
            $scope.receivers.splice(index,1);
             $scope.receivers.push(result);
             $scope.createReceiverForm={
              isEdit:false
             };
             growl.success('Successfully Updated', {ttl: 3000});
             //alert("Successfully Updated");
             formCreateReceiver.$setPristine();
           }
        });
      }
    };

    $scope.editReceiver = function(s){
      $scope.createReceiverForm=angular.copy(s);
      $scope.createReceiverForm.isEdit=true;
    };

    $scope.removeReceiver = function(s){
      receiver.remove({},s, function (result){
        var index=_.findIndex($scope.receivers,{_id:result._id});
        $scope.receivers.splice(index,1);
      });
    };

    //-------------------------------------------Receiver Pricing---------------------------------------
    $scope.receiverPricingform={
      disableSubmit:true
    };

    function formatDataPricing_ReceiverPricing(data){
      for( var p in data){
          if(p!="_id" && p!="storeName" && p!="category"){
            delete data[p];                
          }
        }

        // _.forEachRight(data.category, function(s,i){
        //   var flag=false;
        //   var sid=s._id;
        //   for(var prop in $scope.createVendorForm.selectedCategory){
        //     if(prop==sid){
        //       flag=true;
        //     }
        //   }
        //   if(flag==false){
        //     data.category.splice(i,1);
        //   }
        // });

        _.forEach(data.category, function(str){
          for( var p in str){
            if(p!="_id" && p!="categoryName" && p!="item"){
              delete str[p];                
            }
          } 

          _.forEach(str.item, function(itm){
            for( var p in itm){
              if(p!="_id" && p!="itemName" && p!="preferedUnit" && p!="units"){
                delete itm[p];                
              }
            }

            _.forEach(itm.units, function(u){
              for(var p in u){
                if(p!="_id" && p!="unitName" && p!="conversionFactor"){
                  delete u[p];
                }
              }
            });

            itm["price"]="";
          });
        });
        // if($scope.receiverPricingform.pricing==undefined)
        // {
        //   $scope.receiverPricingform.pricing={};
        //   $scope.receiverPricingform.pricing=data;
        // }
        // else
        // {
        //   $scope.receiverPricingform.pricing=data;
        // }
      
    };
    function formatPricing_setReceiverPricing(data){
      var pricing=null;
      if($scope.receiverPricingform.receiver.pricing!=undefined)
      {
        var flag=false;
        _.forEach($scope.receiverPricingform.receiver.pricing.store, function(s,i){
          if(data._id==s._id){
            flag=true;
            data=angular.copy(s);
          }
        });
        if(flag==false){
          formatDataPricing_ReceiverPricing(data);
        }
      }
      else
      {
        formatDataPricing_ReceiverPricing(data);
      }            
    };
    function formatstores_recieverPricing(stores)
    {
      _.forEach(stores, function(store){
      for( var p in store){
          if(p!="_id" && p!="storeName" && p!="category"){
            delete store[p];                
          }
        }
        _.forEach(store.category, function(str){
          for( var p in str){
            if(p!="_id" && p!="categoryName" && p!="item"){
              delete str[p];                
            }
          } 

          _.forEach(str.item, function(itm){
            for( var p in itm){
              if(p!="_id" && p!="itemName" && p!="preferedUnit" && p!="units"){
                delete itm[p];                
              }
            }

            _.forEach(itm.units, function(u){
              for(var p in u){
                if(p!="_id" && p!="unitName" && p!="conversionFactor"){
                  delete u[p];
                }
              }
            });

            itm["price"]="";
          });
        });
      });

      return stores;
    };
    $scope.receiverChange_receiverPricing = function(r){
      //bind store
      var formattedStores=formatstores_recieverPricing(angular.copy($scope.stores));
      $scope.receiverPricingform.stores=formattedStores;
      $scope.receiverPricingform.categorys={};
      $scope.receiverPricingform.category={};
    };

    function slectedUnit_receiverPricing(item){
      $scope.receiverPricingform.unit={};
      _.forEach(item , function(itm,i){
        _.forEach(itm.units , function(u,ii){
          if(itm.preferedUnit==u._id){
            $scope.receiverPricingform.unit[itm._id]=u;
          }
        });
      });
    };

    $scope.storeChange_receiverPricing = function(s){
      $scope.receiverPricingform.categorys={};
      $scope.receiverPricingform.category={};
      //bind category after format
      formatPricing_setReceiverPricing(s);
      if($scope.receiverPricingform.receiver.pricing!=undefined){
        var flag=false;
        _.forEach($scope.receiverPricingform.receiver.pricing.store, function(ss,i){
          if(ss._id==s._id){
            flag=true;
            $scope.receiverPricingform.categorys=angular.copy(ss.category);
          }
        });

        if(flag==false)
        {
          $scope.receiverPricingform.categorys=angular.copy(s.category);
        }
      }
      else
      {
        $scope.receiverPricingform.categorys=angular.copy(s.category);  
      }
      
    };

    function checkStoreFor_receiverPricing(s){
      if($scope.receiverPricingform.receiver.pricing!=undefined){
        _.forEach($scope.receiverPricingform.receiver.pricing.store , function(str,i){
          if(str._id==s._id){
            _.forEach(str.category, function(cat, ii){
              //if()
            });
          }
        });
      }
    }

    $scope.categoryChange_receiverPricing = function(item){
      slectedUnit_receiverPricing(item);
      $scope.receiverPricingform.disableSubmit=false;
    };

    $scope.clearReceiverPricingform = function(){
      $scope.receiverPricingform={
        disableSubmit:true
      };
    };

    function setPricingBeforUpdate_ReceiverPricing(){
      //check receiver have already items 
      if($scope.receiverPricingform.receiver.pricing==undefined){
        $scope.receiverPricingform.receiver.pricing={store:[]};
        var str=angular.copy($scope.receiverPricingform.stores); 
        _.forEach(str, function(s,i){
          _.forEach(s.category, function(c,ii){
            if($scope.receiverPricingform.category._id==c._id){
              //delete str["category"];
              //str.category=[];      
              s.category.splice(ii,1);
              s.category.push(angular.copy($scope.receiverPricingform.category));
            }
          });
          $scope.receiverPricingform.receiver.pricing.store.push(s);
        });        
      }
      else
      {
        var flag=false;
        _.forEach($scope.receiverPricingform.receiver.pricing.store, function (str,i){
          
          if(str._id==$scope.receiverPricingform.store._id){
            //var str1=angular.copy($scope.receiverPricingform.store); 
            //delete str1["category"];
            //str1.category=[];
            //str.category.push(angular.copy($scope.receiverPricingform.category));
            //$scope.receiverPricingform.receiver.pricing.store.push(str);
            flag=true;
            _.forEach(str.category, function(cat,ii){
              if(cat._id==$scope.receiverPricingform.category._id){
                //cat=angular.copy($scope.receiverPricingform.category);
                $scope.receiverPricingform.receiver.pricing.store[i].category[ii]=angular.copy($scope.receiverPricingform.category);
              }
            });
          }
        });
        if(flag==false){
          var str1=angular.copy($scope.receiverPricingform.store); 
          delete str1["category"];
          str1.category=[];
          str1.category.push(angular.copy($scope.receiverPricingform.category));
          $scope.receiverPricingform.receiver.pricing.store.push(str1);
        }
      }
    };
    $scope.updateReceiverPricing= function(){
      setPricingBeforUpdate_ReceiverPricing();
            
      receiver.update({},$scope.receiverPricingform.receiver, function(result){
        var index=_.findIndex($scope.receivers, {_id:result._id});
        $scope.receivers.splice(index,1);
        $scope.receivers.push(result);
        $scope.receiverPricingform.receiver=$scope.receivers[index];
        growl.success('Successfully Updated', {ttl: 3000});
        //alert("Successfully Updated");
      });
    };

    //---------------------------------------------Vendor Pricing --------------------------------------
    $scope.vendorPricing={
      availableVendor:[],
      disableSubmit:true
    };

    $scope.clearVendorPricingTab=function(){
      $scope.vendorPricing={
        availableVendor:[],
        disableSubmit:true
      };
    };
    
    $scope.dateOptions = {};

    function VendorsByStore_VendorPricing(storeId){
      _.forEach($scope.vendors, function(v,i){
        if(v.pricing._id==storeId){
          $scope.vendorPricing.availableVendor.push($scope.vendors[i]);
        }
      });
    };

    function vendorByCategory_vendorPricing(cat){
      $scope.vendorPricing.availableVendor=[];
      $scope.vendorPricing.vendor={};
      _.forEach($scope.vendors, function(v,i){
        _.forEach(v.pricing.category,  function(e,ii){
          if(e._id==cat._id){
            $scope.vendorPricing.availableVendor.push($scope.vendors[i]);
          }
        })
        
      });
    };

    function Item_vendorByCategory_vendorPricing(ven){
       _.forEach(ven.pricing.category,  function(e,ii){
          if(e._id==$scope.vendorPricing.category._id){
            $scope.vendorPricing.categorys=e;
          }
        })
    };

    function slectedUnit_vendorPricing(item){
      $scope.vendorPricing.unit={};
      _.forEach(item , function(itm,i){
        _.forEach(itm.units , function(u,ii){
          if(itm.preferedUnit==u._id){
            $scope.vendorPricing.unit[itm._id]=u;
          }
        });
      });
    };

    $scope.onCategoryChange_vendorPricing = function(cat){
      vendorByCategory_vendorPricing(cat);
    };

    $scope.OnStoerChange_vendorPricing= function(s){
      //VendorsByStore_VendorPricing(s._id);
      $scope.vendorPricing.categorys={};
      $scope.vendorPricing.category={};
      $scope.vendorPricing.availableVendor=[];
    };

    $scope.onDepChange_vendorPricing = function(){
      $scope.vendorPricing.stores=$scope.stores;
    };

    $scope.onvendorChnage_VendorPricing = function(v){
      Item_vendorByCategory_vendorPricing(v);
      slectedUnit_vendorPricing($scope.vendorPricing.categorys.item);
      $scope.vendorPricing.disableSubmit=false;
    };

    $scope.clearVendorPricingForm= function(){
      $scope.vendorPricing={
        availableVendor:[],
        disableSubmit:true
      };
    };

    $scope.setPrefferedUnit_VendorPricing= function(unit,item_id){
      if(unit!=null)
      {
        _.forEach($scope.vendorPricing.categorys.item, function (itm,i){
          if(itm._id==item_id){
            itm["preferedUnit"]=unit._id;
          }
        });
      }
    };
    function updateAllVendorPricing(ven){
      _.forEach(ven, function(v,i){
        vendor.update({},v, function(result){
          var index=_.findIndex($scope.vendors,{_id:result._id});
          $scope.vendors.splice(index,1);
          $scope.vendors.push(result);
        });
      });
    };

    function checkValidationBeforeUpdate_vendorPricing(cat){
      var flag=false; var validationFail=false;
      _.forEach(cat.item,function(itm,i){
        if(flag==false){
          if(itm.price!="" && itm.fromDate!="" && itm.toDate!="" && itm.preferedUnit!=null){
            
          }
          else
          {
            flag=true;
          }
        }
      });
      return flag;
    };
    $scope.updateVendorPricing =function(){
      var check=checkValidationBeforeUpdate_vendorPricing($scope.vendorPricing.categorys);
      if(check==false){
        $scope.vendorPricing.vendorToBeUpdated=[];
        _.forEach($scope.vendorPricing.availableVendor, function(v,i){
          if($scope.vendorPricing.vendor._id==v._id)
          {
            _.forEachRight(v.pricing.category, function(cat,ii){
              if($scope.vendorPricing.categorys._id==cat._id){
                v.pricing.category.splice(ii,1);
                v.pricing.category.push($scope.vendorPricing.categorys);
              }
            });
            $scope.vendorPricing.vendorToBeUpdated.push($scope.vendorPricing.availableVendor[i]);
          }
        });
        updateAllVendorPricing($scope.vendorPricing.vendorToBeUpdated);
        growl.success('Successfully Updated', {ttl: 3000});
        //alert("Successfully Updated");
        console.log($scope.vendorPricing.availableVendor);
      }
      else
      {
        growl.success('Please fill the below row before update', {ttl: 3000});
        //alert("PLease fill the below row before update");
      }
    };


    //--------------------------------------------------------Pop up Store Setuo ----------------------------------//
    function openModelStoreSetup() {
            $modal.open({
                templateUrl: '/app/stock/StoreManagement/_storeSetupPopUp.html',
                controller: ['$rootScope', '$scope', '$resource', '$modalInstance','$state', function ($rootScope, $scope, $resource, $modalInstance,$state) {
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.start=function(){
                      $state.go('stock.storeMasterSetup');
                      $modalInstance.dismiss('cancel');
                    };

                    $scope.doManualy= function(){
                      $modalInstance.dismiss('cancel');
                    };
                }],
                size: 'lg'
            });
        };

    $scope.onFinishWizard= function(){
      $state.go('stock.storeManagement');
    }

    //-----------------------------------------------------Receipe Management ---------------------------------------//
    $scope.receipeForm={
  		items:[],
  		units:[],
      showReceipeForm:false
  	};

    $scope.availableReceipe={};


  	function itemsInCategory(catId){
      $scope.receipeForm.items=[];
  		_.forEach($scope.items, function(itm,i){
  			if(itm.category._id==catId){
  				$scope.receipeForm.items.push(itm);
  			}
  		});
  	};

  	function bindUnits(){
  		$scope.receipeForm.units=angular.copy($scope.stockUnits);
  	};

  	function bindItems()
  	{
  		$scope.receipeDetailForm.stockItems=angular.copy($scope.stockItems);
  	};

    function removeSelectedItemBeforeLoad(){
      _.forEachRight($scope.receipeDetailForm.stockItems, function(item,i){
        _.forEach($scope.availableReceipe,function(avItem,ii){
          if(item._id==avItem._id){
            $scope.receipeDetailForm.stockItems.splice(i,1);
          }
        });
      });
    };

    function bindAvailableRecipe(index){
      $scope.availableReceipe=$scope.stockRecipes[index].receipeDetails;
    };

  	$scope.bindItems_receipeForm= function(cat){
      if(cat==null)
      {
        $scope.receipeForm.items=[];
        $scope.receipeForm.showReceipeForm=false;
        $scope.receipeForm.units=[];
        $scope.receipeForm.selectedItem={}; 
      }
      else
      {
    		itemsInCategory(cat._id);
        $scope.receipeForm.showReceipeForm=false;
        $scope.receipeForm.units=[];
        $scope.receipeForm.selectedItem={};
      }
  	};


  	$scope.OnItemChange= function(itm){
  		bindUnits();
  		bindItems();

      var index=_.findIndex($scope.stockRecipes,{recipeName:itm.name});
      if(index>=0){
       $scope.receipeForm.selectedItem.quantity=$scope.stockRecipes[index].quantity;
       var uIndex=_.findIndex($scope.receipeForm.units,{_id:$scope.stockRecipes[index].selectedUnitId});
       $scope.receipeForm.selectedUnit=$scope.receipeForm.units[uIndex];
       bindAvailableRecipe(index);
       $scope.receipeForm.showReceipeForm=true;
      }
      else
      {
        $scope.receipeForm.selectedItem.quantity="";
        $scope.receipeForm.showReceipeForm=false;
        $scope.availableReceipe=[];
      }
      removeSelectedItemBeforeLoad();
  	};

  	$scope.createStockRecipe= function(formReceipe){
  		if(formReceipe.$valid){
  			var index=_.findIndex($scope.stockRecipes,{recipeName:$scope.receipeForm.selectedItem.name});
  			if(index>=0)
  			{
  				$scope.stockRecipes[index].selectedUnitId=$scope.receipeForm.selectedUnit._id;
	        	$scope.stockRecipes[index].quantity=$scope.receipeForm.selectedItem.quantity;
  				stockRecipe.update({},$scope.stockRecipes[index],function(result){
  					$scope.stockRecipes.splice(index,1);
  					$scope.stockRecipes.push(result);
  				});
  			}
  			else
  			{
	  			$scope.receipeForm.tenant_id=currentUser.tenant_id;
	        	$scope.receipeForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
	        	$scope.receipeForm.recipeName=$scope.receipeForm.selectedItem.name;
	        	$scope.receipeForm.itemId=$scope.receipeForm.selectedItem._id;
	        	$scope.receipeForm.categoryId=$scope.receipeForm.category._id;
	        	$scope.receipeForm.selectedUnitId=$scope.receipeForm.selectedUnit._id;
	        	$scope.receipeForm.quantity=$scope.receipeForm.selectedItem.quantity;

	        	stockRecipe.saveData({},$scope.receipeForm, function(result){
	        		$scope.stockRecipes.push(result);
              $scope.receipeForm.showReceipeForm=true;
	        	});
        	}

  		}
  	};

    $scope.goNext = function(nextIdx){
      //alert("hi");
       angular.element(document.querySelector('#f_'+nextIdx))[0].focus();           
    }

    //--------------------------------Receipe Detail Form ---------------------------------------------- 
  	$scope.receipeDetailForm={
  		stockItems:[]
  	};

    function CatWithSemiProcessed(ss){
      _.forEach(ss, function(cat,i){
        if(cat.isSemiProcessed==undefined || cat.isSemiProcessed==false){
          $scope.recipeCategory.push(cat);
          $scope.categories.push(cat);    
        }
        else{
          $scope.spCategories.push(cat);
        }
      });
      addToCategory_SemiProcessed($scope.spCategories);
    };
    function addToCategory_SemiProcessed(cat){
      _.forEach(cat, function(c,i){
        c.categoryName=c.categoryName+ "  (Semi-Processed)";
        $scope.categories.push(c);
      });
    };
  	$scope.OnItemChange_receipeDetailForm= function($item, $model, $label){
  		//console.log("items"+ $item);
  		//console.log("model"+ $model);
  		//console.log("label"+ $label);
  		_.forEach($scope.receipeDetailForm.stockItems, function(itm,i){
  		 	if(itm._id==$item._id){
          if($item.units!=undefined){
            $scope.receipeDetailForm.units=[];
            var bUName=$item.units[0].baseUnit.name;
            _.forEach($scope.stockUnits, function(u,ii){
                if(bUName==u.baseUnit.name){
                  $scope.receipeDetailForm.units.push(u);
                }
            });
          }
  		 		//$scope.receipeDetailForm.units=itm.units;
          $scope.receipeDetailForm.item=itm;
  		 	}
  		});
  	};

    function formatItem_receipeDetail(item){
      
          for( var p in item){
            if(p!="_id" && p!="itemName" && p!="preferedUnit" && p!="units"){
              delete item[p];                
            }
          }

          _.forEach(item.units, function(u){
            for(var p in u){
              if(p!="_id" && p!="unitName" && p!="conversionFactor"){
                delete u[p];
              }
            }
          });

          item["quantity"]=$scope.receipeDetailForm.quantity;
          item["selectedUnitId"]=$scope.receipeDetailForm.selectedUnits._id;
          item["selectedUnitName"]=$scope.receipeDetailForm.selectedUnits.unitName;
  };

  function clearDetailForm(){
    $scope.receipeDetailForm.selectedItem="";
    $scope.receipeDetailForm.quantity="";
    $scope.receipeDetailForm.selectedUnits={};
    $scope.receipeDetailForm.units=[];
  };

    $scope.submitRecipeDetails = function(formReceipeDetails){
      if(formReceipeDetails.$valid){
        var index=_.findIndex($scope.stockRecipes,{recipeName:$scope.receipeForm.selectedItem.name});
        if(index>=0)
        {
          formatItem_receipeDetail($scope.receipeDetailForm.item);
          $scope.stockRecipes[index].receipeDetails.push($scope.receipeDetailForm.item);
          stockRecipe.update({},$scope.stockRecipes[index], function(result){
              $scope.stockRecipes.splice(index,1);
              $scope.stockRecipes.push(result);
              var itemINdex=_.findIndex($scope.receipeDetailForm.stockItems,{itemName:$scope.receipeDetailForm.selectedItem});
              $scope.receipeDetailForm.stockItems.splice(itemINdex,1);
              $scope.availableReceipe=$scope.stockRecipes[index].receipeDetails;
              clearDetailForm();
              document.querySelector('#f_1').focus();
          });
        }
      }
    };


    

    $scope.removeRecipeDetails =function(itm)
    {
      var index=_.findIndex($scope.stockRecipes,{itemId:$scope.receipeForm.selectedItem._id});
      _.forEachRight($scope.stockRecipes[index].receipeDetails,function(it,i){
        if(it._id==itm._id){
          $scope.stockRecipes[index].receipeDetails.splice(i,1);
        }
      });
      stockRecipe.update({},$scope.stockRecipes[index], function(result){
              $scope.stockRecipes.splice(index,1);
              $scope.stockRecipes.push(result);
              var itemINdex=_.findIndex($scope.receipeDetailForm.stockItems,{itemName:$scope.receipeDetailForm.selectedItem});
              $scope.receipeDetailForm.stockItems.push(itm);
              $scope.availableReceipe=$scope.stockRecipes[index].receipeDetails;
          });
      console.log(itm);
    }

     //-------------------------Physical Stock --------------------------------------------------
    $scope.physicalStockForm={enableSubmit:true,maxDate:new Date(),billDate:new Date()};
    $scope.clearPhysicalStockTab= function(){
        $scope.physicalStockForm={enableSubmit:true,showBill:true,maxDate:new Date(),billDate:new Date()};
        $scope.physicalStockForm.tempBillNo="temp/"+$filter('date')(new Date(), 'dd-MMM-yyyy');
    };
    $scope.resetPhysicalStockTab= function(){
        if(!$scope.physicalStockForm.isEdit){
            $scope.physicalStockForm={enableSubmit:true,maxDate:new Date(),billDate:new Date()};
        }
        else{
            //edit
            $scope.physicalStockForm.isPrint=false;
            $scope.physicalStockForm.isEdit=false;
            $scope.physicalStockForm.isSaved=false;
        }
    };
    $scope.tempBill_physicalStockForm= function(billDate){
        $scope.physicalStockForm.showBill=true;
        $scope.physicalStockForm.tempBillNo="temp/"+$filter('date')(billDate, 'dd-MMM-yyyy');
    };

    function slectedUnit_physicalStockForm(item){
        $scope.physicalStockForm.selectedUnits={};
        _.forEach(item , function(itm,i){
            _.forEach(itm.units , function(u,ii){
              if(itm.preferedUnit==u._id){
                $scope.physicalStockForm.selectedUnits[itm._id]=u;
              }
            });
        });
    };

    $scope.addRemainingItem_physicalStockForm= function($item, $model, $label){
        var remaining=angular.copy($scope.physicalStockForm.remainingItems);
        _.forEach($scope.physicalStockForm.remainingItems, function(itm,i){
            if(itm._id==$item._id){
                $scope.physicalStockForm.availableItems.push(itm);
                remaining.splice(i,1);
                $scope.physicalStockForm.itemToAdd="";
            }
        });
        $scope.physicalStockForm.remainingItems=remaining;
        slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
    };
    
    $scope.deleteSelectedItemPhysicalForm= function(item){
        var index=_.findIndex($scope.physicalStockForm.availableItems,{"itemName":item.itemName});
        $scope.physicalStockForm.availableItems.splice(index,1);
        $scope.physicalStockForm.remainingItems.push(item);
    };

    $scope.bindCategory_physicalStockForm = function(stores){
        var store=angular.copy(stores);
        $scope.physicalStockForm.availableCategory=[];
        $scope.physicalStockForm.availableItems=[];
        $scope.physicalStockForm.remainingItems=[];
        _.forEach(store.category , function(c, ii){
            $scope.physicalStockForm.availableCategory.push(c);
            _.forEach(c.item, function(item,iii){
                var varFromCategory={"_id":c._id,"categoryName":c.categoryName};
                item["fromCategory"]=varFromCategory;
                $scope.physicalStockForm.remainingItems.push(item);
            });
        });
        //slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
    };
    $scope.bindItems_physicalStockForm = function(catId){
        //var store=angular.copy(stores);
        //$scope.physicalStockForm.availableItems=[];
        var cat=angular.copy($scope.physicalStockForm.availableCategory);
        _.forEach(cat , function(c, ii){
            if($scope.physicalStockForm.selectedCategory[catId]==true){
                if(c._id==catId){
                    _.forEach(c.item , function(itm,ii){
                        var aa = _.findIndex($scope.physicalStockForm.availableItems,{"itemName":itm.itemName});
                        if(aa<0){
                            var varFromCategory={"_id":c._id,"categoryName":c.categoryName};
                            itm["fromCategory"]=varFromCategory;
                            $scope.physicalStockForm.availableItems.push(itm);
                            var index=_.findIndex($scope.physicalStockForm.remainingItems,{"itemName":itm.itemName});
                            $scope.physicalStockForm.remainingItems.splice(index,1);
                        }
                    });
                }
            }
            else{
                if(c._id==catId){
                    _.forEach(c.item , function(itm,ii){
                        var index=_.findIndex($scope.physicalStockForm.availableItems,{_id:itm._id});
                        $scope.physicalStockForm.availableItems.splice(index,1);
                        $scope.physicalStockForm.remainingItems.push(itm);
                    });
                }
            }
        });
        slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
    };
    $scope.goNextPhysicalStock= function(nextIdx){
        var f=angular.element(document.querySelector('#phy_'+nextIdx));
        if(f.length!=0){  
            f.focus();  
        }
        else
        {
            $scope.physicalStockForm.enableSubmit=false;
            angular.element(document.querySelector('#submitPhysicalStock')).focus();
        }    
    };
    function getItemToSave_PhysicalStock(){
        var items=angular.copy($scope.physicalStockForm.availableItems);
        _.forEachRight(items, function(itm,i){
            if(itm.qty!="" || itm.qty!=undefined){
                var flag=false;
                _.forEach($scope.physicalStockForm.selectedUnits, function(u,ii){
                    if(ii==itm._id && itm.qty!="" && itm.qty!=undefined && u!=null){
                        var selectedUnit={"_id":u._id,"unitName":u.unitName};
                        itm["selectedUnit"]=selectedUnit;
                        flag=true;
                    }
                });
                if(flag==false){
                    items.splice(i,1);
                }
            }
        });
        _.forEach(items, function(itm,i){
            for(var p in itm){
                    if(p!="_id" && p!="itemName" && p!="qty" && p!="preferedUnit" && p!="units" && p!="selectedUnit" && p!="fromCategory"){
                        _.forEach(itm.units, function(u,ii){
                            for(var p in u){
                                if(p!="_id" && p!="unitName"){
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
        });
        return items;
    };
    function formatStore_PhysicalStockForm(store){
        var stores=angular.copy(store);
        for(var p in stores){
            if(p!="_id" && p!="storeName"  && p!="storeLocation" && p!="storeUID"){
                delete stores[p];
            }
        }
        return stores;
    };
    $scope.submitPhysicalStock = function(formPhysicalStock){
        //check for necessary data before submit
        var storeToSave=formatStore_PhysicalStockForm($scope.physicalStockForm.store);
        var itemToSave=getItemToSave_PhysicalStock();
        console.log(itemToSave);
        if($scope.physicalStockForm.transactionId!=undefined){
            //alert("trans present");
            StockResource.remove({},$scope.physicalStockForm, function (result){  
            });
        }
        $scope.physicalStockForm.transactionId=guid();
        if(formPhysicalStock.$valid && !$scope.physicalStockForm.isPrint){
          var physicalStockFormToSave={};
            physicalStockFormToSave.transactionid=$scope.physicalStockForm.transactionId;
            physicalStockFormToSave._id=physicalStockFormToSave.transactionid
            physicalStockFormToSave.transactionType="6";
            physicalStockFormToSave.user=currentUser;
            physicalStockFormToSave._store=storeToSave;
            physicalStockFormToSave.created=new Date().toISOString();
            physicalStockFormToSave._items=itemToSave;
            physicalStockFormToSave._vendor=null;
            physicalStockFormToSave.deployment_id=currentUser.deployment_id;
            physicalStockFormToSave.tenant_id=currentUser.tenant_id;
            // var trans=new stockTransactions(currentUser, $rootScope.db,itemToSave,storeToSave,null,null);
            StockResource.save({},physicalStockFormToSave, function (result){
              $scope.physicalStockForm._id=result._id;
                growl.success('Inserted Successfully', {ttl: 3000});
                growl.success(itemToSave.length+' Items Inserted', {ttl: 3000});
                //$scope.physicalStockForm={enableSubmit:true};
                $scope.physicalStockForm.isPrint=true;
                $scope.physicalStockForm.isEdit=true;
                $scope.physicalStockForm.isSaved=true;
            });
        }
        else{
            //Print
        }       
    };

    var guid = (function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return function () {
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
            };
        })();



  }]);

