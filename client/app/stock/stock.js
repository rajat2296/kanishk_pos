'use strict';
'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('stock', {
        url: '/stock',
        templateUrl: 'app/stock/stock.html',
        controller: 'StockCtrl',
        resolve: {

          currentUser: ['$rootScope', '$state', '$stateParams', 'Auth', 'Tab', 'growl',
            function ($rootScope, $state, $stateParams, Auth, Tab,growl) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                $rootScope.currentUser = user;
                return user;
              });
            }],
          stockSettings: ['$rootScope', '$state', '$stateParams', 'localStorageService','property',
            function ($rootScope, $state, $stateParams, localStorageService,property) {
              console.log(localStorageService.get('tenant_id'),localStorageService.get('deployment_id'));
              return property.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:localStorageService.get('deployment_id')}).$promise.then(function (result){
                localStorageService.set('stockSetting', result[0]);
                return result;
              });
            }],
          syncAll:['$rootScope','stockSync', '$modal', '$q','localStorageService','growl','$location','$state',function($rootScope,stockSync,$modal, $q,localStorageService,growl,$location,$state){
            var d = $q.defer();
            if (!localStorageService.get('deployment_id')){
              //$timeout(function() { $state.go('admin'); });
              //$state.go('/');
              growl.error('No Deployment ID set, cannot start stock!', {ttl:3000});
              d.reject();
            }
            else {
              //var d = $q.defer();
              $modal.open({
                template: '<h4 style="margin:20px;"><i class="fa fa-spinner fa-spin"></i> Syncing Stock, please wait...</h4>',
                backdrop: 'static',
                keyboard: 'false',
                controller: ['$scope','$rootScope','Sync', 'stockSync', '$modalInstance', '$timeout', function ($scope,$rootScope,Sync ,stockSync, $modalInstance, $timeout) {
                  $timeout(function () {
                    stockSync.syncAll().then(function (data) {
                      $modalInstance.close("Sync Complete");
                    }).catch(function () {
                      $modalInstance.close("Sync error... loading from offline data.");
                      d.resolve();
                    });
                  }, 10);
                }],
                size: 'sm'
              }).result.then(function (message) {
                d.resolve(message);
              });
            }
            return d.promise;
            /*return Sync.syncAll().then(function(data){
             console.log($rootScope.tabs);
             // return data;
             })*/

          }]

          // closingQty: ['$rootScope', 'StockReportNew','localStorageService', function ($rootScope, StockReportNew,localStorageService) {
          //       $rootScope.closingQty = [];
          //       var  startDate=new Date();
          //       startDate.setHours(0,0,0,0);
          //       var endDate=new Date();
          //       endDate.setHours(0,0,0,0);
          //       endDate.setDate(endDate.getDate() + 1);
          //       var req={fromDate:startDate,toDate:endDate,deployment_id:localStorageService.get('deployment_id'),tenant_id:localStorageService.get('tenant_id')}

          //       return StockReportNew.getClosingQty(req).$promise.then(function (trans) {
          //         console.log(trans);
          //           return $rootScope.closingQty = trans;
          //       });

          //    }]
        }
      })
      .state('stock.storeManagement',{
        url:'/storeManagement',
        templateUrl:'app/stock/storeManagement/storeManagement.html',
        controller: 'StoreManagementCtrl',
        resolve:{
          // stockTransaction: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //           $rootScope.stockTransactions = [];
          //           return stockSync.syncOfflineTransaction($rootScope.currentUser).then(function (trans) {
          //               return $rootScope.stockTransaction = trans;
          //           });

          //       }]
          //Modified
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'Store Management')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission access store management !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
          //end
        }
      })

      .state('stock.storeMasterSetup', {
        url: '/storeMasterSetup',
        templateUrl: 'app/stock/storeMasterSetup/storeMasterSetup.html',
        controller: 'StoreMasterSetupCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth',
            function ($state, $stateParams, Auth) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                return user;
              });
            }]
        }
      })
      .state('stock.stockSetting', {
        url: '/stockSetting',
        templateUrl: 'app/stock/stockSetting/stockSetting.html',
        controller: 'StockSettingCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
               /* if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'Stock Settings')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission access stock settings !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }*/
                return user;
              });
            }],
        }
      })
      .state('stock.recipeManagement', {
        url: '/recipes',
        templateUrl: 'app/stock/recipeManagement/recipeManagement.html',
        controller: 'RecipeManagementCtrl'
      })
      .state('stock.stockReturn', {
        url: '/stockReturn',
        templateUrl: 'app/stock/stockReturn/stockReturn.html',
        controller: 'StockReturnCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth',
            function ($state, $stateParams, Auth) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                return user;
              });
            }]
        }
      })

      .state('stock.editStockEntry', {
        url: '/stockEntry/:id/:transactionType',
        templateUrl: 'app/stock/stockEntry/stockEntry.html',
        params: {
          id: null,
          transactionType: null
        },
        controller: 'StockEntryCtrl',
        resolve:{
          // items: ['$rootScope', 'Sync', function ($rootScope, Sync) {

          //               /* Items Table */
          //               $rootScope.items = [];
          //               return Sync.syncItems($rootScope.currentUser).then(function (items) {
          //                   return $rootScope.items = items;
          //               });

          //           }],
          // stockTransaction: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //               $rootScope.stockTransactions = [];
          //               return stockSync.syncOfflineTransaction($rootScope.currentUser).then(function (trans) {
          //                   return $rootScope.stockTransaction = trans;
          //               });

          //           }],
          // stockTemplate: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //               $rootScope.stockTemplate = [];
          //               return stockSync.syncOfflineTemplates($rootScope.currentUser).then(function (trans) {
          //                   return $rootScope.stockTemplate = trans;
          //               });

          //           }]
          //Modified
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'Edit Entry')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to edit stock entries !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          //end
          Recipes: ['$rootScope', 'stockRecipe','localStorageService', function ($rootScope, stockRecipe,localStorageService) {
            $rootScope.Recipes = [];
            console.log('js Recipes', $rootScope.Recipes);
            return stockRecipe.get({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              $rootScope.Recipes = trans;
              console.log('js Recipes', $rootScope.Recipes);
              return $rootScope.Recipes;
            });
          }],
          LastPriceRaw: ['$rootScope', 'StockReport','localStorageService', function ($rootScope, StockReport,localStorageService) {
            $rootScope.Recipes = [];
            return StockReport.RawMaterialPricing_Receipe({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              return trans;
            });
          }],
          LastPriceBK: ['$rootScope', 'StockReport','localStorageService', function ($rootScope, StockReport,localStorageService) {
            $rootScope.Recipes = [];
            return StockReport.RawMaterialPricing_ReceipeBasekitchen({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              return trans;
            });
          }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
        }
      })

      .state('stock.stockEntry', {
        url: '/stockEntry',
        templateUrl: 'app/stock/stockEntry/stockEntry.html',
        controller: 'StockEntryCtrl',
        resolve:{
          // availableStore:['$rootScope', '$state', '$stateParams', 'Auth','store' , 'growl',
          //               function ($rootScope, $state, $stateParams, Auth, store,growl) {
          //                   return store.get({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then (function (stores){
          //                     $rootScope.availableStore=stores;
          //                     return stores;
          //                   });
          //               }],
          // stores: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {

          //               /* Items Table */
          //               $rootScope.stores = [];
          //               return stockSync.syncStores($rootScope.currentUser).then(function (stores) {
          //                   return $rootScope.stores = stores;
          //               });

          //           }],
          // receivers: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {

          //               /* Items Table */
          //               $rootScope.receivers = [];
          //               return stockSync.syncReceivers($rootScope.currentUser).then(function (receivers) {
          //                   return $rootScope.receivers = receivers;
          //               });

          //           }],
          // vendors: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {

          //               /* Items Table */
          //               $rootScope.vendors = [];
          //               return stockSync.syncVendors($rootScope.currentUser).then(function (vendors) {
          //                   return $rootScope.vendors = vendors;
          //               });

          //           }],

          // items: ['$rootScope', 'Sync', function ($rootScope, Sync) {

          //               /* Items Table */
          //               $rootScope.items = [];
          //               return Sync.syncItems($rootScope.currentUser).then(function (items) {
          //                   return $rootScope.items = items;
          //               });

          //           }],
          // stockTransaction: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //               $rootScope.stockTransactions = [];
          //               return stockSync.syncOfflineTransaction($rootScope.currentUser).then(function (trans) {
          //                   return $rootScope.stockTransaction = trans;
          //               });

          //           }],
          // stockTemplate: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //               $rootScope.stockTemplate = [];
          //               return stockSync.syncOfflineTemplates($rootScope.currentUser).then(function (trans) {
          //                   return $rootScope.stockTemplate = trans;
          //               });

          //           }]
          Recipes: ['$rootScope', 'stockRecipe','localStorageService', function ($rootScope, stockRecipe,localStorageService) {
            $rootScope.Recipes = [];
            //console.log('js Recipes', $rootScope.Recipes);
            return stockRecipe.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:localStorageService.get('deployment_id')}).$promise.then(function (trans) {
              //console.log('js Recipes', $rootScope.Recipes);
              return $rootScope.Recipes = trans;
            });

          }],
          LastPriceRaw: ['$rootScope', 'StockReport','localStorageService', function ($rootScope, StockReport,localStorageService) {
            $rootScope.Recipes = [];
            return StockReport.RawMaterialPricing_Receipe({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              return trans;
            });
          }],
          LastPriceBK: ['$rootScope', 'StockReport','localStorageService', function ($rootScope, StockReport,localStorageService) {
            $rootScope.Recipes = [];
            return StockReport.RawMaterialPricing_ReceipeBasekitchen({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              return trans;
            });
          }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
        }
      })
      .state('stock.indentGrn', {
        url: '/indentGRN',
        templateUrl: 'app/stock/indentGRN/indentGRN.html',
        controller: 'IndentGRNCtrl',
        resolve:{
          LastBillNo: ['$rootScope', 'stockRequirement','localStorageService', function ($rootScope, stockRequirement,localStorageService) {
                       return stockRequirement.getLastRequirementBillNo({tenant_id:localStorageService.get('tenant_id'),deployment_id:localStorageService.get('deployment_id')}).$promise.then(function (trans) {
              return trans.billNo;
            });

          }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
        }
      })
      .state('stock.editentries', {
        url: '/editentries',
        templateUrl: 'app/stock/editentries/editentries.html',
        controller: 'EditentriesCtrl',
        resolve: {     //modified
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'Edit Entry')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to edit stock entries !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
               currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
       
        }
      })
      .state('stock.vendorPayment', {
        url: '/vendorPayment',
        templateUrl: 'app/stock/vendorPayment/vendorPayment.html',
        controller: 'VendorPaymentCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth',
            function ($state, $stateParams, Auth) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                return user;
              });
            }],
          VPayment: ['$rootScope', '$state', '$stateParams', 'VendorPaymentHistory',
            function ($rootScope, $state, $stateParams, VendorPaymentHistory) {
              return VendorPaymentHistory.getTransactionHistoryByTransactionId({}).$promise.then(function (result){
                console.log(result);
                return result;
              });
            }]

        }
      })

      .state('stock.stockImportExport', {
        url: '/stockImportExport',
        templateUrl: 'app/stock/stockImportExport/stockImportExport.html',
        controller: 'StockImportExportCtrl'
      })

      .state('stock.stockReports', {
        url: '/stockReports',
        templateUrl: 'app/stock/stockReports/stockReports.html',
        controller: 'StockReportsCtrl',
        resolve: {
          currentUser: ['$rootScope','$state', '$stateParams', 'Auth',
            function ($rootScope,$state, $stateParams, Auth) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                $rootScope.currentUser=user;
                return user;
              });
            }]
        }
      })
      .state('stock.purchaseOrder', {
        url: '/purchaseOrder',
        templateUrl: 'app/stock/purchaseOrder/purchaseOrder.html',
        controller: 'PurchaseOrderCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                //var flag = false;
                //if (user.role === 'user') {
                //  if (Utils.isUserPermitted(user, 'Stock Settings')) {
                //    flag = true;
                //  }
                //  if (!flag) {
                //    growl.error('No permission access stock settings !!', {ttl: 3000});
                //    Auth.logout();
                //    $location.path('/login');
                //    return false;
                //  }
                //}
                return user;
              });
            }],
            purchaseNumber: ['purchaseOrder', 'localStorageService', function (purchaseOrder, localStorageService) {
            return purchaseOrder.getLastPurchaseOrderNumber({deployment_id: localStorageService.get('deployment_id')}, function (poNumber) {
              console.log(poNumber);
              return poNumber;
            });
          }]
        }
      })
      .state('stock.performStockEntry', {
        url: '/stockEntry/:id',
        templateUrl: 'app/stock/stockEntry/stockEntry.html',
        params: {
          id: null
        },
        controller: 'StockEntryCtrl',
        resolve:{
          // items: ['$rootScope', 'Sync', function ($rootScope, Sync) {

          //               /* Items Table */
          //               $rootScope.items = [];
          //               return Sync.syncItems($rootScope.currentUser).then(function (items) {
          //                   return $rootScope.items = items;
          //               });

          //           }],
          // stockTransaction: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //               $rootScope.stockTransactions = [];
          //               return stockSync.syncOfflineTransaction($rootScope.currentUser).then(function (trans) {
          //                   return $rootScope.stockTransaction = trans;
          //               });

          //           }],
          // stockTemplate: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //               $rootScope.stockTemplate = [];
          //               return stockSync.syncOfflineTemplates($rootScope.currentUser).then(function (trans) {
          //                   return $rootScope.stockTemplate = trans;
          //               });

          //           }]
          //Modified
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'Edit Entry')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to edit stock entries !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          //end
          Recipes: ['$rootScope', 'stockRecipe','localStorageService', function ($rootScope, stockRecipe,localStorageService) {
            $rootScope.Recipes = [];
            console.log('js Recipes', $rootScope.Recipes);
            return stockRecipe.get({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              $rootScope.Recipes = trans;
              console.log('js Recipes', $rootScope.Recipes);
              return $rootScope.Recipes;
            });
          }],
          LastPriceRaw: ['$rootScope', 'StockReport','localStorageService', function ($rootScope, StockReport,localStorageService) {
            $rootScope.Recipes = [];
            return StockReport.RawMaterialPricing_Receipe({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              return trans;
            });
          }],
          LastPriceBK: ['$rootScope', 'StockReport','localStorageService', function ($rootScope, StockReport,localStorageService) {
            $rootScope.Recipes = [];
            return StockReport.RawMaterialPricing_ReceipeBasekitchen({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then(function (trans) {
              return trans;
            });
          }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
        }
      })
      .state('stock.outletIndenting', {
        url: '/outletIndenting',
        templateUrl: 'app/stock/outletIndenting/outletIndenting.html',
        controller: 'OutletIndentingCtrl',
        resolve:{
          LastBillNo: ['$rootScope', 'stockRequirement','localStorageService', function ($rootScope, stockRequirement,localStorageService) {
                       return stockRequirement.getLastRequirementBillNo({tenant_id:localStorageService.get('tenant_id'),deployment_id:localStorageService.get('deployment_id')}).$promise.then(function (trans) {
              return trans.billNo;
            });

          }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
        }
      })
      .state('stock.storeManagementTemp',{
        url:'/storeManagementTemp',
        templateUrl:'app/stock/storeManagement/storeManagementTemp.html',
        controller:'StoreManagementTempCtrl',
        resolve:{
          // stockTransaction: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //           $rootScope.stockTransactions = [];
          //           return stockSync.syncOfflineTransaction($rootScope.currentUser).then(function (trans) {
          //               return $rootScope.stockTransaction = trans;
          //           });

          //       }]
          //Modified
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'Store Management')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission access store management !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
          //end
        }
      })
      .state('stock.storeManagementTemp2',{
        url:'/storeManagementTemp2',
        templateUrl:'app/stock/storeManagement/storeManagementTemp2.html',
        controller:'StoreManagementTemp2Ctrl',
        resolve:{
          // stockTransaction: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //           $rootScope.stockTransactions = [];
          //           return stockSync.syncOfflineTransaction($rootScope.currentUser).then(function (trans) {
          //               return $rootScope.stockTransaction = trans;
          //           });

          //       }]
          //Modified
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'Store Management')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission access store management !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          currentDeployment: ['$http', 'localStorageService', function ($http, localStorageService) {
            return $http({
              url: '/api/deployments/' + localStorageService.get('deployment_id'),
              method: 'GET'
            }).then(function success(result) {
              return result.data;
            })
          }]
          //end
        }
      })
      .state('stock.demand', {
        url: '/demand',
        templateUrl: 'app/stock/LN/LN.html',
        controller: 'LNCtrl',
        resolve:{
          LastBillNo: ['$rootScope', 'stockRequirement','localStorageService', function ($rootScope, stockRequirement,localStorageService) {
            return stockRequirement.getLastRequirementBillNo({tenant_id:localStorageService.get('tenant_id'),deployment_id:localStorageService.get('deployment_id')}).$promise.then(function (trans) {
              return trans.billNo;
            });

          }]
        }
      }).state('stock.stockMasterUser', {
        url: '/stockMasterUser',
        templateUrl: 'app/stock/stockMasterUser/stockMasterUser.html',
        controller: 'StockMasterUserCtrl',
        resolve: {
           //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                return user;
              });
            }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      });
  });
