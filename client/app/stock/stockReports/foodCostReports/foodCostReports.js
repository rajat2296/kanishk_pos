'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('foodCostReports', {
        url: '/foodCostReports',
        templateUrl: 'app/stock/stockReports/foodCostReports/foodCostReports.html',
        controller: 'FoodCostReportsCtrl'
      });
  });