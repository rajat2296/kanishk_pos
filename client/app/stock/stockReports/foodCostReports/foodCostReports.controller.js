'use strict';

angular.module('posistApp')
    .controller('FoodCostReportsCtrl',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','StockReport','localStorageService','StockReportNew','stockItem','StockReportService','baseKitchenItem', 'deployment', 'Utils', 'stockReportResetTime', 'property', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,StockReport,localStorageService,StockReportNew,stockItem,StockReportService,baseKitchenItem, deployment, Utils, stockReportResetTime, property) {
      currentUser.deployment_id=localStorageService.get('deployment_id');
      $scope.stores=[];
      $scope.stockitems=[];
      $scope.user=currentUser;

      var kitchenStores;
      var allowBaseKitchenItemWastage = false;


      $scope.dateWiseForm={
        
        fromDate: new Date(new Date().setDate(new Date().getDate() - 3)),
        toDate: new Date(new Date().setDate(new Date().getDate() - 3)),
        maxDate: new Date(new Date().setDate(new Date().getDate() - 3))
      };

      $scope.stockSettings =[];
      var consumptionViaPhysical =false;

      function getStockSettings() {
      var deferred = $q.defer();
      property.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        console.log('settings result',result);
        var isPhysical =false;
        if(result == '')
                  isPhysical=false;
                else if(result[0].isPhysicalStock == undefined)
                  isPhysical=false;
                else
                  isPhysical=result[0].isPhysicalStock;

        deferred.resolve(isPhysical);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      //return baseKitchenItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    }

      //    function getStores_ByDeployment(){
      //      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
      //    };
      //    var allPromise=$q.all([
      //        getStores_ByDeployment()
      // ]);
      //    allPromise.then(function (value){
      //        $scope.stores = value[0];
      //        $scope.purchaseConsumptionForm.stores=$scope.stores;
      //        //$scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0]._id;
      // });

      function getRawPricing() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeConsumptionReport({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /* return StockReport.RawMaterialPricing_Receipe({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    };

    function getRawPricingBasekitchen() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeBasekitchenConsumptionReport({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /*  return StockReport.RawMaterialPricing_ReceipeBasekitchen({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    }
    $scope.consolidatedForm={}
    $scope.consumptionViaPhysicalForm={}
      store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.stores=result;
         kitchenStores = _.filter($scope.stores, function (store) {
          return store.isKitchen == true;
           });
          if(kitchenStores.length == 1){
              allowBaseKitchenItemWastage = true;
          }
        
        $scope.consolidatedForm.stores=result;
        $scope.consumptionViaPhysicalForm.stores=result;
        $scope.consolidatedForm.store=$scope.consolidatedForm.stores[0];
        $scope.consumptionViaPhysicalForm.store=$scope.consumptionViaPhysicalForm.stores[0];
        // $scope.bindCategory_consolidatedForm($scope.consolidatedForm.store);
        // $scope.bindCategory_ConsumptionViaPhysicalForm($scope.consumptionViaPhysicalForm.store);
        //$scope.dateWiseForm.stores=result
        $scope.dateWiseForm={};
        $scope.dateWiseForm.store=result[0]
        ResetclearDateWiseTab()
        //$scope.bindCategory_DateWiseForm($scope.dateWiseForm.store);
        stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
          $scope.stockitems=result;
          baseKitchenItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (items){
            $scope.basekitchenitems=items;
            _.forEach(items,function(itm,i){
              $scope.stockitems.push(itm);
          });
          });
        });
        // StockReportNew.getLastPriceOfItem_RawMaterial({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        //   $scope.lastPrice=result;
        //   //console.log($scope.lastPrice);
        // });
      });

      $q.all([
          getRawPricing(),
          getRawPricingBasekitchen(),
          getStockSettings()
        ]).then(function (value) {
            $scope.lastPrice=value[0];
            _.forEach(value[1], function (price) {
                $scope.lastPrice.push(price);
            });

            console.log("last Price",$scope.lastPrice);
            $scope.stockSettings =value[2];
            console.log('$scope.stockSettings',value[2]);
            //if(value[2] == '')
              //    consumptionViaPhysical=false;
                //else if(value[2].isPhysicalStock == undefined)
                 // consumptionViaPhysical=false;
                //else
                  consumptionViaPhysical=value[2];

            //consumptionViaPhysical =$scope.stockSettings[0].isPhysicalStock;
            $scope.isPhysicalStock = true;
            if(consumptionViaPhysical)
            {

            growl.success('Consumption will be shown via physical',{ttl:3000})
            }
            else
            {

            growl.success('Consumption will not be shown via physical',{ttl:3000})
            }
            console.log('consumptionViaPhysical',consumptionViaPhysical);
        }).catch(function (err) {
            console.log('Error fetching base kitchen items');
        });


    
      $scope.clearDateWiseTab= function(){
        ResetclearDateWiseTab();
      };

    
     

      function ResetclearDateWiseTab(){
        $scope.dateWiseForm={
          fromDate: new Date(new Date().setDate(new Date().getDate() - 3)),
          toDate: new Date(new Date().setDate(new Date().getDate() - 3)),
          maxDate: new Date(new Date().setDate(new Date().getDate() - 3)),
          stores:angular.copy($scope.stores)
        };
        $scope.dateWiseForm.store=$scope.dateWiseForm.stores[0];
        $scope.bindCategory_DateWiseForm($scope.dateWiseForm.store);
      };
      $scope.checkDateGreater_DateWise= function(date){
        var d=new Date(date);
        var dd=new Date($scope.dateWiseForm.toDate);
        if(d.getTime()>dd.getTime()){
          growl.success('Greater than toDate', {ttl: 3000});
          $scope.dateWiseForm.toDate= new Date(new Date().setDate(new Date().getDate() - 3));
          $scope.dateWiseForm.fromDate=new Date(new Date().setDate(new Date().getDate() - 3));//getYesterDayDate()
        }
      };

      $scope.checkDateLesser_DateWise= function(date){
        var d=new Date(date);
        var dd=new Date($scope.dateWiseForm.fromDate);
        if(d.getTime()<dd.getTime()){
          growl.success('Can not be Lesser than fromDate', {ttl: 3000});
          $scope.dateWiseForm.toDate= new Date(new Date().setDate(new Date().getDate() - 3));
          $scope.dateWiseForm.fromDate=new Date(new Date().setDate(new Date().getDate() - 3));//getYesterDayDate()
        }
      };

    
       $scope.bindCategory_DateWiseForm = function(store) {

            var kitchenStores = _.filter($scope.stores, function(store) {
                return store.isKitchen == true;
            });

            $scope.dateWiseForm.categories = [];
            $scope.dateWiseForm.availableItems = [];
            var str = angular.copy(store);
            if (allowBaseKitchenItemWastage && store.isKitchen) {
                var bItems = [];
                var baseKitchenItemsCat = { _id: "PosistTech333333", categoryName: "Base Kitchen Items" };
                console.log($scope.basekitchenitems);
                _.forEach($scope.basekitchenitems, function(bItem) {
                    var item = {
                        itemName: bItem.itemName,
                        _id: bItem.itemId,
                        itemId: bItem.itemId,
                        itemType: "Base Kitchen Item",
                        // fromCategory: angular.copy(baseKitchenItemsCat),
                        units: bItem.units,
                        preferedUnit: bItem.preferedUnit
                    }
                    bItems.push(item);

                }); // End of For Each
                baseKitchenItemsCat.item = bItems;
                console.log(baseKitchenItemsCat)
                console.log('str', store);
                $scope.dateWiseForm.categories.push(baseKitchenItemsCat)
                console.log($scope.dateWiseForm);
            }
            _.forEach(str.category, function(c, i) {
                $scope.dateWiseForm.categories.push(c);
            });


        };
     
      $scope.startSearch_DateWise=function(email){
        console.log("settings",deployment.settings)
        var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dateWiseForm.fromDate))));
        var toDate = $scope.dateWiseForm.toDate;
        var chkStart = moment(fromDate);
        var chkEnd = moment(toDate);
        var duration = moment.duration(chkEnd.diff(chkStart));
        if(duration.asMonths() > 1){
          growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
          //$scope.purchaseConsumptionForm.isSearch=false;
          return;
        }
        var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dateWiseForm.toDate))));
        var toDate1=new Date(toDateReset);
        toDate1.setDate(toDate1.getDate());
          if ($scope.dateWiseForm.item != undefined || $scope.dateWiseForm.item != null) {
                var item = [{ id: $scope.dateWiseForm.item._id, itemName: $scope.dateWiseForm.item.itemName }];
            } else if ($scope.dateWiseForm.category != undefined || $scope.dateWiseForm.category != null) {
                var item = [];
                _.forEach($scope.dateWiseForm.category.item, function(itm, i) {
                    var obj = { id: itm._id, itemName: itm.itemName };
                    item.push(obj);
                });
            } else if ($scope.dateWiseForm.store != undefined) {
              //
            };
        //console.log(new Date(toDate1))
        var req={"startDate":fromDate,"endDate":toDate1,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, report: 5, email: email,consumptionViaPhysical:consumptionViaPhysical};
        // StockReport.getConsumptionSummary_DateWise(req, function (result){
        //  console.log(result);
        //  var data=formatDataDateWise(result);
        //  console.log(data);
        //  var ss=formatDataDatewise_Date(data)
        //  console.log(ss);
        //  $scope.dateWiseForm.reportData=ss;
        // });
        var day=[]
        day=days(fromDate,toDate1)
        // console.log("days",day)
        stockReportResetTime.getBillsInDataForFoodCost(req,function(bill){
        console.log("bill",bill)
        //$scope.bills-angular.copy(bill)
        var dateWise=new StockReportService('',fromDate,toDate1,item,$scope.lastPrice,$scope.stockitems, deployment,consumptionViaPhysical);

        
        dateWise.getFoodCOstDateWiseReport(req).then(function(val){
          console.log("food",val)
          console.log(day)
          
                _.forEach(day,function(d){

                  d.actual=0
                  d.ideal=0
                  _.forEach(val,function(str){
                    if(str)
                     _.forEach(str,function(data){
                          if(data.date!=undefined)
                          {

                          //console.log(angular.copy(data),"dataaaaaa")
                          var d1=new Date(d.date)
                          // console.log("---------")
                          // console.log("data",data)
                          Date(Utils.getDateFormatted(Utils.getResetDate(angular.copy(deployment.settings), Utils.convertDate(data.date))));
                          var d2=new Date(data.date )
                           //console.log("date",d1)
                           //console.log("day",d2)
                          // d1=d1.getDay()+d1.getDate() +d1.getMonth()
                          // d2=d2.getDay()+d2.getDate() +d2.getMonth()
                          //  console.log("after date",d1)
                          //  console.log("after day",d2)
                          // console.log("month",d2.getMonth())
                             if(d1.getTime() ==d2.getTime() )
                                {
                                 //console.log("matchhhhhhhhhhhhhhhhhh") 
                                 //console.log(data.actualCost,data.idealCost)
                                  d.actual+=data.actualCost;
                                  d.ideal+=data.idealCost
                                  
                                }
                                else
                                {
                                 // console.log("not matchhhhhhhhh") 
                                  //console.log(data.actualCost,data.idealCost)
                                  d.actual+=0;
                                  d.ideal+=0
                                  
                                }
                         }
                         })
                        

                     })
                 })
                var count=0
                _.forEach(day,function(d){
                  d.sale=0
                    _.forEach(bill,function(b){
                      if(b.day==count)
                        d.sale=b.totalNet
                    
                    })
                    count++
                })
          console.log(day,"days")      
          dynamicFoodReport_DateWise(day)
              
           
         // console.log("finalllllllllllll",day)
          //console.log(val);
          //dynamicVarianceReport_DateWise(val);

          
          $scope.dateWiseForm.reportData=val;
          },function(err){
          //console.log(err);
          $scope.dateWiseForm.reportData = {};
          $scope.dateWiseForm.reportData.errorMessage = err;
          });
        }); // end of getBillsInDataForFoodCost
        

      };

      function days(fromDate,toDate) {
          var dateArray = new Array();
          var currentDate = fromDate;
          var toDate = new Date(toDate);
          //console.log('28 ', toDate);
          toDate.setDate(toDate.getDate());
          while (currentDate <= toDate) {
            dateArray.push({date: Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(currentDate)))});
            currentDate = currentDate.addDays(1);
            //console.log(currentDate);
          }
          return dateArray;
        }
      function dynamicFoodReport_DateWise(val){
        console.log('val',val);

        var html="<table class='table table-curved' style='width:100%' border='1' ng-show='dateWiseForm.reportData.length>0'>";
        html+="<thead >";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Date </th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Ideal Cost</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Ideal %</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Actual Cost</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Actual %</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Sale</th>";
      
       

        html+="</thead>";

        html+=" <tbody>";
        
        _.forEach(val, function (d) {
        //  var date=d.date.getDate() + '/' + (d.date.getMonth()+1) + '/' + d.date.getFullYear();
          html += "<tr>";

        //  html+="<td style='background-color:#abcabc' colspan='10'>+moment(+new Date(d.date)).format('YYYY-MM-DD')+"</td>"";
          html += "<td style='background-color:#abcabc'' >" + moment(new Date(d.date)).format('DD-MM-YYYY')  + "</td>";
          html += "<td>" + parseFloat(d.ideal).toFixed(2) + "</td>";
          if(d.sale!=0)
            html += "<td>" + parseFloat(d.ideal/d.sale *100 ).toFixed(2)+ "</td>";
          else
            html += "<td> N.A.</td>";
          html += "<td>" + parseFloat(d.actual).toFixed(2) + "</td>";
          if(d.sale!=0)
            html += "<td>" + parseFloat(d.actual / d.sale* 100).toFixed(2) + "</td>";
          else
            html += "<td> N.A.</td>";
          html += "<td>" + parseFloat(d.sale).toFixed(2) + "</td>";
            //html += "<td>" + d.purchaseQty + "</td>";
          html += "</tr>";
     
        });


        html+=" </tbody>";


        document.getElementById('dateWiseReport').innerHTML = html;
        return html;
      }

      function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
          dateArray.push( {date:new Date (currentDate) });
          currentDate = currentDate.addDays(1);
        }
        //console.log(dateArray);
        return dateArray;
      };

      Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
      };
   
      $scope.exportToPdf_dateWise=function(){
        var table=document.getElementById('dateWiseReport').innerHTML;
        var printer = window.open('', '', 'width=600,height=600');
        printer.document.open("text/html");
        printer.document.write(table);
        printer.document.close();
        printer.focus();
        printer.print();
      };
      $scope.exportToExcel_dateWise=function(){
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('dateWiseReport').innerHTML));
      };

     
    }]);
