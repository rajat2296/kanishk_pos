'use strict';

describe('Controller: FoodCostReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var FoodCostReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FoodCostReportsCtrl = $controller('FoodCostReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
