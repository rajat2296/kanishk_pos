'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('vendorPaymentReports', {
        url: '/vendorPaymentReports',
        templateUrl: 'app/stock/stockReports/vendorPaymentReports/vendorPaymentReports.html',
        controller: 'VendorPaymentReportsCtrl'
      });
  });