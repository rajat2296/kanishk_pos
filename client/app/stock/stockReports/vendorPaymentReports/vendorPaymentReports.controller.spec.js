'use strict';

describe('Controller: VendorPaymentReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var VendorPaymentReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VendorPaymentReportsCtrl = $controller('VendorPaymentReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
