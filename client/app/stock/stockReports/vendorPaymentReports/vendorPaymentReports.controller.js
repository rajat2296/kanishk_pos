
'use strict';

angular.module('posistApp')
  .controller('VendorPaymentReportsCtrl',['$q','$resource','$scope','$filter','growl','store','vendor','receiver','currentUser','$rootScope','$state','$modal','VendorPaymentHistory','VPayment','StockResource','localStorageService','deployment','Utils','property','intercom', 'stockItem', function ($q,$resource,$scope,$filter,growl,store,vendor,receiver,currentUser,$rootScope,$state,$modal,VendorPaymentHistory,VPayment,StockResource,localStorageService,deployment,Utils,property,intercom, stockItem) {


    currentUser.deployment_id=localStorageService.get('deployment_id');
    $scope.stores=$rootScope.stores;
    $scope.vendors=$rootScope.vendors;
    $scope.receivers=$scope.receivers;
    $scope.user=currentUser;
    $scope.payments=VPayment;

  $scope.resetSerialNumber=false
    $scope.hidePricing=false

    //  property.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
    //  if(result.length==0)
    //  {
    //   $scope.resetSerialNumber=true
    //  }
    //  else
    //  {
    //   $scope.settings=result[0];
    //   if($scope.settings.resetSerialNumber==undefined)
    //     $scope.resetSerialNumber=true
    //   else
    //    $scope.resetSerialNumber=$scope.settings.resetSerialNumber
    //    console.log($scope.resetSerialNumber); 
    //  }
     
    // });
    currentUser.deployment_id=localStorageService.get('deployment_id');

    function getStores_ByDeployment(){
      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getVendors_ByDeployment(){
      return vendor.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getReceivers_ByDeployment(){
        return receiver.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getItems() {
                return stockItem.get({
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: localStorageService.get('deployment_id')
                });
            }

    var allPromise=$q.all([
        getStores_ByDeployment() ,getVendors_ByDeployment(),getReceivers_ByDeployment(), getItems()
    ]);

    allPromise.then(function (value){
      $scope.stores=value[0];
      $scope.editEntryForm.stores=value[0];// $rootScope.stores;
      $scope.vendors=value[1];// $rootScope.vendors;
      $scope.receivers=value[2];// $scope.receivers;
      $scope.items = value[3];
    });

    function getYesterDayDate() {
      var d=new Date();
      d.setDate(d.getDate()-1);
      return d;
    };

    function parseDate(str)
    {
        var d=$filter('date')(str, 'dd-MMM-yyyy');
        return d;
    };

    $scope.editEntryForm={
      fromDate:new Date(),//getYesterDayDate(),
      toDate:new Date(),
      maxDate:new Date()
    };
    $scope.editEntryForm.stores=angular.copy($scope.stores);
    $scope.checkDateGreater= function(date){
      var d=new Date(date);
      var dd=new Date($scope.editEntryForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.editEntryForm.fromDate=new Date();//getYesterDayDate()
      }
      else
      {
        //growl.success('Lesser than toDate', {ttl: 3000});
      }

    };

    $scope.checkDateLesser= function(date){
      var d=new Date(date);
      var dd=new Date($scope.editEntryForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.editEntryForm.toDate= new Date();
      }
      else
      {
        //growl.success('Greater than fromDate', {ttl: 3000});
      }
    };

    function bindVendor(store){
      $scope.editEntryForm.vendorReceiver=[];
      if(store!=null){
        _.forEach($scope.vendors, function(v,i){
          if(v.pricing!=undefined){
            if(v.pricing._id==store._id){
              v.name=v.vendorName;
              $scope.editEntryForm.vendorReceiver.push(v);
            }
          }
        });
      }
    };
    function bindReceiver(store){
      $scope.editEntryForm.vendorReceiver=[];
      var receivers=angular.copy($scope.receivers);
        if(store!=null){
            _.forEach(receivers, function(r,i){
              // if(r.pricing!=undefined){
              //     _.forEach(r.pricing.store, function(s,ii){
              //         if(s._id==store._id){
                    r.name=r.receiverName;
                          $scope.editEntryForm.vendorReceiver.push(r);
              //         }
              //     });
              // }
          });
        }
    };

    $scope.bindVenRec_editEntryForm= function(store){
      var entryType=$scope.editEntryForm.entryType;
      if(entryType==1){
        //bindVendor
        bindVendor(angular.copy(store));
      }
      else if(entryType==2){
        //bind REceiver
        bindReceiver(angular.copy(store));
      }
    };

    $scope.bindStore_editEntryForm= function(entryType){
      $scope.editEntryForm.stores=[];
      $scope.editEntryForm.vendorReceiver=[];
      $scope.editEntryForm.stores=angular.copy($scope.stores);
    };

    $scope.startSearch= function(){
      intercom.registerEvent('StockReports');
      $scope.editEntryForm.availableRecords=[];
      var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.editEntryForm.fromDate))));
        console.log('fromDate',fromDate);
        
        var toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.editEntryForm.toDate))));
        console.log('toDate',toDate);
          /*var fromDate=new Date($scope.editEntryForm.fromDate);
          fromDate.setHours(0, 0, 0, 0);
          var toDate=new Date($scope.editEntryForm.toDate);
          toDate.setHours(0, 0, 0, 0);*/
          var strId="";
          if($scope.editEntryForm.store!=undefined){
            strId=$scope.editEntryForm.store._id;
          }

          var venId="";
          if($scope.editEntryForm.vendor!=undefined){
            venId=$scope.editEntryForm.vendor._id;
          }

          if($scope.editEntryForm.entryType=="1"){
            var req={"fromDate":fromDate,"toDate":toDate,"transactionType":$scope.editEntryForm.entryType,"_store._id":strId,"_store.vendor._id":venId,deployment_id:currentUser.deployment_id,tenant_id:currentUser.tenant_id};
          }
          else if($scope.editEntryForm.entryType=="2")
          {
            var req={"fromDate":fromDate,"toDate":toDate,"transactionType":$scope.editEntryForm.entryType,"_store._id":strId,"_store.receiver._id":venId,deployment_id:currentUser.deployment_id,tenant_id:currentUser.tenant_id};
          }

          StockResource.getAllByTranasactionAndDateRangeNew(req, function (result){
            if($scope.editEntryForm.entryType=="1"){
              $scope.editEntryForm.availableRecords=calculateForStockEntry_Rpt(result);
            }
            else
            {
              $scope.editEntryForm.availableRecords=calculateForStockSale_Rpt(result);
            }
          });
    };

    function calculateForStockEntry_Rpt(result){
      console.log('result', result);
      var entry=[];
      var gtQ=0,gtA=0,gtD=0,gtP=0;
      
      var obj={};
      obj.entryType="Stock Entry";
      entry.push(obj);
      entry[0].vendors=[];
      _.forEach(result,function(r,i){
      var disTotal=0,vatTotal=0,discountAmount = 0, netAdditionalCharge = 0;
        //console.log('-----------------------New Transaction---------------------');
        var Q=0,A=0,D=0,P=0,dAmt=0,tVat=0,cartage= 0,dis=0,dAmount=0;
        var index=_.findIndex($scope.payments,{_id:r._id});
        if(index>=0){
          P=angular.copy($scope.payments[index].totalPaidAmount);
        }
        _.forEach(r._store.vendor.category,function(c,ii){
          _.forEach(c.items,function(itm,iii){
          A+=parseFloat(itm.subTotal);
          });
          _.forEach(c.items,function(itm,iii){
         //console.log('-----------------------New Item------------------------')
            Q+=parseFloat(itm.qty);
            //A+=parseFloat(itm.subTotal);
            var dis=0,disAmount=0,perDis=0,vs=0;
         if(r.discount!=undefined && r.discount!=0){
                  if(r.discountType=="percent"){
                    //console.log('total',A);
                    dis= parseFloat(A*0.01*r.discount).toFixed(2);
                    perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(A).toFixed(2)).toFixed(2);
                    //console.log('dis', dis);
                    //console.log('perDis', perDis)
                    //console.log('itm.total',itm.subTotal);
                    disAmount=parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2);
                    //console.log('disAmount',disAmount);
                      if(itm.vatPercent)
                      {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',itm.vatPercent);
                        vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                      }
                      else
                      {
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                      }
                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                    //console.log('disTotal',disTotal);
                  }
                  else{
                    //console.log('total',A);
                    dis= parseFloat(r.discount).toFixed(2);
                    //perDis=parseFloat(dis/numberOfItems).toFixed(2);
                    perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(A).toFixed(2)).toFixed(2);
                    //console.log('dis', dis);
                    //console.log('perDis', perDis);
                    //console.log('itm.total',itm.subTotal);
                    disAmount=parseFloat(parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2)).toFixed(2);
                    //console.log('disAmount',disAmount);
                      if(itm.vatPercent)
                      {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',itm.vatPercent);
                        vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                      }
                      else
                      {
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);  
                      }
                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                    //console.log('disTotal',disTotal);
                    }
                }
                else
                {
                  if(itm.vatPercent)
                  {
                    //console.log('vatTotal vefore vat',vatTotal);
                    //console.log('itm.vatPercent',itm.vatPercent);
                    vs=parseFloat(itm.vatPercent*0.01*parseFloat(itm.subTotal).toFixed(2)).toFixed(2);
                    //console.log('vs',vs);
                    //console.log('disAmount before vat',disAmount);
                    disAmount=parseFloat(parseFloat(itm.subTotal)+parseFloat(vs)).toFixed(2);
                        
                    //disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                    //console.log('disAmount after vat',disAmount);
                    disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                    //console.log('disTotal',disTotal);
                    vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                    //console.log(disAmount);
                    //console.log('vatTotal after vat',vatTotal);
                  }
                else
                  {
                    disTotal=parseFloat(parseFloat(disTotal)+parseFloat(itm.subTotal)).toFixed(2);
                    //console.log('disTotal',disTotal);
                   
                  }                  
                }
          });
        });
        if(r.discount!=0){
          if(r.discountType=="percent"){
            dAmt=parseFloat(A*0.01*parseFloat(r.discount)).toFixed(2);
            discountAmount = parseFloat(parseFloat(A) - parseFloat(dAmt)).toFixed(2);
          }
          else{
            dAmt=parseFloat(r.discount).toFixed(2);
            discountAmount = parseFloat(A).toFixed(2);
          }
        }
        if(r.cartage!=undefined && r.cartage!=0 && r.cartage!=null){
          cartage=parseFloat(r.cartage);
        }
        if(r.charges)
                    {
                        //console.log('afterDis',afterDis);
                        _.forEach(r.charges , function(charge) {
                            if(charge.operationType == 'additive')
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue * 0.01 * discountAmount)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * discountAmount).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                            else
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue * 0.01 * discountAmount)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * discountAmount).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                        });
                    }
        //after discount
        //A=parseFloat(A)-parseFloat(dAmt);
        //After Vat
        //A=parseFloat(A)+parseFloat(tVat);
        //After Cacrtage
        //console.log('disTotal brfore cartage',disTotal)
        //console.log('cartage',cartage)
        console.log(discountAmount, cartage, netAdditionalCharge)
        A=parseFloat(disTotal) + parseFloat(cartage) + parseFloat(netAdditionalCharge);
        //console.log('disTotal after cartage',A);
        D=(A-P);
        r.qty=parseFloat(Q).toFixed(2);
        r.amount=parseFloat(A).toFixed(2);
        r.paid=parseFloat(P).toFixed(2);
        r.due=parseFloat(D).toFixed(2);
        var bill={};
        bill.billDate=r.created;
        bill.invoiceNumber=r.invoiceNumber;
        bill.batchNumber=r.batchNumber;
        if(r.isPOEntry) {
          if($scope.resetSerialNumber)
            bill.billNo = "PO-" + r.daySerialNumber;
          else
            bill.billNo = "PO-" + r.transactionNumber;
        } else {
          if($scope.resetSerialNumber)
            bill.billNo = "SE-" + r.daySerialNumber;
          else
            bill.billNo = "SE-" + r.transactionNumber;
        }
        bill.qty=parseFloat(r.qty).toFixed(3);
        bill.amount=parseFloat(r.amount).toFixed(2);
        bill.paid=parseFloat(r.paid).toFixed(2);
        bill.due=parseFloat(r.due).toFixed(2);
        var ven={_id:r._store.vendor._id,vendorName:r._store.vendor.vendorName,bills:[]};
        var venIndex=_.findIndex(entry[0].vendors,{_id:ven._id});
        if(venIndex<0){
          ven.qty=parseFloat(bill.qty).toFixed(3);
          ven.amount=parseFloat(bill.amount).toFixed(2);
          ven.paid=parseFloat(bill.paid).toFixed(2);
          ven.due=parseFloat(bill.due).toFixed(2);
          ven.bills.push(bill);
          entry[0].vendors.push(ven);
        }
        else
        {
          entry[0].vendors[venIndex].qty=parseFloat(parseFloat(bill.qty)+parseFloat(entry[0].vendors[venIndex].qty)).toFixed(3);
          entry[0].vendors[venIndex].amount=parseFloat(parseFloat(bill.amount)+parseFloat(entry[0].vendors[venIndex].amount)).toFixed(2);
          entry[0].vendors[venIndex].paid=parseFloat(parseFloat(bill.paid)+parseFloat(entry[0].vendors[venIndex].paid)).toFixed(2);
          entry[0].vendors[venIndex].due=parseFloat(parseFloat(bill.due) + parseFloat(entry[0].vendors[venIndex].due)).toFixed(2);
          entry[0].vendors[venIndex].bills.push(bill);
        }
        gtQ+=parseFloat(Q);
        gtP+=parseFloat(P);
        gtD+=parseFloat(D);
        gtA+=parseFloat(A);
      });
      entry[0].qty=parseFloat(gtQ).toFixed(3);
      entry[0].paid=parseFloat(gtP).toFixed(2);
      entry[0].due=parseFloat(gtD).toFixed(2);
      entry[0].amount=parseFloat(gtA).toFixed(2);
      console.log(entry);
      return entry;
    };

     function calculateForStockSale_Rpt(result){
      var entry=[];
      var gtQ=0,gtA=0,gtD=0,gtP=0;
      var obj={};
      obj.entryType="Stock Sale";
      entry.push(obj);
      entry[0].vendors=[];
      _.forEach(result,function(r,i){
        var Q=0,A=0,D=0,P=0;
        var dAmt=0;
        var tvat=0;
        var tSc=0;
        var tcartage=0;
        var tpay=0;
        var index=_.findIndex($scope.payments,{_id:r._id});
        if(index>=0){
          P=angular.copy($scope.payments[index].totalPaidAmount);
        }
        _.forEach(r._store.receiver.category,function(c,ii){
          _.forEach(c.items,function(itm,iii){
            Q+=parseFloat(itm.qty);
            A+=parseFloat((itm.qty)*(itm.price));
          });
        });
        if(r.discount!=0 && r.discount!=undefined && r.discount!=null){
          if(r.discountType=="percent"){
            dAmt=parseFloat(A*0.01*parseFloat(r.discount)).toFixed(2);
          }
          else{
            dAmt=parseFloat(r.discount).toFixed(2);
          }
        }
        var amtAfterDiscount=parseFloat(A)-parseFloat(dAmt);
        if(r.vat!=0 && r.vat!=undefined && r.vat!=null){
          if(r.vatType=="percent"){
            tvat=parseFloat(amtAfterDiscount*0.01*parseFloat(r.vat)).toFixed(2);
          }
          else{
            tvat=parseFloat(r.vat).toFixed(2);
          }
        }
        if(r.serviceCharge!=undefined && r.serviceCharge!=null){
          if(r.serviceChargeType=="percent"){
            tSc=parseFloat(amtAfterDiscount*0.01*parseFloat(r.serviceCharge)).toFixed(2);
          }
          else
          {
            tSc=parseFloat(r.serviceCharge).toFixed(2);
          }
        }
        if(r.cartage!=undefined && r.cartage!=null){
          tcartage=parseFloat(r.cartage).toFixed(2);
        }
        if(r.payment!=undefined && r.payment!=null){
          tpay=parseFloat(r.payment).toFixed(2);
        }
        A=parseFloat(parseFloat(A)-parseFloat(dAmt)+parseFloat(tSc)+parseFloat(tvat)+parseFloat(tcartage)+parseFloat(tpay)).toFixed(2);
        D=(A-P);
        r.qty=parseFloat(Q).toFixed(2);
        r.amount=parseFloat(A).toFixed(2);
        r.paid=parseFloat(P).toFixed(2);
        r.due=parseFloat(D).toFixed(2);
        var bill={};
        bill.billDate=r.created;
        if($scope.resetSerialNumber)
          bill.billNo="SS-"+r.daySerialNumber;
        else
          bill.billNo="SS-"+r.transactionNumber;
        bill.invoiceNumber=r.invoiceNumber;
        bill.qty=parseFloat(r.qty).toFixed(3);
        bill.amount=parseFloat(r.amount).toFixed(2);
        bill.paid=parseFloat(r.paid).toFixed(2);
        bill.due=parseFloat(r.due).toFixed(2);
        var ven={_id:r._store.receiver._id,vendorName:r._store.receiver.receiverName,bills:[]};
        var venIndex=_.findIndex(entry[0].vendors,{_id:ven._id});
        if(venIndex<0){
          ven.qty=parseFloat(bill.qty).toFixed(3);
          ven.amount=parseFloat(bill.amount).toFixed(2);
          ven.paid=parseFloat(bill.paid).toFixed(2);
          ven.due=parseFloat(bill.due).toFixed(2);
          ven.bills.push(bill);
          entry[0].vendors.push(ven);
        }
        else
        {
          entry[0].vendors[venIndex].qty=parseFloat(parseFloat(bill.qty)+parseFloat(entry[0].vendors[venIndex].qty)).toFixed(3);
          entry[0].vendors[venIndex].amount=parseFloat(parseFloat(bill.amount)+parseFloat(entry[0].vendors[venIndex].amount)).toFixed(2);
          entry[0].vendors[venIndex].paid=parseFloat(parseFloat(bill.paid)+parseFloat(entry[0].vendors[venIndex].paid)).toFixed(2);
          entry[0].vendors[venIndex].due=parseFloat(parseFloat(bill.due) + parseFloat(entry[0].vendors[venIndex].due)).toFixed(2);
          entry[0].vendors[venIndex].bills.push(bill);
        }
        gtQ+=parseFloat(Q);
        gtP+=parseFloat(P);
        gtD+=parseFloat(D);
        gtA+=parseFloat(A);
      });
      entry[0].qty=parseFloat(gtQ).toFixed(3);
      entry[0].paid=parseFloat(gtP).toFixed(2);
      entry[0].due=parseFloat(gtD).toFixed(2);
      entry[0].amount=parseFloat(gtA).toFixed(2);
      console.log(entry);
      return entry;
    };
 

    //---------------------------------------------Reports----------------------------------------------
    $scope.exportToPdf_vendorPayment=function(){
      var table=document.getElementById('vendorPaymentReport').innerHTML;
       var printer = window.open('', '', 'width=600,height=600');
       printer.document.open("text/html");
       printer.document.write(table);
       printer.document.close();
       printer.focus();
       printer.print();
    };
    $scope.exportToExcel_vendorPayment=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('vendorPaymentReport').innerHTML));
    };
  }]);

