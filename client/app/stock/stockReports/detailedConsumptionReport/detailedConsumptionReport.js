'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('detailedConsumptionReport', {
        url: '/detailedConsumptionReport',
        templateUrl: 'app/stock/stockReports/detailedConsumptionReport/detailedConsumptionReport.html',
        controller: 'detailedConsumptionReportCtrl'
      });
  });