'use strict';

describe('Controller: detailedConsumptionReportCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var detailedConsumptionReportCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    detailedConsumptionReportCtrl = $controller('detailedConsumptionReportCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
