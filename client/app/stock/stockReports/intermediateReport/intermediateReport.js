'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('intermediateReport', {
        url: '/intermediateReport',
        templateUrl: 'app/stock/stockReports/intermediateReport/intermediateReport.html',
        controller: 'IntermediateReportCtrl'
      });
  });