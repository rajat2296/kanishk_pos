'use strict';

angular.module('posistApp')
    .controller('IntermediateReportCtrl',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','StockReport','localStorageService','stockItem','StockReportNew','stockRecipe','Utils','deployment','stockReportResetTime','intercom', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,StockReport,localStorageService,stockItem,StockReportNew,stockRecipe,Utils,deployment,stockReportResetTime,intercom) {

      currentUser.deployment_id=localStorageService.get('deployment_id');
      $scope.stores=[];
      $scope.user=currentUser;
      $scope.stockitems=[];
      $scope.lastPrice=[];
      $scope.recipeQty=[];
      $scope.semiRecipe=[];

      store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.stores=result;
        $scope.purchaseConsumptionForm.stores=result;
        $scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0];
        //$scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
        stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
          $scope.stockitems=result;
        });

        // StockReportNew.getLastPriceOfItem_RawMaterial({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        //   $scope.lastPrice=result;
        //   console.log($scope.lastPrice);
        // });
        StockReportNew.getLastPriceOfItem_Intermediate({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
          $scope.lastPrice=result;
          console.log($scope.lastPrice);
        });
        StockReportNew.getRecipeQtyInBase({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
          $scope.recipeQty=result;
          console.log($scope.recipeQty);
        });

        stockRecipe.getSemiProcessedRecipes({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
          $scope.semiRecipe=result;
          console.log($scope.semiRecipe);
          $scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
        });
      });

      function getYesterDayDate() {
        var d=new Date();
        d.setDate(d.getDate()-1);
        return d;
      };

      function parseDate(str){
        var d=$filter('date')(str, 'dd-MMM-yyyy');
        return d;
      };

      $scope.purchaseConsumptionForm={
        //fromDate:getYesterDayDate(),
        fromDate:new Date(),
        toDate:new Date(),
        maxDate:new Date()
      };

      //$scope.purchaseConsumptionForm.stores=angular.copy($scope.stores);
      $scope.checkDateGreater= function(date){
        var d=new Date(date);
        var dd=new Date($scope.purchaseConsumptionForm.toDate);
        if(d.getTime()>dd.getTime()){
          growl.success('Greater than toDate', {ttl: 3000});
          $scope.purchaseConsumptionForm.fromDate= new Date();//getYesterDayDate();
        }
      };

      $scope.checkDateLesser= function(date){
        var d=new Date(date);
        var dd=new Date($scope.purchaseConsumptionForm.fromDate);
        if(d.getTime()<dd.getTime()){
          growl.success('Can not be Lesser than fromDate', {ttl: 3000});
          $scope.purchaseConsumptionForm.toDate= new Date();
        }
      };

      $scope.clearPurchaseConsumptionTab=function(){
        $scope.purchaseConsumptionForm={
          fromDate:new Date(),//getYesterDayDate(),
          toDate:new Date(),
          maxDate:new Date(),
          stores:angular.copy($scope.stores)
        };
        $scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0];
        $scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
      };

      function compare(a,b) {
        if (a.itemName < b.itemName)
          return -1;
        if (a.itemName > b.itemName)
          return 1;
        return 0;
      };

      $scope.bindCategory_purchaseConsumptionForm = function(stores){
        var store=stores;
        $scope.purchaseConsumptionForm.categories=[];
        $scope.purchaseConsumptionForm.availableItems=[];
        store.category=[];
        var cat={_id:"PosistTech222222",categoryName:"InterMediate Food"};
        cat.item=[];
        _.forEach(angular.copy($scope.semiRecipe),function(itm){
          itm.itemName=itm.recipeName;
          cat.item.push(itm);
        });
        store.category.push(cat);
        $scope.purchaseConsumptionForm.categories.push(cat);
      };

      $scope.bindItems_purchaseConsumptionForm= function(category){
        var cat=angular.copy(category);
        $scope.purchaseConsumptionForm.availableItems=[];
        $scope.purchaseConsumptionForm.item=undefined;
        if(cat!=null){
          _.forEach(cat.item, function(c,i){
            $scope.purchaseConsumptionForm.availableItems.push(c);
          });
        }
        $scope.purchaseConsumptionForm.availableItems.sort(compare);
      };

      function getItemForSearch(stores){
        var item=[];
        _.forEach(stores.category,function(c,i){
          _.forEach(c.item, function(itm,ii){
            var obj={id:itm._id,itemName:itm.itemName};
            item.push(obj);
          });
        });
        return item;
      };

      function getCategoryForSearch(stores){
        var item=[];
        _.forEach(stores.category,function(c,i){
          var obj={_id:c._id};
          item.push(obj);
        });
        return item;
      };

      $scope.startSearch= function(){
        intercom.registerEvent('StockReports');
        $scope.purchaseConsumptionForm.isSearch=true;
        //var fromDate=new Date($scope.purchaseConsumptionForm.fromDate);
        //fromDate.setHours(0, 0, 0, 0);
        //var toDate=new Date($scope.purchaseConsumptionForm.toDate);
        //toDate = new Date(toDate.setDate(toDate.getDate() + 1));
        //toDate.setHours(0, 0, 0, 0);
        var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.fromDate))));
        var toDate = new Date($scope.purchaseConsumptionForm.toDate);
        var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.toDate))));
        toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));

        var strId=$scope.purchaseConsumptionForm.store._id;
        var isKitchen=$scope.purchaseConsumptionForm.store.isKitchen;
        var catId=[];
        if($scope.purchaseConsumptionForm.item!=undefined || $scope.purchaseConsumptionForm.item!=null){
          var item=[{id:$scope.purchaseConsumptionForm.item._id,itemName:$scope.purchaseConsumptionForm.item.itemName}];
        }
        else if($scope.purchaseConsumptionForm.category!=undefined || $scope.purchaseConsumptionForm.category!=null){
          var item=[];
          _.forEach($scope.purchaseConsumptionForm.category.item, function(itm,i){
            var obj={id:itm._id,itemName:itm.itemName};
            item.push(obj);
          });

          catId.push({_id:$scope.purchaseConsumptionForm.category._id});
        }
        else if($scope.purchaseConsumptionForm.store!=undefined){
          var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
          catId=getCategoryForSearch(angular.copy($scope.purchaseConsumptionForm.store));
        };

        //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
        var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"category":catId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id,resetTime: true};
        //"items":JSON.stringify(item)
        stockReportResetTime.getConsumptionSummary_IntermediateReport(req, function (result){
          if(result.errorMessage!=undefined){
            $scope.purchaseConsumptionForm.reportData=result;
            $scope.purchaseConsumptionForm.isSearch=false;
          }
          else
          {
            console.log(result);
            var result=spliceAllItemsBeforePhysical(result);
            result=calculatePrice(result);
            console.log(result);
            result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
            result=formatDataForReport(result,fromDate,toDateReset);
            console.log(result);
            result=spliceForCategoryAndItem(result,item);
            var res=convertItemInPreferredUnit(result);
            $scope.purchaseConsumptionForm.reportData=res;
            $scope.purchaseConsumptionForm.isSearch=false;
            //console.log(result);
          }
        });
      };
      function spliceAllItemsBeforePhysical_DateWise(result,toDate){
        var data = [];
        _.forEach(result.beforeDate, function (r, i) {
          if (r.totalBalanceQty != undefined) {
            var d = new Date(r.created);
            var a = new Date(d);
            a.setHours(0, 0, 0, 0);
            a = new Date(a);
            var b = new Date(d);
            b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
            if ((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())) {

            } else {
              //console.log('splice all', d);
              d.setDate(d.getDate() + 1);
            }
            d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
            //d.setHours(0, 0, 0, 0);
            r.created = d;
            data.push(r);
          }
          ;
        });
        _.forEach(data, function (d, i) {
          _.forEachRight(result.beforeDate, function (itm, ii) {
            if (itm.itemId == d.itemId) {
              var phyDate = new Date(d.created);
              var compareDate = new Date(itm.created);
              if (phyDate.getTime() > compareDate.getTime()) {
                result.beforeDate.splice(ii, 1);
              }
            }
          });
        });

        // var data2=[];
        _.forEachRight(result.betweenDate, function (r, i) {
          if (r.totalBalanceQty != undefined) {
            console.log(r.created);
            var d = new Date(r.created);
            var cdate = new Date($scope.purchaseConsumptionForm.toDate);
            var a = new Date(d);
            a.setHours(0, 0, 0, 0);
            a = new Date(a);
            var b = new Date(d);
            b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
            console.log(a)
            console.log(b)
            console.log(angular.copy(d))
            if ((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())) {

            } else {
              console.log('splice all', d);
              d.setDate(d.getDate() + 1);
            }
            //d.setDate(d.getDate() + 1);
            d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
            r.created = d;
            //console.log(r.created);
            // if(cdate.getTime()>d.getTime()){
            //   data2.push(r);
            // }
            // else
            // {
            //   result.betweenDate.splice(i,1);
            // }
          }
          ;
        });
        // _.forEach(data2,function(d,i){
        //   _.forEachRight(result.betweenDate,function(itm,ii){
        //     if(itm.itemId==d.itemId){
        //       if(itm.totalBalanceQty!=undefined){
        //           var phyDate=new Date(d.created);
        //           var compareDate=new Date(itm.created);
        //           if(phyDate.getTime()>compareDate.getTime()){
        //             result.betweenDate.splice(ii,1);
        //           }
        //         }
        //     }
        //   });
        //});

        return result;
      };
      function spliceAllItemsBeforePhysical(result,toDate){
        var data = [];
        _.forEach(result.beforeDate, function (r, i) {
          if (r.totalBalanceQty != undefined) {
            var d = new Date(r.created);
            var a = new Date(d);
            a.setHours(0, 0, 0, 0);
            a = new Date(a);
            var b = new Date(d);
            b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
            if ((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())) {

            } else {
              //console.log('splice all', d);
              d.setDate(d.getDate() + 1);
            }
            d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
            //d.setHours(0, 0, 0, 0);
            r.created = d;
            data.push(r);
          }
          ;
        });
        _.forEach(data, function (d, i) {
          _.forEachRight(result.beforeDate, function (itm, ii) {
            if (itm.itemId == d.itemId) {
              var phyDate = new Date(d.created);
              var compareDate = new Date(itm.created);
              if (phyDate.getTime() > compareDate.getTime()) {
                result.beforeDate.splice(ii, 1);
              }
            }
          });
        });

        // var data2=[];
        _.forEachRight(result.betweenDate, function (r, i) {
          if (r.totalBalanceQty != undefined) {
            console.log(r.created);
            var d = new Date(r.created);
            var cdate = new Date($scope.purchaseConsumptionForm.toDate);
            var a = new Date(d);
            a.setHours(0, 0, 0, 0);
            a = new Date(a);
            var b = new Date(d);
            b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
            console.log(a)
            console.log(b)
            console.log(angular.copy(d))
            if ((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())) {

            } else {
              console.log('splice all', d);
              d.setDate(d.getDate() + 1);
            }
            //d.setDate(d.getDate() + 1);
            d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
            r.created = d;
            //console.log(r.created);
            // if(cdate.getTime()>d.getTime()){
            //   data2.push(r);
            // }
            // else
            // {
            //   result.betweenDate.splice(i,1);
            // }
          }
          ;
        });
        // _.forEach(data2,function(d,i){
        //   _.forEachRight(result.betweenDate,function(itm,ii){
        //     if(itm.itemId==d.itemId){
        //       if(itm.totalBalanceQty!=undefined){
        //           var phyDate=new Date(d.created);
        //           var compareDate=new Date(itm.created);
        //           if(phyDate.getTime()>compareDate.getTime()){
        //             result.betweenDate.splice(ii,1);
        //           }
        //         }
        //     }
        //   });
        //});

        return result;
      };

      function calculatePrice(result){
        _.forEach(result.beforeDate,function(itm,i){
          var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
          //if(index>=0){
          if(itm.totalOpeningQty!=undefined){
            if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
              if(index>=0){
                itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalOpeningAmt=0.00;
              }
            }
          }
          else if(itm.totalBalanceQty!=undefined){
            if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
              if(index>=0){
                itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalBalanceAmt=0.00;
              }
            }
          }
          else if(itm.totalPurchaseQty!=undefined){
            if(itm.totalPurchaseAmount==undefined){
              itm.totalPurchaseAmount=0.00;
            }
          }
          else if(itm.totalSaleQty!=undefined){
            if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
              if(index>=0){
                itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalSaleAmount=0.00;
              }
            }
          }
          else if(itm.totalWastageQty!=undefined){
            if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
              if(index>=0){
                itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalWastageAmt=0.00;
              }
            }
          }
          else if(itm.totalTransferQty!=undefined){
            if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
              if(index>=0){
                itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalTransferAmount=0.00;
              }
            }
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
              if(index>=0){
                itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalTransferAmount_toStore=0.00;
              }
            }
          }
          else if(itm.itemSaleQty!=undefined){
            if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
              if(index>=0){
                itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.itemSaleAmt=0.00;
              }
            }
          }
          //}
        });
        _.forEach(result.betweenDate,function(itm,i){
          var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
          //if(index>=0){
          if(itm.totalOpeningQty!=undefined){
            if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
              if(index>=0){
                itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalOpeningAmt=0.00;
              }
            }
          }
          else if(itm.totalBalanceQty!=undefined){
            if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
              if(index>=0){
                itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalBalanceAmt=0.00;
              }
            }
          }
          else if(itm.totalPurchaseQty!=undefined){
            if(itm.totalPurchaseAmount==undefined){
              itm.totalPurchaseAmount=0.00;
            }
          }
          else if(itm.totalSaleQty!=undefined){
            if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
              if(index>=0){
                itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalSaleAmount=0.00;
              }
            }
          }
          else if(itm.totalWastageQty!=undefined){
            if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
              if(index>=0){
                itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalWastageAmt=0.00;
              }
            }
          }
          else if(itm.totalTransferQty!=undefined){
            if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
              if(index>=0){
                itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalTransferAmount=0.00;
              }
            }
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
              if(index>=0){
                itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.totalTransferAmount_toStore=0.00;
              }
            }
          }
          else if(itm.itemSaleQty!=undefined){
            if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
              if(index>=0){
                itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
              else
              {
                itm.itemSaleAmt=0.00;
              }
            }
          }
          //}
        });
        return result;
      };

      function formatDataForReport(result,startDate,endDate){
        result.items = [];
        var endDateToShow = new Date(endDate);
        endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
        _.forEach(result.beforeDate, function (itm, i) {
          if (itm.totalOpeningQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(itm.totalOpeningQty).toFixed(3);
              itm.openingAmt = parseFloat(itm.totalOpeningAmt).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(3);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalOpeningAmt)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalOpeningAmt)).toFixed(2);
            }
          }
          else if (itm.totalBalanceQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.openingAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              //console.log('total balance', itm.totalBalanceQty);
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
          }
          else if (itm.totalPurchaseQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt = parseFloat(itm.totalPurchaseAmount).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
            }
          }
          else if (itm.totalReturnAcceptQty != undefined) {
            //console.log('totalReturnAcceptQty');
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(itm.totalReturnAcceptQty).toFixed(3);
              itm.openingAmt = parseFloat(itm.totalReturnAcceptAmount).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalReturnAcceptAmount)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalReturnAcceptAmount)).toFixed(2);
            }
          }
          else if (itm.totalIntermediateQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt = parseFloat(-itm.totalIntermediateAmt).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalIntermediateAmt)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalIntermediateAmt)).toFixed(2);
            }
          }
          else if (itm.totalSaleQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt = parseFloat(-itm.totalSaleAmount).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalSaleAmount)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalSaleAmount)).toFixed(2);
            }
          }
          else if (itm.totalWastageQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt = parseFloat(-itm.totalWastageAmt).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalWastageAmt)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalWastageAmt)).toFixed(2);
            }
          }
          else if (itm.totalTransferQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt = parseFloat(-itm.totalTransferAmount).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.totalTransferAmount)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalTransferAmount)).toFixed(2);
            }
          }
          else if (itm.totalTransferQty_toStore != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
            }
          }
          else if (itm.itemSaleQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt = parseFloat(-itm.itemSaleAmt).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              itm.UnitName = itm.baseUnit;
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].openingQty = parseFloat(parseFloat(result.items[itmIndex].openingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt = parseFloat(parseFloat(result.items[itmIndex].openingAmt) - parseFloat(itm.itemSaleAmt)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.itemSaleAmt)).toFixed(2);
            }
          }
        });
        _.forEach(result.betweenDate, function (itm, i) {
          if (itm.totalPurchaseQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalPurchaseAmount).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.purchaseAmount = parseFloat(itm.totalPurchaseAmount).toFixed(2);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].purchaseQty = parseFloat(parseFloat(result.items[itmIndex].purchaseQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].purchaseAmount = parseFloat(parseFloat(result.items[itmIndex].purchaseAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalPurchaseAmount)).toFixed(2);
            }
          }
          else if (itm.totalBalanceQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            //console.log('itm', itm);
            var cDate = new Date(itm.created);
            //cDate.setDate(cDate.getDate()-1);
            //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
            //var ed = new Date(endDate);
            //ed = new Date(ed.setDate(ed.getDate()-1));
            //console.log(cDate);
            //console.log(endDate);
            //console.log(ed);
            if (itmIndex < 0) {
              itm.openingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.openingAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
              itm.purchaseQty = parseFloat(0).toFixed(3);
              itm.purchaseAmount = parseFloat(0).toFixed(2);
              itm.consumptionQty = parseFloat(0).toFixed(3);
              itm.consumptionAmount = parseFloat(0).toFixed(2);
              itm.closingQty = parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount = parseFloat(itm.openingAmt).toFixed(2);
              if (endDate.getTime() == cDate.getTime()) {
                itm.physicalQty = parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.physicalAmt = parseFloat(itm.totalBalanceAmt).toFixed(2);
              }
              else{
                itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                itm.physicalDate = new Date(itm.created);
              }
              itm.openingDate = new Date(startDate);
              itm.closingDate = new Date(endDateToShow);
              result.items.push(itm);
            }
            else {
              if (endDate.getTime() == cDate.getTime()) {
                result.items[itmIndex].physicalQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].physicalAmt = parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                result.items[itmIndex].varianceQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
                result.items[itmIndex].varianceInPercent = parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty) / parseFloat(result.items[itmIndex].closingQty)) * parseFloat(100)).toFixed(2);

                //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
              }
              else {
                console.log(itm.itemName, " else");
                console.log(result.items[itmIndex].closingQty);
                //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                if(result.items[itmIndex].physicalDate) {
                  console.log(result.items[itmIndex].physicalDate)
                  console.log(cDate)
                  if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                    console.log('if');
                    result.items[itmIndex].closingQty = Utils.roundNumber(parseFloat(result.items[itmIndex].closingQty), 3) + Utils.roundNumber(parseFloat(itm.totalBalanceQty), 3);
                    console.log(result.items[itmIndex].closingQty);
                  } else {
                    console.log('else');
                    result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                    result.items[itmIndex].physicalDate = cDate;
                  }
                } else {
                  result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  result.items[itmIndex].physicalDate = cDate;
                }
                // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
                // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
                // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
              }
            }
          }
          else if (itm.totalIntermediateQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalIntermediateAmt).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalIntermediateQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalIntermediateAmt).toFixed(2);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalIntermediateAmt)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalIntermediateAmt)).toFixed(2);
            }
          }
          else if (itm.totalSaleQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalSaleAmount).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalSaleQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalSaleAmount).toFixed(2);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalSaleAmount)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalSaleAmount)).toFixed(2);
            }
          }
          else if (itm.totalWastageQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalWastageAmt).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalWastageQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalWastageAmt).toFixed(2);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalWastageAmt)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalWastageAmt)).toFixed(2);
            }
          }
          else if (itm.totalTransferQty_toStore != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              itm.purchaseQty = parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.purchaseAmount = parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].purchaseQty = parseFloat(parseFloat(result.items[itmIndex].purchaseQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].purchaseAmount = parseFloat(parseFloat(result.items[itmIndex].purchaseAmount) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
            }
          }
          else if (itm.totalTransferQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.totalTransferAmount).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.totalTransferQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.totalTransferAmount).toFixed(2);
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalTransferAmount)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.totalTransferAmount)).toFixed(2);
            }
          }
          else if (itm.itemSaleQty != undefined) {
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(-itm.itemSaleAmt).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.purchaseQty == undefined) {
                itm.purchaseQty = parseFloat(0).toFixed(3);
                itm.purchaseAmount = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(itm.itemSaleQty).toFixed(3);
              itm.consumptionAmount = parseFloat(itm.itemSaleAmt).toFixed(2);
              itm.UnitName = itm.baseUnit;
              result.items.push(itm);
            }
            else {
              result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.itemSaleAmt)).toFixed(2);

              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) - parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) - parseFloat(itm.itemSaleAmt)).toFixed(2);
            }
          }
          else if (itm.totalReturnAcceptQty != undefined) {
            //console.log('totalReturnAcceptQty', itm.totalReturnAcceptQty);
            var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
            if (itmIndex < 0) {
              if (itm.openingQty == undefined) {
                itm.openingQty = parseFloat(0).toFixed(3);
                itm.closingQty = parseFloat(itm.totalReturnAcceptQty).toFixed(3);
                itm.openingAmt = parseFloat(0).toFixed(3);
                itm.closingAmount = parseFloat(itm.totalReturnAcceptAmount).toFixed(3);
                itm.openingDate = new Date(startDate);
                itm.closingDate = new Date(endDateToShow);
              }
              if (itm.consumptionQty == undefined) {
                itm.consumptionQty = parseFloat(0).toFixed(3);
                itm.consumptionAmount = parseFloat(0).toFixed(3);
              }
              itm.consumptionQty = parseFloat(-itm.totalReturnAcceptQty).toFixed(3);
              itm.consumptionAmount = parseFloat(-itm.totalReturnAcceptAmount).toFixed(2);
              result.items.push(itm);
            }
            else {
              //console.log(angular.copy(result.items[itmIndex]));
              result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) - parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) - parseFloat(itm.totalReturnAcceptAmount)).toFixed(2);
              //console.log(result.items[itmIndex].closingQty, itm.totalReturnAcceptQty);
              result.items[itmIndex].closingQty = parseFloat(parseFloat(result.items[itmIndex].closingQty) + parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
              result.items[itmIndex].closingAmount = parseFloat(parseFloat(result.items[itmIndex].closingAmount) + parseFloat(itm.totalReturnAcceptAmount)).toFixed(2);
              //console.log(result.items[itmIndex].closingQty)
              //console.log(angular.copy(result.items[itmIndex]));
            }
          }
        });
        return result;
      };

      function spliceForCategoryAndItem(result,items){
        _.forEachRight(result.items,function(itm,i){
          var index=_.findIndex(items,{id:itm.itemId});
          if(index<0){
            result.items.splice(i,1);
          }
        });
        return result;
      };


      function convertItemInPreferredUnit(result){
        _.forEach(result.items,function(itm,i){
          var indeex=_.findIndex($scope.semiRecipe,{_id:itm.itemId});
          if(indeex>=0){
            var item=$scope.semiRecipe[indeex];
            if(item.selectedUnitId!=undefined){
              _.forEach(item.units,function(u,i){
                if(u._id==item.selectedUnitId._id){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        });
        return result;
      };

      //--------------------------------------Day Wise Variance-------------------------------------------------
      $scope.clearDateWiseTab= function(){
        ResetclearDateWiseTab();
      };

      function ResetclearDateWiseTab(){
        $scope.dateWiseForm={
          fromDate:new Date(),
          toDate:new Date(),
          maxDate:new Date(),
          stores:angular.copy($scope.stores)
        };
        $scope.dateWiseForm.store=$scope.dateWiseForm.stores[0];
        $scope.bindCategory_DateWiseForm($scope.dateWiseForm.store);
      };
      $scope.checkDateGreater_DateWise= function(date){
        var d=new Date(date);
        var dd=new Date($scope.dateWiseForm.toDate);
        if(d.getTime()>dd.getTime()){
          growl.success('Greater than toDate', {ttl: 3000});
          $scope.dateWiseForm.fromDate=new Date();
        }
      };

      $scope.checkDateLesser_DateWise= function(date){
        var d=new Date(date);
        var dd=new Date($scope.dateWiseForm.fromDate);
        if(d.getTime()<dd.getTime()){
          growl.success('Can not be Lesser than fromDate', {ttl: 3000});
          $scope.dateWiseForm.toDate= new Date();
        }
      };

      $scope.bindCategory_DateWiseForm=function(store){
        $scope.dateWiseForm.categories=[];
        $scope.dateWiseForm.availableItems=[];
        var str=angular.copy(store);
        _.forEach(str.category, function(c,i){
          $scope.dateWiseForm.categories.push(c);
        });
      };

      $scope.bindItems_DateWiseForm= function(category){
        $scope.dateWiseForm.availableItems=[];
        var cat=angular.copy(category);
        $scope.dateWiseForm.availableItems=[];
        $scope.dateWiseForm.item=undefined;
        if(cat!=null){
          _.forEach(cat.item, function(c,i){
            $scope.dateWiseForm.availableItems.push(c);
          });
        }
        $scope.dateWiseForm.availableItems.sort(compare);
      };

      $scope.startSearch_DateWise=function(){
        intercom.registerEvent('StockReports');
        var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dateWiseForm.fromDate))));
        var toDate = new Date($scope.dateWiseForm.toDate);
        var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dateWiseForm.toDate))));
        toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));



        var strId=$scope.dateWiseForm.store._id;
        var isKitchen=$scope.dateWiseForm.store.isKitchen;
        if($scope.dateWiseForm.item!=undefined || $scope.dateWiseForm.item!=null){
          var item=[{id:$scope.dateWiseForm.item._id,itemName:$scope.dateWiseForm.item.itemName}];
        }
        else if($scope.dateWiseForm.category!=undefined || $scope.dateWiseForm.category!=null){
          var item=[];
          _.forEach($scope.dateWiseForm.category.item, function(itm,i){
            var obj={id:itm._id,itemName:itm.itemName};
            item.push(obj);
          });
        }
        else if($scope.dateWiseForm.store!=undefined){
          var item=getItemForSearch(angular.copy($scope.dateWiseForm.store));
        };
        var toDate1=new Date(toDate);
        toDate1.setDate(toDate1.getDate()+1);
        var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};

        //StockReportNew.getConsumptionSummary(req, function (result){
        stockReportResetTime.getConsumptionSummary_IntermediateReport(req, function (result){
          console.log(result);
          var result=spliceAllItemsBeforePhysical_DateWise(result,toDateReset);
          result=formatDataForReport_DateWise(result,fromDate,toDateReset);
          result=spliceForCategoryAndItem(result,item);
          var res=convertItemInPreferredUnit_DateWise(result);
          var days=getDates(fromDate,toDateReset);
          var gtR=generateDateWiseVarianceReport(res.items,days);
          var index=gtR.length-1;
          gtR.splice(index,1);
          $scope.dateWiseForm.reportData=gtR;
        });
      };

      function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
          dateArray.push( {date:new Date (currentDate) });
          currentDate = currentDate.addDays(1);
        }
        console.log(dateArray);
        return dateArray;
      };

      Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
      };
      function convertItemInPreferredUnit_DateWise(result){
        _.forEach(result.items,function(itm,i){
          var indeex=_.findIndex($scope.semiRecipe,{_id:itm.itemId});
          if(indeex>=0){
            var item=$scope.semiRecipe[indeex];
            if(item.selectedUnitId!=undefined){
              _.forEach(item.units,function(u,i){
                if(u._id==item.selectedUnitId._id){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)/parseFloat(conversionFactor)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)/parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)*parseFloat(conversionFactor)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)*parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)/parseFloat(pconFac)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)/parseFloat(pconFac)).toFixed(3);
                        }
                        itm.varianceQty=parseFloat(parseFloat(itm.varianceQty)/parseFloat(pconFac)).toFixed(3);
                      }
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)*parseFloat(pconFac)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)*parseFloat(pconFac)).toFixed(3);
                        }
                        itm.varianceQty=parseFloat(parseFloat(itm.varianceQty)*parseFloat(pconFac)).toFixed(3);
                      }
                    }
                  }
                }
              });
            }
          }
        });
        return result;
      };
      function formatDataForReport_DateWise(result,startDate,endDate){
        result.items=[];
        _.forEach(result.beforeDate,function(itm,i){
          if(itm.totalOpeningQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
            }
          }
          else if(itm.totalBalanceQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              //itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
          }
          else if(itm.totalPurchaseQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
            }
          }
          else if(itm.totalIntermediateQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
            }
          }
          else if(itm.totalSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
            }
          }
          else if(itm.totalWastageQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
            }
          }
          else if(itm.itemSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              itm.UnitName=itm.baseUnit;
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
            }
          }
        });
        _.forEach(result.betweenDate,function(itm,i){
          if(itm.totalPurchaseQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalBalanceQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(0).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
            }
            itm.varianceQty=parseFloat(0).toFixed(3);
            itm.varianceInPercent=parseFloat(0).toFixed(2);
            itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalIntermediateQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalWastageQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            itm.wastageQty=parseFloat(itm.totalWastageQty).toFixed(3);
            itm.wastageAmt=parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalTransferQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.itemSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
            itm.UnitName=itm.baseUnit;
            result.items.push(itm);
          }

        });
        return result;
      };

      function getClosingQtyPreviousDate(items,pdate,item){
        var cQTy=0;
        var index=_.findIndex(items,{date:pdate});
        if(index>=0){
          var itmIndex=_.findIndex(items[index].items,{itemId:item.itemId});
          if(itmIndex>=0){
            if(items[index].items[itmIndex].physicalQty!="NA"){
              cQTy=parseFloat(items[index].items[itmIndex].closingQty).toFixed(3);
            }
            else
            {
              cQTy=parseFloat(items[index].items[itmIndex].physicalQty).toFixed(3);
            }
          }
        }
        return cQTy;
      };
      function getPreviousDate_Data(items,pdate){
        var data=[];
        var index=_.findIndex(items,{date:pdate});
        if(index>=0){
          var d=angular.copy(items[index].items);
          _.forEach(d,function(itm,i){
            if(itm.physicalQty != "NA")
              itm.openingQty=parseFloat(itm.physicalQty).toFixed(3);
            else
              itm.openingQty=parseFloat(itm.closingQty).toFixed(3);
            itm.closingQty = itm.openingQty;
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.physicalQty="NA";//parseFloat(0).toFixed(3);
            itm.varianceQty="NA";//parseFloat(0).toFixed(3);
            itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
            data.push(itm);
          });
        }
        return data;
      };
      function generateDateWiseVarianceReport(items,days){
        items.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        _.forEach(days,function(d,i){
          d.items=[];
          var pdate=new Date(d.date);
          pdate.setDate(pdate.getDate()-1);
          d.items=getPreviousDate_Data(days,pdate);
          var resetDate = new Date(d.date);
          resetDate = new Date(resetDate.setDate(resetDate.getDate() + 1));
          _.forEachRight(items,function(itm,ii){
            var cDate=new Date(itm.created);
            //cDate.setHours(0,0,0,0);
            if (new Date(d.date) > new Date(cDate)) {
              console.log('if');
              var index = _.findIndex(d.items, {itemId: itm.itemId});
              if (index < 0) {
                if (index == -1) {
                  itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                }
                else {
                  itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                  itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                }
                d.items.push(itm);
                items.splice(ii, 1);
              }
            }
            else if (new Date(d.date) <= new Date(cDate) && new Date(cDate) <= resetDate) {
              console.log('else');
              var index = _.findIndex(d.items, {itemId: itm.itemId});
              if (index < 0) {
                if (index == -1) {
                  itm.openingQty = parseFloat(itm.openingQty).toFixed(3);
                }
                else {
                  itm.openingQty = parseFloat(self.getClosingQtyPreviousDate(days, pdate, itm)).toFixed(3);
                  console.log(d.date, itm.openingQty);
                  itm.closingQty = parseFloat(parseFloat(itm.openingQty) + parseFloat(itm.purchaseQty) - parseFloat(itm.wastageQty) - parseFloat(itm.consumptionQty)).toFixed(3);
                }
                d.items.push(itm);
                items.splice(ii, 1);
              }
              else {
                d.items[index].openingQty = parseFloat(parseFloat(d.items[index].openingQty) + parseFloat(itm.openingQty)).toFixed(3);
                d.items[index].purchaseQty = parseFloat(parseFloat(d.items[index].purchaseQty) + parseFloat(itm.purchaseQty)).toFixed(3);
                d.items[index].consumptionQty = parseFloat(parseFloat(d.items[index].consumptionQty) + parseFloat(itm.consumptionQty)).toFixed(3);
                d.items[index].wastageQty = parseFloat(parseFloat(d.items[index].wastageQty) + parseFloat(itm.wastageQty)).toFixed(3);
                d.items[index].closingQty = parseFloat(parseFloat(d.items[index].closingQty) + parseFloat(itm.closingQty)).toFixed(3);
                if (itm.physicalQty != "NA") {
                  if (d.items[index].physicalQty != "NA") {
                    d.items[index].physicalQty = parseFloat(parseFloat(d.items[index].physicalQty) + parseFloat(itm.physicalQty)).toFixed(3);
                  }
                  else {
                    console.log(itm, d.date);
                    d.items[index].physicalQty = parseFloat(parseFloat(itm.physicalQty)).toFixed(3);
                  }
                  //d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                  //if(d.items[index].physicalQty!="NA"){
                  //  var dNO=parseFloat(i+1);
                  //
                  //  //d.items[index].openingQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                  //  d.items[index].closingQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(d.items[index].purchaseQty)-parseFloat(d.items[index].wastageQty)-parseFloat(d.items[index].consumptionQty)).toFixed(3);
                  //  d.items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                  //  d.items[index].varianceQty=parseFloat(parseFloat(d.items[index].closingQty)-parseFloat(d.items[index].physicalQty)).toFixed(3);
                  //  d.items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(d.items[index].varianceQty)/parseFloat(d.items[index].closingQty))*parseFloat(100)).toFixed(2);
                  //if(dNO>=0){
                  //  days[dNO].items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                  //  days[dNO].items[index].varianceQty=parseFloat(parseFloat(days[dNO].items[index].closingQty)-parseFloat(days[dNO].items[index].physicalQty)).toFixed(3);
                  //  days[dNO].items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(days[dNO].items[index].varianceQty)/parseFloat(days[dNO].items[index].closingQty))*parseFloat(100)).toFixed(2);
                  //}
                  //d.items[index].physicalQty="NA";
                }
                items.splice(ii, 1);
              }
            }
          });
          _.forEach(d.items, function (itm){
            if(itm.physicalQty != "NA"){
              itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(3);
              itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)/parseFloat(itm.closingQty))*parseFloat(100)).toFixed(2);
            }
          });
        });
        return days;
      };

      function formatDataDateWise(result){
        var items=[];
        var sortedArray=[];
        sortedArray=angular.copy(result.items);
        sortedArray.sort(function(a,b){
          return new Date(a.created).getTime()-new Date(b.created).getTime();
        });

        _.forEach(sortedArray, function(itm,i){
          var d=new Date(itm.created);
          d.setHours(0,0,0,0);
          var dINdex=_.findIndex(items,{date:d});
          if(dINdex<0){
            var dObj={date:d};
            dObj.items=[];
            dObj.items.push(itm)
            items.push(dObj);
          }
          else
          {
            var itemINdex=_.findIndex(items[dINdex].items,{itemId:itm.itemId});
            if(itemINdex<0){
              items[dINdex].items.push(itm);
            }
            else
            {
              items[dINdex].items[itemINdex].openingQty +=itm.closingQty==undefined?0:itm.closingQty;
              items[dINdex].items[itemINdex].totalPurchaseQty += (itm.totalPurchaseQty==undefined) ? 0 : itm.totalPurchaseQty;
              items[dINdex].items[itemINdex].totalSaleQty += (itm.totalSaleQty==undefined) ? 0 : itm.totalSaleQty;
              items[dINdex].items[itemINdex].totalTransferQty += (itm.totalTransferQty==undefined) ? 0 : itm.totalTransferQty;
              items[dINdex].items[itemINdex].totalWastageQty += (itm.totalWastageQty==undefined) ? 0 : itm.totalWastageQty;
              items[dINdex].items[itemINdex].closingQty = ((items[dINdex].items[itemINdex].openingQty+items[dINdex].items[itemINdex].totalPurchaseQty+items[dINdex].items[itemINdex].totalTransferQty)-(items[dINdex].items[itemINdex].totalSaleQty+items[dINdex].items[itemINdex].totalWastageQty));
            }
          }
        });

        items.sort(function(a,b){
          return new Date(a.date).getTime()-new Date(b.date).getTime();
        });
        return items;
      };

      function formatDataDatewise_Date(result){
        var items=[];
        _.forEach(result,function(r,i){
          if(i!=0){
            var aboveItem=angular.copy(result[i-1].items);
            var belowItem=angular.copy(result[i].items);
            _.forEach(result[i].items, function(itm,ii){
              itm.openingQty =itm.closingQty==undefined?0:itm.closingQty;
              itm.totalPurchaseQty = (itm.totalPurchaseQty==undefined) ? 0 : itm.totalPurchaseQty;
              itm.totalSaleQty = (itm.totalSaleQty==undefined) ? 0 : itm.totalSaleQty;
              itm.totalTransferQty = (itm.totalTransferQty==undefined) ? 0 : itm.totalTransferQty;
              itm.totalWastageQty = (itm.totalWastageQty==undefined) ? 0 : itm.totalWastageQty;
              itm.closingQty = ((itm.openingQty+itm.totalPurchaseQty+itm.totalTransferQty)-(itm.totalSaleQty+itm.totalWastageQty));
            });

            _.forEach(aboveItem,function(a,j){
              var index=_.findIndex(belowItem,{itemId:a.itemId});
              if(index<0){
                a.openingQty=a.closingQty;
                a.totalTransferQty=0;
                a.totalWastageQty=0;
                a.totalPurchaseQty=0;
                a.totalSaleQty=0;
                result[i].items.push(a);
              }
              else
              {
                if(belowItem[index].closingQty==undefined){
                  belowItem[index].closingQty=aboveItem[j].openingQty;
                }
                aboveItem[j].openingQty=belowItem[index].closingQty;
                aboveItem[j].totalPurchaseQty= (belowItem[index].totalPurchaseQty==undefined) ? 0 : belowItem[index].totalPurchaseQty;
                aboveItem[j].totalSaleQty= (belowItem[index].totalSaleQty==undefined) ? 0 : belowItem[index].totalSaleQty;
                aboveItem[j].totalTransferQty= (belowItem[index].totalTransferQty==undefined) ? 0 : belowItem[index].totalTransferQty;
                aboveItem[j].totalWastageQty= (belowItem[index].totalWastageQty==undefined) ? 0 : belowItem[index].totalWastageQty;
                aboveItem[j].closingQty= ((aboveItem[j].openingQty+aboveItem[j].totalPurchaseQty+aboveItem[j].totalTransferQty)-(aboveItem[j].totalSaleQty+aboveItem[j].totalWastageQty));

                angular.copy(aboveItem[j],result[i].items[index]);
              }
            });

          }
        });
        return result;
      };
      //----------------------------------------------------Reports----------------------------------------------
      $scope.exportToPdf_purchaseConsumption=function(){
        var table=document.getElementById('purchaseConsumptionReport').innerHTML;
        var printer = window.open('', '', 'width=600,height=600');
        printer.document.open("text/html");
        printer.document.write(table);
        printer.document.close();
        printer.focus();
        printer.print();
      };
      $scope.exportToExcel_purchaseConsumption=function(){
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('purchaseConsumptionReport').innerHTML));
      };

      $scope.exportToPdf_dateWise=function(){
        var table=document.getElementById('dateWiseReport').innerHTML;
        var printer = window.open('', '', 'width=600,height=600');
        printer.document.open("text/html");
        printer.document.write(table);
        printer.document.close();
        printer.focus();
        printer.print();
      };
      $scope.exportToExcel_dateWise=function(){
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('dateWiseReport').innerHTML));
      };
    }]);
