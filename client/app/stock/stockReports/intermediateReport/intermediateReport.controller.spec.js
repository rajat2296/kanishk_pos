'use strict';

describe('Controller: IntermediateReportCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var IntermediateReportCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IntermediateReportCtrl = $controller('IntermediateReportCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
