'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('shelfLifeReports', {
        url: '/shelfLifeReports',
        templateUrl: 'app/stock/stockReports/shelfLifeReports/shelfLifeReports.html',
        controller: 'ShelfLifeReportsCtrl'
      });
  });