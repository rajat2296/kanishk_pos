'use strict';

describe('Controller: ShelfLifeReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var ShelfLifeReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShelfLifeReportsCtrl = $controller('ShelfLifeReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
