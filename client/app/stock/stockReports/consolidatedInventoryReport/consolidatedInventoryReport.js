'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('consolidatedInventoryReportCtrl', {
        url: '/consolidatedInventoryReportCtrl',
        templateUrl: 'app/stock/stockReports/consolidatedInventoryReportCtrl/consolidatedInventoryReportCtrl.html',
        controller: 'ConsolidatedInventoryReportCtrl'
      });
  });