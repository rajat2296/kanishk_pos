'use strict';

angular.module('posistApp')
  .controller('ConsolidatedInventoryReportCtrl',['$q','$resource','$scope','$filter','growl','vendor','currentUser','$rootScope','$state','$modal','stockRequirement','Deployment','localStorageService','deployment','Utils','intercom', function ($q,$resource,$scope,$filter,growl,vendor,currentUser,$rootScope,$state,$modal,stockRequirement,Deployment,localStorageService,deployment,Utils,intercom) {
    currentUser.deployment_id=localStorageService.get('deployment_id');
    $scope.vendors=[];
    $scope.user=currentUser;
    $scope.outlets=[];
    $scope.isBaseKitchen = false;
    // function getVendors_ByDeployment(){
    //   return vendor.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id});
    // };

    // var allPromise=$q.all([ 
    //     getVendors_ByDeployment()
    // ]);          

    // allPromise.then(function (value){
      // $scope.vendors=value[0];// $rootScope.vendors;
      // bindHoVendors(value[0]);
    // });

    vendor.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.vendors=result;
        bindHoVendors(angular.copy($scope.vendors));
    });

    Deployment.get({tenant_id:currentUser.tenant_id}).$promise.then(function (result){
        $scope.outlets=result;
        $scope.requirementSupplyForm.availableOutlets=bindOutlets(angular.copy($scope.outlets));
        $scope.requirementForm.availableOutlets=bindOutlets(angular.copy($scope.outlets));
        var index=_.findIndex(result,{_id:localStorageService.get('deployment_id')});
        console.log("Deployment",result[index]);
        $scope.isBaseKitchen=result[index].isBaseKitchen;
    });

    function getYesterDayDate() {
      var d=new Date();
      d.setDate(d.getDate()-1);
      return d;
    };

    function parseDate(str){
      var d=$filter('date')(str, 'dd-MMM-yyyy');
      return d;
  };

    $scope.requirementForm={
      fromDate:new Date(),//getYesterDayDate(),     
      toDate:new Date(),
      maxDate:new Date()
    };

    $scope.clearRequirementTab=function(){
      $scope.requirementForm={
        fromDate:new Date(),//getYesterDayDate(),     
        toDate:new Date(),
        maxDate:new Date()
      };
   bindHoVendors(angular.copy($scope.vendors));
      $scope.requirementForm.availableOutlets=bindOutlets(angular.copy($scope.outlets));
    };

    $scope.checkDateGreater= function(date){
      var d=new Date(date);
      var dd=new Date($scope.requirementForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.requirementForm.fromDate=new Date();//getYesterDayDate();
      }
    };

    $scope.checkDateLesser= function(date){
      var d=new Date(date);
      var dd=new Date($scope.requirementForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.requirementForm.toDate= new Date();
      }
    };

    function bindHoVendors(vendors){
      $scope.requirementForm.vendors=[];
      $scope.requirementForm.availableItems=[];
      $scope.requirementForm.items={};
      _.forEach(vendors, function(ven,i){
        if(ven.type=="HO Vendor"){
          $scope.requirementForm.vendors.push(ven);
        }
      });
      $scope.requirementSupplyForm.vendors = $scope.requirementForm.vendors;
    };

    function compare(a,b) {
      if (a.itemName < b.itemName)
        return -1;
      if (a.itemName > b.itemName)
        return 1;
      return 0;
    };
    
    $scope.bindItems= function(vendor){
      $scope.requirementForm.availableItems=[];
      var ven=angular.copy(vendor);
      _.forEach(ven.pricing.category, function(cat,i){
        _.forEach(cat.item,function(itm,ii){
          $scope.requirementForm.availableItems.push(itm);
        });
      });
      $scope.requirementForm.availableItems.sort(compare);
    };

    function formatRequirement(result,vendor,items){
      if(vendor==undefined){
        _.forEach(result,function(r,i){
          if(r.supplyDate==null){
            _.forEach(r.items, function(sp,ii){
              delete  sp.supplyQty;
              delete  sp.supplyComment;
              delete sp.receiveQty;
            });
          }
          else{
            _.forEach(r.items, function(sp,ii){
              sp.sPrice=sp.price;
              sp.sUnitName=sp.selectedUnit.unitName;
              sp.sAmount=(sp.price*sp.supplyQty);
            });
          }

          if(r.receiveDate==null){
            _.forEach(r.items, function(sp,ii){
              delete sp.receiveQty;  
              sp.suspenceQty="NA";
              sp.suspencePrice="NA";
            });
          }
          else{
            _.forEach(r.items, function(sp,ii){
              sp.rPrice=sp.price;
              sp.rUnitName=sp.selectedUnit.unitName;
              sp.rAmount=(sp.price*sp.receiveQty);
              sp.suspenceQty=parseFloat(sp.supplyQty-sp.receiveQty);
              sp.suspencePrice=parseFloat(sp.sAmount-sp.rAmount);
            });
          }
        });
      }
      else
      {
        _.forEachRight(result,function(r,i){
          if(r.vendorName==vendor.vendorName){
            if(r.supplyDate==null){
              _.forEach(r.items, function(sp,ii){
                delete  sp.supplyQty;
                delete  sp.supplyComment;
                delete sp.receiveQty;
              });
            }
            else{
              _.forEach(r.items, function(sp,ii){
                sp.sPrice=sp.price;
                sp.sUnitName=sp.selectedUnit.unitName;
                sp.sAmount=(sp.price*sp.supplyQty);
              });
            }

            if(r.receiveDate==null){
              _.forEach(r.items, function(sp,ii){
                delete sp.receiveQty;  
                sp.suspenceQty="NA";
                sp.suspencePrice="NA";
              });
            }
            else{
              _.forEach(r.items, function(sp,ii){
                sp.rPrice=sp.price;
                sp.rUnitName=sp.selectedUnit.unitName;
                sp.rAmount=(sp.price*sp.receiveQty);
                sp.suspenceQty=parseFloat(sp.supplyQty-sp.receiveQty);
                sp.suspencePrice=parseFloat(sp.sAmount-sp.rAmount);
              });
            }
          }
          else
          {
            result.splice(i,1);
          }
        });
      }
      return result;
    };

    function formatRequirementBK(result,outlets,items){
      console.log("outlet",outlets)
      //console.log("r",angular.copy(result) )
      if(outlets==undefined){
       
      }
      else
      {
        _.forEachRight(result,function(r,i){
          if(r.deploymentName==outlets.name){
            //console.log("match")
          }
          else
          {
            result.splice(i,1);
          }
        });
      }
      //console.log("res",angular.copy(result) )
      return result;
    };

    function formatStore(res) {
            _.forEach(res,function(r){

            console.log("indent",angular.copy(r))
               var stores = r.store;
                stores.vendor.category = [];
                _.forEach(r.items, function (itm, i) {
                  var index = _.findIndex(stores.vendor.category, {_id: itm.fromCategory._id});
                  if (index < 0) {
                    stores.vendor.category.push(itm.fromCategory);
                  }
                });

                _.forEach(stores.vendor.category, function (c, i) {
                  c.items = [];
                  _.forEachRight(r.items, function (itm, ii) {
                    // console.log("cat",angular.copy(c))
                    // console.log("itmmm",angular.copy(itm))
                    if(itm.fromCategory)
                    if (c._id == itm.fromCategory._id) {
                      delete itm.fromCategory;
                      //console.log("itm",angular.copy(itm))
                      c.items.push(itm);
                    }
                  });
                });
            })
            return res
          };

  $scope.startSearchRequirement=function(){
        intercom.registerEvent('StockReports');
        var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.requirementForm.fromDate))));
        console.log('fromDate',fromDate);
        
        var toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.requirementForm.toDate))));
        console.log('toDate',toDate);
      var req={
        startDate:fromDate,
        endDate:toDate,
        tenant_id:currentUser.tenant_id,
        deployment_id:currentUser.deployment_id,
        isBaseKitchen:$scope.isBaseKitchen
      };
      if ($scope.requirementForm.vendor!=undefined){
        req.vendor_id=$scope.requirementForm.vendor._id;
      }
      console.log('req',req);
      console.log('$scope.requirementForm.outlet',$scope.requirementForm.outlet);
      console.log('BaseKitchen',$scope.isBaseKitchen);
      stockRequirement.getConsolidatedInventoryReport(req,function(res){
      var r=formatStore(res)
      console.log("r",r)
      var result=formatRequirementBK(r,$scope.requirementForm.outlet,$scope.requirementForm.availableItems);
      
      
      
      var arr=[]
      //console.log("group",group)
      _.forEach(result,function(r){
        console.log("r",angular.copy(r) )
          _.forEach(r.store.vendor.category,function(cat){
              arr.push(cat)
          })

      })  
      //console.log("consolidated",arr)
      var group=_.groupBy(arr, function(b) {
        
       return b._id
      })

      // var itemGroup=_.groupBy(group.items, function(b) {
      //     console.log("b",b)
      //  return b._id
      // })
      var catArray=[]

      var itemGroup,cat,catFinal,reportData=[]
      _.forEach(group,function(a,ii){
            //console.log(a,"obj");
            var count=0
            _.forEach(a,function(i){
              
              if(count==0){
                count++
                 cat={
                  _id:i._id,
                  categoryName:i.categoryName,
                  item:[]
                }
              }
              
              _.forEach(i.items,function(n){
          
                  cat.item.push(n)
              })

                //console.log("cat ",cat)
                catFinal=cat
            })  
            //console.log("catFinal",catFinal)
            reportData.push(catFinal)
        })
      // console.log("group",group)

      // console.log(" item group",itemGroup)
      // console.log("cat final",catFinal)
      // console.log("reportdata",angular.copy(reportData) )

            var report = [],cat1
                var count = 0
                _.forEach(reportData, function(r) {
                    cat1 = {
                            _id: r._id,
                            categoryName: r.categoryName,
                            item: []
                        }
                        //console.log("r",r)
                    var c = 0,exist=false
                    _.forEach(r.item, function(itm) {
                            //console.log("it",itm)
                            if (c == 0) {
                                cat1.item.push(itm)
                            }
                            _.forEach(cat1.item, function(it) {
                                if (c != 0)
                                    if (itm._id.toString() == it._id.toString()) {
                                        it.subTotal = it.subTotal + itm.subTotal
                                        //console.log("qty",angular.copy(itm.supplyQty))
                                        it.qty = it.qty + itm.qty
                                        it.supplyQty = itm.supplyQty + it.supplyQty
                                        exist=true
                                    }
                                c++
                            })
                            if(exist==false)
                                 {
                                    if(c!=1)
                                    cat1.item.push(itm)

                                 }
                            
                        })
                        //console.log("cat1",angular.copy(cat1) )
                    report.push(angular.copy(cat1))
                })
      console.log("report ",angular.copy(report) )
      $scope.requirementForm.reportDataBK=undefined;
      $scope.requirementForm.reportDataBK= angular.copy(report)
      //console.log($scope.requirementForm.reportDataBK);
    
    });

    };
    //-------------------------------------------Requirement Supply --------------------------------------------------//
    $scope.requirementSupplyForm={
      fromDate:new Date(),//getYesterDayDate(),     
      toDate:new Date(),
      maxDate:new Date()
    };

    $scope.clearRequirementSupplyTab=function(){
      $scope.requirementSupplyForm={
        fromDate:new Date(),//getYesterDayDate(),     
        toDate:new Date(),
        maxDate:new Date()
      };
      $scope.requirementSupplyForm.availableOutlets=bindOutlets(angular.copy($scope.outlets));
      $scope.requirementSupplyForm.vendors=$scope.requirementForm.vendors;
    };

    $scope.checkDateGreater_requirementSupplyForm= function(date){
      var d=new Date(date);
      var dd=new Date($scope.requirementSupplyForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.requirementSupplyForm.fromDate=new Date();//getYesterDayDate();
      }

    };

    $scope.checkDateLesser_requirementSupplyForm= function(date){
      var d=new Date(date);
      var dd=new Date($scope.requirementSupplyForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.requirementSupplyForm.toDate= new Date();
      }
    };
    //$scope.requirementSupplyForm.availableOutlets=??;
    function bindOutlets(avOut){
      var r=[];
      _.forEach(avOut,function(o,i){
        if(o.isMaster==false){
          r.push(o);
        }
      });
      return r;
    };

   $scope.startSearchSupplyRequirement= function(){
      intercom.registerEvent('StockReports');
          var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.requirementSupplyForm.fromDate))));
          
          console.log('fromDate',fromDate);
          
          var toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.requirementSupplyForm.toDate))));
          console.log('toDate',toDate);
      if($scope.isBaseKitchen!=false)
      {
          var req={
            startDate:fromDate,
            endDate:toDate,
            tenant_id:currentUser.tenant_id,
            deployment_id:currentUser.deployment_id
          };
         stockRequirement.getSuplyRequirementReport(req,function(result){
          $scope.requirementSupplyForm.reportData=undefined;
          console.log(result);
          var ss=formatRequirement(result,$scope.requirementSupplyForm.vendor,$scope.requirementSupplyForm.availableItems);
          $scope.requirementSupplyForm.reportData=ss;
          console.log(ss);
        });
    
      }// end of if
      if($scope.isBaseKitchen==true)
      {
          var req={
          startDate:fromDate,
          endDate:toDate,
          tenant_id:currentUser.tenant_id,
          deployment_id:currentUser.deployment_id,
          isBaseKitchen:$scope.isBaseKitchen
        };
        stockRequirement.getRequirementReport(req,function(result){
        $scope.requirementSupplyForm.reportData=undefined;
        $scope.requirementSupplyForm.reportDataBK=undefined;
        console.log(result);
          var sup=[];
          var ss=formatRequirementBK(result,$scope.requirementSupplyForm.outlet,$scope.requirementSupplyForm.availableItems);
          _.forEach(ss,function(s){
            if(s.supplyDate!=null)
              sup.push(s);
          })
          console.log(sup);
          $scope.requirementSupplyForm.reportDataBK=sup;
          console.log($scope.requirementSupplyForm.reportDataBK);
          console.log(ss);
      });

      }

    };
    //--------------------------------------Reoprts-------------------------------------------------
    $scope.exportToPdf_requirement=function(){
      
        var table=document.getElementById('requirementReportBK').innerHTML;
   
       var printer = window.open('', '', 'width=600,height=600');
       printer.document.open("text/html");
       printer.document.write(table);
       printer.document.close();
       printer.focus();
       printer.print();
    };
    $scope.exportToExcel_requirement=function(){
     window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('requirementReportBK').innerHTML));
       // window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('requirementReport').innerHTML));
    };
    $scope.exportToPdf_requirementSupply=function(){
      if($scope.isBaseKitchen==true)
        var table=document.getElementById('requirementSupplyReportBk').innerHTML;
      else
        var table=document.getElementById('requirementSupplyReport').innerHTML;
       var printer = window.open('', '', 'width=600,height=600');
       printer.document.open("text/html");
       printer.document.write(table);
       printer.document.close();
       printer.focus();
       printer.print();
    };
    $scope.exportToExcel_requirementSupply=function(){
      if($scope.isBaseKitchen==true)
      {
        console.log("csv Supply BK")
        var html =requirementReportHTMLBK($scope.requirementSupplyForm.reportDataBK);
        var csvString = $scope.convertToCSV();
         angular.element(document.getElementById('reqSupply'))
          .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
          .attr('download', 'RequirementReportBK.csv');
    
        //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('requirementSupplyReportBk').innerHTML));
      }
        
      else
      {
        console.log("csv Supply outlet")
        var html =requirementReportHTML($scope.requirementSupplyForm.reportData);
        var csvString = $scope.convertToCSV();
        angular.element(document.getElementById('reqSupply'))
          .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
          .attr('download', 'RequirementSupplyReport.csv');
  
        //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('requirementSupplyReport').innerHTML));
      }
        
    };

    function requirementReportHTML(data)
    {
                  var html='<table class="table table-curved" style="width:100%" border="1">';
                      html+='<thead>';
                      html+='<tr>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="3">Requested Req </th>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="6">Supplied Req </th>';
                       html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="6">Received Req </th>';
                       html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="6">Suspense </th>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='</tr>';
                      html+='</thead>';
                      html+='<tbody>';

                      html+='<tr>';
                        html+='<td style="background-color:#abc123">ItemName </td>';

                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Unit </td>';
                        html+='<td style="background-color:#abc123">Comment </td>';

                        html+='<td style="background-color:#abc123">Date </td>';
                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Unit</td>';
                        html+='<td style="background-color:#abc123">UnitPrice </td>';
                        html+='<td style="background-color:#abc123">Amount </td>';
                        html+='<td style="background-color:#abc123">Comment</td>';

                        html+='<td style="background-color:#abc123">Date </td>';
                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Unit </td>';
                        html+='<td style="background-color:#abc123">UnitPrice </td>';
                        html+='<td style="background-color:#abc123">Amount </td>';
                        html+='<td style="background-color:#abc123">Comment </td>';

                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Amount </td>';
                        html+='</tr>';

                        
                        console.log("data",data)
                      _.forEach(data,function(item,i){
                        console.log("itemss",item)

                        html+='<tr>';
                        html+='</tr>';
                        html+='<tr>';
                              html+='<td style="background-color:#abc123">' + moment(new Date(item.created)).format('DD-MM-YYYY hh:mm a')  +'</td>';
                        html+='</tr>';
                        html+='<tr>';
                        html+='<td style="background-color:#abc123"> Req No </td>';
                        html+='<td style="background-color:#abc123">' + item.billNo  +'</td>';
                        html+='</tr>';
                        html+='<tr>';
                        html+='<td style="background-color:#abc123"> Requester </td>';
                        html+='<td style="background-color:#abc123">' + item.vendorName  +'</td>';
                        
                        
                        html+='</tr>';
                        _.forEach(item.items,function(it,ii){
                          html+='<tr>';
                          html+='<td  style="background-color: #B9FCDD; font-weight:bold;">';
                          html+='<td>'+it.itemName+'</td>';
                          html+='<td>'+it.qty+'</td>';
                          if(it.selectedUnit)
                            html+='<td>'+it.selectedUnit.unitName+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.comment)
                            html+='<td>'+it.comment+'</td>';
                          else
                            html+='<td> </td>';

                          if(item.supplyDate)
                            html+='<td>'+ moment(new Date(item.supplyDate)).format('DD-MM-YYYY hh:mm a')  +'</td>';
                          else
                            html+='<td> </td>';
                          if(it.supplyQty)
                            html+='<td>'+it.supplyQty+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.sUnitName)
                            html+='<td>'+it.sUnitName+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.sPrice)
                            html+='<td>'+it.sPrice+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.sAmount)
                            html+='<td>'+it.sAmount+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.supplyComment)
                            html+='<td>'+it.supplyComment+'</td>';
                          else
                            html+='<td> </td>';
                          if(item.receiveDate)
                            html+='<td>'+moment(new Date(item.receiveDate)).format('DD-MM-YYYY hh:mm a')  +'</td>';
                          else
                            html+='<td> </td>';
                          if(it.receiveQty)
                            html+='<td>'+it.receiveQty+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.rUnitName)
                            html+='<td>'+it.rUnitName+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.rPrice)
                            html+='<td>'+it.rPrice+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.rAmount)
                            html+='<td>'+it.rAmount+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.receiveComment)
                            html+='<td>'+it.receiveComment+'</td>';
                          else
                            html+='<td> </td>';

                          if(it.suspenceQty)
                            html+='<td>'+it.suspenceQty+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.suspencePrice)
                            html+='<td>'+it.suspencePrice+'</td>';                          
                          else
                            html+='<td> </td>';

                          
                          html+='</tr>';
                          
                          
                          
                        });
                       
                      });
                      html+='</tbody>';                   
                      html+='</table>';
                  $scope.CSV = html;
                  return html;
    }

    function requirementReportHTMLBK(data)
    {
                  var html='<table class="table table-curved" style="width:100%" border="1">';
                      html+='<thead>';
                      html+='<tr>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="3">Requested Req </th>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="6">Supplied Req </th>';
                       html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="6">Received Req </th>';
                       html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="6">Suspense </th>';
                      html+='<td style="background-color:#abc123"> </td>';
                      html+='</tr>';
                      html+='</thead>';
                      html+='<tbody>';

                      html+='<tr>';
                        html+='<td style="background-color:#abc123">ItemName </td>';

                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Unit </td>';
                        html+='<td style="background-color:#abc123">Comment </td>';

                        html+='<td style="background-color:#abc123">Date </td>';
                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Unit</td>';
                        html+='<td style="background-color:#abc123">UnitPrice </td>';
                        html+='<td style="background-color:#abc123">Amount </td>';
                        html+='<td style="background-color:#abc123">Comment</td>';

                        html+='<td style="background-color:#abc123">Date </td>';
                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Unit </td>';
                        html+='<td style="background-color:#abc123">UnitPrice </td>';
                        html+='<td style="background-color:#abc123">Amount </td>';
                        html+='<td style="background-color:#abc123">Comment </td>';

                        html+='<td style="background-color:#abc123">Qty </td>';
                        html+='<td style="background-color:#abc123">Amount </td>';
                        html+='</tr>';

                        
                        console.log("data",data)
                      _.forEach(data,function(item,i){
                        console.log("itemss",item)

                        html+='<tr>';
                        html+='</tr>';
                        html+='<tr>';
                              html+='<td style="background-color:#abc123">' + moment(new Date(item.created)).format('DD-MM-YYYY hh:mm a')  +'</td>';
                        html+='</tr>';
                        html+='<tr>';
                        html+='<td style="background-color:#abc123"> Req No </td>';
                        html+='<td style="background-color:#abc123">' + item.billNo  +'</td>';
                        html+='</tr>';
                        html+='<tr>';
                        html+='<td style="background-color:#abc123"> Requester </td>';
                        html+='<td style="background-color:#abc123">' + item.deploymentName  +'</td>';
                        
                        
                        html+='</tr>';
                        _.forEach(item.items,function(it,ii){
                          html+='<tr>';
                          html+='<td  style="background-color: #B9FCDD; font-weight:bold;">';
                          html+='<td>'+it.itemName+'</td>';
                          html+='<td>'+it.qty+'</td>';
                          if(it.selectedUnit)
                            html+='<td>'+it.selectedUnit.unitName+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.comment)
                            html+='<td>'+it.comment+'</td>';
                          else
                            html+='<td> </td>';

                          if(item.supplyDate)
                            html+='<td>'+ moment(new Date(item.supplyDate)).format('DD-MM-YYYY hh:mm a')  +'</td>';
                          else
                            html+='<td> </td>';
                          if(it.supplyQty)
                            html+='<td>'+it.supplyQty+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.sUnitName)
                            html+='<td>'+it.sUnitName+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.sPrice)
                            html+='<td>'+it.sPrice+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.sAmount)
                            html+='<td>'+it.sAmount+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.supplyComment)
                            html+='<td>'+it.supplyComment+'</td>';
                          else
                            html+='<td> </td>';
                          if(item.receiveDate)
                            html+='<td>'+moment(new Date(item.receiveDate)).format('DD-MM-YYYY hh:mm a')  +'</td>';
                          else
                            html+='<td> </td>';
                          if(it.receiveQty)
                            html+='<td>'+it.receiveQty+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.rUnitName)
                            html+='<td>'+it.rUnitName+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.rPrice)
                            html+='<td>'+it.rPrice+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.rAmount)
                            html+='<td>'+it.rAmount+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.receiveComment)
                            html+='<td>'+it.receiveComment+'</td>';
                          else
                            html+='<td> </td>';

                          if(it.suspenceQty)
                            html+='<td>'+it.suspenceQty+'</td>';
                          else
                            html+='<td> </td>';
                          if(it.suspencePrice)
                            html+='<td>'+it.suspencePrice+'</td>';                          
                          else
                            html+='<td> </td>';

                          
                          html+='</tr>';
                          
                          
                          
                        });
                       
                      });
                      html+='</tbody>';                   
                      html+='</table>';
                  $scope.CSV = html;
                  return html;
    }

    $scope.convertToCSV = function(){
      var CSV = $scope.CSV.replace(/,/g,'\,')
      CSV = CSV.replace(/<td\w*[a-zA-Z=%'"-:;# ]*>/g,'')
      CSV = CSV.replace(/<th\w*[a-zA-Z=%'"-:;# ]*>/g,'')
      CSV = CSV.replace(/<h2\w*[a-zA-Z=%'"-:;# ]*>/g,'')
      CSV = CSV.replace(/<table\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<tfoot\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<div\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<caption\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<thead\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<tbody\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<strong\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<i\w*[a-zA-Z=%'"-:;# ]*>/g,'')
      CSV = CSV.replace(/<tr\w*[a-zA-Z=%'"-:;# ]*>/g,'')
      CSV = CSV.replace(/<span\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/<b\w*[a-zA-Z=%"'-:;# ]*>/g,'')
      CSV = CSV.replace(/&nbsp;/g,'')
      CSV = CSV.replace(/&nbsp/g,'')
      CSV = CSV.replace(/<\/span>/g,'')
      CSV = CSV.replace(/<\/b>/g,'')
      CSV = CSV.replace(/<\/i>/g,'')
      CSV = CSV.replace(/<\/thead>/g,'')
      CSV = CSV.replace(/<\/tbody>/g,'')
      CSV = CSV.replace(/<\/strong>/g,'')
      CSV = CSV.replace(/<\/th>/g,',')
      CSV = CSV.replace(/<\/div>/g,',')
      CSV = CSV.replace(/<\/h2>/g,',')
      CSV = CSV.replace(/<\/table>/g,'')
      CSV = CSV.replace(/<\/tfoot>/g,'')
      CSV = CSV.replace(/<\/caption>/g,'')
      CSV = CSV.replace(/<\/td>/g,',')
      CSV = CSV.replace(/<\/tr>/g,"\r\n")
      console.log(CSV);
      return CSV;
    }

  }]);
