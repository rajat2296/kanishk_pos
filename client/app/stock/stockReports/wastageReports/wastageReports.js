'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('wastageReports', {
        url: '/wastageReports',
        templateUrl: 'app/stock/stockReports/wastageReports/wastageReports.html',
        controller: 'WastageReportsCtrl'
      });
  });