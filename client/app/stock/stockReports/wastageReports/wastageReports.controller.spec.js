'use strict';

describe('Controller: WastageReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var WastageReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WastageReportsCtrl = $controller('WastageReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
