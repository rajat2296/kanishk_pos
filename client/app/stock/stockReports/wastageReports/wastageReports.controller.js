'use strict';

angular.module('posistApp')
  .controller('WastageReportsCtrl',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','StockReport','stockItem','stockRecipe','deployment','Utils','property','intercom','stockUnit', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,StockReport,stockItem,stockRecipe,deployment,Utils,property,intercom,stockUnit) {
    $scope.message = 'Hello';
    $scope.stockitems = [];
    $scope.stockRecipes = [];
    $scope.stockUnits = []
    stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
      $scope.stockitems=result;
      $scope.binditems_ItemForm(1);
    });
    stockRecipe.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
      $scope.stockRecipes=result;
      formatRecipe();
    });
    stockUnit.get({tenant_id: currentUser.tenant_id,deployment_id: currentUser.deployment_id}).$promise.then(function (result){
    $scope.stockUnits=result;
    });
      $scope.resetSerialNumber=false
    $scope.hidePricing=false
    //  property.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
     
    //   if(result.length==0)
    //   {   
    //      $scope.resetSerialNumber=true
    //   }
    //   else
    //   {
    //     $scope.settings=result[0];
    //     if($scope.settings.resetSerialNumber==undefined)
    //       $scope.resetSerialNumber=true
    //     else
    //       $scope.resetSerialNumber=$scope.settings.resetSerialNumber
    //       console.log($scope.resetSerialNumber);   
    //   }
     
    // });
    function formatRecipe(){
      _.forEachRight($scope.stockRecipes,function(r,i){
        if(r.isSemiProcessed==false){
          r.itemName=r.recipeName;
        }
        else
        {
          $scope.stockRecipes.splice(i,1);
        }
      });
    };
    function getYesterDayDate() {
      var d=new Date();
      d.setDate(d.getDate()-1);
      return d;
    };

    function parseDate(str){
      var d=$filter('date')(str, 'dd-MMM-yyyy');
      return d;
    };

    $scope.itemWiseForm={
      fromDate:new Date(),//getYesterDayDate(),
      toDate:new Date(),
      maxDate:new Date()
    };

    $scope.checkDateGreater= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.itemWiseForm.fromDate=new Date();//getYesterDayDate()
      }
    };

    $scope.checkDateLesser= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.itemWiseForm.toDate= new Date();
      }
    };
    $scope.binditems_ItemForm=function(entryType){
      if(entryType==1){
        $scope.itemWiseForm.availableItems=$scope.stockitems;
      }
      else
      {
        $scope.itemWiseForm.availableItems=$scope.stockRecipes;
      }
    };
var wastageEntry='';
    $scope.startSearch_ItemWise=function(){
      intercom.registerEvent('StockReports');
      console.log('$scope.itemWiseForm',$scope.itemWiseForm)
      var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseForm.fromDate))));
      console.log('fromDate',fromDate);
      var toDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseForm.toDate))));
      console.log('toDate',toDate);

      /*var fromDate=new Date($scope.itemWiseForm.fromDate);
      var fromDate=new Date($scope.itemWiseForm.fromDate);
      fromDate.setHours(0, 0, 0, 0);
      var toDate=new Date($scope.itemWiseForm.toDate);
      toDate.setHours(0, 0, 0, 0);*/
      var itemType=$scope.itemWiseForm.entryType;
      if(itemType==1)
      {
        itemType = "RawMaterial";
      }
      else
      {
        itemType = "MenuItem";
      }
    var wastageEntryType = $scope.itemWiseForm.wastageEntryType;
    console.log('wastageEntryType',wastageEntryType);
      if(wastageEntryType ==1)
      {
        wastageEntry="Wastage";
      }
      else
      {
        wastageEntry="Re-Use";
      }

      var req={"fromDate":fromDate,"toDate":toDate,"itemType":itemType,"wastageEntryType":wastageEntryType,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};
      //console.log(req);
      StockReport.getWastageReport(req,function(result){
        console.log(result);
        var d=convertItemInPreferredUnit(result);
        //console.log(angular.copy(d));
        var data=formatDataItemWise(d);
        //console.log(angular.copy(data));
        var d=spliceItemForm_ForSelectedItem(data);
        var wastageData=spliceItemForm_ForSelectedWastageType(d);
        console.log('wastageData',wastageData)
        $scope.itemWiseForm.reportData=wastageData;
      });

    };

    function spliceItemForm_ForSelectedItem(result){
      if($scope.itemWiseForm.item!=undefined){
        _.forEachRight(result,function(r,i){
          if(r.itemId!=$scope.itemWiseForm.item._id){
            result.grandTotal=parseFloat(parseFloat(result.grandTotal)-parseFloat(r.Qty)).toFixed(3);
            result.splice(i,1);
          }
        });
      }
      return result;
    };

    function spliceItemForm_ForSelectedWastageType(result){
      if($scope.itemWiseForm.wastageEntryType!=undefined){
        _.forEachRight(result,function(r,i){
           _.forEachRight(r.bills,function(b,j){
          if(b.wastageEntryType!=wastageEntry){
            r.bills.splice(j,1);
          }
          });
        });      }
      return result;
    };

$scope.checkBills = function(bills)
{
  console.log('bills',bills);
if(bills == '')
  return true;
else
  return false;
};

    function formatDataItemWise(result){
      //console.log(angular.copy(result));
      var items=[];
      var gt=0;
      _.forEach(result,function(itm,i){
        var index=_.findIndex(items,{itemId:itm.itemId});
        if(itm.totalQty!=undefined) {
          //console.log('if');
          if (index < 0) {
            //console.log('1')
            var obj = {itemName: itm.itemName, itemId: itm.itemId, itemCode:itm.itemCode || '-', Qty: parseFloat(itm.totalQty).toFixed(3)};
            obj.bills = [];
            if(itm.returnWastage)
            {
              if($scope.resetSerialNumber)
                 itm.billNo = "RS-" + itm.billNo;
              else
                 itm.billNo = "RS-" + itm.transactionNumber;
              var wastageObject=angular.copy(itm);
              wastageObject.wastageEntryType='Wastage';
              wastageObject.totalQty = parseFloat(itm.returnWastageQuantity).toFixed(3);
              //console.log('w',wastageObject);
              obj.bills.push(wastageObject);
              var reuseObject=angular.copy(itm);
              reuseObject.wastageEntryType='Re-Use';
              reuseObject.totalQty = parseFloat(itm.returnAcceptQuantity).toFixed(3);
              //console.log('r',reuseObject);
              obj.bills.push(reuseObject);
            }
            else
            {
              if(itm.wastageEntryType == 'Wastage')
              {
                if($scope.resetSerialNumber)
                  itm.billNo = "WS-" + itm.billNo;  
                else
                  itm.billNo = "WS-" + itm.transactionNumber;  
              }
              else
              {
                if($scope.resetSerialNumber)
                  itm.billNo = "RU-" + itm.billNo;  
                else
                  itm.billNo = "RU-" + itm.transactionNumber;  
              }
              
              itm.totalQty = parseFloat(itm.totalQty).toFixed(3);
              obj.bills.push(itm);
            }
            var a={};
            //obj.bills.push(itm);
            items.push(obj);
            gt = parseFloat(parseFloat(itm.totalQty) + parseFloat(gt)).toFixed(3);
          }
          else {
            //console.log('2')
            //itm.totalQty = parseFloat(itm.totalQty).toFixed(3);
            var test=[];
            var returnStockWastage=0,returnStockReuse=0,wastage=0;
            if(itm.returnWastage)
            {
              if($scope.resetSerialNumber)
                itm.billNo = "RS-" + itm.billNo;
              else
                itm.billNo = "RS-" + itm.transactionNumber;
              var wastageObject=angular.copy(itm);
              wastageObject.wastageEntryType='Wastage';
              wastageObject.totalQty = parseFloat(itm.returnWastageQuantity).toFixed(3);
              returnStockWastage=parseFloat(wastageObject.totalQty).toFixed(3);
              //console.log('w',wastageObject);
              items[index].bills.push(wastageObject);
              test.push(wastageObject)
              var reuseObject=angular.copy(itm);
              reuseObject.wastageEntryType='Re-Use';
              reuseObject.totalQty = parseFloat(itm.returnAcceptQuantity).toFixed(3);
              returnStockReuse=parseFloat(reuseObject.totalQty).toFixed(3);
              //console.log('r',reuseObject);
              items[index].bills.push(reuseObject);
              test.push(reuseObject)
            //items[index].bills.push(itm);
            }
            else
            {
              if(itm.wastageEntryType == 'Wastage')
              {
                if($scope.resetSerialNumber)
                  itm.billNo = "WS-" + itm.billNo;   
                else
                  itm.billNo = "WS-" + itm.transactionNumber;   
              }
              else
              {
                if($scope.resetSerialNumber)
                  itm.billNo = "RU-" + itm.billNo;   
                else
                  itm.billNo = "RU-" + itm.transactionNumber   
              }
             
            items[index].bills.push(itm);
            test.push(itm);
            wastage=itm.totalQty;

            }
            //items[index].bills.push(itm);
            items[index].Qty = Number(parseFloat(parseFloat(getQty(test,wastageEntry)).toFixed(3)+parseFloat(items[index].Qty).toFixed(3)).toFixed(3));//parseFloat(parseFloat(totalQty)+parseFloat(items[index].Qty)).toFixed(3);
            gt = parseFloat(parseFloat(wastage) +parseFloat(returnStockWastage)+ parseFloat(returnStockReuse)+ parseFloat(gt)).toFixed(3);
          test=[];
          }
        }
        else if(itm.totalReturnWastageQty!=undefined) {
          //console.log('else if')
          if (index < 0) {
            var obj = {itemName: itm.itemName, itemId: itm.itemId,itemCode:itm.itemCode || '-', Qty: parseFloat(itm.totalQty).toFixed(3)};

            obj.bills = [];
            
              if($scope.resetSerialNumber)
                itm.billNo = "RS-" + itm.billNo;
              else
                itm.billNo = "RS-" + itm.transactionNumber;

              var wastageObject=angular.copy(itm);
              wastageObject.wastageEntryType='Wastage';
              wastageObject.totalQty = parseFloat(itm.returnWastageQuantity).toFixed(3);
              //console.log('w',wastageObject);
              obj.bills.push(wastageObject);
              var reuseObject=angular.copy(itm);
              reuseObject.wastageEntryType='Re-Use';
              reuseObject.totalQty = parseFloat(itm.returnAcceptQuantity).toFixed(3);
              //console.log('r',reuseObject);
              obj.bills.push(reuseObject);
            
            items.push(obj);
            gt = parseFloat(parseFloat(itm.totalReturnWastageQty) + parseFloat(gt)).toFixed(3);
          }
          else {

             console.log('else')
              if($scope.resetSerialNumber)
                itm.billNo = "RS-" + itm.billNo;
              else
                itm.billNo = "RS-" + itm.transactionNumber;

              var wastageObject=angular.copy(itm);
              wastageObject.wastageEntryType='Wastage';
              wastageObject.totalQty = parseFloat(itm.returnWastageQuantity).toFixed(3);
              //console.log('w',wastageObject);
              items[index].bills.push(wastageObject);
              //obj.bills.push(wastageObject);-
              var reuseObject=angular.copy(itm);
              reuseObject.wastageEntryType='Re-Use';
              reuseObject.totalQty = parseFloat(itm.returnAcceptQuantity).toFixed(3);
              //console.log('r',reuseObject);
              items[index].bills.push(reuseObject);
              //obj.bills.push(reuseObject);
            //items[index].bills.push(itm);
            items[index].Qty = parseFloat(parseFloat(itm.totalReturnWastageQty) + parseFloat(items[index].Qty)).toFixed(3);
            gt = parseFloat(parseFloat(itm.totalReturnWastageQty) + parseFloat(gt)).toFixed(3);
          }
        }
      });
      items.grandTotal=parseFloat(gt).toFixed(3);
      return items;
    }

    $scope.calculateSum = function(result)
    {
      var sum=0;
      //console.log('$scope.itemWiseForm.reportData',$scope.itemWiseForm.reportData)
      //console.log('calculateSum',result)
      _.forEach(result,function(r,i)
      {
        //console.log('r.totalQty',r.totalQty);
        sum+=Number(parseFloat(r.totalQty).toFixed(3));
      });
      //console.log('sum',sum);
            return parseFloat(sum).toFixed(3);
    }

    
    $scope.calculateGrandTotal = function(result)
    {
      var grandTotal=0;
      //console.log('$scope.itemWiseForm.reportData',$scope.itemWiseForm.reportData)
      //console.log('calculateGrandTotal',result)
      var itemWiseSum=0;
      _.forEach(result,function(r,i)
      {
       _.forEach(r.bills,function(b,i)
        {
        //console.log('b.totalQty',b.totalQty)
        itemWiseSum+=Number(parseFloat(b.totalQty).toFixed(3));
        });
      grandTotal += Number(parseFloat(itemWiseSum).toFixed(3));
      itemWiseSum=0;
      });
      return parseFloat(grandTotal).toFixed(3);
    }

function getQty(bills,wastageEntryType)
{
if(bills.length ==2)
{
_.forEach(bills,function(b,i)
{
  if(b.wastageEntryType == wastageEntryType)
  {
    console.log('bills.length',bills.length)
    console.log('b.totalQty',b.totalQty)
    return b.totalQty;
  }
})
}
else
{
  console.log('bills.length',bills.length)
  console.log('b.totalQty',bills[0].totalQty)
  return bills[0].totalQty;
}
}
    function convertItemInPreferredUnit(result){
      console.log("wastage");
      console.log(result)
      if($scope.itemWiseForm.entryType==1){
        _.forEach(result,function(itm,i){
          console.log('itm',angular.copy(itm))
          var indeex=_.findIndex($scope.stockitems,{_id:itm.itemId});
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
            console.log('$scope.stockitems[indeex]',$scope.stockitems[indeex]);
            if(!itm.itemCode)
                  itm.itemCode = item.itemCode;
            itm.itemName=$scope.stockitems[indeex].itemName;
            if(item.preferedUnit!=undefined){
              _.forEach(item.units,function(u,i){
                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                {
                  var ind = _.findIndex($scope.stockUnits, {_id: u._id});
                  if(ind >= 0)
                  {
                  u.baseUnit = $scope.stockUnits[ind].baseUnit;
                  u.conversionFactor = $scope.stockUnits[ind].conversionFactor;
                  u.baseConversionFactor = $scope.stockUnits[ind].baseConversionFactor;
                  }  
                }
                if(u._id==item.preferedUnit){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        });
        return result;
      }
      else
      {
        _.forEach(result,function(itm,i){
          var indeex=_.findIndex($scope.itemWiseForm.availableItems,{_id:itm.itemId});
          if(indeex>=0){
            console.log('$scope.itemWiseForm.availableItems[indeex]',$scope.itemWiseForm.availableItems[indeex])
            var item=$scope.itemWiseForm.availableItems[indeex];
            itm.itemName=$scope.itemWiseForm.availableItems[indeex].itemName;
            var index=_.findIndex($scope.stockitems,{_id:itm._id});
            if(index>=0){
            var item=$scope.stockitems[indeex];
            if(!itm.itemCode)
                  itm.itemCode = item.itemCode;
              }
            if(item.selectedUnitId!=undefined){
              _.forEach(item.units,function(u,i){
                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                {
                  var ind = _.findIndex($scope.stockUnits, {_id: u._id});
                  if(ind >= 0)
                  {
                  u.baseUnit = $scope.stockUnits[ind].baseUnit;
                  u.conversionFactor = $scope.stockUnits[ind].conversionFactor;
                  u.baseConversionFactor = $scope.stockUnits[ind].baseConversionFactor;
                  }  
                }
                if(u._id==item.selectedUnitId._id){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        });
        return result;
      }
    };
    //-----------------------------------------Day Wise Report-------------------------------------------------//
    $scope.dayWiseForm={
      fromDate:new Date(),//getYesterDayDate(),
      toDate:new Date(),
      maxDate:new Date()
    };
    $scope.checkDateGreater_DayWise= function(date){
      var d=new Date(date);
      var dd=new Date($scope.dayWiseForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.dayWiseForm.fromDate=new Date();//getYesterDayDate()
      }
    };

    $scope.checkDateLesser_DayWise= function(date){
      var d=new Date(date);
      var dd=new Date($scope.dayWiseForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.dayWiseForm.toDate= new Date();
      }
    };

    $scope.startSearch_DayWise=function(){
      intercom.registerEvent('StockReports');
      var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dayWiseForm.fromDate))));
      console.log('fromDate',fromDate);
      var toDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dayWiseForm.toDate))));
      console.log('toDate',toDate);
      /*var fromDate=new Date($scope.dayWiseForm.fromDate);
      fromDate.setHours(0, 0, 0, 0);
      var toDate=new Date($scope.dayWiseForm.toDate);
      toDate.setHours(0, 0, 0, 0);*/
      var req={"fromDate":fromDate,"toDate":toDate,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};
      //console.log(req);
      StockReport.getWastageReport(req,function(result){
        console.log("Result",result);
        console.log('getWastageReport',result);
        result.sort(function(a,b){
          return new Date(a.created).getTime()-new Date(b.created).getTime();
        });
        result.reverse();
        var data=formatDataDayWise(result);
        console.log(data);
        data=convertItemInPreferredUnit_DayWise(data);
        $scope.dayWiseForm.reportData=data;
      });
    };

        function formatDataDayWise(result){

      console.log('formatDataDayWise',result);
      var items=[];
      var gt=0;

      _.forEach(result,function(itm,i){
        var d=new Date(itm.created);
        //d.setHours(0, 0, 0, 0);
        var index=_.findIndex(items,{date:d});
        if(index<0){
          if(itm.itemType =='RawMaterial')
            {
             //console.log('in 1',itm.totalQty)
              if(itm.wastageEntryType == 'Wastage')
                {
                itm.returnWastageQuantity = parseFloat(itm.totalQty).toFixed(3);
                itm.returnAcceptQuantity =0;
                }
                else
                {
                itm.returnWastageQuantity = 0;
                itm.returnAcceptQuantity =parseFloat(itm.totalQty).toFixed(3);
                }
            }
            else
              {
                console.log('itm1',itm)
                if(itm.wastageEntryType==undefined)
                {
                itm.returnWastageQuantity = parseFloat(itm.returnWastageQuantity).toFixed(3);
                itm.returnAcceptQuantity = parseFloat(itm.returnAcceptQuantity).toFixed(3);  
                }
             
             else
             {
              if(itm.wastageEntryType == 'Wastage')
                {
                itm.returnWastageQuantity = parseFloat(itm.totalQty).toFixed(3);
                itm.returnAcceptQuantity =0;
                }
                else
                {
                itm.returnWastageQuantity = 0;
                itm.returnAcceptQuantity =parseFloat(itm.totalQty).toFixed(3);
                }
              }
            }
           // console.log('herrrrrrr3eee')
          var mObj={date:d};
          mObj.Type=[];
          mObj.Type.push({itemType:itm.itemType});
          //mObj.itemType.push={itemType:itm.itemType};
          mObj.Type[0].items=[];
          mObj.Type[0].items.push(itm);
          items.push(mObj);
          gt+=itm.totalQty;
        }
        else
        {
          //console.log(items[index].Type);
          var indexItemType=_.findIndex(items[index].Type,{itemType:itm.itemType});
          if(indexItemType<0){
            var newIndex=items[index].Type.length;
            var obj={};
            obj.itemType=itm.itemType;
            obj.items=[];
            if(itm.itemType =='RawMaterial')
            {
              console.log('in 2',itm.totalQty)
          if(itm.wastageEntryType == 'Wastage')
              {
                itm.returnWastageQuantity = parseFloat(itm.totalQty).toFixed(3);
                itm.returnAcceptQuantity =0;
                obj.items.push(itm);
                items[index].Type.push(obj);

              }
          else
              {
          itm.returnWastageQuantity = 0;
          itm.returnAcceptQuantity =parseFloat(itm.totalty).toFixed(3);
          obj.items.push(itm);
          items[index].Type.push(obj);
              }
            }
      	 	else
          {
            console.log('itm2',itm)
                  if(itm.wastageEntryType==undefined)
                      {
                      itm.returnWastageQuantity = parseFloat(itm.returnWastageQuantity).toFixed(3);
                      itm.returnAcceptQuantity = parseFloat(itm.returnAcceptQuantity).toFixed(3);  
                      } 
                   else
                   {
                    if(itm.wastageEntryType == 'Wastage')
                      {
                      itm.returnWastageQuantity = parseFloat(itm.totalQty).toFixed(3);
                      itm.returnAcceptQuantity =0;
                      }
                      else
                      {
                      itm.returnWastageQuantity = 0;
                      itm.returnAcceptQuantity = parseFloat(itm.totalQty).toFixed(3);
                      }
                    }
                  obj.items.push(itm);
                  items[index].Type.push(obj);
          }
                  // items[index].Type[newIndex]=[];
            // items[index].Type[newIndex].push({itemType:itm.itemType});
            // items[index].Type[newIndex].items=[];
            // items[index].Type[newIndex].items.push(itm);
          }
          else
          {
            if(itm.itemType == 'RawMaterial')
            {
        console.log('in 3',itm.totalQty)
              if(itm.wastageEntryType == 'Wastage')
                 {
                itm.returnWastageQuantity = parseFloat(itm.totalQty).toFixed(3);
                itm.returnAcceptQuantity =0;
                items[index].Type[indexItemType].items.push(itm);
                 }
              else
              {
              itm.returnWastageQuantity = 0;
              itm.returnAcceptQuantity = parseFloat(itm.totalQty).toFixed(3);
              items[index].Type[indexItemType].items.push(itm);
              }
            }
              else
              {    
                     console.log('itm3',itm)
                          if(itm.wastageEntryType==undefined)
                              {
                              itm.returnWastageQuantity = parseFloat(itm.returnWastageQuantity).toFixed(3);
                              itm.returnAcceptQuantity = parseFloat(itm.returnAcceptQuantity).toFixed(3);  
                              } 
                           else
                           {
                            if(itm.wastageEntryType == 'Wastage')
                              {
                              itm.returnWastageQuantity = parseFloat(itm.totalQty).toFixed(3);
                              itm.returnAcceptQuantity =0;
                              }
                              else
                              {
                              itm.returnWastageQuantity = 0;
                              itm.returnAcceptQuantity = parseFloat(itm.totalQty).toFixed(3);
                              }
                            }           
                          items[index].Type[indexItemType].items.push(itm);
              }
          }
        }
      });
      items.grandTotal=gt;
      return items;
    };


    function convertItemInPreferredUnit_DayWise(result){
      console.log("wastage",result);
      _.forEach(result,function(r,ri){
        _.forEach(r.Type,function(t,ti){
          if(t.itemType=="MenuItem"){
            _.forEach(t.items,function(itm,itmi){
              var indeex=_.findIndex($scope.stockRecipes,{_id:itm.itemId});
              if(indeex>=0){
                var item=$scope.stockRecipes[indeex];
                if(!itm.itemCode)
                  itm.itemCode = item.itemCode;
                itm.itemName=$scope.stockRecipes[indeex].recipeName;
                if(item.selectedUnitId!=undefined){
                  _.forEach(item.units,function(u,i){
                    if(u._id==item.selectedUnitId._id){
                      var conversionFactor=1;
                      var pconFac=parseFloat(u.conversionFactor);
                      itm.UnitName=u.baseUnit.name;
                      if(u.baseUnit.id==2 || u.baseUnit.id==3){
                        conversionFactor=parseFloat(u.baseConversionFactor);
                        if(pconFac>conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(conversionFactor)).toFixed(3);
                        }
                        else if(pconFac<conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                      else
                      {
                        if(pconFac>conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(pconFac)).toFixed(3);
                        }
                        else if(pconFac<conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(pconFac)).toFixed(3);
                        }
                      }
                    }
                  });
                }
              }
            });
          }
          else
          {
            _.forEach(t.items,function(itm,itmi){
              var indeex=_.findIndex($scope.stockitems,{_id:itm.itemId});
              if(indeex>=0){
                var item=$scope.stockitems[indeex];
                if(!itm.itemCode)
                  itm.itemCode = item.itemCode;
                itm.itemName=$scope.stockitems[indeex].itemName;
                if(item.preferedUnit!=undefined){
                  _.forEach(item.units,function(u,i){
                    if(u._id==item.preferedUnit){
                      var conversionFactor=1;
                      var pconFac=parseFloat(u.conversionFactor);
                      itm.UnitName=u.baseUnit.name;
                      if(u.baseUnit.id==2 || u.baseUnit.id==3){
                        conversionFactor=parseFloat(u.baseConversionFactor);
                        if(pconFac>conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(conversionFactor)).toFixed(3);
                        }
                        else if(pconFac<conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                      else
                      {
                        if(pconFac>conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)/parseFloat(pconFac)).toFixed(3);
                        }
                        else if(pconFac<conversionFactor){
                          itm.totalQty=parseFloat(parseFloat(itm.totalQty)*parseFloat(pconFac)).toFixed(3);
                        }
                      }
                    }
                  });
                }
              }
            });
          }
        });
      });
      return result;
    };

    //------------------------------------------------Reports-----------------------------------------------
    $scope.exportToPdf_itemWise=function(){
      var table=document.getElementById('itemWiseReport').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    $scope.exportToExcel_itemWise=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('itemWiseReport').innerHTML));
    };
    $scope.exportToPdf_dayWise=function(){
      var table=document.getElementById('dayWiseReport').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    $scope.exportToExcel_dayWise=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('dayWiseReport').innerHTML));
    };
  }]);;
