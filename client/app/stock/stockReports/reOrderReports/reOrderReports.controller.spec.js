'use strict';

describe('Controller: ReOrderReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var ReOrderReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReOrderReportsCtrl = $controller('ReOrderReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
