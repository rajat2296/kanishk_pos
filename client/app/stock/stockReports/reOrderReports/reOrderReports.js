'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('reOrderReports', {
        url: '/reOrderReports',
        templateUrl: 'app/stock/stockReports/reOrderReports/reOrderReports.html',
        controller: 'ReOrderReportsCtrl'
      });
  });