'use strict';

angular.module('posistApp')
  .controller('ReOrderReportsCtrl',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','StockReport','localStorageService','stockItem','StockReportNew','deployment','Utils','stockUnit', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,StockReport,localStorageService,stockItem,StockReportNew,deployment,Utils,stockUnit) {
  	currentUser.deployment_id=localStorageService.get('deployment_id');
  	$scope.stores=[];
    $scope.user=currentUser;
    $scope.stockitems=[];
    $scope.stockUnits =[];
    $scope.purchaseConsumptionForm={
      fromDate:new Date(),      
  		toDate:new Date(),
  		maxDate:new Date()
  	};
    store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
			$scope.stores=result;
              $scope.purchaseConsumptionForm.stores=result;
              $scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0];
              $scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
        stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
          $scope.stockitems=result;
        });
        stockUnit.get({tenant_id: currentUser.tenant_id,deployment_id: currentUser.deployment_id}).$promise.then(function (result){
          $scope.stockUnits=result;
        });
    });

    $scope.bindCategory_purchaseConsumptionForm=function(store){
  		$scope.purchaseConsumptionForm.categories=[];
  		$scope.purchaseConsumptionForm.availableItems=[];
  		var str=angular.copy(store);
  		_.forEach(str.category, function(c,i){
  			$scope.purchaseConsumptionForm.categories.push(c);
  		});
  	};

  	$scope.bindItems_purchaseConsumptionForm= function(category){
  		var cat=angular.copy(category);
  		$scope.purchaseConsumptionForm.availableItems=[];
  		$scope.purchaseConsumptionForm.item=undefined;
  		if(cat!=null){
  			_.forEach(cat.item, function(c,i){
	  			$scope.purchaseConsumptionForm.availableItems.push(c);
	  		});	
  		}  
      $scope.purchaseConsumptionForm.availableItems.sort(compare);
  	};
 	function getItemForSearch(stores){
  		var item=[];
  		_.forEach(stores.category,function(c,i){
  			_.forEach(c.item, function(itm,ii){
  				var obj={id:itm._id,itemName:itm.itemName};
  				item.push(obj);
  			});
  		});	
  		return item;
  	};
  	function getCategoryForSearch(stores){
      var item=[];
      _.forEach(stores.category,function(c,i){
        var obj={_id:c._id};
        item.push(obj);
      }); 
      return item;
    };
 	$scope.startSearch= function(){
      $scope.purchaseConsumptionForm.isSearch=true;
  		          var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.fromDate))));
        console.log('fromDate',fromDate);
        //var toDate = new Date($scope.purchaseConsumptionForm.toDate);
        var toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.toDate))));
      /*var fromDate=new Date();
  		fromDate.setDate(fromDate.getDate()-1);
  		fromDate.setHours(0, 0, 0, 0);
  		var toDate=new Date();
  		toDate.setHours(0, 0, 0, 0);*/
  		var strId=$scope.purchaseConsumptionForm.store._id;
      var isKitchen=$scope.purchaseConsumptionForm.store.isKitchen;
      var catId=[];
  		if($scope.purchaseConsumptionForm.item!=undefined || $scope.purchaseConsumptionForm.item!=null){
  			var item=[{id:$scope.purchaseConsumptionForm.item._id,itemName:$scope.purchaseConsumptionForm.item.itemName}];
  		}
  		else if($scope.purchaseConsumptionForm.category!=undefined || $scope.purchaseConsumptionForm.category!=null){
  			var item=[];
  			_.forEach($scope.purchaseConsumptionForm.category.item, function(itm,i){
  				var obj={id:itm._id,itemName:itm.itemName};
  				item.push(obj);
  			});

        catId.push({_id:$scope.purchaseConsumptionForm.category._id});
  		}
  		else if($scope.purchaseConsumptionForm.store!=undefined){  			
  			var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
        catId=getCategoryForSearch(angular.copy($scope.purchaseConsumptionForm.store));
  		};

  		//var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
  		var req={"fromDate":fromDate,"toDate":toDate,"storeId":strId,"isKitchen":isKitchen,"category":catId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};
      //"items":JSON.stringify(item)
          StockReportNew.getConsumptionSummary(req, function (result){
            if(result.errorMessage!=undefined){
              $scope.purchaseConsumptionForm.reportData=result;
              $scope.purchaseConsumptionForm.isSearch=false;
            }
            else
            {
              console.log(result);
              var result=spliceAllItemsBeforePhysical(result);
              result=calculatePrice(result);
              console.log(result);
              result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
              result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
              result=formatDataForReport(result,$scope.purchaseConsumptionForm.fromDate,$scope.purchaseConsumptionForm.toDate);
              console.log(result);
              result=spliceForCategoryAndItem(result,item);
              var res=convertItemInPreferredUnit(result);
              res=spliceItemWhoseReoderComes(res);
              $scope.purchaseConsumptionForm.reportData=res;
              $scope.purchaseConsumptionForm.isSearch=false;
              console.log(res);
            }
          });
      };

      function spliceItemWhoseReoderComes(result){
        var data=[];
        _.forEach(result.items,function(r,i){
          if(r.reorderLevel!="0.000"){
            if(parseFloat(r.reorderLevel)>=parseFloat(r.closingQty)){
              data.push(r);
            }
          }
        });
        return data;
      };
      function spliceAllItemsBeforePhysical(result){
        var data=[];
        _.forEach(result.beforeDate,function(r,i){
          if(r.totalBalanceQty!=undefined){
            var d=new Date(r.created);
            d.setDate(d.getDate() + 1);
            d.setHours(0, 0, 0, 0);
            r.created=d;
            data.push(r);
          };
        });
        _.forEach(data,function(d,i){
          _.forEachRight(result.beforeDate,function(itm,ii){
            if(itm.itemId==d.itemId){
              var phyDate=new Date(d.created);
              var compareDate=new Date(itm.created);
              if(phyDate.getTime()>compareDate.getTime()){
                result.beforeDate.splice(ii,1);
              }
            }
          });
        });

        _.forEachRight(result.betweenDate,function(r,i){
          if(r.totalBalanceQty!=undefined){
            var d=new Date(r.created);
            var cdate=new Date($scope.purchaseConsumptionForm.toDate);
            d.setDate(d.getDate() + 1);
            d.setHours(0, 0, 0, 0);
            r.created=d;
          };
        });

        return result;
      };
      
      function calculatePrice(result){
        _.forEach(result.beforeDate,function(itm,i){
          var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
          //if(index>=0){
            if(itm.totalOpeningQty!=undefined){
              if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
                if(index>=0){
                  itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalOpeningAmt=0.00;
                }
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
                if(index>=0){
                  itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalBalanceAmt=0.00;
                }
              }
            }
            else if(itm.totalPurchaseQty!=undefined){
              if(itm.totalPurchaseAmount==undefined){
                itm.totalPurchaseAmount=0.00;
              }
            }
            else if(itm.totalSaleQty!=undefined){
              if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
                if(index>=0){
                  itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalSaleAmount=0.00;
                }
              }
            }
            else if(itm.totalWastageQty!=undefined){
              if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
                if(index>=0){
                  itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalWastageAmt=0.00;
                }
              }
            }
            else if(itm.totalTransferQty!=undefined){
              if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
                if(index>=0){
                  itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalTransferAmount=0.00;
                }
              }
            }
            else if(itm.totalTransferQty_toStore!=undefined){
              if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
                if(index>=0){
                  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalTransferAmount_toStore=0.00;
                }
              }
            }
            else if(itm.itemSaleQty!=undefined){
              if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
                if(index>=0){
                  itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.itemSaleAmt=0.00;
                }
              }
            }
          //}
        });
        _.forEach(result.betweenDate,function(itm,i){
          var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
          //if(index>=0){
            if(itm.totalOpeningQty!=undefined){
              if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
                if(index>=0){
                  itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalOpeningAmt=0.00;
                }
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
                if(index>=0){
                  itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalBalanceAmt=0.00;
                }
              }
            }
            else if(itm.totalPurchaseQty!=undefined){
              if(itm.totalPurchaseAmount==undefined){
                itm.totalPurchaseAmount=0.00;
              }
            }
            else if(itm.totalSaleQty!=undefined){
              if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
                if(index>=0){
                  itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalSaleAmount=0.00;
                }
              }
            }
            else if(itm.totalWastageQty!=undefined){
              if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
                if(index>=0){
                  itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalWastageAmt=0.00;
                }
              }
            }
            else if(itm.totalTransferQty!=undefined){
              if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
                if(index>=0){
                  itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalTransferAmount=0.00;
                }
              }
            }
            else if(itm.totalTransferQty_toStore!=undefined){
              if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
                if(index>=0){
                  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.totalTransferAmount_toStore=0.00;
                }
              }
            }
            else if(itm.itemSaleQty!=undefined){
              if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
                if(index>=0){
                  itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
                }
                else
                {
                  itm.itemSaleAmt=0.00;
                }
              }
            }
          //}
        });
        return result;
      };

      function formatDataForReport(result,startDate,endDate){
         result.items=[];
        _.forEach(result.beforeDate,function(itm,i){
            if(itm.totalOpeningQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
              }
            }
            else if(itm.totalPurchaseQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
              }
            }
            else if(itm.totalIntermediateQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
              }
            }
            else if(itm.totalSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
            }
            else if(itm.totalWastageQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
              }
            }
            else if(itm.totalTransferQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }
            else if(itm.totalTransferQty_toStore!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
              }
            }
            else if(itm.itemSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                itm.UnitName=itm.baseUnit;
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
              }
            }
            else if(itm.totalStockReturnQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                itm.openingAmt=parseFloat(-itm.totalStockReturnAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }
            }
            else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
            }

        });
        _.forEach(result.betweenDate,function(itm,i){
            if(itm.totalPurchaseQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
              }
            }
            else if(itm.totalBalanceQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(2);
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(2);
                itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
                itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
                result.items.push(itm);
              }
              else
              {
                // result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                // result.items[itmIndex].openingAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                var cDate=new Date(itm.created);
                cDate.setDate(cDate.getDate()-1);
                if(endDate.getTime()==cDate.getTime()){

                }
                else
                {
                  result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                }
              }
            }
            else if(itm.totalIntermediateQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIntermediateAmt)).toFixed(2);
              }
            }
            else if(itm.totalSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
            }
            else if(itm.totalWastageQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
              }
            }
            else if(itm.totalTransferQty_toStore!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
              }
            }
            else if(itm.totalTransferQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              }
            }
            else if(itm.itemSaleQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                itm.UnitName=itm.baseUnit;
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
              }
            }
            else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.consumptionQty==undefined){
                  itm.consumptionQty=parseFloat(0).toFixed(3);
                  itm.consumptionAmount=parseFloat(0).toFixed(3);
                }
                itm.purchaseQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.purchaseAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
            }
            else if(itm.totalStockReturnQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                if(itm.openingQty==undefined){
                  itm.openingQty=parseFloat(0).toFixed(3);
                  itm.closingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                  itm.openingAmt=parseFloat(0).toFixed(3);
                  itm.closingAmount=parseFloat(-itm.totalStockReturnAmount).toFixed(3);
                  itm.openingDate=new Date(startDate);
                  itm.closingDate=new Date(endDate);
                }
                if(itm.purchaseQty==undefined){
                  itm.purchaseQty=parseFloat(0).toFixed(3);
                  itm.purchaseAmount=parseFloat(0).toFixed(3);
                }
                itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {
                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);

                result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }
            }

        });
        return result;
      };
      
      function spliceForCategoryAndItem(result,items){
        _.forEachRight(result.items,function(itm,i){
          var index=_.findIndex(items,{id:itm.itemId});
          if(index<0){
            result.items.splice(i,1);
          }
          else
          {
            itm.itemName=items[index].itemName;
          }
        });
        return result;
      };


      function convertItemInPreferredUnit(result){
        _.forEach(result.items,function(itm,i){
          var indeex=_.findIndex($scope.stockitems,{_id:itm.itemId});
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
            if(!itm.itemCode)
              itm.itemCode = item.itemCode;
            //if(item.reorderLevel!=undefined && item.reorderUnit!=undefined){
            if(item.reorderLevel!=undefined ){
              itm.reorderLevel=parseFloat($scope.stockitems[indeex].reorderLevel).toFixed(3);
              _.forEach(item.units,function(u,i){
                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                {
                  var ind = _.findIndex($scope.stockUnits, {_id: u._id});
                  if(ind >= 0)
                  {
                  u.baseUnit = $scope.stockUnits[ind].baseUnit;
                  u.conversionFactor = $scope.stockUnits[ind].conversionFactor;
                  u.baseConversionFactor = $scope.stockUnits[ind].baseConversionFactor;
                  }  
                }
                if(u._id==item.reorderUnit){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  itm.reorderLevel=parseFloat($scope.stockitems[indeex].reorderLevel).toFixed(3);
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                        itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                        itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                        itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                        itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                      }
                      else if(pconFac<conversionFactor){
                        itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                        itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                        itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                        itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                        itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                        itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                        itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                        itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                      }
                      else if(pconFac<conversionFactor){
                        itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                        itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                        itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                        itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
            else
            {
            	itm.reorderLevel=parseFloat(0).toFixed(3);
            }
          }
        });
        return result;
      };

      $scope.exportToPdf_purchaseConsumption=function(){
       var table=document.getElementById('purchaseConsumptionReport').innerHTML;
       var printer = window.open('', '', 'width=600,height=600');
       printer.document.open("text/html");
       printer.document.write(table);
       printer.document.close();
       printer.focus();
       printer.print();
    };
    $scope.exportToExcel_purchaseConsumption=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('purchaseConsumptionReport').innerHTML));
    };
    
 }]);
