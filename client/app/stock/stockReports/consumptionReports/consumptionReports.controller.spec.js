'use strict';

describe('Controller: ConsumptionReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var ConsumptionReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ConsumptionReportsCtrl = $controller('ConsumptionReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
