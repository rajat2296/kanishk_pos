'use strict';

angular.module('posistApp')
  .controller('ConsumptionReportsCtrl',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','StockReport','localStorageService','stockItem','stockReportResetTime','baseKitchenItem','baseKitchenUnit', 'deployment', 'Utils', 'stockConsumptionReport','property','intercom','stockUnit', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,StockReport,localStorageService,stockItem,stockReportResetTime,baseKitchenItem,baseKitchenUnit, deployment, Utils, stockConsumptionReport, property, intercom, stockUnit) {

    currentUser.deployment_id=localStorageService.get('deployment_id');
    $scope.stores = [];
    $scope.user = currentUser;
    $scope.stockitems = [];
    $scope.lastPrice = [];
    $scope.recipeQty = [];
    $scope.stockUnits = [];
    var kitchenStores;
    var allowBaseKitchenItemWastage = false;
    $scope.stockSettings =[];
    var consumptionViaPhysical =false;
    var isYieldEnabled = false;

    //    function getStores_ByDeployment(){
    //      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    //    };
    //    var allPromise=$q.all([
    //        getStores_ByDeployment()
    // ]);
    //    allPromise.then(function (value){
    //        $scope.stores = value[0];
    //        $scope.purchaseConsumptionForm.stores=$scope.stores;
    //        //$scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0]._id;
    // });

    function getRawPricing() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeConsumptionReport({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /* return StockReport.RawMaterialPricing_Receipe({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    };

    function getRawPricingBasekitchen() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeBasekitchenConsumptionReport({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }


    function getStockSettings() {
      var deferred = $q.defer();
      property.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        console.log('settings result',result);
        /*var isPhysical =false;
        if(result == '')
        {
          isPhysical= false;
          isYieldEnabled = false;
        }
        else if(result[0].isPhysicalStock == undefined)
          isPhysical = false;
        else if(result[0].isYieldEnabled == undefined)
          isYieldEnabled = false;
        else
        {
          isPhysical=result[0].isPhysicalStock;
          isYield=result[0].isYieldEnabled;
        }
            
        deferred.resolve(isPhysical);*/
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    function getStockUnits()
    {
      var deferred = $q.defer();
      stockUnit.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(units) {
        console.log('units',units);
        deferred.resolve(units);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
      $scope.stores=result;
      $scope.purchaseConsumptionForm.stores=result;
      $scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0];
      $scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
      stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.stockitems=result;
         kitchenStores = _.filter($scope.stores, function (store) {
          return store.isKitchen == true;
           });
          if(kitchenStores.length == 1){
              allowBaseKitchenItemWastage = true;
          }
        baseKitchenItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (items){
          _.forEach(items,function(itm,i){
            $scope.stockitems.push(itm);
          });
          $scope.basekitchenitems=items;
          console.log('base',$scope.basekitchenitems);
        });
      });

      $q.all([
          getRawPricing(),
          getRawPricingBasekitchen(),
          getStockSettings(),
          getStockUnits()
        ]).then(function (value) {
            $scope.lastPrice = value[0];
            $scope.stockUnits = value[3];
            console.log('$scope.stockUnits',$scope.stockUnits);
            _.forEach(value[1], function (price) {
                $scope.lastPrice.push(price);
            });      
            console.log($scope.lastPrice);
            $scope.stockSettings =value[2];
            console.log('$scope.stockSettings',$scope.stockSettings[0]);
            if($scope.stockSettings == '')
            {
              consumptionViaPhysical= false;
              isYieldEnabled = false;
            }
            else if($scope.stockSettings[0].isPhysicalStock == undefined)
              consumptionViaPhysical = false;
            else if($scope.stockSettings[0].isYieldEnabled == undefined)
              isYieldEnabled = false;
            else
            {
              consumptionViaPhysical=$scope.stockSettings[0].isPhysicalStock;
              isYieldEnabled=$scope.stockSettings[0].isYieldEnabled;
            }
            //consumptionViaPhysical=value[2];
            //consumptionViaPhysical =$scope.stockSettings[0].isPhysicalStock;
            $scope.isPhysicalStock = true;
            if(consumptionViaPhysical)
            {
            growl.success('Consumption will be shown via physical',{ttl:3000})
            }
            else
            {
            growl.success('Consumption will not be shown via physical',{ttl:3000})
            }
            /*if(isYieldEnabled)
            {
            growl.success('Yield is enabled',{ttl:3000})
            }
            else
            {
            growl.success('Yield is not enabled',{ttl:3000})
            }*/
            console.log('isYieldEnabled',isYieldEnabled);
        }).catch(function (err) {
            console.log('Error fetching base kitchen items');
        });

      // stockReportResetTime.getLastPriceOfItem_RawMaterial({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
      //   $scope.lastPrice=result;
      // });
      stockReportResetTime.getRecipeQtyInBase({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.recipeQty=result;
        //console.log($scope.recipeQty);
      });
    });

    function getYesterDayDate() {
      var d=new Date();
      d.setDate(d.getDate()-1);
      return d;
    };

    function parseDate(str){
      var d=$filter('date')(str, 'dd-MMM-yyyy');
      return d;
    };

    $scope.purchaseConsumptionForm={
      //fromDate:getYesterDayDate(),
      fromDate:new Date(),
      toDate:new Date(),
      maxDate:new Date()
    };

    //$scope.purchaseConsumptionForm.stores=angular.copy($scope.stores);
    $scope.checkDateGreater= function(date){
      var d=new Date(date);
      var dd=new Date($scope.purchaseConsumptionForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.purchaseConsumptionForm.fromDate= new Date();//getYesterDayDate();
      }
    };

    $scope.checkDateLesser= function(date){
      var d=new Date(date);
      var dd=new Date($scope.purchaseConsumptionForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.purchaseConsumptionForm.toDate= new Date();
      }
    };

    $scope.clearPurchaseConsumptionTab=function(){
      $scope.purchaseConsumptionForm={
        fromDate:new Date(),//getYesterDayDate(),
        toDate:new Date(),
        maxDate:new Date(),
        stores:angular.copy($scope.stores)
      };
      $scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0];
     // $scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
    };

    function compare(a,b) {
      if (a.itemName < b.itemName)
        return -1;
      if (a.itemName > b.itemName)
        return 1;
      return 0;
    };
   
    $scope.bindCategory_purchaseConsumptionForm=function(store){
      //console.log("store",store);
      //console.log("base k",$scope.basekitchenitems)
      $scope.purchaseConsumptionForm.categories=[];
      $scope.purchaseConsumptionForm.availableItems=[];
      $scope.purchaseConsumptionForm.item=undefined;
      var str=angular.copy(store);
      if(allowBaseKitchenItemWastage && store.isKitchen){
        var bItems = [];
         var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
      // console.log(baseKitchenItemsCat);
       
         _.forEach($scope.basekitchenitems, function (bItem) {
              var item = {
              itemName: bItem.itemName,
              _id: bItem.itemId,
              itemId: bItem.itemId,
              itemType: "Base Kitchen Item",
              // fromCategory: angular.copy(baseKitchenItemsCat),
              units: bItem.units,
              preferedUnit: bItem.preferedUnit
            }
            bItems.push(item);

        }); // End of For Each
         console.log( baseKitchenItemsCat.item );
        baseKitchenItemsCat.item = bItems;
        console.log(baseKitchenItemsCat)
        console.log('str',store);
        $scope.purchaseConsumptionForm.categories.push(baseKitchenItemsCat)
        }
          _.forEach(str.category, function(c,i){
            $scope.purchaseConsumptionForm.categories.push(c);
          });
       
        console.log($scope.purchaseConsumptionForm);
            
    };

    $scope.bindItems_purchaseConsumptionForm= function(category){
      
      var cat=angular.copy(category);
      console.log(cat);
      $scope.purchaseConsumptionForm.availableItems=[];
      $scope.purchaseConsumptionForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.purchaseConsumptionForm.availableItems.push(c);
        });
      }
      $scope.purchaseConsumptionForm.availableItems.sort(compare);
    };

    function getItemForSearch(stores){
      var item=[];
      _.forEach(stores.category,function(c,i){
        _.forEach(c.item, function(itm,ii){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
      });
      console.log('$scope.basekitchenitems',$scope.basekitchenitems);
      _.forEach($scope.basekitchenitems,function(itm){
        var obj={id:itm.itemId,itemName:itm.itemName};
        item.push(obj);
      });
      return item;
    }; 

    function getCategoryForSearch(stores){
      var item=[];
      _.forEach(stores.category,function(c,i){
        var obj={_id:c._id};
        item.push(obj);
      });
      return item;
    };

    $scope.openEmailModal = function (report) {
      var modalInstance = $modal.open({
      component: 'modalComponent',
      templateUrl: 'emailModal.html',
      size: 'sm',
      controller: 'EmailModalCtrl'
    });

    modalInstance.result.then(function (email) {
      if(report == 1)
        $scope.startSearch(email);
      if(report == 2)
        $scope.startSearch_stockSummary(email);
      if(report == 3)
        $scope.startSearch_ItemConsumption(email);
    }, function () {
      console.log('modal-component dismissed at: ' + new Date());
    });
    }

    $scope.startSearch = function(email) {
      intercom.registerEvent('StockReports');
      $scope.purchaseConsumptionForm.isSearch=true;
      //console.log(deployment.settings);
      console.log('$scope.purchaseConsumptionForm',$scope.purchaseConsumptionForm);
      var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.fromDate))));
      var toDate = new Date($scope.purchaseConsumptionForm.toDate);
      var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.toDate))));
      toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
      //alert(moment(toDateReset).subtract(1, 'months').toDate() < new Date(fromDate));
      var chkStart = moment(fromDate);
      var chkEnd = moment(toDate);
      var duration = moment.duration(chkEnd.diff(chkStart));
      if(duration.asMonths() > 1){
        growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
        $scope.purchaseConsumptionForm.isSearch = false;
        return;
      }
      //console.log(fromDate);
      //console.log(toDateReset);
      var strId=$scope.purchaseConsumptionForm.store._id;
      var isKitchen=$scope.purchaseConsumptionForm.store.isKitchen;
      var catId = [];
      if($scope.purchaseConsumptionForm.item!=undefined || $scope.purchaseConsumptionForm.item!=null){
        var item=[{id:$scope.purchaseConsumptionForm.item._id,itemName:$scope.purchaseConsumptionForm.item.itemName}];
      }
      else if($scope.purchaseConsumptionForm.category!=undefined || $scope.purchaseConsumptionForm.category!=null){
        var item=[];
        _.forEach($scope.purchaseConsumptionForm.category.item, function(itm,i){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });

        catId.push({_id:$scope.purchaseConsumptionForm.category._id});
      }
      else if($scope.purchaseConsumptionForm.store!=undefined){
        var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
        catId=getCategoryForSearch(angular.copy($scope.purchaseConsumptionForm.store));
      };

      //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
      var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"category":catId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, itemId: item._id, email: email, report: 1};
      console.log()
      //items:JSON.stringify(item)
      //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
      if(!consumptionViaPhysical)
      {
      stockReportResetTime.getConsumptionSummary(req, function (result){
        if(result.errorMessage!=undefined){
          $scope.purchaseConsumptionForm.reportData=result;
          $scope.purchaseConsumptionForm.isSearch=false;
        }
        else
        {
          console.log('result', angular.copy(result));
          //spliceandmovePhysical(result,$scope.purchaseConsumptionForm.fromDate)
          result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
          var result=spliceAllItemsBeforePhysical(result);
          console.log('splice', result);
          result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
          result=calculatePrice(angular.copy(result));
          console.log('price', angular.copy(result));
          //result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
          //result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
          result=formatDataForReport(result,fromDate,toDateReset);
          console.log('format', angular.copy(result));
          result=spliceForCategoryAndItem(result,item);
          console.log('spliceForCategoryAndItem',result);
          var res=convertItemInPreferredUnit(result);
         // console.log('convertItemInPreferredUnit',JSON.stringify(res));
          $scope.purchaseConsumptionForm.reportData=res;
          var html= consumptionReport(res.items);
          document.getElementById('consumptionReport').innerHTML = html;
          $scope.purchaseConsumptionForm.isSearch=false;
          console.log(result);
          //console.log(result);
          // $scope.purchaseConsumptionForm.reportData=result;
          // $scope.purchaseConsumptionForm.isSearch=false;
        }
      });
    }
    else
    {
      console.log('Inside getConsumptionViaPhysical');
     stockReportResetTime.getConsumptionViaPhysical(req, function (result){
        if(result.errorMessage!=undefined){
          $scope.purchaseConsumptionForm.reportData=result;
          $scope.purchaseConsumptionForm.isSearch=false;
        }
        else
        {
          console.log('result', angular.copy(result));
          //spliceandmovePhysical(result,$scope.purchaseConsumptionForm.fromDate)
          result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
          /*result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
          var result=spliceAllItemsBeforePhysical(result);
          console.log('splice', result);
          result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
          */result=calculatePrice(angular.copy(result));
          console.log('price', angular.copy(result));
          //result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
          //result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
          result=formatDataForReportViaPhysical(result,fromDate,toDateReset);
          console.log('format', angular.copy(result));
          result=spliceForCategoryAndItem(result,item);
          console.log('spliceForCategoryAndItem',result);
          var res=convertItemInPreferredUnit(result);
         // console.log('convertItemInPreferredUnit',JSON.stringify(res));
          $scope.purchaseConsumptionForm.reportData=res;
          var html= consumptionReport(res.items);
          document.getElementById('consumptionReport').innerHTML = html;
          $scope.purchaseConsumptionForm.isSearch=false;
          console.log(result);
          //console.log(result);
          // $scope.purchaseConsumptionForm.reportData=result;
          // $scope.purchaseConsumptionForm.isSearch=false;
        }
      }); 
    }
    };

    function spliceandmovePhysical(result,toDate){
      _.forEachRight(result.betweenDate,function(b,i){
        if(b.totalBalanceQty!=undefined){
          var d=new Date(toDate);
          //d.setHours(0,0,0,0);

          var cDate=new Date(b.created);
          if(d.getTime()> cDate.getTime()){
            result.beforeDate.push(b);
            result.betweenDate.splice(i,1);
          }
        }
      });
    };





    function spliceAllItemsBeforePhysical(result){
      var data=[];


var latestPhysical = null;
          var latestPhysicalBetween = null;
          var i = 0;
          var j = 0;
          _.forEach(result.beforeDate, function(r,i){
            if(r.totalBalanceQty!=undefined){
              var d = new Date(r.created);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              //d.setHours(0, 0, 0, 0);
              r.created = new Date(d);
              if(!latestPhysical){
                i = 0;
                latestPhysical = new Date(d);
              } else {
                if(latestPhysical.getTime() == d.getTime()){
                  i++;
                } else if(d.getTime() > latestPhysical.getTime()) {
                  i = 0;
                  latestPhysical = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = i;
              data.push(r);
            };
          });
          _.forEach(data,function(d,i){
            _.forEachRight(result.beforeDate,function(itm,ii){
              if(itm.itemId==d.itemId){
                var phyDate=new Date(d.created);
                var compareDate=new Date(itm.created);
                if(phyDate.getTime()>compareDate.getTime()){
                  result.beforeDate.splice(ii,1);
                }
              }
            });
          });

          // var data2=[];
          _.forEachRight(result.betweenDate,function(r,i){
            if(r.totalBalanceQty!=undefined){
              //console.log(r.created);
              r.actualDate = r.created;
              var d=new Date(r.created);
              //var cdate=new Date($scope.purchaseConsumptionForm.toDate);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              //console.log(a)
              //console.log(b)
              //console.log(angular.copy(d))
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              //d.setDate(d.getDate() + 1);
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              r.created = new Date(d);
              if(!latestPhysicalBetween){
                j = 0;
                latestPhysicalBetween = new Date(d);
              } else {
                if(latestPhysicalBetween.getTime() == d.getTime()){
                  j++;
                } else if(d.getTime() > latestPhysicalBetween.getTime()) {
                  j = 0;
                  latestPhysicalBetween = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = j;
              //console.log(r.created);
              // if(cdate.getTime()>d.getTime()){
              //   data2.push(r);
              // }
              // else
              // {
              //   result.betweenDate.splice(i,1);
              // }
            };
          });
          // _.forEach(data2,function(d,i){
          //   _.forEachRight(result.betweenDate,function(itm,ii){
          //     if(itm.itemId==d.itemId){
          //       if(itm.totalBalanceQty!=undefined){
          //           var phyDate=new Date(d.created);
          //           var compareDate=new Date(itm.created);
          //           if(phyDate.getTime()>compareDate.getTime()){
          //             result.betweenDate.splice(ii,1);
          //           }
          //         }
          //     }
          //   });
          //});
          //console.log(angular.copy(self._data));
          return result;













/*      _.forEach(result.beforeDate, function(r,i){
        if(r.totalBalanceQty!=undefined){
          var d = new Date(r.created);
          var a = new Date(d);
          a.setHours(0,0,0,0);
          a = new Date(a);
          var b = new Date(d);
          b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
          if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

          }else {
            //console.log('splice all', d);
            d.setDate(d.getDate() + 1);
          }
          d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
          //d.setHours(0, 0, 0, 0);
          r.created=d;
          data.push(r);
        };
      });
      _.forEach(data,function(d,i){
        _.forEachRight(result.beforeDate,function(itm,ii){
          if(itm.itemId==d.itemId){
            var phyDate=new Date(d.created);
            var compareDate=new Date(itm.created);
            if(phyDate.getTime()>compareDate.getTime()){
              result.beforeDate.splice(ii,1);
            }
          }
        });
      });

      // var data2=[];
      _.forEachRight(result.betweenDate,function(r,i){
        if(r.totalBalanceQty!=undefined){
          console.log(r.created);
          var d=new Date(r.created);
          var cdate=new Date($scope.purchaseConsumptionForm.toDate);
          var a = new Date(d);
          a.setHours(0,0,0,0);
          a = new Date(a);
          var b = new Date(d);
          b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
          console.log(a)
          console.log(b)
          console.log(angular.copy(d))
          if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

          }else {
            console.log('splice all', d);
            d.setDate(d.getDate() + 1);
          }
          //d.setDate(d.getDate() + 1);
          d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
          r.created=d;
          //console.log(r.created);
          // if(cdate.getTime()>d.getTime()){
          //   data2.push(r);
          // }
          // else
          // {
          //   result.betweenDate.splice(i,1);
          // }
        };
      });
      // _.forEach(data2,function(d,i){
      //   _.forEachRight(result.betweenDate,function(itm,ii){
      //     if(itm.itemId==d.itemId){
      //       if(itm.totalBalanceQty!=undefined){
      //           var phyDate=new Date(d.created);
      //           var compareDate=new Date(itm.created);
      //           if(phyDate.getTime()>compareDate.getTime()){
      //             result.betweenDate.splice(ii,1);
      //           }
      //         }
      //     }
      //   });
      //});

      return result;
*/    };

function consumptionReport(data){
  data.sort(function(a,b) {return (a.itemName > b.itemName) ? 1 : ((b.itemName > a.itemName) ? -1 : 0);} ); 

  /*data.sort(function(a,b)
  {
    return a.itemName >b.itemName;

  })*/
var html = '<table class="table  table-curved" border="1" style="width:100%">';
    html+= '<thead>';
                      html+='<th colspan="2" style="background-color:#24242D;font-weight:bold;color:#fff">Item</th>';
                      html+='<th colspan="4" style="background-color:#24242D;font-weight:bold;color:#fff">Opening Balance</th>';
                      html+='<th colspan="3" style="background-color:#24242D;font-weight:bold;color:#fff">Purchase</th>';
                      html+='<th colspan="3" style="background-color:#24242D;font-weight:bold;color:#fff">Normal Consumption</th>';
                      //html+='<th colspan="3" ng-hide="true" style="background-color:#24242D;font-weight:bold;color:#fff">NC Consumption</th>';
                      html+='<th colspan="4" style="background-color:#24242D;font-weight:bold;color:#fff">Closing Balance</th>';
                        html+='</thead>';
                        html+='<tbody>';
                        html+='<tr>';
                         html+='<td style="background-color: #abcabc;font-weight: bold;">Item Code</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Name</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        //html+='<td ng-hide="true" style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        //html+='<td ng-hide="true" style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        //html+='<td ng-hide="true" style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';
                      html+='</tr>';
                      for(var i in data)
                      {
                        html+='<tr>';
                        if(!data[i].itemCode)
                          html+='<td>-</td>';
                        else
                          html+='<td>'+data[i].itemCode+'</td>';
                        html+='<td>'+data[i].itemName+'</td>';
                        
                        html+='<td>'+moment(new Date(data[i].openingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+data[i].openingQty+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+data[i].openingAmt+'</td>';

                        //<!-- <td>{{r.totalPurchaseQty+r.totalTransferQty}}</td> -->
                        html+='<td>'+data[i].purchaseQty+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+data[i].purchaseAmount+'</td>';
                        //<!-- <td>{{r.totalPurchaseAmount+r.totalTransferAmount}}</td> -->

                        //<!-- <td>{{r.totalSaleQty+r.totalWastageQty}}</td> -->
                        html+='<td>'+data[i].consumptionQty+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+data[i].consumptionAmount+'</td>';
                        //<!-- <td>{{r.totalSaleAmount+r.totalWastageAmt}}</td> -->

                        //html+='<td ng-hide="true">NA</td>';
                        //html+='<td ng-hide="true">NA</td>';
                        //html+='<td ng-hide="true">NA</td>';
                        html+='<td>'+moment(+new Date(data[i].closingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+data[i].closingQty+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+data[i].closingAmount+'</td>'
                        html+='</tr>';
                      }
                       html+='</tbody>';
                      html+='</table>';
      return html;
}

function stockSummary(data)
{
  data.sort(function(a,b) {return (a.itemName > b.itemName) ? 1 : ((b.itemName > a.itemName) ? -1 : 0);} ); 

  var html='<table class="table table-curved" style="width:100%" border="1">';
                    html+='<thead>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="2">Item</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="4">Opening Balance</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="4">Closing Balance</th>';
                    html+='</thead>';
                    html+='<tbody>';
                      html+='<tr>';
                      html+='<td style="background-color: #abcabc;font-weight: bold;">Item Code</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Item Name</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';
                      html+='</tr>';
                      for(var i in data)
                      {
                      //<tr ng-repeat="r in stockSummaryForm.reportData.items">
                       html+='<tr>';
                        if(!data[i].itemCode)
                          html+='<td>-</td>';
                        else
                          html+='<td>'+data[i].itemCode+'</td>';
                        html+='<td>'+data[i].itemName+'</td>';
                        html+='<td>'+moment(+new Date(data[i].openingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+data[i].openingQty+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+data[i].openingAmt+'</td>';
                        html+='<td>'+moment(+new Date(data[i].closingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+data[i].closingQty+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+data[i].closingAmount+'</td>';
                      html+='</tr>';
                    }
                    html+='</tbody>';
                  html+='</table>';
                  return html;
}

function itemWiseConsumption(data)
{
  data.sort(function(a,b) {return (a.itemName > b.itemName) ? 1 : ((b.itemName > a.itemName) ? -1 : 0);} ); 
  console.log('data2',data)
  var html = "";
  html +='<table class="table table-curved" style="width:100%" border="1">';
                    html+='<thead>';
                    html+='<th style="background-color:#24242D;font-weight:bold;color:#fff"></th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Products</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Consumption</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Value</th>';
                    html+='</thead>';
                    html+='<tbody>';
                    var s1=0,s2=0,s3=0,s4=0;
                    
                    for(var i in data)
                    {
                      //if(data[i].consumptionQty.toString() !== '0.000'){
                      var total=[];
                      html+='<tr>';
                        html+='<td colspan="5" style="background-color:#abcabc;"><b>'+data[i].itemName+'</b></td>';
                      html+='</tr>';
                      /*<!-- <tr ng-repeat="item in r.receipeDetails">
                        html+='<td>{{item.itemName}}</td>
                        html+='<td>{{item.qty}}</td>
                        html+='<td>{{item.UnitName}}</td>
                        html+='<td>{{item.amount}}</td>
                      </tr> -->*/
                      for(var j in data[i].items)
                      {
                      
                        //html+='<tr ng-hide="item.consumptionQty==='0.000'" ng-repeat="item in r.items">
                      //<!-- <tr ng-repeat="item in r.items"> -->
                        if(data[i].items[j].consumptionQty.toString() !== '0.000'){
                          html+='<tr>';
                          if(!data[i].items[j].itemCode)
                            html+='<td>-</td>';
                          else
                            html+='<td>'+data[i].items[j].itemCode+'</td>';
                            html+='<td>'+data[i].items[j].itemName+'</td>';
                            // s1+=data[i].items[j].itemName;
                            html+='<td>'+data[i].items[j].consumptionQty+'</td>';
                            //  s2+=data[i].items[j].consumptionQty;
                            html+='<td>'+data[i].items[j].UnitName+'</td>';
                            //  s3+=data[i].items[j].UnitName;
                            if(data[i].items[j].consumptionAmount>0)
                            {
                            html+='<td>'+data[i].items[j].consumptionAmount+'</td>';
                            }//s4+=data[i].items[j].consumptionAmount;
                            else
                            {
                            html+='<td>'+parseFloat(parseFloat(data[i].items[j].consumptionAmount)).toFixed(2)+'</td>';  
                            }                         
                          html+='</tr>';
                        }
                      }
                      total=$scope.calculateItemWiseTotal(data[i].items)
                      //<!--<tr ng-repeat-end></tr>-->
                      
                      html+='<tr>';
                      html+='<td style="background-color: #00a3ff"></td>';
                        html+='<td style="background-color: #00a3ff"><strong>Total</strong></td>';
                        html+='<td style="background-color: #00a3ff"><b>'+total.consumptionQty+'</b></td>';
                        html+='<td style="background-color: #00a3ff"><b>'+total.UnitName+'</b></td>';
                        if(total.consumptionAmount >0)
                        {
                        html+='<td style="background-color: #00a3ff"><b>'+total.consumptionAmount+'</b></td>';
                        }
                        else
                        {
                         html+='<td style="background-color: #00a3ff"><b>'+parseFloat(parseFloat(total.consumptionAmount)).toFixed(3)+'</b></td>'; 
                        }
                      html+='</tr>';
                      total=[];
                      //}
                  }
                  html+='</tbody>';
                  html+='</table>';
                  return html;
};




    function calculatePrice(result){
      //console.log(result);
      _.forEach(result.beforeDate,function(itm,i){
        //console.log(JSON.stringify(itm));
        var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
        //console.log(itm.itemName, itm.totalPurchaseQty, $scope.lastPrice[index].monthAverage, itm.ConversionFactor, itm.UnitName);
        //if(index>=0){
        if(itm.totalOpeningQty!=undefined){
          if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
            if(index>=0){
              itm.totalOpeningAmt=parseFloat(parseFloat(itm.totalOpeningQty)* parseFloat($scope.lastPrice[index].monthAverage)/*Utils.roundNumber(parseFloat($scope.lastPrice[index].monthAverage),0)*/).toFixed(2);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            }
            else
            {
              itm.totalOpeningAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              itm.totalOpeningAmt=parseFloat(parseFloat(itm.totalOpeningQty) *parseFloat($scope.lastPrice[index].monthAverage)/*Utils.roundNumber(parseFloat($scope.lastPrice[index].monthAverage),0)*/).toFixed(2);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            }
            else
            {
              itm.totalOpeningAmt=0.00;
            } 
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
            if(index>=0){
              itm.totalBalanceAmt=parseFloat(parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            else
            {
              itm.totalBalanceAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              itm.totalBalanceAmt=parseFloat(parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
            } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
            else
            {
              itm.totalBalanceAmt=0.00;
            } 
          }
        }
        else if(itm.totalPurchaseQty!=undefined){
          // if(itm.totalPurchaseAmount==undefined){
          //   itm.totalPurchaseAmount=0.00;
          // }
          if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
        
          if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseQty)*parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          }
          else{
            itm.totalPurchaseAmount=0.00;
          }
        }
        else
        {
         if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseQty)*parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          }
          else{
            itm.totalPurchaseAmount=0.00;
          } 
        }
        }
        else if(itm.totalSaleQty!=undefined){
        if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalSaleAmount=parseFloat(parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            }
          }
          else
          {
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalSaleAmount=parseFloat(parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            }
          }
        }
        else if(itm.totalWastageQty!=undefined){
          if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
            if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            
            }
            else
            {
              itm.totalWastageAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            
            }
            else
            {
              itm.totalWastageAmt=0.00;
            } 
          }
        }
        else if(itm.totalTransferQty!=undefined){
          if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
            if(index>=0){
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
            itm.totalTransferAmount=parseFloat(parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            
            }
            else
            {
              itm.totalTransferAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
            itm.totalTransferAmount=parseFloat(parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            
            }
            else
            {
              itm.totalTransferAmount=0.00;
            } 
          }
        }
        else if(itm.totalTransferQty_toStore!=undefined){
          if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
            if(index>=0){
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            
            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
            //  itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            
            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            } 
          }
        }
        else if(itm.itemSaleQty!=undefined){
          console.log('itemSaleQuantity',itm);
          if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
            if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);
              if(!isYieldEnabled)
                itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              else {
                  var itemIndex = _.findIndex($scope.stockitems, function(item) {
                  return itm._id == item._id;
                });
                if(itemIndex >= 0)
                {
                  if($scope.stockitems[itemIndex].yeild) {
                    var price = 0, yieldAdjustedPrice = 0;;
                    price=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
                    yieldAdjustedPrice = ((200 - $scope.stockitems[itemIndex].yeild)  * 0.01 * price);
                    itm.itemSaleAmt=parseFloat(yieldAdjustedPrice).toFixed(2);
                  }
                }
                else
                {
                 itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2); 
                }
              }
            }
            else
            {
              itm.itemSaleAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);
            
              if(!isYieldEnabled)
                itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              else {
                  var itemIndex = _.findIndex($scope.stockitems, function(item) {
                  return itm.itemId == item._id;
                });
                if(itemIndex >= 0)
                {
                  if($scope.stockitems[itemIndex].yeild) {
                    var price = 0,yieldAdjustedPrice = 0;;
                    price=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
                    yieldAdjustedPrice = ((200 - $scope.stockitems[itemIndex].yeild)  * 0.01 * price);
                    itm.itemSaleAmt=parseFloat(yieldAdjustedPrice).toFixed(2);
                  }
                }
                else
                {
                 itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2); 
                }
              }
            }
            else
            {
              itm.itemSaleAmt=0.00;
            } 
          }
        }
        else if(itm.totalIntermediateQty!=undefined){
          if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            } 
          }
        }
        else if(itm.totalStockReturnQty != undefined) {
          //console.log('price', itm.itemName, itm.itemId, index);
          if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
            if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              
              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              
              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            } 
          }
        }
        else if(itm.totalStockReceiveQty!=undefined){
          if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(parseFloat(itm.totalStockReceiveQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalStockReceiveAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(parseFloat(itm.totalStockReceiveQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalStockReceiveAmount=0.00;
            } 
          }
        }
        else if(itm.totalIndentingChallanQty!=undefined){
        if(itm.totalIndentingChallanAmt==undefined || itm.totalIndentingChallanAmt==0){
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              console.log(itm.totalIndentingChallanQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalIndentingChallanAmt=parseFloat(parseFloat(itm.totalIndentingChallanQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIndentingChallanAmt=0.00;
            }
          }
          else
          {
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              console.log(itm.totalIndentingChallanQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalIndentingChallanAmt=parseFloat(parseFloat(itm.totalIndentingChallanQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIndentingChallanAmt=0.00;
            }
          }
        }
        //}
      });
      _.forEach(result.betweenDate,function(itm,i){
        //console.log(JSON.stringify(itm));
        var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
    
        //if(index>=0){
        if(itm.totalOpeningQty!=undefined){
          if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
            if(index>=0){
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalOpeningAmt=parseFloat(parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalOpeningAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalOpeningAmt=parseFloat(parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalOpeningAmt=0.00;
            } 
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
            if(index>=0){
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalBalanceAmt=parseFloat(parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalBalanceAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalBalanceAmt=parseFloat(parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalBalanceAmt=0.00;
            } 
          }
        }
        else if(itm.totalPurchaseQty!=undefined){
         /* if(itm.totalPurchaseAmount==undefined){
            itm.totalPurchaseAmount=0.00;
          }*/
      if(itm.totalPurchaseAmount==undefined || itm.totalPurchaseAmount==0){
          if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseQty)*parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          }
          else{
            itm.totalPurchaseAmount=0.00;
          }
        }
        else
        {
         if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.totalPurchaseAmount=parseFloat(parseFloat(itm.totalPurchaseQty)*parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          }
          else{
            itm.totalPurchaseAmount=0.00;
          } 
        }
        }
        else if(itm.totalSaleQty!=undefined){
          if(itm.totalSaleAmount==undefined || itm.totalSaleAmount==0){
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
            console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalSaleAmount=parseFloat(parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
            console.log(itm.totalSaleQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalSaleAmount=parseFloat(parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalSaleAmount=0.00;
            } 
          }
        }
        else if(itm.totalWastageQty!=undefined){
          if(itm.totalWastageAmt==undefined || itm.totalWastageAmt==0){
            if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalWastageAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalWastageAmt=parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalWastageAmt=parseFloat(parseFloat(itm.totalWastageQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalWastageAmt=0.00;
            } 
          }
        }
        else if(itm.totalTransferQty!=undefined){
          if(itm.totalTransferAmount==undefined || itm.totalTransferAmount==0){
            if(index>=0){
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount=parseFloat(parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalTransferAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount=parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount=parseFloat(parseFloat(itm.totalTransferQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalTransferAmount=0.00;
            } 
          }
        }
        else if(itm.totalTransferQty_toStore!=undefined){
          if(itm.totalTransferAmount_toStore==undefined || itm.totalTransferAmount_toStore==0){
            if(index>=0){
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
           
            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalTransferAmount_toStore=parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalTransferAmount_toStore=parseFloat(parseFloat(itm.totalTransferQty_toStore)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
           
            }
            else
            {
              itm.totalTransferAmount_toStore=0.00;
            } 
          }
        }
        else if(itm.itemSaleQty!=undefined){
       //  console.log('price', itm.itemName, itm.itemId, index);
          if(itm.itemSaleAmt==undefined || itm.itemSaleAmt==0){
            if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);
            
              if(!isYieldEnabled)
                itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              else {
                  var itemIndex = _.findIndex($scope.stockitems, function(item) {
                  return itm.itemId == item._id;
                });
                if(itemIndex >= 0)
                {
                  if($scope.stockitems[itemIndex].yeild) {
                    var price = 0,yieldAdjustedPrice = 0;;
                    price=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
                    yieldAdjustedPrice = ((200 - $scope.stockitems[itemIndex].yeild)  * 0.01 * price);
                    itm.itemSaleAmt=parseFloat(yieldAdjustedPrice).toFixed(2);
                  }
                }
                else
                {
                 itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2); 
                }
              }
              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.itemSaleAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.itemSaleAmt=parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
              //console.log(itm.itemSaleQty,$scope.lastPrice[index].monthAverage);
            
              if(!isYieldEnabled)
                itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              else {
                  var itemIndex = _.findIndex($scope.stockItems, function(item) {
                  return itm._id == item._id;
                });
                if(itemIndex >= 0)
                {
                  if($scope.stockItems[itemIndex].yeild) {
                    var price = 0,yieldAdjustedPrice = 0;;
                    price=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
                    yieldAdjustedPrice = ((200 - $scope.stockItems[itemIndex].yeild)  * 0.01 * price);
                    itm.itemSaleAmt=parseFloat(yieldAdjustedPrice).toFixed(2);
                  }
                }
                else
                {
                 itm.itemSaleAmt=parseFloat(parseFloat(itm.itemSaleQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2); 
                }
              }
              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.itemSaleAmt=0.00;
            } 
          }
        }
        else if(itm.totalStockReturnQty != undefined) {
          //console.log('price', itm.itemName, itm.itemId, index);
          if(itm.totalStockReturnAmount==undefined || itm.totalStockReturnAmount==0){
            if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              
              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalStockReturnQty=parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReturnAmount=parseFloat(parseFloat(itm.totalStockReturnQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
              
              //console.log(itm, $scope.lastPrice[index]);
            }
            else
            {
              itm.totalStockReturnAmount=0.00;
            } 
          }
        }
        else if(itm.totalIntermediateQty!=undefined){
          if(itm.totalIntermediateAmt==undefined || itm.totalIntermediateAmt==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalIntermediateAmt=parseFloat(parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIntermediateAmt=0.00;
            } 
          }
        }
        else if(itm.totalStockReceiveQty!=undefined){
          if(itm.totalStockReceiveAmount==undefined || itm.totalStockReceiveAmount==0){
            if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(parseFloat(itm.totalStockReceiveQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalStockReceiveAmount=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalIntermediateAmt=parseFloat(itm.totalIntermediateQty)*parseFloat($scope.lastPrice[index].lastPrice);
              itm.totalStockReceiveAmount=parseFloat(parseFloat(itm.totalStockReceiveQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalStockReceiveAmount=0.00;
            } 
          }
        }
        else if(itm.totalIndentingChallanQty!=undefined){
          if(itm.totalIndentingChallanAmt==undefined || itm.totalIndentingChallanAmt==0){
            if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
            console.log(itm.totalIndentingChallanQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalIndentingChallanAmt=parseFloat(parseFloat(itm.totalIndentingChallanQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIndentingChallanAmt=0.00;
            }
          }
          else
          {
           if(index>=0){
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
              //itm.totalSaleAmount=parseFloat(itm.totalSaleQty)*parseFloat($scope.lastPrice[index].lastPrice);
            console.log(itm.totalIndentingChallanQty,$scope.lastPrice[index].monthAverage);
            
              itm.totalIndentingChallanAmt=parseFloat(parseFloat(itm.totalIndentingChallanQty)*parseFloat($scope.lastPrice[index].monthAverage)).toFixed(2);
            }
            else
            {
              itm.totalIndentingChallanAmt=0.00;
            } 
          }
        }
        //}
      });
      return result;
    };

    function formatDataForReport(result,startDate,endDate){
      console.log('result', result);
      result.items = [];
      var endDateToShow = new Date(endDate);
      var startDateToShow = new Date(startDate)
      endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
      _.forEach(result.beforeDate,function(itm,i){
        if(itm.totalOpeningQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(startDate.getTime() < new Date(itm.created).getTime())
              startDateToShow = new Date(itm.created);
            itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            //console.log('total balance', itm.totalBalanceQty);
            result.items[itmIndex].openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(itm.totalBalanceAmt).toFixed(2);
          }
        }
        else if(itm.totalPurchaseQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
          }
        }
        else if(itm.totalIntermediateQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
          }
        }
        else if(itm.totalSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalTransferQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
          }
        }
        else if(itm.totalTransferQty_toStore!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
          }
        }
        else if(itm.itemSaleQty!=undefined){
          console.log(itm.itemName, itm.itemSaleAmt);
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            itm.UnitName=itm.baseUnit;
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
          }
        }
        else if(itm.totalStockReturnQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalStockReturnAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        }
        else if(itm.totalStockReceiveQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
          }
        }
        else if(itm.totalIndentingChallanQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalIndentingChallanAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
          }
        }
      });
      _.forEach(result.betweenDate,function(itm,i){
        if(itm.totalPurchaseQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          //console.log('itm', itm);
          var cDate=new Date(itm.created);
          //alert(cDate)
          //cDate.setDate(cDate.getDate()-1);
          //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
          //var ed = new Date(endDate);
          //ed = new Date(ed.setDate(ed.getDate()-1));
          //console.log(cDate);
          //console.log(endDate);
          //console.log(ed);
          if(itmIndex<0){
            //itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            //itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            //itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            if(endDate.getTime()==cDate.getTime()){
              console.log('if', itm.itemName, endDate.getTime(), cDate.getTime());
              itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
              itm.closingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            }
            else{
              console.log('else', itm.itemName, endDate.getTime(), cDate.getTime());
              itm.closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              itm.physicalDate = new Date(itm.created);
              console.log(itm.closingQty);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            if(endDate.getTime()==cDate.getTime()){
              result.items[itmIndex].physicalQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].physicalAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
              result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
              result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);

              //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
            else {
              console.log(itm.itemName, " else");
              console.log(result.items[itmIndex].closingQty);
              //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              if(result.items[itmIndex].physicalDate) {
                console.log(result.items[itmIndex].physicalDate)
                console.log(cDate)
                if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                  console.log('if');
                  result.items[itmIndex].closingQty = Utils.roundNumber(parseFloat(itm.totalBalanceQty), 3);
                  console.log(result.items[itmIndex].closingQty);
                } else {
                  console.log('else');
                  result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                  result.items[itmIndex].physicalDate = cDate;
                }
              } else {
                result.items[itmIndex].closingQty = parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].physicalDate = cDate;
              }
              // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
              // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
              // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
          }
        }
        else if(itm.totalIntermediateQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIntermediateAmt)).toFixed(2);
          }
        }
        else if(itm.totalSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalTransferQty_toStore!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
          }
        }
        else if(itm.totalTransferQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
          }
        }
        else if(itm.itemSaleQty!=undefined){
          console.log(itm.itemName, itm.itemSaleAmt);
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
            itm.UnitName=itm.baseUnit;
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
          }
        }
        else if(itm.totalStockReceiveQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            //console.log(itmIndex);
            result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
          }
        }
        else if(itm.totalStockReturnQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalStockReturnAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
            result.items.push(itm); 3
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        }
        else if(itm.totalIndentingChallanQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalIndentingChallanQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalIndentingChallanAmt).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalIndentingChallanQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
          }
        }

      });
      return result;
    };

function formatDataForReportViaPhysical(result,startDate,endDate){
      console.log('result', result);
      result.items = [];
      var endDateToShow = new Date(endDate);
      var startDateToShow = new Date(startDate)
      endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
      _.forEach(result.beforeDate,function(itm,i){
        if(itm.totalOpeningQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            /*if(startDate.getTime() < new Date(itm.created).getTime())
              startDateToShow = new Date(itm.created);
            */itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            //console.log('total balance', itm.totalBalanceQty);
            result.items[itmIndex].openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);

            /*result.items[itmIndex].consumptionQty=parseFloat(parseFloat(itm.openingQty)+parseFloat(itm.purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(itm.openingAmt)+parseFloat(itm.purchaseAmount)-parseFloat(itm.totalBalanceAmt)).toFixed(2)
            */

            result.items[itmIndex].closingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(itm.totalBalanceAmt).toFixed(2);
          }
        }
        else if(itm.totalPurchaseQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
          }
        }
        /*else if(itm.totalIntermediateQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

    $scope.bindCategory_stockSummaryForm=function(store){
      console.log("summary");
      $scope.stockSummaryForm.categories=[];
      $scope.stockSummaryForm.availableItems=[];
      $scope.stockSummaryForm.item=undefined;
      console.log($scope.basekitchenitems);
        var bItems = [];
        var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
        console.log($scope.baseKitchenItems);
         _.forEach($scope.basekitchenitems, function (bItem) {
              var item = {
              itemName: bItem.itemName,
              _id: bItem.itemId,
              itemId: bItem.itemId,
              itemType: "Base Kitchen Item",
              // fromCategory: angular.copy(baseKitchenItemsCat),
              units: bItem.units,
              preferedUnit: bItem.preferedUnit
            }
            bItems.push(item);

        }); // End of For Each
        baseKitchenItemsCat.item = bItems;
        console.log(baseKitchenItemsCat)
        //console.log('str',store);
        var str=angular.copy(store);
        _.forEach(str.category, function(c,i){
             $scope.stockSummaryForm.categories.push(c);
          });
        $scope.stockSummaryForm.categories.push(baseKitchenItemsCat)
        console.log($scope.stockSummaryForm);
    
    };

    $scope.bindItems_stockSummaryForm= function(category){
      $scope.stockSummaryForm.availableItems=[];
      var cat=angular.copy(category);
      $scope.stockSummaryForm.availableItems=[];
      $scope.stockSummaryForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.stockSummaryForm.availableItems.push(c);
        });
      }
      $scope.stockSummaryForm.availableItems.sort(compare);
    };
            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalTransferQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
          }
        }*/
        else if(itm.totalTransferQty_toStore!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
          }
        }
        /*else if(itm.itemSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            itm.UnitName=itm.baseUnit;
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);
    };
    $scope.checkDateGreater_itemWiseConsumption= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseConsumptionForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.itemWiseConsumptionForm.fromDate=new Date();//getYesterDayDate()
      }
    };

    $scope.checkDateLesser_itemWiseConsumption= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseConsumptionForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.itemWiseConsumptionForm.toDate= new Date();
      }
    };

    $scope.bindCategory_itemWiseConsumption=function(store){
      $scope.itemWiseConsumptionForm.categories=[];
      $scope.itemWiseConsumptionForm.availableItems=[];
      $scope.itemWiseConsumptionForm.item=undefined;

       
        var bItems = [];
        var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
        console.log($scope.basekitchenitems);
         _.forEach($scope.basekitchenitems, function (bItem) {
              var item = {
              itemName: bItem.itemName,
              _id: bItem.itemId,
              itemId: bItem.itemId,
              itemType: "Base Kitchen Item",
              // fromCategory: angular.copy(baseKitchenItemsCat),
              units: bItem.units,
              preferedUnit: bItem.preferedUnit
            }
            bItems.push(item);

        }); // End of For Each
        baseKitchenItemsCat.item = bItems;
        console.log(baseKitchenItemsCat)
        //console.log('str',store);
        var str=angular.copy(store);
        _.forEach(str.category, function(c,i){
             $scope.itemWiseConsumptionForm.categories.push(c);
          });
        $scope.itemWiseConsumptionForm.categories.push(baseKitchenItemsCat)
        console.log($scope.itemWiseConsumptionForm);
    };

    $scope.bindItems_itemWiseConsumption= function(category){
      var cat=angular.copy(category);
      $scope.itemWiseConsumptionForm.availableItems=[];
      $scope.itemWiseConsumptionForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.itemWiseConsumptionForm.availableItems.push(c);
        });
      }
      $scope.itemWiseConsumptionForm.availableItems.sort(compare);
    };

    $scope.startSearch_ItemConsumption=function(email){

      var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseConsumptionForm.fromDate))));
      var toDate = new Date($scope.itemWiseConsumptionForm.toDate);
      var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseConsumptionForm.toDate))));
      toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
      console.log(fromDate);
      console.log(toDateReset);
      var chkStart = moment(fromDate);
      var chkEnd = moment(toDate);
      var duration = moment.duration(chkEnd.diff(chkStart));
      if(duration.asMonths() > 1){
        growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
        //$scope.purchaseConsumptionForm.isSearch=false;
        return;
      }
      var strId=$scope.itemWiseConsumptionForm.store._id;
      var isKitchen = $scope.itemWiseConsumptionForm.store.isKitchen;
      if($scope.itemWiseConsumptionForm.item!=undefined || $scope.itemWiseConsumptionForm.item!=null){
        var item=[{id:$scope.itemWiseConsumptionForm.item._id,itemName:$scope.itemWiseConsumptionForm.item.itemName}];
      }
      else if($scope.itemWiseConsumptionForm.category!=undefined || $scope.itemWiseConsumptionForm.category!=null){
        var item=[];
        _.forEach($scope.itemWiseConsumptionForm.category.item, function(itm,i){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
      }
      else if($scope.purchaseConsumptionForm.store!=undefined){
        var item=getItemForSearch(angular.copy($scope.itemWiseConsumptionForm.store));
      };
            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        }*/
        else if(itm.totalStockReceiveQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
          }
        }

      });
      _.forEach(result.betweenDate,function(itm,i){
        if(itm.totalPurchaseQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          /*console.log('index <0 item',itm);
          console.log('index <0 physical :- ',itm.itemName,itm.purchaseQty,itm.openingQty,itm.totalBalanceQty,itm.consumptionQty);
          */var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          var cDate=new Date(itm.created);
          if(itmIndex<0){
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(parseFloat(itm.openingQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
            itm.consumptionAmount=parseFloat(parseFloat(itm.openingAmt)-parseFloat(itm.totalBalanceAmt)).toFixed(2);
            
            itm.closingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            /*console.log('else item',itm);
            console.log('else physical :- ',itm.itemName,itm.purchaseQty,itm.openingQty,itm.totalBalanceQty,itm.consumptionQty);
           */result.items[itmIndex].closingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
           result.items[itmIndex].closingAmount = parseFloat(itm.totalBalanceAmt).toFixed(3);
                  
           result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(result.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
           result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(result.items[itmIndex].purchaseAmount)-parseFloat(itm.totalBalanceAmt)).toFixed(2)
          }
        }
        /*else if(itm.totalIntermediateQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIntermediateAmt)).toFixed(2);
          }
        }
        else if(itm.totalSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }*/
        else if(itm.totalTransferQty_toStore!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
           /* console.log('index < 0 item',itm);
            console.log('index < 0 transfer :- ',itm.itemName,itm.totalTransferQty_toStore);
           */
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            /* console.log('else item',itm);
            console.log('else transfer :- ',itm.itemName,itm.totalTransferQty_toStore);
            */result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
          }
        }
        /*else if(itm.totalTransferQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
          }
        }*/
        /*else if(itm.itemSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
            itm.UnitName=itm.baseUnit;
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
          }
        }*/
        else if(itm.totalStockReceiveQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            //console.log(itmIndex);
            result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
          }
        }
        /*else if(itm.totalStockReturnQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalStockReturnAmount).toFixed(3);
              itm.wastageQty = parseFloat(0).toFixed(3);
              itm.totalBalanceQty = parseFloat(0).toFixed(3);
              itm.wastageAmt = parseFloat(0).toFixed(2);
              itm.varianceQty = parseFloat(0).toFixed(2);
              itm.varianceInPercent = parseFloat(0).toFixed(2);
              itm.physicalQty = parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDateToShow);
              itm.closingDate=new Date(endDateToShow);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmount=parseFloat(0).toFixed(3);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        }*/

      });
      return result;
    };

    function spliceForCategoryAndItem(result,items, itemId){
      console.log('items',angular.copy(items));
       console.log('result',angular.copy(result));
      _.forEachRight(result.items,function(itm,i){
        var index=_.findIndex(items,{id:itm.itemId});
        if(index<0){
          result.items.splice(i,1);
        }
        else
        {
          itm.itemName=items[index].itemName;
        }
      });
      return result;
    };


    function convertItemInPreferredUnit(result){
      console.log('result cvi',result);
      _.forEach(result.items,function(itm,i){
        var indeex=_.findIndex($scope.stockitems,function(item) {
          return item._id == itm.itemId || item.itemId == itm.itemId;
        });
        if(indeex>=0){
          var item=$scope.stockitems[indeex];
          itm.itemCode = item.itemCode;
          //console.log('item', item);
          if(item.preferedUnit != undefined){
            console.log(item.itemName, item.preferedUnit);
            if(typeof item.preferedUnit == 'string'){
              _.forEach(item.units,function(u,i){
                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                {
                  var ind = _.findIndex($scope.stockUnits, {_id: u._id});
                  if(ind >= 0)
                  {
                  u.baseUnit = $scope.stockUnits[ind].baseUnit;
                  u.conversionFactor = $scope.stockUnits[ind].conversionFactor;
                  u.baseConversionFactor = $scope.stockUnits[ind].baseConversionFactor;
                  }  
                }
                if(u._id==item.preferedUnit){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            } else {
              //console.log(item.itemName);
              _.forEach(item.units,function(u,i){
                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                {
                  var ind = _.findIndex($scope.stockUnits, {_id: u._id});
                  if(ind >= 0)
                  {
                  u.baseUnit = $scope.stockUnits[ind].baseUnit;
                  u.conversionFactor = $scope.stockUnits[ind].conversionFactor;
                  u.baseConversionFactor = $scope.stockUnits[ind].baseConversionFactor;
                  }  
                }
                if(u._id==item.preferedUnit._id){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id == 2 || u.baseUnit.id == 3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        }
      });
      return result;
    };
    //-------------------------------------stockSummaryForm--------------------------------------------------//
    $scope.clearStockSummaryTab= function(){
      ResetStockSummary();
    };

    function ResetStockSummary(){
      $scope.stockSummaryForm={
        fromDate:new Date(),//getYesterDayDate(),
        toDate:new Date(),
        maxDate:new Date(),
        stores:angular.copy($scope.stores)
      };
      $scope.stockSummaryForm.store=$scope.stockSummaryForm.stores[0];
      $scope.bindCategory_stockSummaryForm($scope.stockSummaryForm.store);
    };
    $scope.checkDateGreater_stockSummary= function(date){
      var d=new Date(date);
      var dd=new Date($scope.stockSummaryForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.stockSummaryForm.fromDate=new Date();//getYesterDayDate()
      }
    };

    $scope.checkDateLesser_stockSummary= function(date){
      var d=new Date(date);
      var dd=new Date($scope.stockSummaryForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.stockSummaryForm.toDate= new Date();
      }
    };

    $scope.bindCategory_stockSummaryForm=function(store){
         console.log("summary");
      $scope.stockSummaryForm.categories=[];
      $scope.stockSummaryForm.availableItems=[];
      $scope.stockSummaryForm.item=undefined;
      console.log($scope.basekitchenitems);
      var str=angular.copy(store);
       if(allowBaseKitchenItemWastage && store.isKitchen){
        var bItems = [];
        var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
        console.log($scope.baseKitchenItems);
         _.forEach($scope.basekitchenitems, function (bItem) {
              var item = {
              itemName: bItem.itemName,
              _id: bItem.itemId,
              itemId: bItem.itemId,
              itemType: "Base Kitchen Item",
              // fromCategory: angular.copy(baseKitchenItemsCat),
              units: bItem.units,
              preferedUnit: bItem.preferedUnit
            }
            bItems.push(item);

        }); // End of For Each
        baseKitchenItemsCat.item = bItems;
        console.log(baseKitchenItemsCat)
        $scope.stockSummaryForm.categories.push(baseKitchenItemsCat)
        console.log($scope.stockSummaryForm);
        //console.log('str',store);
      }
        
        _.forEach(str.category, function(c,i){
             $scope.stockSummaryForm.categories.push(c);
          });
       
    };

    $scope.bindItems_stockSummaryForm= function(category){
      $scope.stockSummaryForm.availableItems=[];
      var cat=angular.copy(category);
      $scope.stockSummaryForm.availableItems=[];
      $scope.stockSummaryForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.stockSummaryForm.availableItems.push(c);
        });
      }
      $scope.stockSummaryForm.availableItems.sort(compare);
    };

    $scope.startSearch_stockSummary=function(email){
      intercom.registerEvent('StockReports');
      $scope.stockSummaryForm.isSearch=true;
      var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.stockSummaryForm.fromDate))));
      var toDate = new Date($scope.stockSummaryForm.toDate);
      var toDateReset = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.stockSummaryForm.toDate))));
      toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
      //console.log(toDateReset);
      //console.log(fromDate);
      //console.log(toDate);
      //console.log(toDateReset);
      var chkStart = moment(fromDate);
      var chkEnd = moment(toDate);
      var duration = moment.duration(chkEnd.diff(chkStart));
      if(duration.asMonths() > 1){
        growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
        //$scope.purchaseConsumptionForm.isSearch=false;
        return;
      }
      var strId=$scope.stockSummaryForm.store._id;
      var isKitchen=$scope.stockSummaryForm.store.isKitchen;
      if($scope.stockSummaryForm.item!=undefined || $scope.stockSummaryForm.item!=null){
        var item=[{id:$scope.stockSummaryForm.item._id,itemName:$scope.stockSummaryForm.item.itemName}];
      }
      else if($scope.stockSummaryForm.category!=undefined || $scope.stockSummaryForm.category!=null){
        var item=[];
        _.forEach($scope.stockSummaryForm.category.item, function(itm,i){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
      }
      else if($scope.stockSummaryForm.store!=undefined){
        var item=getItemForSearch(angular.copy($scope.stockSummaryForm.store));
      };

      //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
      var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, report: 2, email: email};
      //console.log(req);
      //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
      if(!consumptionViaPhysical)
      {
      stockReportResetTime.getConsumptionSummary(req, function (result){
        //console.log(result);
        if(result.errorMessage){
          $scope.stockSummaryForm.reportData = {};
          $scope.stockSummaryForm.reportData.errorMessage = result.errorMessage;
          return;
        }
        result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
        var result=spliceAllItemsBeforePhysical(result);
        result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
        result=calculatePrice(result);
        //console.log(result);
        /*result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        */result=formatDataForReport(result,fromDate,toDateReset);
        //console.log(result);
        result=spliceForCategoryAndItem(result,item);
        var res=convertItemInPreferredUnit(result);
        $scope.stockSummaryForm.reportData=res;
        var html=stockSummary(res.items);
        $scope.stockSummaryForm.isSearch=false;
        document.getElementById('stockSummary').innerHTML=html;
        console.log(result);
        // $scope.stockSummaryForm.reportData = {};
        // $scope.stockSummaryForm.reportData.successMessage = result.successMessage;
        return;
      });
      }
      else
      {
       stockReportResetTime.getConsumptionViaPhysical(req, function (result){
        //console.log(result);
        if(result.errorMessage){
          $scope.stockSummaryForm.reportData = {};
          $scope.stockSummaryForm.reportData.errorMessage = result.errorMessage;
          return;
        }
        result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
        /*result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
        var result=spliceAllItemsBeforePhysical(result);
        result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });*/
        result=calculatePrice(result);
        //console.log(result);
        /*result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        */result=formatDataForReportViaPhysical(result,fromDate,toDateReset);
        //console.log(result);
        result=spliceForCategoryAndItem(result,item);
        var res=convertItemInPreferredUnit(result);
        $scope.stockSummaryForm.reportData=res;
        var html=stockSummary(res.items);
        $scope.stockSummaryForm.isSearch=false;
        document.getElementById('stockSummary').innerHTML=html;
        console.log(result);
        // $scope.stockSummaryForm.reportData = {};
        // $scope.stockSummaryForm.reportData.successMessage = result.successMessage;
        return;
      }); 
      }
    };

    //-------------------------------------itemWiseConsumptionForm----------------------------------------------
    $scope.clearItemWiseTab= function(){
      ResetItemWiseConsumption();
    };

    function ResetItemWiseConsumption(){
      $scope.itemWiseConsumptionForm={
        fromDate:new Date(),//getYesterDayDate(),
        toDate:new Date(),
        maxDate:new Date(),
        stores:angular.copy($scope.stores)
      };
      $scope.itemWiseConsumptionForm.store=$scope.itemWiseConsumptionForm.stores[0];
      $scope.bindCategory_itemWiseConsumption($scope.itemWiseConsumptionForm.store);

    };
    $scope.checkDateGreater_itemWiseConsumption= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseConsumptionForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.itemWiseConsumptionForm.fromDate=new Date();//getYesterDayDate()
      }
    };

    $scope.checkDateLesser_itemWiseConsumption= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseConsumptionForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.itemWiseConsumptionForm.toDate= new Date();
      }
    };

    $scope.bindCategory_itemWiseConsumption=function(store){
         $scope.itemWiseConsumptionForm.categories=[];
      $scope.itemWiseConsumptionForm.availableItems=[];
      $scope.itemWiseConsumptionForm.item=undefined;
      var str=angular.copy(store);
      if(allowBaseKitchenItemWastage && store.isKitchen){
        var bItems = [];
        var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
        console.log($scope.basekitchenitems);
         _.forEach($scope.basekitchenitems, function (bItem) {
              var item = {
              itemName: bItem.itemName,
              _id: bItem.itemId,
              itemId: bItem.itemId,
              itemType: "Base Kitchen Item",
              // fromCategory: angular.copy(baseKitchenItemsCat),
              units: bItem.units,
              preferedUnit: bItem.preferedUnit
            }
            bItems.push(item);

        }); // End of For Each
        baseKitchenItemsCat.item = bItems;
        //console.log(baseKitchenItemsCat)
        $scope.itemWiseConsumptionForm.categories.push(baseKitchenItemsCat)
        console.log($scope.itemWiseConsumptionForm);
        //console.log('str',store);
      }//
      
        _.forEach(str.category, function(c,i){
             $scope.itemWiseConsumptionForm.categories.push(c);
          });
 
    };

    $scope.bindItems_itemWiseConsumption= function(category){
      var cat=angular.copy(category);
      $scope.itemWiseConsumptionForm.availableItems=[];
      $scope.itemWiseConsumptionForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.itemWiseConsumptionForm.availableItems.push(c);
        });
      }
      $scope.itemWiseConsumptionForm.availableItems.sort(compare);
    };

    $scope.startSearch_ItemConsumption=function(email){
      intercom.registerEvent('StockReports');
      var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseConsumptionForm.fromDate))));
      var toDate = new Date($scope.itemWiseConsumptionForm.toDate);
      var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseConsumptionForm.toDate))));
      toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
      console.log(fromDate);
      console.log(toDateReset);
      var chkStart = moment(fromDate);
      var chkEnd = moment(toDate);
      var duration = moment.duration(chkEnd.diff(chkStart));
      if(duration.asMonths() > 1){
        growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
        //$scope.purchaseConsumptionForm.isSearch=false;
        return;
      }
      var strId=$scope.itemWiseConsumptionForm.store._id;
      var isKitchen = $scope.itemWiseConsumptionForm.store.isKitchen;
      if($scope.itemWiseConsumptionForm.item!=undefined || $scope.itemWiseConsumptionForm.item!=null){
        var item=[{id:$scope.itemWiseConsumptionForm.item._id,itemName:$scope.itemWiseConsumptionForm.item.itemName}];
      }
      else if($scope.itemWiseConsumptionForm.category!=undefined || $scope.itemWiseConsumptionForm.category!=null){
        var item=[];
        _.forEach($scope.itemWiseConsumptionForm.category.item, function(itm,i){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
      }
      else if($scope.purchaseConsumptionForm.store!=undefined){
        var item=getItemForSearch(angular.copy($scope.itemWiseConsumptionForm.store));
      };

      //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
      //var req={"fromDate":fromDate,"toDate":toDate,"storeId":strId,"items":JSON.stringify(item),"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};

      var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, isKitchen: isKitchen, resetTime: true, email: email, report: 3};
      //console.log(req);
      //StockReport.getConsumptionSummary(req, function (result){
      // StockReport.getConsumptionSummary_ItemReport(req, function (result){
      //  $scope.itemWiseConsumptionForm.reportData=FormatItemData(result);
      //  console.log(result);
      // });
      //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
        if(!consumptionViaPhysical)
      {
      stockReportResetTime.getConsumptionSummary(req, function (result){
        //console.log(angular.copy(result));
        if(result.errorMessage){
          $scope.itemWiseConsumptionForm.reportData = {};
          $scope.itemWiseConsumptionForm.reportData.errorMessage = result.errorMessage;
          return;
        }
        result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
        var result=spliceAllItemsBeforePhysical(result);
        result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
        
        result=calculatePrice(result);
        console.log(angular.copy(result));
        var data=formatDataForItemReport(result,fromDate,toDateReset);
        console.log(angular.copy(data));
        var d=getItemdetails(data);
        console.log(angular.copy(d));
        d=convertItemInPreferredUnit_ItemReport_New(d);
        console.log(angular.copy(d));
        d=spliceForCategoryAndItem_ItemReport(d,item);
        console.log(angular.copy(d));
        // var rItems=getUniqueRawItems(result);
        // console.log(rItems);
        // var uItems=getUniqueItems(result);
        // console.log(uItems);
        // var calculatedItem=calculatePrice_ItemReport(result);
        // var inpfUnit=convertItemInPreferredUnit_ItemReport(calculatedItem);
        // var itemWithRecipeDetail=getItemsReceipeDetails(result,uItems);
        // console.log(inpfUnit);
        // console.log(calculatedItem);
        // console.log(result);
        // $scope.itemWiseConsumptionForm.reportData=itemWithRecipeDetail;
        $scope.itemWiseConsumptionForm.reportData=d;
        $scope.itemWiseConsumptionForm.isSearch=false;
        var html=itemWiseConsumption(d);
        console.log(document.getElementById('itemWiseConsumption').innerHTML);
        //console.log(itemWiseConsumption(d));
        document.getElementById('itemWiseConsumption').innerHTML=itemWiseConsumption(d);
        // $scope.itemWiseConsumptionForm.reportData = {};
        // $scope.itemWiseConsumptionForm.reportData.successMessage = result.successMessage;
        return;
      });
      }
      else
      {
        stockReportResetTime.getConsumptionViaPhysical(req, function (result){
        //console.log(angular.copy(result));
        if(result.errorMessage){
          $scope.itemWiseConsumptionForm.reportData = {};
          $scope.itemWiseConsumptionForm.reportData.errorMessage = result.errorMessage;
          return;
        }
        result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
        /*result.beforeDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
              result.betweenDate.sort(function (a, b) {
                return new Date(a.created) - new Date(b.created);
              });
        var result=spliceAllItemsBeforePhysical(result);
        result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
              });
              result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
              });
        */
        result=calculatePrice(result);
        console.log(angular.copy(result));
        var data=formatDataForReportViaPhysical(result,fromDate,toDateReset);
        console.log('format',angular.copy(data));
        var d=getItemdetailsViaPhysical(data);
        console.log('getItemdetailsViaPhysical',angular.copy(d));
        d=convertItemInPreferredUnit_ItemReport_New(d);
        console.log(angular.copy(d));
        d=spliceForCategoryAndItem_ItemReport(d,item);
        console.log(angular.copy(d));
        // var rItems=getUniqueRawItems(result);
        // console.log(rItems);
        // var uItems=getUniqueItems(result);
        // console.log(uItems);
        // var calculatedItem=calculatePrice_ItemReport(result);
        // var inpfUnit=convertItemInPreferredUnit_ItemReport(calculatedItem);
        // var itemWithRecipeDetail=getItemsReceipeDetails(result,uItems);
        // console.log(inpfUnit);
        // console.log(calculatedItem);
        // console.log(result);
        // $scope.itemWiseConsumptionForm.reportData=itemWithRecipeDetail;
        $scope.itemWiseConsumptionForm.reportData=d;
        $scope.itemWiseConsumptionForm.isSearch=false;
        var html=itemWiseConsumption(d);
        //console.log(document.getElementById('itemWiseConsumption').innerHTML);
        //console.log(itemWiseConsumption(d));
        document.getElementById('itemWiseConsumption').innerHTML=itemWiseConsumption(d);
        // $scope.itemWiseConsumptionForm.reportData = {};
        // $scope.itemWiseConsumptionForm.reportData.successMessage = result.successMessage;
        return;
      });
      }
    };

    function spliceForCategoryAndItem_ItemReport(d,items){
      console.log('items',items);
      console.log('d',d);
      _.forEachRight(d,function(itm,i){
        var index=_.findIndex(items,{id:itm.itemId});
        if(index<0){
          d.splice(i,1);
        }
        else
        {
          itm.itemName=items[index].itemName;
        }
      });
      return d;
    };


    function getItemdetails(data){
      var items=data.items;
      console.log(data.items);
      _.forEach(items,function(it,i){
        it.items=[];
        if(it.isBaseKitchenItem)
          it.UnitName = it.baseUnit;
        //it.items.push(angular.copy(it));
        _.forEach(data.betweenDate,function(itm,i){
          //console.log('---------------------------------------------------');
          //console.log(itm);
         // console.log('itm',itm);
          if(itm.isBaseKitchenItem)
            console.log("BASE");
          //alert(itm.itemName);
          if(itm.itemId==it.itemId && itm.menuId == undefined){
            if(itm.totalSaleQty!=undefined){
              //if(itm.menuId!=undefined){
                console.log('itm.itemName',itm.itemName);
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                {
                  console.log('base itm',itm)
                  obj.UnitName=itm.UnitName;
                }
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                it.items.push(obj);
              }
              else {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
              //}
            }
            else if(itm.totalWastageQty!=undefined){
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.UnitName;
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
              //}
            }
            else if(itm.totalTransferQty!=undefined){
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              //}
            }
            else if(itm.totalStockReturnQty!=undefined){
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.UnitName;
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              //}
            }
            /*else if(itm.totalStockReceiveQty!=undefined){
                 // console.log('itm in tsr',itm);
              //if(itm.menuId!=undefined){
            var itmIndex=_.findIndex(it.items,function (item){
              return item.itemId == itm.itemId;
            });
            if(itmIndex<0){
              var obj={};
              obj.itemId=itm.itemId;
              obj.itemName=itm.itemName;
              if(itm.isBaseKitchenItem)
                obj.UnitName=itm.UnitName;
              else obj.UnitName=itm.UnitName;
              obj.consumptionQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
              obj.consumptionAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
              it.items.push(obj);
            }
            else
            {
              it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
              it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
            }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
            //}
              }*/
              else if(itm.totalIntermediateQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                //}
              }
              else if(itm.itemSaleQty!=undefined){
                if(itm.recipeId!=undefined){
                  //alert(itm.recipeId);
                  //var itmIndex=_.findIndex(it.items,{itemId:itm.recipeId});
                  //if(itmIndex<0){
                  alert(itm.baseUnit);
                  var obj={};
                  obj.itemId=itm.itemId;
                  obj.itemName=itm.recipeName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.baseUnit;
                  obj.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                  it.items.push(obj);
                  //}
                  //else
                  //{
                  //  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  //  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);
                  //}
                  it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);

                  //it.items[0].consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                  //it.items[0].consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
                }
              }
              else if(itm.totalIndentingChallanQty!=undefined){
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.UnitName;
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(itm.totalIndentingChallanQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
              //}
            }
            } else if(itm.itemId==it.itemId && itm.menuId != undefined){
              if(itm.totalSaleQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
                }
                it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
                //}
              }
              else if(itm.totalWastageQty!=undefined){
                console.log('itmWastage',itm);
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

                }
                it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
                //}
              }
              else if(itm.totalTransferQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.baseUnit;
                  obj.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
                //}
              }
              else if(itm.totalStockReturnQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
                //}
              }
              else if(itm.totalIntermediateQty!=undefined){
               
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                //}
                }
              else if(itm.itemSaleQty!=undefined){
                if(itm.recipeId!=undefined){
                  //alert(itm.recipeId);
                  //var itmIndex=_.findIndex(it.items,{itemId:itm.recipeId});
                  //if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.recipeName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.baseUnit;
                  obj.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                  it.items.push(obj);
                  //}
                  //else
                  //{
                  //  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  //  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);
                  //}
                  it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);

                  //it.items[0].consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                  //it.items[0].consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
                }
              }
              else if(itm.totalIndentingChallanQty!=undefined){
                console.log('itmWastage',itm);
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalIndentingChallanQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);

                }
                it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalIndentingChallanQty)).toFixed(2);
                //}
              }
            }
          });
        });
        return items;

      };


function getItemdetailsViaPhysical(data){
      var items=data.items;
      //console.log('data.items :- ',JSON.stringify(data.items));
      _.forEach(items,function(it,i){
        it.items=[];
        if(it.isBaseKitchenItem)
          it.UnitName = it.baseUnit;
        //it.items.push(angular.copy(it));
        _.forEach(data.betweenDate,function(itm,i){
          //console.log('---------------------------------------------------');
          //console.log(itm);
         // console.log('itm',itm);
          if(itm.isBaseKitchenItem)
            console.log("BASE");
          //alert(itm.itemName);
          if(itm.itemId==it.itemId){
            if(itm.totalBalanceQty!=undefined){
              //if(itm.menuId!=undefined){
                //console.log('itm.itemName',itm.itemName);
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                console.log('index <0 itm',it);
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                {
                  console.log('base itm',itm)
                  obj.UnitName=itm.UnitName;
                }
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(it.consumptionQty).toFixed(3);
                obj.consumptionAmount=parseFloat(it.consumptionAmount).toFixed(2);
                it.items.push(obj);
              }
              else {
              console.log('else itm',itm);
              it.items[itmIndex].consumptionQty=parseFloat(it.consumptionQty).toFixed(3);
              it.items[itmIndex].consumptionAmount=parseFloat(it.consumptionAmount).toFixed(2)
          
              }
              it.consumptionQty=parseFloat(it.consumptionQty).toFixed(3);
              it.consumptionAmount=parseFloat(it.consumptionAmount).toFixed(2);
              //}
            }
            
            } 
            /*else if(itm.itemId==it.itemId && itm.menuId != undefined){
              if(itm.totalBalanceQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalBalanceAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].openingQty)+parseFloat(it.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].openingAmt)+parseFloat(it.items[itmIndex].purchaseAmount)-parseFloat(itm.totalBalanceAmt)).toFixed(2)
                }
                it.consumptionQty=parseFloat(parseFloat(itm.openingQty)+parseFloat(itm.purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(itm.openingQty)+parseFloat(itm.purchaseAmount)-parseFloat(itm.totalBalanceAmt)).toFixed(2);//}
              }
              
            }*/
          });
        });
        return items;

      };




    $scope.calculateItemWiseTotal = function(items) {
      var consumptionQty = 0;
      var consumptionAmount = 0;
      var UnitName = "";

      _.forEach(items, function (item) {
        consumptionQty += Utils.roundNumber(item.consumptionQty, 3);
        consumptionAmount += Utils.roundNumber(item.consumptionAmount, 3);
        UnitName = item.UnitName;
      });

      return {
        consumptionQty: parseFloat(consumptionQty).toFixed(3),
        consumptionAmount: parseFloat(consumptionAmount).toFixed(3),
        UnitName: UnitName
      };
    };

    function getUniqueRawItems(result){
      var items=[];
      _.forEach(result.beforeDate,function(r,i){
        var index=_.findIndex(items,{itemId:r.itemId});
        if(index<0){
          var obj={itemId:r.itemId,itemName:r.itemName};
          items.push(obj);
        }
      });
      _.forEach(result.betweenDate,function(r,i){
        var index=_.findIndex(items,{itemId:r.itemId});
        if(index<0){
          var obj={itemId:r.itemId,itemName:r.itemName};
          items.push(obj);
        }
      });
      return items;
    };

    function getUniqueItems(result){
      var items=[];
      _.forEach(result,function(r,i){
        var index=_.findIndex(items,{itemId:r.itemId});
        if(index<0){
          var obj={itemId:r.itemId,itemName:r.itemName};
          items.push(obj);
        }
        var rIndex=_.findIndex($scope.recipeQty,{itemId:r.itemId});
        if(rIndex>=0){
          r.totalSaleQtyinbase=parseFloat(r.totalSaleQtyinbase)/parseFloat($scope.recipeQty[rIndex].receipeQtyinBase);
          r.rawQuantityUsed=parseFloat(r.rawQuantity)*parseFloat(r.totalSaleQtyinbase);
          if(r.semiRawMaterialsQty!=undefined){
            r.semiRawItems=[];
            _.forEachRight(r.semiRawMaterialsQty,function(sr,ii){
              sr=parseFloat(sr)*parseFloat(r.totalSaleQtyinbase);
              r.semiRawMaterialsQty[ii]=sr;
              var obj={itemId:r.semiRawMaterialsId[ii],itemName:r.semiRawMaterials[ii],qty:r.semiRawMaterialsQty[ii]};
              r.semiRawItems.push(obj);
            });
            delete r.semiRawMaterials;
            delete r.semiRawMaterialsId;
            delete r.semiRawMaterialsQty;
          }
        }
      });
      return items;
    };

    function calculatePrice_ItemReport(result){
      _.forEach(result,function(r,i){
        r.rawAmount=parseFloat(0).toFixed(2);
        if(r.isSemi==true){
          if(r.semiRawItems!=undefined){
            var t=0;
            _.forEach(r.semiRawItems,function(itm,ii){
              var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
              if(index>=0){
                t=parseFloat(t)+parseFloat(itm.qty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
            });
            r.rawAmount=parseFloat(t).toFixed(2);
          }
        }
        else
        {
          var index=_.findIndex($scope.lastPrice,{itemId:r.rawId});
          if(r.rawQuantityUsed!=undefined){
            if(index>=0){
              r.rawAmount=parseFloat(parseFloat(r.rawQuantityUsed)*parseFloat($scope.lastPrice[index].lastPrice)).toFixed(2);
            }
          }
        }
      });
      return result;
    };

    function convertItemInPreferredUnit_ItemReport(result){
      _.forEach(result,function(r,i){
        var indeex=_.findIndex($scope.stockitems,{_id:r.rawId});
        if(indeex>=0){
          var item=$scope.stockitems[indeex];
          if(item.preferedUnit!=undefined){
            _.forEach(item.units,function(u,i){
              if(u._id==item.preferedUnit){
                var conversionFactor=1;
                var pconFac=parseFloat(u.conversionFactor);
                r.rawUnitName=u.unitName;
                if(u.baseUnit.id == 2 || u.baseUnit.id == 3){
                  conversionFactor=parseFloat(u.baseConversionFactor);
                  if(pconFac>conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)/parseFloat(conversionFactor)).toFixed(3);
                  }
                  else if(pconFac<conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)*parseFloat(conversionFactor)).toFixed(3);
                  }
                }
                else
                {
                  if(pconFac>conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)/parseFloat(pconFac)).toFixed(3);
                  }
                  else if(pconFac<conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)*parseFloat(pconFac)).toFixed(3);
                  }
                }
              }
            });
          }
        }
      });
      return result;
    };

    function convertItemInPreferredUnit_ItemReport_New(result){
      console.log($scope.stockitems);
      console.log(result);
      _.forEach(result,function(r,ii){
        _.forEach(r.items,function(itm, i){
          //alert(itm.itemName + " " + itm.UnitName)
          var indeex=_.findIndex($scope.stockitems,{_id:itm.itemId});
          console.log(indeex, itm.itemName);
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
             if(!itm.itemCode)
            itm.itemCode = item.itemCode;
            //console.log('item 101',item)
            var unitId;
            if(typeof item.preferedUnit == Object){
              console.log(item.itemName);
              unitId = item.preferedUnit._id;
            } else{
              //console.log('else', item.itemName);
              unitId = item.preferedUnit;
            }
            if(item.preferedUnit!=undefined){
              _.forEach(item.units,function(u,i){
                if(u._id==unitId){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  //console.log(itm);
                  console.log(u);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>=conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
            else
            {
            _.forEach(item.units,function(u,i){
                
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  //console.log(itm);
                  console.log(u);
                  itm.UnitName=u.baseUnit.name;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>=conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                
              });
            }
          }
        });
      });
      return result;
    };
    function getItemsReceipeDetails(result,items){
      _.forEach(items,function(itm,i){
        itm.receipeDetails=[];
        _.forEach(result,function(r,ii){
          if(r.itemId==itm.itemId){
            //var index=_.findIndex(itm.receipeDetails,{rawId:r.rawId});
            var index=_.findIndex(itm.receipeDetails,{menuId:r.menuId});
            if(index<0){
              var obj={menuId:r.menuId,itemName:r.menuName,qty:parseFloat(r.rawQuantityUsed).toFixed(3),amount:parseFloat(r.rawAmount).toFixed(2),UnitName:r.rawUnitName};
              itm.receipeDetails.push(obj);
            }
            else
            {
              itm.receipeDetails[index].qty=parseFloat(parseFloat(itm.receipeDetails[index].qty)+parseFloat(r.rawQuantityUsed)).toFixed(3);
            }
          }
        });
      });
      return items;
    };

    function formatDataForItemReportViaPhysical(result,startDate,endDate){
        result.items=[];
        var endDateToShow = new Date(endDate);
      var startDateToShow = new Date(startDate)
      endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
      _.forEach(result.beforeDate,function(itm,i){
        if(itm.totalOpeningQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            if(startDate.getTime() < new Date(itm.created).getTime())
              startDateToShow = new Date(itm.created);
            itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            //console.log('total balance', itm.totalBalanceQty);
            result.items[itmIndex].openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(itm.totalBalanceAmt).toFixed(2);
          }
        }
        else if(itm.totalPurchaseQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
          }
        }
        else if(itm.totalIntermediateQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
          }
        }
        else if(itm.totalSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalTransferQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
          }
        }
        else if(itm.totalTransferQty_toStore!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
          }
        }
        else if(itm.itemSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            itm.UnitName=itm.baseUnit;
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
          }
        }
        else if(itm.totalStockReturnQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(-itm.totalStockReturnQty).toFixed(3);
            itm.openingAmt=parseFloat(-itm.totalStockReturnAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        }
        else if(itm.totalStockReceiveQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
            itm.openingAmt=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.consumptionAmount=parseFloat(0).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.varianceQty = parseFloat(0).toFixed(2);
            itm.varianceInPercent = parseFloat(0).toFixed(2);
            itm.physicalQty = parseFloat(0).toFixed(3);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);

            result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
            result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
          }
        }

      });
      _.forEach(result.betweenDate,function(itm,i){
        if(itm.totalBalanceQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            console.log('index <0 item',itm);
            console.log('index <0 physical :- ',itm.itemName,itm.purchaseQty,itm.openingQty,itm.totalBalanceQty,itm.consumptionQty);
          
            itm.openingQty=parseFloat(0).toFixed(3);
            itm.openingAmt=parseFloat(0).toFixed(2);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.purchaseAmount=parseFloat(0).toFixed(2);
            itm.consumptionQty=parseFloat(parseFloat(itm.openingQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
            itm.consumptionAmount=parseFloat(parseFloat(itm.openingAmt)-parseFloat(itm.totalBalanceAmt)).toFixed(2);
            itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
            itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
            itm.wastageQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = parseFloat(0).toFixed(2);
            itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            console.log('else item',itm);
            console.log('else physical :- ',itm.itemName,itm.purchaseQty,itm.openingQty,itm.totalBalanceQty,itm.consumptionQty);
           //result.items[itmIndex].closingQty = parseFloat(itm.totalBalanceQty).toFixed(3);
           //result.items[itmIndex].closingAmount = parseFloat(itm.totalBalanceAmt).toFixed(3);
                  
           result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(result.items[itmIndex].purchaseQty)-parseFloat(itm.totalBalanceQty)).toFixed(3);
           result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(result.items[itmIndex].purchaseAmount)-parseFloat(itm.totalBalanceAmt)).toFixed(2)
           console.log('result.items[itmIndex].consumptionQty',result.items[itmIndex].consumptionQty);
           console.log('result.items[itmIndex].consumptionAmount',result.items[itmIndex].consumptionAmount);
          }
        }
        /*if(itm.totalSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined) {
          var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
          if (itmIndex < 0) {
            itm.consumptionQty = parseFloat(itm.totalWastageQty).toFixed(3);
            itm.consumptionAmount = parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
          }
          else {
            result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalStockReturnQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {

            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        } 
       else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.consumptionQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {

                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
        }
        else if(itm.totalTransferQty!=undefined){
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
                      }
        }
        else if(itm.itemSaleQty!=undefined){
                      //alert(itm.itemName);
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      //alert(itmIndex);
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);
                      }
        }
        else if(itm.totalIntermediateQty!=undefined){
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                      }
        }*/
      });
      return result;
    };

    function formatDataForItemReport(result){
      result.items=[];
      console.log('result.betweenDate',result.betweenDate);
      _.forEach(result.betweenDate,function(itm,i){
        if(itm.totalSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined) {
          var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
          if (itmIndex < 0) {
            itm.consumptionQty = parseFloat(itm.totalWastageQty).toFixed(3);
            itm.consumptionAmount = parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
          }
          else {
            result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalStockReturnQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {

            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        } 
       else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.consumptionQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {

                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
        }
        else if(itm.totalTransferQty!=undefined){
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
                      }
        }
        else if(itm.itemSaleQty!=undefined){
                      //alert(itm.itemName);
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      //alert(itmIndex);
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);
                      }
        }
        else if(itm.totalIntermediateQty!=undefined){
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                      }
        }
           else if(itm.totalIndentingChallanQty!=undefined) {
          var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
          if (itmIndex < 0) {
            itm.consumptionQty = parseFloat(itm.totalIndentingChallanQty).toFixed(3);
            itm.consumptionAmount = parseFloat(itm.totalIndentingChallanAmt).toFixed(2);
            result.items.push(itm);
          }
          else {
            result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalIndentingChallanQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalIndentingChallanAmt)).toFixed(2);
          }
        }
      });
      return result;
    };


    function FormatItemData(result){
      _.forEach(result,function(r,ii){
        _.forEach(r.receipeDetails,function(itm,i){
          if(itm.UnitName==undefined){
            itm.UnitName=itm.selectedUnitName;
          }
          itm.consumptionQty=0;itm.consumptionAmount=0;
          if(itm.totalSaleQty==undefined || itm.totalSaleAmount==undefined){
            itm.totalSaleQty=0;itm.totalSaleAmount=0;
          }
          if(itm.totalWastageQty==undefined || itm.totalWastageAmt==undefined){
            itm.totalWastageQty=0;itm.totalWastageAmt=0;
          }
          if(itm.totalTransferQty==undefined || itm.totalTransferAmount==undefined){
            itm.totalTransferQty=0;itm.totalTransferAmount=0;
          }
          if(itm.totalIntermediateQty==undefined || itm.totalIntermediateAmt==undefined){
            itm.totalIntermediateQty=0;itm.totalIntermediateAmt=0;
          }
          itm.consumptionQty=parseFloat(parseFloat(itm.totalSaleQty)+parseFloat(itm.totalWastageQty)+parseFloat(itm.totalTransferQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
          itm.consumptionAmount=parseFloat(parseFloat(itm.totalSaleAmount)+parseFloat(itm.totalWastageAmt)+parseFloat(itm.totalTransferAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
        });
      });
      return result;
    };

    //----------------------------------------------------Reports----------------------------------------------
    $scope.exportToPdf_purchaseConsumption=function(){
      var table=document.getElementById('consumptionReport').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    
    $scope.exportToExcel_purchaseConsumption=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('consumptionReport').innerHTML));
    };
    $scope.exportToPdf_stockSummaryForm=function(){
      var table=document.getElementById('stockSummary').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    $scope.exportToExcel_stockSummaryForm=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('stockSummary').innerHTML));
    };
    $scope.exportToPdf_itemWiseConsumptionForm=function(){
      var table=document.getElementById('itemWiseConsumption').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    $scope.exportToExcel_itemWiseConsumptionForm=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('itemWiseConsumption').innerHTML));
    };

  }]).directive('stopccp', function(){
    return {
        scope: {},
        link:function(scope,element){
            element.on('cut copy paste', function (event) {
              event.preventDefault();
            });
        }
    };
}).controller('EmailModalCtrl', ['$scope', '$modalInstance', 'growl', function($scope, $modalInstance, growl) {
  console.log('modal opened');
    $scope.emailData = {};
    $scope.emailData.ok = function () {
      console.log('ok');
      if($scope.emailData.email.toLowerCase() == $scope.emailData.confirmEmail.toLowerCase()){
        $modalInstance.close($scope.emailData.email);
      } else {
        growl.error("Email doesn't match!", {ttl: 3000});
      }
    } 
    $scope.emailData.cancel = function () {
      $modalInstance.dismiss('cancel');
    } 
}]);