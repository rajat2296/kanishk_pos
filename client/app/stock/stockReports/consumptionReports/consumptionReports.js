'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('consumptionReports', {
        url: '/consumptionReports',
        templateUrl: 'app/stock/stockReports/consumptionReports/consumptionReports.html',
        controller: 'ConsumptionReportsCtrl'
      });
  });