'use strict';

describe('Controller: VarianceReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var VarianceReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VarianceReportsCtrl = $controller('VarianceReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
