'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('varianceReports', {
        url: '/varianceReports',
        templateUrl: 'app/stock/stockReports/varianceReports/varianceReports.html',
        controller: 'VarianceReportsCtrl'
      });
  });