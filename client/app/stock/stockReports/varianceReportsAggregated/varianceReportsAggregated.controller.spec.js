'use strict';

describe('Controller: VarianceReportsCtrlAggr', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var VarianceReportsCtrlAggr, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VarianceReportsCtrlAggr = $controller('VarianceReportsCtrlAggr', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
