'use strict';

angular.module('posistApp')
    .controller('VarianceReportsCtrlAggr',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','StockReport','localStorageService','StockReportNew','stockItem','StockReportService','baseKitchenItem', 'deployment', 'Utils', 'stockReportResetTime','intercom', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,StockReport,localStorageService,StockReportNew,stockItem,StockReportService,baseKitchenItem, deployment, Utils, stockReportResetTime, intercom) {
      currentUser.deployment_id=localStorageService.get('deployment_id');
      $scope.stores=[];
      $scope.stockitems=[];
      $scope.user=currentUser;
      //    function getStores_ByDeployment(){
      //      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
      //    };
      //    var allPromise=$q.all([
      //        getStores_ByDeployment()
      // ]);
      //    allPromise.then(function (value){
      //        $scope.stores = value[0];
      //        $scope.purchaseConsumptionForm.stores=$scope.stores;
      //        //$scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0]._id;
      // });

      function getRawPricing() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_Receipe({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /* return StockReport.RawMaterialPricing_Receipe({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    };

    function getRawPricingBasekitchen() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeBasekitchen({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /*  return StockReport.RawMaterialPricing_ReceipeBasekitchen({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    }

      store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.stores=result;
        $scope.consolidatedForm.stores=result;
        $scope.consolidatedForm.store=$scope.consolidatedForm.stores[0];
        $scope.bindCategory_consolidatedForm($scope.consolidatedForm.store);
        stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
          $scope.stockitems=result;
          baseKitchenItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (items){
            $scope.basekitchenitems=items;
          });
        });
        // StockReportNew.getLastPriceOfItem_RawMaterial({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        //   $scope.lastPrice=result;
        //   //console.log($scope.lastPrice);
        // });
      });

      $q.all([
          getRawPricing(),
          getRawPricingBasekitchen()
        ]).then(function (value) {
            $scope.lastPrice=value[0];
            _.forEach(value[1], function (price) {
                $scope.lastPrice.push(price);
            });      
            console.log($scope.lastPrice);
        }).catch(function (err) {
            console.log('Error fetching base kitchen items');
        });

      function getYesterDayDate() {
        var d=new Date();
        d.setDate(d.getDate()-1);
        return d;
      };

      function parseDate(str){
        var d=$filter('date')(str, 'dd-MMM-yyyy');
        return d;
      };

      $scope.consolidatedForm={
        fromDate:new Date(new Date().setDate(new Date().getDate() - 2)),//getYesterDayDate(),
        toDate:new Date(new Date().setDate(new Date().getDate() - 2)),
        maxDate:new Date(new Date().setDate(new Date().getDate() - 2))
      };

      $scope.checkDateGreater= function(date){
        var d=new Date(date);
        var dd=new Date($scope.consolidatedForm.toDate);
        if(d.getTime()>dd.getTime()){
          growl.success('Greater than toDate', {ttl: 3000});
          $scope.consolidatedForm.fromDate=new Date();//getYesterDayDate()
        }
      };

      $scope.checkDateLesser= function(date){
        var d=new Date(date);
        var dd=new Date($scope.consolidatedForm.fromDate);
        if(d.getTime()<dd.getTime()){
          growl.success('Can not be Lesser than fromDate', {ttl: 3000});
          $scope.consolidatedForm.toDate= new Date();
        }
      };

      $scope.clearConsolidatedTab=function(){
        $scope.consolidatedForm={
          fromDate:new Date(new Date().setDate(new Date().getDate() - 2)),//getYesterDayDate(),
          toDate:new Date(new Date().setDate(new Date().getDate() - 2)),
          maxDate:new Date(new Date().setDate(new Date().getDate() - 2)),
          stores:angular.copy($scope.stores)
        };
        $scope.consolidatedForm.store=$scope.consolidatedForm.stores[0];
        $scope.bindCategory_consolidatedForm($scope.consolidatedForm.store);
      };

      $scope.bindCategory_consolidatedForm=function(store){
        $scope.consolidatedForm.categories=[];
        $scope.consolidatedForm.availableItems=[];
        var str=angular.copy(store);
        _.forEach(str.category, function(c,i){
          $scope.consolidatedForm.categories.push(c);
        });
      };

      function compare(a,b) {
        if (a.itemName < b.itemName)
          return -1;
        if (a.itemName > b.itemName)
          return 1;
        return 0;
      };

      $scope.bindItems_consolidatedForm= function(category){
        var cat=angular.copy(category);
        $scope.consolidatedForm.availableItems=[];
        $scope.consolidatedForm.item=undefined;
        if(cat!=null){
          _.forEach(cat.item, function(c,i){
            $scope.consolidatedForm.availableItems.push(c);
          });
        }
        $scope.consolidatedForm.availableItems.sort(compare);
      };

      function getItemForSearch(stores){
        var item=[];
        _.forEach(stores.category,function(c,i){
          _.forEach(c.item, function(itm,ii){
            var obj={id:itm._id,itemName:itm.itemName};
            item.push(obj);
          });
        });
        _.forEach($scope.basekitchenitems,function(itm){
          var obj={id:itm.itemId,itemName:itm.itemName};
          item.push(obj);
        });
        return item;
      };

      function getCategoryForSearch(stores){
        var item=[];
        _.forEach(stores.category,function(c,i){
          var obj={_id:c._id};
          item.push(obj);
        });
        return item;
      };

      $scope.openEmailModal = function (report) {
      var modalInstance = $modal.open({
      component: 'modalComponent',
      templateUrl: 'emailModal.html',
      size: 'sm',
      controller: 'EmailModalCtrl'
    });

    modalInstance.result.then(function (email) {
      if(report == 4)
        $scope.startSearch(email);
      if(report == 5){
        $scope.startSearch_DateWise(email);
      }
    }, function () {
      console.log('modal-component dismissed at: ' + new Date());
    });
    }

      $scope.startSearch= function(email){
        intercom.registerEvent('StockReports');
        $scope.consolidatedForm.isSearch=true;
        var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.consolidatedForm.fromDate))));
        var toDate = new Date($scope.consolidatedForm.toDate);
        var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.consolidatedForm.toDate))));
        var chkStart = moment(fromDate);
        var chkEnd = moment(toDate);
        var duration = moment.duration(chkEnd.diff(chkStart));
        if(duration.asMonths() > 1){
          growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
          //$scope.purchaseConsumptionForm.isSearch=false;
          $scope.consolidatedForm.isSearch=false;
          return;
        }
        toDate = new Date(toDate.setDate(toDate.getDate() + 1));
        //console.log(fromDate);
        //console.log(toDateReset);
        var strId=$scope.consolidatedForm.store._id;
        var isKitchen=$scope.consolidatedForm.store.isKitchen;
        var catId=[];
        if($scope.consolidatedForm.item!=undefined || $scope.consolidatedForm.item!=null){
          var item=[{id:$scope.consolidatedForm.item._id,itemName:$scope.consolidatedForm.item.itemName}];
        }
        else if($scope.consolidatedForm.category!=undefined || $scope.consolidatedForm.category!=null){
          var item=[];
          _.forEach($scope.consolidatedForm.category.item, function(itm,i){
            var obj={id:itm._id,itemName:itm.itemName};
            item.push(obj);
          });
          catId.push({_id:$scope.consolidatedForm.category._id});
        }
        else if($scope.consolidatedForm.store!=undefined){
          var item=getItemForSearch(angular.copy($scope.consolidatedForm.store));
          catId=getCategoryForSearch(angular.copy($scope.consolidatedForm.store));
        };
        var toDate1=new Date(toDateReset);
        toDate1.setDate(toDate1.getDate()+1);
        //console.log(new Date(toDate1))
        

        //var req={"fromDate":fromDate,"toDate":toDate1,"storeId":strId,"category":catId,"isKitchen":isKitchen,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, report: 4, email: email};

        var req={"startDate":fromDate,"endDate":toDate1,"store_id":strId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};

        //console.log(req);

        // StockReport.getConsumptionSummary(req, function (result){
        //  $scope.consolidatedForm.reportData=result;
        //  console.log(result);
        // });
        var varianceConsolidatedReport=new StockReportService('',fromDate,toDate1,item,$scope.lastPrice,$scope.stockitems, deployment);
        varianceConsolidatedReport.getVarianceConsolidatedAggregated(req, $scope.lastPrice).then(function(val){
          //console.log(val);
          $scope.consolidatedForm.reportData=val;
          dynamicVarianceReport(val);
          $scope.consolidatedForm.isSearch=false;
        },function(err){
          //console.log(err);
          $scope.consolidatedForm.reportData = {};
          $scope.consolidatedForm.reportData.errorMessage = err;
          $scope.consolidatedForm.isSearch=false;
        });

        // StockReportNew.getConsumptionSummary(req, function (result){
        //   console.log(result);
        //   var cReport=new StockReportService(result,fromDate,toDate,item,$scope.lastPrice,$scope.stockitems);
        //   // var result=cReport.spliceandmovePhysical();
        //   // spliceandmovePhysical(result,fromDate);
        //   //var result=spliceAllItemsBeforePhysical_Cons(result,toDate);
        //   result=cReport.spliceAllItemsBeforePhysical();
        //   // result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        //   // result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        //   // result=formatDataForReport(result,fromDate,toDate);
        //   result=cReport.formatDataForReport();
        //   //result=spliceForCategoryAndItem(result,item);
        //   result=cReport.spliceForCategoryAndItem();
        //   //var res=convertItemInPreferredUnit(result);
        //   var res=cReport.convertItemInPreferredUnit();
        //   $scope.consolidatedForm.reportData=res;
        //   $scope.consolidatedForm.isSearch=false;
        // });
      };
      function dynamicVarianceReport(val) {
        var html = " <table class='table  table-curved' style='width:100%' border='1' ng-show='consolidatedForm.reportData.items.length>0'>";
        html+="<thead >";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Item Name</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff''  >Opening Stock</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Purchases</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Consumption</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Wastage</th>"
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Closing Qty</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Physical Stock</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Variance</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Variance Cost</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Unit</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff'' >Variance(In %)</th>";
        html+="</thead>";

        html+=" <tbody>";
        _.forEach(val.items, function (i) {
        html += "<tr>";
        html += "<td>" + i.itemName + "</td>";
        html += "<td>" + i.openingQty + "</td>";
        html += "<td>" + i.purchaseQty + "</td>";
        html += "<td>" + i.consumptionQty + "</td>";
        html += "<td>" + i.wastageQty + "</td>";
        html += "<td>" + i.closingQty + "</td>";
        html += "<td>" + i.physicalQty + "</td>";
        html += "<td>" + i.varianceQty + "</td>";
        html += "<td>" + i.varianceAmount + "</td>";
        html += "<td>" + i.UnitName + "</td>";
        html += "<td>" + i.varianceInPercent + "</td>";
        html += "</tr>";
        })
        html+=" <tbody>";
        html+="</table>";

        document.getElementById('consolidatedReport').innerHTML = html;
        return html;
      }
      function spliceandmovePhysical(result,toDate){
        _.forEachRight(result.betweenDate,function(b,i){
          if(b.totalBalanceQty!=undefined){
            var d=new Date(toDate);
            d.setHours(0,0,0,0);

            var cDate=new Date(b.created);
            cDate.setHours(0,0,0,0);
            if(d.getTime()>= cDate.getTime()){
              result.beforeDate.push(b);
              result.betweenDate.splice(i,1);
            }
          }
        });
      };
      function spliceAllItemsBeforePhysical(result,toDate){
        var data=[];
        _.forEach(result.beforeDate,function(r,i){
          if(r.totalBalanceQty!=undefined){
            var d=new Date(r.created);
            d.setDate(d.getDate() + 1);
            d.setHours(0, 0, 0, 0);
            r.created=d;
            data.push(r);
          };
        });
        _.forEach(data,function(d,i){
          _.forEachRight(result.beforeDate,function(itm,ii){
            if(itm.itemId==d.itemId){
              //if(itm.totalBalanceQty!=undefined){
              var phyDate=new Date(d.created);
              var compareDate=new Date(itm.created);
              if(phyDate.getTime()>compareDate.getTime()){
                result.beforeDate.splice(ii,1);
              }
              //}
            }
          });
        });

        // var data2=[];
        _.forEachRight(result.betweenDate,function(r,i){
          if(r.totalBalanceQty!=undefined){
            var d=new Date(r.created);
            var cdate=new Date(toDate);
            d.setDate(d.getDate() + 1);
            d.setHours(0, 0, 0, 0);
            r.created=d;
            // if(cdate.getTime()>=d.getTime()){
            //   data2.push(r);
            // }
            // else
            // {
            //   result.betweenDate.splice(i,1);
            // }
          };
        });
        // _.forEach(data2,function(d,i){
        //   _.forEachRight(result.betweenDate,function(itm,ii){
        //     if(itm.itemId==d.itemId){
        //       if(itm.totalBalanceQty!=undefined){
        //         var phyDate=new Date(d.created);
        //         var compareDate=new Date(toDate);
        //         if(phyDate.getTime()>compareDate.getTime()){
        //           result.betweenDate.splice(ii,1);
        //         }
        //       }
        //     }
        //   });
        // });

        return result;
      };
      function spliceAllItemsBeforePhysical_Cons(result,toDate){
        var data=[];
        _.forEach(result.beforeDate,function(r,i){
          if(r.totalBalanceQty!=undefined){
            var d=new Date(r.created);
            d.setDate(d.getDate() + 1);
            d.setHours(0, 0, 0, 0);
            r.created=d;
            data.push(r);
          };
        });
        _.forEach(data,function(d,i){
          _.forEachRight(result.beforeDate,function(itm,ii){
            if(itm.itemId==d.itemId){
              var phyDate=new Date(d.created);
              var compareDate=new Date(itm.created);
              if(phyDate.getTime()>compareDate.getTime()){
                result.beforeDate.splice(ii,1);
              }
            }
          });
        });

        // var data2=[];
        _.forEachRight(result.betweenDate,function(r,i){
          if(r.totalBalanceQty!=undefined){
            var d=new Date(r.created);
            var cdate=new Date(toDate);
            d.setDate(d.getDate() + 1);
            d.setHours(0, 0, 0, 0);
            r.created=d;
            // if(cdate.getTime()>=d.getTime()){
            //   data2.push(r);
            // }
            // else
            // {
            //   result.betweenDate.splice(i,1);
            // }
          };
        });
        // _.forEach(data2,function(d,i){
        //   _.forEachRight(result.betweenDate,function(itm,ii){
        //     if(itm.itemId==d.itemId){
        //       if(itm.totalBalanceQty!=undefined){
        //         var phyDate=new Date(d.created);
        //         var compareDate=new Date(itm.created);
        //         if(phyDate.getTime()>compareDate.getTime()){
        //           result.betweenDate.splice(ii,1);
        //         }
        //       }
        //     }
        //   });
        // });
        return result;
      };
      function formatDataForReport(result,startDate,endDate){
        result.items=[];
        _.forEach(result.beforeDate,function(itm,i){
          if(itm.totalOpeningQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
            }
          }
          else if(itm.totalBalanceQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
          }
          else if(itm.totalIntermediateQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
            }
          }
          else if(itm.totalPurchaseQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
            }
          }
          else if(itm.totalSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
            }
          }
          else if(itm.totalWastageQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
            }
          }
          else if(itm.itemSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty=parseFloat(0).toFixed(3);
              itm.varianceQty=parseFloat(0).toFixed(2);
              itm.varianceInPercent=parseFloat(0).toFixed(2);
              itm.UnitName=itm.baseUnit;
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
            }
          }
        });
        _.forEach(result.betweenDate,function(itm,i){
          if(itm.totalPurchaseQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              if(itm.openingQty==undefined){
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(3);
                itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
                itm.physicalQty=parseFloat(0).toFixed(3);
                itm.varianceQty=parseFloat(0).toFixed(2);
                itm.varianceInPercent=parseFloat(0).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
              }
              if(itm.consumptionQty==undefined){
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(3);
              }
              if(itm.wastageQty==undefined){
                itm.wastageQty=parseFloat(0).toFixed(3);
              }
              itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
            }
          }
          else if(itm.totalIntermediateQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              if(itm.openingQty==undefined){
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(3);
                itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
              }
              if(itm.purchaseQty==undefined){
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(3);
              }
              itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
              itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalIntermediateAmt)).toFixed(2);
            }
          }
          else if(itm.totalSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              if(itm.openingQty==undefined){
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(3);
                itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
                itm.physicalQty=parseFloat(0).toFixed(3);
                itm.varianceQty=parseFloat(0).toFixed(2);
                itm.varianceInPercent=parseFloat(0).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
              }
              if(itm.purchaseQty==undefined){
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(3);
              }
              if(itm.wastageQty==undefined){
                itm.wastageQty=parseFloat(0).toFixed(3);
              }
              itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
              itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
            }
          }
          else if(itm.totalWastageQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              if(itm.openingQty==undefined){
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(3);
                itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
                itm.physicalQty=parseFloat(0).toFixed(3);
                itm.varianceQty=parseFloat(0).toFixed(2);
                itm.varianceInPercent=parseFloat(0).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
              }
              if(itm.purchaseQty==undefined){
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(3);
              }
              if(itm.consumptionQty==undefined){
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(3);
              }
              itm.wastageQty=parseFloat(itm.totalWastageQty).toFixed(3);
              itm.wastageAmt=parseFloat(itm.totalWastageAmt).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].wastageQty=parseFloat(parseFloat(result.items[itmIndex].wastageQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].wastageAmt=parseFloat(parseFloat(result.items[itmIndex].wastageAmt)+parseFloat(itm.totalWastageAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              if(itm.openingQty==undefined){
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(3);
                itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
                itm.physicalQty=parseFloat(0).toFixed(3);
                itm.varianceQty=parseFloat(0).toFixed(2);
                itm.varianceInPercent=parseFloat(0).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
              }
              if(itm.consumptionQty==undefined){
                itm.consumptionQty=parseFloat(0).toFixed(3);
                itm.consumptionAmount=parseFloat(0).toFixed(3);
              }
              if(itm.wastageQty==undefined){
                itm.wastageQty=parseFloat(0).toFixed(3);
              }
              itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              if(itm.openingQty==undefined){
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(3);
                itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
                itm.physicalQty=parseFloat(0).toFixed(3);
                itm.varianceQty=parseFloat(0).toFixed(2);
                itm.varianceInPercent=parseFloat(0).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
              }
              if(itm.purchaseQty==undefined){
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(3);
              }
              if(itm.wastageQty==undefined){
                itm.wastageQty=parseFloat(0).toFixed(3);
              }
              itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
              itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
            }
          }
          else if(itm.itemSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              if(itm.openingQty==undefined){
                itm.openingQty=parseFloat(0).toFixed(3);
                itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
                itm.openingAmt=parseFloat(0).toFixed(3);
                itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
                itm.physicalQty=parseFloat(0).toFixed(3);
                itm.varianceQty=parseFloat(0).toFixed(2);
                itm.varianceInPercent=parseFloat(0).toFixed(2);
                itm.openingDate=new Date(startDate);
                itm.closingDate=new Date(endDate);
              }
              if(itm.purchaseQty==undefined){
                itm.purchaseQty=parseFloat(0).toFixed(3);
                itm.purchaseAmount=parseFloat(0).toFixed(3);
              }
              if(itm.wastageQty==undefined){
                itm.wastageQty=parseFloat(0).toFixed(3);
              }
              itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
              itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
              itm.UnitName=itm.baseUnit;
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
            }
          }
          else if(itm.totalBalanceQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            var cDate=new Date(itm.created);
            cDate.setDate(cDate.getDate()-1);
            //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
            //var ed = new Date(endDate);
            //ed.setDate(ed.getDate()-1);
            //console.log(cDate);
            //console.log(endDate);
            //console.log(ed);

            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              if(endDate.getTime()==cDate.getTime() || ed.getTime()==cDate.getTime()){
                itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
                itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
              }
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              result.items.push(itm);
            }
            else
            {
              if(endDate.getTime()==cDate.getTime()){
                result.items[itmIndex].physicalQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].physicalAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
                result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
                result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);

                // result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
              }
              else
              {
                //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
                // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
                // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
                // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
              }
            }
          }
          // else if(itm.totalBalanceQty!=undefined){
          //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          //   if(itmIndex<0){
          //     if(itm.openingQty==undefined){
          //       itm.openingQty=parseFloat(0).toFixed(3);
          //       itm.closingQty=parseFloat(0).toFixed(3);
          //       itm.openingAmt=parseFloat(0).toFixed(3);
          //       itm.closingAmount=parseFloat(0).toFixed(3);
          //       itm.openingDate=new Date(startDate);
          //       itm.closingDate=new Date(endDate);
          //     }
          //     if(itm.purchaseQty==undefined){
          //       itm.purchaseQty=parseFloat(0).toFixed(3);
          //       itm.purchaseAmount=parseFloat(0).toFixed(3);
          //     }
          //     if(itm.wastageQty==undefined){
          //       itm.wastageQty=parseFloat(0).toFixed(3);
          //     }
          //     itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
          //     itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);

          //     itm.varianceQty=parseFloat(parseFloat(itm.closingQty)-parseFloat(itm.physicalQty)).toFixed(2);
          //     itm.varianceInPercent=parseFloat(parseFloat(parseFloat(itm.varianceQty)*parseFloat(100))/parseFloat(itm.closingQty)).toFixed(2);
          //     itm.UnitName=itm.baseUnit;
          //     result.items.push(itm);
          //   }
          //   else
          //   {
          //     result.items[itmIndex].physicalQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
          //     result.items[itmIndex].physicalAmt=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
          //     result.items[itmIndex].varianceQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(result.items[itmIndex].physicalQty)).toFixed(3);
          //     result.items[itmIndex].varianceInPercent=parseFloat(parseFloat(parseFloat(result.items[itmIndex].varianceQty)/parseFloat(result.items[itmIndex].closingQty))*parseFloat(100)).toFixed(2);
          //   }
          // }

        });
        return result;
      };

      function spliceForCategoryAndItem(result,items){
        _.forEachRight(result.items,function(itm,i){
          var index=_.findIndex(items,{id:itm.itemId});
          if(index<0){
            result.items.splice(i,1);
          }
        });
        return result;
      };
      function convertItemInPreferredUnit(result){
        _.forEach(result.items,function(itm,i){
          var indeex=_.findIndex($scope.stockitems,{_id:itm.itemId});
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
            if(item.preferedUnit!=undefined){
              _.forEach(item.units,function(u,i){
                if(u._id==item.preferedUnit){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)/parseFloat(conversionFactor)).toFixed(3);
                      if(itm.physicalQty==undefined){
                        itm.physicalQty=parseFloat(0).toFixed(3);
                      }
                      else
                      {
                        itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)/parseFloat(conversionFactor)).toFixed(3);
                      }
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)*parseFloat(conversionFactor)).toFixed(3);
                      if(itm.physicalQty==undefined){
                        itm.physicalQty=parseFloat(0).toFixed(3);
                      }
                      else
                      {
                        itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)*parseFloat(conversionFactor)).toFixed(3);
                      }
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)/parseFloat(pconFac)).toFixed(3);
                      if(itm.physicalQty==undefined){
                        itm.physicalQty=parseFloat(0).toFixed(3);
                      }
                      else
                      {
                        itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)/parseFloat(pconFac)).toFixed(3);
                      }
                      itm.varianceQty=parseFloat(parseFloat(itm.varianceQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)*parseFloat(pconFac)).toFixed(3);
                      if(itm.physicalQty==undefined){
                        itm.physicalQty=parseFloat(0).toFixed(3);
                      }
                      else
                      {
                        itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)*parseFloat(pconFac)).toFixed(3);
                      }
                      itm.varianceQty=parseFloat(parseFloat(itm.varianceQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        });
        return result;
      };
      //-------------------------------------stockSummaryForm--------------------------------------------------//
      $scope.clearDateWiseTab= function(){
        ResetclearDateWiseTab();
      };

      function ResetclearDateWiseTab(){
        $scope.dateWiseForm={
          fromDate:new Date(new Date().setDate(new Date().getDate() - 2)),//getYesterDayDate(),
          toDate:new Date(new Date().setDate(new Date().getDate() - 2)),
          maxDate:new Date(new Date().setDate(new Date().getDate() - 2)),
          stores:angular.copy($scope.stores)
        };
        $scope.dateWiseForm.store=$scope.dateWiseForm.stores[0];
        $scope.bindCategory_DateWiseForm($scope.dateWiseForm.store);
      };
      $scope.checkDateGreater_DateWise= function(date){
        var d=new Date(date);
        var dd=new Date($scope.dateWiseForm.toDate);
        if(d.getTime()>dd.getTime()){
          growl.success('Greater than toDate', {ttl: 3000});
          $scope.dateWiseForm.fromDate=new Date();//getYesterDayDate()
        }
      };

      $scope.checkDateLesser_DateWise= function(date){
        var d=new Date(date);
        var dd=new Date($scope.dateWiseForm.fromDate);
        if(d.getTime()<dd.getTime()){
          growl.success('Can not be Lesser than fromDate', {ttl: 3000});
          $scope.dateWiseForm.toDate= new Date();
        }
      };

      $scope.bindCategory_DateWiseForm=function(store){
        $scope.dateWiseForm.categories=[];
        $scope.dateWiseForm.availableItems=[];
        var str=angular.copy(store);
        _.forEach(str.category, function(c,i){
          $scope.dateWiseForm.categories.push(c);
        });
      };

      $scope.bindItems_DateWiseForm= function(category){
        $scope.dateWiseForm.availableItems=[];
        var cat=angular.copy(category);
        $scope.dateWiseForm.availableItems=[];
        $scope.dateWiseForm.item=undefined;
        if(cat!=null){
          _.forEach(cat.item, function(c,i){
            $scope.dateWiseForm.availableItems.push(c);
          });
        }
        $scope.dateWiseForm.availableItems.sort(compare);
      };

      $scope.startSearch_DateWise=function(email){
        intercom.registerEvent('StockReports');
        var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dateWiseForm.fromDate))));
        var toDate = $scope.dateWiseForm.toDate;
        var chkStart = moment(fromDate);
        var chkEnd = moment(toDate);
        var duration = moment.duration(chkEnd.diff(chkStart));
        if(duration.asMonths() > 1){
          growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
          //$scope.purchaseConsumptionForm.isSearch=false;
          return;
        }
        var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.dateWiseForm.toDate))));
        //console.log(fromDate);
        //console.log(toDateReset);

        var strId=$scope.dateWiseForm.store._id;
        var isKitchen=$scope.dateWiseForm.store.isKitchen;
        if($scope.dateWiseForm.item!=undefined || $scope.dateWiseForm.item!=null){
          var item=[{id:$scope.dateWiseForm.item._id,itemName:$scope.dateWiseForm.item.itemName}];
        }
        else if($scope.dateWiseForm.category!=undefined || $scope.dateWiseForm.category!=null){
          var item=[];
          _.forEach($scope.dateWiseForm.category.item, function(itm,i){
            var obj={id:itm._id,itemName:itm.itemName};
            item.push(obj);
          });
        }
        else if($scope.dateWiseForm.store!=undefined){
          var item=getItemForSearch(angular.copy($scope.dateWiseForm.store));
        };
        //"items":JSON.stringify(item)
        var toDate1=new Date(toDateReset);
        toDate1.setDate(toDate1.getDate()+1);
        //console.log(new Date(toDate1))


        //var req={"fromDate":fromDate,"toDate":toDate1,"storeId":strId,"isKitchen":isKitchen,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, report: 5, email: email};
        var req={"startDate":fromDate,"endDate":toDate1,"store_id":strId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};


        // StockReport.getConsumptionSummary_DateWise(req, function (result){
        //  console.log(result);
        //  var data=formatDataDateWise(result);
        //  console.log(data);
        //  var ss=formatDataDatewise_Date(data)
        //  console.log(ss);
        //  $scope.dateWiseForm.reportData=ss;
        // });
        var varianceDateWise=new StockReportService('',fromDate,toDate1,item,$scope.lastPrice,$scope.stockitems, deployment);
        varianceDateWise.getVarianceDateWiseReportAggregated(req).then(function(val){
          //console.log(val);
          dynamicVarianceReport_DateWise(val);

          var divLocal = new Date();
          var divUtc = new Date(divLocal.getUTCFullYear(), divLocal.getUTCMonth(), divLocal.getUTCDate(),  divLocal.getUTCHours(), divLocal.getUTCMinutes(), divLocal.getUTCSeconds()).toUTCString();
          //var ist=     new Date(divLocal.getUTCFullYear(), divLocal.getUTCMonth(), divLocal.getUTCDate(),  divLocal.getUTCHours(), divLocal.getUT
          //Minutes(), divLocal.getISTSeconds());
          //var divUtc = "Sat Oct 15 2016 09:45:14 GMT+0530 (India Standard Time)"
              //var divLocal = "Sat Oct 15 2016 15:15:14 GMT+0530 (India Standard Time)"
              //put UTC time into divUTC
          console.log('local',divLocal);
          console.log('utc',divUtc);
        //  var d=moment.utc(divUtc).format('YYYY-MM-DD HH:mm:ss');
          var a=moment(divUtc, 'MM-DD-YYYY HH:mm:ss').utc().format("YYYY-MM-DD HH:mm:ss");
          var b=new Date(a);
          b.setUTCHours(b.getUTCHours()+2);
          //console.log('d',d)
          console.log(b,'b');
              //get text from divUTC and conver to local timezone
              var localTime  = moment.utc(divUtc).toDate();
              localTime = moment(localTime).format('YYYY-MM-DD HH:mm:ss');
              //console.log(localTime);
              console.log('l time',localTime);

          $scope.dateWiseForm.reportData=val;
        },function(err){
          //console.log(err);
          $scope.dateWiseForm.reportData = {};
          $scope.dateWiseForm.reportData.errorMessage = err;
        });
        // StockReportNew.getConsumptionSummary(req, function (result){
        //   console.log(result);
        //   //var sr=new StockReportService(result,fromDate,toDate);
        //   var result=spliceAllItemsBeforePhysical(result,toDate);
        //   //var result=sr.spliceAllItemsBeforePhysical();
        //   result=formatDataForReport_DateWise(result,fromDate,toDate);
        //   result=spliceForCategoryAndItem(result,item);
        //   var res=convertItemInPreferredUnit_DateWise(result);
        //   var days=getDates(fromDate,toDate1);
        //   var gtR=generateDateWiseVarianceReport(res.items,days);
        //   var index=gtR.length-1;
        //   gtR.splice(index,1);
        //  //  console.log(res);
        //  //  var data=formatDataDateWise(res);
        //  // console.log(data);
        //  // var ss=formatDataDatewise_Date(data)
        //  // console.log(ss);
        //  $scope.dateWiseForm.reportData=gtR;
        //   //console.log(result);
        // });
      };
      function dynamicVarianceReport_DateWise(val){
        var html="<table class='table table-curved' style='width:100%' border='1' ng-show='dateWiseForm.reportData.length>0'>";
        html+="<thead >";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Item Name</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Opening Stock</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Purchases</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Consumption</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Wastage</th>"
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Closing Qty</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Physical Stock</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Variance</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Unit</th>";
        html += "<th style='background-color:#24242D;font-weight:bold;color:#fff' >Variance(In %)</th>";
        html+="</thead>";

        html+=" <tbody>";

        _.forEach(val, function (d) {
          var date=d.date.getDate() + '/' + (d.date.getMonth()+1) + '/' + d.date.getFullYear();
          html += "<tr>";

        //  html+="<td style='background-color:#abcabc' colspan='10'>+moment(+new Date(d.date)).format('YYYY-MM-DD')+"</td>"";
          html += "<td style='background-color:#abcabc'' colspan='10'>" + date  + "</td>";
          html += "</tr>";


          _.forEach(d.items, function (i) {
            html += "<tr>";
            html += "<tr>";
            html += "<td>" + i.itemName + "</td>";
            html += "<td>" + i.openingQty + "</td>";
            html += "<td>" + i.purchaseQty + "</td>";
            html += "<td>" + i.consumptionQty + "</td>";
            html += "<td>" + i.wastageQty + "</td>";
            html += "<td>" + i.closingQty + "</td>";
            html += "<td>" + i.physicalQty + "</td>";
            html += "<td>" + i.varianceQty + "</td>";
            html += "<td>" + i.UnitName + "</td>";
            html += "<td>" + i.varianceInPercent + "</td>";
            html += "</tr>";
          });

        });


        html+=" </tbody>";


        document.getElementById('dateWiseReport').innerHTML = html;
        return html;
      }

      function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
          dateArray.push( {date:new Date (currentDate) });
          currentDate = currentDate.addDays(1);
        }
        //console.log(dateArray);
        return dateArray;
      };

      Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
      };
      function convertItemInPreferredUnit_DateWise(result){
        _.forEach(result.items,function(itm,i){
          var indeex=_.findIndex($scope.stockitems,{_id:itm.itemId});
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
            if(item.preferedUnit!=undefined){
              _.forEach(item.units,function(u,i){
                if(u._id==item.preferedUnit){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)/parseFloat(conversionFactor)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)/parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)*parseFloat(conversionFactor)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)*parseFloat(conversionFactor)).toFixed(3);
                        }
                      }
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)/parseFloat(pconFac)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)/parseFloat(pconFac)).toFixed(3);
                        }
                        itm.varianceQty=parseFloat(parseFloat(itm.varianceQty)/parseFloat(pconFac)).toFixed(3);
                      }
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                      itm.wastageQty=parseFloat(parseFloat(itm.wastageQty)*parseFloat(pconFac)).toFixed(3);
                      if(itm.physicalQty!="NA"){
                        if(itm.physicalQty==undefined){
                          itm.physicalQty=parseFloat(0).toFixed(3);
                        }
                        else
                        {
                          itm.physicalQty=parseFloat(parseFloat(itm.physicalQty)*parseFloat(pconFac)).toFixed(3);
                        }
                        itm.varianceQty=parseFloat(parseFloat(itm.varianceQty)*parseFloat(pconFac)).toFixed(3);
                      }
                    }
                  }
                }
              });
            }
          }
        });
        return result;
      };
      function formatDataForReport_DateWise(result,startDate,endDate){
        result.items=[];
        _.forEach(result.beforeDate,function(itm,i){
          if(itm.totalOpeningQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
            }
          }
          else if(itm.totalBalanceQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              //itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(itm.totalBalanceQty).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
          }
          else if(itm.totalPurchaseQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
            }
          }
          else if(itm.totalIntermediateQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalIntermediateAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
            }
          }
          else if(itm.totalSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalSaleAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalSaleAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
            }
          }
          else if(itm.totalWastageQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalWastageAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalWastageAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)- parseFloat(itm.totalWastageAmt)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.totalTransferAmount).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.totalTransferAmount)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
            }
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
            }
          }
          else if(itm.itemSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itmIndex<0){
              itm.openingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(-itm.itemSaleAmt).toFixed(2);
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(2);
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(2);
              itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
              itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
              itm.wastageQty=parseFloat(0).toFixed(3);
              itm.wastageAmt=parseFloat(0).toFixed(2);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
              itm.physicalQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceQty="NA";//parseFloat(0).toFixed(3);
              itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
              itm.UnitName=itm.baseUnit;
              result.items.push(itm);
            }
            else
            {
              result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)-parseFloat(itm.itemSaleAmt)).toFixed(2);

              result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
              result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
            }
          }
        });
        _.forEach(result.betweenDate,function(itm,i){
          if(itm.totalPurchaseQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalPurchaseAmount).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalPurchaseAmount).toFixed(2);
            result.items.push(itm);
            // }
            // else
            // {
            //   result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            //   result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

            //   result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
            //   result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
            // }
          }
          else if(itm.totalBalanceQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(0).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(0).toFixed(3);
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
            }
            itm.varianceQty=parseFloat(0).toFixed(3);
            itm.varianceInPercent=parseFloat(0).toFixed(2);
            itm.physicalQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            itm.physicalAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalIntermediateQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalIntermediateQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalIntermediateAmt).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
            result.items.push(itm);
          }
          else if(itm.totalSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalSaleAmount).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
            // }
            // else
            // {
            //   result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
            //   result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);

            //   result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)- parseFloat(itm.totalSaleQty)).toFixed(3);
            //   result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount) -parseFloat(itm.totalSaleAmount)).toFixed(2);
            // }
          }
          else if(itm.totalWastageQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalWastageQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalWastageAmt).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            itm.wastageQty=parseFloat(itm.totalWastageQty).toFixed(3);
            itm.wastageAmt=parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
            //}
            // else
            // {
            //   result.items[itmIndex].wastageQty=parseFloat(parseFloat(itm.totalWastageQty)).toFixed(3);
            //   result.items[itmIndex].wastageAmt=parseFloat(parseFloat(itm.totalWastageAmt)).toFixed(2);

            //   result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
            //   result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
            // }
          }
          else if(itm.totalTransferQty_toStore!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.consumptionQty==undefined){
              itm.consumptionQty=parseFloat(0).toFixed(3);
              itm.consumptionAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.purchaseQty=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
            itm.purchaseAmount=parseFloat(itm.totalTransferAmount_toStore).toFixed(2);
            result.items.push(itm);
            //}
            // else
            // {
            //   result.items[itmIndex].purchaseQty=parseFloat(parseFloat(result.items[itmIndex].purchaseQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            //   result.items[itmIndex].purchaseAmount=parseFloat(parseFloat(result.items[itmIndex].purchaseAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);

            //   result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
            //   result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalTransferAmount_toStore)).toFixed(2);
            // }
          }
          else if(itm.totalTransferQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.totalTransferQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.totalTransferAmount).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
            result.items.push(itm);
            //}
            // else
            // {
            //   result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
            //   result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);

            //   result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
            //   result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
            // }
          }
          else if(itm.itemSaleQty!=undefined){
            var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
            //if(itmIndex<0){
            if(itm.openingQty==undefined){
              itm.openingQty=parseFloat(0).toFixed(3);
              itm.closingQty=parseFloat(-itm.itemSaleQty).toFixed(3);
              itm.openingAmt=parseFloat(0).toFixed(3);
              itm.closingAmount=parseFloat(-itm.itemSaleAmt).toFixed(3);
              itm.totalBalanceQty=parseFloat(0).toFixed(3);
              itm.physicalQty="NA";
              itm.varianceQty="NA";
              itm.varianceInPercent="NA";
              itm.openingDate=new Date(startDate);
              itm.closingDate=new Date(endDate);
            }
            if(itm.purchaseQty==undefined){
              itm.purchaseQty=parseFloat(0).toFixed(3);
              itm.purchaseAmount=parseFloat(0).toFixed(3);
            }
            if(itm.wastageQty==undefined){
              itm.wastageQty=parseFloat(0).toFixed(3);
            }
            itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
            itm.UnitName=itm.baseUnit;
            result.items.push(itm);
            //}
            // else
            // {
            //   result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
            //   result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);

            //   result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)-parseFloat(itm.itemSaleQty)).toFixed(3);
            //   result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)-parseFloat(itm.itemSaleAmt)).toFixed(2);
            // }
          }
        });
        return result;
      };

      function getClosingQtyPreviousDate(items,pdate,item){
        var cQTy=0;
        var index=_.findIndex(items,{date:pdate});
        if(index>=0){
          var itmIndex=_.findIndex(items[index].items,{itemId:item.itemId});
          if(itmIndex>=0){
            if(items[index].items[itmIndex].physicalQty!="NA"){
              cQTy=parseFloat(items[index].items[itmIndex].closingQty).toFixed(3);
            }
            else
            {
              cQTy=parseFloat(items[index].items[itmIndex].physicalQty).toFixed(3);
            }
          }
        }
        return cQTy;
      };
      function getPreviousDate_Data(items,pdate){
        var data=[];
        var index=_.findIndex(items,{date:pdate});
        if(index>=0){
          var d=angular.copy(items[index].items);
          _.forEach(d,function(itm,i){
            itm.openingQty=parseFloat(itm.closingQty).toFixed(3);
            itm.purchaseQty=parseFloat(0).toFixed(3);
            itm.consumptionQty=parseFloat(0).toFixed(3);
            itm.wastageQty=parseFloat(0).toFixed(3);
            itm.physicalQty="NA";//parseFloat(0).toFixed(3);
            itm.varianceQty="NA";//parseFloat(0).toFixed(3);
            itm.varianceInPercent="NA";//parseFloat(0).toFixed(2);
            data.push(itm);
          });
        }
        return data;
      };
      function generateDateWiseVarianceReport(items,days){
        //items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
        items.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        _.forEach(days,function(d,i){
          d.items=[];
          var pdate=new Date(d.date);
          pdate.setDate(pdate.getDate()-1);
          d.items=getPreviousDate_Data(days,pdate);
          _.forEachRight(items,function(itm,ii){
            var cDate=new Date(itm.created);
            cDate.setHours(0,0,0,0);
            if(new Date(d.date)>=new Date(cDate)){
              var index=_.findIndex(d.items,{itemId:itm.itemId});
              if(index<0){
                if(i==0){
                  //itm.openingQty=parseFloat(itm.closingQty).toFixed(3);
                  itm.openingQty=parseFloat(itm.openingQty).toFixed(3);
                }
                else
                {
                  itm.openingQty=parseFloat(getClosingQtyPreviousDate(days,pdate,itm)).toFixed(3);
                  itm.closingQty=parseFloat(parseFloat(itm.openingQty)+parseFloat(itm.purchaseQty)-parseFloat(itm.wastageQty)-parseFloat(itm.consumptionQty)).toFixed(3);
                }
                d.items.push(itm);
                items.splice(ii,1);
              }
              else
              {
                d.items[index].openingQty=parseFloat(parseFloat(d.items[index].openingQty)+parseFloat(itm.openingQty)).toFixed(3);
                d.items[index].purchaseQty=parseFloat(parseFloat(d.items[index].purchaseQty)+parseFloat(itm.purchaseQty)).toFixed(3);
                d.items[index].consumptionQty=parseFloat(parseFloat(d.items[index].consumptionQty)+parseFloat(itm.consumptionQty)).toFixed(3);
                d.items[index].wastageQty=parseFloat(parseFloat(d.items[index].wastageQty)+parseFloat(itm.wastageQty)).toFixed(3);
                d.items[index].closingQty=parseFloat(parseFloat(d.items[index].closingQty)+parseFloat(itm.closingQty)).toFixed(3);
                if(itm.physicalQty!="NA"){
                  if(d.items[index].physicalQty!="NA"){
                    d.items[index].physicalQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(itm.physicalQty)).toFixed(3);
                  }
                  else
                  {
                    d.items[index].physicalQty=parseFloat(parseFloat(itm.physicalQty)).toFixed(3);
                  }

                  if(d.items[index].physicalQty!="NA"){
                    var dNO=parseFloat(i-1);

                    d.items[index].openingQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                    d.items[index].closingQty=parseFloat(parseFloat(d.items[index].physicalQty)+parseFloat(d.items[index].purchaseQty)-parseFloat(d.items[index].wastageQty)-parseFloat(d.items[index].consumptionQty)).toFixed(3);
                    if(dNO>=0){
                      days[dNO].items[index].physicalQty=parseFloat(d.items[index].physicalQty).toFixed(3);
                      days[dNO].items[index].varianceQty=parseFloat(parseFloat(days[dNO].items[index].closingQty)-parseFloat(days[dNO].items[index].physicalQty)).toFixed(3);
                      days[dNO].items[index].varianceInPercent=parseFloat(parseFloat(parseFloat(days[dNO].items[index].varianceQty)/parseFloat(days[dNO].items[index].closingQty))*parseFloat(100)).toFixed(2);
                    }
                    d.items[index].physicalQty="NA";//parseFloat(0).toFixed(3);
                  }
                }
                items.splice(ii,1);
              }
            }
          });
        });
        return days;
      };

      function formatDataDateWise(result){
        var items=[];
        var sortedArray=[]
        // _.forEachRight(result.items,function(r,i){
        //   if(r.totalBalanceQty!=0){
        //     result.items.splice(i,1);
        //   }
        // });
        sortedArray=angular.copy(result.items);
        sortedArray.sort(function(a,b){
          return new Date(a.created).getTime()-new Date(b.created).getTime();
        });

        _.forEach(sortedArray, function(itm,i){
          var d=new Date(itm.created);
          d.setHours(0,0,0,0);
          var dINdex=_.findIndex(items,{date:d});
          if(dINdex<0){
            var dObj={date:d};
            dObj.items=[];
            dObj.items.push(itm)
            items.push(dObj);
          }
          else
          {
            var itemINdex=_.findIndex(items[dINdex].items,{itemId:itm.itemId});
            if(itemINdex<0){
              items[dINdex].items.push(itm);
            }
            else
            {
              items[dINdex].items[itemINdex].openingQty +=itm.closingQty==undefined?0:itm.closingQty;
              items[dINdex].items[itemINdex].totalPurchaseQty += (itm.totalPurchaseQty==undefined) ? 0 : itm.totalPurchaseQty;
              items[dINdex].items[itemINdex].totalSaleQty += (itm.totalSaleQty==undefined) ? 0 : itm.totalSaleQty;
              items[dINdex].items[itemINdex].totalTransferQty += (itm.totalTransferQty==undefined) ? 0 : itm.totalTransferQty;
              items[dINdex].items[itemINdex].totalWastageQty += (itm.totalWastageQty==undefined) ? 0 : itm.totalWastageQty;
              items[dINdex].items[itemINdex].closingQty = ((items[dINdex].items[itemINdex].openingQty+items[dINdex].items[itemINdex].totalPurchaseQty+items[dINdex].items[itemINdex].totalTransferQty)-(items[dINdex].items[itemINdex].totalSaleQty+items[dINdex].items[itemINdex].totalWastageQty));
            }
          }
        });

        items.sort(function(a,b){
          return new Date(a.date).getTime()-new Date(b.date).getTime();
        });

        // array.sort(function(a,b){
        //  var c = new Date(a.date);
        //  var d = new Date(b.date);
        //  return c-d;
        // });
        return items;
      };

      function formatDataDatewise_Date(result){
        var items=[];
        _.forEach(result,function(r,i){
          if(i!=0){
            var aboveItem=angular.copy(result[i-1].items);
            var belowItem=angular.copy(result[i].items);
            _.forEach(result[i].items, function(itm,ii){
              itm.openingQty =itm.closingQty==undefined?0:itm.closingQty;
              itm.totalPurchaseQty = (itm.totalPurchaseQty==undefined) ? 0 : itm.totalPurchaseQty;
              itm.totalSaleQty = (itm.totalSaleQty==undefined) ? 0 : itm.totalSaleQty;
              itm.totalTransferQty = (itm.totalTransferQty==undefined) ? 0 : itm.totalTransferQty;
              itm.totalWastageQty = (itm.totalWastageQty==undefined) ? 0 : itm.totalWastageQty;
              //itm.totalPhysicalQty = (itm.totalWastageQty==undefined) ? 0 : itm.totalWastageQty;
              itm.closingQty = ((itm.openingQty+itm.totalPurchaseQty+itm.totalTransferQty)-(itm.totalSaleQty+itm.totalWastageQty));
            });

            _.forEach(aboveItem,function(a,j){
              var index=_.findIndex(belowItem,{itemId:a.itemId});
              if(index<0){
                a.openingQty=a.closingQty;
                a.totalTransferQty=0;
                a.totalWastageQty=0;
                a.totalPurchaseQty=0;
                a.totalSaleQty=0;
                result[i].items.push(a);
              }
              else
              {
                if(belowItem[index].closingQty==undefined){
                  belowItem[index].closingQty=aboveItem[j].openingQty;
                }
                //aboveItem[j].openingAmt: 960
                aboveItem[j].openingQty=belowItem[index].closingQty;
                //aboveItem[j].totalPurchaseAmount=
                aboveItem[j].totalPurchaseQty= (belowItem[index].totalPurchaseQty==undefined) ? 0 : belowItem[index].totalPurchaseQty;
                //aboveItem[j].totalSaleAmount= 0
                aboveItem[j].totalSaleQty= (belowItem[index].totalSaleQty==undefined) ? 0 : belowItem[index].totalSaleQty;
                //aboveItem[j].totalTransferAmount= 0
                aboveItem[j].totalTransferQty= (belowItem[index].totalTransferQty==undefined) ? 0 : belowItem[index].totalTransferQty;
                //aboveItem[j].totalWastageAmt= 0
                aboveItem[j].totalWastageQty= (belowItem[index].totalWastageQty==undefined) ? 0 : belowItem[index].totalWastageQty;
                //aboveItem[j].closingAmount= null
                aboveItem[j].closingQty= ((aboveItem[j].openingQty+aboveItem[j].totalPurchaseQty+aboveItem[j].totalTransferQty)-(aboveItem[j].totalSaleQty+aboveItem[j].totalWastageQty));

                angular.copy(aboveItem[j],result[i].items[index]);
              }
            });

          }
        });
        return result;
      };


      //------------------------------------------Reports----------------------------------------------------------
      $scope.exportToPdf_consolidated=function(){
        var table=document.getElementById('consolidatedReport').innerHTML;
        var printer = window.open('', '', 'width=600,height=600');
        printer.document.open("text/html");
        printer.document.write(table);
        printer.document.close();
        printer.focus();
        printer.print();
      };
      $scope.exportToExcel_consolidated=function(){
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('consolidatedReport').innerHTML));
      };
      $scope.exportToPdf_dateWise=function(){
        var table=document.getElementById('dateWiseReport').innerHTML;
        var printer = window.open('', '', 'width=600,height=600');
        printer.document.open("text/html");
        printer.document.write(table);
        printer.document.close();
        printer.focus();
        printer.print();
      };
      $scope.exportToExcel_dateWise=function(){
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('dateWiseReport').innerHTML));
      };
    }]);
