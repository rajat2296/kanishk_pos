'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('varianceReportsAggregated', {
        url: '/varianceReportsAggregated',
        templateUrl: 'app/stock/stockReports/varianceReportsAggregated/varianceReportsAggregated.html',
        controller: 'VarianceReportsCtrlAggr'
      });
  });