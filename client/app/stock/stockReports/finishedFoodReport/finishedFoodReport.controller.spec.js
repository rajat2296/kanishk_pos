'use strict';

describe('Controller: FinishedFoodReportCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var FinishedFoodReportCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FinishedFoodReportCtrl = $controller('FinishedFoodReportCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
