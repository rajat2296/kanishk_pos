'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('finishedFoodReport', {
        url: '/finishedFoodReport',
        templateUrl: 'app/stock/stockReports/finishedFoodReport/finishedFoodReport.html',
        controller: 'FinishedFoodReportCtrl'
      });
  });