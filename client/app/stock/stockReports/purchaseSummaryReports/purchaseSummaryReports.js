'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('purchaseSummaryReports', {
        url: '/purchaseSummaryReports',
        templateUrl: 'app/stock/stockReports/purchaseSummaryReports/purchaseSummaryReports.html',
        controller: 'PurchaseSummaryReportsCtrl'
      });
  });