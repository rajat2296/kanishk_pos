'use strict';

angular.module('posistApp')
  .controller('PurchaseSummaryReportsCtrl',['$q','$resource','$scope','$filter','growl','store','stockItem','currentUser','$rootScope','$state','$modal','StockResource','localStorageService','deployment','Utils','intercom','stockUnit',
  		function ($q,$resource,$scope,$filter,growl,store,stockItem,currentUser,$rootScope,$state,$modal,StockResource,localStorageService,deployment,Utils,intercom,stockUnit) {

    currentUser.deployment_id=localStorageService.get('deployment_id');
  	$scope.stores=[];
    $scope.user=currentUser;

  	function getStores_ByDeployment(){
      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };
    function getStockItems(){
      return stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
      // .$promise.then(function (result){
      //     $scope.stockitems=result;
      //     $scope.binditems_ItemForm(1);
      // });
    };
     function getStockUnits()
    {
      var deferred = $q.defer();
      stockUnit.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(units) {
        console.log('units',units);
        deferred.resolve(units);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }
    var allPromise=$q.all([
              getStores_ByDeployment(), getStockItems(), getStockUnits()
	]);
    allPromise.then(function (value){
        $scope.stores = value[0];
        $scope.purchaseSummaryForm.stores=$scope.stores;
        $scope.stockitems=value[1];
        $scope.stockUnits = value[2];
        console.log($scope.stockitems);
	});

	$scope.purchaseSummaryForm={
		fromDate:new Date(),//getYesterDayDate(),
  		toDate:new Date(),
  		maxDate:new Date()
	};
	function getYesterDayDate() {
  		var d=new Date();
  		d.setDate(d.getDate()-1);
  		return d;
  	};

  	function parseDate(str){
	    var d=$filter('date')(str, 'dd-MMM-yyyy');
	    return d;
	};
	$scope.checkDateGreater= function(date){
  		var d=new Date(date);
  		var dd=new Date($scope.purchaseSummaryForm.toDate);
  		if(d.getTime()>dd.getTime()){
  			growl.success('Greater than toDate', {ttl: 3000});
  			$scope.purchaseSummaryForm.fromDate=new Date();//getYesterDayDate()
  		}
  		else
  		{
  			//growl.success('Lesser than toDate', {ttl: 3000});
  		}
  	};
  	$scope.checkDateLesser= function(date){
  		var d=new Date(date);
  		var dd=new Date($scope.purchaseSummaryForm.fromDate);
  		if(d.getTime()<dd.getTime()){
  			growl.success('Can not be Lesser than fromDate', {ttl: 3000});
  			$scope.purchaseSummaryForm.toDate= new Date();
  		}
  		else
  		{
  			//growl.success('Greater than fromDate', {ttl: 3000});
  		}
  	};
    function compare(a,b) {
      if (a.itemName < b.itemName)
        return -1;
      if (a.itemName > b.itemName)
        return 1;
      return 0;
    };
  	$scope.bindItems_PurchaseSummary= function(store){
  		$scope.purchaseSummaryForm.availableItems=[];
  		_.forEach(store.category,function(c,i){
  				_.forEach(c.item, function(itm,ii){
  					var index=_.findIndex($scope.purchaseSummaryForm.availableItems,{_id:itm._id});
  					if(index<0){
  						$scope.purchaseSummaryForm.availableItems.push(itm);
  					}
  					else
  					{

  					}
  				});
  			});
      $scope.purchaseSummaryForm.availableItems.sort(compare);
  	};
  	$scope.startSearch= function(){
      intercom.registerEvent('StockReports');
  		$scope.purchaseSummaryForm.availableRecords=[];
  		    var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseSummaryForm.fromDate))));
        console.log('fromDate',fromDate);
        //var toDate = new Date($scope.purchaseConsumptionForm.toDate);
        var toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseSummaryForm.toDate))));
        console.log('toDate',toDate);
      /*var fromDate=new Date($scope.purchaseSummaryForm.fromDate);
          fromDate.setHours(0, 0, 0, 0);
          var toDate=new Date($scope.purchaseSummaryForm.toDate);
          toDate.setHours(0, 0, 0, 0);
      */    var strId="";
          if($scope.purchaseSummaryForm.store!=undefined){
          	strId=$scope.purchaseSummaryForm.store._id;
          };

          var req={"fromDate":fromDate,"toDate":toDate,"transactionType":"1","_store._id":strId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};
          StockResource.getBasicEntryReportsForPurchaseSummary(req, function (result){
            console.log('result :- ',result);
          	var rpt=formatDataForReport(result);
            console.log('rpt',rpt);
          	var cData=calculateItemWiseSummary(rpt);
            cData=convertItemInPrefferedUnit(cData);
          	if($scope.purchaseSummaryForm.item!=undefined){
          		if($scope.purchaseSummaryForm.item._id!=undefined){
          			$scope.purchaseSummaryForm.reportData=formatForItemWise(cData,$scope.purchaseSummaryForm.item._id);
          		}
          	}
          	else{
          		$scope.purchaseSummaryForm.reportData=cData;
          	}
            console.log($scope.purchaseSummaryForm.reportData);
          });
  	};
  	function formatForItemWise(result,id){
  		_.forEachRight(result,function(r,i){
  			if(r._id!=id){
  				result.splice(i,1);
  			}
  		});
  		result.grandTotal.qty=parseFloat(result[0].qty).toFixed(3);
		result.grandTotal.amount=parseFloat(result[0].amount).toFixed(2);
		result.grandTotal.discount=parseFloat(result[0].discount).toFixed(2);
		result.grandTotal.serviceCharge=parseFloat(result[0].serviceCharge).toFixed(2);
		result.grandTotal.vat=parseFloat(result[0].vat).toFixed(2);
		result.grandTotal.cartage=parseFloat(result[0].cartage).toFixed(2);
		result.grandTotal.total=parseFloat(result[0].total).toFixed(2);

		return result;
  	};
  	function calculateItemWiseSummary(result){
  		var itemss=[];
  		itemss.grandTotal={};
  		var gtQ=0,gtA=0,gtD=0,gtSC=0,gtV=0,gtC=0,gtT=0,gtN=0;
      console.log('result in ciws',result);
  		_.forEach(result, function(r,i){
  			console.log('r',r)
        var itm={};
  			itm._id=r._id;
  			itm.itemName=r.itemName;
        
  			//itm.qty=r.qty;
  			/*_.forEach(r.calculateInUnits,function(u,ii){
          if(u!=null){
            //if(u.type=="selectedUnit"){
            if(u.type=="baseUnit"){
              itm.unitName=u.unitName;
              itm.amount=parseFloat(u.subTotal);
              itm.qty=parseFloat(u.baseQty);
              //itm.vat=parseFloat(u.vatAmt);
              itm.vat=parseFloat(u.addedAmt);
              itm.discount=r.discount;
              itm.serviceCharge=r.serviceCharge;
              itm.cartage=r.cartage;
              itm.total=parseFloat(u.totalAmount);
            }
          }
  			});*/

  			var index=_.findIndex(itemss,{_id:r._id});
  			if(index<0){
          itm.qty=parseFloat(r.qty).toFixed(2);
          itm.amount=parseFloat(r.subTotal).toFixed(2);
          itm.vat=parseFloat(r.vat).toFixed(2);
          itm.discount=parseFloat(r.discount).toFixed(2);
          //itm.serviceCharge=parseFloat(r.serviceCharge).toFixed(2);
          itm.cartage=parseFloat(r.cartage).toFixed(2);
          itm.netAdditionalCharge=parseFloat(r.netAdditionalCharge).toFixed(2);
          itm.total=parseFloat(parseFloat(r.subTotal)+parseFloat(r.vat)-parseFloat(r.discount)+parseFloat(r.cartage) + parseFloat(r.netAdditionalCharge)).toFixed(2);
  				itemss.push(itm);
  			}
  			else
  			{
          itemss[index].qty=parseFloat(parseFloat(itemss[index].qty)+parseFloat(r.qty)).toFixed(2);
  				itemss[index].amount=parseFloat(parseFloat(itemss[index].amount)+parseFloat(r.subTotal)).toFixed(2);
  				itemss[index].vat=parseFloat(parseFloat(itemss[index].vat)+parseFloat(r.vat)).toFixed(2);
  				itemss[index].discount=parseFloat(parseFloat(itemss[index].discount)+parseFloat(r.discount)).toFixed(2);
  				//itemss[index].serviceCharge=parseFloat(itemss[index].serviceCharge)+parseFloat(r.serviceCharge);
  				itemss[index].cartage=parseFloat(parseFloat(itemss[index].cartage)+parseFloat(r.cartage)).toFixed(2);
          itemss[index].netAdditionalCharge=parseFloat(parseFloat(itemss[index].netAdditionalCharge)+parseFloat(r.netAdditionalCharge)).toFixed(2);
  				itemss[index].total=parseFloat(parseFloat(itemss[index].total)+parseFloat(r.subTotal)+parseFloat(r.vat)-parseFloat(r.discount)+parseFloat(r.cartage) +parseFloat(r.netAdditionalCharge)).toFixed(2);
  			}
  			gtQ=parseFloat(parseFloat(gtQ)+parseFloat(r.qty)).toFixed(3);
  			gtA=parseFloat(parseFloat(gtA)+parseFloat(r.subTotal)).toFixed(2);
  			gtD=parseFloat(parseFloat(gtD)+parseFloat(r.discount)).toFixed(2);
  			//gtSC=parseFloat(parseFloat(gtSC)+parseFloat(r.serviceCharge)).toFixed(2);
  			gtV=parseFloat(parseFloat(gtV)+parseFloat(r.vat)).toFixed(2);
  			gtC=parseFloat(parseFloat(gtC)+parseFloat(r.cartage)).toFixed(2);
        gtN=parseFloat(parseFloat(gtN)+parseFloat(r.netAdditionalCharge)).toFixed(2);
  			gtT=parseFloat(parseFloat(gtT)+parseFloat(r.subTotal)+parseFloat(r.vat)-parseFloat(r.discount)+parseFloat(r.cartage) +parseFloat(r.netAdditionalCharge)).toFixed(2);
  		});
		itemss.grandTotal.qty=parseFloat(gtQ).toFixed(3);
		itemss.grandTotal.amount=parseFloat(gtA).toFixed(2);
		itemss.grandTotal.discount=parseFloat(gtD).toFixed(2);
		//itemss.grandTotal.serviceCharge=parseFloat(gtSC).toFixed(2);
		itemss.grandTotal.vat=parseFloat(gtV).toFixed(2);
		itemss.grandTotal.cartage=parseFloat(gtC).toFixed(2);
    itemss.grandTotal.netAdditionalCharge=parseFloat(gtN).toFixed(2);
		itemss.grandTotal.total=parseFloat(gtT).toFixed(2);
  		return itemss;
  	};
  	function formatDataForReport(result){
  		var itemss=[];
      
  		_.forEach(result,function(r,i){
        var billTotal=0;
        var  dis=0;
        var disAmount =0;
        var perDis =0;
        var vs=0;
        var n =0;
        var netAdditionalCharge = 0;
        
        _.forEach(r._store.vendor.category, function(c,ii){
          _.forEach(c.items,function(itm,iii){
            //console.log('itm',itm);
            billTotal=parseFloat(parseFloat(billTotal)+parseFloat(itm.subTotal)).toFixed(3);
            n++;
            
          });
        });
  			_.forEach(r._store.vendor.category, function(c,ii){
  				if(r.discount!=undefined && r.discount!=0)
          {
            if(r.discountType =='percent')
              dis= parseFloat(billTotal*0.01*r.discount).toFixed(2);
            else
              dis= parseFloat(r.discount).toFixed(2);
          }


            _.forEach(c.items,function(itm,iii){
              //console.log('itm',angular.copy(itm));
              netAdditionalCharge = 0;
            //billTotal=parseFloat(parseFloat(billTotal)+parseFloat(itm.subTotal)).toFixed(3);
              perDis = parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(billTotal).toFixed(2)).toFixed(2);
              disAmount=parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2);
              itm.discount=parseFloat(perDis).toFixed(2);
              if(r.cartage)
              itm.cartage=parseFloat(parseFloat(r.cartage)/parseFloat(n)).toFixed(2);
              else
              itm.cartage=parseFloat(0).toFixed(2);  
              if(itm.vatPercent)
                      {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',itm.vatPercent);
                        vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                        itm.vat=parseFloat(vs).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        //disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        //disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        //vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                      }
                      else{
                        itm.vat=parseFloat(0).toFixed(2);
                      }
                      if (r.charges != undefined) {
                        console.log('disAmount',disAmount);
                                 _.forEach(r.charges , function(charge) {
                                    if(charge.operationType == 'additive')
                                    {
                                        if(charge.type == 'percent')
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(disAmount * 0.01 * charge.chargeValue)).toFixed(2)
                                        else 
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue)).toFixed(2)
                                    }
                                    else
                                    {
                                      console.log('else of charges')
                                        if(charge.type == 'percent')
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(disAmount * 0.01 * charge.chargeValue)).toFixed(2)
                                        else 
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue)).toFixed(2)
                                    }
                                 });    
                                itm.netAdditionalCharge = parseFloat(netAdditionalCharge / n).toFixed(2);
                            } else {
                                itm.netAdditionalCharge = parseFloat(0).toFixed(2);
                            }                  
            //itm.discountAmount=0;
            console.log('item pushing',itm);
  					itemss.push(itm);
  				});
  			});
  		});
      console.log('itemss',itemss);
  		return itemss;
  	};

    function convertItemInPrefferedUnit(result){
        _.forEach(result,function(itm,i){
          var indeex=_.findIndex($scope.stockitems,{_id:itm._id});
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
            if(!itm.itemCode)
              itm.itemCode = item.itemCode;
            if(item.preferedUnit!=undefined){
              _.forEach(item.units,function(u,i){
                if(u.baseUnit == undefined || u.conversionFactor == undefined || u.baseConversionFactor == undefined)
                {
                  var ind = _.findIndex($scope.stockUnits, {_id: u._id});
                  if(ind >= 0)
                  {
                  u.baseUnit = $scope.stockUnits[ind].baseUnit;
                  u.conversionFactor = $scope.stockUnits[ind].conversionFactor;
                  u.baseConversionFactor = $scope.stockUnits[ind].baseConversionFactor;
                  }  
                }
                if(u._id==item.preferedUnit){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  console.log('pconFac',pconFac);
                  itm.unitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                        itm.qty=parseFloat(parseFloat(itm.qty)/parseFloat(conversionFactor)).toFixed(3);
                      }
                      else if(pconFac<conversionFactor){
                        itm.qty=parseFloat(parseFloat(itm.qty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                        itm.qty=parseFloat(parseFloat(itm.qty)).toFixed(3);
                      }
                      else if(pconFac<conversionFactor){
                        itm.qty=parseFloat(parseFloat(itm.qty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        });
        return result;
      };
    //-----------------------------------------------Reports----------------------------------------------

    $scope.exportToPdf_purchaseSummary=function(){
      var table=document.getElementById('purchaseSummaryReport').innerHTML;
       var printer = window.open('', '', 'width=600,height=600');
       printer.document.open("text/html");
       printer.document.write(table);
       printer.document.close();
       printer.focus();
       printer.print();
    };
    $scope.exportToExcel_purchaseSummary=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('purchaseSummaryReport').innerHTML));
    };
  }]);
