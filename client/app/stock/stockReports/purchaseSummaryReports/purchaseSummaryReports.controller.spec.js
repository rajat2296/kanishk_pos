'use strict';

describe('Controller: PurchaseSummaryReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var PurchaseSummaryReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PurchaseSummaryReportsCtrl = $controller('PurchaseSummaryReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
