'use strict';

angular.module('posistApp')
    .controller('PurchaseDetailReportsCtrl', ['$q', '$resource', '$scope', '$filter', 'growl', 'store', 'currentUser', '$rootScope', '$state', '$modal', 'StockResource', 'localStorageService', 'deployment', 'Utils', 'property', 'intercom', 'stockItem',
        function($q, $resource, $scope, $filter, growl, store, currentUser, $rootScope, $state, $modal, StockResource, localStorageService, deployment, Utils, property, intercom, stockItem) {

            currentUser.deployment_id = localStorageService.get('deployment_id');
            $scope.stores = [];
            $scope.user = currentUser;

            function getStores_ByDeployment() {
                return store.get({ tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id });
            };

            function getItems() {
                return stockItem.get({
                    tenant_id: localStorageService.get('tenant_id'),
                    deployment_id: localStorageService.get('deployment_id')
                });
            }
            var allPromise = $q.all([
                getStores_ByDeployment(),
                getItems()
            ]);
            allPromise.then(function(value) {
                $scope.stores = value[0];
                $scope.items = value[1];
                $scope.purchaseDetailForm.stores = $scope.stores;
            });
            $scope.resetSerialNumber = false

            $scope.hidePricing = false
                // property.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
                //      if(result.length==0)
                //      {   
                //         $scope.resetSerialNumber=true
                //      }
                //      else
                //      {
                //         $scope.settings=result[0];
                //         if($scope.settings.resetSerialNumber==undefined)
                //            $scope.resetSerialNumber=true
                //          else
                //            $scope.resetSerialNumber=$scope.settings.resetSerialNumber
                //            console.log($scope.resetSerialNumber);    
                //      }

            //    });
            $scope.purchaseDetailForm = {
                fromDate: new Date(), //getYesterDayDate(),
                toDate: new Date(),
                maxDate: new Date()
            };

            function getYesterDayDate() {
                var d = new Date();
                d.setDate(d.getDate() - 1);
                return d;
            };

            function parseDate(str) {
                var d = $filter('date')(str, 'dd-MMM-yyyy');
                return d;
            };
            $scope.checkDateGreater = function(date) {
                var d = new Date(date);
                var dd = new Date($scope.purchaseDetailForm.toDate);
                if (d.getTime() > dd.getTime()) {
                    growl.success('Greater than toDate', { ttl: 3000 });
                    $scope.purchaseDetailForm.fromDate = new Date(); //getYesterDayDate()
                } else {
                    //growl.success('Lesser than toDate', {ttl: 3000});
                }
            };
            $scope.checkDateLesser = function(date) {
                var d = new Date(date);
                var dd = new Date($scope.purchaseDetailForm.fromDate);
                if (d.getTime() < dd.getTime()) {
                    growl.success('Can not be Lesser than fromDate', { ttl: 3000 });
                    $scope.purchaseDetailForm.toDate = new Date();
                } else {
                    //growl.success('Greater than fromDate', {ttl: 3000});
                }
            };

            function compare(a, b) {
                if (a.itemName < b.itemName)
                    return -1;
                if (a.itemName > b.itemName)
                    return 1;
                return 0;
            };

            $scope.bindItems_PurchaseDetail = function(store) {
                $scope.purchaseDetailForm.availableItems = [];
                _.forEach(store.category, function(c, i) {
                    _.forEach(c.item, function(itm, ii) {
                        var index = _.findIndex($scope.purchaseDetailForm.availableItems, { _id: itm._id });
                        if (index < 0) {
                            $scope.purchaseDetailForm.availableItems.push(itm);
                        }
                    });
                });
                $scope.purchaseDetailForm.availableItems.sort(compare);
            };
            $scope.startSearch = function() {
                intercom.registerEvent('StockReports');
                $scope.purchaseDetailForm.availableRecords = [];
                var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseDetailForm.fromDate))));
                console.log('fromDate', fromDate);
                //var toDate = new Date($scope.purchaseConsumptionForm.toDate);
                var toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseDetailForm.toDate))));
                console.log('toDate', toDate);
                /*var fromDate=new Date($scope.purchaseDetailForm.fromDate);
                    fromDate.setHours(0, 0, 0, 0);
                    var toDate=new Date($scope.purchaseDetailForm.toDate);
                    toDate.setHours(0, 0, 0, 0);
                */
                var strId = "";
                if ($scope.purchaseDetailForm.store != undefined) {
                    strId = $scope.purchaseDetailForm.store._id;
                };

                var req = { "fromDate": fromDate, "toDate": toDate, "transactionType": "1", "_store._id": strId, "tenant_id": currentUser.tenant_id, "deployment_id": currentUser.deployment_id };
                StockResource.getBasicEntryReportsForPurchaseDetail(req, function(result) {
                    console.log(angular.copy(result));
                    var rpt = formatDataForReport(result);
                    var cData = calculateItemWiseSummary(rpt);
                    if ($scope.purchaseDetailForm.item != undefined) {
                        if ($scope.purchaseDetailForm.item._id != undefined) {
                            $scope.purchaseDetailForm.reportData = formatForItemWise(cData, $scope.purchaseDetailForm.item._id);
                        }
                    } else {
                        $scope.purchaseDetailForm.reportData = cData;
                    }
                    console.log($scope.purchaseDetailForm.reportData);
                });
            };

            function formatForItemWise(result, id) {
                console.log('result', result);
                _.forEachRight(result, function(r, i) {
                    var index = _.findIndex($scope.items, function(item) {
                        return item._id == id;
                    });
                    if (!r.itemCode)
                        r.itemCode = $scope.items[index].itemCode;
                    if (r._id != id) {
                        result.splice(i, 1);
                    }
                });
                result.grandTotal.qty = parseFloat(result[0].qty).toFixed(3);
                result.grandTotal.amount = parseFloat(result[0].amount).toFixed(2);
                result.grandTotal.discount = parseFloat(result[0].discount).toFixed(2);
                result.grandTotal.serviceCharge = parseFloat(result[0].serviceCharge).toFixed(2);
                result.grandTotal.vat = parseFloat(result[0].vat).toFixed(2);
                result.grandTotal.cartage = parseFloat(result[0].cartage).toFixed(2);
                result.grandTotal.total = parseFloat(result[0].total).toFixed(2);

                return result;
            };

            function calculateItemWiseSummary(result) {
                console.log('result', result);
                console.log('items', $scope.items);
                result.grandTotal = {};
                var gtQ = 0,
                    gtA = 0,
                    gtD = 0,
                    gtSC = 0,
                    gtV = 0,
                    gtC = 0,
                    gtN = 0,
                    gtT = 0;
                _.forEach(result, function(r, i) {
                    var index = _.findIndex($scope.items, function(item) {
                        return item._id == r._id;
                    });
                    if (index > 0) {
                        if (!r.itemCode && r.itemCode != '-')
                            r.itemCode = $scope.items[index].itemCode;
                        else
                            r.itemCode = '';
                    }
                    var itmQ = 0,
                        itmA = 0,
                        itmD = 0,
                        itmSC = 0,
                        itmV = 0,
                        itmC = 0,
                        itmN = 0,
                        itmT = 0;
                    _.forEach(r.bills, function(b, ii) {
                        itmQ = parseFloat(parseFloat(itmQ) + parseFloat(b.qty)).toFixed(3);
                        itmA = parseFloat(parseFloat(itmA) + parseFloat(b.amount)).toFixed(2);
                        itmD = parseFloat(parseFloat(itmD) + parseFloat(b.discount)).toFixed(2);
                        itmSC = parseFloat(parseFloat(itmSC) + parseFloat(b.serviceCharge)).toFixed(2);
                        itmV = parseFloat(parseFloat(itmV) + parseFloat(b.vat)).toFixed(2);
                        itmC = parseFloat(parseFloat(itmC) + parseFloat(b.cartage)).toFixed(2);
                        itmN = parseFloat(parseFloat(itmN) + parseFloat(b.netAdditionalCharge)).toFixed(2);
                        itmT = parseFloat(parseFloat(itmT) + parseFloat(b.total)).toFixed(2);
                    });
                    r.qty = parseFloat(itmQ).toFixed(3);
                    r.amount = parseFloat(itmA).toFixed(2);
                    r.discount = parseFloat(itmD).toFixed(2);
                    r.serviceCharge = parseFloat(itmSC).toFixed(2);
                    r.vat = parseFloat(itmV).toFixed(2);
                    r.cartage = parseFloat(itmC).toFixed(2);
                    r.netAdditionalCharge = parseFloat(itmN).toFixed(2);
                    r.total = parseFloat(itmT).toFixed(2);
                    gtQ = parseFloat(parseFloat(gtQ) + parseFloat(itmQ)).toFixed(3);
                    gtA = parseFloat(parseFloat(gtA) + parseFloat(itmA)).toFixed(2);
                    gtD = parseFloat(parseFloat(gtD) + parseFloat(itmD)).toFixed(2);
                    gtSC = parseFloat(parseFloat(gtSC) + parseFloat(itmSC)).toFixed(2);
                    gtV = parseFloat(parseFloat(gtV) + parseFloat(itmV)).toFixed(2);
                    gtC = parseFloat(parseFloat(gtC) + parseFloat(itmC)).toFixed(2);
                    gtN = parseFloat(parseFloat(gtN) + parseFloat(itmN)).toFixed(2);
                    gtT = parseFloat(parseFloat(gtT) + parseFloat(itmT)).toFixed(2);
                });
                result.grandTotal.qty = parseFloat(gtQ).toFixed(3);
                result.grandTotal.amount = parseFloat(gtA).toFixed(2);
                result.grandTotal.discount = parseFloat(gtD).toFixed(2);
                result.grandTotal.serviceCharge = parseFloat(gtSC).toFixed(2);
                result.grandTotal.vat = parseFloat(gtV).toFixed(2);
                result.grandTotal.cartage = parseFloat(gtC).toFixed(2);
                result.grandTotal.netAdditionalCharge = parseFloat(gtN).toFixed(2);
                result.grandTotal.total = parseFloat(gtT).toFixed(2);
                return result;
            };

            function formatDataForReport(result) {
                var itemss = [];
                console.log('result', result)
                _.forEach(result, function(r, i) {
                    _.forEach(r._store.vendor.category, function(c, ii) {
                        var tQ = 0,
                            t = 0,
                            d = 0,
                            vs = 0,
                            gt = 0,
                            p = 0,
                            cartage = 0;
                        _.forEach(c.items, function(itm, iii) {
                            var bill = {};
                            bill._id = r._id;
                            bill.billDate = r.created;
                            bill.invoiceNumber = r.invoiceNumber;
                            bill.batchNumber = r.batchNumber;
                            if (r.isPOEntry) {
                                if ($scope.resetSerialNumber)
                                    bill.billNo = "PO-" + r.daySerialNumber;
                                else
                                    bill.billNo = "PO-" + r.transactionNumber;

                            } else {
                                if ($scope.resetSerialNumber)
                                    bill.billNo = "SE-" + r.daySerialNumber;
                                else
                                    bill.billNo = "SE-" + r.transactionNumber;
                            }
                            //bill.qty=itm.qty;
                            var it = parseFloat(parseFloat(itm.qty) * parseFloat(itm.price)).toFixed(2);
                            itm.qty = parseFloat(itm.qty).toFixed(3);
                            itm.price = parseFloat(itm.price).toFixed(2);
                            tQ = parseFloat(tQ) + parseFloat(itm.qty);
                            t = parseFloat(parseFloat(t) + parseFloat(itm.qty) * parseFloat(itm.price)).toFixed(2);
                            itm.total = parseFloat(parseFloat(itm.qty) * parseFloat(itm.price)).toFixed(2);
                            var dis = 0,
                                disAmount = 0,
                                netAdditionalCharge = 0;
                            var length = c.items.length;

                            if (r.discount != undefined && r.discount != 0) {
                                if (r.discountType == "percent") {
                                    dis = parseFloat(it * 0.01 * r.discount).toFixed(2);
                                    disAmount = parseFloat(it) - parseFloat(dis);
                                    bill.discount = parseFloat(dis).toFixed(2);
                                } else {
                                    dis = parseFloat(r.discount).toFixed(2);
                                    disAmount = parseFloat(it) - parseFloat(dis);
                                    bill.discount = parseFloat(0).toFixed(2);
                                }
                            } else {
                                bill.discount = parseFloat(0).toFixed(2);
                                disAmount = parseFloat(it).toFixed(2);
                            }
                            if (itm.vatPercent != undefined) {
                                vs = parseFloat(parseFloat(itm.vatPercent * 0.01 * parseFloat(disAmount))).toFixed(2);
                            }
                            else
                            {
                                vs=parseFloat(0).toFixed(2);
                            }
                            if (r.cartage != undefined) {
                                bill.cartage = parseFloat(r.cartage / length).toFixed(2);
                            } else {
                                bill.cartage = parseFloat(0).toFixed(2);
                            }

                            if (r.charges != undefined) {
                                 _.forEach(r.charges , function(charge) {
                                    if(charge.operationType == 'additive')
                                    {
                                        if(charge.type == 'percent')
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(disAmount * 0.01 * charge.chargeValue)).toFixed(2)
                                        else 
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue)).toFixed(2)
                                    }
                                    else
                                    {
                                        if(charge.type == 'percent')
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(disAmount * 0.01 * charge.chargeValue)).toFixed(2)
                                        else 
                                            netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue)).toFixed(2)
                                    }
                                 });    
                                bill.netAdditionalCharge = parseFloat(netAdditionalCharge / length).toFixed(2);
                            } else {
                                bill.netAdditionalCharge = parseFloat(0).toFixed(2);
                            }

                            _.forEach(itm.calculateInUnits, function(u, ii) {
                                if (u != null) {;
                                    // console.log('u',u)
                                    if (u.type == "selectedUnit") {
                                        bill.qty = itm.qty;
                                        bill.unitName = u.unitName;
                                        bill.amount = parseFloat(u.subTotal).toFixed(2);
                                        // bill.discountType=r.discountType;
                                        //bill.serviceChargeType=r.serviceChargeType;
                                        //bill.vat=parseFloat(u.vatAmt).toFixed(2);
                                        bill.vat = parseFloat(vs).toFixed(2);
                                        // if(r.discountType=="percent"){
                                        //   bill.discount= parseFloat(parseFloat(bill.amount)*0.01*r.discount).toFixed(2);
                                        // }
                                        // else{
                                        //   bill.discount= parseFloat(r.discount).toFixed(2);
                                        // }
                                        /* if(r.discount!=undefined){
                                           bill.discount=parseFloat(dis).toFixed(2);
                                         }
                                         else
                                         {
                                           bill.discount=parseFloat(0).toFixed(2);
                                         }*/
                                        /* if(r.serviceCharge!=undefined){
                bill.serviceCharge=parseFloat(r.serviceCharge).toFixed(2);
              }
              else
              {
                bill.serviceCharge=parseFloat(0).toFixed(2);
              }*/
                                        /* if(r.cartage!=undefined){
                bill.cartage=parseFloat(r.cartage).toFixed(2);
              }
              else
              {
                bill.cartage=parseFloat(0).toFixed(2);
              }*/

                                        //bill.cartage=parseFloat(r.cartage).toFixed(2);
                                        bill.total = parseFloat(parseFloat(bill.amount) - parseFloat(bill.discount) + parseFloat(bill.vat) + parseFloat(bill.cartage) + parseFloat(bill.netAdditionalCharge)).toFixed(2);
                                    }
                                }
                            });
                            var index = _.findIndex(itemss, { _id: itm._id });
                            if (index < 0) {
                                itm.bills = [];
                                itm.bills.push(bill);
                                itemss.push(itm);
                            } else {
                                //push bill
                                itemss[index].bills.push(bill);
                            }
                        });
                    });
                });
                return itemss;
            };
            //--------------------------------------Reports--------------------------------------------------
            $scope.exportToPdf_purchaseDetail = function() {
                var table = document.getElementById('purchaseDetailReport').innerHTML;
                var printer = window.open('', '', 'width=600,height=600');
                printer.document.open("text/html");
                printer.document.write(table);
                printer.document.close();
                printer.focus();
                printer.print();
            };
            $scope.exportToExcel_purchaseDetail = function() {
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('purchaseDetailReport').innerHTML));
            };
        }
    ]);
