'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('purchaseDetailReports', {
        url: '/purchaseDetailReports',
        templateUrl: 'app/stock/stockReports/purchaseDetailReports/purchaseDetailReports.html',
        controller: 'PurchaseDetailReportsCtrl'
      });
  });