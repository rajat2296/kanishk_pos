'use strict';

describe('Controller: PurchaseDetailReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var PurchaseDetailReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PurchaseDetailReportsCtrl = $controller('PurchaseDetailReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
