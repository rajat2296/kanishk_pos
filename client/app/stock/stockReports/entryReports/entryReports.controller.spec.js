'use strict';

describe('Controller: EntryReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var EntryReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EntryReportsCtrl = $controller('EntryReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
