'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('entryReports', {
        url: '/entryReports',
        templateUrl: 'app/stock/stockReports/entryReports/entryReports.html',
        controller: 'EntryReportsCtrl'
      });
  });
