'use strict';

angular.module('posistApp')

.controller('EntryReportsCtrl', ['$q', '$resource', '$scope', '$filter', 'growl', 'store', 'vendor', 'receiver', 'currentUser', '$rootScope', '$state', '$modal', 'VendorPaymentHistory', 'VPayment', 'StockResource', 'localStorageService', "property", "deployment", "Utils", "intercom", function($q, $resource, $scope, $filter, growl, store, vendor, receiver, currentUser, $rootScope, $state, $modal, VendorPaymentHistory, VPayment, StockResource, localStorageService, property, deployment, Utils, intercom) {


    console.log(deployment);
    currentUser.deployment_id = localStorageService.get('deployment_id');

    function getStores_ByDeployment() {
        return store.get({ tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id });
    };

    function getVendors_ByDeployment() {
        return vendor.get({ tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id });
    };

    function getReceivers_ByDeployment() {
        return receiver.get({ tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id });
    };

    var allPromise = $q.all([
        getStores_ByDeployment(), getVendors_ByDeployment(), getReceivers_ByDeployment()
    ]);

    allPromise.then(function(value) {
        $scope.stores = value[0];
        $scope.editEntryForm.stores = value[0]; // $rootScope.stores;
        $scope.vendors = value[1]; // $rootScope.vendors;
        $scope.receivers = value[2]; // $scope.receivers;
    });

    property.get({ tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id }).$promise.then(function(result) {

        if (result.length == 0) {
            $scope.resetSerialNumber = false
        } else {
            $scope.settings = result[0];
            if ($scope.settings.resetSerialNumber == undefined)
                $scope.resetSerialNumber = false
            else
                $scope.resetSerialNumber = $scope.settings.resetSerialNumber
            console.log($scope.resetSerialNumber);
        }

    });
    store.get({ tenant_id: currentUser.tenant_id, deployment_id: $scope.currentUser.deployment_id }).$promise.then(function(result) {
        $scope.editEntryForm.stores = result;
    });
    //$scope.stores=St;
    $scope.user = currentUser;
    $scope.payments = VPayment;

    // VendorPaymentHistory.getTransactionHistoryByTransactionId({},function (result){
    //   $scope.payments=result;
    // });


    function getYesterDayDate() {
        var d = new Date();
        d.setDate(d.getDate() - 1);
        return d;
    };

    function parseDate(str) {
        var d = $filter('date')(str, 'dd-MMM-yyyy');
        return d;
    };

    $scope.editEntryForm = {
        fromDate: new Date(), //getYesterDayDate(),
        toDate: new Date(),
        maxDate: new Date()
    };
    $scope.editEntryForm.stores = angular.copy($scope.stores);
    $scope.checkDateGreater = function(date) {
        var d = new Date(date);
        var dd = new Date($scope.editEntryForm.toDate);
        if (d.getTime() > dd.getTime()) {
            growl.success('Greater than toDate', { ttl: 3000 });
            $scope.editEntryForm.fromDate = new Date(); //getYesterDayDate()
        } else {
            //growl.success('Lesser than toDate', {ttl: 3000});
        }

    };

    $scope.checkDateLesser = function(date) {
        var d = new Date(date);
        var dd = new Date($scope.editEntryForm.fromDate);
        if (d.getTime() < dd.getTime()) {
            growl.success('Can not be Lesser than fromDate', { ttl: 3000 });
            $scope.editEntryForm.toDate = new Date();
        } else {
            //growl.success('Greater than fromDate', {ttl: 3000});
        }
    };

    function bindVendor(store) {
        $scope.editEntryForm.vendorReceiver = [];
        if (store != null) {
            _.forEach($scope.vendors, function(v, i) {
                if (v.pricing != undefined) {
                    if (v.pricing._id == store._id) {
                        v.name = v.vendorName;
                        $scope.editEntryForm.vendorReceiver.push(v);
                    }
                }
            });
        }
    };

    function bindReceiver(store) {
        $scope.editEntryForm.vendorReceiver = [];
        var receivers = angular.copy($scope.receivers);
        if (store != null) {
            _.forEach(receivers, function(r, i) {
                // if(r.pricing!=undefined){
                //     _.forEach(r.pricing.store, function(s,ii){
                //         if(s._id==store._id){
                r.name = r.receiverName;
                $scope.editEntryForm.vendorReceiver.push(r);
                //         }
                //     });
                // }
            });
        }
    };

    $scope.bindVenRec_editEntryForm = function(store) {
        var entryType = $scope.editEntryForm.entryType;
        $scope.editEntryForm.availableItems = [];
        if (entryType == 1) {
            //bindVendor
            bindVendor(angular.copy(store));
        } else if (entryType == 2) {
            //bind REceiver
            bindReceiver(store);
        } else {
            _.forEach(store.category, function(c, i) {
                _.forEach(c.item, function(itm, ii) {
                    var index = _.findIndex($scope.editEntryForm.availableItems, { _id: itm._id });
                    if (index < 0) {
                        $scope.editEntryForm.availableItems.push(itm);
                    } else {

                    }
                });
            });
        }
    };
    $scope.editEntryForm.hideVendorReceiver = true;
    $scope.bindStore_editEntryForm = function(entryType) {
        $scope.editEntryForm.stores = [];
        $scope.editEntryForm.vendorReceiver = [];
        $scope.editEntryForm.vendor = {};
        $scope.editEntryForm.store = {};
        $scope.editEntryForm.stores = angular.copy($scope.stores);
        if (entryType == "1" || entryType == "2") {
            $scope.editEntryForm.hideVendorReceiver = true;
        } else {
            $scope.editEntryForm.hideVendorReceiver = false;
        }
    };
    $scope.bindItems = function(store, vendor) {
        _.forEach(store.category, function(c, i) {
            _.forEach(c.item, function(itm, ii) {
                var index = _.findIndex($scope.editEntryForm.availableItems, { _id: itm._id });
                if (index < 0) {
                    $scope.editEntryForm.availableItems.push(itm);
                } else {

                }
            });
        });
    };

    $scope.transfer = function(itm) {
        if (itm.price == 0) {
            $scope.flag = true

        }

        return true

    }


    $scope.startSearch = function() {
        $scope.flag = false
        $scope.flag2 = false
        intercom.registerEvent('StockReports');
        $scope.editEntryForm.availableRecords = [];
        var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.editEntryForm.fromDate))));
        console.log('fromDate', fromDate);
        //      var fromDate=new Date($scope.editEntryForm.fromDate);
        //    fromDate.setHours(0, 0, 0, 0);
        var toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.editEntryForm.toDate))));
        console.log('toDate', toDate);
        //var toDate=new Date($scope.editEntryForm.toDate);
        //  toDate.setHours(0, 0, 0, 0);
        var strId = "";
        var venId = "";
        var itemId = "";
        console.log('$scope.editEntryForm', $scope.editEntryForm);
        if ($scope.editEntryForm.store != undefined) {
            strId = $scope.editEntryForm.store._id;
        };
        if ($scope.editEntryForm.vendor != undefined) {
            venId = $scope.editEntryForm.vendor._id;
        };
        if ($scope.editEntryForm.items != undefined) {
            itemId = $scope.editEntryForm.items._id;
        };
        if ($scope.editEntryForm.entryType == "1") {
            var req = { "fromDate": fromDate, "toDate": toDate, "transactionType": $scope.editEntryForm.entryType, "_store._id": strId, "_store.vendor._id": venId, "itemId": itemId, deployment_id: currentUser.deployment_id, tenant_id: currentUser.tenant_id };
        } else if ($scope.editEntryForm.entryType == "2") {
            var req = { "fromDate": fromDate, "toDate": toDate, "transactionType": $scope.editEntryForm.entryType, "_store._id": strId, "_store.receiver._id": venId, "itemId": itemId, deployment_id: currentUser.deployment_id, tenant_id: currentUser.tenant_id };
        } else {
            var req = { "fromDate": fromDate, "toDate": toDate, "transactionType": $scope.editEntryForm.entryType, "_store._id": strId, "itemId": itemId, deployment_id: currentUser.deployment_id, tenant_id: currentUser.tenant_id };
        }
        console.log('req', req);
        //var para=formatQueryParamater(req);
        //StockResource.getAllByTranasactionAndDateRange(req, function (result){
        StockResource.getBasicEntryReportsForEntries(req, function(obj) {
            // var res = _.groupBy(obj, function(r) {
            //         // console.log(new Date(r.created).getTime())
            //         var date = new Date(r.created).getTime()
            //         r.unique = date
            //         return r.unique
            //     })
            //     // console.log(res);
            // var arr = [];
            // var i = 0;

            // _.forEach(res, function(a, ii) {
            //         console.log(a, "obj");
            //         var count = 0
            //         _.forEach(a, function(a1) {
            //             if (count == 0)
            //                 arr.push(a1)
            //             count++;


            //         })



            //     })
                //console.log(arr);
            var result = angular.copy(obj)
            console.log(result)
            if ($scope.editEntryForm.entryType == "1" || $scope.editEntryForm.entryType == "2") {
                $scope.editEntryForm.rptPhysicalWastageTransfer = "";
                $scope.editEntryForm.rptTransfer = "";
                $scope.editEntryForm.reportData = formatDataForReport_StockEntry(result);
                console.log($scope.editEntryForm.reportData);
                calculateBillWiseTotal($scope.editEntryForm.reportData);
            } else if ($scope.editEntryForm.entryType == "3" || $scope.editEntryForm.entryType == "6" || $scope.editEntryForm.entryType == "7" || $scope.editEntryForm.entryType == "10" || $scope.editEntryForm.entryType == "11" || $scope.editEntryForm.entryType == "13") {
                $scope.editEntryForm.reportData = "";
                $scope.editEntryForm.rptTransfer = "";
                $scope.editEntryForm.rptPhysicalWastageTransfer = formatDataForReport_PWST(result);
                console.log("bills", $scope.editEntryForm.rptPhysicalWastageTransfer);
                calculateBillWiseTotal_PWST($scope.editEntryForm.rptPhysicalWastageTransfer);
                //console.log($scope.editEntryForm.rptPhysicalWastageTransfer);
            } else if ($scope.editEntryForm.entryType == "4") {
                $scope.editEntryForm.reportData = "";
                $scope.editEntryForm.rptTransfer = "";
                $scope.editEntryForm.rptPhysicalWastageTransfer = formatDataForReport_PWST(result);
                console.log($scope.editEntryForm.rptPhysicalWastageTransfer);
                calculateBillWiseTotal_WastageEntry($scope.editEntryForm.rptPhysicalWastageTransfer);
                //console.log($scope.editEntryForm.rptPhysicalWastageTransfer);
            } else if ($scope.editEntryForm.entryType == "5") {
                $scope.editEntryForm.reportData = "";
                $scope.editEntryForm.rptPhysicalWastageTransfer = "";
                $scope.editEntryForm.rptTransfer = "";
                $scope.editEntryForm.rptTransfer = formatDataForReport_PWST(result);
                console.log($scope.editEntryForm.rptTransfer);
                calculateBillWiseTotal_transferEntry($scope.editEntryForm.rptTransfer);
            }

            //  calculateForStockEntry(result);
            // $scope.editEntryForm.availableRecords=result;
            // console.log(result);

        });
    };

    function formatDataForReport_PWST(result) {
        //console.log(angular.copy(result));
        var sss = result;
        var reportData = [];
        _.forEach(sss, function(ss, si) {
            _.forEach(ss._store.category, function(s, i) {
                //console.log(ss.transactionType);
                var strIndex = _.findIndex(reportData, { _id: ss._store._id });
                //console.log(strIndex);
                _.forEach(s.items, function(itm, ii) {
                    //console.log("ittttt",itm)
                    if (itm.itemPrice == undefined)
                        itm.price = 0;
                    else if (itm.itemPrice == 0)
                        itm.price = 0
                    else
                        itm.price = itm.itemPrice
                    itm.qty = parseFloat(itm.qty).toFixed(3);
                    if (ss.transactionType == "15") {
                        if (itm.returnWastageQty) {
                            itm.qty = parseFloat(itm.returnWastageQty).toFixed(3);
                        } else {
                            itm.qty = parseFloat(0).toFixed(3);
                        }
                    }
                });
                if (strIndex < 0) {
                    var billData = [];
                    var bill = {};
                    if (ss.transactionType == "3") {
                        if ($scope.resetSerialNumber)
                            bill.billNo = "PH-" + ss.daySerialNumber;
                        else
                            bill.billNo = "PH-" + ss.transactionNumber;
                    } else if (ss.transactionType == "4") {
                        if (ss.wastageEntryType == "Wastage") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "WS-" + ss.daySerialNumber;
                            else
                                bill.billNo = "WS-" + ss.transactionNumber;
                        } else {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "RU-" + ss.daySerialNumber;
                            else
                                bill.billNo = "RU-" + ss.transactionNumber;
                        }
                    } else if (ss.transactionType == "15") {
                        if ($scope.resetSerialNumber)
                            bill.billNo = "RS-" + ss.daySerialNumber;
                        else
                            bill.billNo = "RS-" + ss.transactionNumber;
                    } else if (ss.transactionType == "5") {

                        if ($scope.resetSerialNumber)
                            bill.billNo = "ST-" + ss.daySerialNumber;
                        else
                            bill.billNo = "ST-" + ss.transactionNumber;
                    } else if (ss.transactionType == "6") {
                        if ($scope.resetSerialNumber)
                            bill.billNo = "OS-" + ss.daySerialNumber;
                        else
                            bill.billNo = "OS-" + ss.transactionNumber;
                    } else if (ss.transactionType == "7") {
                        if ($scope.resetSerialNumber)
                            bill.billNo = "IR-" + ss.daySerialNumber;
                        else
                            bill.billNo = "IR-" + ss.transactionNumber;
                    } else if (ss.transactionType == "10") {
                        if ($scope.resetSerialNumber)
                            bill.billNo = "OS-" + ss.daySerialNumber;
                        else
                            bill.billNo = "OS-" + ss.transactionNumber;
                    } else if (ss.transactionType == "11") {
                        if ($scope.resetSerialNumber)
                            bill.billNo = "PE-" + ss.daySerialNumber;
                        else
                            bill.billNo = "PE-" + ss.transactionNumber;
                    } else if (ss.transactionType == "13") {
                        if ($scope.resetSerialNumber)
                            bill.billNo = "OS-" + ss.daySerialNumber;
                        else
                            bill.billNo = "OS-" + ss.transactionNumber;
                    }
                    bill.billDate = ss.created;
                    bill.items = s.items;
                    bill.daySerialNumber = ss.daySerialNumber;
                    if (ss.transactionType == "5") {
                        try {
                            bill.toStore = ss._store.category[0].items[0].toStore.storeName;
                        } catch (err) {
                            console.log(err);
                        }
                    }
                    billData.push(bill);
                    reportData.push({ _id: ss._store._id, storeName: ss._store.storeName, billData: billData, toStore: ss.toStore });
                } else {
                    var bINdex = _.findIndex(reportData[strIndex].billData, { billDate: ss.created, daySerialNumber: ss.daySerialNumber });
                    if (bINdex < 0) {
                        var bill = {};
                        if (ss.transactionType == "3") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "PH-" + ss.daySerialNumber;
                            else
                                bill.billNo = "PH-" + ss.transactionNumber;
                        } else if (ss.transactionType == "4") {
                            if (ss.wastageEntryType == "Wastage") {
                                if ($scope.resetSerialNumber)
                                    bill.billNo = "WS-" + ss.daySerialNumber;
                                else
                                    bill.billNo = "WS-" + ss.transactionNumber;
                            } else {
                                if ($scope.resetSerialNumber)
                                    bill.billNo = "RU-" + ss.daySerialNumber;
                                else
                                    bill.billNo = "RU-" + ss.transactionNumber;
                            }
                        } else if (ss.transactionType == "15") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "RS-" + ss.daySerialNumber;
                            else
                                bill.billNo = "RS-" + ss.transactionNumber;
                        } else if (ss.transactionType == "5") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "ST-" + ss.daySerialNumber;
                            else
                                bill.billNo = "ST-" + ss.transactionNumber;
                        } else if (ss.transactionType == "6") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "OS-" + ss.daySerialNumber;
                            else
                                bill.billNo = "OS-" + ss.transactionNumber;
                        } else if (ss.transactionType == "7") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "IR-" + ss.daySerialNumber;
                            else
                                bill.billNo = "IR-" + ss.transactionNumber;
                        } else if (ss.transactionType == "10") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "OS-" + ss.daySerialNumber;
                            else
                                bill.billNo = "OS-" + ss.transactionNumber;
                        } else if (ss.transactionType == "11") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "PE-" + ss.daySerialNumber;
                            else
                                bill.billNo = "PE-" + ss.transactionNumber;
                        } else if (ss.transactionType == "13") {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "OS-" + ss.daySerialNumber;
                            else
                                bill.billNo = "OS-" + ss.transactionNumber;
                        }
                        //bill.billNo=ss.daySerialNumber;
                        bill.billDate = ss.created;
                        bill.daySerialNumber = ss.daySerialNumber;
                        bill.items = s.items;
                        if (ss.transactionType == "5") {
                            try {
                                bill.toStore = ss._store.category[0].items[0].toStore.storeName;
                            } catch (err) {
                                console.log(err);
                            }
                        }
                        reportData[strIndex].billData.push(bill);
                    } else {
                        _.forEach(s.items, function(itm, im) {
                            reportData[strIndex].billData[bINdex].items.push(itm);
                        });
                    }
                }
            });
        });
        return reportData;
    };

    function calculateBillWiseTotal_PWST(result) {
        _.forEach(result, function(r, i) {
            r.totalQty = 0;
            var rtQ = 0,
                rA = 0;
            if (r.transactionType == "15") {
                _.forEach(r.billData, function(b, iii) {
                    var tQ = 0,
                        t = 0,
                        d = 0,
                        vs = 0,
                        gt = 0,
                        p = 0;
                    _.forEach(b.items, function(itm, iiii) {
                        tQ = parseFloat(parseFloat(tQ) + parseFloat(itm.returnWastageQty)).toFixed(3);
                        t = parseFloat(t) + parseFloat(parseFloat(itm.returnWastageQty) * parseFloat(itm.price));
                    });
                    b.totalQty = parseFloat(tQ).toFixed(3);
                    b.total = parseFloat(t).toFixed(3);
                    b.discount = parseFloat(d).toFixed(2);
                    b.vatSCharge = parseFloat(vs).toFixed(2);
                    b.gt = parseFloat(t - d + vs).toFixed(3);
                    b.payment = p;
                    rtQ = parseFloat(rtQ) + parseFloat(tQ);
                });
            } else {
                _.forEach(r.billData, function(b, iii) {
                    var tQ = 0,
                        t = 0,
                        d = 0,
                        vs = 0,
                        gt = 0,
                        p = 0;
                    _.forEach(b.items, function(itm, iiii) {
                        tQ = parseFloat(parseFloat(tQ) + parseFloat(itm.qty)).toFixed(3);
                        t = parseFloat(t) + parseFloat(parseFloat(itm.qty) * parseFloat(itm.price));
                    });
                    b.totalQty = parseFloat(tQ).toFixed(3);
                    b.total = parseFloat(t).toFixed(3);
                    b.discount = parseFloat(d).toFixed(2);
                    b.vatSCharge = parseFloat(vs).toFixed(2);
                    b.gt = parseFloat(t - d + vs).toFixed(3);
                    b.payment = p;
                    rtQ = parseFloat(rtQ) + parseFloat(tQ);
                });
            }
            r.totalQty = parseFloat(parseFloat(r.totalQty) + parseFloat(rtQ)).toFixed(3);
        });
    };


    function calculateBillWiseTotal_WastageEntry(result) {
        _.forEach(result, function(r, i) {
            r.totalQty = 0;
            r.totalPrice = 0
            var rtQ = 0,
                rA = 0,
                index = 0,
                rtP = 0;
            _.forEach(r.billData, function(b, iii) {
                var tQ = 0,
                    t = 0,
                    d = 0,
                    vs = 0,
                    gt = 0,
                    p = 0,
                    checkQuantity = 0
                index = iii;
                _.forEach(b.items, function(items, w) {
                    checkQuantity = parseFloat(parseFloat(checkQuantity) + parseFloat(items.qty)).toFixed(3);
                    tQ = parseFloat(parseFloat(tQ) + parseFloat(items.qty)).toFixed(3);
                    t = parseFloat(t) + parseFloat(parseFloat(items.qty) * parseFloat(items.price));
                });
                if (Number(checkQuantity) == 0) {
                    r.billData.splice(index, 1);
                } else {
                    b.totalQty = parseFloat(tQ).toFixed(3);
                    b.total = parseFloat(t).toFixed(3);
                    b.discount = parseFloat(d).toFixed(2);
                    b.vatSCharge = parseFloat(vs).toFixed(2);
                    b.gt = parseFloat(t - d + vs).toFixed(3);
                    b.payment = p;
                    rtQ = parseFloat(rtQ) + parseFloat(tQ);
                    rtP = parseFloat(t) + parseFloat(rtP)
                }

            });
            r.totalPrice = parseFloat(parseFloat(r.totalPrice) + parseFloat(rtP)).toFixed(2);
            r.totalQty = parseFloat(parseFloat(r.totalQty) + parseFloat(rtQ)).toFixed(3);
        });
    };

    function calculateBillWiseTotal_transferEntry(result) {
        _.forEach(result, function(r, i) {
            r.totalQty = 0;
            r.totalPrice = 0
            var rtQ = 0,
                rA = 0,
                index = 0,
                rtP = 0;
            _.forEach(r.billData, function(b, iii) {
                var tQ = 0,
                    t = 0,
                    d = 0,
                    vs = 0,
                    gt = 0,
                    p = 0,
                    checkQuantity = 0
                index = iii;
                _.forEach(b.items, function(items, w) {
                    checkQuantity = parseFloat(parseFloat(checkQuantity) + parseFloat(items.qty)).toFixed(3);
                    tQ = parseFloat(parseFloat(tQ) + parseFloat(items.qty)).toFixed(3);
                    t = parseFloat( parseFloat(t) + parseFloat( parseFloat(items.qty) * parseFloat(items.itemPrice) ) ) .toFixed(2);
                });
                if (Number(checkQuantity) == 0) {
                    r.billData.splice(index, 1);
                } else {
                    b.totalQty = parseFloat(tQ).toFixed(3);
                    b.total = parseFloat(t).toFixed(2);
                    b.discount = parseFloat(d).toFixed(2);
                    b.vatSCharge = parseFloat(vs).toFixed(2);
                    b.gt = parseFloat(t - d + vs).toFixed(3);
                    b.payment = p;
                    rtQ = parseFloat(rtQ) + parseFloat(tQ);
                    rtP = parseFloat(parseFloat(t) + parseFloat(rtP)).toFixed(2)
                }
            });
            r.totalPrice = parseFloat(parseFloat(r.totalPrice) + parseFloat(rtP)).toFixed(2);
            r.totalQty = parseFloat(parseFloat(r.totalQty) + parseFloat(rtQ)).toFixed(3);
        });
    };

    function formatDataForReport_StockEntry(result) {
        var sss = result;
        var reportData = [];
        _.forEach(sss, function(ss, si) {
            if (ss.transactionType == "1") {
                //console.log(ss);
                _.forEach(ss._store.vendor.category, function(s, i) {
                    var strIndex = _.findIndex(reportData, { _id: ss._store._id });
                    if (strIndex < 0) {
                        var vend = [];
                        var ven = {};
                        var billData = [];
                        var bill = {};
                        ven._id = ss._store.vendor._id;
                        ven.vendorName = ss._store.vendor.vendorName;
                        if (ss.isPOEntry) {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "PO-" + ss.daySerialNumber;
                            else
                                bill.billNo = "PO-" + ss.transactionNumber;
                        } else {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "SE-" + ss.daySerialNumber;
                            else
                                bill.billNo = "SE-" + ss.transactionNumber;
                        }
                        bill.isEntry = true;
                        bill.invoiceNumber = ss.invoiceNumber;
                        bill.batchNumber = ss.batchNumber;
                        bill.billDate = ss.created;
                        bill.items = s.items;
                        if (ss.discount != undefined) {
                            bill.discount = parseFloat(ss.discount).toFixed(2);
                        } else {
                            bill.discount = parseFloat(0).toFixed(2);
                        }
                        if (ss.cartage != undefined) {
                            bill.cartage = parseFloat(ss.cartage).toFixed(2);
                        } else {
                            bill.cartage = parseFloat(0).toFixed(2);
                        }
                        bill.discountType = ss.discountType;
                        if(ss.charges)
                        bill.charges = ss.charges;
                        else
                        bill.charges = [];
                        bill.transactionType = ss.transactionType;
                        billData.push(bill);
                        ven.billData = billData;
                        vend.push(ven);
                        reportData.push({ _id: ss._store._id, storeName: ss._store.storeName, vendor: vend });
                    } else {
                        var ven = {};
                        var bill = {};
                        ven._id = ss._store.vendor._id;
                        ven.vendorName = ss._store.vendor.vendorName;
                        if (ss.isPOEntry) {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "PO-" + ss.daySerialNumber;
                            else
                                bill.billNo = "PO-" + ss.transactionNumber;
                        } else {
                            if ($scope.resetSerialNumber)
                                bill.billNo = "SE-" + ss.daySerialNumber;
                            else
                                bill.billNo = "SE-" + ss.transactionNumber;
                        }
                        bill.billDate = ss.created;
                        if (ss.discount != undefined) {
                            bill.discount = parseFloat(ss.discount).toFixed(2);
                        } else {
                            bill.discount = parseFloat(0).toFixed(2);
                        }
                        if (ss.cartage != undefined) {
                            bill.cartage = parseFloat(ss.cartage).toFixed(2);
                        } else {
                            bill.cartage = parseFloat(0).toFixed(2);
                        }
                        bill.isEntry = true;
                        bill.invoiceNumber = ss.invoiceNumber;
                        bill.batchNumber = ss.batchNumber;
                        bill.discountType = ss.discountType;
                        if(ss.charges)
                        bill.charges = ss.charges;
                        else
                        bill.charges = [];    
                        bill.transactionType = ss.transactionType;
                        bill.items = s.items;
                        ven.billData = [];
                        ven.billData.push(bill);
                        var venIndex = _.findIndex(reportData[strIndex].vendor, { _id: ss._store.vendor._id });
                        if (venIndex < 0) {
                            reportData[strIndex].vendor.push(ven);
                        } else {
                            var bINdex = _.findIndex(reportData[strIndex].vendor[venIndex].billData, { billDate: ss.created });
                            if (bINdex < 0) {
                                reportData[strIndex].vendor[venIndex].billData.push(bill);
                            } else {
                                _.forEach(s.items, function(itm, it) {
                                    reportData[strIndex].vendor[venIndex].billData[bINdex].items.push(itm);
                                });
                            }
                        }
                    }
                });
            } else if (ss.transactionType == "2") {
                _.forEach(ss._store.receiver.category, function(s, i) {
                    var strIndex = _.findIndex(reportData, { _id: ss._store._id });
                    if (strIndex < 0) {
                        var vend = [];
                        var ven = {};
                        var billData = [];
                        var bill = {};
                        ven._id = ss._store.receiver._id;
                        ven.vendorName = ss._store.receiver.receiverName;
                        if ($scope.resetSerialNumber)
                            bill.billNo = "SS-" + ss.daySerialNumber;
                        else
                            bill.billNo = "SS-" + ss.transactionNumber;
                        bill.invoiceNumber = ss.invoiceNumber;
                        bill.billDate = ss.created;
                        if (ss.discount != undefined && ss.discount != null) {
                            bill.discount = parseFloat(ss.discount).toFixed(2);
                        } else {
                            bill.discount = parseFloat(0).toFixed(2);
                        }
                        if (ss.cartage != undefined && ss.cartage != null) {
                            bill.cartage = parseFloat(ss.cartage).toFixed(2);
                        } else {
                            bill.cartage = parseFloat(0).toFixed(2);
                        }
                        if (ss.serviceCharge != undefined && ss.serviceCharge != null) {
                            bill.serviceCharge = parseFloat(ss.serviceCharge).toFixed(2);
                        } else {
                            bill.serviceCharge = parseFloat(0).toFixed(2);
                        }
                        if (ss.vat != undefined && ss.vat != null) {
                            bill.vat = parseFloat(ss.vat).toFixed(2);
                        } else {
                            bill.vat = parseFloat(0).toFixed(2);
                        }
                        if (ss.payment != undefined) {
                            bill.payment = parseFloat(ss.payment).toFixed(2);
                        }
                        bill.isEntry = false;
                        bill.discountType = ss.discountType;
                        if(ss.charges)
                        bill.charges = ss.charges;
                        else
                        bill.charges = [];
                        bill.transactionType = ss.transactionType;
                        bill.vatType = ss.vatType;
                        bill.serviceChargeType = ss.serviceChargeType;
                        bill.items = s.items;
                        billData.push(bill);
                        ven.billData = billData;
                        vend.push(ven);
                        reportData.push({ _id: ss._store._id, storeName: ss._store.storeName, vendor: vend });
                    } else {
                        var ven = {};
                        var bill = {};
                        ven._id = ss._store.receiver._id;
                        ven.vendorName = ss._store.receiver.receiverName;
                        if ($scope.resetSerialNumber)
                            bill.billNo = "SS-" + ss.daySerialNumber;
                        else
                            bill.billNo = "SS-" + ss.transactionNumber;
                        bill.billDate = ss.created;
                        if (ss.discount != undefined && ss.discount != null) {
                            bill.discount = parseFloat(ss.discount).toFixed(2);
                        } else {
                            bill.discount = parseFloat(0).toFixed(2);
                        }
                        if (ss.cartage != undefined && ss.cartage != null) {
                            bill.cartage = parseFloat(ss.cartage).toFixed(2);
                        } else {
                            bill.cartage = parseFloat(0).toFixed(2);
                        }
                        if (ss.serviceCharge != undefined && ss.serviceCharge != null) {
                            bill.serviceCharge = parseFloat(ss.serviceCharge).toFixed(2);
                        } else {
                            bill.serviceCharge = parseFloat(0).toFixed(2);
                        }
                        if (ss.vat != undefined && ss.vat != null) {
                            bill.vat = parseFloat(ss.vat).toFixed(2);
                        } else {
                            bill.vat = parseFloat(0).toFixed(2);
                        }
                        if (ss.payment != undefined) {
                            bill.payment = parseFloat(ss.payment).toFixed(2);
                        }
                        bill.isEntry = false;
                        bill.discountType = ss.discountType;
                        if(ss.charges)
                        bill.charges = ss.charges;
                        else
                        bill.charges = [];
                        bill.invoiceNumber = ss.invoiceNumber;
                        bill.transactionType = ss.transactionType;
                        bill.vatType = ss.vatType;
                        bill.serviceChargeType = ss.serviceChargeType;
                        bill.items = s.items;
                        ven.billData = [];
                        ven.billData.push(bill);
                        var venIndex = _.findIndex(reportData[strIndex].vendor, { _id: ss._store.receiver._id });
                        if (venIndex < 0) {
                            reportData[strIndex].vendor.push(ven);
                        } else {
                            var bINdex = _.findIndex(reportData[strIndex].vendor[venIndex].billData, { billDate: ss.created });
                            if (bINdex < 0) {
                                reportData[strIndex].vendor[venIndex].billData.push(bill);
                            } else {
                                _.forEach(s.items, function(itm, it) {
                                    reportData[strIndex].vendor[venIndex].billData[bINdex].items.push(itm);
                                });
                            }
                            //reportData[strIndex].vendor[venIndex].billData.push(bill);
                        }
                    }
                });
            }
        });
        console.log('------', reportData);
        return reportData;
    };


    function calculateBillWiseTotal(result) {
        _.forEach(result, function(r, i) {
            var rtQ = 0,
                rA = 0;
            _.forEach(r.vendor, function(v, ii) {
                var vtQ = 0,
                    tA = 0;
                _.forEach(v.billData, function(b, iii) {
                    var tQ = 0,
                        t = 0,
                        d = 0,
                        vs = 0,
                        gt = 0,
                        p = 0,
                        cartage = 0,
                        numberOfItems = 0,
                        disc = 0,
                        disTotal = 0,
                        vatTotal = 0,
                        sumvs = 0;
                    numberOfItems = b.items.length;
                    _.forEach(b.items, function(itm) {
                        t += Utils.roundNumber(itm.qty, 2) * Utils.roundNumber(itm.price, 2);
                    });
                    _.forEach(b.items, function(itm, iiii) {
                        //console.log('-----------New Item-----------------')
                        if (!itm.itemCode)
                            itm.itemCode = '';
                        itm.qty = parseFloat(itm.qty).toFixed(3);
                        itm.price = parseFloat(itm.price).toFixed(2);
                        tQ = parseFloat(tQ) + parseFloat(itm.qty);
                        //t=parseFloat(parseFloat(t)+parseFloat(itm.qty)*parseFloat(itm.price)).toFixed(2);
                        itm.total = parseFloat(parseFloat(itm.qty) * parseFloat(itm.price)).toFixed(2);
                        if (b.transactionType == "1") {

                            var dis = 0,
                                disAmount = 0,
                                perDis = 0;

                            if (b.discount != undefined && b.discount != 0) {
                                if (b.discountType == "percent") {
                                    //console.log('total',t);
                                    dis = parseFloat(t * 0.01 * b.discount).toFixed(2);
                                    perDis = parseFloat(parseFloat(parseFloat(itm.total) * parseFloat(dis)).toFixed(2) / parseFloat(t).toFixed(2)).toFixed(2);
                                    //console.log('dis', dis);
                                    //console.log('perDis', perDis)
                                    //console.log('itm.total',itm.total);
                                    disAmount = parseFloat(itm.total).toFixed(2) - parseFloat(perDis).toFixed(2);
                                    //console.log('disAmount',disAmount);
                                    if (itm.vatPercent) {
                                        //console.log('vatTotal vefore vat',vatTotal);
                                        //console.log('itm.vatPercent',itm.vatPercent);
                                        vs = parseFloat(itm.vatPercent * 0.01 * parseFloat(disAmount).toFixed(2)).toFixed(2);
                                        //console.log('vs',vs);
                                        //console.log('disAmount before vat',disAmount);
                                        disAmount = parseFloat(parseFloat(disAmount) + parseFloat(vs)).toFixed(2);
                                        //console.log('disAmount after vat',disAmount);
                                        disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                                        //console.log('disTotal',disTotal);
                                        vatTotal = parseFloat(parseFloat(vatTotal) + parseFloat(vs)).toFixed(2);
                                        //console.log(disAmount);
                                        //console.log('vatTotal after vat',vatTotal);
                                    } else {
                                        disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                                        //console.log('disTotal',disTotal);
                                    }
                                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                                    //console.log('disTotal',disTotal);
                                } else {
                                    //console.log('total',t);
                                    dis = parseFloat(b.discount).toFixed(2);
                                    //perDis=parseFloat(dis/numberOfItems).toFixed(2);
                                    perDis = parseFloat(parseFloat(parseFloat(itm.total) * parseFloat(dis)).toFixed(2) / parseFloat(t).toFixed(2)).toFixed(2);
                                    //console.log('dis', dis);
                                    //console.log('perDis', perDis);
                                    //console.log('itm.total',itm.total);
                                    disAmount = parseFloat(parseFloat(itm.total).toFixed(2) - parseFloat(perDis).toFixed(2)).toFixed(2);
                                    //console.log('disAmount',disAmount);
                                    if (itm.vatPercent) {
                                        //console.log('vatTotal vefore vat',vatTotal);
                                        //console.log('itm.vatPercent',itm.vatPercent);
                                        vs = parseFloat(itm.vatPercent * 0.01 * parseFloat(disAmount).toFixed(2)).toFixed(2);
                                        //console.log('vs',vs);
                                        //console.log('disAmount before vat',disAmount);
                                        disAmount = parseFloat(parseFloat(disAmount) + parseFloat(vs)).toFixed(2);
                                        //console.log('disAmount after vat',disAmount);
                                        disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                                        //console.log('disTotal',disTotal);
                                        vatTotal = parseFloat(parseFloat(vatTotal) + parseFloat(vs)).toFixed(2);
                                        //console.log(disAmount);
                                        //console.log('vatTotal after vat',vatTotal);
                                    } else {
                                        disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                                        //console.log('disTotal',disTotal);  
                                    }
                                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                                    //console.log('disTotal',disTotal);
                                }
                            } else {
                                if (itm.vatPercent) {
                                    //console.log('vatTotal vefore vat',vatTotal);
                                    //console.log('itm.vatPercent',itm.vatPercent);
                                    vs = parseFloat(itm.vatPercent * 0.01 * parseFloat(itm.total).toFixed(2)).toFixed(2);
                                    //console.log('vs',vs);
                                    //console.log('disAmount before vat',disAmount);
                                    disAmount = parseFloat(parseFloat(itm.total) + parseFloat(vs)).toFixed(2);

                                    //disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                                    //console.log('disAmount after vat',disAmount);
                                    disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                                    //console.log('disTotal',disTotal);
                                    vatTotal = parseFloat(parseFloat(vatTotal) + parseFloat(vs)).toFixed(2);
                                    //console.log(disAmount);
                                    //console.log('vatTotal after vat',vatTotal);
                                } else {
                                    disTotal = parseFloat(parseFloat(disTotal) + parseFloat(itm.total)).toFixed(2);
                                    //console.log('disTotal',disTotal);

                                }
                            }
                        }
                    });

                    b.totalQty = parseFloat(tQ).toFixed(3);
                    b.total = parseFloat(t).toFixed(2);
                    if (b.discount != undefined && b.discount != 0) {
                        if (b.discountType == "percent") {
                            d = parseFloat(t * 0.01 * b.discount).toFixed(2);
                        } else {
                            d = parseFloat(b.discount).toFixed(2);
                        }
                    }
                    var afterDis = parseFloat(t) - parseFloat(d);
                    var serviceCharge = 0;
                    var netAdditionalCharge =0;
                    if (b.transactionType == "2") {
                        disTotal = afterDis;
                        if (b.serviceChargeType == "percent") {
                            vatTotal = parseFloat(b.serviceCharge * 0.01 * parseFloat(afterDis)).toFixed(2);
                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(vatTotal)).toFixed(2);
                            //console.log('disTotal',disTotal);

                        } else {
                            vatTotal = parseFloat(b.serviceCharge);
                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(vatTotal)).toFixed(2);
                            //console.log('disTotal',disTotal);

                        }
                        if (b.vatType == "percent") {
                            serviceCharge = parseFloat(b.vat * 0.01 * parseFloat(afterDis)).toFixed(2);
                            //console.log('serviceCharge',serviceCharge)
                            vatTotal = parseFloat(parseFloat(vatTotal) + parseFloat(b.vat * 0.01 * parseFloat(afterDis))).toFixed(2);


                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(serviceCharge)).toFixed(2);
                            //console.log('disTotal',disTotal);

                        } else {

                            serviceCharge = parseFloat(b.vat).toFixed(2);
                            //console.log('serviceCharge',serviceCharge);
                            vatTotal = parseFloat(vatTotal) + parseFloat(b.vat);
                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(serviceCharge)).toFixed(2);
                            //console.log('disTotal',disTotal);

                        }
                    }
                    if (b.cartage != undefined) {
                        cartage = parseFloat(b.cartage);
                    }

                    if (b.payment != undefined) {
                        p = parseFloat(b.payment).toFixed(2);
                    }
                    if(b.charges)
                    {
                        console.log('afterDis',afterDis);
                        _.forEach(b.charges , function(charge) {
                            if(charge.operationType == 'additive')
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue * 0.01 * afterDis)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * afterDis).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                            else
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue * 0.01 * afterDis)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * afterDis).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                        });
                    }
                    b.discount = parseFloat(d).toFixed(2);
                    //console.log('vs', vs)
                    b.vatSCharge = parseFloat(vatTotal).toFixed(2);
                    b.payment = p;
                    console.log('b.netAdditionalCharge',netAdditionalCharge);
                    b.netAdditionalCharge = parseFloat(netAdditionalCharge).toFixed(2);
                    b.gt = parseFloat(parseFloat(disTotal) + parseFloat(cartage) + parseFloat(p) + parseFloat(netAdditionalCharge)).toFixed(2);
                    vtQ = parseFloat(vtQ) + parseFloat(tQ);
                    //tA+=t;
                    tA = parseFloat(parseFloat(tA) + parseFloat(b.gt)).toFixed(2);
                });
                v.totalQty = parseFloat(vtQ).toFixed(2);
                v.totalAmt = parseFloat(tA).toFixed(2);
                rtQ = parseFloat(parseFloat(rtQ) + parseFloat(vtQ)).toFixed(2);
                rA = parseFloat(parseFloat(rA) + parseFloat(tA)).toFixed(2);
            });
            r.totalQty = parseFloat(rtQ).toFixed(2);
            r.totalAmt = parseFloat(rA).toFixed(2);
        });
        //console.log(result);
    };

    function formatQueryParamater(q) {
        _.forIn(q, function(value, key) {
            if (value == "") {
                delete q[key];
            }
        });
        return q;
    };
    $scope.editEditEntryForm = function(item) {
        $state.go('stock.editStockEntry', { id: item._id, transactionType: $scope.editEntryForm.entryType });
    };

    function calculateForStockEntry(result) {
        _.forEach(result, function(r, iii) {
            var tQty = 0;
            var tAmt = 0;
            var tDue = 0;
            var tPaid = 0;
            var req = { mainTranasctionId: r._id };
            var index = _.findIndex($scope.payments, { "_id": r._id });
            if (index >= 0) {
                tPaid = angular.copy($scope.payments[index].totalPaidAmount);
            }
            if (r.transactionType == "1") {
                _.forEach(r._store.vendor.category, function(c, ii) {
                    _.forEach(c.items, function(itm, i) {
                        tQty += itm.qty;
                        tAmt += itm.subTotal;
                    });
                });
                tDue = (tAmt - tPaid);
            } else if (r.transactionType == "2") {
                _.forEach(r._store.receiver.category, function(c, ii) {
                    _.forEach(c.items, function(itm, i) {
                        tQty += itm.qty;
                        tAmt += (itm.qty) * (itm.price);
                    });
                });
                tDue = (tAmt - tPaid);
            }
            r.totalQty = parseFloat(tQty).toFixed(2);
            r.totalAmt = parseFloat(tAmt).toFixed(2);
            r.totalPaid = parseFloat(tPaid).toFixed(2);
            r.totalDue = parseFloat(tDue).toFixed(2);
        });
    }

    function calculateQtyAmtAndVatBillWise(result) {
        _.forEach(result, function(r, i) {
            if (r.transactionType == 1) {
                calculateForStockEntry(result);
            }
        })
    };

    //---------------------------------------------------Reports----------------------------------------------------
    function stockEntryHTML(data) {
        console.log('data',data);
        var html = '<table class="table table-curved" style="width:100%" border="1">';
        html += '<thead>';
        html += '<tr>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Code</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Name</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Units</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit Name</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit Price</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Total</th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        _.forEach(data, function(item, i) {
            html += '<tr>';
            html += '<td colspan="6" style="background-color: #CDF5E3; font-weight:bold;">';
            html += '<b>' + item.storeName + '</b>';
            //html+='<span></span>';
            html += '</td>';
            html += '</tr>';
            _.forEach(item.vendor, function(v, ii) {
                html += '<tr>';
                html += '<td colspan="6" style="background-color: #B9FCDD; font-weight:bold;">';
                html += '<b>' + v.vendorName + '</b>';
                html += '<span></span>';
                html += '</td>';
                html += '</tr>';
                var sortedBills = v.billData;
                sortedBills.sort(function(a, b) {
                    return new Date(a.billDate).getTime() - new Date(b.billDate).getTime();
                })
                _.forEach(sortedBills, function(b, iii) {
                    html += '<tr>';
                    html += '<td colspan="3">';
                    html += '<b>BillNo:' + b.billNo + '</b> &nbsp; &nbsp;';
                    if (b.invoiceNumber)
                        html += '<b>Invoice No:' + b.invoiceNumber + '</b> &nbsp; &nbsp;';
                    if (b.batchNumber)
                        html += '<b>        Batch No:' + b.batchNumber + '</b>';
                    html += '<span></span>';
                    html += '</td>';
                    var date = new Date(b.billDate);
                    html += '<td></td><td></td><td><b>Bill Date: ' + moment(new Date(b.billDate)).format('DD-MM-YYYY hh:mm a') + '</b></td>';
                    html += '</tr>';
                    _.forEach(b.items, function(itm, iiii) {
                        console.log('itm', itm);
                        html += '<tr>';
                        if (!itm.itemCode)
                            html += '<td>-</td>';
                        else
                            html += '<td>' + itm.itemCode + '</td>';
                        html += '<td>' + itm.itemName + '</td>';
                        html += '<td>' + itm.qty + '</td>';
                        if (itm.calculateInUnits[2] != null) {
                            html += '<td>' + itm.calculateInUnits[2].unitName + '</td>';
                        } else {
                            if (itm.calculateInUnits[1] != null) {
                                html += '<td>' + itm.calculateInUnits[1].unitName + '</td>';
                            } else {
                                html += '<td></td>';
                            }
                        }
                        html += '<td>' + itm.price + '</td>';
                        html += '<td>' + itm.total + '</td>';
                        html += '</tr>';
                    });
                    html += '<tr>';
                    html += '<td></td><td colspan="2" style="text-align:right;background-color: #8A59EA;">';
                    html += '<b>Bill Wise Total</b>';
                    html += '</td>';
                    html += '<td style="text-align:right;background-color: #8A59EA;"><b>' + b.totalQty + '</b></td>';
                    html += '<td colspan="2" style="text-align:right;background-color: #8A59EA;">';
                    html += '<td></td>';
                    html += '<b>Total</b>';
                    html += '</td>';
                    html += '<td style="text-align:right;background-color: #8A59EA;"><b>' + b.total + '</b></td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>(-) Discount</b>';
                    html += '</td>';
                    html += '<td>';
                    html += '<b>' + b.discount + '</b>';
                    html += '</td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>(+)  Vat+Service Charge</b>';
                    html += '</td>';
                    html += '<td>';
                    html += '<b>' + b.vatSCharge + '</b>';
                    html += '</td>';
                    html += '</tr>';
                    _.forEach(b.charges , function(charge) {
                        html += '<tr>';
                        html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                        if(charge.operationType == 'additive')
                            html += '<b>(+) '+charge.chargeName+'</b>';
                        else
                            html += '<b>(-) '+charge.chargeName+'</b>';
                        html += '</td>';
                        html += '<td>';
                        html += '<b>' + charge.chargableAmount + '</b>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>Cartage</b>';
                    html += '</td>';
                    html += '<td>';
                    html += '<b>' + b.cartage + '</b>';
                    html += '</td>';
                    html += '</tr>';
                    /*html+='<tr>';
                    html+='<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html+='<b>Payment</b>';
                    html+='</td>';
                    html+='<td>';
                    html+='<b>'+b.payment+'</b>';
                    html+='</td>';
                    html+='</tr>';*/
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>Grand Total</b>';
                    html += '</td>';
                    html += '<td>';
                    html += '<b>' + b.gt + '</b>';
                    html += '</td>';
                    html += '</tr>';
                    html += '<tr></tr>';
                });
                html += '<tr>';
                html += '<td></td><td colspan="2" style="text-align:right; background-color: #8ECCF9;">';
                html += '<b>Vendor/Receiver Wise Total</b>';
                html += '</td>';
                html += '<td  style="background-color: #8ECCF9;">';
                html += '<b>' + v.totalQty + '</b>';
                html += '</td>';
                html += '<td colspan="2"  style="background-color: #8ECCF9;"></td>';
                html += '<td></td>';
                html += '<td style="background-color: #8ECCF9;">';
                html += '<b>' + v.totalAmt + '</b>';
                html += '</td>';
                html += '</tr>';
                html += '<tr></tr>';
            });
            html += '<tr>';
            html += '<td></td><td colspan="2" style="text-align:right;background-color: #1CF291;">';
            html += '<b>Store Wise Total</b>';
            html += '</td>';
            html += '<td style="background-color: #1CF291;"><b>' + item.totalQty + '</b></td>';
            html += '<td style="background-color: #1CF291;" colspan="2"></td>';
            html += '<td></td>';
            html += '<td style="background-color: #1CF291;"><b>' + item.totalAmt + '</b></td>';
            html += '</tr>';
            html += '<tr></tr>';
        });
        html += '</tbody>';
        html += '</table>';
        $scope.CSV = html;
        return html;
    }


    function stockSaleHTML(data) {
        var html = '<table class="table table-curved" style="width:100%" border="1" >';
        html += '<thead>';
        html += '<tr>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Code</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Name</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Units</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit Name</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit Price</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Total</th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        _.forEach(data, function(item, i) {
            html += '<tr>';
            html += '<td colspan="6" style="background-color: #CDF5E3; font-weight:bold;">';
            html += '<b>' + item.storeName + '</b>';
            html += ' <span></span>';
            html += '</td>';
            html += '</tr>';
            _.forEach(item.vendor, function(v, ii) {
                html += '<tr>';
                html += '<td colspan="6" style="background-color: #B9FCDD; font-weight:bold;">';
                html += '<b>' + v.vendorName + '</b>';
                html += '<span></span>';
                html += '</td>';
                html += '</tr>';
                var sortedBills = v.billData;
                sortedBills.sort(function(a, b) {
                    return new Date(a.billDate).getTime() - new Date(b.billDate).getTime();
                })
                _.forEach(sortedBills, function(b, iii) {
                    html += '<tr>';
                    html += '<td colspan="3">';
                    html += '<b>BillNo: ' + b.billNo + '</b> &nbsp; &nbsp';
                    if (b.invoiceNumber)
                        html += '<b">        Invoice No:' + b.invoiceNumber + '</b> &nbsp; &nbsp';
                    html += '<span></span>';
                    html += '</td>';
                    html += '<td></td><td></td><td colspan="3"><b>Bill Date: ' + moment(new Date(b.billDate)).format('DD-MM-YYYY hh:mm a') + '</b>';
                    html += '<span></span>';
                    html += '</td>';
                    html += '</tr>';
                    _.forEach(b.items, function(itm, iiii) {
                        html += '<tr>';
                        if (!itm.itemCode)
                            html += '<td>-</td>';
                        else
                            html += '<td>' + itm.itemCode + '</td>';
                        html += '<td>' + itm.itemName + '</td>';
                        html += '<td>' + itm.qty + '</td>';
                        if (itm.calculateInUnits[2] != null) {
                            html += '<td>' + itm.calculateInUnits[2].unitName + '</td>';
                        } else {
                            if (itm.calculateInUnits[1] != null) {
                                html += '<td>' + itm.calculateInUnits[1].unitName + '</td>';
                            } else {
                                html += '<td></td>';
                            }
                        }
                        html += '<td>' + itm.price + '</td>';
                        html += '<td>' + itm.total + '</td>';
                        html += '</tr>';
                    });
                    html += '<tr>';
                    html += '<td></td><td colspan="2" style="text-align:right;background-color: #8A59EA;">';
                    html += '<b>Bill Wise Total</b>';
                    html += '</td>';
                    html += '<td style="text-align:right;background-color: #8A59EA;"><b>' + b.totalQty + '</b></td>';
                    html += '<td></td>';
                    html += '<td colspan="2" style="text-align:right;background-color: #8A59EA;">';
                    html += '<b>Total</b>';
                    html += '</td>';
                    html += '<td style="text-align:right;background-color: #8A59EA;"><b>' + b.total + '</b></td>';

                    html += '</tr>';
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>(-) Discount</b>';
                    html += '</td>';
                    html += '<td>';
                    html += '<b>' + b.discount + '</b>';
                    html += '</td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>(+)  Vat+Service Charge</b>';
                    html += '</td>';
                    html += '<td><b>' + b.vatSCharge + '</b></td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>Cartage</b>';
                    html += '</td>';
                    html += '<td><b>' + b.cartage + '</b></td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>Payment</b>';
                    html += '</td>';
                    html += '<td>';
                    html += '<b>' + b.payment + '</b>';
                    html += '</td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td></td><td></td><td></td><td></td><td colspan="5" style="text-align:right;">';
                    html += '<b>Grand Total</b>';
                    html += '</td>';
                    html += '<td>';
                    html += '<b>' + b.gt + '</b>';
                    html += '</td>';
                    html += '</tr>';
                    html += '<tr></tr>';
                });
                html += '<tr>';
                html += '<td></td><td colspan="2" style="text-align:right; background-color: #8ECCF9;">';
                html += '<b>Vendor/Receiver Wise Total</b>';
                html += '</td>';
                html += '<td  style="background-color: #8ECCF9;">';
                html += '<b>' + v.totalQty + '</b>';
                html += '</td>';
                html += '<td colspan="2"  style="background-color: #8ECCF9;"></td>';
                html += '<td></td>';
                html += '<td  style="background-color: #8ECCF9;">';
                html += '<b>' + v.totalAmt + '</b>';
                html += '</td>';
                html += '</tr>';
                html += '<tr></tr>';
            });
            html += '<tr>';
            html += '<td></td><td colspan="2" style="text-align:right;background-color: #1CF291;">';
            html += '<b>Store Wise Total</b>';
            html += '</td>';
            html += '<td style="background-color: #1CF291;"><b>' + item.totalQty + '</b></td>';
            html += '<td style="background-color: #1CF291;" colspan="2"></td>';
            html += '<td></td>';
            html += '<td style="background-color: #1CF291;"><b>' + item.totalAmt + '</b></td>';
            html += '</tr>';
            html += '<tr></tr>';
        });
        html += '</tbody>';
        html += '</table>';
        $scope.CSV = html;
        return html;
    }

    function storetransferSHTML(data) {
        var html = '<table class="table table-curved"  style="width:100%" border="1">';
        html += '<thead>';
        html += '<tr>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Code</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Name</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Quanity</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Price</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit Name</th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        _.forEach(data, function(item, i) {
            html += '<tr>';
            html += '<td colspan="4" style="background-color:#CDF5E3;font-weight:bold;">';
            html += '<b>' + item.storeName + '</b>';
            html += '<span></span>';
            html += '</td>';
            html += '</tr>';
            var sortedBills = item.billData;
            sortedBills.sort(function(a, b) {
                return new Date(a.billDate).getTime() - new Date(b.billDate).getTime();
            });
            _.forEach(sortedBills, function(b, ii) {
                html += '<tr>';
                html += '<td colspan="2" style="background-color:#abc123;">';
                html += '<b>BillNo:  ' + b.billNo + '</b>';
                if (b.invoiceNumber)
                    html += '<b>        Invoice No: ' + b.invoiceNumber + '</b>';
                html += '<span></span>';
                html += '</td>';
                html += '<td colspan="1" style="background-color: #abc123">';
                html += '<b>Bill Date: ' + moment(new Date(b.billDate)).format('DD-MM-YYYY hh:mm a') + '</b>';
                html += '<span></span>';
                html += '</td>';
                html += '<td colspan="1" style="background-color: #abc123">';
                html += '<span>';
                if (b.toStore)
                    html += '<b>Transferred To: ' + b.toStore + '</b>';
                html += '<span></span>';
                html += '</span>';
                html += '</td>';
                html += '</tr>';
                _.forEach(b.items, function(itm, iii) {
                    //console.log("itm",itm)
                    html += '<tr>';
                    if (!itm.itemCode)
                        html += '<td>-</td>';
                    else
                        html += '<td>' + itm.itemCode + '</td>';
                    html += '<td>' + itm.itemName + '</td>';
                    html += '<td>' + itm.qty + '</td>';
                    if (itm.price == undefined)
                        html += '<td>N.A.</td>';
                    else if (itm.price == 0)
                        html += '<td>N.A.</td>';
                    else
                        html += '<td>' + itm.price + '</td>';
                    if (itm.calculateInUnits[2] != null) {
                        html += '<td>' + itm.calculateInUnits[2].unitName + '</td>';
                    } else {
                        if (itm.calculateInUnits[1] != null) {
                            html += '<td>' + itm.calculateInUnits[1].unitName + '</td>';
                        } else {
                            html += '<td></td>';
                        }
                    }
                    html += '</tr>';
                    //html+='<tr></tr>';
                });
                html += '<tr>';
                html += '<td></td><td  colspan="2" style="text-align:right;background-color: #FFF26C;">';
                html += '<b>Bill Wise Total</b>';
                html += '</td>';
                html += '<td style="text-align:right;background-color: #FFF26C;"><b>' + b.totalQty + '</b></td>';
                html += '<td style="text-align:right;background-color: #FFF26C;"><b>' + b.total + '</b></td>';
                html += '</tr>';
                //html+='<tr></tr>';
            });
            html += '<tr>';
            html += '<td></td><td colspan="2" style="text-align:right;background-color: #abcabc;">';
            html += '<b>Store Wise Total</b>';
            html += '</td>';
            html += '<td style="text-align:right;background-color: #abcabc;"><b>' + item.totalQty + '</b></td>';
            html += '<td style="text-align:right;background-color: #abcabc;"><b>' + item.totalPrice + '</b></td>';
            html += '</tr>';
            //html+='<tr></tr>';
        });
        html += '</tbody>';
        html += '</table>';
        $scope.CSV = html;
        return html;
    }

    function PWSHTML(data) {
        var html = '<table class="table table-curved"  style="width:100%" border="1">';
        html += '<thead>';
        html += '<tr>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Code</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Item Name</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Quanity</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Price</th>';
        html += '<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit Name</th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        _.forEach(data, function(item, i) {
            html += '<tr>';
            html += '<td colspan="4" style="background-color:#CDF5E3;font-weight:bold;">';
            html += '<b>' + item.storeName + '</b>';
            html += '<span></span>';
            html += '</td>';
            html += '</tr>';
            var sortedBills = item.billData;
            sortedBills.sort(function(a, b) {
                return new Date(a.billDate).getTime() - new Date(b.billDate).getTime();
            });
            _.forEach(sortedBills, function(b, ii) {
                html += '<tr>';
                html += '<td colspan="2" style="background-color:#abc123;">';
                html += '<b>BillNo:  ' + b.billNo + '</b>';
                if (b.invoiceNumber)
                    html += '<b>        Invoice No: ' + b.invoiceNumber + '</b>';
                html += '<span></span>';
                html += '</td>';
                html += '<td colspan="1" style="background-color: #abc123">';
                html += '<b>Bill Date: ' + moment(new Date(b.billDate)).format('DD-MM-YYYY hh:mm a') + '</b>';
                html += '<span></span>';
                html += '</td>';
                html += '<td colspan="1" style="background-color: #abc123">';
                html += '<span>';
                if (b.toStore)
                    html += '<b>Transferred To: ' + b.toStore + '</b>';
                html += '<span></span>';
                html += '</span>';
                html += '</td>';
                html += '</tr>';
                _.forEach(b.items, function(itm, iii) {
                    //console.log("itm",itm)
                    html += '<tr>';
                    if (!itm.itemCode)
                        html += '<td>-</td>';
                    else
                        html += '<td>' + itm.itemCode + '</td>';
                    html += '<td>' + itm.itemName + '</td>';
                    html += '<td>' + itm.qty + '</td>';
                    if (itm.price == undefined)
                        html += '<td>N.A.</td>';
                    else if (itm.price == 0)
                        html += '<td>N.A.</td>';
                    else
                        html += '<td>' + itm.price + '</td>';
                    if (itm.calculateInUnits[2] != null) {
                        html += '<td>' + itm.calculateInUnits[2].unitName + '</td>';
                    } else {
                        if (itm.calculateInUnits[1] != null) {
                            html += '<td>' + itm.calculateInUnits[1].unitName + '</td>';
                        } else {
                            html += '<td></td>';
                        }
                    }
                    html += '</tr>';
                    //html+='<tr></tr>';
                });
                html += '<tr>';
                html += '<td></td><td  colspan="2" style="text-align:right;background-color: #FFF26C;">';
                html += '<b>Bill Wise Total</b>';
                html += '</td>';
                html += '<td style="text-align:right;background-color: #FFF26C;"><b>' + b.totalQty + '</b></td>';
                html += '</tr>';
                //html+='<tr></tr>';
            });
            html += '<tr>';
            html += '<td></td><td colspan="2" style="text-align:right;background-color: #abcabc;">';
            html += '<b>Store Wise Total</b>';
            html += '</td>';
            html += '<td style="text-align:right;background-color: #abcabc;"><b>' + item.totalQty + '</b></td>';
            html += '</tr>';
            //html+='<tr></tr>';
        });
        html += '</tbody>';
        html += '</table>';
        $scope.CSV = html;
        return html;
    }

    $scope.convertToCSV = function() {
        var CSV = $scope.CSV.replace(/,/g, '\,')
        CSV = CSV.replace(/<td\w*[a-zA-Z=%'"-:;# ]*>/g, '')
        CSV = CSV.replace(/<th\w*[a-zA-Z=%'"-:;# ]*>/g, '')
        CSV = CSV.replace(/<h2\w*[a-zA-Z=%'"-:;# ]*>/g, '')
        CSV = CSV.replace(/<table\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<tfoot\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<div\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<caption\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<thead\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<tbody\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<strong\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<i\w*[a-zA-Z=%'"-:;# ]*>/g, '')
        CSV = CSV.replace(/<tr\w*[a-zA-Z=%'"-:;# ]*>/g, '')
        CSV = CSV.replace(/<span\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/<b\w*[a-zA-Z=%"'-:;# ]*>/g, '')
        CSV = CSV.replace(/&nbsp;/g, '')
        CSV = CSV.replace(/&nbsp/g, '')
        CSV = CSV.replace(/<\/span>/g, '')
        CSV = CSV.replace(/<\/b>/g, '')
        CSV = CSV.replace(/<\/i>/g, '')
        CSV = CSV.replace(/<\/thead>/g, '')
        CSV = CSV.replace(/<\/tbody>/g, '')
        CSV = CSV.replace(/<\/strong>/g, '')
        CSV = CSV.replace(/<\/th>/g, ',')
        CSV = CSV.replace(/<\/div>/g, ',')
        CSV = CSV.replace(/<\/h2>/g, ',')
        CSV = CSV.replace(/<\/table>/g, '')
        CSV = CSV.replace(/<\/tfoot>/g, '')
        CSV = CSV.replace(/<\/caption>/g, '')
        CSV = CSV.replace(/<\/td>/g, ',')
        CSV = CSV.replace(/<\/tr>/g, "\r\n")

        return CSV;
    }

    $scope.exportToPdf = function(entryType) {
        var table = "";
        if (entryType == 1) {
            table = document.getElementById('stockEntrySaleReport').innerHTML;
        } else if (entryType == 2) {
            table = document.getElementById('stockEntrySaleReport').innerHTML;
        } else if (entryType == 3) {
            table = document.getElementById('physicalWastageTransferReport').innerHTML;
        } else if (entryType == 4) {
            table = document.getElementById('physicalWastageTransferReport').innerHTML;
        } else if (entryType == 5) {
            table = document.getElementById('transferReport').innerHTML;
        } else if (entryType == 6) {
            table = document.getElementById('physicalWastageTransferReport').innerHTML;
        } else if (entryType == 7) {
            table = document.getElementById('physicalWastageTransferReport').innerHTML;
        } else if (entryType == 10) {
            table = document.getElementById('physicalWastageTransferReport').innerHTML;
        } else if (entryType == 11) {
            table = document.getElementById('physicalWastageTransferReport').innerHTML;
        } else if (entryType == 13) {
            table = document.getElementById('physicalWastageTransferReport').innerHTML;
        }
        var printer = window.open('', '', 'width=600,height=600');
        printer.document.open("text/html");
        printer.document.write(table);
        printer.document.close();
        printer.focus();
        printer.print();
        printer.close();
    };






    $scope.exportToExcel = function(entryType) {
        if (entryType == 1) {
            console.log('$scope.editEntryForm.reportData', $scope.editEntryForm.reportData);
            var html = stockEntryHTML($scope.editEntryForm.reportData);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'StockEntryReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('stockEntrySaleReport').innerHTML));  
        } else if (entryType == 2) {
            var html = stockSaleHTML($scope.editEntryForm.reportData);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'StockSaleReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('stockEntrySaleReport').innerHTML));
        } else if (entryType == 3) {
            var html = PWSHTML($scope.editEntryForm.rptPhysicalWastageTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'PhysicalStockReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        } else if (entryType == 4) {
            var html = PWSHTML($scope.editEntryForm.rptPhysicalWastageTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'WastageReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        } else if (entryType == 5) {
            var html = storetransferSHTML($scope.editEntryForm.rptTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'StockTransferReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        } else if (entryType == 6) {
            var html = PWSHTML($scope.editEntryForm.rptPhysicalWastageTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'OpeningStockReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        } else if (entryType == 7) {
            var html = PWSHTML($scope.editEntryForm.rptPhysicalWastageTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'IntermediateEntryReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        } else if (entryType == 10) {
            var html = PWSHTML($scope.editEntryForm.rptPhysicalWastageTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'IntermediateOpeningReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        } else if (entryType == 11) {
            var html = PWSHTML($scope.editEntryForm.rptPhysicalWastageTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'FinishedFoodEntryReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        } else if (entryType == 13) {
            var html = PWSHTML($scope.editEntryForm.rptPhysicalWastageTransfer);
            var csvString = $scope.convertToCSV();
            angular.element(document.getElementById('excel'))
                .attr('href', 'data:application/octet-stream;charset=utf-8,' + encodeURI(csvString))
                .attr('download', 'FinishedFoodOpeningReport.csv');
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('physicalWastageTransferReport').innerHTML));
        }
    };


}]);
