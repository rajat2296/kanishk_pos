'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('requirementReports', {
        url: '/requirementReports',
        templateUrl: 'app/stock/stockReports/requirementReports/requirementReports.html',
        controller: 'RequirementReportsCtrl'
      });
  });