'use strict';

describe('Controller: RequirementReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var RequirementReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RequirementReportsCtrl = $controller('RequirementReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
