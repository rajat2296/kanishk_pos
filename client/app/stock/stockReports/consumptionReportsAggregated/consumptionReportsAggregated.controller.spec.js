'use strict';

describe('Controller: ConsumptionReportsCtrlAggr', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var ConsumptionReportsCtrlAggr, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ConsumptionReportsCtrlAggr = $controller('ConsumptionReportsCtrlAggr', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
