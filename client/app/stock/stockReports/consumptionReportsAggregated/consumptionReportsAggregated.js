'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('consumptionReportsAggregated', {
        url: '/consumptionReportsAggregated',
        templateUrl: 'app/stock/stockReports/consumptionReportsAggregated/consumptionReportsAggregated.html',
        controller: 'ConsumptionReportsCtrlAggr'
      });
  });