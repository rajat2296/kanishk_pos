'use strict';

angular.module('posistApp')
  .controller('ConsumptionReportsCtrlAggr',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','StockReport','localStorageService','stockItem','stockReportResetTime','baseKitchenItem','baseKitchenUnit', 'deployment', 'Utils', 'stockConsumptionReport','intercom', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,StockReport,localStorageService,stockItem,stockReportResetTime,baseKitchenItem,baseKitchenUnit, deployment, Utils, stockConsumptionReport, intercom) {

    currentUser.deployment_id=localStorageService.get('deployment_id');
    $scope.stores=[];
    $scope.user=currentUser;
    $scope.stockitems=[];
    $scope.lastPrice=[];
    //    function getStores_ByDeployment(){
    //      return store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    //    };
    //    var allPromise=$q.all([
    //        getStores_ByDeployment()
    // ]);
    //    allPromise.then(function (value){
    //        $scope.stores = value[0];
    //        $scope.purchaseConsumptionForm.stores=$scope.stores;
    //        //$scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0]._id;
    // });
    var stockAggregateService = $resource('/api/stockAggregate/:controller/:id',
      {
        id: '@_id'
      },
      {
        getData: {method: 'GET', params: {controller: 'getData'}, isArray: false}
      }
    );

    function getRawPricing() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeConsumptionReport({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /* return StockReport.RawMaterialPricing_Receipe({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    };

    function getRawPricingBasekitchen() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeBasekitchenConsumptionReport({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /*  return StockReport.RawMaterialPricing_ReceipeBasekitchen({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    }

    store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
      $scope.stores=result;
      $scope.purchaseConsumptionForm.stores=result;
      $scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0];
      $scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
      stockItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.stockitems=result;
        baseKitchenItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (items){
          _.forEach(items,function(itm,i){
            $scope.stockitems.push(itm);
          });
          //console.log("basekitchenitems : ",items);
          $scope.basekitchenitems=items;
        });
      });

      $q.all([
          getRawPricing(),
          getRawPricingBasekitchen()
        ]).then(function (value) {
            $scope.lastPrice=value[0];
            _.forEach(value[1], function (price) {
                $scope.lastPrice.push(price);
            });      
            console.log($scope.lastPrice);
        }).catch(function (err) {
            console.log('Error fetching base kitchen items');
        });

      // stockReportResetTime.getLastPriceOfItem_RawMaterial({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
      //   $scope.lastPrice=result;
      //   console.log($scope.lastPrice);
      // });
      stockReportResetTime.getRecipeQtyInBase({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
        $scope.recipeQty=result;
        //console.log($scope.recipeQty);
      });
    });

    function getYesterDayDate() {
      var d=new Date();
      d.setDate(d.getDate()-1);
      return d;
    };

    function parseDate(str){
      var d=$filter('date')(str, 'dd-MMM-yyyy');
      return d;
    };

    $scope.purchaseConsumptionForm={
      //fromDate:getYesterDayDate(),
      fromDate: new Date(new Date().setDate(new Date().getDate() - 2)),
      toDate: new Date(new Date().setDate(new Date().getDate() - 2)),
      maxDate: new Date(new Date().setDate(new Date().getDate() - 2))
    };

    //$scope.purchaseConsumptionForm.stores=angular.copy($scope.stores);
    $scope.checkDateGreater= function(date){
      var d = new Date(date);
      var dd = new Date($scope.purchaseConsumptionForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.purchaseConsumptionForm.fromDate= new Date();//getYesterDayDate();
      }
    };

    $scope.checkDateLesser= function(date){
      var d=new Date(date);
      var dd=new Date($scope.purchaseConsumptionForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.purchaseConsumptionForm.toDate= new Date();
      }
    };

    $scope.clearPurchaseConsumptionTab=function(){
      $scope.purchaseConsumptionForm={
        fromDate: new Date(new Date().setDate(new Date().getDate() - 2)),
        toDate: new Date(new Date().setDate(new Date().getDate() - 2)),
        maxDate: new Date(new Date().setDate(new Date().getDate() - 2)),
        stores:angular.copy($scope.stores)
      };
      $scope.purchaseConsumptionForm.store=$scope.purchaseConsumptionForm.stores[0];
      $scope.bindCategory_purchaseConsumptionForm($scope.purchaseConsumptionForm.store);
    };

    function compare(a,b) {
      if (a.itemName < b.itemName)
        return -1;
      if (a.itemName > b.itemName)
        return 1;
      return 0;
    };

    $scope.bindCategory_purchaseConsumptionForm=function(store){
      $scope.purchaseConsumptionForm.categories=[];
      $scope.purchaseConsumptionForm.availableItems=[];
      $scope.purchaseConsumptionForm.item=undefined;
      var str=angular.copy(store);
      _.forEach(str.category, function(c,i){
        $scope.purchaseConsumptionForm.categories.push(c);
      });
    };

    $scope.bindItems_purchaseConsumptionForm= function(category){
      var cat=angular.copy(category);
      $scope.purchaseConsumptionForm.availableItems=[];
      $scope.purchaseConsumptionForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.purchaseConsumptionForm.availableItems.push(c);
        });
      }
      $scope.purchaseConsumptionForm.availableItems.sort(compare);
    };

    function getItemForSearch(stores){
      var item=[];
      _.forEach(stores.category,function(c,i){
        _.forEach(c.item, function(itm,ii){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
      });
      _.forEach($scope.basekitchenitems,function(itm){
        var obj={id:itm.itemId,itemName:itm.itemName};
        item.push(obj);
      });
      return item;
    };

    function getCategoryForSearch(stores){
      var item=[];
      _.forEach(stores.category,function(c,i){
        var obj={_id:c._id};
        item.push(obj);
      });
      return item;
    };

    $scope.openEmailModal = function (report) {
      var modalInstance = $modal.open({
      component: 'modalComponent',
      templateUrl: 'emailModal.html',
      size: 'sm',
      controller: 'EmailModalCtrl'
    });

    modalInstance.result.then(function (email) {
      if(report == 1)
        $scope.startSearch(email);
      if(report == 2)
        $scope.startSearch_stockSummary(email);
      if(report == 3)
        $scope.startSearch_ItemConsumption(email);
    }, function () {
      console.log('modal-component dismissed at: ' + new Date());
    });
    }

    // $scope.startSearch = function(email) {
    //   $scope.purchaseConsumptionForm.isSearch=true;
    //   //console.log(deployment.settings);
    //   var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.fromDate))));
    //   var toDate = new Date($scope.purchaseConsumptionForm.toDate);
    //   var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.toDate))));
    //   toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
    //   //alert(moment(toDateReset).subtract(1, 'months').toDate() < new Date(fromDate));
    //   var chkStart = moment(fromDate);
    //   var chkEnd = moment(toDate);
    //   var duration = moment.duration(chkEnd.diff(chkStart));
    //   if(duration.asMonths() > 1){
    //     growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
    //     $scope.purchaseConsumptionForm.isSearch=false;
    //     return;
    //   }
    //   //console.log(fromDate);
    //   //console.log(toDateReset);
    //   var strId=$scope.purchaseConsumptionForm.store._id;
    //   var isKitchen=$scope.purchaseConsumptionForm.store.isKitchen;
    //   var catId = [];
    //   if($scope.purchaseConsumptionForm.item!=undefined || $scope.purchaseConsumptionForm.item!=null){
    //     var item=[{id:$scope.purchaseConsumptionForm.item._id,itemName:$scope.purchaseConsumptionForm.item.itemName}];
    //   }
    //   else if($scope.purchaseConsumptionForm.category!=undefined || $scope.purchaseConsumptionForm.category!=null){
    //     var item=[];
    //     _.forEach($scope.purchaseConsumptionForm.category.item, function(itm,i){
    //       var obj={id:itm._id,itemName:itm.itemName};
    //       item.push(obj);
    //     });

    //     catId.push({_id:$scope.purchaseConsumptionForm.category._id});
    //   }
    //   else if($scope.purchaseConsumptionForm.store!=undefined){
    //     var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
    //     catId=getCategoryForSearch(angular.copy($scope.purchaseConsumptionForm.store));
    //   };

    //   //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
    //   var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"category":catId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, itemId: item._id, email: email, report: 1};
    //   console.log()
    //   //items:JSON.stringify(item)
    //   //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
    //   stockReportResetTime.getConsumptionSummary(req, function (result){
    //     if(result.errorMessage!=undefined){
    //       $scope.purchaseConsumptionForm.reportData=result;
    //       $scope.purchaseConsumptionForm.isSearch=false;
    //     }
    //     else
    //     {
    //       console.log('result', angular.copy(result));
    //       //spliceandmovePhysical(result,$scope.purchaseConsumptionForm.fromDate)
    //       var result=spliceAllItemsBeforePhysical(result);
    //       console.log('splice', result);
    //       result=calculatePrice(angular.copy(result));
    //       console.log('price', angular.copy(result));
    //       result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
    //       result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
    //       result=formatDataForReport(result,fromDate,toDateReset);
    //       console.log('format', angular.copy(result));
    //       result=spliceForCategoryAndItem(result,item);
    //       console.log('spliceForCategoryAndItem',result);
    //       var res=convertItemInPreferredUnit(result);
    //       //console.log('convertItemInPreferredUnit',JSON.stringify(res));
    //       $scope.purchaseConsumptionForm.reportData=res;
    //       var html= consumptionReport(res.items);
    //       document.getElementById('consumptionReport').innerHTML = html;
    //       $scope.purchaseConsumptionForm.isSearch=false;
    //       console.log(result);
    //       console.log(result);
    //       // $scope.purchaseConsumptionForm.reportData=result;
    //       // $scope.purchaseConsumptionForm.isSearch=false;
    //     }
    //   });
    // };

    $scope.startSearch = function(email) {
      intercom.registerEvent('StockReports');
      $scope.purchaseConsumptionForm.isSearch=true;
      //console.log(deployment.settings);
      var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.fromDate))));
      var toDate = new Date($scope.purchaseConsumptionForm.toDate);
      var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.purchaseConsumptionForm.toDate))));
      toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
      //alert(moment(toDateReset).subtract(1, 'months').toDate() < new Date(fromDate));
      var chkStart = moment(fromDate);
      var chkEnd = moment(toDate);
      var duration = moment.duration(chkEnd.diff(chkStart));
      if(duration.asMonths() > 1){
        growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
        $scope.purchaseConsumptionForm.isSearch=false;
        return;
      }
      //console.log(fromDate);
      //console.log(toDateReset);
      var strId=$scope.purchaseConsumptionForm.store._id;
      var isKitchen=$scope.purchaseConsumptionForm.store.isKitchen;
      var catId = [];
      if($scope.purchaseConsumptionForm.item!=undefined || $scope.purchaseConsumptionForm.item!=null){
        var item=[{id:$scope.purchaseConsumptionForm.item._id,itemName:$scope.purchaseConsumptionForm.item.itemName}];
      }
      else if($scope.purchaseConsumptionForm.category!=undefined || $scope.purchaseConsumptionForm.category!=null){
        var item=[];
        _.forEach($scope.purchaseConsumptionForm.category.item, function(itm,i){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
        catId.push({_id:$scope.purchaseConsumptionForm.category._id});
      }
      else if($scope.purchaseConsumptionForm.store!=undefined){
        var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
        catId=getCategoryForSearch(angular.copy($scope.purchaseConsumptionForm.store));
      };

      //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));



      //var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"category":catId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, itemId: item._id, email: email, report: 1};
      var req={"startDate":fromDate,"endDate":toDateReset,"store_id":strId, "tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};

      //items:JSON.stringify(item)
      //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
      //stockReportResetTime.getConsumptionSummary(req, function (result){
      stockAggregateService.getData(req, function (result) {
        if(result.errorMessage!=undefined){
          $scope.purchaseConsumptionForm.reportData=result;
          $scope.purchaseConsumptionForm.isSearch=false;
        }
        else
        {

          result.beforeDate.sort(function (a, b) {
            return new Date(a.created) - new Date(b.created);
          });
          result.betweenDate.sort(function (a, b) {
            return new Date(a.created) - new Date(b.created);
          });
          console.log('result', angular.copy(result));
          //spliceandmovePhysical(result,$scope.purchaseConsumptionForm.fromDate)
          var result=spliceAllItemsBeforePhysical(result);
          console.log('splice', angular.copy(result));
          result=calculatePrice(angular.copy(result));
          //console.log('price', angular.copy(result));
          result.beforeDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return a.num - b.num;
          });
          result.betweenDate.sort(function (a, b) {
                if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
                  return new Date(a.created) - new Date(b.created);
                else
                  return b.num - a.num;
          });
          // result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
          // result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
          result=formatDataForReport(result,fromDate,toDateReset);
          console.log('format', angular.copy(result));
          result=spliceForCategoryAndItem(result,item);
          //console.log('spliceForCategoryAndItem',result);
          var res=convertItemInPreferredUnit(result);
          //console.log('convertItemInPreferredUnit',JSON.stringify(res));
          $scope.purchaseConsumptionForm.reportData=res;
          var html= consumptionReport(res.items);
          document.getElementById('consumptionReport').innerHTML = html;
          $scope.purchaseConsumptionForm.isSearch=false;
          // console.log(result);
          // console.log(result);
          // $scope.purchaseConsumptionForm.reportData=result;
          // $scope.purchaseConsumptionForm.isSearch=false;
        }
      });
    };    

    function spliceandmovePhysical(result,toDate){
      _.forEachRight(result.betweenDate,function(b,i){
        if(b.totalBalanceQty!=undefined){
          var d=new Date(toDate);
          //d.setHours(0,0,0,0);

          var cDate=new Date(b.created);
          if(d.getTime()> cDate.getTime()){
            result.beforeDate.push(b);
            result.betweenDate.splice(i,1);
          }
        }
      });
    };
    function spliceAllItemsBeforePhysical(result){
      var data=[];


        var latestPhysical = null;
          var latestPhysicalBetween = null;
          var i = 0;
          var j = 0;
          _.forEach(result.beforeDate, function(r,i){
            if(r.totalBalanceQty!=undefined){
              var d = new Date(r.created);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              //d.setHours(0, 0, 0, 0);
              r.created = new Date(d);
              if(!latestPhysical){
                i = 0;
                latestPhysical = new Date(d);
              } else {
                if(latestPhysical.getTime() == d.getTime()){
                  i++;
                } else if(d.getTime() > latestPhysical.getTime()) {
                  i = 0;
                  latestPhysical = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = i;
              data.push(r);
            };
          });
          _.forEach(data,function(d,i){
            _.forEachRight(result.beforeDate,function(itm,ii){
              if(itm.itemId==d.itemId){
                var phyDate=new Date(d.created);
                var compareDate=new Date(itm.created);
                if(phyDate.getTime()>compareDate.getTime()){
                  result.beforeDate.splice(ii,1);
                }
              }
            });
          });

          // var data2=[];
          _.forEachRight(result.betweenDate,function(r,i){
            if(r.totalBalanceQty!=undefined){
              //console.log(r.created);
              r.actualDate = r.created;
              var d=new Date(r.created);
              //var cdate=new Date($scope.purchaseConsumptionForm.toDate);
              var a = new Date(d);
              a.setHours(0,0,0,0);
              a = new Date(a);
              var b = new Date(d);
              b = (Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(b))));
              //console.log(a)
              //console.log(b)
              //console.log(angular.copy(d))
              if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

              }else {
                //console.log('splice all', d);
                d.setDate(d.getDate() + 1);
              }
              //d.setDate(d.getDate() + 1);
              d = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(d))));
              r.created = new Date(d);
              if(!latestPhysicalBetween){
                j = 0;
                latestPhysicalBetween = new Date(d);
              } else {
                if(latestPhysicalBetween.getTime() == d.getTime()){
                  j++;
                } else if(d.getTime() > latestPhysicalBetween.getTime()) {
                  j = 0;
                  latestPhysicalBetween = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = j;
            };
          });
          return result;

    };

function consumptionReport(data){
var html = '<table class="table  table-curved" border="1" style="width:100%">';
    html+= '<thead>';
    html+='<th style="background-color:#24242D;font-weight:bold;color:#fff"></th>';
                      html+='<th colspan="4" style="background-color:#24242D;font-weight:bold;color:#fff">Opening Balance</th>';
                      html+='<th colspan="3" style="background-color:#24242D;font-weight:bold;color:#fff">Purchase</th>';
                      html+='<th colspan="3" style="background-color:#24242D;font-weight:bold;color:#fff">Normal Consumption</th>';
                      //html+='<th colspan="3" ng-hide="true" style="background-color:#24242D;font-weight:bold;color:#fff">NC Consumption</th>';
                      html+='<th colspan="4" style="background-color:#24242D;font-weight:bold;color:#fff">Closing Balance</th>';
                        html+='</thead>';
                        html+='<tbody>';
                        html+='<tr>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Item Name</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        //html+='<td ng-hide="true" style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        //html+='<td ng-hide="true" style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        //html+='<td ng-hide="true" style="background-color: #abcabc;font-weight: bold;">Value</td>';

                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';
                      html+='</tr>';
                      for(var i in data)
                      {
                        console.log('data',data);
                        html+='<tr>';
                        html+='<td>'+data[i].itemName+'</td>';
                        
                        html+='<td>'+moment(new Date(data[i].openingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+parseFloat(data[i].openingQty).toFixed(3)+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+parseFloat(data[i].openingAmt).toFixed(2)+'</td>';

                        //<!-- <td>{{r.totalPurchaseQty+r.totalTransferQty}}</td> -->
                        html+='<td>'+parseFloat(data[i].purchaseQty).toFixed(3)+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+parseFloat(data[i].purchaseAmount).toFixed(2)+'</td>';
                        //<!-- <td>{{r.totalPurchaseAmount+r.totalTransferAmount}}</td> -->

                        //<!-- <td>{{r.totalSaleQty+r.totalWastageQty}}</td> -->
                        html+='<td>'+parseFloat(data[i].consumptionQty).toFixed(3)+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+parseFloat(data[i].consumptionAmount).toFixed(2)+'</td>';
                        //<!-- <td>{{r.totalSaleAmount+r.totalWastageAmt}}</td> -->

                        //html+='<td ng-hide="true">NA</td>';
                        //html+='<td ng-hide="true">NA</td>';
                        //html+='<td ng-hide="true">NA</td>';
                        html+='<td>'+moment(+new Date(data[i].closingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+parseFloat(data[i].closingQty).toFixed(3)+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+parseFloat(data[i].closingAmount).toFixed(2)+'</td>'
                        html+='</tr>';
                      }
                       html+='</tbody>';
                      html+='</table>';
      return html;
}

function stockSummary(data)
{
  var html='<table class="table table-curved" style="width:100%" border="1">';
                    html+='<thead>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff"></th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="4">Opening Balance</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff" colspan="4">Closing Balance</th>';
                    html+='</thead>';
                    html+='<tbody>';
                      html+='<tr>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Item Name</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Date</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Qty</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Unit</td>';
                        html+='<td style="background-color: #abcabc;font-weight: bold;">Value</td>';
                      html+='</tr>';
                      for(var i in data)
                      {
                      //<tr ng-repeat="r in stockSummaryForm.reportData.items">
                       html+='<tr>';
                        html+='<td>'+data[i].itemName+'</td>';
                        html+='<td>'+moment(+new Date(data[i].openingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+parseFloat(data[i].openingQty).toFixed(3)+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+parseFloat(data[i].openingAmt).toFixed(2)+'</td>';
                        html+='<td>'+moment(+new Date(data[i].closingDate)).format('DD-MM-YYYY')+'</td>';
                        html+='<td>'+parseFloat(data[i].closingQty).toFixed(3)+'</td>';
                        html+='<td>'+data[i].UnitName+'</td>';
                        html+='<td>'+parseFloat(data[i].closingAmount).toFixed(2)+'</td>';
                      html+='</tr>';
                    }
                    html+='</tbody>';
                  html+='</table>';
                  return html;
}

function itemWiseConsumption(data)
{
  var html = "";
  html +='<table class="table table-curved" style="width:100%" border="1">';
                    html+='<thead>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Products</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Consumption</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Unit</th>';
                      html+='<th style="background-color:#24242D;font-weight:bold;color:#fff">Value</th>';
                    html+='</thead>';
                    html+='<tbody>';
                    var s1=0,s2=0,s3=0,s4=0;
                    
                    for(var i in data)
                    {
                      
var total=[];                    //<tbody ng-repeat="r in itemWiseConsumptionForm.reportData">
                      html+='<tr>';
                        html+='<td colspan="4" style="background-color:#abcabc;"><b>'+data[i].itemName+'</b></td>';
                      html+='</tr>';
                      /*<!-- <tr ng-repeat="item in r.receipeDetails">
                        html+='<td>{{item.itemName}}</td>
                        html+='<td>{{item.qty}}</td>
                        html+='<td>{{item.UnitName}}</td>
                        html+='<td>{{item.amount}}</td>
                      </tr> -->*/
                      for(var j in data[i].items)
                      {
                      
                        //html+='<tr ng-hide="item.consumptionQty==='0.000'" ng-repeat="item in r.items">
                      //<!-- <tr ng-repeat="item in r.items"> -->
                        if(data[i].items[j].consumptionQty !== 0.000){
                          html+='<tr>';
                            html+='<td>'+data[i].items[j].itemName+'</td>';
                            // s1+=data[i].items[j].itemName;
                            html+='<td>'+data[i].items[j].consumptionQty+'</td>';
                            //  s2+=data[i].items[j].consumptionQty;
                            html+='<td>'+data[i].items[j].UnitName+'</td>';
                            //  s3+=data[i].items[j].UnitName;
                            html+='<td>'+data[i].items[j].consumptionAmount+'</td>';
                            //s4+=data[i].items[j].consumptionAmount;                         
                          html+='</tr>';
                        }
                      }
                      total=$scope.calculateItemWiseTotal(data[i].items)
                      //<!--<tr ng-repeat-end></tr>-->
                      html+='<tr>';
                        html+='<td style="background-color: #00a3ff"><strong>Total</strong></td>';
                        html+='<td style="background-color: #00a3ff"><b>'+total.consumptionQty+'</b></td>';
                        html+='<td style="background-color: #00a3ff"><b>'+total.UnitName+'</b></td>';
                        html+='<td style="background-color: #00a3ff"><b>'+total.consumptionAmount+'</b></td>';
                      html+='</tr>';
                      total=[];
                  }
                  html+='</tbody>';
                  html+='</table>';
                  return html;
}


function calculatePrice(result){
      //console.log(result);
  _.forEach(result.beforeDate, function(itm,i){
    var index = _.findIndex($scope.lastPrice,{itemId:itm.itemId});
    if(itm.totalOpeningQty!=undefined){
      if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
        if(index>=0){
          itm.totalOpeningAmt=Number(itm.totalOpeningQty*$scope.lastPrice[index].monthAverage);
        }
        else {
          itm.totalOpeningAmt=0.00;
        }
      }
      else {
        if(index>=0){
          itm.totalOpeningAmt=Number(itm.totalOpeningQty * $scope.lastPrice[index].monthAverage);
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
        }
        else{
          itm.totalOpeningAmt=0.00;
        } 
      }
    }
    else if(itm.totalBalanceQty!=undefined){
      if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
        if(index>=0){
          itm.totalBalanceAmt=Number(itm.totalBalanceQty*$scope.lastPrice[index].monthAverage);
        }
        else {
          itm.totalBalanceAmt=0.00;
        }
      }
      else{
        if(index>=0){
          itm.totalBalanceAmt=Number(itm.totalBalanceQty*$scope.lastPrice[index].monthAverage);
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor));
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
        } //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage));
        else{
          itm.totalBalanceAmt=0.00;
        } 
      }
    }
    else if(itm.purchase != undefined && itm.wastage != undefined && itm.consumption != undefined) {
      if(itm.purchaseAmount==undefined || itm.purchaseAmount==0){
    
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.purchaseAmount=Number(itm.purchase*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.purchaseAmount=0.00;
        }
      }
      else{

        if(index>=0){
          itm.purchaseAmount=Number(itm.purchase*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.purchaseAmount=0.00;
        } 
      }

      if(itm.consumptionAmount==undefined || itm.consumptionAmount==0){
    
          if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.consumptionAmount=Number(itm.consumption*$scope.lastPrice[index].monthAverage);
          }
          else{
            itm.consumptionAmount=0.00;
          }
      }
      else{
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.consumptionAmount=Number(itm.consumption*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.consumptionAmount=0.00;
        } 
      }

      if(itm.wastageAmt==undefined || itm.wastageAmt==0){
    
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.wastageAmt=Number(itm.wastage*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.wastageAmt=0.00;
        }
      }
      else{
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.wastageAmt=Number(itm.wastage*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.wastageAmt=0.00;
        } 
      }
    }
  });
  _.forEach(result.betweenDate,function(itm,i){
    console.log(JSON.stringify(itm));
    var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});

    //if(index>=0){
    if(itm.totalOpeningQty!=undefined){
      if(itm.totalOpeningAmt==undefined || itm.totalOpeningAmt==0){
        if(index>=0){
          itm.totalOpeningAmt=Number(itm.totalOpeningQty*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.totalOpeningAmt=0.00;
        }
      }
      else
      {
       if(index>=0){
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalOpeningAmt=parseFloat(itm.totalOpeningQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalOpeningAmt=Number(itm.totalOpeningQty*$scope.lastPrice[index].monthAverage);
        }
        else
        {
          itm.totalOpeningAmt=0.00;
        } 
      }
    }
    else if(itm.totalBalanceQty!=undefined){
      if(itm.totalBalanceAmt==undefined || itm.totalBalanceAmt==0){
        if(index>=0){
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalBalanceAmt=Number(itm.totalBalanceQty*$scope.lastPrice[index].monthAverage);
        }
        else
        {
          itm.totalBalanceAmt=0.00;
        }
      }
      else
      {
       if(index>=0){
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat(parseFloat($scope.lastPrice[index].monthAverage) * parseFloat(itm.ConversionFactor)).toFixed(2);
          //itm.totalBalanceAmt=parseFloat(itm.totalBalanceQty)*parseFloat($scope.lastPrice[index].lastPrice);
          itm.totalBalanceAmt=Number(itm.totalBalanceQty*$scope.lastPrice[index].monthAverage);
        }
        else
        {
          itm.totalBalanceAmt=0.00;
        } 
      }
    }
    else if(itm.purchase != undefined && itm.consumption != undefined && itm.wastage != undefined) {
      if(itm.purchaseAmount==undefined || itm.purchaseAmount==0){
    
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.purchaseAmount=Number(itm.purchase*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.purchaseAmount=0.00;
        }
      }
      else
      {
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.purchaseAmount=Number(itm.purchase*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.purchaseAmount=0.00;
        } 
      }

      if(itm.consumptionAmount==undefined || itm.consumptionAmount==0){
    
          if(index>=0){
            //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
            itm.consumptionAmount=Number(itm.consumption*$scope.lastPrice[index].monthAverage);
          }
          else{
            itm.consumptionAmount=0.00;
          }
      }
      else
      {
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.consumptionAmount=Number(itm.consumption*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.consumptionAmount=0.00;
        } 
      }

      if(itm.wastageAmt==undefined || itm.wastageAmt==0){
    
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.wastageAmt=Number(itm.wastage*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.wastageAmt=0.00;
        }
      }
      else
      {
        if(index>=0){
          //console.log(itm.totalPurchaseQty,$scope.lastPrice[index].monthAverage,itm.ConversionFactor);
          itm.wastageAmt=Number(itm.wastage*$scope.lastPrice[index].monthAverage);
        }
        else{
          itm.wastageAmt=0.00;
        } 
      } 
    }
  });
  return result;
}   

    function formatDataForReport(result,startDate,endDate){
      console.log('result', result);
      result.items = [];
      var endDateToShow = new Date(endDate);
      var startDateToShow = new Date(startDate)
      endDateToShow = endDateToShow.setDate(endDateToShow.getDate() - 1);
      _.forEach(result.beforeDate,function(itm,i){
        if(itm.totalOpeningQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itm.itemName == "Oil (O)"){
            console.log(itm.itemName, itm.totalOpeningQty);
          }
          if(itmIndex<0){
            if(startDate.getTime() < new Date(itm.created).getTime())
              startDateToShow = new Date(itm.created);
            itm.openingQty=Number(itm.totalOpeningQty);
            itm.openingAmt=Number(itm.totalOpeningAmt);
            itm.purchaseQty=Number(0);
            itm.purchaseAmount=Number(0);
            itm.consumptionQty=Number(0);
            itm.consumptionAmount= Number(0);
            itm.closingQty=Number(itm.openingQty);
            itm.closingAmount=Number(itm.openingAmt);
            itm.wastageQty = Number(0);
            itm.totalBalanceQty = Number(0);
            itm.wastageAmt = Number(0);
            itm.varianceQty = Number(0);
            itm.varianceInPercent = Number(0);
            itm.physicalQty = Number(0);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].openingQty=Number(result.items[itmIndex].openingQty+itm.totalOpeningQty);
            result.items[itmIndex].openingAmt=Number(result.items[itmIndex].openingAmt+itm.totalOpeningAmt);

            result.items[itmIndex].closingQty=Number(result.items[itmIndex].closingQty+itm.totalOpeningQty);
            result.items[itmIndex].closingAmount=Number(result.items[itmIndex].closingAmount+itm.totalOpeningAmt);
          }
        }
        else if(itm.totalBalanceQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=Number(itm.totalBalanceQty);
            itm.openingAmt=Number(itm.totalBalanceAmt);
            itm.purchaseQty=Number(0);
            itm.purchaseAmount=Number(0);
            itm.consumptionQty=Number(0);
            itm.consumptionAmount=Number(0);
            itm.closingQty=Number(itm.openingQty);
            itm.closingAmount=Number(itm.openingAmt);
            itm.wastageQty = Number(0);
            itm.totalBalanceQty = Number(0);
            itm.wastageAmt = Number(0);
            itm.varianceQty = Number(0);
            itm.varianceInPercent = Number(0);
            itm.physicalQty = Number(0);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            //console.log('total balance', itm.totalBalanceQty);
            result.items[itmIndex].openingQty=Number(itm.totalBalanceQty);
            result.items[itmIndex].openingAmt=Number(itm.totalBalanceAmt);

            result.items[itmIndex].closingQty=Number(itm.totalBalanceQty);
            result.items[itmIndex].closingAmount=Number(itm.totalBalanceAmt);
          }
        }
        else if(itm.purchase != undefined && itm.consumption != undefined && itm.wastage != undefined) {
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itm.itemName == "Oil (O)"){
            console.log(itm.itemName, itm.openingQty, itm.purchase, itm.consumption, itm.wastage);
          }
          if(itmIndex<0){
            itm.openingQty=Number(itm.purchase - itm.consumption - itm.wastage);
            itm.openingAmt=Number(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt);
            itm.purchaseQty=Number(0);
            itm.purchaseAmount=Number(0);
            itm.consumptionQty=Number(0);
            itm.consumptionAmount=Number(0);
            itm.closingQty=Number(itm.openingQty);
            itm.closingAmount=Number(itm.openingAmt);
            itm.wastageQty = Number(0);
            itm.totalBalanceQty = Number(0);
            itm.wastageAmt = Number(0);
            itm.varianceQty = Number(0);
            itm.varianceInPercent = Number(0);
            itm.physicalQty = Number(0);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            console.log('total balance', result.items[itmIndex].openingQty);
            result.items[itmIndex].openingQty += Number(itm.purchase - itm.consumption - itm.wastage);
            result.items[itmIndex].openingAmt += Number(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt);

            result.items[itmIndex].closingQty += Number(itm.purchase - itm.consumption - itm.wastage);
            result.items[itmIndex].closingAmount += Number(itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt);
            console.log('total balance',result.items[itmIndex].openingQty);            
          } 
        }
      });
      _.forEach(result.betweenDate,function(itm,i){
        
        if(itm.totalBalanceQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          //console.log('itm', itm);
          var cDate=new Date(itm.created);
          //alert(cDate)
          //cDate.setDate(cDate.getDate()-1);
          //cDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate(cDate))));
          //var ed = new Date(endDate);
          //ed = new Date(ed.setDate(ed.getDate()-1));
          //console.log(cDate);
          //console.log(endDate);
          //console.log(ed);
          if(itmIndex<0){
            //itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
            //itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
            itm.openingQty=Number(0);
            itm.openingAmt=Number(0);
            itm.purchaseQty=Number(0);
            itm.purchaseAmount=Number(0);
            itm.consumptionQty=Number(0);
            itm.consumptionAmount=Number(0);
            itm.closingQty=Number(itm.openingQty);
            itm.closingAmount=Number(itm.openingAmt);
            itm.wastageQty = Number(0);
            //itm.totalBalanceQty = parseFloat(0).toFixed(3);
            itm.wastageAmt = Number(0);
            itm.varianceQty = Number(0);
            itm.varianceInPercent = Number(0);
            itm.physicalQty = Number(0);
            if(endDate.getTime()==cDate.getTime()){
              console.log('if', itm.itemName, endDate.getTime(), cDate.getTime());
              itm.physicalQty=Number(itm.totalBalanceQty);
              itm.physicalAmt=Number(itm.totalBalanceAmt);
              itm.closingQty=Number(itm.totalBalanceQty);
            }
            else{
              console.log('else', itm.itemName, endDate.getTime(), cDate.getTime());
              itm.closingQty = Number(itm.totalBalanceQty);
              itm.physicalDate = new Date(itm.created);
              console.log(itm.closingQty);
            }
            if(itm.wastageQty == undefined) {
              itm.wastageQty=Number(0);
              itm.wastageAmount=Number(0);
            }
            if(itm.varianceQty == undefined) {
              itm.varianceQty=parseFloat(0).toFixed(3);
            }
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {
            if(endDate.getTime()==cDate.getTime()){
              result.items[itmIndex].physicalQty=Number(itm.totalBalanceQty);
              result.items[itmIndex].physicalAmt=Number(itm.totalBalanceAmt);
              result.items[itmIndex].varianceQty=Number(result.items[itmIndex].closingQty - result.items[itmIndex].physicalQty);
              result.items[itmIndex].varianceInPercent=Number(result.items[itmIndex].varianceQty/result.items[itmIndex].closingQty*100);

              //result.items[itmIndex].closingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              //result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
            else {
              console.log(itm.itemName, " else");
              console.log(result.items[itmIndex].closingQty);
              //  result.items[itmIndex].openingQty=parseFloat(parseFloat(itm.totalBalanceQty)).toFixed(3);
              if(result.items[itmIndex].physicalDate) {
                console.log(result.items[itmIndex].physicalDate)
                console.log(cDate)
                if (result.items[itmIndex].physicalDate.getTime() == cDate.getTime()) {
                  console.log('if');
                  result.items[itmIndex].closingQty = Number(itm.totalBalanceQty);
                  console.log(result.items[itmIndex].closingQty);
                } else {
                  console.log('else');
                  result.items[itmIndex].closingQty = Number(itm.totalBalanceQty);
                  result.items[itmIndex].physicalDate = cDate;
                }
              } else {
                result.items[itmIndex].closingQty = Number(itm.totalBalanceQty);
                result.items[itmIndex].physicalDate = cDate;
              }
              // result.items[itmIndex].purchaseQty=parseFloat(parseFloat(0)).toFixed(3);
              // result.items[itmIndex].consumptionQty=parseFloat(parseFloat(0)).toFixed(3);
              // result.items[itmIndex].closingAmount=parseFloat(parseFloat(itm.totalBalanceAmt)).toFixed(2);
            }
          }
        }
        else if(itm.purchase != undefined && itm.consumption != undefined && itm.wastage != undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.openingQty=Number(0);
            itm.openingAmt=Number(0);
            itm.purchaseQty=Number(itm.purchase);
            itm.purchaseAmount=Number(itm.purchaseAmount);
            itm.consumptionQty=Number(itm.consumption + itm.wastage);
            itm.consumptionAmount=Number(itm.consumptionAmount + itm.wastageAmt);
            itm.closingQty=Number(itm.openingQty);
            itm.closingAmount=Number(itm.openingAmt);
            itm.wastageQty = Number(itm.wastage);
            itm.totalBalanceQty = Number(0);
            itm.wastageAmt = Number(itm.wastage);
            itm.varianceQty = Number(itm.wastageAmt);
            itm.varianceInPercent = Number(0);
            itm.physicalQty = Number(0);
            itm.openingDate=new Date(startDateToShow);
            itm.closingDate=new Date(endDateToShow);
            result.items.push(itm);
          }
          else
          {

            result.items[itmIndex].purchaseQty=Number(result.items[itmIndex].purchaseQty + itm.purchase);
            result.items[itmIndex].purchaseAmount=Number(result.items[itmIndex].purchaseAmount + itm.purchaseAmount);

            result.items[itmIndex].consumptionQty=Number(result.items[itmIndex].consumptionQty + itm.consumption + itm.wastage);
            result.items[itmIndex].consumptionAmount=Number(result.items[itmIndex].consumptionAmount + itm.consumptionAmount + itm.wastageAmt);

            result.items[itmIndex].closingQty = Number(result.items[itmIndex].closingQty + itm.purchase - itm.consumption - itm.wastage);
            result.items[itmIndex].closingAmount = Number(result.items[itmIndex].closingAmount + itm.purchaseAmount - itm.consumptionAmount - itm.wastageAmt);

          }
        }
      });
      return result;
    };    

    function spliceForCategoryAndItem(result,items, itemId){
      console.log('items',items);
      console.log('result',result);
      _.forEachRight(result.items,function(itm,i){
        var index=_.findIndex(items,{id:itm.itemId});
        if(index<0){
          result.items.splice(i,1);
        }
        else
        {
          itm.itemName=items[index].itemName;
        }
      });
      return result;
    };


    function convertItemInPreferredUnit(result){
      console.log('result cvi',result);
      _.forEach(result.items,function(itm,i){
        var indeex=_.findIndex($scope.stockitems,function(item) {
          return item._id == itm.itemId || item.itemId == itm.itemId;
        });
        if(indeex>=0){
          var item=$scope.stockitems[indeex];
          //console.log('item', item);
          if(item.preferedUnit != undefined){
            console.log(item.itemName, item.preferedUnit);
            if(typeof item.preferedUnit == 'string'){
              _.forEach(item.units,function(u,i){
                if(u._id==item.preferedUnit){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            } else {
              //console.log(item.itemName);
              _.forEach(item.units,function(u,i){
                if(u._id==item.preferedUnit._id){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>conversionFactor){
                      itm.openingQty=parseFloat(parseFloat(itm.openingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)/parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)/parseFloat(pconFac)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.openingxQty=parseFloat(parseFloat(itm.openingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.closingQty=parseFloat(parseFloat(itm.closingQty)*parseFloat(pconFac)).toFixed(3);
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                      itm.purchaseQty=parseFloat(parseFloat(itm.purchaseQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        }
      });
      return result;
    };
    //-------------------------------------stockSummaryForm--------------------------------------------------//
    $scope.clearStockSummaryTab= function(){
      ResetStockSummary();
    };

    function ResetStockSummary(){
      $scope.stockSummaryForm={
        fromDate: new Date(new Date().setDate(new Date().getDate() - 2)),
        toDate: new Date(new Date().setDate(new Date().getDate() - 2)),
        maxDate: new Date(new Date().setDate(new Date().getDate() - 2)),
        stores:angular.copy($scope.stores)
      };
      $scope.stockSummaryForm.store=$scope.stockSummaryForm.stores[0];
      $scope.bindCategory_stockSummaryForm($scope.stockSummaryForm.store);
    };
    $scope.checkDateGreater_stockSummary= function(date){
      var d=new Date(date);
      var dd=new Date($scope.stockSummaryForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.stockSummaryForm.fromDate=new Date();//getYesterDayDate()
      }
    };

    $scope.checkDateLesser_stockSummary= function(date){
      var d=new Date(date);
      var dd=new Date($scope.stockSummaryForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.stockSummaryForm.toDate= new Date();
      }
    };

    $scope.bindCategory_stockSummaryForm=function(store){
      $scope.stockSummaryForm.categories=[];
      $scope.stockSummaryForm.availableItems=[];
      $scope.stockSummaryForm.item=undefined;
      var str=angular.copy(store);
      _.forEach(str.category, function(c,i){
        $scope.stockSummaryForm.categories.push(c);
      });
    };

    $scope.bindItems_stockSummaryForm= function(category){
      $scope.stockSummaryForm.availableItems=[];
      var cat=angular.copy(category);
      $scope.stockSummaryForm.availableItems=[];
      $scope.stockSummaryForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.stockSummaryForm.availableItems.push(c);
        });
      }
      $scope.stockSummaryForm.availableItems.sort(compare);
    };

    // $scope.startSearch_stockSummary=function(email){
    //   $scope.stockSummaryForm.isSearch=true;
    //   var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.stockSummaryForm.fromDate))));
    //   var toDate = new Date($scope.stockSummaryForm.toDate);
    //   var toDateReset = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.stockSummaryForm.toDate))));
    //   toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
    //   //console.log(toDateReset);
    //   //console.log(fromDate);
    //   //console.log(toDate);
    //   //console.log(toDateReset);
    //   var chkStart = moment(fromDate);
    //   var chkEnd = moment(toDate);
    //   var duration = moment.duration(chkEnd.diff(chkStart));
    //   if(duration.asMonths() > 1){
    //     growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
    //     //$scope.purchaseConsumptionForm.isSearch=false;
    //     return;
    //   }
    //   var strId=$scope.stockSummaryForm.store._id;
    //   var isKitchen=$scope.stockSummaryForm.store.isKitchen;
    //   if($scope.stockSummaryForm.item!=undefined || $scope.stockSummaryForm.item!=null){
    //     var item=[{id:$scope.stockSummaryForm.item._id,itemName:$scope.stockSummaryForm.item.itemName}];
    //   }
    //   else if($scope.stockSummaryForm.category!=undefined || $scope.stockSummaryForm.category!=null){
    //     var item=[];
    //     _.forEach($scope.stockSummaryForm.category.item, function(itm,i){
    //       var obj={id:itm._id,itemName:itm.itemName};
    //       item.push(obj);
    //     });
    //   }
    //   else if($scope.stockSummaryForm.store!=undefined){
    //     var item=getItemForSearch(angular.copy($scope.stockSummaryForm.store));
    //   };

    //   //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
    //   var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, report: 2, email: email};
    //   //console.log(req);
    //   //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
    //   stockReportResetTime.getConsumptionSummary(req, function (result){
    //     //console.log(result);
    //     if(result.errorMessage){
    //       $scope.stockSummaryForm.reportData = {};
    //       $scope.stockSummaryForm.reportData.errorMessage = result.errorMessage;
    //       return;
    //     }
    //     var result=spliceAllItemsBeforePhysical(result);
    //     result=calculatePrice(result);
    //     //console.log(result);
    //     result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
    //     result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
    //     result=formatDataForReport(result,fromDate,toDateReset);
    //     //console.log(result);
    //     result=spliceForCategoryAndItem(result,item);
    //     var res=convertItemInPreferredUnit(result);
    //     $scope.stockSummaryForm.reportData=res;
    //     var html=stockSummary(res.items);
    //     $scope.stockSummaryForm.isSearch=false;
    //     document.getElementById('stockSummary').innerHTML=html;
    //     console.log(result);
    //     // $scope.stockSummaryForm.reportData = {};
    //     // $scope.stockSummaryForm.reportData.successMessage = result.successMessage;
    //     return;
    //   });
    // };

    $scope.startSearch_stockSummary=function(email){
      intercom.registerEvent('StockReports');
      $scope.stockSummaryForm.isSearch=true;
      var fromDate=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.stockSummaryForm.fromDate))));
      var toDate = new Date($scope.stockSummaryForm.toDate);
      var toDateReset = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.stockSummaryForm.toDate))));
      toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
      //console.log(toDateReset);
      //console.log(fromDate);
      //console.log(toDate);
      //console.log(toDateReset);
      var chkStart = moment(fromDate);
      var chkEnd = moment(toDate);
      var duration = moment.duration(chkEnd.diff(chkStart));
      if(duration.asMonths() > 1){
        growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
        //$scope.purchaseConsumptionForm.isSearch=false;
        return;
      }
      var strId=$scope.stockSummaryForm.store._id;
      var isKitchen=$scope.stockSummaryForm.store.isKitchen;
      if($scope.stockSummaryForm.item!=undefined || $scope.stockSummaryForm.item!=null){
        var item=[{id:$scope.stockSummaryForm.item._id,itemName:$scope.stockSummaryForm.item.itemName}];
      }
      else if($scope.stockSummaryForm.category!=undefined || $scope.stockSummaryForm.category!=null){
        var item=[];
        _.forEach($scope.stockSummaryForm.category.item, function(itm,i){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
      }
      else if($scope.stockSummaryForm.store!=undefined){
        var item=getItemForSearch(angular.copy($scope.stockSummaryForm.store));
      };

      //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));


      //var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"isKitchen":isKitchen,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, resetTime: true, report: 2, email: email};
      var req={"startDate":fromDate,"endDate":toDateReset,"store_id":strId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};

      //console.log(req);
      //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
      stockAggregateService.getData(req, function (result){
        //console.log(result);
        if(result.errorMessage){
          $scope.stockSummaryForm.reportData = {};
          $scope.stockSummaryForm.reportData.errorMessage = result.errorMessage;
          return;
        }
        result.beforeDate.sort(function (a, b) {
          return new Date(a.created) - new Date(b.created);
        });
        result.betweenDate.sort(function (a, b) {
          return new Date(a.created) - new Date(b.created);
        });
        var result=spliceAllItemsBeforePhysical(result);
        result=calculatePrice(result);
        //console.log(result);
        // result.beforeDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        // result.betweenDate.sort(function(a,b){return new Date(a.created) - new Date(b.created);});
        result.beforeDate.sort(function (a, b) {
          if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
            return new Date(a.created) - new Date(b.created);
          else
            return a.num - b.num;
        });
        result.betweenDate.sort(function (a, b) {
          if(new Date(a.created).getTime() - new Date(b.created).getTime() != 0)
            return new Date(a.created) - new Date(b.created);
          else
            return b.num - a.num;
        });
        result=formatDataForReport(result,fromDate,toDateReset);
        //console.log(result);
        result=spliceForCategoryAndItem(result,item);
        var res=convertItemInPreferredUnit(result);
        $scope.stockSummaryForm.reportData=res;
        var html=stockSummary(res.items);
        $scope.stockSummaryForm.isSearch=false;
        document.getElementById('stockSummary').innerHTML=html;
        console.log(result);
        // $scope.stockSummaryForm.reportData = {};
        // $scope.stockSummaryForm.reportData.successMessage = result.successMessage;
        return;
      });
    };

    //-------------------------------------itemWiseConsumptionForm----------------------------------------------
    $scope.clearItemWiseTab= function(){
      ResetItemWiseConsumption();
    };

    function ResetItemWiseConsumption(){
      $scope.itemWiseConsumptionForm={
        fromDate:new Date(),//getYesterDayDate(),
        toDate:new Date(),
        maxDate:new Date(),
        stores:angular.copy($scope.stores)
      };
      $scope.itemWiseConsumptionForm.store=$scope.itemWiseConsumptionForm.stores[0];
      $scope.bindCategory_itemWiseConsumption($scope.itemWiseConsumptionForm.store);

    };
    $scope.checkDateGreater_itemWiseConsumption= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseConsumptionForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.itemWiseConsumptionForm.fromDate=new Date();//getYesterDayDate()
      }
    };

    $scope.checkDateLesser_itemWiseConsumption= function(date){
      var d=new Date(date);
      var dd=new Date($scope.itemWiseConsumptionForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.itemWiseConsumptionForm.toDate= new Date();
      }
    };

    $scope.bindCategory_itemWiseConsumption=function(store){
      $scope.itemWiseConsumptionForm.categories=[];
      $scope.itemWiseConsumptionForm.availableItems=[];
      $scope.itemWiseConsumptionForm.item=undefined;
      var str=angular.copy(store);
      _.forEach(str.category, function(c,i){
        $scope.itemWiseConsumptionForm.categories.push(c);
      });
    };

    $scope.bindItems_itemWiseConsumption= function(category){
      var cat=angular.copy(category);
      $scope.itemWiseConsumptionForm.availableItems=[];
      $scope.itemWiseConsumptionForm.item=undefined;
      if(cat!=null){
        _.forEach(cat.item, function(c,i){
          $scope.itemWiseConsumptionForm.availableItems.push(c);
        });
      }
      $scope.itemWiseConsumptionForm.availableItems.sort(compare);
    };

    $scope.startSearch_ItemConsumption=function(email){
      intercom.registerEvent('StockReports');
      var fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseConsumptionForm.fromDate))));
      var toDate = new Date($scope.itemWiseConsumptionForm.toDate);
      var toDateReset=new Date(Utils.getDateFormatted(Utils.getResetDate(deployment.settings, Utils.convertDate($scope.itemWiseConsumptionForm.toDate))));
      toDateReset = new Date(toDateReset.setDate(toDateReset.getDate() + 1));
      console.log(fromDate);
      console.log(toDateReset);
      var chkStart = moment(fromDate);
      var chkEnd = moment(toDate);
      var duration = moment.duration(chkEnd.diff(chkStart));
      if(duration.asMonths() > 1){
        growl.error("Please Select a shorter date range (Less than or equal to 1 month)", {ttl: 3000});
        //$scope.purchaseConsumptionForm.isSearch=false;
        return;
      }
      var strId=$scope.itemWiseConsumptionForm.store._id;
      var isKitchen = $scope.itemWiseConsumptionForm.store.isKitchen;
      if($scope.itemWiseConsumptionForm.item!=undefined || $scope.itemWiseConsumptionForm.item!=null){
        var item=[{id:$scope.itemWiseConsumptionForm.item._id,itemName:$scope.itemWiseConsumptionForm.item.itemName}];
      }
      else if($scope.itemWiseConsumptionForm.category!=undefined || $scope.itemWiseConsumptionForm.category!=null){
        var item=[];
        _.forEach($scope.itemWiseConsumptionForm.category.item, function(itm,i){
          var obj={id:itm._id,itemName:itm.itemName};
          item.push(obj);
        });
      }
      else if($scope.purchaseConsumptionForm.store!=undefined){
        var item=getItemForSearch(angular.copy($scope.itemWiseConsumptionForm.store));
      };

      //var item=getItemForSearch(angular.copy($scope.purchaseConsumptionForm.store));
      //var req={"fromDate":fromDate,"toDate":toDate,"storeId":strId,"items":JSON.stringify(item),"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id};

      var req={"fromDate":fromDate,"toDate":toDateReset,"storeId":strId,"tenant_id":currentUser.tenant_id,"deployment_id":currentUser.deployment_id, isKitchen: isKitchen, resetTime: true, email: email, report: 3};
      //console.log(req);
      //StockReport.getConsumptionSummary(req, function (result){
      // StockReport.getConsumptionSummary_ItemReport(req, function (result){
      //  $scope.itemWiseConsumptionForm.reportData=FormatItemData(result);
      //  console.log(result);
      // });
      //stockConsumptionReport.getConsumptionSummaryMailer(req, function (result){
      stockReportResetTime.getConsumptionSummary(req, function (result){
        //console.log(angular.copy(result));
        if(result.errorMessage){
          $scope.itemWiseConsumptionForm.reportData = {};
          $scope.itemWiseConsumptionForm.reportData.errorMessage = result.errorMessage;
          return;
        }
        var result=spliceAllItemsBeforePhysical(result);
        result=calculatePrice(result);
        console.log(angular.copy(result));
        var data=formatDataForItemReport(result);
        console.log(angular.copy(data));
        var d=getItemdetails(data);
        console.log(angular.copy(d));
        d=convertItemInPreferredUnit_ItemReport_New(d);
        console.log(angular.copy(d));
        d=spliceForCategoryAndItem_ItemReport(d,item);
        console.log(angular.copy(d));
        // var rItems=getUniqueRawItems(result);
        // console.log(rItems);
        // var uItems=getUniqueItems(result);
        // console.log(uItems);
        // var calculatedItem=calculatePrice_ItemReport(result);
        // var inpfUnit=convertItemInPreferredUnit_ItemReport(calculatedItem);
        // var itemWithRecipeDetail=getItemsReceipeDetails(result,uItems);
        // console.log(inpfUnit);
        // console.log(calculatedItem);
        // console.log(result);
        // $scope.itemWiseConsumptionForm.reportData=itemWithRecipeDetail;
        $scope.itemWiseConsumptionForm.reportData=d;
        $scope.itemWiseConsumptionForm.isSearch=false;
        var html=itemWiseConsumption(d);
        console.log(document.getElementById('itemWiseConsumption').innerHTML);
        console.log(itemWiseConsumption(d));
        document.getElementById('itemWiseConsumption').innerHTML=itemWiseConsumption(d);
        // $scope.itemWiseConsumptionForm.reportData = {};
        // $scope.itemWiseConsumptionForm.reportData.successMessage = result.successMessage;
        return;
      });
    };

    function spliceForCategoryAndItem_ItemReport(d,items){
      _.forEachRight(d,function(itm,i){
        var index=_.findIndex(items,{id:itm.itemId});
        if(index<0){
          d.splice(i,1);
        }
        else
        {
          itm.itemName=items[index].itemName;
        }
      });
      return d;
    };


    function getItemdetails(data){
      var items=data.items;
      console.log(data.items);
      _.forEach(items,function(it,i){
        it.items=[];
        if(it.isBaseKitchenItem)
          it.UnitName = it.baseUnit;
        //it.items.push(angular.copy(it));
        _.forEach(data.betweenDate,function(itm,i){
          //console.log('---------------------------------------------------');
          //console.log(itm);
         // console.log('itm',itm);
          if(itm.isBaseKitchenItem)
            console.log("BASE");
          //alert(itm.itemName);
          if(itm.itemId==it.itemId && itm.menuId == undefined){
            if(itm.totalSaleQty!=undefined){
              //if(itm.menuId!=undefined){
                console.log('itm.itemName',itm.itemName);
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                {
                  console.log('base itm',itm)
                  obj.UnitName=itm.UnitName;
                }
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                it.items.push(obj);
              }
              else {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
              //}
            }
            else if(itm.totalWastageQty!=undefined){
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.UnitName;
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

              }
              it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
              //}
            }
            else if(itm.totalTransferQty!=undefined){
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.baseUnit;
                else obj.UnitName=itm.baseUnit;
                obj.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
              //}
            }
            else if(itm.totalStockReturnQty!=undefined){
              //if(itm.menuId!=undefined){
              var itmIndex=_.findIndex(it.items,function (item){
                return item.itemId == itm.itemId;
              });
              if(itmIndex<0){
                var obj={};
                obj.itemId=itm.itemId;
                obj.itemName=itm.itemName;
                if(itm.isBaseKitchenItem)
                  obj.UnitName=itm.UnitName;
                else obj.UnitName=itm.UnitName;
                obj.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                obj.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                it.items.push(obj);
              }
              else
              {
                it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              }

              it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
              it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
              //}
            }
            /*else if(itm.totalStockReceiveQty!=undefined){
                 // console.log('itm in tsr',itm);
              //if(itm.menuId!=undefined){
            var itmIndex=_.findIndex(it.items,function (item){
              return item.itemId == itm.itemId;
            });
            if(itmIndex<0){
              var obj={};
              obj.itemId=itm.itemId;
              obj.itemName=itm.itemName;
              if(itm.isBaseKitchenItem)
                obj.UnitName=itm.UnitName;
              else obj.UnitName=itm.UnitName;
              obj.consumptionQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
              obj.consumptionAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
              it.items.push(obj);
            }
            else
            {
              it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
              it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
            }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
            //}
              }*/
              else if(itm.totalIntermediateQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                //}
              }
              else if(itm.itemSaleQty!=undefined){
                if(itm.recipeId!=undefined){
                  //alert(itm.recipeId);
                  //var itmIndex=_.findIndex(it.items,{itemId:itm.recipeId});
                  //if(itmIndex<0){
                  alert(itm.baseUnit);
                  var obj={};
                  obj.itemId=itm.itemId;
                  obj.itemName=itm.recipeName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.baseUnit;
                  obj.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                  it.items.push(obj);
                  //}
                  //else
                  //{
                  //  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  //  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);
                  //}
                  it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);

                  //it.items[0].consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                  //it.items[0].consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
                }
              }
            } else if(itm.itemId==it.itemId && itm.menuId != undefined){
              if(itm.totalSaleQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
                }
                it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalSaleQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalSaleAmount)).toFixed(2);
                //}
              }
              else if(itm.totalWastageQty!=undefined){
                console.log('itmWastage',itm);
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);

                }
                it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)-parseFloat(itm.totalWastageQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)-parseFloat(itm.totalWastageAmt)).toFixed(2);
                //}
              }
              else if(itm.totalTransferQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.baseUnit;
                  obj.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
                //}
              }
              else if(itm.totalStockReturnQty!=undefined){
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.UnitName;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalStockReturnQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalStockReturnAmount)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalStockReturnAmount)).toFixed(2);
                //}
              }
              else if(itm.totalIntermediateQty!=undefined){
               
                //if(itm.menuId!=undefined){
                var itmIndex=_.findIndex(it.items,function (item){
                  return item.menuId == itm.menuId;
                });
                if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.menuName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.UnitName;
                  obj.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                  it.items.push(obj);
                }
                else
                {
                  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                }

                it.consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                it.consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                //}
              }
              else if(itm.itemSaleQty!=undefined){
                if(itm.recipeId!=undefined){
                  //alert(itm.recipeId);
                  //var itmIndex=_.findIndex(it.items,{itemId:itm.recipeId});
                  //if(itmIndex<0){
                  var obj={};
                  obj.itemId=itm.menuId;
                  obj.itemName=itm.recipeName;
                  if(itm.isBaseKitchenItem)
                    obj.UnitName=itm.baseUnit;
                  else obj.UnitName=itm.baseUnit;
                  obj.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                  obj.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                  it.items.push(obj);
                  //}
                  //else
                  //{
                  //  it.items[itmIndex].consumptionQty=parseFloat(parseFloat(it.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  //  it.items[itmIndex].consumptionAmount=parseFloat(parseFloat(it.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);
                  //}
                  it.consumptionQty=parseFloat(parseFloat(it.consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                  it.consumptionAmount=parseFloat(parseFloat(it.consumptionAmount)+parseFloat(itm.itemSaleAmount)).toFixed(2);

                  //it.items[0].consumptionQty=parseFloat(parseFloat(it.items[0].consumptionQty)-parseFloat(itm.totalTransferQty)).toFixed(3);
                  //it.items[0].consumptionAmount=parseFloat(parseFloat(it.items[0].consumptionAmount)-parseFloat(itm.totalTransferAmount)).toFixed(2);
                }
              }
            }
          });
        });
        return items;
    };

    $scope.calculateItemWiseTotal = function(items) {
      var consumptionQty = 0;
      var consumptionAmount = 0;
      var UnitName = "";

      _.forEach(items, function (item) {
        consumptionQty += Utils.roundNumber(item.consumptionQty, 3);
        consumptionAmount += Utils.roundNumber(item.consumptionAmount, 3);
        UnitName = item.UnitName;
      });

      return {
        consumptionQty: parseFloat(consumptionQty).toFixed(3),
        consumptionAmount: parseFloat(consumptionAmount).toFixed(3),
        UnitName: UnitName
      };
    };

    function getUniqueRawItems(result){
      var items=[];
      _.forEach(result.beforeDate,function(r,i){
        var index=_.findIndex(items,{itemId:r.itemId});
        if(index<0){
          var obj={itemId:r.itemId,itemName:r.itemName};
          items.push(obj);
        }
      });
      _.forEach(result.betweenDate,function(r,i){
        var index=_.findIndex(items,{itemId:r.itemId});
        if(index<0){
          var obj={itemId:r.itemId,itemName:r.itemName};
          items.push(obj);
        }
      });
      return items;
    };

    function getUniqueItems(result){
      var items=[];
      _.forEach(result,function(r,i){
        var index=_.findIndex(items,{itemId:r.itemId});
        if(index<0){
          var obj={itemId:r.itemId,itemName:r.itemName};
          items.push(obj);
        }
        var rIndex=_.findIndex($scope.recipeQty,{itemId:r.itemId});
        if(rIndex>=0){
          r.totalSaleQtyinbase=parseFloat(r.totalSaleQtyinbase)/parseFloat($scope.recipeQty[rIndex].receipeQtyinBase);
          r.rawQuantityUsed=parseFloat(r.rawQuantity)*parseFloat(r.totalSaleQtyinbase);
          if(r.semiRawMaterialsQty!=undefined){
            r.semiRawItems=[];
            _.forEachRight(r.semiRawMaterialsQty,function(sr,ii){
              sr=parseFloat(sr)*parseFloat(r.totalSaleQtyinbase);
              r.semiRawMaterialsQty[ii]=sr;
              var obj={itemId:r.semiRawMaterialsId[ii],itemName:r.semiRawMaterials[ii],qty:r.semiRawMaterialsQty[ii]};
              r.semiRawItems.push(obj);
            });
            delete r.semiRawMaterials;
            delete r.semiRawMaterialsId;
            delete r.semiRawMaterialsQty;
          }
        }
      });
      return items;
    };

    function calculatePrice_ItemReport(result){
      _.forEach(result,function(r,i){
        r.rawAmount=parseFloat(0).toFixed(2);
        if(r.isSemi==true){
          if(r.semiRawItems!=undefined){
            var t=0;
            _.forEach(r.semiRawItems,function(itm,ii){
              var index=_.findIndex($scope.lastPrice,{itemId:itm.itemId});
              if(index>=0){
                t=parseFloat(t)+parseFloat(itm.qty)*parseFloat($scope.lastPrice[index].lastPrice);
              }
            });
            r.rawAmount=parseFloat(t).toFixed(2);
          }
        }
        else
        {
          var index=_.findIndex($scope.lastPrice,{itemId:r.rawId});
          if(r.rawQuantityUsed!=undefined){
            if(index>=0){
              r.rawAmount=parseFloat(parseFloat(r.rawQuantityUsed)*parseFloat($scope.lastPrice[index].lastPrice)).toFixed(2);
            }
          }
        }
      });
      return result;
    };

    function convertItemInPreferredUnit_ItemReport(result){
      _.forEach(result,function(r,i){
        var indeex=_.findIndex($scope.stockitems,{_id:r.rawId});
        if(indeex>=0){
          var item=$scope.stockitems[indeex];
          if(item.preferedUnit!=undefined){
            _.forEach(item.units,function(u,i){
              if(u._id==item.preferedUnit){
                var conversionFactor=1;
                var pconFac=parseFloat(u.conversionFactor);
                r.rawUnitName=u.unitName;
                if(u.baseUnit.id == 2 || u.baseUnit.id == 3){
                  conversionFactor=parseFloat(u.baseConversionFactor);
                  if(pconFac>conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)/parseFloat(conversionFactor)).toFixed(3);
                  }
                  else if(pconFac<conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)*parseFloat(conversionFactor)).toFixed(3);
                  }
                }
                else
                {
                  if(pconFac>conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)/parseFloat(pconFac)).toFixed(3);
                  }
                  else if(pconFac<conversionFactor){
                    r.rawQuantityUsed=parseFloat(parseFloat(r.rawQuantityUsed)*parseFloat(pconFac)).toFixed(3);
                  }
                }
              }
            });
          }
        }
      });
      return result;
    };

    function convertItemInPreferredUnit_ItemReport_New(result){
      console.log($scope.stockitems);
      console.log(result);
      _.forEach(result,function(r,ii){
        _.forEach(r.items,function(itm, i){
          //alert(itm.itemName + " " + itm.UnitName)
          var indeex=_.findIndex($scope.stockitems,{_id:itm.itemId});
          console.log(indeex, itm.itemName);
          if(indeex>=0){
            var item=$scope.stockitems[indeex];
            console.log('item 101',item)
            var unitId;
            if(typeof item.preferedUnit == Object){
              console.log(item.itemName);
              unitId = item.preferedUnit._id;
            } else{
              console.log('else', item.itemName);
              unitId = item.preferedUnit;
            }
            if(item.preferedUnit!=undefined){
              _.forEach(item.units,function(u,i){
                if(u._id==unitId){
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  //console.log(itm);
                  console.log(u);
                  itm.UnitName=u.unitName;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>=conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
            else
            {
            _.forEach(item.units,function(u,i){
                
                  var conversionFactor=1;
                  var pconFac=parseFloat(u.conversionFactor);
                  //console.log(itm);
                  console.log(u);
                  itm.UnitName=u.baseUnit.name;
                  if(u.baseUnit.id==2 || u.baseUnit.id==3){
                    conversionFactor=parseFloat(u.baseConversionFactor);
                    if(pconFac>conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)/parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else
                  {
                    if(pconFac>=conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)).toFixed(3);
                    }
                    else if(pconFac<conversionFactor){
                      itm.UnitName= u.baseUnit.name;
                      itm.consumptionQty=parseFloat(parseFloat(itm.consumptionQty)*parseFloat(pconFac)).toFixed(3);
                    }
                  }
                
              });
            }
          }
        });
      });
      return result;
    };
    function getItemsReceipeDetails(result,items){
      _.forEach(items,function(itm,i){
        itm.receipeDetails=[];
        _.forEach(result,function(r,ii){
          if(r.itemId==itm.itemId){
            //var index=_.findIndex(itm.receipeDetails,{rawId:r.rawId});
            var index=_.findIndex(itm.receipeDetails,{menuId:r.menuId});
            if(index<0){
              var obj={menuId:r.menuId,itemName:r.menuName,qty:parseFloat(r.rawQuantityUsed).toFixed(3),amount:parseFloat(r.rawAmount).toFixed(2),UnitName:r.rawUnitName};
              itm.receipeDetails.push(obj);
            }
            else
            {
              itm.receipeDetails[index].qty=parseFloat(parseFloat(itm.receipeDetails[index].qty)+parseFloat(r.rawQuantityUsed)).toFixed(3);
            }
          }
        });
      });
      return items;
    };

    function formatDataForItemReport(result){
      //console.log('result', result);
      result.items=[];
      //_.forEach(result.beforeDate,function(itm,i){
      // if(itm.totalOpeningQty!=undefined){
      //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //   if(itmIndex<0){
      //     itm.openingQty=parseFloat(itm.totalOpeningQty).toFixed(3);
      //     itm.openingAmt=parseFloat(itm.totalOpeningAmt).toFixed(2);
      //     itm.purchaseQty=parseFloat(0).toFixed(3);
      //     itm.purchaseAmount=parseFloat(0).toFixed(2);
      //     itm.consumptionQty=parseFloat(0).toFixed(3);
      //     itm.consumptionAmount=parseFloat(0).toFixed(2);
      //     itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
      //     itm.closingAmount=parseFloat(itm.openingAmt).toFixed(3);
      //     itm.openingDate=new Date(startDate);
      //     itm.closingDate=new Date(endDate);
      //     result.items.push(itm);
      //   }
      //   else
      //   {
      //     result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
      //     result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalOpeningAmt)).toFixed(2);

      //     result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalOpeningQty)).toFixed(3);
      //     result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalOpeningAmt)).toFixed(2);
      //   }
      // }
      // else if(itm.totalBalanceQty!=undefined){
      //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //   if(itmIndex<0){
      //     itm.openingQty=parseFloat(itm.totalBalanceQty).toFixed(3);
      //     itm.openingAmt=parseFloat(itm.totalBalanceAmt).toFixed(2);
      //     itm.purchaseQty=parseFloat(0).toFixed(3);
      //     itm.purchaseAmount=parseFloat(0).toFixed(2);
      //     itm.consumptionQty=parseFloat(0).toFixed(3);
      //     itm.consumptionAmount=parseFloat(0).toFixed(2);
      //     itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
      //     itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
      //     itm.openingDate=new Date(startDate);
      //     itm.closingDate=new Date(endDate);
      //     result.items.push(itm);
      //   }
      //   else
      //   {
      //     result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
      //     result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalBalanceAmt)).toFixed(2);

      //     result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalBalanceQty)).toFixed(3);
      //     result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalBalanceAmt)).toFixed(2);
      //   }
      // }
      // else if(itm.totalPurchaseQty!=undefined){
      //   var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //   if(itmIndex<0){
      //     itm.openingQty=parseFloat(itm.totalPurchaseQty).toFixed(3);
      //     itm.openingAmt=parseFloat(itm.totalPurchaseAmount).toFixed(2);
      //     itm.purchaseQty=parseFloat(0).toFixed(3);
      //     itm.purchaseAmount=parseFloat(0).toFixed(2);
      //     itm.consumptionQty=parseFloat(0).toFixed(3);
      //     itm.consumptionAmount=parseFloat(0).toFixed(2);
      //     itm.closingQty=parseFloat(itm.openingQty).toFixed(3);
      //     itm.closingAmount=parseFloat(itm.openingAmt).toFixed(2);
      //     itm.openingDate=new Date(startDate);
      //     itm.closingDate=new Date(endDate);
      //     result.items.push(itm);
      //   }
      //   else
      //   {
      //     result.items[itmIndex].openingQty=parseFloat(parseFloat(result.items[itmIndex].openingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
      //     result.items[itmIndex].openingAmt=parseFloat(parseFloat(result.items[itmIndex].openingAmt)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);

      //     result.items[itmIndex].closingQty=parseFloat(parseFloat(result.items[itmIndex].closingQty)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
      //     result.items[itmIndex].closingAmount=parseFloat(parseFloat(result.items[itmIndex].closingAmount)+parseFloat(itm.totalPurchaseAmount)).toFixed(2);
      //   }
      // }
      // else
      //     if(itm.totalSaleQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
      //       }
      //     }
      //     else if(itm.totalWastageQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalWastageQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalWastageAmt).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalWastageQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalWastageAmt)).toFixed(2);
      //       }
      //     }
      //     else if(itm.totalTransferQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
      //       }
      //     }
      //     else if(itm.itemSaleQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);
      //       }
      //     }
      //     else if(itm.totalIntermediateQty!=undefined){
      //       var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
      //       if(itmIndex<0){
      //         itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
      //         itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
      //         result.items.push(itm);
      //       }
      //       else
      //       {
      //         result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
      //         result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
      //       }
      //     }
      // });
      _.forEach(result.betweenDate,function(itm,i){
        if(itm.totalSaleQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalSaleQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalSaleAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {
            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalSaleQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalSaleAmount)).toFixed(2);
          }
        }
        else if(itm.totalWastageQty!=undefined) {
          var itmIndex = _.findIndex(result.items, {itemId: itm.itemId});
          if (itmIndex < 0) {
            itm.consumptionQty = parseFloat(itm.totalWastageQty).toFixed(3);
            itm.consumptionAmount = parseFloat(itm.totalWastageAmt).toFixed(2);
            result.items.push(itm);
          }
          else {
            result.items[itmIndex].consumptionQty = parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalWastageQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount = parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalWastageAmt)).toFixed(2);
          }
        }
        else if(itm.totalStockReturnQty!=undefined){
          var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
          if(itmIndex<0){
            itm.consumptionQty=parseFloat(itm.totalStockReturnQty).toFixed(3);
            itm.consumptionAmount=parseFloat(itm.totalStockReturnAmount).toFixed(2);
            result.items.push(itm);
          }
          else
          {

            result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReturnQty)).toFixed(3);
            result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReturnAmount)).toFixed(2);
          }
        }
       /* else if(itm.totalStockReceiveQty!=undefined){
              var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
              if(itmIndex<0){
                itm.consumptionQty=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                itm.consumptionAmount=parseFloat(itm.totalStockReceiveAmount).toFixed(2);
                result.items.push(itm);
              }
              else
              {

                result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty) + parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount) + parseFloat(itm.totalStockReceiveAmount)).toFixed(2);
              }
            }
       */             else if(itm.totalTransferQty!=undefined){
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalTransferQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalTransferAmount).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalTransferQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalTransferAmount)).toFixed(2);
                      }
                    }
                    else if(itm.itemSaleQty!=undefined){
                      //alert(itm.itemName);
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      //alert(itmIndex);
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.itemSaleQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.itemSaleAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.itemSaleQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.itemSaleAmt)).toFixed(2);
                      }
                    }
                    else if(itm.totalIntermediateQty!=undefined){
                      var itmIndex=_.findIndex(result.items,{itemId:itm.itemId});
                      if(itmIndex<0){
                        itm.consumptionQty=parseFloat(itm.totalIntermediateQty).toFixed(3);
                        itm.consumptionAmount=parseFloat(itm.totalIntermediateAmt).toFixed(2);
                        result.items.push(itm);
                      }
                      else
                      {
                        result.items[itmIndex].consumptionQty=parseFloat(parseFloat(result.items[itmIndex].consumptionQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
                        result.items[itmIndex].consumptionAmount=parseFloat(parseFloat(result.items[itmIndex].consumptionAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
                      }
                    }
                  });
      return result;
    };


    function FormatItemData(result){
      _.forEach(result,function(r,ii){
        _.forEach(r.receipeDetails,function(itm,i){
          if(itm.UnitName==undefined){
            itm.UnitName=itm.selectedUnitName;
          }
          itm.consumptionQty=0;itm.consumptionAmount=0;
          if(itm.totalSaleQty==undefined || itm.totalSaleAmount==undefined){
            itm.totalSaleQty=0;itm.totalSaleAmount=0;
          }
          if(itm.totalWastageQty==undefined || itm.totalWastageAmt==undefined){
            itm.totalWastageQty=0;itm.totalWastageAmt=0;
          }
          if(itm.totalTransferQty==undefined || itm.totalTransferAmount==undefined){
            itm.totalTransferQty=0;itm.totalTransferAmount=0;
          }
          if(itm.totalIntermediateQty==undefined || itm.totalIntermediateAmt==undefined){
            itm.totalIntermediateQty=0;itm.totalIntermediateAmt=0;
          }
          itm.consumptionQty=parseFloat(parseFloat(itm.totalSaleQty)+parseFloat(itm.totalWastageQty)+parseFloat(itm.totalTransferQty)+parseFloat(itm.totalIntermediateQty)).toFixed(3);
          itm.consumptionAmount=parseFloat(parseFloat(itm.totalSaleAmount)+parseFloat(itm.totalWastageAmt)+parseFloat(itm.totalTransferAmount)+parseFloat(itm.totalIntermediateAmt)).toFixed(2);
        });
      });
      return result;
    };
    //----------------------------------------------------Reports----------------------------------------------
    $scope.exportToPdf_purchaseConsumption=function(){
      var table=document.getElementById('consumptionReport').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    
    $scope.exportToExcel_purchaseConsumption=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('consumptionReport').innerHTML));
    };
    $scope.exportToPdf_stockSummaryForm=function(){
      var table=document.getElementById('stockSummary').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    $scope.exportToExcel_stockSummaryForm=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('stockSummary').innerHTML));
    };
    $scope.exportToPdf_itemWiseConsumptionForm=function(){
      var table=document.getElementById('itemWiseConsumption').innerHTML;
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };
    $scope.exportToExcel_itemWiseConsumptionForm=function(){
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('itemWiseConsumption').innerHTML));
    };

  }]).directive('stopccp', function(){
    return {
        scope: {},
        link:function(scope,element){
            element.on('cut copy paste', function (event) {
              event.preventDefault();
            });
        }
    };
}).controller('EmailModalCtrl', ['$scope', '$modalInstance', 'growl', function($scope, $modalInstance, growl) {
  console.log('modal opened');
    $scope.emailData = {};
    $scope.emailData.ok = function () {
      console.log('ok');
      if($scope.emailData.email.toLowerCase() == $scope.emailData.confirmEmail.toLowerCase()){
        $modalInstance.close($scope.emailData.email);
      } else {
        growl.error("Email doesn't match!", {ttl: 3000});
      }
    } 
    $scope.emailData.cancel = function () {
      $modalInstance.dismiss('cancel');
    } 
}]);
