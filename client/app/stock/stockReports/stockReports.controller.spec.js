'use strict';

describe('Controller: StockReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StockReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StockReportsCtrl = $controller('StockReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
