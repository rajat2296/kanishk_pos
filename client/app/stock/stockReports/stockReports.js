'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('stockReports', {
        url: '/stockReports',
        templateUrl: 'app/stock/stockReports/stockReports.html',
        controller: 'StockReportsCtrl',
        resolve: {
             //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Stock Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view stock reports !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],

         deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]

          //end
        }
      })
      .state('stockReports.entryReport', {
        url: '/entryReports',
        templateUrl: 'app/stock/stockReports/entryReports/entryReports.html',
        controller: 'EntryReportsCtrl',
        resolve: {
           //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Stock Entry Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view stock entry report !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          //end
          VPayment: ['$rootScope', '$state', '$stateParams', 'VendorPaymentHistory',
          function ($rootScope, $state, $stateParams, VendorPaymentHistory) {
            return VendorPaymentHistory.getTransactionHistoryByTransactionId({}).$promise.then(function (result){
              return result;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]

        }
      })
      .state('stockReports.consumptionReport', {
        url: '/consumptionReports',
        templateUrl: 'app/stock/stockReports/consumptionReports/consumptionReports.html',
        controller: 'ConsumptionReportsCtrl',
        resolve: {
           //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Consumption Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view consumption report !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      })
 .state('stockReports.detailedConsumptionReport', {
        url: '/detailedConsumptionReport',
        templateUrl: 'app/stock/stockReports/detailedConsumptionReport/detailedConsumptionReport.html',
        controller: 'detailedConsumptionReportCtrl',
        resolve: {
           //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Consumption Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view consumption report !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      })
      .state('stockReports.consumptionReportAggr', {
        url: '/consumptionReportsAggregated',
        templateUrl: 'app/stock/stockReports/consumptionReportsAggregated/consumptionReportsAggregated.html',
        controller: 'ConsumptionReportsCtrlAggr',
        resolve: {
           //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Consumption Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view consumption report !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      })
      .state('stockReports.purchaseDetailReport', {
        url: '/purchaseDetailReports',
        templateUrl: 'app/stock/stockReports/purchaseDetailReports/purchaseDetailReports.html',
        controller: 'PurchaseDetailReportsCtrl',
        resolve: {
          //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Purchase Detail Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view purchase detail report !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      })
      .state('stockReports.purchaseSummaryReport', {
        url: '/purchaseSummaryReports',
        templateUrl: 'app/stock/stockReports/purchaseSummaryReports/purchaseSummaryReports.html',
        controller: 'PurchaseSummaryReportsCtrl',
        resolve: {
             //modified
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Purchase Summary Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view purchase summary report !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      })
      .state('stockReports.reorderReport', {
        url: '/reorderReports',
        templateUrl: 'app/stock/stockReports/reOrderReports/reOrderReports.html',
        controller: 'ReOrderReportsCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      })
      .state('stockReports.requirementReport', {
        url: '/requirementReports',
        templateUrl: 'app/stock/stockReports/requirementReports/requirementReports.html',
        controller: 'RequirementReportsCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      })
          .state('stockReports.consolidatedInventoryReport', {
        url: '/consolidatedInventoryReport',
        templateUrl: 'app/stock/stockReports/consolidatedInventoryReport/consolidatedInventoryReport.html',
        controller: 'ConsolidatedInventoryReportCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      })
      .state('stockReports.shelfLifeReport', {
        url: '/shelfLifeReports',
        templateUrl: 'app/stock/stockReports/shelfLifeReports/shelfLifeReports.html',
        controller: 'ShelfLifeReportsCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }]
        }
      })
      .state('stockReports.varianceReport', {
        url: '/varianceReports',
        templateUrl: 'app/stock/stockReports/varianceReports/varianceReports.html',
        controller: 'VarianceReportsCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      })
      .state('stockReports.varianceReportAggr', {
        url: '/varianceReportsAggregated',
        templateUrl: 'app/stock/stockReports/varianceReportsAggregated/varianceReportsAggregated.html',
        controller: 'VarianceReportsCtrlAggr',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      })
      .state('stockReports.vendorPaymentReport', {
        url: '/vendorPaymentReports',
        templateUrl: 'app/stock/stockReports/vendorPaymentReports/vendorPaymentReports.html',
        controller: 'VendorPaymentReportsCtrl',
        resolve: {
          currentUser: ['$state', '$rootScope','$stateParams', 'Auth',
          function ($state,$rootScope ,$stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              return user;
            });
          }],
          VPayment: ['$rootScope', '$state', '$stateParams', 'VendorPaymentHistory',
          function ($rootScope, $state, $stateParams, VendorPaymentHistory) {
            return VendorPaymentHistory.getTransactionHistoryByTransactionId({}).$promise.then(function (result){
              return result;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      })
      .state('stockReports.wastageReport', {
        url: '/wastageReports',
        templateUrl: 'app/stock/stockReports/wastageReports/wastageReports.html',
        controller: 'WastageReportsCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]

        }
      })
      .state('stockReports.intermediateReport', {
        url: '/intermediateReport',
        templateUrl: 'app/stock/stockReports/intermediateReport/intermediateReport.html',
        controller: 'IntermediateReportCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      })
      .state('stockReports.foodCostReports', {
        url: '/foodCostReports',
        templateUrl: 'app/stock/stockReports/foodCostReports/foodCostReports.html',
        controller: 'FoodCostReportsCtrl',
        resolve: {
           //modified
          /*currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location',
            function ($state, $stateParams, Auth,Utils,growl,$location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag=false;
                if (user.role === 'user') {
                  if (Utils.isUserPermitted(user, 'View Food Cost Report')) {
                    flag = true;
                  }
                  if(!flag){
                    growl.error('No permission to view Food Cost report !!',{ttl:3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],*/
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
          //end
        }
      })
      .state('stockReports.finishedFoodReport', {
        url: '/finishedFoodReport',
        templateUrl: 'app/stock/stockReports/finishedFoodReport/finishedFoodReport.html',
        controller: 'FinishedFoodReportCtrl',
        resolve: {
          currentUser: ['$state','$rootScope', '$stateParams', 'Auth',
          function ($state,$rootScope, $stateParams, Auth) {
            return Auth.getCurrentUser().$promise.then(function (user) {
              $rootScope.currentUser = user;
              return user;
            });
          }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
      });
  });
