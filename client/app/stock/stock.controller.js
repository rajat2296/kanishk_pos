'use strict';

angular.module('posistApp')
  .controller('StockCtrl',['$rootScope','$scope','$q','Utils','$state','stockSettings','Deployment','localStorageService','currentUser', 'store', function ($rootScope,$scope,$q,Utils,$state,stockSettings,Deployment,localStorageService,currentUser,store) {
  	$scope.isVendor=false;
  	$scope.showMasterSetupButton=true;
    $scope.enableOutletIndenting = false;
  	hideMasterSetUp();
    currentUser.deployment_id=localStorageService.get('deployment_id');

//store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id},function(err,result))

    function getStores_ByDeployment(){
      var deferred = $q.defer();
      store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id},
      function success(result)
      {
        console.log('stores',angular.copy(result));
        var kitchenStores = _.filter(result, function (store) {
        return store.isKitchen == true;
        });
        deferred.resolve(kitchenStores); 
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    };

    function getAllDeployments() {
      var deferred = $q.defer();
     /* var obj ={
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }
      console.log(obj);*/
      Deployment.getSingleDeployment({
        
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        //var deployment = [];
        /*_.forEach(result,function(Deployment)
        {
          if(Deployment._id == currentUser.deployment_id)
            deployment.push(Deployment);
        })*/
        //deployment.push(result);
        console.log('result',angular.copy(result))
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    };

    var allPromise=$q.all([
      getStores_ByDeployment(),
      getAllDeployments()
    ]);
   allPromise.then(function (value){
      $scope.stores=value[0];
      //$scope.deployment =value[1];
      console.log('value[0]',angular.copy(value));
      if(!value[1].isBaseKitchen)
      {
      var kitchenStores = _.filter(value[0], function (store) {
      return store.isKitchen == true;
      });
      if(kitchenStores.length == 1){
        console.log('in');
      $scope.enableOutletIndenting = true;
    }
    }
    });
/*store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id},function(result)
{
    console.log('stores',angular.copy(result));
    var kitchenStores = _.filter(result, function (store) {
      return store.isKitchen == true;
      });
      if(kitchenStores.length == 1){
        console.log('in');
        $scope.enableOutletIndenting = true;
      }
      else{
        console.log('else');
        $scope.enableOutletIndenting = false;
      }
})

*/ 

      //modified
    $scope.user = currentUser;
    $scope.utils = Utils;
    //end

    
    disableEntryForVendor('Vendor');
    
    function disableEntryForVendor(name){
        if($rootScope.currentUser.selectedRoles.length==1 && $rootScope.currentUser.selectedRoles[0].name==name){
            $scope.isVendor=true;
        }
    };
    function hideMasterSetUp(){
    	if($rootScope.stores.length>0){
    		$scope.showMasterSetupButton=false;
    		//$state.go('stock.storeManagement');
    	}
    };
    
    $scope.hideMasterLink=function(){
    	$scope.showMasterSetupButton=false;
    };

  }]);
