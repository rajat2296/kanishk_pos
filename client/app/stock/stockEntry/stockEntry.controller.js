'use strict';

angular.module('posistApp')
    .controller('StockEntryCtrl', ['$rootScope', '$scope', 'currentUser', 'growl', '$modal', '$compile', '$templateCache', '$filter', '$interval', '$location', 'StockTransaction', '$resource', 'stockRecipe', '$state', 'StockResource', 'EditTransactionHistory', 'localStorageService', 'store', 'stockCategory', 'stockItem', 'vendor', 'receiver', 'StockReportNew', 'ClosingQuantity', 'Email', 'LastPriceRaw', 'LastPriceBK', 'StockReport', 'Utils', 'currentDeployment', '$timeout', '$q', 'Recipes', 'property', 'purchaseOrder', 'baseKitchenItem', 'intercom', 'stockUnit', function($rootScope, $scope, currentUser, growl, $modal, $compile, $templateCache, $filter, $interval, $location, StockTransaction, $resource, stockRecipe, $state, StockResource, EditTransactionHistory, localStorageService, store, stockCategory, stockItem, vendor, receiver, StockReportNew, ClosingQuantity, Email, LastPriceRaw, LastPriceBK, StockReport, Utils, currentDeployment, $timeout, $q, Recipes, property, purchaseOrder, baseKitchenItem, intercom, stockUnit) {

        //right ids
        // $scope.processedCat={_id:"556eadf102fad90807111556",categoryName:"Processed Food"};
        // $scope.processedSemi_Cat={_id:"556e9b0302fad90807111e55",categoryName:"InterMediate Food"};

        //modified
        //console.log(Recipes);
        $scope.stockSettings = {};
        var oldPO
        var cannotAssignBaseCategory=false
        var isPhysicalStock = false;
        $scope.enableCharges = {
            value: false
        };
        $scope.stockEntryCharges = {
            ishidden: true
        };

        $scope.currentDeployment = currentDeployment;
        $scope.user = currentUser;
        //$scope.allowBackDateEntry = Utils.isUserPermitted(currentUser, 'Allow Back Date Billing');
        $scope.utils = Utils;
        //end
        $scope.i = 0;
        var stop = $interval(function() {
            $scope.i++;
            //console.log($scope.i);
            if ($scope.i == 10) {
                $interval.cancel(stop);
            }
        }, 100)
        currentUser.deployment_id = localStorageService.get('deployment_id');
        $scope.stores = $rootScope.stores;
        $scope.vendors = $rootScope.vendors;
        $scope.receivers = $rootScope.receivers;
        $scope.setting = localStorageService.get('stockSetting');

        function getStockSettings() {
            var deferred = $q.defer();
            property.get({
                tenant_id: currentUser.tenant_id,
                deployment_id: currentUser.deployment_id
            }, function success(result) {

                $scope.stockSettings = result[0];

                deferred.resolve(result);

            }, function error(err) {
                deferred.reject(err);
            });
            return deferred.promise;
            //return baseKitchenItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
        }
        /*  var allPromise = $q.all([
          getStockSettings()
          ]);

        allPromise.then(function (value) {
          //console.log(value);
          $scope.stockSettings =value[0];
          if($scope.stockSettings == '')
          isPhysicalStock = false;
          else if($scope.stockSettings[0].isPhysicalStock == undefined)
          isPhysicalStock=false;
          else
          isPhysicalStock=$scope.stockSettings[0].isPhysicalStock;
                 
          //isPhysicalStock =$scope.stockSettings[0].isPhysicalStock;
        });*/



        //console.log($scope.stores);
        //$scope.menuItems=$rootScope.items;
        var stockProperty = null
            //TODO comment this line and uncomment others
        $scope.resetSerialNumber = false
        $scope.hidePricing = false
            // property.get({
            //   tenant_id: localStorageService.get("tenant_id"),
            //   deployment_id: localStorageService.get("deployment_id")
            // }, function (properties) {
            //   if(properties.length==0)
            //   {
            //      $scope.hidePricing = false;
            //      $scope.resetSerialNumber=false
            //   }
            //   else
            //   {

        //       isPhysicalStock = $scope.stockSettings.isPhysicalStock;
        //       if(properties[0].resetSerialNumber==undefined)
        //       $scope.resetSerialNumber=false
        //     else  
        //       $scope.resetSerialNumber=properties[0].resetSerialNumber
        //     console.log("reset serial ",$scope.resetSerialNumber);
        //     stockProperty = properties[0];
        //     if (!stockProperty.property.hidePricingEnabled)
        //       $scope.hidePricing = false;
        //     else {
        //       _.forEach(currentUser.selectedRoles, function (role) {
        //         if (stockProperty.property.hidePricing[role.name]) {
        //           $scope.hidePricing = true;
        //         }
        //       });
        //   }
        //   }

        // });

        // $scope.itemLastAndAveragePrice=LastPriceRaw;
        // $scope.itemLastAndAveragePriceBasekitchen=LastPriceBK;
        // console.log("LastPriceRaw",LastPriceRaw);
        // console.log("LastPriceBK",LastPriceBK);

        var kitchenStores = _.filter($scope.stores, function(store) {
            return store.isKitchen == true;
        });

        //console.log(kitchenStores);

        var allowBaseKitchenItemWastage = false;

        
        
            baseKitchenItem.getUniqueBaseKitchenItems({ tenant_id: localStorageService.get('tenant_id'), deployment_id: localStorageService.get('deployment_id') }, function(bItems) {
                $scope.baseKitchenItems = bItems;
                //console.log($scope.baseKitchenItems);
                //console.log(allowBaseKitchenItemWastage);
            }, function(err) {
                console.log('Error fetching Base Kitchen Items: ', err);
            });
        

        $scope.menuRecipes = Recipes;

        /*  stockUnit.get({
              tenant_id: currentUser.tenant_id,
              deployment_id: currentUser.deployment_id
            }).$promise.then(function (units) {
              $scope.stockUnits = units;
              console.log('$scope.stockUnits',$scope.stockUnits);
            }).catch(function (err) {
              console.log(err);
            });*/
        //console.log($scope.menuRecipes)
        StockReport.RawMaterialPricing_Receipe({
            tenant_id: currentUser.tenant_id,
            deployment_id: currentUser.deployment_id
        }).$promise.then(function(sItems) {
            //console.log('sItems', sItems);
            $scope.itemLastAndAveragePrice = sItems;
            $scope.editItems = sItems;
            StockReport.RawMaterialPricing_ReceipeBasekitchen({
                tenant_id: currentUser.tenant_id,
                deployment_id: currentUser.deployment_id
            }).$promise.then(function(rItems) {
                //console.log('rItems', rItems);
                $scope.itemLastAndAveragePriceBasekitchen = rItems;
                getDynamicPricingReport($scope.menuRecipes);
                getIRecipe();
            }).catch(function(err) {
                console.log(err);
                // console.log('IR ERROR  ............................');
            });
        });




        function getDynamicPricingReport(recipes) {
            _.forEach(recipes, function(rec, iiii) {
                rec.receipeDetails = getDynamicPricing(rec.receipeDetails);
                rec.price = angular.copy((rec.receipeDetails.totalTodayPrice) / (rec.recipeQty));
                // console.log("Recipe", rec);
            });
        };
        var processedCat = { _id: "PosistTech111111", categoryName: "Processed Food" };

        $scope.stockTransactionTypes = [
            { id: '1', Name: 'StockEntry', transactionType: '1' },
            { id: '2', Name: 'StockSale', transactionType: '2' },
            { id: '3', Name: 'PhysicalStock', transactionType: '3' },
            { id: '4', Name: 'WastageEntry', transactionType: '4' },
            { id: '5', Name: 'StockTransfer', transactionType: '5' },
            { id: '6', Name: 'OpeningStock', transactionType: '6' },
            { id: '7', Name: 'InterMediateEntry', transactionType: '7' },
            { id: '8', Name: 'Stock Return', transactionType: '9' },
            { id: '9', Name: 'InterMediateEntry Opening', transactionType: '10' }
        ];

        $scope.startsWith = function(item, viewValue) {
            return item.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
        };
        var startDate = new Date();
        var startDateReset = new Date(Utils.getDateFormatted(Utils.getResetDate($scope.currentDeployment.settings, Utils.convertDate(startDate))));
        startDateReset = new Date(startDateReset.setDate(startDateReset.getDate()));
        var endDate = new Date();
        var endDateReset = new Date(Utils.getDateFormatted(Utils.getResetDate($scope.currentDeployment.settings, Utils.convertDate(endDate))));
        endDateReset = new Date(endDateReset.setDate(endDateReset.getDate() + 1));


        //startDate.setHours(0, 0, 0, 0);
        //startDate.setDate(startDate.getDate());

        // endDate.setHours(0, 0, 0, 0);
        // endDate.setDate(endDate.getDate() + 1);
        var req = {
            fromDate: startDateReset,
            toDate: endDateReset,
            deployment_id: localStorageService.get('deployment_id'),
            tenant_id: localStorageService.get('tenant_id')
        };

        var cls = new ClosingQuantity(null, startDateReset, endDateReset, localStorageService.get('deployment_id'), localStorageService.get('tenant_id'));
        console.log('cls', cls);
        cls.getClosingQty().then(function(data) {
            $scope.closingQty = data;
            //console.log($scope.closingQty);
            //console.log($state);
            if ($state.params != undefined) {
                if ($state.params.transactionType != undefined || $state.params.transactionType != null) {
                    if ($state.params.transactionType == 1) {
                        $scope.entry = {
                            enableTabEntry: true,
                            enableTabSale: false,
                            enableTabPhysical: false,
                            enableTabWastage: false,
                            enableTabTransfer: false,
                            enableProcessedFoodEntry: false
                        }
                    } else if ($state.params.transactionType == 2) {
                        $scope.entry = {
                            enableTabEntry: false,
                            enableTabSale: true,
                            enableTabPhysical: false,
                            enableTabWastage: false,
                            enableTabTransfer: false,
                            enableProcessedFoodEntry: false
                        }
                    } else if ($state.params.transactionType == 3) {
                        $scope.entry = {
                            enableTabEntry: false,
                            enableTabSale: false,
                            enableTabPhysical: true,
                            enableTabWastage: false,
                            enableTabTransfer: false,
                            enableProcessedFoodEntry: false
                        }
                    } else if ($state.params.transactionType == 4) {
                        $scope.entry = {
                            enableTabEntry: false,
                            enableTabSale: false,
                            enableTabPhysical: false,
                            enableTabWastage: true,
                            enableTabTransfer: false,
                            enableProcessedFoodEntry: false
                        }
                    } else if ($state.params.transactionType == 5) {
                        $scope.entry = {
                            enableTabEntry: false,
                            enableTabSale: false,
                            enableTabPhysical: false,
                            enableTabWastage: false,
                            enableTabTransfer: true,
                            enableProcessedFoodEntry: false
                        }
                    } else if ($state.params.transactionType == 7) {
                        $scope.entry = {
                            enableTabEntry: true,
                            enableTabSale: false,
                            enableTabPhysical: false,
                            enableTabWastage: false,
                            enableTabTransfer: false,
                            enableProcessedFoodEntry: false
                        }
                    } else if ($state.params.transactionType == 11) {
                        $scope.entry = {
                            enableTabEntry: false,
                            enableTabSale: false,
                            enableTabPhysical: false,
                            enableTabWastage: false,
                            enableTabTransfer: false,
                            enableProcessedFoodEntry: true
                        }
                    }
                    getTransactionById($state.params.id);
                }
            }
        });

        //console.log($state.current);
        //console.log($state.params.id);
        $scope.order = null;
        if ($state.current.name == 'stock.performStockEntry') {

            if ($state.params.id) {
                //console.log($state.params.id);
                $scope.entry = {
                    enableTabEntry: true,
                    enableTabSale: false,
                    enableTabPhysical: false,
                    enableTabWastage: false,
                    enableTabTransfer: false,
                    enableProcessedFoodEntry: false
                }
                getPurchaseOrderById($state.params.id);
            }
        }

        function getPurchaseOrderById(id) {
            purchaseOrder.get({ id: id }, function(order) {
                    if(order.isReceived){
                        $state.go("stock.stockEntry");
                        growl.error("Entry for this purchase has already been done!", { ttl: 3000 });
                } else {    
                        $scope.order = order;
                        bindOrderWithEntry(order);
                }
            });
        }

        function makeEntryTransactionForPurchaseOrder(order) {

        }

        function bindOrderWithEntry(order) {
            oldPO=angular.copy(order)
            console.log("oldPO",order)
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 1, null, null);
            $scope.transaction.generateBillNumbers(1).then(function (snumbers) {
             console.log(snumbers);
             $scope.transaction._transactionNumber = snumbers.billnumber;
             $scope.transaction.transactionNumber = snumbers.billnumber;
             $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
             $scope.stockEntryForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
             console.log("transaction",$scope.transaction)
            });
            $scope.transaction.isPOEntry = true;
            $scope.transaction.poNumber = order.poNumber;
            $scope.transaction.poDaySerialNumber = order.daySerialNumber;
            // $scope.transaction._transactionNumber = order.poNumber;
            // $scope.transaction.transactionNumber = order.poNumber;
            // $scope.transaction.daySerialNumber = order.daySerialNumber;
            //alert('hello');
            // $scope.stockEntryForm.tempBillNo = "Temp/PO -" + order.poNumber;
            $scope.stockEntryForm.cartage = order.cartage;
            $scope.stockEntryForm.discountType = order.discountType;
            $scope.stockEntryForm.discount = order.discount;
            $scope.stockEntryForm.charges = order.charges;

            $scope.transaction.created = new Date();
            $scope.stockEntryForm.showBill = true;
            $scope.stockEntryForm.showOpenBill = false;
            //$scope.stockEntryForm.openBillNumber="SE - "+(result.transactionNumber);
            //$scope.stockEntryForm.openBillNumber = "SE - " + (result.daySerialNumber);
            //$scope.stockEntryForm.daySerialNumber = "SE - " + (result.daySerialNumber);
            //$scope.stockEntryForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
            //$scope.stockEntryForm.mainTransactionId = result._id;
            //$scope.stockEntryForm.transactionId = result.transactionId;
            console.log("$scope.stockEntryForm.availableItems start",angular.copy($scope.stockEntryForm.availableItems) )
            var st_index = _.findIndex($scope.stores, { "_id": order.store._id });
            $scope.stockEntryForm.store = $scope.stores[st_index];

            $scope.bindVendor_entryForm($scope.stockEntryForm.store);
            console.log("$scope.stockEntryForm.availableItems after bind vendor",angular.copy($scope.stockEntryForm.availableItems) )
            var vn_index = _.findIndex($scope.stockEntryForm.availableVendor, { "_id": order.vendor._id });
            $scope.stockEntryForm.vendor = $scope.stockEntryForm.availableVendor[vn_index];
            $scope.bindCategory_entryForm($scope.stockEntryForm.vendor, $scope.stockEntryForm.store);
            console.log("$scope.stockEntryForm.availableItems after bind cat ",angular.copy($scope.stockEntryForm.availableItems) )
            //$scope.stockEntryForm.cartage = result.cartage;
            //$scope.stockEntryForm.discount = result.discount;
            //$scope.stockEntryForm.discountType = result.discountType;

            $scope.stockEntryForm.selectedCategory = [];
            var categories = [];
            _.forEach(order.items, function(item) {
                categories.push(item.category);
            });
            categories = _.uniq(categories, '_id');
            _.forEach(categories, function(c) {
                $scope.stockEntryForm.selectedCategory[c._id] = true;
            })
            _.forEach(order.items, function(itm, i) {
                itm["fromCategory"] = itm.category;
                addItemEntry(itm);
                $scope.calculateAmt_entryForm(itm.qty, itm.price, itm, itm.vatPercent)
                    //$scope.stockEditEntry.push(angular.copy(itm));
            });
            $scope.stockEntryForm.isNew = true;
            $scope.stockEntryForm.enableSubmit = false;
            console.log("$scope.stockEntryForm ",angular.copy($scope.stockEntryForm) )
        };


        disableEntryForVendor('Vendor');

        function getDynamicPricing_intermediate(recipes) {
            var _ttoday = 0;
            var _tmonthly = 0;
            _.forEach(recipes.rawItems, function(itm, i) {
                var l_index = _.findIndex($scope.itemLastAndAveragePrice, { itemId: itm._id });
                if (l_index > -1) {
                    var lastPrice = $scope.itemLastAndAveragePrice[l_index].lastPrice;
                    var conversionFactor = getConFactor(itm.selectedUnitId);
                    var baseConFac = 1;
                    var avMonthPrice = $scope.itemLastAndAveragePrice[l_index].monthAverage;

                    if (lastPrice == 0) {
                        itm.calculatedPrice = parseFloat(0).toFixed(2);
                    } else {
                        itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(conversionFactor));
                    }
                    if (avMonthPrice == 0) {
                        itm.calculatedAveragePrice = parseFloat(0).toFixed(2);
                    } else {
                        itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(conversionFactor));
                    }

                    _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice));
                    _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice));
                } else {
                    var lastPrice = 0;
                    var conversionFactor = getConFactor(itm.selectedUnitId);
                    var baseConFac = 1;
                    var avMonthPrice = 0;

                    if (lastPrice == 0) {
                        itm.calculatedPrice = parseFloat(0).toFixed(2);
                    } else {
                        itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(conversionFactor));
                    }
                    if (avMonthPrice == 0) {
                        itm.calculatedAveragePrice = parseFloat(0).toFixed(2);
                    } else {
                        itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(conversionFactor));
                    }

                    _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice));
                    _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice));
                }
            });
            var totalTodayPrice = parseFloat(_ttoday).toFixed(4);
            var tDayPrice1Unit = parseFloat(parseFloat(totalTodayPrice) / parseFloat(recipes.recipeQty)).toFixed(4);
            var totalMonthlyPrice = parseFloat(parseFloat(_tmonthly)).toFixed(4);
            if (isNaN(totalMonthlyPrice)) {
                totalMonthlyPrice = parseFloat(0).toFixed(4);
            }
            var totalMonthlyPrice1Unit = parseFloat(parseFloat(totalMonthlyPrice) / parseFloat(recipes.recipeQty)).toFixed(4);

            var ir = {
                "recipeName": recipes.itemName,
                "totalTodayPricePerUnit": tDayPrice1Unit,
                "totalMonthlyPricePerUnit": totalMonthlyPrice1Unit,
                "qty": recipes.quantity
            };
            //console.log(ir);
            return ir;
        };

        function getConFactor(unit) {
            return parseFloat(unit.conversionFactor);
        }

        function getDynamicPricing(recipes) {
            var _ttoday = 0;
            var _tmonthly = 0;
            _.forEach(recipes, function(itm, i) { //$scope.stockRecipes[index].receipeDetails
                //console.log("ITEM IN RECIPE", itm)
                if (itm.isBaseKitchenItem) {
                    //console.log("array base kitvhen prices", $scope.itemLastAndAveragePriceBasekitchen)
                    var l_index = _.findIndex($scope.itemLastAndAveragePriceBasekitchen, { itemId: itm.itemId });
                    //console.log("LOCATION OF", $scope.itemLastAndAveragePriceBasekitchen[l_index])
                    var lastPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].lastPrice;
                    var avMonthPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage;
                    var conversionFactor = getConFactor(itm.selectedUnitId);
                    if (lastPrice == 0) {
                        itm.calculatedPrice = 0.00;
                    } else {
                        itm.calculatedPrice = parseFloat(parseFloat(lastPrice) * parseFloat(itm.baseQuantity)).toFixed(2);
                    }

                    if (avMonthPrice == 0) {
                        itm.calculatedAveragePrice = 0.00;
                    } else {
                        itm.calculatedAveragePrice = parseFloat(parseFloat(avMonthPrice) * parseFloat(itm.baseQuantity)).toFixed(2);
                    }
                    _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice)).toFixed(2);
                    _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice)).toFixed(2);
                } else if (itm.isSemiProcessed == undefined) {
                    var l_index = _.findIndex($scope.itemLastAndAveragePrice, { itemId: itm._id });
                    if (l_index > -1) {
                        //console.log(itm);
                        var lastPrice = $scope.itemLastAndAveragePrice[l_index].lastPrice;
                        var avMonthPrice = $scope.itemLastAndAveragePrice[l_index].monthAverage;
                        var conversionFactor = getConFactor(itm.selectedUnitId);
                        if (lastPrice == 0) {
                            itm.calculatedPrice = parseFloat(0).toFixed(2);
                        } else {
                            itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(2);
                        }

                        if (avMonthPrice == 0) {
                            itm.calculatedAveragePrice = parseFloat(0).toFixed(2);
                        } else {
                            itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(2);
                        }
                        _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice)).toFixed(2);
                        _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice)).toFixed(2);
                    } else {
                        var lastPrice = 0;
                        var avMonthPrice = 0;
                        var conversionFactor = getConFactor(itm.selectedUnitId);
                        if (lastPrice == 0) {
                            itm.calculatedPrice = parseFloat(0).toFixed(2);
                        } else {
                            itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(2);
                        }

                        if (avMonthPrice == 0) {
                            itm.calculatedAveragePrice = parseFloat(0).toFixed(2);
                        } else {
                            itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(2);
                        }
                        _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice)).toFixed(2);
                        _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice)).toFixed(2);
                    }
                } else {
                    var sss = getDynamicPricing_intermediate(itm);
                    if (itm.baseQuantity != undefined) {
                        itm.calculatedPrice = parseFloat(parseFloat(sss.totalTodayPricePerUnit) * parseFloat(itm.baseQuantity)).toFixed(2);
                        itm.calculatedAveragePrice = parseFloat(parseFloat(sss.totalMonthlyPricePerUnit) * parseFloat(itm.baseQuantity)).toFixed(2);
                    } else {
                        itm.calculatedPrice = parseFloat(parseFloat(sss.totalTodayPricePerUnit) * parseFloat(itm.quantity) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(2);
                        itm.calculatedAveragePrice = parseFloat(parseFloat(sss.totalMonthlyPricePerUnit) * parseFloat(itm.quantity) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(2);
                    }
                    _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice)).toFixed(2);
                    _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice)).toFixed(2);
                }
            });
            recipes.totalTodayPrice = parseFloat(_ttoday).toFixed(2);
            recipes.totalMonthlyPrice = parseFloat(_tmonthly).toFixed(2);
            return recipes;
            //console.log("recipes pricing", recipes);
        };

        function disableEntryForVendor(name) {
            if (currentUser.selectedRoles.length == 1 && currentUser.selectedRoles[0].name == name) {
                $rootScope.$state.go('stock.indentGrn');
            }
        };

        $scope.entry = {
            enableTabEntry: true,
            enableTabSale: false,
            enableTabPhysical: false,
            enableTabWastage: false,
            enableTabTransfer: false
        }

        function getTransactionById(id) {
            var records = [];
            StockResource.editEntriesRedirect({ id: id }).$promise.then(function(result) {
                records = result;
                //console.log(result)
                if ($state.params.transactionType == 1) {
                    bindEditTrans_Entry(result);
                } else if ($state.params.transactionType == 2) {
                    bindEditTrans_Sale(result);
                } else if ($state.params.transactionType == 3) {
                    bindEditTrans_Physical(result);
                } else if ($state.params.transactionType == 4) {
                    bindEditTrans_Wastage(result);
                } else if ($state.params.transactionType == 5) {
                    bindEditTrans_Transfer(result);
                } else if ($state.params.transactionType == 7) {
                    bindEditTrans_Intermediate(result);
                } else if ($state.params.transactionType == 11) {
                    bindEditTrans_FinishedFood(result);
                }
            });
        }

        /*$scope.bindCategory_MenuItemStockForm = function (stores) {
          console.log('stores', $scope.stores);
          console.log('recipes', $scope.menuRecipes);
          var store = angular.copy(stores);
          $scope.menuItemStockForm.availableCategory = [];
          $scope.menuItemStockForm.selectedCategory = {};
          $scope.menuItemStockForm.availableItems = [];
          $scope.menuItemStockForm.remainingItems = [];
          store.category = [];
          _.forEach(store.processedFoodCategory, function (cat, iii) {
            $scope.menuItemStockForm.availableCategory.push(cat);
            _.forEachRight(cat.item, function (item, i) {
              var index = _.findIndex($scope.menuRecipes, {itemId: item._id});
              if (index >= 0) {
                $scope.menuRecipes[index].itemName = item.itemName;
                item = angular.copy($scope.menuRecipes[index]);
                var varFromCategory = {"_id": cat._id, "categoryName": cat.categoryName};
                item["fromCategory"] = varFromCategory;
                item["itemType"] = "MenuItem";
                cat.item[i] = item;
                $scope.menuItemStockForm.remainingItems.push(item);
                console.log('item     ', item);
              }
              else {
                cat.item.splice(i, 1);
              }
            });
          });
        };*/


        function bindEditTrans_FinishedFood(result) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 11, null, null);
            $scope.menuItemStockForm.showBill = true;
            $scope.menuItemStockForm.mainTransactionId = result._id;
            $scope.menuItemStockForm.tempBillNo = "PE - " + (result.daySerialNumber);
            $scope.menuItemStockForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
            $scope.menuItemStockForm.transactionId = result.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": result._store._id });
            $scope.menuItemStockForm.store = $scope.stores[st_index];
            $scope.bindCategory_MenuItemStockForm($scope.menuItemStockForm.store);
            $scope.menuItemStockForm.selectedCategory = [];
            console.log('$scope.menuRecipes', $scope.menuRecipes);
            _.forEach(result._store.category, function(c, ii) {
                _.forEach(c.items, function(itm, i) {
                    console.log('editing items', itm);
                    var index = _.findIndex($scope.menuRecipes, { itemName: itm.itemName });
                    if (index >= 0) {
                        console.log('match found', itm);
                        itm.price = $scope.menuRecipes[index].price;
                    } else {

                    }
                    //$scope.menuRecipes[index].itemName = item.itemName;

                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    itm["fromCategory"] = varFromCategory;
                    $scope.menuItemStockForm.selectedCategory[c._id] = true;
                    editMenuItemStock(itm);
                    $scope.stockEditEntry.push(angular.copy(itm));
                });
            });
            editSelectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
            $scope.menuItemStockForm.isNew = true;
            $scope.menuItemStockForm.enableSubmit = false;
        };

        function bindVendor_editEntryForm(store) {
            $scope.stockEntryForm.availableCategory = [];
            $scope.stockEntryForm.availableItems = [];
            $scope.stockEntryForm.availableVendor = [];
            $scope.stockEntryForm.vendor = {};
            if (store != null) {
                _.forEach($scope.vendors, function(v, i) {
                    if (v.pricing != undefined) {
                        if (v.pricing._id == store._id) {
                            $scope.stockEntryForm.availableVendor.push(angular.copy(v));
                        }
                    }
                });
            }
        };

        function bindEditTrans_Sale(result) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 2, null, null);
            //$scope.stockSaleForm.showBill=false;
            $scope.stockSaleForm.showOpenBill = true;
            $scope.stockSaleForm.mainTransactionId = result._id;
            //$scope.stockSaleForm.openBillNumber="SS -"+(result.transactionNumber);
            $scope.stockSaleForm.openBillNumber = "SS -" + (result.daySerialNumber);
            $scope.stockSaleForm.invoiceNumber = result.invoiceNumber;
            $scope.stockSaleForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
            $scope.stockSaleForm.transactionId = result.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": result._store._id });
            $scope.stockSaleForm.store = $scope.stores[st_index];
            $scope.bindReceiver_saleForm($scope.stockSaleForm.store);
            var vn_index = _.findIndex($scope.stockSaleForm.availableReceiver, { "_id": result._store.receiver._id });
            $scope.stockSaleForm.receiver = $scope.stockSaleForm.availableReceiver[vn_index];

            $scope.bindCategory_saleForm($scope.stockSaleForm.receiver, $scope.stockSaleForm.store);
            $scope.stockSaleForm.cartage = result.cartage == "undefined" ? "" : result.cartage;
            $scope.stockSaleForm.discount = result.discount == "undefined" ? "" : result.discount;
            $scope.stockSaleForm.discountType = result.discountType;

            $scope.stockSaleForm.serviceCharge = result.serviceCharge == "undefined" ? "" : result.serviceCharge;
            $scope.stockSaleForm.serviceChargeType = result.serviceChargeType;
            $scope.stockSaleForm.vat = result.vat == "undefined" ? "" : result.vat;
            $scope.stockSaleForm.vatType = result.vatType;
            $scope.stockSaleForm.payment = result.payment == "undefined" ? "" : result.payment;
            $scope.stockSaleForm.heading = result.heading;

            $scope.stockSaleForm.selectedCategory = [];
            _.forEach(result._store.receiver.category, function(c, ii) {
                _.forEach(c.items, function(itm, i) {
                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    itm["fromCategory"] = varFromCategory;
                    $scope.stockSaleForm.selectedCategory[c._id] = true;
                    addItemSale(itm);
                    $scope.stockEditEntry.push(angular.copy(itm));
                });
            });

            $scope.stockSaleForm.isNew = true;
            $scope.stockSaleForm.enableSubmit = false;
            $scope.calculateAmt_saleForm();
        };

        function bindEditTrans_Physical(result) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 3, null, null);
            //$scope.physicalStockForm.showBill=false;
            $scope.physicalStockForm.showOpenBill = true;
            $scope.physicalStockForm.mainTransactionId = result._id;
            //$scope.physicalStockForm.openBillNumber="PH - "+(result.transactionNumber);
            $scope.physicalStockForm.openBillNumber = "PH - " + (result.daySerialNumber);
            $scope.physicalStockForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
            $scope.physicalStockForm.transactionId = result.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": result._store._id });
            $scope.physicalStockForm.store = $scope.stores[st_index];
            $scope.bindCategory_physicalStockForm($scope.physicalStockForm.store);
            $scope.physicalStockForm.selectedCategory = [];
            _.forEach(result._store.category, function(c, ii) {
                _.forEach(c.items, function(itm, i) {
                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    itm["fromCategory"] = varFromCategory;
                    $scope.physicalStockForm.selectedCategory[c._id] = true;
                    addItemPhysical(itm);
                    $scope.stockEditEntry.push(angular.copy(itm));
                });
            });
            $scope.physicalStockForm.isNew = true;
            $scope.physicalStockForm.enableSubmit = false;
        };

        function bindEditTrans_Wastage(result) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 4, null, null);
            $scope.wastageEntryForm.showOpenBill = true;
            $scope.wastageEntryForm.mainTransactionId = result._id;
            //$scope.wastageEntryForm.openBillNumber="WS - "+(result.transactionNumber);
            $scope.wastageEntryForm.openBillNumber = "WS - " + (result.daySerialNumber);
            $scope.wastageEntryForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
            //$scope.wastageEntryForm.tempBillNo="WS - "+(result.daySerialNumber+1);
            $scope.wastageEntryForm.transactionId = result.transactionId;
            $scope.wastageEntryForm.entryType = result.wastageEntryType;
            //var st_index=_.findIndex($scope.wastageEntryForm.entryFor,{"Name":result.wastageItemType});
            //$scope.wastageEntryForm.forEntry=$scope.wastageEntryForm.entryFor[st_index];
            $scope.wastageEntryForm.remainingItems = [];
            $scope.wastageEntryForm.availableItems = [];
            // loadWastageItems(result.wastageItemType);
            // _.forEach(result._store.category, function(c,ii){
            //     _.forEach(c.items, function(itm,i){
            //         var str={"_id":result._store._id,"storeName":result._store.storeName};
            //         itm["store"]=str;
            //         var varFromCategory={"_id":c._id,"categoryName":c.categoryName};
            //         itm["fromCategory"]=varFromCategory;
            //         addItemWastage(itm,result.wastageItemType);
            //         $scope.stockEditEntry.push(angular.copy(itm));
            //         //$scope.stockEntryForm.selectedCategory[c._id]=true;
            //         //addItemEntry(itm);
            //     });
            // });

            var st_index = _.findIndex($scope.stores, { "_id": result._store._id });
            $scope.wastageEntryForm.store = $scope.stores[st_index];
            $scope.bindCategory_wastageEntryForm($scope.wastageEntryForm.store);
            $scope.wastageEntryForm.selectedCategory = [];
            _.forEach(result._store.category, function(c, ii) {
                _.forEach(c.items, function(itm, i) {
                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    itm["fromCategory"] = varFromCategory;
                    $scope.wastageEntryForm.selectedCategory[c._id] = true;
                    addItemWastage(itm);
                    $scope.stockEditEntry.push(angular.copy(itm));
                });
            });

            $scope.wastageEntryForm.isNew = true;
            $scope.wastageEntryForm.enableSubmit = false;
        };

        function bindToCategory_stockTransferFormEdit(toStore) {
          console.log("storeeeeeee",toStore)
          var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};

          if ($scope.stockTransferForm.fromStore._id != toStore._id) {
            $scope.stockTransferForm.toAvailableCategory = [];
            $scope.stockTransferForm.toAvailableCategory.push(baseKitchenItemsCat)
            _.forEach(toStore.category, function (c, ii) {
              $scope.stockTransferForm.toAvailableCategory.push(angular.copy(c));
            });
          }
          else {
            $scope.stockTransferForm.toStore = {};
            $scope.stockTransferForm.toAvailableCategory = [];
            alert("Not able to select same store. Try other");
          }
          if ($scope.stockTransferForm.availableItems != undefined || $scope.stockTransferForm.availableItems.length != 0) {
            toStore.category.push(baseKitchenItemsCat)
            selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, toStore);
          }
        };




        function bindEditTrans_Transfer(result) {
          $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, null, null);
          $scope.stockTransferForm.showOpenBill = true;
          //$scope.stockTransferForm.showBill=false;
          $scope.stockTransferForm.mainTransactionId = result._id;
          //$scope.stockTransferForm.openBillNumber="ST - "+(result.transactionNumber);//(indexxx+1);
          $scope.stockTransferForm.openBillNumber = "ST - " + (result.daySerialNumber);
          $scope.stockTransferForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
          $scope.stockTransferForm.transactionId = result.transactionId;
          var st_index = _.findIndex($scope.stores, {"_id": result._store._id});
          $scope.stockTransferForm.fromStore = $scope.stores[st_index];
          $scope.bindFromCategory_stockTransferForm($scope.stockTransferForm.fromStore);
          var st_to_index = _.findIndex($scope.stockTransferForm.toAvailableStore, {"_id": result._store.category[0].items[0].toStore._id});
          $scope.stockTransferForm.toStore = $scope.stockTransferForm.toAvailableStore[st_to_index];
          bindToCategory_stockTransferFormEdit($scope.stockTransferForm.toStore);
          $scope.stockTransferForm.selectedCategory = [];
          _.forEach(result._store.category, function (c, ii) {
            _.forEach(c.items, function (itm, i) {
              var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
              itm["fromCategory"] = varFromCategory;
              $scope.stockTransferForm.selectedCategory[c._id] = true;
              addItemTransfer(itm, $scope.stockTransferForm.toStore.storeName);
              $scope.stockEditEntry.push(angular.copy(itm));
            });
          });
          selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, $scope.stockTransferForm.toStore);
          $scope.stockTransferForm.isNew = true;
          $scope.stockTransferForm.enableSubmit = false;

        };

        function bindEditTrans_Intermediate(result) {
            $scope.stockEntryForm.isIR = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 7, null, null);
            $scope.iRForm.showBill = true;
            $scope.iRForm.mainTransactionId = result._id;
            $scope.iRForm.tempBillNo = "IR - " + (result.daySerialNumber);
            $scope.iRForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
            $scope.iRForm.transactionId = result.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": result._store._id });
            $scope.iRForm.store = $scope.stores[st_index];
            $scope.bindCategory_intermediateStockForm($scope.iRForm.store);
            $scope.iRForm.selectedCategory = [];
            _.forEach(result._store.category, function(c, ii) {
                _.forEach(c.items, function(itm, i) {
                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    itm["fromCategory"] = varFromCategory;
                    $scope.iRForm.selectedCategory[c._id] = true;
                    addItemIr_ot(itm);
                    $scope.stockEditEntry.push(angular.copy(itm));
                });
            });
            $scope.iRForm.isNew = true;
            $scope.iRForm.enableSubmit = false;
        };

        function bindEditTrans_Entry(result) {
            console.log('836',result.charges);
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 1, null, null);
            $scope.stockEntryForm.showOpenBill = true;
            $scope.stockEntryForm.showBill = false;
            //$scope.stockEntryForm.openBillNumber="SE - "+(result.transactionNumber);
            if (result.isPOEntry) {
                $scope.stockEntryForm.openBillNumber = "PO - " + (result.daySerialNumber);
                $scope.stockEntryForm.daySerialNumber = "PO - " + (result.daySerialNumber);
            } else {
                $scope.stockEntryForm.openBillNumber = "SE - " + (result.daySerialNumber);
                $scope.stockEntryForm.daySerialNumber = "SE - " + (result.daySerialNumber);
            }
            $scope.stockEntryForm.invoiceNumber = result.invoiceNumber;
            $scope.stockEntryForm.billDate = $filter('date')(new Date(result.created), 'dd-MMM-yyyy');
            $scope.stockEntryForm.mainTransactionId = result._id;
            $scope.stockEntryForm.charges = result.charges;
            $scope.stockEntryForm.transactionId = result.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": result._store._id });
            $scope.stockEntryForm.store = $scope.stores[st_index];

            $scope.bindVendor_entryForm($scope.stockEntryForm.store);
            var vn_index = _.findIndex($scope.stockEntryForm.availableVendor, { "_id": result._store.vendor._id });
            $scope.stockEntryForm.vendor = $scope.stockEntryForm.availableVendor[vn_index];
            $scope.bindCategory_entryForm($scope.stockEntryForm.vendor, $scope.stockEntryForm.store);
            $scope.stockEntryForm.cartage = result.cartage;
            $scope.stockEntryForm.discount = result.discount;
            $scope.stockEntryForm.discountType = result.discountType;
            $scope.stockEntryForm.selectedCategory = [];
            _.forEach(result._store.vendor.category, function(c, ii) {
                _.forEach(c.items, function(itm, i) {
                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    itm["fromCategory"] = varFromCategory;
                    $scope.stockEntryForm.selectedCategory[c._id] = true;
                    addItemEntry(itm);
                    $scope.stockEditEntry.push(angular.copy(itm));
                });
            });
            $scope.stockEntryForm.isNew = true;
            $scope.stockEntryForm.enableSubmit = false;
            $scope.calculateAmt_entryForm();
        };

        // get all open transaction......
        //$scope.allTransactions=angular.copy($rootScope.stockTransaction);
        //$scope.templates=angular.copy($rootScope.stockTemplate);

        //-------------------------------------------------stock Templates--------------------------------------------------------//
        $scope.template_Entry = [];
        $scope.template_Sale = [];
        $scope.template_Physical = [];
        $scope.template_Wastage = [];
        $scope.template_transfer = [];
        findTemplates();

        function findTemplates() {
            _.forEach($scope.templates, function(s, i) {
                if (s.templateName != "") {
                    s["_id"] = s.transactionId;
                    if (s.transactionType == "101") {
                        $scope.template_Entry.push(s);
                    } else if (s.transactionType == "102") {
                        $scope.template_Sale.push(s);
                    } else if (s.transactionType == "103") {
                        $scope.template_Physical.push(s);
                    } else if (s.transactionType == "104") {
                        $scope.template_Wastage.push(s);
                    } else if (s.transactionType == "105") {
                        $scope.template_transfer.push(s);
                    }
                }
            });
        }

        //------------------------------------------------stock InterMediate Recipe Entry----------------------------------------------//
        $scope.iRForm = { enableSubmit: true, showBill: true, billDate: new Date(), maxDate: new Date() };
        $scope.intermediateRecipes = [];
        //getIRecipe();
        function getIRecipe() {
            var IR = angular.copy($scope.menuRecipes);
            //console.log('IR', IR);
            _.forEachRight(IR, function(r, i) {
                if (r.isSemiProcessed != undefined) {
                    if (r.isSemiProcessed == true) {
                        r.receipeDetails = getDynamicPricing(r.receipeDetails);
                        //r.price=angular.copy(r.receipeDetails.totalTodayPrice);
                        r.price = angular.copy((r.receipeDetails.totalTodayPrice) / (r.recipeQty));
                        $scope.intermediateRecipes.push(r);
                        //console.log("IntermediateItem", r);
                        $scope.menuRecipes.splice(i, 1);
                    }
                }
            });
            //console.log('Intermediate Recipes', $scope.intermediateRecipes);
        };
        $scope.bindCategory_intermediateStockForm = function(stores) {
            var store = angular.copy(stores);
            $scope.iRForm.availableCategory = [];
            $scope.iRForm.availableItems = [];
            $scope.iRForm.remainingItems = [];
            $scope.iRForm.selectedCategory = {};
            var ir = angular.copy($scope.intermediateRecipes);
            store.category = [];
            var cat = { _id: "PosistTech222222", categoryName: "InterMediate Food" };
            cat.item = [];
            _.forEach(angular.copy($scope.intermediateRecipes), function(itm) {
                itm.itemName = itm.recipeName;
                cat.item.push(itm);
            });
            store.category.push(cat);
            $scope.iRForm.availableCategory.push(cat);
            // _.forEach(store.category , function(c, ii){
            _.forEach(cat.item, function(item, iii) {
                var varFromCategory = { "_id": cat._id, "categoryName": cat.categoryName };
                item["fromCategory"] = varFromCategory;
                item["itemType"] = "IntermediateItem";
                $scope.iRForm.remainingItems.push(item);
            });
            // });
        };
        $scope.bindItems_intermediateStockForm = function(catId) {
            var cat = angular.copy($scope.iRForm.availableCategory);
            //console.log(cat);
            _.forEach(cat, function(c, ii) {
                if ($scope.iRForm.selectedCategory[catId] == true) {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var aa = _.findIndex($scope.iRForm.availableItems, { _id: itm._id });
                            if (aa < 0) {
                                var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                itm["fromCategory"] = varFromCategory;
                                $scope.iRForm.availableItems.push(itm);
                                var index = _.findIndex($scope.iRForm.remainingItems, { _id: itm._id });
                                $scope.iRForm.remainingItems.splice(index, 1);
                            }
                        });
                    }
                } else {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var index = _.findIndex($scope.iRForm.availableItems, { _id: itm._id });
                            if (index >= 0) {
                                $scope.iRForm.availableItems.splice(index, 1);
                                $scope.iRForm.remainingItems.push(itm);
                            }
                        });
                    }
                }
            });
            slectedUnit_iRForm($scope.iRForm.availableItems);
        };

        function convertPriceInSelectedUnit_INtermediate(item, unit) {

            if (item.selectedUnit != undefined) {
                var pInd = _.findIndex(item.units, { _id: item.selectedUnit._id });
            } else if (item.selectedUnitId != undefined) {
                var pInd = _.findIndex(item.units, { _id: item.selectedUnitId._id });
            } else {
                var unit = $scope.stockEntryForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (ii == item._id) {
                        item.selectedUnitId = u._id;
                        item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                    }
                });
                var pInd = _.findIndex(item.units, { _id: item.selectedUnit._id });
            }

            var preferedUnits = angular.copy(item.units[pInd]);
            var preferedconversionFactor = item.units[pInd].conversionFactor;
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            if (item.newConFactor == undefined) {
                item.newConFactor = preferedconversionFactor;
            }
            item.newprice = parseFloat(item.price) / parseFloat(item.newConFactor);
            _.forEach(unit, function(u, ii) {
                if (u != null) {
                    if (ii == item._id) {
                        selectedConversionFactor = u.conversionFactor;
                    }
                }
            });
            item.price = parseFloat(item.newprice) * parseFloat(selectedConversionFactor);
            item.newConFactor = selectedConversionFactor;
        };

        $scope.enableSubmitButton_Intermediate = function(items, selectedItem, unit) {
            if (selectedItem != undefined) {
                convertPriceInSelectedUnit_INtermediate(selectedItem, unit);
            }
            var flag = false;
            _.forEach(items, function(item, i) {
                if (flag == false) {
                    if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.iRForm.selectedUnits[item._id] != undefined && $scope.iRForm.selectedUnits[item._id] != null) {
                        flag = true;
                    }
                }
                var unit = $scope.iRForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (ii == item._id) {
                        item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                    }
                });
            });

            if (flag == true) {
                $scope.iRForm.enableSubmit = false;
            } else {
                $scope.iRForm.enableSubmit = true;
                growl.error('Please fill the full row before update', { ttl: 3000 });
            }
            //return flag;
        };
        $scope.addRemainingItem_intermediateStockForm = function($item, $model, $label) {
            var remaining = angular.copy($scope.iRForm.remainingItems);
            _.forEach($scope.iRForm.remainingItems, function(itm, i) {
                if (itm._id == $item._id) {
                    $scope.iRForm.availableItems.unshift(itm);
                    remaining.splice(i, 1);
                    $scope.iRForm.itemToAdd = "";
                    $scope.iRForm.selectedCategory[itm.fromCategory._id] = true;
                }
            });
            $scope.iRForm.remainingItems = remaining;
            slectedUnit_iRForm($scope.iRForm.availableItems);
        };
        $scope.newIntermediate_StockEntry = function() {
            $scope.stockEntryForm.isIR = true;
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            console.log("serverDate", serverD)
            console.log("diffMili", diffInMili)
            console.log("diffSec", sec)
                //var newBillDate= new Date(serverD.setSeconds( serverD.getSeconds() + sec) )
            $scope.iRForm = {
                enableSubmit: true,
                showBill: true,
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            };

            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 7, null, null);
            $scope.transaction.generateBillNumbers(7).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.iRForm.tempBillNo = "Temp/IR -" + snumbers.daySerialNumber;
                else
                    $scope.iRForm.tempBillNo = "Temp/IR -" + snumbers.billnumber;
            });

        };

        $scope.resetIntermediateTab = function() {
            if (!$scope.iRForm.isEdit) {
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                console.log("serverDate", serverD)
                console.log("diffMili", diffInMili)
                console.log("diffSec", sec)
                $scope.iRForm = {
                    enableSubmit: true,
                    showBill: true,
                    billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                    maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
                };

                $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 7, null, null);
                $scope.transaction.generateBillNumbers(7).then(function(snumbers) {
                    $scope.transaction._transactionNumber = snumbers.billnumber;
                    $scope.transaction.transactionNumber = snumbers.billnumber;
                    $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                    if ($scope.resetSerialNumber)
                        $scope.iRForm.tempBillNo = "Temp/IR -" + snumbers.daySerialNumber;
                    else
                        $scope.iRForm.tempBillNo = "Temp/IR -" + snumbers.billnumber;
                    $scope.iRForm.tempBillNo = "Temp/IR -" + $scope.transaction._transactionNumber;
                });
            } else {
                $scope.iRForm.isSaved = false;
                $scope.iRForm.isEdit = false;
                $scope.iRForm.isPrint = false;
            }
        }

        function slectedUnit_iRForm(item) {
            $scope.iRForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId._id == u._id) {
                            $scope.iRForm.selectedUnits[itm._id] = u;
                            itm.price = parseFloat((itm.price) * (u.conversionFactor));
                        }
                    }
                    else{
                        if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.iRForm.selectedUnits[itm._id]=u;
                                itm.selectedUnit=u;
                            }
                        }
                    }
                });
            });
        };

        function slectedUnit_iRForm_Ot(item) {
            $scope.iRForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.iRForm.selectedUnits[itm._id] = u;
                        }
                    }
                    else{
                        if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.iRForm.selectedUnits[itm._id]=u;
                                itm.selectedUnit=u;
                            }
                        }
                    }
                });
            });
        };

        $scope.goNextIR = function(nextIdx) {
            var f = angular.element(document.querySelector('#ir_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                $scope.iRForm.enableSubmit = false;
                angular.element(document.querySelector('#submitIR')).focus();
            }
        };
        $scope.ot_iREntry = function(openTransaction, indexxx) {
            $scope.stockEntryForm.isIR = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 7, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber);
            $scope.iRForm.showOpenBill = true;
            $scope.iRForm.showBill = false;
            $scope.iRForm.isOpen = true;
            if ($scope.resetSerialNumber)
                $scope.iRForm.openBillNumber = "Temp/SE - " + openTransaction.daySerialNumber;
            else
                $scope.iRForm.openBillNumber = "Temp/SE - " + openTransaction._transactionNumber;
            //$scope.iRForm.openBillNumber = "Temp/IR - " + openTransaction._transactionNumber;
            $scope.iRForm.transactionId = openTransaction.transactionId;

            var st_index = _.findIndex($scope.stores, { "_id": openTransaction._store._id });
            $scope.iRForm.store = $scope.stores[st_index];

            $scope.bindCategory_intermediateStockForm($scope.iRForm.store);
            $scope.iRForm.selectedCategory = [];
            _.forEach(openTransaction._items, function(itm, i) {
                $scope.iRForm.selectedCategory[itm.fromCategory._id] = true;
                addItemIr_ot(itm);
            });
            $scope.iRForm.isNew = true;
            $scope.iRForm.enableSubmit = false;
            $scope.iRForm.isPrint = false;
            $scope.iRForm.isEdit = false;
            $scope.iRForm.isSaved = false;
        };

        function addItemIr_ot(item) {
            _.forEachRight($scope.iRForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    $scope.iRForm.availableItems.push(item);
                    $scope.iRForm.remainingItems.splice(i, 1);
                }
            });
            slectedUnit_iRForm_Ot($scope.iRForm.availableItems);
        };

        $scope.deleteSelectedItemIRForm = function(item) {
            var index = _.findIndex($scope.iRForm.availableItems, { _id: item._id });
            $scope.iRForm.availableItems.splice(index, 1);
            item.qty = undefined;
            $scope.iRForm.remainingItems.push(item);
        };

        function getItemToSave_iRForm(items) {
            var items = angular.copy(items);
            _.forEachRight(items, function(itm, i) {
                if (itm.qty != "" || itm.qty != undefined) {
                    var flag = false;
                    _.forEach($scope.iRForm.selectedUnits, function(u, ii) {
                        if (ii == itm._id && itm.qty != "" && itm.qty != undefined && u != null && $scope.iRForm.selectedUnits[itm._id] != undefined) {
                            flag = true;
                        }
                        if (itm.itemType != "RawMaterial") {
                            var ss = $scope.transaction.calculateForRecipeDetails(itm);
                            itm = ss;
                        }
                    });

                    if (flag == false) {
                        items.splice(i, 1);
                    }
                }
            });

            _.forEach(items, function(itm, i) {
                for (var p in itm) {
                    if (p != "_id" && p != "itemName" && p != "menuQty" && p != "recipeQty" && p != "price" && p != "recipeName" && p != "receipeDetails" && p != "rawItems" && p != "qty" && p != "calculateInUnits" && p != "selectedUnit" && p != "preferedUnit" && p != "itemType" && p != "fromCategory" && p != "units") {
                        _.forEach(itm.units, function(u, ii) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
                var ss = $scope.transaction.basePrefferedAnsSelectedUnitsByItem(itm, $scope.iRForm.selectedUnits, 1);
                itm.calculateInUnits = ss;
                //console.log(itm);
            });
            return items;
        };

        function formatStore_IntermediateStockForm(store) {
            var stores = angular.copy(store);
            for (var p in stores) {
                if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID") {
                    delete stores[p];
                }
            }
            return stores;
        };

        function formatStoreForIntermediateStock(item, store) {
            var stores = store;
            stores.category = [];
            _.forEach(item, function(itm, i) {
                var index = _.findIndex(stores.category, { _id: itm.fromCategory._id });
                if (index < 0) {
                    stores.category.push(itm.fromCategory);
                }
            });
            _.forEach(stores.category, function(c, i) {
                c.items = [];
                _.forEachRight(item, function(itm, ii) {
                    if (c._id == itm.fromCategory._id) {
                        delete itm.fromCategory;
                        c.items.push(itm);
                        item.splice(ii, 1);
                    }
                });
            });
            //console.log(stores);
            return stores;
        };

        $scope.checkIntermediatekEntryForOpenTransactionAndSave = function() {
            if ($state.params.id == undefined) {
                // var ss=$scope.transaction.basePrefferedAnsSelectedUnitsByItem(item,$scope.stockEntryForm.selectedUnits,1);
                // item.calculateInUnits=ss;

                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.iRForm.availableItems, $scope.iRForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_iRForm(itms);

                //console.log(itemToSave);
                if (itemToSave.length > 0) {

                    var storeToSave = formatStore_IntermediateStockForm($scope.iRForm.store);
                    var sToSave = formatStoreForIntermediateStock(angular.copy(itemToSave), angular.copy(storeToSave));

                    //process for open transaction
                    if ($scope.iRForm.transactionId != undefined) {
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = sToSave;
                        $scope.transaction.processTransaction($scope.iRForm, 7);
                        $scope.transaction.updateTransaction({}, 'update');
                    } else {
                        $scope.iRForm.transactionId = $scope.transaction._id;

                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = sToSave;
                        $scope.transaction.billDate = new Date($scope.iRForm.billDate);
                        $scope.transaction.processTransaction($scope.iRForm, 7);
                        $scope.transaction.updateTransaction({}, 'insert');
                    }
                }
            } else {

            }
        };

        $scope.submitIntermediateRecipe = function(formIntermediateRecipe) {
            if ($state.params.id == undefined) {
                $scope.checkIntermediatekEntryForOpenTransactionAndSave();
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.iRForm.availableItems, $scope.iRForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_iRForm(itms);
                if (formIntermediateRecipe.$valid && !$scope.iRForm.isPrint) {
                    var storeToSave = formatStore_IntermediateStockForm($scope.iRForm.store);
                    var sToSave = formatStoreForIntermediateStock(angular.copy(itemToSave), angular.copy(storeToSave));
                    $scope.transaction._items = itemToSave;
                    $scope.transaction.isOpen = false;
                    $scope.transaction._store = sToSave;
                    $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                        if (results != null) {
                            $scope.iRForm.isPrint = true;
                            $scope.iRForm.isEdit = true;
                            $scope.iRForm.isSaved = true;
                            $scope.iRForm.printItems = itemToSave;
                            growl.success('Success in Intermediate entry', { ttl: 3000 });
                            growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                        }
                    });
                } else {
                    printIntermediateEntry(itemToSave);
                }
            } else {
                $scope.iRForm.enableSubmit = true;
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.iRForm.availableItems, $scope.iRForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_iRForm(itms);
                var storeToSave = formatStore_IntermediateStockForm($scope.iRForm.store);
                var sToSave = formatStoreForIntermediateStock(angular.copy(itemToSave), angular.copy(storeToSave));
                if (itemToSave.length > 0 && $scope.iRForm.store != undefined) {
                    getItemsHistory_Physical($scope.iRForm.availableItems);
                    $scope.stockEditHitory.editHistoryId = guid();
                    $scope.stockEditHitory.transactionId = $scope.iRForm.transactionId;
                    $scope.stockEditHitory.mainTransactionId = $scope.iRForm.mainTransactionId;
                    $scope.stockEditHitory.billNo = $scope.iRForm.tempBillNo;
                    $scope.stockEditHitory.user = formatUser(currentUser);
                    $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                    $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                    EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                        growl.success('New history created', { ttl: 3000 });
                        if ($state.params != null) {
                            $location.replace().path('/stock/stockEntry');
                        }
                    });

                    var toSave = { _store: sToSave, _id: $scope.iRForm.mainTransactionId };
                    StockResource.update({}, toSave, function(bill) {
                        //console.log(bill);
                    });
                } else {
                    $scope.iRForm.enableSubmit = false;
                }
            }
        };

        //------------------------------------------------stock Entry open TRansaction----------------------------------------------//
        $scope.openTrans_Entry = [];
        $scope.openTrans_Sale = [];
        $scope.openTrans_Physical = [];
        $scope.openTrans_Wastage = [];
        $scope.openTrans_transfer = [];
        $scope.openTrans_IR = [];
        //find_ot_entry();
        function find_ot_entry() {
            _.forEach($scope.allTransactions, function(s, i) {
                if (s.transactionType == "1") {
                    $scope.openTrans_Entry.push(s);
                } else if (s.transactionType == "2") {
                    $scope.openTrans_Sale.push(s);
                } else if (s.transactionType == "3") {
                    $scope.openTrans_Physical.push(s);
                } else if (s.transactionType == "4") {
                    $scope.openTrans_Wastage.push(s);
                } else if (s.transactionType == "5") {
                    $scope.openTrans_transfer.push(s);
                } else if (s.transactionType == "7") {
                    $scope.openTrans_IR.push(s);
                }
            });
        };
        $scope.new_StockEntry = function() {
            $scope.stockEntryForm.isNew = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 1, null, null);
            $scope.transaction.generateBillNumbers(1).then(function(snumbers) {
                //console.log(snumbers);
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.stockEntryForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                else
                    $scope.stockEntryForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
            });

            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            console.log("serverDate", serverD)
            console.log("diffMili", diffInMili)
            console.log("diffSec", sec)
            $scope.stockEntryForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))

            //console.log($scope.transaction);
        };

        function addItemEntry(item) {
            _.forEachRight($scope.stockEntryForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    var qty = parseFloat(item.qty).toFixed(3)

                    if(item.receiveQty !=undefined)
                        var receiveQty = parseFloat(item.receiveQty).toFixed(3)
                    else
                        var receiveQty = 0.000
                    if(item.receiveQty != undefined){
                       
                        item.qty = ""
                        item.qty= qty - receiveQty
                       
                          
 
                    }
                     if(qty!= receiveQty)

                       { $scope.stockEntryForm.availableItems.push(item);
                            console.log("innnnnnnnnnn",qty,receiveQty)
                        $scope.stockEntryForm.remainingItems.splice(i, 1);
                       }
                    else
                    {}
                    
                }
            });
            slectedUnit_entryForm($scope.stockEntryForm.availableItems);
        };

        function addItemEntry_ot(item) {
            console.log("item",item);
            console.log('$scope.stockEntryForm.remainingItems',$scope.stockEntryForm.remainingItems)
            _.forEachRight($scope.stockEntryForm.remainingItems, function(itm, i) {
                console.log("before matchitm",itm)
                if (itm._id == item._id) {
                    console.log("match")
                    $scope.stockEntryForm.availableItems.push(item);
                    $scope.stockEntryForm.remainingItems.splice(i, 1);
                }
            });
            slectedUnit_entryForm_ot($scope.stockEntryForm.availableItems);
        };

        $scope.ot_stockEntry = function(openTransaction, indexxx) {
            console.log("openTransaction",openTransaction)
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 1, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber, openTransaction.invoiceNumber);
            $scope.stockEntryForm.showOpenBill = true;
            $scope.stockEntryForm.showBill = false;
            $scope.stockEntryForm.isOpen = true;
            if ($scope.resetSerialNumber)
                $scope.stockEntryForm.openBillNumber = "Temp/SE - " + openTransaction.daySerialNumber;
            else
                $scope.stockEntryForm.openBillNumber = "Temp/SE - " + openTransaction._transactionNumber;
            $scope.stockEntryForm.transactionId = openTransaction.transactionId;
            $scope.stockEntryForm.invoiceNumber = openTransaction.invoiceNumber
                //$scope.stockEntryForm.store=angular.copy(openTransaction.store);
            var st_index = _.findIndex($scope.stores, { "_id": openTransaction._store._id });
            $scope.stockEntryForm.store = $scope.stores[st_index];

            $scope.bindVendor_entryForm($scope.stockEntryForm.store);
            var vn_index = _.findIndex($scope.stockEntryForm.availableVendor, { "_id": openTransaction._vendor._id });
            $scope.stockEntryForm.vendor = $scope.stockEntryForm.availableVendor[vn_index];
            console.log(openTransaction._vendor, $scope.stockEntryForm.store)
            $scope.bindCategory_entryForm(openTransaction._vendor, $scope.stockEntryForm.store);
            $scope.stockEntryForm.cartage = openTransaction.cartage;
            $scope.stockEntryForm.discount = openTransaction.discount;
            $scope.stockEntryForm.discountType = openTransaction.discountType;
            if(openTransaction.charges) {
                var charges = [];
                _.forEach(openTransaction.charges, function(charge) {
                    if(charge.chargeName && charge.chargeValue && !isNaN(charge.chargeValue) && charge.operationType && charge.type && charge.isEdit != true) {
                        charges.push(charge)
                    }
                });
                $scope.stockEntryForm.charges = charges;
            }
            else {
                $scope.stockEntryForm.charges = [];
            }
            //$scope.stockEntryForm.availableItems=openTransaction.items;

            $scope.stockEntryForm.selectedCategory = [];
            _.forEach(openTransaction._items, function(itm, i) {
                $scope.stockEntryForm.selectedCategory[itm.fromCategory._id] = true;
                if(itm.vatPercent)
                    itm.vatPercent = undefined;
                //addItemEntry(itm);
                addItemEntry_ot(itm);
            });
            $scope.stockEntryForm.isNew = true;
            $scope.stockEntryForm.enableSubmit = false;
            $scope.calculateAmt_entryForm();
        };

        $scope.checkStockEntryForOpenTransactionAndSave = function() {
            //alert('blur');
            if ($state.params.id == undefined) {
                var itemToSave = getItemToSave_stockEntryForm();
                //console.log(itemToSave);
                //alert(itemToSave.length);
                if (itemToSave.length > 0) {

                    var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                    var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));
                    // console.log(storeToSave);
                    // console.log(JSON.stringify(storeToSave));

                    //process for open transaction
                    if ($scope.stockEntryForm.transactionId != undefined) {
                        // $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
                        // var index=_.findIndex($scope.openTrans_Entry,{"transactionId":$scope.stockEntryForm.transactionId});
                        // $scope.openTrans_Entry.splice(index,1);
                        // growl.success('Updating.... in stock entry opening transaction', {ttl: 3000});

                        $scope.transaction._items = itemToSave;
                        $scope.transaction.invoiceNumber = $scope.stockEntryForm.invoiceNumber
                        $scope.transaction._store = str;
                        $scope.transaction._vendor = str.vendor;
                        console.log('$scope.stockEntryForm.charges',$scope.stockEntryForm.charges);
                        if(!$scope.stockEntryForm.charges)
                            $scope.transaction.charges = [];
                        else
                            $scope.transaction.charges = $scope.stockEntryForm.charges; 
                        $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : $scope.stockEntryForm.cartage;
                        $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : $scope.stockEntryForm.discount;
                        $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                        $scope.transaction.billDate = new Date($scope.stockEntryForm.billDate);
                        $scope.transaction.processTransaction($scope.stockEntryForm, 1);
                        $scope.transaction.updateTransaction({}, 'update');
                    } else {
                        //alert('blur insrt');
                        $scope.stockEntryForm.transactionId = $scope.transaction._id;

                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = str;
                        $scope.transaction.invoiceNumber = $scope.stockEntryForm.invoiceNumber
                        $scope.transaction._vendor = str.vendor;
                        console.log('$scope.stockEntryForm.charges',$scope.stockEntryForm.charges);
                        if(!$scope.stockEntryForm.charges)
                            $scope.transaction.charges = [];
                        else
                            $scope.transaction.charges = $scope.stockEntryForm.charges; 
                        $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : $scope.stockEntryForm.cartage;
                        $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : $scope.stockEntryForm.discount;
                        $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                        $scope.transaction.billDate = new Date($scope.stockEntryForm.billDate);
                        $scope.transaction.processTransaction($scope.stockEntryForm, 1);
                        $scope.transaction.updateTransaction({}, 'insert');
                    }
                    // $rootScope.db.insert('stockTransaction',
                    //     {
                    //         "transactionId":$scope.stockEntryForm.transactionId,
                    //         "transactionType":"1",
                    //         "daySerialNumber":0,
                    //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "user":JSON.stringify(currentUser),
                    //         "store":JSON.stringify(str),
                    //         "toStore":"",
                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                    //         "vendor":JSON.stringify($scope.stockEntryForm.vendor),
                    //         "receiver":"",
                    //         "cartage":$scope.stockEntryForm.cartage==undefined?0:$scope.stockEntryForm.cartage,
                    //         "discount":$scope.stockEntryForm.discount==undefined?0:$scope.stockEntryForm.discount,
                    //         "discountType":$scope.stockEntryForm.discountType,
                    //         "serviceCharge":0,
                    //         "serviceChargeType":"",
                    //         "vat":0,
                    //         "vatType":"",
                    //         "payment":0,
                    //         "heading":"",
                    //         "wastageItemType":"",
                    //         "wastageEntryType":"",
                    //         "created": "",
                    //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                    //         "templateName":"",
                    //         "deployment_id": currentUser.deployment_id,
                    //         "tenant_id": currentUser.tenant_id,
                    //         "isSynced" : false,
                    //         "syncedOn" : undefined
                    //     }).then(function (result) {
                    //         growl.success('Saving.... in stock entry opening transaction', {ttl: 3000});
                    //         //growl.success('Success in stock entry', {ttl: 3000});
                    //         //growl.success(itemToSave.length+'Items added', {ttl: 3000});
                    //         //$scope.stockEntryForm.isPrint=true;
                    //         //$scope.stockEntryForm.isEdit=true;
                    //         //$scope.stockEntryForm.isSaved=true;
                    //         var ot={
                    //             transactionId:$scope.stockEntryForm.transactionId,
                    //             transactionType:"1",
                    //             transactionNumber:$scope.transaction._transactionNumber,
                    //             user:angular.copy(currentUser),
                    //             store:angular.copy(storeToSave),
                    //             vendor:angular.copy($scope.stockEntryForm.vendor),
                    //             cartage:$scope.stockEntryForm.cartage,
                    //             discount:$scope.stockEntryForm.discount,
                    //             discountType:$scope.stockEntryForm.discountType,
                    //             created: null,
                    //             items: itemToSave,// JSON.stringify(this),
                    //             deployment_id: currentUser.deployment_id,
                    //             tenant_id: currentUser.tenant_id
                    //         };
                    //         $scope.openTrans_Entry.push(ot);
                    // });
                }
            } else {
                //alert('else');
            }
        };

        function print_stockEntry(items) {
            console.log('print', items)
            console.log('setting', localStorageService.get('stockSetting'));
                //var billNo = $scope.stockEntryForm.tempBillNo == undefined ? $scope.stockEntryForm.openBillNumber : $scope.stockEntryForm.tempBillNo;
            if ($scope.stockEntryForm.invoiceNumber)
                var invoiceNumber = $scope.stockEntryForm.invoiceNumber;
            var batchNumber = $scope.stockEntryForm.batchNumber;
            var billNo = $scope.stockEntryForm.tempBillNo == undefined ? $scope.stockEntryForm.openBillNumber : $scope.stockEntryForm.tempBillNo;
            var storeLocation = $scope.stockEntryForm.store.storeLocation == undefined ? "" : $scope.stockEntryForm.store.storeLocation;
            var storeUID = $scope.stockEntryForm.store.storeUID == undefined ? "" : $scope.stockEntryForm.store.storeUID;
            var setting = localStorageService.get('stockSetting');
            if (setting == null) {
                setting = { title: '', address: '', vatTin: '', taxTin: '', billRightFooter: '', billFooter: '' };
            }
            else if(setting.title == undefined)
                setting.title = '';
            else if(setting.address == undefined)
                setting.address = '';
            else if(setting.vatTin == undefined)
                setting.vatTin = '';
            else if(setting.taxTin == undefined)
                setting.taxTin = '';
            else if(setting.billRightFooter == undefined)
                setting.billRightFooter = '';
            else if(setting.billFooter == undefined)
                setting.billFooter = '';
            var table1 = "";
            table1 += "<table id='table1' runat='server' style='width:100%;'>";
            table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
            if(!setting.title)
                table1 += "<tr><td colspan='10' style='border:1px solid black;text-align:center;'></td></tr>";
            else                
            table1 += "<tr><td colspan='10' style='border:1px solid black;text-align:center;'>" + setting.title + "</td></tr>";
            if(!setting.address)
                table1 += "<tr><td colspan='10' style='border:1px solid black;text-align:center'></td></tr>";
            else
            table1 += "<tr><td colspan='10' style='border:1px solid black;text-align:center'>" + setting.address + "</td></tr>";
            table1 += "</table></br>"
            table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-bottom:2px solid black;'>"
                //table1+="<tr><td colspan='10' style=' width:400px; text-align:center; line-height:0px;'><span style='text-align:center; text-decoration:underline'><b>CustomerName</b></span><br /></td></tr>"
            table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
            table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
            table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Good Receipt Note <br /><br /></td></tr>";
            table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Entry NO : </td><td style='border:1px solid black'>" + billNo + "</td></tr>";
            if (invoiceNumber)
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Invoice No.  : </td><td style='border:1px solid black'>" + invoiceNumber + "</td></tr>";
            if (batchNumber != '' && batchNumber != undefined && batchNumber != null)
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Batch No.  : </td><td style='border:1px solid black'>" + batchNumber + "</td></tr>";
            table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Entry Date : </td><td style='border:1px solid black'>" + $filter('date')(new Date($scope.stockEntryForm.billDate), 'dd-MMM-yyyy') + "</td></tr>";
            table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Store Location : </td><td style='border:1px solid black'>" + storeLocation + "</td></tr>";
            table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Store UID : </td><td style='border:1px solid black'>" + storeUID + "</td></tr>";
            table1 += "<tr><td colspan='10'><br /></td></tr><tr><td colspan='10'>";

            table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Code</th><th>Item Name</th><th>Unit</th>";
            if (!$scope.hidePricing)
                table1 += "<th>Rate</th>";
            table1 += "<th>Quantity</th>";
            if (!$scope.hidePricing)
                table1 += "<th>Sub Total</th><th>Vat</th><th>Amount</th>";
            table1 += "</thead>";
            table1 += "<tbody>";
            var tot1 = 0;
            var vt = 0;
            var disc = 0;
            var t = 0;
            var disAmount = 0,
                vatTotal = 0,
                disTotal = 0;
            _.forEach(items, function(item, i) {
                t += Utils.roundNumber(item.qty, 2) * Utils.roundNumber(item.price, 2);
            });
            _.forEach(items, function(item, i) {
                //console.log('---------------------New Item------------------------')
                //console.log('item.qty',item.qty);
                //console.log('item.price',item.price);
                var code = '-';
                if (item.itemCode)
                    code = item.itemCode;
                table1 += "<tr>";
                table1 += "<td>" + code + "</td>";
                table1 += "<td>" + item.itemName + "</td>";
                table1 += "<td>" + item.calculateInUnits[2].unitName + "</td>";
                if (!$scope.hidePricing)
                    table1 += "<td>" + parseFloat(item.price).toFixed(2) + "</td>";
                table1 += "<td>" + parseFloat(item.qty).toFixed(3) + "</td>";
                if (!$scope.hidePricing) {
                    table1 += "<td>" + parseFloat((item.qty) * (item.price)).toFixed(2) + "</td>";
                    table1 += "<td>" + parseFloat(item.addedAmt).toFixed(2) + "</td>";
                    table1 += "<td>" + parseFloat(item.totalAmount).toFixed(2) + "</td>";
                }
                table1 += "</tr>";
                tot1 = parseFloat(parseFloat(tot1) + parseFloat((item.qty) * (item.price))).toFixed(2);
                var dis = 0,
                    disAmount = 0,
                    perDis = 0,
                    vs = 0,
                    subTotal = 0;
                subTotal = parseFloat(parseFloat(item.qty) * parseFloat(item.price)).toFixed(2);

                if ($scope.stockEntryForm.discount != undefined && $scope.stockEntryForm.discount != 0) {
                    if ($scope.stockEntryForm.discountType == "percent") {
                        //console.log('total',t);
                        dis = parseFloat(t * 0.01 * $scope.stockEntryForm.discount).toFixed(2);
                        perDis = parseFloat(parseFloat(parseFloat(subTotal) * parseFloat(dis)).toFixed(2) / parseFloat(t).toFixed(2)).toFixed(2);
                        //console.log('dis', dis);
                        //console.log('perDis', perDis)
                        //console.log('itm.total',subTotal);
                        disAmount = parseFloat(subTotal).toFixed(2) - parseFloat(perDis).toFixed(2);
                        console.log('disAmount', disAmount);
                        if (item.vatPercent) {
                            //console.log('vatTotal vefore vat',vatTotal);
                            //console.log('itm.vatPercent',item.vatPercent);
                            vs = parseFloat(item.vatPercent * 0.01 * parseFloat(disAmount).toFixed(2)).toFixed(2);
                            //console.log('vs',vs);
                            //console.log('disAmount before vat',disAmount);
                            disAmount = parseFloat(parseFloat(disAmount) + parseFloat(vs)).toFixed(2);
                            //console.log('disAmount after vat',disAmount);
                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                            //console.log('disTotal',disTotal);
                            vatTotal = parseFloat(parseFloat(vatTotal) + parseFloat(vs)).toFixed(2);
                            //console.log(disAmount);
                            //console.log('vatTotal after vat',vatTotal);
                        } else {
                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                            //console.log('disTotal',disTotal);
                        }
                        //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                        //console.log('disTotal',disTotal);
                    } else {
                        //console.log('total',t);
                        dis = parseFloat($scope.stockEntryForm.discount).toFixed(2);
                        //perDis=parseFloat(dis/numberOfItems).toFixed(2);
                        perDis = parseFloat(parseFloat(parseFloat(subTotal) * parseFloat(dis)).toFixed(2) / parseFloat(t).toFixed(2)).toFixed(2);
                        //console.log('dis', dis);
                        // console.log('perDis', perDis);
                        //console.log('itm.total',subTotal);
                        disAmount = parseFloat(parseFloat(subTotal).toFixed(2) - parseFloat(perDis).toFixed(2)).toFixed(2);
                        //console.log('disAmount',disAmount);
                        if (item.vatPercent) {
                            // console.log('vatTotal vefore vat',vatTotal);
                            // console.log('itm.vatPercent',item.vatPercent);
                            vs = parseFloat(item.vatPercent * 0.01 * parseFloat(disAmount).toFixed(2)).toFixed(2);
                            // console.log('vs',vs);
                            // console.log('disAmount before vat',disAmount);
                            disAmount = parseFloat(parseFloat(disAmount) + parseFloat(vs)).toFixed(2);
                            // console.log('disAmount after vat',disAmount);
                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                            // console.log('disTotal',disTotal);
                            vatTotal = parseFloat(parseFloat(vatTotal) + parseFloat(vs)).toFixed(2);
                            //console.log(disAmount);
                            // console.log('vatTotal after vat',vatTotal);
                        } else {
                            disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                            // console.log('disTotal',disTotal);
                        }
                        //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                        //console.log('disTotal',disTotal);
                    }
                } else {
                    if (item.vatPercent) {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',item.vatPercent);
                        vs = parseFloat(item.vatPercent * 0.01 * parseFloat(subTotal).toFixed(2)).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        disAmount = parseFloat(parseFloat(subTotal) + parseFloat(vs)).toFixed(2);

                        //disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        disTotal = parseFloat(parseFloat(disTotal) + parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        vatTotal = parseFloat(parseFloat(vatTotal) + parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                    } else {
                        disTotal = parseFloat(parseFloat(disTotal) + parseFloat(subTotal)).toFixed(2);
                        //console.log('disTotal',disTotal);

                    }
                }
            });

            if ($scope.stockEntryForm.discount != "" && $scope.stockEntryForm.discount != undefined) {
                if ($scope.stockEntryForm.discountType == "percent") {
                    disc = parseFloat(t * 0.01 * parseFloat($scope.stockEntryForm.discount)).toFixed(2);
                    disAmount = parseFloat(t - disc).toFixed(2);
                } else {
                    disc = parseFloat($scope.stockEntryForm.discount).toFixed(2);
                    disAmount = parseFloat(t - disc).toFixed(2);
                }
            } else {
                disAmount = parseFloat(t).toFixed(2);
            }
            /*
                if (item.vatPercent != "" && item.vatPercent != undefined) {
                  vt = parseFloat(0.01* parseFloat(item.vatPercent) * parseFloat(disAmount)).toFixed(2);
                }*/

            var cartage = 0;
            var netAdditionalCharge = 0;
            table1 += "</tbody></table>";
            table1 += "<br /><br /></td></tr>";
            if (!$scope.hidePricing)
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Total :</td><td style='border:1px solid black; text-align:right'>" + parseFloat(tot1).toFixed(2) + "</td></tr>";

            if ($scope.stockEntryForm.cartage != "" && $scope.stockEntryForm.cartage != undefined) {
                cartage = parseFloat($scope.stockEntryForm.cartage);
            }
            if (!$scope.hidePricing)
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>(-) Discount :</td><td style='border:1px solid black; text-align:right;'>" + parseFloat(disc).toFixed(2) + "</td></tr>";

            var availAmt1 = parseFloat(tot1 - parseFloat(disc)).toFixed(2);
            if (!$scope.hidePricing) {
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Available Amount :</td><td style='border:1px solid black; text-align:right;'>" + parseFloat(availAmt1).toFixed(2) + "</td></tr>";
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>(+) Vat :</td><td style='border:1px solid black; text-align:right;'>" + parseFloat(vatTotal).toFixed(2) + "</td></tr>"
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>(+) Cartage :</td><td style='border:1px solid black; text-align:right;'>" + parseFloat(cartage).toFixed(2) + "</td></tr>";
                _.forEach($scope.stockEntryForm.charges, function(charge) {
                    var chargableAmount = 0;
                    if (charge.operationType == 'additive')
                    {
                        if(charge.type == 'percent')
                        {

                            chargableAmount = (0.01 * charge.chargeValue * disAmount);
                            netAdditionalCharge += chargableAmount;
                        }
                        else
                        {
                        chargableAmount = (charge.chargeValue);
                        netAdditionalCharge += chargableAmount;
                        }    
                        table1 += "<tr><td colspan='8'></td><td style='text-align:right'>(+)" + charge.chargeName + ":</td><td style='border:1px solid black; text-align:right;'>" + parseFloat(chargableAmount).toFixed(2) + "</td></tr>";
                        
                    }
                    else
                    {
                        if(charge.type == 'percent')
                        {

                            chargableAmount = (0.01 * charge.chargeValue * disAmount);
                            netAdditionalCharge -= chargableAmount;
                        }
                        else
                        {
                        chargableAmount = (charge.chargeValue);
                        netAdditionalCharge -= chargableAmount;
                        }
                        table1 += "<tr><td colspan='8'></td><td style='text-align:right'>(-)" + charge.chargeName + ":</td><td style='border:1px solid black; text-align:right;'>" + parseFloat(chargableAmount).toFixed(2) + "</td></tr>";
                    }
                });
            }
            //console.log('disTotal',disTotal)
            var gt1 = parseFloat(parseFloat(disTotal) + parseFloat(cartage) + parseFloat(netAdditionalCharge)).toFixed(2);
            if (!$scope.hidePricing)
                table1 += "<tr><td colspan='8'></td><td style='text-align:right'>Grand Total :</td><td style='border:1px solid black; text-align:right;'>" + parseFloat(gt1).toFixed(2) + "</td></tr>";
            table1 += "<tr><td colspan='10' style='font-size:12px;'><br /><br />1) Inter state purchase is Against 'C' Form and I/we will Issue the 'C' form Quarterly              Yes/No: .........<br /><br /></td></tr>            <tr><td colspan='10'><br /></td></tr>";
            if(setting.vatTin)
            table1 += "<tr><td>VAT TIN :</td><td colspan='3' style='border:1px solid black; text-align:left; width:250px;'>" + setting.vatTin || '' + "</td><td colspan='5' style='text-align:right'>For Franchise</td><td>___________________________________</td></tr>";
            else
                table1 += "<tr><td>VAT TIN :</td><td colspan='3' style='border:1px solid black; text-align:left; width:250px;'></td><td colspan='5' style='text-align:right'>For Franchise</td><td>___________________________________</td></tr>";
            if(setting.taxTin)
            table1 += "<tr><td>CST TIN :</td><td colspan='3' style='border:1px solid black; text-align:left; width:250px;'>" + setting.taxTin || '' + "</td><td colspan='6' ></td></tr>";
            else
                table1 += "<tr><td>CST TIN :</td><td colspan='3' style='border:1px solid black; text-align:left; width:250px;'></td><td colspan='6' ></td></tr>";
            if (setting.billRightFooter)
            {
                console.log('setting.billRightFooter',setting.billRightFooter)
                table1 += "<tr><td colspan='9'></td><td style='font-size:14px;'>" + setting.billRightFooter + "</td></tr>";
            }
            else
                table1 += "<tr><td colspan='9'></td><td style='font-size:14px;'></td></tr>";
            table1 += "<tr><td colspan='10'><br /><br /><br /></td></tr>";
            if (setting.billFooter)
                table1 += "<tr><td colspan='10' style='text-align:center;font-size:12px;'>" + setting.billFooter + "</td></tr>";
            else
                table1 += "<tr><td colspan='10' style='text-align:center;font-size:12px;'></td></tr>";
            table1 += "</table>";
            var printer = window.open('', '', 'width=600,height=600');
            //var htmlToPrint = '' + '<style type="text/css">.table-curved {    border-collapse: separate;}.table-curved {    border: solid #ccc 1px;    border-radius: 6px;    border-left:0px;}.table-curved td, .table-curved th {    border-left: 1px solid #ccc;    border-top: 1px solid #ccc;}.table-curved th {    border-top: none;    background-color: #24242D; /*#337AB7;*/    color: #fff;}.table-curved th:first-child {    border-radius: 6px 0 0 0;}.table-curved th:last-child {    border-radius: 0 6px 0 0;}.table-curved th:only-child{    border-radius: 6px 6px 0 0;}.table-curved tr:last-child td:first-child {    border-radius: 0 0 0 6px;}.table-curved tr:last-child td:last-child {    border-radius: 0 0 6px 0;}</style>';
            //htmlToPrint += table;
            printer.document.open("text/html");
            printer.document.write(table1);
            printer.document.close();
            printer.focus();
            printer.print();
        };
        //------------------------------------------------stock Entry ----------------------------------------------//
        $scope.stockEntryForm = {
            isNew: false,
            enableSubmit: true,
            discountType: 'percent',
            showBill: true,
            isTemplateDelete: true,
            isTemplateSave: true,
            billDate: new Date(),
            maxDate: new Date()
        };
        $scope.stockEditEntry = [];
        $scope.stockEditHitory = {};

        $scope.clearStockEntryTab = function() {
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            console.log("serverDate", serverD)
            console.log("diffMili", diffInMili)
            console.log("diffSec", sec)
            $scope.stockEntryForm = {
                isNew: false,
                enableSubmit: true,
                showBill: true,
                discountType: 'percent',
                isTemplateDelete: true,
                isTemplateSave: true,
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            };

            if ($state.params != null) {
                $location.replace().path('/stock/stockEntry');
                //$location.path('/stock/stockEntry').replace();
                //$location.url('/stock/stockEntry').replace();
                //$rootScope.$apply();
            }
            var op = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 1, null);
            op.getOpenTransactions(1).then(function(results) {
                //console.log(results);
                $scope.openTrans_Entry = results;
            });
            op.getOpenTransactions(7).then(function(results) {
                $scope.openTrans_IR = results;
            });

            op.getTemplates(101).then(function(results) {
                $scope.template_Entry = results;
            });
        };
        if ($state.params.id == undefined) {
            $scope.clearStockEntryTab();
        };
        $scope.resetStockEntryTab = function() {
            if (!$scope.stockEntryForm.isEdit) {
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                console.log("serverDate", serverD)
                console.log("diffMili", diffInMili)
                console.log("diffSec", sec)

                $scope.stockEntryForm = {
                    isNew: true,
                    enableSubmit: true,
                    showBill: true,
                    discountType: 'percent',
                    isTemplateDelete: true,
                    isTemplateSave: true,
                    billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                    maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
                };
                $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 1, null, null);
                $scope.transaction.generateBillNumbers(1).then(function(snumbers) {
                    $scope.transaction._transactionNumber = snumbers.billnumber;
                    $scope.transaction.transactionNumber = snumbers.billnumber;
                    $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                    if ($scope.resetSerialNumber)
                        $scope.stockEntryForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                    else
                        $scope.stockEntryForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                });
                //console.log($scope.stockEntryForm.showBill);
                //$scope.stockEntryForm.tempBillNo="Temp/SE -"+ $scope.transaction._transactionNumber;
            } else {
                // $scope.transaction.isSynced=false;
                // $scope.transaction.isEdit=true;
                // $scope.transaction.syncedOn=null;
                $scope.stockEntryForm.isSaved = false;
                $scope.stockEntryForm.isEdit = false;
                $scope.stockEntryForm.isPrint = false;
            }
        }
        $scope.newReset_StockEntry = function() {
            $scope.stockEntryForm = {
                isNew: true,
                enableSubmit: true,
                showBill: true,
                discountType: 'percent',
                isTemplateDelete: true,
                isTemplateSave: true,
                billDate: new Date($rootScope.serverTime.serverTime),
                maxDate: new Date($rootScope.serverTime.serverTime)
            };

            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 1, null, null);
            $scope.transaction.generateBillNumbers(1).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.stockEntryForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                else
                    $scope.stockEntryForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
            });
            //console.log($scope.stockEntryForm.showBill);
        };

        $scope.billDateChange = function(d) {
            alert(d);
        };


        $scope.tempBill_entryForm = function(billDate) {
            $scope.stockEntryForm.showBill = true;
            $scope.stockEntryForm.showOpenBill = false;
            $scope.stockEntryForm.tempBillNo = "Temp/SE -" + $scope.transaction._transactionNumber;
            var serverDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate($rootScope.serverTime.serverTime))));
            var selectedDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(billDate))));
            console.log("selected date", selectedDate)
            console.log("server date", serverDate)
            var d = serverDate.getTime() - selectedDate.getTime()
            var di = new Date(d)
            var hours = Math.abs(serverDate - selectedDate) / 36e5;
            console.log("ho=ur", hours)
            if (hours >= 72) {
                growl.success('Back Date Entry has been restricted to 48 Hours.', { ttl: 3000 });
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                console.log("serverDate", serverD)
                console.log("diffMili", diffInMili)
                console.log("diffSec", sec)
                $scope.stockEntryForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            }
        };

        $scope.tempBill_iRForm = function(billDate) {
            $scope.iRForm.showBill = true;
            var serverDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate($rootScope.serverTime.serverTime))));
            var selectedDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(billDate))));
            console.log("selected date", selectedDate)
            console.log("server date", serverDate)
            var d = serverDate.getTime() - selectedDate.getTime()
            var di = new Date(d)
            var hours = Math.abs(serverDate - selectedDate) / 36e5;
            console.log("ho=ur", hours)
            if (hours >= 72) {
                growl.success('Back Date Entry has been restricted to 48 Hours.', { ttl: 3000 });
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                console.log("serverDate", serverD)
                console.log("diffMili", diffInMili)
                console.log("diffSec", sec)
                $scope.iRForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            }

        }

        var guid = (function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return function() {
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            };
        })();
        $scope.bindVendor_entryForm = function(store) {
            $scope.stockEntryForm.availableCategory = [];
            $scope.stockEntryForm.availableItems = [];
            $scope.stockEntryForm.availableVendor = [];
            $scope.stockEntryForm.vendor = {};
            $scope.stockEntryForm.remainingItems = [];
            if (store != null) {
                _.forEach($scope.vendors, function(v, i) {
                    if (v.pricing != undefined) {
                        if (v.pricing._id == store._id) {
                            $scope.stockEntryForm.availableVendor.push(v);
                        }
                    }
                });
            }
            //console.log($scope.stockEntryForm.availableVendor);
        };

        $scope.bindCategory_entryForm = function(vendor, stores) {
            $scope.stockEntryForm.availableItems = [];
            $scope.stockEntryForm.remainingItems = [];
            $scope.stockEntryForm.availableCategory = [];
            $scope.stockEntryForm.selectedCategory = {};

            $scope.stockEntryForm.availableTemplate = [];
            $scope.stockEntryForm.selectedTemplate = {};

            var store = angular.copy(stores);
            if (vendor.pricing != undefined) {
                _.forEach($scope.template_Entry, function(t, i) {
                    if (store._id == t._store._id) {
                        if (vendor._id == t._vendor._id) {
                            $scope.stockEntryForm.availableTemplate.push(angular.copy(t));
                        }
                    }
                });

                if (vendor.pricing._id == store._id) {
                    _.forEach(store.category, function(c, i) {
                        _.forEach(vendor.pricing.category, function(cat, ii) {
                            if (cat._id == c._id) {
                                $scope.stockEntryForm.availableCategory.push(angular.copy(c));
                            }
                        });
                    });

                    var itemsforcompare = [];
                    _.forEach(vendor.pricing.category, function(c, i) {
                        _.forEach(c.item, function(it, iii) {
                            var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                            it["fromCategory"] = varFromCategory;
                            if (it.itemCode == undefined) {
                                //console.log('setting itemcode of '+it.itemName+'to nothing. 2090');
                                it["itemCode"] = '';
                            }
                            itemsforcompare.push(angular.copy(it));
                        });
                    });
                    console.log('$scope.stockEntryForm.availableCategory',$scope.stockEntryForm.availableCategory);
                    _.forEach($scope.stockEntryForm.availableCategory, function(ac, i) {
                        _.forEachRight(ac.item, function(itm, ii) {
                            //console.log(itm);
                            var code = '';
                            if (itm.itemCode) {
                                code = itm.itemCode;
                                //console.log('-----------------------------item code of '+itm.itemName+' is :-',itm.itemCode);
                            } else {
                                //console.log('setting itemcode of '+itm.itemName+'to nothing.');
                                code = '';
                            }
                            var ind = _.findIndex(itemsforcompare, { "itemName": itm.itemName });
                            var varFromCategory = { "_id": ac._id, "categoryName": ac.categoryName };
                            //itemsforcompare[ind].itemCode = code;
                            itm["fromCategory"] = varFromCategory;
                            if (ind < 0) {
                                itm.itemcode = code;
                                $scope.stockEntryForm.remainingItems.push(angular.copy(itm));
                            } else {
                                ac.item.splice(ii, 1);
                                if (!itemsforcompare[ind].itemCode) {
                                    itemsforcompare[ind].itemCode = code;
                                }
                                itemsforcompare[ind].units[0].baseUnit = itm.units[0].baseUnit;
                                ac.item.push(itemsforcompare[ind]);
                                $scope.stockEntryForm.remainingItems.push(angular.copy(itemsforcompare[ind]));
                            }
                        });
                    });
                    console.log('$scope.stockEntryForm.remainingItems', angular.copy($scope.stockEntryForm.remainingItems));
                }
            }
        };

        $scope.getSearchResultsForEntries = function(searchField, transactionType) {
            var result = [];
            console.log("searchField",searchField)
            if (transactionType == 'Stock Entry') {
                _.forEach($scope.stockEntryForm.remainingItems, function(item, i) {
                    if (_.contains(item.itemCode.toLowerCase(), searchField.toLowerCase()) || _.contains(item.itemName.toLowerCase(), searchField.toLowerCase()))
                        result.push(item);
                });
            } else if (transactionType == 'Stock Sale') {
                _.forEach($scope.stockSaleForm.remainingItems, function(item, i) {
                    if (_.contains(item.itemCode.toLowerCase(), searchField.toLowerCase()) || _.contains(item.itemName.toLowerCase(), searchField.toLowerCase()))
                        result.push(item);
                });
            } else if (transactionType == 'Physical Stock') {
                _.forEach($scope.physicalStockForm.remainingItems, function(item, i) {
                    if (_.contains(item.itemCode.toLowerCase(), searchField.toLowerCase()) || _.contains(item.itemName.toLowerCase(), searchField.toLowerCase()))
                        result.push(item);
                });
            } else if (transactionType == 'Wastage Entry') {
                _.forEach($scope.wastageEntryForm.remainingItems, function(item, i) {
                    if (_.contains(item.itemCode.toLowerCase(), searchField.toLowerCase()) || _.contains(item.itemName.toLowerCase(), searchField.toLowerCase()))
                        result.push(item);
                });
            } else if (transactionType == 'Stock Transfer') {
                _.forEach($scope.stockTransferForm.remainingItems, function(item, i) {
                    if (_.contains(item.itemCode.toLowerCase(), searchField.toLowerCase()) || _.contains(item.itemName.toLowerCase(), searchField.toLowerCase()))
                        result.push(item);
                });
            }
            return result;
        }

        $scope.searchDropdown = function(name, code) {
            if (code == '' || code == null || code == undefined)
                return name;
            else
                return code + ' - ' + name;
        }

        function slectedUnit_entryForm_ot(item) {
            $scope.stockEntryForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId._id == u._id) {
                            $scope.stockEntryForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                        }
                    } else if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.stockEntryForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                        }
                    }
                    else if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.stockEntryForm.selectedUnits[itm._id]=u;
                                itm.selectedUnit=u;
                            }

                    }
                });
            });
        };

        function slectedUnit_entryForm(item) {
            $scope.stockEntryForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    console.log("itm",itm)
                    if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId._id == u._id) {
                            $scope.stockEntryForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                        }
                        if (itm.selectedUnitId == u._id) {
                            $scope.stockEntryForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                        }
                    } else if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.stockEntryForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                        }
                    }else if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.stockEntryForm.selectedUnits[itm._id]=u;
                                itm.selectedUnit=u;
                            }

                    }

                });
            });
        };

        $scope.bindItems_entryFormTemplate = function(templateId) {
            if ($scope.stockEntryForm.availableItems == undefined) {
                $scope.stockEntryForm.availableItems = [];
            }

            _.forEach($scope.stockEntryForm.availableTemplate, function(t, i) {
                if (t._id == templateId) {
                    console.log("template",t)
                    _.forEach(t._items, function(itm, ii) {
                        var av = _.findIndex($scope.stockEntryForm.availableItems, { "_id": itm._id });
                        if (av < 0) {
                            $scope.stockEntryForm.availableItems.push(angular.copy(itm));
                            var index = _.findIndex($scope.stockEntryForm.remainingItems, { "_id": itm._id });
                            $scope.stockEntryForm.remainingItems.splice(index, 1);
                            var cS = itm.fromCategory._id;
                            $scope.stockEntryForm.selectedCategory[cS] = true;
                        } else {
                            $scope.stockEntryForm.availableItems.splice(av, 1);
                            $scope.stockEntryForm.availableItems.push(angular.copy(itm));
                        }
                    });
                }
            });
            slectedUnit_entryForm($scope.stockEntryForm.availableItems);
            //slectedUnit_entryForm_ot($scope.stockEntryForm.availableItems);
        };

        $scope.bindItems_entryForm = function(catId) {
            if ($scope.stockEntryForm.availableItems == undefined) {
                $scope.stockEntryForm.availableItems = [];
            }
            var isItemExpired = false;
            _.forEach($scope.stockEntryForm.availableCategory, function(c, i) {
                if ($scope.stockEntryForm.selectedCategory[catId] == true) {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var av = _.findIndex($scope.stockEntryForm.availableItems, { "_id": itm._id });
                            if (av < 0) {
                                var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                itm["fromCategory"] = varFromCategory;

                                if (c.categoryName == "Processed Food") {
                                    itm.itemType = "MenuItem";
                                } else if (c.categoryName == "InterMediate Food") {
                                    itm.itemType = "IntermediateItem";
                                } else {
                                    itm.itemType = "RawMaterial";
                                }

                                //check date range
                                if (itm.toDate != undefined || itm.fromDate != undefined) {
                                    var toD = new Date(itm.toDate);
                                    var fD = new Date(itm.fromDate);
                                    var cD = new Date();
                                    if (cD < fD || cD > toD) {
                                        itm.hide = true;
                                        isItemExpired = true;
                                        //growl.error("Disabled items contract has been expired", {ttl:3000});
                                    }
                                }
                                if (!itm.itemCode)
                                    itm.itemCode = '';
                                $scope.stockEntryForm.availableItems.push(angular.copy(itm));
                                var index = _.findIndex($scope.stockEntryForm.remainingItems, { "_id": itm._id });
                                $scope.stockEntryForm.remainingItems.splice(index, 1);
                            }
                        });
                    }
                } else {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var index = _.findIndex($scope.stockEntryForm.availableItems, { _id: itm._id });
                            if (index >= 0) {
                                $scope.stockEntryForm.availableItems.splice(index, 1)
                                if (itm.toDate != undefined || itm.fromDate != undefined) {
                                    var toD = new Date(itm.toDate);
                                    var fD = new Date(itm.fromDate);
                                    var cD = new Date();
                                    if (cD < fD || cD > toD) {
                                        itm.hide = true;
                                        isItemExpired = true;
                                    }
                                }
                                if(!itm.itemCode)
                                    itm.itemCode = '';
                                $scope.stockEntryForm.remainingItems.push(angular.copy(itm));
                            }
                        });
                    }
                }
            });
            slectedUnit_entryForm($scope.stockEntryForm.availableItems);
            if (isItemExpired == true) {
                alert("Disabled items contract has been expired");
            }
        };
        $scope.goNext = function(nextIdx) {
            var f = angular.element(document.querySelector('#f_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                // $scope.stockEntryForm.enableSubmit=false;
                // angular.element(document.querySelector('#submitEntry')).focus();
            }
            $scope.stockEntryForm.isOpen = true;
        };


  $scope.calculateAmt_entryForm = function (qty, price, item, vat) {

  var billTotal = 0;
  var grandTotal = $scope.stockEntryForm.cartage ? Number($scope.stockEntryForm.cartage) : 0;
  var discount = 0;
  var discountableAmount = 0;
  var netAdditionalCharge = 0;
  _.forEach($scope.stockEntryForm.availableItems, function (item) {
    if(!isNaN(item.qty)){
      if(!isNaN(item.price)) {
        billTotal += (item.qty * item.price);
      }
    }
  });

  if(!isNaN($scope.stockEntryForm.discount)){
    if($scope.stockEntryForm.discountType == 'percent') {
      discount = ($scope.stockEntryForm.discount * billTotal / 100);
    }
    else {
      discount = Number($scope.stockEntryForm.discount);
    }
  }
  //console.log(discount);
  console.log("$scope.stockEntryForm.availableItems  calculateAmt_entryForm",$scope.stockEntryForm.availableItems)
  _.forEach($scope.stockEntryForm.availableItems, function (item) {
    if(!isNaN(item.qty)) {
     
            
      if(!isNaN(item.price)) {
        item.subTotal = item.qty * item.price;
        item.totalAmount = item.subTotal - (item.subTotal * discount / billTotal);
        discountableAmount += item.totalAmount; 
        if (item.vatPercent == "" || item.vatPercent == undefined || isNaN(item.vatPercent)) {
          item.addedAmt = 0;
          item.vatPercent = "";
        }
        else {
          var vatamt = (item.totalAmount * .01 * parseFloat(item.vatPercent));
          item.addedAmt = vatamt;
        }
        item.totalAmount += item.addedAmt;
        grandTotal += item.totalAmount;
        //console.log(item.totalAmount, item.subTotal, discount, billTotal, item.addedAmt);
      }
    }
  });

  _.forEach($scope.stockEntryForm.charges , function(charge) {
    if(charge.isEdit == false) {
        if(charge.operationType == 'additive') {
            if(charge.type == 'percent')
            {
                netAdditionalCharge += (charge.chargeValue * 0.01 * discountableAmount);
                charge.chargableAmount = (charge.chargeValue * 0.01 * discountableAmount);
            }
            else
            {
                netAdditionalCharge += (charge.chargeValue);
                charge.chargableAmount = (charge.chargeValue);
            }
        }
        else
        {
            if(charge.type == 'percent')
            {
                netAdditionalCharge -= (charge.chargeValue * 0.01 * discountableAmount);
                charge.chargableAmount = (charge.chargeValue * 0.01 * discountableAmount);;
            }
            else
            {
                netAdditionalCharge -= (charge.chargeValue);
                charge.chargableAmount = (charge.chargeValue);
            }
        }
    }
  })

  //if(qty && price && item && vat){
  if(qty && price && item){
    var index = _.findIndex($scope.stockEntryForm.availableItems, {_id: item._id});
    if (!isNaN(qty)) {
      if (!isNaN(price)) {
        $scope.stockEntryForm.isOpen = true;
        var ss = $scope.transaction.basePrefferedAnsSelectedUnitsByItem(item, $scope.stockEntryForm.selectedUnits, 1);
        item.calculateInUnits = ss;
        //console.log(ss);
        $scope.enableSubmitButton_Entry($scope.stockEntryForm.availableItems);
      }
      else {
        $scope.stockEntryForm.availableItems[index].price = "";
      }
    }
    else {
      $scope.stockEntryForm.availableItems[index].qty = "";
    }
  }
  $scope.stockEntryForm.billTotal = grandTotal + netAdditionalCharge;
  console.log("$scope.stockEntryForm.billTotal",$scope.stockEntryForm.billTotal,grandTotal , netAdditionalCharge)
}

        function calculateAmtinPrefferedAndBaseUnit_EntryForm(qty, price, item, vat) {

            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            _.forEach($scope.stockEntryForm.selectedUnits, function(u, ii) {
                if (ii == item._id) {
                    selectedConversionFactor = u.conversionFactor;
                }
            });

            var baseUnit = {};
            if (item.units[0].baseUnit != undefined) {
                baseUnit = angular.copy(item.units[0].baseUnit);
                baseUnit.unitName = baseUnit.name;
            } else {
                if (item.baseUnit != undefined) {
                    baseUnit = angular.copy(item.baseUnit);
                    baseUnit.unitName = baseUnit.name;
                } else {
                    baseUnit = angular.copy(item.units[0]);
                }
            }

            var baseAmt = parseFloat(qty / selectedConversionFactor) * parseFloat(price);
            if (vat == "" || vat == undefined) {
                baseUnit.totalAmount = baseAmt;
                baseUnit.subTotal = baseAmt;
                baseUnit.addedAmt = 0;
            } else {
                var vatamt = (baseAmt * .01 * parseFloat(vat));
                baseUnit.addedAmt = vatamt;
                baseUnit.subTotal = baseAmt;
                baseUnit.totalAmount = baseAmt + parseFloat(vatamt);
            }
            baseUnit.type = "baseUnit";
            baseUnit.baseQty = parseFloat(qty / selectedConversionFactor);
            baseUnit.basePrice = parseFloat(price / selectedConversionFactor);
            baseUnit.conversionFactor = conversionFactor;
            item.baseUnit = baseUnit;

            var pInd = _.findIndex(item.units, { _id: item.preferedUnit });
            if (pInd >= 0) {
                var preferedUnits = angular.copy(item.units[pInd]);
                var preferedconversionFactor = item.units[pInd].conversionFactor;
                if (preferedconversionFactor == conversionFactor) {
                    preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
                    preferedUnits.subTotal = parseFloat(baseUnit.subTotal);
                } else if (preferedconversionFactor > conversionFactor) {
                    preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                    preferedUnits.subTotal = parseFloat(baseUnit.subTotal) * parseFloat(preferedconversionFactor);
                } else if (preferedconversionFactor < conversionFactor) {
                    preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                    preferedUnits.subTotal = parseFloat(baseUnit.subTotal) / parseFloat(preferedconversionFactor);
                }

                preferedUnits.addedAmt = 0;
                preferedUnits.type = "preferedUnit";
                if (vat == "" || vat == undefined) {
                    preferedUnits.totalAmount = preferedUnits.subTotal;
                } else {
                    var vatamt1 = (preferedUnits.subTotal * .01 * parseFloat(vat));
                    preferedUnits.addedAmt = vatamt1;
                    preferedUnits.totalAmount = preferedUnits.subTotal + parseFloat(vatamt);
                }
                delete preferedUnits.baseUnit;
                item.preferedUnits = preferedUnits;
            }
        };

        function formatStore_stockEntryForm(store) {
            var stores = angular.copy(store);
            for (var p in stores) {
                if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID") {
                    delete stores[p];
                }
            }
            return stores;
        };

        function getFromCategoryOfItem_stockEntryForm() {
            _.forEach($scope.stockEntryForm.selectedCategory, function(sc, i) {
                //console.log(sc +" =sc , i= "+i);
                if (sc == true) {
                    _.forEach($scope.stockEntryForm.availableCategory, function(ac, ii) {
                        if (ac._id == i) {
                            _.forEach(ac.item, function(itm, iii) {
                                if (ac.categoryName == "Processed Food") {
                                    itm.itemType = "MenuItem";
                                } else if (ac.categoryName == "InterMediate Food") {
                                    itm.itemType = "IntermediateItem";
                                } else {
                                    itm.itemType = "RawMaterial";
                                }
                                var varFromCategory = { "_id": ac._id, "categoryName": ac.categoryName };
                                itm["fromCategory"] = varFromCategory;
                            });
                        }
                    });
                }
            });
        };

        function getItemToSave_stockEntryForm() {
            console.log('$scope.stockEntryForm.availableItems',$scope.stockEntryForm.availableItems);
            var items = angular.copy($scope.stockEntryForm.availableItems);
            _.forEachRight(items, function(itm, i) {
                var flag = false;
                console.log('$scope.stockEntryForm.selectedUnits',$scope.stockEntryForm.selectedUnits)
                _.forEach($scope.stockEntryForm.selectedUnits, function(u, ii) {
                    if (ii == itm._id && itm.qty != "" && itm.qty != undefined && !isNaN(itm.qty) && itm.price != "" && itm.price != undefined && !isNaN(itm.price) && u != null && $scope.stockEntryForm.selectedUnits[itm._id] != undefined) {
                        // var selectedUnit={"_id":u._id,"unitName":u.unitName,"baseQty":itm.qty,"subTotal":itm.subTotal,"vatAmt":itm.addedAmt,"totalAmount":itm.totalAmount,"type":"selectedUnit","conversionFactor":u.conversionFactor};
                        // itm["selectedUnit"]=selectedUnit;
                        // if(itm.baseUnit!=undefined){
                        //     itm.calculateInUnits=[];
                        //     itm.calculateInUnits.push(itm.baseUnit);
                        //     if(itm.preferedUnits!=undefined){
                        //         itm.calculateInUnits.push(itm.preferedUnits);
                        //     }
                        //     else
                        //     {
                        //         var preUnit={"_id":u._id,"unitName":u.unitName,"baseQty":itm.qty,"subTotal":itm.subTotal,"vatAmt":itm.addedAmt,"totalAmount":itm.totalAmount,"type":"preferedUnit","conversionFactor":u.conversionFactor};
                        //         itm["preferedUnit"]=preUnit;
                        //         itm.calculateInUnits.push(itm.preferedUnit);
                        //         itm.preferedUnit=u._id;
                        //     }
                        //     itm.calculateInUnits.push(itm.selectedUnit);
                        // }

                        flag = true;
                    }
                });
                if (flag == false) {
                    items.splice(i, 1);
                }
            });
            _.forEach(items, function(itm, i) {
                for (var p in itm) {
                    if (p != "_id" && p != "itemName" && p != "qty" && p != "calculateInUnits" && p != "itemType" && p != "preferedUnit" && p != "fromCategory" && p != "price" && p != "subTotal" && p != "comment" && p != "vatPercent" && p != "mfgDate" && p != "totalAmount" && p != "addedAmt" && p != "units" && p != "selectedUnit" && p != "itemCode") {
                        // && p!="fromCategory" && p!="selectedUnit"  && p!="preferedUnits" && p!="baseUnit"
                        _.forEach(itm.units, function(u, ii) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "conversionFactor" && p != "baseUnit") {
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
            });
            return items;
        };

        $scope.addRemainingItem_stockEntryForm = function($item, $model, $label) {
            var remaining = angular.copy($scope.stockEntryForm.remainingItems);
            _.forEach($scope.stockEntryForm.remainingItems, function(itm, i) {
                if (itm._id == $item._id) {
                    if (itm.toDate != undefined || itm.fromDate != undefined) {
                        var toD = new Date(itm.toDate);
                        var fD = new Date(itm.fromDate);
                        var cD = new Date();
                        if (cD < fD || cD > toD) {
                            itm.hide = true;
                            alert("Disabled items contract has been expired");
                            //growl.error("Disabled items contract has been expired", {ttl:3000});
                        }
                    }
                    $scope.stockEntryForm.selectedCategory[itm.fromCategory._id] = true;
                    $scope.stockEntryForm.availableItems.unshift(itm);
                    //$scope.stockEntryForm.remainingItems.splice(i,1);
                    remaining.splice(i, 1);
                    $scope.stockEntryForm.itemToAdd = "";
                }
            });
            $scope.stockEntryForm.remainingItems = remaining;
            slectedUnit_entryForm($scope.stockEntryForm.availableItems);
        };

        $scope.getItemsForSearching = function() {

        }

        $scope.deleteSelectedItemEntryForm = function(item) {
            var index = _.findIndex($scope.stockEntryForm.availableItems, { _id: item._id });
            $scope.stockEntryForm.availableItems.splice(index, 1);
            _.forEach($scope.stockEntryForm.availableCategory, function(ac, i) {
                _.forEachRight(ac.item, function(itm, ii) {
                    if (itm._id == item._id) {
                        var varFromCategory = { "_id": ac._id, "categoryName": ac.categoryName };
                        itm["fromCategory"] = varFromCategory;
                        $scope.stockEntryForm.remainingItems.push(angular.copy(itm));
                    }
                });
            });
            //$scope.calculateAmt_entryForm(itm.qty, itm.price, itm, itm.vatPercent)

            $scope.enableSubmitButton_Entry($scope.stockEntryForm.availableItems);
            //$scope.stockEntryForm.remainingItems.push(item);
            $scope.calculateAmt_entryForm();
        };

        function formatStoreForStockEntry(item, vendor, store) {
            var stores = store;
            //delete vendor.pricing;
            stores.vendor = vendor;
            stores.vendor.category = [];
            _.forEach(item, function(itm, i) {
                var index = _.findIndex(stores.vendor.category, { _id: itm.fromCategory._id });
                if (index < 0) {
                    stores.vendor.category.push(itm.fromCategory);
                }
            });

            _.forEach(stores.vendor.category, function(c, i) {
                c.items = [];
                _.forEachRight(item, function(itm, ii) {
                    if (c._id == itm.fromCategory._id) {
                        delete itm.fromCategory;
                        c.items.push(itm);
                        item.splice(ii, 1);
                    }
                });
            });
            //console.log(stores);
            return stores;
        };

        function getItemsHistory_StockEntry(items) {
            if ($state.params != undefined) {
                $scope.stockEditHitory.items = [];
                _.forEach($scope.stockEditEntry, function(itm, i) {
                    itm.status = "deleted";
                    $scope.stockEditHitory.items.push(itm);
                });
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.stockEditHitory.items, { _id: itm._id });
                    if (index < 0) {
                        itm.status = "inserted";
                    } else {
                        itm.updatedFeild = [];
                        if (itm.qty != $scope.stockEditHitory.items[index].qty) {
                            itm.status = "updated";
                            itm.updatedFeild.push("qty");
                        } else if (itm.price != $scope.stockEditHitory.items[index].price) {
                            itm.status = "updated";
                            itm.updatedFeild.push("qty");
                        } else {
                            itm.status = "inserted";
                        }
                        $scope.stockEditHitory.items.splice(index, 1);
                    }
                    $scope.stockEditHitory.items.push(itm);
                    //console.log($scope.stockEditHitory.items);
                });
            };
        };


        /*$scope.submitStockEntry= function(formStockEntry){
         if($state.params.id==undefined){
         $scope.stockEntryForm.enableSubmit=true;
         var itemToSave=getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
         if(formStockEntry.$valid && !$scope.stockEntryForm.isPrint){
         // if($scope.stockEntryForm.transactionId!=undefined){
         //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
         //     var index=_.findIndex($scope.openTrans_Entry,{"transactionId":$scope.stockEntryForm.transactionId});
         //     $scope.openTrans_Entry.splice(index,1);
         // }
         // else{
         //     $scope.stockEntryForm.transactionId=guid();
         // }
         if(itemToSave.length<$scope.stockEntryForm.availableItems.length){
         var isGood=confirm("Some Items are Escaped from Entry ! Are You Confirm?");
         if(isGood==true){
         var storeToSave=formatStore_stockEntryForm($scope.stockEntryForm.store);
         console.log(itemToSave);

         var str=formatStoreForStockEntry(angular.copy(itemToSave),angular.copy($scope.stockEntryForm.vendor),angular.copy(storeToSave));

         $scope.transaction._items=itemToSave;
         $scope.transaction._store=str;
         $scope.transaction.isOpen=false;
         $scope.transaction.cartage=$scope.stockEntryForm.cartage==undefined?0:parseFloat($scope.stockEntryForm.cartage);
         $scope.transaction.discount=$scope.stockEntryForm.discount==undefined?0:parseFloat($scope.stockEntryForm.discount);
         $scope.transaction.discountType=$scope.stockEntryForm.discountType;
         $scope.transaction.processTransaction($scope.stockEntryForm,1);

         $scope.transaction.updateTransaction({},'update').then(function(results){
         if(results!=null){
         manageClosingQty(itemToSave,str._id,'',1);
         $scope.stockEntryForm.isPrint=true;
         $scope.stockEntryForm.isEdit=true;
         $scope.stockEntryForm.isSaved=true;
         $scope.stockEntryForm.printItems=itemToSave;
         growl.success('Success in stock entry', {ttl: 3000});
         growl.success(itemToSave.length+'Items added', {ttl: 3000});
         $scope.stockEntryForm.enableSubmit=false;
         }
         });
         }
         else
         {
         $scope.stockEntryForm.enableSubmit=false;
         }
         }
         else
         {
         var storeToSave=formatStore_stockEntryForm($scope.stockEntryForm.store);
         console.log(itemToSave);

         var str=formatStoreForStockEntry(angular.copy(itemToSave),angular.copy($scope.stockEntryForm.vendor),angular.copy(storeToSave));

         $scope.transaction._items=itemToSave;
         $scope.transaction._store=str;
         $scope.transaction.isOpen=false;
         $scope.transaction.cartage=$scope.stockEntryForm.cartage==undefined?0:parseFloat($scope.stockEntryForm.cartage);
         $scope.transaction.discount=$scope.stockEntryForm.discount==undefined?0:parseFloat($scope.stockEntryForm.discount);
         $scope.transaction.discountType=$scope.stockEntryForm.discountType;
         $scope.transaction.processTransaction($scope.stockEntryForm,1);

         $scope.transaction.updateTransaction({},'update').then(function(results){
         if(results!=null){
         manageClosingQty(itemToSave,str._id,'',1);
         $scope.stockEntryForm.isPrint=true;
         $scope.stockEntryForm.isEdit=true;
         $scope.stockEntryForm.isSaved=true;
         $scope.stockEntryForm.printItems=itemToSave;
         growl.success('Success in stock entry', {ttl: 3000});
         growl.success(itemToSave.length+'Items added', {ttl: 3000});
         $scope.stockEntryForm.enableSubmit=false;
         }
         });
         }
         // $rootScope.db.insert('stockTransaction',
         //     {
         //         "transactionId":$scope.stockEntryForm.transactionId,
         //         "transactionType":"1",
         //         "daySerialNumber":0,
         //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
         //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
         //         "user":JSON.stringify(currentUser),
         //         "store":JSON.stringify(angular.copy(str)),
         //         "toStore":"",
         //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
         //         "vendor":JSON.stringify($scope.stockEntryForm.vendor),
         //         "receiver":"",
         //         "cartage":$scope.stockEntryForm.cartage==undefined?0:$scope.stockEntryForm.cartage,
         //         "discount":$scope.stockEntryForm.discount==undefined?0:$scope.stockEntryForm.discount,
         //         "discountType":$scope.stockEntryForm.discountType,
         //         "serviceCharge":0,
         //         "serviceChargeType":"",
         //         "vat":0,
         //         "vatType":"",
         //         "payment":0,
         //         "heading":"",
         //         "wastageItemType":"",
         //         "wastageEntryType":"",
         //         "created": new Date($scope.stockEntryForm.billDate).toISOString(),
         //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
         //         "templateName":"",
         //         "deployment_id": currentUser.deployment_id,
         //         "tenant_id": currentUser.tenant_id,
         //         "isSynced" : false,
         //         "syncedOn" : undefined
         //     }).then(function (result) {
         //         growl.success('Success in stock entry', {ttl: 3000});
         //         growl.success(itemToSave.length+'Items added', {ttl: 3000});
         //         //$scope.stockEntryForm={enableSubmit:true,discountType:'percent',isTemplateDelete:true,isTemplateSave:true};
         //         //$scope.stockEntryForm.availableItems=[];
         //         if(result.transactionId!=undefined){
         //             //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
         //             var index=_.findIndex($scope.openTrans_Entry,{"transactionId":result.transactionId});
         //             $scope.openTrans_Entry.splice(index,1);
         //         }
         //         $scope.stockEntryForm.isPrint=true;
         //         $scope.stockEntryForm.isEdit=true;
         //         $scope.stockEntryForm.isSaved=true;
         // });
         }
         else
         {
         print_stockEntry(itemToSave);
         $scope.stockEntryForm.enableSubmit=false;
         //Print Here ....
         }
         }
         else
         {
         $scope.stockEntryForm.enableSubmit=true;
         var itemToSave=getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
         if($scope.stockEntryForm.store!=undefined && $scope.stockEntryForm.vendor!=undefined && itemToSave.length>0){
         var isGood=confirm("Some Items are Escaped from Entry ! Are You Confirm?");
         if(isGood) {
         getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
         $scope.stockEditHitory.editHistoryId = guid();
         $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
         $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
         $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
         $scope.stockEditHitory.user = formatUser(currentUser);
         $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
         $scope.stockEditHitory.deployment_id = currentUser.deployment_id;

         EditTransactionHistory.saveData({}, $scope.stockEditHitory, function (result) {
         growl.success('New history created', {ttl: 3000});
         if ($state.params != null) {
         $location.replace().path('/stock/stockEntry');
         }
         });


         var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
         var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));
         $scope.stockEntryForm._store = str;
         $scope.stockEntryForm._id = $scope.stockEntryForm.mainTransactionId;
         var toSave = {
         _store: str, _id: $scope.stockEntryForm._id, discount: parseFloat($scope.stockEntryForm.discount),
         discountType: $scope.stockEntryForm.discountType, cartage: $scope.stockEntryForm.cartage
         };
         StockResource.update({}, toSave, function (bill) {
         console.log(bill);
         });
         }
         }
         else
         {
         $scope.stockEntryForm.enableSubmit=true;
         }
         }
         };*/

        //old submit stock before invoice number
        /*$scope.submitStockEntry = function (formStockEntry) {
          $scope.stockEntryForm.billNoNew = "SE - " + $scope.transaction.daySerialNumber;
          if ($state.params.id == undefined) {
            $scope.checkStockEntryForOpenTransactionAndSave();
            $scope.stockEntryForm.enableSubmit = true;
            var itemToSave = getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
            if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint) {
              // if($scope.stockEntryForm.transactionId!=undefined){
              //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
              //     var index=_.findIndex($scope.openTrans_Entry,{"transactionId":$scope.stockEntryForm.transactionId});
              //     $scope.openTrans_Entry.splice(index,1);
              // }
              // else{
              //     $scope.stockEntryForm.transactionId=guid();
              // }
              console.log(angular.copy($scope.transaction));
              if (itemToSave.length < $scope.stockEntryForm.availableItems.length) {
                var isGood = confirm("Some Items are Escaped from Entry ! Are You Confirm?");
                if (isGood == true) {
                  var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                  console.log(itemToSave);


                  var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor),angular.copy(storeToSave));

                  $scope.transaction._items = itemToSave;
                  $scope.transaction._store = str;
                  $scope.transaction.isOpen = false;
                  $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                  $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                  $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                  $scope.transaction.processTransaction($scope.stockEntryForm, 1);

                  $scope.transaction.updateTransaction({}, 'update').then(function (results) {
                    if (results != null) {
                      manageClosingQty(itemToSave, str._id, '', 1);
                      $scope.stockEntryForm.isPrint = true;
                      $scope.stockEntryForm.isEdit = true;
                      $scope.stockEntryForm.isSaved = true;
                      $scope.stockEntryForm.printItems = itemToSave;
                      growl.success('Success in stock entry', {ttl: 3000});
                      growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                      $scope.stockEntryForm.enableSubmit = false;
                    }
                  });
                }
                else {
                  $scope.stockEntryForm.enableSubmit = false;
                }
              }
              else {
                var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                console.log(itemToSave);

                var str = formatStoreForStockEntry(angular.copy(itemToSave),angular.copy($scope.stockEntryForm.vendor),angular.copy(storeToSave));

                $scope.transaction._items = itemToSave;
                $scope.transaction._store = str;
                $scope.transaction.isOpen = false;
                $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                $scope.transaction.processTransaction($scope.stockEntryForm, 1);

                $scope.transaction.updateTransaction({}, 'update').then(function (results) {
                  if (results != null) {
                    manageClosingQty(itemToSave, str._id, '', 1);
                    $scope.stockEntryForm.isPrint = true;
                    $scope.stockEntryForm.isEdit = true;
                    $scope.stockEntryForm.isSaved = true;
                    $scope.stockEntryForm.printItems = itemToSave;
                    growl.success('Success in stock entry', {ttl: 3000});
                    growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                    $scope.stockEntryForm.enableSubmit = false;
                  }
                });
              }
              // $rootScope.db.insert('stockTransaction',
              //     {
              //         "transactionId":$scope.stockEntryForm.transactionId,
              //         "transactionType":"1",
              //         "daySerialNumber":0,
              //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
              //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
              //         "user":JSON.stringify(currentUser),
              //         "store":JSON.stringify(angular.copy(str)),
              //         "toStore":"",
              //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
              //         "vendor":JSON.stringify($scope.stockEntryForm.vendor),
              //         "receiver":"",
              //         "cartage":$scope.stockEntryForm.cartage==undefined?0:$scope.stockEntryForm.cartage,
              //         "discount":$scope.stockEntryForm.discount==undefined?0:$scope.stockEntryForm.discount,
              //         "discountType":$scope.stockEntryForm.discountType,
              //         "serviceCharge":0,
              //         "serviceChargeType":"",
              //         "vat":0,
              //         "vatType":"",
              //         "payment":0,
              //         "heading":"",
              //         "wastageItemType":"",
              //         "wastageEntryType":"",
              //         "created": new Date($scope.stockEntryForm.billDate).toISOString(),
              //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
              //         "templateName":"",
              //         "deployment_id": currentUser.deployment_id,
              //         "tenant_id": currentUser.tenant_id,
              //         "isSynced" : false,
              //         "syncedOn" : undefined
              //     }).then(function (result) {
              //         growl.success('Success in stock entry', {ttl: 3000});
              //         growl.success(itemToSave.length+'Items added', {ttl: 3000});
              //         //$scope.stockEntryForm={enableSubmit:true,discountType:'percent',isTemplateDelete:true,isTemplateSave:true};
              //         //$scope.stockEntryForm.availableItems=[];
              //         if(result.transactionId!=undefined){
              //             //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
              //             var index=_.findIndex($scope.openTrans_Entry,{"transactionId":result.transactionId});
              //             $scope.openTrans_Entry.splice(index,1);
              //         }
              //         $scope.stockEntryForm.isPrint=true;
              //         $scope.stockEntryForm.isEdit=true;
              //         $scope.stockEntryForm.isSaved=true;
              // });
            }
            else {
              print_stockEntry(itemToSave);
              $scope.stockEntryForm.enableSubmit = false;
              //Print Here ....
            }
          }
          else if ($state.current.name == 'stock.performStockEntry' && $state.params.id){
            var itemToSave = getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
            if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint) {
              // if($scope.stockEntryForm.transactionId!=undefined){
              //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
              //     var index=_.findIndex($scope.openTrans_Entry,{"transactionId":$scope.stockEntryForm.transactionId});
              //     $scope.openTrans_Entry.splice(index,1);
              // }
              // else{
              //     $scope.stockEntryForm.transactionId=guid();
              // }
              console.log(angular.copy($scope.transaction));
              if (itemToSave.length < $scope.stockEntryForm.availableItems.length) {
                var isGood = confirm("Some Items are Escaped from Entry ! Are You Confirm?");
                if (isGood == true) {
                  var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                  console.log(itemToSave);
                  var str = formatStoreForStockEntry(angular.copy(itemToSave),angular.copy($scope.stockEntryForm.vendor),angular.copy(storeToSave));

                  $scope.transaction._items = itemToSave;
                  $scope.transaction._store = str;
                  $scope.transaction.isOpen = false;
                  $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                  $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                  $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                  $scope.transaction.processTransaction($scope.stockEntryForm, 1);
                  //$scope.transaction.isSynced = true;
                  //$scope.transaction.updateTransaction({}, 'update').then(function (results) {
                  //  if (results != null) {
                  //    //manageClosingQty(itemToSave, str._id, '', 1);
                  //    //$scope.stockEntryForm.isPrint = true;
                  //    //$scope.stockEntryForm.isEdit = true;
                  //    //$scope.stockEntryForm.isSaved = true;
                  //    //$scope.stockEntryForm.printItems = itemToSave;
                  //    //growl.success('Success in stock entry', {ttl: 3000});
                  //    //growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                  //    //$scope.stockEntryForm.enableSubmit = false;
                  //  }
                  //});
                  StockResource.createWithTransactionId({}, bills, function (bill) {
                    growl.success("Stock Transaction Synced", {ttl: 3000});
                    $scope.order.isReceived = true;
                    var po = new purchaseOrder($scope.order);
                    po.$update(function (o) {

                    });
                    $state.go("stock.stockEntry");
                  }, function (err) {
                    growl.error("Error syncing transaction!", {ttl: 3000});
                  });
                }
                else {
                  $scope.stockEntryForm.enableSubmit = false;
                }
              }
              else {
                var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                console.log(itemToSave);

                var str = formatStoreForStockEntry(angular.copy(itemToSave),angular.copy($scope.stockEntryForm.vendor),angular.copy(storeToSave));

                $scope.transaction._items = itemToSave;
                $scope.transaction._store = str;
                $scope.transaction.isOpen = false;
                $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                $scope.transaction.processTransaction($scope.stockEntryForm, 1);

                //$scope.transaction.updateTransaction({}, 'update').then(function (results) {
                //  if (results != null) {
                //    manageClosingQty(itemToSave, str._id, '', 1);
                //    $scope.stockEntryForm.isPrint = true;
                //    $scope.stockEntryForm.isEdit = true;
                //    $scope.stockEntryForm.isSaved = true;
                //    $scope.stockEntryForm.printItems = itemToSave;
                //    growl.success('Success in stock entry', {ttl: 3000});
                //    growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                //    $scope.stockEntryForm.enableSubmit = false;
                //  }
                //});
                StockResource.createWithTransactionId({}, $scope.transaction, function (bill) {
                  growl.success("Stock Transaction Synced", {ttl: 3000});
                  $scope.order.isReceived = true;
                  var po = new purchaseOrder($scope.order);
                  po.$update(function (o) {

                  });
                  $state.go("stock.stockEntry");
                }, function (err) {
                  growl.error("Error syncing transaction", {ttl:3000});
                });
              }
              // $rootScope.db.insert('stockTransaction',
              //     {
              //         "transactionId":$scope.stockEntryForm.transactionId,
              //         "transactionType":"1",
              //         "daySerialNumber":0,
              //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
              //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
              //         "user":JSON.stringify(currentUser),
              //         "store":JSON.stringify(angular.copy(str)),
              //         "toStore":"",
              //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
              //         "vendor":JSON.stringify($scope.stockEntryForm.vendor),
              //         "receiver":"",
              //         "cartage":$scope.stockEntryForm.cartage==undefined?0:$scope.stockEntryForm.cartage,
              //         "discount":$scope.stockEntryForm.discount==undefined?0:$scope.stockEntryForm.discount,
              //         "discountType":$scope.stockEntryForm.discountType,
              //         "serviceCharge":0,
              //         "serviceChargeType":"",
              //         "vat":0,
              //         "vatType":"",
              //         "payment":0,
              //         "heading":"",
              //         "wastageItemType":"",
              //         "wastageEntryType":"",
              //         "created": new Date($scope.stockEntryForm.billDate).toISOString(),
              //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
              //         "templateName":"",
              //         "deployment_id": currentUser.deployment_id,
              //         "tenant_id": currentUser.tenant_id,
              //         "isSynced" : false,
              //         "syncedOn" : undefined
              //     }).then(function (result) {
              //         growl.success('Success in stock entry', {ttl: 3000});
              //         growl.success(itemToSave.length+'Items added', {ttl: 3000});
              //         //$scope.stockEntryForm={enableSubmit:true,discountType:'percent',isTemplateDelete:true,isTemplateSave:true};
              //         //$scope.stockEntryForm.availableItems=[];
              //         if(result.transactionId!=undefined){
              //             //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
              //             var index=_.findIndex($scope.openTrans_Entry,{"transactionId":result.transactionId});
              //             $scope.openTrans_Entry.splice(index,1);
              //         }
              //         $scope.stockEntryForm.isPrint=true;
              //         $scope.stockEntryForm.isEdit=true;
              //         $scope.stockEntryForm.isSaved=true;
              // });
            }
            else {
              print_stockEntry(itemToSave);
              $scope.stockEntryForm.enableSubmit = false;
              //Print Here ....
            }
          }
          else {
            console.log('Here while editing',$scope.stockEntryForm)
            $scope.stockEntryForm.enableSubmit = true;
            var itemToSave = getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
            if (itemToSave.length < $scope.stockEntryForm.availableItems.length) {
              var isGood = confirm("Some Items are Escaped from Entry ! Are You Confirm?");
              //alert(isGood);
              if (isGood) {
                if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint && $scope.stockEntryForm.store != undefined && $scope.stockEntryForm.vendor != undefined && itemToSave.length > 0) {

                  getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                  $scope.stockEditHitory.editHistoryId = guid();
                  $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                  $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                  $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                  $scope.stockEditHitory.user = formatUser(currentUser);
                  $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                  $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                  $scope.stockEditHitory.discount =$scope.stockEntryForm.discount;
                  $scope.stockEditHitory.discountType =$scope.stockEntryForm.discountType;


                  EditTransactionHistory.saveData({}, $scope.stockEditHitory, function (result) {
                    growl.success('New history created', {ttl: 3000});
                    if ($state.params != null) {
                      $location.replace().path('/stock/stockEntry');
                    }
                  });
                }
                else {
                  getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                  $scope.stockEditHitory.editHistoryId = guid();
                  $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                  $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                  $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                  $scope.stockEditHitory.user = formatUser(currentUser);
                  $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                  $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                  $scope.stockEditHitory.discount =$scope.stockEntryForm.discount;
                  $scope.stockEditHitory.discountType =$scope.stockEntryForm.discountType;

                  EditTransactionHistory.saveData({}, $scope.stockEditHitory, function (result) {
                    growl.success('New history created', {ttl: 3000});
                    if ($state.params != null) {
                      $location.replace().path('/stock/stockEntry');
                    }
                  });
                }
                var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                var str = formatStoreForStockEntry(angular.copy(itemToSave),angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));
                $scope.stockEntryForm._store = str;
                $scope.stockEntryForm._id = $scope.stockEntryForm.mainTransactionId;
                var toSave = {
                  _store: str, _id: $scope.stockEntryForm._id, discount: parseFloat($scope.stockEntryForm.discount),
                  discountType: $scope.stockEntryForm.discountType, cartage: $scope.stockEntryForm.cartage
                };
                console.log(toSave);
                StockResource.update({}, toSave, function (bill) {
                  console.log(bill);
                });
              }
              else {
                $scope.stockEntryForm.enableSubmit = true;
              }
            }
            else {
              if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint && $scope.stockEntryForm.store != undefined && $scope.stockEntryForm.vendor != undefined && itemToSave.length > 0) {

                getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                $scope.stockEditHitory.editHistoryId = guid();
                $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                $scope.stockEditHitory.user = formatUser(currentUser);
                $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                $scope.stockEditHitory.discount =$scope.stockEntryForm.discount;
                $scope.stockEditHitory.discountType =$scope.stockEntryForm.discountType;

                EditTransactionHistory.saveData({}, $scope.stockEditHitory, function (result) {
                  growl.success('New history created', {ttl: 3000});
                  if ($state.params != null) {
                    $location.replace().path('/stock/stockEntry');
                  }
                });
              }
              else {
                getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                $scope.stockEditHitory.editHistoryId = guid();
                $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                $scope.stockEditHitory.user = formatUser(currentUser);
                $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                $scope.stockEditHitory.discount =$scope.stockEntryForm.discount;
                $scope.stockEditHitory.discountType =$scope.stockEntryForm.discountType;

                EditTransactionHistory.saveData({}, $scope.stockEditHitory, function (result) {
                  growl.success('New history created', {ttl: 3000});
                  if ($state.params != null) {
                    $location.replace().path('/stock/stockEntry');
                  }
                });
              }
              var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
              var str = formatStoreForStockEntry(angular.copy(itemToSave),angular.copy($scope.stockEntryForm.vendor),angular.copy(storeToSave));
              $scope.stockEntryForm._store = str;
              $scope.stockEntryForm._id = $scope.stockEntryForm.mainTransactionId;
              var toSave = {
                _store: str, _id: $scope.stockEntryForm._id, discount: parseFloat($scope.stockEntryForm.discount),
                discountType: $scope.stockEntryForm.discountType, cartage: $scope.stockEntryForm.cartage
              };
              StockResource.update({}, toSave, function (bill) {
                console.log(bill);
              });

            }
          }
        };*/
        function checkAllThingsStockEntry() {
            var flag = false;
                if($scope.stockEntryForm.charges)
                {
                    if($scope.stockEntryForm.charges.length > 0)
                    {
                        for(var i =0;i<$scope.stockEntryForm.charges.length; ++i) {
                        var charge = $scope.stockEntryForm.charges[i];
                        if(!charge.chargeName || !charge.chargeValue || isNaN(charge.chargeValue) || !charge.operationType || !charge.type || charge.isEdit == true) {
                                flag = true;
                                break;
                            }
                        };
                    }
                }
            return flag;
        }
        $scope.submitStockEntry = function(formStockEntry) {
            if(checkAllThingsStockEntry() == false)
            {
                if ($state.params.id == undefined) {
                    $scope.checkStockEntryForOpenTransactionAndSave();
                    console.log('testing 1',formStockEntry);
                    $scope.stockEntryForm.enableSubmit = true;
                    var itemToSave = getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
                    //var flag = checkAllThingsStockEntry(itemToSave);
                    if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint) {
                        
                        if (itemToSave.length < $scope.stockEntryForm.availableItems.length && itemToSave.length != 0) {
                            var isGood = confirm("Some Items are Escaped from Entry ! Are You Confirm?");

                            if (isGood == true) {

                                if (!$scope.stockEntryForm.invoiceNumber)
                                    var isInvoiceNumber = confirm('Invoice Number skipped from entry ! Still you want to submit ? ')
                                else
                                    isInvoiceNumber = true
                                if (isInvoiceNumber == true) {
                                    var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                                    console.log(itemToSave);
                                    var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));

                                    $scope.transaction._items = itemToSave;
                                    if(str.vendor.pricing)
                                    delete str.vendor.pricing;
                                    $scope.transaction._store = str;
                                    $scope.transaction.invoiceNumber = $scope.stockEntryForm.invoiceNumber;
                                    $scope.transaction.batchNumber = $scope.stockEntryForm.batchNumber == undefined ? '' : $scope.stockEntryForm.batchNumber;
                                    $scope.transaction.isOpen = false;
                                    $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                                    $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                                    $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                                    $scope.transaction.charges = $scope.stockEntryForm.charges;
                                    $scope.transaction.processTransaction($scope.stockEntryForm, 1);
                                    $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                        if (results != null) {
                                            manageClosingQty(itemToSave, str._id, '', 1);
                                            $scope.stockEntryForm.isPrint = true;
                                            $scope.stockEntryForm.isEdit = true;
                                            $scope.stockEntryForm.isSaved = true;
                                            $scope.stockEntryForm.printItems = itemToSave;
                                            growl.success('Success in stock entry', { ttl: 3000 });
                                            growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                                            $scope.stockEntryForm.enableSubmit = false;
                                            intercom.registerEvent('StockTransaction');
                                        }
                                    });
                                } else {
                                    $scope.stockEntryForm.enableSubmit = false;
                                }
                            } else {
                                $scope.stockEntryForm.enableSubmit = false;
                            }
                        }else if(itemToSave.length == 0)
                        {
                           growl.error('There should be atleast one item for transaction!',  {ttl:3000}); 
                        } else {
                            if (!$scope.stockEntryForm.invoiceNumber)
                                var isInvoiceNumber = confirm('Invoice Number skipped from entry ! Still you want to submit ? ')
                            else
                                isInvoiceNumber = true
                            if (isInvoiceNumber == true) {
                                console.log('inside ifff');
                                var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                                console.log(itemToSave);

                                var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));

                                $scope.transaction._items = itemToSave;
                                $scope.transaction._store = str;
                                $scope.transaction.isOpen = false;
                                $scope.transaction.invoiceNumber = $scope.stockEntryForm.invoiceNumber;
                                $scope.transaction.batchNumber = $scope.stockEntryForm.batchNumber == undefined ? '' : $scope.stockEntryForm.batchNumber;
                                $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                                $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                                $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                                $scope.transaction.charges = $scope.stockEntryForm.charges;
                                $scope.transaction.processTransaction($scope.stockEntryForm, 1);
                                $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                    if (results != null) {
                                        manageClosingQty(itemToSave, str._id, '', 1);
                                        $scope.stockEntryForm.isPrint = true;
                                        $scope.stockEntryForm.isEdit = true;
                                        $scope.stockEntryForm.isSaved = true;
                                        $scope.stockEntryForm.printItems = itemToSave;
                                        growl.success('Success in stock entry', { ttl: 3000 });
                                        growl.success(itemToSave.length + 'Items added', { ttl: 3000 });

                                        // print_stockEntry(itemToSave);
                                        $scope.stockEntryForm.enableSubmit = false;
                                        intercom.registerEvent('StockTransaction');
                                    }
                                });





                            } // end of Invoice Number check
                            else {
                                console.log('inside else');
                                $scope.stockEntryForm.enableSubmit = false;
                            }


                        }
                        // $rootScope.db.insert('stockTransaction',
                        //     {
                        //         "transactionId":$scope.stockEntryForm.transactionId,
                        //         "transactionType":"1",
                        //         "daySerialNumber":0,
                        //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                        //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                        //         "user":JSON.stringify(currentUser),
                        //         "store":JSON.stringify(angular.copy(str)),
                        //         "toStore":"",
                        //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                        //         "vendor":JSON.stringify($scope.stockEntryForm.vendor),
                        //         "receiver":"",
                        //         "cartage":$scope.stockEntryForm.cartage==undefined?0:$scope.stockEntryForm.cartage,
                        //         "discount":$scope.stockEntryForm.discount==undefined?0:$scope.stockEntryForm.discount,
                        //         "discountType":$scope.stockEntryForm.discountType,
                        //         "serviceCharge":0,
                        //         "serviceChargeType":"",
                        //         "vat":0,
                        //         "vatType":"",
                        //         "payment":0,
                        //         "heading":"",
                        //         "wastageItemType":"",
                        //         "wastageEntryType":"",
                        //         "created": new Date($scope.stockEntryForm.billDate).toISOString(),
                        //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                        //         "templateName":"",
                        //         "deployment_id": currentUser.deployment_id,
                        //         "tenant_id": currentUser.tenant_id,
                        //         "isSynced" : false,
                        //         "syncedOn" : undefined
                        //     }).then(function (result) {
                        //         growl.success('Success in stock entry', {ttl: 3000});
                        //         growl.success(itemToSave.length+'Items added', {ttl: 3000});
                        //         //$scope.stockEntryForm={enableSubmit:true,discountType:'percent',isTemplateDelete:true,isTemplateSave:true};
                        //         //$scope.stockEntryForm.availableItems=[];
                        //         if(result.transactionId!=undefined){
                        //             //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
                        //             var index=_.findIndex($scope.openTrans_Entry,{"transactionId":result.transactionId});
                        //             $scope.openTrans_Entry.splice(index,1);
                        //         }
                        //         $scope.stockEntryForm.isPrint=true;
                        //         $scope.stockEntryForm.isEdit=true;
                        //         $scope.stockEntryForm.isSaved=true;
                        // });
                    } else {
                        console.log('elseeeeeeeeeeeee');
                        print_stockEntry(itemToSave);
                        $scope.stockEntryForm.enableSubmit = false;
                        //Print Here ....
                    }
                } else if ($state.current.name == 'stock.performStockEntry' && $state.params.id) {
                    console.log('testing 2')
                    var itemToSave = getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
                    if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint) {
                        // if($scope.stockEntryForm.transactionId!=undefined){
                        //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
                        //     var index=_.findIndex($scope.openTrans_Entry,{"transactionId":$scope.stockEntryForm.transactionId});
                        //     $scope.openTrans_Entry.splice(index,1);
                        // }
                        // else{
                        //     $scope.stockEntryForm.transactionId=guid();
                        // }
                        // console.log(angular.copy($scope.transaction));

                        // console.log("order",$scope.order)
                        
                        comparePOReceive(itemToSave,oldPO)
                        
                        
                        status= $scope.order.status
                        if(qtyChanged==false){
                            
                            
                            poReceiveUpdate(itemToSave,$scope.order)
                            $scope.order.status="complete"
                            status="complete"
                        }
                        else{
                            
                            poReceiveUpdate(itemToSave,$scope.order)
                            
                            $scope.order.status= "partial"
                            status="partial"
                        }

                        if (itemToSave.length < $scope.stockEntryForm.availableItems.length) {
                            var isGood = confirm("Some Items are Escaped from Entry ! Are You Confirm?");
                            if (isGood == true) {
                                var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                                console.log(itemToSave);
                                var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));

                                $scope.transaction._items = itemToSave;
                                
                                $scope.transaction._store = str;
                                $scope.transaction.isOpen = false;
                                $scope.transaction.invoiceNumber = $scope.stockEntryForm.invoiceNumber
                                $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                                $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                                $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                                $scope.transaction.charges = $scope.stockEntryForm.charges;
                                $scope.transaction.processTransaction($scope.stockEntryForm, 1);
                                $scope.transaction.isSynced = true;
                                $scope.transaction.updateTransaction({}, 'insert').then(function (results) {
                                 if (results != null) {
                                   //manageClosingQty(itemToSave, str._id, '', 1);
                                   //$scope.stockEntryForm.isPrint = true;
                                   //$scope.stockEntryForm.isEdit = true;
                                   //$scope.stockEntryForm.isSaved = true;
                                   //$scope.stockEntryForm.printItems = itemToSave;
                                   //growl.success('Success in stock entry', {ttl: 3000});
                                   //growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                                   //$scope.stockEntryForm.enableSubmit = false;
                                 }
                                });
                                StockResource.createWithTransactionId({}, bills, function(bill) {
                                    growl.success("Stock Transaction Synced", { ttl: 3000 });
                                    $scope.order.isReceived = true;
                                     if(status=="partial"){
                                        console.log("receive")
                                        $scope.order.isReceived= false
                                        console.log("receive")
                                    }
                                    else{
                                        $scope.order.isReceived= true
                                    }
                                    console.log("$scope.order",angular.copy($scope.order))
                                    var po = new purchaseOrder( angular.copy($scope.order) );
                                    po.$update(function(o) {

                                    });
                                    $state.go("stock.stockEntry");
                                }, function(err) {
                                    growl.error("Error syncing transaction!", { ttl: 3000 });
                                });
                            } else {
                                $scope.stockEntryForm.enableSubmit = false;
                            }
                        } else {
                            var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                            console.log(itemToSave);

                            var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));

                            $scope.transaction._items = itemToSave;
                            console.log("order",$scope.order)
                            $scope.transaction._store = str;
                            $scope.transaction.isOpen = false;
                            $scope.transaction.invoiceNumber = $scope.stockEntryForm.invoiceNumber
                            $scope.transaction.cartage = $scope.stockEntryForm.cartage == undefined ? 0 : parseFloat($scope.stockEntryForm.cartage);
                            $scope.transaction.discount = $scope.stockEntryForm.discount == undefined ? 0 : parseFloat($scope.stockEntryForm.discount);
                            $scope.transaction.discountType = $scope.stockEntryForm.discountType;
                            $scope.transaction.charges = $scope.stockEntryForm.charges;
                            $scope.transaction.processTransaction($scope.stockEntryForm, 1);
                            $scope.transaction.isSynced = true;
                            $scope.transaction.updateTransaction({}, 'insert').then(function (results) {
                             if (results != null) {
                               // manageClosingQty(itemToSave, str._id, '', 1);
                               // $scope.stockEntryForm.isPrint = true;
                               // $scope.stockEntryForm.isEdit = true;
                               // $scope.stockEntryForm.isSaved = true;
                               // $scope.stockEntryForm.printItems = itemToSave;
                               // growl.success('Success in stock entry', {ttl: 3000});
                               // growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                               // $scope.stockEntryForm.enableSubmit = false;
                             }
                            });
                            StockResource.createWithTransactionId({}, $scope.transaction, function(bill) {
                                growl.success("Stock Transaction Synced", { ttl: 3000 });
                                $scope.order.isReceived = true;
                                 if(status=="partial"){
                                        console.log("receive")
                                        $scope.order.isReceived= false
                                        console.log("receive")
                                    }
                                    else{
                                        $scope.order.isReceived= true
                                    }
                                console.log("$scope.order",angular.copy($scope.order))
                                var po = new purchaseOrder( angular.copy($scope.order) );
                                po.$update(function(o) {

                                });
                                $state.go("stock.stockEntry");
                            }, function(err) {
                                growl.error("Error syncing transaction", { ttl: 3000 });
                            });
                        }
                        // $rootScope.db.insert('stockTransaction',
                        //     {
                        //         "transactionId":$scope.stockEntryForm.transactionId,
                        //         "transactionType":"1",
                        //         "daySerialNumber":0,
                        //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                        //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                        //         "user":JSON.stringify(currentUser),
                        //         "store":JSON.stringify(angular.copy(str)),
                        //         "toStore":"",
                        //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                        //         "vendor":JSON.stringify($scope.stockEntryForm.vendor),
                        //         "receiver":"",
                        //         "cartage":$scope.stockEntryForm.cartage==undefined?0:$scope.stockEntryForm.cartage,
                        //         "discount":$scope.stockEntryForm.discount==undefined?0:$scope.stockEntryForm.discount,
                        //         "discountType":$scope.stockEntryForm.discountType,
                        //         "serviceCharge":0,
                        //         "serviceChargeType":"",
                        //         "vat":0,
                        //         "vatType":"",
                        //         "payment":0,
                        //         "heading":"",
                        //         "wastageItemType":"",
                        //         "wastageEntryType":"",
                        //         "created": new Date($scope.stockEntryForm.billDate).toISOString(),
                        //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                        //         "templateName":"",
                        //         "deployment_id": currentUser.deployment_id,
                        //         "tenant_id": currentUser.tenant_id,
                        //         "isSynced" : false,
                        //         "syncedOn" : undefined
                        //     }).then(function (result) {
                        //         growl.success('Success in stock entry', {ttl: 3000});
                        //         growl.success(itemToSave.length+'Items added', {ttl: 3000});
                        //         //$scope.stockEntryForm={enableSubmit:true,discountType:'percent',isTemplateDelete:true,isTemplateSave:true};
                        //         //$scope.stockEntryForm.availableItems=[];
                        //         if(result.transactionId!=undefined){
                        //             //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockEntryForm.transactionId});
                        //             var index=_.findIndex($scope.openTrans_Entry,{"transactionId":result.transactionId});
                        //             $scope.openTrans_Entry.splice(index,1);
                        //         }
                        //         $scope.stockEntryForm.isPrint=true;
                        //         $scope.stockEntryForm.isEdit=true;
                        //         $scope.stockEntryForm.isSaved=true;
                        // });
                    } else {
                        print_stockEntry(itemToSave);
                        $scope.stockEntryForm.enableSubmit = false;
                        //Print Here ....
                    }
                } else {
                    console.log('testing 3')
                    console.log('Here while editing', $scope.stockEntryForm)
                    $scope.stockEntryForm.enableSubmit = true;
                    var itemToSave = getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
                    if (itemToSave.length < $scope.stockEntryForm.availableItems.length) {
                        var isGood = confirm("Some Items are Escaped from Entry ! Are You Confirm?");
                        //alert(isGood);
                        if (isGood) {
                            if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint && $scope.stockEntryForm.store != undefined && $scope.stockEntryForm.vendor != undefined && itemToSave.length > 0) {

                                getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                                $scope.stockEditHitory.editHistoryId = guid();
                                $scope.stockEditHitory.invoiceNumber = $scope.stockEntryForm.invoiceNumber;
                                $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                                $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                                $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                                $scope.stockEditHitory.user = formatUser(currentUser);
                                $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                                $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                                $scope.stockEditHitory.discount = $scope.stockEntryForm.discount;
                                $scope.stockEditHitory.discountType = $scope.stockEntryForm.discountType;
                                $scope.stockEditHistory.charges = $scope.stockEntryForm.charges;

                                console.log('-----------------------');
                                console.log($scope.stockEditHitory);
                                EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                                    growl.success('New history created', { ttl: 3000 });
                                    registerEvent('StockTransaction_StockEntry');
                                    if ($state.params != null) {
                                        $location.replace().path('/stock/stockEntry');
                                    }
                                });
                            } else {
                                getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                                $scope.stockEditHitory.editHistoryId = guid();
                                $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                                $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                                $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                                $scope.stockEditHitory.invoiceNumber = $scope.stockEntryForm.invoiceNumber;
                                $scope.stockEditHitory.user = formatUser(currentUser);
                                $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                                $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                                $scope.stockEditHitory.discount = $scope.stockEntryForm.discount;
                                $scope.stockEditHitory.discountType = $scope.stockEntryForm.discountType;
                                $scope.stockEditHitory.charges = $scope.stockEntryForm.charges;

                                console.log('-----------------------');
                                console.log($scope.stockEditHitory);
                                EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                                    growl.success('New history created', { ttl: 3000 });
                                    registerEvent('StockTransaction_StockEntry');
                                    if ($state.params != null) {
                                        $location.replace().path('/stock/stockEntry');
                                    }
                                });
                            }
                            var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                            var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));
                            $scope.stockEntryForm._store = str;
                            $scope.stockEntryForm._id = $scope.stockEntryForm.mainTransactionId;
                            var toSave = {
                                _store: str,
                                _id: $scope.stockEntryForm._id,
                                discount: parseFloat($scope.stockEntryForm.discount),
                                discountType: $scope.stockEntryForm.discountType,
                                cartage: $scope.stockEntryForm.cartage,
                                invoiceNumber: $scope.stockEntryForm.invoiceNumber
                            };
                            console.log('Update transaction----------------');
                            console.log(toSave);
                            StockResource.update({}, toSave, function(bill) {
                                console.log(bill);
                            });
                        } else {
                            $scope.stockEntryForm.enableSubmit = true;
                        }
                    } else {
                        if (formStockEntry.$valid && !$scope.stockEntryForm.isPrint && $scope.stockEntryForm.store != undefined && $scope.stockEntryForm.vendor != undefined && itemToSave.length > 0) {
                            console.log('$scope.stockEntryForm',$scope.stockEntryForm);
                            getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                            $scope.stockEditHitory.editHistoryId = guid();
                            $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                            $scope.stockEditHitory.invoiceNumber = $scope.stockEntryForm.invoiceNumber;
                            $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                            $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                            $scope.stockEditHitory.user = formatUser(currentUser);
                            $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                            $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                            $scope.stockEditHitory.discount = $scope.stockEntryForm.discount;
                            $scope.stockEditHitory.discountType = $scope.stockEntryForm.discountType;
                            

                            
                            console.log($scope.stockEditHitory);
                            EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                                growl.success('New history created', { ttl: 3000 });
                                if ($state.params != null) {
                                    $location.replace().path('/stock/stockEntry');
                                }
                            });
                        } else {
                            getItemsHistory_StockEntry($scope.stockEntryForm.availableItems);
                            $scope.stockEditHitory.editHistoryId = guid();
                            $scope.stockEditHitory.transactionId = $scope.stockEntryForm.transactionId;
                            $scope.stockEditHitory.mainTransactionId = $scope.stockEntryForm.mainTransactionId;
                            $scope.stockEditHitory.billNo = $scope.stockEntryForm.tempBillNo;
                            $scope.stockEditHitory.invoiceNumber = $scope.stockEntryForm.invoiceNumber;
                            $scope.stockEditHitory.user = formatUser(currentUser);
                            $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                            $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                            $scope.stockEditHitory.discount = $scope.stockEntryForm.discount;
                            $scope.stockEditHitory.discountType = $scope.stockEntryForm.discountType;

                            console.log('-----------------------');
                            console.log($scope.stockEditHitory);
                            EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                                growl.success('New history created', { ttl: 3000 });
                                if ($state.params != null) {
                                    $location.replace().path('/stock/stockEntry');
                                }
                            });
                        }
                        var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                        var str = formatStoreForStockEntry(angular.copy(itemToSave), angular.copy($scope.stockEntryForm.vendor), angular.copy(storeToSave));
                        $scope.stockEntryForm._store = str;
                        $scope.stockEntryForm._id = $scope.stockEntryForm.mainTransactionId;
                        var toSave = {
                            _store: str,
                            _id: $scope.stockEntryForm._id,
                            discount: parseFloat($scope.stockEntryForm.discount),
                            discountType: $scope.stockEntryForm.discountType,
                            charges: $scope.stockEntryForm.charges,
                            cartage: $scope.stockEntryForm.cartage,
                            invoiceNumber: $scope.stockEntryForm.invoiceNumber
                        };
                        console.log('Update transaction-------------------');
                        console.log(toSave);
                        StockResource.update({}, toSave, function(bill) {
                            console.log(bill);
                        });

                    }
                }
            }
            else{
                growl.error('Please fill out the details correctly.' ,{ttl:3000})
            }    
        };

         var qtyChanged=false,status;
        function comparePOReceive(itemToSave,oldPO){
            _.forEach(itemToSave,function(item){
                _.forEach(oldPO.items,function(oldPO){
                    if(item._id==oldPO._id){
                        var itemQty=parseFloat(item.qty).toFixed(3)
                        var oldPOQty=parseFloat(oldPO.qty- oldPO.receiveQty).toFixed(3)
                        if(itemQty< oldPOQty ){
                            qtyChanged=true
                           //oldPO.qty=itemQty 
                         //  oldPO.receiveQty=itemQty 
                        }
                    
                    }
                })
            })


            _.forEach(oldPO.items,function(item){
                _.forEach($scope.order.items,function(newPO){
                    if(item._id==newPO._id){
                           console.log("order qty",newPO.qty) 
                           console.log("new rec",item.qty)
                           console.log("rece qty",newPO.receiveQty) 
                           console.log("recc qty",item.receiveQty)
                           newPO.receiveQty=item.receiveQty 


                           newPO.qty= item.qty

                           console.log("new rec",newPO.receiveQty)
                    }
                })
            })
        }
       
        function poReceiveUpdate(itemToSave,order){
            console.log("calledddddddddddddddd")
               _.forEach(itemToSave,function(item){
                _.forEach($scope.order.items,function(oldPO){
                    if(item._id==oldPO._id){
                        var itemQty= parseFloat(item.qty).toFixed(3)
                        var oldPOQty=  parseFloat(oldPO.qty).toFixed(3)
                        if(itemQty == oldPOQty ){
                          
                          console.log("---------------------")
                          console.log("oldPO",angular.copy(oldPO) )
                          console.log("oldPOQty",oldPOQty)
                          console.log("itemQty",itemQty)
                          if(oldPO.receiveQty!=0)
                            oldPO.receiveQty = parseFloat( parseFloat(itemQty) + parseFloat(oldPOQty) ).toFixed(3)
                          else
                            oldPO.receiveQty = parseFloat( parseFloat(oldPOQty) ).toFixed(3)
                        }
                        else if(itemQty < oldPOQty){
                            console.log("elseeeeeeeee---------------------")
                            console.log("itemQty",item.qty)
                            console.log("oldPO.receiveQty",oldPO.receiveQty)
                            oldPO.receiveQty = parseFloat( parseFloat(item.qty) + parseFloat(oldPO.receiveQty) ).toFixed(3)
                            console.log("old receive",angular.copy(oldPO.receiveQty ) )
                        }
                    
                    }
                })
            })
            console.log("order ------", angular.copy($scope.order) )

        }

        function formatUser(user) {
            var u = angular.copy(user);
            for (var p in u) {
                if (p != "_id" && p != "firstname" && p != "lastname" && p != "username" && p != "tenant_id" && p != "deployment_id" && p != "name" && p != "role") {
                    delete u[p];
                }
            }
            return u;
        }

        //--------------------------------------Stock Sale Entry OPen Transaction -------------------------------//
        $scope.new_StockSaleEntry = function() {
            $scope.stockSaleForm.isNew = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 2, null, null);
            $scope.transaction.generateBillNumbers(2).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.stockSaleForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                else
                    $scope.stockSaleForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                // $scope.stockSaleForm.tempBillNo = "Temp/SS -" + snumbers.billnumber;
            });
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            console.log("serverDate", serverD)
            console.log("diffMili", diffInMili)
            console.log("diffSec", sec)
            $scope.stockSaleForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
        };

        function addItemSale_ot(item) {
            _.forEachRight($scope.stockSaleForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    $scope.stockSaleForm.availableItems.push(item);
                    $scope.stockSaleForm.remainingItems.splice(i, 1);
                }
            });
            slectedUnit_saleForm_ot($scope.stockSaleForm.availableItems);
        };

        function addItemSale(item) {
            _.forEachRight($scope.stockSaleForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    $scope.stockSaleForm.availableItems.push(item);
                    $scope.stockSaleForm.remainingItems.splice(i, 1);
                }
            });
            slectedUnit_saleForm($scope.stockSaleForm.availableItems);
        };


        $scope.ot_stockSale = function(openTransaction, indexxx) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, openTransaction._receiver, 2, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber, openTransaction.invoiceNumber);
            $scope.stockSaleForm.showBill = false;
            $scope.stockSaleForm.showOpenBill = true;
            $scope.stockSaleForm.isOpen = true;
            $scope.stockSaleForm.invoiceNumber = openTransaction.invoiceNumber
            if ($scope.resetSerialNumber)
                $scope.stockSaleForm.openBillNumber = "Temp/SE - " + openTransaction.daySerialNumber;
            else
                $scope.stockSaleForm.openBillNumber = "Temp/SE - " + openTransaction._transactionNumber;
            //$scope.stockSaleForm.openBillNumber = "Temp/SS -" + openTransaction._transactionNumber;
            $scope.stockSaleForm.transactionId = openTransaction.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": openTransaction._store._id });
            $scope.stockSaleForm.store = $scope.stores[st_index];
            $scope.bindReceiver_saleForm($scope.stockSaleForm.store);
            var vn_index = _.findIndex($scope.stockSaleForm.availableReceiver, { "_id": openTransaction._receiver._id });
            $scope.stockSaleForm.receiver = $scope.stockSaleForm.availableReceiver[vn_index];

            $scope.bindCategory_saleForm(openTransaction._receiver, $scope.stockSaleForm.store);
            $scope.stockSaleForm.cartage = openTransaction.cartage == "undefined" ? "" : openTransaction.cartage;
            $scope.stockSaleForm.discount = openTransaction.discount == "undefined" ? "" : openTransaction.discount;
            $scope.stockSaleForm.discountType = openTransaction.discountType;

            $scope.stockSaleForm.serviceCharge = openTransaction.serviceCharge == "undefined" ? "" : openTransaction.serviceCharge;
            $scope.stockSaleForm.serviceChargeType = openTransaction.serviceChargeType;
            $scope.stockSaleForm.vat = openTransaction.vat == "undefined" ? "" : openTransaction.vat;
            $scope.stockSaleForm.vatType = openTransaction.vatType;
            $scope.stockSaleForm.payment = openTransaction.payment == "undefined" ? "" : openTransaction.payment;
            $scope.stockSaleForm.heading = openTransaction.heading;

            $scope.stockSaleForm.selectedCategory = [];
            _.forEach(openTransaction._items, function(itm, i) {
                $scope.stockSaleForm.selectedCategory[itm.fromCategory._id] = true;
                addItemSale_ot(itm);
            });
            $scope.stockSaleForm.isNew = true;
            $scope.stockSaleForm.enableSubmit = false;
            $scope.calculateAmt_saleForm();
        };

        $scope.checkStockSaleForOpenTransactionAndSave = function() {
            if ($state.params.id == undefined) {
                var itemToSave = getItemToSave_stockSaleForm();
                if (itemToSave.length > 0) {
                    //process for open transaction
                    // if($scope.stockSaleForm.transactionId!=undefined){
                    //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockSaleForm.transactionId});
                    //     var index=_.findIndex($scope.openTrans_Sale,{"transactionId":$scope.stockSaleForm.transactionId});
                    //     $scope.openTrans_Sale.splice(index,1);
                    //     growl.success('Updating....', {ttl: 3000});
                    // }
                    // else{
                    //     $scope.stockSaleForm.transactionId=guid();
                    // }
                    var storeToSave = formatStore_stockEntryForm($scope.stockSaleForm.store);
                    var itemToSave = getItemToSave_stockSaleForm($scope.stockSaleForm.availableItems);
                    console.log(itemToSave);
                    console.log(storeToSave);
                    var rec = formatReceiver($scope.stockSaleForm.receiver);
                    var storeToSave1 = formatStoreForStockSale(angular.copy(itemToSave), rec, angular.copy(storeToSave));

                    console.log('$scope.stockSaleForm', angular.copy($scope.stockSaleForm));
                    if ($scope.stockSaleForm.transactionId != undefined) {
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = storeToSave1;
                        $scope.transaction.invoiceNumber = $scope.stockSaleForm.invoiceNumber;
                        $scope.transaction._receiver = $scope.stockSaleForm.receiver;
                        $scope.transaction.cartage = $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage;
                        $scope.transaction.discount = $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount;
                        $scope.transaction.discountType = $scope.stockSaleForm.discountType;
                        $scope.transaction.billDate = new Date($scope.stockSaleForm.billDate);
                        $scope.transaction.serviceCharge = $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge;
                        $scope.transaction.serviceChargeType = $scope.stockSaleForm.serviceChargeType;
                        $scope.transaction.vat = $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat;
                        $scope.transaction.vatType = $scope.stockSaleForm.vatType;
                        $scope.transaction.payment = $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment;
                        $scope.transaction.heading = $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name;
                        $scope.transaction.processTransaction($scope.stockSaleForm, 2);
                        $scope.transaction.updateTransaction({}, 'update');
                    } else {
                        $scope.stockSaleForm.transactionId = $scope.transaction._id;
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = storeToSave1;
                        $scope.transaction.invoiceNumber = $scope.stockSaleForm.invoiceNumber;
                        $scope.transaction._receiver = $scope.stockSaleForm.receiver;
                        $scope.transaction.cartage = $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage;
                        $scope.transaction.discount = $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount;
                        $scope.transaction.discountType = $scope.stockSaleForm.discountType;
                        $scope.transaction.billDate = new Date($scope.stockSaleForm.billDate);
                        $scope.transaction.serviceCharge = $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge;
                        $scope.transaction.serviceChargeType = $scope.stockSaleForm.serviceChargeType;
                        $scope.transaction.vat = $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat;
                        $scope.transaction.vatType = $scope.stockSaleForm.vatType;
                        $scope.transaction.payment = $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment;
                        $scope.transaction.heading = $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name;
                        $scope.transaction.processTransaction($scope.stockSaleForm, 2);
                        $scope.transaction.updateTransaction({}, 'insert');
                    }


                    // $rootScope.db.insert('stockTransaction',
                    //     {
                    //         "transactionId":$scope.stockSaleForm.transactionId,
                    //         "transactionType":"2",
                    //         "daySerialNumber":0,
                    //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "user":JSON.stringify(currentUser),
                    //         "store":JSON.stringify(angular.copy(storeToSave)),
                    //         "toStore":"",
                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                    //         "vendor":"",
                    //         "receiver":JSON.stringify($scope.stockSaleForm.receiver),
                    //         "cartage":$scope.stockSaleForm.cartage==undefined?0:$scope.stockSaleForm.cartage,
                    //         "discount":$scope.stockSaleForm.discount==undefined?0:$scope.stockSaleForm.discount,
                    //         "discountType":$scope.stockSaleForm.discountType,
                    //         "serviceCharge":$scope.stockSaleForm.serviceCharge==undefined?0:$scope.stockSaleForm.serviceCharge,
                    //         "serviceChargeType":$scope.stockSaleForm.serviceChargeType,
                    //         "vat":$scope.stockSaleForm.vat==undefined?0:$scope.stockSaleForm.vat,
                    //         "vatType":$scope.stockSaleForm.vatType,
                    //         "payment":$scope.stockSaleForm.payment==undefined?0:$scope.stockSaleForm.payment,
                    //         "heading":$scope.stockSaleForm.heading.Name==""?"":$scope.stockSaleForm.heading.Name,
                    //         "wastageItemType":"",
                    //         "wastageEntryType":"",
                    //         "created": "",
                    //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                    //         "templateName":"",
                    //         "deployment_id": currentUser.deployment_id,
                    //         "tenant_id": currentUser.tenant_id,
                    //         "isSynced" : false,
                    //         "syncedOn" : undefined
                    //     }).then(function (result) {
                    //         growl.success('Saving.... ', {ttl: 3000});
                    //         var ot={
                    //             transactionId:$scope.stockSaleForm.transactionId,
                    //             transactionNumber:$scope.transaction._transactionNumber,
                    //             transactionType:"2",
                    //             user:angular.copy(currentUser),
                    //             store:angular.copy(storeToSave),
                    //                 //"toStore":"",
                    //                 //"category":"",
                    //             //vendor:angular.copy($scope.stockEntryForm.vendor),
                    //             receiver:angular.copy($scope.stockSaleForm.receiver),
                    //             cartage:$scope.stockSaleForm.cartage,
                    //             discount:$scope.stockSaleForm.discount,
                    //             discountType:$scope.stockSaleForm.discountType,
                    //             serviceCharge:$scope.stockSaleForm.serviceCharge,
                    //             serviceChargeType:$scope.stockSaleForm.serviceChargeType,
                    //             vat:$scope.stockSaleForm.vat,
                    //             vatType:$scope.stockSaleForm.vatType,
                    //             payment:$scope.stockSaleForm.payment,
                    //             heading:$scope.stockSaleForm.heading.Name=""?"":$scope.stockSaleForm.heading.Name,
                    //                 //"wastageItemType":"",
                    //                 //"wastageEntryType":"",
                    //             created: null,
                    //             items: itemToSave,// JSON.stringify(this),
                    //             deployment_id: currentUser.deployment_id,
                    //             tenant_id: currentUser.tenant_id
                    //         };
                    //         $scope.openTrans_Sale.push(ot);
                    // });
                }
            }
        };

        function printStockSale(it) {
            // var setting=localStorageService.get('stockSetting');
            // var table="";
            // table+="<table style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'  id='printTable' class='table table-curved'>";
            // table+="<thead><th colspan='2'>Retail Invoice</th></thead>";
            // table+="<tbody>";
            // table+="<tr><td colspan='2' style='text-align:left;'>"+setting.title+"</td></tr>";
            // table+="<tr><td style='text-align: right;'>Sale No:</td><td style='text-align:left;'>"+$scope.stockSaleForm.tempBillNo+"</td></tr>";
            // table+="<tr><td style='text-align: right;'>Sale Date:</td><td style='text-align:left;'>"+ $filter('date')(new Date($scope.stockSaleForm.billDate), 'dd-MMM-yyyy')+"</td></tr>";
            // table+="<tr><td style='text-align: right;'>Tin No:</td><td style='text-align:left;'>"+ setting.vatTin+"</td></tr>";
            // table+="<tr><td colspan='2'>";
            // table+="<table style='width:100%;' border='1' cellspacing='0' cellpadding='0' class='table table-curved'>";
            // table+="<thead><th>Item Name</th><th>Unit</th><th>Rate</th><th>Quantity</th><th>Amount</th></thead>";
            // table+="<tbody>";
            // var tot=0;
            // _.forEach(it, function(item,i){
            //     table+="<tr>";
            //     table+="<td>"+item.itemName+"</td>";
            //     table+="<td>"+item.calculateInUnits[2].unitName+"</td>";
            //     table+="<td>"+parseFloat(item.price).toFixed(2) +"</td>";
            //     table+="<td>"+parseFloat(item.qty).toFixed(3)+"</td>";
            //     table+="<td>"+parseFloat((item.qty)*(item.price)).toFixed(2)+"</td>";
            //     table+="</tr>";
            //     tot=parseFloat(parseFloat(tot)+parseFloat(item.qty)*parseFloat(item.price)).toFixed(2);
            // });

            // table+="</tbody>";
            // table+="</table>";
            // table+="</td></tr>";
            // table+="<tr><td style='text-align: right;'>Total :</td><td style='text-align:left;'>"+parseFloat(tot).toFixed(2) +"</td></tr>";
            // var disc=0;
            // var vat=0;
            // var sc=0;
            // var paid=0;
            // if($scope.stockSaleForm.discount!="" && $scope.stockSaleForm.discount!=undefined){
            //     if($scope.stockSaleForm.discountType=="percent"){
            //         disc=parseFloat(tot*0.01*parseFloat($scope.stockSaleForm.discount)).toFixed(2);
            //     }
            //     else{
            //         disc=parseFloat($scope.stockSaleForm.discount).toFixed(2);
            //     }
            // }

            // var availableAmt=parseFloat(parseFloat(tot)-parseFloat(disc)).toFixed(2);

            // if($scope.stockSaleForm.vat!="" && $scope.stockSaleForm.vat!=undefined){
            //     if($scope.stockSaleForm.vatType=="percent"){
            //         vat=parseFloat(availableAmt*0.01*parseFloat($scope.stockSaleForm.vat)).toFixed(2);
            //     }
            //     else{
            //         vat=parseFloat($scope.stockSaleForm.vat).toFixed(2);
            //     }
            // }


            // table+="<tr><td style='text-align: right;'>(-) Discount :</td><td style='text-align:left;'>"+parseFloat(disc).toFixed(2)+"</td></tr>";
            // table+="<tr><td style='text-align: right;'>Available Amount :</td><td style='text-align:left;'>"+parseFloat(availableAmt).toFixed(2)+"</td></tr>";
            // table+="<tr><td style='text-align: right;'>(+) Vat :</td><td style='text-align:left;'>"+parseFloat(vat).toFixed(2)+"</td></tr>";
            // var cartage=0;
            // if($scope.stockSaleForm.cartage!="" && $scope.stockSaleForm.cartage!=undefined){
            //     cartage=parseFloat($scope.stockSaleForm.cartage).toFixed(2);
            // }

            // if($scope.stockSaleForm.payment!="" && $scope.stockSaleForm.payment!=undefined){
            //     paid=parseFloat($scope.stockSaleForm.payment).toFixed(2);
            // }

            // table+="<tr><td style='text-align: right;'>(+) Cartage</td><td style='text-align:left;'>"+parseFloat(cartage).toFixed(2)+"</td></tr>";
            // if($scope.stockSaleForm.serviceCharge!="" && $scope.stockSaleForm.serviceCharge!=undefined){
            //     if($scope.stockSaleForm.serviceChargeType=="percent"){
            //         sc=parseFloat(availableAmt*0.01*parseFloat($scope.stockSaleForm.serviceCharge)).toFixed(2);
            //     }
            //     else{
            //         sc=parseFloat($scope.stockSaleForm.serviceCharge).toFixed(2);
            //     }
            // }
            // table+="<tr><td style='text-align: right;'>(+) Service Charge :</td><td style='text-align:left;'>"+parseFloat(sc).toFixed(2)+"</td></tr>";
            // table+="<tr><td style='text-align: right;'>(+) Payment :</td><td style='text-align:left;'>"+parseFloat(paid).toFixed(2)+"</td></tr>";
            // var gt=parseFloat(parseFloat(availableAmt)+parseFloat(vat)+parseFloat(cartage)+parseFloat(sc)+parseFloat(paid)).toFixed(2);
            // table+="<tr><td style='text-align: right;'>Grand Total :</td><td style='text-align:left;'>"+ parseFloat(gt).toFixed(2)+"</td></tr>";
            // table+="</tbody></table>";


            var table = document.getElementById('printStockSale').innerHTML;
            var printer = window.open('', '', 'width=600,height=600');
            printer.document.open("text/html");
            printer.document.write(table);
            printer.document.close();
            printer.focus();
            printer.print();
        };

        function printIntermediateEntry(it) {
            var table = document.getElementById('entryPrint_Intermediate').innerHTML;
            var printer = window.open('', '', 'width=600,height=600');
            printer.document.open("text/html");
            printer.document.write(table);
            printer.document.close();
            printer.focus();
            printer.print();
        };
        //--------------------------------------Stock Sale Entry ----------------------------------------
        $scope.stockSaleForm = {
            isNew: false,
            enableSubmit: true,
            showBill: true,
            serviceChargeType: 'percent',
            vatType: 'percent',
            discountType: 'percent',
            billDate: new Date(),
            maxDate: new Date()
        };
        $scope.clearStockSaleTab = function() {
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            console.log("serverDate", serverD)
            console.log("diffMili", diffInMili)
            console.log("diffSec", sec)
            $scope.stockSaleForm = {
                isNew: false,
                enableSubmit: true,
                showBill: true,
                serviceChargeType: 'percent',
                vatType: 'percent',
                discountType: 'percent',
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            };

            $scope.stockSaleForm.headingType = [
                { Name: 'Estimate' }, { Name: 'Delivery Challan' }, { Name: 'Invoice' }
            ];
            $scope.stockSaleForm.heading = { Name: '' };

            var op = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 2, null);
            op.getOpenTransactions(2).then(function(results) {
                console.log(results);
                $scope.openTrans_Sale = results;
                //ot_stockEntry
            });

            op.getTemplates(102).then(function(results) {
                $scope.template_Sale = results;
            });

            if ($state.params != null) {
                $location.replace().path('/stock/stockEntry');
            }

        };
        $scope.resetStockSaleTab = function() {
            if (!$scope.stockSaleForm.isEdit) {
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                console.log("serverDate", serverD)
                console.log("diffMili", diffInMili)
                console.log("diffSec", sec)

                $scope.stockSaleForm = {
                    isNew: true,
                    showBill: true,
                    enableSubmit: true,
                    discountType: 'percent',
                    billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                    maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
                };
                $scope.stockSaleForm.headingType = [
                    { Name: 'Estimate' }, { Name: 'Delivery Challan' }, { Name: 'Invoice' }
                ];
                //$scope.stockSaleForm.tempBillNo="Temp/SS -"+ $scope.transaction._transactionNumber;
                $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 2, null, null);
                $scope.transaction.generateBillNumbers(2).then(function(snumbers) {
                    $scope.transaction._transactionNumber = snumbers.billnumber;
                    $scope.transaction.transactionNumber = snumbers.billnumber;
                    $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                    //$scope.stockSaleForm.tempBillNo = "Temp/SS -" + snumbers.billnumber;
                    if ($scope.resetSerialNumber)
                        $scope.stockSaleForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                    else
                        $scope.stockSaleForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                });
                console.log($scope.stockSaleForm.showBill);
            } else {
                //edit
                $scope.stockSaleForm.isPrint = false;
                $scope.stockSaleForm.isEdit = false;
                $scope.stockSaleForm.isSaved = false;
            }
        }
        $scope.newReset_StockSale = function() {
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            console.log("serverDate", serverD)
            console.log("diffMili", diffInMili)
            console.log("diffSec", sec)
            $scope.stockSaleForm = {
                isNew: true,
                enableSubmit: true,
                showBill: true,
                discountType: 'percent',
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            };
            $scope.stockSaleForm.headingType = [
                { Name: 'Estimate' }, { Name: 'Delivery Challan' }, { Name: 'Invoice' }
            ];
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 2, null, null);
            $scope.transaction.generateBillNumbers(2).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.stockSaleForm.tempBillNo = "Temp/SS -" + snumbers.daySerialNumber;
                else
                    $scope.stockSaleForm.tempBillNo = "Temp/SS -" + snumbers.billnumber;
            });
            //console.log($scope.stockSaleForm.showBill);
        };

        $scope.stockSaleForm.headingType = [
            { Name: 'Estimate' }, { Name: 'Delivery Challan' }, { Name: 'Invoice' }
        ];
        $scope.tempBill_saleForm = function(billDate) {
            $scope.stockSaleForm.showBill = true;
            $scope.stockSaleForm.showOpenBill = false;
            $scope.stockSaleForm.tempBillNo = "Temp/SS -" + $scope.transaction._transactionNumber
            var serverDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate($rootScope.serverTime.serverTime))));
            var selectedDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(billDate))));
            console.log("selected date", selectedDate)
            console.log("server date", serverDate)
            var d = serverDate.getTime() - selectedDate.getTime()
            var di = new Date(d)
            var hours = Math.abs(serverDate - selectedDate) / 36e5;
            console.log("ho=ur", hours)
            if (hours >= 72) {
                growl.success('Back Date Entry has been restricted to 48 Hours.', { ttl: 3000 });
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                console.log("serverDate", serverD)
                console.log("diffMili", diffInMili)
                console.log("diffSec", sec)
                $scope.stockSaleForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            }
        };

        $scope.addRemainingItem_stockSaleForm = function($item, $model, $label) {
            var remaining = angular.copy($scope.stockSaleForm.remainingItems);
            console.log('remaining.length before', remaining.length);
            _.forEach($scope.stockSaleForm.remainingItems, function(itm, i) {
                if (itm._id == $item._id) {
                    console.log('1', itm._id);
                    $scope.stockSaleForm.availableItems.unshift(itm);
                    remaining.splice(i, 1);
                    console.log('remaining.length after', remaining.length);
                    $scope.stockSaleForm.itemToAdd = "";
                    $scope.stockSaleForm.selectedCategory[itm.fromCategory._id] = true;
                }
            });
            $scope.stockSaleForm.remainingItems = remaining;
            slectedUnit_saleForm($scope.stockSaleForm.availableItems);
        };

        $scope.bindReceiver_saleForm = function(store) {
            $scope.stockSaleForm.availableCategory = [];
            $scope.stockSaleForm.availableItems = [];
            $scope.stockSaleForm.availableReceiver = [];
            $scope.stockSaleForm.availableTemplate = [];
            $scope.stockSaleForm.receiver = {};
            $scope.stockSaleForm.remainingItems = [];
            var receivers = angular.copy($scope.receivers);
            if (store != null) {
                //_.forEach(receivers, function(r,i){
                //if(r.pricing!=undefined){
                //_.forEach(r.pricing.store, function(s,ii){
                //if(s._id==store._id){
                //$scope.stockSaleForm.availableReceiver.push(r);
                $scope.stockSaleForm.availableReceiver = receivers;
                //}
                //});
                //}
                //});
            }
        };
        $scope.bindCategory_saleForm = function(receiver, store) {
            $scope.stockSaleForm.availableItems = [];
            $scope.stockSaleForm.availableCategory = [];
            $scope.stockSaleForm.selectedCategory = {};
            $scope.stockSaleForm.remainingItems = [];
            $scope.stockSaleForm.availableTemplate = [];
            $scope.stockSaleForm.selectedTemplate = {};
            var Store = angular.copy(store);


            var items = angular.copy($scope.menuRecipes);
            var citems = [];
            _.forEach(Store.processedFoodCategory, function(c, i) {
                _.forEach(c.item, function(itm, iipp) {
                    var obj = { _id: itm._id, itemName: itm.itemName };
                    citems.push(obj);
                });
            });
            _.forEachRight(items, function(ii, i) {
                var ind = _.findIndex(citems, { _id: ii.itemId });
                if (ind >= 0) {
                    ii.itemName = ii.recipeName;
                    ii.preferedUnit = ii.selectedUnitId._id;
                    if (ii.units == undefined) {
                        ii.units = [{ _id: "9980", unitName: "Pack" }];
                    }
                } else {
                    items.splice(i, 1);
                }
            });

            var processedCat = { _id: "PosistTech111111", categoryName: "Processed Food" };
            processedCat.item = items;

            /*if(allowBaseKitchenItemWastage && store.isKitchen){

              var bItems = [];
              var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
              _.forEach($scope.baseKitchenItems, function (bItem) {
                var item = {
                  itemName: bItem.itemName,
                  _id: bItem.itemId,
                  itemId: bItem.itemId,
                  itemType: "Base Kitchen Item",
                  fromCategory: angular.copy(baseKitchenItemsCat),
                  units: bItem.units,
                  preferedUnit: bItem.preferedUnit
                }
                bItems.push(item);
                $scope.stockSaleForm.remainingItems.push(angular.copy(item));
              });
              baseKitchenItemsCat.item = bItems;
              $scope.stockSaleForm.availableCategory.push(angular.copy(baseKitchenItemsCat));
            }*/

            var ir = angular.copy($scope.intermediateRecipes);
            _.forEach(ir, function(ii, i) {
                ii.itemName = ii.recipeName;
                ii.preferedUnit = ii.selectedUnitId._id;
                if (ii.units == undefined) {
                    ii.units = [{ _id: "9980AZ", unitName: "Pack" }];
                }
                8
            });
            var processedSemi_Cat = { _id: "PosistTech222222", categoryName: "InterMediate Food" };
            processedSemi_Cat.item = ir;

            if (receiver != null) {
                if (store.isKitchen == true) {
                    //if(currentDeployment.isBaseKitchen)
                    $scope.stockSaleForm.availableCategory.push(angular.copy(processedCat));
                }

                $scope.stockSaleForm.availableCategory.push(angular.copy(processedSemi_Cat));


                _.forEach(Store.category, function(c, i) {
                    $scope.stockSaleForm.availableCategory.push(angular.copy(c));
                });
                _.forEach($scope.template_Sale, function(t, i) {
                    if (store._id == t._store._id) {
                        _.forEach(t._receiver, function(r, ii) {
                            if (receiver._id == r._id) {
                                $scope.stockSaleForm.availableTemplate.push(angular.copy(t));
                            }
                        });
                    }
                });

                var itemsforcompare = [];
                if (receiver.pricing != undefined) {
                    _.forEach(receiver.pricing.store, function(s, i) {
                        if (s._id == store._id) {
                            _.forEach(s.category, function(c, ii) {
                                _.forEach(c.item, function(itm, iii) {
                                    if (itm.itemCode == undefined) {
                                        //console.log('setting itemcode of '+it.itemName+'to nothing. 2090');
                                        itm["itemCode"] = '';
                                    }
                                    itemsforcompare.push(angular.copy(itm));
                                });
                            });
                        }
                    });
                }

                _.forEach($scope.stockSaleForm.availableCategory, function(ac, i) {
                    _.forEachRight(ac.item, function(itm, ii) {
                        var code = '';
                        if (itm.itemCode) {
                            code = itm.itemCode;
                            //console.log('-----------------------------item code of '+itm.itemName+' is :-',itm.itemCode);
                        } else {
                            //console.log('setting itemcode of '+itm.itemName+'to nothing.');
                            code = '';
                        }
                        itm.itemCode = code;
                        var ind = _.findIndex(itemsforcompare, { "_id": itm._id });
                        var varFromCategory = { "_id": ac._id, "categoryName": ac.categoryName };
                        itm["fromCategory"] = varFromCategory;

                        if (ac.categoryName == "Processed Food") {
                            itm.itemType = "MenuItem";
                            var index = _.findIndex(itemsforcompare, { "_id": itm.itemId });
                            if (index >= 0) {
                                itm.price = angular.copy(itemsforcompare[index].price);
                                itm.selectedUnitId = angular.copy(itemsforcompare[index].selectedUnitId);
                            }
                        } else if (ac.categoryName == "InterMediate Food") {
                            itm.itemType = "IntermediateItem";
                            var index = _.findIndex(itemsforcompare, { "_id": itm.itemId });
                            if (index >= 0) {
                                itm.price = angular.copy(itemsforcompare[index].price);
                                itm.selectedUnitId = angular.copy(itemsforcompare[index].selectedUnitId);
                            }
                        } else {
                            itm.itemType = "RawMaterial";
                        }
                        if (ind < 0) {
                            $scope.stockSaleForm.remainingItems.push(angular.copy(itm));
                        } else {
                            if (!itemsforcompare[ind].itemCode) {
                                itemsforcompare[ind].itemCode = code;
                            }
                            // if(itm.itemType!="RawMaterial"){
                            //     itm.price=itemsforcompare[ind].price;
                            //     //ac.item.splice(ii,1);
                            //     // itemsforcompare[ind].units[0].baseUnit=itm.units[0].baseUnit;
                            //     // ac.item.push(angular.copy(itemsforcompare[ind]));
                            //     $scope.stockSaleForm.remainingItems.push(angular.copy(itm));
                            // }
                            // else{
                            itemsforcompare[ind].itemType = itm.itemType;
                            ac.item.splice(ii, 1);
                            itemsforcompare[ind].units[0].baseUnit = itm.units[0].baseUnit;
                            ac.item.push(angular.copy(itemsforcompare[ind]));
                            itemsforcompare[ind].fromCategory = varFromCategory;
                            $scope.stockSaleForm.remainingItems.push(angular.copy(itemsforcompare[ind]));
                            //}
                        }
                    });
                });
            }
        };


        $scope.bindItems_saleFormTemplate = function(templateId) {

            if ($scope.stockSaleForm.availableItems == undefined) {
                $scope.stockSaleForm.availableItems = [];
            }

            _.forEach($scope.stockSaleForm.availableTemplate, function(t, i) {
                if (t._id == templateId) {
                    _.forEach(t._items, function(itm, ii) {
                        var av = _.findIndex($scope.stockSaleForm.availableItems, { "_id": itm._id });
                        if (av < 0) {
                            $scope.stockSaleForm.availableItems.push(angular.copy(itm));
                            var index = _.findIndex($scope.stockSaleForm.remainingItems, { "_id": itm._id });
                            $scope.stockSaleForm.remainingItems.splice(index, 1);
                            var cS = itm.fromCategory._id;
                            $scope.stockSaleForm.selectedCategory[cS] = true;
                        } else {
                            $scope.stockSaleForm.availableItems.splice(av, 1);
                            $scope.stockSaleForm.availableItems.push(angular.copy(itm));
                        }
                    });
                }
            });
            slectedUnit_saleForm($scope.stockSaleForm.availableItems);
        };

        $scope.bindItems_saleForm = function(catId) {
            if ($scope.stockSaleForm.availableItems == undefined) {
                $scope.stockSaleForm.availableItems = [];
            }
            var aCat = $scope.stockSaleForm.availableCategory;
            _.forEach(aCat, function(c, i) {
                if ($scope.stockSaleForm.selectedCategory[catId] == true) {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var av = _.findIndex($scope.stockSaleForm.availableItems, { _id: itm._id });
                            if (av < 0) {

                                var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                itm["fromCategory"] = varFromCategory;

                                if (c.categoryName == "Processed Food") {
                                    itm.itemType = "MenuItem";
                                } else if (c.categoryName == "InterMediate Food") {
                                    itm.itemType = "IntermediateItem";
                                } else {
                                    itm.itemType = "RawMaterial";
                                }
                                $scope.stockSaleForm.availableItems.push(angular.copy(itm));
                                var index = _.findIndex($scope.stockSaleForm.remainingItems, { _id: itm._id });
                                $scope.stockSaleForm.remainingItems.splice(index, 1);
                            }

                            //$scope.stockSaleForm.availableItems.push(itm);
                        });
                    }
                } else {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var index = _.findIndex($scope.stockSaleForm.availableItems, { _id: itm._id });
                            if (index >= 0) {
                                $scope.stockSaleForm.availableItems.splice(index, 1);
                                $scope.stockSaleForm.remainingItems.push(angular.copy(itm));
                            }
                        });
                    }
                }
            });
            slectedUnit_saleForm($scope.stockSaleForm.availableItems);
        };

        function slectedUnit_saleForm_ot(item) {
            $scope.stockSaleForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId == u._id) {
                            $scope.stockSaleForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                            itm.selectedUnit = u;
                        }
                    } else if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.stockSaleForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                        }
                    }
                    else{
                        if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.stockSaleForm.selectedUnits[itm._id]=u;
                                itm.selectedUnit=u;
                            }
                        }
                    }
                });
            });
        };

        function slectedUnit_saleForm(item) {
            $scope.stockSaleForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.stockSaleForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                            itm.selectedUnit = u;
                        }
                    } else if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId._id == u._id) {
                            $scope.stockSaleForm.selectedUnits[itm._id] = u;
                            itm.newConFactor = parseFloat(u.conversionFactor);
                        }
                    }
                    else{
                        if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.stockSaleForm.selectedUnits[itm._id]=u;
                                itm.selectedUnit=u;
                            }
                        }
                    }
                });
            });
        };

        $scope.goNextSale = function(nextIdx) {
            var f = angular.element(document.querySelector('#fs_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                // $scope.stockSaleForm.enableSubmit=false;
                // angular.element(document.querySelector('#submitSale')).focus();
            }
        };
        
        $scope.calculateAmt_saleForm = function (qty, price, item) {
            var gTotal = 0;
            var total = 0;
            var vatAmt = 0;
            var sChAmt = 0;
            _.forEach($scope.stockSaleForm.availableItems, function (item){
                //console.log(item.itemName, isNaN(item.qty), isNaN(item.price));
                if (item.qty && !isNaN(item.qty)) {
                if (item.price && !isNaN(item.price)) {
                    var amt = parseFloat(item.qty) * parseFloat(item.price);
                    item.Total = amt;
                    console.log('itemTotal', item.Total);
                    total += item.Total;

                    $scope.stockSaleForm.isOpen = true;
                    var ss = $scope.transaction.basePrefferedAnsSelectedUnitsByItem(item, $scope.stockSaleForm.selectedUnits, 2);
                    item.calculateInUnits = ss;
                    console.log(ss);
                    //calculateAmtinPrefferedAndBaseUnit_SaleForm(qty,price,item);
                    $scope.enableSubmitButton_Sale($scope.stockSaleForm.availableItems);
                }
                else {
                    var index1 = _.findIndex($scope.stockSaleForm.availableItems, {_id: item._id});
                    $scope.stockSaleForm.availableItems[index1].price = "";
                }

                }
                else {
                var index = _.findIndex($scope.stockSaleForm.availableItems, {_id: item._id});
                $scope.stockSaleForm.availableItems[index].qty = "";
                }
            });

            console.log('-----------------------------total----------------------', total, vatAmt, sChAmt);

            if(!isNaN($scope.stockSaleForm.discount)){
                if($scope.stockSaleForm.discountType == "percent") 
                total -= total * $scope.stockSaleForm.discount / 100;
                else
                total -= Number($scope.stockSaleForm.discount);
            }
            if(!isNaN($scope.stockSaleForm.vat)){
                if($scope.stockSaleForm.vatType == "percent")
                vatAmt = total * $scope.stockSaleForm.vat / 100;
                else
                vatAmt = Number($scope.stockSaleForm.vat);
            }
            if(!isNaN($scope.stockSaleForm.serviceCharge)){
                if($scope.stockSaleForm.serviceChargeType == "percent")
                sChAmt = total * $scope.stockSaleForm.serviceCharge / 100;
                else
                sChAmt =  Number($scope.stockSaleForm.serviceCharge);
            }

            console.log('-----------------------------total----------------------', total, vatAmt, sChAmt);

            gTotal = total + vatAmt + sChAmt;
            if(!isNaN($scope.stockSaleForm.cartage)) 
                gTotal += Number($scope.stockSaleForm.cartage);
            if(!isNaN($scope.stockSaleForm.payment)) 
                gTotal += Number($scope.stockSaleForm.payment);
            $scope.stockSaleForm.totalAmount = Number(gTotal);
            };

        function calculateAmtinPrefferedAndBaseUnit_SaleForm(qty, price, item) {
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            _.forEach($scope.stockSaleForm.selectedUnits, function(u, ii) {
                if (ii == item._id) {
                    selectedConversionFactor = u.conversionFactor;
                }
            });
            var baseUnit = {};
            if (item.units[0].baseUnit != undefined) {
                baseUnit = item.units[0].baseUnit;
                baseUnit.unitName = baseUnit.name;
            } else {
                var ind = _.findIndex(item.units, { "type": "baseUnit" });
                baseUnit = item.units[ind];
                baseUnit.unitName = baseUnit.name;
            }


            var baseAmt = parseFloat(qty * selectedConversionFactor) * parseFloat(price);
            baseUnit.baseQty = parseFloat(qty * selectedConversionFactor);
            baseUnit.totalAmount = baseAmt;
            baseUnit.subTotal = baseAmt;
            baseUnit.addedAmt = 0;
            baseUnit.conversionFactor = conversionFactor;
            baseUnit.type = "baseUnit";
            item.baseUnit = baseUnit;

            var pInd = _.findIndex(item.units, { _id: item.preferedUnit });
            if (pInd >= 0) {
                var preferedUnits = angular.copy(item.units[pInd]);
                var preferedconversionFactor = item.units[pInd].conversionFactor;
                if (preferedconversionFactor == conversionFactor) {
                    preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
                    preferedUnits.subTotal = parseFloat(baseUnit.subTotal);
                } else if (preferedconversionFactor > conversionFactor) {
                    preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                    preferedUnits.subTotal = parseFloat(baseUnit.subTotal) / parseFloat(preferedconversionFactor);
                } else if (preferedconversionFactor < conversionFactor) {
                    preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                    preferedUnits.subTotal = parseFloat(baseUnit.subTotal) * parseFloat(preferedconversionFactor);
                }

                preferedUnits.addedAmt = 0;
                preferedUnits.totalAmount = preferedUnits.subTotal;
                preferedUnits.type = "preferedUnit";
                item.preferedUnits = preferedUnits;
            }
        };

        function getFromCategoryOfItem_stockSaleForm() {
            _.forEach($scope.stockSaleForm.selectedCategory, function(sc, i) {
                //console.log(sc +" =sc , i= "+i);
                if (sc == true) {
                    _.forEach($scope.stockSaleForm.availableCategory, function(ac, ii) {
                        if (ac._id == i) {
                            _.forEach(ac.item, function(itm, iii) {
                                if (ac.categoryName == "Processed Food") {
                                    itm.itemType = "MenuItem";
                                } else if (ac.categoryName == "InterMediate Food") {
                                    itm.itemType = "IntermediateItem";
                                } else {
                                    itm.itemType = "RawMaterial";
                                }
                                var varFromCategory = { "_id": ac._id, "categoryName": ac.categoryName };
                                itm["fromCategory"] = varFromCategory;
                            });
                        }
                    });
                }
            });
        };

        function getItemToSave_stockSaleForm() {
            var items = angular.copy($scope.stockSaleForm.availableItems);
            _.forEachRight(items, function(itm, i) {
                var flag = false;
                _.forEach($scope.stockSaleForm.selectedUnits, function(u, ii) {
                    if (ii == itm._id && itm.qty != "" && itm.qty != undefined && itm.price != "" && itm.price != undefined && u != null && $scope.stockSaleForm.selectedUnits[itm._id] != undefined) {
                        // var selectedUnit={"_id":u._id,"unitName":u.unitName,"baseQty":itm.qty,"subTotal":(itm.qty)*(itm.price),"vatAmt":0,"totalAmount":(itm.qty)*(itm.price),"type":"selectedUnit","conversionFactor":u.conversionFactor};
                        // itm["selectedUnit"]=selectedUnit;
                        // if(itm.baseUnit!=undefined){
                        //     itm.calculateInUnits=[];
                        //     itm.calculateInUnits.push(itm.baseUnit);
                        //     if(itm.preferedUnits!=undefined){
                        //         itm.calculateInUnits.push(itm.preferedUnits);
                        //     }
                        //     else
                        //     {
                        //         var preUnit={"_id":u._id,"unitName":u.unitName,"baseQty":itm.qty,"subTotal":(itm.qty)*(itm.price),"vatAmt":0,"totalAmount":(itm.qty)*(itm.price),"type":"preferedUnit","conversionFactor":u.conversionFactor};
                        //         itm["preferedUnit"]=preUnit;
                        //         itm.calculateInUnits.push(itm.preferedUnit);
                        //         itm.preferedUnit=u._id;
                        //     }
                        //     itm.calculateInUnits.push(itm.selectedUnit);
                        // }
                        if (itm.itemType != "RawMaterial") {
                            // var ss=$scope.transaction.calculateForProcessedAndSemiProcessed_sale(itm);
                            // itm=ss;
                            var rd = $scope.transaction.calculateForRecipeDetails(itm);
                            itm = rd;
                        }
                        flag = true;
                    }
                });
                if (flag == false) {
                    items.splice(i, 1);
                }
            });
            _.forEach(items, function(itm, i) {
                for (var p in itm) {
                    if (p != "_id" && p != "itemName" && p != "qty" && p != "rawItems" && p != "calculateInUnits" && p != "menuQty" && p != "recipeQty" && p != "receipeDetails" && p != "isSemiProcessed" && p != "quantity" && p != "preferedUnit" && p != "itemType" && p != "price" && p != "Total" && p != "units" &&
                        p != "fromCategory" && p != "selectedUnit" && p != "comment" && p != "itemCode") {
                        //&& p!="selectedUnit" && p!="preferedUnits" && p!="baseUnit"
                        _.forEach(itm.units, function(u, ii) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
            });
            return items;
        };

        $scope.deleteSelectedItemSaleForm = function(item) {
            var index = _.findIndex($scope.stockSaleForm.availableItems, { "itemName": item.itemName });
            $scope.stockSaleForm.availableItems.splice(index, 1);
            $scope.stockSaleForm.remainingItems.push(item);
            $scope.enableSubmitButton_Sale($scope.stockSaleForm.availableItems);
        };

        function formatStoreForStockSale(item, receiver, store) {
            var stores = store;
            var rec = receiver;
            delete rec.pricing;
            stores.receiver = rec;
            stores.receiver.category = [];
            _.forEach(item, function(itm, i) {
                var index = _.findIndex(stores.receiver.category, { _id: itm.fromCategory._id });
                if (index < 0) {
                    stores.receiver.category.push(itm.fromCategory);
                }
            });

            _.forEach(stores.receiver.category, function(c, i) {
                c.items = [];
                _.forEachRight(item, function(itm, ii) {
                    if (c._id == itm.fromCategory._id) {
                        delete itm.fromCategory;
                        c.items.push(itm);
                        item.splice(ii, 1);
                    }
                });
            });
            console.log(stores);
            return stores;
        };

        function formatReceiver(receiver) {
            for (var p in receiver) {
                if (p != "_id" && p != "receiverName" && p != "contactNumber" && p != "address" && p != "deployment_id" && p != "tenant_id") {
                    delete receiver[p];
                }
            }
            return receiver;
        };

        function calculateTotal_Sale(items) {
            var tot = 0;
            var discount = 0;
            var AvailableAmount = 0;
            var vat = 0;
            var serviceCharge = 0;
            var payment = 0;
            var grandTotal = 0;
            var cartage = 0;
            _.forEach(items, function(itm, i) {
                tot = parseFloat(parseFloat(tot) + parseFloat(itm.Total));
            });
            if ($scope.stockSaleForm.discount != undefined) {
                if ($scope.stockSaleForm.discountType == "percent") {
                    discount = parseFloat(tot * 0.01 * parseFloat($scope.stockSaleForm.discount));
                } else {
                    discount = parseFloat($scope.stockSaleForm.discount);
                }
            }
            AvailableAmount = parseFloat(tot) - parseFloat(discount);
            if ($scope.stockSaleForm.vat != undefined) {
                if ($scope.stockSaleForm.vatType == "percent") {
                    vat = parseFloat(AvailableAmount * 0.01 * parseFloat($scope.stockSaleForm.vat));
                } else {
                    vat = parseFloat($scope.stockSaleForm.vat);
                }
            }
            if ($scope.stockSaleForm.serviceCharge != undefined) {
                if ($scope.stockSaleForm.serviceChargeType == "percent") {
                    serviceCharge = parseFloat(AvailableAmount * 0.01 * parseFloat($scope.stockSaleForm.serviceCharge));
                } else {
                    serviceCharge = parseFloat($scope.stockSaleForm.serviceCharge);
                }
            }
            if ($scope.stockSaleForm.cartage != undefined) {
                cartage = parseFloat($scope.stockSaleForm.cartage);
            }
            if ($scope.stockSaleForm.payment != undefined) {
                payment = parseFloat($scope.stockSaleForm.payment);
            }

            grandTotal = parseFloat(tot - discount + vat + serviceCharge + cartage + payment).toFixed(2);
            $scope.stockSaleForm.pTotal = parseFloat(tot).toFixed(2);
            $scope.stockSaleForm.pDiscount = parseFloat(discount).toFixed(2);
            $scope.stockSaleForm.pAvailableAmount = parseFloat(AvailableAmount).toFixed(2);
            $scope.stockSaleForm.pVat = parseFloat(vat).toFixed(2);
            $scope.stockSaleForm.pCartage = parseFloat(cartage).toFixed(2);
            $scope.stockSaleForm.pServiceCharge = parseFloat(serviceCharge).toFixed(2);
            $scope.stockSaleForm.pPayment = parseFloat(payment).toFixed(2);
            $scope.stockSaleForm.pGrandTotal = parseFloat(grandTotal).toFixed(2);

        };

        // old Stock sale before INvoice Number
        /*$scope.submitStockSaleEntry = function (formStockSaleEntry) {

          $scope.stockSaleForm.billNoNew = "SS - " + $scope.transaction.daySerialNumber;
          if ($state.params.id == undefined) {
            $scope.checkStockSaleForOpenTransactionAndSave();
            $scope.stockSaleForm.enableSubmit = true;
            var storeToSave = formatStore_stockEntryForm($scope.stockSaleForm.store);
            var itemToSave = getItemToSave_stockSaleForm($scope.stockSaleForm.availableItems);
            console.log(itemToSave);
            console.log(storeToSave);
            var rec = formatReceiver($scope.stockSaleForm.receiver);
            var storeToSave1 = formatStoreForStockSale(angular.copy(itemToSave), rec, angular.copy(storeToSave));

            if (formStockSaleEntry.$valid && !$scope.stockSaleForm.isPrint) {
              if (itemToSave.length < $scope.stockSaleForm.availableItems.length) {
                var isGood = confirm("Some Items are Escaped from Sale ! Are You Confirm?");
                if (isGood == true) {
                  $scope.transaction._items = itemToSave;
                  $scope.transaction._store = storeToSave1;
                  $scope.transaction.isOpen = false;
                  $scope.transaction.cartage = $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage;
                  $scope.transaction.discount = $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount;
                  $scope.transaction.discountType = $scope.stockSaleForm.discountType == "" ? "" : $scope.stockSaleForm.discountType;
                  $scope.transaction.serviceCharge = $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge;
                  $scope.transaction.serviceChargeType = $scope.stockSaleForm.serviceChargeType == "" ? "" : $scope.stockSaleForm.serviceChargeType;
                  $scope.transaction.vat = $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat;
                  $scope.transaction.vatType = $scope.stockSaleForm.vatType == "" ? "" : $scope.stockSaleForm.vatType;
                  $scope.transaction.payment = $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment;
                  $scope.transaction.heading = $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name;
                  $scope.transaction.processTransaction($scope.stockSaleForm, 2);
                  $scope.transaction.updateTransaction({}, 'update').then(function (results) {
                    if (results != null) {
                      manageClosingQty(itemToSave, storeToSave1._id, '', 2);
                      $scope.stockSaleForm.isPrint = true;
                      $scope.stockSaleForm.isEdit = true;
                      $scope.stockSaleForm.isSaved = true;
                      $scope.stockSaleForm.printItems = itemToSave;
                      calculateTotal_Sale(itemToSave);
                      growl.success('Success in stock Sale', {ttl: 3000});
                      growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                      $scope.stockSaleForm.enableSubmit = false;
                    }
                  });
                }
                else {
                  $scope.stockSaleForm.enableSubmit = false;
                }
              }
              else {
                $scope.transaction._items = itemToSave;
                $scope.transaction._store = storeToSave1;
                $scope.transaction.isOpen = false;
                $scope.transaction.cartage = $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage;
                $scope.transaction.discount = $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount;
                $scope.transaction.discountType = $scope.stockSaleForm.discountType == "" ? "" : $scope.stockSaleForm.discountType;
                $scope.transaction.serviceCharge = $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge;
                $scope.transaction.serviceChargeType = $scope.stockSaleForm.serviceChargeType == "" ? "" : $scope.stockSaleForm.serviceChargeType;
                $scope.transaction.vat = $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat;
                $scope.transaction.vatType = $scope.stockSaleForm.vatType == "" ? "" : $scope.stockSaleForm.vatType;
                $scope.transaction.payment = $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment;
                $scope.transaction.heading = $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name;
                $scope.transaction.processTransaction($scope.stockSaleForm, 2);
                $scope.transaction.updateTransaction({}, 'update').then(function (results) {
                  if (results != null) {
                    manageClosingQty(itemToSave, storeToSave1._id, '', 2);
                    $scope.stockSaleForm.isPrint = true;
                    $scope.stockSaleForm.isEdit = true;
                    $scope.stockSaleForm.isSaved = true;
                    $scope.stockSaleForm.printItems = itemToSave;
                    calculateTotal_Sale(itemToSave);
                    growl.success('Success in stock Sale', {ttl: 3000});
                    growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                    $scope.stockSaleForm.enableSubmit = false;
                  }
                });
              }

              // $rootScope.db.insert('stockTransaction',
              //     {
              //         "transactionId":$scope.stockSaleForm.transactionId,
              //         "transactionType":"2",
              //         "daySerialNumber":0,
              //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
              //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
              //         "user":JSON.stringify(currentUser),
              //         "store":JSON.stringify(angular.copy(storeToSave1)),
              //         "toStore":"",
              //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
              //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
              //         "receiver":JSON.stringify($scope.stockSaleForm.receiver),
              //         "cartage":$scope.stockSaleForm.cartage==undefined?0:$scope.stockSaleForm.cartage,
              //         "discount":$scope.stockSaleForm.discount==undefined?0:$scope.stockSaleForm.discount,
              //         "discountType":$scope.stockSaleForm.discountType==""?"":$scope.stockSaleForm.discountType,
              //         "serviceCharge":$scope.stockSaleForm.serviceCharge==undefined?0:$scope.stockSaleForm.serviceCharge,
              //         "serviceChargeType":$scope.stockSaleForm.serviceChargeType==""?"":$scope.stockSaleForm.serviceChargeType,
              //         "vat":$scope.stockSaleForm.vat==undefined?0:$scope.stockSaleForm.vat,
              //         "vatType":$scope.stockSaleForm.vatType==""?"":$scope.stockSaleForm.vatType,
              //         "payment":$scope.stockSaleForm.payment==undefined?0:$scope.stockSaleForm.payment,
              //         "heading":$scope.stockSaleForm.heading==undefined?"":$scope.stockSaleForm.heading.Name,
              //         "wastageItemType":"",
              //         "wastageEntryType":"",
              //         "created": new Date($scope.stockSaleForm.billDate).toISOString(),
              //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
              //         "templateName":"",
              //         "deployment_id": currentUser.deployment_id,
              //         "tenant_id": currentUser.tenant_id,
              //         "isSynced" : false,
              //         "syncedOn" : undefined
              //     }).then(function (result) {
              //         growl.success('Success in stock Sale', {ttl: 3000});
              //         growl.success(itemToSave.length+'Items added', {ttl: 3000});
              //         if(result.transactionId!=undefined){
              //                 //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockSaleForm.transactionId});
              //                 var index=_.findIndex($scope.openTrans_Sale,{"transactionId":result.transactionId});
              //                 $scope.openTrans_Sale.splice(index,1);
              //         }
              //         //$scope.stockSaleForm={enableSubmit:true,discountType:'percent'};
              //         //$scope.stockSaleForm.availableItems=[];
              //         $scope.stockSaleForm.isPrint=true;
              //         $scope.stockSaleForm.isEdit=true;
              //         $scope.stockSaleForm.isSaved=true;
              // });
            }
            else {
              //print.......
              printStockSale(itemToSave);
              $scope.stockSaleForm.enableSubmit = false;
            }
          }
          else {
            $scope.stockSaleForm.enableSubmit = true;
            var itemToSave = getItemToSave_stockSaleForm($scope.stockSaleForm.availableItems);
            if ($scope.stockSaleForm.store != undefined && $scope.stockSaleForm.receiver != undefined && itemToSave.length > 0) {

              getItemsHistory_StockEntry($scope.stockSaleForm.availableItems);
              $scope.stockEditHitory.editHistoryId = guid();
              $scope.stockEditHitory.transactionId = $scope.stockSaleForm.transactionId;
              $scope.stockEditHitory.mainTransactionId = $scope.stockSaleForm.mainTransactionId;
              $scope.stockEditHitory.billNo = $scope.transaction._transactionNumber;
              $scope.stockEditHitory.user = formatUser(currentUser);
              $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
              $scope.stockEditHitory.deployment_id = currentUser.deployment_id;

              EditTransactionHistory.saveData({}, $scope.stockEditHitory, function (result) {
                growl.success('New history created', {ttl: 3000});
                if ($state.params != null) {
                  $location.replace().path('/stock/stockEntry');
                }
              });

              var storeToSave = formatStore_stockEntryForm($scope.stockSaleForm.store);
              var rec = formatReceiver($scope.stockSaleForm.receiver);
              var str = formatStoreForStockSale(angular.copy(itemToSave), rec, angular.copy(storeToSave));
              var toSave = {
              _store: str,_receiver:rec, _id: $scope.stockSaleForm.mainTransactionId,
                "cartage": $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage,
                "discount": $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount,
                "discountType": $scope.stockSaleForm.discountType == "" ? "" : $scope.stockSaleForm.discountType,
                "serviceCharge": $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge,
                "serviceChargeType": $scope.stockSaleForm.serviceChargeType == "" ? "" : $scope.stockSaleForm.serviceChargeType,
                "vat": $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat,
                "vatType": $scope.stockSaleForm.vatType == "" ? "" : $scope.stockSaleForm.vatType,
                "payment": $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment,
                "heading": $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name
              };
              StockResource.update({}, toSave, function (bill) {
                console.log(bill);
              });
            }
            else {
              $scope.stockSaleForm.enableSubmit = false;
            }
          }
        };*/

        $scope.submitStockSaleEntry = function(formStockSaleEntry) {
            $scope.stockSaleForm.billNoNew = "SS - " + $scope.transaction.daySerialNumber;
            if ($state.params.id == undefined) {
                $scope.checkStockSaleForOpenTransactionAndSave();
                $scope.stockSaleForm.enableSubmit = true;
                var storeToSave = formatStore_stockEntryForm($scope.stockSaleForm.store);
                var itemToSave = getItemToSave_stockSaleForm($scope.stockSaleForm.availableItems);
                console.log(itemToSave);
                console.log(storeToSave);
                var rec = formatReceiver($scope.stockSaleForm.receiver);
                var storeToSave1 = formatStoreForStockSale(angular.copy(itemToSave), rec, angular.copy(storeToSave));

                if (formStockSaleEntry.$valid && !$scope.stockSaleForm.isPrint) {
                    if (itemToSave.length < $scope.stockSaleForm.availableItems.length) {
                        var isGood = confirm("Some Items are Escaped from Sale ! Are You Confirm?");
                        if (isGood == true) {
                            if (!$scope.stockSaleForm.invoiceNumber)
                                var isInvoiceNumber = confirm('Invoice Number skipped from entry ! Still you want to submit ? ')
                            else
                                isInvoiceNumber = true

                            if (isInvoiceNumber == true) {

                                $scope.transaction._items = itemToSave;
                                $scope.transaction._store = storeToSave1;
                                $scope.transaction.isOpen = false;
                                $scope.transaction.invoiceNumber = $scope.stockSaleForm.invoiceNumber;
                                $scope.transaction.charges = $scope.stockSaleForm.charges;
                                $scope.transaction.cartage = $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage;
                                $scope.transaction.discount = $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount;
                                $scope.transaction.discountType = $scope.stockSaleForm.discountType == "" ? "" : $scope.stockSaleForm.discountType;
                                $scope.transaction.serviceCharge = $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge;
                                $scope.transaction.serviceChargeType = $scope.stockSaleForm.serviceChargeType == "" ? "" : $scope.stockSaleForm.serviceChargeType;
                                $scope.transaction.vat = $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat;
                                $scope.transaction.vatType = $scope.stockSaleForm.vatType == "" ? "" : $scope.stockSaleForm.vatType;
                                $scope.transaction.payment = $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment;
                                $scope.transaction.heading = $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name;
                                $scope.transaction.processTransaction($scope.stockSaleForm, 2);
                                $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                    if (results != null) {
                                        manageClosingQty(itemToSave, storeToSave1._id, '', 2);
                                        $scope.stockSaleForm.isPrint = true;
                                        $scope.stockSaleForm.isEdit = true;
                                        $scope.stockSaleForm.isSaved = true;
                                        $scope.stockSaleForm.printItems = itemToSave;
                                        calculateTotal_Sale(itemToSave);
                                        growl.success('Success in stock Sale', { ttl: 3000 });
                                        growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                                        $scope.stockSaleForm.enableSubmit = false;
                                        intercom.registerEvent('StockTransaction');
                                    }
                                });


                            } // end of invoice number check
                            else {
                                $scope.stockSaleForm.enableSubmit = false;
                            }


                        } else {
                            $scope.stockSaleForm.enableSubmit = false;
                        }
                    } else {

                        if (!$scope.stockSaleForm.invoiceNumber)
                            var isInvoiceNumber = confirm('Invoice Number skipped from entry ! Still you want to submit ? ')
                        else
                            isInvoiceNumber = true

                        if (isInvoiceNumber == true) {
                            $scope.transaction._items = itemToSave;
                            $scope.transaction._store = storeToSave1;
                            $scope.transaction.isOpen = false;
                            $scope.transaction.invoiceNumber = $scope.stockSaleForm.invoiceNumber;
                            $scope.transaction.charges = $scope.stockSaleForm.charges;
                            $scope.transaction.cartage = $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage;
                            $scope.transaction.discount = $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount;
                            $scope.transaction.discountType = $scope.stockSaleForm.discountType == "" ? "" : $scope.stockSaleForm.discountType;
                            $scope.transaction.serviceCharge = $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge;
                            $scope.transaction.serviceChargeType = $scope.stockSaleForm.serviceChargeType == "" ? "" : $scope.stockSaleForm.serviceChargeType;
                            $scope.transaction.vat = $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat;
                            $scope.transaction.vatType = $scope.stockSaleForm.vatType == "" ? "" : $scope.stockSaleForm.vatType;
                            $scope.transaction.payment = $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment;
                            $scope.transaction.heading = $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name;
                            $scope.transaction.processTransaction($scope.stockSaleForm, 2);
                            $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                if (results != null) {
                                    manageClosingQty(itemToSave, storeToSave1._id, '', 2);
                                    $scope.stockSaleForm.isPrint = true;
                                    $scope.stockSaleForm.isEdit = true;
                                    $scope.stockSaleForm.isSaved = true;
                                    $scope.stockSaleForm.printItems = itemToSave;
                                    calculateTotal_Sale(itemToSave);
                                    growl.success('Success in stock Sale', { ttl: 3000 });
                                    growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                                    $scope.stockSaleForm.enableSubmit = false;
                                    intercom.registerEvent('StockTransaction');
                                }
                            });

                        } // end of check
                        else {
                            $scope.stockSaleForm.enableSubmit = false;
                        }

                    }

                    // $rootScope.db.insert('stockTransaction',
                    //     {
                    //         "transactionId":$scope.stockSaleForm.transactionId,
                    //         "transactionType":"2",
                    //         "daySerialNumber":0,
                    //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "user":JSON.stringify(currentUser),
                    //         "store":JSON.stringify(angular.copy(storeToSave1)),
                    //         "toStore":"",
                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                    //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                    //         "receiver":JSON.stringify($scope.stockSaleForm.receiver),
                    //         "cartage":$scope.stockSaleForm.cartage==undefined?0:$scope.stockSaleForm.cartage,
                    //         "discount":$scope.stockSaleForm.discount==undefined?0:$scope.stockSaleForm.discount,
                    //         "discountType":$scope.stockSaleForm.discountType==""?"":$scope.stockSaleForm.discountType,
                    //         "serviceCharge":$scope.stockSaleForm.serviceCharge==undefined?0:$scope.stockSaleForm.serviceCharge,
                    //         "serviceChargeType":$scope.stockSaleForm.serviceChargeType==""?"":$scope.stockSaleForm.serviceChargeType,
                    //         "vat":$scope.stockSaleForm.vat==undefined?0:$scope.stockSaleForm.vat,
                    //         "vatType":$scope.stockSaleForm.vatType==""?"":$scope.stockSaleForm.vatType,
                    //         "payment":$scope.stockSaleForm.payment==undefined?0:$scope.stockSaleForm.payment,
                    //         "heading":$scope.stockSaleForm.heading==undefined?"":$scope.stockSaleForm.heading.Name,
                    //         "wastageItemType":"",
                    //         "wastageEntryType":"",
                    //         "created": new Date($scope.stockSaleForm.billDate).toISOString(),
                    //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                    //         "templateName":"",
                    //         "deployment_id": currentUser.deployment_id,
                    //         "tenant_id": currentUser.tenant_id,
                    //         "isSynced" : false,
                    //         "syncedOn" : undefined
                    //     }).then(function (result) {
                    //         growl.success('Success in stock Sale', {ttl: 3000});
                    //         growl.success(itemToSave.length+'Items added', {ttl: 3000});
                    //         if(result.transactionId!=undefined){
                    //                 //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockSaleForm.transactionId});
                    //                 var index=_.findIndex($scope.openTrans_Sale,{"transactionId":result.transactionId});
                    //                 $scope.openTrans_Sale.splice(index,1);
                    //         }
                    //         //$scope.stockSaleForm={enableSubmit:true,discountType:'percent'};
                    //         //$scope.stockSaleForm.availableItems=[];
                    //         $scope.stockSaleForm.isPrint=true;
                    //         $scope.stockSaleForm.isEdit=true;
                    //         $scope.stockSaleForm.isSaved=true;
                    // });
                } else {
                    //print.......
                    printStockSale(itemToSave);
                    $scope.stockSaleForm.enableSubmit = false;
                }
            } else {
                $scope.stockSaleForm.enableSubmit = true;
                var itemToSave = getItemToSave_stockSaleForm($scope.stockSaleForm.availableItems);
                if ($scope.stockSaleForm.store != undefined && $scope.stockSaleForm.receiver != undefined && itemToSave.length > 0) {

                    getItemsHistory_StockEntry($scope.stockSaleForm.availableItems);
                    $scope.stockEditHitory.editHistoryId = guid();
                    $scope.stockEditHitory.invoiceNumber = $scope.stockSaleForm.invoiceNumber
                    $scope.stockEditHitory.transactionId = $scope.stockSaleForm.transactionId;
                    $scope.stockEditHitory.mainTransactionId = $scope.stockSaleForm.mainTransactionId;
                    $scope.stockEditHitory.billNo = $scope.transaction._transactionNumber;
                    $scope.stockEditHitory.user = formatUser(currentUser);
                    $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                    $scope.stockEditHitory.deployment_id = currentUser.deployment_id;

                    EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                        growl.success('New history created', { ttl: 3000 });
                        if ($state.params != null) {
                            $location.replace().path('/stock/stockEntry');
                        }
                    });

                    var storeToSave = formatStore_stockEntryForm($scope.stockSaleForm.store);
                    var rec = formatReceiver($scope.stockSaleForm.receiver);
                    var str = formatStoreForStockSale(angular.copy(itemToSave), rec, angular.copy(storeToSave));
                    var toSave = {
                        invoiceNumber: $scope.stockEditHitory.invoiceNumber,
                        _store: str,
                        _receiver: rec,
                        _id: $scope.stockSaleForm.mainTransactionId,
                        "cartage": $scope.stockSaleForm.cartage == undefined ? 0 : $scope.stockSaleForm.cartage,
                        "discount": $scope.stockSaleForm.discount == undefined ? 0 : $scope.stockSaleForm.discount,
                        "discountType": $scope.stockSaleForm.discountType == "" ? "" : $scope.stockSaleForm.discountType,
                        "serviceCharge": $scope.stockSaleForm.serviceCharge == undefined ? 0 : $scope.stockSaleForm.serviceCharge,
                        "serviceChargeType": $scope.stockSaleForm.serviceChargeType == "" ? "" : $scope.stockSaleForm.serviceChargeType,
                        "vat": $scope.stockSaleForm.vat == undefined ? 0 : $scope.stockSaleForm.vat,
                        "charges": $scope.stockSaleForm.charges == undefined ? [] : $scope.stockSaleForm.charges,
                        "vatType": $scope.stockSaleForm.vatType == "" ? "" : $scope.stockSaleForm.vatType,
                        "payment": $scope.stockSaleForm.payment == undefined ? 0 : $scope.stockSaleForm.payment,
                        "heading": $scope.stockSaleForm.heading == undefined ? "" : $scope.stockSaleForm.heading.Name
                    };
                    StockResource.update({}, toSave, function(bill) {
                        console.log(bill);
                    });
                } else {
                    $scope.stockSaleForm.enableSubmit = false;
                }
            }
        };


        //-------------------------Physical Stock Open Transaction--------------------------------------------------

        $scope.new_physicalStockForm = function() {
            $scope.physicalStockForm.isNew = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 3, null, null);
            $scope.transaction.generateBillNumbers(3).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.physicalStockForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                else
                    $scope.physicalStockForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                //$scope.physicalStockForm.tempBillNo = "Temp/PH -" + snumbers.billnumber;
            });

            console.log($scope.transaction);
            // getPhysical_TransactionNumber(function(n){
            //     $scope.getPhysical_TransactionNumber=n;
            //     console.log(n);
            //     $scope.physicalStockForm.tempBillNo="Temp/PH -"+ $scope.getPhysical_TransactionNumber;
            // });

        };

        function addItemPhysical(item) {

            _.forEachRight($scope.physicalStockForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    var avQtyIndex = _.findIndex($scope.closingQty, {
                        itemId: item._id,
                        storeId: $scope.physicalStockForm.store._id
                    });
                    if (avQtyIndex >= 0) {
                        item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                        item.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    } else {
                        item.availableStock = parseFloat(0).toFixed(3);
                    }
                    $scope.physicalStockForm.availableItems.push(item);
                    $scope.physicalStockForm.remainingItems.splice(i, 1);
                }
            });

            slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
        };


        $scope.ot_physicalStockForm = function(openTransaction, indexxx) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 3, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber);
            $scope.physicalStockForm.showBill = false;
            $scope.physicalStockForm.isOpen = true;
            $scope.physicalStockForm.showOpenBill = true;
            if ($scope.resetSerialNumber)
                $scope.physicalStockForm.openBillNumber = "Temp/SE - " + openTransaction.daySerialNumber;
            else
                $scope.physicalStockForm.openBillNumber = "Temp/SE - " + openTransaction._transactionNumber;
            //$scope.physicalStockForm.openBillNumber = "Temp/PH - " + openTransaction._transactionNumber;
            //$scope.getPhysical_TransactionNumber=parseFloat(openTransaction.transactionNumber);
            $scope.physicalStockForm.transactionId = openTransaction.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": openTransaction._store._id });
            $scope.physicalStockForm.store = $scope.stores[st_index];

            $scope.bindCategory_physicalStockForm($scope.physicalStockForm.store);
            // console.log("Menu Recipe :" , $scope.menuRecipes);
            // _.forEach($scope.menuRecipes,function(m,i){
            //     m.itemName=m.recipeName;
            //     $scope.physicalStockForm.remainingItems.push(m);
            // });
            $scope.physicalStockForm.selectedCategory = [];
            _.forEach(openTransaction._items, function(itm, i) {
                $scope.physicalStockForm.selectedCategory[itm.fromCategory._id] = true;
                //$scope.bindItems_physicalStockForm(itm.fromCategory._id);
                addItemPhysical(itm);
            });
            $scope.physicalStockForm.isNew = true;
            $scope.physicalStockForm.enableSubmit = false;
        };

        function checkForEntry_physcical() {
            var r = false;
            _.forEach($scope.physicalStockForm.availableItems, function(itm, i) {
                if (r == false) {
                    var qty = parseFloat(itm.qty);
                    var avs = parseFloat(itm.availableStock);
                    if (qty > 0 && qty <= avs) {
                        r = true;
                    }
                }
            });
            return r;
        };

        $scope.checkPhysicalStockForOpenTransactionAndSave = function() {
            if ($state.params.id == undefined) {
                //var check=checkForEntry_physcical();
                var storeToSave = formatStore_PhysicalStockForm($scope.physicalStockForm.store);
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.physicalStockForm.availableItems, $scope.physicalStockForm.selectedUnits, $scope.stockUnits);
                //console.log(itms);
                //calculateAmtinPrefferedAndBaseUnit_PhysicalStock();
                //if(check==true){
                var itemToSave = getItemToSave_PhysicalStock(itms);
                //        var storeToSave = formatStore_PhysicalStockForm($scope.physicalStockForm.store);
                //  console.log('storeToSave :-   ',storeToSave);
                //calculateAmtinPrefferedAndBaseUnit_PhysicalStock();
                //var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.physicalStockForm.availableItems, $scope.physicalStockForm.selectedUnits,$scope.stockUnits);
                //var itemToSave = getItemToSave_PhysicalStock(itms);
                console.log('itemToSave :-   ', itemToSave);

                var sToSave = formatStoreForPhysicalStock(angular.copy(itemToSave), angular.copy(storeToSave));
                if (itemToSave.length > 0) {
                    // //process for open transaction
                    // if($scope.physicalStockForm.transactionId!=undefined){
                    //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.physicalStockForm.transactionId});
                    //     var index=_.findIndex($scope.openTrans_Physical,{"transactionId":$scope.physicalStockForm.transactionId});
                    //     $scope.openTrans_Physical.splice(index,1);
                    //     growl.success('Updating....', {ttl: 3000});
                    // }
                    // else{
                    //     $scope.physicalStockForm.transactionId=guid();
                    // }
                    // $rootScope.db.insert('stockTransaction',
                    //     {
                    //         "transactionId":$scope.physicalStockForm.transactionId,
                    //         "transactionType":"3",
                    //         "daySerialNumber":0,
                    //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "user":JSON.stringify(currentUser),
                    //         "store":JSON.stringify(angular.copy(storeToSave)),
                    //         "toStore":"",
                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                    //         "vendor":"",
                    //         "receiver":"",//JSON.stringify($scope.stockSaleForm.receiver),
                    //         "cartage":0,//$scope.stockSaleForm.cartage,
                    //         "discount":0,//$scope.stockSaleForm.discount,
                    //         "discountType":"",//$scope.stockSaleForm.discountType,
                    //         "serviceCharge":0,//$scope.stockSaleForm.serviceCharge,
                    //         "serviceChargeType":"",//$scope.stockSaleForm.serviceChargeType,
                    //         "vat":0,//$scope.stockSaleForm.vat,
                    //         "vatType":0,//$scope.stockSaleForm.vatType,
                    //         "payment":"",//$scope.stockSaleForm.payment,
                    //         "heading":"",//$scope.stockSaleForm.heading,
                    //         "wastageItemType":"",
                    //         "wastageEntryType":"",
                    //         "created": "",
                    //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                    //         "templateName":"",
                    //         "deployment_id": currentUser.deployment_id,
                    //         "tenant_id": currentUser.tenant_id,
                    //         "isSynced" : false,
                    //         "syncedOn" : undefined
                    //     }).then(function (result) {
                    //         growl.success('Saving.... ', {ttl: 3000});
                    //         var ot={
                    //             transactionId:$scope.physicalStockForm.transactionId,
                    //             transactionNumber:$scope.transaction._transactionNumber,
                    //             transactionType:"3",
                    //             user:angular.copy(currentUser),
                    //             store:angular.copy(storeToSave),
                    //                 //"toStore":"",
                    //                 //"category":"",
                    //             //vendor:angular.copy($scope.stockEntryForm.vendor),
                    //             //receiver:angular.copy($scope.stockEntryForm.receiver),
                    //             //cartage:$scope.stockEntryForm.cartage,
                    //             //discount:$scope.stockEntryForm.discount,
                    //             //discountType:$scope.stockEntryForm.discountType,
                    //             //serviceCharge:$scope.stockEntryForm.serviceCharge,
                    //             //serviceChargeType:$scope.stockEntryForm.serviceChargeType,
                    //             //vat:$scope.stockEntryForm.vat,
                    //             //vatType:$scope.stockEntryForm.vatType,
                    //             //payment:$scope.stockEntryForm.payment,
                    //             //heading:$scope.stockEntryForm.heading,
                    //                 //"wastageItemType":"",
                    //                 //"wastageEntryType":"",
                    //             created: null,
                    //             items: itemToSave,// JSON.stringify(this),
                    //             deployment_id: currentUser.deployment_id,
                    //             tenant_id: currentUser.tenant_id
                    //         };
                    //         $scope.openTrans_Physical.push(ot);
                    // });

                    if ($scope.physicalStockForm.transactionId != undefined) {
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = sToSave;
                        $scope.transaction.processTransaction($scope.physicalStockForm, 3);
                        $scope.transaction.updateTransaction({}, 'update');
                    } else {
                        $scope.physicalStockForm.transactionId = $scope.transaction._id;
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = sToSave;
                        $scope.transaction.billDate = new Date($scope.physicalStockForm.billDate);
                        $scope.transaction.processTransaction($scope.physicalStockForm, 3);
                        $scope.transaction.updateTransaction({}, 'insert');
                    }
                }
                // }
                // else
                // {
                //     $scope.physicalStockForm.enableSubmit=true;
                //     growl.error('Qty must be lesser than availableStock', {ttl: 3000});
                // }
            } else {

            }
        };

        function printPhysicalStock(itemToSave) {
            var table = "";
            table += "<table class='table table-curved' id='printTable'>";
            table += "<thead><th>Item Code</th><th>Item</th><th>Qty</th><th>Unit</th></thead>";
            table += "<tbody>";

            _.forEach(itemToSave, function(item, i) {
                table += "<tr>";
                if (!item.itemCode)
                    table += "<td>-</td>";
                else
                    table += "<td>" + item.itemCode + "</td>";
                table += "<td>" + item.itemName + "</td>";
                table += "<td>" + item.qty + "</td>";
                table += "<td>" + item.calculateInUnits[2].unitName + "</td>";
                table += "</tr>";
            });
            table += "</tbody>";
            table += "</table>";
            var printer = window.open('', '', 'width=600,height=600');
            printer.document.open("text/html");
            printer.document.write(table);
            printer.document.close();
            printer.focus();
            printer.print();
        };
        //-------------------------Physical Stock --------------------------------------------------
        $scope.physicalStockForm = { isNew: false, enableSubmit: true, maxDate: new Date(), billDate: new Date() };
        $scope.clearPhysicalStockTab = function() {
            $scope.physicalStockForm = {
                isNew: false,
                enableSubmit: true,
                showBill: true,
                billDate: new Date(),
                maxDate: new Date()
            };
            //$scope.physicalStockForm.tempBillNo="Temp/PH -"+ $scope.transaction._transactionNumber;
            var op = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 3, null);
            op.getOpenTransactions(3).then(function(results) {
                console.log(results);
                $scope.openTrans_Physical = results;
            });

            op.getTemplates(103).then(function(results) {
                $scope.template_Physical = results;
            });
            if ($state.params != null) {
                $location.replace().path('/stock/stockEntry');
            }

        };
        $scope.resetPhysicalStockTab = function() {
            if (!$scope.physicalStockForm.isEdit) {
                $scope.physicalStockForm = {
                    isNew: true,
                    showBill: true,
                    enableSubmit: true,
                    billDate: new Date(),
                    maxDate: new Date()
                };
                //$scope.physicalStockForm.tempBillNo="temp/"+$filter('date')(new Date(), 'dd-MMM-yyyy');
                //$scope.physicalStockForm.tempBillNo="Temp/PH -"+ $scope.transaction._transactionNumber;
                $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 3, null, null);
                $scope.transaction.generateBillNumbers(3).then(function(snumbers) {
                    console.log("snum", snumbers);
                    $scope.transaction._transactionNumber = snumbers.billnumber;
                    $scope.transaction.transactionNumber = snumbers.billnumber;
                    $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                    if ($scope.resetSerialNumber) {
                        console.log("iff");
                        console.log("day", snumbers.daySerialNumber);
                        $scope.physicalStockForm.tempBillNo = "Temp/SE - " + snumbers.daySerialNumber;

                    } else {
                        console.log("else");
                        console.log("bill no", snumbers.billumber);
                        $scope.physicalStockForm.tempBillNo = "Temp/SE - " + snumbers.billnumber;

                    }

                    // $scope.physicalStockForm.tempBillNo = "Temp/PH -" + snumbers.billnumber;
                });
            } else {
                //edit
                $scope.physicalStockForm.isPrint = false;
                $scope.physicalStockForm.isEdit = false;
                $scope.physicalStockForm.isSaved = false;
            }
        };
        $scope.newReset_Physical = function() {
            $scope.physicalStockForm = {
                isNew: true,
                enableSubmit: true,
                showBill: true,
                billDate: new Date(),
                maxDate: new Date()
            };
            $scope.physicalStockForm.showBill = true;
            $scope.physicalStockForm.showOpenBill = false;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 3, null, null);
            $scope.transaction.generateBillNumbers(3).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                if ($scope.resetSerialNumber)
                    $scope.physicalStockForm.tempBillNo = "Temp/PH -" + snumbers.daySerialNumber;
                else
                    $scope.physicalStockForm.tempBillNo = "Temp/PH -" + snumbers.billnumber;
            });
            console.log($scope.physicalStockForm.showBill);
        };
        $scope.tempBill_physicalStockForm = function(billDate) {
            $scope.physicalStockForm.showBill = true;
            $scope.physicalStockForm.showOpenBill = false;
            //$scope.physicalStockForm.tempBillNo="temp/"+$filter('date')(billDate, 'dd-MMM-yyyy');
            $scope.physicalStockForm.tempBillNo = "Temp/PH -" + $scope.transaction._transactionNumber;
        };

        $scope.checkQtyPhysical = function(qty, itemId) {
            var index = _.findIndex($scope.physicalStockForm.availableItems, { _id: itemId });
            if (isNaN(qty)) {

                $scope.physicalStockForm.availableItems[index].qty = "";
            } else {
                if (parseFloat(qty) < 0) {
                    growl.error('Qty must not be negative', { ttl: 3000 });
                    $scope.physicalStockForm.availableItems[index].qty = "";
                }
                // else if(parseFloat(qty)>parseFloat($scope.physicalStockForm.availableItems[index].availableStock)){
                //     $scope.physicalStockForm.availableItems[index].qty="";
                //     growl.error('Qty must not be greater than availableStock', {ttl: 3000});
                // }
                else {
                    $scope.enableSubmitButton_Physical($scope.physicalStockForm.availableItems);
                }
            }
        };

        function slectedUnit_physicalStockForm(item) {
            $scope.physicalStockForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.physicalStockForm.selectedUnits[itm._id] = u;
                            if (itm.availableStock != 0) {
                                itm.availableStock = parseFloat(parseFloat(itm.closingQtyInBase) / parseFloat(u.conversionFactor)).toFixed(3);
                            }
                        }
                    }
                    else{
                        if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.physicalStockForm.selectedUnits[itm._id]=u;
                                if(itm.availableStock!=0){
                                    itm.availableStock=parseFloat(parseFloat(itm.closingQtyInBase)/parseFloat(u.conversionFactor)).toFixed(3);
                                }
                            }
                        }
                    }
                });
            });
        };

        $scope.addRemainingItem_physicalStockForm = function($item, $model, $label) {
            // var remaining = angular.copy($scope.physicalStockForm.remainingItems);
            // _.forEach($scope.physicalStockForm.remainingItems, function (itm, i) {
            //   if (itm._id == $item._id) {
            //     $scope.physicalStockForm.availableItems.push(angular.copy(itm));
            //     remaining.splice(i, 1);
            //     $scope.physicalStockForm.itemToAdd = "";
            //     $scope.physicalStockForm.selectedCategory[itm.fromCategory._id] = true;
            //   }
            // });
            // $scope.physicalStockForm.remainingItems = remaining;
            // slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);

            var remaining = angular.copy($scope.physicalStockForm.remainingItems);
            _.forEach($scope.physicalStockForm.remainingItems, function(itm, i) {
                if (itm._id == $item._id) {
                    if (itm.toDate != undefined || itm.fromDate != undefined) {
                        var toD = new Date(itm.toDate);
                        var fD = new Date(itm.fromDate);
                        var cD = new Date();
                        if (cD < fD || cD > toD) {
                            itm.hide = true;
                            alert("Disabled items contract has been expired");
                            //growl.error("Disabled items contract has been expired", {ttl:3000});
                        }
                    }
                    $scope.physicalStockForm.selectedCategory[itm.fromCategory._id] = true;
                    var avQtyIndex = _.findIndex($scope.closingQty, {
                        itemId: itm._id,
                        storeId: $scope.physicalStockForm.store._id
                    });
                    if (avQtyIndex >= 0) {
                        itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                        itm.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    } else {
                        itm.availableStock = parseFloat(0).toFixed(3);
                    }
                    $scope.physicalStockForm.availableItems.unshift(itm);
                    //$scope.stockEntryForm.remainingItems.splice(i,1);
                    remaining.splice(i, 1);
                    $scope.physicalStockForm.itemToAdd = "";
                }
            });
            $scope.physicalStockForm.remainingItems = remaining;
            slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
        };

        $scope.deleteSelectedItemPhysicalForm = function(item) {
            var index = _.findIndex($scope.physicalStockForm.availableItems, { "itemName": item.itemName });
            $scope.physicalStockForm.availableItems.splice(index, 1);
            $scope.physicalStockForm.remainingItems.push(angular.copy(item));
        };

        $scope.bindCategory_physicalStockForm = function(stores) {
            var store = angular.copy(stores);
            $scope.physicalStockForm.availableCategory = [];
            $scope.physicalStockForm.availableItems = [];
            $scope.physicalStockForm.remainingItems = [];
            $scope.physicalStockForm.selectedCategory = {};
            $scope.physicalStockForm.availableTemplate = [];
            $scope.physicalStockForm.selectedTemplate = {};

            var items = angular.copy($scope.menuRecipes);
            var citems = [];
            var processedCat = { _id: "PosistTech111111", categoryName: "Processed Food" };
            _.forEach(store.processedFoodCategory, function(c, i) {
                _.forEach(c.item, function(itm, iipp) {
                    var obj = { _id: itm._id, itemName: itm.itemName };
                    citems.push(obj);
                });
            });
            _.forEachRight(items, function(ii, i) {
                var ind = _.findIndex(citems, { _id: ii.itemId });
                if (ind >= 0) {
                    ii.itemName = ii.recipeName;
                    ii.preferedUnit = ii.selectedUnitId._id;

                    if (ii.units == undefined) {
                        ii.units = [{ _id: "9980", unitName: "Pack" }];
                    }
                    var varFromCategory = { "_id": processedCat._id, "categoryName": processedCat.categoryName };
                    ii["fromCategory"] = varFromCategory;
                    if (!ii.itemCode)
                        ii.itemCode = '';
                    $scope.physicalStockForm.remainingItems.push(angular.copy(ii));
                } else {
                    items.splice(i, 1);
                }
            });
            processedCat.item = items;
           var bkItems=[];
           var group=_.groupBy($scope.baseKitchenItems, function(b) { return b.itemId})
            // console.log("group",group)
            
             
            _.forEach(group,function(g){
              // console.log("g",g)
              var c=0
              _.forEach(g,function(itm){
                 if(c==0)
                bkItems.push(itm)  
               c++
              })
            })
            
            var bItems = [];
            var baseKitchenItemsCat = { _id: "PosistTech333333", categoryName: "Base Kitchen Items" };
            _.forEach(bkItems, function(bItem) {
                var item = {
                    itemName: bItem.itemName,
                    _id: bItem.itemId,
                    itemId: bItem.itemId,
                    itemType: "Base Kitchen Item",
                    fromCategory: angular.copy(baseKitchenItemsCat),
                    units: bItem.units,
                    preferedUnit: bItem.preferedUnit,
                    itemCode: ''
                }
                bItems.push(item);
                if (!item.itemCode)
                        item.itemCode = '';
                $scope.physicalStockForm.remainingItems.push(angular.copy(item));
            });
            baseKitchenItemsCat.item = bItems;
            $scope.physicalStockForm.availableCategory.push(angular.copy(baseKitchenItemsCat));
        
            var processedSemi_Cat = { _id: "PosistTech222222", categoryName: "InterMediate Food" };
            var ir = angular.copy($scope.intermediateRecipes);
            _.forEach(ir, function(ii, i) {
                ii.itemName = ii.recipeName;
                ii.preferedUnit = ii.selectedUnitId._id;
                if (ii.units == undefined) {
                    ii.units = [{ _id: "9980", unitName: "Pack" }];
                }
                var varFromCategory = { "_id": processedSemi_Cat._id, "categoryName": processedSemi_Cat.categoryName };
                ii["fromCategory"] = varFromCategory;
                if (!ii.itemCode)
                    ii.itemCode = '';
                $scope.physicalStockForm.remainingItems.push(angular.copy(ii));
            });

            processedSemi_Cat.item = ir;

            if (store.isKitchen == true) {
                $scope.physicalStockForm.availableCategory.push(angular.copy(processedCat));
            }
            $scope.physicalStockForm.availableCategory.push(angular.copy(processedSemi_Cat));

            _.forEach(store.category, function(c, ii) {
                $scope.physicalStockForm.availableCategory.push(angular.copy(c));
                _.forEach(c.item, function(item, iii) {
                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    item["fromCategory"] = varFromCategory;
                    if (c.categoryName == "Processed Food") {
                        item.itemType = "MenuItem";
                    } else if (c.categoryName == "InterMediate Food") {
                        item.itemType = "IntermediateItem";
                    } else {
                        item.itemType = "RawMaterial";
                    }
                    if (!item.itemCode)
                        item.itemCode = '';
                    var avQtyIndex = _.findIndex($scope.closingQty, {
                        itemId: item._id,
                        storeId: $scope.physicalStockForm.store._id
                    });
                    if (avQtyIndex >= 0) {
                        item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                        item.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    } else {
                        item.availableStock = parseFloat(0).toFixed(3);
                    }
                    $scope.physicalStockForm.remainingItems.push(angular.copy(item));
                });
            });

            _.forEach($scope.template_Physical, function(t, i) {
                if (stores._id == t._store._id) {
                    $scope.physicalStockForm.availableTemplate.push(angular.copy(t));
                }
            });
        };
        // $scope.bindItem_physicalStockForm= function(catId){
        //     if( $scope.stockSaleForm.availableItems==undefined){
        //          $scope.stockSaleForm.availableItems=[];
        //     }
        //     var aCat=angular.copy($scope.stockSaleForm.availableCategory);
        //     _.forEach(aCat, function(c,i){
        //         if($scope.stockSaleForm.selectedCategory[catId]==true){
        //             if(c._id==catId){
        //                 _.forEach(c.item , function(itm,ii){
        //                     $scope.stockSaleForm.availableItems.push(itm);
        //                 });
        //             }
        //         }
        //         else{
        //             if(c._id==catId){
        //                 _.forEach(c.item , function(itm,ii){
        //                     var index=_.findIndex($scope.stockSaleForm.availableItems,{_id:itm._id});
        //                     $scope.stockSaleForm.availableItems.splice(index,1);
        //                 });
        //             }
        //         }
        //     });
        //     slectedUnit_saleForm($scope.stockSaleForm.availableItems);
        // };

        $scope.bindItems_physicalStockFormTemplate = function(templateId) {
            if ($scope.physicalStockForm.availableItems == undefined) {
                $scope.physicalStockForm.availableItems = [];
            }

            _.forEach($scope.physicalStockForm.availableTemplate, function(t, i) {
                if (t._id == templateId) {
                    _.forEach(t._items, function(itm, ii) {
                        var av = _.findIndex($scope.physicalStockForm.availableItems, { "_id": itm._id });
                        if (av < 0) {
                            $scope.physicalStockForm.availableItems.push(angular.copy(itm));
                            var index = _.findIndex($scope.physicalStockForm.remainingItems, { "_id": itm._id });
                            $scope.physicalStockForm.remainingItems.splice(index, 1);
                            var cS = itm.fromCategory._id;
                            $scope.physicalStockForm.selectedCategory[cS] = true;
                        } else {
                            $scope.physicalStockForm.availableItems.splice(av, 1);
                            $scope.physicalStockForm.availableItems.push(angular.copy(itm));
                        }
                    });
                }
            });
            slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
        };

        $scope.bindItems_physicalStockForm = function(catId) {
            //var store=angular.copy(stores);
            //$scope.physicalStockForm.availableItems=[];
            var cat = angular.copy($scope.physicalStockForm.availableCategory);
            _.forEach(cat, function(c, ii) {
                if ($scope.physicalStockForm.selectedCategory[catId] == true) {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var aa = _.findIndex($scope.physicalStockForm.availableItems, { "_id": itm._id });
                            if (aa < 0) {
                                var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                itm["fromCategory"] = varFromCategory;
                                if (c.categoryName == "Processed Food") {
                                    itm.itemType = "MenuItem";
                                } else if (c.categoryName == "InterMediate Food") {
                                    itm.itemType = "IntermediateItem";
                                } else {
                                    itm.itemType = "RawMaterial";
                                }
                                var avQtyIndex = _.findIndex($scope.closingQty, {
                                    itemId: itm._id,
                                    storeId: $scope.physicalStockForm.store._id
                                });
                                if (avQtyIndex >= 0) {
                                    itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                                    itm.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                                } else {
                                    itm.availableStock = parseFloat(0).toFixed(3);
                                }
                                $scope.physicalStockForm.availableItems.push(angular.copy(itm));
                                var index = _.findIndex($scope.physicalStockForm.remainingItems, { "_id": itm._id });
                                $scope.physicalStockForm.remainingItems.splice(index, 1);
                            }
                        });
                    }
                } else {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var index = _.findIndex($scope.physicalStockForm.availableItems, { _id: itm._id });
                            if (index >= 0) {
                                var avQtyIndex = _.findIndex($scope.closingQty, {
                                    itemId: itm._id,
                                    storeId: $scope.physicalStockForm.store._id
                                });
                                if (avQtyIndex >= 0) {
                                    itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                                    itm.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                                } else {
                                    itm.availableStock = parseFloat(0).toFixed(3);
                                }
                                var itemToSplice = angular.copy($scope.physicalStockForm.availableItems[index]);
                                $scope.physicalStockForm.availableItems.splice(index, 1);
                                $scope.physicalStockForm.remainingItems.push(itemToSplice);
                            }
                        });
                    }
                }
            });
            slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
        };
        $scope.goNextPhysicalStock = function(nextIdx) {
            var f = angular.element(document.querySelector('#phy_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                // $scope.physicalStockForm.enableSubmit=false;
                // angular.element(document.querySelector('#submitPhysicalStock')).focus();
            }
        };

        function getItemToSave_PhysicalStock(itemss) {
            var items = angular.copy(itemss);
            _.forEachRight(items, function(itm, i) {
                if (itm.qty != "" || itm.qty != undefined) {
                    var flag = false;
                    var it = angular.copy(itm);
                    _.forEach($scope.physicalStockForm.selectedUnits, function(u, ii) {
                        if (it) {
                            if (ii == it._id && it.qty != "" && it.qty != undefined && u != null && $scope.physicalStockForm.selectedUnits[it._id] != undefined) {
                                // var selectedUnit={"_id":u._id,"unitName":u.unitName,"conversionFactor":u.conversionFactor,"baseQty":itm.qty,"type":"selectedUnit"};
                                // itm["selectedUnit"]=selectedUnit;
                                // itm.calculateInUnits=[];
                                // itm.calculateInUnits.push(itm.baseUnit);
                                // if(itm.preferedUnits!=undefined){
                                //         itm.calculateInUnits.push(itm.preferedUnits);
                                //     }
                                //     else
                                //     {
                                //         var preUnit={"_id":u._id,"unitName":u.unitName,"conversionFactor":u.conversionFactor,"baseQty":itm.qty,"type":"preferedUnit"};
                                //         itm["preferedUnit"]=preUnit;
                                //         itm.calculateInUnits.push(itm.preferedUnit);
                                //         itm.preferedUnit=u._id;
                                //     }
                                // itm.calculateInUnits.push(itm.selectedUnit);
                                if (it.itemType != "RawMaterial") {
                                    var ss = $scope.transaction.calculateForProcessedAndSemiProcessed_sale(it);
                                    it = ss;
                                }
                                flag = true;
                            }
                        }
                    });
                    if (flag == false) {
                        items.splice(i, 1);
                    }
                }
            });
            _.forEach(items, function(itm, i) {
                for (var p in itm) {
                    if (p != "_id" && p != "itemName" && p != "qty" && p != "rawItems" && p != "calculateInUnits" && p != "menuQty" && p != "recipeQty" && p != "receipeDetails" && p != "isSemiProcessed" && p != "quantity" && p != "selectedUnit" && p != "preferedUnit" && p != "itemType" && p != "fromCategory" && p != "units" && p != "itemCode") {
                        //&& p!="baseUnit"  && p!="preferedUnits" && p!="selectedUnit"
                        _.forEach(itm.units, function(u, ii) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
            });
            return items;
        };

        function formatStore_PhysicalStockForm(store) {
            var stores = angular.copy(store);
            for (var p in stores) {
                if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID") {
                    delete stores[p];
                }
            }
            return stores;
        };

        function calculateAmtinPrefferedAndBaseUnit_PhysicalStock() {
            var items = $scope.physicalStockForm.availableItems;
            _.forEach(items, function(item, i) {
                var conversionFactor = 1;
                var selectedConversionFactor = 1;
                if (item.calculateInUnits != undefined) {
                    selectedConversionFactor = item.calculateInUnits[2].conversionFactor;
                } else {
                    _.forEach($scope.physicalStockForm.selectedUnits, function(u, ii) {
                        if (ii == item._id) {
                            selectedConversionFactor = u.conversionFactor;
                        }
                    });
                }


                var baseUnit = {};
                if (item.units[0].baseUnit != undefined) {
                    baseUnit = item.units[0].baseUnit;
                    baseUnit.unitName = baseUnit.name;
                } else {
                    var ind = _.findIndex(item.units, { "type": "baseUnit" });
                    baseUnit = item.units[ind];
                    baseUnit.unitName = baseUnit.name;
                }


                var baseQty = parseFloat(item.qty * selectedConversionFactor);
                baseUnit.baseQty = baseQty;
                baseUnit.conversionFactor = conversionFactor;
                baseUnit.type = "baseUnit";
                item.baseUnit = baseUnit;

                var pInd = _.findIndex(item.units, { _id: item.preferedUnit });
                if (pInd >= 0) {
                    var preferedUnits = angular.copy(item.units[pInd]);
                    var preferedconversionFactor = item.units[pInd].conversionFactor;
                    if (preferedconversionFactor == conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
                    } else if (preferedconversionFactor > conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                    } else if (preferedconversionFactor < conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                    }
                    preferedUnits.type = "preferedUnit";
                    delete preferedUnits.baseUnit;
                    preferedUnits.conversionFactor = preferedconversionFactor;
                    item.preferedUnits = preferedUnits;
                }
            });
            return items;
        };

        function formatStoreForPhysicalStock(item, store) {
            var stores = store;
            stores.category = [];
            _.forEach(item, function(itm, i) {
                var index = _.findIndex(stores.category, { _id: itm.fromCategory._id });
                if (index < 0) {
                    stores.category.push(angular.copy(itm.fromCategory));
                }
            });
            _.forEach(stores.category, function(c, i) {
                c.items = [];
                _.forEachRight(item, function(itm, ii) {
                    if (c._id == itm.fromCategory._id) {
                        delete itm.fromCategory;
                        c.items.push(itm);
                        item.splice(ii, 1);
                    }
                });
            });
            return stores;
        };

        function getItemsHistory_Physical(items) {
            if ($state.params != undefined) {
                $scope.stockEditHitory.items = [];
                _.forEach($scope.stockEditEntry, function(itm, i) {
                    itm.status = "deleted";
                    $scope.stockEditHitory.items.push(itm);
                });
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.stockEditHitory.items, { _id: itm._id });
                    if (index < 0) {
                        itm.status = "deleted";
                    } else {
                        itm.updatedFeild = [];
                        if (itm.qty != $scope.stockEditHitory.items[index].qty) {
                            itm.status = "updated";
                            itm.updatedFeild.push("qty");
                        } else {
                            itm.status = "inserted";
                        }
                        $scope.stockEditHitory.items.splice(index, 1);
                    }
                    $scope.stockEditHitory.items.push(itm);
                    //console.log($scope.stockEditHitory.items);
                });
            };
        };
        $scope.submitPhysicalStock = function(formPhysicalStock) {
            if ($state.params.id == undefined) {
                $scope.checkPhysicalStockForOpenTransactionAndSave();
                $scope.physicalStockForm.enableSubmit = true;
                //check for necessary data before submit
                var storeToSave = formatStore_PhysicalStockForm($scope.physicalStockForm.store);
                //calculateAmtinPrefferedAndBaseUnit_PhysicalStock();
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.physicalStockForm.availableItems, $scope.physicalStockForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_PhysicalStock(itms);
                
                var sToSave = formatStoreForPhysicalStock(angular.copy(itemToSave), angular.copy(storeToSave));
                if (!sToSave.category) {
                    var stores = storeToSave;
                    stores.category = [];
                    _.forEach(itemToSave, function(itm, i) {
                        var index = _.findIndex(stores.category, { _id: itm.fromCategory._id });
                        if (index < 0) {
                            stores.category.push(angular.copy(itm.fromCategory));
                        }
                    });
                    _.forEach(stores.category, function(c, i) {
                        c.items = [];
                        _.forEachRight(itemToSave, function(itm, ii) {
                            if (c._id == itm.fromCategory._id) {
                                delete itm.fromCategory;
                                c.items.push(itm);
                                item.splice(ii, 1);
                            }
                        });
                    });
                    sToSave = angular.copy(stores);
                    
                } else if (sToSave.category.length == 0) {
                    var stores = storeToSave;
                    stores.category = [];
                    _.forEach(itemToSave, function(itm, i) {
                        var index = _.findIndex(stores.category, { _id: itm.fromCategory._id });
                        if (index < 0) {
                            stores.category.push(angular.copy(itm.fromCategory));
                        }
                    });
                    _.forEach(stores.category, function(c, i) {
                        c.items = [];
                        _.forEachRight(itemToSave, function(itm, ii) {
                            if (c._id == itm.fromCategory._id) {
                                delete itm.fromCategory;
                                c.items.push(itm);
                                item.splice(ii, 1);
                            }
                        });
                    });
                    sToSave = angular.copy(stores);
                    
                }
                if (formPhysicalStock.$valid && !$scope.physicalStockForm.isPrint) {
                    if (itemToSave.length > 0) {

                        if (itemToSave.length < $scope.physicalStockForm.availableItems.length) {
                            var isGood = confirm("Some Items are Escaped from Physical Entry ! Are You Confirm?");
                            if (isGood == true) {
                                $scope.transaction._items = itemToSave;
                                $scope.transaction._store = sToSave;
                                $scope.transaction.isOpen = false;
                                $scope.transaction.processTransaction($scope.physicalStockForm, 3);
                                $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                    if (results != null) {
                                        manageClosingQty(itemToSave, sToSave._id, '', 3);
                                        $scope.physicalStockForm.isPrint = true;
                                        $scope.physicalStockForm.isEdit = true;
                                        $scope.physicalStockForm.isSaved = true;
                                        growl.success('Success in Physical Entry', { ttl: 3000 });
                                        growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                                        $scope.physicalStockForm.enableSubmit = false;
                                        intercom.registerEvent('StockTransaction');
                                    }
                                }).catch(function(err) {
                                    console.log('update failed', err);
                                });
                            } else {
                                $scope.physicalStockForm.enableSubmit = false;
                            }
                        } else {

                            $scope.transaction._items = itemToSave;
                            $scope.transaction._store = sToSave;
                            $scope.transaction.isOpen = false;
                            $scope.transaction.processTransaction($scope.physicalStockForm, 3);
                            $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                if (results != null) {
                                    manageClosingQty(itemToSave, sToSave._id, '', 3);
                                    $scope.physicalStockForm.isPrint = true;
                                    $scope.physicalStockForm.isEdit = true;
                                    $scope.physicalStockForm.isSaved = true;
                                    growl.success('Success in Physical Entry', { ttl: 3000 });
                                    growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                                    $scope.physicalStockForm.enableSubmit = false;
                                    intercom.registerEvent('StockTransaction');
                                }

                            }).catch(function(err) {
                                console.log('update failed', err);
                            });
                        }
                    }
                } else {
                    printPhysicalStock(itemToSave);
                    $scope.physicalStockForm.enableSubmit = false;
                }

                // if($scope.physicalStockForm.transactionId!=undefined){
                //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.physicalStockForm.transactionId});
                //     var index=_.findIndex($scope.openTrans_Physical,{"transactionId":$scope.physicalStockForm.transactionId});
                //     $scope.openTrans_Physical.splice(index,1);
                // }
                // else{
                //     $scope.physicalStockForm.transactionId=guid();
                // }

                // if(formPhysicalStock.$valid && !$scope.physicalStockForm.isPrint){
                //     $rootScope.db.insert('stockTransaction',
                //     {
                //         "transactionId":$scope.physicalStockForm.transactionId,
                //         "transactionType":"3",
                //         "daySerialNumber":0,
                //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                //         "user":JSON.stringify(currentUser),
                //         "store":JSON.stringify(angular.copy(sToSave)),
                //         "toStore":"",//JSON.stringify(angular.copy(tstore)),
                //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                //         "receiver":"",
                //         "cartage":0,//$scope.stockEntryForm.cartage,
                //         "discount":0,//$scope.stockEntryForm.discount,
                //         "discountType":"",//$scope.stockEntryForm.discountType,
                //         "serviceCharge":0,
                //         "serviceChargeType":"",
                //         "vat":0,
                //         "vatType":"",
                //         "payment":0,
                //         "heading":"",
                //         "wastageItemType":"",
                //         "wastageEntryType":"",
                //         "created": new Date($scope.physicalStockForm.billDate).toISOString(),
                //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                //         "templateName":"",
                //         "deployment_id": currentUser.deployment_id,
                //         "tenant_id": currentUser.tenant_id,
                //         "isSynced" : false,
                //         "syncedOn" : undefined
                //     }).then(function (result) {
                //         growl.success('Inserted Successfully', {ttl: 3000});
                //         growl.success(itemToSave.length+' Items Inserted', {ttl: 3000});
                //         //$scope.physicalStockForm={enableSubmit:true};
                //         if(result.transactionId!=undefined){
                //             //$rootScope.db.del('stockTransaction',{"transactionId":$scope.physicalStockForm.transactionId});
                //             var index=_.findIndex($scope.openTrans_Physical,{"transactionId":result.transactionId});
                //             $scope.openTrans_Physical.splice(index,1);
                //         }
                //         //$scope.getPhysical_TransactionNumber=parseFloat($scope.getPhysical_TransactionNumber)+1;
                //         $scope.physicalStockForm.isPrint=true;
                //         $scope.physicalStockForm.isEdit=true;
                //         $scope.physicalStockForm.isSaved=true;
                //     });
                // }
                // else{
                //     //Print
                //     printPhysicalStock(itemToSave);
                // }
            } else {

                $scope.physicalStockForm.enableSubmit = true;
                var storeToSave = formatStore_PhysicalStockForm($scope.physicalStockForm.store);
                calculateAmtinPrefferedAndBaseUnit_PhysicalStock();
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.physicalStockForm.availableItems, $scope.physicalStockForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_PhysicalStock(itms);
                if (itemToSave.length > 0 && $scope.physicalStockForm.store != undefined) {

                    getItemsHistory_Physical($scope.physicalStockForm.availableItems);
                    $scope.stockEditHitory.editHistoryId = guid();
                    $scope.stockEditHitory.transactionId = $scope.physicalStockForm.transactionId;
                    $scope.stockEditHitory.mainTransactionId = $scope.physicalStockForm.mainTransactionId;
                    $scope.stockEditHitory.billNo = $scope.physicalStockForm.tempBillNo;
                    $scope.stockEditHitory.user = formatUser(currentUser);
                    $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                    $scope.stockEditHitory.deployment_id = currentUser.deployment_id;

                    EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                        growl.success('New history created', { ttl: 3000 });
                        if ($state.params != null) {
                            $location.replace().path('/stock/stockEntry');
                        }
                    });

                    var sToSave = formatStoreForPhysicalStock(angular.copy(itemToSave), angular.copy(storeToSave));
                    var toSave = { _store: sToSave, _id: $scope.physicalStockForm.mainTransactionId };
                    StockResource.update({}, toSave, function(bill) {
                        console.log(bill);
                    });
                } else {
                    $scope.physicalStockForm.enableSubmit = false;
                }
            }
        };

        //----------------------------------------Wastage Open TRansaction------------------------------------------//

        $scope.new_wastageEntryForm = function() {
            $scope.wastageEntryForm.isNew = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 4, null, null);
            $scope.transaction.generateBillNumbers(4).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.wastageEntryForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                else
                    $scope.wastageEntryForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                //$scope.wastageEntryForm.tempBillNo = "Temp/WS -" + snumbers.billnumber;
            });
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            $scope.wastageEntryForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
        };

        function loadWastageItems(itemtype) {
            $scope.wastageEntryForm.availableItems = [];
            $scope.wastageEntryForm.remainingItems = [];
            if (itemtype != "MenuItem" && itemtype != "InterMediate-Item") {
                $scope.wastageEntryForm.hideMenuItem = true;
                $scope.wastageEntryForm.hideRawMaterial = false;
                var str = angular.copy($scope.stores);
                _.forEach(str, function(s, i) {
                    if (s.storeName == itemtype) {
                        _.forEach(s.category, function(c, ii) {
                            _.forEachRight(c.item, function(itm, iii) {
                                var index = _.findIndex($scope.wastageEntryForm.remainingItems, { 'itemName': itm.itemName });
                                if (index < 0) {
                                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                    itm["fromCategory"] = varFromCategory;
                                    $scope.wastageEntryForm.remainingItems.push(angular.copy(itm));
                                }

                            });
                        });
                    }
                });
            } else {
                $scope.wastageEntryForm.hideMenuItem = false;
                $scope.wastageEntryForm.hideRawMaterial = true;
                $scope.wastageEntryForm.remainingItems = angular.copy($scope.menuRecipes); //($scope.menuItems);
                _.forEach($scope.wastageEntryForm.remainingItems, function(itm, i) {
                    if (itm.units == "" || itm.units == undefined) {
                        var u = [{ "_id": i, "unitName": "pack" }];
                        itm["units"] = u;
                    }
                });
            }
        };

        function addItemWastage(item) {
            _.forEachRight($scope.wastageEntryForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    $scope.wastageEntryForm.availableItems.push(item);
                    $scope.wastageEntryForm.remainingItems.splice(i, 1);
                }
            });

            slectedUnit_wastageEntryForm($scope.wastageEntryForm.availableItems);
        };

        function addItemWastage_old(item, itemtype) {

            //if(itemtype=="RawMaterial"){
            if (itemtype != "MenuItem" && itemtype != "InterMediate-Item") {
                _.forEachRight($scope.wastageEntryForm.remainingItems, function(itm, i) {
                    if (itm._id == item._id) {
                        $scope.wastageEntryForm.availableItems.push(angular.copy(item));
                        $scope.wastageEntryForm.remainingItems.splice(i, 1);
                    }
                });
                selectedUnit_rawMaterialWastageForm($scope.wastageEntryForm.availableItems);
            } else {
                _.forEachRight($scope.wastageEntryForm.remainingItems, function(itm, i) {
                    if (itm._id == item._id) {
                        $scope.wastageEntryForm.availableItems.push(angular.copy(item));
                        $scope.wastageEntryForm.remainingItems.splice(i, 1);
                    }
                });

                selectedUnit_menuItemWastageForm($scope.wastageEntryForm.availableItems);
            }

        };

        $scope.ot_wastageEntryForm = function(openTransaction, indexxx) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 4, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber);
            $scope.wastageEntryForm.showBill = false;
            $scope.wastageEntryForm.isOpen = true;
            $scope.wastageEntryForm.showOpenBill = true;
            if ($scope.resetSerialNumber)
                $scope.wastageEntryForm.openBillNumber = "Temp/SE - " + openTransaction.daySerialNumber;
            else
                $scope.wastageEntryForm.openBillNumber = "Temp/SE - " + openTransaction._transactionNumber;
            //$scope.wastageEntryForm.openBillNumber = "Temp/WS - " + openTransaction._transactionNumber;
            //$scope.getPhysical_TransactionNumber=parseFloat(openTransaction.transactionNumber);
            $scope.wastageEntryForm.transactionId = openTransaction.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": openTransaction._store._id });
            $scope.wastageEntryForm.store = $scope.stores[st_index];

            $scope.bindCategory_wastageEntryForm($scope.wastageEntryForm.store);
            _.forEach($scope.menuRecipes, function(m, i) {
                $scope.wastageEntryForm.remainingItems.push(m);
            });
            $scope.wastageEntryForm.selectedCategory = [];
            _.forEach(openTransaction._items, function(itm, i) {
                $scope.wastageEntryForm.selectedCategory[itm.fromCategory._id] = true;
                //$scope.bindItems_physicalStockForm(itm.fromCategory._id);
                //addItemPhysical(itm);
                addItemWastage(itm);
            });
            $scope.wastageEntryForm.isNew = true;
            $scope.wastageEntryForm.enableSubmit = false;
        };
        $scope.ot_wastageEntryForm_old = function(openTransaction, indexxx) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 4, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber);
            $scope.wastageEntryForm.showBill = false;
            $scope.wastageEntryForm.isOpen = true;
            $scope.wastageEntryForm.showOpenBill = true;
            if ($scope.resetSerialNumber)
                $scope.wastageEntryForm.openBillNumber = "Temp/SE - " + openTransaction.daySerialNumber;
            else
                $scope.wastageEntryForm.openBillNumber = "Temp/SE - " + openTransaction._transactionNumber;
            //$scope.wastageEntryForm.openBillNumber = "Temp/WS - " + openTransaction._transactionNumber; //(indexxx+1);
            $scope.wastageEntryForm.transactionId = openTransaction.transactionId;
            $scope.wastageEntryForm.entryType = openTransaction.wastageEntryType;
            var st_index = _.findIndex($scope.wastageEntryForm.entryFor, { "Name": openTransaction.wastageItemType });
            $scope.wastageEntryForm.forEntry = $scope.wastageEntryForm.entryFor[st_index];
            $scope.wastageEntryForm.remainingItems = [];
            $scope.wastageEntryForm.availableItems = [];
            loadWastageItems(openTransaction.wastageItemType);
            _.forEach(openTransaction._items, function(itm, i) {
                addItemWastage(itm, openTransaction.wastageItemType);
            });
            $scope.wastageEntryForm.isNew = true;
            $scope.wastageEntryForm.enableSubmit = true;
        };

        function getItemToSave_wastageEntry(itemss) {
            var items = angular.copy(itemss);
            _.forEachRight(items, function(itm, i) {
                if (itm.qty != "" || itm.qty != undefined) {
                    var flag = false;
                    _.forEach($scope.wastageEntryForm.selectedUnits, function(u, ii) {
                        if (ii == itm._id && itm.qty != "" && itm.qty != undefined && u != null && $scope.wastageEntryForm.selectedUnits[itm._id] != undefined) {
                            if (itm.itemType != "RawMaterial") {
                                // var ss=$scope.transaction.calculateForProcessedAndSemiProcessed_sale(itm);
                                // itm=ss;
                                var rd = $scope.transaction.calculateForRecipeDetails(itm);
                                itm = rd;
                            }
                            flag = true;
                        }
                    });
                    if (flag == false) {
                        items.splice(i, 1);
                    }
                }
            });
            _.forEach(items, function(itm, i) {
                for (var p in itm) {
                    if (p != "_id" && p != "itemName" && p != "qty" && p != "rawItems" && p != "calculateInUnits" && p != "menuQty" && p != "recipeQty" && p != "receipeDetails" && p != "isSemiProcessed" && p != "quantity" && p != "selectedUnit" && p != "preferedUnit" && p != "itemType" && p != "fromCategory" && p != "units" && p != "comment" && p != 'itemCode') {
                        _.forEach(itm.units, function(u, ii) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
            });
            return items;
        };

        function formatStoreForWastage(item, store) {
            var stores = store;
            stores.category = [];
            _.forEach(item, function(itm, i) {
                var index = _.findIndex(stores.category, { _id: itm.fromCategory._id });
                if (index < 0) {
                    stores.category.push(itm.fromCategory);
                }
            });
            _.forEach(stores.category, function(c, i) {
                c.items = [];
                _.forEachRight(item, function(itm, ii) {
                    if (c._id == itm.fromCategory._id) {
                        delete itm.fromCategory;
                        c.items.push(itm);
                        item.splice(ii, 1);
                    }
                });
            });
            return stores;
        };
        $scope.checkWsatageEntryForOpenTransactionAndSave = function() {
            if ($state.params.id == undefined) {
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.wastageEntryForm.availableItems, $scope.wastageEntryForm.selectedUnits, $scope.stockUnits);
                //var itemToSave=formatItemsForInsert_wastageForm(itms);
                var itemToSave = getItemToSave_wastageEntry(itms)
                var str = formatStore_PhysicalStockForm($scope.wastageEntryForm.store);
                var storeToSave = formatStoreForWastage(angular.copy(itemToSave), angular.copy(str));
                //var storeToSave=formatStoreForWastage(angular.copy(itemToSave));
                if (itemToSave.length > 0) {
                    //process for open transaction
                    // if($scope.wastageEntryForm.transactionId!=undefined){
                    //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.wastageEntryForm.transactionId});
                    //     var index=_.findIndex($scope.openTrans_Wastage,{"transactionId":$scope.wastageEntryForm.transactionId});
                    //     $scope.openTrans_Wastage.splice(index,1);
                    //     growl.success('Updating....', {ttl: 3000});
                    // }
                    // else{
                    //     $scope.wastageEntryForm.transactionId=guid();
                    // }
                    // //var storeToSave=formatStore_PhysicalStockForm($scope.physicalStockForm.store);

                    // $rootScope.db.insert('stockTransaction',
                    //     {
                    //         "transactionId":$scope.wastageEntryForm.transactionId,
                    //         "transactionType":"4",
                    //         "daySerialNumber":0,
                    //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "user":JSON.stringify(currentUser),
                    //         "store":"",//JSON.stringify(angular.copy(storeToSave)),
                    //         "toStore":"",
                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                    //         "vendor":"",
                    //         "receiver":"",//JSON.stringify($scope.stockSaleForm.receiver),
                    //         "cartage":0,//$scope.stockSaleForm.cartage,
                    //         "discount":0,//$scope.stockSaleForm.discount,
                    //         "discountType":"",//$scope.stockSaleForm.discountType,
                    //         "serviceCharge":0,//$scope.stockSaleForm.serviceCharge,
                    //         "serviceChargeType":"",//$scope.stockSaleForm.serviceChargeType,
                    //         "vat":0,//$scope.stockSaleForm.vat,
                    //         "vatType":"",//$scope.stockSaleForm.vatType,
                    //         "payment":0,//$scope.stockSaleForm.payment,
                    //         "heading":"",//$scope.stockSaleForm.heading,
                    //         "wastageItemType":$scope.wastageEntryForm.forEntry.Name,
                    //         "wastageEntryType":$scope.wastageEntryForm.entryType,
                    //         "created": "",
                    //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                    //         "templateName":"",
                    //         "deployment_id": currentUser.deployment_id,
                    //         "tenant_id": currentUser.tenant_id,
                    //         "isSynced" : false,
                    //         "syncedOn" : undefined
                    //     }).then(function (result) {
                    //         growl.success('Saving.... ', {ttl: 3000});
                    //         var ot={
                    //             transactionId:$scope.wastageEntryForm.transactionId,
                    //             transactionNumber:$scope.transaction._transactionNumber,
                    //             transactionType:"4",
                    //             user:angular.copy(currentUser),
                    //             wastageItemType:$scope.wastageEntryForm.forEntry.Name,
                    //             wastageEntryType:$scope.wastageEntryForm.entryType,
                    //             created: null,
                    //             items: itemToSave,// JSON.stringify(this),
                    //             deployment_id: currentUser.deployment_id,
                    //             tenant_id: currentUser.tenant_id
                    //         };
                    //         $scope.openTrans_Wastage.push(ot);
                    // });

                    if ($scope.wastageEntryForm.transactionId != undefined) {
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = storeToSave;
                        //$scope.transaction.wastageItemType=$scope.wastageEntryForm.forEntry.Name;
                        $scope.transaction.wastageEntryType = $scope.wastageEntryForm.entryType;
                        $scope.transaction.processTransaction($scope.wastageEntryForm, 4);
                        $scope.transaction.updateTransaction({}, 'update');
                    } else {
                        $scope.wastageEntryForm.transactionId = $scope.transaction._id;
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = storeToSave;
                        //$scope.transaction.wastageItemType=$scope.wastageEntryForm.forEntry.Name;
                        $scope.transaction.wastageEntryType = $scope.wastageEntryForm.entryType;
                        $scope.transaction.billDate = new Date($scope.wastageEntryForm.billDate);
                        $scope.transaction.processTransaction($scope.wastageEntryForm, 4);
                        $scope.transaction.updateTransaction({}, 'insert');
                    }
                }
            } else {

            }
        };

        function printWastageEntry(itemToSave) {
            var table = "";
            table += "<table class='table table-curved' id='printTable'>";
            table += "<thead><th>Item Code</th><th>Item</th><th>Qty</th><th>Unit</th></thead>";
            table += "<tbody>";

            _.forEach(itemToSave, function(item, i) {
                table += "<tr>";
                if (!item.itemCode)
                    table += "<td>-</td>";
                else
                    table += "<td>" + item.itemCode + "</td>";
                table += "<td>" + item.itemName + "</td>";
                table += "<td>" + item.qty + "</td>";
                table += "<td>" + item.calculateInUnits[2].unitName + "</td>";
                table += "</tr>";
            });
            table += "</tbody>";
            table += "</table>";
            var printer = window.open('', '', 'width=600,height=600');
            printer.document.open("text/html");
            printer.document.write(table);
            printer.document.close();
            printer.focus();
            printer.print();
        };

        //---------------------------------------Wastage Entry ---------------------------------------//
        $scope.wastageEntryForm = {
            isNew: false,
            entryType: 'Wastage',
            hideMenuItem: true,
            hideRawMaterial: true,
            enableSubmit: false,
            stockTransactionTypes: 'WastageEntry',
            maxDate: new Date(),
            billDate: new Date()
        };

        function getStoresForWastage() {
            //var stores=angular.copy($scope.stores);
            var str = [];
            _.forEach($scope.stores, function(s, i) {
                var obj = { _id: s._id, Name: s.storeName };
                str.push(obj);
            });
            var obj1 = { _id: "PosMenuItem", Name: "MenuItem" };
            var obj2 = { _id: "PosIntermediateItem", Name: "InterMediate-Item" };
            str.push(obj1);
            str.push(obj2);
            return str;
        };

        //$scope.wastageEntryForm.entryFor=getStoresForWastage();
        // $scope.wastageEntryForm.entryFor=[
        //     {Name:'RawMaterial'},{Name:'MenuItem'},{Name:'InterMediate-Item'}
        // ];
        $scope.clearWastageEntryTab = function() {

            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            $scope.wastageEntryForm = {
                isNew: false,
                entryType: 'Wastage',
                showBill: true,
                hideMenuItem: true,
                hideRawMaterial: true,
                enableSubmit: true,
                stockTransactionTypes: 'WastageEntry',
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                isSaved: false
            };
            $scope.wastageEntryForm.entryFor = getStoresForWastage();

            var op = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 4, null);
            op.getOpenTransactions(4).then(function(results) {
                console.log(results);
                $scope.openTrans_Wastage = results;
            });

            op.getTemplates(104).then(function(results) {
                $scope.template_Wastage = results;
                // $scope.wastageEntryForm.availableTemplate=[];
                // _.forEach($scope.template_Wastage, function(t,i){
                //     $scope.wastageEntryForm.availableTemplate.push(t);
                // });
            });


            if ($state.params != null) {
                $location.replace().path('/stock/stockEntry');
            }
        };

        $scope.bindCategory_wastageEntryForm = function(stores) {
            var store = angular.copy(stores);
            $scope.wastageEntryForm.availableCategory = [];
            $scope.wastageEntryForm.availableItems = [];
            $scope.wastageEntryForm.remainingItems = [];
            $scope.wastageEntryForm.selectedCategory = {};
            $scope.wastageEntryForm.availableTemplate = [];
            $scope.wastageEntryForm.selectedTemplate = {};

            // var items=angular.copy($scope.menuRecipes);
            // _.forEach(items, function(ii,i){
            //     ii.itemName=ii.recipeName;
            //     ii.preferedUnit=ii.selectedUnitId._id;
            //     if(ii.units==undefined){
            //         ii.units=[{_id:"9980",unitName:"Pack"}];
            //     }
            // });
            var items = angular.copy($scope.menuRecipes);
            var citems = [];
            var processedCat = { _id: "PosistTech111111", categoryName: "Processed Food" };
            _.forEach(store.processedFoodCategory, function(c, i) {
                _.forEach(c.item, function(itm, iipp) {
                    var obj = { _id: itm._id, itemName: itm.itemName };
                    citems.push(obj);
                });
            });

            _.forEachRight(items, function(ii, i) {
                var ind = _.findIndex(citems, { _id: ii.itemId });
                if (ind >= 0) {
                    ii.itemName = ii.recipeName;
                    ii.preferedUnit = ii.selectedUnitId._id;
                    ii.itemType = "MenuItem";
                    var varFromCategory = { "_id": processedCat._id, "categoryName": processedCat.categoryName };
                    ii["fromCategory"] = varFromCategory;
                    if (ii.units == undefined) {
                        ii.units = [{ _id: "9980", unitName: "Pack" }];
                    }
                    ii.itemCode = '';
                    $scope.wastageEntryForm.remainingItems.push(angular.copy(ii));
                } else {
                    items.splice(i, 1);
                }
            });

            processedCat.item = items;
            var bkItems=[]
            var group=_.groupBy($scope.baseKitchenItems, function(b) { return b.itemId})
                // console.log("group",group)
                
                 
                _.forEach(group,function(g){
                  // console.log("g",g)
                  var c=0
                  _.forEach(g,function(itm){
                     if(c==0)
                    bkItems.push(itm)  
                   c++
                  })
                })
                var bItems = [];
                var baseKitchenItemsCat = { _id: "PosistTech333333", categoryName: "Base Kitchen Items" };
                _.forEach(bkItems, function(bItem) {
                    var item = {
                        itemName: bItem.itemName,
                        _id: bItem.itemId,
                        itemId: bItem.itemId,
                        itemType: "Base Kitchen Item",
                        fromCategory: angular.copy(baseKitchenItemsCat),
                        units: bItem.units,
                        preferedUnit: bItem.preferedUnit,
                        itemCode: ''
                    }
                    bItems.push(item);
                     if (!item.itemCode)
                        item.itemCode = '';
                    $scope.wastageEntryForm.remainingItems.push(angular.copy(item));
                });
                baseKitchenItemsCat.item = bItems;
               
                $scope.wastageEntryForm.availableCategory.push(angular.copy(baseKitchenItemsCat));
            

            var processedSemi_Cat = { _id: "PosistTech222222", categoryName: "InterMediate Food" };
            var ir = angular.copy($scope.intermediateRecipes);
            _.forEach(ir, function(ii, i) {
                ii.itemName = ii.recipeName;
                ii.preferedUnit = ii.selectedUnitId._id;
                ii.itemType = "IntermediateItem";
                var varFromCategory = { "_id": processedSemi_Cat._id, "categoryName": processedSemi_Cat.categoryName };
                ii["fromCategory"] = varFromCategory;
                if (ii.units == undefined) {
                    ii.units = [{ _id: "9980AZ", unitName: "Pack" }];
                }
                ii.itemCode = '';
                $scope.wastageEntryForm.remainingItems.push(angular.copy(ii));
            });

            processedSemi_Cat.item = ir;

            if (store.isKitchen == true) {
                $scope.wastageEntryForm.availableCategory.push(angular.copy(processedCat));
            }
            $scope.wastageEntryForm.availableCategory.push(angular.copy(processedSemi_Cat));

            _.forEach(store.category, function(c, ii) {
                $scope.wastageEntryForm.availableCategory.push(angular.copy(c));
                _.forEach(c.item, function(item, iii) {

                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    item["fromCategory"] = varFromCategory;
                    if (c.categoryName == "Processed Food") {
                        item.itemType = "MenuItem";
                    } else if (c.categoryName == "InterMediate Food") {
                        item.itemType = "IntermediateItem";
                    } else {
                        item.itemType = "RawMaterial";
                    }
                    if (!item.itemCode)
                        item.itemCode = '';
                    $scope.wastageEntryForm.remainingItems.push(angular.copy(item));
                });
            });

            _.forEach($scope.template_Wastage, function(t, i) {
                if (stores._id == t._store._id) {
                    $scope.wastageEntryForm.availableTemplate.push(angular.copy(t));
                }
            });
        };
        $scope.resetWastageEntryTab = function() {
            if (!$scope.wastageEntryForm.isEdit) {
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                $scope.wastageEntryForm = {
                    isNew: true,
                    entryType: 'Wastage',
                    showBill: true,
                    hideMenuItem: true,
                    enableSubmit: true,
                    billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                    maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                };
                $scope.wastageEntryForm.entryFor = [
                    { Name: 'RawMaterial' }, { Name: 'MenuItem' }, { Name: 'InterMediate-Item' }
                ];
                //$scope.wastageEntryForm.tempBillNo="temp/"+$filter('date')(new Date(), 'dd-MMM-yyyy');
                //$scope.wastageEntryForm.tempBillNo="Temp/WS -"+ $scope.transaction._transactionNumber;
                $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 4, null, null);
                $scope.transaction.generateBillNumbers(4).then(function(snumbers) {
                    $scope.transaction._transactionNumber = snumbers.billnumber;
                    $scope.transaction.transactionNumber = snumbers.billnumber;
                    $scope.transaction.daySerialNumer = snumbers.daySerialNumber;
                    if ($scope.resetSerialNumber)
                        $scope.wastageEntryForm.tempBillNo = "Temp/WS -" + snumbers.daySerialNumber;
                    else
                        $scope.wastageEntryForm.tempBillNo = "Temp/WS -" + snumbers.billnumber;
                });
            } else {
                //edit
                $scope.wastageEntryForm.isSaved = false;
                $scope.wastageEntryForm.isPrint = false;
                $scope.wastageEntryForm.isEdit = false;
            }
        };

        $scope.newReset_Wastage = function() {
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            $scope.wastageEntryForm = {
                isNew: true,
                entryType: 'Wastage',
                showBill: true,
                hideMenuItem: true,
                enableSubmit: true,
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
            };
            // $scope.wastageEntryForm.entryFor=[
            //     {Name:'RawMaterial'},{Name:'MenuItem'},{Name:'InterMediate-Item'}
            // ];
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 4, null, null);
            $scope.transaction.generateBillNumbers(4).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.wastageEntryForm.tempBillNo = "Temp/WS -" + snumbers.daySerialNumber;
                else
                    $scope.wastageEntryForm.tempBillNo = "Temp/WS -" + snumbers.billnumber;
            });
            $scope.wastageEntryForm.entryFor = getStoresForWastage();
        };

        $scope.tempBill_wastageEntryForm = function(billDate) {
            $scope.wastageEntryForm.showBill = true;
            $scope.wastageEntryForm.showOpenBill = false;
            $scope.wastageEntryForm.tempBillNo = "Temp/WS -" + $scope.transaction._transactionNumber;
            var serverDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate($rootScope.serverTime.serverTime))));
            var selectedDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(billDate))));
            var d = serverDate.getTime() - selectedDate.getTime()
            var di = new Date(d)
            var hours = Math.abs(serverDate - selectedDate) / 36e5;
            if (hours >= 72) {
                growl.success('Back Date Entry has been restricted to 48 Hours.', { ttl: 3000 });
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                $scope.wastageEntryForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            }
        };

        function selectedUnit_menuItemWastageForm(item) {
            $scope.wastageEntryForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    //$scope.wastageEntryForm.selectedUnits[itm._id]=u;
                    if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId._id == u._id) {
                            $scope.wastageEntryForm.selectedUnits[itm._id] = u;
                        }
                    }
                });
            });
        };

        function selectedUnit_rawMaterialWastageForm(item) {
            $scope.wastageEntryForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    // if(itm.preferedUnit!=undefined){
                    //     if(itm.preferedUnit==u._id){
                    //         $scope.wastageEntryForm.selectedUnits[itm._id]=u;
                    //     }
                    // }
                    // else{
                    if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.wastageEntryForm.selectedUnits[itm._id] = u;
                        }
                    }
                    //}
                });
            });
        };

        $scope.checkQtyWastage = function(qty, itemId) {
            var index = _.findIndex($scope.wastageEntryForm.availableItems, { _id: itemId });
            if (isNaN(qty)) {
                $scope.wastageEntryForm.availableItems[index].qty = "";
            } else {
                $scope.enableSubmitButton_Wastage($scope.wastageEntryForm.availableItems);
            }
        };

        $scope.addRemainingItem_wastageEntryForm = function($item, $model, $label) {
            // var remaining=angular.copy($scope.wastageEntryForm.remainingItems);
            // _.forEach($scope.wastageEntryForm.remainingItems, function(itm,i){
            //     if(itm._id==$item._id){
            //         $scope.wastageEntryForm.availableItems.push(angular.copy(itm));
            //         remaining.splice(i,1);
            //         $scope.wastageEntryForm.itemToAdd="";
            //     }
            // });
            // $scope.wastageEntryForm.remainingItems=remaining;
            // if($scope.wastageEntryForm.forEntry.Name=="RawMaterial"){
            //     selectedUnit_rawMaterialWastageForm($scope.wastageEntryForm.availableItems);
            // }
            // else
            // {
            //     _.forEach($scope.wastageEntryForm.availableItems, function(itm,i){
            //         if(itm.units=="" || itm.units==undefined){
            //             var u=[{"_id":i,"unitName":"pack"}];
            //             itm["units"]=u;
            //         }
            //     });
            //     selectedUnit_menuItemWastageForm($scope.wastageEntryForm.availableItems);
            // }
            var remaining = angular.copy($scope.wastageEntryForm.remainingItems);
            _.forEach($scope.wastageEntryForm.remainingItems, function(itm, i) {
                if (itm._id == $item._id) {
                    $scope.wastageEntryForm.availableItems.unshift(angular.copy(itm));
                    remaining.splice(i, 1);
                    $scope.wastageEntryForm.itemToAdd = "";
                    $scope.wastageEntryForm.selectedCategory[itm.fromCategory._id] = true;
                }
            });
            $scope.wastageEntryForm.remainingItems = remaining;
            slectedUnit_wastageEntryForm($scope.wastageEntryForm.availableItems);
        };

        $scope.deleteSelectedItemWastageForm = function(item) {
            var index = _.findIndex($scope.wastageEntryForm.availableItems, { _id: item._id });
            $scope.wastageEntryForm.availableItems.splice(index, 1);
            $scope.wastageEntryForm.remainingItems.push(angular.copy(item));
        };

        $scope.bindItems_wastageEntryFormTemplate = function(templateId) {
            if ($scope.wastageEntryForm.availableItems == undefined) {
                $scope.wastageEntryForm.availableItems = [];
            }

            _.forEach($scope.wastageEntryForm.availableTemplate, function(t, i) {
                if (t._id == templateId) {
                    _.forEach(t._items, function(itm, ii) {
                        var av = _.findIndex($scope.wastageEntryForm.availableItems, { _id: itm._id });
                        if (av < 0) {
                            $scope.wastageEntryForm.availableItems.push(angular.copy(itm));
                            var index = _.findIndex($scope.wastageEntryForm.remainingItems, { _id: itm._id });
                            $scope.wastageEntryForm.remainingItems.splice(index, 1);
                            var cS = itm.fromCategory._id;
                            $scope.wastageEntryForm.selectedCategory[cS] = true;
                        } else {
                            $scope.wastageEntryForm.availableItems.splice(av, 1);
                            $scope.wastageEntryForm.availableItems.push(angular.copy(itm));
                        }
                    });
                }
            });
            slectedUnit_wastageEntryForm($scope.wastageEntryForm.availableItems);
        };
        $scope.bindItems_wastageEntryFormTemplate_old = function(templateId) {
            if ($scope.wastageEntryForm.availableItems == undefined) {
                $scope.wastageEntryForm.availableItems = [];
            }
            var entry = "";
            _.forEach($scope.wastageEntryForm.availableTemplate, function(t, i) {
                var ind = _.findIndex($scope.wastageEntryForm.entryFor, { "Name": t.wastageItemType });
                $scope.wastageEntryForm.forEntry = $scope.wastageEntryForm.entryFor[ind];
                entry = t.wastageItemType;
                $scope.wastageEntryForm.wastageEntryType = t.wastageEntryType;
                if (t._id == templateId) {
                    _.forEach(t._items, function(itm, ii) {
                        var av = _.findIndex($scope.wastageEntryForm.availableItems, { "_id": itm._id });

                        if (entry == "MenuItem") {
                            itm.itemType = "MenuItem";
                        } else if (entry == "InterMediate-Item") {
                            itm.itemType = "IntermediateItem";
                        } else {
                            itm.itemType = "RawMaterial";
                        }

                        if (av < 0) {
                            $scope.wastageEntryForm.availableItems.push(angular.copy(itm));
                            //var index=_.findIndex($scope.wastageEntryForm.remainingItems,{"itemName":itm.itemName});
                            //$scope.wastageEntryForm.remainingItems.splice(index,1);
                        } else {
                            $scope.wastageEntryForm.availableItems.splice(av, 1);
                            $scope.wastageEntryForm.availableItems.push(angular.copy(itm));
                        }
                    });
                }
            });
            if (entry != "MenuItem" || entry != "InterMediate-Item") {
                $scope.wastageEntryForm.hideMenuItem = true;
                $scope.wastageEntryForm.hideRawMaterial = false;
                selectedUnit_rawMaterialWastageForm($scope.wastageEntryForm.availableItems);
            } else {
                $scope.wastageEntryForm.hideMenuItem = false;
                $scope.wastageEntryForm.hideRawMaterial = true;
                selectedUnit_menuItemWastageForm($scope.wastageEntryForm.availableItems);
            }
        };

        function bindWastageItem(forEntry) {
            $scope.wastageEntryForm.availableItems = [];
            $scope.wastageEntryForm.selectedUnits = [];
            $scope.wastageEntryForm.remainingItems = [];
            if (forEntry == "RawMaterial") {
                $scope.wastageEntryForm.hideMenuItem = true;
                $scope.wastageEntryForm.hideRawMaterial = false;
                var str = angular.copy($scope.stores);
                _.forEach(str, function(s, i) {
                    _.forEach(s.category, function(c, ii) {
                        _.forEach(c.item, function(itm, iii) {
                            var index = _.findIndex($scope.wastageEntryForm.availableItems, { 'itemName': itm.itemName });
                            itm.itemType = "RawMaterial";
                            itm.store = { "_id": s._id, "storeName": s.storeName, "UID": s.storeUID };
                            if (index < 0) {
                                var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                itm["fromCategory"] = varFromCategory;
                                $scope.wastageEntryForm.availableItems.push(angular.copy(itm));
                                var aa = _.findIndex($scope.wastageEntryForm.remainingItems, { "itemName": itm.itemName });
                                $scope.wastageEntryForm.remainingItems.splice(aa, 1);
                            }
                        });
                    });
                });
                selectedUnit_rawMaterialWastageForm($scope.wastageEntryForm.availableItems);
            } else {
                $scope.wastageEntryForm.hideMenuItem = false;
                $scope.wastageEntryForm.hideRawMaterial = true;
                $scope.wastageEntryForm.availableItems = angular.copy($scope.menuRecipes); //($scope.menuItems);

                _.forEach($scope.wastageEntryForm.availableItems, function(itm, i) {
                    if (itm.isSemiProcessed == true) {
                        itm.store = { "_id": "store123intermediate", "storeName": "Intermediate", "UID": "" };
                        itm.itemType = "IntermediateItem";
                    } else {
                        itm.store = { "_id": "store123MenuItem", "storeName": "MenuItem", "UID": "" };
                        itm.itemType = "MenuItem";
                    }

                    if (itm.units == "" || itm.units == undefined) {
                        var u = [{ "_id": i, "unitName": "pack" }];
                        itm["units"] = u;
                    }
                });
                selectedUnit_menuItemWastageForm($scope.wastageEntryForm.availableItems);
            }
        }

        $scope.bindItem_wastageEntry = function(entryFor) {
            $scope.wastageEntryForm.availableItems = [];
            $scope.wastageEntryForm.selectedUnits = [];
            $scope.wastageEntryForm.remainingItems = [];

            if (entryFor.Name == "MenuItem") {
                $scope.wastageEntryForm.hideMenuItem = false;
                $scope.wastageEntryForm.hideRawMaterial = true;
                $scope.wastageEntryForm.availableItems = angular.copy($scope.menuRecipes); //angular.copy($scope.menuItems);
                _.forEach($scope.wastageEntryForm.availableItems, function(itm, i) {
                    itm.name = itm.recipeName;
                    itm.itemType = "MenuItem";
                    itm.store = { "_id": "store123MenuItem", "storeName": "MenuItem", "UID": "" };
                    var varFromCategory = { "_id": "0001PosistTech0012", "categoryName": "MenuItem" };
                    itm["fromCategory"] = varFromCategory;

                    if (itm.units == "" || itm.units == undefined) {
                        var u = [{ "_id": i, "unitName": "pack" }];
                        itm["units"] = u;
                    }
                });
                selectedUnit_menuItemWastageForm($scope.wastageEntryForm.availableItems);
            } else if (entryFor.Name == "InterMediate-Item") {
                $scope.wastageEntryForm.hideMenuItem = false;
                $scope.wastageEntryForm.hideRawMaterial = true;
                $scope.wastageEntryForm.availableItems = angular.copy($scope.intermediateRecipes); //angular.copy($scope.menuItems);
                _.forEach($scope.wastageEntryForm.availableItems, function(itm, i) {
                    itm.name = itm.recipeName;
                    itm.store = { "_id": "store123intermediate", "storeName": "Intermediate", "UID": "" };
                    itm.itemType = "IntermediateItem";
                    var varFromCategory = { "_id": "001processed1212sd2", "categoryName": "IntermediateItem" };
                    itm["fromCategory"] = varFromCategory;

                    if (itm.units == "" || itm.units == undefined) {
                        var u = [{ "_id": i, "unitName": "pack" }];
                        itm["units"] = u;
                    }
                });
                selectedUnit_menuItemWastageForm($scope.wastageEntryForm.availableItems);
            } else {
                $scope.wastageEntryForm.hideMenuItem = true;
                $scope.wastageEntryForm.hideRawMaterial = false;
                //var str=angular.copy($scope.stores);

                var str = angular.copy($scope.wastageEntryForm.forEntry);
                var ind = _.findIndex($scope.stores, { _id: str._id });
                var store = angular.copy($scope.stores[ind]);
                console.log(str);
                //_.forEach(str, function(s,i){
                _.forEach(store.category, function(c, ii) {
                    _.forEach(c.item, function(itm, iii) {
                        var index = _.findIndex($scope.wastageEntryForm.availableItems, { 'itemName': itm.itemName });
                        itm.itemType = "RawMaterial";
                        //itm.store={"_id":s._id,"storeName":s.storeName,"UID":s.storeUID};
                        itm.store = { "_id": store._id, "storeName": store.storeName, "UID": store.storeUID };
                        if (index < 0) {
                            var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                            itm["fromCategory"] = varFromCategory;
                            $scope.wastageEntryForm.availableItems.push(angular.copy(itm));
                            var aa = _.findIndex($scope.wastageEntryForm.remainingItems, { "itemName": itm.itemName });
                            $scope.wastageEntryForm.remainingItems.splice(aa, 1);
                        }
                    });
                });
                //});
                selectedUnit_rawMaterialWastageForm($scope.wastageEntryForm.availableItems);
            }
        };
        $scope.bindItems_wastageEntryForm = function(catId) {
            var cat = angular.copy($scope.wastageEntryForm.availableCategory);
            _.forEach(cat, function(c, ii) {
                if ($scope.wastageEntryForm.selectedCategory[catId] == true) {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var aa = _.findIndex($scope.wastageEntryForm.availableItems, { "_id": itm._id });
                            if (aa < 0) {
                                var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                itm["fromCategory"] = varFromCategory;
                                if (c.categoryName == "Processed Food") {
                                    itm.itemType = "MenuItem";
                                } else if (c.categoryName == "InterMediate Food") {
                                    itm.itemType = "IntermediateItem";
                                } else {
                                    itm.itemType = "RawMaterial";
                                }
                                var index = _.findIndex($scope.wastageEntryForm.remainingItems, { "_id": itm._id });
                                var itemToSplice = angular.copy($scope.wastageEntryForm.remainingItems[index]);
                                $scope.wastageEntryForm.availableItems.push(itemToSplice);
                                //var index = _.findIndex($scope.wastageEntryForm.remainingItems, {"itemName": itm.itemName});
                                $scope.wastageEntryForm.remainingItems.splice(index, 1);
                            }
                        });
                    }
                } else {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var index = _.findIndex($scope.wastageEntryForm.availableItems, { _id: itm._id });
                            if (index >= 0) {
                                var itemToSplice = angular.copy($scope.wastageEntryForm.availableItems[index]);
                                $scope.wastageEntryForm.remainingItems.push(itemToSplice);
                                $scope.wastageEntryForm.availableItems.splice(index, 1);
                            }
                        });
                    }
                }
            });
            slectedUnit_wastageEntryForm($scope.wastageEntryForm.availableItems);
        };

        function slectedUnit_wastageEntryForm(item) {
            $scope.wastageEntryForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.wastageEntryForm.selectedUnits[itm._id] = u;
                        }
                    }
                    else{
                        if(itm.preferedUnit!=undefined){
                            if(itm.preferedUnit==u._id){
                                $scope.wastageEntryForm.selectedUnits[itm._id]=u;
                            }
                        }
                    }
                });
            });
        };

        $scope.goNextWastageEntry = function(nextIdx, item) {
            var flag = false;
            _.forEach($scope.wastageEntryForm.selectedUnits, function(u, i) {
                if (i == item._id && u != null) {
                    flag = true;
                }
            });
            if (item.qty != "" && item.qty != undefined && isNaN(item.qty) != true && flag == true) {
                item["entryStatus"] = "stockEntryStatus_Ok";
            } else {
                item["entryStatus"] = "stockEntryStatus_Error";
            }
            var f = angular.element(document.querySelector('#w_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                // $scope.wastageEntryForm.enableSubmit=false;
                // angular.element(document.querySelector('#submitWastageEntry')).focus();
            }
        };
        $scope.goNextWastageEntry_menuItem = function(nextIdx, item) {
            if (item.qty != "" && item.qty != undefined && isNaN(item.qty) != true) {
                item["entryStatus"] = "stockEntryStatus_Ok";
            } else {
                item["entryStatus"] = "stockEntryStatus_Error";
            }
            var f = angular.element(document.querySelector('#wi_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                $scope.wastageEntryForm.enableSubmit = false;
                angular.element(document.querySelector('#submitWastageEntry')).focus();
            }
        };

        function formatItemsForInsert_wastageForm(itemss) {
            //var items=angular.copy($scope.wastageEntryForm.availableItems);
            var items = angular.copy(itemss);
            _.forEachRight(items, function(itm, i) {
                if (itm.qty != "" || itm.qty != undefined || $scope.wastageEntryForm.selectedUnits[itm._id] != undefined) {
                    var flag = false;
                    _.forEach($scope.wastageEntryForm.selectedUnits, function(u, ii) {
                        if (ii == itm._id && itm.qty != "" && itm.qty != undefined && u != null) {
                            // var selectedUnit={"_id":u._id,"unitName":u.unitName,"conversionFactor":u.conversionFactor,"baseQty":itm.qty,"type":"selectedUnit"};
                            // itm["selectedUnit"]=selectedUnit;
                            // itm.calculateInUnits=[];
                            // itm.calculateInUnits.push(itm.baseUnit);
                            // if(itm.preferedUnits!=undefined){
                            //         itm.calculateInUnits.push(itm.preferedUnits);
                            // }
                            // else
                            // {
                            //         var preUnit={"_id":u._id,"unitName":u.unitName,"conversionFactor":u.conversionFactor,"baseQty":itm.qty,"type":"preferedUnit"};
                            //         itm["preferedUnit"]=preUnit;
                            //         itm.calculateInUnits.push(itm.preferedUnit);
                            //         itm.preferedUnit=u._id;
                            // }
                            // itm.calculateInUnits.push(itm.selectedUnit);
                            if (itm.itemType != "RawMaterial") {
                                var ss = $scope.transaction.calculateForProcessedAndSemiProcessed_sale(itm);
                                itm = ss;
                            }
                            flag = true;
                        }
                    });
                    if (flag == false) {
                        items.splice(i, 1);
                    }
                }
            });
            _.forEach(items, function(itm, i) {
                //if($scope.wastageEntryForm.forEntry.Name!="MenuItem" && $scope.wastageEntryForm.forEntry.Name!="InterMediate-Item"){
                for (var p in itm) {
                    if (p != "_id" && p != "name" && p != "itemName" && p != "quantity" && p != "qty" && p != "preferedUnit" && p != "rawItems" && p != "menuQty" && p != "recipeQty" && p != "receipeDetails" && p != "isSemiProcessed" && p != "itemType" && p != "fromCategory" && p != "store" && p != "rate" && p != "units" && p != "selectedUnit" && p != "calculateInUnits") {
                        //&& p!="preferedUnits" && p!="baseUnit"  && p!="preferedUnit"  && p!="selectedUnit"
                        _.forEach(itm.units, function(u, ii) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
                // }
                // else
                // {
                //     for(var p in itm){
                //         if(p!="_id" && p!="name" && p!="qty" && p!="preferedUnit" && p!="rawItems" && p!="calculateInUnits" && p!="menuQty" && p!="recipeQty"  && p!="receipeDetails" && p!="isSemiProcessed" && p!="itemType"  && p!="fromCategory" && p!="store"  && p!="rate" && p!="units" && p!="selectedUnit"){
                //             //&& p!="preferedUnits" && p!="baseUnit" && p!="selectedUnit"
                //             delete itm[p];
                //         }
                //     }
                // }
            });
            return items;
        };

        function calculateAmtinPrefferedAndBaseUnit_Wastage() {
            var items = $scope.wastageEntryForm.availableItems;
            _.forEach(items, function(item, i) {
                var conversionFactor = 1;
                var selectedConversionFactor = 1;
                _.forEach($scope.wastageEntryForm.selectedUnits, function(u, ii) {
                    if (ii == item._id) {
                        selectedConversionFactor = u.conversionFactor;
                    }
                });
                var baseUnit = item.units[0].baseUnit;
                var baseQty = parseFloat(item.qty * selectedConversionFactor);
                baseUnit.baseQty = baseQty;
                baseUnit.unitName = baseUnit.name;
                baseUnit.conversionFactor = conversionFactor;
                baseUnit.type = "baseUnit";
                item.baseUnit = baseUnit;

                var pInd = _.findIndex(item.units, { _id: item.preferedUnit });
                if (pInd >= 0) {
                    var preferedUnits = angular.copy(item.units[pInd]);
                    var preferedconversionFactor = item.units[pInd].conversionFactor;
                    if (preferedconversionFactor == conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
                    } else if (preferedconversionFactor > conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                    } else if (preferedconversionFactor < conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                    }
                    preferedUnits.type = "preferedUnit";
                    delete preferedUnits.baseUnit;
                    preferedUnits.conversionFactor = preferedconversionFactor;
                    item.preferedUnits = preferedUnits;
                }

            });
            return items;
        };

        function formatStoreForWastage_old(itemToSave) {
            var item = angular.copy(itemToSave);
            var stores = item[0].store;
            stores.category = [];
            _.forEach(item, function(itm, i) {
                var index = _.findIndex(stores.category, { _id: itm.fromCategory._id });
                if (index < 0) {
                    stores.category.push(itm.fromCategory);
                }
            });
            _.forEach(stores.category, function(c, i) {
                c.items = [];
                _.forEachRight(item, function(itm, ii) {
                    if (c._id == itm.fromCategory._id) {
                        delete itm.fromCategory;
                        delete itm.store;
                        c.items.push(itm);
                        item.splice(ii, 1);
                    }
                });
            });
            console.log(stores);
            return stores;
        };
        $scope.submitWastageEntry = function(formWastageEntry) {
            if ($state.params.id == undefined) {
                $scope.checkWsatageEntryForOpenTransactionAndSave();
                $scope.wastageEntryForm.enableSubmit = true;
                if (formWastageEntry.$valid) {
                    //calculateAmtinPrefferedAndBaseUnit_Wastage();
                    //check for necessary data before submit
                    var str = formatStore_PhysicalStockForm($scope.wastageEntryForm.store);
                    //calculateAmtinPrefferedAndBaseUnit_PhysicalStock();
                    var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.wastageEntryForm.availableItems, $scope.wastageEntryForm.selectedUnits, $scope.stockUnits);
                    var itemToSave = getItemToSave_wastageEntry(itms);
                    var storeToSave = formatStoreForWastage(angular.copy(itemToSave), angular.copy(str));
                    // var itms=$scope.transaction.basePrefferedAnsSelectedUnitsByItems($scope.wastageEntryForm.availableItems,$scope.wastageEntryForm.selectedUnits);
                    // var itemToSave=formatItemsForInsert_wastageForm(itms);
                    // var storeToSave=formatStoreForWastage(angular.copy(itemToSave));
                    // var index=_.findIndex($scope.stockTransactionTypes,{'Name':$scope.wastageEntryForm.stockTransactionTypes});
                    // var transactionType=$scope.stockTransactionTypes[index].transactionType;
                    if (formWastageEntry.$valid && !$scope.wastageEntryForm.isPrint) {
                        if (itemToSave.length > 0) {
                            if (itemToSave.length < $scope.wastageEntryForm.availableItems.length) {
                                var isGood = confirm("Some Items are Escaped from Physical Entry ! Are You Confirm?");
                                if (isGood == true) {
                                    $scope.transaction._items = itemToSave;
                                    $scope.transaction._store = storeToSave;
                                    $scope.transaction.isOpen = false;
                                    //$scope.transaction.wastageItemType=$scope.wastageEntryForm.forEntry.Name;
                                    $scope.transaction.wastageEntryType = $scope.wastageEntryForm.entryType;
                                    $scope.transaction.processTransaction($scope.wastageEntryForm, 4);
                                    $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                        if (results != null) {
                                            manageClosingQty(itemToSave, storeToSave._id, '', 4);
                                            $scope.wastageEntryForm.isPrint = true;
                                            $scope.wastageEntryForm.isEdit = true;
                                            $scope.wastageEntryForm.isSaved = true;
                                            growl.success('Success in Wastage Entry', { ttl: 3000 });
                                            growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                                            $scope.wastageEntryForm.enableSubmit = false;
                                            intercom.registerEvent('StockTransaction');
                                        }
                                    });
                                } else {
                                    $scope.wastageEntryForm.enableSubmit = false;
                                }
                            } else {
                                $scope.transaction._items = itemToSave;
                                $scope.transaction._store = storeToSave;
                                $scope.transaction.isOpen = false;
                                //$scope.transaction.wastageItemType=$scope.wastageEntryForm.forEntry.Name;
                                $scope.transaction.wastageEntryType = $scope.wastageEntryForm.entryType;
                                $scope.transaction.processTransaction($scope.wastageEntryForm, 4);
                                $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                    if (results != null) {
                                        manageClosingQty(itemToSave, storeToSave._id, '', 4);
                                        $scope.wastageEntryForm.isPrint = true;
                                        $scope.wastageEntryForm.isEdit = true;
                                        $scope.wastageEntryForm.isSaved = true;
                                        growl.success('Success in Wastage Entry', { ttl: 3000 });
                                        growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                                        $scope.wastageEntryForm.enableSubmit = false;
                                        intercom.registerEvent('StockTransaction');
                                    }
                                });
                            }
                        }
                    } else {
                        printWastageEntry(itemToSave);
                        $scope.wastageEntryForm.enableSubmit = false;
                    }
                    // if($scope.wastageEntryForm.transactionId!=undefined){
                    //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.wastageEntryForm.transactionId});
                    //     var delIndex=_.findIndex($scope.openTrans_Entry,{"transactionId":$scope.iRForm.transactionId});
                    //     $scope.openTrans_Wastage.splice(delIndex,1);
                    // }
                    // else{
                    //     $scope.wastageEntryForm.transactionId=guid();
                    // }

                    // if(formWastageEntry.$valid && !$scope.wastageEntryForm.isPrint){
                    //     $rootScope.db.insert('stockTransaction',
                    //     {
                    //         "transactionId":$scope.wastageEntryForm.transactionId,
                    //         "transactionType":transactionType,
                    //         "daySerialNumber":0,
                    //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                    //         "user":JSON.stringify(currentUser),
                    //         "store":JSON.stringify(angular.copy(storeToSave)),
                    //         "toStore":"",//JSON.stringify(angular.copy(tstore)),
                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                    //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                    //         "receiver":"",
                    //         "cartage":0,//$scope.stockEntryForm.cartage,
                    //         "discount":0,//$scope.stockEntryForm.discount,
                    //         "discountType":"",//$scope.stockEntryForm.discountType,
                    //         "serviceCharge":0,
                    //         "serviceChargeType":"",
                    //         "vat":0,
                    //         "vatType":"",
                    //         "payment":0,
                    //         "wastageItemType":$scope.wastageEntryForm.forEntry.Name,
                    //         "wastageEntryType":$scope.wastageEntryForm.entryType,
                    //         "heading":"",
                    //         "created": new Date($scope.wastageEntryForm.billDate).toISOString(),
                    //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                    //         "templateName":"",
                    //         "deployment_id": currentUser.deployment_id,
                    //         "tenant_id": currentUser.tenant_id,
                    //         "isSynced" : false,
                    //         "syncedOn" : undefined
                    //     }).then(function (result) {
                    //         growl.success('Inserted Successfully', {ttl: 3000});
                    //         growl.success(itemToSave.length+' Items Inserted', {ttl: 3000});
                    //         if(result.transactionId!=undefined){
                    //             //$rootScope.db.del('stockTransaction',{"transactionId":$scope.wastageEntryForm.transactionId});
                    //             var delIndex=_.findIndex($scope.openTrans_Entry,{"transactionId":result.transactionId});
                    //             $scope.openTrans_Wastage.splice(delIndex,1);
                    //         }
                    //          $scope.wastageEntryForm={entryType:'Wastage',hideMenuItem:true,hideRawMaterial:false,enableSubmit:true};
                    //         // $scope.wastageEntryForm.entryFor=[
                    //         //     {Name:'RawMaterial'},{Name:'MenuItem'}
                    //         // ];
                    //         $scope.wastageEntryForm.isSaved=true;
                    //         $scope.wastageEntryForm.isPrint=true;
                    //         $scope.wastageEntryForm.isEdit=true;
                    //     });
                    // }
                    // else
                    // {
                    //     //print
                    //     printWastageEntry(itemToSave);
                    // }
                };
            } else {
                $scope.wastageEntryForm.enableSubmit = false;
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.wastageEntryForm.availableItems, $scope.wastageEntryForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_wastageEntry(itms);
                //var itemToSave=formatItemsForInsert_wastageForm(itms);
                var str = formatStore_PhysicalStockForm($scope.wastageEntryForm.store);
                var storeToSave = formatStoreForWastage(angular.copy(itemToSave), angular.copy(str));
                if (itemToSave.length > 0 && $scope.wastageEntryForm.store != undefined) {
                    getItemsHistory_Physical($scope.wastageEntryForm.availableItems);
                    $scope.stockEditHitory.editHistoryId = guid();
                    $scope.stockEditHitory.transactionId = $scope.wastageEntryForm.transactionId;
                    $scope.stockEditHitory.mainTransactionId = $scope.wastageEntryForm.mainTransactionId;
                    $scope.stockEditHitory.billNo = $scope.transaction._transactionNumber;
                    $scope.stockEditHitory.user = formatUser(currentUser);
                    $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                    $scope.stockEditHitory.deployment_id = currentUser.deployment_id;

                    EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                        growl.success('New history created', { ttl: 3000 });
                        if ($state.params != null) {
                            $location.replace().path('/stock/stockEntry');
                        }
                    });


                    var toSave = { _store: storeToSave, _id: $scope.wastageEntryForm.mainTransactionId };
                    StockResource.update({}, toSave, function(bill) {
                        console.log(bill);
                    });
                } else {
                    $scope.wastageEntryForm.enableSubmit = false;
                }

            }
        };

        //-------------------------------------Stock Transfer OPen TRansaction--------------------------------

        $scope.new_stockTransferForm = function() {
            $scope.stockTransferForm.isNew = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, null, null);
            $scope.transaction.generateBillNumbers(5).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber) {
                    $scope.stockTransferForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber
                        // $scope.stockTransferForm.openBillNumber = "Temp/SE -" + snumbers.daySerialNumber
                } else {
                    $scope.stockTransferForm.tempBillNo = "Temp/SE -" + snumbers.billnumber
                        //  $scope.stockTransferForm.openBillNumber = "Temp/SE -" + snumbers.billnumber
                }

                //$scope.stockTransferForm.tempBillNo = "Temp/ST -" + snumbers.billnumber;
                
            });

            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            $scope.stockTransferForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            
        };

        function addItemTransfer(item, storeName_To) {
            _.forEachRight($scope.stockTransferForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    var asn = [{
                        category: item.toStore.toCategory.categoryName,
                        storeName: item.toStore.storeName
                    }];
                    //item.AssingnedStoreCategory=itm.AssingnedStoreCategory;
                    item.AssingnedStoreCategory = asn;
                    var avQtyIndex = _.findIndex($scope.closingQty, {
                        itemId: item._id,
                        storeId: $scope.stockTransferForm.fromStore._id
                    });
                    if (avQtyIndex >= 0) {
                        item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                        item.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    } else {
                        item.availableStock = parseFloat(0).toFixed(3);
                    }
                    $scope.stockTransferForm.availableItems.push(angular.copy(item));
                    $scope.stockTransferForm.remainingItems.splice(i, 1);
                }
            });
            slectedUnit_stockTransferForm($scope.stockTransferForm.availableItems);
        };


        $scope.ot_stockTransferForm = function (openTransaction, indexxx) {
          console.log("openTransaction",openTransaction)
          $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber);
          $scope.stockTransferForm.showOpenBill = true;
          $scope.stockTransferForm.showBill = false;
          $scope.stockTransferForm.isOpen = true;
          if($scope.resetSerialNumber)
            $scope.stockTransferForm.openBillNumber= "Temp/SE - " + openTransaction.daySerialNumber;
          else
            $scope.stockTransferForm.openBillNumber= "Temp/SE - " + openTransaction._transactionNumber;
          $scope.stockTransferForm.openBillNumber = "Temp/ST - " + openTransaction._transactionNumber; //(indexxx+1);
          $scope.stockTransferForm.transactionId = openTransaction.transactionId;
          var st_index = _.findIndex($scope.stores, {"_id": openTransaction._store._id});
          $scope.stockTransferForm.fromStore = $scope.stores[st_index];

          $scope.bindFromCategory_stockTransferFormNew($scope.stockTransferForm.fromStore);

          var st_to_index = _.findIndex($scope.stockTransferForm.toAvailableStore, {"_id": openTransaction._toStore._id});
          $scope.stockTransferForm.toStore = $scope.stockTransferForm.toAvailableStore[st_to_index];
          bindToCategory_stockTransferForm($scope.stockTransferForm.toStore);
          $scope.stockTransferForm.selectedCategory = [];
          _.forEach(openTransaction._items, function (itm, i) {
            $scope.stockTransferForm.selectedCategory[itm.fromCategory._id] = true;
            addItemTransfer(itm, $scope.stockTransferForm.toStore.storeName);
          });

          selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, $scope.stockTransferForm.toStore);

          $scope.stockTransferForm.isNew = true;
          $scope.stockTransferForm.enableSubmit = false;
        };


        function bindFromCategory_stockTransferForm(fromStore) {
            $scope.stockTransferForm.availableCategory = [];
            $scope.stockTransferForm.availableItems = [];
            $scope.stockTransferForm.remainingItems = [];
            $scope.stockTransferForm.toAvailableStore = [];
            $scope.stockTransferForm.availableTemplate = [];
            _.forEach(fromStore.category, function(c, ii) {
                $scope.stockTransferForm.availableCategory.push(angular.copy(c));
                _.forEach(c.item, function(itm, iii) {
                    var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                    itm["fromCategory"] = varFromCategory;
                    itm.itemType = "RawMaterial";
                    $scope.stockTransferForm.remainingItems.push(angular.copy(itm));
                    getAssingnedStoreCategory(itm, $scope.stockTransferForm.fromStore.storeName);
                });
            });
            var ss = $scope.stores;
            _.forEach(ss, function(s, i) {
                if (s._id != fromStore._id) {
                    $scope.stockTransferForm.toAvailableStore.push(angular.copy(s));
                }
            });
            _.forEach($scope.template_transfer, function(t, i) {
                if (fromStore._id == t.store._id) {
                    $scope.stockTransferForm.availableTemplate.push(angular.copy(t));
                }
            });
        };

        function bindToCategory_stockTransferForm(toStore) {
            if ($scope.stockTransferForm.fromStore._id != toStore._id) {
                $scope.stockTransferForm.toAvailableCategory = [];
                _.forEach(toStore.category, function(c, ii) {
                    $scope.stockTransferForm.toAvailableCategory.push(angular.copy(c));
                });
            } else {
                $scope.stockTransferForm.toStore = {};
                $scope.stockTransferForm.toAvailableCategory = [];
                alert("Not able to select same store. Try other");
            }
            if ($scope.stockTransferForm.availableItems != undefined || $scope.stockTransferForm.availableItems.length != 0) {
                selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, toStore);
            }
        };

        function checkForEntry_transfer() {
            var r = false;
            _.forEach($scope.stockTransferForm.availableItems, function(itm, i) {
                if (r == false) {
                    var qty = parseFloat(itm.qty);
                    var avs = parseFloat(itm.availableStock);
                    if (qty >= 0 && qty <= avs) {
                        r = true;
                    }
                }
            });
            return r;
        };

        $scope.checkStockTransferForOpenTransactionAndSave = function() {
            if ($state.params.id == undefined) {
                var vTransfer = validateStockTransfer($scope.stockTransferForm.availableItems);
                if (vTransfer == true) {
                    var fstore = formatStore_stockTransferForm($scope.stockTransferForm.fromStore);
                    var tstore = formatStore_stockTransferForm($scope.stockTransferForm.toStore);
                    var check = checkForEntry_transfer();
                    if (check == true) {
                        $scope.stockTransferForm.enableSubmit = false;
                        if (tstore != undefined) {
                            //getToAndFromCategoryOfItem_stockTransferForm();
                            // var itms=$scope.transaction.basePrefferedAnsSelectedUnitsByItems($scope.stockTransferForm.availableItems,$scope.stockTransferForm.selectedUnits);
                            // console.log(itms);
                            //var itemToSave=formatItems_stockTransferForm(itms);
                            var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.stockTransferForm.availableItems, $scope.stockTransferForm.selectedUnits, $scope.stockUnits);
                            var itemToSave = formatItems_stockTransferForm(itms);
                            getToStoreOfTransfer(itemToSave);
                            tstore = formatStore_stockTransferForm($scope.stockTransferForm.toStore);
                            fstore = formatStoreForStockTRanfer(angular.copy($scope.stockTransferForm.fromStore), angular.copy(itemToSave)); //formatStore_stockTransferForm($scope.stockTransferForm.fromStore);
                            //console.log(itemToSave);
                            //var str=angular.copy($scope.stockTransferForm.fromStore);

                            //console.log(tstore);
                            //getToAndFromCategoryOfItem_stockTransferForm();
                            //var itmsss=calculateAmtinPrefferedAndBaseUnit_StockTransfer();
                            //console.log('$scope.stockTransferForm.availableItems',angular.copy($scope.stockTransferForm.availableItems))
                            //var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.stockTransferForm.availableItems, $scope.stockTransferForm.selectedUnits, $scope.stockUnits);
                            //console.log(itms)
                            //var itemToSave = formatItems_stockTransferForm(itms);
                            //getToStoreOfTransfer(itemToSave);
                            //fstore = formatStoreForStockTRanfer(angular.copy($scope.stockTransferForm.fromStore),angular.copy(itemToSave));//formatStore_stockTransferForm($scope.stockTransferForm.fromStore);
                            //console.log(itemToSave);
                            //var str=angular.copy($scope.stockTransferForm.fromStore);

                            if (itemToSave.length > 0) {
                                // if($scope.stockTransferForm.transactionId!=undefined){
                                //     $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockTransferForm.transactionId});
                                //     var index=_.findIndex($scope.openTrans_transfer,{"transactionId":$scope.stockTransferForm.transactionId});
                                //     $scope.openTrans_transfer.splice(index,1);
                                //     growl.success('Updating....', {ttl: 3000});
                                // }
                                // else{
                                //     $scope.stockTransferForm.transactionId=guid();
                                // }

                                // $rootScope.db.insert('stockTransaction',
                                //     {
                                //         "transactionId":$scope.stockTransferForm.transactionId,
                                //         "transactionType":"5",
                                //         "daySerialNumber":0,
                                //         "transactionNumber":parseFloat($scope.transaction._transactionNumber),
                                //         "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
                                //         "user":JSON.stringify(currentUser),
                                //         "store":JSON.stringify(angular.copy(fstore)),
                                //         "toStore":JSON.stringify(angular.copy(tstore)),
                                //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                //         "receiver":"",
                                //         "cartage":0,//$scope.stockEntryForm.cartage,
                                //         "discount":0,//$scope.stockEntryForm.discount,
                                //         "discountType":"",//$scope.stockEntryForm.discountType,
                                //         "serviceCharge":0,
                                //         "serviceChargeType":"",
                                //         "vat":0,
                                //         "vatType":"",
                                //         "payment":0,
                                //         "heading":"",
                                //         "wastageItemType":"",
                                //         "wastageEntryType":"",
                                //         "created": "",
                                //         "items": JSON.stringify(itemToSave),// JSON.stringify(this),
                                //         "templateName":"",
                                //         "deployment_id": currentUser.deployment_id,
                                //         "tenant_id": currentUser.tenant_id,
                                //         "isSynced" : false,
                                //         "syncedOn" : undefined
                                //     }).then(function (result) {
                                //         growl.success(itemToSave.length+' Items Transferred Successfully', {ttl: 3000});
                                //         var ot={
                                //             transactionId:$scope.stockTransferForm.transactionId,
                                //             transactionNumber:$scope.transaction._transactionNumber,
                                //             transactionType:"5",
                                //             user:angular.copy(currentUser),
                                //             store:angular.copy(fstore),
                                //             toStore:angular.copy(tstore),
                                //             created: null,
                                //             items: itemToSave,
                                //             deployment_id: currentUser.deployment_id,
                                //             tenant_id: currentUser.tenant_id
                                //         };
                                //         $scope.openTrans_transfer.push(ot);

                                //     });

                                fstore.processedFoodCategory = {};
                                if ($scope.stockTransferForm.transactionId != undefined) {
                                    $scope.transaction._items = itemToSave;
                                    $scope.transaction._store = fstore;
                                    $scope.transaction._toStore = tstore;
                                    $scope.transaction.processTransaction($scope.stockTransferForm, 5);
                                    $scope.transaction.updateTransaction({}, 'update');
                                } else {
                                    $scope.stockTransferForm.transactionId = $scope.transaction._id;
                                    $scope.transaction._items = itemToSave;
                                    $scope.transaction._store = fstore;
                                    $scope.transaction._toStore = tstore;
                                    $scope.transaction.billDate = new Date($scope.stockTransferForm.billDate);
                                    $scope.transaction.processTransaction($scope.stockTransferForm, 4);
                                    $scope.transaction.updateTransaction({}, 'insert');
                                }
                            } else {
                                growl.error('Enter full row', { ttl: 3000 });
                                $scope.stockTransferForm.enableSubmit = true;
                            }
                        } else {
                            growl.error('Please select tostore first', { ttl: 3000 });
                            $scope.stockTransferForm.enableSubmit = true;
                        }
                    } else {
                        $scope.stockTransferForm.enableSubmit = true;
                        growl.error('Qty must lesser than equals availableStock', { ttl: 3000 });
                    }
                } else {
                    $scope.stockTransferForm.enableSubmit = true;
                    growl.error('Entry rows are either filled or unfilled.', { ttl: 3000 });
                }
            } else {

            }
        };

        function printStockTransfer(itemToSave) {
            var table = "";
            table += "<table class='table table-curved' id='printTable'>";
            table += "<thead><th>Item Code</th><th>Item</th><th>Qty</th><th>Unit</th></thead>";
            table += "<tbody>";

            _.forEach(itemToSave, function(item, i) {
                table += "<tr>";
                if (!item.itemCode)
                    table += "<td>-</td>";
                else
                    table += "<td>" + item.itemCode + "</td>";
                table += "<td>" + item.itemName + "</td>";
                table += "<td>" + item.qty + "</td>";
                table += "<td>" + item.calculateInUnits[2].unitName + "</td>";
                table += "</tr>";
            });
            table += "</tbody>";
            table += "</table>";
            var printer = window.open('', '', 'width=600,height=600');
            printer.document.open("text/html");
            printer.document.write(table);
            printer.document.close();
            printer.focus();
            printer.print();
        };

        //--------------------------------------Stock Transfer -------------------------------------------//
        $scope.stockTransferForm = { isNew: false, enableSubmit: true, maxDate: new Date(), billDate: new Date() };
        $scope.clearStockTransferTab = function() {
            $scope.stockTransferForm = {
                isNew: false,
                enableSubmit: true,
                showBill: true,
                billDate: new Date($rootScope.serverTime.serverTime),
                maxDate: new Date($rootScope.serverTime.serverTime),
            };
            var op = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, null);
            op.getOpenTransactions(5).then(function(results) {
                console.log(results);
                $scope.openTrans_transfer = results;
            });
            op.getTemplates(105).then(function(results) {
                $scope.template_transfer = results;
            });
            if ($state.params != null) {
                $location.replace().path('/stock/stockEntry');
            }
        };
        $scope.tempBill_stockTransferForm = function(billDate) {
            $scope.stockTransferForm.showBill = true;
            $scope.stockTransferForm.showOpenBill = false;
            $scope.stockTransferForm.tempBillNo = "Temp/ST -" + $scope.transaction._transactionNumber;
            var serverDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate($rootScope.serverTime.serverTime))));
            var selectedDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(billDate))));
            var d = serverDate.getTime() - selectedDate.getTime()
            var di = new Date(d)
            var hours = Math.abs(serverDate - selectedDate) / 36e5;
            if (hours >= 72) {
                growl.success('Back Date Entry has been restricted to 48 Hours.', { ttl: 3000 });

                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                $scope.stockTransferForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            }
        };

        $scope.bindFromCategory_stockTransferForm = function(fromStore) {
            console.log("fromStore",angular.copy(fromStore) )
            $scope.stockTransferForm.availableCategory = [];
            $scope.stockTransferForm.availableItems = [];
            $scope.stockTransferForm.remainingItems = [];
            $scope.stockTransferForm.toAvailableStore = [];
            $scope.stockTransferForm.availableTemplate = [];

            if (fromStore != null) {
                var items = angular.copy($scope.menuRecipes);
                //var items = $scope.menuRecipes;
                //var processedCat = {_id: "PosistTech111111", categoryName: "Processed Food", item: []};

                // _.forEach(items, function (ii, i) {
                //   ii.itemName = ii.recipeName;
                //   ii.preferedUnit = ii.selectedUnitId._id;
                //   ii.AssingnedStoreCategory = [];
                //   var varFromCategory = {"_id": processedCat._id, "categoryName": processedCat.categoryName};
                //   ii["fromCategory"] = varFromCategory;
                //   ii.itemType = "MenuItem";
                //   if (ii.units == undefined) {
                //     ii.units = [{_id: "9980", unitName: "Pack"}];
                //   }
                //   var ss = $scope.stores;
                //   _.forEach(ss, function (str, sti) {
                //     var sc = {"storeName": str.storeName, "category": "Processed Food"};
                //     ii.AssingnedStoreCategory.push(sc);
                //   });
                //   processedCat.item.push(angular.copy(ii));
                //   if (fromStore.isKitchen == true) {
                //     $scope.stockTransferForm.remainingItems.push(angular.copy(ii));
                //   }
                // });

                // if (fromStore.isKitchen == true) {
                //   $scope.stockTransferForm.availableCategory.push(processedCat);
                // }
                var ir = angular.copy($scope.intermediateRecipes);
                var ir = $scope.intermediateRecipes;
                var processedSemi_Cat = { _id: "PosistTech222222", categoryName: "InterMediate Food", item: [] };
                _.forEach(ir, function(ii, i) {
                    ii.itemName = ii.recipeName;
                    ii.preferedUnit = ii.selectedUnitId._id;
                    ii.AssingnedStoreCategory = [];
                    var varFromCategory = { "_id": processedSemi_Cat._id, "categoryName": processedSemi_Cat.categoryName };
                    ii["fromCategory"] = varFromCategory;
                    ii.itemType = "IntermediateItem";
                    if (ii.units == undefined) {
                        ii.units = [{ _id: "9980AZ", unitName: "Pack" }];
                    }
                    var ss = $scope.stores;
                    _.forEach(ss, function(str, sti) {
                        var sc = { "storeName": str.storeName, "category": "InterMediate Food" };
                        ii.AssingnedStoreCategory.push(sc);
                    });
                    if (!ii.itemCode)
                        ii.itemCode = '';
                    processedSemi_Cat.item.push(angular.copy(ii));
                    $scope.stockTransferForm.remainingItems.push(angular.copy(ii));
                });

                 var bItems = [];
                var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
                var group=_.groupBy($scope.baseKitchenItems, function(b) { return b.itemId})
                 //console.log("group",group)
                 
                 var bkItems=[]
                  _.forEach(group,function(g){
                    // console.log("g",g)
                    var c=0
                      _.forEach(g,function(itm){
                         if(c==0)
                          bkItems.push(itm)  
                        c++
                      })
                    })
                  _.forEach(bkItems, function (bItem) {
                      var item = {
                      itemName: bItem.itemName,
                      _id: bItem.itemId,
                      itemId: bItem.itemId,
                      itemType: "Base Kitchen Item",
                      fromCategory: angular.copy(baseKitchenItemsCat),
                      units: bItem.units,
                      preferedUnit: bItem.preferedUnit,
                      itemCode: ''
                     }


                     bItems.push(item);
                     // console.log("closing",$scope.closingQty)
                     
                      var avQtyIndex = _.findIndex($scope.closingQty, {
                      itemId: bItem.itemId,
                      storeId: $scope.stockTransferForm.fromStore._id
                      });
                      if (avQtyIndex >= 0) {
                        item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                        item.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                      }
                      else {
                        item.availableStock = parseFloat(0).toFixed(3);
                      }
                      if (!item.itemCode)
                        item.itemCode = '';
                      $scope.stockTransferForm.remainingItems.push(angular.copy(item));
                      });
                       baseKitchenItemsCat.item=[]
                       baseKitchenItemsCat.item=bItems
                      
                      $scope.stockTransferForm.availableCategory.push(baseKitchenItemsCat);
                     $scope.stockTransferForm.availableCategory.push(processedSemi_Cat);

                _.forEach(fromStore.category, function(c, ii) {
                    $scope.stockTransferForm.availableCategory.push(c);
                    _.forEach(c.item, function(itm, iii) {
                        var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                        itm["fromCategory"] = varFromCategory;
                        itm.itemType = "RawMaterial";
                        if (!itm.itemCode)
                            itm.itemCode = '';
                        var avQtyIndex = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: fromStore._id });
                        if (avQtyIndex >= 0) {
                            itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                            itm.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                        } else {
                            itm.availableStock = parseFloat(0).toFixed(3);
                        }
                        $scope.stockTransferForm.remainingItems.push(angular.copy(itm));
                        getAssingnedStoreCategory(itm, $scope.stockTransferForm.fromStore.storeName);
                    });
                });
                var ss = $scope.stores;
                _.forEach(ss, function(s, i) {
                    if (s._id != fromStore._id) {
                        $scope.stockTransferForm.toAvailableStore.push(angular.copy(s));
                    }
                });

                _.forEach($scope.template_transfer, function(t, i) {
                    if (fromStore._id == t._store._id) {
                        //$scope.stockTransferForm.availableTemplate.push(angular.copy(t));
                        $scope.stockTransferForm.availableTemplate.push(t);
                    }
                });
            }
        };
        $scope.bindFromCategory_stockTransferFormNew = function (fromStore) {
  
          $scope.stockTransferForm.availableCategory = [];
          $scope.stockTransferForm.availableItems = [];
          $scope.stockTransferForm.remainingItems = [];
          $scope.stockTransferForm.toAvailableStore = [];
          $scope.stockTransferForm.availableTemplate = [];
          
            var bItems = [];
             var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
             var bkItems=[]
             var group=_.groupBy($scope.baseKitchenItems, function(b) { return b.itemId})
             // console.log("group",group)
             
             var bkItems=[]
              _.forEach(group,function(g){
                // console.log("g",g)
                var c=0
                _.forEach(g,function(itm){
                   if(c==0)
                    bkItems.push(itm)  
                  c++
                })
              })
              _.forEach(bkItems, function (bItem) {
                  var item = {
                  itemName: bItem.itemName,
                  _id: bItem.itemId,
                  itemId: bItem.itemId,
                  itemType: "Base Kitchen Item",
                  fromCategory: angular.copy(baseKitchenItemsCat),
                  units: bItem.units,
                  preferedUnit: bItem.preferedUnit,
                  itemCode: ''
                 }


                 bItems.push(item);
                 // console.log("closing",$scope.closingQty)
                 
                  var avQtyIndex = _.findIndex($scope.closingQty, {
                  itemId: bItem.itemId,
                  storeId: $scope.stockTransferForm.fromStore._id
                  });
                  if (avQtyIndex >= 0) {
                    item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    item.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                  }
                  else {
                    item.availableStock = parseFloat(0).toFixed(3);
                  }
                   if (!item.itemCode)
                    item.itemCode = '';
                  $scope.stockTransferForm.remainingItems.push(angular.copy(item));
              });
             baseKitchenItemsCat.item=[]
             baseKitchenItemsCat.item=bItems

          
          if (fromStore != null) {
            var items=angular.copy($scope.menuRecipes);
            //var items = $scope.menuRecipes;
            //var processedCat = {_id: "PosistTech111111", categoryName: "Processed Food", item: []};

            // _.forEach(items, function (ii, i) {
            //   ii.itemName = ii.recipeName;
            //   ii.preferedUnit = ii.selectedUnitId._id;
            //   ii.AssingnedStoreCategory = [];
            //   var varFromCategory = {"_id": processedCat._id, "categoryName": processedCat.categoryName};
            //   ii["fromCategory"] = varFromCategory;
            //   ii.itemType = "MenuItem";
            //   if (ii.units == undefined) {
            //     ii.units = [{_id: "9980", unitName: "Pack"}];
            //   }
            //   var ss = $scope.stores;
            //   _.forEach(ss, function (str, sti) {
            //     var sc = {"storeName": str.storeName, "category": "Processed Food"};
            //     ii.AssingnedStoreCategory.push(sc);
            //   });
            //   processedCat.item.push(angular.copy(ii));
            //   if (fromStore.isKitchen == true) {
            //     $scope.stockTransferForm.remainingItems.push(angular.copy(ii));
            //   }
            // });

            // if (fromStore.isKitchen == true) {
            //   $scope.stockTransferForm.availableCategory.push(processedCat);
            // }
            var ir=angular.copy($scope.intermediateRecipes);
            var ir = $scope.intermediateRecipes;
            var processedSemi_Cat = {_id: "PosistTech222222", categoryName: "InterMediate Food", item: []};
            _.forEach(ir, function (ii, i) {
              ii.itemName = ii.recipeName;
              ii.preferedUnit = ii.selectedUnitId._id;
              ii.AssingnedStoreCategory = [];
              var varFromCategory = {"_id": processedSemi_Cat._id, "categoryName": processedSemi_Cat.categoryName};
              ii["fromCategory"] = varFromCategory;
              ii.itemType = "IntermediateItem";
              if (ii.units == undefined) {
                ii.units = [{_id: "9980AZ", unitName: "Pack"}];
              }
              var ss = $scope.stores;
              _.forEach(ss, function (str, sti) {
                var sc = {"storeName": str.storeName, "category": "InterMediate Food"};
                ii.AssingnedStoreCategory.push(sc);
              });
              processedSemi_Cat.item.push(angular.copy(ii));



              $scope.stockTransferForm.remainingItems.push(angular.copy(ii));
            });

            // var bItems = [];
            //  var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
            //  console.log("bk item",$scope.baseKitchenItems)
            //   _.forEach($scope.baseKitchenItems, function (bItem) {
            //       var item = {
            //       itemName: bItem.itemName,
            //       _id: bItem.itemId,
            //       itemId: bItem.itemId,
            //       itemType: "Base Kitchen Item",
            //       fromCategory: angular.copy(baseKitchenItemsCat),
            //       units: bItem.units,
            //       preferedUnit: bItem.preferedUnit
            //      }


            //      bItems.push(item);
            //      // console.log("closing",$scope.closingQty)
            //      console.log("item",item)
            //       var avQtyIndex = _.findIndex($scope.closingQty, {
            //       itemId: bItem.itemId,
            //       storeId: $scope.stockTransferForm.fromStore._id
            //       });
            //       if (avQtyIndex >= 0) {
            //         item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
            //         item.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
            //       }
            //       else {
            //         item.availableStock = parseFloat(0).toFixed(3);
            //       }
            //       $scope.stockTransferForm.remainingItems.push(angular.copy(item));
            //   });
            //  baseKitchenItemsCat.item=[]
            //  baseKitchenItemsCat.item=bItems
            $scope.stockTransferForm.availableCategory.push(processedSemi_Cat);
            // $scope.stockTransferForm.availableCategory.push(baseKitchenItemsCat);

            _.forEach(fromStore.category, function (c, ii) {
              
              $scope.stockTransferForm.availableCategory.push(c);
              _.forEach(c.item, function (itm, iii) {
                var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
                itm["fromCategory"] = varFromCategory;
                itm.itemType = "RawMaterial";
                var avQtyIndex = _.findIndex($scope.closingQty, {itemId: itm._id, storeId: fromStore._id});
                if (avQtyIndex >= 0) {
                  itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                  itm.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                }
                else {
                  itm.availableStock = parseFloat(0).toFixed(3);
                }
                $scope.stockTransferForm.remainingItems.push(angular.copy(itm));
                // if(itm.fromCategory.categoryName == "Base Kitchen Items")
                  // getAssingnedStoreCategoryBase(itm, $scope.stockTransferForm.fromStore.storeName);
                // else
                  getAssingnedStoreCategory(itm, $scope.stockTransferForm.fromStore.storeName);
              });
            });
            var ss = $scope.stores;
            _.forEach(ss, function (s, i) {
              if (s._id != fromStore._id) {
                {
                  
                  s.category.push(baseKitchenItemsCat)
                  $scope.stockTransferForm.toAvailableStore.push(angular.copy(s));

                }
              }
            });

            _.forEach($scope.template_transfer, function (t, i) {
              if (fromStore._id == t._store._id) {
                //$scope.stockTransferForm.availableTemplate.push(angular.copy(t));
                $scope.stockTransferForm.availableTemplate.push(t);
              }
            });
          }
        };



        $scope.bindToCategory_stockTransferForm = function (toStore) {
            console.log("toStore",toStore)
            if (toStore != null) {
              
              if ($scope.stockTransferForm.fromStore.storeName != toStore.storeName) {
                   console.log($scope.stockTransferForm.toAvailableCategory)
         
                  $scope.stockTransferForm.toAvailableCategory = [];
                   //$scope.stockTransferForm.toAvailableCategory.unshift(angular.copy(baseKitchenItemsCat) )
                   
                  _.forEach(toStore.category, function (c, ii) {
                    $scope.stockTransferForm.toAvailableCategory.push(angular.copy(c));
                  });

                  //var processedCat = {_id: "PosistTech111111", categoryName: "Processed Food", item: []};
                  var items = $scope.menuRecipes;
                  // _.forEach(items, function (ii, i) {
                  //   ii.itemName = ii.recipeName;
                  //   ii.preferedUnit = ii.selectedUnitId._id;
                  //   ii.itemType = "MenuItem";
                  //   if (ii.units == undefined) {
                  //     ii.units = [{_id: "9980", unitName: "Pack"}];
                  //   }
                  //   processedCat.item.push(angular.copy(ii));
                  // });

                  // if (toStore.isKitchen == true) {
                  //   toStore.category.push(processedCat);
                  //   $scope.stockTransferForm.toAvailableCategory.push(processedCat);
                  // }
                  // else {
                  //   var index = _.findIndex($scope.stockTransferForm.availableCategory, {_id: "PosistTech111111"});
                  //   if (index >= 0) {
                  //     $scope.stockTransferForm.availableCategory.splice(index, 1);
                  //   }
                  // }
                  //var ir=angular.copy($scope.intermediateRecipes);
                  var processedSemi_Cat = {_id: "PosistTech222222", categoryName: "InterMediate Food", item: []};
                  var ir = $scope.intermediateRecipes;
                  _.forEach(ir, function (ii, i) {
                    ii.itemName = ii.recipeName;
                    ii.preferedUnit = ii.selectedUnitId._id;
                    ii.itemType = "IntermediateItem";
                    if (ii.units == undefined) {
                      ii.units = [{_id: "9980AZ", unitName: "Pack"}];
                    }
                    processedSemi_Cat.item.push(angular.copy(ii));
                  });

                    var bItems = [];
                    var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
                    var group=_.groupBy($scope.baseKitchenItems, function(b) { return b.itemId})
                     //console.log("group",group)
                     
                     var bkItems=[]
                      _.forEach(group,function(g){
                        // console.log("g",g)
                        var c=0
                          _.forEach(g,function(itm){
                             if(c==0)
                              bkItems.push(itm)  
                            c++
                          })
                        })
                  _.forEach(bkItems, function (bItem) {
                    var item = {
                    itemName: bItem.itemName,
                    _id: bItem.itemId,
                    itemId: bItem.itemId,
                    itemType: "Base Kitchen Item",
                    fromCategory: angular.copy(baseKitchenItemsCat),
                    units: bItem.units,
                    preferedUnit: bItem.preferedUnit,
                    itemCode: ''
                   }
                    bItems.push(item);
                     //$scope.physicalStockForm.remainingItems.push(angular.copy(item));
                  });
                  baseKitchenItemsCat.item=[]
                   baseKitchenItemsCat.item=bItems

                  
                  // var baseKitchenCategoryPresent=false
                  // _.forEach(toStore.category,function(cat){
                  //     if(cat.categoryName == "Base Kitchen Items")
                  //       baseKitchenCategoryPresent=true
                  // })
                   // if(baseKitchenCategoryPresent==false)
                    toStore.category.push(baseKitchenItemsCat);
                    toStore.category.push(processedSemi_Cat);
                    // if(baseKitchenCategoryPresent==false)
                    $scope.stockTransferForm.toAvailableCategory.push(baseKitchenItemsCat);
                    $scope.stockTransferForm.toAvailableCategory.push(processedSemi_Cat);
                }
                else {
                  $scope.stockTransferForm.toStore = {};
                  $scope.stockTransferForm.toAvailableCategory = [];
                  alert("Not able to select same store. Try other");
                }
                if ($scope.stockTransferForm.availableItems != undefined || $scope.stockTransferForm.availableItems.length != 0) {
                  selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, toStore);
                }
              }
              else {
                $scope.stockTransferForm.toStoreCategory = {};
                $scope.stockTransferForm.toAvailableCategory = [];
                $scope.stockTransferForm.disable_toStoreCategory = {};
              }
        };

        $scope.bindToCategory_stockTransferFormNew = function (toStore) {
            if (toStore != null) {
              
              if ($scope.stockTransferForm.fromStore.storeName != toStore.storeName) {
                   console.log($scope.stockTransferForm.toAvailableCategory)
         
                  $scope.stockTransferForm.toAvailableCategory = [];
                   //$scope.stockTransferForm.toAvailableCategory.unshift(angular.copy(baseKitchenItemsCat) )
                   
                  _.forEach(toStore.category, function (c, ii) {
                    $scope.stockTransferForm.toAvailableCategory.push(angular.copy(c));
                  });

                  //var processedCat = {_id: "PosistTech111111", categoryName: "Processed Food", item: []};
                  var items = $scope.menuRecipes;
                  // _.forEach(items, function (ii, i) {
                  //   ii.itemName = ii.recipeName;
                  //   ii.preferedUnit = ii.selectedUnitId._id;
                  //   ii.itemType = "MenuItem";
                  //   if (ii.units == undefined) {
                  //     ii.units = [{_id: "9980", unitName: "Pack"}];
                  //   }
                  //   processedCat.item.push(angular.copy(ii));
                  // });

                  // if (toStore.isKitchen == true) {
                  //   toStore.category.push(processedCat);
                  //   $scope.stockTransferForm.toAvailableCategory.push(processedCat);
                  // }
                  // else {
                  //   var index = _.findIndex($scope.stockTransferForm.availableCategory, {_id: "PosistTech111111"});
                  //   if (index >= 0) {
                  //     $scope.stockTransferForm.availableCategory.splice(index, 1);
                  //   }
                  // }
                  //var ir=angular.copy($scope.intermediateRecipes);
                  var processedSemi_Cat = {_id: "PosistTech222222", categoryName: "InterMediate Food", item: []};
                  var ir = $scope.intermediateRecipes;
                  _.forEach(ir, function (ii, i) {
                    ii.itemName = ii.recipeName;
                    ii.preferedUnit = ii.selectedUnitId._id;
                    ii.itemType = "IntermediateItem";
                    if (ii.units == undefined) {
                      ii.units = [{_id: "9980AZ", unitName: "Pack"}];
                    }
                    processedSemi_Cat.item.push(angular.copy(ii));
                  });

                  var bItems = [];
                    var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
                    var group=_.groupBy($scope.baseKitchenItems, function(b) { return b.itemId})
                     //console.log("group",group)
                     
                     var bkItems=[]
                      _.forEach(group,function(g){
                        // console.log("g",g)
                        var c=0
                          _.forEach(g,function(itm){
                             if(c==0)
                              bkItems.push(itm)  
                            c++
                          })
                        })
                  _.forEach(bkItems, function (bItem) {
                    var item = {
                    itemName: bItem.itemName,
                    _id: bItem.itemId,
                    itemId: bItem.itemId,
                    itemType: "Base Kitchen Item",
                    fromCategory: angular.copy(baseKitchenItemsCat),
                    units: bItem.units,
                    preferedUnit: bItem.preferedUnit,
                    itemCode: ''
                   }
                    bItems.push(item);
                     $scope.stockTransferForm.remainingItems.push(angular.copy(item));
                  });
                  baseKitchenItemsCat.item=[]
                   baseKitchenItemsCat.item=bItems

                  
                  // var baseKitchenCategoryPresent=false
                  // _.forEach(toStore.category,function(cat){
                  //     if(cat.categoryName == "Base Kitchen Items")
                  //       baseKitchenCategoryPresent=true
                  // })
                   // if(baseKitchenCategoryPresent==false)
                    //toStore.category.push(baseKitchenItemsCat);
                    //toStore.category.push(processedSemi_Cat);
                    // if(baseKitchenCategoryPresent==false)
                    //$scope.stockTransferForm.toAvailableCategory.push(baseKitchenItemsCat);
                    //$scope.stockTransferForm.toAvailableCategory.push(processedSemi_Cat);
                }
                else {
                  $scope.stockTransferForm.toStore = {};
                  $scope.stockTransferForm.toAvailableCategory = [];
                  alert("Not able to select same store. Try other");
                }
                if ($scope.stockTransferForm.availableItems != undefined || $scope.stockTransferForm.availableItems.length != 0) {
                  selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, toStore);
                }
              }
              else {
                $scope.stockTransferForm.toStoreCategory = {};
                $scope.stockTransferForm.toAvailableCategory = [];
                $scope.stockTransferForm.disable_toStoreCategory = {};
              }
        };


        $scope.bindItems_stockTransferFormTemplate = function(templateId) {
            if ($scope.stockTransferForm.availableItems == undefined) {
                $scope.stockTransferForm.availableItems = [];
            }
            if ($scope.stockTransferForm.selectedCategory == undefined) {
                $scope.stockTransferForm.selectedCategory = {};
            }

            _.forEach($scope.stockTransferForm.availableTemplate, function(t, i) {
                if (t._id == templateId) {
                    var tS = _.findIndex($scope.stockTransferForm.toAvailableStore, { _id: t._toStore._id });
                    $scope.stockTransferForm.toStore = $scope.stockTransferForm.toAvailableStore[tS];
                    $scope.bindToCategory_stockTransferForm($scope.stockTransferForm.toStore);
                    _.forEach(t._items, function(itm, ii) {
                        var aa = _.findIndex($scope.stockTransferForm.availableItems, { _id: itm._id });
                        if (aa < 0) {
                            $scope.stockTransferForm.availableItems.push(angular.copy(itm));
                            getAssingnedStoreCategory(itm, $scope.stockTransferForm.fromStore.storeName);

                            var index = _.findIndex($scope.stockTransferForm.remainingItems, { _id: itm._id });
                            $scope.stockTransferForm.remainingItems.splice(index, 1);

                            var cS = itm.fromCategory._id;
                            $scope.stockTransferForm.selectedCategory[cS] = true;

                            var indd = _.findIndex($scope.stockTransferForm.toAvailableCategory, { _id: itm.toStore.toCategory._id });
                            $scope.stockTransferForm.toStoreCategory[itm._id] = $scope.stockTransferForm.toAvailableCategory[indd];

                            $scope.stockTransferForm.disable_toStoreCategory[itm._id] = true;
                        } else {
                            $scope.stockTransferForm.availableItems.splice(aa, 1);
                            $scope.stockTransferForm.availableItems.push(angular.copy(itm));
                        }
                    });
                }

            });

            slectedUnit_stockTransferForm($scope.stockTransferForm.availableItems);
            //selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems,$scope.stockTransferForm.toStore);
        };

        $scope.bindItems_stockTransferForm = function (catId) {
          if ($scope.stockTransferForm.availableItems == undefined) {
            $scope.stockTransferForm.availableItems = [];
          }
          //console.log("$scope.stockTransferForm.availableCategory",$scope.stockTransferForm.availableCategory)
          _.forEach($scope.stockTransferForm.availableCategory, function (c, i) {
            if ($scope.stockTransferForm.selectedCategory[catId] == true) {
              if (c._id == catId) {
                _.forEach(c.item, function (itm, ii) {
                  
                  var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
                  itm["fromCategory"] = varFromCategory;
                  if (c.categoryName == "InterMediate Food") {
                    itm.itemType = "IntermediateItem";
                  }
                  else {
                    itm.itemType = "RawMaterial";
                  }
                  var aa = _.findIndex($scope.stockTransferForm.availableItems, {_id: itm._id});
                  if (aa < 0) {
                    var avQtyIndex = _.findIndex($scope.closingQty, {
                      itemId: itm._id,
                      storeId: $scope.stockTransferForm.fromStore._id
                    });
                    if (avQtyIndex >= 0) {
                      itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                      itm.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    }
                    else {
                      itm.availableStock = parseFloat(0).toFixed(3);
                    }
                    if(itm.fromCategory.categoryName == "Base Kitchen Items")
                      getAssingnedStoreCategoryBase(itm, $scope.stockTransferForm.fromStore.storeName);
                    else
                      getAssingnedStoreCategory(itm, $scope.stockTransferForm.fromStore.storeName);
                    $scope.stockTransferForm.availableItems.push(angular.copy(itm));
                    //console.log("itmmm",itm);
                    //$scope.stockTransferForm.availableItems.push(itm);
                    var index = _.findIndex($scope.stockTransferForm.remainingItems, {_id: itm._id});
                    $scope.stockTransferForm.remainingItems.splice(index, 1);
                  }
                });
              }
            }
            else {
              if (c._id == catId) {
                _.forEach(c.item, function (itm, ii) {
                  var index = _.findIndex($scope.stockTransferForm.availableItems, {_id: itm._id});
                  if (index >= 0) {
                    $scope.stockTransferForm.availableItems.splice(index, 1);
                    var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
                    itm["fromCategory"] = varFromCategory;
                    var avQtyIndex = _.findIndex($scope.closingQty, {
                      itemId: itm._id,
                      storeId: $scope.stockTransferForm.fromStore._id
                    });
                    if (avQtyIndex >= 0) {
                      itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                      itm.availableStock = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    }
                    else {
                      itm.availableStock = parseFloat(0).toFixed(3);
                    }
                     if(itm.fromCategory.categoryName == "Base Kitchen Items")
                      getAssingnedStoreCategoryBase(itm, $scope.stockTransferForm.fromStore.storeName);
                    else
                      getAssingnedStoreCategory(itm, $scope.stockTransferForm.fromStore.storeName);
                    itm.qty = "";
                    $scope.stockTransferForm.remainingItems.push(angular.copy(itm));
                  }
                });
              }
            }
          });
          slectedUnit_stockTransferForm($scope.stockTransferForm.availableItems);
          
          selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, $scope.stockTransferForm.toStore);

        };

        $scope.addRemainingItem_stockTransferForm = function ($item, $model, $label) {

          var remaining = angular.copy($scope.stockTransferForm.remainingItems);
          _.forEach($scope.stockTransferForm.remainingItems, function (itm, i) {
            if (itm._id == $item._id) {
                
              if($item.fromCategory.categoryName == "Base Kitchen Items"){
                
                getAssingnedStoreCategoryBase($item, $scope.stockTransferForm.fromStore.storeName);
                $scope.stockTransferForm.availableItems.unshift(angular.copy(itm));
                if ($scope.stockTransferForm.selectedCategory == undefined) {
                  $scope.stockTransferForm.selectedCategory = {};
                }
                $scope.stockTransferForm.selectedCategory[itm.fromCategory._id] = true;
                remaining.splice(i, 1);
                $scope.stockTransferForm.itemToAdd = "";
              }
              else{
                
                getAssingnedStoreCategory(itm, $scope.stockTransferForm.fromStore.storeName);
                console.log("itm after assigned",itm)
                $scope.stockTransferForm.availableItems.unshift(angular.copy(itm));
                if ($scope.stockTransferForm.selectedCategory == undefined) {
                  $scope.stockTransferForm.selectedCategory = {};
                }
                $scope.stockTransferForm.selectedCategory[itm.fromCategory._id] = true;

                remaining.splice(i, 1);
                $scope.stockTransferForm.itemToAdd = "";
              }
            }
          });
          $scope.stockTransferForm.remainingItems = remaining;
          
          slectedUnit_stockTransferForm($scope.stockTransferForm.availableItems);
          if($item.fromCategory.categoryName == "Base Kitchen Items")
          {
            

            selectedToCategory_stockTransferFormBase($scope.stockTransferForm.availableItems, $scope.stockTransferForm.toStore);
          }
          else
            selectedToCategory_stockTransferForm($scope.stockTransferForm.availableItems, $scope.stockTransferForm.toStore);
          console.log("$scope.stockTransferForm.availableItems",$scope.stockTransferForm.availableItems)
        };

        function getAssingnedStoreCategory(item, storeName) {

          _.forEach($scope.stores, function (s, i) {
            if (s.storeName != storeName) {

              _.forEach(s.category, function (c, ii) {
                
                var index = _.findIndex(c.item, {'_id': item._id});
                if (index >= 0) {
                  if (item.AssingnedStoreCategory == undefined) {
                    item.AssingnedStoreCategory = [];
                  }
                  //console.log("s.storeName=" + s.storeName + " " + i);
                  var sc = {"storeName": s.storeName, "category": c.categoryName};
                  item.AssingnedStoreCategory.push(sc);
                }
              })
            }
          });
        };

        function getAssingnedStoreCategoryBase(item, storeName) {
          //var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
          _.forEach($scope.stores, function (s, i) {
            if (s.storeName != storeName) {
            //  s.category.push(baseKitchenItemsCat)
              _.forEach(s.category, function (c, ii) {
                  
                
                  if (item.AssingnedStoreCategory == undefined) {
                    item.AssingnedStoreCategory = [];
                  }
                  //console.log("s.storeName=" + s.storeName + " " + i);
                  var sc = {"storeName": s.storeName, "category": c.categoryName};
                  item.AssingnedStoreCategory.push(sc);
                  // console.log("sc",sc)
                
              })
            }
          });
        };



        function slectedUnit_stockTransferForm(item) {
          $scope.stockTransferForm.selectedUnits = {};
          _.forEach(item, function (itm, i) {
            _.forEach(itm.units, function (u, ii) {
              if (itm.selectedUnit != undefined) {
                if (itm.selectedUnit._id == u._id) {
                  $scope.stockTransferForm.selectedUnits[itm._id] = u;
                  if (itm.availableStock != 0) {
                    itm.availableStock = parseFloat(parseFloat(itm.closingQtyInBase) / parseFloat(u.conversionFactor)).toFixed(3);
                  }
                }
              }
              else{
                  if(itm.preferedUnit!=undefined){
                      if(itm.preferedUnit==u._id){
                          $scope.stockTransferForm.selectedUnits[itm._id]=u;
                          if(itm.availableStock!=0){
                              itm.availableStock=parseFloat(parseFloat(itm.closingQtyInBase)/parseFloat(u.conversionFactor)).toFixed(3);
                          }
                      }
                  }
              }
            });
          });
        };

        function selectedToCategory_stockTransferFormBase(items, toStore) {
          console.log("base")
          $scope.stockTransferForm.toStoreCategory = {};
          $scope.stockTransferForm.disable_toStoreCategory = {};
          if (toStore != undefined) {
            if (toStore.length != 0) {
              _.forEach(items, function (item, j) {

                _.forEach(item.AssingnedStoreCategory, function (asc, i) {
                  if (asc.storeName == toStore.storeName) {
                      _.forEach(toStore.category, function (c, ii) {
                        
                      //if($scope.stockTransferForm.disable_toStoreCategory[item._id]==false){
                     if (c.categoryName == asc.category) {
                        $scope.stockTransferForm.disable_toStoreCategory[item._id] = "true";
                        var index = _.findIndex($scope.stockTransferForm.toAvailableCategory, {_id: c._id});
                        //$scope.stockTransferForm.toStoreCategory[item._id]=c;
                        var baseKitchenItemsCat = {_id: "PosistTech333333", categoryName: "Base Kitchen Items"};
                        
                        if(item.fromCategory.categoryName == "Base Kitchen Items")
                        {
                          //console.log($scope.stockTransferForm.toAvailableCategory)
                          _.forEach($scope.stockTransferForm.toAvailableCategory,function(toAvailableCat){
                               if(toAvailableCat.categoryName == "Base Kitchen Items")
                                  {
                                    $scope.stockTransferForm.toStoreCategory[item._id] = toAvailableCat;
                                     
                                  }
                          })
                          
                         
                        }
                        else
                            $scope.stockTransferForm.toStoreCategory[item._id] = $scope.stockTransferForm.toAvailableCategory[index];
                        
                        // var itm_index=_.findIndex($scope.stockTransferForm.availableItems,{_id:item._id});
                        // var toStore_item={"_id":toStore._id,"storeName":toStore.storeName};
                        // toStore_item.toStoreCategory={_id:$scope.stockTransferForm.toAvailableCategory[index]._id,categoryName:$scope.stockTransferForm.toAvailableCategory[index].categoryName};
                        // $scope.stockTransferForm.availableItems[itm_index].toStore=toStore_item;
                        // console.log(toStore_item);
                      }
                      // else
                      // {
                      //     $scope.stockTransferForm.disable_toStoreCategory[item._id]="false";
                      // }
                      // }
                      // else
                      // {

                      // }
                    });
                   
                  }
                });
              });
            }
          }
        };

        function selectedToCategory_stockTransferForm(items, toStore) {
          $scope.stockTransferForm.toStoreCategory = {};
          $scope.stockTransferForm.disable_toStoreCategory = {};
          if (toStore != undefined) {
            if (toStore.length != 0) {
              _.forEach(items, function (item, j) {
                 
                  // console.log("itm",item)
                _.forEach(item.AssingnedStoreCategory, function (asc, i) {
                  if (asc.storeName == toStore.storeName) {
                   
                    // console.log("selecteed cat raw item toStore",toStore)

                    _.forEach(toStore.category, function (c, ii) {
                      //if($scope.stockTransferForm.disable_toStoreCategory[item._id]==false){
                      
                      if (c.categoryName == asc.category) {
                       
                         item.matched=true
                        $scope.stockTransferForm.disable_toStoreCategory[item._id] = "true";
                        var index = _.findIndex($scope.stockTransferForm.toAvailableCategory, {_id: c._id});
                        //$scope.stockTransferForm.toStoreCategory[item._id]=c;
                        if(item.fromCategory.categoryName == "Base Kitchen Items")
                          {
                             _.forEach($scope.stockTransferForm.toAvailableCategory,function(toAvailableCat){
                               if(toAvailableCat.categoryName == "Base Kitchen Items")
                                  {
                                    $scope.stockTransferForm.toStoreCategory[item._id] = toAvailableCat;
                                     //console.log("toStore",$scope.stockTransferForm.toStoreCategory)
                                  }
                            })
                          }
                        else
                          $scope.stockTransferForm.toStoreCategory[item._id] = $scope.stockTransferForm.toAvailableCategory[index];
                        // console.log("toStore",$scope.stockTransferForm.toStoreCategory)
                        // var itm_index=_.findIndex($scope.stockTransferForm.availableItems,{_id:item._id});
                        // var toStore_item={"_id":toStore._id,"storeName":toStore.storeName};
                        // toStore_item.toStoreCategory={_id:$scope.stockTransferForm.toAvailableCategory[index]._id,categoryName:$scope.stockTransferForm.toAvailableCategory[index].categoryName};
                        // $scope.stockTransferForm.availableItems[itm_index].toStore=toStore_item;
                        // console.log(toStore_item);
                      }
                    
                      // else
                      // {
                      //     $scope.stockTransferForm.disable_toStoreCategory[item._id]="false";
                      // }
                      // }
                      // else
                      // {

                      // }
                    });
                  }
                });
                // console.log("final item",item)
                // if(item.matched==undefined)
                //   {
                //      if(item.fromCategory.categoryName != "Base Kitchen Items")
                //        _.forEach($scope.stockTransferForm.toAvailableCategory,function(toAvailableCat,i){
                //         console.log("available cat",toAvailableCat)
                //               if(toAvailableCat!= undefined)
                //                if(toAvailableCat.categoryName == "Base Kitchen Items")
                //                   {
                //                     $scope.stockTransferForm.toAvailableCategory.splice(i,1)
                //                   }
                //          })  
                //   }
              });
            }
          }
        };


        
        $scope.goNext_stockTransferForm = function(nextIdx) {
            var f = angular.element(document.querySelector('#t_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                $scope.stockTransferForm.enableSubmit = false;
                angular.element(document.querySelector('#submitStockTransfer')).focus();
            }
        };

        function formatStore_stockTransferForm(store) {
            //var stores=angular.copy(store);
            // for(var p in store){
            //     if(p!="_id" && p!="storeName"  && p!="storeLocation" && p!="storeUID"){
            //         delete stores[p];
            //     }
            // }
            var obj = {
                _id: store._id,
                storeName: store.storeName,
                storeLocation: store.storeLocation,
                storeUID: store.storeUID
            };
            return obj;
            //return stores;
        };

        function formatCategory_stockTransferForm(cat) {
            // var c=angular.copy(cat);
            // for(var p in c){
            //     if(p!="_id" && p!="categoryName"){
            //         delete c[p];
            //     }
            // }
            // return c;
            var obj = { _id: cat._id, categoryName: cat.categoryName };
            return obj;
        };

        function formatItems_stockTransferForm(items) {
            //var itm=angular.copy(items);
            var itm = [];

            _.forEachRight(items, function(itmm, i) {
                if ($scope.stockTransferForm.selectedUnits[itmm._id] != undefined && $scope.stockTransferForm.toStoreCategory[itmm._id] != undefined && $scope.stockTransferForm.toStoreCategory[itmm._id] != null) {
                    var flag = false;

                    if (itmm.qty != "" && itmm.qty != undefined && itmm.units != null && itmm.fromCategory != null && itmm.fromCategory != undefined) {
                        //&& itmm.toCategory!=null && itmm.toCategory!=undefined
                        if (itmm.itemType != "RawMaterial") {
                            //var ss=$scope.transaction.calculateForProcessedAndSemiProcessed_sale(itmm);
                            var ss = $scope.transaction.calculateForRecipeDetails(itmm);
                            itmm = ss;
                        }
                        flag = true;
                        itm.push(angular.copy(itmm));
                    }
                    // if(flag==false){
                    //     itm.splice(i,1);
                    // }
                }
                // else
                // {
                //     itm.splice(i,1);
                // }
            });

            _.forEach(itm, function(it) {
                for (var p in it) {
                    //console.log("p",p)

                    if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "itemType" && p != "rawItems" && p != "receipeDetails" && p != "isSemiProcessed" && p != "quantity" && p != "calculateInUnits" && p != "assignedToStore" && p != "units" && p != "qty" && p != "fromCategory" && p != "toStore" && p != "selectedUnit" && p != "price" && p != "itemCode") {
                        //&& p!="preferedUnit" && p!="selectedUnit"
                        _.forEach(it.units, function(u) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                                    delete u[p];
                                }
                            }
                        });
                        delete it[p];
                    }
                }
            });

            return itm;
        };

        function getToAndFromCategoryOfItem_stockTransferForm() {
            _.forEach($scope.stockTransferForm.selectedCategory, function(sc, i) {
                //console.log(sc +" =sc , i= "+i);
                if (sc == true) {
                    _.forEach($scope.stockTransferForm.availableCategory, function(ac, ii) {
                        if (ac._id == i) {
                            //_.forEach($scope.stockTransferForm.availableItems, function(itm,iii){
                            _.forEach(ac.item, function(itm, iii) {
                                //if(itm.qty!="" && itm.qty!=undefined){
                                _.forEach($scope.stockTransferForm.toStoreCategory, function(tc, t) {
                                    if (itm._id == t) {
                                        //console.log(itm.itemName);

                                        var tostore = {
                                            "_id": $scope.stockTransferForm.toStore._id,
                                            "storeName": $scope.stockTransferForm.toStore.storeName,
                                            "storeUID": $scope.stockTransferForm.toStore.storeUID
                                        };
                                        var varToCategory = { "_id": tc._id, "categoryName": tc.categoryName };
                                        var varFromCategory = { "_id": ac._id, "categoryName": ac.categoryName };
                                        itm["fromCategory"] = varFromCategory;
                                        itm["toCategory"] = varToCategory;
                                        tostore["toCategory"] = varToCategory;
                                        itm["toStore"] = tostore;
                                        _.forEach($scope.stockTransferForm.selectedUnits, function(su, u) {
                                            if (itm._id == u) {
                                                var selectedUnit = {
                                                    "_id": su._id,
                                                    "unitName": su.unitName,
                                                    "conversionFactor": su.conversionFactor,
                                                    "type": "selectedUnit",
                                                    "baseQty": itm.qty
                                                };
                                                itm["selectedUnit"] = selectedUnit;
                                            }
                                        });

                                    }
                                });
                                //}
                            });
                        }
                    });
                }
            });
            /*_.forEach($scope.stockTransferForm.toStoreCategory, function(tc, t) {

                console.log(t);
            });*/
        };

        $scope.deleteSelectedItemTransferForm = function(item) {
            var index = _.findIndex($scope.stockTransferForm.availableItems, { _id: item._id });
            $scope.stockTransferForm.availableItems.splice(index, 1);
            item.qty = "";
            $scope.stockTransferForm.remainingItems.push(item);
        };

        $scope.resetStockTransferTab = function() {

            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            var newBillDate = serverD.setSeconds(serverD.getSeconds() + sec)
            if (!$scope.stockTransferForm.isEdit) {
                $scope.stockTransferForm = {
                    isNew: true,
                    enableSubmit: true,
                    showBill: true,
                    billDate: new Date(newBillDate),
                    maxDate: new Date(newBillDate),
                };
                //$scope.stockTransferForm.tempBillNo="Temp/ST -"+ $scope.transaction._transactionNumber;
                $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, null, null);
                $scope.transaction.generateBillNumbers(5).then(function(snumbers) {
                    $scope.transaction._transactionNumber = snumbers.billnumber;
                    $scope.transaction.transactionNumber = snumbers.billnumber;
                    $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                    //$scope.stockTransferForm.tempBillNo = "Temp/ST -" + snumbers.billnumber;
                    if ($scope.resetSerialNumber)
                        $scope.stockTransferForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                    else
                        $scope.stockTransferForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                });
                
            } else {
                //edit
                $scope.stockTransferForm.isSaved = false;
                $scope.stockTransferForm.isPrint = false;
                $scope.stockTransferForm.isEdit = false;
            }
        };

        $scope.newReset_Transfer = function() {
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000
            var newBillDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            $scope.stockTransferForm = {
                isNew: true,
                enableSubmit: true,
                showBill: true,
                billDate: new Date(newBillDate),
                maxDate: new Date(newBillDate),
            };
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, null, null);
            $scope.transaction.generateBillNumbers(5).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.stockTransferForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                else
                    $scope.stockTransferForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                //  $scope.stockTransferForm.tempBillNo = "Temp/ST -" + snumbers.billnumber;
            });
        };

        $scope.checkQtyTransfer = function(qty, itemId) {
            var index = _.findIndex($scope.stockTransferForm.availableItems, { _id: itemId });
            if (isNaN(qty)) {
                $scope.stockTransferForm.availableItems[index].qty = "";
            } else {
                if (parseFloat(qty) < 0) {
                    growl.error('Qty must not be negative', { ttl: 3000 });
                    $scope.stockTransferForm.availableItems[index].qty = "";
                } else if (parseFloat(qty) > parseFloat($scope.stockTransferForm.availableItems[index].availableStock)) {
                    $scope.stockTransferForm.availableItems[index].qty = "";
                    growl.error('Qty must not be greater than availableStock', { ttl: 3000 });
                } else {
                    $scope.enableSubmitButton_Transfer($scope.stockTransferForm.availableItems);
                }
            }
        };

        function formatStoreForStockTRanfer(store, item) {
              var stores = store;
              stores.category = [];
              _.forEach(item, function (itm, i) {
                var index = _.findIndex(stores.category, {_id: itm.fromCategory._id});
                if (index < 0) {
                  stores.category.push(itm.fromCategory);
                }
              });

              _.forEach(stores.category, function (c, i) {
                c.items = [];
                _.forEachRight(item, function (itm, ii) {
                  if (c._id == itm.fromCategory._id) {
                    
                    console.log("itm",itm)
                    if(itm.fromCategory.categoryName != "Base Kitchen Items" && itm.toStore.toCategory.categoryName == "Base Kitchen Items")
                    {
                      
                        //growl.error("Raw Material cammot be transferred to Base Kitchen Item Category",{ttl:3000})
                        cannotAssignBaseCategory=true
                       
                    }
                    else
                    {
                    c.items.push(itm);
                    item.splice(ii, 1);
                  }
                   delete itm.fromCategory;
                  }
                });
            });
            return stores;
        };

        function calculateAmtinPrefferedAndBaseUnit_StockTransfer() {
            var items = $scope.stockTransferForm.availableItems;
            _.forEach(items, function(item, i) {
                var conversionFactor = 1;
                var selectedConversionFactor = 1;
                _.forEach($scope.stockTransferForm.selectedUnits, function(u, ii) {
                    if (ii == item._id) {
                        selectedConversionFactor = u.conversionFactor;
                    }
                });
                var baseUnit = {};
                if (item.units[0].baseUnit != undefined) {
                    baseUnit = item.units[0].baseUnit;
                    baseUnit.unitName = baseUnit.name;
                } else {
                    var ind = _.findIndex(item.units, { "type": "baseUnit" });
                    baseUnit = item.units[ind];
                    baseUnit.unitName = baseUnit.name;
                }

                var baseQty = parseFloat(item.qty * selectedConversionFactor);
                baseUnit.baseQty = baseQty;
                baseUnit.type = "baseUnit";
                baseUnit.conversionFactor = conversionFactor;
                item.baseUnit = baseUnit;

                var pInd = _.findIndex(item.units, { _id: item.preferedUnit });
                if (pInd >= 0) {
                    var preferedUnits = angular.copy(item.units[pInd]);
                    var preferedconversionFactor = item.units[pInd].conversionFactor;
                    if (preferedconversionFactor == conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
                    } else if (preferedconversionFactor > conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
                    } else if (preferedconversionFactor < conversionFactor) {
                        preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
                    }
                    preferedUnits.type = "preferedUnit";
                    delete preferedUnits.baseUnit;
                    preferedUnits.conversionFactor = preferedconversionFactor;
                    item.preferedUnits = preferedUnits;
                }
                _.forEach($scope.stockTransferForm.selectedUnits, function(u, ii) {
                    if (ii == item._id && item.qty != "" && item.qty != undefined && u != null) {
                        var selectedUnit = {
                            "_id": u._id,
                            "unitName": u.unitName,
                            "baseQty": item.qty,
                            "type": "selectedUnit",
                            "conversionFactor": u.conversionFactor
                        };
                        item["selectedUnit"] = selectedUnit;
                        if (item.preferedUnits == undefined) {
                            var pUnit = {
                                "_id": u._id,
                                "unitName": u.unitName,
                                "baseQty": item.qty,
                                "type": "preferedUnit",
                                "conversionFactor": u.conversionFactor
                            };
                            item["preferedUnits"] = pUnit;
                        }
                    }
                });
                item.calculateInUnits = [];
                item.calculateInUnits.push(item.baseUnit);
                item.calculateInUnits.push(item.preferedUnits);
                item.calculateInUnits.push(item.selectedUnit);
            });
            return items;
        };

        function getToStoreOfTransfer(itemToSave) {
            _.forEach(itemToSave, function(itm, i) {
                var tstore = formatStore_stockTransferForm($scope.stockTransferForm.toStore);
                itm.toStore = tstore;
                if ($scope.stockTransferForm.toStoreCategory[itm._id] != undefined) {
                    _.forEach($scope.stockTransferForm.toStoreCategory, function(cat, i) {
                        if (itm._id == i) {
                            itm.toStore.toCategory = { _id: cat._id, "categoryName": cat.categoryName };
                        }
                    });
                } else {
                    $scope.stockTransferForm.enableSubmit = true;
                    growl.error('Please select a category', { ttl: 3000 });
                }
            });
        };

       
         $scope.submitStockTransfer = function (formStockTransfer) {
          cannotAssignBaseCategory=false
          if ($state.params.id == undefined) {
            $scope.checkStockTransferForOpenTransactionAndSave();
            $scope.stockTransferForm.enableSubmit = true;
            var tstore = formatStore_stockTransferForm($scope.stockTransferForm.toStore);
            //console.log(tstore);
            //getToAndFromCategoryOfItem_stockTransferForm();
            //var itmsss=calculateAmtinPrefferedAndBaseUnit_StockTransfer();
            //console.log('$scope.stockTransferForm.availableItems',angular.copy($scope.stockTransferForm.availableItems))
            var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.stockTransferForm.availableItems, $scope.stockTransferForm.selectedUnits, $scope.stockUnits);
            //console.log(itms)
            var itemToSave = formatItems_stockTransferForm(itms);
            getToStoreOfTransfer(itemToSave);
            var fstore = formatStoreForStockTRanfer(angular.copy($scope.stockTransferForm.fromStore),angular.copy(itemToSave));//formatStore_stockTransferForm($scope.stockTransferForm.fromStore);
            if(cannotAssignBaseCategory==false)
            {
            console.log("fromStore",fstore);
            var str=angular.copy($scope.stockTransferForm.fromStore);

            /* if(!fstore.category){
              console.log('-------------------2----------------------');
              var stores = str;
              stores.category = [];
              _.forEach(itemToSave, function (itm, i) {
                var index = _.findIndex(stores.category, {_id: itm.fromCategory._id});
                if (index < 0) {
                  stores.category.push(angular.copy(itm.fromCategory));
                }
              });
              _.forEach(stores.category, function (c, i) {
                c.items = [];
                _.forEachRight(itemToSave, function (itm, ii) {
                  if (c._id == itm.fromCategory._id) {
                    delete itm.fromCategory;
                    c.items.push(itm);
                    item.splice(ii, 1);
                  }
                });
              });

              fstore=angular.copy(stores)
              console.log("fstore",fstore);
            } else if(fstore.category.length == 0) {
              console.log('-------------------3----------------------');
              var stores = str;
              stores.category = [];
              _.forEach(itemToSave, function (itm, i) {
                var index = _.findIndex(stores.category, {_id: itm.fromCategory._id});
                if (index < 0) {
                  stores.category.push(angular.copy(itm.fromCategory));
                }
              });
              _.forEach(stores.category, function (c, i) {
                c.items = [];
                _.forEachRight(itemToSave, function (itm, ii) {
                  if (c._id == itm.fromCategory._id) {
                    delete itm.fromCategory;
                    c.items.push(itm);
                    item.splice(ii, 1);
                  }
                });
              });
              //console.log("checks");
              fstore=angular.copy(stores)
            }*/
            if (formStockTransfer.$valid && !$scope.stockTransferForm.isPrint) {
              if (itemToSave.length > 0) {
                if (itemToSave.length < $scope.stockTransferForm.availableItems.length) {
                  var isGood = confirm("Some Items have Escaped from Physical Entry ! Are You Sure?");
                  if (isGood == true) {
                    fstore.processedFoodCategory={};
                    $scope.transaction.isOpen = false;
                    if ($scope.stockTransferForm.transactionId != undefined) {
                      
                      var ite=calculatePrice(itemToSave)
                      $scope.transaction._items = ite;
                      var st=calculatePriceStr(fstore)
                      $scope.transaction._store = st;
                      console.log("$scope.transaction._store",$scope.transaction._store)
                      $scope.transaction._toStore = tstore;
                      $scope.transaction.processTransaction($scope.stockTransferForm, 5);
                      $scope.transaction.updateTransaction({}, 'update').then(function (results) {
                        if (results != null) {
                          manageClosingQty(itemToSave, fstore._id, tstore._id, 5);
                          $scope.stockTransferForm.isPrint = true;
                          $scope.stockTransferForm.isEdit = true;
                          $scope.stockTransferForm.isSaved = true;
                          var splicedItem=deleteBaseKitchenItem(angular.copy(itemToSave))
                          checkAndAddItemsToStoreIfNotPresent(angular.copy(splicedItem));
                          growl.success('Success in Stock Transfer', {ttl: 3000});
                          growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                          $scope.stockTransferForm.enableSubmit = false;
                          intercom.registerEvent('StockTransaction');
                        }
                      });
                    }
                    else {
                      fstore.processedFoodCategory={};
                      $scope.stockTransferForm.transactionId = $scope.transaction._id;
                      
                      var ite=calculatePrice(itemToSave)
                      $scope.transaction._items = ite;
                      var st=calculatePriceStr(fstore)
                      $scope.transaction._store = st;
                      console.log("$scope.transaction._store",$scope.transaction._store)
                      $scope.transaction._toStore = tstore;
                      $scope.transaction.billDate = new Date($scope.stockTransferForm.billDate);
                      $scope.transaction.processTransaction($scope.stockTransferForm, 4);
                      $scope.transaction.updateTransaction({}, 'insert').then(function (results) {
                        if (results != null) {
                          manageClosingQty(itemToSave, fstore._id, tstore._id, 5);
                          $scope.stockTransferForm.isPrint = true;
                          $scope.stockTransferForm.isEdit = true;
                          $scope.stockTransferForm.isSaved = true;
                          var splicedItem=deleteBaseKitchenItem(angular.copy(itemToSave))
                          checkAndAddItemsToStoreIfNotPresent(angular.copy(splicedItem));
                          growl.success('Success in Stock Transfer', {ttl: 3000});
                          growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                          $scope.stockTransferForm.enableSubmit = false;
                          intercom.registerEvent('StockTransaction');
                        }
                      });
                    }
                  }
                  else {
                    $scope.stockTransferForm.enableSubmit = false;
                  }
                }
                else {
                  fstore.processedFoodCategory={};
                  $scope.transaction.isOpen = false;
                  if ($scope.stockTransferForm.transactionId != undefined) {
                    
                    var ite=calculatePrice(itemToSave)
                    $scope.transaction._items = ite;
                    var st=calculatePriceStr(fstore)
                    $scope.transaction._store = st;
                    console.log("$scope.transaction._store",$scope.transaction._store)
                    $scope.transaction._toStore = tstore;
                    $scope.transaction.processTransaction($scope.stockTransferForm, 5);
                    $scope.transaction.updateTransaction({}, 'update').then(function (results) {
                      if (results != null) {
                        manageClosingQty(itemToSave, fstore._id, tstore._id, 5);
                        $scope.stockTransferForm.isPrint = true;
                        $scope.stockTransferForm.isEdit = true;
                        $scope.stockTransferForm.isSaved = true;
                        var splicedItem=deleteBaseKitchenItem(angular.copy(itemToSave))
                        checkAndAddItemsToStoreIfNotPresent(angular.copy(splicedItem));
                        growl.success('Success in Stock Transfer', {ttl: 3000});
                        growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                        $scope.stockTransferForm.enableSubmit = false;
                        intercom.registerEvent('StockTransaction');
                      }
                    });
                  }
                  else {
                    fstore.processedFoodCategory={};
                    $scope.stockTransferForm.transactionId = $scope.transaction._id;
                    
                    var ite=calculatePrice(itemToSave)
                    $scope.transaction._items = ite;
                    var st=calculatePriceStr(fstore)
                    $scope.transaction._store = st;
                    console.log("$scope.transaction._store",$scope.transaction._store)
                    $scope.transaction._toStore = tstore;
                    $scope.transaction.billDate = new Date($scope.stockTransferForm.billDate);
                    $scope.transaction.processTransaction($scope.stockTransferForm, 4);
                    $scope.transaction.updateTransaction({}, 'insert').then(function (results) {
                      if (results != null) {
                        manageClosingQty(itemToSave, fstore._id, tstore._id, 5);
                        $scope.stockTransferForm.isPrint = true;
                        $scope.stockTransferForm.isEdit = true;
                        $scope.stockTransferForm.isSaved = true;
                        console.log("itemTosave",angular.copy(itemToSave))
                        var splicedItem=deleteBaseKitchenItem(angular.copy(itemToSave))
                        checkAndAddItemsToStoreIfNotPresent(angular.copy(splicedItem));
                        growl.success('Success in Stock Transfer', {ttl: 3000});
                        growl.success(itemToSave.length + 'Items added', {ttl: 3000});
                        $scope.stockTransferForm.enableSubmit = false;
                        intercom.registerEvent('StockTransaction');
                      }
                    });
                  }
                }
              }
              else {
                growl.error('No Items present for entry', {ttl: 3000});
                $scope.stockTransferForm.enableSubmit = true;
              }
            }
            else {
              printStockTransfer(itemToSave);
              $scope.stockTransferForm.enableSubmit = false;
            }

            //    if($scope.stockTransferForm.transactionId!=undefined){
            //        $rootScope.db.del('stockTransaction',{"transactionId":$scope.stockTransferForm.transactionId});
            //        var index=_.findIndex($scope.openTrans_transfer,{"transactionId":$scope.stockTransferForm.transactionId});
            //        $scope.openTrans_transfer.splice(index,1);
            //    }
            //    else{
            //        $scope.stockTransferForm.transactionId=guid();
            //    }

            // if(formStockTransfer.$valid && !$scope.stockTransferForm.isPrint){
            //    $rootScope.db.insert('stockTransaction',
            //        {
            //            "transactionId":$scope.stockTransferForm.transactionId,
            //            "transactionType":"5",
            //            "daySerialNumber":0,
            //            "transactionNumber":parseFloat($scope.transaction._transactionNumber),
            //            "tempTransactionNumber":parseFloat($scope.transaction._transactionNumber),
            //            "user":JSON.stringify(currentUser),
            //            "store":JSON.stringify(angular.copy(fstore)),
            //            "toStore":JSON.stringify(angular.copy(tstore)),
            //            "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
            //            "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
            //            "receiver":"",
            //            "cartage":0,//$scope.stockEntryForm.cartage,
            //            "discount":0,//$scope.stockEntryForm.discount,
            //            "discountType":"",//$scope.stockEntryForm.discountType,
            //            "serviceCharge":0,
            //            "serviceChargeType":"",
            //            "vat":0,
            //            "vatType":"",
            //            "payment":0,
            //            "heading":"",
            //            "wastageItemType":"",
            //            "wastageEntryType":"",
            //            "created": new Date($scope.stockTransferForm.billDate).toISOString(),
            //            "items": JSON.stringify(itemToSave),// JSON.stringify(this),
            //            "templateName":"",
            //            "deployment_id": currentUser.deployment_id,
            //            "tenant_id": currentUser.tenant_id,
            //            "isSynced" : false,
            //            "syncedOn" : undefined
            //        }).then(function (result) {
            //            growl.success(itemToSave.length+' Items Transferred Successfully', {ttl: 3000});
            //            //$scope.stockTransferForm={enableSubmit:true};
            //            if(result.transactionId!=undefined){
            //                //$rootScope.db.del('stockTransaction',{"transactionId":$scope.stockTransferForm.transactionId});
            //                var index=_.findIndex($scope.openTrans_transfer,{"transactionId":result.transactionId});
            //                $scope.openTrans_transfer.splice(index,1);
            //            }
            //            $scope.stockTransferForm.isSaved=true;
            //            $scope.stockTransferForm.isPrint=true;
            //            $scope.stockTransferForm.isEdit=true;
            //            checkAndAddItemsToStoreIfNotPresent(angular.copy(itemToSave));
            //        });
            // }
            // else{
            //    //print
            //    printStockTransfer(itemToSave);
            // }

          }
          else
          {
            growl.error("Cannot Transfer Base kitchen item to Otrher category",{ttl:3000})
          }
          }
          else {
            $scope.stockTransferForm.enableSubmit = true;
            //console.log($scope.stockTransferForm.availableItems);
            var tstore = formatStore_stockTransferForm($scope.stockTransferForm.toStore);
            console.log(tstore);
            var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.stockTransferForm.availableItems, $scope.stockTransferForm.selectedUnits,$scope.stockUnits);
            //console.log(itms)
            var itemToSave = formatItems_stockTransferForm(itms);
            getToStoreOfTransfer(itemToSave);
            var fstore = formatStoreForStockTRanfer(angular.copy($scope.stockTransferForm.fromStore),angular.copy(itemToSave));//formatStore_stockTransferForm($scope.stockTransferForm.fromStore);
            var ite=calculatePrice(itemToSave)
            itemToSave={}
            itemToSave = angular.copy(ite);
            var st=calculatePriceStr(fstore)
            fstore={}
            fstore = angular.copy(st);
            //console.log(itemToSave);
            if (itemToSave.length > 0 && $scope.stockTransferForm.fromStore != undefined && $scope.stockTransferForm.toStore != undefined) {
              getItemsHistory_Physical($scope.stockTransferForm.availableItems);
              $scope.stockEditHitory.editHistoryId = guid();
              $scope.stockEditHitory.transactionId = $scope.stockTransferForm.transactionId;
              $scope.stockEditHitory.mainTransactionId = $scope.stockTransferForm.mainTransactionId;
              $scope.stockEditHitory.billNo = $scope.stockTransferForm.tempBillNo;
              $scope.stockEditHitory.user = formatUser(currentUser);
              $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
              $scope.stockEditHitory.deployment_id = currentUser.deployment_id;

              EditTransactionHistory.saveData({}, $scope.stockEditHitory, function (result) {
                growl.success('New history created', {ttl: 3000});
                if ($state.params != null) {
                  $location.replace().path('/stock/stockEntry');
                }
              });


              if (fstore.category.length > 0) {
                var toSave = {_store: fstore, _id: $scope.stockTransferForm.mainTransactionId};
                toSave._store.processedFoodCategory={}
                StockResource.update({}, toSave, function (bill) {
                  console.log(bill);

                });
              }
            }
            else {
              $scope.stockTransferForm.enableSubmit = false;
            }
          }

        };

        function deleteBaseKitchenItem(item){
            console.log("delete bk item ",angular.copy(item) )

            _.forEach(item,function(it,i){
                if(it)
                if(it.itemType == "Base Kitchen Item")
                    item.splice(i,1)
            })
            console.log("itm",angular.copy(item) )
            return item
        }


        function calculatePrice(data){
           
          var itm=angular.copy(data);
          console.log("price itm",itm)
          _.forEach(itm, function (it) {
            if(it.isSemiProcessed==true)
              it.itemPrice=parseFloat( it.price * it.selectedUnit.conversionFactor).toFixed(2) 
             else if(it.itemType == "Base Kitchen Item")
              {

                var l_index = _.findIndex($scope.itemLastAndAveragePriceBasekitchen, {itemId: it._id});  
                 // console.log("price",it)
                 // console.log("index",l_index)
                  if (l_index >= 0)
                  it.itemPrice=parseFloat( it.selectedUnit.conversionFactor * $scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage).toFixed(2)
                else
                  it.itemPrice=parseFloat( 0).toFixed(2)
              }
              else
              {
                var l_index = _.findIndex($scope.itemLastAndAveragePrice, {itemId: it._id});
                 if (l_index >= 0)
                  it.itemPrice=parseFloat( it.selectedUnit.conversionFactor * $scope.itemLastAndAveragePrice[l_index].monthAverage).toFixed(2)
                else
                  it.itemPrice=parseFloat( 0).toFixed(2)
              }
              
            
           
           
            
          });
          //console.log("item",angular.copy(it))
          return itm;

        }

        function calculatePriceStr(str){
          // console.log("str",angular.copy(str) )
            _.forEach(str.category, function (cat) {
              _.forEach(cat.items, function (item) {
                // console.log("price strr",item)
                  
                if(item.isSemiProcessed==true)
                  item.itemPrice=parseFloat( item.price* item.selectedUnit.conversionFactor).toFixed(2)
                else if(item.itemType == "Base Kitchen Item")
                {
                  var l_index = _.findIndex($scope.itemLastAndAveragePriceBasekitchen, {itemId: item._id});  
                 // console.log("index str",l_index)
                 if (l_index >= 0)
                   item.itemPrice=parseFloat( item.selectedUnit.conversionFactor * $scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage).toFixed(2)
                  else
                   item.itemPrice=parseFloat( 0).toFixed(2)
                }
                else
                {
                  var l_index = _.findIndex($scope.itemLastAndAveragePrice, {itemId: item._id});  
                  
                 if (l_index >= 0)
                   item.itemPrice=parseFloat( item.selectedUnit.conversionFactor * $scope.itemLastAndAveragePrice[l_index].monthAverage).toFixed(2)
                  else
                   item.itemPrice=parseFloat( 0).toFixed(2)
                }
                 

            });
            
            });

           
            return str
        }



        function checkAndAddItemsToStoreIfNotPresent(availableItems) {
            _.forEach(availableItems, function(itm, i) {
                //var str=angular.copy($scope.stockTransferForm.toAvailableStore);
                var str = angular.copy($scope.stores);
                _.forEach(str, function(s, ii) {
                    if (itm.toStore._id == s._id) {
                        _.forEach(s.category, function(c, iii) {
                            if (c._id == itm.toStore.toCategory._id) {
                                var index = _.findIndex(c.item, { _id: itm._id });
                                if (index < 0) {
                                    //add to category
                                    if (itm.assignedToStore == undefined) {
                                        itm.assignedToStore = [];
                                    };
                                    itm.assignedToStore.push({ _id: $scope.stores[ii]._id, storeName: $scope.stores[ii].storeName });
                                    delete itm.qty;
                                    $scope.stores[ii].category[iii].item.push(itm);
                                    var catToUpdate = angular.copy($scope.stores[ii].category[iii]);
                                    store.update({}, $scope.stores[ii], function(s) {
                                        if (s.error == undefined) {
                                            if (catToUpdate != undefined) {
                                                stockCategory.update({}, catToUpdate, function(cat) {
                                                    if (cat.error == undefined) {
                                                        if (itm.preferedUnit == undefined) {
                                                            for (var key in $scope.stockTransferForm.selectedUnits) {
                                                                if (key == itm._id) {
                                                                    itm.preferedUnit = $scope.stockTransferForm.selectedUnits[key]._id;
                                                                }
                                                            }
                                                        }
                                                        updateVendorsOnItemChange(itm, s, cat);
                                                        for (var key in $scope.stockTransferForm.selectedUnits) {
                                                            if (key == itm._id) {
                                                                itm.selectedUnitId = $scope.stockTransferForm.selectedUnits[key]._id;
                                                            }
                                                        }
                                                        updateReceiverOnItemChange(itm, s, cat);
                                                    }
                                                });

                                                stockItem.update({}, itm, function(it) {

                                                });
                                            };
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            });
        };

        function updateVendorsOnItemChange(item, str, cat) {
            _.forEach($scope.vendors, function(v, i) {
                var flag = false;
                if (v.pricing != undefined) {
                    if (v.pricing._id == str._id) {
                        _.forEach(v.pricing.category, function(c, ii) {
                            if (c._id == cat._id) {
                                var index = _.findIndex(c.item, { _id: item._id });
                                if (index < 0) {
                                    c.item.push(item);
                                    flag = true;
                                }
                            }
                        });
                    }
                }
                if (flag == true) {
                    vendor.update({}, v, function(ven) {
                    });
                }
            });
        };

        function updateReceiverOnItemChange(item, str, cat) {
            _.forEach($scope.receivers, function(r, i) {
                var flag = false;
                if (r.pricing != undefined) {
                    _.forEach(r.pricing.store, function(s, ii) {
                        if (s._id == str._id) {
                            _.forEach(s.category, function(c, iii) {
                                if (c._id == cat._id) {
                                    var index = _.findIndex(c.item, { _id: item._id });
                                    if (index < 0) {
                                        c.item.push(item);
                                        flag = true;
                                    }
                                }
                            });
                        }
                    });
                }
                if (flag == true) {
                    receiver.update({}, r, function(re) {
                    });
                }
            });
        };


        $scope.CheckForFloat_cartage_entry = function(text) {
            if (isNaN(text)) {
                $scope.stockEntryForm.cartage = undefined;
            }
        };
        $scope.CheckForFloat_cartage_sale = function(text) {
            if (isNaN(text)) {
                $scope.stockSaleForm.cartage = undefined;
            }
        };
        $scope.CheckForFloat_Discount_entry = function(text) {
            if (isNaN(text)) {
                $scope.stockEntryForm.discount = undefined;
            }
        };
        $scope.CheckForFloat_Discount_sale = function(text) {
            if (isNaN(text)) {
                $scope.stockSaleForm.discount = undefined;
            }
        };

        $scope.CheckForFloat_SCharge_sale = function(text) {
            if (isNaN(text)) {
                $scope.stockSaleForm.serviceCharge = undefined;
            }
        };

        $scope.CheckForFloat_Vat_sale = function(text) {
            if (isNaN(text)) {
                $scope.stockSaleForm.vat = undefined;
            }
        };

        $scope.CheckForFloat_Payment_sale = function(text) {
            if (isNaN(text)) {
                $scope.stockSaleForm.payment = undefined;
            }
            $scope.calculateAmt_saleForm();
        };
        //----------------------------------------Templates------------------------------------------------------//

        $scope.createEntryTemplate = function() {
            var availableTemplate = angular.copy($scope.stockEntryForm.availableTemplate);
            var templateType = "Stock Entry Template";
            getFromCategoryOfItem_stockEntryForm();
            var itemToSave = angular.copy($scope.stockEntryForm.availableItems); //getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
            if (itemToSave.length > 0) {
                var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
                var billNo = $scope.stockEntryForm.tempBillNo;
                var discount = $scope.stockEntryForm.discount;
                var discountType = $scope.stockEntryForm.discountType;
                var cartage = ($scope.stockEntryForm.cartage != undefined) ? $scope.stockEntryForm.cartage : 0;
                var vendor = $scope.stockEntryForm.vendor;
                $modal.open({
                    templateUrl: 'app/stock/stockEntry/_entryTemplate.html',
                    controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'growl', function($rootScope, $scope, $resource, $modalInstance, growl) {

                        $scope.cancel = function() {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.entry = {};
                        $scope.entry.templateType = templateType;
                        $scope.entry.itemToSave = itemToSave;
                        _.forEach($scope.entry.itemToSave, function(itm, i) {
                            itm.qty = "";
                            itm.subTotal = "";
                            itm.addedAmt = "";
                            itm.totalAmount = "";
                            itm.vatPercent = "";
                            itm.comment = ""
                        });
                        $scope.entry.storeToSave = storeToSave;
                        $scope.entry.billNo = billNo;
                        $scope.entry.discount = discount;
                        $scope.entry.vendor = vendor;
                        $scope.entry.discountType = discountType;
                        $scope.entry.cartage = cartage;
                        $scope.entry.availableTemplate = availableTemplate;

                        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 101, null, null);
                        $scope.transaction._items = $scope.entry.itemToSave;
                        $scope.transaction._store = $scope.entry.storeToSave;
                        $scope.transaction._vendor = $scope.entry.vendor;
                        $scope.transaction.templateName = $scope.entry.templateName;
                        $scope.transaction.discount = $scope.entry.discount;
                        $scope.transaction.discountType = $scope.entry.discountType;
                        $scope.transaction.cartage = $scope.entry.cartage;
                        //$scope.transaction.processTransaction($scope.stockEntryForm,1);
                        $scope.ok = function() {
                            var indexxx = _.findIndex($scope.entry.availableTemplate, { 'templateName': $scope.entry.templateName });

                            if ($scope.entry.templateName != undefined) {
                                $scope.transaction.templateName = $scope.entry.templateName;
                                if (indexxx < 0) {
                                    //$scope.transaction.templateName=$scope.entry.templateName;
                                    $scope.transaction.isOpen = false;
                                    $scope.transaction.updateTransaction({}, 'insert').then(function(results) {
                                        if (results != null) {
                                            growl.success('New entry template created', { ttl: 3000 });
                                            $modalInstance.dismiss('cancel');
                                        }
                                    });
                                    // $rootScope.db.insert('stockTransaction',
                                    //     {
                                    //         "transactionId":guid(),
                                    //         "transactionType":"101",
                                    //         "daySerialNumber":0,
                                    //         "transactionNumber":0,
                                    //         "user":JSON.stringify(currentUser),
                                    //         "store":JSON.stringify(angular.copy($scope.entry.storeToSave)),
                                    //         "toStore":"",
                                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                    //         "vendor":JSON.stringify($scope.entry.vendor),
                                    //         "receiver":"",
                                    //         "cartage":$scope.entry.cartage=undefined?0:$scope.entry.cartage,
                                    //         "discount":$scope.entry.discount=undefined?0:$scope.entry.discount,
                                    //         "discountType":$scope.entry.discountType,
                                    //         "serviceCharge":0,
                                    //         "serviceChargeType":"",
                                    //         "vat":0,
                                    //         "vatType":"",
                                    //         "payment":0,
                                    //         "heading":"",
                                    //         "wastageItemType":"",
                                    //         "wastageEntryType":"",
                                    //         "created":new Date().toISOString() ,
                                    //         "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                    //         "templateName":$scope.entry.templateName,
                                    //         "deployment_id": currentUser.deployment_id,
                                    //         "tenant_id": currentUser.tenant_id,
                                    //         "isSynced" : false,
                                    //         "syncedOn" : undefined
                                    //     }).then(function (result) {
                                    //         growl.success('New entry template created', {ttl: 3000});
                                    //         $modalInstance.dismiss('cancel');
                                    // });
                                } else {
                                    if (confirm("Template already present ok will replace the existing template") == true) {
                                        $scope.transaction._id = $scope.entry.availableTemplate[indexxx]._id;
                                        $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                            if (results != null) {
                                                growl.success('New entry template Updated', { ttl: 3000 });
                                                $modalInstance.dismiss('cancel');
                                            }
                                        });
                                        // $rootScope.db.update('stockTransaction',
                                        //        {

                                        //            "transactionType":"101",
                                        //            "daySerialNumber":0,
                                        //            "transactionNumber":0,
                                        //            "user":JSON.stringify(currentUser),
                                        //            "store":JSON.stringify(angular.copy($scope.entry.storeToSave)),
                                        //            "toStore":"",
                                        //            "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                        //            "vendor":JSON.stringify($scope.entry.vendor),
                                        //            "receiver":"",
                                        //            "cartage":$scope.entry.cartage=undefined?0:$scope.entry.cartage,
                                        //            "discount":$scope.entry.discount=undefined?0:$scope.entry.discount,
                                        //            "discountType":$scope.entry.discountType,
                                        //            "serviceCharge":0,
                                        //            "serviceChargeType":"",
                                        //            "vat":0,
                                        //            "vatType":"",
                                        //            "payment":0,
                                        //            "heading":"",
                                        //            "wastageItemType":"",
                                        //            "wastageEntryType":"",
                                        //            "created":new Date().toISOString() ,
                                        //            "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                        //            "templateName":$scope.entry.templateName,
                                        //            "deployment_id": currentUser.deployment_id,
                                        //            "tenant_id": currentUser.tenant_id,
                                        //            "isSynced" : false,
                                        //            "syncedOn" : undefined
                                        //        },
                                        //        {
                                        //            "templateName":$scope.entry.templateName
                                        //        }).then(function (result) {
                                        //            growl.success('New entry template created', {ttl: 3000});
                                        //            //growl.success(itemToSave.length+'Items added', {ttl: 3000});
                                        //            //$scope.stockEntryForm={enableSubmit:true,discountType:'percent',isTemplateDelete:true,isTemplateSave:true};
                                        //            //$scope.stockEntryForm.availableItems=[];
                                        //            $modalInstance.dismiss('cancel');
                                        //    });

                                    } else {
                                        //Cancelled
                                    }

                                }
                            } else {
                                growl.success('Provide a template name before OK', { ttl: 3000 });
                            }

                        };
                    }],
                    size: ''
                });
            } else {
                growl.success("No Items Available. Please add at least one item", { ttl: 3000 });
            }
        };

        $scope.createSaleTemplate = function() {
            var templateType = "Stock Sale Template";
            var availableTemplate = angular.copy($scope.stockSaleForm.availableTemplate);
            getFromCategoryOfItem_stockSaleForm();
            var itemToSave = angular.copy($scope.stockSaleForm.availableItems); //getItemToSave_stockSaleForm($scope.stockSaleForm.availableItems);
            if (itemToSave.length > 0) {
                var storeToSave = formatStore_stockEntryForm($scope.stockSaleForm.store);
                var billNo = $scope.stockSaleForm.tempBillNo;
                var cartage = $scope.stockSaleForm.cartage = undefined ? 0 : $scope.stockSaleForm.cartage;
                var discount = $scope.stockSaleForm.discount = undefined ? 0 : $scope.stockSaleForm.discount;
                var discountType = $scope.stockSaleForm.discountType;
                var serviceCharge = $scope.stockSaleForm.serviceCharge = undefined ? 0 : $scope.stockSaleForm.serviceCharge;
                var serviceChargeType = $scope.stockSaleForm.serviceChargeType;
                var vat = $scope.stockSaleForm.vat = undefined ? 0 : $scope.stockSaleForm.vat;
                var vatType = $scope.stockSaleForm.vatType;
                var payment = $scope.stockSaleForm.payment = undefined ? 0 : $scope.stockSaleForm.payment;
                var heading = $scope.stockSaleForm.heading;
                var rec = $scope.stockSaleForm.availableReceiver;
                _.forEach(itemToSave, function(itm, i) {
                    itm.qty = "";
                    itm.subTotal = "";
                    //itm.totalAmount = "";
                    itm.comment = "";
                });
                $modal.open({
                    templateUrl: 'app/stock/stockEntry/_entryTemplate.html',
                    controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'growl', function($rootScope, $scope, $resource, $modalInstance, growl) {

                        $scope.cancel = function() {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.entry = {};
                        $scope.entry.templateType = templateType;
                        $scope.entry.itemToSave = itemToSave;
                        $scope.entry.storeToSave = storeToSave;
                        $scope.entry.billNo = billNo;
                        $scope.entry.cartage = cartage;
                        $scope.entry.discount = discount;
                        $scope.entry.discountType = discountType;
                        $scope.entry.serviceCharge = serviceCharge;
                        $scope.entry.serviceChargeType = serviceChargeType;
                        $scope.entry.vat = vat;
                        $scope.entry.vatType = vatType;
                        $scope.entry.payment = payment;
                        $scope.entry.heading = heading;
                        $scope.entry.availableReceiver = rec;
                        $scope.entry.availableTemplate = availableTemplate;

                        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 102, null, null);
                        $scope.transaction._items = $scope.entry.itemToSave;
                        $scope.transaction._store = $scope.entry.storeToSave;
                        $scope.transaction._receiver = $scope.entry.availableReceiver;
                        $scope.transaction.templateName = $scope.entry.templateName;
                        $scope.transaction.discount = $scope.entry.discount;
                        $scope.transaction.discountType = $scope.entry.discountType;
                        $scope.transaction.cartage = $scope.entry.cartage;
                        $scope.transaction.serviceCharge = $scope.entry.serviceCharge;
                        $scope.transaction.serviceChargeType = $scope.entry.serviceChargeType;
                        $scope.transaction.vat = $scope.entry.vat;
                        $scope.transaction.vatType = $scope.entry.vatType;
                        $scope.transaction.payment = $scope.entry.payment;
                        $scope.transaction.heading = $scope.entry.heading;


                        $scope.ok = function() {
                            var indexxx = _.findIndex($scope.entry.availableTemplate, { 'templateName': $scope.entry.templateName });

                            if ($scope.entry.templateName != undefined) {
                                $scope.transaction.templateName = $scope.entry.templateName;

                                if (indexxx < 0) {
                                    $scope.transaction.isOpen = false;
                                    $scope.transaction.updateTransaction({}, 'insert').then(function(results) {
                                        if (results != null) {
                                            growl.success('New entry template created', { ttl: 3000 });
                                            $modalInstance.dismiss('cancel');
                                        }
                                    });

                                    // $rootScope.db.insert('stockTransaction',
                                    //     {
                                    //         "transactionId":guid(),
                                    //         "transactionType":"102",
                                    //         "daySerialNumber":0,
                                    //         "transactionNumber":0,
                                    //         "user":JSON.stringify(currentUser),
                                    //         "store":JSON.stringify(angular.copy($scope.entry.storeToSave)),
                                    //         "toStore":"",
                                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                    //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                    //         "receiver":JSON.stringify($scope.entry.availableReceiver),
                                    //         "cartage":$scope.entry.cartage,
                                    //         "discount":$scope.entry.discount,
                                    //         "discountType":$scope.entry.discountType,
                                    //         "serviceCharge":$scope.entry.serviceCharge,
                                    //         "serviceChargeType":$scope.entry.serviceChargeType,
                                    //         "vat":$scope.entry.vat,
                                    //         "vatType":$scope.entry.vatType,
                                    //         "payment":$scope.entry.payment,
                                    //         "heading":$scope.entry.heading,
                                    //         "wastageItemType":"",
                                    //         "wastageEntryType":"",
                                    //         "created": new Date().toISOString(),
                                    //         "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                    //         "templateName":$scope.entry.templateName,
                                    //         "deployment_id": currentUser.deployment_id,
                                    //         "tenant_id": currentUser.tenant_id,
                                    //         "isSynced" : false,
                                    //         "syncedOn" : undefined
                                    //     }).then(function (result) {
                                    //         growl.success('New stock Sale Template created', {ttl: 3000});
                                    //         $modalInstance.dismiss('cancel');
                                    // });
                                } else {
                                    if (confirm("Template already present ok will replace the existing template") == true) {
                                        $scope.transaction._id = $scope.entry.availableTemplate[indexxx]._id;
                                        $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                            if (results != null) {
                                                growl.success('New entry template Updated', { ttl: 3000 });
                                                $modalInstance.dismiss('cancel');
                                            }
                                        });
                                        // $rootScope.db.update('stockTransaction',
                                        //     {
                                        //         "transactionType":"102",
                                        //         "daySerialNumber":0,
                                        //         "transactionNumber":0,
                                        //         "user":JSON.stringify(currentUser),
                                        //         "store":JSON.stringify(angular.copy($scope.entry.storeToSave)),
                                        //         "toStore":"",
                                        //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                        //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                        //         "receiver":JSON.stringify($scope.entry.availableReceiver),
                                        //         "cartage":$scope.entry.cartage,
                                        //         "discount":$scope.entry.discount,
                                        //         "discountType":$scope.entry.discountType,
                                        //         "serviceCharge":$scope.entry.serviceCharge,
                                        //         "serviceChargeType":$scope.entry.serviceChargeType,
                                        //         "vat":$scope.entry.vat,
                                        //         "vatType":$scope.entry.vatType,
                                        //         "payment":$scope.entry.payment,
                                        //         "heading":$scope.entry.heading,
                                        //         "wastageItemType":"",
                                        //         "wastageEntryType":"",
                                        //         "created": new Date().toISOString(),
                                        //         "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                        //         "templateName":$scope.entry.templateName,
                                        //         "deployment_id": currentUser.deployment_id,
                                        //         "tenant_id": currentUser.tenant_id,
                                        //         "isSynced" : false,
                                        //         "syncedOn" : undefined
                                        //     },{
                                        //         "templateName":$scope.entry.templateName
                                        //     }).then(function (result) {
                                        //         growl.success('New stock Sale Template created', {ttl: 3000});
                                        //         $modalInstance.dismiss('cancel');
                                        // });
                                    } else {
                                        //Cancelled
                                    }
                                }
                            } else {
                                growl.success('Provide a template name before OK', { ttl: 3000 });
                            }

                        };
                    }],
                    size: ''
                });
            } else {
                growl.success("No Items Available for template", { ttl: 3000 });
            }
        };

        $scope.createPhysicalTemplate = function() {
            var templateType = "Physical Stock Template";
            var availableTemplate = angular.copy($scope.physicalStockForm.availableTemplate);
            var itemToSave = angular.copy($scope.physicalStockForm.availableItems);
            if (itemToSave.length > 0) {
                var storeToSave = formatStore_PhysicalStockForm($scope.physicalStockForm.store);
                _.forEach(itemToSave, function(itm, i) {
                    itm.qty = "";
                });
                $modal.open({
                    templateUrl: 'app/stock/stockEntry/_entryTemplate.html',
                    controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'growl', function($rootScope, $scope, $resource, $modalInstance, growl) {

                        $scope.cancel = function() {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.entry = {};
                        $scope.entry.templateType = templateType;
                        $scope.entry.itemToSave = itemToSave;
                        $scope.entry.storeToSave = storeToSave;
                        $scope.entry.availableTemplate = availableTemplate;

                        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 103, null, null);
                        $scope.transaction._items = $scope.entry.itemToSave;
                        $scope.transaction._store = $scope.entry.storeToSave;
                        $scope.transaction.templateType = $scope.entry.templateType;
                        $scope.transaction.templateName = $scope.entry.templateName;

                        $scope.ok = function() {

                            if ($scope.entry.templateName != undefined) {
                                $scope.transaction.templateName = $scope.entry.templateName;
                                var indexxx = _.findIndex($scope.entry.availableTemplate, { "templateName": $scope.entry.templateName });

                                if (indexxx < 0) {
                                    //$scope.transaction._id=$scope.entry.availableTemplate[indexxx]._id;
                                    $scope.transaction.isOpen = false;
                                    $scope.transaction.updateTransaction({}, 'insert').then(function(results) {
                                        if (results != null) {
                                            growl.success('New Physical template created', { ttl: 3000 });
                                            $modalInstance.dismiss('cancel');
                                        }
                                    });
                                    // $rootScope.db.insert('stockTransaction',
                                    //         {
                                    //             "transactionId":guid(),
                                    //             "transactionType":"103",
                                    //             "daySerialNumber":0,
                                    //             "transactionNumber":0,
                                    //             "user":JSON.stringify(currentUser),
                                    //             "store":JSON.stringify(angular.copy($scope.entry.storeToSave)),
                                    //             "toStore":"",//JSON.stringify(angular.copy(tstore)),
                                    //             "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                    //             "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                    //             "receiver":"",
                                    //             "cartage":0,//$scope.stockEntryForm.cartage,
                                    //             "discount":0,//$scope.stockEntryForm.discount,
                                    //             "discountType":"",//$scope.stockEntryForm.discountType,
                                    //             "serviceCharge":0,
                                    //             "serviceChargeType":"",
                                    //             "vat":0,
                                    //             "vatType":"",
                                    //             "payment":0,
                                    //             "heading":"",
                                    //             "wastageItemType":"",
                                    //             "wastageEntryType":"",
                                    //             "created": new Date().toISOString(),
                                    //             "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                    //             "templateName":$scope.entry.templateName,
                                    //             "deployment_id": currentUser.deployment_id,
                                    //             "tenant_id": currentUser.tenant_id,
                                    //             "isSynced" : false,
                                    //             "syncedOn" : undefined
                                    //         }).then(function (result) {
                                    //             growl.success('New Physical Template created', {ttl: 3000});
                                    //             $modalInstance.dismiss('cancel');
                                    //         });
                                } else {
                                    //replace
                                    if (confirm("Template already available Ok will replace the Template") == true) {
                                        $scope.transaction._id = $scope.entry.availableTemplate[indexxx]._id;
                                        $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                            if (results != null) {
                                                growl.success('Physical template Updated', { ttl: 3000 });
                                                $modalInstance.dismiss('cancel');
                                            }
                                        });
                                        // $rootScope.db.update('stockTransaction',
                                        //    {
                                        //        "transactionType":"103",
                                        //        "daySerialNumber":0,
                                        //        "transactionNumber":0,
                                        //        "user":JSON.stringify(currentUser),
                                        //        "store":JSON.stringify(angular.copy($scope.entry.storeToSave)),
                                        //        "toStore":"",//JSON.stringify(angular.copy(tstore)),
                                        //        "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                        //        "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                        //        "receiver":"",
                                        //        "cartage":0,//$scope.stockEntryForm.cartage,
                                        //        "discount":0,//$scope.stockEntryForm.discount,
                                        //        "discountType":"",//$scope.stockEntryForm.discountType,
                                        //        "serviceCharge":0,
                                        //        "serviceChargeType":"",
                                        //        "vat":0,
                                        //        "vatType":"",
                                        //        "payment":0,
                                        //        "heading":"",
                                        //        "wastageItemType":"",
                                        //        "wastageEntryType":"",
                                        //        "created": new Date().toISOString(),
                                        //        "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                        //        "templateName":$scope.entry.templateName,
                                        //        "deployment_id": currentUser.deployment_id,
                                        //        "tenant_id": currentUser.tenant_id,
                                        //        "isSynced" : false,
                                        //        "syncedOn" : undefined
                                        //    },{
                                        //        "templateName":$scope.entry.templateName
                                        //    }).then(function (result) {
                                        //        growl.success('New Physical Template created', {ttl: 3000});
                                        //        $modalInstance.dismiss('cancel');
                                        //    });
                                    } else {
                                        //Cancelled
                                    }
                                }
                            } else {
                                growl.success('Provide a template name before OK', { ttl: 3000 });
                            }
                        };
                    }],
                    size: ''
                });
            } else {
                growl.success("No Items for template.");
            }

        };

        $scope.createWastageTemplate = function() {
            var templateType = "Wastage Template";
            var availableTemplate = angular.copy($scope.wastageEntryForm.availableTemplate);
            var itemToSave = angular.copy($scope.wastageEntryForm.availableItems); //formatItemsForInsert_wastageForm();
            if (itemToSave.length > 0) {
                //var itemtype=$scope.wastageEntryForm.forEntry.Name;
                var storeToSave = formatStore_PhysicalStockForm($scope.wastageEntryForm.store);
                var entryType = $scope.wastageEntryForm.entryType;
                _.forEach(itemToSave, function(itm, i) {
                    itm.qty = "";
                    itm.comment = "";
                });
                $modal.open({
                    templateUrl: 'app/stock/stockEntry/_entryTemplate.html',
                    controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'growl', function($rootScope, $scope, $resource, $modalInstance, growl) {

                        $scope.cancel = function() {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.entry = {};
                        $scope.entry.templateType = templateType;
                        $scope.entry.itemToSave = itemToSave;
                        $scope.entry.storeToSave = storeToSave;
                        //$scope.entry.itemtype=itemtype;
                        $scope.entry.entryType = entryType;
                        $scope.entry.availableTemplate = availableTemplate;

                        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 104, null, null);
                        $scope.transaction._items = $scope.entry.itemToSave;
                        $scope.transaction._store = $scope.entry.storeToSave;
                        $scope.transaction.wastageEntryType = $scope.entry.entryType;
                        //$scope.transaction.wastageItemType=$scope.entry.itemtype;
                        $scope.transaction.templateType = $scope.entry.templateType;
                        $scope.transaction.templateName = $scope.entry.templateName;

                        $scope.ok = function() {
                            if ($scope.entry.templateName != undefined) {
                                $scope.transaction.templateName = $scope.entry.templateName;
                                var indexxx = _.findIndex($scope.entry.availableTemplate, { "templateName": $scope.entry.templateName });

                                if (indexxx < 0) {

                                    //$scope.transaction._id=$scope.entry.availableTemplate[indexxx]._id;
                                    $scope.transaction.isOpen = false;
                                    $scope.transaction.updateTransaction({}, 'insert').then(function(results) {
                                        if (results != null) {
                                            growl.success('New Wastage template created', { ttl: 3000 });
                                            $modalInstance.dismiss('cancel');
                                        }
                                    });

                                    // $rootScope.db.insert('stockTransaction',
                                    // {
                                    //     "transactionId":guid(),
                                    //     "transactionType":"104",
                                    //     "daySerialNumber":0,
                                    //     "transactionNumber":0,
                                    //     "user":JSON.stringify(currentUser),
                                    //     "store":"",//JSON.stringify(angular.copy(storeToSave)),
                                    //     "toStore":"",//JSON.stringify(angular.copy(tstore)),
                                    //     "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                    //     "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                    //     "receiver":"",
                                    //     "cartage":0,//$scope.stockEntryForm.cartage,
                                    //     "discount":0,//$scope.stockEntryForm.discount,
                                    //     "discountType":"",//$scope.stockEntryForm.discountType,
                                    //     "serviceCharge":0,
                                    //     "serviceChargeType":"",
                                    //     "vat":0,
                                    //     "vatType":"",
                                    //     "payment":0,
                                    //     "wastageItemType":$scope.entry.itemtype,
                                    //     "wastageEntryType":$scope.entry.entryType,
                                    //     "heading":0,
                                    //     "created": new Date().toISOString(),
                                    //     "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                    //     "templateName":$scope.entry.templateName,
                                    //     "deployment_id": currentUser.deployment_id,
                                    //     "tenant_id": currentUser.tenant_id,
                                    //     "isSynced" : false,
                                    //     "syncedOn" : undefined
                                    // }).then(function (result) {
                                    //     growl.success('New wastage Template created', {ttl: 3000});
                                    //     $modalInstance.dismiss('cancel');
                                    // });
                                } else {
                                    if (confirm("Template already exist with same name Ok will replace the existing Template") == true) {

                                        $scope.transaction._id = $scope.entry.availableTemplate[indexxx]._id;
                                        $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                            if (results != null) {
                                                growl.success('Wastage template Updated', { ttl: 3000 });
                                                $modalInstance.dismiss('cancel');
                                            }
                                        });
                                        // $rootScope.db.update('stockTransaction',
                                        // {
                                        //     //"transactionId":$scope.entry.availableTemplate[indexxx]._id,
                                        //     "transactionType":"104",
                                        //     "daySerialNumber":0,
                                        //     "transactionNumber":0,
                                        //     "user":JSON.stringify(currentUser),
                                        //     "store":"",//JSON.stringify(angular.copy(storeToSave)),
                                        //     "toStore":"",//JSON.stringify(angular.copy(tstore)),
                                        //     "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                        //     "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                        //     "receiver":"",
                                        //     "cartage":0,//$scope.stockEntryForm.cartage,
                                        //     "discount":0,//$scope.stockEntryForm.discount,
                                        //     "discountType":"",//$scope.stockEntryForm.discountType,
                                        //     "serviceCharge":0,
                                        //     "serviceChargeType":"",
                                        //     "vat":0,
                                        //     "vatType":"",
                                        //     "payment":0,
                                        //     "wastageItemType":$scope.entry.itemtype,
                                        //     "wastageEntryType":$scope.entry.entryType,
                                        //     "heading":"",
                                        //     "created": new Date().toISOString(),
                                        //     "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                        //     "templateName":$scope.entry.templateName,
                                        //     "deployment_id": currentUser.deployment_id,
                                        //     "tenant_id": currentUser.tenant_id,
                                        //     "isSynced" : false,
                                        //     "syncedOn" : undefined
                                        // },{
                                        //     "templateName":$scope.entry.templateName
                                        // }).then(function (result) {
                                        //     growl.success('New wastage Template created', {ttl: 3000});
                                        //     $modalInstance.dismiss('cancel');
                                        //     //console.log($scope.$parent.wastageEntryForm);
                                        // });
                                    } else {
                                        //cancelled
                                    }
                                }
                            } else {
                                growl.success('Provide a template name before OK', { ttl: 3000 });
                            }
                        };
                    }],
                    size: ''
                });
            } else {
                growl.success("No Items present for Template", { ttl: 3000 });
            }
        };

        $scope.createTransferTemplate = function() {
            var templateType = "Stock Transfer Template";
            var availableTemplate = angular.copy($scope.stockTransferForm.availableTemplate);
            var fstore = formatStore_stockTransferForm($scope.stockTransferForm.fromStore);
            var tstore = formatStore_stockTransferForm($scope.stockTransferForm.toStore);
            getToAndFromCategoryOfItem_stockTransferForm();
            var itemToSave = angular.copy($scope.stockTransferForm.availableItems); //formatItems_stockTransferForm($scope.stockTransferForm.availableItems);
            _.forEach(itemToSave, function(itm, i) {
                itm.qty = "";
            });
            if (itemToSave.length > 0) {
                $modal.open({
                    templateUrl: 'app/stock/stockEntry/_entryTemplate.html',
                    controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'growl', function($rootScope, $scope, $resource, $modalInstance, growl) {

                        $scope.cancel = function() {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.entry = {};
                        $scope.entry.templateType = templateType;
                        $scope.entry.fromStore = fstore;
                        $scope.entry.toStore = tstore;
                        $scope.entry.itemToSave = itemToSave;
                        $scope.entry.availableTemplate = availableTemplate;


                        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 105, null, null);
                        $scope.transaction._items = $scope.entry.itemToSave;
                        $scope.transaction._store = $scope.entry.fromStore;
                        $scope.transaction._toStore = $scope.entry.toStore;
                        $scope.transaction.templateName = $scope.entry.templateName;


                        $scope.ok = function() {
                            if ($scope.entry.templateName != undefined) {
                                $scope.transaction.templateName = $scope.entry.templateName;
                                var indexxx = _.findIndex($scope.entry.availableTemplate, { "templateName": $scope.entry.templateName });
                                if (indexxx < 0) {
                                    //$scope.transaction._id=$scope.entry.availableTemplate[indexxx]._id;
                                    $scope.transaction.isOpen = false;
                                    $scope.transaction.updateTransaction({}, 'insert').then(function(results) {
                                        if (results != null) {
                                            growl.success('New Transfer template created', { ttl: 3000 });
                                            $modalInstance.dismiss('cancel');
                                        }
                                    });
                                    // $rootScope.db.insert('stockTransaction',
                                    // {
                                    //         "transactionId":guid(),
                                    //         "transactionType":"105",
                                    //         "daySerialNumber":0,
                                    //         "transactionNumber":0,
                                    //         "user":JSON.stringify(currentUser),
                                    //         "store":JSON.stringify(angular.copy($scope.entry.fromStore)),
                                    //         "toStore":JSON.stringify(angular.copy($scope.entry.toStore)),
                                    //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                    //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                    //         "receiver":"",
                                    //         "cartage":0,//$scope.stockEntryForm.cartage,
                                    //         "discount":0,//$scope.stockEntryForm.discount,
                                    //         "discountType":"",//$scope.stockEntryForm.discountType,
                                    //         "serviceCharge":0,
                                    //         "serviceChargeType":"",
                                    //         "vat":0,
                                    //         "vatType":"",
                                    //         "payment":0,
                                    //         "heading":"",
                                    //         "wastageItemType":"",
                                    //         "wastageEntryType":"",
                                    //         "created": new Date().toISOString(),
                                    //         "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                    //         "templateName":$scope.entry.templateName,
                                    //         "deployment_id": currentUser.deployment_id,
                                    //         "tenant_id": currentUser.tenant_id,
                                    //         "isSynced" : false,
                                    //         "syncedOn" : undefined
                                    //     }
                                    //     ).then(function (result) {
                                    //     growl.success("New Transfer Template created", {ttl: 3000});
                                    //     $modalInstance.dismiss('cancel');
                                    // });
                                } else {
                                    if (confirm("Template with same name already exist Ok will replace existing Template") == true) {
                                        // $rootScope.db.update('stockTransaction',
                                        // {
                                        //         "transactionType":"105",
                                        //         "daySerialNumber":0,
                                        //         "transactionNumber":0,
                                        //         "user":JSON.stringify(currentUser),
                                        //         "store":JSON.stringify(angular.copy($scope.entry.fromStore)),
                                        //         "toStore":JSON.stringify(angular.copy($scope.entry.toStore)),
                                        //         "category":"",//JSON.stringify($scope.stockEntryForm.availableCategory),
                                        //         "vendor":"",//JSON.stringify($scope.stockEntryForm.availableVendor),
                                        //         "receiver":"",
                                        //         "cartage":0,//$scope.stockEntryForm.cartage,
                                        //         "discount":0,//$scope.stockEntryForm.discount,
                                        //         "discountType":"",//$scope.stockEntryForm.discountType,
                                        //         "serviceCharge":0,
                                        //         "serviceChargeType":"",
                                        //         "vat":0,
                                        //         "vatType":"",
                                        //         "payment":0,
                                        //         "heading":"",
                                        //         "wastageItemType":"",
                                        //         "wastageEntryType":"",
                                        //         "created": new Date().toISOString(),
                                        //         "items": JSON.stringify($scope.entry.itemToSave),// JSON.stringify(this),
                                        //         "templateName":$scope.entry.templateName,
                                        //         "deployment_id": currentUser.deployment_id,
                                        //         "tenant_id": currentUser.tenant_id,
                                        //         "isSynced" : false,
                                        //         "syncedOn" : undefined
                                        //     },{
                                        //         "templateName":$scope.entry.templateName
                                        //     }).then(function (result) {
                                        //     growl.success("New Transfer Template created", {ttl: 3000});
                                        //     $modalInstance.dismiss('cancel');
                                        // });

                                        $scope.transaction._id = $scope.entry.availableTemplate[indexxx]._id;
                                        $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                                            if (results != null) {
                                                growl.success('Transfer template Updated', { ttl: 3000 });
                                                $modalInstance.dismiss('cancel');
                                            }
                                        });
                                    } else {
                                        //cancelled
                                    }
                                }
                            } else {
                                growl.success('Provide a template name before OK', { ttl: 3000 });
                            }
                        };
                    }],
                    size: ''
                });
            } else {
                growl.success("No Items for teplate", { ttl: 3000 });
            }
        };
        // --------------------------------------------------Prepared food Entry -------------------------------------//
        $scope.menuItemStockForm = { enableSubmit: true, maxDate: new Date(), billDate: new Date() };
        $scope.clearPreparedFoodEntryTab = function() {
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000;
            $scope.menuItemStockForm = {
                isNew: false,
                enableSubmit: true,
                showBill: true,
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
            };
            var op = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 11, null);
            op.getOpenTransactions(11).then(function(results) {
                $scope.openTrans_menuItem = results;
            });
            op.getTemplates(106).then(function(results) {
                $scope.template_menuItem = results;
            });
            if ($state.params != null) {
                $location.replace().path('/stock/stockEntry');
            }
        };
        $scope.new_stockMenuItemForm = function() {
            $scope.menuItemStockForm.isNew = true;
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 11, null, null);
            $scope.transaction.generateBillNumbers(11).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                if ($scope.resetSerialNumber)
                    $scope.menuItemStockForm.tempBillNo = "Temp/SE -" + snumbers.daySerialNumber;
                else
                    $scope.menuItemStockForm.tempBillNo = "Temp/SE -" + snumbers.billnumber;
                //$scope.menuItemStockForm.tempBillNo = "Temp/PE -" + snumbers.billnumber;
            });
            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000;
            $scope.menuItemStockForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))

        };
        $scope.newMenuItemStock = function() {

            var systemDate = new Date()
            var serverD = new Date($rootScope.serverTime.serverTime)
            var diffInMili = Number(systemDate.getTime() - serverD.getTime())
            var sec = diffInMili / 1000;
            $scope.menuItemStockForm = {
                isNew: true,
                enableSubmit: true,
                showBill: true,
                maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            };
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 11, null, null);
            $scope.transaction.generateBillNumbers(11).then(function(snumbers) {
                $scope.transaction._transactionNumber = snumbers.billnumber;
                $scope.transaction.transactionNumber = snumbers.billnumber;
                $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                $scope.menuItemStockForm.tempBillNo = "Temp/PE -" + snumbers.billnumber;
            });
        };

        $scope.resetMenuItemStockTab = function() {
            if (!$scope.menuItemStockForm.isEdit) {
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000;
                $scope.menuItemStockForm = {
                    isNew: true,
                    showBill: true,
                    enableSubmit: true,
                    billDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                    maxDate: new Date(serverD.setSeconds(serverD.getSeconds() + sec)),
                };
                $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 11, null, null);
                $scope.transaction.generateBillNumbers(11).then(function(snumbers) {
                    $scope.transaction._transactionNumber = snumbers.billnumber;
                    $scope.transaction.transactionNumber = snumbers.billnumber;
                    $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
                    if ($scope.resetSerialNumber)
                        $scope.menuItemStockForm.tempBillNo = "Temp/PE -" + snumbers.billnumber;
                    else
                        $scope.menuItemStockForm.tempBillNo = "Temp/PE -" + snumbers.daySerialNumber;
                });
            } else {
                //edit
                $scope.menuItemStockForm.isPrint = false;
                $scope.menuItemStockForm.isEdit = false;
                $scope.menuItemStockForm.isSaved = false;
            }
        };


        $scope.tempBill_menuItemStockForm = function(billDate) {
            $scope.menuItemStockForm.showBill = true;
            var serverDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate($rootScope.serverTime.serverTime))));
            var selectedDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(billDate))));
            var d = serverDate.getTime() - selectedDate.getTime()
            var di = new Date(d)
            var hours = Math.abs(serverDate - selectedDate) / 36e5;
            if (hours >= 72) {
                growl.success('Back Date Entry has been restricted to 48 Hours.', { ttl: 3000 });
                var systemDate = new Date()
                var serverD = new Date($rootScope.serverTime.serverTime)
                var diffInMili = Number(systemDate.getTime() - serverD.getTime())
                var sec = diffInMili / 1000
                $scope.menuItemStockForm.billDate = new Date(serverD.setSeconds(serverD.getSeconds() + sec))
            }
        };


        function setSelectedUnits(item) {
            $scope.menuItemStockForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId._id == u._id) {
                            $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                        }
                    } else if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                        }
                    }

                });
            });
        };

        function slectedUnit_MenuItemStockForm(item) {
            $scope.menuItemStockForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                var count = 0;
                _.forEach(itm.units, function(u, ii) {

                    if (count == 0) {
                        if (itm.selectedUnitId != undefined) {
                            if (itm.selectedUnitId._id == u._id) {
                                $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                                itm.price = parseFloat((itm.price) * (u.conversionFactor));

                            }
                            count++;
                        } else if (itm.selectedUnit != undefined) {
                            if (itm.selectedUnit._id == u._id) {
                                $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                                itm.price = parseFloat((itm.price) * (u.conversionFactor));

                            }
                            count++;
                        }
                        else{
                              if(itm.preferedUnit!=undefined){
                                  if(itm.preferedUnit==u._id){
                                      $scope.menuItemStockForm.selectedUnits[itm._id]=u;
                                  }
                              }
                            }
                    }
                
                });
            });
        };

        function setFinishedFoodPricing(itm) {
            if (!$scope.menuItemStockForm.selectedUnits)
                $scope.menuItemStockForm.selectedUnits = {};
            _.forEach(itm.units, function(u, ii) {
                if (itm.selectedUnitId != undefined) {
                    if (itm.selectedUnitId._id == u._id) {
                        $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                        itm.price = parseFloat((itm.actualPrice) * (u.conversionFactor));
                    }
                } else if (itm.selectedUnit != undefined) {
                    if (itm.selectedUnit._id == u._id) {
                        $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                        itm.price = parseFloat((itm.actualPrice) * (u.conversionFactor));
                    }
                }
            });
        };



        $scope.addRemainingItem_MenuItemStockForm = function($item, $model, $label) {
            var remaining = angular.copy($scope.menuItemStockForm.remainingItems);
            _.forEach($scope.menuItemStockForm.remainingItems, function(itm, i) {
                if (itm._id == $item._id) {
                    setFinishedFoodPricing(itm);
                    $scope.menuItemStockForm.availableItems.push(itm);
                    remaining.splice(i, 1);
                    $scope.menuItemStockForm.itemToAdd = "";
                    $scope.menuItemStockForm.selectedCategory[itm.fromCategory._id] = true;
                }
            });
            $scope.menuItemStockForm.remainingItems = remaining;
            //slectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
        };

        $scope.deleteSelectedItemMenuItemForm = function(item) {
            var index = _.findIndex($scope.menuItemStockForm.availableItems, { _id: item._id });
            $scope.menuItemStockForm.availableItems.splice(index, 1);
            //item.price = 0;
            $scope.menuItemStockForm.remainingItems.push(item);
        };

        $scope.bindCategory_MenuItemStockForm = function(stores) {
            var store = angular.copy(stores);
            $scope.menuItemStockForm.availableCategory = [];
            $scope.menuItemStockForm.selectedCategory = {};
            $scope.menuItemStockForm.availableItems = [];
            $scope.menuItemStockForm.remainingItems = [];
            store.category = [];
            _.forEach(store.processedFoodCategory, function(cat, iii) {
                $scope.menuItemStockForm.availableCategory.push(cat);
                _.forEachRight(cat.item, function(item, i) {
                    var index = _.findIndex($scope.menuRecipes, { itemId: item._id });
                    if (index >= 0) {
                        $scope.menuRecipes[index].itemName = item.itemName;
                        $scope.menuRecipes[index].actualPrice = $scope.menuRecipes[index].price;
                        item = angular.copy($scope.menuRecipes[index]);
                        var varFromCategory = { "_id": cat._id, "categoryName": cat.categoryName };
                        item["fromCategory"] = varFromCategory;
                        item["itemType"] = "MenuItem";
                        cat.item[i] = item;
                        $scope.menuItemStockForm.remainingItems.push(item);
                    } else {
                        cat.item.splice(i, 1);
                    }
                });
            });

        };

        $scope.bindItems_MenuItemStockForm = function(catId) {
            var cat = angular.copy($scope.menuItemStockForm.availableCategory);
            _.forEach(cat, function(c, ii) {
                if ($scope.menuItemStockForm.selectedCategory[catId] == true) {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var aa = _.findIndex($scope.menuItemStockForm.availableItems, { _id: itm._id });
                            if (aa < 0) {
                                setFinishedFoodPricing(itm);
                                /*var index = _.findIndex($scope.menuRecipes, {itemName: itm.itemName});
            if (index >= 0) {
            console.log('match found',itm);
            itm.price = $scope.menuRecipes[index].price;
            }
            else
            {

            }
      */
                                var varFromCategory = { "_id": c._id, "categoryName": c.categoryName };
                                itm["fromCategory"] = varFromCategory;
                                $scope.menuItemStockForm.availableItems.push(itm);
                                var index = _.findIndex($scope.menuItemStockForm.remainingItems, { _id: itm._id });
                                $scope.menuItemStockForm.remainingItems.splice(index, 1);
                            }
                        });
                    }
                } else {
                    if (c._id == catId) {
                        _.forEach(c.item, function(itm, ii) {
                            var index = _.findIndex($scope.menuItemStockForm.availableItems, { _id: itm._id });
                            if (index >= 0) {
                                setFinishedFoodPricing(itm);
                                /*var lindex = _.findIndex($scope.menuRecipes, {itemName: itm.itemName});
                                if (lindex >= 0) {
                                console.log('match found',itm);
                                itm.price = $scope.menuRecipes[index].price;
                                }
                                else
                                {

                                }*/
                                $scope.menuItemStockForm.availableItems.splice(index, 1);
                                $scope.menuItemStockForm.remainingItems.push(itm);
                            }
                        });
                    }
                }
            });
            setSelectedUnits($scope.menuItemStockForm.availableItems);
        };

        $scope.enableSubmitButton_openingMenuItem = function(items, selectedItem, units) {
            if (selectedItem != undefined) {
                convertPriceInSelectedUnit_INtermediate(selectedItem, units);
            }
            var flag = false;
            _.forEach(items, function(item, i) {
                if (flag == false) {
                    if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.menuItemStockForm.selectedUnits[item._id] != undefined && $scope.menuItemStockForm.selectedUnits[item._id] != null) {
                        flag = true;
                    }
                }
                var unit = $scope.menuItemStockForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (u != null) {
                        if (ii == item._id) {
                            item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                        }
                    }
                });
            });

            if (flag == true) {
                $scope.menuItemStockForm.enableSubmit = false;
            } else {
                $scope.menuItemStockForm.enableSubmit = true;
                growl.error('Please fill the full row before update', { ttl: 3000 });
            }
            //return flag;
        };

        $scope.goNextMenuItemStock = function(nextIdx, item) {
            var f = angular.element(document.querySelector('#mi_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            }
        };

        function getItemToSave_MenuItemStock(items) {
            var items = angular.copy(items);
            _.forEachRight(items, function(itm, i) {
                if (itm.qty != "" || itm.qty != undefined) {
                    var flag = false;
                    _.forEach($scope.menuItemStockForm.selectedUnits, function(u, ii) {
                        if (ii == itm._id && itm.qty != "" && itm.qty != undefined && u != null && $scope.menuItemStockForm.selectedUnits[itm._id] != undefined) {
                            if (itm.itemType != "RawMaterial") {
                                // var ss=$scope.transaction.calculateForProcessedAndSemiProcessed_sale(itm);
                                // itm=ss;
                                var rd = $scope.transaction.calculateForRecipeDetails(itm);
                                itm = rd;
                            }
                            flag = true;
                        }
                    });
                    if (flag == false) {
                        items.splice(i, 1);
                    }
                }
            });
            _.forEach(items, function(itm, i) {
                for (var p in itm) {
                    if (p != "_id" && p != "itemName" && p != "receipeDetails" && p != "rawItems" && p != "qty" && p != "calculateInUnits" && p != "selectedUnit" && p != "preferedUnit" && p != "itemType" && p != "fromCategory" && p != "units" && p != "menuQty" && p != "recipeQty" && p != "price") {
                        _.forEach(itm.units, function(u, ii) {
                            for (var p in u) {
                                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                                    delete u[p];
                                }
                            }
                        });
                        delete itm[p];
                    }
                }
                var ss = $scope.transaction.basePrefferedAnsSelectedUnitsByItem(itm, $scope.menuItemStockForm.selectedUnits, 1);
                itm.calculateInUnits = ss;
            });
            return items;
        };

        function formatStore_MenuItemStockForm(store) {
            var stores = angular.copy(store);
            for (var p in stores) {
                if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID") {
                    delete stores[p];
                }
            }
            return stores;
        };

        function formatStoreForMenuItemStock(item, store) {
            var stores = store;
            stores.category = [];
            _.forEach(item, function(itm, i) {
                var index = _.findIndex(stores.category, { _id: itm.fromCategory._id });
                if (index < 0) {
                    stores.category.push(itm.fromCategory);
                }
            });
            _.forEach(stores.category, function(c, i) {
                c.items = [];
                _.forEachRight(item, function(itm, ii) {
                    if (c._id == itm.fromCategory._id) {
                        delete itm.fromCategory;
                        c.items.push(itm);
                        item.splice(ii, 1);
                    }
                });
            });
            return stores;
        };
        $scope.checkMenuItemStockForOpenTransactionAndSave = function() {
            if ($state.params.id == undefined) {
                var storeToSave = formatStore_MenuItemStockForm($scope.menuItemStockForm.store);
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.menuItemStockForm.availableItems, $scope.menuItemStockForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_MenuItemStock(itms);
                var sToSave = formatStoreForMenuItemStock(angular.copy(itemToSave), angular.copy(storeToSave));
                if (itemToSave.length > 0) {
                    if ($scope.menuItemStockForm.transactionId != undefined) {
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = sToSave;
                        $scope.transaction.processTransaction($scope.menuItemStockForm, 13);
                        $scope.transaction.updateTransaction({}, 'update');
                    } else {
                        $scope.menuItemStockForm.transactionId = $scope.transaction._id;
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = storeToSave;
                        $scope.transaction.billDate = new Date($scope.menuItemStockForm.billDate);
                        $scope.transaction.processTransaction($scope.menuItemStockForm, 3);
                        $scope.transaction.updateTransaction({}, 'insert');
                    }
                }
            } else {

            }
        };
        $scope.submitMenuItemStock = function(formMenuItemStock) {
            //check for necessary data before submit

            if ($state.params.id == undefined) {
                $scope.checkMenuItemStockForOpenTransactionAndSave();
                $scope.menuItemStockForm.enableSubmit = true;
                var storeToSave = formatStore_MenuItemStockForm($scope.menuItemStockForm.store);
                //setFinishedFoodPricing($scope.menuItemStockForm.availableItems);
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.menuItemStockForm.availableItems, $scope.menuItemStockForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_MenuItemStock(itms);
                var sToSave = formatStoreForMenuItemStock(angular.copy(itemToSave), angular.copy(storeToSave));
                if (formMenuItemStock.$valid && !$scope.menuItemStockForm.isPrint) {
                    if (itemToSave.length > 0) {
                        $scope.transaction._items = itemToSave;
                        $scope.transaction._store = sToSave;
                        $scope.transaction.isOpen = false;
                        $scope.transaction.processTransaction($scope.menuItemStockForm, 13);
                        $scope.transaction.updateTransaction({}, 'update').then(function(results) {
                            if (results != null) {
                                $scope.menuItemStockForm.isPrint = true;
                                $scope.menuItemStockForm.isEdit = true;
                                $scope.menuItemStockForm.isSaved = true;
                                $scope.menuItemStockForm.enableSubmit = false;
                                intercom.registerEvent('StockTransaction');
                            }
                        });
                    }
                } else {
                    printPhysicalStock(itemToSave);
                    $scope.menuItemStockForm.enableSubmit = false;
                }
            } else {
                $scope.menuItemStockForm.enableSubmit = true;
                var storeToSave = formatStore_MenuItemStockForm($scope.menuItemStockForm.store);
                //slectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
                //setFinishedFoodPricing($scope.menuItemStockForm.availableItems);
                var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsWithCheck($scope.menuItemStockForm.availableItems, $scope.menuItemStockForm.selectedUnits, $scope.stockUnits);
                var itemToSave = getItemToSave_MenuItemStock(itms);
                if (itemToSave.length > 0 && $scope.menuItemStockForm.store != undefined) {
                    getItemsHistory_Physical($scope.menuItemStockForm.availableItems);
                    $scope.stockEditHitory.editHistoryId = guid();
                    $scope.stockEditHitory.transactionId = $scope.menuItemStockForm.transactionId;
                    $scope.stockEditHitory.mainTransactionId = $scope.menuItemStockForm.mainTransactionId;
                    $scope.stockEditHitory.billNo = $scope.menuItemStockForm.tempBillNo;
                    $scope.stockEditHitory.user = formatUser(currentUser);
                    $scope.stockEditHitory.tenant_id = currentUser.tenant_id;
                    $scope.stockEditHitory.deployment_id = currentUser.deployment_id;
                    EditTransactionHistory.saveData({}, $scope.stockEditHitory, function(result) {
                        growl.success('New history created', { ttl: 3000 });
                        if ($state.params != null) {
                            $location.replace().path('/stock/stockEntry');
                        }
                    });

                    var sToSave = formatStoreForMenuItemStock(angular.copy(itemToSave), angular.copy(storeToSave));
                    var toSave = { _store: sToSave, _id: $scope.menuItemStockForm.mainTransactionId };
                    StockResource.update({}, toSave, function(bill) {
                    });
                } else {
                    $scope.menuItemStockForm.enableSubmit = false;
                }
            }
        };

        function addMenuItemStock(item) {
            _.forEachRight($scope.menuItemStockForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    $scope.menuItemStockForm.availableItems.push(item);
                    $scope.menuItemStockForm.remainingItems.splice(i, 1);
                }
            });
            slectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
        };

        function editMenuItemStock(item) {
            _.forEachRight($scope.menuItemStockForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    $scope.menuItemStockForm.availableItems.push(item);
                    $scope.menuItemStockForm.remainingItems.splice(i, 1);
                }
            });
            //editSelectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
        };


        function editSelectedUnit_MenuItemStockForm(item) {
            $scope.menuItemStockForm.selectedUnits = {};
            _.forEach(item, function(itm, i) {
                _.forEach(itm.units, function(u, ii) {
                    if (itm.selectedUnitId != undefined) {
                        if (itm.selectedUnitId._id == u._id) {
                            $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                            itm.price = parseFloat((itm.price) * (u.conversionFactor));
                        }
                    } else if (itm.selectedUnit != undefined) {
                        if (itm.selectedUnit._id == u._id) {
                            $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                            itm.price = parseFloat((itm.price) * (u.conversionFactor));
                        }
                    }
                    // else{
                    //   if(itm.preferedUnit!=undefined){
                    //       if(itm.preferedUnit==u._id){
                    //           $scope.menuItemStockForm.selectedUnits[itm._id]=u;
                    //       }
                    //   }
                    // }
                });
            });
        };


        /*var l_index = _.findIndex($scope.itemLastAndAveragePrice, {itemId: itm._id});
            if (l_index > -1) {
              var lastPrice = $scope.itemLastAndAveragePrice[l_index].lastPrice;
              var conversionFactor = getConFactor(itm.selectedUnitId);
              var baseConFac = 1;
              var avMonthPrice = $scope.itemLastAndAveragePrice[l_index].monthAverage;

              if (lastPrice == 0) {
                itm.calculatedPrice = parseFloat(0).toFixed(2);
              }
              else {
                itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(conversionFactor));
              }
              if (avMonthPrice == 0) {
                itm.calculatedAveragePrice = parseFloat(0).toFixed(2);
              }
              else {
                itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(conversionFactor));
              }

              _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice));
              _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice));
            } else {
              var lastPrice = 0;
              var conversionFactor = getConFactor(itm.selectedUnitId);
              var baseConFac = 1;
              var avMonthPrice = 0;

              if (lastPrice == 0) {
                itm.calculatedPrice = parseFloat(0).toFixed(2);
              }
              else {
                itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(conversionFactor));
              }
              if (avMonthPrice == 0) {
                itm.calculatedAveragePrice = parseFloat(0).toFixed(2);
              }
              else {
                itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(conversionFactor));
              }

              _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice));
              _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice));
            }*/

        /*function editSelectedUnit_MenuItemStockForm(item) {
          //console.log('$scope.editItems',$scope.editItems);
          console.log('Inside Here');
          $scope.menuItemStockForm.selectedUnits = {};
          _.forEach(item, function (itm, i) {
            var l_index = _.findIndex($scope.editItems, {itemName: itm.itemName});
            if(l_index > -1)
            {
              var lastPrice = $scope.editItems[l_index].lastPrice;
              console.log('last price if ',lastPrice,itm);
              
                _.forEach(itm.units, function (u, ii) 
                {
                  if (itm.selectedUnitId != undefined)
                  {
                    if (itm.selectedUnitId._id == u._id)
                    {
                      $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                      itm.price = parseFloat((lastPrice) * (u.conversionFactor));
                    }
                  }
                  else if (itm.selectedUnit != undefined) 
                  {
                    if (itm.selectedUnit._id == u._id) 
                    {
                      $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                      itm.price = parseFloat((lastPrice) * (u.conversionFactor));
                    }
                  }
                });
              
            }
            else
            {
              var lastPrice = 0;
              console.log('last price else ',lastPrice ,itm);
              
                _.forEach(itm.units, function (u, ii) 
                {
                  if (itm.selectedUnitId != undefined)
                  {
                    if (itm.selectedUnitId._id == u._id)
                    {
                      $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                      itm.price = parseFloat((lastPrice) * (u.conversionFactor));
                    }
                  }
                  else if (itm.selectedUnit != undefined) 
                  {
                    if (itm.selectedUnit._id == u._id) 
                    {
                      $scope.menuItemStockForm.selectedUnits[itm._id] = u;
                      itm.price = parseFloat((lastPrice) * (u.conversionFactor));
                    }
                  }
                });
              
            }
          });
        };*/


        function ot_addMenuItemStock(item) {
            _.forEachRight($scope.menuItemStockForm.remainingItems, function(itm, i) {
                if (itm._id == item._id) {
                    $scope.menuItemStockForm.availableItems.push(item);
                    $scope.menuItemStockForm.remainingItems.splice(i, 1);
                }
            });
            //slectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
        };


        $scope.ot_MenuItemStockForm = function(openTransaction, indexxx) {
            $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 11, openTransaction._transactionNumber, openTransaction.transactionId, openTransaction.daySerialNumber);
            $scope.menuItemStockForm.showBill = true;
            $scope.menuItemStockForm.isOpen = true;
            if ($scope.resetSerialNumber)
                $scope.menuItemStockForm.tempBillNo = "Temp/SE - " + openTransaction.daySerialNumber;
            else
                $scope.menuItemStockForm.tempBillNo = "Temp/SE - " + openTransaction._transactionNumber;
            $scope.menuItemStockForm.tempBillNo = "Temp/PE - " + openTransaction._transactionNumber;
            $scope.menuItemStockForm.transactionId = openTransaction.transactionId;
            var st_index = _.findIndex($scope.stores, { "_id": openTransaction._store._id });
            $scope.menuItemStockForm.store = $scope.stores[st_index];

            $scope.bindCategory_MenuItemStockForm($scope.menuItemStockForm.store);
            $scope.menuItemStockForm.selectedCategory = [];
            _.forEach(openTransaction._items, function(itm, i) {
                var index = _.findIndex($scope.menuRecipes, { itemName: itm.itemName });
                if (index >= 0) {
                    itm.price = $scope.menuRecipes[index].price;
                } else {

                }
                $scope.menuItemStockForm.selectedCategory[itm.fromCategory._id] = true;
                ot_addMenuItemStock(itm);
            });
            editSelectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
            $scope.menuItemStockForm.isNew = true;
            $scope.menuItemStockForm.enableSubmit = false;
        };
        //----------------------------------------Submit And Prints---------------------------------------------//
        $scope.submitAndPrintStockEntry = function() {
            getFromCategoryOfItem_stockEntryForm();
            var itemToSave = getItemToSave_stockEntryForm($scope.stockEntryForm.availableItems);
            var storeToSave = formatStore_stockEntryForm($scope.stockEntryForm.store);
            var cDate = new Date().toISOString();
            var billNo = $scope.stockEntryForm.tempBillNo;
            var discount = $scope.stockEntryForm.discount;
            var discountType = $scope.stockEntryForm.discountType;
            var cartage = ($scope.stockEntryForm.cartage != undefined) ? $scope.stockEntryForm.cartage : 0;
            $rootScope.db.insert('stockTransaction', {
                "transactionId": guid(),
                "transactionType": "1",
                "daySerialNumber": 0,
                "transactionNumber": 0,
                "user": JSON.stringify(currentUser),
                "store": JSON.stringify(angular.copy(storeToSave)),
                "toStore": "",
                "category": "", //JSON.stringify($scope.stockEntryForm.availableCategory),
                "vendor": JSON.stringify($scope.stockEntryForm.availableVendor),
                "receiver": "",
                "cartage": $scope.stockEntryForm.cartage,
                "discount": $scope.stockEntryForm.discount,
                "discountType": $scope.stockEntryForm.discountType,
                "serviceCharge": "",
                "serviceChargeType": "",
                "vat": "",
                "vatType": "",
                "payment": "",
                "heading": "",
                "wastageItemType": "",
                "wastageEntryType": "",
                "created": cDate,
                "items": JSON.stringify(itemToSave), // JSON.stringify(this),
                "templateName": "",
                "deployment_id": currentUser.deployment_id,
                "tenant_id": currentUser.tenant_id,
                "isSynced": false,
                "syncedOn": undefined
            }).then(function(result) {
                growl.success('Success in stock entry', { ttl: 3000 });
                growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                $scope.stockEntryForm = {
                    enableSubmit: true,
                    discountType: 'percent',
                    isTemplateDelete: true,
                    isTemplateSave: true
                };
                $scope.stockEntryForm.availableItems = [];
            });
            $modal.open({
                templateUrl: 'app/stock/stockEntry/_printStockEntry.html',
                controller: ['$rootScope', '$scope', '$resource', '$modalInstance', function($rootScope, $scope, $resource, $modalInstance) {
                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.entry = {};
                    $scope.transferItems = angular.copy(itemToSave);
                    $scope.store = angular.copy(storeToSave);
                    $scope.entry.entryDate = angular.copy(cDate);
                    $scope.entry.entryNo = billNo;
                    $scope.entry.cartage = (cartage).toFixed(2);
                    calculateTotal(itemToSave);

                    function calculateTotal(itemToSave) {
                        var total = 0;
                        var availableAmt = 0;
                        var vat = 0;
                        var gt = 0;
                        _.forEach(itemToSave, function(item, i) {
                            vat = vat + item.addedAmt;
                            total = total + item.totalAmount;
                        });
                        $scope.entry.total = (total).toFixed(2);
                        if (discountType == "percent") {
                            var dAmt = (total * .01 * discount);
                            $scope.entry.discount = (dAmt).toFixed(2);
                        } else {
                            $scope.entry.discount = discount;
                        }
                        $scope.entry.availableAmt = total - ($scope.entry.discount);
                        $scope.entry.vat = (vat).toFixed(2);
                        gt = vat + cartage + ($scope.entry.availableAmt);
                        $scope.entry.grandTotal = (gt).toFixed(2);
                    };

                    $scope.print = function() {
                        //exporttoexcel();
                        printByCrome();
                    };

                    function exporttoexcel() {
                        var table = document.getElementById("printTable");
                        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table.outerHTML));
                    };

                    function printByCrome() {
                        var table = document.getElementById("printTable");
                        var printer = window.open('', '', 'width=600,height=600');
                        var htmlToPrint = '' + '<style type="text/css">.table-curved {    border-collapse: separate;} .table-curved {    border: solid #ccc 1px;    border-radius: 6px;    border-left:0px;}.table-curved td, .table-curved th {    border-left: 1px solid #ccc;    border-top: 1px solid #ccc;}.table-curved th {    border-top: none;    background-color: #24242D; /*#337AB7;*/    color: #fff;}.table-curved th:first-child {    border-radius: 6px 0 0 0;}.table-curved th:last-child {    border-radius: 0 6px 0 0;}.table-curved th:only-child{    border-radius: 6px 6px 0 0;}.table-curved tr:last-child td:first-child {    border-radius: 0 0 0 6px;}.table-curved tr:last-child td:last-child {    border-radius: 0 0 6px 0;}</style>';
                        htmlToPrint += table.outerHTML;
                        printer.document.open("text/html");
                        printer.document.write(htmlToPrint);
                        printer.document.close();
                        printer.focus();
                        printer.print();
                    };
                }],
                size: 'lg'
            });
        };
        $scope.submitAndPrintStockSale = function() {
            getFromCategoryOfItem_stockSaleForm();
            var storeToSave = formatStore_stockEntryForm($scope.stockSaleForm.store);
            var itemToSave = getItemToSave_stockSaleForm($scope.stockSaleForm.availableItems);
            var cDate = new Date().toISOString();
            var billNo = $scope.stockSaleForm.tempBillNo;
            var cartage = $scope.stockSaleForm.cartage;
            var discount = $scope.stockSaleForm.discount;
            var discountType = $scope.stockSaleForm.discountType;
            var serviceCharge = $scope.stockSaleForm.serviceCharge;
            var serviceChargeType = $scope.stockSaleForm.serviceChargeType;
            var vat = $scope.stockSaleForm.vat;
            var vatType = $scope.stockSaleForm.vatType;
            var payment = $scope.stockSaleForm.payment;
            var heading = $scope.stockSaleForm.heading;
            $rootScope.db.insert('stockTransaction', {
                "transactionId": guid(),
                "transactionType": "2",
                "daySerialNumber": 0,
                "transactionNumber": 0,
                "user": JSON.stringify(currentUser),
                "store": JSON.stringify(angular.copy(storeToSave)),
                "toStore": "",
                "category": "", //JSON.stringify($scope.stockEntryForm.availableCategory),
                "vendor": "", //JSON.stringify($scope.stockEntryForm.availableVendor),
                "receiver": JSON.stringify($scope.stockSaleForm.availableReceiver),
                "cartage": $scope.stockSaleForm.cartage,
                "discount": $scope.stockSaleForm.discount,
                "discountType": $scope.stockSaleForm.discountType,
                "serviceCharge": $scope.stockSaleForm.serviceCharge,
                "serviceChargeType": $scope.stockSaleForm.serviceChargeType,
                "vat": $scope.stockSaleForm.vat,
                "vatType": $scope.stockSaleForm.vatType,
                "payment": $scope.stockSaleForm.payment,
                "heading": $scope.stockSaleForm.heading,
                "wastageItemType": "",
                "wastageEntryType": "",
                "created": cDate,
                "items": JSON.stringify(itemToSave), // JSON.stringify(this),
                "templateName": "",
                "deployment_id": currentUser.deployment_id,
                "tenant_id": currentUser.tenant_id,
                "isSynced": false,
                "syncedOn": undefined
            }).then(function(result) {
                growl.success('Success in stock Sale', { ttl: 3000 });
                growl.success(itemToSave.length + 'Items added', { ttl: 3000 });
                $scope.stockSaleForm = { enableSubmit: true, discountType: 'percent' };
                $scope.stockSaleForm.availableItems = [];
            });

            $modal.open({
                templateUrl: 'app/stock/stockEntry/_printStockSale.html',
                controller: ['$rootScope', '$scope', '$resource', '$modalInstance', function($rootScope, $scope, $resource, $modalInstance) {
                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.entry = {};
                    $scope.transferItems = angular.copy(itemToSave);
                    $scope.store = angular.copy(storeToSave);
                    $scope.entry.entryDate = angular.copy(cDate);
                    $scope.entry.entryNo = billNo;
                    $scope.entry.cartage = (cartage).toFixed(2);
                    calculateTotal(itemToSave);

                    function calculateTotal(itemToSave) {
                        var total = 0;
                        var availableAmt = 0;
                        var gt = 0;
                        _.forEach(itemToSave, function(item, i) {
                            //vat=vat+item.addedAmt;
                            total = total + (item.qty * item.price);
                        });
                        $scope.entry.total = (total).toFixed(2);
                        if (discountType == "percent") {
                            var dAmt = (total * .01 * discount);
                            $scope.entry.discount = (dAmt).toFixed(2);
                        } else {
                            $scope.entry.discount = discount;
                        }
                        $scope.entry.availableAmt = total - ($scope.entry.discount);
                        if (vatType == "percent") {
                            var dAmt = (total * .01 * vat);
                            $scope.entry.vat = (dAmt).toFixed(2);
                        } else {
                            $scope.entry.vat = vat;
                        }
                        $scope.entry.vat = (vat).toFixed(2);
                        gt = vat + cartage + ($scope.entry.availableAmt);
                        $scope.entry.grandTotal = (gt).toFixed(2);
                    };
                    $scope.print = function() {
                        exporttoexcel();
                        //printByCrome();
                    };

                    function exporttoexcel() {
                        var table = document.getElementById("printTable");
                        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table.outerHTML));
                    };

                    function printByCrome() {
                        var table = document.getElementById("printTable");
                        var printer = window.open('', '', 'width=600,height=600');
                        printer.document.open("text/html");
                        printer.document.write(table.outerHTML);
                        printer.document.close();
                        printer.focus();
                        printer.print();
                    };
                }],
                size: 'lg'
            });
        };
        $scope.submitAndPrintPhysicalStock = function() {
            var storeToSave = formatStore_PhysicalStockForm($scope.physicalStockForm.store);
            var itemToSave = getItemToSave_PhysicalStock();
            console.log(itemToSave);
            $rootScope.db.insert('stockTransaction', {
                "transactionId": guid(),
                "transactionType": "3",
                "daySerialNumber": 0,
                "transactionNumber": 0,
                "user": JSON.stringify(currentUser),
                "store": JSON.stringify(angular.copy(storeToSave)),
                "toStore": "", //JSON.stringify(angular.copy(tstore)),
                "category": "", //JSON.stringify($scope.stockEntryForm.availableCategory),
                "vendor": "", //JSON.stringify($scope.stockEntryForm.availableVendor),
                "receiver": "",
                "cartage": "", //$scope.stockEntryForm.cartage,
                "discount": "", //$scope.stockEntryForm.discount,
                "discountType": "", //$scope.stockEntryForm.discountType,
                "serviceCharge": "",
                "serviceChargeType": "",
                "vat": "",
                "vatType": "",
                "payment": "",
                "heading": "",
                "wastageItemType": "",
                "wastageEntryType": "",
                "created": new Date().toISOString(),
                "items": JSON.stringify(itemToSave), // JSON.stringify(this),
                "templateName": "",
                "deployment_id": currentUser.deployment_id,
                "tenant_id": currentUser.tenant_id,
                "isSynced": false,
                "syncedOn": undefined
            }).then(function(result) {
                growl.success('Inserted Successfully', { ttl: 3000 });
                growl.success(itemToSave.length + ' Items Inserted', { ttl: 3000 });
                $scope.physicalStockForm = { enableSubmit: true };
            });
            $modal.open({
                templateUrl: 'app/stock/stockEntry/_printPhysicalStock.html',
                controller: ['$rootScope', '$scope', '$resource', '$modalInstance', function($rootScope, $scope, $resource, $modalInstance) {
                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.transferItems = angular.copy(itemToSave);
                    $scope.print = function() {
                        //exporttoexcel();
                        printByCrome();
                    };

                    function exporttoexcel() {
                        var table = document.getElementById("printTable");
                        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table.outerHTML));
                    };

                    function printByCrome() {
                        var table = document.getElementById("printTable");
                        var printer = window.open('', '', 'width=600,height=600');
                        printer.document.open("text/html");
                        printer.document.write(table.outerHTML);
                        printer.document.close();
                        printer.focus();
                        printer.print();
                    };
                }],
                size: 'lg'
            });
        };
        $scope.submitAndPrintWastageEntry = function() {
            var itemToSave = formatItemsForInsert_wastageForm();
            console.log(itemToSave);
            var index = _.findIndex($scope.stockTransactionTypes, { 'Name': $scope.wastageEntryForm.stockTransactionTypes });
            var transactionType = $scope.stockTransactionTypes[index].transactionType;
            $rootScope.db.insert('stockTransaction', {
                "transactionId": guid(),
                "transactionType": transactionType,
                "daySerialNumber": 0,
                "transactionNumber": 0,
                "user": JSON.stringify(currentUser),
                "store": "", //JSON.stringify(angular.copy(storeToSave)),
                "toStore": "", //JSON.stringify(angular.copy(tstore)),
                "category": "", //JSON.stringify($scope.stockEntryForm.availableCategory),
                "vendor": "", //JSON.stringify($scope.stockEntryForm.availableVendor),
                "receiver": "",
                "cartage": "", //$scope.stockEntryForm.cartage,
                "discount": "", //$scope.stockEntryForm.discount,
                "discountType": "", //$scope.stockEntryForm.discountType,
                "serviceCharge": "",
                "serviceChargeType": "",
                "vat": "",
                "vatType": "",
                "payment": "",
                "wastageItemType": $scope.wastageEntryForm.forEntry.Name,
                "wastageEntryType": $scope.wastageEntryForm.entryType,
                "heading": "",
                "created": new Date().toISOString(),
                "items": JSON.stringify(itemToSave), // JSON.stringify(this),
                "templateName": "",
                "deployment_id": currentUser.deployment_id,
                "tenant_id": currentUser.tenant_id,
                "isSynced": false,
                "syncedOn": undefined
            }).then(function(result) {
                growl.success('Inserted Successfully', { ttl: 3000 });
                growl.success(itemToSave.length + ' Items Inserted', { ttl: 3000 });
                $scope.wastageEntryForm = {
                    entryType: 'Wastage',
                    hideMenuItem: true,
                    hideRawMaterial: false,
                    enableSubmit: true
                };
                $scope.wastageEntryForm.entryFor = [
                    { Name: 'RawMaterial' }, { Name: 'MenuItem' }
                ];
            });
            $modal.open({
                templateUrl: 'app/stock/stockEntry/_printWastageEntry.html',
                controller: ['$rootScope', '$scope', '$resource', '$modalInstance', function($rootScope, $scope, $resource, $modalInstance) {
                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.transferItems = angular.copy(itemToSave);
                    $scope.print = function() {
                        //exporttoexcel();
                        printByCrome();
                    };

                    function exporttoexcel() {
                        var table = document.getElementById("printTable");
                        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table.outerHTML));
                    };

                    function printByCrome() {
                        var table = document.getElementById("printTable");
                        var printer = window.open('', '', 'width=600,height=600');
                        printer.document.open("text/html");
                        printer.document.write(table.outerHTML);
                        printer.document.close();
                        printer.focus();
                        printer.print();
                    };
                }],
                size: 'lg'
            });
        };

        $scope.submitAndPrintStockTransfer = function() {
            var fstore = formatStore_stockTransferForm($scope.stockTransferForm.fromStore);
            var tstore = formatStore_stockTransferForm($scope.stockTransferForm.toStore);
            console.log(fstore);
            getToAndFromCategoryOfItem_stockTransferForm();
            var itemToSave = formatItems_stockTransferForm($scope.stockTransferForm.availableItems);
            console.log(itemToSave);

            $rootScope.db.insert('stockTransaction', {
                "transactionId": guid(),
                "transactionType": "5",
                "daySerialNumber": 0,
                "transactionNumber": 0,
                "user": JSON.stringify(currentUser),
                "store": JSON.stringify(angular.copy(fstore)),
                "toStore": JSON.stringify(angular.copy(tstore)),
                "category": "", //JSON.stringify($scope.stockEntryForm.availableCategory),
                "vendor": "", //JSON.stringify($scope.stockEntryForm.availableVendor),
                "receiver": "",
                "cartage": "", //$scope.stockEntryForm.cartage,
                "discount": "", //$scope.stockEntryForm.discount,
                "discountType": "", //$scope.stockEntryForm.discountType,
                "serviceCharge": "",
                "serviceChargeType": "",
                "vat": "",
                "vatType": "",
                "payment": "",
                "heading": "",
                "wastageItemType": "",
                "wastageEntryType": "",
                "created": new Date().toISOString(),
                "items": JSON.stringify(itemToSave), // JSON.stringify(this),
                "templateName": "",
                "deployment_id": currentUser.deployment_id,
                "tenant_id": currentUser.tenant_id,
                "isSynced": false,
                "syncedOn": undefined
            }).then(function(result) {
                growl.success(itemToSave.length + ' Items Transferred Successfully', { ttl: 3000 });
                $scope.stockTransferForm = { enableSubmit: true };
            });
            $modal.open({
                templateUrl: 'app/stock/stockEntry/_printStockTransfer.html',
                controller: ['$rootScope', '$scope', '$resource', '$modalInstance', function($rootScope, $scope, $resource, $modalInstance) {
                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.transferItems = angular.copy(itemToSave);
                    $scope.print = function() {
                        //exporttoexcel();
                        printByCrome();
                    };

                    function exporttoexcel() {
                        var table = document.getElementById("printTable");
                        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table.outerHTML));
                    };

                    function printByCrome() {
                        var table = document.getElementById("printTable");
                        var printer = window.open('', '', 'width=600,height=600');
                        printer.document.open("text/html");
                        printer.document.write(table.outerHTML);
                        printer.document.close();
                        printer.focus();
                        printer.print();
                    };
                }],
                size: 'lg'
            });
        };

        $interval(syncOnline, 60000);

        function syncOnline() {
            console.log('syncing');
            //if($rootScope.online==true){
            //get All data of web Sql
            //$rootScope.db.selectDynamic("select transactionData from stocktransactions where isOpen='false' and isSynced='false' order by created limit 100").then(function(result){
            $rootScope.db.selectDynamic("select transactionData from stocktransactions where isOpen='false' and isSynced='false'").then(function(result) {
                console.log(result.rows.length);
                var offlineBills = [];
                if (result.rows.length > 0) {
                    for (var i = 0; i < result.rows.length; i++) {
                        //var data=(result.rows.item(i).transactionData);
                        var _ob = angular.fromJson(result.rows.item(i).transactionData);
                        //offlineBills.push( ( angular.toJson(JSON.parse(data))));
                        offlineBills.push(_ob);
                    }
                    if (offlineBills.length > 0)
                        sendOnline_Transactions(offlineBills);
                    //$scope.autoCallSaveBill(( allBillsData),0);
                }
            });

            // $rootScope.db.selectAll('stocktransactions').then(function (results) {
            //     //if(results.isSynced!="" || results.isSynced!=undefined){
            //         var _rows = [];
            //         var offlineBills = [];
            //         _.forEach(results.rows,function(r,i){
            //             if(r.isOpen=="false"){
            //                 if(r.isSynced=="false"){
            //                     _rows.push(r);
            //                 }
            //             }
            //         });
            //         _.forEach(_rows, function (ob) {
            //                 var _ob=angular.fromJson(ob.transactionData);
            //                 offlineBills.push(_ob);
            //         });

            //         sendOnline_Transactions(offlineBills);//-------------------------------------Auto sync online uncomment

            // });
            //}
            //else{
            //alert("Offline");

            //}
        };

        function sendOnline_Transactions(offlineBills) {
            _.forEach(offlineBills, function(bills) {
                /*alert("Delete offline bills after syncing?");*/
                if (bills.isSynced == false && bills.isOpen == false) {
                    // bills._id=bills.transactionId;
                    // bills._id=bills._id;
                    var timestamp = Date.parse(bills.billDate);
                    if (isNaN(timestamp) == false) {
                        bills.created = new Date(timestamp);
                    } else {
                        bills.created = new Date();
                    }
                }
                StockResource.createWithTransactionId({}, bills, function(bill) {
                    console.log(bill);
                    $rootScope.db.update('stocktransactions', {
                        "isSynced": true,
                        "syncedOn": new Date().toISOString(),
                        "daySerialNumber": bill.daySerialNumber,
                        "transactionNumber": bill.transactionNumber
                    }, {
                        "transactionId": bill.transactionId,
                    }).then(function() {
                        growl.success("Synced transaction!", { ttl: 3000 });
                        //deleteOpenTransactions(bill);
                    });
                }, function(err) {
                    if (err.data.errorcode == 1) {
                        $rootScope.db.update('stocktransactions', {
                            "isSynced": 'true',
                            "syncedOn": new Date().toISOString()
                                //"syncedOn": new Date().toISOString()
                        }, {
                            "transactionId": bills.transactionId
                        }).then(function() {
                            //growl.error("Couldn't Sync transaction", {ttl: 3000});
                            //growl.success("Synced transaction!", {ttl: 3000});
                            //deleteOpenTransactions(bill);
                        });
                    } else {
                        growl.error("Couldn't Sync transaction", { ttl: 3000 });
                    }
                });
            });
        };

        function deleteOpenTransactions(bill) {
            if (bill.transactionType == "1") {
                var index = _.findIndex($scope.openTrans_Entry, { "transactionId": bill.transactionId });
                $scope.openTrans_Entry.splice(index, 1);
            } else if (bill.transactionType == "2") {
                var index = _.findIndex($scope.openTrans_Sale, { "transactionId": bill.transactionId });
                $scope.openTrans_Sale.splice(index, 1);
            } else if (bill.transactionType == "3") {
                var index = _.findIndex($scope.openTrans_Physical, { "transactionId": bill.transactionId });
                $scope.openTrans_Physical.splice(index, 1);
            } else if (bill.transactionType == "4") {
                var index = _.findIndex($scope.openTrans_Wastage, { "transactionId": bill.transactionId });
                $scope.openTrans_Wastage.splice(index, 1);
            } else if (bill.transactionType == "5") {
                var index = _.findIndex($scope.openTrans_transfer, { "transactionId": bill.transactionId });
                $scope.openTrans_transfer.splice(index, 1);
            }
        };

        function convertPriceInSelectedUnit_Entry(item, unit, type) {
            //if(item.preferedUnit!=undefined){

            //var pInd=_.findIndex(item.units,{_id:item.preferedUnit});

            if (item.selectedUnit != undefined) {
                var pInd = _.findIndex(item.units, { _id: item.selectedUnit._id });
            } else if (item.selectedUnitId != undefined) {
                var pInd = _.findIndex(item.units, { _id: item.selectedUnitId._id });
            } else {
                var unit = $scope.stockEntryForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (ii == item._id) {
                        item.selectedUnitId = u._id;
                        item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                    }
                });
                var pInd = _.findIndex(item.units, { _id: item.selectedUnit._id });
            }
            //var pInd=_.findIndex(item.units,{_id:item.selectedUnitId._id});
            var preferedUnits = angular.copy(item.units[pInd]);
            var preferedconversionFactor = item.units[pInd].conversionFactor;
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            if (item.newConFactor == undefined) {
                item.newConFactor = preferedconversionFactor;
            }
            item.newprice = parseFloat(item.price) / parseFloat(item.newConFactor);
            _.forEach(unit, function(u, ii) {
                if (u != null) {
                    if (ii == item._id) {
                        selectedConversionFactor = u.conversionFactor;
                    }
                }
            });

            // //if(item.newprice==undefined && item.newConFactor==undefined){
            //     //if(item.newprice==undefined){
            //         if(item.price==undefined){item.price=0;} ;
            //     //price in base
            //         if(conversionFactor==selectedConversionFactor){
            //             if(selectedConversionFactor>1){
            //                 item.newprice=parseFloat(item.price)*parseFloat(selectedConversionFactor);
            //             }
            //             else if(selectedConversionFactor<1){
            //                 item.newprice=parseFloat(item.price)/parseFloat(selectedConversionFactor);
            //             }
            //             else{
            //                 item.newprice=parseFloat(item.price);
            //             }
            //         }
            //         else if(conversionFactor>selectedConversionFactor){
            //             item.newprice=parseFloat(item.price)/parseFloat(selectedConversionFactor);
            //         }
            //         else if(conversionFactor<selectedConversionFactor){
            //             item.newprice=parseFloat(item.price)*parseFloat(selectedConversionFactor);
            //         }
            //         //if(item.newConFactor==undefined){
            //            item.newConFactor=selectedConversionFactor;
            //         //}
            //     //};
            // //};
            //price in preferred
            // if(item.newConFactor>selectedConversionFactor){
            //     //item.price=parseFloat(item.newprice)/parseFloat(item.newConFactor);
            //     item.price=parseFloat(item.newprice)/parseFloat(selectedConversionFactor);
            // }
            // else if(item.newConFactor<selectedConversionFactor){
            //     //item.price=parseFloat(item.newprice)*parseFloat(item.newConFactor);
            //     item.price=parseFloat(item.newprice)*parseFloat(selectedConversionFactor);
            // }
            item.price = parseFloat(item.newprice) * parseFloat(selectedConversionFactor);
            item.newConFactor = selectedConversionFactor;

            $scope.calculateAmt_entryForm(item.qty, item.price, item, item.vatPercent)
                //$scope.transaction.basePrefferedAnsSelectedUnitsByItem(item,unit,type);
                //}
        };

        function convertPriceInSelectedUnit_Sale(item, unit, type) {
            //if(item.preferedUnit!=undefined){

            //var pInd=_.findIndex(item.units,{_id:item.preferedUnit});
            if (item.selectedUnit != undefined) {
                var pInd = _.findIndex(item.units, { _id: item.selectedUnit._id });
            } else if (item.selectedUnitId != undefined) {
                var pInd = _.findIndex(item.units, { _id: item.selectedUnitId._id });
            } else {
                var unit = $scope.stockSaleForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (u != null) {
                        if (ii == item._id) {
                            item.selectedUnitId = u._id;
                            item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                        }
                    }
                });
                var pInd = _.findIndex(item.units, { _id: item.selectedUnit._id });
            }
            var preferedUnits = angular.copy(item.units[pInd]);
            var preferedconversionFactor = item.units[pInd].conversionFactor;
            var conversionFactor = 1;
            var selectedConversionFactor = 1;
            if (item.newConFactor == undefined) {
                item.newConFactor = preferedconversionFactor;
            }
            item.newprice = parseFloat(item.price) / parseFloat(item.newConFactor);
            _.forEach(unit, function(u, ii) {
                if (u != null) {
                    if (ii == item._id) {
                        selectedConversionFactor = u.conversionFactor;
                    }
                }
            });

            // //if(item.newprice==undefined && item.newConFactor==undefined){
            //     //if(item.newprice==undefined){
            //         if(item.price==undefined){item.price=0;} ;
            //     //price in base
            //         if(preferedconversionFactor==selectedConversionFactor){
            //             if(preferedconversionFactor>1){
            //                 item.newprice=parseFloat(item.price)*parseFloat(preferedconversionFactor);
            //             }
            //             else if(preferedconversionFactor<1){
            //                 item.newprice=parseFloat(item.price)/parseFloat(preferedconversionFactor);
            //             }
            //             else{
            //                 item.newprice=parseFloat(item.price);
            //             }
            //         }
            //         else if(preferedconversionFactor>selectedConversionFactor){
            //             item.newprice=parseFloat(item.price)/parseFloat(selectedConversionFactor);
            //         }
            //         else if(preferedconversionFactor<selectedConversionFactor){
            //             item.newprice=parseFloat(item.price)*parseFloat(selectedConversionFactor);
            //         }
            //         if(item.newConFactor==undefined){
            //             item.newConFactor=preferedconversionFactor;
            //         }
            //     //};
            // //};
            //     //price in preferred
            //     if(item.newConFactor>selectedConversionFactor){
            //         item.price=parseFloat(item.newprice)/parseFloat(item.newConFactor);
            //     }
            //     else if(item.newConFactor<selectedConversionFactor){
            //         item.price=parseFloat(item.newprice)*parseFloat(item.newConFactor);
            //     }
            item.price = parseFloat(item.newprice) * parseFloat(selectedConversionFactor);
            item.newConFactor = selectedConversionFactor;

            $scope.calculateAmt_saleForm(item.qty, item.price, item);
            //$scope.transaction.basePrefferedAnsSelectedUnitsByItem(item,unit,type);
            //}
        };

        function calculateTotal_Entry() {
            _.forEach($scope.stockEntryForm.availableItems, function(item, i) {
                if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && item.price != "" && item.price != undefined && !isNaN(item.price) && $scope.stockEntryForm.selectedUnits[item._id] != undefined && $scope.stockEntryForm.selectedUnits[item._id] != null) {

                }
            })
        };
        $scope.enableSubmitButton_Entry = function(items, selectedItem, unit) {
            if (selectedItem != undefined) {
                convertPriceInSelectedUnit_Entry(selectedItem, unit, 1);
            }

            var flag = false;
            _.forEach(items, function(item, i) {
                if (flag == false) {
                    //console.log(item.qty, item.price, $scope.stockEntryForm.selectedUnits[item._id]);
                    if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && item.price != "" && item.price != undefined && !isNaN(item.price) && $scope.stockEntryForm.selectedUnits[item._id] != undefined && $scope.stockEntryForm.selectedUnits[item._id] != null) {
                        flag = true;
                    }
                }
                //if(item.preferedUnit==undefined){
                _.forEach(unit, function(u, ii) {
                    if (u != null) {
                        if (ii == item._id) {
                            item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                            //item.preferedUnit=u._id;
                        }
                    }
                });
                //}
            });

            if (flag == true) {
                $scope.stockEntryForm.enableSubmit = false;

            } else {
                $scope.stockEntryForm.enableSubmit = true;
                growl.error('Please fill the full row before update', { ttl: 3000 });
            }
        };


        $scope.enableSubmitButton_PreparedFoodEntry = function() {

            var items = $scope.preparedFoodEntryForm.availableItems;
            var unit = $scope.preparedFoodEntryForm.selectedUnits;
            var flag = false;

            var flag = false;
            _.forEach(items, function(item, i) {
                if (flag == false) {
                    if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.preparedFoodEntryForm.selectedUnits[item._id] != undefined && $scope.preparedFoodEntryForm.selectedUnits[item._id] != null) {
                        flag = true;
                    }
                }
                _.forEach(unit, function(u, ii) {
                    if (u != null) {
                        if (ii == item._id) {
                            item.selectedUnitId = u._id;
                            item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                        }
                    }
                });

            });

            if (flag == true) {
                $scope.preparedFoodEntryForm.enableSubmit = false;
            } else {
                $scope.preparedFoodEntryForm.enableSubmit = true;
                growl.error('Please fill the full row before update', { ttl: 3000 });
            }

        };

        $scope.enableSubmitButton_Sale = function(items, item) {
            if (item != undefined) {
                convertPriceInSelectedUnit_Sale(item, $scope.stockSaleForm.selectedUnits, 2);
            }
            var flag = false;
            _.forEach(items, function(item, i) {
                if (flag == false) {
                    if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && item.price != "" && item.price != undefined && !isNaN(item.price) && $scope.stockSaleForm.selectedUnits[item._id] != undefined && $scope.stockSaleForm.selectedUnits[item._id] != null) {
                        flag = true;
                    }
                }
                //if(item.preferedUnit==undefined){
                var unit = $scope.stockSaleForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (u != null) {
                        if (ii == item._id) {
                            item.selectedUnitId = u._id;
                            item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                        }
                    }
                });
                //}
                //item.selectedUnitId=item.selectedUnit._id;
                // if(item.price!="" && item.price!=undefined){
                //     if(item.preferedUnits!=undefined){item.preferedUnit=item.preferedUnits._id};
                //     convertPriceInSelectedUnit_Sale(item,$scope.stockSaleForm.selectedUnits,2);
                //     // var ss=$scope.transaction.basePrefferedAnsSelectedUnitsByItem(item,$scope.stockSaleForm.selectedUnits,2);
                //     // item.calculateInUnits=ss;
                //     //console.log(ss);
                // }
            });

            if (flag == true) {
                $scope.stockSaleForm.enableSubmit = false;
            } else {
                $scope.stockSaleForm.enableSubmit = true;
                growl.error('Please fill the full row before update', { ttl: 3000 });
            }
        };

        $scope.enableSubmitButton_Physical = function(items) {
            var flag = false;
            _.forEach(items, function(item, i) {
                if (flag == false) {
                    if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.physicalStockForm.selectedUnits[item._id] != undefined && $scope.physicalStockForm.selectedUnits[item._id] != null) {
                        //if(parseFloat(item.qty)>0 && parseFloat(item.qty)<=parseFloat(item.availableStock)){
                        flag = true;
                        //}
                    }
                }
                var unit = $scope.physicalStockForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (u != null) {
                        if (ii == item._id) {
                            item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                            if (item.availableStock != 0) {
                                item.availableStock = parseFloat(parseFloat(item.closingQtyInBase) / parseFloat(u.conversionFactor)).toFixed(3);
                            }
                        }
                    }
                });
            });

            if (flag == true) {
                $scope.physicalStockForm.enableSubmit = false;
                $scope.physicalStockForm.isOpen = true;
            } else {
                $scope.physicalStockForm.enableSubmit = true;
                // var ss=$scope.transaction.basePrefferedAnsSelectedUnitsByItem(items,$scope.physicalStockForm.selectedUnits,3);
                // item.calculateInUnits=ss;
                // console.log(ss);
                growl.error('Please fill the full row before update', { ttl: 3000 });
            }
        };

        $scope.enableSubmitButton_Wastage = function(items) {
            var flag = false;
            console.log('items', items);
            _.forEach(items, function(item, i) {
                if (flag == false) {
                    if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.wastageEntryForm.selectedUnits[item._id] != undefined && $scope.wastageEntryForm.selectedUnits[item._id] != null) {
                        flag = true;
                    }
                }

                var unit = $scope.wastageEntryForm.selectedUnits;
                _.forEach(unit, function(u, ii) {
                    if (u != null) {
                        if (ii == item._id) {
                            item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                        }
                    }
                });
            });


            if (flag == true) {
                $scope.wastageEntryForm.enableSubmit = false;
                $scope.wastageEntryForm.isOpen = true;
            } else {
                //var ss=$scope.transaction.basePrefferedAnsSelectedUnitsByItems(items,$scope.wastageEntryForm.selectedUnits);
                //item.calculateInUnits=ss;
                //console.log(ss);
                $scope.wastageEntryForm.enableSubmit = true;
                growl.error('Please fill the full row before update', { ttl: 3000 });
            }
        };

        function validateStockTransfer(items) {
            var flag = true;
            _.forEach(items, function(itm, i) {
                if (itm.qty == "" && itm.qty == undefined && $scope.stockTransferForm.toStoreCategory[itm._id] == null && $scope.stockTransferForm.selectedUnits[itm._id] == null) {} else if (itm.qty != "" && itm.qty != undefined && $scope.stockTransferForm.toStoreCategory[itm._id] != null && $scope.stockTransferForm.toStoreCategory[itm._id] != undefined && $scope.stockTransferForm.selectedUnits[itm._id] != null && $scope.stockTransferForm.selectedUnits[itm._id] != undefined) {} else {
                    flag = false;
                }
            });
            return flag;
        };

        $scope.enableSubmitButton_Transfer = function(items) {
            var flag = false;
            var vTransfer = validateStockTransfer(items);
            if (vTransfer == true) {
                _.forEach(items, function(item, i) {
                    if (flag == false) {
                        if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.stockTransferForm.selectedUnits[item._id] != undefined && $scope.stockTransferForm.selectedUnits[item._id] != null && $scope.stockTransferForm.toStoreCategory[item._id] != undefined && $scope.stockTransferForm.toStoreCategory[item._id] != null) {
                            if (parseFloat(item.qty) > 0 && parseFloat(item.qty) <= parseFloat(item.availableStock)) {
                                flag = true;
                            }
                        }
                    }
                    console.log('$scope.stockTransferForm', $scope.stockTransferForm);
                    var unit = $scope.stockTransferForm.selectedUnits;
                    _.forEach(unit, function(u, ii) {
                        if (u != null) {
                            if (ii == item._id) {
                                item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                                if (item.availableStock != 0) {
                                    item.availableStock = parseFloat(parseFloat(item.closingQtyInBase) / parseFloat(u.conversionFactor)).toFixed(3);
                                }
                            }
                        }
                    });
                });

                if (flag == true) {
                    $scope.stockTransferForm.isOpen = true;
                    $scope.stockTransferForm.enableSubmit = false;
                } else {
                    $scope.stockTransferForm.enableSubmit = true;
                    growl.error('Please fill the full row before update', { ttl: 3000 });
                }
            } else {
                _.forEach(items, function(item, i) {
                    var unit = $scope.stockTransferForm.selectedUnits;
                    _.forEach(unit, function(u, ii) {
                        if (u != null) {
                            if (ii == item._id) {
                                console.log('ii', ii);
                                console.log('u', u);
                                console.log('item', item);
                                item.selectedUnit = { _id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor };
                                if (item.availableStock != 0) {
                                    item.availableStock = parseFloat(parseFloat(item.closingQtyInBase) / parseFloat(u.conversionFactor)).toFixed(3);
                                }
                            }
                        }
                    });
                });
                $scope.stockTransferForm.enableSubmit = true;
                growl.error('Entry rows are either filled or unfilled.', { ttl: 3000 });
            }
        };

        function manageClosingQty(items, storeId, toStoreId, type) {
            if (type == 1) {
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: storeId });
                    var bIndex = _.findIndex(itm.calculateInUnits, { type: 'baseUnit' });

                    var qtyInbase = 0;
                    if (itm.calculateInUnits[bIndex].id == 2 || itm.calculateInUnits[bIndex].id == 3) {
                        qtyInbase = parseFloat(parseFloat(itm.calculateInUnits[bIndex].baseQty) * 1000).toFixed(3);
                    } else {
                        qtyInbase = parseFloat(itm.calculateInUnits[bIndex].baseQty).toFixed(3);
                    }

                    if (index < 0) {
                        var obj = { itemId: itm._id, storeId: storeId, closingQtyInBase: parseFloat(qtyInbase) };
                        $scope.closingQty.push(obj);
                    } else {
                        $scope.closingQty[index].closingQtyInBase = parseFloat(parseFloat($scope.closingQty[index].closingQtyInBase) + parseFloat(qtyInbase)).toFixed(3);
                    }
                });
            } else if (type == 2) {
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: storeId });
                    var index = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: storeId });
                    var bIndex = _.findIndex(itm.calculateInUnits, { type: 'baseUnit' });
                    var qtyInbase = 0;
                    if (itm.calculateInUnits[bIndex].id == 2 || itm.calculateInUnits[bIndex].id == 3) {
                        qtyInbase = parseFloat(parseFloat(itm.calculateInUnits[bIndex].baseQty) * 1000).toFixed(3);
                    } else {
                        qtyInbase = parseFloat(itm.calculateInUnits[bIndex].baseQty).toFixed(3);
                    }

                    if (index < 0) {
                        var obj = { itemId: itm._id, storeId: storeId, closingQtyInBase: parseFloat(-qtyInbase) };
                        $scope.closingQty.push(obj);
                    } else {
                        $scope.closingQty[index].closingQtyInBase = parseFloat(parseFloat($scope.closingQty[index].closingQtyInBase) - parseFloat(qtyInbase)).toFixed(3);
                    }
                });
            } else if (type == 3) {
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: storeId });
                    var bIndex = _.findIndex(itm.calculateInUnits, { type: 'baseUnit' });
                    var qtyInbase = 0;
                    if (itm.calculateInUnits[bIndex].id == 2 || itm.calculateInUnits[bIndex].id == 3) {
                        qtyInbase = parseFloat(parseFloat(itm.calculateInUnits[bIndex].baseQty) * 1000).toFixed(3);
                    } else {
                        qtyInbase = parseFloat(itm.calculateInUnits[bIndex].baseQty).toFixed(3);
                    }

                    if (index < 0) {
                        var obj = { itemId: itm._id, storeId: storeId, closingQtyInBase: parseFloat(qtyInbase) };
                        $scope.closingQty.push(obj);
                    } else {
                        //$scope.closingQty[index].closingQtyInBase=parseFloat(parseFloat(itm.calculateInUnits[bIndex].baseQty)).toFixed(3);
                    }
                });
            } else if (type == 4) {
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: storeId });
                    var bIndex = _.findIndex(itm.calculateInUnits, { type: 'baseUnit' });
                    var qtyInbase = 0;
                    if (itm.calculateInUnits[bIndex].id == 2 || itm.calculateInUnits[bIndex].id == 3) {
                        qtyInbase = parseFloat(parseFloat(itm.calculateInUnits[bIndex].baseQty) * 1000).toFixed(3);
                    } else {
                        qtyInbase = parseFloat(itm.calculateInUnits[bIndex].baseQty).toFixed(3);
                    }

                    if (index < 0) {
                        var obj = { itemId: itm._id, storeId: storeId, closingQtyInBase: parseFloat(-qtyInbase) };
                        $scope.closingQty.push(obj);
                    } else {
                        $scope.closingQty[index].closingQtyInBase = parseFloat(parseFloat($scope.closingQty[index].closingQtyInBase) - parseFloat(qtyInbase)).toFixed(3);
                    }
                });
            } else if (type == 5) {
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: storeId });
                    var bIndex = _.findIndex(itm.calculateInUnits, { type: 'baseUnit' });
                    var qtyInbase = 0;
                    if (itm.calculateInUnits[bIndex].id == 2 || itm.calculateInUnits[bIndex].id == 3) {
                        qtyInbase = parseFloat(parseFloat(itm.calculateInUnits[bIndex].baseQty) * 1000).toFixed(3);
                    } else {
                        qtyInbase = parseFloat(itm.calculateInUnits[bIndex].baseQty).toFixed(3);
                    }

                    if (index < 0) {
                        var obj = { itemId: itm._id, storeId: storeId, closingQtyInBase: parseFloat(-qtyInbase) };
                        $scope.closingQty.push(obj);
                    } else {
                        $scope.closingQty[index].closingQtyInBase = parseFloat(parseFloat($scope.closingQty[index].closingQtyInBase) - parseFloat(qtyInbase)).toFixed(3);
                    }
                });
                _.forEach(items, function(itm, i) {
                    var index = _.findIndex($scope.closingQty, { itemId: itm._id, storeId: toStoreId });
                    var bIndex = _.findIndex(itm.calculateInUnits, { type: 'baseUnit' });
                    var qtyInbase = 0;
                    if (itm.calculateInUnits[bIndex].id == 2 || itm.calculateInUnits[bIndex].id == 3) {
                        qtyInbase = parseFloat(parseFloat(itm.calculateInUnits[bIndex].baseQty) * 1000).toFixed(3);
                    } else {
                        qtyInbase = parseFloat(itm.calculateInUnits[bIndex].baseQty).toFixed(3);
                    }

                    if (index < 0) {
                        var obj = { itemId: itm._id, storeId: storeId, closingQtyInBase: parseFloat(qtyInbase) };
                        $scope.closingQty.push(obj);
                    } else {
                        $scope.closingQty[index].closingQtyInBase = parseFloat(parseFloat($scope.closingQty[index].closingQtyInBase) + parseFloat(qtyInbase)).toFixed(3);
                    }
                });
            }
        };

        function registerEvent(eventName) {
            var event = {
                event_name: eventName,
                user_id: localStorageService.get('deployment_id'),
                created: Date.parse(new Date() / 1000)
            };
            intercom.sendEvent(event);
        }

        $scope.checkForBackDateEntry = function(v) {
            v.open = true;
            if (Utils.isUserPermitted(currentUser, 'Allow Back Date Entry')) {
                v.open = true;
            }
        }



        $scope.stockEntryForm.enableCharges = {
            value: false
        };

        $scope.stockEntryCharges = {
            chargeName: '',
            chargeValue: 0,
            type: 'percent',
            operationType : 'additive',
            isEditable : false
        };

        $scope.stockEntryForm.charges = [];

        $scope.addChargesToEntryForm = function() {
            if(!$scope.stockEntryForm.charges)
              $scope.stockEntryForm.charges = [];
            $scope.stockEntryForm.charges.push({
              chargeName: null,
              chargeValue: null,
              type: 'percent',
              operationType : 'additive',
              isEdit: true
            });
            $scope.checkStockEntryForOpenTransactionAndSave();
            checkIfAllChargesApplied();
        }

        $scope.removeCharge = function (charge, index) {
          if(confirm("Are you sure you want to remove " + charge.chargeName + "?")){
            $scope.stockEntryForm.charges.splice(index, 1);
            $scope.calculateAmt_entryForm();
            checkIfAllChargesApplied();
            $scope.checkStockEntryForOpenTransactionAndSave();
          }
        }

        $scope.acceptCharge = function(charge, index,formStockEntry) {
            console.log('$scope.stockEntryForm.charges',$scope.stockEntryForm.charges)
          console.log('acceptCharge', charge);
            if(charge.chargeName && charge.chargeValue && !isNaN(charge.chargeValue) && charge.operationType && charge.type) {
                    charge.isEdit = false;
                    $scope.calculateAmt_entryForm();
                    checkIfAllChargesApplied();
                    $scope.checkStockEntryForOpenTransactionAndSave();
              } else {
                growl.error("Please complete the charge details.", {ttl: 3000});
              }
          }
        
        $scope.changeChargeEditStatus = function(index, operationType) {
            $scope.stockEntryForm.charges[index].isEdit = true;
            $scope.stockEntryForm.charges[index].ope
            checkIfAllChargesApplied();
            $scope.checkStockEntryForOpenTransactionAndSave();
        }

        $scope.removeChargesFromEntryForm = function(index) {
            $scope.stockEntryForm.charges.splice(index, 1);
            $scope.calculateAmt_entryForm();
            checkIfAllChargesApplied();
            $scope.checkStockEntryForOpenTransactionAndSave();
        }
        function checkIfAllChargesApplied() {
            console.log('checkIfAllChargesApplied',$scope.stockEntryForm.charges);
            var flag = true;
            for (var i = 0; i<$scope.stockEntryForm.charges.length; ++i) {
                var charge = $scope.stockEntryForm.charges[i];
                if(!charge.chargeName || !charge.chargeValue || isNaN(charge.chargeValue) || !charge.operationType || !charge.type || charge.isEdit == true) {
                console.log('if checkIfAllChargesApplied');
                    flag = true;
                    break;
                }
                else
                {
                    console.log('else checkIfAllChargesApplied');
                    flag = false;
                }
            };
            $scope.stockEntryForm.enableSubmit = flag;
        }


       


    }]);
