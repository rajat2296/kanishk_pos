'use strict';

describe('Controller: StockEntryCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StockEntryCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StockEntryCtrl = $controller('StockEntryCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
