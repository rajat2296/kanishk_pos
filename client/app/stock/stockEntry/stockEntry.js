'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('stockEntry', {
        url: '/stockEntry',
        templateUrl: 'app/stock/stockEntry/stockEntry.html',
        controller: 'StockEntryCtrl',
        resolve:{
          // availableStore:['$rootScope', '$state', '$stateParams', 'Auth','store' , 'growl',
          //               function ($rootScope, $state, $stateParams, Auth, store,growl) {
          //                  return store.get({tenant_id:$rootScope.currentUser.tenant_id,deployment_id:$rootScope.currentUser.deployment_id}).$promise.then (function (stores){
          //                       $rootScope.availableStore=stores;
          //                       return stores;
          //                     });
                            
          //               }],
          // stores: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {
          //               /* Items Table */
          //               $rootScope.stores = [];
          //               return stockSync.syncStores($rootScope.currentUser).then(function (stores) {
          //                   return $rootScope.stores = stores;
          //               });

          //           }],
          // receivers: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {

          //               /* Items Table */
          //               $rootScope.receivers = [];
          //               return stockSync.syncReceivers($rootScope.currentUser).then(function (receivers) {
          //                   return $rootScope.receivers = receivers;
          //               });

          //           }],
          // vendors: ['$rootScope', 'stockSync', function ($rootScope, stockSync) {

          //               /* Items Table */
          //               $rootScope.vendors = [];
          //               return stockSync.syncVendors($rootScope.currentUser).then(function (vendors) {
          //                   return $rootScope.vendors = vendors;
          //               });

          //           }]
        }
      });
  });