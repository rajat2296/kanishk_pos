'use strict';

angular.module('posistApp')
  .controller('EditentriesCtrl',['$q','$resource','$scope','$filter','growl','store','currentUser','$rootScope','$state','$modal','localStorageService','currentDeployment','Utils','property', function ($q,$resource,$scope,$filter,growl,store,currentUser,$rootScope,$state,$modal,localStorageService,currentDeployment,Utils,property) {
    currentUser.deployment_id=localStorageService.get('deployment_id');
    $scope.stores=$rootScope.stores;
    $scope.vendors=$rootScope.vendors;
    $scope.receivers=$scope.receivers;
    $scope.user=currentUser;
    $scope.stockTransactionsTodb = $resource('/api/stockTransactions/:controller/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray:false},
        get: {method: 'GET', isArray: true},
        find:{method: 'GET'},
        update:{method:'PUT'},
        getAllByTranasactionAndDateRange: {method: 'GET', params: {controller: "getAllByTranasactionAndDateRange"}, isArray: true}
      }
    );
  $scope.resetSerialNumber=false
    $scope.hidePricing=false
  // property.get({
  //     tenant_id: localStorageService.get("tenant_id"),
  //     deployment_id: localStorageService.get("deployment_id")
  //   }, function (properties) {
  //       if(properties.length==0)
  //     {
       
  //        $scope.resetSerialNumber=false
  //     }
  //     else
  //     {

  //       if(properties[0].resetSerialNumber==undefined)
  //         $scope.resetSerialNumber=true
  //       else  
  //         $scope.resetSerialNumber=properties[0].resetSerialNumber
  //         console.log("reset serial ",$scope.resetSerialNumber);
  //     }
  //     // stockProperty = properties[0];
  //     // if (!stockProperty.property.hidePricingEnabled)
  //     //   $scope.hidePricing = false;
  //     // else {
  //     //   _.forEach(currentUser.selectedRoles, function (role) {
  //     //     if (stockProperty.property.hidePricing[role.name]) {
  //     //       $scope.hidePricing = true;
  //     //     }
  //     //   });
  //     // }
  //   });

    function getYesterDayDate() {
      var d=new Date();
      d.setDate(d.getDate()-1);
      return d;//
    };

    function parseDate(str)
  {
      // var s = str.split(" "),
      //     d = s[0].split("-"),
      //     t = s[1].replace(/:/g, "");
      // return d[2] + d[1] + d[0] + t;
      var d=$filter('date')(str, 'dd-MMM-yyyy');
      return d;
  };

    $scope.editEntryForm={
      fromDate:getYesterDayDate(),      
      toDate:new Date(),
      maxDate:new Date()
    };
    $scope.editEntryForm.stores=angular.copy($scope.stores);
    $scope.checkDateGreater= function(date){
      var d=new Date(date);
      var dd=new Date($scope.editEntryForm.toDate);
      if(d.getTime()>dd.getTime()){
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.editEntryForm.fromDate=getYesterDayDate()
      }
      else
      {
        //growl.success('Lesser than toDate', {ttl: 3000}); 
      }

    };

    $scope.checkDateLesser= function(date){
      var d=new Date(date);
      var dd=new Date($scope.editEntryForm.fromDate);
      if(d.getTime()<dd.getTime()){
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.editEntryForm.toDate= new Date();
      }
      else
      {
        //growl.success('Greater than fromDate', {ttl: 3000});  
      }
    };

    function bindVendor(store){
      $scope.editEntryForm.vendorReceiver=[];
      if(store!=null){
        _.forEach($scope.vendors, function(v,i){
          if(v.pricing!=undefined){
            if(v.pricing._id==store._id){
              v.name=v.vendorName;
              $scope.editEntryForm.vendorReceiver.push(v);
            } 
          }
        });
      }
    };
    function bindReceiver(store){
      $scope.editEntryForm.vendorReceiver=[];
      var receivers=angular.copy($scope.receivers);
        if(store!=null){
            _.forEach(receivers, function(r,i){
              // if(r.pricing!=undefined){
              //     _.forEach(r.pricing.store, function(s,ii){
              //         if(s._id==store._id){
                    r.name=r.receiverName;
                          $scope.editEntryForm.vendorReceiver.push(r);
              //         }
              //     });                   
              // }
          });
        }
    };

    $scope.bindVenRec_editEntryForm= function(store){
      $scope.editEntryForm.availableRecords=[];
      $scope.editEntryForm.vendor={};
      var entryType=$scope.editEntryForm.entryType;
      if(entryType==1){
        //bindVendor
        bindVendor(angular.copy(store));
      }
      else if(entryType==2){
        //bind REceiver
        bindReceiver(store);
      }
    };

    $scope.bindStore_editEntryForm= function(entryType){
      $scope.editEntryForm.stores=[];
      $scope.editEntryForm.vendorReceiver=[];
      $scope.editEntryForm.availableRecords=[];
      $scope.editEntryForm.stores=angular.copy($scope.stores);
    };


    $scope.startSearch= function(){
      $scope.editEntryForm.availableRecords=[];
        var fromDate=new Date($scope.editEntryForm.fromDate);
      fromDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(fromDate)))); 
      var toDate=new Date($scope.editEntryForm.toDate);
       toDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(toDate)))); 
        
          var strId="";
          if($scope.editEntryForm.store!=undefined){
            strId=$scope.editEntryForm.store._id;
          }

          var venId="";
          if($scope.editEntryForm.vendor!=undefined){
            venId=$scope.editEntryForm.vendor._id;
          }

          if($scope.editEntryForm.entryType=="1"){
            var req={"fromDate":fromDate,"toDate":toDate,"transactionType":$scope.editEntryForm.entryType,"_store._id":strId,"_store.vendor._id":venId,deployment_id:currentUser.deployment_id,tenant_id:currentUser.tenant_id};          
          }
          else if($scope.editEntryForm.entryType=="2")
          {
            var req={"fromDate":fromDate,"toDate":toDate,"transactionType":$scope.editEntryForm.entryType,"_store._id":strId,"_store.receiver._id":venId,deployment_id:currentUser.deployment_id,tenant_id:currentUser.tenant_id};          
          }
          else{
            var req={"fromDate":fromDate,"toDate":toDate,"transactionType":$scope.editEntryForm.entryType,"_store._id":strId,deployment_id:currentUser.deployment_id,tenant_id:currentUser.tenant_id};  
          }          
          
          $scope.stockTransactionsTodb.getAllByTranasactionAndDateRange(req, function (result){
            console.log('data',result);
            calculateQtyAmtAndVatBillWise(result);
            $scope.editEntryForm.availableRecords=result;
            console.log(result);        
          });
    };

    $scope.editEditEntryForm= function(item){
      $state.go('stock.editStockEntry',{id:item._id,transactionType:$scope.editEntryForm.entryType});
    };

    function calculateForStockEntry(result){

 _.forEach(result, function(r,iii){
  _.forEach(r.store,function(s,i)
  {
    if(!s.category)
    {
      result.splice(r);
    }
  });
  //console.log('-------------------New result-------------------')
  var tQty=0;
  var tAmt=0;
  var dAmt=0;
  var tvat=0;
  var cartage=0;
  var vs=0;
  var disTotal=0,vatTotal=0,sumvs=0;
  _.forEach(r._store.vendor.category, function(c,ii){
    
   _.forEach(c.items, function(itm,i){
    tAmt+=parseFloat(itm.subTotal);
   });
   _.forEach(c.items, function(itm,i){
    //console.log('---------------New Item------------------')
    tQty+=parseFloat(itm.qty);
    //tAmt+=parseFloat(itm.subTotal);
         //tvat+=parseFloat(itm.addedAmt);

         var dis=0,disAmount=0,perDis=0;
         if(r.discount!=undefined && r.discount!=0){
                  if(r.discountType=="percent"){
                    //console.log('total',tAmt);
                    dis= parseFloat(tAmt*0.01*r.discount).toFixed(2);
                    perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(tAmt).toFixed(2)).toFixed(2);
                    //console.log('dis', dis);
                    //console.log('perDis', perDis)
                    //console.log('itm.total',itm.subTotal);
                    disAmount=parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2);
                    //console.log('disAmount',disAmount);
                      if(itm.vatPercent)
                      {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',itm.vatPercent);
                        vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                      }
                      else
                      {
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                      }
                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                    //console.log('disTotal',disTotal);
                  }
                  else{
                    //console.log('total',tAmt);
                    dis= parseFloat(r.discount).toFixed(2);
                    //perDis=parseFloat(dis/numberOfItems).toFixed(2);
                    perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(tAmt).toFixed(2)).toFixed(2);
                    //console.log('dis', dis);
                    //console.log('perDis', perDis);
                    //console.log('itm.total',itm.subTotal);
                    disAmount=parseFloat(parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2)).toFixed(2);
                    //console.log('disAmount',disAmount);
                      if(itm.vatPercent)
                      {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',itm.vatPercent);
                        vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                      }
                      else
                      {
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);  
                      }
                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                    //console.log('disTotal',disTotal);
                    }
                }
                else
                {
                  if(itm.vatPercent)
                  {
                    //console.log('vatTotal vefore vat',vatTotal);
                    //console.log('itm.vatPercent',itm.vatPercent);
                    vs=parseFloat(itm.vatPercent*0.01*parseFloat(itm.subTotal).toFixed(2)).toFixed(2);
                    //console.log('vs',vs);
                    //console.log('disAmount before vat',disAmount);
                    disAmount=parseFloat(parseFloat(itm.subTotal)+parseFloat(vs)).toFixed(2);
                        
                    //disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                    //console.log('disAmount after vat',disAmount);
                    disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                    //console.log('disTotal',disTotal);
                    vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                    //console.log(disAmount);
                    //console.log('vatTotal after vat',vatTotal);
                  }
                else
                  {
                    disTotal=parseFloat(parseFloat(disTotal)+parseFloat(itm.subTotal)).toFixed(2);
                    //console.log('disTotal',disTotal);
                   
                  }                  
                }
   });
  });
     var dis=0,amtafterDiscount=0, netAdditionalCharge = 0;
     if(r.discount!=0 && r.discount!=undefined && r.discount!=null){
       if(r.discountType=="percent"){
         dis=parseFloat(tAmt*0.01*parseFloat(r.discount)).toFixed(2);
         amtafterDiscount=parseFloat(tAmt)-parseFloat(dis);
       }
       else{
         dis=parseFloat(r.discount).toFixed(2);
         amtafterDiscount=parseFloat(tAmt)-parseFloat(dis);
       }
     }
     if(r.charges)
                    {
                        console.log('amtafterDiscount',amtafterDiscount);
                        _.forEach(r.charges , function(charge) {
                            if(charge.operationType == 'additive')
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue * 0.01 * amtafterDiscount)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * amtafterDiscount).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                            else
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue * 0.01 * amtafterDiscount)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * amtafterDiscount).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                        });
                    }
     //var amtafterDiscount=parseFloat(tAmt)-parseFloat(dAmt);
     /*if(r.vat!=0 && r.vat!=undefined && r.vat!=null){
       if(r.vatType=="percent"){
         //tvat=parseFloat(amtafterDiscount*0.01*parseFloat(r.vat)).toFixed(2);
         tvat= parseFloat(parseFloat(r.vat*0.01*parseFloat(amtafterDiscount))).toFixed(2);
       }
       else{
         tvat=parseFloat(r.vat).toFixed(2);
       }
     }*/
     r.totalQty=parseFloat(tQty).toFixed(2);
     r.totalDiscount=parseFloat(dis).toFixed(2);
     //var amtafterDiscount2=parseFloat(tAmt)-parseFloat(amtafterDiscount);
     if(r.cartage!=undefined){
       r.cartage=parseFloat(r.cartage).toFixed(2);
     }
     r.totalAmt=parseFloat(parseFloat(tAmt)).toFixed(2);
  r.totalVat=parseFloat(vatTotal).toFixed(2);
  r.netAdditionalCharge = parseFloat(netAdditionalCharge).toFixed(2);
 });
};
 function calculateForStockSale(result){
   _.forEach(result, function(r,iii){
     var tQty=0;
     var tAmt=0;
     var dAmt=0;
     var tvat=0;
     var tSc=0;
     var tcartage=0;
     var tpay=0;
     _.forEach(r._store.receiver.category, function(c,ii){
       _.forEach(c.items, function(itm,i){
         tQty+=parseFloat(itm.qty);
         tAmt+=parseFloat(parseFloat(itm.qty)*parseFloat(itm.price));
       });
     });
     r.totalQty=parseFloat(tQty).toFixed(2);
     r.totalAmt=parseFloat(tAmt).toFixed(2);
     if(r.discount!=0 && r.discount!=undefined && r.discount!=null){
       if(r.discountType=="percent"){
         dAmt=parseFloat(tAmt*0.01*parseFloat(r.discount)).toFixed(2);
       }
       else{
         dAmt=parseFloat(r.discount).toFixed(2);
       }
     }
     var amtafterDiscount=parseFloat(tAmt)-parseFloat(dAmt);
     if(r.vat!=0 && r.vat!=undefined && r.vat!=null){
       if(r.vatType=="percent"){
         tvat=parseFloat(amtafterDiscount*0.01*parseFloat(r.vat)).toFixed(2);
       }
       else{
         tvat=parseFloat(r.vat).toFixed(2);
       }
     }
     if(r.serviceCharge!=undefined && r.serviceCharge!=null){
       if(r.serviceChargeType=="percent"){
         tSc=parseFloat(amtafterDiscount*0.01*parseFloat(r.serviceCharge)).toFixed(2);
       }
       else
       {
         tSc=parseFloat(r.serviceCharge).toFixed(2);
       }
     }
     if(r.cartage!=undefined && r.cartage!=null){
       tcartage=parseFloat(r.cartage).toFixed(2);
     }
     if(r.payment!=undefined && r.payment!=null){
       tpay=parseFloat(r.payment).toFixed(2);
     }
     r.totalDiscount=parseFloat(dAmt).toFixed(2);
     r.totalVat=parseFloat(tvat).toFixed(2);
     r.totalAmt=parseFloat(parseFloat(tAmt)-parseFloat(dAmt)+parseFloat(tSc)+parseFloat(tvat)+parseFloat(tcartage)+parseFloat(tpay)).toFixed(2);
   });
 };
    function calculateForStockPWS(result){
      console.log(result)
      var arr =[];
    /*_.forEach(result,function(r,i)
    {
      var flag =false;
      _.forEach(r._store,function(c,ii)
      {
      if(!c.category)
      {
        flag = true;
      }  
      });
      if(flag == false)
      arr.push(r);
    });*/      
      _.forEach(result, function(r,iii){
        var tQty=0;
        var tAmt=0;
        var dAmt=0;
        var tvat=0;
         
        _.forEach(r._store.category, function(c,ii){
          _.forEach(c.items, function(itm,i){
            tQty+=parseFloat(itm.qty);
            //tAmt+=(itm.qty)*(itm.price);
          });         
        });
        r.totalQty=parseFloat(tQty).toFixed(2);
        r.totalAmt=parseFloat(tAmt).toFixed(2);
        r.totalDiscount=parseFloat(dAmt).toFixed(2);
        r.totalVat=parseFloat(tvat).toFixed(2);
        
      });
    };
    function calculateQtyAmtAndVatBillWise(result){
      _.forEach(result, function(r,i){
        if(r.transactionType==1){
          calculateForStockEntry(result);
        } 
        else if(r.transactionType==2){
          calculateForStockSale(result);
        }       
        else {
            /*var flag = false;
            _.forEach(r,function(b,ii)
            {
            if(b.category == undefined)
              {
                flag =true;
              }
          });
            if(flag == true)*/
            
          calculateForStockPWS(result);
        }
      })
    };

    $scope.historyEditEntryForm = function (item) {
      console.log('historyEditEntryForm',item);
            var deployment_id=$scope.user.deployment_id;
            var tenant_id=$scope.user.tenant_id;
            var transactionId=item.transactionId;
            var billAmount=item.totalAmt;
            var dueAmount=item.totalDue;
            var tPaid=item.totalPaid;
            var mainTranasctionId=item._id;
            if($scope.resetSerialNumber)
                var billNo=item.daySerialNumber;
            else
              var billNo=item.transactionNumber;
            var user=$scope.user;
            var billDate=item.created;
            $modal.open({
                templateUrl: 'app/stock/editentries/_editTransatcionHistory.html',
                controller: ['$rootScope', '$scope', '$resource', '$modalInstance','EditTransactionHistory','growl', function ($rootScope, $scope, $resource, $modalInstance,EditTransactionHistory,growl) {
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.eHistory={};
                    $scope.historys=[];
                    $scope.eHistory.billNo=billNo;
                    $scope.eHistory.billAmount=billAmount;
                    $scope.eHistory.dueAmount=dueAmount;
                    $scope.eHistory.deployment_id= deployment_id;
                    $scope.eHistory.tenant_id= tenant_id;
                    $scope.eHistory.transactionId= transactionId;
                    $scope.eHistory.totalPaid=tPaid;
                    $scope.eHistory.mainTranasctionId=mainTranasctionId;
                    $scope.eHistory.user=user;
                    $scope.eHistory.billDate=billDate;

                    EditTransactionHistory.getHistoryByMainTransactionId({mainTransactionId:$scope.eHistory.mainTranasctionId}).$promise.then( function (result){
                    console.log('edit History result',result);
                    _.forEach(result,function(r,i1)
                    {
                    var dis=0,disAmount=0,vs=0,tAmt=0;;
                    _.forEach(r.items,function(itm,i2)
                    {
                    tAmt+=itm.subTotal; 
                    });

                    _.forEach(r.items,function(itm,i1)
                    {
                      var dis=0,disAmount=0,perDis=0;
                     if(r.discount!=undefined && r.discount!=0){
                              if(r.discountType=="percent"){
                                //console.log('total',tAmt);
                                dis= parseFloat(tAmt*0.01*r.discount).toFixed(2);
                                perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(tAmt).toFixed(2)).toFixed(2);
                                //console.log('dis', dis);
                                //console.log('perDis', perDis)
                                //console.log('itm.total',itm.subTotal);
                                disAmount=parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2);
                                //console.log('disAmount',disAmount);
                                  if(itm.vatPercent)
                                  {
                                    //console.log('vatTotal vefore vat',vatTotal);
                                    //console.log('itm.vatPercent',itm.vatPercent);
                                    vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                                    //console.log('vs',vs);
                                    itm.vatAmount=vs;
                                  }
                                  else
                                  {
                                    //disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                                    //console.log('disTotal',disTotal);
                                  }
                                //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                                //console.log('disTotal',disTotal);
                              }
                              else{
                                //console.log('total',tAmt);
                                dis= parseFloat(r.discount).toFixed(2);
                                //perDis=parseFloat(dis/numberOfItems).toFixed(2);
                                perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(tAmt).toFixed(2)).toFixed(2);
                                //console.log('dis', dis);
                                //console.log('perDis', perDis);
                                //console.log('itm.total',itm.subTotal);
                                disAmount=parseFloat(parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2)).toFixed(2);
                                //console.log('disAmount',disAmount);
                                  if(itm.vatPercent)
                                  {
                                    //console.log('vatTotal vefore vat',vatTotal);
                                    //console.log('itm.vatPercent',itm.vatPercent);
                                    vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                                    //console.log('vs',vs);
                                    itm.vatAmount=vs;
                                  }
                                  else
                                  {
                                    //disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                                    //console.log('disTotal',disTotal);  
                                  }
                                //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                                //console.log('disTotal',disTotal);
                                }
                            }
                            else
                            {
                              if(itm.vatPercent)
                              {
                                //console.log('vatTotal vefore vat',vatTotal);
                                //console.log('itm.vatPercent',itm.vatPercent);
                                vs=parseFloat(itm.vatPercent*0.01*parseFloat(itm.subTotal).toFixed(2)).toFixed(2);
                                //console.log('vs',vs);
                                itm.vatAmount=vs; 
                              }
                            else
                              {
                                //disTotal=parseFloat(parseFloat(disTotal)+parseFloat(itm.subTotal)).toFixed(2);
                                //console.log('disTotal',disTotal);
                               
                              }                  
                            }
                    });
                    });
  console.log('edit get result',result);
  $scope.historys=result;
                    });
                }],
                size: 'lg'
            });
    };
  }]);
