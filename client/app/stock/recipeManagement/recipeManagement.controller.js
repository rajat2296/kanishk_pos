'use strict';

angular.module('posistApp')
  .controller('RecipeManagementCtrl', ['$rootScope', '$q', '$scope', 'currentUser', 'Deployment', 'Category', 'Item', 'stockUnit', 'stockItem', 'stockRecipe', '$resource', 'growl', '$modal', 'localStorageService', 'StockReport', 'store', 'baseKitchenItem', 'baseKitchenUnit', 'Utils', 'vendor', 'property', function ($rootScope, $q, $scope, currentUser, Deployment, Category, Item, stockUnit, stockItem, stockRecipe, $resource, growl, $modal, localStorageService, StockReport, store, baseKitchenItem, baseKitchenUnit, Utils, vendor, property) {
    $scope.deployment = {};
    $scope.categories = [];
    $scope.allCategories = [];
    $scope.spCategories = [];
    $scope.items = [];
    $scope.stockUnit = [];
    $scope.stockItem = [];
    $scope.stockRecipes = [];
    $scope.allTransactions = [];
    $scope.itemLastAndAveragePrice = [];
    $scope.recipeCategory = [];
    $scope.stores = [];
    $scope.stockSettings = [];
    var isYieldEnabled = false;

    //modified
    $scope.user = currentUser;
    $scope.utils = Utils;
    //end
    Deployment.findOne({}, {id: localStorageService.get('deployment_id')}, function (deployment){
      $scope.currentDeployment = deployment;
    });

    currentUser.deployment_id = localStorageService.get('deployment_id');
    disableEntryForVendor('Vendor');
    function disableEntryForVendor(name) {
      if (currentUser.selectedRoles.length == 1 && currentUser.selectedRoles[0].name == name) {
        $rootScope.$state.go('stock.indentGrn');
      }
    }

    $scope.stockTransactionsTodb = $resource('/api/stockTransactions/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        findOne: {method: 'GET'},
        update: {method: 'PUT'}
      }
    );
    $scope.startsWith = function (item, viewValue) {
      return item.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
    };
    function getDeploymentByTenant() {
      var deferred = $q.defer();
      Deployment.get({tenant_id: currentUser.tenant_id}, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return Deployment.get({tenant_id: currentUser.tenant_id});
    };

    function getStores_ByDeployment() {
      var deferred = $q.defer();
      store.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id}, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return store.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getCategories() {
      var deferred = $q.defer();
      Category.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      //return Category.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };
    function getItems() {
      var deferred = $q.defer();
      Item.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id}, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return Item.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getstockUnits() {
      var deferred = $q.defer();
      stockUnit.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return stockUnit.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getstockItems() {
      var deferred = $q.defer();
      stockItem.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return stockItem.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getstockRecies() {
      var deferred = $q.defer();
      stockRecipe.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return stockRecipe.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getTransactions() {
      var deferred = $q.defer();
      $scope.stockTransactionsTodb.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /* return $scope.stockTransactionsTodb.get({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    };

    function getRawPricing() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_Receipe({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /* return StockReport.RawMaterialPricing_Receipe({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    };

    function getRawPricingBasekitchen() {
      var deferred = $q.defer();
      StockReport.RawMaterialPricing_ReceipeBasekitchen({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      /*  return StockReport.RawMaterialPricing_ReceipeBasekitchen({
       tenant_id: currentUser.tenant_id,
       deployment_id: currentUser.deployment_id
       });*/
    }

    function getbaseKitchenItems() {
      var deferred = $q.defer();
      baseKitchenItem.getUniqueBaseKitchenItems({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      //  return baseKitchenItem.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id})
    }

    function getbaseKitchenUnits() {
      var deferred = $q.defer();
      baseKitchenUnit.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      //return baseKitchenUnit.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id})
    }

    function getStockSettings() {
      var deferred = $q.defer();
      property.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    // var allPromise=$q.all([
    //   getCategories() , getItems() , getstockUnits(), getstockItems() , getstockRecies()
    // ]);

    // allPromise.then(function (value){
    //  console.log(value[0]);
    //  console.log(value[1]);
    //  console.log(value[2]);
    //  console.log(value[3]);
    //  console.log(value[4]);
    // });

    var getDeployment = Deployment.get({tenant_id: currentUser.tenant_id}, function (dep) {
      $scope.deployment = dep;

      var allPromise = $q.all([
        getCategories(), getItems(), getstockUnits(), getstockItems(), getstockRecies(), getRawPricing(), getStores_ByDeployment(), getbaseKitchenItems(), getRawPricingBasekitchen(), getbaseKitchenUnits(), getStockSettings()
        //getTransactions()
      ]);

      allPromise.then(function (value) {
        $scope.allCategories = value[0];
        //CatWithSemiProcessed(categories);
        CatWithSemiProcessed(value[0]);
        $scope.items = value[1];
        $scope.stockItem = value[3];
        setItemCode();
        $scope.stockUnit = value[2];
        //$scope.stockItem=value[3];
        $scope.stockRecipes = value[4];
        //console.log('$scope.stockRecipes', $scope.stockRecipes)
        //$scope.recipeReportForm.items=value[4];

        //$scope.allTransactions=value[5];
        $scope.pricing = value[5];
        $scope.itemLastAndAveragePrice = value[5];

        //console.log('setItemCodeAndYieldCosting',angular.copy($scope.itemLastAndAveragePrice));
        //console.log(value[5]);
        $scope.stores = value[6];
        $scope.baseKitchenItems = value[7];
        $scope.itemLastAndAveragePriceBasekitchen = value[8];
        //console.log("last price BK", angular.copy($scope.itemLastAndAveragePriceBasekitchen) );
        $scope.baseKitchenUnits = value[9];
        $scope.stockSettings = value[10];
        if($scope.stockSettings == '')
          isYieldEnabled = false;
        else if($scope.stockSettings[0].isYieldEnabled == undefined)
          isYieldEnabled = false;
        else
        {
          isYieldEnabled = $scope.stockSettings[0].isYieldEnabled;
          if(isYieldEnabled == true)
          setYieldCosting($scope.stockRecipes,$scope.stockItem,$scope.pricing);
        }
        setMaterialEstimation()
      });

      /*Category.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }).$promise.then(function (categories) {
        CatWithSemiProcessed(categories);
      });*/

      // Item.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.deployment[0]._id}).$promise.then (function (items) {
      //     $scope.items=items;
      // });

      // stockUnit.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.deployment[0]._id}).$promise.then (function (units) {
      //     $scope.stockUnits=units;
      // });

      /*stockItem.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }).$promise.then (function (sItems) {
        $scope.stockItem = sItems;
        //calAvAndLastPrice_Items(sItems);
      });*/

      // stockRecipe.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.deployment[0]._id}).$promise.then(function (recipes){
      //  $scope.stockRecipes=recipes;
      //   $scope.recipeReportForm.items=angular.copy(recipes);
      //});
    });


    //----------------------------------Receipe Form--------------------------------------------
    function setItemCode() {
        _.forEach($scope.stockItem, function(item) {
          if(!item.itemCode)
          item.itemCode = '';
        });
    }

    function setYieldCosting(recipes, stockItems, pricing) {
      //console.log('$scope.stockRecipes', $scope.stockRecipes);
      //console.log('$scope.itemLastAndAveragePrice',$scope.itemLastAndAveragePrice);
      //console.log('$scope.stockItem',$scope.stockItem);
      _.forEach($scope.itemLastAndAveragePrice, function (price) {
        price.yieldAdjustedPrice = false;
      });
      _.forEach($scope.stockRecipes, function(recipe) {
        _.forEach(recipe.receipeDetails, function(recipeItem) {
          if(!recipeItem.isSemiProcessed) {
            //console.log('!isSemiProcessed :-', recipeItem.itemName)
            //console.log('recipeItem',recipeItem);
            var itemIndex = _.findIndex($scope.stockItem, function(item) {
                return item._id == recipeItem._id;
              });
            if(itemIndex >= 0)
            {
              //console.log('match found in stockItem for :- ', recipeItem.itemName);

              if($scope.stockItem[itemIndex].yeild) {
                //console.log('yeild :- ', $scope.stockItem[itemIndex].yeild)
                //console.log('stockItems[itemIndex].itemName',stockItems[itemIndex].itemName);
              var priceIndex = _.findIndex($scope.itemLastAndAveragePrice, function(itm) {
                  return recipeItem._id == itm.itemId;
                }); 
                if(priceIndex >= 0)
                {
                  //console.log('match found in pricing for :- ', recipeItem.itemName);
                  if($scope.itemLastAndAveragePrice[priceIndex].yieldAdjustedPrice == false)
                  {
                  $scope.itemLastAndAveragePrice[priceIndex].lastPrice = ((200 - $scope.stockItem[itemIndex].yeild)  * 0.01 * $scope.itemLastAndAveragePrice[priceIndex].lastPrice);
                  //console.log('$scope.itemLastAndAveragePrice[priceIndex].lastPrice',$scope.itemLastAndAveragePrice[priceIndex].lastPrice);
                  $scope.itemLastAndAveragePrice[priceIndex].monthAverage = ((200 - $scope.stockItem[itemIndex].yeild)  * 0.01 * $scope.itemLastAndAveragePrice[priceIndex].monthAverage);
                  //console.log('$scope.itemLastAndAveragePrice[priceIndex].monthAverage',$scope.itemLastAndAveragePrice[priceIndex].monthAverage);
                  $scope.itemLastAndAveragePrice[priceIndex].yieldAdjustedPrice = true;
                  }
                }
                else
                {
                  //console.log('no match found in pricing for :- ', recipeItem.itemName);
                }
              }
            }
            else
            {
              //console.log('No match found for :- ',recipeItem.itemName)
            }
          }
        });
      });
    }

    $scope.getSearchResultsForRecipeItems = function(searchField) {
      var result = [];
      _.forEach($scope.receipeDetailForm.stockItems, function(item, i) {
        if(!item.itemCode)
          item.itemCode = '';
        if (_.contains(item.itemCode.toLowerCase(), searchField.toLowerCase()) || _.contains(item.itemName.toLowerCase(), searchField.toLowerCase()))
            result.push(item);
      });
      return result;
    }

    $scope.searchDropdown = function(name, code) {
        if (code == '' || code == null || code == undefined)
            return name;
        else
            return code + ' - ' + name;
    }

    function calAvAndLastPrice_Items(item) {
      _.forEach(item, function (it, i) {
        var lastPrice = getStockItem_EntryLastPrice(it.itemName);
        var avMonthPrice = getStockItem_LastMonthEstimatedAverage(it.itemName);
        var obj = {"itemName": it.itemName, "lastPrice": lastPrice, "monthAverage": avMonthPrice};
        $scope.itemLastAndAveragePrice.push(obj);
      });
    };

    function CatWithSemiProcessed(ss) {
      _.forEach(ss, function (cat, i) {
        if (cat.isSemiProcessed == undefined || cat.isSemiProcessed == false) {
          $scope.recipeCategory.push(cat);
          $scope.categories.push(cat);
        }
        else {
          $scope.spCategories.push(cat);
        }
      });
      addToCategory_SemiProcessed($scope.spCategories);
    };

    function addToCategory_SemiProcessed(cat) {
      _.forEach(cat, function (c, i) {
        c.categoryName = c.categoryName + "  (Semi-Processed)";
        $scope.categories.push(c);
      });
    };

    $scope.receipeForm = {
      items: [],
      units: [],
      showReceipeForm: false
    };

    $scope.availableReceipe = {};

    $scope.clearRecipeForm = function () {
      // $scope.receipeForm={
      //   items:[],
      //   units:[],
      //   showReceipeForm:false
      // };
      // $scope.recipeReportForm={};
      // $scope.receipeDetailForm={
      //   stockItems:[]
      // };
      $scope.OnItemChange($scope.receipeForm.selectedItem);
      
    };

    function itemsInCategory(catId) {
      $scope.receipeForm.items = [];
      _.forEach($scope.items, function (itm, i) {
        if (itm.category._id == catId) {
          $scope.receipeForm.items.push(itm);
        }
      });
    };

    function bindUnits() {
      $scope.receipeForm.uunits = angular.copy($scope.stockUnit);
    };

    function bindItems() {
      $scope.receipeDetailForm.stockItems = angular.copy($scope.stockItem);
    };

    function bindItemsWithSemiProcessed() {
      $scope.receipeDetailForm.stockItems = angular.copy($scope.stockItem);

      _.forEach(angular.copy($scope.receipeForm.spItems), function (Item, i) {
        Item.itemName = Item.recipeName + "  (Semi-Processed)";
        var index = _.findIndex($scope.receipeDetailForm.stockItems, {itemId: Item.itemId});
        Item.isSemiProcessed = true;
        if (index < 0) {
          if ($scope.receipeForm.selectedItem._id != Item.itemId) {
            if(!Item.itemCode)
                Item.itemCode = '';
            $scope.receipeDetailForm.stockItems.push(Item);
            var index_st = _.findIndex($scope.stockItem, {itemId: Item.itemId});
            if (index_st < 0) {
              
              $scope.stockItem.push(Item);
            }
          }
        }
        else {
          $scope.receipeDetailForm.stockItems[index] = Item;
          if ($scope.receipeForm.selectedItem._id == Item.itemId) {
            $scope.receipeDetailForm.stockItems.splice(index, 1);
          }
        }
      });

    

      _.forEach($scope.baseKitchenItems, function (bki, l) {
        Item.itemName = bki.itemName + "(Base Kitchen Item)";
        bki._id = bki.itemId;
        var index = _.findIndex($scope.receipeDetailForm.stockItems, {itemId: bki.itemId});
        bki.isBaseKitchenItem = true;
        bki.isSemiProcessed = false;
        if (index < 0) {
          if ($scope.receipeForm.selectedItem._id != bki.itemId) {
            if(!bki.itemCode)
              bki.itemCode = '';
            $scope.receipeDetailForm.stockItems.push(bki);
            $scope.stockItem.push(bki);
          }
        }
        else {
          $scope.receipeDetailForm.stockItems[index] = bki;
          if ($scope.receipeForm.selectedItem._id == bki.itemId) {
            $scope.receipeDetailForm.stockItems.splice(index, 1);
          }
        }
      })
    };

    function removeSelectedItemBeforeLoad() {
      _.forEachRight($scope.receipeDetailForm.stockItems, function (item, i) {
        _.forEach($scope.availableReceipe, function (avItem, ii) {
          if (item._id == avItem._id) {
            $scope.receipeDetailForm.stockItems.splice(i, 1);
          }
        });
      });
    };

    function bindAvailableRecipe(index) {
      var _ttoday = 0;
      var _tmonthly = 0;
      _.forEach($scope.stockRecipes[index].receipeDetails, function (itm, i) {
        if (itm.calculatedPrice != "NA" && itm.calculatedAveragePrice != "NA") {
          _ttoday += parseFloat(itm.calculatedPrice);
          _tmonthly += parseFloat(itm.calculatedAveragePrice);
        }
      });

      $scope.availableReceipe = $scope.stockRecipes[index].receipeDetails;
      $scope.availableReceipe.totalTodayPrice = parseFloat(_ttoday).toFixed(4);
      $scope.availableReceipe.totalMonthlyPrice = parseFloat(_tmonthly).toFixed(4);
    };


    function itemPriceInbaseConversion() {
      var basePrice = 0;
      basePrice = $scope.receipeDetailForm.selectedUnits.conversionFactor
      return basePrice;
    };

    $scope.bindItems_receipeForm = function (cat) {
      if (cat == null) {
        $scope.receipeForm.items = [];
        $scope.receipeForm.showReceipeForm = false;
        $scope.receipeForm.uunits = [];
        $scope.receipeForm.selectedItem = {};
      }
      else {
        itemsInCategory(cat._id);
        $scope.receipeForm.showReceipeForm = false;
        $scope.receipeForm.uunits = [];
        $scope.receipeForm.selectedItem = {};
      }
    };

    function getAllSemiProcessedItems() {
      $scope.receipeForm.spItems = [];
      _.forEach($scope.spCategories, function (cat, i) {
        _.forEach($scope.stockRecipes, function (itm, i) {
          if (itm.categoryId == cat._id) {
            $scope.receipeForm.spItems.push(itm);
          }
        });
      });
    };

    $scope.OnItemChange = function (itm) {
      bindUnits();
      //bindItems();
      getAllSemiProcessedItems();
      bindItemsWithSemiProcessed();
      var index = _.findIndex($scope.stockRecipes, {itemId: itm._id});
      if (index >= 0) {
        $scope.receipeForm.selectedItem.quantity = $scope.stockRecipes[index].quantity;
        var uIndex = _.findIndex($scope.receipeForm.uunits, {_id: $scope.stockRecipes[index].selectedUnitId._id});
        $scope.receipeForm.selectedUnit = $scope.receipeForm.uunits[uIndex];
        bindAvailableRecipe(index);
        $scope.receipeForm.showReceipeForm = true;
        getDynamicPricing($scope.stockRecipes[index].receipeDetails);
      }
      else {
        $scope.receipeForm.selectedItem.quantity = "";
        $scope.receipeForm.showReceipeForm = false;
        $scope.receipeForm.selectedUnit = undefined;
        $scope.availableReceipe = [];
      }
      removeSelectedItemBeforeLoad();
    };

    function getAllUNitsAcToBaseUnit(name) {
      var uu = [];
      _.forEach($scope.stockUnit, function (u, i) {
        if (u.baseUnit.name == name) {
          uu.push(u);
        }
      });
      return uu;
    }

    $scope.unitFilter = function (unit) {
      if($scope.receipeForm.selectedItem)
      if (unit && $scope.receipeForm.selectedItem.unit) {
        return unit.baseUnit.id == $scope.receipeForm.selectedItem.unit.baseUnit.id;
      }
      return false;
    }

    $scope.deleteRecipe = function (selectedItem) {
      $scope.updateValidation.disable = true;
      var index = _.findIndex($scope.stockRecipes, {itemId: selectedItem._id});
      var index_Sp = _.findIndex($scope.receipeDetailForm.stockItems, {itemId: selectedItem._id});
      stockRecipe.remove({}, $scope.stockRecipes[index], function (result) {
        $scope.stockRecipes.splice(index, 1);
        $scope.receipeDetailForm.stockItems.splice(index_Sp, 1);
        $scope.receipeForm.showReceipeForm = false;
        $scope.receipeForm.selectedItem = {};
        $scope.receipeForm.selectedUnit = {};
        $scope.updateValidation.disable = false;
        vendor.deleteItemFromHOVendor({itemId: selectedItem._id}, {
          deployment_id: localStorageService.get('deployment_id'),
          tenant_id: localStorageService.get('tenant_id'),
          type: "HO Vendor"
        }, function (result) {

        })
      });

    };

    $scope.clearItemModel = function (formReceipeDetails) {
      if (!formReceipeDetails.$invalid) {

        $scope.receipeDetailForm.selectedUnits = {};
        $scope.receipeDetailForm.selectedItem = "";

      }
    }

    $scope.updateValidation = {};

    $scope.createStockRecipe = function (formReceipe) {
      if (formReceipe.$valid) {
        formReceipe.$valid = false;
        $scope.updateValidation.disable = true;
        var recipe = $scope.receipeForm.selectedItem;
        var unit = $scope.receipeForm.selectedUnit;
        if ($scope.receipeForm.selectedItem.stockQuantity != undefined && $scope.receipeForm.selectedItem.stockQuantity != null && $scope.receipeForm.selectedItem.unit != undefined) {
          $scope.receipeForm.selectedItem.menuQty = parseFloat($scope.receipeForm.selectedItem.stockQuantity) * parseFloat($scope.receipeForm.selectedItem.unit.conversionFactor);
          $scope.receipeForm.selectedItem.recipeQty = parseFloat(parseFloat($scope.receipeForm.selectedItem.quantity) * parseFloat($scope.receipeForm.selectedUnit.conversionFactor)).toFixed(3);
          var index = _.findIndex($scope.stockRecipes, {itemId: $scope.receipeForm.selectedItem._id});
          if (index >= 0) {
            $scope.stockRecipes[index].units = [];
            $scope.stockRecipes[index].selectedUnitId = $scope.receipeForm.selectedUnit;
            $scope.stockRecipes[index].menuQty = parseFloat($scope.receipeForm.selectedItem.menuQty);
            $scope.stockRecipes[index].recipeQty = parseFloat($scope.receipeForm.selectedItem.recipeQty);
            $scope.stockRecipes[index].unit = $scope.receipeForm.selectedItem.unit;
            var ind = _.findIndex($scope.stockUnit, {_id: $scope.receipeForm.selectedUnit._id});
            $scope.stockRecipes[index].units = getAllUNitsAcToBaseUnit($scope.stockUnit[ind].baseUnit.name);
            //$scope.stockRecipes[index].units.push(angular.copy($scope.stockUnit[ind]));
            $scope.stockRecipes[index].quantity = parseFloat($scope.receipeForm.selectedItem.quantity);

            var index_Semi = _.findIndex($scope.receipeForm.spItems, {itemId: $scope.receipeForm.selectedItem._id});
            $scope.stockRecipes[index].isSemiProcessed = false;
            if (index_Semi >= 0) {
              $scope.stockRecipes[index].isSemiProcessed = true;
            }

            stockRecipe.update({}, $scope.stockRecipes[index], function (result) {
              $scope.stockRecipes.splice(index, 1);
              $scope.stockRecipes.push(result);
              if (result.isSemiProcessed == true) {
                updatedAllItemsWhenIntermediateItemUpdated(result, false);
              }
              $scope.updateValidation.disable = false;
            });
          }
          else {
            $scope.receipeForm.tenant_id = currentUser.tenant_id;
            $scope.receipeForm.deployment_id = $scope.currentUser.deployment_id;//$scope.deployment[0]._id;
            $scope.receipeForm.recipeName = $scope.receipeForm.selectedItem.name;
            $scope.receipeForm.itemId = $scope.receipeForm.selectedItem._id;
            $scope.receipeForm.categoryId = $scope.receipeForm.category._id;
            $scope.receipeForm.selectedUnitId = $scope.receipeForm.selectedUnit;
            $scope.receipeForm.unit = $scope.receipeForm.selectedItem.unit;
            $scope.receipeForm.menuQty = $scope.receipeForm.selectedItem.menuQty;
            $scope.receipeForm.recipeQty = $scope.receipeForm.selectedItem.recipeQty;
            var ind = _.findIndex($scope.stockUnit, {_id: $scope.receipeForm.selectedUnit._id});
            $scope.receipeForm.units = [];
            $scope.receipeForm.units = getAllUNitsAcToBaseUnit($scope.stockUnit[ind].baseUnit.name);
            //$scope.receipeForm.units.push(angular.copy($scope.stockUnit[ind]));
            $scope.receipeForm.quantity = parseFloat($scope.receipeForm.selectedItem.quantity);
            // var index_Semi=_.findIndex($scope.receipeForm.spItems, {itemId:$scope.receipeForm.selectedItem._id});
            // $scope.receipeForm.isSemiProcessed=false;
            // if(index_Semi>=0){
            //   $scope.receipeForm.isSemiProcessed=true;
            // }
            $scope.receipeForm.isSemiProcessed = $scope.receipeForm.selectedItem.category.isSemiProcessed;
            // modified
              vendor.getAll({id: currentUser.tenant_id}, function (data) {
                //  console.log(data,'HO Vendor');
                $scope.allVendors = data;
                var vendor_id, flag = 0;
                _.forEach($scope.allVendors, function (v) {

                  if (v.type == 'HO Vendor') {
                    if ($scope.receipeForm.deployment_id == v.baseKitchenId) {
                      var it = {};
                      _.forEach(v.pricing.category, function (c) {
                        if (c._id == $scope.receipeForm.category._id) {
                          var updatedItemIndex = _.findIndex(c.item, function (i) {
                            return i._id == $scope.receipeForm.selectedItem._id;
                          });
                          vendor_id = v._id;
                          if (updatedItemIndex == -1) {
                            flag = 1;
                            it._id = $scope.receipeForm.selectedItem._id;
                            it.itemName = $scope.receipeForm.selectedItem.name;
                            _.forEach($scope.items, function (i) {
                              if (i.name == $scope.receipeForm.selectedItem.name) {
                                it.units = [];
                                it.units.push({});
                                it.units[0]._id = i.unit._id;
                                it.units[0].baseUnit = i.unit.baseUnit;
                                it.units[0].unitName = i.unit.unitName;
                                it.units[0].baseConversionFactor = i.unit.baseConversionFactor;
                                it.units[0].conversionFactor = i.unit.conversionFactor;
                                
                              }
                            });
                            c.item.push(it);
                            
                          }
                          /*else {
                           _.forEach($scope.items, function (i) {
                           if (i.name == $scope.receipeForm.selectedItem.name) {
                           c.item[updatedItemIndex].units = [];
                           c.item[updatedItemIndex].units.push({});
                           c.item[updatedItemIndex].units[0]._id = i.unit._id;
                           c.item[updatedItemIndex].units[0].baseUnit = i.unit.baseUnit;
                           c.item[updatedItemIndex].units[0].unitName = i.unit.unitName;
                           c.item[updatedItemIndex].units[0].baseConversionFactor = i.unit.baseConversionFactor;
                           c.item[updatedItemIndex].units[0].conversionFactor = i.unit.conversionFactor;
                           //console.log(it.units);
                           }
                           });
                           }*/
                        }
                      })
                      if (flag == 1) {
                        v.$update(function (data) {
                        });
                      }
                      /*v.$update(function (data) {
                       console.log('Updated vendor', data);
                       });*/
                    }
                  }
                })
                //console.log($scope.allVendors);
              });
            // end

            _.forEach($scope.stores, function (s, i) {
              
              if (s.isKitchen) {
                _.forEach(s.processedFoodCategory, function (c, j) {
                  if (c._id == $scope.receipeForm.category._id) {
                    $scope.receipeForm.kitchenDetails = {_id: s._id, kitchenName: s.storeName}
                    return
                  }
                })
              }
            })
            stockRecipe.saveData({}, $scope.receipeForm, function (result) {
              $scope.stockRecipes.push(result);
              $scope.receipeForm.showReceipeForm = true;
              //modified
              _.forEach($scope.stores, function (store) {

                var flag = 0;
                _.forEach(store.processedFoodCategory, function (cat) {
                  if (cat.categoryName === recipe.category.categoryName) {
                    var updatedItemIndex = _.findIndex(cat.item, function (it) {
                      return it._id == recipe._id;
                    });
                    if (updatedItemIndex > -1) {
                      cat.item[updatedItemIndex].number = recipe.number;
                      cat.item[updatedItemIndex].unit = unit;
                      cat.item[updatedItemIndex].units = recipe.units;
                      cat.item[updatedItemIndex].itemName = recipe.name;
                      cat.item[updatedItemIndex]._id = recipe._id;
                      flag = 1;
                    }
                    else {
                      cat.item.push({
                        number: recipe.number,
                        unit: unit,
                        units: recipe.units,
                        itemName: recipe.name,
                        _id: recipe._id
                      });
                      flag = 1;
                    }
                  }
                });
                store.$update();
              });
              _.forEach($scope.stockRecipes, function (recipe) {

              })
              $scope.updateValidation.disable = false;
              //end
            });
          }
        }
        else {
          growl.error('To add this item Please provide stockQuantity and unit in menuItems', {ttl: 3000});
        }
      }
    };

    function getRawItems_Old(items, ritems, isDelete) {
      //var items=angular.copy(itemss);
      var rawItems = [];
      if (ritems.rawItems != undefined) {
        rawItems = angular.copy(ritems.rawItems);
      }

      if (items.rawItems == undefined) {
        var iIndex = _.findIndex(rawItems, {_id: items._id});
        if (iIndex < 0) {
          items.baseQuantity = parseFloat(items.quantity) * parseFloat(items.selectedUnitId.conversionFactor);
          if (items.selectedUnitId.baseUnit.id == 2 || items.selectedUnitId.baseUnit.id == 3) {
            items.baseQuantity = parseFloat(items.baseQuantity) / 1000;
          }
          rawItems.push(items);
        }
        else {
          items.baseQuantity = parseFloat(items.quantity) * parseFloat(items.selectedUnitId.conversionFactor);
          if (items.selectedUnitId.baseUnit.id == 2 || items.selectedUnitId.baseUnit.id == 3) {
            items.baseQuantity = parseFloat(items.baseQuantity) / 1000;
          }
          if (isDelete == true) {
            rawItems[iIndex].baseQuantity = parseFloat(parseFloat(rawItems[iIndex].baseQuantity) - parseFloat(items.baseQuantity));
            if (rawItems[iIndex].baseQuantity == 0) {
              rawItems.splice(iIndex, 1);
            }
          }
          else {
            rawItems[iIndex].baseQuantity = parseFloat(rawItems[iIndex].baseQuantity) + parseFloat(items.baseQuantity);
          }
        }
      }
      else {
        _.forEach(items.rawItems, function (r, i) {
          var baseConFac = 1;
          var selconFac = parseFloat(r.selectedUnitId.conversionFactor);
          var rInd = _.findIndex($scope.stockRecipes, {_id: items._id});
          var recipeBaseQty = parseFloat($scope.stockRecipes[rInd].quantity) * parseFloat($scope.stockRecipes[rInd].selectedUnitId.conversionFactor);
          var ind = _.findIndex(rawItems, {_id: r._id});
          var itemBaseQty = parseFloat(items.quantity) * parseFloat(items.selectedUnitId.conversionFactor);

          //if(r.baseQuantity==undefined){
          r.baseQuantity = parseFloat(r.quantity) * parseFloat(r.selectedUnitId.conversionFactor);
          // }
          // else
          // {
          //   r.baseQuantity=parseFloat(r.baseQuantity);
          // }
          r.baseQuantity = parseFloat(r.baseQuantity) * parseFloat(itemBaseQty) / parseFloat(recipeBaseQty);
          if (r.baseQuantity == undefined) {
            if (r.selectedUnitId.baseUnit.id == 2 || r.selectedUnitId.baseUnit.id == 3) {
              r.baseQuantity = parseFloat(r.baseQuantity) / 1000;
            }
          }

          if (ind < 0) {
            rawItems.push(r);
          }
          else {
            if (isDelete == true) {
              rawItems[ind].baseQuantity = parseFloat(rawItems[ind].baseQuantity) - parseFloat(r.baseQuantity);
              if (rawItems[ind].baseQuantity == 0) {
                rawItems.splice(ind, 1);
              }
            }
            else {
              rawItems[ind].baseQuantity = parseFloat(rawItems[ind].baseQuantity) + parseFloat(r.baseQuantity);
            }
          }
        });
      }
      return rawItems;
    }

    function updatedAllItemsWhenIntermediateItemUpdated(item, isDelete) {
      var rec = angular.copy($scope.stockRecipes);
      _.forEach(rec, function (r, i) {
        if (r._id != item._id) {
          var flag = false;
          var rawItems = [];
          r.rawItems = [];
          _.forEach(r.receipeDetails, function (d, ii) {
            if (d._id == item._id) {
              d.rawItems = angular.copy(item.rawItems);
              d.receipeDetails[ii] = item;
              item.quantity = d.quantity;
              item.baseQuantity = d.baseQuantity;
              item.selectedUnitId = d.selectedUnitId;
              r.receipeDetails[ii] = angular.copy(item);
              r.receipeDetails[ii].itemName = item.recipeName;
              d.receipeDetails[ii].itemName = item.recipeName;
              d.receipeDetails[ii].selectedUnitName = item.selectedUnitId.unitName;
              flag = true;
              var ind = _.findIndex($scope.stockRecipes, {_id: r._id});
              //var raw=getRawItems(d,r,isDelete);
              // _.forEach(raw,function(ra,iii){
              //   rawItems.push(ra);
              // });
              // d.rawItems=raw;
            }
            else {
              flag = false;
              // var raw=getRawItems(angular.copy(d),r,isDelete);
              // _.forEach(raw,function(ra,iii){
              //   rawItems.push(ra);
              // });
            }
          });

          if (flag == true) {
            //r.rawItems=rawItems;
            r.rawItems = getRawItems('', r, '');
            stockRecipe.update({}, r, function (result) {
              var ind = _.findIndex($scope.stockRecipes, {_id: result._id});
              $scope.stockRecipes[ind] = result;
              var iitem = _.findIndex($scope.Items, {_id: result._id});
              if (iitem >= 0) {
                $scope.Items[iitem] = result;
              }
              $scope.AllRawItemInRecipe = [];
              if (result.isSemiProcessed == true) {
                var spInd = _.findIndex($scope.receipeForm.spItems, {_id: result._id});
                $scope.receipeForm.spItems[spInd] = result;
                updatedAllItemsWhenIntermediateItemUpdated(result, isDelete);
              }
            });
          }
        }
      });
    };

    $scope.checkQty_menu = function (quantity) {
      if (isNaN(quantity)) {
        $scope.receipeForm.selectedItem.quantity = "";
      }
    };

    $scope.checkDetailqty = function (quantity) {
      if (isNaN(quantity)) {
        $scope.receipeDetailForm.quantity = "";
      }
    };

    $scope.goNext = function (nextIdx) {
      //alert("hi");
      angular.element(document.querySelector('#f_' + nextIdx))[0].focus();
    }

    //--------------------------------Receipe Detail Form ----------------------------------------------
    $scope.receipeDetailForm = {
      stockItems: []
    };


    /*function bindUnitsForDetailForm(index) {

      var itm = $scope.receipeDetailForm.stockItems[index];
      $scope.receipeDetailForm.units = [];
      if (itm.isSemiProcessed == undefined) {
        if (itm.units != undefined) {
          var bUName = itm.units[0].baseUnit.name;
          _.forEach($scope.stockUnit, function (u, ii) {
            if (bUName == u.baseUnit.name) {
              $scope.receipeDetailForm.units.push(u);
            }
          });
        }
      }
      else {
        if (itm.units != undefined) {
          if(itm.isBaseKitchenItem == true)
          {
            var units = [];
            _.forEach($scope.stockUnit, function(u) {
              if(u.baseUnit.id == itm.units[0].baseUnit.id)
              {
                units.push(u);
              }
            });
            $scope.receipeDetailForm.units = units;
          }
          else
          {
            $scope.receipeDetailForm.units = itm.units;
          }
          //console.log('angular.copy units', angular.copy(units));
          // var uni=_.findIndex($scope.stockUnit, {"unitName":itm.unit});

          // if(uni<0){
          //   var u={unitName:"Pc",_id:"awqw123"};
          //   $scope.receipeDetailForm.units.push(u);
          // }
          // else
          // {
          //   var bUName=$scope.stockUnit[uni].baseUnit.name;
          //   _.forEach($scope.stockUnit, function(u,ii){
          //     if(bUName==u.baseUnit.name){
          //       $scope.receipeDetailForm.units.push(u);
          //     }
          //   });
          // }
          
        }
        else {
          var u = {unitName: "Pc", _id: "awqw123"};
          $scope.receipeDetailForm.units.push(u);
        }
      }
    };*/

    function bindUnitsForDetailForm(index) {
      var itm = $scope.receipeDetailForm.stockItems[index];
      $scope.receipeDetailForm.units = [];
      if (itm.isSemiProcessed == undefined) {
        if (itm.units != undefined) {
          var bUName = itm.units[0].baseUnit.name;
          _.forEach($scope.stockUnit, function (u, ii) {
            if (bUName == u.baseUnit.name) {
              $scope.receipeDetailForm.units.push(u);
            }
          });
        }
      }
      else {
        if (itm.units != undefined) {
          // var uni=_.findIndex($scope.stockUnit, {"unitName":itm.unit});

          // if(uni<0){
          //   var u={unitName:"Pc",_id:"awqw123"};
          //   $scope.receipeDetailForm.units.push(u);
          // }
          // else
          // {
          //   var bUName=$scope.stockUnit[uni].baseUnit.name;
          //   _.forEach($scope.stockUnit, function(u,ii){
          //     if(bUName==u.baseUnit.name){
          //       $scope.receipeDetailForm.units.push(u);
          //     }
          //   });
          // }
          $scope.receipeDetailForm.units = itm.units;
        }
        else {
          var u = {unitName: "Pc", _id: "awqw123"};
          $scope.receipeDetailForm.units.push(u);
        }
        console.log(itm);
      }
    };
    $scope.OnItemChange_receipeDetailForm = function ($item, $model, $label) {
      //console.log("items"+ $item);
      //console.log("model"+ $model);
      //console.log("label"+ $label);

      _.forEach($scope.receipeDetailForm.stockItems, function (itm, i) {
        if (itm._id == $item._id) {
          var index = _.findIndex($scope.stockItem, function(it) {
            return it._id == $item._id;
          })
          if(index >= 0)
              itm.itemCode = $scope.stockItem[index].itemCode;
            else
              itm.itemCode = '';
          bindUnitsForDetailForm(i);
          $scope.receipeDetailForm.item = itm;
        }
      });
    };

    function getAllEntryTypeTransactions(trans) {
      var aa = [];
      _.forEach(trans, function (itm, i) {
        //add list of entry transaction type
        if (itm.transactionType == 1) {
          aa.push(itm);
        }
      });
      return aa;
    };

    function sortByDateTransaction(trans) {
      return trans.sort(function (a, b) {
        return new Date(a.created) - new Date(b.created)
      });
    };

    function filteredTransactionForLastMonth(sortedItems) {
      var currentDate = new Date();
      var compareDate = new Date();
      compareDate.setDate(currentDate.getDate() - 30);

      var newArray = [];

      _.forEach(sortedItems, function (si, i) {
        if (new Date(si.created) > new Date(compareDate) && new Date(si.created) < new Date(currentDate)) {
          newArray.push(si);
        }
      });
      return newArray;
    };

    function getStockItem_LastMonthEstimatedAverage(itemName) {
      var items = getAllEntryTypeTransactions(angular.copy($scope.allTransactions));
      var averagePrice = "NA";
      var tPrice = 0;
      var count = 0;
      if (items.length > 0) {
        var sortedItems = sortByDateTransaction(items);
        var lastMonthsItems = filteredTransactionForLastMonth(sortedItems);
        if (lastMonthsItems.length > 0) {
          _.forEach(lastMonthsItems, function (l, i) {
            _.forEach(l._store.vendor.category, function (c, i) {
              _.forEach(c.items, function (itm, ii) {
                if (itm.itemName == itemName) {
                  _.forEach(itm.calculateInUnits, function (u, iii) {
                    if (u.type == "baseUnit") {
                      var ss = u.subTotal / itm.qty;
                      tPrice += parseFloat(ss);//parseFloat(itm.price);
                      count += 1;
                    }
                  });
                }
              });
            });
            // _.forEach(l._items, function(itm,ii){
            //   if(itm.itemName==itemName){
            //     _.forEach(itm.units, function(u,ii){
            //         if(u._id==itm.preferedUnit){
            //           var conversionFactor=u.conversionFactor;
            //           tPrice +=parseFloat(itm.price)/parseFloat(conversionFactor);
            //           count+=1;
            //         }
            //       });
            //   }
            // });
          });
        }
        //console.log(lastMonthsItems);
      }

      if (tPrice != 0 && count != 0) {
        averagePrice = parseFloat(tPrice / count);
      }

      return averagePrice;
    };

    function getStockItem_EntryLastPrice(itemName) {

      var items = getAllEntryTypeTransactions(angular.copy($scope.allTransactions));
      var lastPrice = "NA";
      if (items.length > 0) {
        var sortedItems = sortByDateTransaction(items);

        _.forEachRight(sortedItems, function (s, j) {
          _.forEach(s._store.vendor.category, function (c, i) {
            _.forEach(c.items, function (itm, ii) {
              if (itm.itemName == itemName) {
                if (lastPrice == "NA") {

                  _.forEach(itm.calculateInUnits, function (u, iii) {
                    console.log(s);
                    console.log(u);
                    if (u.type != undefined) {
                      if (u.type == "baseUnit") {

                        var ss = u.subTotal / itm.qty;
                        lastPrice = parseFloat(ss);//(itm.price);//(itm.totalAmount/ss);
                      }
                    }
                    else {
                      lastPrice = 0;
                      console.log(s);
                    }
                  });
                }
              }
            });
          });

          // _.forEachRight(s._items, function(itm,i){
          //   if(itm.itemName==itemName){
          //     if(lastPrice=="NA"){
          //       _.forEach(itm.units, function(u,ii){
          //         if(u._id==itm.preferedUnit){
          //           var conversionFactor=u.conversionFactor;
          //           lastPrice=parseFloat(itm.price)/parseFloat(conversionFactor);
          //         }
          //       });

          //     }
          //   }
          // });
        });
      }
      ;
      return lastPrice;
    };


    function formatItem_receipeDetail(item) {
      for (var p in item) {
        if (p != "_id" && p != "itemId" && p != "itemName" && p != "preferedUnit" && p != "menuQty" && p != "recipeQty" && p != "rawItems"
          && p != "receipeDetails" && p != "unit" && p != "units" && p != "calculatedPrice" && p != "calculatedAveragePrice"
          && p != "isSemiProcessed" && p != "isBaseKitchenItem" && p != "toDeployment_id") {
          delete item[p];
        }
      }
      _.forEach(item.units, function (u) {
        for (var p in u) {
          if (p != "_id" && p != "unitName" && p != "conversionFactor") {
            delete u[p];
          }
        }
      });

      item["quantity"] = parseFloat($scope.receipeDetailForm.quantity);
      item["selectedUnitId"] = $scope.receipeDetailForm.selectedUnits;
      item["selectedUnitName"] = $scope.receipeDetailForm.selectedUnits.unitName;

      return item;
    };

    function clearDetailForm() {
      $scope.receipeDetailForm.selectedItem = "";
      $scope.receipeDetailForm.quantity = "";
      $scope.receipeDetailForm.selectedUnits = {};
      $scope.receipeDetailForm.units = [];
    };

    function itemPriceInbaseConversion(itemName) {
      var basePrice = 0;
      var items = getAllEntryTypeTransactions(angular.copy($scope.allTransactions));
      _.forEachRight(items, function (s, ii) {
        _.forEachRight(s._store.vendor.category, function (cat, i) {
          _.forEach(cat.items, function (itm, ii) {
            if (itm.itemName == itemName) {
              _.forEach(itm.calculateInUnits, function (u, iii) {
                if (u.type == "baseUnit") {
                  basePrice = u.subTotal;
                }
              });
            }
          });
        });
      });
      //basePrice= $scope.receipeDetailForm.selectedUnits.conversionFactor
      return basePrice;
    };
    function getConFactor(unitId) {
      var con = 1;
      var index = _.findIndex($scope.stockUnit, {"_id": unitId});
      if (index >= 0) {
        con = parseFloat($scope.stockUnit[index].conversionFactor);
      }
      return con;
    };
   
    function getDynamicPricing_intermediate(recipes) {
      var _ttoday = 0;
      var _tmonthly = 0;
      _.forEach(recipes.rawItems, function (itm, i) {

        
        if(itm.isBaseKitchenItem == true)
        {
        var l_index = _.findIndex($scope.itemLastAndAveragePriceBasekitchen, {itemId: itm._id});
        if(l_index>-1)
          var lastPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].lastPrice;
        else lastPrice=0;
        var conversionFactor = getConFactor(itm.selectedUnitId._id);
        var baseConFac = 1;
        if(l_index>-1)
          var avMonthPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage;
        else avMonthPrice=0;
        if (lastPrice == 0) {
          itm.calculatedPrice = parseFloat(0).toFixed(4);
        }
        else {
            if(itm.selectedUnitId.baseUnit.id==2 || itm.selectedUnitId.baseUnit.id==3)
              itm.calculatedPrice = parseFloat(parseFloat(avMonthPrice) * parseFloat(itm.baseQuantity) * 1000 ).toFixed(4);
            else
              itm.calculatedPrice = parseFloat(parseFloat(avMonthPrice) * parseFloat(itm.baseQuantity)  ).toFixed(4);
          }

        if (avMonthPrice == 0) {
          itm.calculatedAveragePrice = parseFloat(0).toFixed(4);
        }
       else {
            if(itm.selectedUnitId.baseUnit.id==2 || itm.selectedUnitId.baseUnit.id==3)
              itm.calculatedAveragePrice = parseFloat(parseFloat(avMonthPrice) * parseFloat(itm.baseQuantity) * 1000 ).toFixed(4);
            else
              itm.calculatedAveragePrice = parseFloat(parseFloat(avMonthPrice) * parseFloat(itm.baseQuantity)  ).toFixed(4);
          }

        _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice));
        _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice));
        console.log("itmmmmmmmmm",itm)
        console.log(_ttoday,_tmonthly)


      }
      else
        {

        var l_index = _.findIndex($scope.itemLastAndAveragePrice, {itemId: itm._id});
        if(l_index>-1)
          var lastPrice = $scope.itemLastAndAveragePrice[l_index].lastPrice;
        else lastPrice=0;
        var conversionFactor = getConFactor(itm.selectedUnitId._id);
        var baseConFac = 1;
        if(l_index>-1)
          var avMonthPrice = $scope.itemLastAndAveragePrice[l_index].monthAverage;
        else avMonthPrice=0;
        if (lastPrice == 0) {
          itm.calculatedPrice = parseFloat(0).toFixed(4);
        }
        else {
          itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(conversionFactor)).toFixed(4);
        }
        if (avMonthPrice == 0) {
          itm.calculatedAveragePrice = parseFloat(0).toFixed(4);
        }
        else {
          itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(conversionFactor)).toFixed(4);
        }

        _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice));
        _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice));
      } 
      });
      var totalTodayPrice = parseFloat(_ttoday).toFixed(4);
      var tDayPrice1Unit = parseFloat(parseFloat(totalTodayPrice) / parseFloat(recipes.recipeQty)).toFixed(4);
      var totalMonthlyPrice = parseFloat(parseFloat(_tmonthly)).toFixed(4);
      if (isNaN(totalMonthlyPrice)) {
        totalMonthlyPrice = parseFloat(0).toFixed(4);
      }
      var totalMonthlyPrice1Unit = parseFloat(parseFloat(totalMonthlyPrice) / parseFloat(recipes.recipeQty)).toFixed(4);

      var ir = {
        "recipeName": recipes.itemName,
        "totalTodayPricePerUnit": tDayPrice1Unit,
        "totalMonthlyPricePerUnit": totalMonthlyPrice1Unit,
        "qty": recipes.quantity
      };
      console.log(ir);
      return ir;
    };


    function baseUnit_Intermediate(id) {
      var index = _.findIndex($scope.stockRecipes, {itemId: id});
      var basePrice = 1;
      if (index >= 0) {
        var units = angular.copy($scope.stockRecipes[index].units);
        _.forEach(units, function (u, i) {
          if (u._id == $scope.stockRecipes[index].selectedUnitId) {
            basePrice = u.conversionFactor;
          }
        });
      }
      return basePrice;
    };

    function getDynamicPricing(recipes) {
      var _ttoday = 0;
      var _tmonthly = 0;
      _.forEach(recipes, function (itm, i) {   //$scope.stockRecipes[index].receipeDetails
        if (itm.isBaseKitchenItem) {
          var l_index = _.findIndex($scope.itemLastAndAveragePriceBasekitchen, {itemId: itm.itemId});
          var lastPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].lastPrice;
          var avMonthPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage;
          var conversionFactor = getConFactor(itm.selectedUnitId._id);
          if (lastPrice == 0) {
            itm.calculatedPrice = 0.0000;
          }
          else {
            if(itm.selectedUnitId.baseUnit.id==2 || itm.selectedUnitId.baseUnit.id==3)
              itm.calculatedPrice = parseFloat(parseFloat(lastPrice) * parseFloat(itm.baseQuantity) * 1000).toFixed(4);
            else
              itm.calculatedPrice = parseFloat(parseFloat(lastPrice) * parseFloat(itm.baseQuantity)  ).toFixed(4);
          }

          if (avMonthPrice == 0) {
            itm.calculatedAveragePrice = 0.0000;
          }
          else {
            if(itm.selectedUnitId.baseUnit.id==2 || itm.selectedUnitId.baseUnit.id==3)
              itm.calculatedAveragePrice = parseFloat(parseFloat(avMonthPrice) * parseFloat(itm.baseQuantity) * 1000 ).toFixed(4);
            else
              itm.calculatedAveragePrice = parseFloat(parseFloat(avMonthPrice) * parseFloat(itm.baseQuantity)  ).toFixed(4);
          }
          _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice)).toFixed(4);
          _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice)).toFixed(4);
        }
        else if (itm.isSemiProcessed == undefined) {
          var l_index = _.findIndex($scope.itemLastAndAveragePrice, {itemId: itm._id});
          if(l_index>-1) {
            if(!itm.itemCode)
            {
              var itemIndex = _.findIndex($scope.stockItem, function(item) {
                return itm._id == item._id;
              });
              if(itemIndex >= 0)
                itm.itemCode = $scope.stockItem[itemIndex].itemCode;
              else
                itm.itemCode = '';
            }
            
            var lastPrice = $scope.itemLastAndAveragePrice[l_index].lastPrice;
            var avMonthPrice = $scope.itemLastAndAveragePrice[l_index].monthAverage;
            var conversionFactor = getConFactor(itm.selectedUnitId._id);
            if (lastPrice == 0) {
              itm.calculatedPrice = parseFloat(0).toFixed(4);
            }
            else {
              itm.calculatedPrice = parseFloat(parseFloat(itm.quantity) * parseFloat(lastPrice) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(4);
            }

            if (avMonthPrice == 0) {
              itm.calculatedAveragePrice = parseFloat(0).toFixed(4);
            }
            else {
              itm.calculatedAveragePrice = parseFloat(parseFloat(itm.quantity) * parseFloat(avMonthPrice) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(4);
            }
            _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice)).toFixed(4);
            _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice)).toFixed(4);
            // _ttoday+=parseFloat(itm.calculatedPrice);
            // _tmonthly+=parseFloat(itm.calculatedAveragePrice);
          }else{
            //TODO:
          }
        }
        else {
          var iitem = _.findIndex($scope.stockRecipes, {_id: itm._id});
          var sss = getDynamicPricing_intermediate(itm);
          if (itm.baseQuantity != undefined) {
            itm.calculatedPrice = parseFloat(parseFloat(sss.totalTodayPricePerUnit) * parseFloat(itm.baseQuantity)).toFixed(4);
            itm.calculatedAveragePrice = parseFloat(parseFloat(sss.totalMonthlyPricePerUnit) * parseFloat(itm.baseQuantity)).toFixed(4);
          }
          else {
            itm.calculatedPrice = parseFloat(parseFloat(sss.totalTodayPricePerUnit) * parseFloat(itm.quantity) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(4);
            itm.calculatedAveragePrice = parseFloat(parseFloat(sss.totalMonthlyPricePerUnit) * parseFloat(itm.quantity) * parseFloat(itm.selectedUnitId.conversionFactor)).toFixed(4);
          }
          _ttoday = parseFloat(parseFloat(_ttoday) + parseFloat(itm.calculatedPrice)).toFixed(4);
          _tmonthly = parseFloat(parseFloat(_tmonthly) + parseFloat(itm.calculatedAveragePrice)).toFixed(4);
        }
      });
      $scope.availableReceipe = recipes;
      $scope.availableReceipe.totalTodayPrice = parseFloat(_ttoday).toFixed(4);
      $scope.availableReceipe.totalMonthlyPrice = parseFloat(_tmonthly).toFixed(4);
    };

    function baseUnit_Entered(uId) {
      var index = _.findIndex($scope.stockUnit, {_id: uId});
      var basePrice = 1;
      if (index >= 0) {
        basePrice = $scope.stockUnit[index].conversionFactor;
      }
      return basePrice;
    };
    $scope.submitRecipeDetails = function (formReceipeDetails) {
      if (formReceipeDetails.$valid && $scope.receipeDetailForm.selectedUnits.unitName != undefined) {
        formReceipeDetails.$invalid = true;
        var index = _.findIndex($scope.stockRecipes, {itemId: $scope.receipeForm.selectedItem._id});
        if (index >= 0) {
          var itemssssssss = formatItem_receipeDetail(angular.copy($scope.receipeDetailForm.item));
          var ind = _.findIndex($scope.receipeDetailForm.units, {_id: itemssssssss.preferedUnit});
          if (ind >= 0) {
            itemssssssss.preferedUnit = angular.copy($scope.receipeDetailForm.units[ind]);
          }
          itemssssssss.baseQuantity = parseFloat(itemssssssss.quantity) * parseFloat(itemssssssss.selectedUnitId.conversionFactor);
          if (itemssssssss.selectedUnitId.baseUnit.id == 2 || itemssssssss.selectedUnitId.baseUnit.id == 3) {
            itemssssssss.baseQuantity = parseFloat(itemssssssss.baseQuantity) / 1000;
          }
          $scope.stockRecipes[index].receipeDetails.push(itemssssssss);
          $scope.stockRecipes[index].rawItems = getRawItems(itemssssssss, $scope.stockRecipes[index], false);
          stockRecipe.update({}, $scope.stockRecipes[index], function (result) {
            formReceipeDetails.$invalid;
            // $scope.stockRecipes.splice(index,1);
            // $scope.stockRecipes.push(result);
            $scope.stockRecipes[index] = result;
            var itemINdex = _.findIndex($scope.receipeDetailForm.stockItems, {itemName: $scope.receipeDetailForm.selectedItem});
            $scope.receipeDetailForm.stockItems.splice(itemINdex, 1);
            var index1 = _.findIndex($scope.stockRecipes, {itemId: result.itemId});
            bindAvailableRecipe(index1);
            //$scope.availableReceipe=$scope.stockRecipes[index].receipeDetails;
            // $scope.availableReceipe.totalTodayPrice=_ttoday;
            // $scope.availableReceipe.totalMonthlyPrice=_tmonthly;
            if (result.isSemiProcessed == true) {
              // var spInd=_.findIndex($scope.receipeForm.spItems,{_id:result._id});
              // $scope.receipeForm.spItems[spInd]=result;
              updatedAllItemsWhenIntermediateItemUpdated(result, false);
            }
            clearDetailForm();
            document.querySelector('#f_1').focus();
          });
        }
        getDynamicPricing($scope.stockRecipes[index].receipeDetails);
      }
    };


    $scope.removeRecipeDetails = function (itm) {
      var index = _.findIndex($scope.stockRecipes, {itemId: $scope.receipeForm.selectedItem._id});

      _.forEachRight($scope.stockRecipes[index].receipeDetails, function (it, i) {
        if (it._id == itm._id) {
          $scope.stockRecipes[index].receipeDetails.splice(i, 1);
        }
      });
      $scope.stockRecipes[index].rawItems = getRawItems(itm, $scope.stockRecipes[index], true);
      stockRecipe.update({}, $scope.stockRecipes[index], function (result) {
        $scope.stockRecipes.splice(index, 1);
        $scope.stockRecipes.push(result);
        var itemINdex = _.findIndex($scope.stockItem, {itemName: itm.itemName});
        //var itemINdex=_.findIndex($scope.receipeDetailForm.stockItems,{itemName:itm.itemName});
        if(!$scope.stockItem[itemINdex].itemCode)
          $scope.stockItem[itemINdex].itemCode = '';
        $scope.receipeDetailForm.stockItems.push(angular.copy($scope.stockItem[itemINdex]));
        //$scope.availableReceipe=$scope.stockRecipes[index].receipeDetails;
        var index1 = _.findIndex($scope.stockRecipes, {itemId: result.itemId});
        bindAvailableRecipe(index1);
        if (result.isSemiProcessed == true) {
          // var spInd=_.findIndex($scope.receipeForm.spItems,{_id:result._id});
          // $scope.receipeForm.spItems[spInd]=result;
          updatedAllItemsWhenIntermediateItemUpdated(result, true);
        }
      });
    }

    function getRawItems(items, ritems, isDelete) {
      var rawItems = [];
      _.forEach(ritems.receipeDetails, function (r, i) {
        if (r.isSemiProcessed == true) {
          _.forEach(r.rawItems, function (itm, ii) {
            var index = _.findIndex(rawItems, {_id: itm._id});
            if (index >= 0) {
              rawItems[index].baseQuantity = parseFloat(rawItems[index].baseQuantity) + parseFloat(itm.baseQuantity);
            }
            else {
              rawItems.push(angular.copy(itm));
            }
          });
        }
        else {
          var index = _.findIndex(rawItems, {_id: r._id});
          if (index >= 0) {
            rawItems[index].baseQuantity = parseFloat(rawItems[index].baseQuantity) + parseFloat(r.baseQuantity);
          }
          else {
            rawItems.push(angular.copy(r));
          }
        }
      });
      return rawItems;
    }

    //-------------------------------Recipe Report--------------------------
    $scope.recipeReportForm = {};

    function getDynamicPricingReport(recipes) {
      _.forEach(recipes, function (rec, iiii) {
        getDynamicPricing(rec.receipeDetails);
      });
      return recipes;
    };

    function reportItems() {
      $scope.recipeReportForm.items = [];
      _.forEachRight($scope.stockRecipes, function (r, i) {
        var index = _.findIndex($scope.items, {_id: r.itemId});
        if (index >= 0) {
          r.recipeName = $scope.items[index].name;
        }
        else {
          $scope.stockRecipes.splice(i, 1);//deleted Recipes.
        }

        _.forEach($scope.items, function (i, ii) {
          if (i.name == r.recipeName) {
            //console.log(i.name);
            if (r.receipeDetails.length > 0) {
              $scope.recipeReportForm.items.push(angular.copy(r));
            }
          }
        })

      });
    };
    $scope.setReportItems = function () {

      reportItems();
      // if($scope.stockRecipes.isConverted==undefined){
      //   $scope.stockRecipes.isConverted=true;
      $scope.recipeReportForm.items = getDynamicPricingReport($scope.recipeReportForm.items);
      getRecipeItemTotal($scope.recipeReportForm.items);
      //console.log($scope.recipeReportForm.items);
      //$scope.recipeReportForm.items=angular.copy($scope.stockRecipes);
      $scope.recipeReportForm.menuItems = {};
    };

    function getRecipeItemTotal(items) {
      _.forEach(items, function (rec, iiii) {
        var gt_avPrice = 0;
        var gt_ltPrice = 0;
        _.forEach(rec.receipeDetails, function (itm, i) {
          gt_avPrice = parseFloat(gt_avPrice) + parseFloat(itm.calculatedAveragePrice);
          gt_ltPrice = parseFloat(gt_ltPrice) + parseFloat(itm.calculatedPrice);
        });
        rec.receipeDetails.sumAveragePrice = parseFloat(gt_avPrice).toFixed(4);
        rec.receipeDetails.sumLastPrice = parseFloat(gt_ltPrice).toFixed(4);
      });
    };

    $scope.itemChange_recipeReportForm = function (menuItems) {
      $scope.recipeReportForm.items = [];
      if (menuItems == null) {
        reportItems();
        $scope.recipeReportForm.items = getDynamicPricingReport($scope.recipeReportForm.items);
        getRecipeItemTotal($scope.recipeReportForm.items);
        //$scope.recipeReportForm.items=angular.copy($scope.recipeReportForm.items);
      }
      else {
        var index = _.findIndex($scope.stockRecipes, {itemId: $scope.recipeReportForm.menuItems._id});
        $scope.recipeReportForm.items.push(angular.copy($scope.stockRecipes[index]));
        $scope.recipeReportForm.items = getDynamicPricingReport($scope.recipeReportForm.items);
        getRecipeItemTotal($scope.recipeReportForm.items);
      }
    };

    function getRawMaterials() {
      var rawMaterial = [];
      var itmss = angular.copy($scope.materialEstimationForm.items);
      _.forEach(itmss, function (itm, i) {
        if (itm.qty != "" && !isNaN(itm.qty)) {
          var index = _.findIndex($scope.stockRecipes, {recipeName: itm.name});
          if (index >= 0) {
            var rIt = angular.copy($scope.stockRecipes[index].receipeDetails);
            _.forEach(rIt, function (r, ii) {
              if (r.isSemiProcessed == undefined) {
                var rIndex = _.findIndex(rawMaterial, {itemName: r.itemName});
                var bUName = getUnitBaseUNitName(r.selectedUnitId.unitName);
                var conversionFactor = getUnitBaseConversion(r.selectedUnitId._id);
                r.baseUnitName = bUName;
                var qty = parseFloat(itm.qty) * parseFloat(r.quantity) * parseFloat(conversionFactor);
                r.quantity = qty;
                if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                  r.quantity = parseFloat(r.quantity) / 1000;
                }
                if (rIndex >= 0) {
                  rawMaterial[rIndex].quantity += qty;
                }
                else {
                  rawMaterial.push(r);
                }
              }
              else {
                var index_Semi = _.findIndex($scope.stockRecipes, {recipeName: r.itemName});
                if (index_Semi >= 0) {
                  var r1It = angular.copy($scope.stockRecipes[index_Semi].receipeDetails);
                  _.forEach(r1It, function (r1, ii) {
                    if (r1.isSemiProcessed == undefined) {
                      var r1Index = _.findIndex(rawMaterial, {itemName: r1.itemName});
                      var bUName = getUnitBaseUNitName(r1.selectedUnitId);
                      var conversionFactor = getUnitBaseConversion(r1.selectedUnitId);
                      r1.baseUnitName = bUName;
                      var qty = parseFloat(itm.qty) * parseFloat(r1.quantity) * parseFloat(conversionFactor);
                      r1.quantity = qty;
                      if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                        r.quantity = parseFloat(r.quantity) / 1000;
                      }
                      if (r1Index >= 0) {
                        rawMaterial[r1Index].quantity += qty;
                      }
                      else {
                        rawMaterial.push(r);
                      }
                    }
                  });
                }
              }
            });
          }
        }
      });
      return rawMaterial;
    };

    function getMaterials_WithSemiItems() {
      var rawMaterial = [];
      var itmss = angular.copy($scope.materialEstimationForm.items);
      
      _.forEach(itmss, function (itm, i) {
        if (itm.stockQuantity != undefined) {
          if (itm.qty != "" && !isNaN(itm.qty)) {
            var index = _.findIndex($scope.stockRecipes, {itemId: itm._id});
            if (index >= 0) {
              var rIt = angular.copy($scope.stockRecipes[index].rawItems);
              _.forEach(rIt, function (r, ii) {
                
                var recipeQty = parseFloat($scope.stockRecipes[index].quantity).toFixed(3);
                var recConFactor = parseFloat($scope.stockRecipes[index].selectedUnitId.conversionFactor);
                var rIndex = _.findIndex(rawMaterial, {_id: r._id});
                var bUName = r.selectedUnitId.baseUnit.name;
                var conversionFactor = getUnitBaseConversion(r.selectedUnitId._id);
                r.baseUnitName = bUName;
                var menuQty = parseFloat(itm.stockQuantity) * parseFloat(itm.unit.conversionFactor);
                recipeQty = parseFloat(recipeQty) * parseFloat(recConFactor);
                var qty1 = parseFloat(parseFloat(menuQty) / parseFloat(recipeQty));
                var qty = parseFloat(itm.qty) * parseFloat(qty1) * parseFloat(r.quantity) * parseFloat(conversionFactor);
                r.calculatedQuantity = parseFloat(qty).toFixed(3);
                if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                    r.calculatedQuantity = parseFloat(parseFloat(r.calculatedQuantity) / 1000).toFixed(3);
                    
                }
                if (rIndex >= 0) {
                    rawMaterial[rIndex].calculatedQuantity = parseFloat(parseFloat(rawMaterial[rIndex].calculatedQuantity) + parseFloat(r.calculatedQuantity)).toFixed(3);
                    
                  
                  
                }
                else {

                  rawMaterial.push(r);
                }
              });
            }
          }
        }
        else {
          growl.error(itm.itemName + " don't have stockQuantity defined escaped from Estimation");
        }
      });
      return rawMaterial;
    };

    function getUnitBaseUNitName(unitId) {
      var index = _.findIndex($scope.stockUnit, {_id: unitId});
      var unitName = "";
      if (index >= 0) {
        unitName = $scope.stockUnit[index].baseUnit.name;
      }
      return unitName;
    };

    function getUnitBaseConversion(unitId) {
      var index = _.findIndex($scope.stockUnit, {_id: unitId});
      var conversionFactor = 1;
      if (index >= 0) {
        conversionFactor = $scope.stockUnit[index].conversionFactor;
      }
      return conversionFactor;
    };

    $scope.enableSubmitButton_MaterialEstimation = function (items) {
      var flag = false;
      _.forEach(items, function (item, i) {
        if (flag == false) {
          if (item.qty != "" && item.qty != undefined && !isNaN(item.qty)) {
            flag = true;
          }
          if (isNaN(item.qty)) {
            item.qty = "";
          }
        }
      });

      if (flag == true) {
        $scope.materialEstimationForm.enableSubmit = false;
      }
      else {
        $scope.materialEstimationForm.enableSubmit = true;
        growl.error('Please provide quantity', {ttl: 3000});
      }
      //return flag;
    };

    // $scope.submitMaterialEstimation = function () {
    //   var rawMaterial = getMaterials_WithSemiItems();
    //   //var rawMaterial=getRawMaterials();
    //   $modal.open({
    //     templateUrl: 'app/stock/recipeManagement/_materialEstimation.html',
    //     controller: ['$rootScope', '$scope', '$resource', '$modalInstance', function ($rootScope, $scope, $resource, $modalInstance) {
    //       $scope.cancel = function () {
    //         $modalInstance.dismiss('cancel');
    //       };

    //       $scope.rawMaterial = rawMaterial;
    //       $scope.print = function () {
    //         exporttoexcel();
    //         //printByCrome();
    //       };
    //       function exporttoexcel() {
    //         var table = document.getElementById("printTable");
    //         window.open('data:application/vnd.ms-excel,' + encodeURI(table.outerHTML));
    //         //window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#printTable').html()));
    //       };
    //       function printByCrome() {
    //         var table = document.getElementById("printTable");
    //         var printer = window.open('', '', 'width=600,height=600');
    //         printer.document.open("text/html");
    //         printer.document.write(table.outerHTML);
    //         printer.document.close();
    //         printer.focus();
    //         printer.print();
    //       };
    //     }],
    //     size: 'lg'
    //   });
    // };

    $scope.exportRecipeReport = function () {
      var table = document.getElementById("printTable_ReceipeReport");
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table.outerHTML));
    };


    //-------------------------------------------Material Estimation -----------------------------------------------//
    $scope.materialEstimationForm = {enableSubmit: true};
    $scope.materialEstimationForm.selectedCategory = {};
    $scope.material = function(){

    }
    function setMaterialEstimation  () {
      $scope.itm = {};
      $scope.materialEstimationForm = {enableSubmit: true};
      $scope.materialEstimationForm.selectedCategory = {};
      $scope.materialEstimationForm.availableCategory = angular.copy($scope.recipeCategory);
      //$scope.materialEstimationForm.remainingItems=angular.copy($scope.items);
      $scope.itm = angular.copy($scope.items);
      getAvailableItems_ME();
    }
    function getAvailableItems_ME() {
      $scope.materialEstimationForm.remainingItems = [];
      _.forEach($scope.materialEstimationForm.availableCategory, function (cat, i) {
        _.forEach($scope.itm, function (itm, ii) {
          if (itm.category._id == cat._id) {
            $scope.materialEstimationForm.remainingItems.push(itm);
          }
        });
      });
    };

    function itemsInCategory_materialEstimation(catId) {
      if ($scope.materialEstimationForm.items == undefined) {
        $scope.materialEstimationForm.items = [];
      }
      _.forEach($scope.items, function (itm, i) {
        if (itm.category._id == catId) {
          var chk = checkCategoryRemainingItemAdd(itm);
          if (chk == true) {
            $scope.materialEstimationForm.items.push(angular.copy(itm));
            var index = _.findIndex($scope.materialEstimationForm.remainingItems, {_id: itm._id});
            if (index >= 0) {
              $scope.materialEstimationForm.remainingItems.splice(index, 1);
            }
          }
          else {
            //growl.success('Items Dont have recipes Select another to add or create recipes first', {ttl: 3000});
          }
        }
      });
      if ($scope.materialEstimationForm.items.length <= 0) {
        growl.success('Items Dont have recipes Select another to add or create recipes first', {ttl: 3000});
      }
    };
    $scope.bindItems_materialEstimationForm = function (catId) {
      if ($scope.materialEstimationForm.selectedCategory != undefined) {
        if ($scope.materialEstimationForm.selectedCategory[catId] == true) {
          itemsInCategory_materialEstimation(catId);
        }
        else {
          _.forEachRight($scope.materialEstimationForm.items, function (itm, i) {
            if (itm.category._id == catId) {
              $scope.materialEstimationForm.items.splice(i, 1);
              $scope.materialEstimationForm.remainingItems.push(itm);
            }
          });
        }
      }
    };

    $scope.goNextME = function (index) {
      var f = angular.element(document.querySelector('#me_' + index));
      if (f.length != 0) {
        f.focus();
      }
      else {
        // $scope.materialEstimationForm.enableSubmit=false;
        // angular.element(document.querySelector('#submitMaterialEstimationForm')).focus();
      }
    };

    function checkCategoryRemainingItemAdd(itm) {
      //var recipes=angular.copy($scope.stockRecipes);
      var index = _.findIndex($scope.stockRecipes, {itemId: itm._id});
      if (index < 0) {
        return false;
      }
      else {
        return true;
      }
    };

    $scope.deleteSelectedMEForm = function (item) {
      var index = _.findIndex($scope.materialEstimationForm.items, {"name": item.name});
      $scope.materialEstimationForm.items.splice(index, 1);
      $scope.materialEstimationForm.remainingItems.push(item);
    };

    $scope.addRemainingItem_materialEstimationForm = function ($item, $model, $label) {
      var remaining = angular.copy($scope.materialEstimationForm.remainingItems);
      if ($scope.materialEstimationForm.items == undefined) {
        $scope.materialEstimationForm.items = [];
      }

      var chk = checkCategoryRemainingItemAdd($item);
      if (chk == true) {
        _.forEach($scope.materialEstimationForm.remainingItems, function (itm, i) {
          if (itm._id == $item._id) {
            $scope.materialEstimationForm.items.push(itm);
            remaining.splice(i, 1);
            $scope.materialEstimationForm.itemToAdd = "";
          }
        });
        $scope.materialEstimationForm.remainingItems = remaining;
        var cS = $item.category._id;
        $scope.materialEstimationForm.selectedCategory[cS] = true;
      }
      else {
        growl.success('Items Dont have recipes Select another to add or create recipes first', {ttl: 3000});
        $scope.materialEstimationForm.itemToAdd = "";
      }
    };

    function getRawMaterials() {
      var rawMaterial = [];
      var itmss = angular.copy($scope.materialEstimationForm.items);
      _.forEach(itmss, function (itm, i) {
        if (itm.qty != "" && !isNaN(itm.qty)) {
          var index = _.findIndex($scope.stockRecipes, {recipeName: itm.name});
          if (index >= 0) {
            var rIt = angular.copy($scope.stockRecipes[index].receipeDetails);
            _.forEach(rIt, function (r, ii) {
              if (r.isSemiProcessed == undefined) {
                var rIndex = _.findIndex(rawMaterial, {itemName: r.itemName});
                var bUName = getUnitBaseUNitName(r.selectedUnitId.unitName);
                var conversionFactor = getUnitBaseConversion(r.selectedUnitId._id);
                r.baseUnitName = bUName;
                var qty = parseFloat(itm.qty) * parseFloat(r.quantity) * parseFloat(conversionFactor);
                r.quantity = qty;
                if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                  r.quantity = parseFloat(r.quantity) / 1000;
                }
                if (rIndex >= 0) {
                  rawMaterial[rIndex].quantity += qty;
                }
                else {
                  rawMaterial.push(r);
                }
              }
              else {
                var index_Semi = _.findIndex($scope.stockRecipes, {recipeName: r.itemName});
                if (index_Semi >= 0) {
                  var r1It = angular.copy($scope.stockRecipes[index_Semi].receipeDetails);
                  _.forEach(r1It, function (r1, ii) {
                    if (r1.isSemiProcessed == undefined) {
                      var r1Index = _.findIndex(rawMaterial, {itemName: r1.itemName});
                      var bUName = getUnitBaseUNitName(r1.selectedUnitId);
                      var conversionFactor = getUnitBaseConversion(r1.selectedUnitId);
                      r1.baseUnitName = bUName;
                      var qty = parseFloat(itm.qty) * parseFloat(r1.quantity) * parseFloat(conversionFactor);
                      r1.quantity = qty;
                      if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                        r.quantity = parseFloat(r.quantity) / 1000;
                      }
                      if (r1Index >= 0) {
                        rawMaterial[r1Index].quantity += qty;
                      }
                      else {
                        rawMaterial.push(r);
                      }
                    }
                  });
                }
              }
            });
          }
        }
      });
      return rawMaterial;
    };

   // according to billing qty
function getMaterials_WithSemiItems() {
      var rawMaterial = [];
      var itmss = angular.copy($scope.materialEstimationForm.items);
      _.forEach(itmss, function (itm, i) {
        if (itm.stockQuantity != undefined) {
          if (itm.qty != "" && !isNaN(itm.qty)) {
            var index = _.findIndex($scope.stockRecipes, {itemId: itm._id});
            //console.log("INDEX", index)
            if (index >= 0) {
              //console.log("STOCK RECIPE", $scope.stockRecipes[index])
              var estimationFactor = parseFloat((Number(itm.stockQuantity)  * Number(itm.qty) * Number(itm.unit.conversionFactor))/(Number($scope.stockRecipes[index].quantity) * $scope.stockRecipes[index].selectedUnitId.conversionFactor)).toFixed(3);
              var obj ={
              stockQuantity :itm.stockQuantity,
              qty :itm.qty,
              conversionFactor : itm.unit.conversionFactor,
              quantity : $scope.stockRecipes[index].quantity,
              selectedUnitIdconversionFactor : $scope.stockRecipes[index].selectedUnitId.conversionFactor                                                                        
              }
              //console.log(itm.stockQuantity,itm.qty,itm.unit.conversionFactor,$scope.stockRecipes[index].quantity,$scope.stockRecipes[index].selectedUnitId.conversionFactor);
              var rIt = angular.copy($scope.stockRecipes[index].receipeDetails);

              _.forEach(rIt, function (r, ii) {
                if(_.has(r, 'receipeDetails')){
                  var irIndex = _.findIndex($scope.stockRecipes, {_id: r._id})
                  if(irIndex > -1){
                    var irIt = angular.copy($scope.stockRecipes[irIndex].receipeDetails);
                    var irEstimationFactor = parseFloat((Number(r.quantity) * Number(r.selectedUnitId.baseConversionFactor)) / (Number($scope.stockRecipes[irIndex].quantity) * $scope.stockRecipes[irIndex].selectedUnitId.conversionFactor)).toFixed(3);
                    _.forEach(irIt, function (it) {
                      var rIndex = _.findIndex(rawMaterial, {_id: it._id});
                      if(rIndex > -1) {
                        var conversionFactor = 1;
                        if(it.selectedUnitId){
                          conversionFactor = it.selectedUnitId.baseConversionFactor;

                        }
                        rawMaterial[rIndex].calculatedQuantity = parseFloat(parseFloat(rawMaterial[rIndex].calculatedQuantity) + parseFloat(it.quantity * conversionFactor * irEstimationFactor * estimationFactor)).toFixed(3);        
                      } else {
                        var conversionFactor = 1;
                        if(it.selectedUnitId){
                          conversionFactor = it.selectedUnitId.baseConversionFactor;
                             if(it.isBaseKitchenItem==true)
                           {
                             if(it.selectedUnitId.baseUnit.id==1  || it.selectedUnitId.baseUnit.id==4)
                                conversionFactor = it.selectedUnitId.baseUnit.value;
                             else if(it.selectedUnitId.baseUnit.id==2  || it.selectedUnitId.baseUnit.id==3)
                                conversionFactor = 1;
                           }
                        }
                        it.calculatedQuantity = parseFloat(estimationFactor * irEstimationFactor * it.quantity * conversionFactor).toFixed(3);

                        console.log(it.itemName, estimationFactor, it.quantity, conversionFactor);
                        console.log(it)
                          if(it.isBaseKitchenItem!=undefined)
                          {
                              
                              var l_index = _.findIndex($scope.itemLastAndAveragePriceBasekitchen, {itemId: it._id});
                              var avgPrice
                              console.log("bk item",$scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage)
                              if(l_index>-1)
                                 avgPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage
                               else
                                avgPrice=0
                              if(it.selectedUnitId.baseUnit.id == 2 || it.selectedUnitId.baseUnit.id == 3){
                                console.log("lit")
                                it.price = parseFloat( it.selectedUnitId.conversionFactor * it.calculatedQuantity * avgPrice ).toFixed(2)
                              }
                                
                              else
                                it.price = parseFloat( it.selectedUnitId.conversionFactor * it.calculatedQuantity * avgPrice ).toFixed(2)
                          }
                          else
                          {

                            var l_index = _.findIndex($scope.itemLastAndAveragePrice, {itemId: it._id});
                            var avgPrice
                              if(l_index>-1)
                                 avgPrice = $scope.itemLastAndAveragePrice[l_index].monthAverage
                               else
                                avgPrice=0
                                if(it.selectedUnitId.baseUnit.id == 2 || it.selectedUnitId.baseUnit.id == 3)
                                  it.price = parseFloat(   parseFloat(it.calculatedQuantity * avgPrice) * it.selectedUnitId.conversionFactor ).toFixed(2)
                                else
                                   it.price = parseFloat(   parseFloat(it.calculatedQuantity * avgPrice) ).toFixed(2)
                          }
                        rawMaterial.push(it);
                      }
                    });
                  }

                  // var recipeQty = parseFloat($scope.stockRecipes[index].quantity).toFixed(3);
                  // var recConFactor = parseFloat($scope.stockRecipes[index].selectedUnitId.conversionFactor);
                  // var rIndex = _.findIndex(rawMaterial, {_id: r._id});
                  // var bUName = r.selectedUnitId.baseUnit.name;
                  // var conversionFactor = getUnitBaseConversion(r.selectedUnitId._id);
                  // r.baseUnitName = bUName;
                  // var menuQty = parseFloat(itm.stockQuantity) * parseFloat(itm.unit.conversionFactor);
                  // recipeQty = parseFloat(recipeQty) * parseFloat(recConFactor);
                  // var qty1 = parseFloat(parseFloat(menuQty) / parseFloat(recipeQty));
                  // var qty = parseFloat(itm.qty) * parseFloat(qty1) * parseFloat(r.quantity) * parseFloat(conversionFactor);
                  // r.calculatedQuantity = parseFloat(qty).toFixed(3);
                  // console.log("calculated quantity for item", r.calculatedQuantity)
                  // if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                  //   r.calculatedQuantity = parseFloat(parseFloat(r.calculatedQuantity) / 1000).toFixed(3);
                  // }
                  // if (rIndex >= 0) {
                  //   rawMaterial[rIndex].calculatedQuantity = parseFloat(parseFloat(rawMaterial[rIndex].calculatedQuantity) + parseFloat(r.calculatedQuantity)).toFixed(3);
                  // }
                  // else {
                  //   rawMaterial.push(r);
                  // }
                } else {
                  var rIndex = _.findIndex(rawMaterial, {_id: r._id});
                  if(rIndex > -1) {
                    var conversionFactor = 1;
                    if(r.selectedUnitId){
                      conversionFactor = r.selectedUnitId.baseConversionFactor;
                        if(r.isBaseKitchenItem==true)
                       {
                         if(r.selectedUnitId.baseUnit.id==1  || r.selectedUnitId.baseUnit.id==4)
                            conversionFactor = r.selectedUnitId.baseUnit.value;
                         else if(r.selectedUnitId.baseUnit.id==2  || r.selectedUnitId.baseUnit.id==3)
                            conversionFactor = 1;
                       }
                    }
                    rawMaterial[rIndex].calculatedQuantity = parseFloat(parseFloat(rawMaterial[rIndex].calculatedQuantity) + parseFloat(r.quantity * estimationFactor) * conversionFactor).toFixed(3);        
                  } else {
                    var conversionFactor = 1;
                    if(r.selectedUnitId){
                      conversionFactor = r.selectedUnitId.baseConversionFactor;
                        if(r.isBaseKitchenItem==true)
                        {
                          if(r.selectedUnitId.baseUnit.id==1  || r.selectedUnitId.baseUnit.id==4)
                            conversionFactor = parseFloat(r.selectedUnitId.baseUnit.value).toFixed(3);
                          else if(r.selectedUnitId.baseUnit.id==2  || r.selectedUnitId.baseUnit.id==3)
                            conversionFactor = parseFloat(1).toFixed(3);  
                        }
                    }
                    r.calculatedQuantity = parseFloat(estimationFactor * r.quantity * conversionFactor).toFixed(3);

                    console.log(r.itemName, estimationFactor, r.quantity, conversionFactor);
                    console.log(r)
                    if(r.isBaseKitchenItem!=undefined)
                    {
                        
                        var l_index = _.findIndex($scope.itemLastAndAveragePriceBasekitchen, {itemId: r._id});
                        var avgPrice
                        console.log("bk item",$scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage)
                        if(l_index>-1)
                           avgPrice = $scope.itemLastAndAveragePriceBasekitchen[l_index].monthAverage
                         else
                          avgPrice=0
                      if(r.selectedUnitId.baseUnit.id == 2 || r.selectedUnitId.baseUnit.id == 3){
                        console.log("litre")
                        r.price = parseFloat( r.selectedUnitId.conversionFactor * r.calculatedQuantity * avgPrice  ).toFixed(2)
                      }
                        
                      else
                        r.price = parseFloat(r.selectedUnitId.conversionFactor * r.calculatedQuantity *  avgPrice  ).toFixed(2)
                    }
                    else
                    {

                      var l_index = _.findIndex($scope.itemLastAndAveragePrice, {itemId: r._id});
                      var avgPrice
                        if(l_index>-1)
                           avgPrice = $scope.itemLastAndAveragePrice[l_index].monthAverage
                         else
                          avgPrice=0
                      if(r.selectedUnitId.baseUnit.id == 2 || r.selectedUnitId.baseUnit.id == 3)
                        r.price = parseFloat(  parseFloat(r.calculatedQuantity *  avgPrice ) * r.selectedUnitId.conversionFactor  ).toFixed(2)
                      else
                        r.price = parseFloat(  parseFloat(r.calculatedQuantity *  avgPrice )  ).toFixed(2)
                    }
                  
                    rawMaterial.push(r);
                  }
                }
              });
            }
          }
        }
        else {
          growl.error(itm.itemName + " don't have stockQuantity defined escaped from Estimation");
        }
      });
      return rawMaterial;
    };

    function convertItemInPrefferedUnit(result) {
      _.forEach(result, function (itm, i) {
          if(itm.isBaseKitchenItem==true)
        {
         // console.log("bsae",itm)
          itm.baseUnitName = itm.selectedUnitId.unitName;
        }
        var indeex = _.findIndex($scope.stockItem, {_id: itm._id});
        if (indeex >= 0) {
          var item = $scope.stockItem[indeex];
          if (item.preferedUnit != undefined) {
            _.forEach(item.units, function (u, i) {
              if (u._id == item.preferedUnit) {
                var conversionFactor = 1;
                var pconFac = parseFloat(u.conversionFactor);
                itm.baseUnitName = u.unitName;
                if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
                  conversionFactor = parseFloat(u.baseConversionFactor);
                  if (pconFac > conversionFactor) {
                    itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) / parseFloat(conversionFactor)).toFixed(3);
                  }
                  else if (pconFac < conversionFactor) {
                    itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) * parseFloat(conversionFactor)).toFixed(3);
                  }
                }
                else {
                  if (pconFac > conversionFactor) {
                    itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) / parseFloat(pconFac)).toFixed(3);
                  }
                  else if (pconFac < conversionFactor) {
                    itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) * parseFloat(pconFac)).toFixed(3);
                  }
                }
              }
            });
          }
          else
          {
               if(itm.baseUnitName==undefined)
                itm.baseUnitName = itm.selectedUnitId.baseUnit.name;
          }
        }
      });
      return result;
    };

    function getUnitBaseUNitName(unitId) {
      var index = _.findIndex($scope.stockUnit, {_id: unitId});
      var unitName = "";
      if (index >= 0) {
        unitName = $scope.stockUnit[index].baseUnit.name;
      }
      return unitName;
    };

    function getUnitBaseConversion(unitId) {
      var index = _.findIndex($scope.stockUnit, {_id: unitId});
      var conversionFactor = 1;
      if (index >= 0) {
        conversionFactor = $scope.stockUnit[index].conversionFactor;
      }
      return conversionFactor;
    };

    $scope.enableSubmitButton_MaterialEstimation = function (items) {
      var flag = false;
      _.forEach(items, function (item, i) {
        if (flag == false) {
          if (item.qty != "" && item.qty != undefined && !isNaN(item.qty)) {
            flag = true;
          }
          if (isNaN(item.qty)) {
            item.qty = "";
          }
        }
      });

      if (flag == true) {
        $scope.materialEstimationForm.enableSubmit = false;
      }
      else {
        $scope.materialEstimationForm.enableSubmit = true;
        growl.error('Please provide quantity', {ttl: 3000});
      }
      //return flag;
    };

    $scope.submitMaterialEstimation = function () {
      var rawMaterial = getMaterials_WithSemiItems();

      rawMaterial = convertItemInPrefferedUnit(rawMaterial);
      //var rawMaterial=getRawMaterials();
      $modal.open({
        templateUrl: 'app/stock/recipeManagement/_materialEstimation.html',
        controller: ['$rootScope', '$scope', '$resource', '$modalInstance', function ($rootScope, $scope, $resource, $modalInstance) {
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.rawMaterial = rawMaterial;
          $scope.print = function () {
            exporttoexcel();
            //printByCrome();
          };
          function exporttoexcel() {
            var table = document.getElementById("printTable");
            window.open('data:application/vnd.ms-excel,' + encodeURI(table.outerHTML));
            //window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#printTable').html()));
          };
          function printByCrome() {
            var table = document.getElementById("printTable");
            var printer = window.open('', '', 'width=600,height=600');
            printer.document.open("text/html");
            printer.document.write(table.outerHTML);
            printer.document.close();
            printer.focus();
            printer.print();
          };
        }],
        size: 'lg'
      });
    };

  }]);
