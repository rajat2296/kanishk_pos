'use strict';

describe('Controller: RecipeManagementCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var RecipeManagementCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RecipeManagementCtrl = $controller('RecipeManagementCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
