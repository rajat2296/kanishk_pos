'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('recipeManagement', {
        url: '/recipes',
        templateUrl: 'app/stock/recipeManagement/recipeManagement.html',
        controller: 'RecipeManagementCtrl'
      });
  });