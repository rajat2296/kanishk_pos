'use strict';

describe('Controller: OutletIndentingCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var OutletIndentingCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OutletIndentingCtrl = $controller('OutletIndentingCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
