'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('outletIndenting', {
        url: '/outletIndenting',
        templateUrl: 'app/stock/outletIndenting/outletIndenting.html',
        controller: 'OutletIndentingCtrl'
      });
  });