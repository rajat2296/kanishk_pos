'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('indentGRN', {
        url: 'indentGRN',
        templateUrl: 'app/stock/indentGRN/indentGRN.html',
        controller: 'IndentGRNCtrl',
        resolve: {
                    currentUser: ['$state', '$stateParams', 'Auth',
                        function ($state, $stateParams, Auth) {
                            return Auth.getCurrentUser().$promise.then(function (user) {
                                return user;
                            });
                        }]
                }
      });
  });