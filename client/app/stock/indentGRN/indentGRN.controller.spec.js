'use strict';

describe('Controller: IndentGRNCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var IndentGRNCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IndentGRNCtrl = $controller('IndentGRNCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
