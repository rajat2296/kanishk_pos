'use strict'

angular.module('posistApp')
  .controller('IndentGRNCtrl', ['$q', '$scope', '$rootScope', '$filter', '$timeout', '$compile', 'Email', 'StockTransaction', 'currentUser', 'Deployment', 'store', 'stockUnit', 'vendor', 'receiver', 'stockRequirement', '$resource', 'growl', 'localStorageService', 'LastBillNo', 'Item', 'stockRecipe', 'stockItem', 'StockResource', 'ClosingQuantity', 'baseKitchenItem', 'baseKitchenUnit', 'property', '$http','Utils','currentDeployment', 'StockReportNew', 'StockComment', function ($q, $scope, $rootScope, $filter, $timeout, $compile, Email, StockTransaction, currentUser, Deployment, store, stockUnit, vendor, receiver, stockRequirement, $resource, growl, localStorageService, LastBillNo, Item, stockRecipe, stockItem, StockResource, ClosingQuantity, baseKitchenItem, baseKitchenUnit, property, $http,Utils,currentDeployment, StockReportNew, StockComment) {

    currentUser.deployment_id = localStorageService.get('deployment_id');
    //console.log(currentUser);
    $scope.stores = [];
    $scope.stockUnits = [];
    $scope.vendors = [];
    $scope.receivers = [];
    $scope.deliveryChallans = [];
    $scope.receiveGRN = [];
    $scope.requirements = [];
    $scope.requirementOrder = [];
    $scope.requirementChallan = [];
    $scope.requirementReceive = [];
    $scope.deployments = [];
    $scope.receiveComments = [];

     
    $scope.billNo = LastBillNo;
    $scope.today = new Date();
    $scope.setting = localStorageService.get('stockSetting');
    $scope.POTransactionTypes = [
      {id: '1', Name: 'indentGRNOrder', transactionType: '1'},
      {id: '2', Name: 'indentGRNDeliveryChallan', transactionType: '2'},
      {id: '3', Name: 'indentGRNReceive', transactionType: '3'}
    ];

    //modified 14/7/16
    $scope.consolidationpagination = {};
    $scope.consolidationpagination.currentPage = 1;
    $scope.consolidationpagination.itemsPerPage = 20;
    $scope.consolidationPagination=function(){
      $scope.Pagination.numPages =
        Math.ceil($scope.indentGrnDeliveryChallanForm.consolidatedRequirements.length /
          $scope.Pagination.perPage);
      $scope.Pagination.totalItems = $scope.indentGrnDeliveryChallanForm.consolidatedRequirements.length;
    };

    var stockProperty = null;
    //Todo comment this line and uncomment others
    $scope.hidePricing=false;
/*    property.get({
      tenant_id: localStorageService.get("tenant_id"),
      deployment_id: localStorageService.get("deployment_id")
    }, function (properties) {
      if(properties.length==0)
      {
         $scope.hidePricing = false;
        
      }
      else
      {
          stockProperty = properties[0];
          $scope.isPhysicalStock=stockProperty.isPhysicalStock
          if (!stockProperty.property.hidePricingEnabled)
            $scope.hidePricing = false;
          else {
            _.forEach(currentUser.selectedRoles, function (role) {
              if (stockProperty.property.hidePricing[role.name]) {
                $scope.hidePricing = true;
              }
            });
          }
      }
  
    });
*/
    $scope.limitIndent = 15;
    $scope.skipIndent = 0;
    $scope.limitSupply = 15;
    $scope.skipSupply = 0;
    $scope.limitReceive = 20;
    $scope.skipReceive = 0;
    // $scope.deployments = deployments;
    $scope.loadMore = false;
    $scope.loadMoreSupply = false;
    $scope.loadMoreReceive = false;
    $scope.req = {
      indents :[],
      suppliedIndents:[],

    };
    $scope.newPage=function(currentPageNumber){
      var skip=(currentPageNumber-1)*15;
      var limit=15
      $scope.fetchChallan(skip,limit);
    };
    $scope.newPageSupply=function(currentPageNumber){
      var skip=(currentPageNumber-1)*15;
      var limit=15
      $scope.fetchSuppliedIndent(skip,limit);
    };
    $scope.newPageReceive=function(currentPageNumber){
      var skip=(currentPageNumber-1)*15;
      var limit=15
      $scope.fetchReceiveIndent(skip,limit);
    };
    $scope.fetchChallan = function (skipIndent, limitIndent) {

      if ((skipIndent || skipIndent == 0) && limitIndent) {
        $scope.req.indents = [];
        $scope.skipIndent = skipIndent;
        $scope.limitIndent = limitIndent;
      }
      var query = {
        toDeployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),
        isSupplied:0
      }
      query.skip = skipIndent == 0 || skipIndent ? skipIndent : $scope.skipIndent;
      query.limit = limitIndent ? limitIndent : $scope.limitIndent;
      stockRequirement.getRequirementsPagination(query, function (stockreq) {
        // console.log(stockreq);
        $scope.req.indents=stockreq;
        });
      stockRequirement.getCount({ toDeployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),isSupplied:0},function(count){
        //console.log(count.count);
        $scope.countChallan=count.count;
      });

    };
    $scope.fetchChallan(0,15);
    //chk Req pagination
    $scope.chkpagination = {};
    $scope.chkpagination.currentPage = 1;
    $scope.chkpagination.itemsPerPage = 15;

    $scope.fetchSuppliedIndent=function(skipSupply, limitSupply){
      if ((skipSupply || skipSupply == 0) && limitSupply) {
        $scope.req.suppliedIndents = [];
        $scope.skipSupply = skipSupply;
        $scope.limitSupply = limitSupply;
      }
      var query = {
        toDeployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),
        isSupplied:1
      }
      query.skip = skipSupply == 0 || skipSupply ? skipSupply : $scope.skipSupply;
      query.limit = limitSupply ? limitSupply : $scope.limitSupply;
      stockRequirement.getRequirementsPagination(query, function (stockreq) {
        $scope.req.suppliedIndents=stockreq;
      });
      stockRequirement.getCount({ toDeployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),isSupplied:1},function(count){
        $scope.countSupply=count.count;
      });

    };
    $scope.fetchSuppliedIndent(0,15);
    //Supply Req pagination
    $scope.supplypagination = {};
    $scope.supplypagination.currentPage = 1;
    $scope.supplypagination.itemsPerPage = 15;

    $scope.fetchReceiveIndent=function(skipReceive, limitReceive){
      if ((skipReceive || skipReceive == 0) && limitReceive) {
        $scope.req.receiveIndents = [];
        $scope.skipReceive = skipReceive;
        $scope.limitReceive = limitReceive;
      }
      var query = {
        deployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),
        isSupplied:2,

      }
      query.skip = skipReceive == 0 || skipReceive ? skipReceive : $scope.skipReceive;
      query.limit = limitReceive ? limitReceive : $scope.limitReceive;
      stockRequirement.getRequirementsPagination(query, function (stockreq) {
        
        $scope.req.receiveIndents = stockreq;
      })
      stockRequirement.getCount({ deployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),isSupplied:2},function(count){
        $scope.countReceive=count.count;
      });
    }

    $scope.fetchReceiveIndent(0,15);
    //Receive Req pagination
    $scope.receivepagination = {};
    $scope.receivepagination.currentPage = 1;
    $scope.receivepagination.itemsPerPage = 15;

    console.log();



    $scope.transaction = [{id: '11', Name: 'transferItemsToki'}]
    $scope.processedCat = {_id: "PosistTech111111", categoryName: "Processed Food"};
    $scope.processedSemi_Cat = {_id: "PosistTech222222", categoryName: "InterMediate Food"};
    function selectSelectedPreferredUnit() {
      _.forEach($scope.indentGrnOrderForm.availableItems, function (st) {

        $scope.indentGrnOrderForm.selectedUnits[st._id] = st.preferedUnit
      });
    };

    $scope.startsWith = function (item, viewValue) {
      return item.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
    };

    var startDate = new Date();
    startDate.setDate(startDate.getDate());
    startDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(startDate))));
    var endDate = new Date();
    endDate.setDate(endDate.getDate() + 1);
    endDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(endDate))));
    console.log(currentDeployment);
    var req = {
      fromDate: startDate,
      toDate: endDate,
      deployment_id: localStorageService.get('deployment_id'),
      tenant_id: localStorageService.get('tenant_id')
    };
    var cls = new ClosingQuantity(null, startDate, endDate, localStorageService.get('deployment_id'), localStorageService.get('tenant_id'));
    cls.getClosingQty().then(function (data) {
      $scope.closingQty = data;
      });

    function getDeployment() {
      return Deployment.get({tenant_id: currentUser.tenant_id});
    };
    function getStores_ByDeployment() {
      var deferred = $q.defer();
      store.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id}, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      // return store.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getStockUnits_ByDeployment() {
      var deferred = $q.defer();
      stockUnit.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      //return stockUnit.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };
    function getVendors_ByDeployment() {
      var deferred = $q.defer();
      vendor.getHOVendor({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      //return vendor.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getReceivers_ByDeployment() {
      var deferred = $q.defer();
      receiver.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      // return receiver.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getRequirements_ByDeployment() {
      //if(currentUser.isBaseKitchen)
      //console.log("IS BASE", $scope.currentDeployment.isBaseKitchen)
      if ($scope.currentDeployment.isBaseKitchen)
        return stockRequirement.get({tenant_id: currentUser.tenant_id, toDeployment_id: currentUser.deployment_id});
      else
        return stockRequirement.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };
    function getRequirements_BillNo_ByDeployment() {
      return stockRequirement.getLastRequirementBillNo({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      });
    };

    function getItems_ProcessedFood() {
      var deferred = $q.defer();
      Item.getProcessedItem({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      // return Item.getProcessedItem({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getItems_ProcessedFoodByDeployment(deploymentId) {
      var deferred = $q.defer();
      Item.getProcessedItem({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      //console.log("deployment", deploymentId)
      //return Item.getProcessedItem({tenant_id: currentUser.tenant_id, deployment_id: deploymentId});
    };
    function getItems_SemiProcessedFood() {
      var deferred = $q.defer();
      Item.getSemiProcessedItem({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      // return Item.getSemiProcessedItem({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };
    function getstockRecies() {
      var deferred = $q.defer();
      stockRecipe.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      //  return stockRecipe.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    //  function getItems(){
    //  return Item.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    // };

    function getstockItems() {
      var deferred = $q.defer();
      stockRecipe.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      //  return stockItem.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function getItems() {
      var deferred = $q.defer();
      Item.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id}, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return Item.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };
    function getSettings() {
      var deferred = $q.defer();
      property.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(err);
      });
      return deferred.promise;
      // return property.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id});
    };

    function print_Entry(table1) {
      //console.log("TABLE", table1);
      var printer = window.open('', '', 'width=600,height=600');
      if(!printer){
        growl.error('Unable to open print window. Please Enable Popup in your Browser Settings.',{ttl:3000})
      } else {
        //console.log("PRINTER", printer);
        printer.document.open("text/html");
        printer.document.write(table1);
        printer.document.close();
        printer.focus();
        printer.print();
      }
    };

    function getItemsOfAnotherDeploymentByCategory(data) {
      var uniqueListOfProcessedCategories = [];
      //console.log("process in func custom", data)
      _.forEach(data, function (c, i) {
        //3console.log("COMP custom", c)
        var check = false;
        _.forEach(uniqueListOfProcessedCategories, function (u, i) {
          //console.log("U", u)
          //console.log("C", c)
          if (u._id == c.category._id) {
            var object = {
              _id: c._id,
              itemName: c.itemName,
              units: c.units,
              unit: c.unit,
              number: c.number,
              tabs: c.tabs,
              stockQuantity: c.stockQuantity,
              rate: c.rate
            }
            u.item.push(object);
            //console.log("PUSHING IYEM custom", object)
            check = true;
          }
          ;
        });
        //console.log("out of loop cusyom")
        //console.log("CHECK custom", check)
        if (!check) {
          //console.log("CHECK FALSE custom", check);
          var object = {
            _id: c.category._id,
            categoryName: c.category.categoryName,
            item: [{
              _id: c._id,
              itemName: c.itemName,
              units: c.units,
              unit: c.unit,
              number: c.number,
              tabs: c.tabs,
              stockQuantity: c.stockQuantity,
              rate: c.rate
            }],
            type: "MenuItem"
          }
          uniqueListOfProcessedCategories.push(object);
          //console.log("PUSHING OBJECT", object)
        }
      })
      return uniqueListOfProcessedCategories
    };


    function getItemsByCategory() {
      var uniqueListOfProcessedCategories = [];
      //console.log("process in func", $scope.processedCat.item)
      _.forEach($scope.processedCat.item, function (c, i) {
        //console.log("COMP", c)
        var check = false;
        _.forEach(uniqueListOfProcessedCategories, function (u, i) {
          if (u._id == c.category._id) {
            var object = {_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number};
            u.item.push(object);
            check = true;
          }
        });
        if (!check) {
          var object = {
            _id: c.category._id,
            categoryName: c.category.categoryName,
            isSemiProcessed: c.category.isSemiProcessed,
            item: [{_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number}]
          }
          uniqueListOfProcessedCategories.push(object);
        }
      });
      var uniqueListOfSemiProcessedCategories = []
      //console.log("process in func", $scope.processedCat.item)
      _.forEach($scope.processedSemi_Cat.item, function (c, i) {
        //console.log("COMP", c)
        var check = false;
        _.forEach(uniqueListOfSemiProcessedCategories, function (u, i) {
          if (u._id == c.category._id) {
            var object = {_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number};
            u.item.push(object);
            check = true;
          }
          ;
        });
        if (!check) {
          var object = {
            _id: c.category._id,
            categoryName: c.category.categoryName,
            isSemiProcessed: c.category.isSemiProcessed,
            item: [{_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number}]
          }
          uniqueListOfSemiProcessedCategories.push(object);
        }
      })
      //console.log("PROCESSED created", uniqueListOfProcessedCategories)
      $scope.processedItemsByCategory = uniqueListOfProcessedCategories;
      //console.log("SEMI PROCESSED CREATED", uniqueListOfSemiProcessedCategories)
      $scope.semiprocessedItemsByCategory = uniqueListOfSemiProcessedCategories;
      $scope.allItemsByCategories = [];
      _.forEach($scope.processedItemsByCategory, function (c, i) {
        c.type = 'processed';
        $scope.allItemsByCategories.push(c)
      });

      _.forEach($scope.semiprocessedItemsByCategory, function (c, i) {
        c.type = 'semiProcessed';
        $scope.allItemsByCategories.push(c)
      });
      _.forEach($scope.stores, function (s, i) {
        _.forEach(s.category, function (c, i) {
          c.type = 'raw';
          $scope.allItemsByCategories.push(c)
        });
      });
      //console.log("stores copy", $scope.storesCopy)
      //console.log("all items by cates", $scope.allItemsByCategories)
    }

    //console.log("CURRENT USR Deployment", currentUser.deployment_id)


    Deployment.get({tenant_id: currentUser.tenant_id}).$promise.then(function (dep) {
      $scope.deployment = dep;
      _.forEach(dep, function (d, i) {
        if (d._id == currentUser.deployment_id) {
          $scope.currentDeployment = d;
          return
        }
      });
      var isVendor = false;
      _.forEach(currentUser.selectedRoles, function (role) {
        if(role.name == 'Vendor')
          isVendor = true;
      });
      console.log(isVendor);
      if ($scope.currentDeployment.isBaseKitchen && isVendor) {
        stockRequirement.get({
          tenant_id: currentUser.tenant_id,
          toDeployment_id: currentUser.deployment_id
        }).$promise.then(function (result) {
          $scope.requirements = result;
          GetAllRequirements(result);
          getItemsByCategory();
        });
      }
      else {
        stockRequirement.get({
          tenant_id: currentUser.tenant_id,
          deployment_id: currentUser.deployment_id
        }).$promise.then(function (result) {
          $scope.requirements = result;
          GetAllRequirements(result);
          getItemsByCategory();
        });
      }
      property.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }).$promise.then(function (resultSet) {
        $scope.settings = resultSet[0];
        if (resultSet[0] == undefined) {
          $scope.settings = {};
        }
        //console.log("resultSet", resultSet);
        //console.log("settings", $scope.settings)
      });

      function getReceiveComments () {
        var d = $q.defer();
        StockComment.query({deployment_id: localStorageService.get('deployment_id'), commentForId: 1}, function (comments) {
          d.resolve(comments);
        }, function (err) {
          d.reject(err);
        });
        return d.promise;
      }

      var allPromise = $q.all([
        getStores_ByDeployment(), getStockUnits_ByDeployment(),
        getVendors_ByDeployment(), getDeployment(), getstockRecies(),
        getItems_ProcessedFood(), getItems_SemiProcessedFood(), getstockItems(), getItems(), 
        getReceiveComments()
        //getRequirements_BillNo_ByDeployment()
      ]);

      allPromise.then(function (value) {
        $scope.stores = value[0];
        $scope.stockUnits = value[1];
        $scope.vendors = value[2];
        $scope.deployments = value[3];
        $scope.stockRecipes = value[4];
        $scope.processedCat.item = value[5];
        $scope.processedSemi_Cat.item = value[6];
        $scope.stockItem = value[7];
        $scope.items = value[8];
        $scope.receiveComments = value[9];
        /*console.log("stock items", value[8]);
         console.log("items", value[9]);
         console.log("STROES", value[0])
         console.log("RECIPES", value[7])*/
      });
    });
    //console.log("store", $scope.stores)
    //-------------------------------------------Indent-GRN Orders  ----------------------------------------------------
    $scope.indentGrnOrderForm = {
      isNew: false,
      enableSubmit: true,
      discountType: 'percent',
      showBill: true,
      isTemplateDelete: true,
      isTemplateSave: true,
      billDate: new Date(),
      maxDate: new Date(),
      itemTypes: [{id: '0', type: 'Raw'}, {id: '1', type: 'Semi-Processed'}, {id: '2', type: 'Processed'}],
    };
     $scope.indentGrnOrderForm.tempBillNo = "IN -" + $scope.billNo;
    $scope.clearStockEntryTab = function () {
      $scope.indentGrnOrderForm = {
        isNew: false,
        enableSubmit: true,
        showBill: true,
        discountType: 'percent',
        isTemplateDelete: true,
        isTemplateSave: true,
        billDate: new Date(),
        maxDate: new Date()
      };
      $scope.indentGrnOrderForm.tempBillNo = "IN -" + $scope.billNo;
    };
    function addKitchenDetails(items) {
      _.forEach(items, function (it, k) {
        _.forEach($scope.stores, function (s, o) {
          if (s.isKitchen) {
            _.forEach(s.processedFoodCategory, function (ps, u) {
              if (ps._id == it.fromCategory._id) {
                it.kitchenDetails = s;
                //console.log("KITCHEN DETAILS ASSSIGNED", it)
                return
              }
            });
          }
        });
      });
    };

    function checkRecipeAvailable(items) {
      _.forEach(items, function (it, j) {
        _.forEach($scope.stockRecipes, function (rec, p) {
          if (rec.itemId == it._id) {
            it.recipeAvailable = true;
          }
        });
      });
    };

    function GetAllRequirements(req) {
      _.forEach(req, function (r, i) {
        if (r.supplyDate == null && r.ReceiveDate == null) {
          $scope.requirementChallan.push(r);
        }
        else if (r.supplyDate != null && r.ReceiveDate == null) {
          $scope.requirementReceive.push(r);
          //console.log("recieved requireennts", $scope.requirementReceive)
        }
      });
      GetSuppliedRequirements(req);
      getItemsByCategory();
      _.forEach(req, function (r, o) {
        addKitchenDetails(r.items);
        checkRecipeAvailable(r.items)
      })
    };

    function GetSuppliedRequirements(req) {
      var requirement = req;
      $scope.unsuppliedRequirements = [];
      $scope.suppliedRequirements = [];
      _.forEach(requirement, function (r, i) {
        if (r.supplyDate == null) {
          $scope.unsuppliedRequirements.push(r);
        }
        else {
          $scope.suppliedRequirements.push(r);
        }
      });
    };

    $scope.hasKitchenDetailAndrecipe = function () {
      var checkIfOk = false;
      _.forEach($scope.indentGrnDeliveryChallanForm.consolidatedRequirements, function (item, i) {
        //console.log("iTEMS", item);
        if (item.recipeAvailable && item.kitchenDetails) {
          checkIfOk = true;
        }
      })
      //console.log(checkIfOk)
      return checkIfOk
    };

    $scope.showSelectedSuppliedIndentDetails = function (s) {
      stockRequirement.getStoreNItems({id: s._id}, function (data) {
        $scope.suppliedRequirementView = true;
        s.items = data.items;
        $scope.suppliedIndentDetails = s;
        //console.log("details", s)
      });
      /*$scope.suppliedRequirementView = true;
       $scope.suppliedIndentDetails = s;
       //console.log("details", s)*/
    };

    $scope.resetStockEntryTab = function () {
      if (!$scope.indentGrnOrderForm.isEdit) {
        $scope.indentGrnOrderForm = {
          isNew: false,
          enableSubmit: true,
          showBill: true,
          discountType: 'percent',
          isTemplateDelete: true,
          isTemplateSave: true,
          billDate: new Date(),
          maxDate: new Date()
        };
        $scope.indentGrnOrderForm.tempBillNo = "IN -" + $scope.billNo;
      }
      else {
        $scope.indentGrnOrderForm.isSaved = false;
        $scope.indentGrnOrderForm.isEdit = false;
        $scope.indentGrnOrderForm.isPrint = false;
      }
    };

    $scope.billDateChange = function (d) {
      alert(d);
    };

    $scope.bindItemType_entryForm = function () {
      $scope.indentGrnOrderForm.availableCategory = [];
      $scope.indentGrnOrderForm.availableItems = [];
      $scope.indentGrnOrderForm.remainingItems = [];
      //console.log("COPIED ITEMS", angular.copy($scope.copiedItems));
      _.forEach($scope.copiedItems, function (c, i) {
        //console.log("C", c)
        //console.log(c.isSemiProcessed)
        //console.log($scope.indentGrnOrderForm.selecteditemType.id == 2)
        //console.log($scope.indentGrnOrderForm.selecteditemType);
        //console.log("SELECTED TRUE OR NOT ", $scope.indentGrnOrderForm.selecteditemType.id != 2)
        //console.log(typeof c.isSemiProcessed === 'undefined')
        if (c.isSemiProcessed && $scope.indentGrnOrderForm.selecteditemType.id == 1) {
          c.type = "semiProcessed"
          $scope.indentGrnOrderForm.availableCategory.push(c)
        }
        else if (c.isSemiProcessed == false && $scope.indentGrnOrderForm.selecteditemType.id == 2) {
          c.type = "processed"
          $scope.indentGrnOrderForm.availableCategory.push(c)
        }
        else if ($scope.indentGrnOrderForm.selecteditemType.id == 0 && c.isSemiProcessed == undefined) {
          c.type = "raw"
          $scope.indentGrnOrderForm.availableCategory.push(c)
        }
      });
      };

    $scope.tempBill_entryForm = function (billDate) {
      // $scope.indentGrnOrderForm.showBill=true;
      // $scope.indentGrnOrderForm.showOpenBill=false;
      // $scope.indentGrnOrderForm.tempBillNo="temp/"+$filter('date')(billDate, 'dd-MMM-yyyy');
    };

    var guid = (function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      }

      return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      };
    })();

    $scope.bindVendor_entryForm = function (store) {
      getItemsByCategory();
      $scope.indentGrnOrderForm.availableCategory = [];
      $scope.indentGrnOrderForm.allAvailableCategory = [];
      $scope.indentGrnOrderForm.availableItems = [];
      $scope.indentGrnOrderForm.availableVendor = [];
      $scope.indentGrnOrderForm.vendor = {};
      if (store != null) {

        _.forEach(angular.copy($scope.vendors), function (v, i) {
          if (v.pricing != undefined) {
            if (v.pricing._id == store._id) {
              if (v.type == "HO Vendor") {
                $scope.indentGrnOrderForm.availableVendor.push(v);
                console.log("PUSHED IN VENDORS")
              }
            }
          }
        });
      }
    };

    $scope.hideConsolidatedList = function () {
      $scope.indentGrnDeliveryChallanForm.showConsolidated = false;
      $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements = {};
      $scope.selectedForConsoloidated = false;
    };

    function filteringItems(vendor) {
      if (!$scope.selectedVendor) {
        vendor = $scope.selectedVendor;
        var itemsforcompare = [];
        _.forEach(vendor.pricing.category, function (c, i) {
          _.forEach(c.item, function (it, iii) {
            itemsforcompare.push(it);
          });
        });
        _.forEach($scope.indentGrnOrderForm.availableCategory, function (ac, i) {
          _.forEachRight(ac.item, function (itm, ii) {
            var varFromCategory = {"_id": ac._id, "categoryName": ac.categoryName, "type": ac.type};
            itm["fromCategory"] = varFromCategory;
            if (itm.units.length == 1 && itm.units[0].unitName == 'pack') {
              $scope.indentGrnOrderForm.unitsMissing = true;
            }
            else {
              var ind = _.findIndex(itemsforcompare, {"itemName": itm.itemName});
              if (ind < 0) {
                $scope.indentGrnOrderForm.remainingItems.push(itm);
              }
              else {
                ac.item.splice(ii, 1);
                ac.item.push(itemsforcompare[ind]);
                $scope.indentGrnOrderForm.remainingItems.push(itemsforcompare[ind]);
              }
            }
          });
        });
      }
    };
    $scope.bindCategory_entryForm = function (vendor, stores) {
      $scope.indentGrnOrderForm.availableItems = [];
      $scope.indentGrnOrderForm.remainingItems = [];
      $scope.indentGrnOrderForm.availableCategory = [];
      $scope.indentGrnOrderForm.selectedCategory = {};
      $scope.indentGrnOrderForm.unitsMissing = false;
      $scope.indentGrnOrderForm.availableTemplate = [];
      $scope.indentGrnOrderForm.selectedTemplate = {};

      var store = angular.copy(stores);
      if (vendor.pricing != undefined) {
        _.forEach($scope.template_Entry, function (t, i) {
          if (store._id == t._store._id) {
            if (vendor._id == t._vendor._id) {
              $scope.indentGrnOrderForm.availableTemplate.push(angular.copy(t));
            }
          }
        });

        if (vendor.pricing._id == store._id) {
          //_.forEach(store.category, function(c,i){
          _.forEach(vendor.pricing.category, function (cat, ii) {
            //if(cat._id==c._id){
            $scope.indentGrnOrderForm.availableCategory.push(angular.copy(cat));
            //}
          });
          //});

          var itemsforcompare = [];
          _.forEach(vendor.pricing.category, function (c, i) {
            _.forEach(c.item, function (it, iii) {
              var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
              it["fromCategory"] = varFromCategory;
              itemsforcompare.push(angular.copy(it));
            });
          });

          _.forEach($scope.indentGrnOrderForm.availableCategory, function (ac, i) {
            _.forEachRight(ac.item, function (itm, ii) {
              var ind = _.findIndex(itemsforcompare, {"itemName": itm.itemName});
              var varFromCategory = {"_id": ac._id, "categoryName": ac.categoryName};
              itm["fromCategory"] = varFromCategory;
              if (ind < 0) {
                $scope.indentGrnOrderForm.remainingItems.push(angular.copy(itm));
              }
              else {
                ac.item.splice(ii, 1);
                itemsforcompare[ind].units[0].baseUnit = itm.units[0].baseUnit;
                ac.item.push(itemsforcompare[ind]);
                $scope.indentGrnOrderForm.remainingItems.push(angular.copy(itemsforcompare[ind]));
              }
            });
          });
        }
      }
    };

    $scope.bindCategory_entryForm_Old = function (vendor, stores) {
      $scope.indentGrnOrderForm.availableItems = [];
      $scope.indentGrnOrderForm.remainingItems = [];
      $scope.indentGrnOrderForm.availableCategory = [];
      $scope.indentGrnOrderForm.selectedCategory = {};
      $scope.indentGrnOrderForm.unitsMissing = false;
      $scope.indentGrnOrderForm.availableTemplate = [];
      $scope.indentGrnOrderForm.selectedTemplate = {};
      $scope.selectedVendor = vendor;
      $scope.indentGrnOrderForm.selectedVendor = vendor;
      var store = angular.copy(stores);
      if (vendor.pricing != undefined) {
        _.forEach($scope.template_Entry, function (t, i) {
          if (store._id == t.store._id) {
            $scope.indentGrnOrderForm.availableTemplate.push(t);
          }
        });
         if (vendor.pricing._id == store._id) {
          //_.forEach($scope.allItemsByCategories, function(c,i){
          //   _.forEach(store.category, function(c, i){
          //     _.forEach(vendor.pricing.category, function(vc,ii){
          //         console.log("all cats cat", c)
          //         console.log("vendor", vc)
          //         if(c._id==vc._id){
          //           console.log("VENDOR PRICING CAT", vc)
          //           console.log("CATEGORY", c)
          //           //c.item = angular.copy(vc.item);
          //            // $scope.indentGrnOrderForm.availableCategory.push(c);
          //           $scope.indentGrnOrderForm.availableCategory.push(vc);
          //          console.log("avaulble cats", angular.copy($scope.indentGrnOrderForm.availableCategory));
          //         }
          //     });
          // });
          _.forEach(vendor.pricing.category, function (vc, ii) {
            $scope.indentGrnOrderForm.availableCategory.push(vc);
          });
          _.forEach($scope.indentGrnOrderForm.availableCategory, function (ac, i) {
            _.forEachRight(ac.item, function (itm, ii) {
              var ind = _.findIndex(itemsforcompare, {"itemName": itm.itemName});
              var varFromCategory = {"_id": ac._id, "categoryName": ac.categoryName};
              itm["fromCategory"] = varFromCategory;
              if (ind < 0) {
                $scope.stockEntryForm.remainingItems.push(angular.copy(itm));
              }
              else {
                ac.item.splice(ii, 1);
                itemsforcompare[ind].units[0].baseUnit = itm.units[0].baseUnit;
                ac.item.push(itemsforcompare[ind]);
                $scope.stockEntryForm.remainingItems.push(angular.copy(itemsforcompare[ind]));
              }
            });
          });
          // var listOfCats =  getItems_ProcessedFoodByDeployment(vendor.baseKitchenId);
          // console.log("LIST", listOfCats)
          // var dataByCategory;
          // listOfCats.$promise.then(function(data){
          //   console.log("DATA", data)
          //   // _.forEach(data, function(c, i){
          //   //      console.log("c", c)
          //   //      $scope.indentGrnOrderForm.availableCategory.push(c)
          //   // })
          //   dataByCategory = getItemsOfAnotherDeploymentByCategory(data);
          //   console.log("DATA BY CATEGOYR", dataByCategory)
          //   _.forEach(dataByCategory, function(c, i){
          //     console.log("c", c)
          //     $scope.indentGrnOrderForm.availableCategory.push(c)
          //     console.log("PUSED", angular.copy($scope.indentGrnOrderForm.availableCategory))
          //   });
          //   $scope.copiedItems=angular.copy($scope.indentGrnOrderForm.availableCategory)
          //   filteringItems(vendor);
          // });
          // console.log("checking cats", angular.copy($scope.indentGrnOrderForm.availableCategory))
        }
      }
    };

    function slectedUnit_entryForm(item) {
      $scope.indentGrnOrderForm.selectedUnits = {};
      _.forEach(item, function (itm, i) {
        //console.log('-----------item--------------', itm);
        _.forEach(itm.units, function (u, ii) {
          if (itm.selectedUnitId != undefined) {
            if (itm.selectedUnitId._id == u._id) {
              $scope.indentGrnOrderForm.selectedUnits[itm._id] = u;
              itm.newConFactor = parseFloat(u.conversionFactor);
            }
          }
          else if (itm.selectedUnit != undefined) {
            if (itm.selectedUnit._id == u._id) {
              $scope.indentGrnOrderForm.selectedUnits[itm._id] = u;
              itm.newConFactor = parseFloat(u.conversionFactor);
            }
          }
          else if (itm.preferedUnit != undefined) {
            if (itm.preferedUnit == u._id) {
              $scope.indentGrnOrderForm.selectedUnits[itm._id] = u;
              itm.selectedUnit = u;
            }
          }
          else {
            $scope.indentGrnOrderForm.selectedUnits[itm._id] = itm.units[0];
            itm.selectedUnit = itm.units[0];
          }
        });
      });
      $scope.enableSubmitButton_IndentGRNOrder($scope.indentGrnOrderForm.availableItems);
    };

    $scope.bindItems_entryFormTemplate = function (templateId) {
      if ($scope.indentGrnOrderForm.availableItems == undefined) {
        $scope.indentGrnOrderForm.availableItems = [];
      }

      _.forEach($scope.indentGrnOrderForm.availableTemplate, function (t, i) {
        if (t._id == templateId) {
          _.forEach(t.items, function (itm, ii) {
            if (itm.units.length == 1 && itm.units[0].unitName == 'pack') {
              $scope.indentGrnOrderForm.unitsMissing = true;
            } else {
              var av = _.findIndex($scope.indentGrnOrderForm.availableItems, {"itemName": itm.itemName});
              if (av < 0) {
                $scope.indentGrnOrderForm.availableItems.push(itm);
                var index = _.findIndex($scope.indentGrnOrderForm.remainingItems, {"itemName": itm.itemName});
                $scope.indentGrnOrderForm.remainingItems.splice(index, 1);
                var cS = itm.fromCategory._id;
                $scope.indentGrnOrderForm.selectedCategory[cS] = true;
              }
              else {
                $scope.indentGrnOrderForm.availableItems.splice(av, 1);
                $scope.indentGrnOrderForm.availableItems.push(itm);
              }
            }
          });
        }
      });
      slectedUnit_entryForm($scope.indentGrnOrderForm.availableItems);
    };

    $scope.bindItems_entryForm = function (catId) {
      if ($scope.indentGrnOrderForm.availableItems == undefined) {
        $scope.indentGrnOrderForm.availableItems = [];
      }
      var isItemExpired = false;
      _.forEach($scope.indentGrnOrderForm.availableCategory, function (c, i) {
        if ($scope.indentGrnOrderForm.selectedCategory[catId] == true) {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var av = _.findIndex($scope.indentGrnOrderForm.availableItems, {_id: itm._id});
              if (av < 0) {
                var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
                itm["fromCategory"] = varFromCategory;
                //check date range
                if (itm.toDate != undefined || itm.fromDate != undefined) {
                  var toD = new Date(itm.toDate);
                  var fD = new Date(itm.fromDate);
                  var cD = new Date();
                  if (cD < fD || cD > toD) {
                    itm.hide = true;
                    isItemExpired = true;
                  }
                }
                //itm.availableQtyInBase=120.00;
                var avQtyIndex = _.findIndex($scope.closingQty, {
                  itemId: itm._id,
                  storeId: $scope.indentGrnOrderForm.store._id
                });
                if (avQtyIndex >= 0) {
                  itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                  itm.availableQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                }
                else {
                  itm.availableQtyInBase = parseFloat(0).toFixed(3);
                }
                $scope.indentGrnOrderForm.availableItems.push(angular.copy(itm));
                var index = _.findIndex($scope.indentGrnOrderForm.remainingItems, {_id: itm._id});
                $scope.indentGrnOrderForm.remainingItems.splice(index, 1);
              }
            });
          }
        }
        else {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var index = _.findIndex($scope.indentGrnOrderForm.availableItems, {_id: itm._id});
              if (index >= 0) {
                $scope.indentGrnOrderForm.availableItems.splice(index, 1)
                if (itm.toDate != undefined || itm.fromDate != undefined) {
                  var toD = new Date(itm.toDate);
                  var fD = new Date(itm.fromDate);
                  var cD = new Date();
                  if (cD < fD || cD > toD) {
                    itm.hide = true;
                    isItemExpired = true;
                  }
                }
                $scope.indentGrnOrderForm.remainingItems.push(angular.copy(itm));
              }
            });
          }
        }
      });
      slectedUnit_entryForm($scope.indentGrnOrderForm.availableItems);
      if (isItemExpired == true) {
        alert("Disabled items contract has been expired");
      }
    };
    
    $scope.bindItems_entryForm_Old = function (catId) {
      if ($scope.indentGrnOrderForm.availableItems == undefined) {
        $scope.indentGrnOrderForm.availableItems = [];
      }
      _.forEach($scope.indentGrnOrderForm.availableCategory, function (c, i) {
        if ($scope.indentGrnOrderForm.selectedCategory[catId] == true) {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              if (itm.units.length == 1 && itm.units[0].unitName == 'pack') {
                $scope.indentGrnOrderForm.unitsMissing = true;
              }
              else {
                var av = _.findIndex($scope.indentGrnOrderForm.availableItems, {"itemName": itm.itemName});
                if (av < 0) {
                  var varFromCategory = {"_id": c._id, "categoryName": c.categoryName, "type": c.type};
                  itm["fromCategory"] = varFromCategory;

                  //itm.availableQtyInBase=120.00;
                  var avQtyIndex = _.findIndex($scope.closingQty, {
                    itemId: itm._id,
                    storeId: $scope.indentGrnOrderForm.store._id
                  });
                  if (avQtyIndex >= 0) {
                    itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    itm.availableQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                  }
                  else {
                    itm.availableQtyInBase = parseFloat(0).toFixed(3);
                  }
                  $scope.indentGrnOrderForm.availableItems.push(itm);
                  var index = _.findIndex($scope.indentGrnOrderForm.remainingItems, {"itemName": itm.itemName});
                  $scope.indentGrnOrderForm.remainingItems.splice(index, 1);
                }
              }
            });
          }
        }
        else {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              if (itm.units.length > 0) {
                var index = _.findIndex($scope.indentGrnOrderForm.availableItems, {_id: itm._id});
                $scope.indentGrnOrderForm.availableItems.splice(index, 1);
                $scope.indentGrnOrderForm.remainingItems.push(itm);
              }
            });
          }
        }
      });
      slectedUnit_entryForm($scope.indentGrnOrderForm.availableItems);
      };

    $scope.goNext = function (nextIdx) {
      var f = angular.element(document.querySelector('#f_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
      else {
        $scope.indentGrnOrderForm.enableSubmit = false;
        angular.element(document.querySelector('#submitEntry')).focus();
      }
    };

    $scope.calculateAmt_entryForm = function (qty, price, item, vat) {
      var index = _.findIndex($scope.indentGrnOrderForm.availableItems, {_id: item._id});
      if (!isNaN(qty)) {
        if (!isNaN(price)) {
          var amt = parseFloat(qty) * parseFloat(price);
          item.subTotal = amt;
          if (item.vatPercent == "" || item.vatPercent == undefined || isNaN(item.vatPercent)) {
            item.totalAmount = amt;
            item.addedAmt = 0;
            $scope.indentGrnOrderForm.availableItems[index].vatPercent = "";
          }
          else {
            var vatamt = (amt * .01 * parseFloat(vat));
            item.addedAmt = vatamt;
            var t = amt + parseFloat(vatamt);
            item.totalAmount = t;
          }
          ;
          var ss = basePrefferedAnsSelectedUnitsByItem(item, $scope.indentGrnOrderForm.selectedUnits, 1);
          item.calculateInUnits = ss;
          $scope.enableSubmitButton_IndentGRNOrder($scope.indentGrnOrderForm.availableItems);
        }
        else {

          $scope.indentGrnOrderForm.availableItems[index].price = "";
        }

      }
      else {
        $scope.indentGrnOrderForm.availableItems[index].qty = "";
      }
    };

    function basePrefferedAnsSelectedUnitsByItem(item, selectedUnits, type) {
      var conversionFactor = 1;
      var selectedConversionFactor = 1;
      _.forEach(selectedUnits, function (u, ii) {
        if (ii == item._id) {
          selectedConversionFactor = u.conversionFactor;
          if (type == 1) {
            var selectedUnit1 = {
              "_id": u._id,
              "unitName": u.unitName,
              "basePrice": parseFloat(item.price),
              "baseQty": parseFloat(item.qty),
              "subTotal": parseFloat(item.subTotal),
              "addedAmt": parseFloat(item.addedAmt),
              "totalAmount": parseFloat(item.totalAmount),
              "type": "selectedUnit",
              "conversionFactor": parseFloat(u.conversionFactor)
            };
            selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
            selectedUnit1.addedAmt = 0;
            if (item.vatPercent != "" && item.vatPercent != undefined) {
              selectedUnit1.addedAmt = parseFloat(parseFloat(selectedUnit1.subTotal) * .01 * parseFloat(item.vatPercent));
            }
            selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
            item["selectedUnit"] = selectedUnit1;

            if (item.preferedUnit == undefined) {
              item["preferedUnits"] = angular.copy(selectedUnit1);
              item.preferedUnits.type = "preferedUnit";
            }
          }
          else if (type == 2) {
            var selectedUnit1 = {
              "_id": u._id,
              "unitName": u.unitName,
              "basePrice": parseFloat(item.price),
              "baseQty": parseFloat(item.qty),
              "subTotal": parseFloat(item.subTotal),
              "addedAmt": parseFloat(item.addedAmt),
              "totalAmount": parseFloat(item.totalAmount),
              "type": "selectedUnit",
              "conversionFactor": parseFloat(u.conversionFactor)
            };
            selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
            selectedUnit1.addedAmt = 0;
            selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
            item["selectedUnit"] = selectedUnit1;

            if (item.preferedUnit == undefined) {
              item["preferedUnits"] = angular.copy(selectedUnit1);
              item.preferedUnits.type = "preferedUnit";
            }
          }
        }
      });

      var baseUnit = {};
      if (item.units[0].baseUnit != undefined) {
        baseUnit = angular.copy(item.units[0].baseUnit);
        baseUnit.unitName = baseUnit.name;
      }
      else {
        if (item.baseUnit != undefined) {
          baseUnit = angular.copy(item.baseUnit);
          baseUnit.unitName = baseUnit.name;
        }
        else {
          baseUnit = angular.copy(item.units[0]);
        }
      }

      baseUnit.type = "baseUnit";
      baseUnit.conversionFactor = conversionFactor;

      baseUnit.baseQty = parseFloat(item.qty * selectedConversionFactor);
      baseUnit.basePrice = parseFloat(item.price / selectedConversionFactor);

      var baseAmt = parseFloat(item.qty) * parseFloat(item.price);
      if (item.vatPercent == "" || item.vatPercent == undefined) {
        baseUnit.totalAmount = baseAmt;
        baseUnit.subTotal = baseAmt;
        baseUnit.addedAmt = 0;
      }
      else {
        var vatamt = (baseAmt * .01 * parseFloat(item.vatPercent));
        baseUnit.addedAmt = vatamt;
        baseUnit.subTotal = baseAmt;
        baseUnit.totalAmount = baseAmt + parseFloat(vatamt);
      }

      item.baseUnit = baseUnit;

      var pInd = _.findIndex(item.units, {_id: item.preferedUnit});

      if (pInd >= 0) {
        var preferedUnits = angular.copy(item.units[pInd]);
        var preferedconversionFactor = item.units[pInd].conversionFactor;
        preferedUnits.addedAmt = 0;

        if (preferedconversionFactor == selectedConversionFactor) {
          preferedUnits = angular.copy(item.selectedUnit);
        }
        else if (preferedconversionFactor > selectedConversionFactor) {
          preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
          if (type == 1) {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
          }
          else {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
            baseUnit.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
            item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) / parseFloat(preferedconversionFactor);
          }
          preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
          if (item.vatPercent != "" && item.vatPercent != undefined) {
            preferedUnits.addedAmt = parseFloat(parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
          }

          preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
        }
        else if (preferedconversionFactor < selectedConversionFactor) {
          preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
          if (type == 1) {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
          }
          else {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
            baseUnit.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
            item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) * parseFloat(preferedconversionFactor);
          }
          preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
          if (item.vatPercent != "" && item.vatPercent != undefined) {
            preferedUnits.addedAmt = (parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
          }
          preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
        }

        if (type == 2) {
          baseUnit.subTotal = parseFloat(baseUnit.basePrice) * parseFloat(baseUnit.baseQty);
          baseUnit.addedAmt = 0;
          baseUnit.totalAmount = parseFloat(baseUnit.subTotal);

          item.selectedUnit.subTotal = parseFloat(item.selectedUnit.basePrice) * parseFloat(item.selectedUnit.baseQty);
          item.selectedUnit.addedAmt = 0;
          item.selectedUnit.totalAmount = parseFloat(item.selectedUnit.subTotal);
        }
        preferedUnits.type = "preferedUnit";
        delete preferedUnits.baseUnit;
        item.preferedUnits = preferedUnits;
      }
      if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
        var cf = 1000;
        if (selectedConversionFactor == cf) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
        }
        else if (selectedConversionFactor > cf) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) / parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) * parseFloat(cf);
        }
        else if (cf > selectedConversionFactor) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
        }
        ;
      }
      ;

      item.calculateInUnits = [];
      item.calculateInUnits.push(item.baseUnit);
      item.calculateInUnits.push(item.preferedUnits);
      item.calculateInUnits.push(item.selectedUnit);

      return item.calculateInUnits;
    };

    function basePrefferedAnsSelectedUnitsByItem_Old(item, selectedUnits, type) {
      var conversionFactor = 1;
      var selectedConversionFactor = 1;
      _.forEach(selectedUnits, function (u, ii) {
        if (u != null || u != undefined) {
          if (ii == item._id) {
            selectedConversionFactor = u.conversionFactor;
            if (type == 1) {
              var selectedUnit1 = {
                "_id": u._id,
                "unitName": u.unitName,
                "basePrice": parseFloat(item.price),
                "baseQty": parseFloat(item.qty),
                "subTotal": parseFloat(item.subTotal),
                "addedAmt": parseFloat(item.addedAmt),
                "totalAmount": parseFloat(item.totalAmount),
                "type": "selectedUnit",
                "conversionFactor": parseFloat(u.conversionFactor)
              };
              selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
              selectedUnit1.addedAmt = 0;
              if (item.vatPercent != "" && item.vatPercent != undefined) {
                selectedUnit1.addedAmt = parseFloat(parseFloat(selectedUnit1.subTotal) * .01 * parseFloat(item.vatPercent));
              }
              selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
              item["selectedUnit"] = selectedUnit1;
              if (item.preferedUnit == undefined) {
                item["preferedUnits"] = angular.copy(selectedUnit1);
                item.preferedUnits.type = "preferedUnit";
              }
            }
            else if (type == 2) {
              var selectedUnit1 = {
                "_id": u._id,
                "unitName": u.unitName,
                "basePrice": parseFloat(item.price),
                "baseQty": parseFloat(item.qty),
                "subTotal": parseFloat(item.subTotal),
                "addedAmt": parseFloat(item.addedAmt),
                "totalAmount": parseFloat(item.totalAmount),
                "type": "selectedUnit",
                "conversionFactor": parseFloat(u.conversionFactor)
              };
              selectedUnit1.subTotal = parseFloat(item.price) * parseFloat(item.qty);
              selectedUnit1.addedAmt = 0;
              selectedUnit1.totalAmount = parseFloat(selectedUnit1.subTotal) + parseFloat(selectedUnit1.addedAmt);
              item["selectedUnit"] = selectedUnit1;
              if (item.preferedUnit == undefined) {
                item["preferedUnits"] = angular.copy(selectedUnit1);
                item.preferedUnits.type = "preferedUnit";
              }
            }
          }
        }
      });

      var baseUnit = {};
      if (item.units[0].baseUnit != undefined) {
        baseUnit = angular.copy(item.units[0].baseUnit);
        baseUnit.unitName = baseUnit.name;
      }
      else {
        if (item.baseUnit != undefined) {
          baseUnit = angular.copy(item.baseUnit);
          baseUnit.unitName = baseUnit.name;
        }
        else {
          baseUnit = angular.copy(item.units[0]);
        }
      }
      baseUnit.type = "baseUnit";
      baseUnit.conversionFactor = conversionFactor;
      baseUnit.baseQty = parseFloat(item.qty * selectedConversionFactor);
      baseUnit.basePrice = parseFloat(item.price / selectedConversionFactor);
      var baseAmt = parseFloat(item.qty) * parseFloat(item.price);
      if (item.vatPercent == "" || item.vatPercent == undefined) {
        baseUnit.totalAmount = baseAmt;
        baseUnit.subTotal = baseAmt;
        baseUnit.addedAmt = 0;
      }
      else {
        var vatamt = (baseAmt * .01 * parseFloat(item.vatPercent));
        baseUnit.addedAmt = vatamt;
        baseUnit.subTotal = baseAmt;
        baseUnit.totalAmount = baseAmt + parseFloat(vatamt);
      }
      item.baseUnit = baseUnit;
      var pInd = _.findIndex(item.units, {_id: item.preferedUnit});
      if (pInd >= 0) {
        var preferedUnits = angular.copy(item.units[pInd]);
        var preferedconversionFactor = item.units[pInd].conversionFactor;
        preferedUnits.addedAmt = 0;
        if (preferedconversionFactor == selectedConversionFactor) {
          preferedUnits = angular.copy(item.selectedUnit);
        }
        else if (preferedconversionFactor > selectedConversionFactor) {
          preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
          if (type == 1) {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
          }
          else {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
            baseUnit.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
            item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) / parseFloat(preferedconversionFactor);
          }
          preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
          if (item.vatPercent != "" && item.vatPercent != undefined) {
            preferedUnits.addedAmt = parseFloat(parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
          }
          preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
        }
        else if (preferedconversionFactor < selectedConversionFactor) {
          preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
          if (type == 1) {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice) / parseFloat(preferedconversionFactor);
          }
          else {
            preferedUnits.basePrice = parseFloat(baseUnit.basePrice);
            baseUnit.basePrice = parseFloat(baseUnit.basePrice) * parseFloat(preferedconversionFactor);
            item.selectedUnit.basePrice = parseFloat(item.selectedUnit.basePrice) * parseFloat(preferedconversionFactor);
          }
          preferedUnits.subTotal = parseFloat(preferedUnits.baseQty) * parseFloat(preferedUnits.basePrice);
          if (item.vatPercent != "" && item.vatPercent != undefined) {
            preferedUnits.addedAmt = (parseFloat(preferedUnits.subTotal) * .01 * parseFloat(item.vatPercent));
          }
          preferedUnits.totalAmount = parseFloat(preferedUnits.subTotal) + parseFloat(preferedUnits.addedAmt);
        }
        if (type == 2) {
          baseUnit.subTotal = parseFloat(baseUnit.basePrice) * parseFloat(baseUnit.baseQty);
          baseUnit.addedAmt = 0;
          baseUnit.totalAmount = parseFloat(baseUnit.subTotal);
          item.selectedUnit.subTotal = parseFloat(item.selectedUnit.basePrice) * parseFloat(item.selectedUnit.baseQty);
          item.selectedUnit.addedAmt = 0;
          item.selectedUnit.totalAmount = parseFloat(item.selectedUnit.subTotal);
        }
        preferedUnits.type = "preferedUnit";
        delete preferedUnits.baseUnit;
        item.preferedUnits = preferedUnits;
      }
      if (item.baseUnit.id == 2 || item.baseUnit.id == 3) {
        var cf = 1000;
        if (selectedConversionFactor == cf) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
        }
        else if (selectedConversionFactor > cf) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) / parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) * parseFloat(cf);
        }
        else if (cf > selectedConversionFactor) {
          item.baseUnit.basePrice = parseFloat(item.baseUnit.basePrice) * parseFloat(cf);
          item.baseUnit.baseQty = parseFloat(item.baseUnit.baseQty) / parseFloat(cf);
        }
        ;
      }
      ;
      item.calculateInUnits = [];
      item.calculateInUnits.push(item.baseUnit);
      item.calculateInUnits.push(item.preferedUnits);
      item.calculateInUnits.push(item.selectedUnit);
      return item.calculateInUnits;
    };

    function formatStore_indentGrnOrderForm(store) {
      var stores = angular.copy(store);
      for (var p in stores) {
        if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID" && p!='isKitchen' && p!="isMain") {
          delete stores[p];
        }
      }
      return stores;
    };

    function getFromCategoryOfItem_indentGrnOrderForm() {
      _.forEach($scope.indentGrnOrderForm.selectedCategory, function (sc, i) {
        if (sc == true) {
          _.forEach($scope.indentGrnOrderForm.availableCategory, function (ac, ii) {
            if (ac._id == i) {
              _.forEach(ac.item, function (itm, iii) {
                var varFromCategory = {"_id": ac._id, "categoryName": ac.categoryName, "type": ac.type};
                itm["fromCategory"] = varFromCategory;
              });
            }
          });
        }
      });
    };

    function getItemToSave_indentGrnOrderForm() {
      var isItemSkipped = false;
      var items = angular.copy($scope.indentGrnOrderForm.availableItems);
      _.forEachRight(items, function (itm, i) {
        var flag = false;
        _.forEach($scope.indentGrnOrderForm.selectedUnits, function (u, ii) {
          if (ii == itm._id && itm.qty != "" && itm.qty != undefined && itm.price != "" && itm.price != undefined && u != null) {
            var selectedUnit = {"_id": u._id, "unitName": u.unitName, "conversionFactor": u.conversionFactor};
            itm["selectedUnit"] = selectedUnit;
            flag = true;
          }
        });
        if (flag == false) {
          isItemSkipped = true;
          items.splice(i, 1);
        }
      });
      _.forEach(items, function (itm, i) {
        for (var p in itm) {
          if (p != "_id" && p != "itemName" && p != "qty" && p != "calculateInUnits" && p != "price" && p != "fromCategory" && p != "subTotal" && p != "comment" && p != "vatPercent" && p != "mfgDate" && p != "totalAmount" && p != "addedAmt" && p != "preferedUnits" && p != "units" && p != "fromCategory" && p != "selectedUnit") {
            _.forEach(itm.units, function (u, ii) {
              for (var p in u) {
                if (p != "_id" && p != "unitName" && p != "conversionFactor" && p != "baseUnit") {
                  delete u[p];
                }
              }
            });
            delete itm[p];
          }
        }
        itm.supplyQty = itm.qty;
      });
      if (isItemSkipped)
        if (confirm("Few items skipped because of insufficient data. Are you sure you want to generate Indent?"))
          return items;
        else return [];
      else return items;
    };

    $scope.addRemainingItem_indentGrnOrderForm = function ($item, $model, $label) {
      var remaining = angular.copy($scope.indentGrnOrderForm.remainingItems);
      _.forEach($scope.indentGrnOrderForm.remainingItems, function (itm, i) {
        if (itm._id == $item._id) {
          if (itm.units.length == 1 && itm.units[0].unitName == 'pack') {
            $scope.indentGrnOrderForm.unitsMissing = true;
          }
          else {
            var avQtyIndex = _.findIndex($scope.closingQty, {
              itemId: itm._id,
              storeId: $scope.indentGrnOrderForm.store._id
            });
            if (avQtyIndex >= 0) {
              itm.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
              itm.availableQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
            }
            else {
              itm.availableQtyInBase = parseFloat(0).toFixed(3);
            }
            $scope.indentGrnOrderForm.availableItems.unshift(itm);
            remaining.splice(i, 1);
            $scope.indentGrnOrderForm.itemToAdd = "";
          }
        }
      });
      $scope.indentGrnOrderForm.remainingItems = remaining;
      slectedUnit_entryForm($scope.indentGrnOrderForm.availableItems);
    };

    $scope.deleteSelectedItemEntryForm = function (item) {
      var index = _.findIndex($scope.indentGrnOrderForm.availableItems, {"itemName": item.itemName});
      $scope.indentGrnOrderForm.availableItems.splice(index, 1);
      $scope.indentGrnOrderForm.remainingItems.push(item);
    };

    function formatVendor(itm) {
      for (var p in itm) {
        //if(p!="_id" && p!="deployment_id" && p!="tenant_id" && p!="vendorName" && p!="type" && p != "baseKitchenId" ){
        if (p == "pricing" && p == "active" && p == "created" && p == "updated") {
          delete itm[p];
        }
      }
      return itm;
    };

    function formatStoreForINdentGRN(item, vendor, store) {
      var stores = store;
      delete vendor.pricing;
      stores.vendor = vendor;
      stores.vendor.category = [];
      _.forEach(item, function (itm, i) {
        var index = _.findIndex(stores.vendor.category, {_id: itm.fromCategory._id});
        if (index < 0) {
          stores.vendor.category.push(itm.fromCategory);
        }
      });

      _.forEach(stores.vendor.category, function (c, i) {
        c.items = [];
        _.forEachRight(item, function (itm, ii) {
          if (c._id == itm.fromCategory._id) {
            delete itm.fromCategory;
            c.items.push(itm);
            item.splice(ii, 1);
          }
        });
      });
      console.log(stores);
      return stores;
    };

    function formatPreferredUnit(itemPreferredUnit) {
      for (var key in itemPreferredUnit) {
        if (key !== 'conversionFactor' || 'unitName' || '_id') {
        }
      }
      return itemPreferredUnit
    };

    function formatAndSaveBaseKitchenUnits(unit) {
      for (var p in unit) {
        var object;
        if (unit[p] != null || undefined) {
          object = {
            unitName: unit[p].unitName,
            unitId: unit[p]._id,
            baseUnit: unit[p].baseUnit,
            conversionFactor: unit[p].conversionFactor,
            baseConversionFactor: unit[p].baseConversionFactor,
            tenant_id: unit[p].tenant_id,
            deployment_id: currentUser.deployment_id,
            toDeployment_id: $scope.indentGrnOrderForm.vendor.baseKitchenId

          }
          baseKitchenUnit.saveData({}, object, function (result) {
            console.log("RESULT AFTER SAVING UNIT", result);
          })
        }
      }
    };

    function formatAndSaveBaseKitchenItems(arrayOfItems) {
      _.forEach(arrayOfItems, function (item, i) {
        var itemObject = {
          itemName: item.itemName,
          itemId: item._id,
          units: item.units,
          tenant_id: currentUser.tenant_id,
          category: item.fromCategory,
          preferedUnit: {
            _id: item.selectedUnit._id,
            conversionFactor: item.selectedUnit.conversionFactor,
            unitName: item.selectedUnit.unitName
          },
          deployment_id: currentUser.deployment_id,
          toDeployment_id: $scope.indentGrnOrderForm.vendor.baseKitchenId
        }
        baseKitchenItem.saveData({}, itemObject, function (result) {
          console.log("RESULT AFTER SAVING", result);
        });
      });
    };

    function formatAndSaveBaseKitchenItemsBulk(arrayOfItems) {
      var bKItems = [];
      _.forEach(arrayOfItems, function (item, i) {
        var itemObject = {
          itemName: item.itemName,
          itemId: item._id,
          units: item.units,
          tenant_id: currentUser.tenant_id,
          category: item.fromCategory,
          preferedUnit: {
            _id: item.selectedUnit._id,
            conversionFactor: item.selectedUnit.conversionFactor,
            unitName: item.selectedUnit.unitName
          },
          deployment_id: currentUser.deployment_id,
          toDeployment_id: $scope.indentGrnOrderForm.vendor.baseKitchenId
        }
        bKItems.push(itemObject);
      });
      baseKitchenItem.saveAll({bKItems: bKItems}, function (result) {

      });
    };

    /*  $scope.submitStockEntry = function(formStockEntry){
     $scope.indentGrnOrderForm.enableSubmit=true;
     var itemToSave=getItemToSave_indentGrnOrderForm($scope.indentGrnOrderForm.availableItems);
     if (itemToSave.length > 0) {
     if(formStockEntry.$valid && !$scope.indentGrnOrderForm.isPrint){
     if($scope.indentGrnOrderForm.transactionId!=undefined){
     }
     else{
     $scope.indentGrnOrderForm.transactionId=guid();
     }
     var storeToSave=formatStore_indentGrnOrderForm($scope.indentGrnOrderForm.store);
     var ven=formatVendor($scope.indentGrnOrderForm.vendor);
     //var str=formatStoreForINdentGRN(angular.copy(itemToSave),angular.copy($scope.indentGrnOrderForm.vendor),angular.copy(storeToSave));
     var str=formatStoreForINdentGRN(angular.copy(itemToSave),$scope.indentGrnOrderForm.vendor,storeToSave);
     $scope.indentGrnOrderForm.store=str;
     $scope.indentGrnOrderForm.items=itemToSave;
     $scope.indentGrnOrderForm.vendor=ven;
     $scope.indentGrnOrderForm.category = $scope.indentGrnOrderForm.availableCategory;
     var _emailScope = $rootScope.$new();
     var table1="";
     $scope.indentGrnOrderForm.requirementType=1;
     $scope.indentGrnOrderForm.users=1;
     $scope.indentGrnOrderForm.sumOfItems=0;
     var gt=0;
     var dpe=_.findIndex($scope.deployments,{"_id":currentUser.deployment_id});
     if(dpe>=0){
     $scope.indentGrnOrderForm.deploymentName=$scope.deployments[dpe].name;
     }
     $scope.indentGrnOrderForm.created=$scope.indentGrnOrderForm.billDate;
     $scope.indentGrnOrderForm._id=$scope.indentGrnOrderForm.transactionId;
     $scope.indentGrnOrderForm.deployment_id= currentUser.deployment_id;
     $scope.indentGrnOrderForm.toDeployment_id = $scope.indentGrnOrderForm.vendor.baseKitchenId;
     $scope.indentGrnOrderForm.fromDeployment_id = currentUser.deployment_id;
     $scope.indentGrnOrderForm.tenant_id= currentUser.tenant_id;
     $scope.indentGrnOrderForm.user=currentUser;
     $scope.settings.address==undefined?'':$scope.settings.address;
     $scope.indentGrnOrderForm.deploymentAddress = $scope.settings.address;
     stockRequirement.save({},$scope.indentGrnOrderForm, function(s){
     formatAndSaveBaseKitchenItems(itemToSave)
     formatAndSaveBaseKitchenUnits($scope.indentGrnOrderForm.selectedUnits);
     table1+="<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'>";
     table1+="<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
     table1+="<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
     table1+="<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Good Receipt Note <br /><br /></td></tr>";
     table1+="<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Requested Quantity</th></thead>";
     table1+="<tbody>";
     console.log("S", s)
     var gt=0;
     _.forEach(s.items, function(item,i){
     console.log("ITEM IN LOOP", item)
     table1+="<tr>";
     table1+="<td>"+item.itemName+"</td>";
     table1+="<td>"+item.selectedUnit.unitName+"</td>";
     table1+="<td>"+item.qty+"</td>"
     table1+="</tr>";
     console.log("quan" + item.qty)
     console.log("price", item.price)
     gt+= (item.qty * item.price);
     });
     $scope.indentGrnOrderForm.sumOfItems =gt;
     console.log("GT", gt)
     console.log("SUM OF ITEMS",$scope.indentGrnOrderForm.sumOfItems )
     var table2;
     console.log("TAB;E", table1)
     var _message = $compile($('<div>' + table1+ '</div>'))(_emailScope);
     var _to ='ajittechit@gmail.com'
     console.log("compiled message" , _message)
     Email.send({}, {
     messageText: _message.text(),
     messageHtml: _message.html(),
     to: _to.toString()
     },function (data) {
     console.log("DATA" ,  data)
     $timeout(function() {
     table2 = document.getElementById('indentOrderRequest').innerHTML;
     print_Entry(table2)
     $scope.clearStockEntryTab();
     formStockEntry.$setPristine();
     _emailScope.$destroy();
     }, 0);
     },function (err) {
     console.log("ERROR", err);
     });
     $scope.billNo=parseFloat(s.requirementNumber)+1;
     $scope.requirementChallan.push(s);
     });
     }
     else
     {
     //Print
     $scope.indentGrnOrderForm.enableSubmit=false;
     }
     }
     };*/

    $scope.submitStockEntryMultipart = function (formStockEntry) {
      $scope.indentGrnOrderForm.enableSubmit = true;
      var itemToSave = getItemToSave_indentGrnOrderForm($scope.indentGrnOrderForm.availableItems);
      if (itemToSave.length > 0) {
        if (formStockEntry.$valid && !$scope.indentGrnOrderForm.isPrint) {
          if ($scope.indentGrnOrderForm.transactionId != undefined) {
          }
          else {
            $scope.indentGrnOrderForm.transactionId = guid();
          }
          var storeToSave = formatStore_indentGrnOrderForm($scope.indentGrnOrderForm.store);
          var ven = formatVendor($scope.indentGrnOrderForm.vendor);
          //var str=formatStoreForINdentGRN(angular.copy(itemToSave),angular.copy($scope.indentGrnOrderForm.vendor),angular.copy(storeToSave));
          var str = formatStoreForINdentGRN(angular.copy(itemToSave), $scope.indentGrnOrderForm.vendor, storeToSave);
          $scope.indentGrnOrderForm.store = str;
          $scope.indentGrnOrderForm.items = itemToSave;
          $scope.indentGrnOrderForm.vendor = ven;
          $scope.indentGrnOrderForm.category = $scope.indentGrnOrderForm.availableCategory;
          var _emailScope = $rootScope.$new();
          var table1 = "";
          $scope.indentGrnOrderForm.requirementType = 1;
          $scope.indentGrnOrderForm.users = 1;
          $scope.indentGrnOrderForm.sumOfItems = 0;
          var gt = 0;
          var dpe = _.findIndex($scope.deployments, {"_id": currentUser.deployment_id});
          if (dpe >= 0) {
            $scope.indentGrnOrderForm.deploymentName = $scope.deployments[dpe].name;
          }
          $scope.indentGrnOrderForm.created = $scope.indentGrnOrderForm.billDate;
          $scope.indentGrnOrderForm._id = $scope.indentGrnOrderForm.transactionId;
          $scope.indentGrnOrderForm.deployment_id = currentUser.deployment_id;
          $scope.indentGrnOrderForm.toDeployment_id = $scope.indentGrnOrderForm.vendor.baseKitchenId;
          $scope.indentGrnOrderForm.fromDeployment_id = currentUser.deployment_id;
          $scope.indentGrnOrderForm.tenant_id = currentUser.tenant_id;
          $scope.indentGrnOrderForm.user = currentUser;
          $scope.settings.address == undefined ? '' : $scope.settings.address;
          $scope.indentGrnOrderForm.deploymentAddress = $scope.settings.address;
          var ch = angular.copy($scope.indentGrnOrderForm);
          var payload = new FormData();
          var bt = new Blob([JSON.stringify(ch)], {type: 'text/plain'});
          payload.append('bt2', bt);

          $http({
            url: '/api/stockRequirements/' + ch._id,
            method: 'post',
            data: payload,
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
          }).then(function success(result) {

            var s = result.data;
            //formatAndSaveBaseKitchenItems(itemToSave);
            formatAndSaveBaseKitchenUnits($scope.indentGrnOrderForm.selectedUnits);
            table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'>";
            table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
            table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
            table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Good Receipt Note <br /><br /></td></tr>";
            table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Requested Quantity</th></thead>";
            table1 += "<tbody>";
            var gt = 0;
            _.forEach(s.items, function (item, i) {
              table1 += "<tr>";
              table1 += "<td>" + item.itemName + "</td>";
              table1 += "<td>" + item.selectedUnit.unitName + "</td>";
              table1 += "<td>" + item.qty + "</td>"
              table1 += "</tr>";
              gt += (item.qty * item.price);
            });
            $scope.indentGrnOrderForm.sumOfItems = gt;
            //console.log("GT", gt)
            //console.log("SUM OF ITEMS", $scope.indentGrnOrderForm.sumOfItems)
            var table2;
            //console.log("TAB;E", table1)
            var _message = $compile($('<div>' + table1 + '</div>'))(_emailScope);
            var _to = 'ajittechit@gmail.com'
            //console.log("compiled message", _message)
            Email.send({}, {
              messageText: _message.text(),
              messageHtml: _message.html(),
              to: _to.toString()
            }, function (data) {
              formStockEntry.$setPristine();
              growl.success('Indent Generated Successfully.',{ttl:3000})
              $timeout(function () {
                table2 = document.getElementById('indentOrderRequest').innerHTML;
               
                print_Entry(table2)
                $scope.clearStockEntryTab();
                _emailScope.$destroy();
              }, 10);
            }, function (err) {
            });
            $scope.billNo = parseFloat(s.requirementNumber) + 1;
            $scope.requirementChallan.push(s);
          });
        }
        else {
          //Print
          $scope.indentGrnOrderForm.enableSubmit = false;
        }
      }
    };

    function convertPriceInSelectedUnit_GRN(item, unit, type) {
      //var pInd=_.findIndex(item.units,{_id:item.selectedUnit? item.selectedUnit._id : item.unit._id});
      var pInd = _.findIndex(item.units, {_id: unit[item._id]._id});
      var preferedUnits = angular.copy(item.units[pInd]);
      var preferedconversionFactor = item.units[pInd].conversionFactor;
      var conversionFactor = 1;
      var selectedConversionFactor = 1;
      _.forEach(unit, function (u, ii) {
        if (ii == item._id) {
          selectedConversionFactor = u.conversionFactor;
        }
      });
      if (item.price == undefined) {
        item.price = 0;
      }
      ;
      if (preferedconversionFactor == selectedConversionFactor) {
        if (preferedconversionFactor > 1) {
          item.newprice = parseFloat(item.price) * parseFloat(preferedconversionFactor);
        }
        else if (preferedconversionFactor < 1) {
          item.newprice = parseFloat(item.price) / parseFloat(preferedconversionFactor);
        }
        else {
          item.newprice = parseFloat(item.price);
        }
      }
      else if (preferedconversionFactor > selectedConversionFactor) {
        item.newprice = parseFloat(item.price) / parseFloat(selectedConversionFactor);
      }
      else if (preferedconversionFactor < selectedConversionFactor) {
        item.newprice = parseFloat(item.price) * parseFloat(selectedConversionFactor);
      }
      if (item.newConFactor == undefined) {
        item.newConFactor = preferedconversionFactor;
      }
      if (item.newConFactor > selectedConversionFactor) {
        item.price = parseFloat(item.newprice) / parseFloat(item.newConFactor);
      }
      else if (item.newConFactor < selectedConversionFactor) {
        item.price = parseFloat(item.newprice) * parseFloat(item.newConFactor);
      }
      item.newConFactor = selectedConversionFactor;
      $scope.calculateAmt_entryForm(item.qty, item.price, item, item.vatPercent);
    };

    $scope.enableSubmitButton_IndentGRNOrder = function (items, selectedItem, unit) {
      if (selectedItem != undefined && unit[selectedItem._id] != undefined || null) {
        convertPriceInSelectedUnit_GRN(selectedItem, unit, 1);
      }
      var flag = false;
      var atleastOneOk = false;
      _.forEach(items, function (item, i) {
         //item.price = item.rate;
        if (flag == false) {
          if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && item.price != "" && item.price != undefined && !isNaN(item.price) && $scope.indentGrnOrderForm.selectedUnits[item._id] != undefined && $scope.indentGrnOrderForm.selectedUnits[item._id] != null) {
            flag = true;
            atleastOneOk = true;
          }
          _.forEach(unit, function (u, ii) {
            if (ii == item._id) {
              item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
            }
          });
        }

        var unit = $scope.indentGrnOrderForm.selectedUnits;
        _.forEach(unit, function (u, ii) {
          if (ii == item._id) {
            if (u != null) {
              item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
              if (item.availableQtyInBase != 0) {
                item.availableQtyInBase = parseFloat(parseFloat(item.closingQtyInBase) / parseFloat(u.conversionFactor)).toFixed(3);
              }
            }
          }
        });
      });
      if (flag == true) {
        $scope.indentGrnOrderForm.enableSubmit = false;
      }
      else {
        $scope.indentGrnOrderForm.enableSubmit = true;
        growl.error('Please fill the full row before entry', {ttl: 3000});
      }
    };

    $scope.clearChallan = function () {
      $scope.indentGrnDeliveryChallanForm = {};
      $scope.indentGrnDeliveryChallanForm.isNew = true;
    };

    $scope.indentGrnDeliveryChallanForm = {};
    var openIndentStore={}
    $scope.indentGrnDeliveryChallanForm.isNew = true;

    $scope.openIndentGrnDeliveryChallanForm = function (r) {
      var isKitchen,openingNotFound;
      stockRequirement.getStoreNItems({id: r._id}, function (result) {
        //_.forEach($scope.req.indents, function (req) {
        //  $scope.req.indents.slice(req);
        //})
       // $scope.outletDeployment_id=result.deployment_id;
        //$scope.storeId=result.store._id
        $scope.storeName=result.store.storeName
        openIndentStore=angular.copy(result.store)
        console.log(result);
        var tempArray = [];
        r.items = result.items;
        tempArray = r.items;
        $scope.EnableSubmitChallan(tempArray)

       // closingOfOutletItems=getClosingQty()
        //console.log(closingOfOutletItems);


        // For Outlet Closing
        openingNotFound=result.store.isKitchen;
        console.log(result.store.isKitchen);
        if(result.store.isKitchen!=undefined)
        {
          isKitchen=result.store.isKitchen
        }
        else
        {
          isKitchen=false;
        }
        var startDate = new Date();
        startDate.setDate(startDate.getDate());
        startDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(startDate))));
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate = new Date(Utils.getDateFormatted(Utils.getResetDate(currentDeployment.settings, Utils.convertDate(endDate))));
        //console.log(currentDeployment);
        var req = {
          fromDate: startDate,
          toDate: endDate,
          deployment_id: result.deployment_id,
          tenant_id: localStorageService.get('tenant_id'),
          storeId:result.store._id,
          isKitchen:isKitchen
        };
      
            StockReportNew.getClosingQtyOutlet(req).$promise.then(function (result){
            //var data=self.splicePhysicalGreaterThanStartDate(resultProcessed);
            if(result.errorMessage==undefined) {
              var data1 = spliceAllItemsBeforePhysical(result,startDate);
              //console.log(data1);
              var res = storeWiseItems(data1);
              // console.log('000000',res);

              getStorewiseUniqueItems(res).then(function (items) {
                //   console.log(items)
                //  res.items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                var uItems = items;
                var ret = getClosingQtyStoreWise(uItems, res,startDate);
                //   console.log(ret);

                _.forEach(r.items, function (item, i) {
                  //var avQtyIndex=_.findIndex($scope.closingQty,{itemId:item._id,storeId:$scope.stockTransferForm.fromStore._id});
                  var avQtyIndex = _.findIndex(ret, {itemName: item.itemName});
                  if (avQtyIndex >= 0) {
                    item.closingQtyInOutlet = ret[avQtyIndex].closingQtyInBase;
                    // console.log
                    item.availableQtyInOutlet = parseFloat(parseFloat(ret[avQtyIndex].closingQtyInBase) / parseFloat(item.selectedUnit.conversionFactor)).toFixed(3);
                  }
                  else {
                    item.availableQtyInOutlet = parseFloat(0).toFixed(3);
                  }
                });


                _.forEach(r.items, function (item, i) {
                  //var avQtyIndex=_.findIndex($scope.closingQty,{itemId:item._id,storeId:$scope.stockTransferForm.fromStore._id});
                  var avQtyIndex = _.findIndex($scope.closingQty, {itemName: item.itemName});
                  if (avQtyIndex >= 0) {
                    item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                    // console.log
                    item.availableQtyInBase = parseFloat(parseFloat($scope.closingQty[avQtyIndex].closingQtyInBase) / parseFloat(item.selectedUnit.conversionFactor)).toFixed(3);
                  }
                  else {
                    item.availableQtyInBase = parseFloat(0).toFixed(3);
                  }
                });
                $scope.indentGrnDeliveryChallanForm = r;

                $scope.indentGrnDeliveryChallanForm.isNew = false;
                $scope.indentGrnDeliveryChallanForm.open = true;


              });// End of getStoreWise
            }
            else
            {
              //console.log('else');
              _.forEach(r.items, function (item, i) {
                //var avQtyIndex=_.findIndex($scope.closingQty,{itemId:item._id,storeId:$scope.stockTransferForm.fromStore._id});
                var avQtyIndex = _.findIndex($scope.closingQty, {itemName: item.itemName});
                if (avQtyIndex >= 0) {
                  item.closingQtyInOutlet = "NA"
                  // console.log
                  item.availableQtyInOutlet = "NA"
                }
                else {
                  item.availableQtyInOutlet = 'NA'
                }
              });


              _.forEach(r.items, function (item, i) {
                //var avQtyIndex=_.findIndex($scope.closingQty,{itemId:item._id,storeId:$scope.stockTransferForm.fromStore._id});
                var avQtyIndex = _.findIndex($scope.closingQty, {itemName: item.itemName});
                if (avQtyIndex >= 0) {
                  item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
                  // console.log
                  item.availableQtyInBase = parseFloat(parseFloat($scope.closingQty[avQtyIndex].closingQtyInBase) / parseFloat(item.selectedUnit.conversionFactor)).toFixed(3);
                }
                else {
                  item.availableQtyInBase = parseFloat(0).toFixed(3);
                }
              });
              $scope.indentGrnDeliveryChallanForm = r;

              $scope.indentGrnDeliveryChallanForm.isNew = false;
              $scope.indentGrnDeliveryChallanForm.open = true;
              if(openingNotFound!=undefined)
                growl.error('Opening Entry of Outlet Not Found', {ttl: 3000});

            }  
           }); // END of StockReport New
          
  
          


       
      //  console.log("open", r)
      });
      /*var tempArray = [];
       tempArray = r.items;
       console.log("OPEN DELIVERY CHALLAN", r)
       $scope.EnableSubmitChallan(tempArray)
       _.forEach(r.items,function(item,i){
       //var avQtyIndex=_.findIndex($scope.closingQty,{itemId:item._id,storeId:$scope.stockTransferForm.fromStore._id});
       var avQtyIndex=_.findIndex($scope.closingQty,{itemName:item.itemName});
       if(avQtyIndex>=0){
       item.closingQtyInBase=$scope.closingQty[avQtyIndex].closingQtyInBase;
       item.availableQtyInBase=parseFloat(parseFloat($scope.closingQty[avQtyIndex].closingQtyInBase)/parseFloat(item.selectedUnit.conversionFactor)).toFixed(3);
       }
       else
       {
       item.availableQtyInBase=parseFloat(0).toFixed(3);
       }
       });
       $scope.indentGrnDeliveryChallanForm=r;
       $scope.indentGrnDeliveryChallanForm.isNew=false;
       $scope.indentGrnDeliveryChallanForm.open = true;
       console.log("open", r)*/
    };

  // function storeWiseItems(result){
                
  //               _.forEach(result,function(r,i){
  //                   r.items=[];
  //                   r.items2=[];
  //                   /*r.beforeDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
  //                   r.betweenDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
  //                   */
  //                   _.forEach(r.beforeDate,function(b,ii){
  //                       //if(!b.totalBalanceQty)
  //                       r.items.push(b);
  //                       //else
  //                       //r.items2.push(b);
  //                   });
  //                   _.forEach(r.betweenDate,function(b,ii){
  //                       //if(!b.totalBalanceQty)
  //                       r.items.push(b);
  //                       //else
  //                       //r.items2.push(b);
  //                   });
              
  //                   r.beforeDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
  //                   r.betweenDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
  //                   if(r.items!=undefined){

  //                       r.items.sort(function(a,b){
  //                           if(new Date(a.created).getTime() != new Date(b.created).getTime())
  //                               return new Date(b.created) - new Date(a.created);
  //                           else return b.num - a.num;
  //                       });
                      
  //                   }
  //                   /*if(r.items2!=undefined){

  //                       r.items2.sort(function(a,b){return new Date(b.actualDate) - new Date(a.actualDate);});
  //                       console.log('------------After Sorting in storeWiseItems when r.items2!=undefined--------------------------');
  //                       console.log(r.items2);
  //                       _.merge(r.items,r.items2);
  //                   }*/
  //               });
  //               return result;
  //           }



    function storeWiseItems(result){
      var obj={};
      obj.items=[];
      obj.beforeDate=result.beforeDate;
      obj.betweenDate=result.betweenDate;
      obj.isKitchen=result.isKitchen;;
      obj.storeId=result.storeId;
      obj.storeName=$scope.storeName;

        _.forEach(obj.beforeDate,function(b,ii){
          obj.items.push(b);
        });
        _.forEach(obj.betweenDate,function(b,ii){
          obj.items.push(b);
        });
      // console.log(r);
        // obj.beforeDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
        // obj.betweenDate.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
        if(obj.items!=undefined){
          //obj.items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});

           obj.items.sort(function(a,b){
                            if(new Date(a.created).getTime() != new Date(b.created).getTime())
                                return new Date(b.created) - new Date(a.created);
                            else return b.num - a.num;
                        });
        }

      return obj;
    }

    // function spliceAllItemsBeforePhysical (result){

    //   var data=[];
    //   _.forEach(result,function(r,i){
    //     _.forEach(r.beforeDate,function(rb,ii){
    //       if(rb.totalBalanceQty!=undefined){
    //         var d=new Date(rb.created);
    //         d.setDate(d.getDate() + 1);
    //         //d.setHours(0, 0, 0, 0);
    //         rb.created=d;
    //         data.push(rb);
    //       };
    //     });

    //   });
    //   return result;
    // }

   function spliceAllItemsBeforePhysical(rs,startDate){
                
                var data=[];
                var latestPhysical = null;
                var latestPhysicalBetween = null;
                var i = 0;
                var j = 0;

                
                    rs.beforeDate.sort(function (a, b) {
                    return new Date(a.created) - new Date(b.created);
                    });
                    rs.betweenDate.sort(function (a, b) {
                    return new Date(a.created) - new Date(b.created);
                    });
                
                //     _.forEach(r.beforeDate,function(rb,ii){
                //         if(rb.totalBalanceQty!=undefined){
                //             var d=new Date(rb.created);
                //             d.setDate(d.getDate() + 1);
                //             d.setHours(0, 0, 0, 0);
                //             rb.created=d;
                //             data.push(rb);
                //         };
                //     });
                //     // _.forEach(data,function(d,iii){
                //     //     _.forEachRight(r.beforeDate,function(itm,ii){
                //     //         if(itm.itemId==d.itemId){
                //     //             var phyDate=new Date(d.created);
                //     //             var compareDate=new Date(itm.created);
                //     //             if(phyDate.getTime()>compareDate.getTime()){
                //     //               result[i].beforeDate.splice(ii,1);
                //     //             }
                //     //         }
                //     //     });
                //     // });
                _.forEach(rs.beforeDate, function(r,i){
                    if(r.totalBalanceQty!=undefined){
                      // if(r.itemName == "Manchow Chicken Soup(bk)"){
                      //   console.log('beforeDate in SAIBP',r.itemName, new Date(r.created), r.totalBalanceQty);
                      // }
                      var d = new Date(r.created);
                      r.actualDate=new Date(d);
                      var a = new Date(d);
                      a.setHours(0,0,0,0);
                      a = new Date(a);
                      var b = new Date(d);
                      b = new Date(b.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0));
                      if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

                      }else {
                        //console.log('splice all', d);
                        d.setDate(d.getDate() + 1);
                      }
                      d = new Date(d.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0));
                      //d.setHours(0, 0, 0, 0);
                      r.created = new Date(d);
                      //r.created = new Date(d);
              if(!latestPhysical){
                i = 0;
                latestPhysical = new Date(d);
              } else {
                if(latestPhysical.getTime() == d.getTime()){
                  i++;
                } else if(d.getTime() > latestPhysical.getTime()) {
                  i = 0;
                  latestPhysical = new Date(d);
                }
              }
              r.actualDate=new Date(d);
              r.num = i;
              data.push(r);
                      //data.push(r);
                    };
                  });
                  _.forEach(data,function(d,i){
                    _.forEachRight(rs.beforeDate,function(itm,ii){
                      if(itm.itemId==d.itemId){
                        var phyDate=new Date(d.created);
                        var compareDate=new Date(itm.created);
                        if(phyDate.getTime()>compareDate.getTime()){
                          rs.beforeDate.splice(ii,1);
                        }
                      }
                    });
                  });

                  // var data2=[];
                  _.forEachRight(rs.betweenDate,function(r,i){
                    if(r.totalBalanceQty!=undefined){
                      //console.log(r.created);
                      // if(r.itemName == "Manchow Chicken Soup(bk)"){
                      //   console.log('betweenDate in SAIBP',r.itemName, new Date(r.created), r.totalBalanceQty);
                      // }
                      r.actualDate = r.created;
                      var d=new Date(r.created);
                      //var cdate=new Date($scope.purchaseConsumptionForm.toDate);
                      var a = new Date(d);
                      a.setHours(0,0,0,0);
                      a = new Date(a);
                      var b = new Date(d);
                      b = new Date(b.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0));
                      console.log(a)
                      console.log(b)
                      console.log(angular.copy(d))
                      if((new Date(d).getTime() >= new Date(a).getTime() && new Date(d).getTime() < new Date(b).getTime())){

                      }else {
                        //console.log('splice all', d);
                        d.setDate(d.getDate() + 1);
                      }
                      //d.setDate(d.getDate() + 1);
                      d = new Date(d.setHours(startDate.getHours(), startDate.getMinutes(), 0, 0));
                      r.created = new Date(d);
                      r.actualDate = new Date(d);
                      if(!latestPhysicalBetween){
                j = 0;
                latestPhysicalBetween = new Date(d);
              } else {
                if(latestPhysicalBetween.getTime() == d.getTime()){
                  j++;
                } else if(d.getTime() > latestPhysicalBetween.getTime()) {
                  j = 0;
                  latestPhysicalBetween = new Date(d);
                }
              }
              //r.actualDate=new Date(d);
              r.num = j;
                      //console.log(r.created);
                      // if(cdate.getTime()>d.getTime()){
                      //   data2.push(r);
                      // }
                      // else
                      // {
                      //   result.betweenDate.splice(i,1);
                      // }
                    };
                  });
               

                return rs;
            }



    function getStorewiseUniqueItems(s){

      var deferred=$q.defer();
      var data=[];
      //baseKitchenItem.get({tenant_id:self.tenant_id,deployment_id:self.deployment_id}).$promise.then(function (items){

        _.forEach(s.items,function(itm,ii){


           // console.log('inside find');
            var obj={storeId:s.storeId,storeName:s.storeName,itemId:itm.itemId,itemName:itm.itemName,unitName:itm.UnitName};
            data.push(obj);

        });



      //data.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
      deferred.resolve(data);
      //});
      return deferred.promise;
      //return data;
    }


  // function getClosingQtyStoreWise (items,s){
                
  //               //items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
  //               _.forEach(items,function(it,i){
                   
  //                       _.forEachRight(s.items,function(itm,iii){
  //                           /*if(itm.itemName == "Rose Syrup (O)"){
  //                                           console.log('Rose Syrup (O)', itm);
  //                                       }*/
  //                           if(s.storeId==it.storeId && itm.itemId==it.itemId){
                                
  //                               if(itm.totalOpeningQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(itm.totalOpeningQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalOpeningQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalBalanceQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
  //                                       var cDate=new Date(itm.created);
  //                                       cDate = new Date(cDate.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
  //                                       if(itm.itemName == "Manchow Chicken Soup(bk)"){
  //                                           console.log('totalBalanceQty in Manchow Chicken Soup(bk)', itm.itemName, cDate, itm.totalBalanceQty);
  //                                       }
  //                                       cDate.setDate(cDate.getDate() - 1);
  //                                       var td=new Date();
  //                                       td = new Date(td.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
  //                                       if(td.getTime()==cDate.getTime()){
  //                                           console.log('---------------------Inside Match --------------------------------------')
  //                                       }
  //                                       else
  //                                       {
  //                                           if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                               it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
  //                                           }
  //                                           else
  //                                           {
  //                                               it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
  //                                           }
  //                                       }

  //                                   }
  //                                   else
  //                                   {
  //                                       var cDate=new Date(itm.created);
  //                                       cDate = new Date(cDate.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
  //                                       if(itm.itemName == "Manchow Chicken Soup(bk)"){
  //                                           console.log('totalBalanceQty in Manchow Chicken Soup(bk)', itm.itemName, cDate, itm.totalBalanceQty);
  //                                       }
  //                                       cDate.setDate(cDate.getDate() - 1);
  //                                       var td=new Date();
  //                                       td = new Date(td.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
  //                                       if(td.getTime()==cDate.getTime()){ 

  //                                       }
  //                                       else
  //                                       {
  //                                           if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                               it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
  //                                           }
  //                                           else
  //                                           {
  //                                               it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
  //                                           }
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalPurchaseQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
  //                                       }

  //                                   }
  //                               }
  //                               else if(itm.totalReturnAcceptQty!=undefined){
  //                                 if(it.closingQtyInBase==undefined){
  //                                   //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
  //                                   if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                     it.closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
  //                                   }
  //                                   else
  //                                   {
  //                                     it.closingQtyInBase=parseFloat(itm.totalReturnAcceptQty).toFixed(3);
  //                                   }
  //                                 }
  //                                 else
  //                                 {
  //                                   if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                     var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
  //                                     it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
  //                                   }
  //                                   else
  //                                   {
  //                                     it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
  //                                   }

  //                                 }
  //                               }
  //                               else if(itm.totalSaleQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(-itm.totalSaleQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalSaleQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalSaleQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalWastageQty!=undefined){
  //                                   if(itm.itemName == "Manchow Chicken Soup(bk)"){
  //                                           console.log('totalWastageQty in Manchow Chicken Soup(bk)', itm.itemName, itm.totalWastageQty);
  //                                       }
  //                                   if(it.closingQtyInBase==undefined){
                                        
  //                                       //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(-itm.totalWastageQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalWastageQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalWastageQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalReturnWastageQty!=undefined){
  //                                 if(it.closingQtyInBase==undefined){
  //                                   //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
  //                                   if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                     it.closingQtyInBase=parseFloat(parseFloat(-itm.totalReturnWastageQty)*1000).toFixed(3);
  //                                   }
  //                                   else
  //                                   {
  //                                     it.closingQtyInBase=parseFloat(-itm.totalReturnWastageQty).toFixed(3);
  //                                   }
  //                                 }
  //                                 else
  //                                 {
  //                                   if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                     var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnWastageQty)*1000).toFixed(3);
  //                                     it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
  //                                   }
  //                                   else
  //                                   {
  //                                     it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalReturnWastageQty)).toFixed(3);
  //                                   }
  //                                 }
  //                               }
  //                               else if(itm.totalTransferQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(-itm.totalTransferQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalTransferQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalTransferQty_toStore!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.itemSaleQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(-itm.itemSaleQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.itemSaleQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.itemSaleQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalStockReceiveQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(itm.totalStockReceiveQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalIntermediateQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(-itm.totalIntermediateQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(-itm.totalIntermediateQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalIntermediateQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                               else if(itm.totalStockReturnQty!=undefined){
  //                                   if(it.closingQtyInBase==undefined){
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           it.closingQtyInBase=parseFloat(parseFloat(-itm.totalStockReturnQty)*1000).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(-itm.totalStockReturnQty).toFixed(3);
  //                                       }
  //                                   }
  //                                   else
  //                                   {
  //                                       if(it.unitName=="Litre" || it.unitName=="Meter"){
  //                                           var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReturnQty)*1000).toFixed(3);
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
  //                                       }
  //                                       else
  //                                       {
  //                                           it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
  //                                       }
  //                                   }
  //                               }
  //                           }
  //                       });
                   
             
  //               return items;
  //           }




    // function getClosingQtyStoreWise (items,s,startDate){
    //   console.log(items);

    //   //items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
    //   _.forEach(items,function(it,i){

    //       _.forEachRight(s.items,function(itm,iii){
    //         if(s.storeId==it.storeId && itm.itemId==it.itemId){
    //           if(itm.totalOpeningQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(itm.totalOpeningQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalOpeningQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalBalanceQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
    //               var cDate=new Date(itm.created);
    //               cDate.setDate(cDate.getDate()-1);
    //               var td=new Date();
    //               td.setHours(0,0,0,0)
    //               if(td.getTime()==cDate.getTime()){

    //               }
    //               else
    //               {
    //                 if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                   it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
    //                 }
    //                 else
    //                 {
    //                   it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
    //                 }
    //               }

    //             }
    //             else
    //             {
    //               var cDate=new Date(itm.created);
    //               cDate.setDate(cDate.getDate()-1);
    //               var td=new Date();
    //               td.setHours(0,0,0,0)
    //               if(td.getTime()==cDate.getTime()){

    //               }
    //               else
    //               {
    //                 if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                   it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
    //                 }
    //                 else
    //                 {
    //                   it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
    //                 }
    //               }
    //             }
    //           }
    //           else if(itm.totalPurchaseQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
    //               }

    //             }
    //           }
    //           else if(itm.totalReturnAcceptQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(itm.totalReturnAcceptQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
    //               }

    //             }
    //           }
    //           else if(itm.totalSaleQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(-itm.totalSaleQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalSaleQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalSaleQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalWastageQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(-itm.totalWastageQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalWastageQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalWastageQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalReturnWastageQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(-itm.totalReturnWastageQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(-itm.totalReturnWastageQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnWastageQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalReturnWastageQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalTransferQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(-itm.totalTransferQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalTransferQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalTransferQty_toStore!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.itemSaleQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(-itm.itemSaleQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.itemSaleQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.itemSaleQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalStockReceiveQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(itm.totalStockReceiveQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalIntermediateQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(-itm.totalIntermediateQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(-itm.totalIntermediateQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalIntermediateQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
    //               }
    //             }
    //           }
    //           else if(itm.totalStockReturnQty!=undefined){
    //             if(it.closingQtyInBase==undefined){
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 it.closingQtyInBase=parseFloat(parseFloat(-itm.totalStockReturnQty)*1000).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(-itm.totalStockReturnQty).toFixed(3);
    //               }
    //             }
    //             else
    //             {
    //               if(it.unitName=="Litre" || it.unitName=="Meter"){
    //                 var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReturnQty)*1000).toFixed(3);
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
    //               }
    //               else
    //               {
    //                 it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
    //               }
    //             }
    //           }
    //         }
    //       });

    //   });


    //   return items;
    // }

     function getClosingQtyStoreWise (items,s,startDate){
               
                //items.sort(function(a,b){return new Date(b.created) - new Date(a.created);});
                _.forEach(items,function(it,i){
                  
                        _.forEachRight(s.items,function(itm,iii){
                            /*if(itm.itemName == "Rose Syrup (O)"){
                                            console.log('Rose Syrup (O)', itm);
                                        }*/
                            if(s.storeId==it.storeId && itm.itemId==it.itemId){
                                
                                if(itm.totalOpeningQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalOpeningQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalOpeningQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalOpeningQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalBalanceQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                        var cDate=new Date(itm.created);
                                        cDate = new Date(cDate.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
                                        /*if(itm.itemName == "Manchow Chicken Soup(bk)"){
                                            console.log('totalBalanceQty in Manchow Chicken Soup(bk)', itm.itemName, cDate, itm.totalBalanceQty);
                                        }*/
                                        cDate.setDate(cDate.getDate() - 1);
                                        var td=new Date();
                                        td = new Date(td.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
                                        if(td.getTime()==cDate.getTime()){
                                            //console.log('---------------------Inside Match --------------------------------------')
                                        }
                                        else
                                        {
                                            if(it.unitName=="Litre" || it.unitName=="Meter"){
                                                it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
                                            }
                                            else
                                            {
                                                it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        var cDate=new Date(itm.created);
                                        cDate = new Date(cDate.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
                                        /*if(itm.itemName == "Manchow Chicken Soup(bk)"){
                                            console.log('totalBalanceQty in Manchow Chicken Soup(bk)', itm.itemName, cDate, itm.totalBalanceQty);
                                        }*/
                                        cDate.setDate(cDate.getDate() - 1);
                                        var td=new Date();
                                        td = new Date(td.setHours(startDate.getHours(),startDate.getMinutes(),0,0));
                                        if(td.getTime()==cDate.getTime()){ 

                                        }
                                        else
                                        {
                                            if(it.unitName=="Litre" || it.unitName=="Meter"){
                                                it.closingQtyInBase=parseFloat(parseFloat(itm.totalBalanceQty)*1000).toFixed(3);
                                            }
                                            else
                                            {
                                                it.closingQtyInBase=parseFloat(itm.totalBalanceQty).toFixed(3);
                                            }
                                        }
                                    }
                                }
                                else if(itm.totalPurchaseQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalPurchaseQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalPurchaseQty)).toFixed(3);
                                        }

                                    }
                                }
                                else if(itm.totalReturnAcceptQty!=undefined){
                                  if(it.closingQtyInBase==undefined){
                                    //it.closingQtyInBase=parseFloat(itm.totalPurchaseQty).toFixed(3);
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      it.closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(itm.totalReturnAcceptQty).toFixed(3);
                                    }
                                  }
                                  else
                                  {
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnAcceptQty)*1000).toFixed(3);
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalReturnAcceptQty)).toFixed(3);
                                    }

                                  }
                                }
                                else if(itm.totalSaleQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalSaleQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalSaleQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalSaleQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalWastageQty!=undefined){
                                    /*if(itm.itemName == "Manchow Chicken Soup(bk)"){
                                            console.log('totalWastageQty in Manchow Chicken Soup(bk)', itm.itemName, itm.totalWastageQty);
                                        }*/
                                    if(it.closingQtyInBase==undefined){
                                        
                                        //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalWastageQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalWastageQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalWastageQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalReturnWastageQty!=undefined){
                                  if(it.closingQtyInBase==undefined){
                                    //it.closingQtyInBase=parseFloat(-itm.totalWastageQty).toFixed(3);
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      it.closingQtyInBase=parseFloat(parseFloat(-itm.totalReturnWastageQty)*1000).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(-itm.totalReturnWastageQty).toFixed(3);
                                    }
                                  }
                                  else
                                  {
                                    if(it.unitName=="Litre" || it.unitName=="Meter"){
                                      var closingQtyInBase=parseFloat(parseFloat(itm.totalReturnWastageQty)*1000).toFixed(3);
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                    }
                                    else
                                    {
                                      it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalReturnWastageQty)).toFixed(3);
                                    }
                                  }
                                }
                                else if(itm.totalTransferQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalTransferQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalTransferQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalTransferQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalTransferQty_toStore!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalTransferQty_toStore).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalTransferQty_toStore)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalTransferQty_toStore)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.itemSaleQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.itemSaleQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.itemSaleQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.itemSaleQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalStockReceiveQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.itemSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(itm.totalStockReceiveQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReceiveQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)+parseFloat(itm.totalStockReceiveQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalIntermediateQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        //it.closingQtyInBase=parseFloat(-itm.totalSaleQty).toFixed(3);
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalIntermediateQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalIntermediateQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalIntermediateQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalIntermediateQty)).toFixed(3);
                                        }
                                    }
                                }
                                else if(itm.totalStockReturnQty!=undefined){
                                    if(it.closingQtyInBase==undefined){
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            it.closingQtyInBase=parseFloat(parseFloat(-itm.totalStockReturnQty)*1000).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(-itm.totalStockReturnQty).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        if(it.unitName=="Litre" || it.unitName=="Meter"){
                                            var closingQtyInBase=parseFloat(parseFloat(itm.totalStockReturnQty)*1000).toFixed(3);
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(closingQtyInBase)).toFixed(3);
                                        }
                                        else
                                        {
                                            it.closingQtyInBase=parseFloat(parseFloat(it.closingQtyInBase)-parseFloat(itm.totalStockReturnQty)).toFixed(3);
                                        }
                                    }
                                }
                            }
                        });
                  
                });
                return items;
            }

    $scope.openConsolidatedIndentReport = function () {
      /*$scope.indentGrnDeliveryChallanForm.showConsolidated=true;
       $scope.indentGrnDeliveryChallanForm.consolidatedRequirements =[];
       $scope.indentGrnDeliveryChallanForm.recipesNotAvailable = false;
       $scope.indentGrnDeliveryChallanForm.getMaterialEstimation =false;
       $scope.indentGrnDeliveryChallanForm.enableSubmitPrint = true;
       $scope.indentGrnDeliveryChallanForm.open = false;
       console.log($scope.indentGrnDeliveryChallanForm.selectedOutletRequirements);
       for(var prop in $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements){
       var value = $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements[prop];
       if(value){
       _.forEach($scope.requirementChallan, function(r, i){
       console.log("Requirement chal", r)
       if(r._id == prop){
       _.forEach(r.items, function(it, j){
       var boolCheck = false;
       var recipecheck = false;
       _.forEach($scope.stores, function(s, o) {
       if(s.isKitchen){
       _.forEach(s.processedFoodCategory, function(ps, u){
       if(ps._id == it.fromCategory._id){
       it.kitchenDetails = s;
       console.log("KITCHEN DETAILS ASSSIGNED", it)
       return
       }
       })
       }
       })
       _.forEach($scope.stockRecipes, function(rec, p){
       if(rec.itemId== it._id){
       recipecheck = true;
       }
       })
       _.forEach($scope.indentGrnDeliveryChallanForm.consolidatedRequirements, function(c, t){
       if(c._id == it._id){
       boolCheck = true;
       c.consolidatedQuantity = c.consolidatedQuantity + it.qty;
       c.orderId.push(prop);
       }
       })
       if(!boolCheck){
       var firstItem = angular.copy(it);
       //firstItem.relatedInfo = angular.copy(r)
       firstItem.consolidatedQuantity = it.qty;
       firstItem.orderId =[];
       firstItem.recipeAvailable = recipecheck;
       if(!recipecheck)
       $scope.indentGrnDeliveryChallanForm.recipesNotAvailable = true;
       firstItem.orderId.push(prop)
       $scope.indentGrnDeliveryChallanForm.consolidatedRequirements.push(firstItem)
       }
       })
       }
       })
       }
       }
       console.log("CONSOL REQ", $scope.indentGrnDeliveryChallanForm.consolidatedRequirements);*/

      //modified
      $scope.indentGrnDeliveryChallanForm.showConsolidated = true;
      $scope.indentGrnDeliveryChallanForm.consolidatedRequirements = [];
      $scope.indentGrnDeliveryChallanForm.recipesNotAvailable = false;
      $scope.indentGrnDeliveryChallanForm.getMaterialEstimation = false;
      $scope.indentGrnDeliveryChallanForm.enableSubmitPrint = true;
      $scope.indentGrnDeliveryChallanForm.open = false;
      stockRequirement.getItemsInSelectedRequirements({ids: $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements}, function (data) {
        for (var prop in $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements) {
          var value = $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements[prop];
          if (value) {
            _.forEach($scope.requirementChallan, function (r, i) {
              var dat = _.find(data, function (d) {
                return r._id === d._id;
              });
              if (dat) {
                console.log(dat);
                r.items = dat.items;
              }
              if (r._id == prop) {

                _.forEach(r.items, function (it, j) {
                  var boolCheck = false;
                  var recipecheck = false;
                  _.forEach($scope.stores, function (s, o) {
                    if (s.isKitchen) {
                      _.forEach(s.processedFoodCategory, function (ps, u) {
                        if (ps._id == it.fromCategory._id) {
                          it.kitchenDetails = s;
                          return
                        }
                      })
                    }
                  })
                  _.forEach($scope.stockRecipes, function (rec, p) {
                    if (rec.itemId == it._id) {
                      recipecheck = true;
                    }
                  })
                  _.forEach($scope.indentGrnDeliveryChallanForm.consolidatedRequirements, function (c, t) {
                    if (c._id == it._id) {
                      boolCheck = true;
                      c.consolidatedQuantity = c.consolidatedQuantity + it.qty;
                      c.orderId.push(prop);
                    }
                  })
                  if (!boolCheck) {
                    var firstItem = angular.copy(it);
                    //firstItem.relatedInfo = angular.copy(r)
                    firstItem.consolidatedQuantity = it.qty;
                    firstItem.orderId = [];
                    firstItem.recipeAvailable = recipecheck;
                    if (!recipecheck)
                      $scope.indentGrnDeliveryChallanForm.recipesNotAvailable = true;
                    firstItem.orderId.push(prop)
                    $scope.indentGrnDeliveryChallanForm.consolidatedRequirements.push(firstItem)
                  }
                })
              }
            })
          }
        }
        });

      //end
    };

    function calculateRawMaterialByKitchen() {
      $scope.rawMaterialByKitchen = [];
      var reqChallanCopy = $scope.requirementChallan; //angular.copy($scope.requirementChallan);
      _.forEach(reqChallanCopy, function (r, i) {
        for (var prop in $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements) {
          var check = $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements[r._id];
          if (check) {
            _.forEach(r.items, function (itm, l) {
              var copyOfRawMaterial = angular.copy(itm.rawMaterialForItem)
              _.forEach(copyOfRawMaterial, function (rm, q) {
                var kitchencheck = false;
                _.forEach($scope.rawMaterialByKitchen, function (k, j) {
                  if (itm.kitchenDetails) {
                    if (itm.kitchenDetails['_id'] == k._id) {
                      kitchencheck = true;
                      var itemcheck = false;
                      _.forEach(k.items, function (it, l) {
                        if (it._id == rm._id) {
                          itemcheck = true;
                          it.calculatedQuantity = parseFloat(parseFloat(it.calculatedQuantity) + parseFloat(rm.calculatedQuantity)).toFixed(3);
                          }
                      })
                      if (itemcheck == false) {
                        k.items.push(rm)
                        }
                    }
                  }
                })
                if (kitchencheck == false) {
                  if (itm.kitchenDetails) {
                    var object = {_id: itm.kitchenDetails['_id'], name: itm.kitchenDetails['storeName'], items: []}
                    object.items.push(rm);
                    $scope.rawMaterialByKitchen.push(object)
                    }
                  else {
                    console.log("KITCJEN DETAIS MISSING FOR ", itm)
                  }
                }
              })
            })
            return
          }
        }
      })
      };

    function getUnitBaseConversion(unitId) {
      var index = _.findIndex($scope.stockUnits, {_id: unitId});
      var conversionFactor = 1;
      if (index >= 0) {
        conversionFactor = $scope.stockUnits[index].conversionFactor;
      }
      return conversionFactor;
    };

    function getAllRawMaterialsPerItem() {
      var rawMaterial = [];
      $scope.indentGrnDeliveryChallanForm.getEstimation = true;
      console.log($scope.requirementChallan)
      var itemsArray = $scope.items; //angular.copy($scope.items)
      _.forEach($scope.requirementChallan, function (rc, i) {
        for (var prop in $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements) {
          var check = $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements[rc._id]
          if (check) {
            _.forEach(rc.items, function (itm, i) {
              if (itm.qty != "" && !isNaN(itm.qty)) {
                var rawMaterialForItem = [];
                var index = _.findIndex($scope.stockRecipes, {itemId: itm._id});
                if (index >= 0) {
                  var rIt = angular.copy($scope.stockRecipes[index].rawItems);
                  _.forEach(rIt, function (r, ii) {
                    var recipeQty = parseFloat($scope.stockRecipes[index].quantity).toFixed(3);
                    var recConFactor = parseFloat($scope.stockRecipes[index].selectedUnitId.conversionFactor);
                    var rIndex = _.findIndex(rawMaterial, {_id: r._id});
                    var rIntIndex = _.findIndex(rawMaterialForItem, {_id: r._id});
                    var bUName = r.selectedUnitId.baseUnit.name;
                    var conversionFactor = getUnitBaseConversion(r.selectedUnitId._id);
                    r.baseUnitName = bUName;
                    var ItemForCal;
                    _.forEach(itemsArray, function (c, i) {
                      if (c._id == itm._id) {
                        ItemForCal = angular.copy(c);
                        return;
                      }
                    })
                    var menuQty = parseFloat(ItemForCal.stockQuantity) * parseFloat(ItemForCal.unit.conversionFactor);
                    recipeQty = parseFloat(recipeQty) * parseFloat(recConFactor);
                    var qty1 = parseFloat(parseFloat(menuQty) / parseFloat(recipeQty));
                    var qty = parseFloat(itm.qty) * parseFloat(qty1) * parseFloat(r.quantity) * parseFloat(conversionFactor);
                    r.calculatedQuantity = parseFloat(qty).toFixed(3);
                    if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                      r.calculatedQuantity = parseFloat(parseFloat(r.calculatedQuantity) / 1000).toFixed(3);
                    }
                    if (rIndex >= 0) {
                      rawMaterial[rIndex].calculatedQuantity = parseFloat(parseFloat(rawMaterial[rIndex].calculatedQuantity) + parseFloat(r.calculatedQuantity)).toFixed(3);
                      //console.log("aleady in array hence adding" +itm.itemName + "for" + r.itemName + "is"  , angular.copy(rawMaterial[rIndex]))
                    }
                    else {
                      rawMaterial.push(r);
                      //console.log("pushing",  angular.copy(rawMaterial));
                    }

                    var rawMaterialCopy = angular.copy(r)
                    if (rIntIndex >= 0) {
                      rawMaterialForItem[rIntIndex].calculatedQuantity = parseFloat(parseFloat(rawMaterialForItem[rIntIndex].calculatedQuantity) + parseFloat(rawMaterialCopy.calculatedQuantity)).toFixed(3);
                      //console.log("RZAW MATERIAL UPDSTED" , angular.copy(rawMaterialForItem))
                    }
                    else {
                      //console.log("RAW MATERIAL PUSHING", angular.copy(rawMaterialCopy));
                      rawMaterialForItem.push(rawMaterialCopy)
                    }
                  });
                }
                itm.rawMaterialForItem = rawMaterialForItem;
                //console.log(" TALKING OF ITM ", angular.copy(itm));
              }
            })
            return
          }
        }
      });
      calculateRawMaterialByKitchen();
      $scope.indentGrnDeliveryChallanForm.getEstimation = true;
      return rawMaterial;
    };

    function getMaterials_WithSemiItems() {
      var rawMaterial = [];
      $scope.rawMaterialByKitchen = [];
      $scope.rawMaterialByCategory = [];
      var itemsArray = $scope.items; //angular.copy($scope.items)
      var itmss = [];
      _.forEach(itemsArray, function (c, i) {
        _.forEach($scope.indentGrnDeliveryChallanForm.consolidatedRequirements, function (f, i) {
          if (c._id == f._id) {
            var d = angular.copy(c);
            d.consolidatedQuantity = f.consolidatedQuantity;
            d.fromCategory = f.fromCategory;
            d.type = f.type;
            d.relatedInfo = f.relatedInfo;
            d.orderId = f.orderId;
            itmss.push(d);
          }
        })
      });
      _.forEach(itmss, function (itm, i) {
        if (itm.stockQuantity != undefined) {
          if (itm.consolidatedQuantity != "" && !isNaN(itm.consolidatedQuantity)) {
            var index = _.findIndex($scope.stockRecipes, {itemId: itm._id});
            if (index >= 0) {
              var rIt = angular.copy($scope.stockRecipes[index].rawItems);
              _.forEach(rIt, function (r, ii) {
                r.categoryId = $scope.stockRecipes[index].categoryId
                _.forEach($scope.stores, function (s, o) {
                  if (s.isKitchen) {
                    _.forEach(s.processedFoodCategory, function (ps, u) {
                      if (ps._id == itm.fromCategory._id) {
                        r.kitchenDetails = s;
                        return
                      }
                    })
                  }
                })
                r.orderId = itm.orderId;
                var recipeQty = parseFloat($scope.stockRecipes[index].quantity).toFixed(3);
                var recConFactor = parseFloat($scope.stockRecipes[index].selectedUnitId.conversionFactor);
                var rIndex = _.findIndex(rawMaterial, {_id: r._id});
                var bUName = r.selectedUnitId.baseUnit.name;
                var conversionFactor = getUnitBaseConversion(r.selectedUnitId._id);
                r.baseUnitName = bUName;
                var menuQty = parseFloat(itm.stockQuantity) * parseFloat(itm.unit.conversionFactor);
                recipeQty = parseFloat(recipeQty) * parseFloat(recConFactor);
                var qty1 = parseFloat(parseFloat(menuQty) / parseFloat(recipeQty));
                var qty = parseFloat(itm.consolidatedQuantity) * parseFloat(qty1) * parseFloat(r.quantity) * parseFloat(conversionFactor);
                r.calculatedQuantity = parseFloat(qty).toFixed(3);
                if (r.baseUnitName == "Litre" || r.baseUnitName == "Meter") {
                  r.calculatedQuantity = parseFloat(parseFloat(r.calculatedQuantity) / 1000).toFixed(3);
                }
                if (rIndex >= 0) {
                  rawMaterial[rIndex].calculatedQuantity = parseFloat(parseFloat(rawMaterial[rIndex].calculatedQuantity) + parseFloat(r.calculatedQuantity)).toFixed(3);
                }
                else {
                  r.relatedInfo = itm.relatedInfo;
                  rawMaterial.push(r);
                }
              });
            }
          }
        }
        else {
          growl.error(itm.itemName + " don't have stockQuantity defined escaped from Estimation");
        }
      });
      _.forEach(rawMaterial, function (r, i) {
        var kitchencheck = false;
        _.forEach($scope.rawMaterialByKitchen, function (k, j) {
          if (r.kitchenDetails['_id'] == k._id) {
            kitchencheck = true;
            var itemcheck = false;
            _.forEach(k.items, function (it, l) {
              if (it._id == r._id) {
                itemcheck = true;
                it.calculatedQuantity = it.calculatedQuantity + r.calculatedQuantity;
              }
            })
            if (itemcheck == false) {
              k.items.push(r)
            }
          }
        })
        if (kitchencheck == false) {
          var object = {_id: r.kitchenDetails['_id'], name: r.kitchenDetails['storeName'], items: [], orderId: []}
          object.items.push(r);
          //console.log("insrted with order id", angular.copy(object))
          $scope.rawMaterialByKitchen.push(object)
        }
      })
      _.forEach($scope.rawMaterialByKitchen, function (r, i) {
        _.forEach(r.items, function (it, k) {
          _.forEach(it.orderId, function (o, l) {
            var ind = r.orderId.indexOf(o)
            if (ind < 0) {
              r.orderId.push(o)
            }
          })
        })
      })
      _.forEach($scope.requirementChallan, function (r, i) {
        //console.log("r", r)
        _.forEach($scope.rawMaterialByKitchen, function (k, t) {
         _.forEach(k.orderId, function (n, m) {
            if (n == r._id) {
              var object = {_id: k._id, name: k.name};
              r.assignedKitchen = object;
            }
          })
        })
      })
      return $scope.rawMaterialByKitchen
    };

    /*$scope.estimateRawMaterials = function () {
     $scope.indentGrnDeliveryChallanForm.getMaterialEstimation = true;
     $scope.indentGrnDeliveryChallanForm.getEstimation = true;
     var rawMaterial = getAllRawMaterialsPerItem();
     console.log("raw material returned", rawMaterial);
     //rawMaterial=convertItemInPrefferedUnit($scope.rawMaterialByKitchen);
     $scope.rawMaterial = rawMaterial;

     console.log("RAW", $scope.rawMaterial)
     convertItemInPrefferedUnit($scope.rawMaterialByKitchen);
     $scope.indentGrnDeliveryChallanForm.getEstimation = false;
     };*/

    $scope.estimateRawMaterials = function () {
      $scope.indentGrnDeliveryChallanForm.allowTransfer = true;
      $scope.indentGrnDeliveryChallanForm.unavailableItems = [];
      $scope.indentGrnDeliveryChallanForm.getMaterialEstimation = true;
      $scope.indentGrnDeliveryChallanForm.getEstimation = true;
      var rawMaterial = getAllRawMaterialsPerItem();
      //$scope.closingQty;
      
      _.forEach($scope.stores,function(s){
        if(s.isMain){
          $scope.mainStore=s;
        }
      });
      _.forEach($scope.closingQty,function(cQty){
        // console.log('inside foreach 1');
        _.forEach(rawMaterial,function(r){

          // console.log('inside foreach 2');
          if(cQty.storeId==$scope.mainStore._id){
            if(cQty.itemId== r._id){
              var qty=parseInt(r.calculatedQuantity);
              if(cQty.closingQtyInBase < qty)
              {
                $scope.indentGrnDeliveryChallanForm.allowTransfer = false;
                $scope.indentGrnDeliveryChallanForm.unavailableItems.push(cQty.itemName);
              }
            }
          }
        })
      });

      //rawMaterial=convertItemInPrefferedUnit($scope.rawMaterialByKitchen);
      $scope.rawMaterial = rawMaterial;

      convertItemInPrefferedUnit($scope.rawMaterialByKitchen);
      $scope.indentGrnDeliveryChallanForm.getEstimation = false;
    };

    function convertItemInPrefferedUnit(result) {
      _.forEach(result, function (r, i) {
        _.forEach(r.items, function (itm, ii) {
          var indeex = _.findIndex($scope.stockItem, {_id: itm._id});
          if (indeex >= 0) {
            var item = $scope.stockItem[indeex];
            if (item.preferedUnit != undefined) {
              _.forEach(item.units, function (u, i) {
                if (u._id == item.preferedUnit) {
                  var conversionFactor = 1;
                  var pconFac = parseFloat(u.conversionFactor);
                  itm.baseUnitName = u.unitName;
                  if (u.baseUnit.id == 2 || u.baseUnit.id == 3) {
                    conversionFactor = parseFloat(u.baseConversionFactor);
                    if (pconFac > conversionFactor) {
                      itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) / parseFloat(conversionFactor)).toFixed(3);
                    }
                    else if (pconFac < conversionFactor) {
                      itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) * parseFloat(conversionFactor)).toFixed(3);
                    }
                  }
                  else {
                    if (pconFac > conversionFactor) {
                      itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) / parseFloat(pconFac)).toFixed(3);
                    }
                    else if (pconFac < conversionFactor) {
                      itm.calculatedQuantity = parseFloat(parseFloat(itm.calculatedQuantity) * parseFloat(pconFac)).toFixed(3);
                    }
                  }
                }
              });
            }
          }
        });
      });
      return result;
    };


    //---------------------------------------------Delivery Challan----------------------------------------------------

    $scope.indentChallan = {
      enableSubmit: true,
      isPrint: false,
      enableForm: true,
      activeOrder: true,
      activeChallan: false
    };
    $scope.indentGrnReceiveForm = {enableSubmit: true, isPrint: false};
    $scope.indentGrnReceiveForm.isNew = true;
    $scope.indentGrnReceive = {enableSubmit: true, isPrint: false};

    disableGRNChallanFormWhenNotVEndor('Vendor');
    function disableGRNChallanFormWhenNotVEndor(name) {
      _.forEach(currentUser.selectedRoles, function (r, i) {
        if (r.name == name) {
          $scope.indentChallan.enableForm = false;
          $scope.indentChallan.activeOrder = false;
          $scope.indentChallan.activeChallan = true;
          $scope.indentChallan.disableSupplied = true;
        }
      });
    };

    $scope.clearReceive = function () {
      $scope.indentGrnReceiveForm = {enableSubmit: false, isPrint: false, isNew: true};
    };

    $scope.goNextChallan = function (nextIdx) {
      var f = angular.element(document.querySelector('#ch_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
      else {
        $scope.indentChallan.enableSubmit = false;
        angular.element(document.querySelector('#submitChallan')).focus();
      }
    };

    function formatItemsRec(items) {
      _.forEach(items, function (itm, i) {
        var it = {
          _id: itm._id,
          receiveQty: itm.supplyQty
        };
        itm = it;
      });
    };

    // function formatItemsRecNCh(items, nCh) {
    //   nCh.items = [];
    //   _.forEach(items, function (itm, i) {
    //     var it = {
    //       _id: itm._id,
    //       supplyQty: itm.supplyQty,
    //       receiveQty: itm.supplyQty,
    //       supplyComment: itm.supplyComment,
    //       recipeAvailable: itm.recipeAvailable,
    //       closingQtyInBase: itm.closingQtyInBase,
    //       availableQtyInBase: itm.availableQtyInBase
    //     };
    //     nCh.items.push(it);
    //   });
    // };

    function formatItemsRecNCh(items, nCh) {
      nCh.items = [];
      _.forEach(items, function (itm, i) {
        var it = {
          _id: itm._id,
          supplyQty: itm.supplyQty,
          receiveQty: itm.supplyQty,
          supplyComment: itm.supplyComment,
          recipeAvailable: itm.recipeAvailable,
          closingQtyInBase: itm.closingQtyInBase,
          availableQtyInBase: itm.availableQtyInBase
        };
        nCh.items.push(it);
        
      });
      console.log(openIndentStore)
       var stores = openIndentStore;
        stores.vendor.category = [];
        _.forEach(items, function (itm, i) {
          var index = _.findIndex(stores.vendor.category, {_id: itm.fromCategory._id});
          if (index < 0) {
            stores.vendor.category.push(itm.fromCategory);
          }
        });

        _.forEach(stores.vendor.category, function (c, i) {
          c.items = [];
          _.forEachRight(items, function (itm, ii) {
            if(itm.fromCategory)
            if (c._id == itm.fromCategory._id) {
              delete itm.fromCategory;
              console.log("itm",angular.copy(itm))
              c.items.push(itm);
            }
          });
        });
        nCh.store=[]
        nCh.store=angular.copy(stores)
    };
    function checkItemAssignments(items) {
      $scope.InvalidItems = [];
      addKitchenDetails(items);
      checkRecipeAvailable(items);
      _.forEach(items, function (it, l) {
        var enableItem = false;
        if (it.fromCategory) {
          if (it.kitchenDetails) {
            if (it.recipeAvailable) {
              enableItem = true;
            } else {
              it.noRecipeDetail = true;
              $scope.InvalidItems.push(it);
            }
          } else {
            it.noKitchenDetail = true;
            $scope.InvalidItems.push(it)
          }
        } else {
          it.noCategory = true;
          $scope.InvalidItems.push(it)
        }
      })
    };

    $scope.EnableSubmitChallan = function (items) {
      //console.log("ITEMS PASSED", items)
      var flag = false;
      var validAssignment = checkItemAssignments(items);
      //console.log("VALID ssign,ent", validAssignment)
      //console.log("invalid items array length ", $scope.InvalidItems.length)
      if ($scope.InvalidItems.length == 0) {
        _.forEach(items, function (item, i) {
          if (flag == false) {
            if (!isNaN(parseInt(item.supplyQty)) || parseInt(item.supplyQty) == 0) {
              if (item.supplyQty == "" && !(parseInt(item.supplyQty) >= 0)) {
                flag = true;
                growl.error('Please provide a valid number', {ttl: 3000});
                //console.log("SUPPLY", item.supplyQty);
              } else if (item.supplyQty == item.qty) {
                flag = false;
              }
              else if (item.supplyQty < item.qty) {
                if (item.supplyComment != undefined && item.supplyComment != "") {
                  flag = false;
                }
                else {
                  growl.error('Please provide a valid comment', {ttl: 3000});
                  flag = true;
                }
              }
              else if (item.supplyQty > item.qty) {

                if (item.supplyComment != undefined && item.supplyComment != "") {
                  flag = false;
                }
                else {
                  growl.error('Please provide a valid comment', {ttl: 3000});
                  flag = true;
                }
              }
            }
            else {
              //console.log('ELSE EM STRING')
              item.supplyQty = "";
              flag = true;
              growl.error('Please provide a valid number', {ttl: 3000});
              //console.log("SUPPLY", item.supplyQty);
            }
          }
        });
      } else {
        //console.log("IN ELSE TO DISABLE BUTTON")
        flag = true;
      }
      if (flag == false) {
        $scope.indentChallan.enableSubmit = false;
      }
      else {
        $scope.indentChallan.enableSubmit = true;
      }
    };

    //Modified (28/6/2016)
    $scope.EnableSubmitChallanForm = function (items) {
      //console.log("ITEMS PASSED", items)
      var flag = false;
      //var validAssignment = checkItemAssignments(items);
      //console.log("VALID ssign,ent", validAssignment)
      //console.log("invalid items array length ", $scope.InvalidItems.length)
      if ($scope.InvalidItems.length == 0) {
        _.forEach(items, function (item, i) {
          if (flag == false) {
            if (!isNaN(parseInt(item.supplyQty)) || parseInt(item.supplyQty) == 0) {
              if (item.supplyQty == "" && !(parseInt(item.supplyQty) >= 0)) {
                flag = true;
                growl.error('Please provide a valid number', {ttl: 3000});
                //console.log("SUPPLY", item.supplyQty);
              } else if (item.supplyQty == item.qty) {
                flag = false;
              }
              else if (item.supplyQty < item.qty) {
                if (item.supplyComment != undefined && item.supplyComment != "") {
                  flag = false;
                }
                else {
                  growl.error('Please provide a valid comment', {ttl: 3000});
                  flag = true;
                }
              }
              else if (item.supplyQty > item.qty) {

                if (item.supplyComment != undefined && item.supplyComment != "") {
                  flag = false;
                }
                else {
                  growl.error('Please provide a valid comment', {ttl: 3000});
                  flag = true;
                }
              }
            }
            else {
              //console.log('ELSE EM STRING')
              item.supplyQty = "";
              flag = true;
              growl.error('Please provide a valid number', {ttl: 3000});
              //console.log("SUPPLY", item.supplyQty);
            }
          }
        });
      } else {
        //console.log("IN ELSE TO DISABLE BUTTON")
        flag = true;
      }
      if (flag == false) {
        $scope.indentChallan.enableSubmit = false;
      }
      else {
        $scope.indentChallan.enableSubmit = true;
      }
    };

    $scope.submitChallanMultipart = function (Challan) {
      $scope.indentChallan.enableSubmit = true;
      //var ch=angular.copy(Challan);
      var ch = angular.copy(Challan);
      var nCh = {
        _id: ch._id
      };
      //console.log("nCh", nCh);
      $scope.indentGrnDeliveryChallanForm = ch;
      $scope.indentGrnDeliveryChallanForm.requirementSupplied = false;
      var _emailScope = $rootScope.$new();
      var table1 = "";
      table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'>";
      table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
      table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
      table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Delivery Challan <br /><br /></td></tr>";
      table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Asked Quantity</th><th>Supplied Quantity</th></thead>";
      table1 += "<tbody>";
      formatItemsRecNCh(ch.items, nCh);
      //ch.items= angular.copy(nCh.items) 
      formatItemsRec(ch.items);
      ch.supplyDate = new Date().toISOString();
      nCh.supplyDate = new Date().toISOString();
      //console.log(nCh);
      //console.log(JSON.stringify(ch).length);
      //var blob=new Blob([ch.toString()],{type:'text/plain'});
      console.log('nch', nCh);
      var payload = new FormData();
      var bt = new Blob([JSON.stringify(nCh)], {type: 'text/plain'});
      payload.append('bt2', bt);
      //console.log(blob);
      createDeliveryChallan_StockTransaction(ch);
      $http({
        url: '/api/stockRequirements/' + nCh._id,
        method: 'put',
        data: nCh,
      }).then(function success(response) {
        growl.success('Delivery Challan Dispatched Successfully.',{ttl:3000})
        //$scope.resdata=response.data;
        //console.log("result", response.data)
        $scope.indentChallan.isPrint = true;
        //$scope.requirementReceive.push(response.data);
        $scope.requirementReceive.push(ch);
        //console.log($scope.req.indents);
        var indexReq = _.findIndex(angular.copy($scope.req.indents), {_id: nCh._id});
        //console.log(indexReq);
        $scope.req.indents.splice(indexReq,1);
        var index = _.findIndex($scope.requirementChallan, {_id: ch._id});
        if (index >= 0) {
          $scope.requirementChallan.splice(index, 1);
        }

        var gt = 0;
        $scope.indentGrnDeliveryChallanForm.sumOfItems = 0;
        _.forEach(ch.items, function (item, i) {
          table1 += "<tr>";
          table1 += "<td>" + item.itemName + "</td>";
          table1 += "<td>" + item.selectedUnit.unitName + "</td>";
          table1 += "<td>" + item.qty + "</td>";
          table1 += "<td>" + item.supplyQty + "</td>"
          table1 += "</tr>";
          gt += (item.supplyQty * item.price)
        });
        $scope.indentGrnDeliveryChallanForm.sumOfItems = gt;

        //var tableCopy = angular.copy(table1)
        var printTable;
        var _message = $compile($('<div>' + table1 + '</div>'))(_emailScope);
        var _to = 'ajittechit@gmail.com'

        Email.send({}, {
          messageText: _message.text(),
          messageHtml: _message.html(),
          to: _to.toString()
        }, function (data) {
          $timeout(function () {
            printTable = document.getElementById('deliveryChallanGenerated').innerHTML;
            // console.log('printTablet', printTable);
            //console.log("DATA" + data)
            print_Entry(printTable)
          }, 10);
        }, function (err) {
          console.log("ERROR", err);
        })
        //$scope.requirementReceive.push($scope.resdata);
        $scope.requirementReceive.push(ch);
        var index = _.findIndex($scope.requirementChallan, {_id: ch._id});
        if (index >= 0) {
          $scope.requirementChallan.splice(index, 1);
        }
        //$scope.suppliedRequirements.push($scope.resdata)
        $scope.suppliedRequirements.push(ch)
        $scope.indentGrnDeliveryChallanForm.requirementSupplied = true;
        //createDeliveryChallan_StockTransaction($scope.resdata);
      })

      /*stockRequirement.update({},ch, function (result){
       });*/

      _emailScope.$destroy();

    }

    /*$scope.submitChallan= function(Challan){
     $scope.indentChallan.enableSubmit=true;
     //var ch=angular.copy(Challan);
     var ch=Challan;
     console.log("challan", Challan);
     $scope.indentGrnDeliveryChallanForm= ch;
     $scope.indentGrnDeliveryChallanForm.requirementSupplied = false;
     var _emailScope = $rootScope.$new();
     var table1="";
     table1+="<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'>";
     table1+="<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
     table1+="<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
     table1+="<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Delivery Challan <br /><br /></td></tr>";
     table1+="<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Asked Quantity</th><th>Supplied Quantity</th></thead>";
     table1+="<tbody>";
     formatItemsRec(ch.items);
     ch.supplyDate=new Date().toISOString();
     stockRequirement.update({},ch, function (result){
     console.log("result", result)
     $scope.indentChallan.isPrint=true;
     $scope.requirementReceive.push(result);
     var index=_.findIndex($scope.requirementChallan,{_id:ch._id});
     if(index>=0){
     $scope.requirementChallan.splice(index,1);
     }
     console.log("BEFORE CREATING TABLE", ch)
     var gt=0;
     $scope.indentGrnDeliveryChallanForm.sumOfItems = 0;
     _.forEach(ch.items, function(item,i){
     table1+="<tr>";
     table1+="<td>"+item.itemName+"</td>";
     table1+="<td>"+item.selectedUnit.unitName+"</td>";
     table1+="<td>"+item.qty+"</td>"
     table1+="<td>" + item.supplyQty+"</td>"
     table1+="</tr>";
     gt  += (item.supplyQty * item.price)
     });
     $scope.indentGrnDeliveryChallanForm.sumOfItems= gt;
     //var tableCopy = angular.copy(table1)
     var printTable ;
     $timeout(function() {
     printTable= document.getElementById('deliveryChallanGenerated').innerHTML;
     }, 0);
     var _message = $compile($('<div>' + table1+ '</div>'))(_emailScope);
     var _to ='ajittechit@gmail.com'
     Email.send({}, {
     messageText: _message.text(),
     messageHtml: _message.html(),
     to: _to.toString()
     }, function (data) {
     console.log("DATA" + data)
     print_Entry(printTable)
     }, function (err) {
     console.log("ERROR", err);
     })
     $scope.requirementReceive.push(result);
     var index=_.findIndex($scope.requirementChallan,{_id:ch._id});
     if(index>=0){
     $scope.requirementChallan.splice(index,1);
     }
     $scope.suppliedRequirements.push(result)
     $scope.indentGrnDeliveryChallanForm.requirementSupplied = true;
     createDeliveryChallan_StockTransaction(result);
     console.log("UPDATED SUPPLIED REQ", $scope.suppliedRequirements)
     });
     _emailScope.$destroy();
     };*/

    $scope.goNextPoReceive = function (nextIdx) {
      var f = angular.element(document.querySelector('#pr_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
      else {
        $scope.indentGrnReceive.enableSubmit = false;
        angular.element(document.querySelector('#submitReceive')).focus();
      }
    };

    $scope.openIndentGrnReceiveForm = function (r) {
      /*$scope.indentGrnReceiveForm=r;
       console.log("OPENED CHALLAN", r)
       var tempArray =[];
       tempArray = r.items;
       $scope.EnableSubmitReciveForm(tempArray,r.store._id);
       $scope.indentGrnReceiveForm.isNew=false;*/

      stockRequirement.getStoreNItems({
        id: r._id,
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function (result) {
        var tempArray = [];
        tempArray = result.items;
        r.items = result.items;
        r.store = result.store;
        r.toDeployment_id = result.toDeployment_id;
        _.forEach(r.items,function(it){
          it.availableQtyBaseKitchen=it.availableQtyInBase;
          it.closingQtyBaseKitchen=it.closingQtyInBase;
        });
        $scope.indentGrnReceiveForm = r;
        $scope.EnableSubmitReciveForm(tempArray, result.store._id);
        $scope.indentGrnReceiveForm.isNew = false;
      });
    };

    $scope.EnableSubmitReciveForm = function (items, storeId) {
      console.log("CALLED");
      var flag = false;
      _.forEach(items, function (item, i) {
        if (flag == false) {
          var avQtyIndex = _.findIndex($scope.closingQty, {itemName: item.itemName});
          if (avQtyIndex >= 0) {
            item.closingQtyInBase = $scope.closingQty[avQtyIndex].closingQtyInBase;
            item.availableQtyInBase = parseFloat(parseFloat($scope.closingQty[avQtyIndex].closingQtyInBase) / parseFloat(item.selectedUnit.conversionFactor)).toFixed(3);
          }
          else {
            item.availableQtyInBase = parseFloat(0).toFixed(3);
          }
           if (!isNaN(parseInt(item.receiveQty)) || parseInt(item.receiveQty) == 0) {
            if (item.receiveQty == "" && !(parseInt(item.receiveQty) >= 0)) {
              flag = true;
              growl.error('Please provide a valid number', {ttl: 3000});
            }
            else if (item.supplyQty == item.receiveQty) {
              flag = false;
               }
            else if (item.supplyQty > item.receiveQty) {
              if (item.receiveComment != undefined && item.receiveComment != "") {
                flag = false;
                 }
              else {
                growl.error('Please provide a valid comment', {ttl: 3000});
                flag = true;
                }
            }
            else if (item.supplyQty < item.receiveQty) {
              growl.error('Receive Quntity must be less than supplied quantity', {ttl: 3000});
              flag = true;
              }
          }
          else {
            item.receiveQty = "";
            flag = true;
            growl.error('Please provide a valid number', {ttl: 3000});
          }
        }
      });
      if (flag == false) {
        $scope.indentGrnReceive.enableSubmit = false;
      }
      else {
        $scope.indentGrnReceive.enableSubmit = true;
      }
    };

    function formatDataForStockEntry(r) {
      console.log(r);
      r._items = angular.copy(r.items);
      r._vendor = angular.copy(r.vendor);
      r._store = angular.copy(r.store);
      //_.forEach(r._store,function(s,i){
      //_.forEach(r._store.vendor,function(v,ii){
      _.forEach(r._store.vendor.category, function (c, iii) {
        _.forEach(c.items, function (itm, iiii) {
          var index = _.findIndex(r._items, {_id: itm._id});
          itm.qty = parseFloat(r._items[index].receiveQty);
          var selectedUnits = {};
          selectedUnits[itm._id] = itm.selectedUnit;
          var ss = basePrefferedAnsSelectedUnitsByItem(itm, selectedUnits, 1);
          itm.calculateInUnits = ss;
        });
      });
      //});
      //});
      return r;
    };

    $scope.resource = $resource('/api/stockTransactions/:id',
      {
        id: '@_id'
      },
      {
        saveData: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        findOne: {method: 'GET', isArray: true},
        update: {method: 'PUT'}
      }
    );

    $scope.selectionForConsolidation = function () {
      $scope.selectedForConsoloidated = false;
      for (var prop in $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements) {
        if ($scope.indentGrnDeliveryChallanForm.selectedOutletRequirements[prop]) {
          $scope.selectedForConsoloidated = true;
          return
        }
      }
    };

    $scope.transferMaterialsKitchenWise = function () {

      var transactions = [];
      $scope.indentGrnDeliveryChallanForm.enableSubmitPrint = false;
      $scope.transactionsToPush = [];
      _.forEach($scope.indentGrnDeliveryChallanForm.selectedOutletRequirements, function (sR, id) {
        var OrderKitchenCombo = {};
        var ch;
        if (sR) {
          ch = _.find($scope.requirementChallan, function (r) {
            return r._id == id;
          });
          _.forEach(ch.items, function (it, ii) {
            if (it.kitchenDetails) {
              if (!OrderKitchenCombo[it.kitchenDetails._id]) {
                OrderKitchenCombo[it.kitchenDetails._id] = angular.copy(ch);
                OrderKitchenCombo[it.kitchenDetails._id].items = [];
              }
              OrderKitchenCombo[it.kitchenDetails._id].items.push(it);
            }
          });
         /*for (var prop in OrderKitchenCombo) {
           $scope.newReset_StockEntry(OrderKitchenCombo[prop]);
           }*/
          for (var prop in OrderKitchenCombo) {
            transactions.push($scope.newReset_StockEntryAsync(OrderKitchenCombo[prop]));
          }
          StockResource.createMultiple({}, {transactions: transactions}, function (result) {
            });
          }
      });

      var table1 = "";
      table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'>"
      table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
      table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
      table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Internal Kitchen Transfer<br /><br /></td></tr>";
      table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Sno. </th><th>Raw Material</th><th>Quantity</th><th>Unit</th></thead>";
      table1 += "<tbody>";
      _.forEach($scope.rawMaterialByKitchen, function (raw, i) {
        table1 += "<tr>";
        table1 += "<td colspan='8'>" + raw.name + "</td>";
        table1 += "</tr>";
        _.forEach(raw.items, function (item, j) {
          table1 += "<tr>";
          table1 += "<td>" + (j + 1) + "</td>"
          table1 += "<td>" + item.itemName + "</td>"
          table1 += "<td>" + item.calculatedQuantity + "</td>"
          table1 += "<td>" + item.baseUnitName + "</td>"
          table1 += "</tr>";
        });
      });
      print_Entry(table1);
    };

    /*$scope.transferMaterialsKitchenWise = function () {

     var transactions = [];
     $scope.indentGrnDeliveryChallanForm.enableSubmitPrint = false;
     $scope.transactionsToPush = [];
     console.log('req chalan', $scope.requirementChallan);
     console.log('selected req', $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements);
     _.forEach($scope.indentGrnDeliveryChallanForm.selectedOutletRequirements, function (sR, id) {
     var OrderKitchenCombo = {};
     var ch;
     if (sR) {
     ch = _.find($scope.requirementChallan, function (r) {
     return r._id == id;
     });
     _.forEach(ch.items, function (it, ii) {
     if (it.kitchenDetails) {
     if (!OrderKitchenCombo[it.kitchenDetails._id]) {
     OrderKitchenCombo[it.kitchenDetails._id] = angular.copy(ch);
     OrderKitchenCombo[it.kitchenDetails._id].items = [];
     }
     OrderKitchenCombo[it.kitchenDetails._id].items.push(it);
     }
     });
     console.log("final OrderKitchenCombo for requirement", OrderKitchenCombo)
     for (var prop in OrderKitchenCombo) {
     $scope.newReset_StockEntry(OrderKitchenCombo[prop]);
     }
     /!*for (var prop in OrderKitchenCombo) {
     //transactions.push($scope.newReset_StockEntryAsync(OrderKitchenCombo[prop]));
     $scope.newReset_StockEntryAsync(OrderKitchenCombo[prop]).then(function (transaction) {
     transactions.push(transaction);
     console.log(transactions);
     });
     }*!/
     /!*$scope.makeTransferTransactionArray(OrderKitchenCombo).then(function (transactions) {
     StockResource.createMultiple({}, {transactions: transactions}, function (result) {
     console.log(result);
     });
     })*!/
     console.log(transactions);
     }
     });

     /!*_.forEach($scope.requirementChallan, function(r, i){
     var OrderKitchenCombo= {};
     var check = false;
     check = $scope.indentGrnDeliveryChallanForm.selectedOutletRequirements[r._id]
     if(check){
     var kitchenAdded = false;
     console.log("REQ CHALLAN", angular.copy(r))
     _.forEach(r.items, function(it, jj){
     console.log("ITEM", it)
     if(it.kitchenDetails){
     var kitCheck = false;
     for(var prop in OrderKitchenCombo){
     console.l/og("prop", prop)
     if(it.kitchenDetails['_id'] == prop){
     kitCheck = true;
     OrderKitchenCombo[prop].items.push(it)
     return;
     }
     }
     if(!kitCheck && it.rawMaterialForItem.length > 0){
     OrderKitchenCombo[it.kitchenDetails['_id']] =  angular.copy(r);
     OrderKitchenCombo[it.kitchenDetails['_id']].items=[];
     OrderKitchenCombo[it.kitchenDetails['_id']].items.push(it)
     }
     }
     })
     }
     console.log("final OrderKitchenCombo for requirement", OrderKitchenCombo)
     for(var prop in OrderKitchenCombo){
     $scope.newReset_StockEntry(OrderKitchenCombo[prop])
     }
     })*!/
     var table1 = "";
     table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'>"
     table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
     table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
     table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Internal Kitchen Transfer<br /><br /></td></tr>";
     table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Sno. </th><th>Raw Material</th><th>Quantity</th><th>Unit</th></thead>";
     table1 += "<tbody>";
     _.forEach($scope.rawMaterialByKitchen, function (raw, i) {
     table1 += "<tr>";
     table1 += "<td colspan='8'>" + raw.name + "</td>";
     table1 += "</tr>";
     _.forEach(raw.items, function (item, j) {
     table1 += "<tr>";
     table1 += "<td>" + j + "</td>"
     table1 += "<td>" + item.itemName + "</td>"
     table1 += "<td>" + item.calculatedQuantity + "</td>"
     table1 += "<td>" + item.baseUnitName + "</td>"
     table1 += "</tr>";
     });
     });
     print_Entry(table1);
     };*/

    $scope.makeTransferTransactionArray = function (kitchens) {
      console.log('kitchens', kitchens);

      var deferred = $q.defer();
      var transactions = [];
      var len = Object.keys(kitchens).length;
      console.log(2982, len);
      for (var prop in kitchens) {
        //transactions.push($scope.newReset_StockEntryAsync(OrderKitchenCombo[prop]));
        $scope.newReset_StockEntryAsync(kitchens[prop]).then(function (transaction) {
          transactions.push(transaction);
          console.log(transactions)
          if(transactions.length == len){
            deferred.resolve(transactions);
          }
        });
      }
      return deferred.promise;
    }

    $scope.newReset_StockEntry = function (reqOrd) {
      var r = angular.copy(reqOrd);
      //console.log("RRRRRRR", r)
      var mainStore;
      $scope.stockEntryForm = {
        isNew: true,
        enableSubmit: true,
        showBill: true,
        discountType: 'percent',
        isTemplateDelete: true,
        isTemplateSave: true,
        billDate: new Date(),
        maxDate: new Date()
      };
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, null, null);
      $scope.transaction.generateBillNumbers(5).then(function (snumbers) {
        //console.log("in bill", snumbers.billnumber)
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        //console.log("bill bumber", snumbers.billnumber)
        return
      });
      _.forEach($scope.stores, function (s, i) {
        //console.log("STORE", s)
        if (s.isMain) {
          mainStore = angular.copy(s);
          return;
        }
      });

      var mainStoreCopy = angular.copy(mainStore)

      _.forEach(r.items, function (itm, o) {
        //console.log("a REQUIREMENT ITEM", itm)
        _.forEach(itm.rawMaterialForItem, function (item, f) {
          //console.log("raw material for item reqi", item)
          _.forEachRight(mainStoreCopy.category, function (c, j) {
            _.forEachRight(c.item, function (k, l) {
              if (!k.qty)
                k.qty = parseFloat(0);
              //console.log("RAW ITEM MATCH WITH STORE", item )
              if (item._id == k._id) {
                k.toStore = {
                  toCategory: {categoryName: itm.fromCategory['categoryName'], _id: itm.fromCategory['_id']},
                  _id: itm.kitchenDetails['_id'],
                  storeName: itm.kitchenDetails['storeName']
                };
                k.itemType = "RawMaterial";
                k.qty += parseFloat(item.calculatedQuantity);
                k.selectedUnit = {
                  _id: item.selectedUnitId['_id'],
                  conversionFactor: item.selectedUnitId['conversionFactor'],
                  unitName: item.selectedUnitId['unitName']
                }
                //console.log("ASSIGNING QUANITTY TO " + k.itemName , angular.copy(k));
                k.itemMatched = true;
                c.categoryCheck = true;
                return
              }
            })
          })
        })
      });
      _.forEachRight(mainStoreCopy.category, function (c, j) {
        //console.log("cat,", c)
        if (!c.categoryCheck) {
          mainStoreCopy.category.splice(j, 1);
          //console.log("CATEGORY SPLICED", c)
        }
        else {
          _.forEachRight(c.item, function (it, lm) {
            //console.log("ITEMS CHCKING FOR SPLICING", it)
            if (!it.itemMatched) {
              //console.log("SPLICIE", it)
              c.item.splice(lm, 1);
            }
          })
        }
        c.items = angular.copy(c.item);
        delete c.item;
      });
      $scope.transaction._store = mainStoreCopy;
      $scope.stockEntryForm._store = mainStoreCopy;
      var itemsArray = [];
      var unitsArray = [];
      var selectedUnits = {};
      //console.log("CATEGORY ITEM IN TRANSACT",angular.copy(mainStoreCopy.category));
      //console.log("TRANSACTION", angular.copy($scope.transaction._store ));
      _.forEach($scope.transaction._store.category, function (c, j) {
        _.forEach(c.items, function (k, l) {
          //console.log("item", k)
          if (k.selectedUnit)
            k.preferedUnit = k.selectedUnit._id;
          var itemId = k._id;
          var str_ind = _.findIndex($scope.rawMaterialByKitchen, {_id: k.toStore._id});
          var ind = _.findIndex($scope.rawMaterialByKitchen[str_ind].items, {_id: k._id});
          k.qty = $scope.rawMaterialByKitchen[str_ind].items[ind].calculatedQuantity;
          itemsArray.push(k)
          selectedUnits[itemId] = k.selectedUnit;
        })
      })
      var returned = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsStockTransfer(itemsArray, selectedUnits);
      _.forEach($scope.transaction._store.category, function (c, j) {
        _.forEach(c.items, function (k, l) {
          _.forEach(returned, function (r, e) {
            if (r._id == k._id) {
              c.items[l] = r;
              return
            }
          })
        })
      })
      $scope.transaction.created = new Date();
      $scope.transaction.timeStamp = Date.parse($scope.transaction.created);
      StockResource.save({}, $scope.transaction, function (result) {
        //console.log("RESUMLT", result)
      });
      //console.log("final one", $scope.transaction)
    };

    //Temp Implementation
    $scope.newReset_StockEntryAsync = function (reqOrd) {
      var r = angular.copy(reqOrd);
      //console.log("RRRRRRR", r)
      var mainStore;
      $scope.stockEntryForm = {
        isNew: true,
        enableSubmit: true,
        showBill: true,
        discountType: 'percent',
        isTemplateDelete: true,
        isTemplateSave: true,
        billDate: new Date(),
        maxDate: new Date()
      };
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 5, null, null);
      _.forEach($scope.stores, function (s, i) {
        //console.log("STORE", s)
        if (s.isMain) {
          mainStore = angular.copy(s);
          return;
        }
      });

      $scope.transaction.generateBillNumbers(5).then(function (snumbers) {
        //console.log("in bill", snumbers.billnumber)
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
      });

      var mainStoreCopy = angular.copy(mainStore)

      _.forEach(r.items, function (itm, o) {
        //console.log("a REQUIREMENT ITEM", itm)
        _.forEach(itm.rawMaterialForItem, function (item, f) {
          //console.log("raw material for item reqi", item)
          _.forEachRight(mainStoreCopy.category, function (c, j) {
            _.forEachRight(c.item, function (k, l) {
              if (!k.qty)
                k.qty = parseFloat(0);
              //console.log("RAW ITEM MATCH WITH STORE", item )
              if (item._id == k._id) {
                k.toStore = {
                  toCategory: {categoryName: itm.fromCategory['categoryName'], _id: itm.fromCategory['_id']},
                  _id: itm.kitchenDetails['_id'],
                  storeName: itm.kitchenDetails['storeName']
                };
                k.itemType = "RawMaterial";
                k.qty += parseFloat(item.calculatedQuantity);
                k.selectedUnit = {
                  _id: item.selectedUnitId['_id'],
                  conversionFactor: item.selectedUnitId['conversionFactor'],
                  unitName: item.selectedUnitId['unitName']
                }
                //console.log("ASSIGNING QUANITTY TO " + k.itemName , angular.copy(k));
                k.itemMatched = true;
                c.categoryCheck = true;
                return
              }
            })
          })
        })
      });
      _.forEachRight(mainStoreCopy.category, function (c, j) {
        //console.log("cat,", c)
        if (!c.categoryCheck) {
          mainStoreCopy.category.splice(j, 1);
          //console.log("CATEGORY SPLICED", c)
        }
        else {
          _.forEachRight(c.item, function (it, lm) {
            //console.log("ITEMS CHCKING FOR SPLICING", it)
            if (!it.itemMatched) {
              //console.log("SPLICIE", it)
              c.item.splice(lm, 1);
            }
          })
        }
        c.items = angular.copy(c.item);
        delete c.item;
      });
      $scope.transaction._store = mainStoreCopy;
      $scope.stockEntryForm._store = mainStoreCopy;
      var itemsArray = [];
      var unitsArray = [];
      var selectedUnits = {};
      //console.log("CATEGORY ITEM IN TRANSACT",angular.copy(mainStoreCopy.category));
      //console.log("TRANSACTION", angular.copy($scope.transaction._store ));
      _.forEach($scope.transaction._store.category, function (c, j) {
        _.forEach(c.items, function (k, l) {
          //console.log("item", k)
          if (k.selectedUnit)
            k.preferedUnit = k.selectedUnit._id;
          var itemId = k._id;
          var str_ind = _.findIndex($scope.rawMaterialByKitchen, {_id: k.toStore._id});
          var ind = _.findIndex($scope.rawMaterialByKitchen[str_ind].items, {_id: k._id});
          k.qty = $scope.rawMaterialByKitchen[str_ind].items[ind].calculatedQuantity;
          itemsArray.push(k)
          selectedUnits[itemId] = k.selectedUnit;
        })
      })
      var returned = $scope.transaction.basePrefferedAnsSelectedUnitsByItemsStockTransfer(itemsArray, selectedUnits);
      _.forEach($scope.transaction._store.category, function (c, j) {
        _.forEach(c.items, function (k, l) {
          _.forEach(returned, function (r, e) {
            if (r._id == k._id) {
              c.items[l] = r;
              return
            }
          })
        })
      })
      $scope.transaction.created = new Date();
      $scope.transaction.timeStamp = Date.parse($scope.transaction.created);
      /*StockResource.save({}, $scope.transaction, function (result) {
       //console.log("RESUMLT", result)
       });*/
      //console.log("final one", $scope.transaction)
      return $scope.transaction;
    };

    //modified for the max limit data error
    /*$scope.submitReceiveMultipart = function (ReceiveForm) {
     $scope.indentGrnReceive.enableSubmit = true;
     //console.log('Receive form',ReceiveForm);
     //var ch=angular.copy(ReceiveForm);
     var ch = angular.copy(ReceiveForm);
     var nCh = {
     _id: ch._id
     }
     $scope.indentGrnReceiveForm = ch;
     ch.ReceiveDate = new Date().toISOString();
     nCh.ReceiveDate = new Date().toISOString();
     var _emailScope = $rootScope.$new();
     nCh.items = [];
     _.forEach(ch.items, function (itm) {
     var it = {
     _id: itm._id,
     receiveQty: itm.receiveQty,
     receiveComment: itm.receiveComment
     }
     nCh.items.push(it);
     });

     var table1 = "";
     table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid #000000;'>";
     table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
     table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
     table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Good Receipt Note <br /><br /></td></tr>";
     table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Asked Quantity</th><th>Supplied Quantity</th><th>Recieved Quantity</th></thead>";
     table1 += "<tbody>";
     //console.log(ch);

     $http({
     url: '/api/stockRequirements/' + nCh._id,
     method: 'put',
     data: nCh
     }).then(function (response) {
     //console.log('success');
     var result = response.data;
     var index = _.findIndex($scope.requirementReceive, {_id: result._id});
     if (index >= 0) {
     $scope.requirementReceive.splice(index, 1);
     }
     $scope.indentGrnReceiveForm.isNew = true;
     });
     receiveIndentStockTransaction(ch, table1, _emailScope);
     };

     //end

     function receiveIndentStockTransaction(ch, table1, _emailScope) {

     console.log(ch);
     var bills = formatDataForStockEntry(ch);//result;
     bills.transactionType = '12';
     bills._id = guid();
     //console.log(bills);
     $scope.resource.save({}, bills, function (bill) {
     growl.success("Stock Entry success!", {ttl: 3000});
     //console.log('Stock entry Success');
     $scope.rejectedGoods = [];
     $scope.acceptedGoods = [];
     //console.log("BEFORE CREATING TABLE", ch)
     _.forEach(ch.items, function (item, i) {
     table1 += "<tr>";
     table1 += "<td>" + item.itemName + "</td>";
     table1 += "<td>" + item.selectedUnit.unitName + "</td>";
     table1 += "<td>" + item.qty + "</td>"
     table1 += "<td>" + item.supplyQty + "</td>"
     table1 += "<td>" + item.receiveQty + "</td>"
     table1 += "</tr>";

     if (item.receiveQty < item.supplyQty) {
     item.reject = true;
     $scope.rejectedGoods.push(item);
     if (item.receiveQty > 0) {
     var itm = angular.copy(item);
     itm.reject = false;
     $scope.acceptedGoods.push(itm)
     }
     }
     else {
     item.reject = false;
     $scope.acceptedGoods.push(item)
     }
     });
     $scope.indentGrnReceiveForm.evaluated = true;
     //var tableCopy = angular.copy(table1);
     var table3;
     $timeout(function () {
     table3 = document.getElementById('indentRecieveToPrint').innerHTML;
     }, 0);
     var _message = $compile($('<div>' + table1 + '</div>'))(_emailScope);
     var _to = 'ajittechit@gmail.com'

     Email.send({}, {
     messageText: _message.text(),
     messageHtml: _message.html(),
     to: _to.toString()
     }, function (data) {
     $timeout(function () {
     table3 = document.getElementById('indentRecieveToPrint').innerHTML;
     //console.log('Receive Table');
     //console.log("DATA" + data)
     print_Entry(table3);
     }, 10);
     }, function (err) {
     //console.log("ERROR", err);
     });
     });
     }*/

    /*$scope.submitReceiveMultipart = function (ReceiveForm) {
     $scope.indentGrnReceive.enableSubmit = true;
     //console.log('Receive form',ReceiveForm);
     //var ch=angular.copy(ReceiveForm);
     var ch = angular.copy(ReceiveForm);
     var nCh = {
     _id: ch._id
     }
     $scope.indentGrnReceiveForm = ch;
     ch.ReceiveDate = new Date().toISOString();
     nCh.ReceiveDate = new Date().toISOString();
     var _emailScope = $rootScope.$new();
     nCh.items = [];
     _.forEach(ch.items, function (itm) {
     var it = {
     _id: itm._id,
     receiveQty: itm.receiveQty,
     receiveComment: itm.receiveComment
     }
     nCh.items.push(it);
     });

     var table1 = "";
     table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid #000000;'>";
     table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
     table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
     table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Good Receipt Note <br /><br /></td></tr>";
     table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Asked Quantity</th><th>Supplied Quantity</th><th>Recieved Quantity</th></thead>";
     table1 += "<tbody>";
     //console.log(ch);

     $http({
     url: '/api/stockRequirements/' + nCh._id,
     method: 'put',
     data: nCh
     }).then(function (response) {
     //console.log('success');
     var result = response.data;
     var index = _.findIndex($scope.requirementReceive, {_id: result._id});
     if (index >= 0) {
     $scope.requirementReceive.splice(index, 1);
     }
     $scope.indentGrnReceiveForm.isNew = true;
     var bills = formatDataForStockEntry(result);//result;
     bills.transactionType = '12';
     bills._id = guid();
     //console.log(bills);
     $scope.resource.save({}, bills, function (bill) {
     growl.success("Stock Entry success!", {ttl: 3000});
     //console.log('Stock entry Success');
     $scope.rejectedGoods = [];
     $scope.acceptedGoods = [];
     //console.log("BEFORE CREATING TABLE", ch)
     _.forEach(ch.items, function (item, i) {
     table1 += "<tr>";
     table1 += "<td>" + item.itemName + "</td>";
     table1 += "<td>" + item.selectedUnit.unitName + "</td>";
     table1 += "<td>" + item.qty + "</td>"
     table1 += "<td>" + item.supplyQty + "</td>"
     table1 += "<td>" + item.receiveQty + "</td>"
     table1 += "</tr>";

     if (item.receiveQty < item.supplyQty) {
     item.reject = true;
     $scope.rejectedGoods.push(item);
     if (item.receiveQty > 0) {
     var itm = angular.copy(item);
     itm.reject = false;
     $scope.acceptedGoods.push(itm)
     }
     }
     else {
     item.reject = false;
     $scope.acceptedGoods.push(item)
     }
     });
     $scope.indentGrnReceiveForm.evaluated = true;
     //var tableCopy = angular.copy(table1);
     var table3;
     $timeout(function () {
     table3 = document.getElementById('indentRecieveToPrint').innerHTML;
     }, 0);
     var _message = $compile($('<div>' + table1 + '</div>'))(_emailScope);
     var _to = 'ajittechit@gmail.com'

     Email.send({}, {
     messageText: _message.text(),
     messageHtml: _message.html(),
     to: _to.toString()
     }, function (data) {
     $timeout(function () {
     table3 = document.getElementById('indentRecieveToPrint').innerHTML;
     //console.log('Receive Table');
     //console.log("DATA" + data)
     print_Entry(table3);
     }, 10);
     }, function (err) {
     //console.log("ERROR", err);
     });
     });
     });
     //receiveIndentStockTransaction(ch, table1, _emailScope);
     };*/

    $scope.submitReceiveMultipart = function(ReceiveForm){
      $scope.indentGrnReceive.enableSubmit=true;
      //console.log('Receive form',ReceiveForm);
      //var ch=angular.copy(ReceiveForm);
      var ch=angular.copy(ReceiveForm);
      var nCh = {
        _id: ch._id
      }
      $scope.indentGrnReceiveForm = ch;
      ch.ReceiveDate=new Date().toISOString();
      nCh.ReceiveDate=new Date().toISOString();
      var _emailScope = $rootScope.$new();
      nCh.items = [];
      _.forEach(ch.items, function (itm) {
        var it = {
          _id: itm._id,
          receiveQty: itm.receiveQty,
          receiveComment: itm.receiveComment
        }
        nCh.items.push(it);
      });

      var table1="";
      table1+="<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid #000000;'>";
      table1+="<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
      table1+="<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
      table1+="<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Good Receipt Note <br /><br /></td></tr>";
      table1+="<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Asked Quantity</th><th>Supplied Quantity</th><th>Recieved Quantity</th></thead>";
      table1+="<tbody>";
      console.log(ch);
      console.log(nCh);
      $http({
        url: '/api/stockRequirements/' + nCh._id,
        method: 'put',
        data: nCh
      }).then(function(response){
        //console.log('success');
        var indexReq = _.findIndex(angular.copy($scope.req.receiveIndents), {_id: nCh._id});
        $scope.req.receiveIndents.splice(indexReq,1);
        
        var result = response.data;
        var index=_.findIndex($scope.requirementReceive,{_id:result._id});
        if(index>=0){
          $scope.requirementReceive.splice(index,1);
          }
        $scope.indentGrnReceiveForm.isNew=true;

        });

      createReceiveTransaction(ch, table1, _emailScope);
    };

    function createReceiveTransaction (ch, table1, _emailScope) {

      _.forEach(ch.items,function(it){
        it.availableQtyInBase = it.availableQtyBaseKitchen;
        it.closingQtyInBase = it.closingQtyBaseKitchen;
        it = _.omit(it, ['closingQtyBaseKitchen', 'availableQtyBaseKitchen']);
      });

      ch.deployment_id=localStorageService.get('deployment_id');
      ch.tenant_id=localStorageService.get('tenant_id');
      ch.fromDeployment_id=localStorageService.get('deployment_id');

      var bills=formatDataForStockEntry(ch);//result;

      bills.transactionType = '12';
      bills._id=guid();
      bills.created=new Date();
      //console.log(bills);
      $scope.resource.save({}, bills, function (bill) {
        growl.success("Stock Entry success!", {ttl:3000});
        //console.log('Stock entry Success');
        $scope.rejectedGoods=[];
        $scope.acceptedGoods=[];
        //console.log("BEFORE CREATING TABLE", ch)
        _.forEach(ch.items, function(item,i){
          table1+="<tr>";
          table1+="<td>"+item.itemName+"</td>";
          table1+="<td>"+item.selectedUnit.unitName+"</td>";
          table1+="<td>"+item.qty+"</td>"
          table1+="<td>" + item.supplyQty+"</td>"
          table1+="<td>" + item.receiveQty+"</td>"
          table1+="</tr>";

          if(item.receiveQty < item.supplyQty){
            item.reject = true;
            $scope.rejectedGoods.push(item);
            if(item.receiveQty>0){
              var itm=angular.copy(item);
              itm.reject = false;
              $scope.acceptedGoods.push(itm)
            }
          }
          else{
            item.reject = false;
            $scope.acceptedGoods.push(item)
          }
        });
        $scope.indentGrnReceiveForm.evaluated = true;
        //var tableCopy = angular.copy(table1);
        var table3;
        $timeout(function() {
          table3=  document.getElementById('indentRecieveToPrint').innerHTML;
        }, 0);
        var _message = $compile($('<div>' + table1+ '</div>'))(_emailScope);
        var _to ='ajittechit@gmail.com'

        Email.send({}, {
          messageText: _message.text(),
          messageHtml: _message.html(),
          to: _to.toString()
        }, function (data) {
          $timeout(function() {
            table3=  document.getElementById('indentRecieveToPrint').innerHTML;
            //console.log('Receive Table');
            //console.log("DATA" + data)
            print_Entry(table3);
          }, 10);
        }, function (err) {
          //console.log("ERROR", err);
        });
      });
    };

    /*$scope.submitReceive = function (ReceiveForm) {
     $scope.indentGrnReceive.enableSubmit = true
     //var ch=angular.copy(ReceiveForm);
     var ch = ReceiveForm;
     $scope.indentGrnReceiveForm = ch;
     ch.ReceiveDate = new Date().toISOString();
     var _emailScope = $rootScope.$new();

     var table1 = "";
     table1 += "<table id='table' runat='server' style='width:100%; border-right:2px solid black; border-left:2px solid black; border-top:2px solid black;border-bottom:2px solid black;'>";
     table1 += "<tr><td colspan='9'></td><td style='width:250px; text-align:right'></td></tr>";
     table1 += "<tr><td colspan='10' style='border-bottom:1px solid black'></td></tr>";
     table1 += "<tr><td colspan='10' style='text-align:center; font-size:x-large; font-weight:bold; text-decoration:underline'>Good Receipt Note <br /><br /></td></tr>";
     table1 += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0'><thead style='background-color:#CFFFE9;'><th>Item Name</th><th>Unit</th><th>Asked Quantity</th><th>Supplied Quantity</th><th>Recieved Quantity</th></thead>";
     table1 += "<tbody>";
     //console.log(ch)
     stockRequirement.update({}, ch, function (result) {
     var index = _.findIndex($scope.requirementReceive, {_id: result._id});
     if (index >= 0) {
     $scope.requirementReceive.splice(index, 1);
     }
     $scope.indentGrnReceiveForm.isNew = true;

     var bills = formatDataForStockEntry(result);//result;
     bills.transactionType = '12';
     bills._id = guid();
     $scope.resource.save({}, bills, function (bill) {
     growl.success("Stock Entry success!", {ttl: 3000});
     $scope.rejectedGoods = [];
     $scope.acceptedGoods = [];
     //console.log("BEFORE CREATING TABLE", ch)
     _.forEach(ch.items, function (item, i) {
     table1 += "<tr>";
     table1 += "<td>" + item.itemName + "</td>";
     table1 += "<td>" + item.selectedUnit.unitName + "</td>";
     table1 += "<td>" + item.qty + "</td>"
     table1 += "<td>" + item.supplyQty + "</td>"
     table1 += "<td>" + item.receiveQty + "</td>"
     table1 += "</tr>";

     if (item.receiveQty < item.supplyQty) {
     item.reject = true;
     $scope.rejectedGoods.push(item);
     if (item.receiveQty > 0) {
     var itm = angular.copy(item);
     itm.reject = false;
     $scope.acceptedGoods.push(itm)
     }
     }
     else {
     item.reject = false;
     $scope.acceptedGoods.push(item)
     }
     });
     $scope.indentGrnReceiveForm.evaluated = true;
     //var tableCopy = angular.copy(table1);
     var table3;
     $timeout(function () {
     table3 = document.getElementById('indentRecieveToPrint').innerHTML;
     }, 0);
     var _message = $compile($('<div>' + table1 + '</div>'))(_emailScope);
     var _to = 'ajittechit@gmail.com'

     Email.send({}, {
     messageText: _message.text(),
     messageHtml: _message.html(),
     to: _to.toString()
     }, function (data) {
     //console.log("DATA" + data)
     print_Entry(table3);
     }, function (err) {
     //console.log("ERROR", err);
     });
     });
     });
     };*/

    function getAllKitchenStores() {
      var str = [];
      _.forEach($scope.stores, function (s, i) {
        if (s.isKitchen == true) {
          str.push({_id: s._id, storeName: s.storeName});
        }
      });
      return str;
    };

    function createDeliveryChallan_StockTransaction(result) {
      var kitchenStores = getAllKitchenStores();
      var str = [];
      _.forEach($scope.stores, function (s, i) {
        var index = _.findIndex(kitchenStores, {_id: s._id});
        if (index >= 0) {
          _.forEach(s.processedFoodCategory, function (cat, ii) {
            _.forEach(cat.item, function (itm, iii) {
              var itmIndex = _.findIndex(result.items, {_id: itm._id});
              if (itmIndex >= 0) {
                //find recipe
                var qty = result.items[itmIndex].supplyQty;
                var rIndex = _.findIndex($scope.stockRecipes, {itemId: itm._id});
                if (rIndex >= 0) {
                  $scope.stockRecipes[rIndex].itemType = "MenuItem";
                  $scope.stockRecipes[rIndex].qty = qty;
                  $scope.stockRecipes[rIndex].itemName = $scope.stockRecipes[rIndex].recipeName;
                  $scope.stockRecipes[rIndex].selectedUnit = $scope.stockRecipes[rIndex].selectedUnitId;
                  var chkStr = _.findIndex(str, {_id: s._id});
                  if (chkStr < 0) {
                    var obj = {_id: s._id, storeName: s.storeName, category: []};
                    var objCat = {_id: cat._id, categoryName: cat.categoryName, items: []};
                    objCat.items.push($scope.stockRecipes[rIndex]);
                    obj.category.push(objCat);
                    str.push(obj);
                  }
                  else {
                    var chkCat = _.findIndex(str[chkStr].category, {itemId: itm._id});
                    if (chkCat < 0) {
                      var objCat = {_id: cat._id, categoryName: cat.categoryName, items: []};
                      objCat.items.push($scope.stockRecipes[rIndex]);
                      str[chkStr].category.push(objCat);
                    }
                    else {
                      str[chkStr].category[chkCat].items.push($scope.stockRecipes[rIndex]);
                    }
                  }
                  //console.log('str', str);
                }
              }
            });
          });
        }
      });
      //console.log("STR",str);
      //console.log("RESULT",result);
      sendChallanDataToSave(str);
    };

    function formatStoreToSave(store, items) {
      _.forEach(store.category, function (c, i) {
        _.forEach(c.items, function (itm, ii) {
          var index = _.findIndex(items, {_id: itm._id});
          if (index >= 0) {
            c.items[ii] = items[index];
          }
        });
      });
      return store;
    };

    function sendChallanDataToSave(str) {
      //console.log('transaction', str);
      _.forEach(str, function (s, i) {
        var items = [];
        var selectedUnits = {};
        $scope.transaction = new StockTransaction(currentUser, null, null, null, null, null, null, 14, null, null)
        _.forEach(s.category, function (c, ii) {
          _.forEach(c.items, function (itm, iii) {
            var rd = $scope.transaction.calculateForRecipeDetails(itm);
            itm = rd;
            items.push(itm);
            selectedUnits[itm._id] = itm.selectedUnit;
          });
        });
        var citems = $scope.transaction.basePrefferedAnsSelectedUnitsByItems(items, selectedUnits);
        var ss = formatStoreToSave(s, citems);
        //console.log("CITEMS",citems);
        $scope.transaction._store = ss;
        //console.log("storeToSave",ss);
        $scope.transaction.created = new Date();
        $scope.transaction.timeStamp = Date.parse($scope.transaction.created);
        //save
        $scope.resource.saveData($scope.transaction, function (r) {
          //console.log("RESULT",r);
        });
      });
    };

     $scope.printSingleChallan = function() {
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('deliveryChallanTable').innerHTML));
     }

     $scope.printConsolidatedChallan = function() {
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById('deliveryConsolidatedTable').innerHTML));
     }

  }]);
