//'use strict';
//
//angular.module('posistApp')
//  .controller('StockSettingCtrl',['$scope','currentUser','Deployment','property', function ($scope,currentUser,Deployment,property) {
//    $scope.deployment={};
//
//    var getDeployment=Deployment.get({tenant_id:currentUser.tenant_id}, function (dep){
//        $scope.deployment=dep;
//
//    });
//    property.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
//           $scope.settings=result;
//           bindSettings();
//           console.log(result);
//        });
//
//    //});
//    property.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
//      $scope.settings=result;
//      bindSettings();
//      console.log(result);
//    });
//
//    $scope.userType=[
//      {id:"1", type:"DeliveryBoy", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"2", type:"Waiters", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"3", type:"StoreUser", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"4", type:"StoreManager", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"5", type:"Admins", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"6", type:"Managers", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"7", type:"Supervisor", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"8", type:"StoreSupervisor", isShowPricing:"true" ,isEditPricing:"true"},
//      {id:"9", type:"Cashier", isShowPricing:"true" ,isEditPricing:"true"}
//    ];
//
//    //-------------------------------Setting------------------------
//    $scope.settings=[];
//    $scope.stockSettingForm={};
//
//    $scope.cancelSettingForm= function(){
//      bindSettings();
//    };
//
//    //bind to setting form if data present
//    function bindSettings(){
//      if($scope.settings.length!=0){
//        $scope.stockSettingForm=angular.copy($scope.settings[0]);
//        $scope.stockPropertyForm=$scope.stockSettingForm;
//        bindProperty();
//        //$scope.stockPropertyForm.isEditPricing=$scope.stockSettingForm.isEditPricing;
//      }
//    }
//
//    $scope.submitSettings= function(formSetting){
//      if(formSetting.$valid) //&& !$scope.stockSettingForm.isEdit)
//      {
//        if($scope.settings.length!=0)
//        {
//          //update
//          property.update({},$scope.stockSettingForm, function (result){
//            var index=_.findIndex($scope.settings,{_id:result._id});
//            $scope.settings.splice(index,1);
//            $scope.settings.push(result);
//            bindSettings();
//          });
//        }
//        else
//        {
//          //insert
//          $scope.stockSettingForm.tenant_id=currentUser.tenant_id;
//          $scope.stockSettingForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
//          property.saveData({},$scope.stockSettingForm, function (result){
//            $scope.settings.push(result);
//            bindSettings();
//          });
//        }
//      }
//    };
//
//    function bindProperty(){
//        if($scope.stockPropertyForm.property!=undefined){
//            $scope.stockPropertyForm.userShowPricing=angular.copy($scope.stockPropertyForm.property.showPricing);
//        }
//      if($scope.stockPropertyForm.property!=undefined){
//            $scope.stockPropertyForm.userEditPricing=angular.copy($scope.stockPropertyForm.property.editPricing);
//        }
//    };
//
//    function bindProperty(){
//      if($scope.stockPropertyForm.property!=undefined){
//        $scope.stockPropertyForm.userShowPricing=angular.copy($scope.stockPropertyForm.property.showPricing);
//      }
//      if($scope.stockPropertyForm.property!=undefined){
//        $scope.stockPropertyForm.userEditPricing=angular.copy($scope.stockPropertyForm.property.editPricing);
//      }
//    };
//
//    function addShowAndEditInProperty() {
//      $scope.stockPropertyForm.property = {};
//      $scope.stockPropertyForm.property.showPricing = ($scope.stockPropertyForm.userShowPricing);
//      $scope.stockPropertyForm.property.editPricing = ($scope.stockPropertyForm.userEditPricing);
//      if (stockPropertyForm.isShowPricing)
//        $scope.stockPropertyForm.property.showPricingEnabled = true;
//      else
//        $scope.stockPropertyForm.property.showPricingEnabled = false;
//    };
//
//    $scope.submitProperty = function(formProperty){
//      if(formProperty.$valid){
//        if($scope.settings.length!=0){
//          addShowAndEditInProperty();
//          property.update({},$scope.stockPropertyForm, function (result){
//            var index=_.findIndex($scope.settings,{_id:result._id});
//            $scope.settings.splice(index,1);
//            $scope.settings.push(result);
//            bindSettings();
//          });
//        }
//        else
//        {
//          $scope.stockPropertyForm.tenant_id=currentUser.tenant_id;
//          $scope.stockPropertyForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
//          property.saveData({},$scope.stockPropertyForm, function (result){
//            $scope.settings.push(result);
//            bindSettings();
//          });
//        }
//      }
//    };
//  }]);

'use strict';

angular.module('posistApp')
  .controller('StockSettingCtrl',['$scope','currentUser','Deployment','property','Role','stockReportResetTime', 'growl', function ($scope,currentUser,Deployment,property,Role,stockReportResetTime, growl) {
    $scope.deployment={};
    $scope.stockPropertyForm = {};
    $scope.stockPropertyForm.userHidePricing={};
    $scope.stockSettingForm = {};
    var getDeployment=Deployment.get({tenant_id:currentUser.tenant_id}, function (dep){

      $scope.deployment=dep;

    });
    property.get({tenant_id:currentUser.tenant_id,deployment_id:$scope.currentUser.deployment_id}).$promise.then( function (result){
      $scope.settings=result;
      bindSettings();
      console.log(result);
    });
    $scope.userType=[];
    $scope.user=currentUser;
    Role.get({tenant_id:currentUser.tenant_id}).$promise.then( function (result){
      $scope.r=result;

      console.log(result);
      _.forEach($scope.r, function (roles) {
          $scope.userType.push({id:roles._id,type:roles.name, isHidePricing:"true" ,isEditPricing:"true"});
        }
      );
      //console.log($scope.userType);
    });
    var fromDate=new Date("11/23/2016")
    var endDate=new Date("12/21/2016")
    var isKitchen=true;
stockReportResetTime.getConsumptionSummaryFoodCosting({fromDate:fromDate,endDate:endDate,deployment_id:currentUser.deployment_id,tenant_id:currentUser.tenant_id,isKitchen:isKitchen, storeId: "5801ec82122f58800cef3661"}).$promise.then(function (result){
 console.log('getConsumptionSummaryFoodCosting',result);
});
   /*var fromDate=new Date("11/08/2016")
    var isKitchen=true;
stockReportResetTime.vendorDataForCockpit({date:fromDate,deployment_id:currentUser.deployment_id,isKitchen:isKitchen, storeId: "5801ec82122f58800cef3661"}).$promise.then(function (result){   console.log('result',result);
console.log('result',result);
});*/
    //$scope.userType=[
    //  {id:"1", type:"DeliveryBoy", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"2", type:"Waiters", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"3", type:"StoreUser", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"4", type:"StoreManager", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"5", type:"Admins", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"6", type:"Managers", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"7", type:"Supervisor", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"8", type:"StoreSupervisor", isShowPricing:"true" ,isEditPricing:"true"},
    //  {id:"9", type:"Cashier", isShowPricing:"true" ,isEditPricing:"true"}
    //];

    //-------------------------------Setting------------------------
    $scope.settings=[];
    $scope.stockSettingForm={};

    $scope.cancelSettingForm= function(){
      bindSettings();
    };

    //bind to setting form if data present
    function bindSettings(){
      if($scope.settings.length!=0){
        $scope.stockSettingForm=angular.copy($scope.settings[0]);
        $scope.stockPropertyForm=$scope.stockSettingForm;
        if(!$scope.stockPropertyForm.property)
        {
          $scope.stockPropertyForm.property={};
        }
        bindProperty();
        //$scope.stockPropertyForm.isEditPricing=$scope.stockSettingForm.isEditPricing;
      }
      else
      {
        $scope.stockSettingForm = {};
        if(!$scope.stockPropertyForm.property)
        {
          $scope.stockPropertyForm.property={};
        }
        bindProperty();
      }
    }

    $scope.submitSettings= function(formSetting){
      if(formSetting.$valid) //&& !$scope.stockSettingForm.isEdit)
      {
        if($scope.settings.length!=0)
        {
          console.log($scope.stockSettingForm);
          //update
          property.update({},$scope.stockSettingForm, function (result){
            var index=_.findIndex($scope.settings,{_id:result._id});
            $scope.settings.splice(index,1);
            $scope.settings.push(result);
            bindSettings();
            growl.success('Settings Created.', {ttl:3000});
          });
        }
        else
        {
          console.log($scope.stockSettingForm);
          //insert
          $scope.stockSettingForm.tenant_id=currentUser.tenant_id;
          $scope.stockSettingForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
          property.saveData({},$scope.stockSettingForm, function (result){
            $scope.settings.push(result);
            bindSettings();
            growl.success('Settings Updated.', {ttl:3000});
          });
        }
      }
    };

    /*function bindProperty(){
      if($scope.stockPropertyForm.property!=undefined){
        $scope.stockPropertyForm.userHidePricing=angular.copy($scope.stockPropertyForm.property.hidePricing);

      }
      else
      {
        //insert
        $scope.stockSettingForm.tenant_id=currentUser.tenant_id;
        $scope.stockSettingForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        property.saveData({},$scope.stockSettingForm, function (result){
          $scope.settings.push(result);
          bindSettings();
        });
      }


      $scope.stockPropertyForm.resetSerialNumber=angular.copy($scope.stockPropertyForm.resetSerialNumber);

    };*/

    function bindProperty(){
      console.log('in here');
      
      if($scope.stockPropertyForm.property.hidePricing!=undefined){
        $scope.stockPropertyForm.userHidePricing=angular.copy($scope.stockPropertyForm.property.hidePricing);
      }
      else
      {
        $scope.stockPropertyForm.isHidePricing=false;
        $scope.stockPropertyForm.userHidePricing={};
        _.forEach($scope.userType,function(u,i)
        {
          var type=u.type;
          $scope.stockPropertyForm.userHidePricing[type]=false;
         
        });
        console.log('$scope.stockPropertyForm.userHidePricing',$scope.stockPropertyForm);
      }
      if($scope.stockPropertyForm.property.editPricing!=undefined){
        $scope.stockPropertyForm.userEditPricing=angular.copy($scope.stockPropertyForm.property.editPricing);
      }
      else
      {
        console.log('in 283')
        $scope.stockPropertyForm.isEditPricing=false;
        $scope.stockPropertyForm.userEditPricing={};
        _.forEach($scope.userType,function(u,i)
        {
          var type=u.type;
          $scope.stockPropertyForm.userEditPricing[type]=false;
        });
      }

      $scope.stockPropertyForm.resetSerialNumber=angular.copy($scope.stockPropertyForm.resetSerialNumber);
      
      
    };

    function addShowAndEditInProperty(){
      $scope.stockPropertyForm.property={};
      console.log('1',$scope.stockPropertyForm)
      console.log('2',$scope.stockPropertyForm.userHidePricing)
      console.log('3',$scope.stockPropertyForm.userEditPricing)
      $scope.stockPropertyForm.property.hidePricing=($scope.stockPropertyForm.userHidePricing);
      $scope.stockPropertyForm.property.editPricing=($scope.stockPropertyForm.userEditPricing);
      $scope.stockPropertyForm.resetSerialNumber=($scope.stockPropertyForm.resetSerialNumber);
      if ($scope.stockPropertyForm.isHidePricing)
        $scope.stockPropertyForm.property.hidePricingEnabled = true;
      else {
        $scope.stockPropertyForm.property.HidePricingEnabled = false;
        _.forEach($scope.userType, function (value){
          var type= value.type;
          $scope.stockPropertyForm.userHidePricing[type] = false;
        });
      }
        if ($scope.stockPropertyForm.isEditPricing)
        $scope.stockPropertyForm.property.editPricingEnabled = true;
      else {
        $scope.stockPropertyForm.property.editPricingEnabled = false;
        _.forEach($scope.userType, function (value){
          var type= value.type;
          $scope.stockPropertyForm.userEditPricing[type] = false;
        });
      }
      //$scope.stockPropertyForm.property={};
      //if(!$scope.stockPropertyForm.isShowPricing)
      //{
      //  console.log('inside else');
      //  $scope.stockPropertyForm.property.enablePricing=false;
      //  //$scope.deployment[0]._id;
      //} else {
      //  $scope.stockPropertyForm.property.showPricing = ($scope.stockPropertyForm.userShowPricing);
      //  $scope.stockPropertyForm.property.editPricing = ($scope.stockPropertyForm.userEditPricing);
      //}
      //property.saveData({},$scope.stockPropertyForm, function (result){
      //  $scope.settings.push(result);
      //  bindSettings();
      //});
    };

    $scope.submitProperty = function(formProperty){
      if(formProperty.$valid){
        if($scope.settings.length!=0){
          addShowAndEditInProperty();
          property.update({},$scope.stockPropertyForm, function (result){
            var index=_.findIndex($scope.settings,{_id:result._id});
            $scope.settings.splice(index,1);
            $scope.settings.push(result);
            bindSettings();
            growl.success('Stock Settings Updated.',{ttl:3000});
          });
        }
        else
        {
          addShowAndEditInProperty();
          $scope.stockPropertyForm.tenant_id=currentUser.tenant_id;
          $scope.stockPropertyForm.deployment_id=$scope.currentUser.deployment_id;//$scope.deployment[0]._id;
          property.saveData({},$scope.stockPropertyForm, function (result){
            $scope.settings.push(result);
            bindSettings();
            growl.success('Stock Settings Updated.',{ttl:3000});
          });
        }
      }
      else
      {
        growl.error('Please select a setting.',{ttl:3000});
      }
    };
  }]);
