'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('stockSetting', {
        url: '/stockSetting',
        templateUrl: 'app/stock/stockSetting/stockSetting.html',
        controller: 'StockSettingCtrl',
        resolve: {
                    currentUser: ['$state', '$stateParams', 'Auth',
                        function ($state, $stateParams, Auth) {
                            return Auth.getCurrentUser().$promise.then(function (user) {
                                return user;
                            });
                        }]
                }
      });
  });