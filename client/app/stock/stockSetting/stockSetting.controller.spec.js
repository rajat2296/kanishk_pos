'use strict';

describe('Controller: StockSettingCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StockSettingCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StockSettingCtrl = $controller('StockSettingCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
