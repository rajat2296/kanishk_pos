'use strict';

describe('Controller: PurchaseOrderCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var PurchaseOrderCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PurchaseOrderCtrl = $controller('PurchaseOrderCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
