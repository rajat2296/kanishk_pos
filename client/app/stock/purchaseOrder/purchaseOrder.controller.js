'use strict';

angular.module('posistApp')
    .controller('PurchaseOrderCtrl', ['$scope', '$timeout', 'property', 'localStorageService', 'purchaseNumber', 'purchaseOrder', 'currentUser', 'growl', 'Deployment', '$state', 'stockUnit', 'Utils', 'stockItem', function($scope, $timeout, property, localStorageService, purchaseNumber, purchaseOrder, currentUser, growl, Deployment, $state, stockUnit, Utils, stockItem) {

        stockUnit.get({ tenant_id: localStorageService.get('tenant_id'), deployment_id: localStorageService.get('deployment_id') }, function(units) {
            $scope.units = units;
        });

        property.get({ deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id') }, function(prop) {
            $scope.setting = prop[0];
        });

        stockItem.get({
            tenant_id: localStorageService.get('tenant_id'),
            deployment_id: localStorageService.get('deployment_id')
        }, function success(items) {
            $scope.items = items;
        }, function error(err) {
            growl.error('Some Error Occured', { ttl: 3000 });
        });

        var currentDeployment = null;
        Deployment.getCurrentDeploymentDetails({ deployment_id: localStorageService.get('deployment_id') }, function(depl) {
            currentDeployment = depl;
            console.log(depl);
        });
         $scope.purchaseOrderForm = {
            chargeName: '',
            chargeValue: 0,
            type: 'percent',
            operationType : 'additive',
            isEditable : false
        };
        //-------------------------------------------Generate Purchase Order-----------------------------------------------------
        var poNumber = null;
        var daySerialNumber = null;
        console.log(purchaseNumber.data);
        if (purchaseNumber.data) {
             console.log("purchase ifff")
            var poNumber = purchaseNumber.data.poNumber + 1;
            var daySerialNumber = null;
            var tDate = new Date();
            tDate.setHours(0, 0, 0, 0);
            if (new Date(purchaseNumber.data.created) < new Date(tDate)) {
                daySerialNumber = 1;
            } else {
                daySerialNumber = purchaseNumber.data.daySerialNumber + 1;
            }
              $scope.purchaseOrderForm = {
                    discountType:"percent",
                    poNumber: poNumber,
                    daySerialNumber: daySerialNumber,
                    billDate: new Date(),
                    availableVendors: [],
                    availableCategories: [],
                    selectedCategories: [],
                    itemsAvailableForSearch: [],
                    itemsByCategories: [],
                    enableSubmit: true
                };
                console.log("PO FORM",$scope.purchaseOrderForm)
        } else {
            console.log("purchase else")
            purchaseOrder.getLastPurchaseOrderNumber({ deployment_id: localStorageService.get('deployment_id') }, function(poN) {
                if (poN.data) {
                    var poNumber = poN.data.poNumber + 1;
                    var daySerialNumber = null;
                    var tDate = new Date();
                    tDate.setHours(0, 0, 0, 0);
                    if (new Date(poN.data.created) < new Date(tDate)) {
                        daySerialNumber = 1;
                    } else {
                        daySerialNumber = poN.data.daySerialNumber + 1;
                    }
                } else {
                    poNumber = 1;
                    daySerialNumber = 1;
                }
                $scope.purchaseOrderForm = {
                    discountType:"percent",
                    poNumber: poNumber,
                    daySerialNumber: daySerialNumber,
                    billDate: new Date(),
                    availableVendors: [],
                    availableCategories: [],
                    selectedCategories: [],
                    itemsAvailableForSearch: [],
                    itemsByCategories: [],
                    enableSubmit: true
                };
                console.log($scope.purchaseOrderForm)
            }, function error(err) {

                poNumber = 1;
                daySerialNumber = 1;
                $scope.purchaseOrderForm = {
                    discountType:"percent",
                    poNumber: poNumber,
                    daySerialNumber: daySerialNumber,
                    billDate: new Date(),
                    availableVendors: [],
                    availableCategories: [],
                    selectedCategories: [],
                    itemsAvailableForSearch: [],
                    itemsByCategories: [],
                    enableSubmit: true
                };
            });
        }

        $scope.purchaseOrderForm = {
            discountType:"percent",
            poNumber: poNumber,
            daySerialNumber: daySerialNumber,
            billDate: new Date(),
            availableVendors: [],
            availableCategories: [],
            selectedCategories: [],
            itemsAvailableForSearch: [],
            itemsByCategories: [],
            enableSubmit: true
        };

        $scope.stores = _.uniq($scope.stores, '_id');
        $scope.vendors = _.uniq($scope.vendors, '_id');
        console.log($scope.vendors);

        //functions
        console.log('$scope.stockItems', $scope.items);
        $scope.bindVendors = function(form) {
            form.availableVendors = [];
            form.availableCategories = [];
            form.itemsByCategories = [];
            form.selectedCategories = [];
            form.itemsAvailableForSearch = [];
            _.forEach($scope.vendors, function(vendor) {
                console.log(vendor);
                if (vendor.pricing._id == form.store._id && vendor.type != 'HO Vendor')
                    form.availableVendors.push(vendor);
            });
        }

        $scope.checkChange = function(model) {
            if (model.isChecked)
                model.isChecked = false;
            else
                model.isChecked = true;
        }

        $scope.bindCategories = function(form) {

            form.availableCategories = [];
            form.selectedCategories = [];
            form.itemsAvailableForSearch = [];
            form.itemsByCategories = [];

            if (form.vendor.email) {
                _.forEach(angular.copy(form.vendor.pricing.category), function(category) {
                    form.availableCategories.push(category);
                    _.forEach(category.item, function(item) {
                        item.category = {
                            _id: category._id,
                            categoryName: category.categoryName
                        }
                        if (item.selectedUnitId) {
                            if (item.selectedUnitId._id) {
                                item.selectedUnit = _.find(item.units, function(unit) {
                                    return unit._id == item.selectedUnitId._id;
                                });
                                item.preUnit = item.selectedUnit;
                            }
                        }
                        else{
                            if(item.preferedUnit!=undefined){
                                if(item.preferedUnit){
                                    item.selectedUnit = _.find(item.units, function(unit) {
                                    return unit._id == item.preferedUnit
                                     });
                                    item.preUnit = item.selectedUnit;
                                }
                            }
                        }
                        var itemIndex = _.findIndex($scope.items, function(itm) {
                            return item.itemName == itm.itemName;
                        });
                        item.receiveQty=0
                        if (itemIndex < 0)
                            item.itemCode = '';
                        else {
                           if (!$scope.items[itemIndex].itemCode)
                                item.itemCode = '';
                            else
                                item.itemCode = $scope.items[itemIndex].itemCode;
                        }
                        //console.log('item',item);
                        form.itemsAvailableForSearch.push(item);
                    });
                });
            } else {
                growl.error("Please update vendor email before placing an order!", { ttl: 3000 });
            }
        }

        $scope.getSearchResultsForEntries = function(searchField, transactionType) {
            var result = [];
            if (transactionType == 'Purchase Order') {
                _.forEach($scope.purchaseOrderForm.itemsAvailableForSearch, function(item, i) {
                    if (_.contains(item.itemCode.toLowerCase(), searchField.toLowerCase()) || _.contains(item.itemName.toLowerCase(), searchField.toLowerCase()))
                        result.push(item);
                        if(item.qty!= undefined || item.price !=undefined || item.selectedUnit!= undefined )
                            $scope.purchaseOrderForm.enableSubmit = false
                });
            }
            return result;
        }

         $scope.searchDropdown = function(name, code) {
          if(code == '' || code ==null || code == undefined)
            return name;
          else
            return code + ' - ' + name;
        }

        $scope.deleteSelectedItem = function(form, item) {
            _.remove(form.itemsByCategories, function(it) {
                return it._id == item._id;
            });
            form.itemsAvailableForSearch.push(item);
        };

        $scope.addItemManually = function(form, item) {
            console.log("form",form)
            console.log(item);
            _.remove(form.itemsAvailableForSearch, function(it) {
                return it._id == item._id;
            });
            form.itemsByCategories.push(item);
            
            form.itemToAdd = null;
            console.log("$scope.purchaseOrderForm",$scope.purchaseOrderForm)
             var index = _.findIndex($scope.purchaseOrderForm.availableCategories, function(cat) {
                            console.log("cat",cat)
                            return cat._id == item.category._id;
                        });
             console.log("index",index)
             if(index >= 0){
                console.log("index found")
                form.availableCategories[index].isChecked=true
               
             }
            console.log("$scope.purchaseOrderForm",$scope.purchaseOrderForm)
        }

        $scope.goNext = function(nextIdx) {
            var f = angular.element(document.querySelector('#f_' + nextIdx));
            if (f.length != 0) {
                f.focus();
            } else {
                // $scope.stockEntryForm.enableSubmit=false;
                // angular.element(document.querySelector('#submitEntry')).focus();
            }
            $scope.purchaseOrderForm.isOpen = true;
        };

        $scope.changeDataForUnit = function(item) {
            console.log(angular.copy(item));
            if (item.preUnit) {
                var p = item.price;
                if (item.preUnit.baseConversionFactor)
                    var cf = item.preUnit.baseConversionFactor / item.selectedUnit.conversionFactor;
                else {
                    var bf = _.find($scope.units, function(unit) {
                        return unit._id == item.preUnit._id;
                    }).baseConversionFactor;
                    var cf = bf / item.selectedUnit.conversionFactor;
                }
                item.price = Utils.roundNumber(p / cf, 2);
            }
            item.preUnit = item.selectedUnit;
        }

        $scope.bindItems = function(form, category) {

            $scope.checkChange(category);
            console.log("category",category)
            // $timeout(function() {
                if (category.isChecked) {
                    console.log("checked")
                    _.forEach(category.item, function(item) {
                        form.itemsByCategories.push(item);
                        _.remove(form.itemsAvailableForSearch, function(it) {
                            return it._id == item._id;
                        });
                    });
                } else {
                    _.forEach(category.item, function(item) {
                        console.log("category Not Checked")
                        _.remove(form.itemsByCategories, function(it) {
                            return it._id == item._id;
                        });
                        form.itemsAvailableForSearch.push(item);
                    });
                }
            // }, 10)
            console.log("forn",form)
            console.log("$scope.purchaseOrderForm",$scope.purchaseOrderForm)
        }

        $scope.validateEachRow = function() {
            var flag = true;
            _.forEach($scope.purchaseOrderForm.itemsByCategories, function(item) {
                if (!item.selectedUnit || !item.qty || item.qty == 0 || !item.price || item.price == 0)
                    flag = false;
            });
            $scope.purchaseOrderForm.enableSubmit = flag;
            console.log();
        }

        $scope.checkValidEntries = function() {
            var itemsToDelete = [];
            _.forEach($scope.purchaseOrderForm.itemsByCategories, function(item, i) {
                if (!item.selectedUnit || !item.qty || isNaN(item.qty) || item.qty == 0 || !item.price || isNaN(item.price) || item.price == 0)
                    itemsToDelete.push(item);
            });
            //$scope.purchaseOrderForm.enableSubmit = flag;
            //console.log();
            if (itemsToDelete.length > 0 && itemsToDelete.length != $scope.purchaseOrderForm.itemsByCategories.length) {
                if (confirm("Rows with incomplete data will be deleted! Do you wish to continue?")) {
                    _.forEach(itemsToDelete, function(item) {
                        var index = _.findIndex($scope.purchaseOrderForm.itemsByCategories, function(it) {
                            return it._id == item._id;
                        });
                        $scope.purchaseOrderForm.itemsByCategories.splice(index, 1);
                    });
                    return true;
                } else {
                    return false;
                }
            } else if (itemsToDelete.length > 0 && itemsToDelete.length == $scope.purchaseOrderForm.itemsByCategories.length) {
                growl.error("All selected items have insufficient data. Please fill complete row to order!", { ttl: 3000 });
                return false;
            } else return true;
        }

         $scope.purchaseOrderForm.enableCharges = {
            value: false
        };

       

        $scope.purchaseOrderForm.charges = [];

        $scope.addChargesToEntryForm = function() {
        if(!$scope.purchaseOrderForm.charges)
          $scope.purchaseOrderForm.charges = [];
            $scope.purchaseOrderForm.charges.push({
              chargeName: null,
              chargeValue: null,
              type: 'percent',
              operationType : 'additive',
              isEdit: true
            });
            checkIfAllChargesApplied();
        }

        $scope.acceptCharge = function(charge, index,formPurchaseOrder) {
          console.log('acceptCharge', charge);
          if(charge.chargeName && charge.chargeValue && !isNaN(charge.chargeValue)) {
            charge.isEdit = false;
            checkIfAllChargesApplied();
            $scope.calculateAmt_entryForm(formPurchaseOrder);
          } else {
            growl.error("Please complete the charge details.", {ttl: 3000});
          }
        }
        $scope.CheckForFloat_cartage_entry = function(text) {
            if (isNaN(text)) {
                $scope.purchaseOrderForm.cartage = undefined;
            }
        };

          $scope.calculateAmt_entryForm = function (formPurchaseOrder,qty, price, item, vat) {
                $scope.purchaseOrderForm.enableSubmit = true;
                
                console.log("form",formPurchaseOrder)
                console.log("valid",$scope.purchaseOrderForm)
                
              var billTotal = 0;
              var grandTotal = $scope.purchaseOrderForm.cartage ? Number($scope.purchaseOrderForm.cartage) : 0;
              var discount = 0,  totalVat=0;
              var discountableAmount = 0;
              var netAdditionalCharge = 0;
              _.forEach($scope.purchaseOrderForm.itemsByCategories, function (item) {
                    if(!isNaN(item.qty)){
                      if(!isNaN(item.price)) {
                        billTotal += (item.qty * item.price);
                        
                      }
                    }
              });

              if(!isNaN($scope.purchaseOrderForm.discount)){
                // $scope.purchaseOrderForm.discountType=percent
                    if($scope.purchaseOrderForm.discountType == 'percent') {
                      discount = ($scope.purchaseOrderForm.discount * billTotal / 100);
                      $scope.purchaseOrderForm.discountAmt = discount

                    }
                    else {
                      discount = Number($scope.purchaseOrderForm.discount);
                      $scope.purchaseOrderForm.discountAmt = discount
                    }
              }
              //console.log(discount);
              console.log("$scope.stockEntryForm.itemsByCategories  calculateAmt_entryForm",$scope.purchaseOrderForm.itemsByCategories)
              _.forEach($scope.purchaseOrderForm.itemsByCategories, function (item) {

                if(!isNaN(item.qty)) {
                 
                        
                  if(!isNaN(item.price)) {
                    $scope.purchaseOrderForm.enableSubmit=true
                    formPurchaseOrder.$invalid=false
                    
                    item.subTotal = item.qty * item.price;
                    item.totalAmount = item.subTotal - (item.subTotal * discount / billTotal);
                    discountableAmount += item.totalAmount; 
                    if (item.vatPercent == "" || item.vatPercent == undefined || isNaN(item.vatPercent)) {
                      item.addedAmt = 0;
                      item.vatPercent = "";
                    }
                    else {
                      var vatamt = (item.totalAmount * .01 * parseFloat(item.vatPercent));
                      item.addedAmt = vatamt;
                      totalVat= totalVat+ vatamt
                    }
                    item.totalAmount += item.addedAmt;
                    grandTotal += item.totalAmount;
                    //console.log(item.totalAmount, item.subTotal, discount, billTotal, item.addedAmt);
                  }
                  // $scope.purchaseOrderForm.enableSubmit=false
                }
              });

              _.forEach($scope.purchaseOrderForm.charges , function(charge) {
                    if(charge.isEdit == false) {
                        if(charge.operationType == 'additive') {
                            if(charge.type == 'percent')
                            {
                                netAdditionalCharge += (charge.chargeValue * 0.01 * discountableAmount);
                                charge.chargableAmount = (charge.chargeValue * 0.01 * discountableAmount);
                            }
                            else
                            {
                                netAdditionalCharge += (charge.chargeValue);
                                charge.chargableAmount = (charge.chargeValue);
                            }
                        }
                        else
                        {
                            if(charge.type == 'percent')
                            {
                                netAdditionalCharge -= (charge.chargeValue * 0.01 * discountableAmount);
                                charge.chargableAmount = (charge.chargeValue * 0.01 * discountableAmount);;
                            }
                            else
                            {
                                netAdditionalCharge -= (charge.chargeValue);
                                charge.chargableAmount = (charge.chargeValue);
                            }
                        }
                    }

                   //$SCOPE.purchaseOrderForm.disableEntries=false
              })

              //if(qty && price && item && vat){
              if(qty && price && item){
                var index = _.findIndex($scope.purchaseOrderForm.itemsByCategories, {_id: item._id});
                if (!isNaN(qty)) {
                  if (!isNaN(price)) {
                  
                  }
                  else {
                    $scope.purchaseOrderForm.itemsByCategories[index].price = "";
                  }
                }
                else {
                  $scope.purchaseOrderForm.itemsByCategories[index].qty = "";
                }
              }
              $scope.purchaseOrderForm.billTotal = grandTotal + netAdditionalCharge;
              
              $scope.discountableAmount = discountableAmount
              $scope.grandTotal = grandTotal
              $scope.netAdditionalCharge = netAdditionalCharge
              $scope.billTotal = billTotal
              $scope.discount = discount
              $scope.totalVat = totalVat
              console.log("discountable amt",discountableAmount)
              console.log("grandTotal",grandTotal)
              console.log("netAdditionalCharge",netAdditionalCharge)
              console.log("billTotal",billTotal)
              console.log("discount form",$scope.purchaseOrderForm.discountAmt)
              console.log("discount",discount)
              
              console.log("vatamt",totalVat)
              
            }

         $scope.removeCharge = function (charge, index,formPurchaseOrder,removeCharge) {
            console.log("chargeeeeeees")
          if(confirm("Are you sure you want to remove " + charge.chargeName + "?")){
            $scope.purchaseOrderForm.charges.splice(index, 1);
            checkIfAllChargesApplied(removeCharge);
          }
        }

        $scope.removeChargesFromEntryFormEdit = function( index,removeCharge,formPurchaseOrder,qty, price, item, vat) {
            console.log("charges")
              $scope.purchaseOrderForm.charges.splice(index, 1);
            calculateAmt_entryForm(formPurchaseOrder,qty, price, item, vat,removeCharge);
            checkIfAllChargesApplied();
        }
        $scope.removeChargesFromEntryForm = function(index,removeCharge,formPurchaseOrder,qty, price, item, vat) {
            console.log("charges")
            $scope.purchaseOrderForm.charges.splice(index, 1);
            calculateAmt_entryForm(formPurchaseOrder,qty, price, item, vat,removeCharge);
            checkIfAllChargesApplied();
        }

         function calculateAmt_entryForm (formPurchaseOrder,qty, price, item, vat,removeCharge) {
                $scope.purchaseOrderForm.enableSubmit = true;
                
                console.log("form",formPurchaseOrder)
                console.log("valid",$scope.purchaseOrderForm)
                
              var billTotal = 0;
              var grandTotal = $scope.purchaseOrderForm.cartage ? Number($scope.purchaseOrderForm.cartage) : 0;
              var discount = 0,  totalVat=0;
              var discountableAmount = 0;
              var netAdditionalCharge = 0;
              _.forEach($scope.purchaseOrderForm.itemsByCategories, function (item) {
                    if(!isNaN(item.qty)){
                      if(!isNaN(item.price)) {
                        billTotal += (item.qty * item.price);
                        
                      }
                    }
              });

              if(!isNaN($scope.purchaseOrderForm.discount)){
                // $scope.purchaseOrderForm.discountType=percent
                    if($scope.purchaseOrderForm.discountType == 'percent') {
                      discount = ($scope.purchaseOrderForm.discount * billTotal / 100);
                      $scope.purchaseOrderForm.discountAmt = discount

                    }
                    else {
                      discount = Number($scope.purchaseOrderForm.discount);
                      $scope.purchaseOrderForm.discountAmt = discount
                    }
              }
              //console.log(discount);
              console.log("$scope.stockEntryForm.itemsByCategories  calculateAmt_entryForm",$scope.purchaseOrderForm.itemsByCategories)
              _.forEach($scope.purchaseOrderForm.itemsByCategories, function (item) {

                if(!isNaN(item.qty)) {
                 
                        
                  if(!isNaN(item.price)) {
                    $scope.purchaseOrderForm.enableSubmit=true
                    formPurchaseOrder.$invalid=false
                    
                    item.subTotal = item.qty * item.price;
                    item.totalAmount = item.subTotal - (item.subTotal * discount / billTotal);
                    discountableAmount += item.totalAmount; 
                    if (item.vatPercent == "" || item.vatPercent == undefined || isNaN(item.vatPercent)) {
                      item.addedAmt = 0;
                      item.vatPercent = "";
                    }
                    else {
                      var vatamt = (item.totalAmount * .01 * parseFloat(item.vatPercent));
                      item.addedAmt = vatamt;
                      totalVat= totalVat+ vatamt
                    }
                    item.totalAmount += item.addedAmt;
                    grandTotal += item.totalAmount;
                    //console.log(item.totalAmount, item.subTotal, discount, billTotal, item.addedAmt);
                  }
                  // $scope.purchaseOrderForm.enableSubmit=false
                }
              });

              _.forEach($scope.purchaseOrderForm.charges , function(charge) {
                    if(charge.isEdit == false) {
                        if(charge.operationType == 'additive') {
                            if(charge.type == 'percent')
                            {
                                netAdditionalCharge += (charge.chargeValue * 0.01 * discountableAmount);
                                charge.chargableAmount = (charge.chargeValue * 0.01 * discountableAmount);
                            }
                            else
                            {
                                netAdditionalCharge += (charge.chargeValue);
                                charge.chargableAmount = (charge.chargeValue);
                            }
                        }
                        else
                        {
                            if(charge.type == 'percent')
                            {
                                netAdditionalCharge -= (charge.chargeValue * 0.01 * discountableAmount);
                                charge.chargableAmount = (charge.chargeValue * 0.01 * discountableAmount);;
                            }
                            else
                            {
                                netAdditionalCharge -= (charge.chargeValue);
                                charge.chargableAmount = (charge.chargeValue);
                            }
                        }
                    }

                   //$SCOPE.purchaseOrderForm.disableEntries=false
              })

              //if(qty && price && item && vat){
              if(qty && price && item){
                var index = _.findIndex($scope.purchaseOrderForm.itemsByCategories, {_id: item._id});
                if (!isNaN(qty)) {
                  if (!isNaN(price)) {
                  
                  }
                  else {
                    $scope.purchaseOrderForm.itemsByCategories[index].price = "";
                  }
                }
                else {
                  $scope.purchaseOrderForm.itemsByCategories[index].qty = "";
                }
              }
              $scope.purchaseOrderForm.billTotal = grandTotal + netAdditionalCharge;
              
              $scope.discountableAmount = discountableAmount
              $scope.grandTotal = grandTotal
              $scope.netAdditionalCharge = netAdditionalCharge
              $scope.billTotal = billTotal
              $scope.discount = discount
              $scope.totalVat = totalVat
              console.log("discountable amt",discountableAmount)
              console.log("grandTotal",grandTotal)
              console.log("netAdditionalCharge",netAdditionalCharge)
              console.log("billTotal",billTotal)
              console.log("discount form",$scope.purchaseOrderForm.discountAmt)
              console.log("discount",discount)
              
              console.log("vatamt",totalVat)
              
            }

       $scope.changeChargeEditStatus = function(index, charge,operationType,formPurchaseOrder) {
            console.log("editt")
            $scope.purchaseOrderForm.charges[index].isEdit = true;
            $scope.purchaseOrderForm.charges[index].ope
            checkIfAllChargesApplied();
        }

        function checkIfAllChargesApplied() {
            console.log('checkIfAllChargesApplied',$scope.purchaseOrderForm.charges);
            var flag = false;
            for (var i = 0; i<$scope.purchaseOrderForm.charges.length; ++i) {
                var charge = $scope.purchaseOrderForm.charges[i];
                if(!charge.chargeName || !charge.chargeValue || isNaN(charge.chargeValue) || !charge.operationType || !charge.type || charge.isEdit == true) {
                console.log('if checkIfAllChargesApplied');
                    flag = false;
                    break;
                }
                else
                {
                    console.log('else checkIfAllChargesApplied');
                    flag = true;
                }
            };
            $scope.purchaseOrderForm.enableSubmit = flag;
        }


        $scope.submitPurchaseOrder = function(form) {
          console.log('$scope.purchaseOrderForm',$scope.purchaseOrderForm);
            if (form.$valid) {
                if ($scope.checkValidEntries()) {
                    if ($scope.purchaseOrderForm.itemsByCategories.length > 0) {
                        $scope.purchaseOrderForm.enableSubmit = false;
                        $scope.purchaseOrderForm.disableEntries = true;
                        $scope.purchaseOrderForm.store.vendor = $scope.purchaseOrderForm.vendor;
                        var poData = {
                            poNumber: $scope.purchaseOrderForm.poNumber,
                            daySerialNumber: $scope.purchaseOrderForm.daySerialNumber,
                            created: new Date(),
                            updated: new Date(),
                            store: {
                                name: $scope.purchaseOrderForm.store.storeName,
                                _id: $scope.purchaseOrderForm.store._id
                            },
                            vendor: {
                                name: $scope.purchaseOrderForm.vendor.vendorName,
                                _id: $scope.purchaseOrderForm.vendor._id,
                                type: $scope.purchaseOrderForm.vendor.type,
                                email: $scope.purchaseOrderForm.vendor.email,
                            },
                            items: $scope.purchaseOrderForm.itemsByCategories,
                            user: {
                                firstname: currentUser.firstname,
                                username: currentUser.username
                            },
                            deployment_id: localStorageService.get('deployment_id'),
                            deployment: currentDeployment,
                            tenant_id: localStorageService.get('tenant_id'),
                            active: true,
                            isReceived: false,
                            status: "partial",
                            charges: $scope.purchaseOrderForm.charges,
                            cartage: $scope.purchaseOrderForm.cartage,
                            discount: $scope.purchaseOrderForm.discount,
                            discountType:$scope.purchaseOrderForm.discountType,
                            discountAmt:$scope.purchaseOrderForm.discountAmt,
                        }
                        var po = new purchaseOrder(poData);
                        console.log(po);
                        po.email = document.getElementById('purchaseOrderPrint').innerHTML;
                        po.$save(function(result) {
                            console.log(result);
                            $scope.purchaseOrderForm.enablePrint = true;
                            $scope.pendingOrders.orders.push(result);
                            growl.success("Purchase Order Successfully Generated!", { ttl: 3000 });
                            $scope.purchaseOrderForm.submitted = true;

                        });
                    } else {
                        $scope.purchaseOrderForm.enableSubmit = true;
                        growl.error("There are no items to order!", { ttl: 3000 });
                    }
                }
            } else {
                $scope.purchaseOrderForm.enableSubmit = true;
                growl.error("Please fill the required fields!", { ttl: 3000 });
            }
        }

        $scope.resetForm = function(arg) {

            if ($scope.purchaseOrderForm.submitted) {
                $scope.purchaseOrderForm.poNumber += 1;
                $scope.purchaseOrderForm.daySerialNumber += 1;
            }
            _.forEach($scope.purchaseOrderForm.availableCategories, function(category) {
                category.isChecked = false;
            });
          


             if (purchaseNumber.data) {
            var poNumber = purchaseNumber.data.poNumber + 1;
            var daySerialNumber = null;
            var tDate = new Date();
            tDate.setHours(0, 0, 0, 0);
            if (new Date(purchaseNumber.data.created) < new Date(tDate)) {
                daySerialNumber = 1;
            } else {
                daySerialNumber = purchaseNumber.data.daySerialNumber + 1;
            }

            $scope.purchaseOrderForm.billDate = new Date();
            $scope.purchaseOrderForm.poNumber = poNumber
            $scope.purchaseOrderForm.daySerialNumber = daySerialNumber
            $scope.purchaseOrderForm.availableVendors = [];
            $scope.purchaseOrderForm.availableCategories = [];
            $scope.purchaseOrderForm.selectedCategories = [];
            $scope.purchaseOrderForm.itemsAvailableForSearch = [];
            $scope.purchaseOrderForm.itemsByCategories = [];
            $scope.purchaseOrderForm.discount= null
            $scope.purchaseOrderForm.discountType= ""
            $scope.purchaseOrderForm.cartage= ""
            $scope.purchaseOrderForm.charges= []
            $scope.purchaseOrderForm.billTotal= ""
            $scope.purchaseOrderForm.store = null;
            $scope.purchaseOrderForm.vendor = null;
            $scope.purchaseOrderForm.enableSubmit = true;
            $scope.purchaseOrderForm.enablePrint = false;
            $scope.purchaseOrderForm.disableEntries = false;
            $scope.purchaseOrderForm.submitted = false;
            $scope.discountableAmount = 0
            $scope.grandTotal = 0
            $scope.netAdditionalCharge = 0
            $scope.billTotal = 0
            $scope.discount = 0
            $scope.totalVat = 0
        } else {
            purchaseOrder.getLastPurchaseOrderNumber({ deployment_id: localStorageService.get('deployment_id') }, function(poN) {
                if (poN.data) {
                    var poNumber = poN.data.poNumber + 1;
                    var daySerialNumber = null;
                    var tDate = new Date();
                    tDate.setHours(0, 0, 0, 0);
                    if (new Date(poN.data.created) < new Date(tDate)) {
                        daySerialNumber = 1;
                    } else {
                        daySerialNumber = poN.data.daySerialNumber + 1;
                    }
                } else {
                    poNumber = 1;
                    daySerialNumber = 1;
                }
             
            $scope.purchaseOrderForm.billDate = new Date();
            $scope.purchaseOrderForm.poNumber = poNumber
            $scope.purchaseOrderForm.daySerialNumber = daySerialNumber
            $scope.purchaseOrderForm.availableVendors = [];
            $scope.purchaseOrderForm.availableCategories = [];
            $scope.purchaseOrderForm.selectedCategories = [];
            $scope.purchaseOrderForm.itemsAvailableForSearch = [];
            $scope.purchaseOrderForm.itemsByCategories = [];
            $scope.purchaseOrderForm.discount= null
            $scope.purchaseOrderForm.discountType= ""
            $scope.purchaseOrderForm.cartage= ""
            $scope.purchaseOrderForm.charges= []
            $scope.purchaseOrderForm.billTotal= ""
            $scope.purchaseOrderForm.store = null;
            $scope.purchaseOrderForm.vendor = null;
            $scope.purchaseOrderForm.enableSubmit = true;
            $scope.purchaseOrderForm.enablePrint = false;
            $scope.purchaseOrderForm.disableEntries = false;
            $scope.purchaseOrderForm.submitted = false;
            $scope.discountableAmount = 0
            $scope.grandTotal = 0
            $scope.netAdditionalCharge = 0
            $scope.billTotal = 0
            $scope.discount = 0
            $scope.totalVat = 0

            }, function error(err) {

                poNumber = 1;
                daySerialNumber = 1;
               _.forEach($scope.purchaseOrderForm.availableCategories, function(category) {
                category.isChecked = false;
            });
            $scope.purchaseOrderForm.billDate = new Date();
            $scope.purchaseOrderForm.poNumber = poNumber
            $scope.purchaseOrderForm.daySerialNumber = daySerialNumber
            $scope.purchaseOrderForm.availableVendors = [];
            $scope.purchaseOrderForm.availableCategories = [];
            $scope.purchaseOrderForm.selectedCategories = [];
            $scope.purchaseOrderForm.itemsAvailableForSearch = [];
            $scope.purchaseOrderForm.itemsByCategories = [];
            $scope.purchaseOrderForm.discount= null
            $scope.purchaseOrderForm.discountType= ""
            $scope.purchaseOrderForm.cartage= ""
            $scope.purchaseOrderForm.charges= []
            $scope.purchaseOrderForm.billTotal= ""
            $scope.purchaseOrderForm.store = null;
            $scope.purchaseOrderForm.vendor = null;
            $scope.purchaseOrderForm.enableSubmit = true;
            $scope.purchaseOrderForm.enablePrint = false;
            $scope.purchaseOrderForm.disableEntries = false;
            $scope.purchaseOrderForm.submitted = false;
            $scope.discountableAmount = 0
            $scope.grandTotal = 0
            $scope.netAdditionalCharge = 0
            $scope.billTotal = 0
            $scope.discount = 0
            $scope.totalVat = 0

            });
        }
        }

        $scope.printPurchaseOrder = function() {
            var table = document.getElementById('purchaseOrderPrint').innerHTML;
            var printer = window.open('', '', 'width=600,height=600');
            printer.document.open("text/html");
            printer.document.write(table);
            printer.document.close();
            printer.focus();
            printer.print();
        }

        //---------------------------------------------------End---------------------------------------------------------------
        //---------------------------------------------------Process Purchase Order--------------------------------------------
        $scope.pendingOrders = {
            orders: []
        };
        purchaseOrder.getAll({ deployment_id: localStorageService.get('deployment_id'), isReceived: false }, function(orders) {
            console.log(orders);
            $scope.pendingOrders.orders = orders;
        });

        $scope.performStockEntry = function(purchaseOrder) {
            //alert(purchaseOrder._id)
            //$state.params = _.extend($state.params, {orderId:purchaseOrder._id.toString()});
            //console.log($state.params);
            $state.go('stock.performStockEntry', { id: purchaseOrder._id });
            console.log($state);
        };
    }]);
