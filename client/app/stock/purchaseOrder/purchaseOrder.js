'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('purchaseOrder', {
        url: '/purchaseOrder',
        templateUrl: 'app/stock/purchaseOrder/purchaseOrder.html',
        controller: 'PurchaseOrderCtrl'
      });
  });
