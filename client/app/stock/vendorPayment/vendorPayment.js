'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('vendorPayment', {
        url: '/vendorPayment',
        templateUrl: 'app/stock/vendorPayment/vendorPayment.html',
        controller: 'VendorPaymentCtrl'
        
      });
  });