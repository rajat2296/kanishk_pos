'use strict';

angular.module('posistApp')
  .controller('VendorPaymentCtrl', ['$q', '$resource', '$scope', '$filter', 'growl', 'store', 'currentUser', '$rootScope', '$state', '$modal', 'VendorPaymentHistory', 'VPayment', 'StockResource','property','localStorageService', function ($q, $resource, $scope, $filter, growl, store, currentUser, $rootScope, $state, $modal, VendorPaymentHistory, VPayment, StockResource,property,localStorageService) {

    $scope.stores = $rootScope.stores;
    $scope.vendors = $rootScope.vendors;
    $scope.receivers = $scope.receivers;
    $scope.user = currentUser;
    $scope.payments = VPayment;

    // VendorPaymentHistory.getTransactionHistoryByTransactionId({},function (result){
    //   $scope.payments=result;
    // });


    function getYesterDayDate() {
      var d = new Date();
      d.setDate(d.getDate() - 1);
      return d;
    };
  $scope.resetSerialNumber=false
    $scope.hidePricing=false
  // property.get({
  //     tenant_id: localStorageService.get("tenant_id"),
  //     deployment_id: localStorageService.get("deployment_id")
  //   }, function (properties) {
  //       if(properties.length==0)
  //     {
        
  //        $scope.resetSerialNumber=false
  //     }
  //     else
  //     {
  //        if(properties[0].resetSerialNumber==undefined)
  //           $scope.resetSerialNumber=true
  //         else  
  //           $scope.resetSerialNumber=properties[0].resetSerialNumber
  //           console.log("reset serial ",$scope.resetSerialNumber);

  //     }
  //     // stockProperty = properties[0];
  //     // if (!stockProperty.property.hidePricingEnabled)
  //     //   $scope.hidePricing = false;
  //     // else {
  //     //   _.forEach(currentUser.selectedRoles, function (role) {
  //     //     if (stockProperty.property.hidePricing[role.name]) {
  //     //       $scope.hidePricing = true;
  //     //     }
  //     //   });
  //     // }
  //   });

    function parseDate(str) {
      // var s = str.split(" "),
      //     d = s[0].split("-"),
      //     t = s[1].replace(/:/g, "");
      // return d[2] + d[1] + d[0] + t;
      var d = $filter('date')(str, 'dd-MMM-yyyy');
      return d;
    };

    $scope.editEntryForm = {
      fromDate: getYesterDayDate(),
      toDate: new Date(),
      maxDate: new Date()
    };
    $scope.editEntryForm.stores = angular.copy($scope.stores);
    $scope.checkDateGreater = function (date) {
      var d = new Date(date);
      var dd = new Date($scope.editEntryForm.toDate);
      if (d.getTime() > dd.getTime()) {
        growl.success('Greater than toDate', {ttl: 3000});
        $scope.editEntryForm.fromDate = getYesterDayDate()
      }
      else {
        //growl.success('Lesser than toDate', {ttl: 3000});
      }

    };

    $scope.checkDateLesser = function (date) {
      var d = new Date(date);
      var dd = new Date($scope.editEntryForm.fromDate);
      if (d.getTime() < dd.getTime()) {
        growl.success('Can not be Lesser than fromDate', {ttl: 3000});
        $scope.editEntryForm.toDate = new Date();
      }
      else {
        //growl.success('Greater than fromDate', {ttl: 3000});
      }
    };

    function bindVendor(store) {
      $scope.editEntryForm.vendorReceiver = [];
      if (store != null) {
        _.forEach($scope.vendors, function (v, i) {
          if (v.pricing != undefined) {
            if (v.pricing._id == store._id) {
              v.name = v.vendorName;
              $scope.editEntryForm.vendorReceiver.push(v);
            }
          }
        });
      }
    };
    function bindReceiver(store) {
      $scope.editEntryForm.vendorReceiver = [];
      var receivers = angular.copy($scope.receivers);
      if (store != null) {
        _.forEach(receivers, function (r, i) {
          // if(r.pricing!=undefined){
          //     _.forEach(r.pricing.store, function(s,ii){
          //         if(s._id==store._id){
          r.name = r.receiverName;
          $scope.editEntryForm.vendorReceiver.push(r);
          //         }
          //     });
          // }
        });
      }
    };

    $scope.bindVenRec_editEntryForm = function (store) {
      var entryType = $scope.editEntryForm.entryType;
      if (entryType == 1) {
        //bindVendor
        bindVendor(angular.copy(store));
      }
      else if (entryType == 2) {
        //bind REceiver
        bindReceiver(store);
      }
    };

    $scope.bindStore_editEntryForm = function (entryType) {
      $scope.editEntryForm.stores = [];
      $scope.editEntryForm.vendorReceiver = [];
      $scope.editEntryForm.stores = angular.copy($scope.stores);
    };

    $scope.startSearch = function () {
      $scope.editEntryForm.availableRecords = [];
      var fromDate = new Date($scope.editEntryForm.fromDate);
      fromDate.setHours(0, 0, 0, 0);
      var toDate = new Date($scope.editEntryForm.toDate);
      toDate.setHours(0, 0, 0, 0);
      var strId = "";
      if ($scope.editEntryForm.store != undefined) {
        strId = $scope.editEntryForm.store._id;
      }

      var venId = "";
      if ($scope.editEntryForm.vendor != undefined) {
        venId = $scope.editEntryForm.vendor._id;
      }

      if ($scope.editEntryForm.entryType == "1") {
        var req = {
          "fromDate": fromDate,
          "toDate": toDate,
          "transactionType": $scope.editEntryForm.entryType,
          "_store._id": strId,
          "_store.vendor._id": venId,
          "deployment_id": currentUser.deployment_id,
          "tenant_id": currentUser.tenant_id
        };
      }
      else if ($scope.editEntryForm.entryType == "2") {
        var req = {
          "fromDate": fromDate,
          "toDate": toDate,
          "transactionType": $scope.editEntryForm.entryType,
          "_store._id": strId,
          "_store.receiver._id": venId,
          "deployment_id": currentUser.deployment_id,
          "tenant_id": currentUser.tenant_id
        };
      }

      StockResource.getAllByTranasactionAndDateRange(req, function (result) {
        //calculateForStockEntry(result);
        //StockResource.getClosingQty(req, function (result){
        calculateQtyAmtAndVatBillWise(result);
        $scope.editEntryForm.availableRecords = result;
        console.log(result);

      });
    };

    $scope.editEditEntryForm = function (item) {
      $state.go('stock.editStockEntry', {id: item._id, transactionType: $scope.editEntryForm.entryType});
    };

   function calculateForStockEntry(result) {
  _.forEach(result, function (r, iii) {
    //console.log('-------------------New Transaction------------')
    var tQty = 0;
    var tAmt = 0;
    var tVat = 0;
    var dAmt = 0;
    var tDue = 0;
    var tPaid = 0;
    var cartage = 0;
    var disTotal=0,vatTotal=0;
    var req = {mainTranasctionId: r._id};
    var index = _.findIndex($scope.payments, {"_id": r._id});
    if (index >= 0) {
      tPaid = angular.copy($scope.payments[index].totalPaidAmount);
    }
    console.log('tpaid', tPaid);
    var dAmount=0, netAdditionalCharge =0;
    _.forEach(r._store.vendor.category, function (c, ii) {
      _.forEach(c.items, function(itm,i){
        tAmt+=parseFloat(itm.subTotal);
   });
      _.forEach(c.items, function (itm, i) {
        //console.log('-------------------New Item---------------------')
        tQty += parseFloat(itm.qty);
        //tAmt += parseFloat(itm.subTotal);
        var dis=0,disAmount=0,perDis=0,vs=0;
         if(r.discount!=undefined && r.discount!=0){
                  if(r.discountType=="percent"){
                    //console.log('total',tAmt);
                    dis= parseFloat(tAmt*0.01*r.discount).toFixed(2);
                    perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(tAmt).toFixed(2)).toFixed(2);
                    //console.log('dis', dis);
                    //console.log('perDis', perDis)
                    //console.log('itm.total',itm.subTotal);
                    disAmount=parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2);
                    //console.log('disAmount',disAmount);
                      if(itm.vatPercent)
                      {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',itm.vatPercent);
                        vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                      }
                      else
                      {
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                      }
                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                    //console.log('disTotal',disTotal);
                  }
                  else{
                    //console.log('total',tAmt);
                    dis= parseFloat(r.discount).toFixed(2);
                    //perDis=parseFloat(dis/numberOfItems).toFixed(2);
                    perDis=parseFloat(parseFloat(parseFloat(itm.subTotal)*parseFloat(dis)).toFixed(2)/parseFloat(tAmt).toFixed(2)).toFixed(2);
                    //console.log('dis', dis);
                    //console.log('perDis', perDis);
                    //console.log('itm.total',itm.subTotal);
                    disAmount=parseFloat(parseFloat(itm.subTotal).toFixed(2)-parseFloat(perDis).toFixed(2)).toFixed(2);
                    //console.log('disAmount',disAmount);
                      if(itm.vatPercent)
                      {
                        //console.log('vatTotal vefore vat',vatTotal);
                        //console.log('itm.vatPercent',itm.vatPercent);
                        vs=parseFloat(itm.vatPercent*0.01*parseFloat(disAmount).toFixed(2)).toFixed(2);
                        //console.log('vs',vs);
                        //console.log('disAmount before vat',disAmount);
                        disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                        //console.log('disAmount after vat',disAmount);
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);
                        vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                        //console.log(disAmount);
                        //console.log('vatTotal after vat',vatTotal);
                      }
                      else
                      {
                        disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                        //console.log('disTotal',disTotal);  
                      }
                    //disTotal=parseFloat(disTotal).toFixed(2)+parseFloat(disAmount).toFixed(2);
                    //console.log('disTotal',disTotal);
                    }
                }
                else
                {
                  if(itm.vatPercent)
                  {
                    //console.log('vatTotal vefore vat',vatTotal);
                    //console.log('itm.vatPercent',itm.vatPercent);
                    vs=parseFloat(itm.vatPercent*0.01*parseFloat(itm.subTotal).toFixed(2)).toFixed(2);
                    //console.log('vs',vs);
                    //console.log('disAmount before vat',disAmount);
                    disAmount=parseFloat(parseFloat(itm.subTotal)+parseFloat(vs)).toFixed(2);
                        
                    //disAmount=parseFloat(parseFloat(disAmount)+parseFloat(vs)).toFixed(2);
                    //console.log('disAmount after vat',disAmount);
                    disTotal=parseFloat(parseFloat(disTotal)+parseFloat(disAmount)).toFixed(2);
                    //console.log('disTotal',disTotal);
                    vatTotal=parseFloat(parseFloat(vatTotal)+parseFloat(vs)).toFixed(2);
                    //console.log(disAmount);
                    //console.log('vatTotal after vat',vatTotal);
                  }
                else
                  {
                    disTotal=parseFloat(parseFloat(disTotal)+parseFloat(itm.subTotal)).toFixed(2);
                    //console.log('disTotal',disTotal);
                   
                  }                  
                }
      });
    });
    if (r.discount != 0 && r.discount != undefined) {
      if (r.discountType == "percent") {
        dAmt = parseFloat(tAmt * 0.01 * parseFloat(r.discount)).toFixed(2);
        dAmount = parseFloat(parseFloat(tAmt) - parseFloat(dAmt)).toFixed(2);
      }
      else {
        dAmt = parseFloat(r.discount).toFixed(2);
        dAmount = parseFloat(tAmt).toFixed();
      }
    }
    if (r.cartage != undefined && r.cartage != 0 && r.cartage != null) {
      cartage = parseFloat(r.cartage);
    }
    if(r.charges)
                    {
                        //console.log('afterDis',afterDis);
                        _.forEach(r.charges , function(charge) {
                            if(charge.operationType == 'additive')
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue * 0.01 * dAmount)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * dAmount).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) + parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                            else
                            {
                                if(charge.type == 'percent')
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue * 0.01 * dAmount)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue * 0.01 * dAmount).toFixed(2);
                                }
                                else
                                {
                                    netAdditionalCharge = parseFloat(parseFloat(netAdditionalCharge) - parseFloat(charge.chargeValue)).toFixed(2);
                                    charge.chargableAmount = parseFloat(charge.chargeValue).toFixed(2);
                                }
                            }
                        });
                    }
    console.log(dAmt, tAmt, tVat, cartage, dAmount, netAdditionalCharge);
    //after discount
    //tAmt = parseFloat(tAmt) - parseFloat(dAmt);
    //after vat
    //tAmt = parseFloat(tAmt) + parseFloat(tVat);
    //After Cartage
    disTotal = parseFloat(disTotal) + parseFloat(cartage) + parseFloat(netAdditionalCharge);
    tDue = (disTotal - tPaid);
    r.totalQty = parseFloat(tQty).toFixed(2);
    r.totalAmt = parseFloat(disTotal).toFixed(2);
    r.totalPaid = parseFloat(tPaid).toFixed(2);
    r.totalDue = parseFloat(tDue).toFixed(2);
  });
};
   function calculateForStockSale(result) {
  _.forEach(result, function (r, iii) {
    var tQty = 0;
    var tAmt = 0;
    var tDue = 0;
    var tPaid = 0;
    var dAmt = 0;
    var tvat = 0;
    var tSc = 0;
    var tcartage = 0;
    var tpay = 0;
    var req = {mainTranasctionId: r._id};
    var index = _.findIndex($scope.payments, {"_id": r._id});
    if (index >= 0) {
      tPaid = angular.copy($scope.payments[index].totalPaidAmount);
    }
    _.forEach(r._store.receiver.category, function (c, ii) {
      _.forEach(c.items, function (itm, i) {
        tQty += parseFloat(itm.qty);
        tAmt += parseFloat((itm.qty) * (itm.price));
      });
    });

    if (r.discount != 0 && r.discount != undefined && r.discount != null) {
      if (r.discountType == "percent") {
        dAmt = parseFloat(tAmt * 0.01 * parseFloat(r.discount)).toFixed(2);
      }
      else {
        dAmt = parseFloat(r.discount).toFixed(2);
      }
    }
    var amtAftreDiscount = parseFloat(tAmt) - parseFloat(dAmt);
    if (r.vat != 0 && r.vat != undefined && r.vat != null) {
      if (r.vatType == "percent") {
        tvat = parseFloat(amtAftreDiscount * 0.01 * parseFloat(r.vat)).toFixed(2);
      }
      else {
        tvat = parseFloat(r.vat).toFixed(2);
      }
    }
    if (r.serviceCharge != undefined && r.serviceCharge != null) {
      if (r.serviceChargeType == "percent") {
        tSc = parseFloat(amtAftreDiscount * 0.01 * parseFloat(r.serviceCharge)).toFixed(2);
      }
      else {
        tSc = parseFloat(r.serviceCharge).toFixed(2);
      }
    }
    if (r.cartage != undefined && r.cartage != null) {
      tcartage = parseFloat(r.cartage).toFixed(2);
    }
    if (r.payment != undefined && r.payment != null) {
      tpay = parseFloat(r.payment).toFixed(2);
    }
    tAmt = parseFloat(parseFloat(tAmt) - parseFloat(dAmt) + parseFloat(tSc) + parseFloat(tvat) + parseFloat(tcartage) + parseFloat(tpay)).toFixed(2);
    tDue = (tAmt - tPaid);
    r.totalQty = parseFloat(tQty).toFixed(2);
    r.totalAmt = parseFloat(tAmt).toFixed(2);
    r.totalPaid = parseFloat(tPaid).toFixed(2);
    r.totalDue = parseFloat(tDue).toFixed(2);
  });
};

    function calculateQtyAmtAndVatBillWise(result) {
      _.forEach(result, function (r, i) {
        if (r.transactionType == 1) {
          calculateForStockEntry(result);
        }
        else {
          calculateForStockSale(result);
        }
      })
    };

    var guid = (function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }

      return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
      };
    })();

    $rootScope.$on('whatevereventnameyouwant', function (event, data) {
      $scope.payments = data;
      //calculateForStockEntry($scope.editEntryForm.availableRecords);
      calculateQtyAmtAndVatBillWise($scope.editEntryForm.availableRecords);
      console.log(data);
    });

    $scope.UpdateVendorPatment = function (item) {
      var deployment_id = $scope.user.deployment_id;
      var tenant_id = $scope.user.tenant_id;
      var transactionId = item.transactionId;
      var billAmount = item.totalAmt;
      var dueAmount = item.totalDue;
      var tPaid = item.totalPaid;
      var mainTranasctionId = item._id;
      if ($scope.editEntryForm.entryType == 1 && item.isPOEntry) {
        var billNo = "PO- " + item.daySerialNumber;
      }
      else if ($scope.editEntryForm.entryType == 1 && !item.isPOEntry) {
        var billNo = "SE- " + item.daySerialNumber;
      }
      else {
        var billNo = "SS- " + item.daySerialNumber;
      }
      var payments = $scope.payments;
      $modal.open({
        templateUrl: 'app/stock/vendorPayment/_updateVendorPayment.html',
        controller: ['$rootScope', '$scope', '$resource', '$modalInstance', 'VendorPaymentHistory', 'growl', function ($rootScope, $scope, $resource, $modalInstance, VendorPaymentHistory, growl) {
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
           $scope.disableVendorPayment = false;
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
          
          $scope.paymentHistory = payments;
          $scope.updateVendor = {};
          $scope.updateVendor.billNo = billNo;
          $scope.updateVendor.billAmount = billAmount;
          $scope.updateVendor.dueAmount = dueAmount;
          $scope.updateVendor.deployment_id = deployment_id;
          $scope.updateVendor.tenant_id = tenant_id;
          $scope.updateVendor.transactionId = transactionId;
          $scope.updateVendor.totalPaid = tPaid;
          $scope.updateVendor.mainTranasctionId = mainTranasctionId;
          $scope.updateVendor.availableItems = [];
          VendorPaymentHistory.getByMainTransactionId({mainTranasctionId: $scope.updateVendor.mainTranasctionId}).$promise.then(function (result) {
            //bindBillNo(result);
            $scope.updateVendor.availableItems = result;
          });
          // function bindBillNo(result){
          //   _.forEach(result, function(r,i){
          //     r.billNo=
          //   });
          // };
if(Number(billAmount)==Number(tPaid))
      {
        $scope.disableVendorPayment = true;
      }
      else
      {
        $scope.disableVendorPayment = false;
      }
          // function bindBillNo(result){
          //   _.forEach(result, function(r,i){
          //     r.billNo=
          //   });
          // };

          $scope.ok = function () {
            if(parseInt($scope.updateVendor.paidAmount) == 0){
              growl.error('Paid Amount cannot be 0', {ttl: 3000});
            }
            else{
              if ($scope.updateVendor.paidAmount != "" && !isNaN($scope.updateVendor.paidAmount)) {
                if (parseFloat($scope.updateVendor.dueAmount) >= parseFloat($scope.updateVendor.paidAmount)) {
                  $scope.updateVendor.paymentId = guid();
                  $scope.updateVendor.disableOk = true;
                  VendorPaymentHistory.saveData({}, $scope.updateVendor, function (result) {
                    $scope.updateVendor.totalPaid += result.paidAmount;
                    var index = _.findIndex($scope.paymentHistory, {"_id": result.mainTranasctionId});
                    if (index >= 0) {
                      $scope.paymentHistory[index].totalPaidAmount += result.paidAmount;
                    }
                    else {
                      $scope.paymentHistory.push({
                        _id: $scope.updateVendor.mainTranasctionId,
                        totalPaidAmount: result.paidAmount
                      });
                    }
                    //$scope.updateVendor.availableItems.push(result);
                    $modalInstance.dismiss('cancel');
                    $scope.$emit('whatevereventnameyouwant', $scope.paymentHistory);
                    $scope.updateVendor.disableOk = false;
                  }, function (err){
                    $scope.updateVendor.disableOk = false;
                  });
                }
                else {
                  growl.error('Paid Amount must be less than equals to Due Amount', {ttl: 3000});
                }

              }

              else {
                growl.error('Paid Amount must be numeric', {ttl: 3000});
              }
          }
        };

          $scope.formatPrice = function (paidAmount) {
            if (isNaN(paidAmount)) {
              $scope.updateVendor.paidAmount = "";
            }
          }
        }],
        size: 'lg'
      });
    };

  }]);

