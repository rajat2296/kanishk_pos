'use strict';

describe('Controller: VendorPaymentCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var VendorPaymentCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VendorPaymentCtrl = $controller('VendorPaymentCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
