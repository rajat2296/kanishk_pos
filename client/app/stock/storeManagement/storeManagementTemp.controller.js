//new code

'use strict';

angular.module('posistApp')
  .controller('StoreManagementTempCtrl', ['$q', '$scope', '$modal', 'store', 'Deployment', 'currentUser', 'stockCategory', 'stockUnit', 'stockItem', 'vendor', 'receiver', '$filter', 'StockTransaction', '$rootScope', '$resource', 'growl', 'Pagination', 'stockSync', 'StockResource', 'localStorageService', 'stockRecipe', 'Category', 'Item', 'baseKitchenItem', 'baseKitchenUnit', 'Utils', '$timeout', 'currentDeployment', function ($q, $scope, $modal, store, Deployment, currentUser, stockCategory, stockUnit, stockItem, vendor, receiver, $filter, StockTransaction, $rootScope, $resource, growl, Pagination, stockSync, StockResource, localStorageService, stockRecipe, Category, Item, baseKitchenItem, baseKitchenUnit, Utils, $timeout, currentDeployment) {
    currentUser.deployment_id = localStorageService.get('deployment_id')
    $scope.currentDeployment = currentDeployment;
    console.log(currentUser);
    $scope.stores = [];
    $scope.deployment = {};
    $scope.stockCategories_Category = [];
    $scope.receipes = [];
    $scope.items = [];
    $scope.baseKitchenItems = [];
    $scope.categories = [];
    $scope.spCategories = [];
    $scope.processedCat = {_id: "PosistTech111111", categoryName: "Processed Food"};
    $scope.processedSemi_Cat = {_id: "PosistTech222222", categoryName: "InterMediate Food"};
    $scope.rawMaterialOpening = true;
    $scope.intermediateOpening = true;
    $scope.menuItemOpening = true;
    $scope.categoryTypes = [];
    $scope.rawCat = [];
    $scope.itemTypes = [{_id: 0, itemType: 'Raw'}, {_id: 1, itemType: 'Semi-Processed'}, {
      _id: 2,
      itemType: 'Processed'
    }];
    //modified
    $scope.pagination = {};
    $scope.pagination.currentPage = 1;
    $scope.pagination.itemsPerPage = 20;
    //end
    //$scope.itemTypes=[{_id:0, itemType:'Raw', catNames:$scope.rawCat}, {_id:1, itemType:'Semi-Processed', catNames:$scope.processedSemi_Cat }, {_id:2, itemType:'Processed', catNames:$scope.processedCat }];
    //$scope.allTransactions=angular.copy($rootScope.stockTransaction);
    $scope.disable = {};
    $scope.disable.storeName = true;
    $scope.disable.storeUID = true;
    $scope.disable.storeLocation = true;

    function getStores_ByDeployment() {
      var deferred = $q.defer();
      store.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id}, function success(result) {
          deferred.resolve(result);
        }, function error(err) {
          deferred.reject(clearMongooseError);
        }
      );
      return deferred.promise;
    };

    function getAllDeployments() {
      var deferred = $q.defer();
      Deployment.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getStockCategories_ByDeployment() {
      var deferred = $q.defer();
      stockCategory.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getStockUnits_ByDeployment() {
      var deferred = $q.defer();
      stockUnit.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getStockItems_ByDeployment() {
      var deferred = $q.defer();
      stockItem.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getVendors_ByDeployment() {
      var deferred = $q.defer();
      vendor.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;

    };

    function getReceivers_ByDeployment() {
      var deferred = $q.defer();
      receiver.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getItems() {
      var deferred = $q.defer();
      Item.get({tenant_id: currentUser.tenant_id, deployment_id: currentUser.deployment_id}, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getItems_ProcessedFood() {
      var deferred = $q.defer();
      Item.getProcessedItemOptimized({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getItems_SemiProcessedFood() {
      var deferred = $q.defer();
      Item.getSemiProcessedItemOptimized({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
    };

    function getstockRecies() {
      var deferred = $q.defer();
      stockRecipe.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      // return stockRecipe.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getstockRecies_Intermediate() {
      var deferred = $q.defer();
      stockRecipe.getSemiProcessedRecipes({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      // return stockRecipe.getSemiProcessedRecipes({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getstockRecies_FinishedFood() {
      var deferred = $q.defer();
      stockRecipe.getProcessedRecipes({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      //return stockRecipe.getProcessedRecipes({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    };

    function getItems_ProcessedFoodForAnotherDeployment(deploymentId) {
      var deferred = $q.defer();
      stockRecipe.get({tenant_id: currentUser.tenant_id, deployment_id: deploymentId}, function (recipes) {
        Item.getProcessedItem({
          tenant_id: currentUser.tenant_id,
          deployment_id: deploymentId
        }, function success(result) {
          var availableItems = [];
          _.forEach(result, function (item) {
            var recipe = _.find(recipes, function (r) {
              return item._id == r.itemId;
            });
            if (recipe)
              availableItems.push(item);
          });
          deferred.resolve(availableItems);
        }, function error(err) {
          deferred.reject(clearMongooseError);
        });
      });
      return deferred.promise;
      // return Item.getProcessedItem({tenant_id:currentUser.tenant_id,deployment_id:deploymentId});
    };

    function getItems_SemiProcessedFoodForAnotherDeployment(deploymentId, isOutlet) {
      var deferred = $q.defer();
      stockRecipe.get({tenant_id: currentUser.tenant_id, deployment_id: deploymentId}, function (recipes) {
        Item.getSemiProcessedItem({
          tenant_id: currentUser.tenant_id,
          deployment_id: deploymentId
        }, function success(result) {
          var availableItems = [];
          _.forEach(result, function (item) {
            var recipe = _.find(recipes, function (r) {
              return item._id == r.itemId;
            });
            if (recipe)
              availableItems.push(item);
          });
          deferred.resolve(availableItems);
        }, function error(err) {
          deferred.reject(clearMongooseError);
        });
      });
      return deferred.promise;
      // return Item.getProcessedItem({tenant_id:currentUser.tenant_id,deployment_id:deploymentId});
    };

    function getBaseKitchenItems() {
      var deferred = $q.defer();
      baseKitchenItem.get({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, function success(result) {
        deferred.resolve(result);
      }, function error(err) {
        deferred.reject(clearMongooseError);
      });
      return deferred.promise;
      //return baseKitchenItem.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id});
    }

    var getDeployment = Deployment.get({tenant_id: currentUser.tenant_id}).$promise.then(function (dep) {
      $scope.deployment = dep;

      $scope.DepForVendor = angular.copy(dep)
      _.forEachRight($scope.DepForVendor, function (d, i) {
        if (d._id == currentUser.deployment_id || d.isMaster) {
          $scope.DepForVendor.splice(i, 1);
        }
      });

      var allPromise = $q.all([
        //getStores_ByDeployment(),
        getStockCategories_ByDeployment(), getStockUnits_ByDeployment(),
        getStockItems_ByDeployment(), getVendors_ByDeployment(), getReceivers_ByDeployment(),
        getstockRecies(),getItems_ProcessedFood(), getItems_SemiProcessedFood(), getstockRecies_Intermediate(),
        getstockRecies_FinishedFood(), getBaseKitchenItems(), getAllDeployments()

      ]);

      allPromise.then(function (value) {
        //console.log(value);
        $scope.stockCategories = value[0];
        //console.log($scope.stockCategories);
        $scope.stockCategories_Category = value[0];
        $scope.stockUnits = value[1];
        $scope.totalUnits = $scope.stockUnits.length;
        $scope.stockItems = value[2];
        enableItemGroups();
        $scope.vendors = value[3];
        $scope.receivers = value[4];
        $scope.receipes = value[5];
        $scope.processedCat.item = value[6];
        //console.log($scope.processedCat.item);
        $scope.processedSemi_Cat.item = value[7];
        //console.log($scope.processedSemi_Cat.item);
        bindUnitsForProcessedFood($scope.processedCat.item);
        bindUnitsForProcessedFood($scope.processedSemi_Cat.item);
        $scope.semiRecipe = value[8];
        $scope.menuRecipe = value[9];
        $scope.baseKitchenItems = value[10];
        //console.log("baseKitchenItem :" , $scope.baseKitchenItems);

        $scope.stockCategories_Category = value[0];
        $scope.totalUnits = $scope.stockUnits.length;
        enableItemGroups();
        $scope.allDeployments = value[11];

        store.get({
          tenant_id: currentUser.tenant_id,
          deployment_id: currentUser.deployment_id
        }).$promise.then(function (result) {
          $scope.stores = result;
          hideKitchenAndMainStore();
          storePagination();
          getItemsByCategory();
          filterKitchenStores();
          structringCategorisedData();
          $scope.disable.storeName = false;
          $scope.disable.storeUID = false;
          $scope.disable.storeLocation = false;
        });
      });
    });

    function filterKitchenStores() {
      $scope.listOfKitchens = [];
      _.forEach($scope.stores, function (c, i) {
        if (c.isKitchen) {
          $scope.listOfKitchens.push(c)
        }
      })
      //console.log("list of kitchens", $scope.listOfKitchens)
    };
    function AddtoItems() {
      _.forEach($scope.baseKitchenItems, function (b, i) {
        b.isBaseKitchen = true;
        $scope.stockItems.push(b);
      })
    };
    function structringCategorisedData(check) {
      _.forEach($scope.allItemsByCategories, function (c, i) {
        _.forEach($scope.stores, function (k, ii) {
          if (k.isKitchen) {
            _.forEach(k.processedFoodCategory, function (p, iii) {
              if (p._id == c._id) {
                c.assignedToKitchen = k._id;
              }
            })
          }
        })
      })

      if (!check) {
        $timeout(function () {
          $scope.copiedAllItemsByCategories = angular.copy($scope.allItemsByCategories)
          //console.log("copied with asined kitchen",  $scope.copiedAllItemsByCategories)
        }, 10);
      }
    };

    function getItemsOfAnotherDeploymentByCategory(data) {
      var uniqueListOfProcessedCategories = [];
      //console.log("process in func custom", data)
      _.forEach(data, function (c, i) {
        //console.log("COMP custom", c)
        var check = false;
        _.forEach(uniqueListOfProcessedCategories, function (u, i) {
          //console.log("U", u)
          //console.log("C", c)
          if (u._id == c.category._id) {
            var object = {
              _id: c._id,
              itemName: c.name,
              units: c.units,
              unit: c.unit,
              number: c.number,
              tabs: c.tabs,
              stockQuantity: c.stockQuantity,
              rate: c.rate
            }
            u.item.push(object);
            //console.log("PUSHING IYEM custom", object)
            check = true;
          }
        })
        //console.log("out of loop cusyom")
        //console.log("CHECK custom", check)
        if (!check) {
          //console.log("CHECK FALSE custom", check)
          var object = {
            _id: c.category._id,
            categoryName: c.category.categoryName,
            item: [{
              _id: c._id,
              itemName: c.name,
              units: c.units,
              unit: c.unit,
              number: c.number,
              tabs: c.tabs,
              stockQuantity: c.stockQuantity,
              rate: c.rate
            }],
            type: "MenuItem"
          }
          uniqueListOfProcessedCategories.push(object);
          //console.log("PUSHING OBJECT", object)
        }
      });
      return uniqueListOfProcessedCategories
    };

    function getItemsByCategory() {
      var uniqueListOfProcessedCategories = [];
      //console.log("process in func", $scope.processedCat.item)
      _.forEach($scope.processedCat.item, function (c, i) {
        //console.log("COMP", c)
        var check = false;
        _.forEach(uniqueListOfProcessedCategories, function (u, i) {
          if (u._id == c.category._id) {
            var object = {_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number};
            u.item.push(object);
            check = true;
          }
        });
        if (!check) {
          var object = {
            _id: c.category._id, categoryName: c.category.categoryName, isSemiProcessed: c.category.isSemiProcessed,
            item: [{_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number}],
            type: "MenuItem"
          };
          uniqueListOfProcessedCategories.push(object);
        }
      });
      var uniqueListOfSemiProcessedCategories = [];
      //console.log("process in func", $scope.processedCat.item)
      _.forEach($scope.processedSemi_Cat.item, function (c, i) {
        //console.log("COMP", c)
        var check = false;
        _.forEach(uniqueListOfSemiProcessedCategories, function (u, i) {
          if (u._id == c.category._id) {
            var object = {_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number};
            u.item.push(object);
            check = true;
          }
        });
        if (!check) {
          var object = {
            _id: c.category._id, categoryName: c.category.categoryName, isSemiProcessed: c.category.isSemiProcessed,
            item: [{_id: c._id, itemName: c.itemName, units: c.units, unit: c.unit, number: c.number}],
            type: "IntermediateItem"
          }
          uniqueListOfSemiProcessedCategories.push(object);
        }
      });
      //console.log("PROCESSED created", uniqueListOfProcessedCategories);
      $scope.processedItemsByCategory = uniqueListOfProcessedCategories;
      //console.log("SEMI PROCESSED CREATED", uniqueListOfSemiProcessedCategories);
      $scope.semiprocessedItemsByCategory = uniqueListOfSemiProcessedCategories;
      $scope.allItemsByCategories = [];
      _.forEach($scope.processedItemsByCategory, function (c, i) {
        $scope.allItemsByCategories.push(c);
      });
      _.forEach($scope.semiprocessedItemsByCategory, function (c, i) {
        $scope.allItemsByCategories.push(c)
      });
      //console.log("ALL ITEMS BY CATEGORIES", $scope.allItemsByCategories)
      //$scope.copiedAllItemsByCategories = angular.copy($scope.allItemsByCategories)
    };

    /*  var getDeployment=Deployment.get({tenant_id:currentUser.tenant_id}).$promise.then( function (dep){
     $scope.deployment=dep;
     $scope.DepForVendor = angular.copy(dep)
     _.forEachRight($scope.DepForVendor, function(d, i) {
     if(d._id == currentUser.deployment_id || d.isMaster || !d.isBaseKitchen){
     $scope.DepForVendor.splice(i, 1);
     }
     });

     var allPromise=$q.all([
     getStockCategories_ByDeployment(),getStockUnits_ByDeployment(),
     getStockItems_ByDeployment() ,getVendors_ByDeployment(),getReceivers_ByDeployment(),
     getstockRecies(),getItems_ProcessedFood(),getItems_SemiProcessedFood(), getAllDeployments()
     ]);

     allPromise.then(function (value){
     console.log(value);
     $scope.stockCategories = value[0];
     console.log($scope.stockCategories);
     $scope.stockCategories_Category=value[0];
     $scope.stockUnits=value[1];
     console.log($scope.stockUnits)
     $scope.totalUnits = $scope.stockUnits.length;
     $scope.stockItems=value[2];
     enableItemGroups();
     $scope.vendors=value[3];
     $scope.receivers=value[4];
     $scope.receipes=value[5];
     $scope.processedCat.item=value[6];
     console.log("DEPLO", $scope.deployment)
     $scope.processedSemi_Cat.item=value[7];
     $scope.allDeployments = value[8];
     console.log("ALL DEPLOYMENTS", value[8]);
     console.log("PROCESSED LIST OF ITEMS",value[6]);
     console.log("SEMI PROCESSED LIST OF ITEMS ", value[7])
     bindUnitsForProcessedFood($scope.processedCat.item);
     bindUnitsForProcessedFood($scope.processedSemi_Cat.item);
     store.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
     $scope.stores=result;
     hideKitchenAndMainStore();
     storePagination();
     getItemsByCategory();
     filterKitchenStores();
     structringCategorisedData();
     });
     });

     });*/


    function CatWithSemiProcessed(ss) {
      _.forEach(ss, function (cat, i) {
        if (cat.isSemiProcessed == undefined || cat.isSemiProcessed == false) {
          $scope.categories.push(cat);
        }
        else {
          $scope.spCategories.push(cat);
        }
      });
      addToCategory_SemiProcessed($scope.spCategories);
    };

    function bindUnitsForProcessedFood(items) {
      _.forEach(items, function (itm, i) {
        if (itm.unit != undefined) {
          itm.units = [];
          var baseUnitId = itm.unit.baseUnit.id;
          _.forEach($scope.stockUnits, function (u, i) {
            if (u.baseUnit.id == baseUnitId) {
              itm.units.push(u);
            }
          });
        }
      });
    };

    function processedAndSemiItems(items) {
      $scope.processedCat = {_id: "PosistTech111111", categoryName: "Processed Food"};
      $scope.processedSemi_Cat = {_id: "PosistTech222222", categoryName: "InterMediate Food"};
      $scope.processedCat.item = [];
      $scope.processedSemi_Cat.item = [];

      _.forEach(items, function (itm, i) {
        itm.itemName = itm.name;
        itm.units = [];
        if (itm.unit != undefined) {
          var baseUnitId = itm.unit.baseUnit.id;
          _.forEach($scope.stockUnits, function (u, i) {
            if (u.baseUnit.id == baseUnitId) {
              itm.units.push(u);
            }
          });
        }
        else {
          var obj = {_id: '5604fbafeb29d0c80a722280', unitName: 'pack', conversionFactor: 1};
          itm.units.push(obj);
        }

        if (itm.category.isSemiProcessed == true) {
          $scope.processedSemi_Cat.item.push(itm);
        }
        else {
          $scope.processedCat.item.push(itm);
        }
      });
    };

    function addToCategory_SemiProcessed(cat) {
      _.forEach(cat, function (c, i) {
        c.categoryName = c.categoryName + "  (Semi-Processed)";
        $scope.categories.push(c);
      });
    };

    $scope.op_trans = [];
    function getOpentransaction(allTRans) {
      _.forEach(allTRans, function (t, i) {
        if (t.transactionType == "6") {
          $scope.op_trans.push(t);
        }
      });
    };

    function getOpen_TransactionNumber() {
      $rootScope.db.selectDynamic("select max(transactionNumber) as tNo from stockTransaction where transactionType=6 and tenant_id='" + currentUser.tenant_id + "' and deployment_id='" + currentUser.deployment_id + "'").then(function (r) {
        //console.log(r.rows[0].tNo);
        if (r.rows[0].tNo != null) {
          var n = parseFloat(r.rows[0].tNo) + 1;
        }
        else {
          var n = 1;
        }
        $scope.getOpen_TransactionNumber = n;
        //console.log($scope.getOpen_TransactionNumber);
        //console.log(r);
      });
    };

    $scope.storeForm = {
      isKitchen: false,
      isMain: false,
      isEdit: false
    };

    $scope.storePagination = Pagination.getNew(10);
    function storePagination() {
      $scope.storePagination.numPages = Math.ceil($scope.stores.length / $scope.storePagination.perPage);
      $scope.storePagination.totalItems = $scope.stores.length;
    };

    $scope.clearStoreTab = function () {
      $scope.storeForm = {
        isKitchen: false,
        isMain: false,
        isEdit: false
      };
      hideKitchenAndMainStore();
      storePagination();
    };

    function hideKitchenAndMainStore() {
      _.forEach($scope.stores, function (s) {
        // if(s.isKitchen==true){
        //   $scope.storeForm.hideKitchen=true;
        // }
        if (s.isMain == true) {
          $scope.storeForm.hideMainStore = true;
          return;
        }
      });
    };

    function checkIsKitchenAndMainStorePresentForUpdate(sa) {
      _.forEach($scope.stores, function (s) {
        // if(s.isKitchen==true){
        //   $scope.storeForm.hideKitchen=true;
        //   if(sa.isKitchen==true){
        //     $scope.storeForm.hideKitchen=false;
        //   }
        // }
        if (s.isMain == true) {
          $scope.storeForm.hideMainStore = true;
          if (sa.isMain == true) {
            $scope.storeForm.hideMainStore = false;
          }
        }
      });
    };

    $scope.changeKitchenToTransfer = function (selectedKitchen) {
      $timeout(function () {
        $scope.storeForm.categoryToTransfer = [];
        //console.log("CHANGE KITCHEN CALL")
        _.forEachRight($scope.allItemsByCategories, function (c, i) {
          var found = false;
          _.forEach($scope.copiedAllItemsByCategories, function (s, i) {
            if (s._id == c._id) {
              found = true;
              return
            }
          })
          if (!found) {
            $scope.copiedAllItemsByCategories.push(c);
          }
        })
        //console.log("on change",  angular.copy($scope.copiedAllItemsByCategories ))
        if (!$scope.storeForm.transfer)
          $scope.storeForm.transfer = {}
        if (!selectedKitchen)
          selectedKitchen = $scope.storeForm.transfer.toKitchen;
        $scope.storeForm.transfer.toKitchen = selectedKitchen;
        //console.log($scope.storeForm.transfer.toKitchen);
        var index_Str = _.findIndex($scope.stores, {_id: selectedKitchen._id});
        $scope.selectedStoreReference = $scope.stores[index_Str];
        // _.forEach($scope.stores, function(s, i){
        //   if(s.isKitchen && s._id == selectedKitchen._id){
        //     $scope.selectedStoreReference = s;
        //     return;
        //   }
        // })
        //console.log("SELECTED KITCHEN", $scope.selectedStoreReference)
        //console.log("KITCHENC CHANGED, TIME TO UPDATE CATS", angular.copy($scope.copiedAllItemsByCategories))
        _.forEachRight($scope.copiedAllItemsByCategories, function (c, i) {
          //console.log("c check assigned kitchen", c)
          if (c.assignedToKitchen) {
            //console.log("whote category object", c)
            //console.log("asignd", c.assignedToKitchen._id )
            if (c.assignedToKitchen == selectedKitchen._id) {
              //console.log("splicing", $scope.copiedAllItemsByCategories[i] )
              $scope.copiedAllItemsByCategories.splice(i, 1);//////// recom
              //console.log("SPLICING", $scope.copiedAllItemsByCategories[i])
            }
          }
        })
        //console.log('all items by categories copied AFTER SPLICING', $scope.copiedAllItemsByCategories)
      }, 10);

    };

    $scope.unAssignCategoryFromKitchen = function (id, index) {
      $scope.storeForm.transferDeleteDisabled = true;
      $scope.storeForm.transferDisabled = true;
      $scope.storeForm.categoryToTransfer = [];
      //console.log("DELETE CALLED")
      var tempStore;
      _.forEachRight($scope.storeForm.transfer.toKitchen.processedFoodCategory, function (s, i) {
        if (s._id == id) {
          tempStore = angular.copy($scope.selectedStoreReference);
          tempStore.processedFoodCategory.splice(i, 1);
          $scope.copiedAllItemsByCategories.push(s)///recom
          store.update({}, tempStore, function (st) {
            //console.log("check for asigned to kitchen", s)
            s.assignedToKitchen = {}
            delete s['assignedToKitchen']
            $scope.selectedStoreReference.processedFoodCategory.splice(i, 1);
            //console.log("pushing back in avalable", s)
            $scope.storeForm.transferDeleteDisabled = false;
            $scope.storeForm.transferDisabled = false;
          })
        }
      })
      // //console.log("LIST OF KITCENS", $scope.listOfKitchens)
      // var kit = angular.copy($scope.listOfKitchens)
      // _.forEach(kit, function(k, i){
      //   if(k.processedFoodCategory){
      //     _.forEachRight(k.processedFoodCategory, function(pf, j){
      //       console.log("comparing id " + id + "with " + pf._id)
      //       console.log("PF ", pf)
      //       if(pf._id == id){
      //         k.processedFoodCategory.splice(j, 1)
      //         store.update({}, k, function(st){
      //           console.log("earlier store containeing PF UPDATED", st)
      //           $scope.storeForm.transferDeleteDisabled=false;
      //         })
      //       }
      //     })
      //   }
      // })
      // console.log("updated ",  $scope.copiedAllItemsByCategories)
    };

    $scope.transferCategories = function () {
      $scope.storeForm.transferDeleteDisabled = true;
      $scope.storeForm.transferDisabled = true;
      for (var prop in $scope.storeForm.categoryToTransfer) {
        if ($scope.storeForm.categoryToTransfer[prop]) {
          _.forEachRight($scope.copiedAllItemsByCategories, function (ct, l) {
            if (ct._id == prop) {
              $scope.selectedStoreReference.processedFoodCategory.push(ct);
              $scope.copiedAllItemsByCategories.splice(l, 1);
              var store_index = _.findIndex($scope.stores, {_id: $scope.selectedStoreReference._id});
              if (store_index >= 0) {
                store.update({}, $scope.selectedStoreReference, function (updateStore) {
                  $scope.stores[store_index] = updateStore;
                  $scope.stores[store_index] = $scope.selectedStoreReference;
                  $scope.storeForm.transferDeleteDisabled = false;
                  $scope.storeForm.transferDisabled = false;
                })
              }
              //return
            }
          });
          // console.log("after deleeting", $scope.copiedAllItemsByCategories)
          // if($scope.storeForm.categoryToTransfer[prop]){
          //   _.forEachRight($scope.stores, function(st, i){
          //     if(st._id == $scope.selectedStoreReference._id){
          //       store.update({},$scope.selectedStoreReference , function(updateStore){
          //         $scope.stores[i] = updateStore;
          //         $scope.stores[i]= $scope.selectedStoreReference;
          //       })
          //     }
          //     else{
          //       _.forEachRight(st.processedFoodCategory, function(pf, j){
          //         if(pf._id == prop){
          //           st.processedFoodCategory.splice(j, 1);
          //           var storeCopy = angular.copy(st);
          //           store.update({},storeCopy , function(updateStore){
          //             $scope.stores[i] = updateStore
          //             $scope.storeForm.categoryToTransfer[prop]= false;
          //           })
          //           return
          //         }
          //       })
          //     }
          //   })
          // }
        }
      }
      $scope.storeForm.categoryToTransfer = [];
      structringCategorisedData(true);
    };

    $scope.createStore = function (formStore) {
      if (formStore.$valid && !$scope.storeForm.isEdit) {
        $scope.storeForm.tenant_id = currentUser.tenant_id;
        $scope.storeForm.deployment_id = $scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        store.saveData({}, $scope.storeForm, function (result) {
          // Remove all error markers
          clearMongooseError(formStore);
          if (result.error) {
            onMongooseError(result, formStore);
          }
          else {
            $scope.stores.push(result);
            $rootScope.stores.push(result);
            // var str=[];
            // str.push(result);
            // stockSync.InsertStores(str,0);
            console.log($scope.deployment);
            $scope.storeForm = {
              isKitchen: false,
              isMain: false,
              isEdit: false
            };
            hideKitchenAndMainStore();
            growl.success('Successfully Created', {ttl: 3000});
            //alert("Successfully Created");
            formStore.$setPristine();
            storePagination();
          }
        });
      }
      else {
        if ($scope.isKitchen) {
          if (!$scope.storeForm.isKitchen) {
            if ($scope.storeForm.processedFoodCategory && $scope.storeForm.processedFoodCategory.length > 0) {
              alert('Please unassign categories from store first!');
            }
            else {
              store.update({}, $scope.storeForm, function (result) {
                clearMongooseError(formStore);
                if (result.error) {
                  onMongooseError(result, formStore);
                }
                else {
                  var id = _.findIndex($scope.stores, {_id: result._id});
                  $scope.stores.splice(id, 1);
                  $scope.stores.push(result);
                  $scope.storeForm = {
                    isKitchen: false,
                    isMain: false,
                    isEdit: false
                  };
                  hideKitchenAndMainStore();
                  UpdateCategoryOnStoreChange(result);
                  updatevendornStoreChange(result);
                  updateReciverOnStoreChange(result);
                  var rindex = _.findIndex($rootScope.stores, {_id: result._id});
                  if (rindex >= 0) {
                    //$rootScope.stores.splice(rindex,1);
                    $rootScope.stores[rindex] = result;
                    console.log($rootScope.stores);
                  }
                  growl.success('Successfully Updated', {ttl: 3000});
                  //alert("Successfully Updated");
                  console.log(formStore);
                  formStore.$setPristine();
                }
              });
            }
          } else {
            store.update({}, $scope.storeForm, function (result) {
              clearMongooseError(formStore);
              if (result.error) {
                onMongooseError(result, formStore);
              }
              else {
                var id = _.findIndex($scope.stores, {_id: result._id});
                $scope.stores.splice(id, 1);
                $scope.stores.push(result);
                $scope.storeForm = {
                  isKitchen: false,
                  isMain: false,
                  isEdit: false
                };
                hideKitchenAndMainStore();
                UpdateCategoryOnStoreChange(result);
                updatevendornStoreChange(result);
                updateReciverOnStoreChange(result);
                var rindex = _.findIndex($rootScope.stores, {_id: result._id});
                if (rindex >= 0) {
                  //$rootScope.stores.splice(rindex,1);
                  $rootScope.stores[rindex] = result;
                  console.log($rootScope.stores);
                }
                growl.success('Successfully Updated', {ttl: 3000});
                //alert("Successfully Updated");
                formStore.$setPristine();
              }
            });
          }
        }
        else {
          store.update({}, $scope.storeForm, function (result) {
            clearMongooseError(formStore);
            if (result.error) {
              onMongooseError(result, formStore);
            }
            else {
              var id = _.findIndex($scope.stores, {_id: result._id});
              $scope.stores.splice(id, 1);
              $scope.stores.push(result);
              $scope.storeForm = {
                isKitchen: false,
                isMain: false,
                isEdit: false
              };
              hideKitchenAndMainStore();
              UpdateCategoryOnStoreChange(result);
              updatevendornStoreChange(result);
              updateReciverOnStoreChange(result);
              var rindex = _.findIndex($rootScope.stores, {_id: result._id});
              if (rindex >= 0) {
                //$rootScope.stores.splice(rindex,1);
                $rootScope.stores[rindex] = result;
                console.log($rootScope.stores);
              }
              growl.success('Successfully Updated', {ttl: 3000});
              //alert("Successfully Updated");
              console.log(formStore);
              formStore.$setPristine();
            }
          });
        }
      }
    };

    function updateReciverOnStoreChange(str) {
      _.forEach($scope.receivers, function (rec, i) {
        _.forEachRight(rec.pricing.store, function (s, ii) {
          if (s._id == str._id) {
            rec.pricing.store[ii].storeName = s.storeName;
            receiver.update({}, rec);
          }
        });
      });
    };

    function updatevendornStoreChange(str) {
      _.forEach($scope.vendors, function (v, i) {
        // _.forEachRight(rec.pricing.store,function(s,ii){
        if (v.pricing._id == str._id) {
          v.pricing.storeName = str.storeName;
          vendor.update({}, v);
        }
        //});
      });
    };

    $scope.editStore = function (s) {
      $scope.storeForm = angular.copy(s);
      $scope.storeForm.isEdit = true;
      $scope.isKitchen = s.isKitchen;
      checkIsKitchenAndMainStorePresentForUpdate(s);
      //hideKitchenAndMainStore();
    };

    function UpdateCategoryOnStoreChange(s) {
      _.forEach($scope.stockCategories, function (c) {
        if (c.store._id == s._id) {
          c.store = s;
          stockCategory.update({}, c, function (result) {
            //console.log(result);
          });
        }
      });
    };

    function checkIsStoreAssigned(storeId) {
      var flag = false;
      _.forEach($scope.stockCategories, function (cat, i) {
        if (cat.store != undefined) {
          if (cat.store._id == storeId) {
            flag = true;
          }
        }
      });
      _.forEach($scope.vendors, function (v, i) {
        if (v.pricing != undefined) {
          if (v.pricing._id == storeId) {
            flag = true;
          }
        }
      });
      return flag;
    };

    $scope.removeStore = function (s) {
      var check = checkIsStoreAssigned(s._id);
      if (check == false) {
        store.remove({}, s, function (result) {
          var index = _.findIndex($scope.stores, {_id: result._id});
          $scope.stores.splice(index, 1);
          storePagination();
          var rindex = _.findIndex($rootScope.stores, {_id: result._id});
          $rootScope.stores.splice(rindex, 1);
          //DeleteVendorOnStoreDelete(s._id);
          $scope.clearStoreTab();
        });
      }
      else {
        growl.error('Used Store can not be deleted', {ttl: 3000});
        //alert("Used Store can not be deleted");
        //$scope.storeForm.isMessage=true;
        //$scope.storeForm.message="Used Store can not be deleted";
      }
    };

    function DeleteVendorOnStoreDelete(storeId) {
      _.forEachRight($scope.vendors, function (v, i) {
        if (v.pricing._id == storeId) {
          vendor.remove({}, v, function (result) {
            $scope.vendors.splice(i, 1);
          });
        }
      });
    };
    $scope.clearStoreForm = function () {
      $scope.storeForm = {
        isKitchen: false,
        isMain: false,
        isEdit: false
      };
      hideKitchenAndMainStore();
      formStore.$setPristine();
    };

    $scope.checkAllStore = function () {
      if (!$scope.storeSelectAll) {
        $scope.storeSelectAll = true;
      }
      else {
        $scope.storeSelectAll = false;
      }
      angular.forEach($scope.stores, function (s) {
        s.selected = $scope.storeSelectAll;
      });
    };

    $scope.removeSelectedStore = function () {
      //var s=angular.copy();
      _.forEach($scope.stores, function (s, i) {
        if (s.selected == true) {
          store.remove({}, s, function (result) {
            var index = _.findIndex($scope.stores, {_id: result._id});
            $scope.stores.splice(index, 1);
            //console.log(result);
            //console.log(s.selected);
            //console.log(i);
          });
        }

      });
    };
    //-------------------------------------------------------------Stock Category-----------------------------------------
    $scope.stockCategories = [];
    $scope.stockCategoryForm = {
      isEdit: false
    };

    $scope.categoryPagination = Pagination.getNew(10);


    function categoryPagination() {
      $scope.categoryPagination.numPages = Math.ceil($scope.stockCategories_Category.length / $scope.categoryPagination.perPage);
      $scope.categoryPagination.totalItems = $scope.stockCategories_Category.length;
    };

    $scope.clearCategoryTab = function () {
      $scope.stockCategoryForm = {
        isEdit: false
      };
      categoryPagination();
      $scope.bindCategory_stockCategory(undefined);
    };
    function clearMongooseError(formname) {
      // Remove all error markers
      for (var key in formname) {
        if (formname[key] != undefined) {
          if (formname[key].$error) {
            formname[key].$error.mongoose = null;
          }
        }
      }
    };
    function onMongooseError(result, formname) {
      // We got some errors, put them into angular
      for (var key in result.error.errors) {
        formname[key].$error.mongoose = result.error.errors[key].message;
        //console.log(formname[key].$error.mongoose);
      }
    };


    function updateReceiveOnCreateCategory(cat) {
      _.forEach($scope.receivers, function (r, i) {
        if (r.pricing != null) {
          if (r.pricing.store != null) {
            _.forEach(r.pricing.store, function (str, ii) {
              if (str._id == cat.store._id) {
                str.category.push(cat);
              }
              ;
            });
          }
        }
        var recToUpdate = r;
        receiver.update({}, recToUpdate, function (result) {
          $scope.receivers[i] = result;
        });
      });
    };

    $scope.bindCategory_stockCategory = function (store) {
      $scope.stockCategories_Category = [];
      if (store != undefined) {
        $scope.stockCategories_Category = angular.copy(store.category);
        _.forEach($scope.stockCategories_Category, function (c, i) {
          c.store = {};
          c.store.storeName = store.storeName;
          c.store._id = store._id;
        });
      }
      else {
        $scope.stockCategories_Category = $scope.stockCategories;
      }
      categoryPagination();
    };

    $scope.createStockCategory = function (formStockCategory) {
      if (formStockCategory.$valid && !$scope.stockCategoryForm.isEdit) {
        $scope.stockCategoryForm.tenant_id = currentUser.tenant_id;
        $scope.stockCategoryForm.deployment_id = $scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        stockCategory.saveData({}, $scope.stockCategoryForm, function (result) {
          //console.log(result);
          clearMongooseError(formStockCategory);
          if (result.error) {
            onMongooseError(result, formStockCategory);
          }
          else {
            $scope.stockCategories.push(result);
            $scope.stockCategories_Category.push(result);
            var index = _.findIndex($scope.stores, {_id: result.store._id});
            var tempStore = $scope.stores[index];
            var tempresult = angular.copy(result);
            delete tempresult.store;
            tempStore.category.push(tempresult);

            updateReceiveOnCreateCategory(result);
            $scope.stockCategoryForm = {
              isEdit: false
            };
            categoryPagination();

            store.update({}, tempStore, function (s) {
              $scope.stores[index] = s;
              $scope.stockCategoryForm.store = $scope.stores[index];
            });
            growl.success('Successfully Created', {ttl: 3000});
            formStockCategory.$setPristine();
          }
        });
      }
      else {
        stockCategory.update({}, $scope.stockCategoryForm, function (result) {
          clearMongooseError(formStockCategory);
          if (result.error) {
            onMongooseError(result, formStockCategory);
          }
          else {
            var id = _.findIndex($scope.stockCategories, {_id: result._id});
            // $scope.stockCategories.splice(id,1);
            // $scope.stockCategories.push(result);
            $scope.stockCategories[id] = result;
            var cid = _.findIndex($scope.stockCategories_Category, {_id: result._id});
            // $scope.stockCategories_Category.splice(cid,1);
            // $scope.stockCategories_Category.push(result);
            $scope.stockCategories_Category[cid] = result;
            var index = _.findIndex($scope.stores, {_id: result.store._id});
            $scope.stockCategoryForm.store = $scope.stores[index];

            var tempStore = $scope.stores[index];
            var tempresult = angular.copy(result);
            delete tempresult.store;

            var cat_index = _.findIndex(tempStore.category, {_id: result._id});
            if (cat_index < 0) {
              tempStore.category.push(tempresult);
            }
            else {
              // tempStore.category.splice(cat_index,1);
              // tempStore.category.push(tempresult);
              tempStore.category[cat_index] = tempresult;
            }

            $scope.stockCategoryForm = {
              isEdit: false
            };
            categoryPagination();

            store.update({}, tempStore, function (s) {
              $scope.stores[index] = s;
              $scope.stockCategoryForm.store = $scope.stores[index];
            });

            growl.success('Successfully Updated', {ttl: 3000});
            //alert("Successfully Updated");
            formStockCategory.$setPristine();
          }
        });
      }
    };
    $scope.editStockCategory = function (s) {
      $scope.stockCategoryForm = angular.copy(s);
      var index = _.findIndex($scope.stores, {_id: s.store._id});
      $scope.stockCategoryForm.store = $scope.stores[index];
      $scope.stockCategoryForm.isEdit = true;
    };

    function updateVendorCategories(cat) {
      _.forEach($scope.vendors, function (v, i) {
        var flag = false;
        var vendorForUpdate = null;
        _.forEachRight(v.pricing.category, function (c, ii) {
          if (c._id == cat._id) {
            v.pricing.category.splice(ii, 1);
            flag = true;
            vendorForUpdate = $scope.vendors[i];
          }
        });
        if (flag == true) {
          vendor.update({}, vendorForUpdate, function (result) {
            $scope.vendors[i] = result;
          });
        }
      });
    };

    function updateReceiverCategories(cat) {
      _.forEach($scope.receivers, function (v, i) {
        var flag = false;
        var rec = null;
        _.forEachRight(v.pricing.store, function (s, ii) {
          _.forEachRight(s.category, function (c, iii) {
            if (c._id == cat._id) {
              s.category.splice(iii, 1);
              flag = true;
              rec = $scope.receivers[i];
            }
          })
        });
        if (flag == true) {
          receiver.update({}, rec, function (result) {
            $scope.receivers[i] = result;
          });
        }
      });
    }

    function updateStoreOnCategoryRemove(cat) {
      _.forEach($scope.stores, function (str, i) {
        var flag = false;
        var storeToUpdate = null;
        if (str._id == cat.store._id) {
          _.forEachRight(str.category, function (c, ii) {
            if (c._id == cat._id) {
              str.category.splice(ii, 1);
              flag = true;
              storeToUpdate = str;
            }
          });
        }
        if (flag == true) {
          store.update({}, storeToUpdate, function (result) {
            $scope.stores[i] = result;
            //str=result;
          });
        }
      });
    };
    function checkCategoryAssigned(cat) {
      var deferred = $q.defer();
      if (cat.item.length > 0) {
        deferred.resolve(true);
        //hi
      }
      else {
        vendor.getNumVendorsHavingCategory({id: cat._id}, function (result) {
          if (result.length > 0)
            deferred.resolve(true);
          else
            deferred.resolve(false);
        });
      }
      return deferred.promise;
    };

    $scope.removeStockCategory = function (s) {
      checkCategoryAssigned(s).then(function (check) {
        if (check == false) {
          stockCategory.remove({}, s, function (result) {
            var index = _.findIndex($scope.stockCategories, {_id: result._id});
            $scope.stockCategories.splice(index, 1);

            var sindex = _.findIndex($scope.stockCategories_Category, {_id: result._id});
            $scope.stockCategories_Category.splice(sindex, 1);
            updateStoreOnCategoryRemove(result);
            updateVendorCategories(result);
            updateReceiverCategories(result);
            categoryPagination();
          });
        }
        else {
          //item assigned in category
          growl.error('Used category can not be deleted', {ttl: 3000});
          //alert("Used Category can not be deleted");
        }
      });
    };
    $scope.clearStockCategoryForm = function () {
      $scope.stockCategoryForm = {
        isEdit: false
      };
      formStockCategory.$setPristine();
    };

    //--------------------------------------------------------------------Stock Units ----------------------------------------
    $scope.baseUnits = [
      {id: 1, name: "Gram", value: 1},
      {id: 2, name: "Litre", value: 0.001},
      {id: 3, name: "Meter", value: 0.001},
      {id: 4, name: "Pc", value: 1}
    ];
    $scope.unitPagination = Pagination.getNew(10);
    $scope.stockUnits = [];
    $scope.stockUnitsForm = {
      isEdit: false
    };

    function unitPagination() {
      $scope.unitPagination.numPages = Math.ceil($scope.stockUnits.length / $scope.unitPagination.perPage);
      $scope.unitPagination.totalItems = $scope.stockUnits.length;
    };

    $scope.clearUnitsTab = function () {
      $scope.stockUnitsForm = {
        isEdit: false
      };
      unitPagination();
    };


    //--------Pagination
    $scope.itemsPerPage = 1;
    $scope.currentPage = 1;
    //$scope.totalUnits = 64;
    //$scope.currentPage = 4;
    //$scope.maxSizeUnits = 5;
    //$scope.bigTotalItems = 175;
    //$scope.bigCurrentPageUnit = 1;

    //$scope.pageChanged = function() {
    //$scope.currentPage=+$scope.currentPage;
    //};

    // getDeployment.$promise.then( function(dep){
    // stockUnit.get({deployment_id:dep[0]._id}, function (result){
    //   $scope.stockUnits=result;
    // $scope.totalUnits = $scope.stockUnits.length;
    //$scope.$watch('currentPage + itemsPerPage', function() {
    //var begin = (($scope.currentPage - 1) * $scope.itemsPerPage),
    //end = begin + $scope.itemsPerPage;
    //$scope.stockUnits1 = $scope.stockUnits.slice(begin, end);
    //});
    //});
    //})


    //---------End Pagination

    $scope.submitStockUnits = function (formStockUnits) {
      if (formStockUnits.$valid && !$scope.stockUnitsForm.isEdit) {
        $scope.stockUnitsForm.tenant_id = currentUser.tenant_id;
        $scope.stockUnitsForm.conversionFactor = parseFloat($scope.stockUnitsForm.baseConversionFactor) / parseFloat($scope.stockUnitsForm.baseUnit.value);
        $scope.stockUnitsForm.deployment_id = $scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        stockUnit.saveData({}, $scope.stockUnitsForm, function (result) {
          //console.log(result);
          clearMongooseError(formStockUnits);
          if (result.error) {
            onMongooseError(result, formStockUnits);
          }
          else {
            $scope.stockUnits.push(result);
            $scope.stockUnitsForm = {
              isEdit: false
            };
            growl.success('Successfully Created', {ttl: 3000});
            //alert("Successfully Created");
            formStockUnits.$setPristine();
            unitPagination();
          }
        });
      }
      else {
        $scope.stockUnitsForm.conversionFactor = parseFloat($scope.stockUnitsForm.baseConversionFactor) / parseFloat($scope.stockUnitsForm.baseUnit.value);
        stockUnit.update({}, $scope.stockUnitsForm, function (result) {
          clearMongooseError(formStockUnits);
          if (result.error) {
            onMongooseError(result, formStockUnits);
          }
          else {
            var id = _.findIndex($scope.stockUnits, {_id: result._id});
            angular.copy($scope.stockUnitsForm, $scope.stockUnits[id]);
            // $scope.stockUnits.splice(id,1);
            // $scope.stockUnits.push(result);
            $scope.stockUnitsForm = {
              isEdit: false
            };
            UpdateItemsUnitOnUnitChange(result);
            growl.success('Successfully Updated', {ttl: 3000});
            //alert("Successfully Updated");
            formStockUnits.$setPristine();
          }
        });
      }
    };
    $scope.editStockUnits = function (s) {
      $scope.stockUnitsForm = angular.copy(s);
      var index = _.findIndex($scope.baseUnits, {id: s.baseUnit.id});
      $scope.stockUnitsForm.baseUnit = $scope.baseUnits[index];
      $scope.stockUnitsForm.isEdit = true;
    };

    function checkIsUnitAssigned(unitId) {
      var flag = false;
      _.forEach($scope.stockItems, function (itm, i) {
        _.forEach(itm.units, function (u, ii) {
          if (u._id == unitId) {
            flag = true;
          }
        });
      });

      return flag;
    };
    $scope.removeStockUnits = function (s) {
      var check = checkIsUnitAssigned(s._id);
      if (check == false) {
        stockUnit.remove({}, s, function (result) {
          var index = _.findIndex($scope.stockUnits, {_id: result._id});
          $scope.stockUnits.splice(index, 1);
          unitPagination();
        });
      }
      else {
        growl.success('UNIT USED CANNOT BE DELETED', {ttl: 3000});
        //alert("Used Unit can not be delete");
      }
    };
    $scope.clearStockUnitForm = function () {
      $scope.stockUnitsForm = {
        isEdit: false
      };
      stockUnitsForm.$setPristine();
    };

    function UpdateItemsUnitOnUnitChange(u) {
      _.forEach($scope.stockItems, function (t) {
        _.forEach(t.units, function (ui) {
          if (ui._id == u._id) {
            var index = _.findIndex(t.units, {_id: u._id});
            t.units.splice(index, 1);
            t.units.push(u);
            stockItem.update({}, t, function (result) {
              //console.log(t);
            });
          }
        });
      });
    };

    //---------------------------------------------------Stock Items----------------------------------------------------------
    $scope.stockItemsForm = {isEdit: false};

    $scope.stockItems = [];
    $scope.stockItemsPUnit = {};
    $scope.itemPagination = Pagination.getNew(10);

    $scope.sort = function (keyname) {
      $scope.sortKey = keyname;   //set the sortKey to the param passed
      $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    function itemPagination() {
      $scope.itemPagination.numPages = Math.ceil($scope.stockItems.length / $scope.itemPagination.perPage);
      $scope.itemPagination.totalItems = $scope.stockItems.length;
    };

    $scope.clearItemsTab = function () {
      $scope.stockItemsForm = {isEdit: false};
      enableItemGroups();
      itemPagination();
    };

    $scope.submitStockItems = function (formStockItems) {
      if ($scope.stockItemsForm.isFirstAssign)
        $scope.stockItemsForm.isFirstAssign = null;
      if (formStockItems.$valid && !$scope.stockItemsForm.isEdit) {
        if ($scope.stockItemsForm.Unit != null || $scope.stockItemsForm.Unit != undefined) {
          var count = 0;
          $scope.stockItemsForm.units = [];
          for (var prop in $scope.stockItemsForm.Unit) {
            var value = $scope.stockItemsForm.Unit[prop];
            if (value == true) {
              count = count + 1;
              //console.log(value);
              var index = _.findIndex($scope.stockUnits, {_id: prop});
              delete $scope.stockItemsForm.Unit[prop];

              $scope.stockItemsForm.units.push($scope.stockUnits[index]);
            }
            else {
              growl.error('Please select at least one unit to add Item', {ttl: 3000});
              //alert("PLease select at least one unit to add Item");
            }
          }
          //console.log($scope.stockItemsForm.units);
          $scope.stockItemsForm.tenant_id = currentUser.tenant_id;
          $scope.stockItemsForm.deployment_id = $scope.currentUser.deployment_id;//$scope.deployment[0]._id;
          if (count > 0) {
            stockItem.saveData({}, $scope.stockItemsForm, function (result) {
              clearMongooseError(formStockItems);
              if (result.error) {
                onMongooseError(result, formStockItems);
                enableItemGroups();
              }
              else {
                $scope.stockItems.push(result);
                $scope.stockItemsForm = {
                  isEdit: false
                };
                itemPagination();
                growl.success('Successfully Created', {ttl: 3000});
                //alert("Successfully Created");
                formStockItems.$setPristine();
                enableItemGroups();
              }
            });
          }
          else {
            growl.error('Please select at least one unit to add Item', {ttl: 3000});
            //alert("PLease select at least one unit to add Item");
          }
        }
        else {
          growl.error('Please select at least one unit to add Item', {ttl: 3000});
          //alert("PLease select at least one unit to add Item");
        }
      }
      else {
        if ($scope.stockItemsForm.Unit != null || $scope.stockItemsForm.Unit != undefined) {
          var count = 0;
          $scope.stockItemsForm.units = [];
          for (var prop in $scope.stockItemsForm.Unit) {
            var value = $scope.stockItemsForm.Unit[prop];
            if (value == true) {
              count = count + 1;
              //console.log(value);
              var index = _.findIndex($scope.stockUnits, {_id: prop});
              delete $scope.stockItemsForm.Unit[prop];

              $scope.stockItemsForm.units.push($scope.stockUnits[index]);
            }
          }
          if (count > 0) {
            stockItem.update({}, $scope.stockItemsForm, function (result) {
              //console.log(result);
              clearMongooseError(formStockItems);
              if (result.error) {
                onMongooseError(result, formStockItems);
              }
              else {
                var id = _.findIndex($scope.stockItems, {_id: result._id});
                $scope.stockItems.splice(id, 1);
                $scope.stockItems.push(result);
                $scope.stockItemsForm = {
                  isEdit: false
                };
                updateStoreOnCategoryChange(result);
                updateCategoryOnItemChange(result);
                updateVendorsOnItemChange(result);
                updateReceiverOnItemChange(result);
                updateReceipeOnItemChange(result);
                enableItemGroups();
                growl.success('Successfully Updated', {ttl: 3000});
                //alert("Successfully Updated");
                formStockItems.$setPristine();
              }
            });
          }
          else {
            growl.error('Please select at least one unit to add Item', {ttl: 3000});
            //alert("PLease select at least one unit to add Item");
          }
        }
        else {
          growl.error('Please select at least one unit to add Item', {ttl: 3000});
          //alert("PLease select at least one unit to add Item");
        }
      }
    };


    function enableItemGroups() {
      _.forEach($scope.baseUnits, function (b) {
        var n = b.name;
        $scope.stockItemsForm[n] = true;
      });
      delete $scope.stockItemsForm.Unit;
    };

    function selectSelectedPreferredUnit() {
      _.forEach($scope.stockItems, function (st) {
        $scope.stockItems.units = st.units;
        var index = _.findIndex($scope.stockItems.units, {unitName: st.preferedUnit});
        $scope.stockItemsPUnit.preferedUnit = {};
        $scope.stockItemsPUnit.preferedUnit = $scope.stockItems.units[index];
        //console.log($scope.stockItemsPUnit.preferedUnit);
      });

    };
    $scope.clearStockItemForm = function () {
      $scope.stockItemsForm = {isEdit: false};
      enableItemGroups();
    };
    $scope.disableOthersGroup = function (key, id) {
      $scope.stockItemsForm.key = true;
      //$scope.selecteddiv=key;
      //console.log(key);
      var count = 0;
      _.forEach($scope.baseUnits, function (b) {
        if (key != b.name) {
          var n = b.name;
          $scope.stockItemsForm[n] = false;
        }
        else {
          var tf = $scope.stockItemsForm.Unit[id];
        }
      });
      for (var prop in $scope.stockItemsForm.Unit) {
        var value = $scope.stockItemsForm.Unit[prop];
        if (value == true) {
          count = count + 1;
        }
      }
      ;
      if ($scope.stockItemsForm.isFirstAssign) {
        if ($scope.stockItemsForm.isFirstAssign == 1) {
          if (count < 1) {
            enableItemGroups();
          }
        }
      } else {
        if (count < 1) {
          enableItemGroups();
        }
      }
    };
    function disableGroupForEditItems(key) {
      $scope.stockItemsForm.key = true;
      //$scope.selecteddiv=key;
      //console.log(key);
      var count = 0;
      _.forEach($scope.baseUnits, function (b) {
        if (key != b.name) {
          var n = b.name;
          $scope.stockItemsForm[n] = false;
        }
      });
    };

    $scope.editStockItems = function (s) {
      $scope.stockItemsForm = angular.copy(s);
      $scope.stockItemsForm.isEdit = true;
      if (s.units.length > 0) {
        var key = s.units[0].baseUnit.name;
        enableItemGroups();
        disableGroupForEditItems(key);
      }
      else if (!key) {
        enableItemGroups();
      }
      $scope.stockItemsForm.Unit = {};
      _.forEach(s.units, function (u) {
        $scope.stockItemsForm.Unit[u._id] = true;
      });

      if (s.units.length === 0) {
        $scope.stockItemsForm.isFirstAssign = 1;
      } else {
        $scope.stockItemsForm.isFirstAssign = 2;
      }

    };

    function checkIsItemAssigned(item) {
      if (item.assignedToStore.length == 0) {
        return false;
      }
      else {
        return true;
      }
    };

    $scope.removeStockItems = function (s) {
      var check = checkIsItemAssigned(s);
      if (check == false) {
        stockItem.remove({}, s, function (result) {
          var index = _.findIndex($scope.stockItems, {_id: result._id});
          $scope.stockItems.splice(index, 1);
          RemoveItem_CategoryOnItemChange(result);
          RemoveItem_VendorsOnItemChange(result);
          RemoveItem_ReceiverOnItemChange(result);
          RemoveItem_StoreOnItemChange(result);
          RemoveItem_Receipe_OnItemChange(result);
          itemPagination();
        });
      }
      else {
        growl.error('Assign Item can not be deleted', {ttl: 3000});
        //alert("Assign Item can not delete");
      }
    };

    function RemoveItem_CategoryOnItemChange(item) {
      _.forEach($scope.stockCategories, function (c, i) {
        _.forEachRight(c.item, function (itm, ii) {
          if (item._id == itm._id) {
            c.item.splice(ii, 1);
            stockCategory.update({}, c, function (cat) {
            });
          }
        });
      });
    };

    function RemoveItem_StoreOnItemChange(item) {
      _.forEach($scope.stores, function (s, i) {
        _.forEachRight(s.category, function (c, ii) {
          _.forEachRight(c.item, function (itm, iii) {
            if (item._id == itm._id) {
              c.item.splice(iii, 1);
              store.update({}, s);
            }
          });
        });
      });
    };

    function RemoveItem_VendorsOnItemChange(item) {
      _.forEach($scope.vendors, function (v, i) {
        _.forEach(v.pricing.category, function (c, ii) {
          _.forEachRight(c.item, function (itm, iii) {
            if (item._id == itm._id) {
              c.item.splice(iii, 1);
              vendor.update({}, v);
            }
          });
        });
      });
    };

    function RemoveItem_ReceiverOnItemChange(item) {
      _.forEach($scope.receivers, function (r, i) {
        if (r.pricing != undefined) {
          _.forEach(r.pricing.store, function (s, ii) {
            _.forEach(s.category, function (c, iii) {
              _.forEachRight(c.item, function (itm, ii) {
                if (item._id == itm._id) {
                  c.item.splice(iii, 1);
                  receiver.update({}, r);
                }
              });
            });
          });
        }
      });
    };


    function RemoveItem_Receipe_OnItemChange(item) {
      _.forEach($scope.receipes, function (r, i) {
        _.forEachRight(r.receipeDetails, function (d, ii) {
          if (item._id == d._id) {
            r.receipeDetails.splice(ii, 1);
            stockRecipe.update({}, r, function (result) {
              //console.log(result);
            });
          }
        });
      });
    };

    function updateCategoryOnItemChange(item) {
      _.forEach($scope.stockCategories, function (c, i) {
        var flag = false;
        _.forEachRight(c.item, function (itm, ii) {
          if (item._id == itm._id) {
            c.item.splice(ii, 1);
            c.item.push(item);
            flag = true;
          }
        });
        if (flag == true) {
          stockCategory.update({}, c, function (cat) {
            $scope.stockCategories[i] = cat;
            //console.log(cat);
          });
        }
      });
    };

    function updateStoreOnCategoryChange(item) {
      _.forEach($scope.stores, function (s, i) {
        var flag = false;
        _.forEachRight(s.category, function (c, ii) {
          _.forEachRight(c.item, function (itm, iii) {
            if (item._id == itm._id) {
              c.item.splice(iii, 1);
              c.item.push(item);
              flag = true;
            }
          });
        });
        if (flag == true) {
          store.update({}, s, function (ss) {
            $scope.stores[i] = ss;
            //console.log(ss);
          });
        }
      });
    };

    function updateVendorsOnItemChange(item) {
      //console.log(item);
      _.forEach($scope.vendors, function (v, i) {
        var flag = false;
        if (v.pricing != undefined) {
          _.forEach(v.pricing.category, function (c, ii) {
            _.forEachRight(c.item, function (itm, iii) {
              if (item._id == itm._id) {
                // item.price=itm.price;
                // item.fromDate=itm.fromDate;
                // item.toDate=itm.toDate;
                // item.selectedUnitId=itm.selectedUnitId;
                itm.preferedUnit = item.preferedUnit;
                itm.itemName = item.itemName;
                itm.units = item.units;
                //item.selectedUnit=itm.selectedUnit;
                //c.item[iii]=item;
                flag = true;
              }
            });
          });
        }

        if (flag == true) {
          vendor.update({}, v, function (ven) {
            $scope.vendors[i] = ven;
            //console.log(ven);
          });
        }
      });
    };

    function updateReceiverOnItemChange(item) {
      _.forEach($scope.receivers, function (r, i) {
        var flag = false;
        if (r.pricing != undefined) {
          _.forEach(r.pricing.store, function (s, ii) {
            _.forEach(s.category, function (c, iii) {
              _.forEachRight(c.item, function (itm, it) {
                if (item._id == itm._id) {
                  // item.price=itm.price;
                  // item.selectedUnitId=itm.selectedUnitId;
                  // c.item.splice(it,1);
                  // c.item.push(item);
                  itm.preferedUnit = item.preferedUnit;
                  itm.itemName = item.itemName;
                  itm.units = item.units;
                  flag = true;
                }
              });
            });
          });
        }
        if (flag == true) {
          receiver.update({}, r, function (rec) {
            $scope.receivers[i] = rec;
            //console.log(rec);
          });
        }
      });
    };

    function updateReceipeOnItemChange(item) {
      //$scope.receipes
      _.forEach($scope.receipes, function (r, i) {
        var flag = false;
        _.forEach(r.receipeDetails, function (d, ii) {
          if (d._id == item._id) {
            item.quantity = d.quantity;
            item.selectedUnitId = d.selectedUnitId;
            item.selectedUnitName = d.selectedUnitName;
            item.baseQuantity = d.baseQuantity;
            r.receipeDetails.splice(ii, 1);
            r.receipeDetails.push(item);
            flag = true;
          }
        });

        for (var i = 0; i < r.rawItems.length; i++) {
          if (r.rawItems[i]._id == item._id) {
            r.rawItems[i].itemName = item.itemName;
            r.rawItems[i].units = item.units;
          }
        }

        if (flag == true) {
          stockRecipe.update({}, r, function (rec) {
            //console.log(rec);
          });
        }
      });
    };

    $scope.updateReOrderLevel = function (s) {
      stockItem.update({}, s, function (result) {
        //console.log(result);
        updateStoreOnCategoryChange(result);
        updateCategoryOnItemChange(result);
        updateVendorsOnItemChange(result);
        updateReceiverOnItemChange(result);
        updateReceipeOnItemChange(result);
      });
    };

    $scope.updatePreferedUnit = function (punit, s) {
      stockItem.update({}, s, function (result) {
        //console.log(result);
        updateStoreOnCategoryChange(result);
        updateCategoryOnItemChange(result);
        updateVendorsOnItemChange(result);
        updateReceiverOnItemChange(result);
        updateReceipeOnItemChange(result);
      });
    };

    $scope.updateReorderUnit = function (punit, s) {
      stockItem.update({}, s, function (result) {
        //console.log(result);
      });
    };
    //---------------------------------------------------Add Item to STore ---------------------------
    $scope.AssignItemToStoreForm = {};

    $scope.siPagination = Pagination.getNew(10);


    function storeItemsPagination() {
      $scope.siPagination.numPages = Math.ceil($scope.AssignItemToStoreForm.selectedCategory.item.length / $scope.siPagination.perPage);
      $scope.siPagination.totalItems = $scope.AssignItemToStoreForm.selectedCategory.item.length;
    };


    $scope.clearItemsInStoreTab = function () {
      $scope.AssignItemToStoreForm = {};
      $scope.AssignItemToStoreForm.itemsToDelete = [];
      console.log($scope.stores);
      console.log($scope.items);
    };

    $scope.OnStoreChange = function (st) {
      $scope.AssignItemToStoreForm.category = {};
      $scope.AssignItemToStoreForm.selectedCategory = {};
      $scope.AssignItemToStoreForm.items = [];
      $scope.AssignItemToStoreForm.category = st.category;
    };
    function ItemsNotInStore(str) {
      var itemsforTRansfer = [];
      _.forEach($scope.stockItems, function (i) {
        if (i.assignedToStore.length == 0) {
          itemsforTRansfer.push(i);
        }
        else {
          var flag = false;
          for (var j in i.assignedToStore) {
            if (i.assignedToStore[j]._id == str) {
              flag = true;
              break;
            }
          }
          ;

          if (flag == false) {
            itemsforTRansfer.push(i);
          }
        }
      });
      return itemsforTRansfer;
    };
    $scope.OnCategoryChange = function (cat) {
      $scope.AssignItemToStoreForm.items = [];
      $scope.AssignItemToStoreForm.selectedItem = null;
      var test = ItemsNotInStore($scope.AssignItemToStoreForm.store._id);
      //console.log('test', test);
      $scope.AssignItemToStoreForm.items = test;
      $scope.siPagination.page = 0;
      storeItemsPagination();
      var cat_index1 = _.findIndex($scope.stockCategories, {_id: cat._id});
      var temp_cat1 = angular.copy($scope.stockCategories[cat_index1]);
      //console.log(temp_cat1);
    };

    function assignItemToVendors_trasnferToStore(item, cat_id, store_id) {
      _.forEach($scope.vendors, function (v, i) {
        var isItemAddedtoVendor = false;
        if (v.pricing._id == store_id) {
          _.forEach(v.pricing.category, function (cat, ii) {
            if (cat._id == cat_id) {
              var flag = false;
              _.forEach(cat.item, function (itm, iii) {
                if (itm._id == item._id) {
                  flag = true;
                }
              });

              if (flag == false) {
                //add item to category
                var it = angular.copy(item);
                for (var p in it) {
                  if (p != "_id" && p != "itemName" && p != "preferedUnit") {
                    _.forEach(it.units, function (u) {
                      for (var p in u) {
                        if (p != "_id" && p != "unitName" && p != "conversionFactor") {
                          delete u[p];
                        }
                      }
                    });
                  }
                }
                it["price"] = "";
                it["fromDate"] = "";
                it["toDate"] = "";
                cat.item.push(it);
                isItemAddedtoVendor = true;
              }
            }
          });
        }
        if (isItemAddedtoVendor == true) {
          //Update vendor
          vendor.update({}, v, function (result) {
            var index = _.findIndex($scope.vendors, {_id: result._id});
            //$scope.vendors.splice(index,1);
            $scope.vendors[index] = result;
          });
        }
      });
    };
    function assignItemToREceivers_trasnferToStore(item, cat_id, store_id) {
      _.forEach($scope.receivers, function (v, i) {
        var isItemAddedtoVendor = false;
        if (v.pricing == undefined) {
          v.pricing = {store: []}
        }
        ;
        _.forEach(v.pricing.store, function (str, j) {
          if (str._id == store_id) {
            _.forEach(str.category, function (cat, ii) {
              if (cat._id == cat_id) {
                var flag = false;
                _.forEach(cat.item, function (itm, iii) {
                  if (itm._id == item._id) {
                    flag = true;
                  }
                });
                if (flag == false) {
                  //add item to category
                  var it = angular.copy(item);
                  for (var p in it) {
                    if (p != "_id" && p != "itemName" && p != "preferedUnit") {
                      _.forEach(it.units, function (u) {
                        for (var p in u) {
                          if (p != "_id" && p != "unitName" && p != "conversionFactor") {
                            delete u[p];
                          }
                        }
                      });
                    }
                  }
                  it["price"] = "";
                  //it["fromDate"]="";
                  //it["toDate"]="";
                  cat.item.push(it);
                  isItemAddedtoVendor = true;
                }
              }
            });
          }
        });
        if (isItemAddedtoVendor == true) {
          //Update receiver
          receiver.update({}, v, function (result) {
            var index = _.findIndex($scope.receivers, {_id: result._id});
            $scope.receivers.splice(index, 1);
            $scope.receivers.push(result);
          });
        }
      });
    };

    $scope.test = {};

    $scope.transferToStore = function (formAssignItemToStore) {
      $scope.test.callingApi = true;
      $rootScope.showLoaderOverlay = true;
      $scope.AssignItemToStoreForm.isSubmitted = true;
      var isP = _.findIndex($scope.AssignItemToStoreForm.selectedCategory.item, {_id: $scope.AssignItemToStoreForm.selectedItem._id});
      var temp_Item = null;
      if (isP < 0) {
        var item_Index = _.findIndex($scope.stockItems, {_id: $scope.AssignItemToStoreForm.selectedItem._id});
        var temp_Item = angular.copy($scope.stockItems[item_Index]);
        if (temp_Item.units.length > 0) {
          var cat_index1 = _.findIndex($scope.stockCategories, {_id: $scope.AssignItemToStoreForm.selectedCategory._id});
          var temp_cat1 = angular.copy($scope.stockCategories[cat_index1]);
          temp_Item.assignedToStore.push({
            _id: $scope.AssignItemToStoreForm.store._id,
            storeName: $scope.AssignItemToStoreForm.store.storeName
          });
          //console.log('temp_Item', angular.copy(temp_Item));
          $scope.AssignItemToStoreForm.selectedCategory.item.push(temp_Item);
          stockItem.update({}, temp_Item, function (result_item) {
            console.log('result_item', angular.copy(result_item));
            $scope.stockItems[item_Index] = result_item;
          }, function (err) {
            $rootScope.showLoaderOverlay = false;
            growl.error("Some error occured!", {ttl: 3000});
          });

          var cat_index = _.findIndex($scope.stockCategories, {_id: $scope.AssignItemToStoreForm.selectedCategory._id});
          var temp_cat = null;
          if (cat_index >= 0) {
            var temp_cat = angular.copy($scope.stockCategories[cat_index]);
            temp_cat.item.push(temp_Item);
            delete $scope.AssignItemToStoreForm.selectedItem;
            stockCategory.update({}, temp_cat, function (result_Cat) {
              if (result_Cat.error == undefined) {
                $scope.stockCategories[cat_index] = result_Cat;
                $scope.OnCategoryChange(result_Cat);
              }
            });
          }

          var store_index = _.findIndex($scope.stores, {_id: $scope.AssignItemToStoreForm.store._id});
          if (store_index >= 0) {
            var temp_store = angular.copy($scope.stores[store_index]);
            var cIndex = _.findIndex(temp_store.category, {_id: $scope.AssignItemToStoreForm.selectedCategory._id});
            //splice category from store first
            temp_store.category.splice(cIndex, 1);
            //now push the new updated category to store
            var rCat = angular.copy(temp_cat);
            delete rCat.store;
            temp_store.category.push(rCat);
            //update the store with category and  item
            store.update({}, temp_store, function (result_store) {
              $scope.stores[store_index] = result_store;
              $scope.AssignItemToStoreForm.store = $scope.stores[store_index];
              //$scope.AssignItemToStoreForm ={};

              //add item to vendor and receiver
              assignItemToVendors_trasnferToStore(temp_Item, temp_cat._id, result_store._id);
              assignItemToREceivers_trasnferToStore(temp_Item, temp_cat._id, result_store._id);
              //console.log(result_item);
              //console.log(result_Cat._id);
              //console.log(result_store._id);
              $scope.AssignItemToStoreForm.isSubmitted = false;
              formAssignItemToStore.$setPristine();
              growl.success('Successfully Transfered', {ttl: 3000});
              $scope.test.callingApi = false;
              //alert("Successfully Transfered");
            });
          }
          $rootScope.showLoaderOverlay = false;
        }
        else {
          growl.error('Item does not have any units defined. Please define unit first.', {ttl: 3000});
          $scope.AssignItemToStoreForm.isSubmitted = false;
        }
      }
      else {
        growl.error('Item already present', {ttl: 3000});
      }
    };


    function removeTransferedStockItems_fromVendors(item, cat_id, store_id) {
      _.forEach($scope.vendors, function (v, i) {
        var isItemAddedtoVendor = false;
        if (v.pricing._id == store_id) {
          _.forEachRight(v.pricing.category, function (cat, ii) {
            if (cat._id == cat_id) {
              var flag = false;
              _.forEachRight(cat.item, function (itm, iii) {
                if (itm._id == item._id) {
                  flag = true;
                  cat.item.splice(iii, 1);
                }
              });
              if (flag == true) {
                isItemAddedtoVendor = true;
              }
            }
          });
        }
        if (isItemAddedtoVendor == true) {
          //Update vendor
          vendor.update({}, v, function (result) {
            var index = _.findIndex($scope.vendors, {_id: result._id});
            //$scope.vendors.splice(index,1);
            $scope.vendors[index] = result;
          });
        }
      });
    };
    function removeTransferedStockItems_fromReceivers(item, cat_id, store_id) {
      _.forEachRight($scope.receivers, function (v, i) {
        var isItemAddedtoVendor = false;
        _.forEachRight(v.pricing.store, function (str, j) {
          if (str._id == store_id) {
            _.forEachRight(str.category, function (cat, ii) {
              if (cat._id == cat_id) {
                var flag = false;
                _.forEachRight(cat.item, function (itm, iii) {
                  if (itm._id != undefined) {
                    if (itm._id == item._id) {
                      flag = true;
                      cat.item.splice(iii, 1);
                    }
                  }
                });
                if (flag == true) {
                  isItemAddedtoVendor = true;
                }
              }
            });
          }
        });

        if (isItemAddedtoVendor == true) {
          //Update receiver
          receiver.update({}, v, function (result) {
            var index = _.findIndex($scope.receivers, {_id: result._id});
            $scope.receivers.splice(index, 1);
            $scope.receivers.push(result);
          });
        }
      });
    };

    $scope.removeTransferedStockItems = function (s, str) {

      var ind = Utils.arrayObjectIndexOf($scope.stockItems, $scope.AssignItemToStoreForm.selectedCategory.item._id, s._id);
      //console.log(ind);
      var item_Index = _.findIndex($scope.stockItems, {_id: s._id});
      ////console.log(temp_Item);
      var temp_Item = angular.copy($scope.stockItems[item_Index]);
      var index_Str = null;//_.findIndex(temp_Item.assignedToStore,str.storeName);
      for (var i = 0; i <= temp_Item.assignedToStore.length - 1; i++) {
        if (temp_Item.assignedToStore[i]._id == str._id) {
          index_Str = i;
          break;
        }
      }
      //  console.log(index_Str);
      //if(index_Str!=null && index_Str>=0){
      if (index_Str != null && index_Str >= 0) {
        temp_Item.assignedToStore.splice(index_Str, 1);
      }
      var se_index = _.findIndex($scope.AssignItemToStoreForm.selectedCategory.item, {_id: temp_Item._id});
      //
      //
      //
      //
      //
      // console.log();
      stockItem.update({}, temp_Item, function (result_item) {
        $scope.stockItems[item_Index] = result_item;
        $scope.AssignItemToStoreForm.selectedCategory.item.splice(se_index, 1);
      });

      var cat_index = _.findIndex($scope.stockCategories, {_id: $scope.AssignItemToStoreForm.selectedCategory._id});
      var temp_cat = angular.copy($scope.stockCategories[cat_index]);
      var Cat_item_Index = _.findIndex(temp_cat.item, {_id: s._id});
      temp_cat.item.splice(Cat_item_Index, 1);


      stockCategory.update({}, temp_cat, function (result_Cat) {
        if (result_Cat.error == undefined) {
          $scope.stockCategories[cat_index] = result_Cat;
          $scope.OnCategoryChange(result_Cat);

          var store_index = _.findIndex($scope.stores, {_id: result_Cat.store._id});
          var temp_store = angular.copy($scope.stores[store_index]);
          var cIndex = _.findIndex(temp_store.category, {_id: result_Cat._id});
          var temp_cat1 = angular.copy(result_Cat);
          delete temp_cat1.store;
          temp_store.category.splice(cIndex, 1);
          temp_store.category.push(temp_cat1);
          store.update({}, temp_store, function (result_store) {
            $scope.stores[store_index] = result_store;
            $scope.AssignItemToStoreForm.store = $scope.stores[store_index];
            //$scope.AssignItemToStoreForm ={};
            removeTransferedStockItems_fromVendors(s, result_Cat._id, result_store._id);
            removeTransferedStockItems_fromReceivers(s, result_Cat._id, result_store._id);
            growl.success('Successfully Removed', {ttl: 3000});
            //alert("Successfully Removed");
          });
        }

      });
      //}
    };

    $scope.removeTransferedStockItemsAsync = function (str) {

      if ($scope.AssignItemToStoreForm.itemsToDelete.length > 0) {
        $rootScope.showLoaderOverlay = true;
        $scope.test.callingApi = true;
        //console.log('2185', $scope.AssignItemToStoreForm.itemsToDelete);
        var itemsToUpdate = [];
        //console.log('2186', $scope.AssignItemToStoreForm.selectedCategory);
        _.forEach($scope.AssignItemToStoreForm.itemsToDelete, function (item) {
          var itm = _.find($scope.stockItems, function (stItem) {
            return stItem._id == item._id;
          });

          if (itm) {
            var tmp_itm = itm;
            var index = _.findIndex(itm.assignedToStore, function (store) {
              return store._id == str._id;
            });
            if (index > -1) {
              tmp_itm.assignedToStore.splice(index, 1);
            }
            itemsToUpdate.push(itm);
            var se_index = _.findIndex($scope.AssignItemToStoreForm.selectedCategory.item, {_id: tmp_itm._id});
            $scope.AssignItemToStoreForm.selectedCategory.item.splice(se_index, 1);
          }
        });
        //console.log('2206', itemsToUpdate);
        //console.log('2205', $scope.AssignItemToStoreForm.selectedCategory);
        stockItem.updateItemsAsync({}, {items: itemsToUpdate}, function (response) {
        });

        /*var ind=Utils.arrayObjectIndexOf($scope.stockItems,$scope.AssignItemToStoreForm.selectedCategory.item._id, s._id);
         ///console.log(ind);
         var item_Index = _.findIndex($scope.stockItems,{_id:s._id});
         //console.log(temp_Item);
         var temp_Item=angular.copy($scope.stockItems[item_Index]);
         var index_Str=null;//_.findIndex(temp_Item.assignedToStore,str.storeName);
         for(var i=0;i<=temp_Item.assignedToStore.length-1;i++){
         if(temp_Item.assignedToStore[i]._id==str._id){
         index_Str=i;
         break;
         }
         }
         //  console.log(index_Str);
         //if(index_Str!=null && index_Str>=0){
         if(index_Str!=null && index_Str>=0){
         temp_Item.assignedToStore.splice(index_Str,1);
         }
         var se_index=_.findIndex($scope.AssignItemToStoreForm.selectedCategory.item,{_id:temp_Item._id});

         stockItem.update({},temp_Item, function(result_item){
         $scope.stockItems[item_Index]=result_item;
         $scope.AssignItemToStoreForm.selectedCategory.item.splice(se_index,1);
         });*/

        var cat_index = _.findIndex($scope.stockCategories, {_id: $scope.AssignItemToStoreForm.selectedCategory._id});
        var temp_cat = angular.copy($scope.stockCategories[cat_index]);
        //var Cat_item_Index=_.findIndex(temp_cat.item,{_id:s._id});
        //temp_cat.item.splice(Cat_item_Index,1);
        temp_cat.item = [];
        temp_cat.item = $scope.AssignItemToStoreForm.selectedCategory.item;
        //console.log('2240', temp_cat);


        stockCategory.update({}, temp_cat, function (result_Cat) {
          if (result_Cat.error == undefined) {
            $scope.stockCategories[cat_index] = result_Cat;
            $scope.OnCategoryChange(result_Cat);

            var store_index = _.findIndex($scope.stores, {_id: result_Cat.store._id});
            var temp_store = angular.copy($scope.stores[store_index]);
            var cIndex = _.findIndex(temp_store.category, {_id: result_Cat._id});
            var temp_cat1 = angular.copy(result_Cat);
            delete temp_cat1.store;
            temp_store.category.splice(cIndex, 1);
            temp_store.category.push(temp_cat1);
            store.update({}, temp_store, function (result_store) {
              $scope.stores[store_index] = result_store;
              $scope.AssignItemToStoreForm.store = $scope.stores[store_index];
              //$scope.AssignItemToStoreForm ={};
              removeTransferedStockItems_fromVendorsMultipleItems(itemsToUpdate, result_Cat._id, result_store._id);
              removeTransferedStockItems_fromReceiversMultipleItems(itemsToUpdate, result_Cat._id, result_store._id);
              growl.success('Successfully Removed', {ttl: 3000});
              $scope.test.callingApi = false;
              $rootScope.showLoaderOverlay = false;
              //Modified (28/6/2016)
              $scope.AssignItemToStoreForm.itemsToDelete = [];
              //alert("Successfully Removed");
            });
          }

        });
      } else {
        growl.warning("Please select items to be deleted!", {ttl: 3000});
      }
      //}
    };

    function removeTransferedStockItems_fromVendorsMultipleItems(items, cat_id, store_id) {
      //console.log(items);
      _.forEach($scope.vendors, function (v, i) {
        var isItemAddedtoVendor = false;
        //console.log('2276', v);
        if (v.pricing._id == store_id) {
          _.forEachRight(v.pricing.category, function (cat, ii) {
            if (cat._id == cat_id) {
              var flag = false;
              _.forEachRight(cat.item, function (itm, iii) {
                var indx = _.find(items, function (it) {
                  return it._id == itm._id;
                });
                if (indx) {
                  flag = true;
                  cat.item.splice(iii, 1);
                }
              });
              if (flag == true) {
                isItemAddedtoVendor = true;
              }
            }
          });
        }
        //console.log('2295', v, isItemAddedtoVendor);
        if (isItemAddedtoVendor == true) {
          //Update vendor
          vendor.update({}, v, function (result) {
            var index = _.findIndex($scope.vendors, {_id: result._id});
            //$scope.vendors.splice(index,1);
            $scope.vendors[index] = result;
          });
        }
      });
    };
    function removeTransferedStockItems_fromReceiversMultipleItems(items, cat_id, store_id) {
      _.forEachRight($scope.receivers, function (v, i) {
        var isItemAddedtoVendor = false;
        _.forEachRight(v.pricing.store, function (str, j) {
          if (str._id == store_id) {
            _.forEachRight(str.category, function (cat, ii) {
              if (cat._id == cat_id) {
                var flag = false;
                _.forEachRight(cat.item, function (itm, iii) {
                  if (itm._id != undefined) {
                    var indx = _.find(items, function (it) {
                      return it._id == itm._id;
                    });
                    if (indx) {
                      flag = true;
                      cat.item.splice(iii, 1);
                    }
                  }
                });
                if (flag == true) {
                  isItemAddedtoVendor = true;
                }
              }
            });
          }
        });

        if (isItemAddedtoVendor == true) {
          //Update receiver
          receiver.update({}, v, function (result) {
            var index = _.findIndex($scope.receivers, {_id: result._id});
            $scope.receivers.splice(index, 1);
            $scope.receivers.push(result);
          });
        }
      });
    };

    //--------------------------------------------------Create Vendors ----------------------------------------
    $scope.vendors = [];
    $scope.createVendorForm = {
      isEdit: false,
      type: "Local Vendor",
      availableVendor: []
    };
    $scope.venPagination = Pagination.getNew(10);


    function vendorPagination() {
      $scope.venPagination.numPages = Math.ceil($scope.createVendorForm.availableVendor.length / $scope.venPagination.perPage);
      $scope.venPagination.totalItems = $scope.createVendorForm.availableVendor.length;
    };

    $scope.clearVendorTab = function () {
      //getItemsByCategory();

      $scope.createVendorForm = {
        isEdit: false,
        type: "Local Vendor",
        availableVendor: [],
        store: {},
        checks: {
          'hasStore': false,
          'hasDeployment': false
        }
      };
      //$scope.clearVendorPricingTab();
    };
    $scope.OnStoreChange_VendorForm = function (st) {
      $scope.createVendorForm.selectedCategory = {};
      $scope.createVendorForm.category = angular.copy(st.category);
      availableVendorsByStore(st._id);
      vendorPagination();
    };


    function availableVendorsByStore(storeId) {
      $scope.createVendorForm.availableVendor = [];
      _.forEach($scope.vendors, function (v, i) {
        if (v.pricing._id == storeId) {
          $scope.createVendorForm.availableVendor.push($scope.vendors[i]);
        }
      });
    };


    $scope.OnCategoryTypeChange_VendorForm = function (id) {
      // $scope.selectedCatTypeNames=[];
      // $scope.createVendorForm.availableCategories=[];
      // _.forEach($scope.categoryTypes, function(c, i){
      // if(c._id == id){

      //   if($scope.createVendorForm.selectedCategoryType[id] == true)
      //    $scope.availableCategories.push({id:c.catNames});
      //       }
      //})

      // console.log("AVAILABLE", $scope.availableCategories)
      //console.log("CAT TYPE", $scope.categoryTypes)


      //console.log("ID", id)
      _.forEach($scope.categoryTypes, function (c, i) {
        if (c._id == id) {
          var valueCheck = $scope.createVendorForm.selectedCategoryType[id]
          if (valueCheck) {
            //console.log($scope.categoryTypes[i])
            $scope.categoryTypes[i].selectedCatTypeNames = $scope.categoryTypes[i].catNames;
          } else {
            $scope.categoryTypes[i].selectedCatTypeNames = [];

          }
        }


        //console.log("SELECTED CAT TYPE NAMES AND ID",$scope.categoryTypes[i].selectedCatTypeNames)

      })


      //  _.forEach($scope.categoryTypes, function(c, i){
      //   if(c.selectedCatNames && c.selectedCatNames.length>0 )
      //     $scope.createVendorForm.category = c;
      // })

      ////console.log("selected cat names",$scope.categoryTypes.selectedCatNames )
    }


    $scope.vendorChk_Changed = function (id) {
      var val = $scope.createVendorForm.selectedCategory[id];
      if (val == false) {
        delete $scope.createVendorForm.selectedCategory[id];
      }
    };

    $scope.clearCreateVendorForm = function () {
      delete $scope.createVendorForm._id;
      $scope.createVendorForm.store = {};
      $scope.createVendorForm.isEdit = false;
      $scope.createVendorForm.disableStore = false;
      $scope.createVendorForm.type = "Local Vendor";
      $scope.createVendorForm.vendorName = "";
      $scope.createVendorForm.contactPerson = "";
      $scope.createVendorForm.contactNumber = "";
      $scope.createVendorForm.address = "";
      $scope.createVendorForm.tinNumber = "";
      $scope.createVendorForm.serviceTaxNumber = "";
      $scope.createVendorForm.email = "";
      $scope.createVendorForm.selectedCategory = {};
      $scope.createVendorForm.category = [];
      $scope.createVendorForm.isHO = false;
      $scope.createVendorForm.deployments = {};//$scope.DepForVendor[0];
    };

    $scope.depChange_CreateVendor = function (d) {
      if (d != null && d.isBaseKitchen) {
        $scope.createVendorForm.isHO = true;
        $scope.createVendorForm.vendorName = d.name;
        $scope.createVendorForm.type = "HO Vendor";
        $scope.createVendorForm.baseKitchenId = d._id;
        var listOfprocessedFoodCategories = getItems_ProcessedFoodForAnotherDeployment(d._id);
        //console.log("LIST", listOfprocessedFoodCategories)
        listOfprocessedFoodCategories.then(function (data) {
          var categoryWiseItems = getItemsOfAnotherDeploymentByCategory(data);
          //console.log("CATEGORY WISE ITEMS", categoryWiseItems)
          $scope.createVendorForm.category = categoryWiseItems;
        });

      } else if(d!=null && !d.isBaseKitchen){
        $scope.createVendorForm.isOutletVendor = true;
        $scope.createVendorForm.vendorName = d.name;
        $scope.createVendorForm.type = "Outlet Vendor";
        $scope.createVendorForm.baseKitchenId = d._id;
        var listOfprocessedFoodCategories = getItems_AllItemsForAnotherDeployment(d._id);
        //console.log("LIST", listOfprocessedFoodCategories)
        listOfprocessedFoodCategories.then(function (data) {
          var categoryWiseItems = getAllItemsOfAnotherDeploymentByCategory(data[0]);
          var categoryWiseStockItems = getAllItemsOfAnotherDeploymentByCategory(data[1], true);
          //console.log("CATEGORY WISE ITEMS", categoryWiseItems)
          $scope.createVendorForm.category = categoryWiseItems;
          _.forEach(categoryWiseStockItems, function (item) {
            $scope.createVendorForm.category.push(item);
          });
        });
      }
      else {
        delete $scope.createVendorForm.isHO;
        delete $scope.createVendorForm.deployments;
        $scope.createVendorForm.vendorName = undefined;
        $scope.createVendorForm.type = "Local Vendor";
        $scope.OnStoreChange_VendorForm($scope.createVendorForm.store);
      }
    };

    // $scope.depChange_CreateVendor = function(d){
    //   console.log("CALLed", d)
    //   if(d!=null){
    //     $scope.createVendorForm.isHO=true;
    //     $scope.createVendorForm.vendorName=d.name;
    //     $scope.createVendorForm.type="HO Vendor";
    //     if(!$scope.createVendorForm.checks.hasDeployment){
    //       $scope.categoryTypes=[];
    //       var listOfprocessedFoodCategories =  getItems_ProcessedFoodForAnotherDeployment(d._id);
    //       console.log("LIST", listOfprocessedFoodCategories)
    //       $scope.createVendorForm.checks.hasDeployment= true;
    //       $scope.createVendorForm.allcategories=  $scope.categoryTypes;
    //     }
    //   }
    //   else
    //   {
    //     delete $scope.createVendorForm.isHO;
    //     delete $scope.createVendorForm.deployments;
    //     $scope.createVendorForm.type="Local Vendor";
    //     for (var i = $scope.categoryTypes.length -1; i >= 0; i--) {
    //       if ($scope.categoryTypes[i]._id == 1 || $scope.categoryTypes[i]._id == 2) {
    //          $scope.categoryTypes.splice(i, 1)
    //       }
    //     }
    //         $scope.createVendorForm.checks.hasDeployment= false;
    //   }
    // };

    function getItems_AllItemsForAnotherDeployment(deploymentId) {

      var deferred = $q.defer();
      $q.all([getItems_ProcessedFoodForAnotherDeployment(deploymentId), getItems_StockItemsForAnotherDeployment(deploymentId)])
        .then(function (result) {
          deferred.resolve(result);
        }).catch(function (err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    function getItems_StockItemsForAnotherDeployment(deploymentId) {
      var deferred = $q.defer();
      stockItem.get({tenant_id: currentUser.tenant_id, deployment_id: deploymentId}, function(sItems) {
        deferred.resolve(sItems);
      }, function (err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    function getAllItemsOfAnotherDeploymentByCategory (data, isStockItem) {
      var uniqueListOfProcessedCategories = [];
      if(isStockItem) {
        _.forEach(data, function (d) {
          d.type = "StockItem";
        });
        var object = {
          _id: guid(),
          categoryName: 'Stock Items',
          item: data,
          type: "StockItem"
        }
        uniqueListOfProcessedCategories.push(object);
      } else {
        //console.log("process in func custom", data)
        _.forEach(data, function (c, i) {
          //console.log("COMP custom", c)
          var check = false;
          _.forEach(uniqueListOfProcessedCategories, function (u, i) {
            //console.log("U", u)
            //console.log("C", c)
            if (u._id == c.category._id) {
              var object = {
                _id: c._id,
                itemName: c.name,
                units: c.units,
                unit: c.unit,
                number: c.number,
                tabs: c.tabs,
                type: "MenuItem",
                stockQuantity: c.stockQuantity,
                rate: c.rate
              }
              u.item.push(object);
              //console.log("PUSHING IYEM custom", object)
              check = true;
            }
          })
          //console.log("out of loop cusyom")
          //console.log("CHECK custom", check)
          if (!check) {
            //console.log("CHECK FALSE custom", check)
            var object = {
              _id: c.category._id,
              categoryName: c.category.categoryName,
              item: [{
                _id: c._id,
                itemName: c.name,
                units: c.units,
                unit: c.unit,
                number: c.number,
                tabs: c.tabs,
                type: "MenuItem",
                stockQuantity: c.stockQuantity,
                rate: c.rate
              }],
              type: "MenuItem"
            }
            uniqueListOfProcessedCategories.push(object);
            //console.log("PUSHING OBJECT", object)
          }
        });
      }
      return uniqueListOfProcessedCategories
    }

    function formatPricing_beforeCreate(data) {
      var pricing = null;
      for (var p in data) {
        //console.log("check props", data)
        if (p != "_id" && p != "storeName" && p != "category") {
          //console.log("deleting")
          delete data[p];
        }
      }
      //console.log(data)
      //console.log("SELECTED CATEGORY", $scope.createVendorForm.selectedCategory)
      //console.log("DATA CATEGORY", data.category)

      _.forEachRight(data.category, function (s, i) {
        var flag = false;
        var sid = s._id;
        //console.log("SID", s._id)
        for (var prop in $scope.createVendorForm.selectedCategory) {
          //console.log("to match", prop)
          if (prop == sid) {
            flag = true;
            //console.log("MATCHED")
          }
        }
        if (flag == false) {
          //console.log("SPLICING", s)
          data.category.splice(i, 1);
        }
      });
      _.forEachRight(data.category, function (s, i) {
        var flag = false;
        var sid = s._id;
        //console.log("SID", s._id)
        for (var prop in $scope.createVendorForm.category) {
          if (prop._id == sid) {
            flag = true;
          }
        }
        if (flag == false) {
          //console.log("deleteing catergpry",s )
        }
      });
      _.forEach(data.category, function (str) {
        for (var p in str) {
          if (p != "_id" && p != "categoryName" && p != "item") {
            delete str[p];
          }
        }
        if (typeof str.item !== 'undefined') {
          _.forEach(str.item, function (itm) {
            for (var p in itm) {
              if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "units" && p != "baseUnit") {
                delete itm[p];
              }
            }

            _.forEach(itm.units, function (u) {
              for (var p in u) {
                if (p != "_id" && p != "unitName" && p != "conversionFactor") {
                  delete u[p];
                }
              }
            });
            itm["price"] = "";
            itm["fromDate"] = "";
            itm["toDate"] = "";
          });
        }
      });
      $scope.createVendorForm.pricing = data;
    };

    function formatPricing_beforeCreate_BaseKitchen(data) {
      var pricing = null;
      for (var p in data) {
        //console.log("check props", data)
        if (p != "_id" && p != "storeName") {
          //console.log("deleting")
          delete data[p];
        }
      }
      data.category = $scope.createVendorForm.category;
      _.forEachRight(data.category, function (s, i) {
        var flag = false;
        var sid = s._id;
        //console.log("SID", s._id)
        for (var prop in $scope.createVendorForm.selectedCategory) {
          //console.log("to match", prop)
          if (prop == sid) {
            flag = true;
            //console.log("MATCHED")
          }
        }
        if (flag == false) {
          //console.log("SPLICING", s)
          data.category.splice(i, 1);
        }
      });
      _.forEachRight(data.category, function (s, i) {
        var flag = false;
        var sid = s._id;
        //console.log("SID", s._id)
        for (var prop in $scope.createVendorForm.category) {
          if (prop._id == sid) {
            flag = true;
          }
        }
        if (flag == false) {
          //console.log("deleteing catergpry",s )
        }
      });
      _.forEach(data.category, function (str) {
        for (var p in str) {
          if (p != "_id" && p != "categoryName" && p != "item" && p!= "type") {
            delete str[p];
          }
        }
        if (typeof str.item !== 'undefined') {
          _.forEach(str.item, function (itm) {
            for (var p in itm) {
              if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "units" && p != "type") {
                delete itm[p];
              }
            }

            _.forEach(itm.units, function (u) {
              for (var p in u) {
                if (p != "_id" && p != "unitName" && p != "conversionFactor" && p != "baseUnit") {
                  delete u[p];
                }
              }
            });
            itm["price"] = "";
            itm["fromDate"] = "";
            itm["toDate"] = "";
          });
        }
      });
      $scope.createVendorForm.pricing = data;
    };
    function formatPricing_beforeUpdate_BaseKitchen(data, items) {
      var pricing = null;

      for (var p in data) {
        if (p != "_id" && p != "storeName" && p != "category") {
          delete data[p];
        }
      }
      data.category = $scope.createVendorForm.category;
      _.forEachRight(data.category, function (s, i) {
        var flag = false;
        var sid = s._id;
        for (var prop in $scope.createVendorForm.selectedCategory) {
          if (prop == sid) {
            flag = true;
          }
        }
        if (flag == false) {
          data.category.splice(i, 1);
        }
      });

      _.forEach(data.category, function (str, i) {
        for (var p in str) {
          if (p != "_id" && p != "categoryName" && p != "item") {
            delete str[p];
          }
        }

        _.forEach(str.item, function (itm, ii) {
          for (var p in itm) {
            if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "units") {
              delete itm[p];
            }
          }

          _.forEach(itm.units, function (u, iii) {
            for (var p in u) {
              if (p != "_id" && p != "unitName" && p != "conversionFactor" && p != "baseUnit") {
                delete u[p];
              }
            }
          });

          var index = _.findIndex(items, {_id: itm._id});
          if (index >= 0) {
            str.item[ii] = items[index];
          }
          else {
            itm["price"] = "";
            itm["fromDate"] = "";
            itm["toDate"] = "";
          }
        });
      });
      $scope.createVendorForm.pricing = data;
      //console.log(data);
    };
    function formatPricing_beforeUpdate(data, items) {
      var pricing = null;

      for (var p in data) {
        if (p != "_id" && p != "storeName" && p != "category") {
          delete data[p];
        }
      }

      _.forEachRight(data.category, function (s, i) {
        var flag = false;
        var sid = s._id;
        for (var prop in $scope.createVendorForm.selectedCategory) {
          if (prop == sid) {
            flag = true;
          }
        }
        if (flag == false) {
          data.category.splice(i, 1);
        }
      });

      _.forEach(data.category, function (str, i) {
        for (var p in str) {
          if (p != "_id" && p != "categoryName" && p != "item") {
            delete str[p];
          }
        }

        _.forEach(str.item, function (itm, ii) {
          for (var p in itm) {
            if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "units") {
              delete itm[p];
            }
          }

          _.forEach(itm.units, function (u, iii) {
            for (var p in u) {
              if (p != "_id" && p != "unitName" && p != "conversionFactor" && p != "baseUnit") {
                delete u[p];
              }
            }
          });

          var index = _.findIndex(items, {_id: itm._id});
          if (index >= 0) {
            str.item[ii] = items[index];
          }
          else {
            itm["price"] = "";
            itm["fromDate"] = "";
            itm["toDate"] = "";
          }
        });
      });
      $scope.createVendorForm.pricing = data;
      //console.log(data);
    };
    function hasCategorySelected(data) {
      var flag = false;
      for (var k in data) {
        if (data.hasOwnProperty(k)) {
          if (data[k] == true) {
            flag = true;
          }
        }
      }
      return flag;
    };

    function formatPricing_beforeUpdate(data, items) {
      var pricing = null;

      for (var p in data) {
        if (p != "_id" && p != "storeName" && p != "category") {
          delete data[p];
        }
      }

      _.forEachRight(data.category, function (s, i) {
        var flag = false;
        var sid = s._id;
        for (var prop in $scope.createVendorForm.selectedCategory) {
          if (prop == sid) {
            flag = true;
          }
        }
        if (flag == false) {
          data.category.splice(i, 1);
        }
      });

      _.forEach(data.category, function (str, i) {
        for (var p in str) {
          if (p != "_id" && p != "categoryName" && p != "item") {
            delete str[p];
          }
        }

        _.forEach(str.item, function (itm, ii) {
          for (var p in itm) {
            if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "units") {
              delete itm[p];
            }
          }

          _.forEach(itm.units, function (u, iii) {
            for (var p in u) {
              if (p != "_id" && p != "unitName" && p != "conversionFactor") {
                delete u[p];
              }
            }
          });


          var index = _.findIndex(items, {_id: itm._id});
          if (index >= 0) {
            str.item[ii] = items[index];
          }
          else {
            itm["price"] = "";
            itm["fromDate"] = "";
            itm["toDate"] = "";
          }
        });
      });
      $scope.createVendorForm.pricing = data;
      //console.log(data);
    };
    function hasCategorySelected(data) {
      var flag = false;
      for (var k in data) {
        if (data.hasOwnProperty(k)) {
          if (data[k] == true) {
            flag = true;
          }
        }
      }
      return flag;
    };

    function getVendorItemsForComapreBeforeUpdate(data) {
      var items = [];
      _.forEach(data.category, function (c, i) {
        _.forEach(c.item, function (itm, ii) {
          items.push(itm);
        });
      });
      return items;
    };

    $scope.createVendor = function (formCreateVendor) {
      // if($scope.createVendorForm.deployments!=undefined){
      //   var hCat=true;
      // }
      // else
      // {
      var hCat = hasCategorySelected($scope.createVendorForm.selectedCategory);
      //}
      if (hCat == true) {
        if (formCreateVendor.$valid && !$scope.createVendorForm.isEdit) {
          if ($scope.createVendorForm.deployments != undefined) {
            formatPricing_beforeCreate_BaseKitchen(angular.copy($scope.createVendorForm.store));
          }
          else {
            formatPricing_beforeCreate(angular.copy($scope.createVendorForm.store));
          }

          //format before save
          $scope.createVendorForm.tenant_id = currentUser.tenant_id;
          $scope.createVendorForm.deployment_id = $scope.currentUser.deployment_id;//$scope.deployment[0]._id;
          vendor.saveData({}, $scope.createVendorForm, function (result) {
            clearMongooseError(formCreateVendor);
            if (result.error) {
              onMongooseError(result, formCreateVendor);
            }
            else {
              //console.log(result);
              $scope.vendors.push(result);
              $scope.createVendorForm.availableVendor.push(result);
              $rootScope.vendors.push(result);
              growl.success('Successfully Created', {ttl: 3000});
              $scope.clearCreateVendorForm();
              formCreateVendor.$setPristine();
            }
          });
        }
        else {
          var ven_Index = _.findIndex($scope.vendors, {_id: $scope.createVendorForm._id});
          if ($scope.vendors[ven_Index].pricing != undefined) {
            var vitems = getVendorItemsForComapreBeforeUpdate($scope.vendors[ven_Index].pricing);
            if ($scope.createVendorForm.deployments != undefined) {
              formatPricing_beforeUpdate_BaseKitchen(angular.copy($scope.createVendorForm.store), vitems);
            }
            else {
              formatPricing_beforeUpdate(angular.copy($scope.createVendorForm.store), vitems);
            }
          }
          else {
            formatPricing_beforeCreate(angular.copy($scope.createVendorForm.store));
          }
          vendor.update({}, $scope.createVendorForm, function (result) {
            clearMongooseError(formCreateVendor);
            if (result.error) {
              onMongooseError(result, formCreateVendor);
            }
            else {
              //console.log(result);
              var index = _.findIndex($scope.vendors, {_id: result._id});
              $scope.vendors[index] = result;
              var venIndex = _.findIndex($scope.createVendorForm.availableVendor, {_id: result._id});
              $scope.createVendorForm.availableVendor.splice(venIndex, 1);
              $scope.createVendorForm.availableVendor.push(result);

              var rindex = _.findIndex($rootScope.vendors, {_id: result._id});
              if (rindex >= 0) {
                $rootScope.vendors.splice(rindex, 1);
              }
              $rootScope.vendors.push(result);

              growl.success('Successfully updated', {ttl: 3000});
              $scope.clearCreateVendorForm();
              $scope.createVendorForm.isEdit = false;
              $scope.createVendorForm.disableStore = false;
              formCreateVendor.$setPristine();
              //$scope.clearCreateVendorForm();
            }
          });
        }
      }
      else {
        growl.error('Select a category first.', {ttl: 3000});
      }
    };

    $scope.removeVendors = function (s) {
      vendor.remove({}, s, function (result) {
        var index = _.findIndex($scope.createVendorForm.availableVendor, {_id: result._id});
        $scope.createVendorForm.availableVendor.splice(index, 1);

        var venIndex = _.findIndex($scope.vendors, {_id: result._id});
        $scope.vendors.splice(venIndex, 1);

        var rindex = _.findIndex($rootScope.vendors, {_id: result._id});
        if (rindex >= 0) {
          $rootScope.vendors.splice(rindex, 1);
        }
      });
    };

    function selectedCatgory_VendorEdit() {
      _.forEach($scope.createVendorForm.pricing.category, function (v) {
        $scope.createVendorForm.selectedCategory[v._id] = true;
      });
    };

    $scope.editVendors = function (s) {
      $scope.createVendorForm = angular.copy(s);
      var index = _.findIndex($scope.stores, {_id: s.pricing._id});
      $scope.createVendorForm.store = $scope.stores[index];
      $scope.createVendorForm.isEdit = true;
      $scope.createVendorForm.disableStore = true;
      $scope.OnStoreChange_VendorForm($scope.createVendorForm.store);
      var vindex = _.findIndex($scope.DepForVendor, {_id: s.baseKitchenId});
      $scope.createVendorForm.deployments = $scope.DepForVendor[vindex];
      if ($scope.createVendorForm.deployments != undefined) {
        $scope.depChange_CreateVendor($scope.createVendorForm.deployments);
      }
      selectedCatgory_VendorEdit();
    };


    //--------------------------------------------Create Receivers-----------------------------------
    $scope.receivers = [];

    $scope.createReceiverForm = {
      isEdit: false
    };

    $scope.recPagination = Pagination.getNew(10);


    function receiverPagination() {
      $scope.recPagination.numPages = Math.ceil($scope.receivers.length / $scope.recPagination.perPage);
      $scope.recPagination.totalItems = $scope.receivers.length;
    };

    $scope.clearReceiverTab = function () {
      $scope.createReceiverForm = {
        isEdit: false
      };
      $scope.receiverPricingform = {
        disableSubmit: true
      };
      receiverPagination();
      //processedAndSemiItems(angular.copy($scope.items));
      // console.log($scope.processedCat);
      // console.log($scope.processedSemi_Cat);

    };

    $scope.clearCreateReceiverForm = function () {
      $scope.createReceiverForm = {
        isEdit: false
      }
    };

    $scope.createReceiver = function (formCreateReceiver) {
      if (formCreateReceiver.$valid && !$scope.createReceiverForm.isEdit) {
        $scope.createReceiverForm.tenant_id = currentUser.tenant_id;
        $scope.createReceiverForm.deployment_id = $scope.currentUser.deployment_id;//$scope.deployment[0]._id;
        receiver.saveData({}, $scope.createReceiverForm, function (result) {
          clearMongooseError(formCreateReceiver);
          if (result.error) {
            onMongooseError(result, formCreateReceiver);
          }
          else {
            //console.log(result);
            $scope.receivers.push(result);
            $scope.createReceiverForm = {
              isEdit: false
            };
            //alert("Successfully Created");
            $rootScope.receivers.push(result);
            growl.success('Successfully Created', {ttl: 3000});
            formCreateReceiver.$setPristine();
            receiverPagination();
          }
        });
      }
      else {
        receiver.update({}, $scope.createReceiverForm, function (result) {
          clearMongooseError(formCreateReceiver);
          if (result.error) {
            onMongooseError(result, formCreateReceiver);
          }
          else {
            //console.log(result);
            var index = _.findIndex($scope.receivers, {_id: result._id});
            $scope.receivers.splice(index, 1);
            $scope.receivers.push(result);
            var rindex = _.findIndex($rootScope.receivers, {_id: result._id});
            if (rindex >= 0) {
              $rootScope.receivers.splice(rindex, 1);
            }
            $rootScope.receivers.push(result);

            $scope.createReceiverForm = {
              isEdit: false
            };
            growl.success('Successfully Updated', {ttl: 3000});
            //alert("Successfully Updated");
            formCreateReceiver.$setPristine();
          }
        });
      }
    };

    $scope.editReceiver = function (s) {
      $scope.createReceiverForm = angular.copy(s);
      $scope.createReceiverForm.isEdit = true;
    };

    $scope.removeReceiver = function (s) {
      receiver.remove({}, s, function (result) {
        var index = _.findIndex($scope.receivers, {_id: result._id});
        $scope.receivers.splice(index, 1);
        ;

        var rindex = _.findIndex($rootScope.receivers, {_id: result._id});
        if (rindex >= 0) {
          $rootScope.receivers.splice(rindex, 1);
        }

        receiverPagination();
      });
    };

    //-------------------------------------------Receiver Pricing---------------------------------------
    $scope.receiverPricingform = {
      disableSubmit: true
    };

    function formatDataPricing_ReceiverPricing(data) {
      for (var p in data) {
        if (p != "_id" && p != "storeName" && p != "category") {
          delete data[p];
        }
      }

      // _.forEachRight(data.category, function(s,i){
      //   var flag=false;
      //   var sid=s._id;
      //   for(var prop in $scope.createVendorForm.selectedCategory){
      //     if(prop==sid){
      //       flag=true;
      //     }
      //   }
      //   if(flag==false){
      //     data.category.splice(i,1);
      //   }
      // });

      _.forEach(data.category, function (str) {
        for (var p in str) {
          if (p != "_id" && p != "categoryName" && p != "item") {
            delete str[p];
          }
        }

        _.forEach(str.item, function (itm) {
          for (var p in itm) {
            if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "selectedUnitId" && p != "units") {
              delete itm[p];
            }
          }

          _.forEach(itm.units, function (u) {
            for (var p in u) {
              if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                delete u[p];
              }
            }
          });

          itm["price"] = "";
        });
      });
      // if($scope.receiverPricingform.pricing==undefined)
      // {
      //   $scope.receiverPricingform.pricing={};
      //   $scope.receiverPricingform.pricing=data;
      // }
      // else
      // {
      //   $scope.receiverPricingform.pricing=data;
      // }

    };

    function formatPricing_setReceiverPricing(data) {
      var pricing = null;
      if ($scope.receiverPricingform.receiver.pricing != undefined) {
        var flag = false;
        _.forEach($scope.receiverPricingform.receiver.pricing.store, function (s, i) {
          if (data._id == s._id) {
            flag = true;
            //data=angular.copy(s);
          }
        });
        if (flag == false) {
          formatDataPricing_ReceiverPricing(data);
        }
      }
      else {
        formatDataPricing_ReceiverPricing(data);
      }
    };

    function formatstores_recieverPricing(stores) {
      _.forEach(stores, function (store) {
        for (var p in store) {
          if (p != "_id" && p != "storeName" && p != "category") {
            delete store[p];
          }
        }
        _.forEach(store.category, function (str) {
          for (var p in str) {
            if (p != "_id" && p != "categoryName" && p != "item") {
              delete str[p];
            }
          }

          _.forEach(str.item, function (itm) {
            for (var p in itm) {
              if (p != "_id" && p != "itemName" && p != "preferedUnit" && p != "selectedUnitId" && p != "units") {
                delete itm[p];
              }
            }

            _.forEach(itm.units, function (u) {
              for (var p in u) {
                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                  delete u[p];
                }
              }
            });

            itm["price"] = "";
          });
        });
      });

      return stores;
    };
    $scope.receiverChange_receiverPricing = function (r) {
      //bind store
      // processedAndSemiItems(angular.copy($scope.items));
      // /console.log($scope.processedCat);
      // console.log($scope.processedSemi_Cat);
      //console.log('computing first', $scope.receiverPricingform, 'primes');
      //console.time('computing stores');
      var str = _.cloneDeep($scope.stores);
      _.forEach(str, function (s, i) {
        var p = _.cloneDeep($scope.processedCat);
        var sp = _.cloneDeep($scope.processedSemi_Cat);
        s.category.push(p);
        s.category.push(sp);
      });

      var formattedStores = formatstores_recieverPricing(str);
      $scope.receiverPricingform.stores = formattedStores;
      // $scope.receiverPricingform.stores=str;
      $scope.receiverPricingform.categorys = {};
      $scope.receiverPricingform.category = {};
      //console.timeEnd('computing stores');
    };

    $scope.unitChange_receiverPricing = function (unit, s) {
      var index = _.findIndex($scope.receiverPricingform.category.item, {_id: s._id});
      $scope.receiverPricingform.category.item[index].selectedUnitId = unit;
    };

    function slectedUnit_receiverPricing(item) {
      $scope.receiverPricingform.unit = {};
      _.forEach(item, function (itm, i) {
        //_.forEach(itm.units , function(u,ii){
        if (itm.selectedUnitId != undefined) {
          var index = _.findIndex(itm.units, {_id: itm.selectedUnitId._id});
          $scope.receiverPricingform.unit[itm._id] = itm.units[index];
        }
        // else if(itm.preferedUnit!=undefined){
        //   var index=_.findIndex(itm.units,{_id:itm.preferedUnit});
        //   $scope.receiverPricingform.unit[itm._id]=itm.units[index];
        // }
        //});
      });
    };

    function addNewProcessedItemToReceiver(receiver, sid) {
      var cat = angular.copy($scope.receiverPricingform.categorys);
      var pCat_index = _.findIndex(cat, {_id: $scope.processedCat._id});
      var sCat_index = _.findIndex(cat, {_id: $scope.processedSemi_Cat._id});
      var pCat = cat[pCat_index];
      var sCat = cat[sCat_index];
      if (receiver.pricing != undefined) {
        _.forEach(receiver.pricing.store, function (ss, i) {
          if (ss._id == sid) {
            _.forEach(ss.category, function (c, ii) {
              if (c._id == pCat._id) {
                _.forEach(pCat.item, function (pItem, pi) {
                  var pINdex = _.findIndex(c.item, {_id: pItem._id});
                  if (pINdex < 0) {
                    c.item.push(pItem);
                  }
                });
              }

              if (c._id == sCat._id) {
                _.forEach(sCat.item, function (pItem, pi) {
                  var pINdex = _.findIndex(c.item, {_id: pItem._id});
                  if (pINdex < 0) {
                    c.item.push(pItem);
                  }
                });
              }
            });
          }
        });
      }
      ;

    };
    $scope.storeChange_receiverPricing = function (s) {
      $scope.receiverPricingform.categorys = {};
      $scope.receiverPricingform.category = {};
      //bind category after format
      //console.log("STORE", s)
      formatPricing_setReceiverPricing(s);
      if ($scope.receiverPricingform.receiver.pricing != undefined) {
        var flag = false;
        _.forEach($scope.receiverPricingform.receiver.pricing.store, function (ss, i) {
          if (ss._id == s._id) {
            flag = true;
            $scope.receiverPricingform.categorys = _.cloneDeep(ss.category);
            addNewProcessedItemToReceiver($scope.receiverPricingform.receiver, s._id);
          }
        });

        if (flag == false) {
          $scope.receiverPricingform.categorys = _.cloneDeep(s.category);
        }
      }
      else {
        $scope.receiverPricingform.categorys = _.cloneDeep(s.category);
      }

    };

    function checkStoreFor_receiverPricing(s) {
      if ($scope.receiverPricingform.receiver.pricing != undefined) {
        _.forEach($scope.receiverPricingform.receiver.pricing.store, function (str, i) {
          if (str._id == s._id) {
            _.forEach(str.category, function (cat, ii) {
              //if()
            });
          }
        });
      }
    }

    $scope.categoryChange_receiverPricing = function (item) {
      slectedUnit_receiverPricing(item);
      $scope.receiverPricingform.disableSubmit = false;
    };

    $scope.clearReceiverPricingform = function () {
      $scope.receiverPricingform = {
        disableSubmit: true
      };
    };

    function setPricingBeforUpdate_ReceiverPricing() {
      //check receiver have already items
      if ($scope.receiverPricingform.receiver.pricing == undefined) {
        $scope.receiverPricingform.receiver.pricing = {store: []};
        var str = $scope.receiverPricingform.stores;
        _.forEach(str, function (s, i) {
          _.forEachRight(s.category, function (c, ii) {
            if ($scope.receiverPricingform.category._id == c._id) {
              s.category.splice(ii, 1);
              s.category.push($scope.receiverPricingform.category);
            }
          });
          $scope.receiverPricingform.receiver.pricing.store.push(s);
        });
      }
      else {
        var flag = false;
        _.forEach($scope.receiverPricingform.receiver.pricing.store, function (str, i) {
          if (str._id == $scope.receiverPricingform.store._id) {
            flag = true;
            _.forEach(str.category, function (cat, ii) {
              if (cat._id == $scope.receiverPricingform.category._id) {
                $scope.receiverPricingform.receiver.pricing.store[i].category[ii] = $scope.receiverPricingform.category;
              }
            });
          }
        });
        if (flag == false) {
          var str1 = $scope.receiverPricingform.store;
          delete str1["category"];
          str1.category = [];
          str1.category.push($scope.receiverPricingform.category);
          $scope.receiverPricingform.receiver.pricing.store.push(str1);
        }
      }
    };
    $scope.updateReceiverPricing = function () {
      setPricingBeforUpdate_ReceiverPricing();

      var chk = $scope.validateReceiverPricingForm();
      //console.log(chk);
      if (chk) {
        receiver.update({}, $scope.receiverPricingform.receiver, function (result) {
          var index = _.findIndex($scope.receivers, {_id: result._id});
          // $scope.receivers.splice(index,1);
          // $scope.receivers.push(result);
          $scope.receivers[index] = result;
          $scope.receiverPricingform.receiver = $scope.receivers[index];
          var rt_index = _.findIndex($rootScope.receivers, {_id: result._id});
          if (rt_index >= 0) {
            $rootScope.receivers.splice(rt_index, 1);
            $rootScope.receivers.push(result);
          }

          growl.success('Successfully Updated', {ttl: 3000});
          $scope.clearReceiverPricingform();
          //alert("Successfully Updated");
        });
      } else {
        growl.error("Please fill full row!", {ttl: 3000});
      }
    };

    $scope.validateReceiverPricingForm = function () {
      //console.log('validating');
      var flag = true;
      for (var i in $scope.receiverPricingform.category.item) {
        //console.log((!$scope.receiverPricingform.category.item[i].price || isNaN($scope.receiverPricingform.category.item[i].isPrice)) && !$scope.receiverPricingform.unit[$scope.receiverPricingform.category.item[i]._id]);
        //console.log(isNaN($scope.receiverPricingform.category.item[i].price));
        //console.log($scope.receiverPricingform.unit[$scope.receiverPricingform.category.item[i]._id]);
        if (($scope.receiverPricingform.category.item[i].price && $scope.receiverPricingform.category.item[i].price != "" && !isNaN($scope.receiverPricingform.category.item[i].price)) && $scope.receiverPricingform.unit[$scope.receiverPricingform.category.item[i]._id]) {
          //console.log('helloo');

        } else if (!$scope.receiverPricingform.category.item[i].price && !$scope.receiverPricingform.unit[$scope.receiverPricingform.category.item[i]._id]) {
          //console.log('OK');
        } else {
          flag = false;
          break;
        }
      }
      ;
      return flag;
    }

    //---------------------------------------------Vendor Pricing --------------------------------------
    $scope.vendorPricing = {
      availableVendor: [],
      disableSubmit: true
    };

    $scope.clearVendorPricingTab = function () {
      $scope.vendorPricing = {
        availableVendor: [],
        disableSubmit: true
      };
    };

    $scope.dateOptions = {
      showWeeks: false,
      formatMonth: 'mm'
    };

    function VendorsByStore_VendorPricing(storeId) {
      _.forEach($scope.vendors, function (v, i) {
        if (v.pricing._id == storeId) {
          $scope.vendorPricing.availableVendor.push($scope.vendors[i]);
        }
      });
    };

    function vendorByCategory_vendorPricing(cat) {
      $scope.vendorPricing.availableVendor = [];
      $scope.vendorPricing.vendor = {};
      if (cat != null && cat != undefined) {
        _.forEach($scope.vendors, function (v, i) {
          //if(v.type !== 'HO Vendor'){
          _.forEach(v.pricing.category, function (e, ii) {
            if (e._id == cat._id) {
              $scope.vendorPricing.availableVendor.push($scope.vendors[i]);
            }
          })
          //}
        });
      }
    };

    function Item_vendorByCategory_vendorPricing(ven) {
      //console.log(ven)
      //console.log("VENDOR PRICING CATEGORY", $scope.vendorPricing.category)
      _.forEach(ven.pricing.category, function (e, ii) {
        //.log(e)
        if (e._id == $scope.vendorPricing.category._id) {
          $scope.vendorPricing.categorys = e;
          //console.log("PUSHING", $scope.vendorPricing.categorys )
        }
      })
    };

    function slectedUnit_vendorPricing(item) {
      $scope.vendorPricing.unit = {};
      _.forEach(item, function (itm, i) {
        if (itm.selectedUnitId != undefined) {
          var index = _.findIndex(itm.units, {_id: itm.selectedUnitId._id});
          $scope.vendorPricing.unit[itm._id] = itm.units[index];
        }
        else if (itm.selectedUnit != undefined) {
          var index = _.findIndex(itm.units, {_id: itm.selectedUnit._id});
          $scope.vendorPricing.unit[itm._id] = itm.units[index];
        }
        // else if(itm.preferedUnit!=undefined){
        //   var index=_.findIndex(itm.units,{_id:itm.preferedUnit});
        //   $scope.vendorPricing.unit[itm._id]=itm.units[index];
        // }
      });
    };

    $scope.onCategoryChange_vendorPricing = function (cat) {
      $scope.vendorPricing.categorys = {};
      vendorByCategory_vendorPricing(cat);
    };

    function getBaseKitchencategory(storeId) {
      var cat = [];
      _.forEach($scope.vendors, function (ven, i) {
        if (ven.baseKitchenId != undefined) {
          if (ven.pricing._id == storeId) {
            _.forEach(ven.pricing.category, function (cate, ii) {
              var obj = angular.copy(cate);
              obj.isBaseKitchen = true;
              cat.push(obj);
            });
          }
        }
      });
      return cat;
    };
    $scope.OnStoerChange_vendorPricing = function (s) {
      //VendorsByStore_VendorPricing(s._id);
      $scope.vendorPricing.categorys = {};
      $scope.vendorPricing.category = {};
      $scope.vendorPricing.availableCategory = [];
      $scope.vendorPricing.availableVendor = [];
      //  $scope.vendorPricing.store.category
      _.forEach(s.category, function (c, i) {
        $scope.vendorPricing.availableCategory.push(c);
      });
      // _.forEach($scope.processedItemsByCategory, function(c , i){
      //   $scope.vendorPricing.store.category.push(c);
      // });

      // _.forEach($scope.semiprocessedItemsByCategory, function(c, i){
      //   $scope.vendorPricing.store.category.push(c);
      // });

      var category = getBaseKitchencategory(s._id);
      _.forEach(category, function (c, i) {
        $scope.vendorPricing.availableCategory.push(c);
      });
    };

    $scope.onDepChange_vendorPricing = function () {
      $scope.vendorPricing.stores = $scope.stores;
    };

    $scope.onvendorChnage_VendorPricing = function (v) {
      //console.log("VENDOR", v)
      Item_vendorByCategory_vendorPricing(v);
      slectedUnit_vendorPricing($scope.vendorPricing.categorys.item);
      $scope.vendorPricing.disableSubmit = false;
    };

    $scope.clearVendorPricingForm = function () {
      $scope.vendorPricing = {
        availableVendor: [],
        disableSubmit: true,
        vendor: {}
      };
    };

    $scope.setPrefferedUnit_VendorPricing = function (unit, item_id) {
      if (unit != null) {
        _.forEach($scope.vendorPricing.categorys.item, function (itm, i) {
          if (itm._id == item_id) {
            var selectedUnitId = {_id: unit._id, unitName: unit.unitName};
            itm.selectedUnitId = selectedUnitId;
          }
        });
      }
    };
    function updateAllVendorPricing(ven) {
      _.forEach(ven, function (v, i) {
        vendor.update({}, v, function (result) {
          var index = _.findIndex($scope.vendors, {_id: result._id});
          $scope.vendors[index] = result;
          var rindex = _.findIndex($rootScope.vendors, {_id: result._id});
          $rootScope.vendors[rindex] = result;
        });
      });
    };

    function checkValidationBeforeUpdate_vendorPricing(cat) {
      var flag = false;
      var validationFail = false;
      _.forEach(cat.item, function (itm, i) {
        if (flag == false) {
          if (itm.price != "" && itm.fromDate != "" && itm.toDate != "" && itm.preferedUnit != null) {

          }
          else {
            flag = true;
          }
        }
      });
      return flag;
    };
    function checkPricing_vendorPricing_New(cat) {
      var flag = false;
      _.forEach(cat.item, function (itm, i) {
        if (itm.fromDate != "" && itm.price != undefined && itm.price != "" && isNaN(itm.price) == false && itm.fromDate != undefined && itm.toDate != "" && itm.toDate != undefined && $scope.vendorPricing.unit[itm._id] != undefined && $scope.vendorPricing.unit[itm._id] != null) {
          flag = true;
        }
      });
      return flag;
    };
    function checkPricin_vendorPricing(cat) {
      var flag = true;
      _.forEach(cat.item, function (itm, i) {
        //if(itm.price!=""){
        //if($scope.vendorPricing.unit[itm._id]!=null){
        if ((itm.fromDate == "" || itm.fromDate == undefined) && (itm.price == "" || itm.price == undefined) && (itm.toDate == "" || itm.toDate == undefined) && $scope.vendorPricing.unit[itm._id] == null) {
          //growl.error('Please fill the full row before update', {ttl: 3000});
          var a = 1;
          //return flag;
        }
        else if (itm.fromDate != "" && itm.price != undefined && itm.price != "" && isNaN(itm.price) == false && itm.fromDate != undefined && itm.toDate != "" && itm.toDate != undefined && $scope.vendorPricing.unit[itm._id] != undefined && $scope.vendorPricing.unit[itm._id] != null) {
          var b = 1;
        }
        else {
          flag = false;
        }
        //}
        //}
      });
      return flag;
    };

    $scope.updateVendorPricing = function () {
      var checkMendetory = checkPricin_vendorPricing($scope.vendorPricing.categorys);
      //var checkMendetory=checkPricing_vendorPricing_New($scope.vendorPricing.categorys);
      if (checkMendetory == true) {
        //var check=checkValidationBeforeUpdate_vendorPricing($scope.vendorPricing.categorys);
        //if(check==true){
        $scope.vendorPricing.vendorToBeUpdated = [];
        _.forEach($scope.vendorPricing.availableVendor, function (v, i) {
          if ($scope.vendorPricing.vendor._id == v._id) {
            _.forEachRight(v.pricing.category, function (cat, ii) {
              if ($scope.vendorPricing.categorys._id == cat._id) {
                v.pricing.category[ii] = $scope.vendorPricing.categorys;
              }
              //_.forEach(cat.item, function (itm){
              //  if($scope.vendorPricing.unit[itm._id]){
              //    itm.selectedUnit = $scope.vendorPricing.unit[itm._id];
              //  }
              //  console.log(itm);
              //});
            });
            $scope.vendorPricing.vendorToBeUpdated.push($scope.vendorPricing.availableVendor[i]);
          }
        });
        updateAllVendorPricing($scope.vendorPricing.vendorToBeUpdated);
        growl.success('Successfully Updated', {ttl: 3000});
        //alert("Successfully Updated");
        //console.log($scope.vendorPricing.availableVendor);
        //}
        // else
        // {
        //   growl.error('Please fill the below row before update', {ttl: 3000});
        //   //alert("PLease fill the below row before update");
        // }
      }
      else {
        growl.error('Please fill the below row before update', {ttl: 3000});
      }
    };

    //-------------------------Physical Stock --------------------------------------------------
    $scope.physicalStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
    $scope.clearPhysicalStockTab = function () {
      // $scope.physicalStockForm={enableSubmit:true,showBill:true,maxDate:new Date(),billDate:new Date()};
      // $scope.transaction=new StockTransaction(currentUser,$rootScope.db,null,null,null,null,null,6,null,null);
      // $scope.transaction.generateBillNumbers(6).then(function (snumbers) {
      //     $scope.transaction._transactionNumber=snumbers.billnumber;
      //     $scope.physicalStockForm.tempBillNo="Temp/OS -"+snumbers.billnumber;
      // });
      $scope.rawMaterialOpening = true;
      $scope.intermediateOpening = true;
      $scope.menuItemOpening = true;
    };
    $scope.resetPhysicalStockTab = function () {
      if (!$scope.physicalStockForm.isEdit) {
        $scope.physicalStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
        $scope.physicalStockForm.showBill = true;
        //$scope.physicalStockForm.tempBillNo="temp/"+$filter('date')(new Date(), 'dd-MMM-yyyy');
        //$scope.physicalStockForm.tempBillNo="Temp/OS -"+ $scope.transaction._transactionNumber;
        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 6, null, null);
        $scope.transaction.generateBillNumbers(6).then(function (snumbers) {
          $scope.transaction._transactionNumber = snumbers.billnumber;
          $scope.transaction.transactionNumber = snumbers.billnumber;
          $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
          $scope.physicalStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
        });
      }
      else {
        //edit
        $scope.physicalStockForm.isPrint = false;
        $scope.physicalStockForm.isEdit = false;
        $scope.physicalStockForm.isSaved = false;
      }
    };
    $scope.tempBill_physicalStockForm = function (billDate) {
      $scope.physicalStockForm.showBill = true;
      //$scope.physicalStockForm.tempBillNo="Temp/OS -"+ $scope.transaction._transactionNumber;
    };

    function slectedUnit_physicalStockForm(item) {
      $scope.physicalStockForm.selectedUnits = {};
      _.forEach(item, function (itm, i) {
        _.forEach(itm.units, function (u, ii) {
          if (itm.selectedUnit != undefined) {
            if (itm.selectedUnit._id == u._id) {
              $scope.physicalStockForm.selectedUnits[itm._id] = u;
            }
          }
          // else{
          //   if(itm.preferedUnit!=undefined){
          //       if(itm.preferedUnit==u._id){
          //           $scope.physicalStockForm.selectedUnits[itm._id]=u;
          //       }
          //   }
          // }
        });
      });
    };

    $scope.addRemainingItem_physicalStockForm = function ($item, $model, $label) {
      var remaining = angular.copy($scope.physicalStockForm.remainingItems);
      _.forEach($scope.physicalStockForm.remainingItems, function (itm, i) {
        if (itm._id == $item._id) {
          $scope.physicalStockForm.availableItems.push(itm);
          remaining.splice(i, 1);
          $scope.physicalStockForm.itemToAdd = "";
          $scope.physicalStockForm.selectedCategory[itm.fromCategory._id] = true;
        }
      });
      $scope.physicalStockForm.remainingItems = remaining;
      slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
    };

    $scope.deleteSelectedItemPhysicalForm = function (item) {
      var index = _.findIndex($scope.physicalStockForm.availableItems, {_id: item._id});
      $scope.physicalStockForm.availableItems.splice(index, 1);
      $scope.physicalStockForm.remainingItems.push(item);
    };

    $scope.bindCategory_physicalStockForm = function (stores) {
      var store = angular.copy(stores);
      $scope.physicalStockForm.availableCategory = [];
      $scope.physicalStockForm.availableItems = [];
      $scope.physicalStockForm.remainingItems = [];
      $scope.physicalStockForm.selectedCategory = {};
      _.forEach(store.category, function (c, ii) {
        $scope.physicalStockForm.availableCategory.push(c);
        _.forEach(c.item, function (item, iii) {
          var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
          item["fromCategory"] = varFromCategory;
          $scope.physicalStockForm.remainingItems.push(item);
        });
      });
      //slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
    };

    $scope.validateDatePricing = function (fromDate, toDate, item) {

      if (fromDate != undefined && fromDate != "" && toDate != undefined && toDate != "") {
        if (new Date(fromDate) > new Date(toDate)) {
          growl.error('From Date Can Not Be Greater Than To Date', {ttl: 3000});
          var index = _.findIndex($scope.vendorPricing.categorys.item, {_id: item._id});
          $scope.vendorPricing.categorys.item[index].fromDate = "";
        }
      }
    };

    $scope.bindItems_physicalStockForm = function (catId) {
      //var store=angular.copy(stores);
      //$scope.physicalStockForm.availableItems=[];
      var cat = angular.copy($scope.physicalStockForm.availableCategory);
      _.forEach(cat, function (c, ii) {
        if ($scope.physicalStockForm.selectedCategory[catId] == true) {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var aa = _.findIndex($scope.physicalStockForm.availableItems, {_id: itm._id});
              if (aa < 0) {
                var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
                itm["fromCategory"] = varFromCategory;
                $scope.physicalStockForm.availableItems.push(itm);
                var index = _.findIndex($scope.physicalStockForm.remainingItems, {_id: itm._id});
                $scope.physicalStockForm.remainingItems.splice(index, 1);
              }
            });
          }
        }
        else {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var index = _.findIndex($scope.physicalStockForm.availableItems, {_id: itm._id});
              if (index >= 0) {
                $scope.physicalStockForm.availableItems.splice(index, 1);
                $scope.physicalStockForm.remainingItems.push(itm);
              }
            });
          }
        }
      });
      slectedUnit_physicalStockForm($scope.physicalStockForm.availableItems);
    };

    $scope.enableSubmitButton = function (item) {
      if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.physicalStockForm.selectedUnits[item._id] != undefined) {
        $scope.physicalStockForm.enableSubmit = false;
      }
      else {
        $scope.physicalStockForm.enableSubmit = true;
      }
    };

    $scope.enableSubmitButton_opening = function (items) {
      var flag = false;
      _.forEach(items, function (item, i) {
        if (flag == false) {
          if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.physicalStockForm.selectedUnits[item._id] != undefined && $scope.physicalStockForm.selectedUnits[item._id] != null) {
            flag = true;
          }
        }
        var unit = $scope.physicalStockForm.selectedUnits;
        _.forEach(unit, function (u, ii) {
          if (ii == item._id) {
            item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
          }
        });
      });

      if (flag == true) {
        $scope.physicalStockForm.enableSubmit = false;
      }
      else {
        $scope.physicalStockForm.enableSubmit = true;
        growl.error('Please fill the full row before update', {ttl: 3000});
      }
      //return flag;
    };

    $scope.goNextPhysicalStock = function (nextIdx, item) {
      var f = angular.element(document.querySelector('#phy_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
      else {
        // $scope.physicalStockForm.enableSubmit=false;
        // angular.element(document.querySelector('#submitPhysicalStock')).focus();
      }
    };
    function getItemToSave_PhysicalStock(items) {
      //var items=angular.copy($scope.physicalStockForm.availableItems);
      var items = angular.copy(items);
      _.forEachRight(items, function (itm, i) {
        if (itm.qty != "" || itm.qty != undefined) {
          var flag = false;
          _.forEach($scope.physicalStockForm.selectedUnits, function (u, ii) {
            if (ii == itm._id && itm.qty != "" && itm.qty != undefined && u != null && $scope.physicalStockForm.selectedUnits[itm._id] != undefined) {
              // var selectedUnit={"_id":u._id,"unitName":u.unitName,"conversionFactor":u.conversionFactor,"baseQty":itm.qty,"type":"selectedUnit"};
              // itm["selectedUnit"]=selectedUnit;
              // itm.calculateInUnits=[];
              // itm.calculateInUnits.push(itm.baseUnit);
              // if(itm.preferedUnits!=undefined){
              //         itm.calculateInUnits.push(itm.preferedUnits);
              //     }
              //     else
              //     {
              //         var preUnit={"_id":u._id,"unitName":u.unitName,"conversionFactor":u.conversionFactor,"baseQty":itm.qty,"type":"preferedUnit"};
              //         itm["preferedUnit"]=preUnit;
              //         itm.calculateInUnits.push(itm.preferedUnit);
              //         //itm.preferedUnit=u._id;
              //     }
              // itm.calculateInUnits.push(itm.selectedUnit);
              flag = true;
            }
          });
          if (flag == false) {
            items.splice(i, 1);
          }
        }
      });
      _.forEach(items, function (itm, i) {
        for (var p in itm) {
          if (p != "_id" && p != "itemName" && p != "qty" && p != "calculateInUnits" && p != "selectedUnit" && p != "preferedUnit" && p != "itemType" && p != "fromCategory" && p != "units") {
            //&& p!="baseUnit"  && p!="preferedUnits" && p!="selectedUnit"
            _.forEach(itm.units, function (u, ii) {
              for (var p in u) {
                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                  delete u[p];
                }
              }
            });
            delete itm[p];
          }
        }
      });
      return items;
    };

    function calculateAmtinPrefferedAndBaseUnit_PhysicalStock() {
      var items = $scope.physicalStockForm.availableItems;
      _.forEach(items, function (item, i) {
        var conversionFactor = 1;
        var selectedConversionFactor = 1;
        if (item.calculateInUnits != undefined) {
          selectedConversionFactor = item.calculateInUnits[2].conversionFactor;
        }
        else {
          _.forEach($scope.physicalStockForm.selectedUnits, function (u, ii) {
            if (ii == item._id) {
              selectedConversionFactor = u.conversionFactor;
            }
          });
        }


        var baseUnit = {};
        if (item.units[0].baseUnit != undefined) {
          baseUnit = item.units[0].baseUnit;
          baseUnit.unitName = baseUnit.name;
        }
        else {
          var ind = _.findIndex(item.units, {"type": "baseUnit"});
          baseUnit = item.units[ind];
          baseUnit.unitName = baseUnit.name;
        }


        var baseQty = parseFloat(item.qty * selectedConversionFactor);
        baseUnit.baseQty = baseQty;
        baseUnit.conversionFactor = conversionFactor;
        baseUnit.type = "baseUnit";
        item.baseUnit = baseUnit;

        var pInd = _.findIndex(item.units, {_id: item.preferedUnit});
        if (pInd >= 0) {
          var preferedUnits = angular.copy(item.units[pInd]);
          var preferedconversionFactor = item.units[pInd].conversionFactor;
          if (preferedconversionFactor == conversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
          }
          else if (preferedconversionFactor > conversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
          }
          else if (preferedconversionFactor < conversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
          }
          preferedUnits.type = "preferedUnit";
          delete preferedUnits.baseUnit;
          preferedUnits.conversionFactor = preferedconversionFactor;
          item.preferedUnits = preferedUnits;
        }
      });
      return items;
    };

    function formatStore_PhysicalStockForm(store) {
      var stores = angular.copy(store);
      for (var p in stores) {
        if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID") {
          delete stores[p];
        }
      }
      return stores;
    };
    function formatStoreForPhysicalStock(item, store) {
      var stores = store;
      stores.category = [];
      _.forEach(item, function (itm, i) {
        var index = _.findIndex(stores.category, {_id: itm.fromCategory._id});
        if (index < 0) {
          stores.category.push(itm.fromCategory);
        }
      });
      _.forEach(stores.category, function (c, i) {
        c.items = [];
        _.forEachRight(item, function (itm, ii) {
          if (c._id == itm.fromCategory._id) {
            delete itm.fromCategory;
            c.items.push(itm);
            item.splice(ii, 1);
          }
        });
      });
      //console.log(stores);
      return stores;
    };

    $scope.submitPhysicalStock = function (formPhysicalStock) {
      //check for necessary data before submit
      // var storeToSave=formatStore_PhysicalStockForm($scope.physicalStockForm.store);
      // calculateAmtinPrefferedAndBaseUnit_PhysicalStock();
      // var itemToSave=getItemToSave_PhysicalStock();
      // console.log(itemToSave);
      // var sToSave=formatStoreForPhysicalStock(angular.copy(itemToSave),angular.copy(storeToSave));
      // if($scope.physicalStockForm.transactionId!=undefined){
      //     //alert("trans present");
      //     StockResource.remove({},$scope.physicalStockForm, function (result){
      //     });
      // }
      // $scope.physicalStockForm.transactionId=guid();
      // if(formPhysicalStock.$valid && !$scope.physicalStockForm.isPrint){
      //   var physicalStockFormToSave={};
      //     physicalStockFormToSave.transactionid=$scope.physicalStockForm.transactionId;
      //     physicalStockFormToSave._id=physicalStockFormToSave.transactionid
      //     physicalStockFormToSave.transactionType="6";
      //     physicalStockFormToSave.user=currentUser;
      //     physicalStockFormToSave._store=sToSave;
      //     physicalStockFormToSave.created=new Date($scope.physicalStockForm.billDate).toISOString();
      //     physicalStockFormToSave._items=itemToSave;
      //     physicalStockFormToSave._vendor=null;
      //     physicalStockFormToSave.deployment_id=currentUser.deployment_id;
      //     physicalStockFormToSave.tenant_id=currentUser.tenant_id;
      //     StockResource.save({},physicalStockFormToSave, function (result){
      //       $scope.physicalStockForm._id=result._id;
      //         growl.success('Inserted Successfully', {ttl: 3000});
      //         growl.success(itemToSave.length+' Items Inserted', {ttl: 3000});
      //         //$scope.physicalStockForm={enableSubmit:true};
      //         $scope.getOpen_TransactionNumber=parseFloat($scope.getOpen_TransactionNumber)+1;
      //         $scope.physicalStockForm.isPrint=true;
      //         $scope.physicalStockForm.isEdit=true;
      //         $scope.physicalStockForm.isSaved=true;
      //     });
      // }
      // else{
      //     //Print
      //     printPhysicalStock(itemToSave);
      // }

      var storeToSave = formatStore_PhysicalStockForm($scope.physicalStockForm.store);
      var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItems($scope.physicalStockForm.availableItems, $scope.physicalStockForm.selectedUnits);
      var itemToSave = getItemToSave_PhysicalStock(itms);

      var sToSave = formatStoreForPhysicalStock(angular.copy(itemToSave), angular.copy(storeToSave));
      if (formPhysicalStock.$valid && !$scope.physicalStockForm.isPrint) {
        if (itemToSave.length > 0) {
          $scope.transaction._items = itemToSave;
          $scope.transaction._store = sToSave;
          $scope.transaction.isOpen = false;
          $scope.transaction.processTransaction($scope.physicalStockForm, 6);
          if ($scope.transaction.isEdit == true) {
            $scope.transaction.updateTransaction({}, 'update').then(function (results) {
              if (results != null) {
                $scope.physicalStockForm.isPrint = true;
                $scope.physicalStockForm.isEdit = true;
                $scope.physicalStockForm.isSaved = true;
              }
            });
          }
          else {
            $scope.transaction.billDate = new Date($scope.physicalStockForm.billDate);
            $scope.transaction.updateTransaction({}, 'insert').then(function (results) {
              if (results != null) {
                $scope.physicalStockForm.isPrint = true;
                $scope.physicalStockForm.isEdit = true;
                $scope.physicalStockForm.isSaved = true;
                $scope.transaction.isEdit = true;
              }
            });
          }
        }
      }
      else {
        printPhysicalStock(itemToSave);
      }
    };

    $scope.showRawMaterialOpening = function () {
      $scope.rawMaterialOpening = false;
      $scope.physicalStockForm = {enableSubmit: true, showBill: true, maxDate: new Date(), billDate: new Date()};
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 6, null, null);
      $scope.transaction.generateBillNumbers(6).then(function (snumbers) {
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        $scope.physicalStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
      });
    };

    $scope.newPhysicalStock = function () {
      $scope.physicalStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
      $scope.physicalStockForm.showBivendorPricing.vendorll = true;
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 6, null, null);
      $scope.transaction.generateBillNumbers(6).then(function (snumbers) {
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        $scope.physicalStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
      });
    };
    //-------------------------------------------------Intermediate Opening Start-------------------------------------------//
    $scope.intermediateStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
    $scope.newIntermediateStock = function () {
      $scope.intermediateStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
      $scope.intermediateStockForm.showBill = true;
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 10, null, null);
      $scope.transaction.generateBillNumbers(10).then(function (snumbers) {
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        $scope.intermediateStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
      });
    };
    $scope.showIntermediateOpening = function () {
      $scope.intermediateOpening = false;
      $scope.intermediateStockForm = {enableSubmit: true, showBill: true, maxDate: new Date(), billDate: new Date()};
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 10, null, null);
      $scope.transaction.generateBillNumbers(10).then(function (snumbers) {
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        $scope.intermediateStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
      });
    };

    $scope.resetIntermediateStockTab = function () {
      if (!$scope.intermediateStockForm.isEdit) {
        $scope.intermediateStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
        $scope.intermediateStockForm.showBill = true;
        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 10, null, null);
        $scope.transaction.generateBillNumbers(10).then(function (snumbers) {
          $scope.transaction._transactionNumber = snumbers.billnumber;
          $scope.transaction.transactionNumber = snumbers.billnumber;
          $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
          $scope.intermediateStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
        });
      }
      else {
        //edit
        $scope.intermediateStockForm.isPrint = false;
        $scope.intermediateStockForm.isEdit = false;
        $scope.intermediateStockForm.isSaved = false;
      }
    };
    $scope.tempBill_intermediateStockForm = function (billDate) {
      $scope.intermediateStockForm.showBill = true;
    };

    function slectedUnit_intermediateStockForm(item) {
      $scope.intermediateStockForm.selectedUnits = {};
      _.forEach(item, function (itm, i) {
        _.forEach(itm.units, function (u, ii) {
          if (itm.selectedUnitId != undefined) {
            if (itm.selectedUnitId._id == u._id) {
              $scope.intermediateStockForm.selectedUnits[itm._id] = u;
            }
          }
          // else{
          //   if(itm.preferedUnit!=undefined){
          //       if(itm.preferedUnit==u._id){
          //           $scope.intermediateStockForm.selectedUnits[itm._id]=u;
          //       }
          //   }
          // }
        });
      });
    };

    $scope.addRemainingItem_intermediateStockForm = function ($item, $model, $label) {
      var remaining = angular.copy($scope.intermediateStockForm.remainingItems);
      _.forEach($scope.intermediateStockForm.remainingItems, function (itm, i) {
        if (itm._id == $item._id) {
          $scope.intermediateStockForm.availableItems.push(itm);
          remaining.splice(i, 1);
          $scope.intermediateStockForm.itemToAdd = "";
          $scope.intermediateStockForm.selectedCategory[itm.fromCategory._id] = true;
        }
      });
      $scope.intermediateStockForm.remainingItems = remaining;
      slectedUnit_intermediateStockForm($scope.intermediateStockForm.availableItems);
    };

    $scope.deleteSelectedItemIntermediateForm = function (item) {
      var index = _.findIndex($scope.intermediateStockForm.availableItems, {_id: item._id});
      $scope.intermediateStockForm.availableItems.splice(index, 1);
      $scope.intermediateStockForm.remainingItems.push(item);
    };

    $scope.bindCategory_intermediateStockForm = function (stores) {
      var store = angular.copy(stores);
      $scope.intermediateStockForm.availableCategory = [];
      $scope.intermediateStockForm.availableItems = [];
      $scope.intermediateStockForm.remainingItems = [];
      $scope.intermediateStockForm.selectedCategory = {};
      var ir = angular.copy($scope.intermediateRecipes);
      store.category = [];
      var cat = {_id: "PosistTech222222", categoryName: "InterMediate Food"};
      cat.item = [];
      _.forEach(angular.copy($scope.semiRecipe), function (itm) {
        itm.itemName = itm.recipeName;
        cat.item.push(itm);
      });
      store.category.push(cat);
      $scope.intermediateStockForm.availableCategory.push(cat);
      // _.forEach(store.category , function(c, ii){
      _.forEach(cat.item, function (item, iii) {
        var varFromCategory = {"_id": cat._id, "categoryName": cat.categoryName};
        item["fromCategory"] = varFromCategory;
        item["itemType"] = "IntermediateItem";
        $scope.intermediateStockForm.remainingItems.push(item);
      });
      // });
    };

    $scope.bindItems_intermediateStockForm = function (catId) {
      var cat = angular.copy($scope.intermediateStockForm.availableCategory);
      _.forEach(cat, function (c, ii) {
        if ($scope.intermediateStockForm.selectedCategory[catId] == true) {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var aa = _.findIndex($scope.intermediateStockForm.availableItems, {_id: itm._id});
              if (aa < 0) {
                var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
                itm["fromCategory"] = varFromCategory;
                $scope.intermediateStockForm.availableItems.push(itm);
                var index = _.findIndex($scope.intermediateStockForm.remainingItems, {_id: itm._id});
                $scope.intermediateStockForm.remainingItems.splice(index, 1);
              }
            });
          }
        }
        else {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var index = _.findIndex($scope.intermediateStockForm.availableItems, {_id: itm._id});
              if (index >= 0) {
                $scope.intermediateStockForm.availableItems.splice(index, 1);
                $scope.intermediateStockForm.remainingItems.push(itm);
              }
            });
          }
        }
      });
      slectedUnit_intermediateStockForm($scope.intermediateStockForm.availableItems);
    };

    $scope.enableSubmitButton_openingIntermediate = function (items) {
      var flag = false;
      _.forEach(items, function (item, i) {
        if (flag == false) {
          if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.intermediateStockForm.selectedUnits[item._id] != undefined && $scope.intermediateStockForm.selectedUnits[item._id] != null) {
            flag = true;
          }
        }
        var unit = $scope.intermediateStockForm.selectedUnits;
        _.forEach(unit, function (u, ii) {
          if (ii == item._id) {
            item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
          }
        });
      });

      if (flag == true) {
        $scope.intermediateStockForm.enableSubmit = false;
      }
      else {
        $scope.intermediateStockForm.enableSubmit = true;
        growl.error('Please fill the full row before update', {ttl: 3000});
      }
      //return flag;
    };

    $scope.goNextIntermediateStock = function (nextIdx, item) {
      var f = angular.element(document.querySelector('#im_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
    };
    function getItemToSave_IntermediateStock(items) {
      var items = angular.copy(items);
      _.forEachRight(items, function (itm, i) {
        if (itm.qty != "" || itm.qty != undefined) {
          var flag = false;
          _.forEach($scope.intermediateStockForm.selectedUnits, function (u, ii) {
            if (ii == itm._id && itm.qty != "" && itm.qty != undefined && u != null && $scope.intermediateStockForm.selectedUnits[itm._id] != undefined) {
              if (itm.itemType != "RawMaterial") {
                // var ss=$scope.transaction.calculateForProcessedAndSemiProcessed_sale(itm);
                // itm=ss;
                var rd = $scope.transaction.calculateForRecipeDetails(itm);
                itm = rd;
              }
              flag = true;
            }
          });
          if (flag == false) {
            items.splice(i, 1);
          }
        }
      });
      _.forEach(items, function (itm, i) {
        for (var p in itm) {
          if (p != "_id" && p != "itemName" && p != "receipeDetails" && p != "rawItems" && p != "qty" && p != "calculateInUnits" && p != "selectedUnit" && p != "preferedUnit" && p != "itemType" && p != "fromCategory" && p != "units") {
            _.forEach(itm.units, function (u, ii) {
              for (var p in u) {
                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                  delete u[p];
                }
              }
            });
            delete itm[p];
          }
        }
      });
      return items;
    };

    function calculateAmtinPrefferedAndBaseUnit_IntermediateStock() {
      var items = $scope.intermediateStockForm.availableItems;
      _.forEach(items, function (item, i) {
        var conversionFactor = 1;
        var selectedConversionFactor = 1;
        if (item.calculateInUnits != undefined) {
          selectedConversionFactor = item.calculateInUnits[2].conversionFactor;
        }
        else {
          _.forEach($scope.intermediateStockForm.selectedUnits, function (u, ii) {
            if (ii == item._id) {
              selectedConversionFactor = u.conversionFactor;
            }
          });
        }


        var baseUnit = {};
        if (item.units[0].baseUnit != undefined) {
          baseUnit = item.units[0].baseUnit;
          baseUnit.unitName = baseUnit.name;
        }
        else {
          var ind = _.findIndex(item.units, {"type": "baseUnit"});
          baseUnit = item.units[ind];
          baseUnit.unitName = baseUnit.name;
        }


        var baseQty = parseFloat(item.qty * selectedConversionFactor);
        baseUnit.baseQty = baseQty;
        baseUnit.conversionFactor = conversionFactor;
        baseUnit.type = "baseUnit";
        item.baseUnit = baseUnit;

        var pInd = _.findIndex(item.units, {_id: item.preferedUnit});
        if (pInd >= 0) {
          var preferedUnits = angular.copy(item.units[pInd]);
          var preferedconversionFactor = item.units[pInd].conversionFactor;
          if (preferedconversionFactor == conversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty);
          }
          else if (preferedconversionFactor > conversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) / parseFloat(preferedconversionFactor);
          }
          else if (preferedconversionFactor < conversionFactor) {
            preferedUnits.baseQty = parseFloat(baseUnit.baseQty) * parseFloat(preferedconversionFactor);
          }
          preferedUnits.type = "preferedUnit";
          delete preferedUnits.baseUnit;
          preferedUnits.conversionFactor = preferedconversionFactor;
          item.preferedUnits = preferedUnits;
        }
      });
      return items;
    };

    function formatStore_IntermediateStockForm(store) {
      var stores = angular.copy(store);
      for (var p in stores) {
        if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID") {
          delete stores[p];
        }
      }
      return stores;
    };
    function formatStoreForIntermediateStock(item, store) {
      var stores = store;
      stores.category = [];
      _.forEach(item, function (itm, i) {
        var index = _.findIndex(stores.category, {_id: itm.fromCategory._id});
        if (index < 0) {
          stores.category.push(itm.fromCategory);
        }
      });
      _.forEach(stores.category, function (c, i) {
        c.items = [];
        _.forEachRight(item, function (itm, ii) {
          if (c._id == itm.fromCategory._id) {
            delete itm.fromCategory;
            c.items.push(itm);
            item.splice(ii, 1);
          }
        });
      });
      //console.log(stores);
      return stores;
    };

    $scope.submitIntermediateStock = function (formIntermediateStock) {
      //check for necessary data before submit
      var allow = true;
      if (new Date($scope.intermediateStockForm.billDate).getDate() != (new Date()).getDate()) {
        if (!Utils.isUserPermitted(currentUser, 'Allow Back Date Entry')) {
          allow = false;
        }
      }
      if (allow) {
        var storeToSave = formatStore_IntermediateStockForm($scope.intermediateStockForm.store);
        var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItems($scope.intermediateStockForm.availableItems, $scope.intermediateStockForm.selectedUnits);
        var itemToSave = getItemToSave_IntermediateStock(itms);

        var sToSave = formatStoreForIntermediateStock(angular.copy(itemToSave), angular.copy(storeToSave));
        if (formIntermediateStock.$valid && !$scope.intermediateStockForm.isPrint) {
          if (itemToSave.length > 0) {
            $scope.transaction._items = itemToSave;
            $scope.transaction._store = sToSave;
            $scope.transaction.isOpen = false;
            $scope.transaction.processTransaction($scope.intermediateStockForm, 10);
            if ($scope.transaction.isEdit == true) {
              $scope.transaction.updateTransaction({}, 'update').then(function (results) {
                if (results != null) {
                  $scope.intermediateStockForm.isPrint = true;
                  $scope.intermediateStockForm.isEdit = true;
                  $scope.intermediateStockForm.isSaved = true;
                }
              });
            }
            else {
              $scope.transaction.billDate = new Date($scope.intermediateStockForm.billDate);
              $scope.transaction.updateTransaction({}, 'insert').then(function (results) {
                if (results != null) {
                  $scope.intermediateStockForm.isPrint = true;
                  $scope.intermediateStockForm.isEdit = true;
                  $scope.intermediateStockForm.isSaved = true;
                  $scope.transaction.isEdit = true;
                }
              });
            }
          }
        }
        else {
          printPhysicalStock(itemToSave);
        }
      } else {
        growl.error("You are not allowed to do a back date entry!", {ttl: 3000});
      }
    };

    //-------------------------------------------------Intermediate Opening End-------------------------------------------//

    //-------------------------------------------------Menu Item Opening Start-------------------------------------------//
    $scope.menuItemStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
    $scope.newMenuItemStock = function () {
      $scope.menuItemStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
      $scope.menuItemStockForm.showBill = true;
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 13, null, null);
      $scope.transaction.generateBillNumbers(10).then(function (snumbers) {
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        $scope.menuItemStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
      });
    };
    $scope.showMenuItemOpening = function () {
      $scope.menuItemOpening = false;
      $scope.menuItemStockForm = {enableSubmit: true, showBill: true, maxDate: new Date(), billDate: new Date()};
      $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 13, null, null);
      $scope.transaction.generateBillNumbers(13).then(function (snumbers) {
        $scope.transaction._transactionNumber = snumbers.billnumber;
        $scope.transaction.transactionNumber = snumbers.billnumber;
        $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
        $scope.menuItemStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
      });
    };

    $scope.resetMenuItemStockTab = function () {
      if (!$scope.menuItemStockForm.isEdit) {
        $scope.menuItemStockForm = {enableSubmit: true, maxDate: new Date(), billDate: new Date()};
        $scope.menuItemStockForm.showBill = true;
        $scope.transaction = new StockTransaction(currentUser, $rootScope.db, null, null, null, null, null, 13, null, null);
        $scope.transaction.generateBillNumbers(13).then(function (snumbers) {
          $scope.transaction._transactionNumber = snumbers.billnumber;
          $scope.transaction.transactionNumber = snumbers.billnumber;
          $scope.transaction.daySerialNumber = snumbers.daySerialNumber;
          $scope.menuItemStockForm.tempBillNo = "Temp/OS -" + snumbers.billnumber;
        });
      }
      else {
        //edit
        $scope.menuItemStockForm.isPrint = false;
        $scope.menuItemStockForm.isEdit = false;
        $scope.menuItemStockForm.isSaved = false;
      }
    };
    $scope.tempBill_menuItemStockForm = function (billDate) {
      $scope.menuItemStockForm.showBill = true;
    };

    function slectedUnit_MenuItemStockForm(item) {
      $scope.menuItemStockForm.selectedUnits = {};
      _.forEach(item, function (itm, i) {
        _.forEach(itm.units, function (u, ii) {
          if (itm.selectedUnitId != undefined) {
            if (itm.selectedUnitId._id == u._id) {
              $scope.menuItemStockForm.selectedUnits[itm._id] = u;
            }
          }
          // else{
          //   if(itm.preferedUnit!=undefined){
          //       if(itm.preferedUnit==u._id){
          //           $scope.menuItemStockForm.selectedUnits[itm._id]=u;
          //       }
          //   }
          // }
        });
      });
    };

    $scope.addRemainingItem_MenuItemStockForm = function ($item, $model, $label) {
      var remaining = angular.copy($scope.menuItemStockForm.remainingItems);
      _.forEach($scope.menuItemStockForm.remainingItems, function (itm, i) {
        if (itm._id == $item._id) {
          $scope.menuItemStockForm.availableItems.push(itm);
          remaining.splice(i, 1);
          $scope.menuItemStockForm.itemToAdd = "";
          $scope.menuItemStockForm.selectedCategory[itm.fromCategory._id] = true;
        }
      });
      $scope.menuItemStockForm.remainingItems = remaining;
      slectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
    };

    $scope.deleteSelectedItemMenuItemForm = function (item) {
      var index = _.findIndex($scope.menuItemStockForm.availableItems, {_id: item._id});
      $scope.menuItemStockForm.availableItems.splice(index, 1);
      $scope.menuItemStockForm.remainingItems.push(item);
    };

    $scope.bindCategory_MenuItemStockForm = function (stores) {
      var store = angular.copy(stores);
      $scope.menuItemStockForm.availableCategory = [];
      $scope.menuItemStockForm.availableItems = [];
      $scope.menuItemStockForm.remainingItems = [];
      $scope.menuItemStockForm.selectedCategory = {};
      store.category = [];
      _.forEach(store.processedFoodCategory, function (cat, iii) {
        $scope.menuItemStockForm.availableCategory.push(cat);
        _.forEachRight(cat.item, function (item, i) {
          var index = _.findIndex($scope.menuRecipe, {itemId: item._id});
          if (index >= 0) {
            $scope.menuRecipe[index].itemName = item.itemName;
            item = angular.copy($scope.menuRecipe[index]);
            var varFromCategory = {"_id": cat._id, "categoryName": cat.categoryName};
            item["fromCategory"] = varFromCategory;
            item["itemType"] = "MenuItem";
            cat.item[i] = item;
            $scope.menuItemStockForm.remainingItems.push(item);
          }
          else {
            cat.item.splice(i, 1);
          }
        });
      });
    };

    $scope.bindItems_MenuItemStockForm = function (catId) {
      var cat = angular.copy($scope.menuItemStockForm.availableCategory);
      _.forEach(cat, function (c, ii) {
        if ($scope.menuItemStockForm.selectedCategory[catId] == true) {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var aa = _.findIndex($scope.menuItemStockForm.availableItems, {_id: itm._id});
              if (aa < 0) {
                var varFromCategory = {"_id": c._id, "categoryName": c.categoryName};
                itm["fromCategory"] = varFromCategory;
                $scope.menuItemStockForm.availableItems.push(itm);
                var index = _.findIndex($scope.menuItemStockForm.remainingItems, {_id: itm._id});
                $scope.menuItemStockForm.remainingItems.splice(index, 1);
              }
            });
          }
        }
        else {
          if (c._id == catId) {
            _.forEach(c.item, function (itm, ii) {
              var index = _.findIndex($scope.menuItemStockForm.availableItems, {_id: itm._id});
              if (index >= 0) {
                $scope.menuItemStockForm.availableItems.splice(index, 1);
                $scope.menuItemStockForm.remainingItems.push(itm);
              }
            });
          }
        }
      });
      slectedUnit_MenuItemStockForm($scope.menuItemStockForm.availableItems);
    };

    $scope.enableSubmitButton_openingMenuItem = function (items) {
      var flag = false;
      _.forEach(items, function (item, i) {
        if (flag == false) {
          if (item.qty != "" && item.qty != undefined && !isNaN(item.qty) && $scope.menuItemStockForm.selectedUnits[item._id] != undefined && $scope.menuItemStockForm.selectedUnits[item._id] != null) {
            flag = true;
          }
        }
        var unit = $scope.menuItemStockForm.selectedUnits;
        _.forEach(unit, function (u, ii) {
          if (ii == item._id) {
            item.selectedUnit = {_id: u._id, unitName: u.unitName, conversionFactor: u.conversionFactor};
          }
        });
      });

      if (flag == true) {
        $scope.menuItemStockForm.enableSubmit = false;
      }
      else {
        $scope.menuItemStockForm.enableSubmit = true;
        growl.error('Please fill the full row before update', {ttl: 3000});
      }
      //return flag;
    };

    $scope.goNextMenuItemStock = function (nextIdx, item) {
      var f = angular.element(document.querySelector('#mi_' + nextIdx));
      if (f.length != 0) {
        f.focus();
      }
    };
    function getItemToSave_MenuItemStock(items) {
      var items = angular.copy(items);
      _.forEachRight(items, function (itm, i) {
        if (itm.qty != "" || itm.qty != undefined) {
          var flag = false;
          _.forEach($scope.menuItemStockForm.selectedUnits, function (u, ii) {
            if (ii == itm._id && itm.qty != "" && itm.qty != undefined && u != null && $scope.menuItemStockForm.selectedUnits[itm._id] != undefined) {
              if (itm.itemType != "RawMaterial") {
                // var ss=$scope.transaction.calculateForProcessedAndSemiProcessed_sale(itm);
                // itm=ss;
                var rd = $scope.transaction.calculateForRecipeDetails(itm);
                itm = rd;
              }
              flag = true;
            }
          });
          if (flag == false) {
            items.splice(i, 1);
          }
        }
      });
      _.forEach(items, function (itm, i) {
        for (var p in itm) {
          if (p != "_id" && p != "itemName" && p != "receipeDetails" && p != "rawItems" && p != "qty" && p != "calculateInUnits" && p != "selectedUnit" && p != "preferedUnit" && p != "itemType" && p != "fromCategory" && p != "units") {
            _.forEach(itm.units, function (u, ii) {
              for (var p in u) {
                if (p != "_id" && p != "unitName" && p != "baseUnit" && p != "conversionFactor") {
                  delete u[p];
                }
              }
            });
            delete itm[p];
          }
        }
      });
      return items;
    };

    function formatStore_MenuItemStockForm(store) {
      var stores = angular.copy(store);
      for (var p in stores) {
        if (p != "_id" && p != "storeName" && p != "storeLocation" && p != "storeUID") {
          delete stores[p];
        }
      }
      return stores;
    };
    function formatStoreForMenuItemStock(item, store) {
      var stores = store;
      stores.category = [];
      _.forEach(item, function (itm, i) {
        var index = _.findIndex(stores.category, {_id: itm.fromCategory._id});
        if (index < 0) {
          stores.category.push(itm.fromCategory);
        }
      });
      _.forEach(stores.category, function (c, i) {
        c.items = [];
        _.forEachRight(item, function (itm, ii) {
          if (c._id == itm.fromCategory._id) {
            delete itm.fromCategory;
            c.items.push(itm);
            item.splice(ii, 1);
          }
        });
      });
      //console.log(stores);
      return stores;
    };

    $scope.submitMenuItemStock = function (formMenuItemStock) {
      //check for necessary data before submit
      var storeToSave = formatStore_MenuItemStockForm($scope.menuItemStockForm.store);
      var itms = $scope.transaction.basePrefferedAnsSelectedUnitsByItems($scope.menuItemStockForm.availableItems, $scope.menuItemStockForm.selectedUnits);
      var itemToSave = getItemToSave_MenuItemStock(itms);

      var sToSave = formatStoreForMenuItemStock(angular.copy(itemToSave), angular.copy(storeToSave));
      if (formMenuItemStock.$valid && !$scope.menuItemStockForm.isPrint) {
        if (itemToSave.length > 0) {
          $scope.transaction._items = itemToSave;
          $scope.transaction._store = sToSave;
          $scope.transaction.isOpen = false;
          $scope.transaction.processTransaction($scope.menuItemStockForm, 13);
          if ($scope.transaction.isEdit == true) {
            $scope.transaction.updateTransaction({}, 'update').then(function (results) {
              if (results != null) {
                $scope.menuItemStockForm.isPrint = true;
                $scope.menuItemStockForm.isEdit = true;
                $scope.menuItemStockForm.isSaved = true;
              }
            });
          }
          else {
            $scope.transaction.billDate = new Date($scope.menuItemStockForm.billDate);
            $scope.transaction.updateTransaction({}, 'insert').then(function (results) {
              if (results != null) {
                $scope.menuItemStockForm.isPrint = true;
                $scope.menuItemStockForm.isEdit = true;
                $scope.menuItemStockForm.isSaved = true;
                $scope.transaction.isEdit = true;
              }
            });
          }
        }
      }
      else {
        printPhysicalStock(itemToSave);
      }
    };

    //-------------------------------------------------Intermediate Opening End-------------------------------------------//
    function printPhysicalStock(itemToSave) {
      var table = "";
      table += "<table style='width:100%;' border='1' cellspacing='0' cellpadding='0' class='table table-curved' id='printTable'>";
      table += "<thead><th>Items</th><th>Qty</th><th>Unit</th></thead>";
      table += "<tbody>";

      _.forEach(itemToSave, function (item, i) {
        table += "<tr>";
        table += "<td>" + item.itemName + "</td>";
        table += "<td>" + item.qty + "</td>";
        table += "<td>" + item.selectedUnit.unitName + "</td>";
        table += "</tr>";
      });
      table += "</tbody>";
      table += "</table>";
      var printer = window.open('', '', 'width=600,height=600');
      printer.document.open("text/html");
      printer.document.write(table);
      printer.document.close();
      printer.focus();
      printer.print();
    };

    var guid = (function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }

      return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
      };
    })();

    $scope.checkForBackDateEntry = function (v) {
      v.open = true;
      if (Utils.isUserPermitted(currentUser, 'Allow Back Date Entry')) {
        v.open = true;
      }
    }

    var guid = function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }

      return function () {
        return (s4() + s4()
        + '-' +
        s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4()
        + '-'+
        s4() + s4() + s4());
      };
    };
  }]);
