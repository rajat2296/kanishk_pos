'use strict';

describe('Controller: StoreManagementCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var StoreManagementCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StoreManagementCtrl = $controller('StoreManagementCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
