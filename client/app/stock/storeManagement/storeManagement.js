'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('storeManagement', {
        url: '/storeManagement',
        templateUrl: 'app/stock/storeManagement/storeManagement.html',
        controller: 'StoreManagementCtrl'
      });
  });
