'use strict';

angular.module('posistApp')
  .controller('AdminCtrl', ['$cookies','$rootScope', '$scope', '$state', '$stateParams', '$modal', '$http', 'Auth', 'User', 'Deployment', 'currentUser', 'Customer', 'Sync', '$location', 'growl', 'localStorageService', 'socket','Utils', function ($cookies,$rootScope, $scope, $state, $stateParams, $modal, $http, Auth, User, Deployment, currentUser, Customer, Sync, $location, growl, localStorageService, socket,Utils) {
    // if($location.search().code){
    //   console.log($location.search().code)
    //   console.log($cookies['shopName'])
    //   var data = {
    //     shopName : $cookies['shopName'],
    //     apiKey : $cookies['apiKey'],
    //     sharedSecret : $cookies['sharedSecret'],
    //     tempCode :  $location.search(),
    //     tenantId : localStorageService.get('tenant_id'),
    //     deploymentId : $cookies['deploymentId'],
    //   }
    //   // $http.post('/api/shopify/getToken',data).then(function(result,err){
    //   //   if(result) {
    //   //     console.log(result)
    //   //     growl.success('Token Successfully Created', {ttl: 3000});
    //   //   }
    //   // })
    // }
    $rootScope.deploymentList=true;
    $rootScope.loading=true;
    $scope.options = {
      detailShow: true,
    }
    $scope.showHide=function(req,req1) {
      _.forEach($scope.options,function(value,key){
        $scope.options[key]=false;
      })
      $scope.options[req]=true;
      if(req=='settingShow')
        $scope.settingToShow=req1;


    }

    $scope.isPermission = function (permissions) {
      var flag = false;
      if (currentUser.role === 'user') {
        // flag= true;
        //   } else {
        var permissionArr = permissions.split(',');
        // console.log(permissionArr);
        _.forEach(permissionArr, function (permname) {
          if (Utils.hasPermission(currentUser.selectedPermissions, permname)) {
            flag = true;
          }
        })
      } else {
        flag = true;
      }
      return flag;
    }
    $scope.getGroupPermission = function (permissions) {
      var flag = false;
      if (currentUser.role === 'user') {
        // flag= true;
        //   } else {
        var permissionArr = permissions.split(',');
        // console.log(permissionArr);
        _.forEach(permissionArr, function (permname) {
          // alert(permname);
          if (Utils.hasUserPermission(currentUser.selectedPermissions, permname)) {
            flag = true;
          }
        })
      } else {
        flag = true;
      }
      return flag;
    }

    $scope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {

      /* Redirect to Admin if User is SuperAdmin */
      /*if (currentUser.role === 'user') {
       $state.go('billing.table', {tabId:25});
       }*/


    });
    if(currentUser.role=='user') {
      if (!(Utils.hasUserPermission(currentUser.selectedPermissions, 'Menu')|| Utils.hasUserPermission(currentUser.selectedPermissions, 'Users')|| Utils.hasUserPermission(currentUser.selectedPermissions, 'Settings'))) {
        growl.error('00ps !! have permission ?', {ttl: 3000});
        $location.path('/login');
      }
    }else{

    }

    $scope.isAdmin=function(){
      var flag=true;
      if(currentUser.role=='superadmin'){
        flag=false;
      }
      return flag;
    }
    $scope.isDeploymentOwner=function(dep_id){
      var flag=false;
      if(currentUser.role=='superadmin'){
        flag=true;
      }else
      {
        if(currentUser.deployment_id==dep_id){
          flag=true;
        }
      }
      return flag;
    }

    $scope.currentUser = currentUser;

    $scope.deploymentForm = {};

    $scope.setPage = function (page) {
      $state.transitionTo(page);
    };


    $scope.deployments = [];
    Deployment.get({tenant_id: localStorageService.get('tenant_id')}).$promise.then(function (deployments) {
      $scope.deployments = deployments;
    }).catch(function (err) {
      growl.error('Could not fetch Deployments!', {ttl: 3000});
    });


    $scope.addDeployment = function (size) {
      var addDeploymentModalInstance = $modal.open({
        templateUrl: 'app/admin/deployments/addDeployment/modalAddDeployment.html',
        controller: 'AdddeploymentCtrl',
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });

      addDeploymentModalInstance.result.then(function (deployment) {
        $scope.deployments.push(deployment);

        /* Ask if Master is to be cloned */
        /*$modal.open({
         templateUrl: 'app/admin/deployments/modalCloneMaster.html',
         controller: function ($scope, deployment, $modalInstance) {
         $scope.deployment = deployment;
         $scope.importDataFromMaster = function (d) {
         console.log(d);
         };

         $scope.cancelImport = function () {
         $modalInstance.dismiss('cancel');
         }
         },
         resolve: {
         deployment: function () {
         return deployment;
         }
         },
         size:''
         }).result.then(function () {
         console.info('Modal dismissed at: ' + new Date());
         });*/
        /* - - - - - - - - - - - - - - - - - */


      }, function () {
        console.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.clearAllData = function (d) {


    };
    /* Import from Master */
    $scope.importDataFromMaster = function (d) {
      /* Get Master Deployment */
      var _masterDeploymentId = _.filter($scope.deployments, {isMaster: true});
      $rootScope.showLoaderOverlay = true;
      /* Start copying master data to specified deployment */
      if (_masterDeploymentId.length > 0) {
        Sync.importMasterData(d, _masterDeploymentId[0]).then(function (masterData) {
          Sync.saveBatchDeployment(masterData);
          $rootScope.showLoaderOverlay = false;
        });
      } else {
        growl.error("You don't have any Master Data created!", {ttl:3000});
      }


    };

    $scope.gotoBilling = function (d) {
      var _depId = localStorageService.get('deployment_id');
      console.log(d);
      if (_depId === null) {
        //if (currentUser.role === 'superadmin') {
        if (_.has(d, 'instances')) {
          if (d.instances.length > 0) {
            // alert('instance');

            $modal.open({
              templateUrl: 'app/admin/instance.html',
              controller: ['$scope', '$modalInstance','instances', function ($scope, $modalInstance,instances) {
                $scope.instances=instances;
                $scope.cancel = function () {
                  $modalInstance.dismiss('')
                };
                $scope.selectedInstance = function (instance) {
                  $modalInstance.close(instance);
                }

              }],
              size: 'md',
              resolve:{
                instances:function(){
                  return  d.instances;
                }
              }
            }).result.then(function (instance) {
              var instanceSetting=Utils.getSettingValue('instance_server',d.settings);
              $rootScope.url= instanceSetting==null||instanceSetting==undefined?'http://jdd.posist.org':instanceSetting;
              $rootScope.isInstance=true;
              localStorageService.set('deployment_id', d._id);
              localStorageService.set('instanceId', instance);
              $rootScope.$state.go('billing');
            });
          }else{
            localStorageService.set('deployment_id', d._id);
            localStorageService.set('instanceId', {});
            $rootScope.$state.go('billing');
          }

          // $rootScope.$state.go('billing');
          /*if (_depId != d._id) {
           growl.error('This Deployment does not belong to this Instance, cannot start Billing!', {ttl:3000});
           } else {
           $rootScope.$state.go('billing');
           }*/

        }else{
          localStorageService.set('deployment_id', d._id);
          localStorageService.set('instanceId',{});
          $rootScope.$state.go('billing');
        }
        // }
      } else {

        if ( _depId === d._id) {
          $rootScope.$state.go('billing');
          /*console.log(d);
           if (_depId != d._id) {
           growl.error('This Deployment does not belong to this Instance, cannot start Billing!', {ttl:3000});
           } else {
           $location.path('/billing');
           }*/

        } else {
          growl.error("You don't have sufficient rights to start Billing on this Deployment", {ttl:3000});
        }
      }
    };


    /*Sync.syncCustomers($rootScope.currentUser);*/
    /* Open Customer Modal */
    $scope.openCustomerModal = function (size, value) {

      var modalInstance = $modal.open({
        templateUrl: 'app/admin/customers/AddCustomer/modalAddCustomer.html',
        controller: 'CustomerCtrl',
        size: size,
        resolve: {
          number: function () {
            return (_.isNumber(value)) ? value : "";
          }
        }
      });

      modalInstance.result.then(function (customer) {
        $scope.newCustomer = customer;
        $scope.newCustomer.tenant_id = currentUser.tenant_id;
        Customer.save({tenant_id: currentUser.tenant_id}, $scope.newCustomer, function (customer) {
          /*Sync.syncCustomers($rootScope.currentUser).then(function (customers) {
           */
          /*$rootScope.customers = customers;*/
          /*
           console.log(customers);
           });*/
          $rootScope.customers.push(customer);
        });

        /*console.info($scope.newCustomer);*/
      }, function () {
        console.info('Modal dismissed at: ' + new Date());
      });
    };


  }]);
