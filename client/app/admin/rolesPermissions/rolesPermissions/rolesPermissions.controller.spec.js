'use strict';

describe('Controller: RolespermissionsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var RolespermissionsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RolespermissionsCtrl = $controller('RolespermissionsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
