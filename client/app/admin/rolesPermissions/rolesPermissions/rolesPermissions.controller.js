'use strict';

angular.module('posistApp')
  .controller('RolespermissionsCtrl', ['$scope', '$rootScope', 'Role', 'Auth', 'Utils', 'currentUser', 'growl', function ($scope, $rootScope, Role, Auth, Utils, currentUser, growl) {

    $scope.currentUser = currentUser;
    $scope.permissions = $rootScope.permissions;

    /* Role Form */
    $scope.roleForm = {
      isEdit: false
    };
    /* Selected Permissions */
    $scope.selectedPermissions = [];

    /* Roles Collection */
    $scope.roles = [];
    Role.get({tenant_id: $scope.currentUser.tenant_id}).$promise.then(function (roles) {
      $scope.roles = roles;
    });
    $scope.roleErrors = "";
    $scope.createRole = function (form) {
      $scope.submitRoleForm = true;

      if (!$scope.roleForm.isEdit) {

        $scope.roleForm.permissions = $scope.selectedPermissions;
        $scope.roleForm.tenant_id = $scope.currentUser.tenant_id;
        if ($scope.selectedPermissions.length > 0) {
          if (form.$valid) {
            Role.save({}, $scope.roleForm, function (r) {
              $scope.roles.push(r);
              $scope.roleForm = {};
              $scope.selectedPermissions = [];
              growl.success("Role: " + r.name + " has been saved successfully!", {ttl: 3000});
            });
          }
        } else {
          growl.error("Please select Permissions for this Role.", {ttl: 3000});
        }

      } else {

        $scope.roleForm.permissions = $scope.selectedPermissions;
        if ($scope.selectedPermissions.length > 0) {
          if (form.$valid) {
            Role.update({}, $scope.roleForm, function (role) {

              $scope.roles.splice(Utils.arrayObjectIndexOf($scope.roles, "_id", role._id), 1);
              $scope.roles.push(role);
              $scope.roleForm = {
                isEdit: false
              };
              $scope.selectedPermissions = [];

              growl.success("Role: " + role.name + " has been updated successfully!", {ttl: 3000});

            }, function (err) {
              growl.error("Something went wrong while updating!", {ttl: 3000});
            });
          }
        } else {
          growl.error("Please select Permissions for this Role.", {ttl: 3000});
        }


      }


    };

    $scope.editRole = function (r) {
      $scope.roleForm._id = r._id;
      $scope.roleForm.name = r.name;
      $scope.roleForm.tenant_id = r.tenant_id;
      $scope.selectedPermissions = r.permissions;
      $scope.roleForm.isEdit = true;
    };

    $scope.deleteRole = function (r) {

      Role.remove({id: r._id}, function (role) {
        $scope.roles.splice(Utils.arrayObjectIndexOf($scope.roles, "_id", role._id), 1);
        growl.success("Role: " + role.name + " deleted successfully!", {ttl: 3000});
      }, function (err) {
        growl.error("Something went wrong while deleting!", {ttl: 3000});
      });

    };

    $scope.updateSelected = function (action, p) {

      if (action == 'add' && Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name) == -1) {
        $scope.selectedPermissions.push(p);
      }
      if (action == 'remove' && Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name) != -1) {
        $scope.selectedPermissions.splice(Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name), 1);
      }
    };

    $scope.setSelected = function (event, p) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      $scope.updateSelected(action, p);
    };

    $scope.isSelected = function (p) {
      return Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name) >= 0;
    };

    $scope.isSelectedAll = function () {
      return $scope.selectedPermissions.length === $scope.permissions.length;
    };

    $scope.getSelectedClass = function (p) {
      return $scope.isSelected(p) ? 'selected' : '';
    };

    $scope.selectAll = function (event, group) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      var _f = _.filter($scope.permissions, function (i) {
        return i.group == group;
      });
      _.forEach(_f, function (p) {
        $scope.updateSelected(action, p)
      })
    };


  }]);
