'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('smssettings', {
        url: '/smssettings',
        templateUrl: 'app/admin/sms/smssettings/smssettings.html',
        controller: 'SmssettingsCtrl'
      });
  });