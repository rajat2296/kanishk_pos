'use strict';

describe('Controller: SmssettingsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var SmssettingsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmssettingsCtrl = $controller('SmssettingsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
