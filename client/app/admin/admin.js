'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('admin', {
        url: '/admin',
        templateUrl: 'app/admin/admin.html',
        controller: 'AdminCtrl',
        authenticate: true,
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth',
            function ($state, $stateParams, Auth) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                return user;
              });
            }],
          tabs: ['Tab', 'localStorageService', '$timeout', '$rootScope', function (Tab, localStorageService, $timeout, $rootScope) {

            return Tab.get(
              {
                tenant_id: localStorageService.get('tenant_id'),
                deployment_id: localStorageService.get('deployment_id')
              }
            ).$promise.then(function (tabs) {
              //console.log(tabs.length);

            });

          }]
        }
      })
      .state('admin.general', {
        url: '/general',
        templateUrl: 'app/admin/general/index.html',
        controller: 'AdminCtrl'
      })
      .state('admin.addons', {
        url: '/addons',
        templateUrl: 'app/admin/addons/index.html',
        controller: 'AdminCtrl'
      })
      .state('admin.smssettings', {
        url: '/smssettings',
        templateUrl: 'app/admin/sms/smssettings.html',
        controller: 'AdminCtrl',
        resolve: {}
      })
      .state('admin.customers', {
        url: '/customers',
        templateUrl: 'app/admin/customers/index.html',
        controller: 'AdminCtrl',
        resolve: {
          customers: ['$rootScope', 'Customer', 'localStorageService', 'growl', function ($rootScope, Customer, localStorageService, growl) {

            return Customer.get({
              // tenant_id: localStorageService.get('tenant_id')
            }).$promise.then(function (customers) {
              $rootScope.customers = customers;
              growl.success('Customers loaded successfully!', {ttl: 3000});
            }).catch(function (err) {
              /*console.log("Error getting Customers");*/
              growl.error('Error fetching Customers!', {ttl: 3000});
            });

          }]
        }
      })
      .state('admin.deployments', {
        url: '/deployments?code&hmac&shop&signature&state&timestamp',
        templateUrl: 'app/admin/deployments/deployments.html',
        controller: 'AdminCtrl'
      })
      .state('admin.rolesPermissions', {
        url: '/rolesPermissions',
        templateUrl: 'app/admin/rolesPermissions/index.html',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              var flag = true;
              return Auth.getCurrentUser().$promise.then(function (user) {
                if (user.role != 'superadmin') {
                  if (user.deployment_id != $stateParams.deploymentId) {

                    growl.error('00ps !! have permission ?', {ttl: 3000});
                    flag = false;
                    $location.path('/login');
                    // return false;

                  }
                }
                if (flag)
                  return user;
              });
            }]
        },
        controller: 'RolespermissionsCtrl'
      })
      .state('admin.deployments.deploymentDetails', {
        url: '/deployments/deploymentDetails/:deploymentId',
        templateUrl: 'app/admin/deployments/details.html',
        parent: 'admin',
        resolve: {
          deployment: ['$state', '$stateParams', 'Deployment', 'growl', '$timeout', '$rootScope',
            function ($state, $stateParams, Deployment, growl, $timeout, $rootScope) {
              return Deployment.findOne({id: $stateParams.deploymentId}).$promise.then(function (d) {
                var tempSettings = angular.copy(d.settings);
                d.settings = [];
                var allSettings = [];
                var isFound = false;
                _.forEach(tempSettings, function (setting) {
                  isFound = false;
                  for (var i = 0; i < $rootScope.settings.length; i++) {
                    if ($rootScope.settings[i].name == setting.name) {
                      isFound = true;
                      setting.group=$rootScope.settings[i].group;

                      if($rootScope.settings[i].name=='settle_bill_on_print'){
                      if($rootScope.settings[i].tabs && !setting.tabs){
                        setting.tabs=$rootScope.settings[i].tabs;
                        setting.group=$rootScope.settings[i].group;
                        if(setting.selected==true){
                          _.forEach(setting.tabs,function(tab){
                            tab.selected=true;
                          });
                        }
                      }
                    }

                      break;
                    }
                  }
                  if (isFound) {
                    allSettings.push(angular.copy(setting));
                  }
                });
                _.forEach($rootScope.settings, function (setting) {
                  var _fS = _.filter(allSettings, {'name': setting.name});
                  if (_fS.length == 0) {
                    allSettings.push(angular.copy(setting));
                  }
                });
                // console.log($rootScope.settings.length);
                //console.log(allSettings);
                d.settings = allSettings;
                return d;
              }).catch(function (err) {
                growl.error("Couldn't resolve deployment!", {ttl: 3000});
                $state.transitionTo("admin");
              });

            }],
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              var flag = true;
              return Auth.getCurrentUser().$promise.then(function (user) {

                if (user.role == 'user') {
                  if (user.deployment_id == $stateParams.deploymentId) {
                    if (!(Utils.hasUserPermission(user.selectedPermissions, 'Menu') || Utils.hasUserPermission(user.selectedPermissions, 'Users') || Utils.hasUserPermission(user.selectedPermissions, 'Settings'))) {
                      // growl.error('00ps !! have permission ?', {ttl: 3000});
                      flag = false;
                      $location.path('/login');
                    }
                  } else {
                    //  growl.error('00ps !! have permission ?', {ttl: 3000});
                    flag = false;
                    $location.path('/login');
                  }
                }
                if (flag) {
                  // console.log(user);
                  return user;
                }
              });
            }],
          //StockUnits: ['$rootScope', '$state', '$stateParams', 'stockUnit','localStorageService',
          //    function ($rootScope, $state, $stateParams, stockUnit,localStorageService) {
          //      return stockUnit.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //        console.log("Got StockUnits - " +result.length);
          //        return result;
          //
          //      });
          //}],
          //StockRecipes: ['$rootScope', '$state', '$stateParams', 'stockRecipe','localStorageService',
          //    function ($rootScope, $state, $stateParams, stockRecipe,localStorageService) {
          //      return stockRecipe.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //        console.log("Got StockRecipes - " +result.length);
          //        return result;
          //
          //      });
          //}],
          //Receivers: ['$rootScope', '$state', '$stateParams', 'receiver','localStorageService',
          //    function ($rootScope, $state, $stateParams, receiver,localStorageService) {
          //      return receiver.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //        console.log("Got receiver - " +result.length);
          //        return result;
          //
          //      });
          //}],
          //StockVendor: ['$rootScope', '$state', '$stateParams', 'vendor','localStorageService',
          //    function ($rootScope, $state, $stateParams, vendor,localStorageService) {
          //      return vendor.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //       console.log('Stock vendor:'+result.length);
          //        return result;
          //      },function(err){
          //          console.log(err);
          //      });
          //}],
          //Store: ['$rootScope', '$state', '$stateParams', 'store','localStorageService',
          //    function ($rootScope, $state, $stateParams, store,localStorageService) {
          //      return store.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //        return result;
          //      },function(err){
          //          console.log(err);
          //      });
          //}]
        },
        controller: 'DeploymentsCtrl'
      });
  });
