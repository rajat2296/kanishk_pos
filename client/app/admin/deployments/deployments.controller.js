'use strict';

angular.module('posistApp')
  .controller('DeploymentsCtrl', ['$window', '$cookies', 'Shopify', 'UrbanPiper', '$scope', '$rootScope', '$state', '$stateParams', 'deployment', 'Deployment', 'Role', 'Utils', 'Auth', 'Tab', 'Supercategory', 'Category', 'Item', 'Tax', 'currentUser', '$timeout', 'User', 'Tenant', 'growl', 'Station', '$modal', 'SilentPrinter', '$q', 'Pagination', '$http', 'localStorageService', '$aside', '$location', 'DateList', '$ngBootbox', 'OfferCode', 'Smstemplates', 'receiver', 'vendor', 'store', 'stockRecipe', 'stockUnit', 'Nomnom', 'ThirdPartyService', 'sectionResource', 'dayBreakup', 'StockResource', 'stockRequirement', 'baseKitchenItem', 'ComplementaryHead', 'ShiftTime',function ($window, $cookies, Shopify, UrbanPiper, $scope, $rootScope, $state, $stateParams, deployment, Deployment, Role, Utils, Auth, Tab, Supercategory, Category, Item, Tax, currentUser, $timeout, User, Tenant, growl, Station, $modal, SilentPrinter, $q, Pagination, $http, localStorageService, $aside, $location, DateList, $ngBootbox, OfferCode, Smstemplates, receiver, vendor, store, stockRecipe, stockUnit, Nomnom, ThirdPartyService, sectionResource, dayBreakup, StockResource, stockRequirement, baseKitchenItem, ComplementaryHead,ShiftTime) {

    /*Recently added  by ajit*/
$rootScope.deploymentList=false;
    $scope.shopify = {};
    $scope.shopKeys = {};
    $scope.urbanPiper = {};
    $scope.urbanPiperKeys = [];
    $scope.nomnom = {};
    $scope.nomnomKeys = [];
    $scope.charges=[];
    $scope.extraCharge={};
    $scope.details={assignedShift:[],cutoffTime:new Date()};
    $scope.deliveryLocations = [];
    $scope.deliveryLocation = {selected:[]};
    Shopify.get({'tenantId': currentUser.tenant_id, 'deploymentId': deployment._id}, function (tabs) {
      console.log(tabs[0]);
      $scope.shopKeys = tabs
    });
    //   console.log(StockUnits);
    //   console.log(StockRecipes);
    //   console.log(StockVendor);
    //$scope.StockUnits = StockUnits;
    //$scope.StockRecipes = StockRecipes;
    //$scope.StockReceivers = Receivers;
    //$scope.StockVendors = StockVendor;
    //$scope.stores = Store;

    function getStockData () {

      console.log('calling apis');
      stockUnit.get({
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: $stateParams.deploymentId
      }).$promise.then(function (result) {
        //console.log("Got StockUnits - " +result.length);
        $scope.StockUnits = result;
      });
      stockRecipe.get({
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: $stateParams.deploymentId
      }).$promise.then(function (result) {
        //console.log("Got StockRecipes - " +result.length);
        $scope.StockRecipes = result;
      });
      receiver.get({
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: $stateParams.deploymentId
      }).$promise.then(function (result) {
        //console.log("Got receiver - " +result.length);
        $scope.StockReceivers = result;
      });
      vendor.get({
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: $stateParams.deploymentId
      }).$promise.then(function (result) {
        //console.log('Stock vendor:'+result.length);
        $scope.StockVendors = result;
      });
      store.get({
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: $stateParams.deploymentId
      }).$promise.then(function (result) {
        $scope.stores = result;
      });
    }

    if($stateParams.deploymentId != undefined && $stateParams.deploymentId != null){
      getStockData();
    }

    $scope.updateItemsUnit = function (punit, s) {
      var ind = _.findIndex($scope.StockUnits, {_id: s.unit._id});
      s.unit = angular.copy($scope.StockUnits[ind]);
      stockRecipe.findByItemId({
        itemId: s._id,
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id')
      }, function (recipe) {
        var flag = true;
        if (!recipe.err) {
          if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
            growl.warning("Please delete recipe first", {ttl: 3000});
            flag = false;
          }
        }
        if(flag) {
          var tabs = []
          _.forEach(s.tabs, function (tab) {
            //tabs.push({id:tab._id,rate:tab.item.rate})
            tabs.push({id:tab._id,rate:tab.item ? tab.item.rate : s.rate})
          });
          var updateHistory = {
            timeStamp:new Date,
            name:s.name,
            tabs:tabs,
            rate:s.rate,
            username:currentUser.username
          }
          s.lastModified = new Date;
          s.updateHistory.push(updateHistory)
          if(s.updateHistory.length>5){
            s.updateHistory.splice(0,s.updateHistory.length-5);
          }
          Item.update({}, s, function (result) {
            console.log(result);
            growl.success("Unit updated successfully", {ttl: 3000});
          });
          vendor.updateItemUnitInHOVendor({itemId: s._id}, {
            deployment_id: localStorageService.get('deployment_id'),
            tenant_id: localStorageService.get('tenant_id'),
            item: s
          }, function (result) {
            console.log('vendors updated');
          });
        }
      });
    };

    $scope.copyPrevUnit = function (item) {
      if(item.unit)
        item.prevUnit = angular.copy(item.unit);
      //console.log(item.prevUnit);
    };

    // $scope.updateItemsUnitNew = function (punit, s) {

    //   var ind = _.findIndex($scope.StockUnits, {_id: s.unit._id});
    //   s.unit = angular.copy($scope.StockUnits[ind]);
    //   console.log('resolving');
    //   validateMenuItemForStockUnitChange(s).then(function (result) {

    //     console.log(result);
    //     if(result.status) {
    //       var tabs = []
    //       _.forEach(s.tabs, function (tab) {
    //         tabs.push({id:tab._id,rate:tab.item.rate})
    //       });
    //       var updateHistory = {
    //         timeStamp:new Date,
    //         name:s.name,
    //         tabs:tabs,
    //         rate:s.rate,
    //         username:currentUser.username
    //       }
    //       s.lastModified = new Date;
    //       s.updateHistory.push(updateHistory)
    //       if(s.updateHistory.length>5){
    //         s.updateHistory.splice(0,s.updateHistory.length-5);
    //       }
    //       Item.update({}, s, function (result) {
    //         console.log(result);
    //         growl.success("Unit updated successfully", {ttl: 3000});
    //       });
    //       vendor.updateItemUnitInHOVendor({itemId: s._id}, {
    //         deployment_id: localStorageService.get('deployment_id'),
    //         tenant_id: localStorageService.get('tenant_id'),
    //         item: s
    //       }, function (result) {
    //         console.log('vendors updated');
    //       });
    //       baseKitchenItem.updateBaseKitchenItemUnit({}, s, function (updated) {
    //         console.log('baseKitchenItems updated');
    //       }, function (err) {
    //         console.log(err);
    //       });
    //     } else {
    //       growl.error(result.err, {ttl: 3000});
    //     }
    //   }).catch(function (err){
    //     growl.error("Some error occured! Please try again.", {ttl: 3000});
    //   });
    //   // var ind = _.findIndex($scope.StockUnits, {_id: s.unit._id});
    //   // s.unit = angular.copy($scope.StockUnits[ind]);
    //   // stockRecipe.findByItemId({
    //   //   itemId: s._id,
    //   //   tenant_id: localStorageService.get('tenant_id'),
    //   //   deployment_id: localStorageService.get('deployment_id')
    //   // }, function (recipe) {
    //   //   var flag = true;
    //   //   if (!recipe.err) {
    //   //     if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
    //   //       growl.warning("Please delete recipe first", {ttl: 3000});
    //   //       flag = false;
    //   //     }
    //   //   }
    //   //   if(flag) {
    //   //     var tabs = []
    //   //     _.forEach(s.tabs, function (tab) {
    //   //       tabs.push({id:tab._id,rate:tab.item.rate})
    //   //     });
    //   //     var updateHistory = {
    //   //       timeStamp:new Date,
    //   //       name:s.name,
    //   //       tabs:tabs,
    //   //       rate:s.rate,
    //   //       username:currentUser.username
    //   //     }
    //   //     s.lastModified = new Date;
    //   //     s.updateHistory.push(updateHistory)
    //   //     if(s.updateHistory.length>5){
    //   //       s.updateHistory.splice(0,s.updateHistory.length-5);
    //   //     }
    //   //     Item.update({}, s, function (result) {
    //   //       console.log(result);
    //   //       growl.success("Unit updated successfully", {ttl: 3000});
    //   //     });
    //   //     vendor.updateItemUnitInHOVendor({itemId: s._id}, {
    //   //       deployment_id: localStorageService.get('deployment_id'),
    //   //       tenant_id: localStorageService.get('tenant_id'),
    //   //       item: s
    //   //     }, function (result) {
    //   //       console.log('vendors updated');
    //   //     });
    //   //   }
    //   // });
    // };

    $scope.updateItemsUnitNew = function (punit, s) {

      var ind = _.findIndex($scope.StockUnits, {_id: s.unit._id});
      if(!s.prevUnit)
        s.prevUnit = angular.copy(s.unit);
      var prevInd = _.findIndex($scope.StockUnits, {_id: s.prevUnit._id});
      s.unit = angular.copy($scope.StockUnits[ind]);
      s.prevUnit = angular.copy($scope.StockUnits[prevInd]);
      Item.validateItemUnitChange({deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id'), itemId: s._id, baseUnitId: s.unit.baseUnit.id}, function (result){
        if(result.unitChangable){
          s.prevUnit = angular.copy(s.unit);
          var tabs = []
          _.forEach(s.tabs, function (tab) {
            tabs.push({id:tab._id,rate:tab.item ? tab.item.rate : s.rate})
          });
          var updateHistory = {
            timeStamp:new Date,
            name:s.name,
            tabs:tabs,
            rate:s.rate,
            username:currentUser.username
          }
          s.lastModified = new Date;
          s.updateHistory.push(updateHistory)
          if(s.updateHistory.length>5){
            s.updateHistory.splice(0,s.updateHistory.length-5);
          }
          Item.update({}, s, function (result) {
            console.log(result);
            growl.success("Unit updated successfully", {ttl: 3000});
          });
          vendor.updateItemUnitInHOVendor({itemId: s._id}, {
            deployment_id: localStorageService.get('deployment_id'),
            tenant_id: localStorageService.get('tenant_id'),
            item: s
          }, function (result) {
            console.log('vendors updated');
          });
          baseKitchenItem.updateBaseKitchenItemUnit({}, {_id: s._id, tenant_id: s.tenant_id, unit: s.unit}, function (item) {
            console.log('bsitem updated');
            console.log(s.unit);
          });
        } else {
          s.unit = angular.copy(s.prevUnit);
          if(result.showMessage)
            growl.error(result.reason, {ttl: 3000});
          else
            growl.error("Cannot change unit.", {ttl: 3000});
        }
      }, function (err) {
        s.unit = angular.copy(s.prevUnit);
        console.log(err);
        growl.error("Some error occured. Please try again!", {ttl: 3000});
      });
    };

    var validateMenuItemForStockUnitChange = function (s) {

      var deferred = $q.defer();
      stockRecipe.findByItemId({
        itemId: s._id,
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id')
      }, function (recipe){

        if (!recipe.err) {
          StockResource.checkIfMenuItemExistsInTransaction({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

          if(result.transactionFound){
            if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
              //growl.warning("Please delete recipe first", {ttl: 3000});
              deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted."});
            } else deferred.resolve({status: true});
          }
          else {
            stockRequirement.checkIfItemExistsIndentExists({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

              if(result.requirementFound){
                if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
                  //growl.warning("Please delete recipe first", {ttl: 3000});
                  deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted."});
                } else deferred.resolve({status: true});
              //deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted."});
              }
              else {
                stockRecipe.checkIfBaseKitchenItemInRecipe({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

                  if(result.recipeFound){
                    if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
                      //growl.warning("Please delete recipe first", {ttl: 3000});
                      deferred.resolve({status: false, err: "Item is being used in recipes in outlets. Please delete the item from recipes."});
                    } else deferred.resolve({status: true});
                    //deferred.resolve({status: false, err: "Item is being used in recipes in outlets. Please delete the item from recipes."});
                  }
                  else {
                    if (!recipe.err) {
                      if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
                        growl.warning("Please delete recipe first", {ttl: 3000});
                        deferred.resolve({status: false, err: "Please delete recipe first."});
                      } else deferred.resolve({status: true});
                    } else deferred.resolve({status: true});
                  }
                }, function (err) {
                  console.log(err);
                  deferred.reject(err);
                  growl.error("Some error occured! Please try again.", {ttl: 3000});
                });
              }
            }, function (err) {
              console.log(err);
              deferred.reject(err);
              growl.error("Some error occured! Please try again.", {ttl: 3000});
            });
          }
          }, function (err) {
            console.log(err);
            deferred.reject(err);
            growl.error("Some error occured! Please try again.", {ttl: 3000});
          });
        } else {
          deferred.resolve({status: true});
        }
      }, function (err) {

      });

      return deferred.promise;
    }

    var validateMenuItemForStockItemDelete = function (s) {

      var deferred = $q.defer();
      stockRecipe.findByItemId({
        itemId: s._id,
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id')
      }, function (recipe){
        if (!recipe.err) {
          StockResource.checkIfMenuItemExistsInTransaction({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

          if(result.transactionFound){
            if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
              //growl.warning("Please delete recipe first", {ttl: 3000});
              deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted.", mail: true});
            } else deferred.resolve({status: true});
          }
          else {
            stockRequirement.checkIfItemExistsIndentExists({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

              if(result.requirementFound){
                if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
                  //growl.warning("Please delete recipe first", {ttl: 3000});
                  deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted.", mail: true});
                } else deferred.resolve({status: true});
              //deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted."});
              }
              else {
                stockRecipe.checkIfBaseKitchenItemInRecipe({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {
                  if(result.recipeFound){
                    if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
                      //growl.warning("Please delete recipe first", {ttl: 3000});
                      deferred.resolve({status: false, err: "Item is being used in recipes in outlets. Please delete the item from recipes."});
                    } else deferred.resolve({status: true});
                    //deferred.resolve({status: false, err: "Item is being used in recipes in outlets. Please delete the item from recipes."});
                  }
                  else {
                    if (!recipe.err) {
                      if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
                        growl.warning("Please delete recipe first", {ttl: 3000});
                        deferred.resolve({status: false, err: "Please delete recipe first."});
                      } else deferred.resolve({status: true});
                    } else deferred.resolve({status: true});
                  }
                }, function (err) {
                  console.log(err);
                  deferred.reject(err);
                  growl.error("Some error occured! Please try again.", {ttl: 3000});
                });
              }
            }, function (err) {
              console.log(err);
              deferred.reject(err);
              growl.error("Some error occured! Please try again.", {ttl: 3000});
            });
          }
          }, function (err) {
            console.log(err);
            deferred.reject(err);
            growl.error("Some error occured! Please try again.", {ttl: 3000});
          });
        } else {
          deferred.resolve({status: true});
        }
      }, function (err) {

      });

      return deferred.promise;
    }

    var validateMenuItemForItemDelete = function (s) {

      var deferred = $q.defer();
      // stockRecipe.findByItemId({
      //   itemId: s._id,
      //   tenant_id: localStorageService.get('tenant_id'),
      //   deployment_id: localStorageService.get('deployment_id')
      // }, function (recipe){

      //   if (!recipe.err) {
      //     StockResource.checkIfMenuItemExistsInTransaction({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

      //     if(result.transactionFound){
      //       if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
      //         //growl.warning("Please delete recipe first", {ttl: 3000});
      //         deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted."});
      //       } else deferred.resolve({status: true});
      //     }
      //     else {
      //       stockRequirement.checkIfItemExistsIndentExists({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

      //         if(result.requirementFound){
      //           if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
      //             //growl.warning("Please delete recipe first", {ttl: 3000});
      //             deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted."});
      //           } else deferred.resolve({status: true});
      //         //deferred.resolve({status: false, err: "Item has been used in transactions and cannot be deleted."});
      //         }
      //         else {
      //           stockRecipe.checkIfBaseKitchenItemInRecipe({itemId: s._id, deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result) {

      //             if(result.recipeFound){
      //               if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
      //                 //growl.warning("Please delete recipe first", {ttl: 3000});
      //                 deferred.resolve({status: false, err: "Item is being used in recipes in outlets. Please delete the item from recipes."});
      //               } else deferred.resolve({status: true});
      //               //deferred.resolve({status: false, err: "Item is being used in recipes in outlets. Please delete the item from recipes."});
      //             }
      //             else {
      //               if (!recipe.err) {
      //                 if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
      //                   growl.warning("Please delete recipe first", {ttl: 3000});
      //                   deferred.resolve({status: false, err: "Please delete recipe first."});
      //                 } else deferred.resolve({status: true});
      //               } else deferred.resolve({status: true});
      //             }
      //           }, function (err) {
      //             console.log(err);
      //             deferred.reject(err);
      //             growl.error("Some error occured! Please try again.", {ttl: 3000});
      //           });
      //         }
      //       }, function (err) {
      //         console.log(err);
      //         deferred.reject(err);
      //         growl.error("Some error occured! Please try again.", {ttl: 3000});
      //       });
      //     }
      //     }, function (err) {
      //       console.log(err);
      //       deferred.reject(err);
      //       growl.error("Some error occured! Please try again.", {ttl: 3000});
      //     });
      //   }
      // }, function (err) {

      // });
      $q.all([
          vendor.findHOVendorWithItem({tenant_id: localStorageService.get('tenant_id'), deployment_id: localStorageService.get('deployment_id'), itemId: s._id}),
          checkIfRecipeExists(s),
          checkIfBaseKitchenItemUsedInOutletRecipe(s)
        ]).then(function (result) {
          //console.log(result);
        if(result.vendorFound)
          deferred.resolve({status: false, err: "Cannot delete items being used by other outlets."});
        else
          return deferred.resolve({status: true})
        }).catch(function (err) {
          deferred.reject(err);
        });
      // vendor.findHOVendorWithItem({tenant_id: localStorageService.get('tenant_id'), deployment_id: localStorageService.get('deployment_id'), itemId: s._id}, function (result){

      //   console.log(result);
      //   if(result.vendorFound)
      //     deferred.resolve({status: false, err: "Cannot delete items being used by other outlets."});
      //   else
      //     return deferred.resolve({status: true})
      // }, function (err){
      //   deferred.reject(err);
      // });

      return deferred.promise;
    }

    var checkIfRecipeExists = function (s) {

      var deferred = $q.defer();
      stockRecipe.findByItemId({
        itemId: s._id,
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id')
      }, function (recipe) {
        var flag = true;
        if (!recipe.err) {
          if (recipe.selectedUnitId.baseUnit.id != s.unit.baseUnit.id) {
            growl.warning("Please delete recipe first", {ttl: 3000});
            flag = false;
          }
        }
        if(flag) {
          var tabs = []
          _.forEach(s.tabs, function (tab) {
            tabs.push({id:tab._id,rate:tab.item.rate})
          });
          var updateHistory = {
            timeStamp:new Date,
            name:s.name,
            tabs:tabs,
            rate:s.rate,
            username:currentUser.username
          }
          s.lastModified = new Date;
          s.updateHistory.push(updateHistory)
          if(s.updateHistory.length>5){
            s.updateHistory.splice(0,s.updateHistory.length-5);
          }
        }
        deferred.resolve(recipe);
      });
    }

    var checkIfBaseKitchenItemUsedInOutletRecipe = function(s) {
      stockRecipe.findByRecipeItemId({
        itemId: s._id,
        tenant_id: localStorageService.get('tenant_id'),
        deployment_id: localStorageService.get('deployment_id')
      }, function (recipe) {
        if(!recipe.err){
          if(recipe){
            growl.error("Item is being used by an", {ttl: 3000})
          }
        }
      });
    }

    $scope.updateStockQuantity=function(item){

      var tabs = []
      _.forEach(item.tabs, function (tab) {
        //tabs.push({id:tab._id,rate:tab.item.rate})
        tabs.push({id:tab._id,rate:tab.item ? tab.item.rate : item.rate})
      });
      var updateHistory = {
        timeStamp:new Date,
        name:item.name,
        tabs:tabs,
        rate:item.rate,
        username:currentUser.username
      }
      item.lastModified = new Date;
      item.updateHistory.push(updateHistory)
      if(item.updateHistory.length>5){
        item.updateHistory.splice(0,item.updateHistory.length-5);
      }
      Item.update({},item,function(result){
        console.log(result);
      });
    };


    //console.log($scope.StockUnits);
    /*$scope.showKeyboard = function () {
     console.log("Show keyboard");
     $aside.open({
     templateUrl: 'app/admin/deployments/keyboard.html',
     controller: function () {

     },
     placement: 'bottom',
     size: 'lg',
     backdrop:'static'
     });
     }*/

    /* Loading Flag */
    $rootScope.loading = true;
    //console.log(Utils.hasUserPermission(currentUser.selectedPermissions,'Menu'));
    // console.log(deployment);
    /*if(currentUser.role!='superadmin') {
     if(currentUser.deployment_id==deployment._id) {
     if (!Utils.hasUserPermission(currentUser.selectedPermissions, 'Menu')) {
     growl.error('00ps !! have permission ?', {ttl: 3000});
     $location.path('/login');
     }
     }else
     {
     growl.error('00ps !! have permission ?', {ttl: 3000});
     $location.path('/login');

     }
     }*/
    /* Permissions Set */
    jQuery.event.props.push('dataTransfer');
    $scope.permissions = $rootScope.permissions;
    $scope.isAdmin = function () {
      var flag = false;
      if (currentUser.role == 'superadmin') {
        flag = true;
      }
      return flag;
    }
    /* Selected Permissions & Roles */
    $scope.selectedRoles = [];
    $scope.selectedPermissions = [];
    // $scope.units = ['BAG10KG', 'Box', 'DOZEN', 'GarlicSauceCase', 'GM', 'Gram', 'KG', 'LITRE', 'ML',
    //   'Nos', 'Pac', 'Pack', 'pattycase', 'Pc', 'PCS', 'Set'];
    /* Helper Function for Roles & Permissions */
    $scope.updateSelectedRole = function (action, role) {
      if (action === 'add' && Utils.arrayObjectIndexOf($scope.selectedRoles, 'name', role.name) == -1) {
        $scope.selectedRoles.push(role);
        _.forEach(role.permissions, function (p) {
          var _f = _.filter($scope.selectedPermissions, function (perm) {
            return perm.name === p.name;
          });
          if (_f.length == 0) {
            $scope.selectedPermissions.push(p);
          }
        });
      }


      if (action === 'remove' && Utils.arrayObjectIndexOf($scope.selectedRoles, 'name', role.name) != -1) {
        $scope.selectedRoles.splice(Utils.arrayObjectIndexOf($scope.selectedRoles, 'name', role.name), 1);

        $scope.selectedPermissions = [];
        _.forEach($scope.selectedRoles, function (role) {
          _.forEach(role.permissions, function (permission) {
            $scope.selectedPermissions.push(permission);
          });
        });
      }
    };

    $scope.updateSelectedPermission = function (action, p) {
      if (action == 'add' && Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name) == -1) {
        $scope.selectedPermissions.push(p);
      }
      if (action == 'remove' && Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name) != -1) {
        $scope.selectedPermissions.splice(Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name), 1);
      }
    };

    /* Deployment User Struct */
    $scope.deploymentUserForm = {
      email: Utils.getRandomEmail()
    };

    /* System Roles */
    $scope.roles = [];
    Role.get({tenant_id: currentUser.tenant_id}, function (roles) {
      $scope.roles = roles;
    });

    /* Current Deployment */
    $scope.deployment = deployment;

    $scope.updateDeployment = function (form) {

      if (form.$valid) {
        Deployment.update({id: deployment._id}, $scope.deployment, function (result) {
          console.log(result);
          growl.success('Deployment successfully updated!', {ttl: 3000});
        }, function (err) {
          growl.error('Error Updating Deployment', {ttl: 3000});
        });
      }

    };

    $scope.setSelectedRole = function (event, role) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      $scope.updateSelectedRole(action, role);
    };

    $scope.isSelectedPermission = function (event, p) {
      return Utils.arrayObjectIndexOf($scope.selectedPermissions, 'name', p.name) >= 0;
    };

    $scope.isSelectedRole = function (event, r) {
      return Utils.arrayObjectIndexOf($scope.selectedRoles, 'name', r.name) >= 0;
    };

    $scope.selectAll = function (event, group) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      var _f = _.filter($scope.permissions, function (i) {
        return i.group == group;
      });
      _.forEach(_f, function (p) {
        $scope.updateSelectedPermission(action, p)
      })
    };

    $scope.setSelected = function (event, p) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      $scope.updateSelectedPermission(action, p);
    };

    $scope.deploymentUsers = [];

    /* Create User with Roles & Permissions */
    $scope.userCreateErrors = "";

    $scope.clearUserForm = function () {
      $scope.deploymentUserForm = {};
      $scope.selectedPermissions = [];
      $scope.selectedRoles = [];
    };

    $scope.editUser = function (user) {
      $scope.deploymentUserForm = {};
      $scope.deploymentUserForm = angular.copy(user);
      $scope.deploymentUserForm.isEditUser = true;
      $scope.selectedPermissions = $scope.deploymentUserForm.selectedPermissions;
      $scope.selectedRoles = $scope.deploymentUserForm.selectedRoles;
      console.log($scope.deploymentUserForm);
      //biometric
      if($scope.deploymentUserForm.details)
      $scope.details = $scope.deploymentUserForm.details;
      else
        $scope.details={};
      console.log($scope.details)
      if($scope.details && $scope.details.assignedShift){
        _.forEach($scope.details.assignedShift,function(shift){
          $scope.shifts[Utils.arrayObjectIndexOf($scope.details.assignedShift, "_id", shift._id)].selected=true;
        })
      }
      else{
        $scope.details.assignedShift=[];
      }
      //biometric
    };

    $scope.createUser = function (form) {
      //console.log(form);
      //console.log($scope.details,$scope.users);

      Tenant.get({id: currentUser.tenant_id}, function (t) {
        console.log(t);
        $scope.submittedUserForm = true;

        if ($scope.selectedPermissions.length > 0 || $scope.selectedRoles.length > 0) {
            if($scope.details && $scope.details.userId){
              if(_.find($scope.deploymentUsers,function(user){
               if(user.details && user.details.userId){
                 if($scope.deploymentUserForm.isEditUser) {
                   if($scope.deploymentUserForm._id == user._id && user.details.userId == $scope.details.userId)
                     return false;
                   else{
                     return user.details.userId == $scope.details.userId;
                   }
                 }
               }
               else{
                 return false;
               }
             })){
               growl.error("User Id already assigned!", {ttl: 3000});
               return;
             }

           }
            if (form.$valid && !$scope.deploymentUserForm.isEditUser) {

              $scope.deploymentUserForm.selectedRoles = $scope.selectedRoles;
              $scope.deploymentUserForm.selectedPermissions = $scope.selectedPermissions;
              $scope.deploymentUserForm.tenant_id = currentUser.tenant_id;
              $scope.deploymentUserForm.deployment_id = $scope.deployment._id;
              $scope.deploymentUserForm.role = 'user';
              $scope.deploymentUserForm.name = t.name;
              $scope.deploymentUserForm.subdomain = t.subdomain;
              $scope.deploymentUserForm.email = Utils.getRandomEmail();
              $scope.deploymentUserForm.mobile= t.mobile;
              $scope.deploymentUserForm.details = $scope.details;//biometric
              if(hasDuplicatePasscode($scope.deploymentUserForm,$scope.deploymentUsers,'create')){
                growl.error("Duplicate passcode",{ttl:3000});
                $scope.submittedUserForm=false;
                return false;
              }
              console.log($scope.deploymentUserForm.details)
              /* Prefix Subdomain in Username */
              var _username = $scope.deploymentUserForm.username;
              $scope.deploymentUserForm.username = t.subdomain + '_' + _username;
              //  $scope.deploymentUserForm.username = _username;
              User.save({}, $scope.deploymentUserForm, function (data) {
                $scope.deploymentUsers.push(data.user);
                $scope.deploymentUserForm = {};
                $scope.selectedRoles = [];
                $scope.selectedPermissions = [];
                $scope.details={assignedShift:[],cutoffTime:new Date()};
                _.forEach($scope.shifts,function(shift){
                  shift.selected = false;
            })
                growl.success("User Saved",{ttl:1000});
              }, function (err) {
                $scope.deploymentUserForm.username = _username;
                $scope.userCreateErrors = err;
                growl.error("Duplicate name found.",{ttl:1000});
              });
              $scope.submittedUserForm = false;

            } else {

              // Update
              console.log($scope.deploymentUserForm, $scope.deploymentUsers)
              if(hasDuplicatePasscode($scope.deploymentUserForm,$scope.deploymentUsers,'update')){
                growl.error("Duplicate passcode",{ttl:3000});
                $scope.submittedUserForm=false;
                return false;
              }

              $scope.deploymentUserForm.details = $scope.details;
              User.save({}, $scope.deploymentUserForm, function (data) {
                var deploymentUsers = angular.copy($scope.deploymentUsers);
                _.forEach(deploymentUsers, function (user, i) {
                  if (user._id == data._id) {
                    $scope.deploymentUsers.splice(i, 1);

                  }
                })
                $scope.deploymentUsers.push(data);
                $scope.deploymentUserForm = {};
                $scope.selectedRoles = [];
                $scope.selectedPermissions = [];
                $scope.details={assignedShift:[],cutoffTime:new Date()};//biometric
                _.forEach($scope.shifts,function(shift){
                  shift.selected = false;
                })
                growl.success("User Saved",{ttl:1000});
              }, function (err) {
                growl.error("Error while Updating User!");
              });
              $scope.submittedUserForm = false;
            }
        } else {
          growl.error("Please select a Role!", {ttl: 3000});
        }

      });
    };

    $scope.deleteUser = function (user) {
      User.remove({id: user._id}, function () {
        $scope.deploymentUsers.splice(Utils.arrayObjectIndexOf($scope.deploymentUsers, "_id", user._id), 1);
      });
    };


    /* TABS */
    $scope.tabForm = {
      tabInEdit: false,
      items: []
    };
    $scope.tabs = [];
    $scope.selectedTabs = [];

    $scope.categories = [];
    $scope.tabCategories = [];

    $scope.lastItemNumber = 0;

    $scope.updateDeploymentSettings = function () {
      console.log($scope.deployment);
      var d = $q.defer();
      var minOrderDelivery = _.filter($scope.deployment.settings, {name: 'minimum_order_delivery'});
      if (minOrderDelivery[0].selected && Number(minOrderDelivery[0].value)<0) {
        growl.error("Minimum Order On Delivery can't be negative", {ttl: 3000});
        d.reject();
      }
      else {
        Deployment.update({id: deployment._id}, $scope.deployment, function (result) {
          growl.success('Deployment Settings successfully updated!', {ttl: 3000});
          d.resolve(result);
        }, function (err) {
          growl.error('Error Updating Deployment Settings!', {ttl: 3000});
          d.reject();
        });
      }
      return d.promise;
    };

    $scope.gettingStartedFinishTabCatMap = function () {
      $scope.mapCatToTab().then(function () {
        $scope.mapItemToTab();
      });
    };

    $scope.gettingStartedFinishTabItemsMap = function () {
      $scope.mapTabsToItem().then(function () {
        $scope.mapItemTaxToTab();
      });
    };

    /* Printer Stations */
    $scope.printStation = {
      psInEdit: false
    };
    $scope.stations = [];
    $scope.systemPrinters = [];

    $scope.mapCategoriesToTab = function () {
      _.forEach($scope.tabs, function (temptab, i) {
        temptab.masterCategories = [];
        temptab.mapAllCat = null;
        _.forEach($scope.categories, function(cat,i){
          temptab.masterCategories.push(cleanCategory(cat));
          var filteredarray = _.filter(temptab.categories, {_id: cat._id});
          temptab.masterCategories[i].selected = filteredarray.length > 0 ? true : false;
          temptab.masterCategories[i].oldSelected = temptab.masterCategories[i].selected;
        });
      });
    };

    /* Get Stations - Entering the Pyramid of Doom */
    /*$rootScope.showLoaderOverlay = true;*/
    Station.get({tenant_id: localStorageService.get('tenant_id'), deployment_id: $scope.deployment._id}, function (stations) {
      console.log("Got Stations - " + stations.length);
      $scope.stations = stations;
       $scope.filteredStations=[];

      _.forEach($scope.stations,function(st){
        if(!st.isMaster)
          $scope.filteredStations.push(st);
      });
      SilentPrinter.getPrinters($rootScope.currentUser).then(function (printers) {
        _.forEach(printers, function (p) {
          if (p.name != "") {
            $scope.systemPrinters.push(p.name);
          }
        });

      }).catch(function (err) {
        growl.error("Error: Couldn't connect to Print Server!", {ttl: 3000});
      });

      /* Get Supercategories */
      Supercategory.get({tenant_id: localStorageService.get('tenant_id'), deployment_id: $scope.deployment._id}, function (scs) {
        console.log("Got Supercategorys - " + scs.length);
        $scope.superCategories = scs;

        /* Get Tabs */
        Tab.get({'tenant_id': currentUser.tenant_id, 'deployment_id': $scope.deployment._id}, function (tabs) {
          console.log("Got Tabs - " + tabs.length);
          $scope.tabs = tabs;
          _.forEach($scope.tabs, function (t) {
            t.items = [];
            t.itemTabPagination = new Pagination.getNew(25);
            t.itemTabPagination.numPages = Math.ceil($scope.items.length / t.itemTabPagination.perPage);
            t.itemTabPagination.totalItems = $scope.items.length;
          });
          $scope.tabCategories = [];

          /* Get Categories */
          Category.get({tenant_id: localStorageService.get('tenant_id'), deployment_id: $scope.deployment._id}, function (categories) {
            console.log("Got Categories - " + categories.length);
            $scope.categories = categories;
            _.forEach($scope.categories, function (t, i) {
              t.selected = false;
              t.icon = '<i class="fa fa-list"></i>';
            });

            _.forEach($scope.tabs, function (temptab, i) {
              temptab.masterCategories = angular.copy($scope.categories);
              _.forEach(temptab.masterCategories, function (tempcat, j) {
                tempcat = cleanCategory(tempcat);
                var filteredarray = _.filter(temptab.categories, {_id: tempcat._id});
                tempcat.selected = filteredarray.length > 0 ? true : false;
              });
            });

            /* Get Items */
            Item.get({tenant_id: localStorageService.get('tenant_id'), deployment_id: $scope.deployment._id}, function (items) {
              console.log("Got Items - " + items.length);
              $scope.items =  items;
              //$scope.items =[];
              var it = _.max($scope.items,'number')
              $scope.lastItemNumber = it.number;
              $scope.itemForm.number = it.number+1;

              /* Get Taxes */
              Tax.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.deployment._id}, function (taxes) {
                console.log("Got Taxes - " + taxes.length);
                $scope.taxes = taxes;

                DateList.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.deployment._id}, function (datelists) {
                  console.log("Got Date Lists-" + datelists.length);
                  $scope.dateLists=datelists;
                  OfferCode.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.deployment._id}, function (offercodes) {
                    console.log("Got Offer Codes-" + offercodes.length);
                    $scope.offerCodes=offercodes;

                    /* Get Users */
                    User.all({
                      'tenant_id': currentUser.tenant_id,
                      'deployment_id': $state.params.deploymentId,
                      'role': 'user'
                    }, function (users) {
                      console.log("Got Users - " + users.length);
                      $scope.deploymentUsers = users;


                      $scope.itemPagination = Pagination.getNew(25);
                      $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
                      $scope.itemPagination.totalItems = $scope.items.length;


                      /* Get Partners @author - Shubhank*/
                      ThirdPartyService.getPartners(deployment._id).success(function(partners){
                        $scope.partners = partners;

                         ComplementaryHead.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.deployment._id},function(heads){
                          $scope.heads = heads;

                          ShiftTime.get({tenant_id: currentUser.tenant_id, deployment_id: $scope.deployment._id},function(shifts){
                            $scope.shifts = shifts;
                            console.log(shifts)
                            /* Loading Done... */
                            $timeout(function () {
                              $rootScope.loading = false;
                              /* $rootScope.showLoaderOverlay = false;*/
                            }, 2000);


                          },function(err){
                            growl.error('Error while Shifts.',{ttl:3000});
                          });
                          //biometric


                        },function(err){
                          growl.error('Error while Complementary Heads.',{ttl:3000});
                        });


                      }).error(function(err){
                        growl.error('Error while loading partners.',{ttl:3000});
                      });


                    }, function (err) {
                      growl.error("Error while fetching Users!", {ttl: 3000});
                    });
                  }, function (err) {
                    growl.error("Error while fetching date lists !", {ttl: 3000});
                  });

                }, function (err) {
                  growl.error("Error while fetching date lists !", {ttl: 3000});
                });

              }, function (err) {
                growl.error("Error while fetching Taxes!", {ttl: 3000});
              });

            }, function (err) {
              growl.error("Error while fetching Items!", {ttl: 3000});
            });

          }, function (err) {
            growl.error("Error while fetching Categories!", {ttl: 3000});
          });

        }, function (err) {
          growl.error("Error while fetching Tabs!", {ttl: 3000});
        });

      }, function (err) {
        growl.error("Error while fetching Supercategorys!", {ttl: 3000});
      });

    }, function (err) {
      growl.error("Error Getting Stations!", {ttl: 3000});
    });

    $scope.isCollapsedItemForm = true;
    $scope.toggleItemFormCollapse = function (keepOpen) {
      if(keepOpen)
        $scope.isCollapsedItemForm = true;
      $scope.isCollapsedItemForm = !$scope.isCollapsedItemForm;
    };


    /* Menu Item Import */
    $scope.isCollapsedImportForm = true;
    $scope.toggleImportForm = function () {
      $scope.isCollapsedImportForm = !$scope.isCollapsedImportForm;
    };

    var modalInstanceForImport;

    $scope.onFinishImport = function (data) {
      console.log(data);
      angular.element(".import input").val("");

      var _errors = [];
      var isCsvValid = false;
      var csvHeadersCheck = true;
      var csvHeaders = ['ItemNumber', 'Item Name', 'Item Description', 'Rate', 'Category Name', 'SuperCategory Name'];

      for(var x in data[0]){
        if(csvHeaders.indexOf(x) == -1){
          csvHeadersCheck = false
          break;
        }
      }

      if(csvHeadersCheck){
        modalInstanceForImport = $modal.open({
          template: '<br><h2 class="text-center">{{csvImportStatus}}</h2><br><progressbar ng-if="progress" animate="false" value="progress" type="success"><b>{{progress | number:0}} %</b></progressbar>',
          backdrop: 'static',
          keyboard: false,
          scope: $scope
        });
        $scope.csvImportStatus = 'Validating Category';

        $scope.validateCategoryMap(data).then(function () {
          $scope.csvImportStatus = 'Mapping Categories  to Items';

          $scope.mapCategoriesToItems(data).then(function (items) {
            $scope.csvImportStatus = 'Validating Items';

            $scope.validateImport(items).then(function (result) {
              if (result.isError === false) {
                $scope.csvImportStatus = 'Importing Items';
                $scope.prepareImportItems(result.items).then(function (_items) {
                  growl.success("Preparing to save Items to Database.", {ttl:3000});
                  saveImportedItem(_items, 0, 15);
                });
              } else {
                modalInstanceForImport.opened.then(function(){
                  modalInstanceForImport.close();
                });
                growl.error(result.message, {ttl:3000});
              }
            });
          });
        }).catch(function (e) {
          console.log(e);
          modalInstanceForImport.opened.then(function(){
            modalInstanceForImport.close();
          });
          growl.error(e, {ttl:10000});
        });
      } else{
        console.log(_.has(data[0],['ItemNumber', 'Item Name', 'Item Description', 'Rate', 'Category Name', 'SuperCategory Name']));
        growl.error('Invalid CSV headers.', {ttl:10000});
      }
    };

    function saveImportedItem(items, i, setSize){
      if(!((items.length - i) > 0)){
        growl.success("Items have been successfully Imported.", {ttl:3000});
        modalInstanceForImport.close();
      }
      else{
        var payloadItems = items.slice(i, i+setSize);
        Item.createMulti({}, {items: payloadItems}, function(result){
          for(var key in result.items){
            $scope.items.push(result.items[key]);
          }
          $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
          $scope.itemPagination.totalItems = $scope.items.length;
          $scope.progress = ((i+1)/items.length)*100;
          i = i+setSize;
          return saveImportedItem(items, i, setSize);
        }, function(err){
          growl.error('Error encountered while importing after Item Number '+ i, {ttl:5000});
          modalInstanceForImport.close();
        });
      }
    };

    $scope.prepareImportItems = function (items) {
      var d = $q.defer();
      var _tItems = [];
      try {
        _.forEach(items, function (item) {
          item.tabs = [];
          $scope.tabs.forEach(function(tab){
            var catIndex = Utils.arrayObjectIndexOf(tab.categories, "_id", item.category._id);
            if(catIndex !== -1){
              var _tab = {
                tabName : tab.tabName,
                _id : tab._id,
                tabType : tab.tabType,
                taxes : [],
                item : {
                  name : item['Item Name'],
                  number : item['ItemNumber'],
                  rate : item['Rate'],
                  category : {
                    categoryName : item.category.categoryName
                  }
                }
              };
              item.tabs.push(_tab);
            }
          });

          if(!(/(^\s*$)/.test(item['Item Name']))){
            _tItems.push({
              name: item['Item Name'],
              description: item['Item Description'],
              rate: parseFloat(item['Rate']),
              category: cleanCategory(item.category),
              tabs: item.tabs,
              number: parseInt(item['ItemNumber']),
              tenant_id: localStorageService.get('tenant_id'),
              deployment_id: $scope.deployment._id
            });
          }
        });

        /* d.resolve(_tItems);*/

      } finally {

        d.resolve(_tItems);
      }

      return d.promise;

    };

    $scope.importItems = function (data) {

      console.log(data);
      var d = $q.defer();
      var _items = data;
      var _tItems = [];

      try {
        /*console.log($scope.deployment);*/
        var _uCats = _.uniq(data, 'Category Name');
        _.forEach(_items, function (item) {
          var _f = _.filter($scope.categories, {'categoryName': item['Category Name']});
          if (_f.length > 0) {
            _tItems.push({
              name: item['Item Name'],
              description: item['Item Description'],
              rate: parseFloat(item['Rate']),
              category: _f[0],
              number: parseInt(item['ItemNumber']),
              tenant_id: localStorageService.get('tenant_id'),
              deployment_id: $scope.deployment._id
            });
          }
        });
        d.resolve(_tItems);

      } catch (e) {
        d.reject('Something went wrong while Importing, please try again!');
      }

      return d.promise;
    };

    $scope.validateCategoryMap = function (data) {
      var d = $q.defer();
      /* Check if Duplicate Cat exists w/ different Super Category */
      try {
        _.forEach(data, function (item,i) {
          if(/(^\s*$)/.test(item['Category Name']))
            throw "Category invalid for Item Number " + item['ItemNumber']

          if(/(^\s*$)/.test(item['SuperCategory Name']))
            throw "SuperCategory invalid for Item Number " + item['ItemNumber']

          var _fRows = _.filter(data, {'Category Name': item['Category Name']});
          if (_fRows.length > 1) {
            _.forEach(_fRows, function (fr,j) {
              if (fr['SuperCategory Name'].trim() != item['SuperCategory Name'].trim()) {
                throw "Incorrect SuperCategory -> Category mapping for Item Number " + item['ItemNumber']
              }
            });
          }
        });
        d.resolve();
      } catch (e) {
        d.reject(e);
      }
      return d.promise;
    };

    $scope.mapCategoriesToItems = function (data) {

      var d = $q.defer();

      try {

        Utils.asyncLoop(data.length, function (loop) {

          //Trim Category and Supercategory of any whitespaces
          data[loop.iteration()]['Category Name'] = data[loop.iteration()]['Category Name'].trim();
          data[loop.iteration()]['SuperCategory Name'] = data[loop.iteration()]['SuperCategory Name'].trim();

          var _f = _.filter($scope.categories, function (category) {
            return ((category.categoryName === data[loop.iteration()]['Category Name']) && (category.superCategory.superCategoryName === data[loop.iteration()]['SuperCategory Name']));
          });

          if (_f.length > 0) {
            data[loop.iteration()].category = _f[0];
            loop.next();
          } else {
            $scope.mapCatToItem(data, loop.iteration(), function (item, index) {
              data[index] = item;
              loop.next();
            })
          }
        }, function () {
          console.log("Finished Map");
          d.resolve(data);
        });
      } catch (e) {
        console.log(e);
        modalInstanceForImport.close();
      }

      return d.promise;
    };

    $scope.mapCatToItem = function (items, index, callback) {

      var _fSC = _.filter($scope.superCategories, function (sc) {
        return sc.superCategoryName === items[index]['SuperCategory Name'];
      });

      var _fC = _.filter($scope.categories, function (c) {
        return c.categoryName === items[index]['Category Name'];
      });

      if (_fSC.length > 0) {
        console.log('SC Found');

        if(_fC.length < 1){
          $scope.categoryForm.categoryName = items[index]['Category Name'];
          $scope.categoryForm.tenant_id = currentUser.tenant_id;
          $scope.categoryForm.deployment_id = $scope.deployment._id;
          $scope.categoryForm.superCategory = cleanSuperCategory(_fSC[0]);

          Category.save({}, $scope.categoryForm, function (categ) {
            $scope.categories.push(categ);
            console.log('Cat Created');
            items[index].category = cleanCategory(categ);
            callback(items[index], index);
          }, function (err) {
            modalInstanceForImport.close();
            growl.error("Error creating Category", {ttl:3000});
          });
        } else{
          items[index].category = cleanCategory(_fC[0]);
          callback(items[index], index);
        }
      } else {

        $scope.superCategoryForm.superCategoryName =  items[index]['SuperCategory Name'];
        $scope.superCategoryForm.tenant_id = currentUser.tenant_id;
        $scope.superCategoryForm.deployment_id = $scope.deployment._id;
        Supercategory.save({}, $scope.superCategoryForm, function (sc) {
          $scope.superCategories.push(sc);

          $scope.categoryForm.categoryName =  items[index]['Category Name'];
          $scope.categoryForm.tenant_id = currentUser.tenant_id;
          $scope.categoryForm.deployment_id = $scope.deployment._id;
          $scope.categoryForm.superCategory = cleanSuperCategory(sc);

          Category.save({}, $scope.categoryForm, function (categ) {
            $scope.categories.push(categ);
            items[index].category = cleanCategory(categ);
            callback(items[index], index);

          }, function (err) {
            modalInstanceForImport.close();
            growl.error("Error Creating Category", {ttl:3000});
          });

        }, function (err) {
          modalInstanceForImport.close();
          growl.error('Could not create Super Category: ' + item['SuperCategory Name'], {ttl:3000});
        });
      }
    };

    $scope.validateImport = function (data) {
      var d = $q.defer();
      try {
        $scope.duplicateItemCheck(data).then(function (result) {
          d.resolve(result);
        });
      } catch (e) {
        console.log(e);
        /*d.reject(e);*/
      }
      return d.promise;
    };

    $scope.duplicateItemCheck = function (data) {
      var d = $q.defer();
      var _hasError = false;
      var blankRate = false;
      var result = [];
      try {
        var _f = -1, _fNumber = -1;

        for(var i =0; i<data.length; i++){
          var _f = Utils.arrayObjectIndexOf($scope.items, 'name', data[i]['Item Name']);
          var _fNumber = Utils.arrayObjectIndexOf($scope.items, 'number', data[i]['ItemNumber']);
          if (_f != -1 || _fNumber != -1){
            if(_f !== _fNumber){
              _hasError = i;
              break;
            }
          } else{
            result.push(data[i]);
          }

          if((/(^\s*$)/.test(data[i]['Rate']))){
            blankRate = i;
            break;
          }
          if((/(^\s*$)/.test(data[i]['Item Name']))){
            blankRate = i;
            break;
          }

          if((/(^\s*$)/.test(data[i]['ItemNumber']))){
            blankRate = i;
            break;
          }
        }


if(Number.isInteger(blankRate))
          d.resolve({isError: true, message:"Information missing for Item " + (parseInt(blankRate)+1)});
        if (_f != -1 && Number.isInteger(_hasError)) {
          d.resolve({isError: true, message:"Duplicate Item Name found for Item Number " + data[_hasError]['ItemNumber']});
        }
        else if(_fNumber != -1 && Number.isInteger(_hasError)) {
          d.resolve({isError: true, message:"Duplicate Item Number found for Item Number " + data[_hasError]['ItemNumber']});
        } else{
          d.resolve({isError: false, message: "", items: result});
        }

      } catch (e) {
        console.log(e);
        d.reject("Something went wrong with Validation!");
      }

      return d.promise;
    };

    $scope.csv = {
      content: null,
      header: true,
      separator: ',',
      result: null
    };
    $scope.menuImportFile = [];
    $scope.fileImportProgress = 0;
    $scope.showProgress = false;
    $scope.rejectedFile = [];
    $scope.uploadMenuImportFile = function (file) {

      $upload.upload({
        url: 'api/uploads',
        file: file[0],
        data: {tenant_id: currentUser.tenant_id, deployment_id: $scope.deployment._id}
      }).progress(function (evt) {

        $scope.showProgress = true;
        $scope.fileImportProgress = parseInt(100.0 * evt.loaded / evt.total);

      }).success(function (data, status, headers, config) {
        // file is uploaded successfully
        $scope.showProgress = false;
        $scope.fileImportProgress = 0;
        /*console.log('file ' + config.file.name + 'is uploaded successfully. Response: ' + data);*/
        console.log(data);

      }).error(function (err) {
        $scope.showProgress = false;
        $scope.fileImportProgress = 0;
        growl.error("Unable to Upload file! Please try again!", {ttl: 3000});
      });

    };

    $scope.downloadItemImportTemplate = function () {
      var csvRows = [];
      csvRows.push('ItemNumber,Item Name,Item Description,Rate,Category Name,SuperCategory Name');
      csvRows.push("1,Shahi Paneer,,250,Veg,Indian");
      var csvString = csvRows.join("\r\n");
      var exportButton = document.createElement('a');
      angular.element(exportButton)
        .attr('href', 'data:application/csv;charset=utf-8,' + encodeURI(csvString))
        .attr('download', 'itemsSample.csv')
      document.body.appendChild(exportButton);
      exportButton.click();
      exportButton.html('');
    };

    $scope.exportItemsCSV = function(){
      var csvRows = [];
      csvRows.push('ItemNumber,Item Name,Item Description,Rate,Category Name,SuperCategory Name');
      _.forEach($scope.items, function(item){
        csvRows.push(item.number+','+item.name+','+(item.description==undefined?'':item.description)+','+item.rate+','+item.category.categoryName+','+item.category.superCategory.superCategoryName);
      });
      var csvString = csvRows.join("\r\n");
      var exportButton = document.createElement('a');
      angular.element(exportButton)
        .attr('href', 'data:application/csv;charset=utf-8,' + encodeURI(csvString))
        .attr('download', 'items.csv')
      document.body.appendChild(exportButton);
      exportButton.click();
      exportButton.html('');
    };

    $scope.onFinishWizard = function () {

      if ($scope.deployment.isMaster) {
        $scope.deployment.isCompleted = true;
        Deployment.update({}, $scope.deployment, function (d) {

          $scope.saveTaxesToItem(true).then(function () {
            console.log("Finished Wizard, do processing here.");

            /* Create Roles*/
            if (d.isCompleted === true) {
              var _rolesQ = [];
              _.forEach($rootScope.roles, function (role) {
                role.tenant_id = currentUser.tenant_id;
                role.created = new Date().toISOString();
                _rolesQ.push(Role.save({}, role).$promise);
              });
              console.log("Roles Q-ed...");
              $q.all(_rolesQ).then(function () {
                console.log("System Default Roles Created");
                $location.path('/admin/deployments');
              });
            }

          });


        }, function (err) {
          alert("Error saving Master Deployment");
        })
      }
    };

    $scope.submitTabForm = function (form) {

      $scope.submitted = true;
      var tabRoutes = {
        'table' : 'billing.table',
        'take_out' : 'billing.takeout',
        'delivery' : 'billing.delivery'
      };

      //Assign Tab Route
      $scope.tabForm.route = tabRoutes[$scope.tabForm.tabType]

      if (form.$valid && !$scope.tabForm.tabInEdit) {
        $scope.tabForm.categories = [];
        $scope.tabForm.tenant_id = currentUser.tenant_id;
        $scope.tabForm.deployment_id = $scope.deployment._id;

        Tab.save({}, $scope.tabForm, function (tab) {
          $scope.tabs.push(tab);
          /*$scope.tabCategories[$scope.tabs.length] = angular.copy($scope.categories);*/
          growl.success(tab.tabName + ' successfully created!', {ttl: 3000});
        }, function (err) {
          growl.error('Error Saving Tab: Something went wrong!', {ttl: 3000});
        });
        $scope.tabForm = {};
        form.$setPristine();
      } else {
        var tabPayload = cleanTabForm($scope.tabForm);
        Tab.update({}, tabPayload, function (tab) {
          $scope.tabs[Utils.arrayObjectIndexOf($scope.tabs, "_id", tab._id)] = tab;

          _.forEach($scope.taxes, function (tax, i) {
            var tabIndex = Utils.arrayObjectIndexOf($scope.taxes[i].tabs, "_id", tab._id);
            if(tabIndex != -1){
              $scope.taxes[i].tabs[tabIndex].tabName = tab.tabName;
              $scope.taxes[i].tabs[tabIndex].tabType = tab.tabType;
            }
          });

          _.forEach($scope.items, function (item, i) {
            var tabIndex = Utils.arrayObjectIndexOf($scope.items[i].tabs, "_id", tab._id);
            if(tabIndex != -1){
              $scope.items[i].tabs[tabIndex].tabName = tab.tabName;
              $scope.items[i].tabs[tabIndex].tabType = tab.tabType;
            }
          });

          notifyOnlineOrderPartners();

        });
        $scope.tabForm = {};
        form.$setPristine();
        //alert("Update");
      }
    };

    //toggleTabSelection
    $scope.toggleTabSelection = function selectedTabs(tabName) {
      var Idx = $scope.selectedTabs.indexOf(tabName);
      //is currently selected
      if (Idx > -1) {
        $scope.selectedTabs.splice(Idx, 1);
      }
      else { // Is newly selected
        $scope.selectedTabs.push(tabName);
      }
    };
    // Remove Tabs
    $scope.removeTab = function (tab) {
      var isfound = false;
      _.forEach($scope.items, function (item) {
        var _t = _.filter(item.tabs, {"_id": tab._id});
        if (_t.length > 0) {
          growl.error('Assigned tab can not be deleted !', {ttl: 3000});
          isfound = true;
          return false;
        }
      });

      if (tab.categories.length > 0) {
        growl.error('Assigned tab can not be deleted !', {ttl: 3000});
        growl.error('Assigned tab can not be deleted !', {ttl: 3000});
        isfound = true;
        return false;
      }
      _.forEach($scope.taxes, function (tax) {
        var _txt = _.filter(tax.tabs, {"_id": tab._id});
        if (_txt.length > 0) {
          growl.error('Assigned tab can not be deleted !', {ttl: 3000});
          isfound = true;
          return false;
        }
      })

      if (!isfound) {
        Tab.remove({id: tab._id}, function () {
          $scope.tabs.splice(Utils.arrayObjectIndexOf($scope.tabs, "_id", tab._id), 1);
        });
      }
    };
    //Clear Tabs Form
    $scope.clearTabForm = function () {
      $scope.tabForm = {};
      $scope.tabForm.tabInEdit = false;
    };
    //Edit Tabs
    $scope.editTab = function (tab) {
      $scope.tabForm = angular.copy(tab);
      $scope.tabForm.tabInEdit = true;
    };


    $scope.psErrors = [];
    $scope.createPrintStation = function (form) {
      $scope.submittedStation = true;
      if (form.$valid && !$scope.printStation.psInEdit) {
        $scope.printStation.tenant_id = currentUser.tenant_id;
        $scope.printStation.deployment_id = $scope.deployment._id;
        Station.save({}, $scope.printStation, function (station) {
            $scope.stations.push(station);
          }
          , function (err) {
            angular.forEach(err.data.errors, function (error, field) {
              form[field].$setValidity('mongoose', false);
              $scope.psErrors[field] = error.message;
            });
          });

        $scope.printStation = {};
        // $scope.printStation.psInEdit=false;
        form.$setPristine();

      } else {
        Station.update({}, $scope.printStation, function (ps) {

            $scope.stations[Utils.arrayObjectIndexOf($scope.stations, "_id", ps._id)] = ps;

            //Update local Categories
            _.forEach($scope.categories, function (category,i) {
              if (category.categoryStation && category.categoryStation._id == ps._id){
                $scope.categories[i].categoryStation = cleanStation(ps);
              }
            });

            //Update local Items
            _.forEach($scope.items, function(item,i){
              if(item.category.categoryStation && item.category.categoryStation._id == ps._id){
                $scope.items[i].category.categoryStation = cleanStation(ps);
              }
            });

            growl.success("Station successfully updated!", {ttl: 3000});
          }
          , function (err) {
            console.log(err);
            angular.forEach(err.data.errors, function (error, field) {
              form[field].$setValidity('mongoose', false);
              $scope.psErrors[field] = error.message;
            });
          });

        $scope.printStation = {};
        // $scope.printStation.psInEdit=false;
        form.$setPristine();
      }
    };
    $scope.editPrintStation = function (station) {
      $scope.printStation = angular.copy(station);
      $scope.printStation.psInEdit = true;
    };
    $scope.editShopify = function (station) {
      $scope.shopify = angular.copy(station);
      $scope.printStation.psInEdit = true;
    };
    $scope.deleteShopify = function(shop){
      Shopify.remove({id: shop._id}, function(){
        $scope.shopKeys.splice(Utils.arrayObjectIndexOf($scope.shopKeys, "_id", shop._id), 1);
      })
    }
    $scope.deletePrintStation = function (station) {
      var deleteConfirm = window.confirm('Deleting this station will unassign it from respective categroies as well. Are you sure?');
      if(deleteConfirm){
        Station.remove({id: station._id}, function () {
          $scope.stations.splice(Utils.arrayObjectIndexOf($scope.stations, "_id", station._id), 1);

          //Update local Categories
          _.forEach($scope.categories, function (category,i) {
            if (category.categoryStation && category.categoryStation._id == station._id){
              delete $scope.categories[i].categoryStation;
            }
          });

          //Update local Items
          _.forEach($scope.items, function(item,i){
            if(item.category.categoryStation && item.category.categoryStation._id == station._id){
              delete $scope.items[i].category.categoryStation;
            }
          });

          growl.success('Station deleted successfully.', {ttl: 3000});
        });
      }
    };
    $scope.openStationCategoriesModal = function (stationIndex) {

      $modal.open({
        templateUrl: 'app/admin/deployments/_stationCategories.html',
        controller: ['$scope', '$modalInstance', 'station', function ($scope, $modalInstance, station) {

          $scope.station = station;


        }],
        size: '',
        resolve: {
          station: function () {
            return $scope.stations[stationIndex];
          }
        }
      }).result.then(function (station) {

      });

    };

    $scope.hasAttrib = function (s) {
      return _.has(s, 'value');
    };

    $scope.fieldType = function (s) {
      if (_.has(s, 'fieldType')) {
        if (s.fieldType === 'timepicker') {
          return 'timepicker';
        }
        if (s.fieldType === 'text') {
          return 'text';
        }
        if (s.fieldType === 'checkbox') {
          return 'checkbox';
        }
        if (s.fieldType === 'textarea') {
          return 'textarea';
        }
      }
    };

    $scope.formatTime = function (s) {
      s.value =  moment(s.value).format('H:mm');
    };

    /* Super Categories & Categories */
    $scope.superCategoryForm = {
      scInEdit: false
    };
    $scope.categoryForm = {
      catInEdit: false
    };
    $scope.superCategories = [];

    /* Map Category to Tabs  */
    var deferredMapItemsToTabs = function () {

      var d = $q.defer();

      $timeout(function () {

        $scope.defaultTab = {
          _id: 'default',
          items : []
        };

        /* Reset Items under Each Tab */
        _.forEach($scope.tabs, function (tab, index) {
          tab.items = [];
        });

        _.forEach($scope.items, function (item, index) {
          console.log("Mapping Item");
          var tempitem = angular.copy(item);
          var temptabitem = {
            _id: tempitem._id,
            rate: tempitem.rate,
            name: tempitem.name,
            number: tempitem.number,
            description: tempitem.description,
            quantity: tempitem.quantity,
            unit: tempitem.unit,
            category: {categoryName: tempitem.category.categoryName},
            rateModified: ''
          };

          _.forEach($scope.tabs, function (tab, index) {
            var _t = _.filter(tab.categories, {'categoryName': item.category.categoryName});
            if (_t.length != 0) {
              _.forEach(item.tabs, function (itab) {
                if (itab._id == tab._id) {
                  if (_.has(itab, 'item')) {
                    if (itab.item.rate != 'undefined') {
                      temptabitem.rate = itab.item.rate;
                    }
                  }
                }
              })
              $scope.tabs[index].items.push(angular.copy(temptabitem));
            }
          });

          $scope.defaultTab.items.push(tempitem);
        });

        console.log("Going to set pagin")

        /* Pagination */
        _.forEach($scope.tabs, function (tab) {

          tab.itemTabPagination = new Pagination.getNew(25)
          tab.itemTabPagination.numPages = Math.ceil(tab.items.length / tab.itemTabPagination.perPage);
          tab.itemTabPagination.totalItems = tab.items.length;
        });

        d.resolve("Mapped");

      }, 2000);

      return d.promise;

    };

    /*********************************************************************************
     ********************************   Item -> Tabs **********************************
     **********************************************************************************/
    $scope.editedItemRates = {};

    $scope.mapItemToTab = function () {
      $scope.ifItemMappedToTab = false;
      deferredMapItemsToTabs().then(function (d) {
        /*console.log(d);*/
        $scope.ifItemMappedToTab = true;
      });
    };

  $scope.setTabRate = function(_tab, _item, index){
      if(!$scope.editedItemRates[_tab._id]){
        $scope.editedItemRates[_tab._id] = {};
        $scope.editedItemRates[_tab._id+'_'] = [];
      }

      var payload = {
        tabId : _tab._id,
        rate : _item.rate,
        itemId : _item._id
      };

      $scope.editedItemRates[_tab._id][_item._id] = payload;
      $scope.editedItemRates[_tab._id+'_'].push(index);
    };

    $scope.updateItemRates = function(tabId){
      if($scope.editedItemRates[tabId]){
        for(var i in $scope.editedItemRates[tabId]){
          var ind = Utils.arrayObjectIndexOf($scope.items, "_id", i);
          var tabs = [];

          _.forEach($scope.items[ind].tabs, function (tab) {
            if(tab._id == tabId || tabId == 'default')
              tabs.push({id: tab._id, rate: $scope.editedItemRates[tabId][i].rate});
            else
              tabs.push({id: tab._id, rate: tab.item.rate});
          });

          var updateHistory = {
            timeStamp: new Date(),
            name: $scope.editedItemRates[tabId][i].name,
            tabs: tabs,
            rate: (tabId == 'default') ? $scope.editedItemRates[tabId][i].rate : $scope.items[ind].rate,
            username: currentUser.username
          };

          $scope.items[ind].updateHistory.push(updateHistory)

          if($scope.items[ind].updateHistory.length>5){
            $scope.items[ind].updateHistory.splice(0,$scope.items[ind].updateHistory.length-5);
          }

          $scope.editedItemRates[tabId][i].updateHistory = $scope.items[ind].updateHistory;
        }

        var payload = _.map($scope.editedItemRates[tabId], function(item, id){
          return item;
        });

        Item.setTabRate({items : payload}, function(){
          var urbanPiperItems = [];

          _.forEach($scope.editedItemRates[tabId+'_'], function(x){
            angular.element(document.getElementById('item_'+x+'_'+tabId.substring(0, 3))).removeClass('bg-warning');
          });

          if(tabId != 'default'){
            _.forEach($scope.tabs[Utils.arrayObjectIndexOf($scope.tabs, "_id", tabId)].items, function(item){
              item.rateModified = '';
            });
          }

          $scope.items.forEach(function(item){
            if($scope.editedItemRates[tabId][item._id]){
              if(tabId == 'default'){
                item.rate = $scope.editedItemRates[tabId][item._id].rate;
                _.forEach(item.tabs, function(tab){
                  tab.item.rate = item.rate;
                });
              }
              else{
                var tabIndex = Utils.arrayObjectIndexOf(item.tabs, '_id', $scope.editedItemRates[tabId][item._id].tabId);
                item.tabs[tabIndex].item.rate = $scope.editedItemRates[tabId][item._id].rate;
              }

              //Updated Items for UrbanPiper payload
              urbanPiperItems.push(item);
            }
          });

          delete $scope.editedItemRates[tabId];
          growl.success('Item rate successfully updated.', {ttl: 3000});

          /*****Update ITEMS IN URBANPIPER*****/
          updateItemsInIntegrations(urbanPiperItems);

          notifyOnlineOrderPartners();

          //Update items in each tab with latest rate
          if(tabId == 'default')
            $scope.mapItemToTab();

        }, function (err) {
            growl.error('Error while updating item rate.', {ttl: 3000});
        });
      }
    };



    /*********************************************************************************
     ***************************    End Of Item -> Tabs   *****************************
     **********************************************************************************/

    $scope.showPermissions = function (user) {
      $modal.open({
        templateUrl: 'app/admin/deployments/_showUserPermissions.html',
        controller: ['$scope', '$modalInstance', 'user', function ($scope, $modalInstance, user) {

          $scope.user = user;
          $scope.close = function () {
            $modalInstance.dismiss('cancel');
          };


        }],
        size: '',
        resolve: {
          user: function () {
            return user;
          }
        }
      }).result.then(function () {

      });

    };

    $scope.tempTab = [];
    /* Map temp item tax  to Tabs  */
    var deferredMapItemTaxesToTabs = function () {
      var d = $q.defer();
      $timeout(function () {
        // $scope.tab.items= _.sortBy()
        $scope.tempTab = [];
        // $scope.tempTab= angular.copy( $scope.tabs);
        _.forEach($scope.tabs, function (tb, i) {
          var tempT = angular.copy(tb);
          var t = {
            _id: tempT._id, tabName: tempT.tabName, tabType: tempT.tabType, tenant_id: tempT.tenant_id,
            deployment_id: tempT.deployment_id, items: [], taxes: [], categories: tempT.categories
          };
          $scope.tempTab.push(angular.copy(t));
        });


        /* Reset Items under Each Tab */
        _.forEach($scope.tempTab, function (tab, index) {
          $scope.tempTab.items = [];
          /* Filter Taxes by Tab */
          var _filteredTax = [];
          _.forEach($scope.taxes, function (tax) {
            var _ft = _.filter(tax.tabs, {'_id': tab._id});
            if (_ft.length > 0) {
              _filteredTax.push(tax);
            }
          });
          //$scope.tempTab[index].taxes = angular.copy(_filteredTax);
          $scope.tempTab[index].taxes = angular.copy(_filteredTax);
        });


        _.forEach($scope.items, function (item, index) {
          var tempitem = angular.copy(item);

          var temptabitem = {
            _id: tempitem._id,
            rate: tempitem.rate,
            name: tempitem.name,
            number: tempitem.number,
            description: tempitem.description,
            quantity: tempitem.quantity,
            unit: tempitem.unit,
            category: {categoryName: tempitem.category.categoryName}
          };

          var tab = _.filter($scope.tempTab, function (tab, index) {
            var _t = _.filter(tab.categories, {'categoryName': item.category.categoryName});
            if (_t.length != 0) {

              /* Filter Taxes by Tab */
              var _filteredTax = [];
              _.forEach($scope.taxes, function (tax) {
                var _ft = _.filter(tax.tabs, {'_id': tab._id});
                if (_ft.length > 0) {
                  var temptax = angular.copy(tax);
                  temptax.selected = isTaxBYTabItem(item, tab._id, tax._id);
                  _filteredTax.push(temptax);
                }
              });
              temptabitem.masterTaxes = angular.copy(_filteredTax);
              $scope.tempTab[index].items.push(angular.copy(temptabitem));

              var hastabinitem = _.filter(item.tabs, {"_id": tab._id});
              if (hastabinitem.length == 0) {
                item.tabs.push({_id: tab._id, tabName: tab.tabName});
              }

            }

          });
        });

        /* Pagination */
        _.forEach($scope.tempTab,function(tab,index){
          /*  var tItem=( $scope.tempTab[index].items);
           $scope.tempTab[index].items=angular.copy(  _.sortBy(tItem, ['category.categoryName']));*/
          // console.log( $scope.tempTab[index].items);
          $scope.tempTab[index].itemTabPagination = new Pagination.getNew(25);
          $scope.tempTab[index].itemTabPagination.numPages = Math.ceil($scope.tempTab[index].items.length / $scope.tempTab[index].itemTabPagination.perPage);
          $scope.tempTab[index].itemTabPagination.totalItems = $scope.tempTab[index].items.length;

        })

        d.resolve("Taxes Mapped to Items");
      });

      return d.promise;

    };

    $scope.mapItemTaxToTab = function () {

      $scope.ifItemTaxToTab = false;
      deferredMapItemTaxesToTabs().then(function () {
        $scope.ifItemTaxToTab = true;
      });
    };

    function isTaxBYTabItem(item, tabid, taxid) {
      var flag = false;
      _.forEach(item.tabs, function (tab) {
        if (tab._id == tabid) {
          var _tx = _.filter(tab.taxes, {"_id": taxid});
          if (_tx.length > 0) {
            flag = true;
            return true;
          }
        }
      });
      return flag;
    }

    $scope.CheckAllTaxes = function (txid, tbid) {
      $scope.checkAllTaxesFlag = true;
      _.forEach($scope.tempTab, function (tab) {
        _.forEach(tab.taxes, function (tax) {
          if (tab._id == tbid && tax._id == txid) {
            if (!(tax.selectedtax == 'undefined')) {
              _.forEach(tab.items, function (item) {
                item.taxModified = 'bg-warning';
                _.forEach(item.masterTaxes, function (mtax) {
                  if (mtax._id == tax._id) {
                    mtax.selected = (tax.selectedtax);
                  }
                })
              })
            }
          }
        });
      })
    };

    var _itemsMappedTaxes = {
      success: [],
      errors: []
    };
    var deferredSaveTaxesToItems = function (isFinishWizard) {
      var d = $q.defer();

      $timeout(function () {
        var _qAllTaxes = [];
        //If executed by onFinishWizard
        if(isFinishWizard == true){
          _.forEach($scope.items, function (item) {
            _.forEach(item.tabs, function (tab) {
              tab.taxes = getSelectedTaxByItemTab(item._id, tab._id);
            });
            _qAllTaxes.push(Item.update({}, item).$promise);
          });
        }
        else{
          var payload = [];

          _.forEach($scope.items, function (item) {
            //Only process items that have been modified or if all items have been selected
            if(item.taxModified == true || $scope.checkAllTaxesFlag == true){
              var tabs = [];

              _.forEach(item.tabs, function (tab) {
                tab.taxes = getSelectedTaxByItemTab(item._id, tab._id);
                if(tab.item)
                  tabs.push({id:tab._id,rate:tab.item.rate})
              });

              var updateHistory = {
                timeStamp:new Date,
                name:item.name,
                tabs:tabs,
                rate:item.rate,
                username:currentUser.username
              };
              item.lastModified = new Date;
              item.updateHistory.push(updateHistory)
              if(item.updateHistory.length>5){
                item.updateHistory.splice(0,item.updateHistory.length-5);
              }

              var _item = {
                _id : item._id,
                tabs : item.tabs,
                updateHistory : item.updateHistory
              };

              payload.push(_item);
            }
          });
          _qAllTaxes.push(Item.mapTaxes({items: payload}).$promise);
        }
        d.resolve(_qAllTaxes);
      });
      return d.promise;
    };

    $scope.selectTaxToItem = function(tabId, itemId){
      var itemIndex = Utils.arrayObjectIndexOf($scope.items, "_id", itemId);
      $scope.items[itemIndex].taxModified = true;
    };

    $scope.saveTaxesToItem = function (isFinishWizard) {
      var defer = $q.defer();
      $scope.ifItemTaxToTab = false;
      var modalInstance = $modal.open({
        template: '<h3 class="text-center"><i class="fa fa-spinner fa-spin"></i>Updating Item to Taxes Mapping.</h3>',
        backdrop: 'static',
        keyboard: false
      });
      deferredSaveTaxesToItems(isFinishWizard).then(function (allTaxes) {
        $q.all(allTaxes).then(function () {
          _.forEach($scope.tempTab, function (tab) {
            _.forEach(tab.items, function (item) {
              item.taxModified = null;
            });
          });

          var urbanPiperItems = [];
          _.forEach($scope.items, function(item){
            if(item.taxModified){
              delete item.taxModified;
              urbanPiperItems.push(item);
            }
          });
          updateItemsInIntegrations(urbanPiperItems);
          notifyOnlineOrderPartners();
          $scope.ifItemTaxToTab = true;
          $scope.checkAllTaxesFlag = false; //Reset checkAll flag
          defer.resolve();
          modalInstance.close();
        });
      });
      return defer.promise;
    };

    function getSelectedTaxByItemTab(itemid, tabid) {
      var filteredTax = [];
      _.forEach($scope.tempTab, function (tab) {
        if (tab._id == tabid) {
          _.forEach(tab.items, function (item) {
            if (item._id == itemid) {

              _.forEach(item.masterTaxes, function (mtx) {
                if (mtx.selected) {
                  _.forEach($scope.taxes, function (tax) {
                    if (mtx._id == tax._id) {
                      // delete tax.tabs;
                      filteredTax.push(cleanTax(tax));
                    }
                  })
                }
              })
              return filteredTax;
            }
          })
        }
      })
      return filteredTax;
    }

    $scope.autoincitemnumber = function () {
      //var getItemNumber = 0;
      //_.forEach($scope.items, function (item, i) {
      //  getItemNumber = parseInt((getItemNumber) < ( item.number) ? item.number : getItemNumber);
      //});
      //$scope.itemForm.number = getItemNumber + 1;
      var itemm = _.max($scope.items,'number')
      //$scope.lastItemNumber = itemm.number;
      $scope.itemForm.number = itemm.number+1;
    };

    /*********************************************************************************
     ********************************   Tabs -> Categories ****************************
     **********************************************************************************/

    var updateCatToTabs = function () {
      var d = $q.defer();

      $timeout(function () {
        _.forEach($scope.tabs, function (tb, i) {
          tb = cleanTabForm(tb);
          Tab.update({id: tb._id}, tb, function (tab) {}, function (err) {
            console.log(err);
            growl.error('Error: Something went wrong!', {ttl: 3000});
          });
        });
        d.resolve("Categories mapped to Tabs");
      }, 500);

      return d.promise;
    };
    $scope.mapCatToTab = function () {
      var defer = $q.defer();
      updateCatToTabs().then(function () {
        growl.success("Categories mapped to Tabs!", {ttl: 3000});
        defer.resolve();
      });
      return defer.promise;
    };


    $scope.checkAllCat = function(tab){
      var tabindex = Utils.arrayObjectIndexOf($scope.tabs, "_id", tab._id);
      $scope.tabs[tabindex].masterCategories.forEach(function(c){
        c.selected = tab.mapAllCat;
        c.modified = (c.selected == c.oldSelected ? 0 : 1);
      });
    };

    $scope.setAllCatToTab = function(tab){
      var modalInstance = $modal.open({
        template: '<h3 class="text-center"><i class="fa fa-spinner fa-spin"></i>Updating Category to Tabs Mapping</h3>',
        backdrop: 'static',
        keyboard: false
      });
      var tabindex = Utils.arrayObjectIndexOf($scope.tabs, "_id", tab._id);
      var mapPromises = [];

      _.forEach(tab.masterCategories, function(cat){
        if(cat.modified == true){
          var payload = {
            category: cleanCategory(cat),
            tab: cleanTabForm(tab),
            items: [],
            push: cat.selected
          };

          payload.tab.taxes = [];

          _.forEach($scope.items, function(item){
            if(item.category._id == cat._id){
              var _item = {
                _id : item._id,
                name : item.name,
                number : item.number,
                rate : item.rate,
                unit : item.unit,
                category : {categoryName : cat.categoryName}
              };
              payload.items.push(_item);
            }
          });

          mapPromises.push(Tab.mapCategories({id: tab._id}, payload));
        }
      });

      $q.all(mapPromises).then(function(newTab){

        //Update Tabs on frontend
        $scope.tabs[tabindex].categories = [];
        $scope.tabs[tabindex].masterCategories.forEach(function(mc){
          if(mc.selected)
            $scope.tabs[tabindex].categories.push(cleanCategory(mc));
          delete mc.modified;
        });

        //Update Items on frontend
        Item.get({tenant_id: localStorageService.get('tenant_id'), deployment_id: $scope.deployment._id}, function (items) {
          $scope.items = items;
          var it = _.max($scope.items,'number')
          $scope.lastItemNumber = it.number;
          $scope.itemForm.number = it.number+1;
          $scope.itemPagination = Pagination.getNew(25);
          $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
          $scope.itemPagination.totalItems = $scope.items.length;

          tab.mapAllCat = null;
          modalInstance.close();
          notifyOnlineOrderPartners();
          growl.success("Category mapping successfully updated.", {ttl: 3000});
        }, function(){
          modalInstance.close();
          growl.error("Error while updating category mapping.", {ttl: 3000});
        });
      });
    };

    /*********************************************************************************
     ******************************* End of Tabs -> Categories ************************
     **********************************************************************************/

      //Remove Super Category
    $scope.removeSuperCategory = function (sc) {
      var hasSC = false;
      _.forEach($scope.categories, function(c){
        if(c.superCategory.superCategoryName == sc.superCategoryName){
          hasSC = true;
          return false;
        }
      });

      if(hasSC == false){
        Supercategory.DeleteAuth({id: sc._id, tenant_id: currentUser.tenant_id}, function (errorstatus) {
          if (errorstatus.status == 'error') {
            growl.error(errorstatus.message, {ttl: 3000});
          } else {
            $scope.superCategories.splice(Utils.arrayObjectIndexOf($scope.superCategories, "_id", sc._id), 1);
            growl.success('Super Category has been deleted!', {ttl: 3000});
          }
        });
      } else{
        growl.error("Cannot delete SuperCategory, it is assigned to category!", {ttl: 3000});
      }
    };

    //Select All Categories
    $scope.selectAllSCategories = function(selected){
      _.forEach($scope.superCategories, function(sc){
        sc.selected = selected;
      });
    };

    $scope.deleteAllSCategories = function(){
      var deleteSCats = [];
      _.forEach($scope.superCategories, function(sc){
        if(sc.selected){
          var hasSC = false;
          _.forEach($scope.categories, function(c){
            if(c.superCategory.superCategoryName == sc.superCategoryName){
              hasSC = true;
              return false;
            }
          });

          if(hasSC == false)
            deleteSCats.push(sc._id);
        }
      });

      if(deleteSCats.length > 0){
        var modalInstance = $modal.open({
          template: '<h3 class="text-center"><i class="fa fa-spinner fa-spin"></i>Deleting Categories</h3>',
          backdrop: 'static',
          keyboard: false
        });
        Supercategory.deleteAuthMulti({id: 'multi'}, {scategories: deleteSCats}, function (response) {
          _.forEach(response.scategories, function(sc){
            $scope.superCategories.splice(Utils.arrayObjectIndexOf($scope.superCategories, "_id", sc), 1);
          });
          growl.success('Unassigned Super Categories have been successfully deleted!', {ttl: 3000});
          modalInstance.close();
        }, function(){
          growl.error('Error while deleting super categories!', {ttl: 3000});
          modalInstance.close();
        });
      }
      else{
        growl.error('No SuperCategories were deleted.', {ttl:3000});
      }
    };

    //Clear Super CAtegory
    $scope.clearSuperCategoryForm = function () {
      $scope.superCategoryForm = {};
      $scope.superCategoryForm.scInEdit = false;
    };
    //Edit Super Category
    $scope.editSuperCategory = function (sc) {
      $scope.superCategoryForm = angular.copy(sc);

      $scope.superCategoryForm.scInEdit = true;
    };

    // Submit Super Category Form
    $scope.scErrors = [];
    $scope.submitSuperCategoryForm = function (scForm) {
      $scope.submitSuperCategory = true;

      if (scForm.$valid && !$scope.superCategoryForm.scInEdit) {   //insert new super category
        $scope.superCategoryForm.tenant_id = currentUser.tenant_id;
        $scope.superCategoryForm.deployment_id = $scope.deployment._id;
        Supercategory.save({}, $scope.superCategoryForm, function (sc) {
          $scope.superCategories.push(sc);
        }, function (err) {

          angular.forEach(err.data.errors, function (error, field) {
            scForm[field].$setValidity('mongoose', false);
            $scope.scErrors[field] = error.message;
          });

        });
        $scope.superCategoryForm = {};
        scForm.$setPristine();

      } else {

        //Uppdate Super Category
        Supercategory.update({}, $scope.superCategoryForm, function (sc) {

          //Update local SuperCategory
          $scope.superCategories[Utils.arrayObjectIndexOf($scope.superCategories, "_id", sc._id)] = sc;

          //Update local Categories
          _.forEach($scope.categories, function (category,i) {
            if (category.superCategory._id == sc._id){
              $scope.categories[i].superCategory = {};
              $scope.categories[i].superCategory.superCategoryName = sc.superCategoryName;
              $scope.categories[i].superCategory._id = sc._id;
            }
          });

          //Update local Items
          _.forEach($scope.items, function(item,i){
            if(item.category.superCategory._id == sc._id){
              $scope.items[i].category.superCategory = {};
              $scope.items[i].category.superCategory.superCategoryName = sc.superCategoryName;
              $scope.items[i].category.superCategory._id = sc._id;
            }
          });

          notifyOnlineOrderPartners();

          growl.success(sc.superCategoryName + " successfully updated!", {ttl: 3000});
        }, function (err) {
          console.log(err);
          angular.forEach(err.data.errors, function (error, field) {
            scForm[field].$setValidity('mongoose', false);
            $scope.scErrors[field] = error.message;
          });
        });

        $scope.superCategoryForm = {};
        scForm.$setPristine();
      }
    };

    //Clear category form
    $scope.clearCatCategoryForm = function () {
      $scope.categoryForm = {};
      $scope.categoryForm.catInEdit = false;
    };

    //Edit Category
    $scope.editCategory = function (c) {
      $scope.categoryForm = angular.copy(c);
      $scope.categoryForm.catInEdit = true;
    };

    //Remove Category
    $scope.removeCategory = function (c) {
      var ifCategoryUsedFlag = ifCategoryUsed(c);

      if(ifCategoryUsedFlag == false){
        Category.remove({id: c._id}, function () {
          $scope.categories.splice(Utils.arrayObjectIndexOf($scope.categories, "_id", c._id), 1);
          growl.success('Category has been deleted!', {ttl: 3000});
        });
      }
      if(ifCategoryUsedFlag == 1)
        growl.error("Cannot delete category, it is assigned to item!", {ttl: 3000});
      if(ifCategoryUsedFlag == 2)
        growl.error("Cannot be deleted due to recipe of this category", {ttl: 3000});
      if(ifCategoryUsedFlag == 3)
        growl.error("Cannot delete category, it's being used!", {ttl: 3000});
    };

    function ifCategoryUsed(c){
      var _hasCats = false;
      var itemHasCat = false;
      _.forEach($scope.tabs, function (tab, i) {
        var tempcat = _.filter(tab.categories, {'_id': c._id});
        if (tempcat.length > 0) {
          _hasCats = true;
        }
      });

      _.forEach($scope.items, function(item){
        if(item.category.categoryName == c.categoryName){
          itemHasCat = true;
          return false
        }
      });

      if (!_hasCats) {
        var index=_.findIndex($scope.StockRecipes,{categoryId:c._id});
        if(index<0){
          if(itemHasCat == false){
            return false;
          }
          else{
            return 1;
          }
        }
        else{
          return 2;
        }
      } else {
        return 3;
      }
    };

    //Select All Categories
    $scope.selectAllCategories = function(selected){
      _.forEach($scope.categories, function(cat){
        cat.selected = selected;
      });
    };

    $scope.deleteAllCategories = function(){
      var deleteCats = [];
      _.forEach($scope.categories, function(c){
        if(c.selected){
          var f = ifCategoryUsed(c);
          if(f == false)
            deleteCats.push(c._id);
        }
      });

      if(deleteCats.length > 0){
        var modalInstance = $modal.open({
          template: '<h3 class="text-center"><i class="fa fa-spinner fa-spin"></i>Deleting Categories</h3>',
          backdrop: 'static',
          keyboard: false
        });
        Category.update({id: 'multi'}, {categories: deleteCats}, function (response) {
          _.forEach(response.categories, function(c){
            $scope.categories.splice(Utils.arrayObjectIndexOf($scope.categories, "_id", c), 1);
          });
          growl.success('Unassigned categories have been successfully deleted!', {ttl: 3000});
          modalInstance.close();
        }, function(){
          growl.error('Error while deleting categories!', {ttl: 3000});
          modalInstance.close();
        });
      }
      else{
        growl.error('No categories were deleted.', {ttl:3000});
      }
    };

    // Submit Category Form
    $scope.cErrors = [];
    $scope.submitCategoryForm = function (cform) {
      $scope.submitCategory = true;
      delete $scope.categoryForm.superCategory.created;
      delete $scope.categoryForm.superCategory.updated;
      delete $scope.categoryForm.superCategory.tenant_id;
      delete $scope.categoryForm.superCategory.deployment_id;
      delete $scope.categoryForm.superCategory.__v;

      if($scope.categoryForm.categoryStation != undefined || $scope.categoryForm.categoryStation != null)
        $scope.categoryForm.categoryStation = cleanStation($scope.categoryForm.categoryStation);
      else
        $scope.categoryForm.categoryStation = null;

      if (cform.$valid && !$scope.categoryForm.catInEdit) {
        $scope.categoryForm.tenant_id = currentUser.tenant_id;
        $scope.categoryForm.deployment_id = $scope.deployment._id;

        Category.save({}, $scope.categoryForm, function (categ) {
          var newtempcat = categ;
          newtempcat.selected = false;
          newtempcat.icon = '<i class="fa fa-list"></i>';
          $scope.categories.push(newtempcat);
          $scope.mapChangedCategorytoTab();
        }, function (err) {
          angular.forEach(err.data.errors, function (error, field) {
            cform[field].$setValidity('mongoose', false);
            $scope.cErrors[field] = error.message;
          });

        });

        $scope.categoryForm = {};
        cform.$setPristine();

      }
      else { //Update

        Category.update({}, $scope.categoryForm, function (c) {

          //Update local Category
          $scope.categories[Utils.arrayObjectIndexOf($scope.categories, "_id", c._id)] = c;

          //Update local items
          _.forEach($scope.items, function(item,i){
            if(item.category._id == c._id){
              $scope.items[i].category = c;
            }
          });

          notifyOnlineOrderPartners();

          //Update local Tabs
          $scope.mapChangedCategorytoTab();

          growl.success(c.categoryName + ' successfully updated!', {ttl: 3000});
        }, function (err) {
          angular.forEach(err.data.errors, function (error, field) {
            cform[field].$setValidity('mongoose', false);
            $scope.cErrors[field] = error.message;
          });
        });
        $scope.categoryForm = {};
        cform.$setPristine();
      }
    };

    //map tab to categories
    $scope.mapChangedCategorytoTab = function () {
      _.forEach($scope.tabs, function (temptab, i) {
        //alert(temptab);
        temptab.masterCategories = angular.copy($scope.categories);
        _.forEach(temptab.masterCategories, function (tempcat, j) {
          var filteredarray = _.filter(temptab.categories, {_id: tempcat._id});
          tempcat.selected = filteredarray.length > 0 ? true : false;
        });
      });
    }

    //Submit Menu Items
    $scope.itemForm = {
      menuItemsInEdit: false
    };
    $scope.items = [];

    //Map Menu to Tab
    var deferredMapTabsToItem = function () {
      var d = $q.defer();
      //$timeout(function () {
      _.forEach($scope.items, function (it, itemindex) {
        var temptabs = [];
        _.forEach($scope.tabs, function (t, tabindex) {

          var _item = _.filter(t.items, {"_id": it._id});
          if (_item.length > 0) {
            var temptab = {};
            temptab._id = t._id;
            temptab.tabName = t.tabName;
            temptab.tabType = t.tabType;
            temptab.tenant_id = t.tenant_id;
            temptab.deployment_id = t.deployment_id;
            //console.log(_item[0]);
            delete _item[0].tabs;
            // console.log(_item[0]);
            temptab.item = _item[0];
            _.forEach(it.tabs, function (itemtab) {
              if (itemtab._id == t._id) {
                if (_.has(itemtab, 'taxes'))
                  temptab.taxes = itemtab.taxes;
              }
            });
            temptabs.push(temptab);
          }
        });
        it.tabs = [];
        it.tabs = angular.copy(temptabs);
      });

      var _qAll = [];
      _.forEach($scope.items, function (itm) {
        var tabs = []
        _.forEach(itm.tabs, function (tab) {
          if(tab.item)
            tabs.push({id:tab._id,rate:tab.item.rate})
        });
        var updateHistory = {
          timeStamp:new Date,
          name:itm.name,
          tabs:tabs,
          rate:itm.rate,
          username:currentUser.username
        }
        itm.lastModified = new Date;
        itm.updateHistory.push(updateHistory)
        if(itm.updateHistory.length>5){
          itm.updateHistory.splice(0,itm.updateHistory.length-5);
        }
        var _p = Item.update({id: itm._id}, itm).$promise;
        _qAll.push(_p);
      });
      $q.all(_qAll).then(function(){
        d.resolve('done');
      });
      //d.resolve(_qAll);

      // });
      return d.promise;
    };

    $scope.mapTabsToItem = function () {
      var defer = $q.defer();
      $scope.ifItemMappedToTab = false;
      deferredMapTabsToItem().then(function (allItems) {
        //$q.all(allItems).then(function () {
        $scope.ifItemMappedToTab = true;
        //});
        defer.resolve();
      });
      /*****Update ITEMS IN URBANPIPER*****/
      if($scope.urbanPiperKeys.length > 0 && $scope.urbanPiperKeys[0].active){
        var postData = {items:$scope.items,urbanPiperKeys:$scope.urbanPiperKeys[0], tabs:$scope.tabs};
        UrbanPiper.updateItemsInUrbanPiper(postData,function(result){
          console.log(result)
          if(result.status==200){
            growl.success('Updated In UrbanPiper!', {ttl: 3000});
          }
          else{
            growl.error('Error Updating In UrbanPiper!', {ttl: 3000});
          }
        })
      }
      /*****UPDATE ITEMS IN URBANPIPER*****/
      return defer.promise;
    };

    /*function updateItemsAll() {
     _.forEach($scope.items, function (itm, i) {
     Item.update({id: itm._id}, itm, function (item) {
     console.log(item);
     });
     });
     };*/

    //Clear menuItem Form
    $scope.clearMenuItemsInEditForm = function () {
      $scope.itemForm = {};
      $scope.itemForm.menuItemsInEdit = false;
      //$scope.itemForm.number = $scope.lastItemNumber;
      var itemm = _.max($scope.items,'number')
      //$scope.lastItemNumber = itemm.number;
      $scope.itemForm.number = itemm.number+1;
      $scope.toggleItemFormCollapse();
    }

    // Edit menu Form
    $scope.editItems = function (menu) {
      $scope.itemForm = angular.copy(menu);
      $scope.originalItem = angular.copy(menu);
      $scope.itemForm.menuItemsInEdit = true;
      $scope.toggleItemFormCollapse(true);
    };

    //Create and update menu Items
   $scope.addMenuItems = function (form){
      $scope.submitMenuItem = true;
      if (form.$valid && !$scope.itemForm.menuItemsInEdit) {
        if(!$scope.isPermission('add_menu_item')){
          growl.error('No permission to add item !!',{ttl:3000});
          return false;
        }
        $scope.itemForm.tenant_id = currentUser.tenant_id;
        $scope.itemForm.deployment_id = $scope.deployment._id;
        $scope.itemForm.category = cleanCategory($scope.itemForm.category);
        $scope.itemForm.unit = ($scope.itemForm.unit !== undefined) ? cleanUnit($scope.itemForm.unit) : null;
        $scope.itemForm.tabs = [];

        //Add tabs to Item for given Category
        $scope.tabs.forEach(function(tab){
          var catIndex = Utils.arrayObjectIndexOf(tab.categories, "_id", $scope.itemForm.category._id);
          if(catIndex !== -1){
            var _tab = {
              tabName : tab.tabName,
              _id : tab._id,
              tabType : tab.tabType,
              taxes : [],
              item : {
                name : $scope.itemForm.name,
                number : $scope.itemForm.number,
                rate : $scope.itemForm.rate,
                category : {
                  categoryName : $scope.itemForm.category.categoryName
                }
              }
            };
            $scope.itemForm.tabs.push(_tab);
          }
        });
        Item.save({}, $scope.itemForm, function (item) {
          growl.success('Successfully Created Item!', {ttl: 3000});
          $scope.items.push(item);
          //Reset Pagination
          $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
          $scope.itemPagination.totalItems = $scope.items.length;

          updateItemInIntegrations(item,0);
          $scope.itemForm = {};
          form.$setPristine();
          $scope.autoincitemnumber();
        }, function (err) {
          growl.error('Error Creating Item!', {ttl: 3000});
        });
      }
      else {
        if(!$scope.isPermission('edit_menu_item')){
          growl.error('No permission to edit item !!',{ttl:3000});
          return false;
        }
        $scope.itemForm.tenant_id = currentUser.tenant_id;
        $scope.itemForm.deployment_id = $scope.deployment._id;
        $scope.itemForm.category = cleanCategory($scope.itemForm.category);
        $scope.itemForm.unit = ($scope.itemForm.unit != undefined || $scope.itemForm.unit != null) ? cleanUnit($scope.itemForm.unit) : null;

        // Reassign Tabs to Item if Category changed.
        if($scope.itemForm.category._id !== $scope.originalItem.category._id){
          $scope.itemForm.tabs = [];
          $scope.tabs.forEach(function(tab){
            var catIndex = Utils.arrayObjectIndexOf(tab.categories, "_id", $scope.itemForm.category._id);
            if(catIndex != -1){
              var _tab = {
                tabName : tab.tabName,
                _id : tab._id,
                tabType : tab.tabType,
                taxes : [],
                item : {
                  _id : $scope.itemForm._id,
                  name : $scope.itemForm.name,
                  number : $scope.itemForm.number,
                  rate : $scope.itemForm.rate,
                  category : {
                    categoryName : $scope.itemForm.category.categoryName
                  }
                }
              };
              $scope.itemForm.tabs.push(_tab);
            }
          });
        }

        var tabs = []
        _.forEach($scope.itemForm.tabs, function (tab) {
          tab.item = {
            _id : $scope.itemForm._id,
            name : $scope.itemForm.name,
            number : $scope.itemForm.number,
            rate : ($scope.itemForm.rate!=$scope.originalItem.rate || tab.item==undefined) ? $scope.itemForm.rate : tab.item.rate,
            category : {
              categoryName : $scope.itemForm.category.categoryName
            }
          };
          tabs.push({id:tab._id,rate:tab.item.rate})
        });

        var updateHistory = {
          timeStamp:new Date,
          name:$scope.itemForm.name,
          tabs:tabs,
          rate:$scope.itemForm.rate,
          username:currentUser.username
        }
        $scope.itemForm.lastModified = new Date();
        $scope.itemForm.updateHistory.push(updateHistory)
        if($scope.itemForm.updateHistory.length>5){
          $scope.itemForm.updateHistory.splice(0,$scope.itemForm.updateHistory.length-5);
        }

        Item.update({}, $scope.itemForm, function (item) {
          $scope.items.splice(Utils.arrayObjectIndexOf($scope.items, "_id", item._id), 1);
          $scope.items.push(item);
          $scope.itemForm = {};
          form.$setPristine();
          $scope.autoincitemnumber();
          updateItemInIntegrations(item,1);
          notifyOnlineOrderPartners();
          //Stock Update
          UpdateRecipeOnItemUpdate(item);
          UpdatevendorOnItemUpdate(item);
          UpdateHOvendorOnItemUpdate(item);
          UpdateReceiverOnItemUpdate(item);
          UpdateStoreProcessedCatOnItemChange(item);
          growl.success('Item update successfully.', {ttl: 3000});
        }, function (err) {
          $scope.autoincitemnumber();
          growl.error('Error Updating Item!', {ttl: 3000});
        });
      }
    };
    function UpdateHOvendorOnItemUpdate(item) {
      console.log(item);
      vendor.getAll({id: localStorageService.get('tenant_id')}, function (result) {

        console.log(result);
        var catChangeFlag = false;
        if ($scope.originalItem.category.categoryName != item.category.categoryName)
          catChangeFlag = true;
        _.forEach(result, function (v, i) {
          if (v.baseKitchenId == localStorageService.get('deployment_id')) {
            /*_.forEach(v.pricing.category, function (c, ii) {
             _.forEach(c.item, function (itm, iii) {
             if(itm){
             if (itm._id == item._id) {
             itm = item.name;
             //update vendor
             console.log(v);
             vendor.update({}, v, function (ven) {
             console.log(ven);
             });
             }
             }
             });
             });*/
            if (catChangeFlag) {
              var oldCategory = _.find(v.pricing.category, function (cat) {
                return cat.categoryName == $scope.originalItem.category.categoryName;
              });

              if (oldCategory) {
                _.remove(oldCategory.item, function (itm) {
                  if (itm)
                    return itm._id == item._id;
                  return false;
                });
              }

              var newCategory = _.find(v.pricing.category, function (cat) {
                console.log(cat.categoryName == item.category.categoryName);
                console.log(cat.categoryName);
                console.log(item.category.categoryName);
                return cat.categoryName == item.category.categoryName;
              });

              if (newCategory)
                newCategory.item.push({_id: item._id, itemName: item.name, units: item.units});
            } else {
              var category = _.find(v.pricing.category, function (cat) {
                return cat.categoryName == item.category.categoryName;
              });

              if (category) {
                var uItm = _.find(category.item, function (itm) {
                  if (itm)
                    return itm._id == item._id;
                  return false;
                });
                if (uItm)
                  uItm.itemName = item.name;
              }
            }
            vendor.update({}, v, function (ven) {
                  if(ven.errorMessage)
                  {
                    growl.error("Some error occured. Please Update Again",{ttl:3000})
                  }
              console.log(ven);
            });
          }
        });
      });
    };

    //Update Recipe
    function UpdateRecipeOnItemUpdate(item) {
      _.forEach($scope.StockRecipes, function (r, i) {
        if (r.itemId == item._id) {
          r.recipeName = item.name;
          //update
          stockRecipe.update({}, r, function (result) {
            console.log(result);
          });
        }
      });
    };
    //update Vendor Processed Item
    function UpdatevendorOnItemUpdate(item) {
      _.forEach($scope.StockVendors, function (v, i) {
        if (v.pricing) {
          _.forEach(v.pricing.category, function (c, ii) {
            _.forEach(c.item, function (itm, iii) {
              if (itm._id == item._id) {
                itm.itemName = item.name;
                //update vendor
                vendor.update({}, v, function (ven) {
                  if(ven.errorMessage)
                  {
                    growl.error("Some error occured. Please Update Again",{ttl:3000})
                  }
                  console.log(ven);

                });
              }
            })
          });
        }
      });
    };

    //update receiver on Item Update
    function UpdateReceiverOnItemUpdate(item) {
      _.forEach($scope.StockReceivers, function (r, i) {
        if (r.pricing) {
          _.forEach(r.pricing.store, function (s, ii) {
            _.forEach(s.category, function (c, iii) {
              if (c._id == "PosistTech111111") {
                _.forEach(c.item, function (itm, iti) {
                  if (itm._id == item._id) {
                    itm.itemName = item.name;
                    //Update Receiver
                    receiver.update({}, r, function (rec) {
                      console.log(rec);
                    });
                  }
                  ;
                });
              }
            })
          });
        }
      });
    };

    //Update Store Processed Item
    function UpdateStoreProcessedCatOnItemChange(item) {
      _.forEach($scope.stores, function (s, i) {
        if (s.processedFoodCategory != undefined) {
          _.forEach(s.processedFoodCategory, function (c, i) {
            _.forEach(c.item, function (itm, ii) {
              if (itm._id == item._id) {
                itm.itemName = item.name;
                //update store
                store.update({}, s, function (result) {
                  console.log(result);
                })
              }
            })
          });
        }
      });
    };

    // Remove Item
    $scope.removeItem = function (item,isMultiDeletion) {
      //return false;
      console.log(item);
      if(deployment.isBaseKitchen)
      {

      Item.validateItemForDeletion({deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id'), itemId: item._id}).$promise.then(function (result) {
        console.log('result',result);
        if(result.err)
          growl.error(result.err, {ttl: 3000});
        else
        {
          if(result.itemDeletable)
          {
          //var d=$q.defer();
          if(!$scope.checkIfItemAssigned(item)) {
            Item.remove({id: item._id}, function (data) {
              console.log('Item.remove data',data)
              deleteItemFromReceiver(data);
              deleteItemFromVendor(data);
              if(item.shopifyId && $scope.shopKeys.length>0)
                $http.post('/api/shopify/deleteProduct',{shopDetails : $scope.shopKeys[0],shopifyId:item.shopifyId}).success(function(success,err){
                  console.log(success);
                  growl.success("Item Deleted from POS and Shopify Successfully",{ttl:3000});
                }).error(function(err){
                  console.log(err);
                  growl.error("Item can not be deleted from Shopify",{ttl:3000});
                  growl.success("Item Deleted Successfully from POS",{ttl:3000});
                })
              else{
                growl.success("Item Deleted Successfully",{ttl:3000});
              }

              //Remove Item from frontend and reset pagination
              $scope.items.splice(Utils.arrayObjectIndexOf($scope.items, "_id", item._id), 1);
              $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
              $scope.itemPagination.totalItems = $scope.items.length;
              var tab = _.filter($scope.tabs, function (tab, index) {
                var _t = _.filter(tab.items, function (item, i) {
                  if (item._id == data._id) {
                    $scope.tabs[index].items.splice(i);
                    d.resolve();
                  }
                });
              });
              notifyOnlineOrderPartners();
            });
            /*****DELETE ITEM IN URBANPIPER*****/
            if ($scope.urbanPiperKeys.length > 0 && $scope.urbanPiperKeys[0].active && item.urbanPiperId && item.urbanPiperId == "1") {
              var postData = {item: item, urbanPiperKeys: $scope.urbanPiperKeys[0]};
              UrbanPiper.deleteItemInUrbanPiper(postData, function (result) {
                console.log(result)
                if (result.status == 200) {
                  growl.success('Deleted In UrbanPiper!', {ttl: 3000});
                }
                else{
                  growl.error('Error deleting In UrbanPiper!', {ttl: 3000});
                }
              })
            }
            /*****DELETE ITEM IN BASE KITCHEN*****/
            baseKitchenItem.deleteBaseKitchenItems({id: item._id}, function (data) {
              console.log('baseKitchenItem.remove data',data)

            });
            /*****DELETE ITEM IN NOMNOM*****/
            if ($scope.nomnomKeys.length > 0 && $scope.nomnomKeys[0].active && item.nomnomId && item.nomnomId == "1") {
              var postData = {item: item, nomnomKeys: $scope.nomnomKeys[0]};
              Nomnom.deleteItemFromNomnom(postData, function (result) {
                console.log(result)
                if (result.status == 200) {
                  growl.success('Deleted In Nomnom!', {ttl: 3000});
                }
                else{
                  growl.error('Error deleting In Nomnom!', {ttl: 3000});
                }
              })
            }
            /*****DELETE ITEM IN URBANPIPER*****/
          }else{
            // alert(isMultiDeletion);
            //d.resolve();
            if(isMultiDeletion==null||isMultiDeletion=='undefined'){
              growl.error("Assigned item can't be deleted!!",{ttl:1000});
            }
          }

          //return d.promise;
          }
          else
          {
            growl.error(result.reason, {ttl: 6000});
            if(result.openModal==true)
            {
            var modalInstance = $modal.open({
                component: 'modalComponent',
                templateUrl: 'deleteItemModal.html',
                size: 'sm',
                controller: 'DeleteItemModalCtrl'
              });

              modalInstance.result.then(function (email) {
                console.log('email',email);
                var req=
                {
                  client:email,
                  company:'support@posist.com',
                  itemName:item.name,
                  deploymentName:deployment.name,
                  itemId:item._id
                }
                Item.sendMailOnDeletion(req).$promise.then(function(result)
                  {
                    console.log('result sendMailOnDeletion',result);
                    _.forEach(result,function(r,i)
                    {
                    if(r.error)
                    {
                      console.log('Mail Error',r.error)
                      growl.error(r.error, {ttl: 3000});
                    }
                    else
                    {
                      console.log('Mail Success',r.message)
                      growl.success('Mail Sent Successfully', {ttl: 3000});
                    }
                    })
                  });
              }, function () {
                console.log('modal-component dismissed at: ' + new Date());
              });
            }
            else
            {
              growl.error(result.reason, {ttl: 3000});
            }
          }
    }
      }).catch(function(err) {
        console.log('Error',err);
        growl.error("Some error occured. Please try again!");
      });
    }
    else
    {
      if(!$scope.checkIfItemAssigned(item))
      {
      Item.remove({id: item._id}, function (data) {
              console.log('Item.remove data',data)
              deleteItemFromReceiver(data);
              //deleteItemFromVendor(data);
              if(item.shopifyId && $scope.shopKeys.length>0)
                $http.post('/api/shopify/deleteProduct',{shopDetails : $scope.shopKeys[0],shopifyId:item.shopifyId}).success(function(success,err){
                  console.log(success);
                  growl.success("Item Deleted from POS and Shopify Successfully",{ttl:3000});
                }).error(function(err){
                  console.log(err);
                  growl.error("Item can not be deleted from Shopify",{ttl:3000});
                  growl.success("Item Deleted Successfully from POS",{ttl:3000});
                })
              else{
                growl.success("Item Deleted Successfully",{ttl:3000});
              }

              //Remove Item from frontend and reset pagination
              $scope.items.splice(Utils.arrayObjectIndexOf($scope.items, "_id", item._id), 1);
              $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
              $scope.itemPagination.totalItems = $scope.items.length;
              var tab = _.filter($scope.tabs, function (tab, index) {
                var _t = _.filter(tab.items, function (item, i) {
                  if (item._id == data._id) {
                    $scope.tabs[index].items.splice(i);
                    d.resolve();
                  }
                });
              });
              notifyOnlineOrderPartners();
            });
            /*****DELETE ITEM IN URBANPIPER*****/
            if ($scope.urbanPiperKeys.length > 0 && $scope.urbanPiperKeys[0].active && item.urbanPiperId && item.urbanPiperId == "1") {
              var postData = {item: item, urbanPiperKeys: $scope.urbanPiperKeys[0]};
              UrbanPiper.deleteItemInUrbanPiper(postData, function (result) {
                console.log(result)
                if (result.status == 200) {
                  growl.success('Deleted In UrbanPiper!', {ttl: 3000});
                }
                else{
                  growl.error('Error deleting In UrbanPiper!', {ttl: 3000});
                }
              })
            }

            /*****DELETE ITEM IN BASE KITCHEN*****/
            // baseKitchenItem.deleteBaseKitchenItems({itemId: item._id}, function (data) {
            //   console.log('baseKitchenItem.remove data',data)

            // });

            /*****DELETE ITEM IN NOMNOM*****/
            if ($scope.nomnomKeys.length > 0 && $scope.nomnomKeys[0].active && item.nomnomId && item.nomnomId == "1") {
              var postData = {item: item, nomnomKeys: $scope.nomnomKeys[0]};
              Nomnom.deleteItemFromNomnom(postData, function (result) {
                console.log(result)
                if (result.status == 200) {
                  growl.success('Deleted In Nomnom!', {ttl: 3000});
                }
                else{
                  growl.error('Error deleting In Nomnom!', {ttl: 3000});
                }
              })
            }
    }



    else{
            // alert(isMultiDeletion);
           // d.resolve();
            if(isMultiDeletion==null||isMultiDeletion=='undefined'){
              growl.error("Assigned item can't be deleted!!",{ttl:1000});
            }
          }
        }

    };

    function deleteItemFromReceiver(item) {
      _.forEach($scope.StockReceivers, function (r, i) {
        if (r.pricing != undefined) {
          var flag = false;
          _.forEach(r.pricing.store, function (s, ii) {
            _.forEach(s.category, function (c, iii) {
              if (c._id == 'PosistTech222222' || c._id == 'PosistTech111111') {
                _.forEachRight(c.item, function (itm, iiii) {
                  if (itm._id == item._id) {
                    flag = true;
                    c.item.splice(iiii, 1);
                  }
                });
              }
            });
          });
          if(flag==true){
            console.log('deleted Item from Receiver',item);
            receiver.update({},r);
          }
        }
      });
    };
    function deleteItemFromVendor(item) {
      _.forEach($scope.StockVendors, function (r, i) {
        if (r.pricing != undefined) {
          var flag = false;

            _.forEach(r.pricing.category, function (c, iii) {
              if (c._id == 'PosistTech222222' || c._id == 'PosistTech111111') {
                _.forEachRight(c.item, function (itm, iiii) {
                  if (itm._id == item._id) {
                    flag = true;
                    c.item.splice(iiii, 1);
                  }
                });
              }
            });

          if(flag==true){
            console.log('deleted Item from vendor',item);
            vendor.update({},r);
          }
        }
      });
    };
    //Section Mapping (Added by Kanishk | 20/9/2016)
    $scope.renderSectionMapping = function () {
      $rootScope.showLoaderOverlay = true;
      $scope.allSections = [];
      sectionResource.get({deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id'), requiredFields: {name: 1}}, function (sections) {
        $scope.allSections = sections;
        $rootScope.showLoaderOverlay = false;

        $scope.sectionsWithCategories = {
          all: []
        };
        //console.log('categories', $scope.categories);
          _.forEach($scope.categories, function (category) {
            if(_.has(category, 'section') && category.section) {
              console.log(category.section)
              _.forEach($scope.allSections, function (section) {
                if (!$scope.sectionsWithCategories[section._id])
                  $scope.sectionsWithCategories[section._id] = [];
                if (category.section._id == section._id)
                  $scope.sectionsWithCategories[section._id].push(category);
              });
            }
            else {
              $scope.sectionsWithCategories.all.push(category);
            }
        });

      }, function (err) {
        growl.error("Error fetching sections!", {ttl: 3000});
        $rootScope.showLoaderOverlay = false;
      });
      $scope.currentSectionsPage = 1;
      $scope.sectionMapForm = {
        isUpdate: false,
        updateIndex: -1,
        deployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),
      };

      $scope.getContextMenuForSection = function(sect) {

        var menuOptions = [];
        if(sect) {
          menuOptions.push(
            ['Unassign', function ($itemScope) {
              var section = $itemScope.category.section;
              $itemScope.category.section = null;
              $rootScope.showLoaderOverlay = true;
              $itemScope.category.$updateSectionInCategoryAndItems(function (cat) {
                if (section) {
                  _.remove($scope.sectionsWithCategories[section._id], function (cat) {
                    return cat._id == $itemScope.category._id;
                  });
                }
                $scope.sectionsWithCategories.all.push(cat);
                $rootScope.showLoaderOverlay = false;
              }, function (err) {
                $rootScope.showLoaderOverlay = false;
                growl.error(err, {ttl: 3000});
              });
            }]
          );
        }
        _.forEach($scope.allSections, function (section){
          var push = true;
          if(sect){
            if(sect._id == section._id) {
              push = false;
            }
          }
          if(push){
            menuOptions.push([
              section.name, function ($itemScope) {
                var sect = $itemScope.category.section;
                $itemScope.category.section = section;
                $rootScope.showLoaderOverlay = true;
                $itemScope.category.$updateSectionInCategoryAndItems(function (cat) {
                  if (!$scope.sectionsWithCategories[section._id])
                    $scope.sectionsWithCategories[section._id] = [];
                  $scope.sectionsWithCategories[section._id].push(cat);
                  if (sect) {
                    _.remove($scope.sectionsWithCategories[sect._id], function (cat) {
                      return cat._id == $itemScope.category._id;
                    });
                  } else {
                    _.remove($scope.sectionsWithCategories.all, function (cat) {
                      return cat._id == $itemScope.category._id;
                    });
                  }
                  $rootScope.showLoaderOverlay = false;
                }, function (err) {
                  $rootScope.showLoaderOverlay = false;
                  growl.error(err, {ttl: 3000});
                });
              }
            ]);
          }
        });
        return menuOptions;
      }

      $scope.createOrUpdateSection = function (form){

        if(form.$valid){
          if(!$scope.sectionMapForm.isUpdate){
            var section = new sectionResource($scope.sectionMapForm);
            section.$saveData(function (sec) {
              growl.success("Section created!",{ttl: 3000});
              $scope.allSections.push(sec);
              $scope.sectionMapForm = {
                isUpdate: false,
                updateIndex: -1,
                deployment_id: localStorageService.get('deployment_id'),
                tenant_id: localStorageService.get('tenant_id'),
              };
            }, function (err) {
              growl.error("Some error occured. Please try again!", {ttl: 3000});
            });
          } else {
            var section = new sectionResource($scope.sectionMapForm);
            section.$update(function (upSec) {
              $scope.allSections[$scope.sectionMapForm.updateIndex] = upSec;
              $scope.sectionMapForm = {
                isUpdate: false,
                updateIndex: -1,
                deployment_id: localStorageService.get('deployment_id'),
                tenant_id: localStorageService.get('tenant_id'),
              };
              growl.success("Section updated successfully.", {ttl: 3000});
            }, function (err){
              console.log(err);
              growl.error("Some error occured while updating, Please try again!.", {ttl: 3000});
            });
          }
        }
      }

      $scope.prepareSectionForUpdate = function (section, index) {
        console.log('prepping section update');
        $scope.sectionMapForm._id = section._id;
        $scope.sectionMapForm.name = section.name;
        $scope.sectionMapForm.isUpdate = true;
        $scope.sectionMapForm.updateIndex = index;
      };

      $scope.cancelUpdate = function () {
        $scope.sectionMapForm = {
          isUpdate: false,
          updateIndex: -1,
          deployment_id: localStorageService.get('deployment_id'),
          tenant_id: localStorageService.get('tenant_id'),
        };
      }

      $scope.deleteSection = function (section, index) {

        if(confirm("Are you sure you want to delete this section?")){
          section.$delete(function (res) {
            console.log(res);
            growl.success("Section deleted successfully", {ttl: 3000});
            $scope.allSections.splice(index, 1);
          }, function (err){
            console.log(err);
            growl.error(err.data.message, {ttl: 3000});
          });
        }
      };
    }
    //end

    //Day Session Breakup (Added by Kanishk | 21/9/2016)
    $scope.renderDaySessionBreakup = function (){

      $scope.daySessionBreakup = {
        isUpdate: false,
        updateIndex: -1,
        allSessions: [],
        daySessionForm: {
          sessionName: "",
          fromTime: new Date('1/1/1999'),
          toTime: new Date('1/1/1999'),
          deployment_id: localStorageService.get('deployment_id'),
          tenant_id: localStorageService.get('tenant_id')
        }
      };

      dayBreakup.get({deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id'), projection: {sessionName: 1, fromTime: 1, toTime: 1}}, function (sessions){
        $scope.daySessionBreakup.allSessions = sessions;
      });

      $scope.daySessionBreakup.createOrUpdateSession = function (sessionForm) {

        if(sessionForm.$valid){
          var isValid = true;
          _.forEach($scope.daySessionBreakup.allSessions, function (session) {

            if($scope.daySessionBreakup.isUpdate){
              console.log(session._id, $scope.daySessionBreakup.daySessionForm._id)
              if(session._id != $scope.daySessionBreakup.daySessionForm._id){
                console.log('in');
                var newFromTimeHour = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getHours();
                var newFromTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getMinutes();
                var newToTimeHours = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getHours();
                var newToTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getMinutes();
                var sessionFromTimeHours = (new Date(session.fromTime)).getHours();
                var sessionFromTimeMinutes = (new Date(session.fromTime)).getMinutes();
                var sessionToTimeHours = (new Date(session.toTime)).getHours();
                var sessionToTimeMinutes = (new Date(session.toTime)).getMinutes();

                //console.log(newFromTimeHour)
                //console.log(newFromTimeMinutes)
                //console.log(newToTimeHours)
                //console.log(newToTimeMinutes)
                //console.log(sessionFromTimeHours)
                //console.log(sessionFromTimeMinutes)
                //console.log(sessionToTimeHours)
                //console.log(sessionToTimeMinutes)
                //console.log((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes));

                if((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes)){
                  isValid = false;
                }
              }
            } else {
              var newFromTimeHour = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getHours();
              var newFromTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getMinutes();
              var newToTimeHours = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getHours();
              var newToTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getMinutes();
              var sessionFromTimeHours = (new Date(session.fromTime)).getHours();
              var sessionFromTimeMinutes = (new Date(session.fromTime)).getMinutes();
              var sessionToTimeHours = (new Date(session.toTime)).getHours();
              var sessionToTimeMinutes = (new Date(session.toTime)).getMinutes();

              //console.log(newFromTimeHour)
              //console.log(newFromTimeMinutes)
              //console.log(newToTimeHours)
              //console.log(newToTimeMinutes)
              //console.log(sessionFromTimeHours)
              //console.log(sessionFromTimeMinutes)
              //console.log(sessionToTimeHours)
              //console.log(sessionToTimeMinutes)
              //console.log((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes));

              if((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes)){
                isValid = false;
              }
            }
          });
          if(isValid) {
            if (!$scope.daySessionBreakup.isUpdate) {
              var session = new dayBreakup($scope.daySessionBreakup.daySessionForm);
              session.$saveData(function (sess) {
                $scope.daySessionBreakup.allSessions.push(sess);
                $scope.daySessionBreakup.daySessionForm = {

                  sessionName: "",
                  fromTime: new Date('1/1/1999'),
                  toTime: new Date('1/1/1999'),
                  deployment_id: localStorageService.get('deployment_id'),
                  tenant_id: localStorageService.get('tenant_id')
                };
                growl.success("Successfully created session", {ttl: 3000});
              }, function (err) {
                console.log(err);
                growl.error("Some error occured. Please try again!", {ttl: 3000});
              });
            }
            else {
              var session = new dayBreakup($scope.daySessionBreakup.daySessionForm);
              session.$update(function (sess) {
                $scope.daySessionBreakup.allSessions[$scope.daySessionBreakup.updateIndex] = sess;
                $scope.daySessionBreakup.isUpdate = false;
                $scope.daySessionBreakup.updateIndex = -1;
                $scope.daySessionBreakup.daySessionForm = {
                  sessionName: "",
                  fromTime: new Date('1/1/1999'),
                  toTime: new Date('1/1/1999'),
                  deployment_id: localStorageService.get('deployment_id'),
                  tenant_id: localStorageService.get('tenant_id')
                };
                growl.success("Successfully updated session", {ttl: 3000});
              }, function (err) {
                console.log(err);
                growl.error("Some error occured, Please try again!", {ttl: 3000});
              });
            }
          } else {
            growl.error("Session time conincides with existing sessions", {ttl: 3000});
          }
        }
      };

      $scope.daySessionBreakup.prepareSessionForUpdate = function (session, index) {
        $scope.daySessionBreakup.daySessionForm = session;
        $scope.daySessionBreakup.isUpdate = true;
        $scope.daySessionBreakup.updateIndex = -1;
      };

      $scope.daySessionBreakup.deleteSession = function (session, index) {
        if(confirm("Are you sure you want to delete this session")) {
          session.$delete(function () {
            $scope.daySessionBreakup.allSessions.splice(index, 1);
            growl.success("Session deleted successfully");
          }, function (err) {
            console.log(err);
          });
        }
      };

      $scope.daySessionBreakup.cancelUpdate = function() {
        $scope.daySessionBreakup.isUpdate = false;
        $scope.daySessionBreakup.updateIndex = -1;
        $scope.daySessionBreakup.daySessionForm = {
          sessionName: "",
          fromTime: new Date('1/1/1999'),
          toTime: new Date('1/1/1999'),
            deployment_id: localStorageService.get('deployment_id'),
            tenant_id: localStorageService.get('tenant_id')
        }
      };
    }
    //end

    $scope.checkIfItemAssigned = function (item) {
      // console.log($scope.tabs);
      var flag = false;
      // if (_.has(item, 'tabs')) {
      //   if (item.tabs.length > 0) {
      //     flag = true;
      //     return true;
      //   }
      // }

      // for (var i = 0; i < $scope.tabs.length; i++) {
      //     for(var k=0;k<$scope.tabs[i].masterCategories.length;k++){
      //    if($scope.tabs[i].masterCategories[k]._id==item.category._id){
      //    flag=true;
      //    break;
      //    }
      //    }
      //   for (var j = 0; j < $scope.tabs[i].items.length; j++) {
      //     if ($scope.tabs[i].items[j]._id == item._id) {
      //       flag = true;
      //       break;
      //     }
      //   }
      // }

      for (var i = 0; i < $scope.StockRecipes.length; i++) {
        if ($scope.StockRecipes[i].itemId == item._id) {
          flag = true;
          break;
        }
      }
      //  alert(flag);
      return flag;
    }

    //Remove Taxes
    $scope.removeTaxes = function (tax) {
      var isfound = false;
      _.forEach($scope.items, function (item) {
        _.forEach(item.tabs, function (tab) {
          var taxLength = tab.taxes ? tab.taxes.length : 0;
          var ctFound = false;
          var tFound = false;

          for(var i =0; i < taxLength; i++){
            if(tab.taxes[i]._id == tax._id){
              tFound = true;
              break;
            } else{
              var _ct = _.filter(tab.taxes[i].cascadingTaxes, {"_id": tax._id, "selected" : true});
              if(tab.taxes[i]._id == tax._id || _ct.length > 0){
                ctFound = true;
                break;
              }
            }
          }

          if (tFound == true){
            isfound = true;
            growl.success('Used taxes can not be deleted!', {ttl: 3000});
            return false;
          }

          if (ctFound == true){
            isfound = true;
            growl.success('Used tax is cascaded. Can not be deleted!', {ttl: 3000});
            return false;
          }
        });
      });
      if (!isfound) {
        Tax.remove({id: tax._id}, function () {
          $scope.taxes.splice(Utils.arrayObjectIndexOf($scope.taxes, "_id", tax._id), 1);
          $scope.cascadingTaxes.splice(Utils.arrayObjectIndexOf($scope.cascadingTaxes, "_id", tax._id), 1);
          $scope.cascadingTaxes.forEach(function(_tax){
            var cctIndex = Utils.arrayObjectIndexOf(_tax.cascadingTaxes, "_id", tax._id);
            if(cctIndex != -1)
              _tax.cascadingTaxes.splice(cctIndex, 1);
          });
          $scope.taxes.forEach(function(_tax){
            var cctIndex = Utils.arrayObjectIndexOf(_tax.cascadingTaxes, "_id", tax._id);
            if(cctIndex != -1)
              _tax.cascadingTaxes.splice(cctIndex, 1);
          });
        });
      }
    };

    //Submit Taxation
    $scope.taxForm = {
      taxInEdit: false,
      tabs: []
    };
    $scope.taxes = [];

    $scope.setSelectedTabOnTax = function (event, tab) {
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      $scope.updateSelectedTabOnTax(action, tab);
    };

    $scope.isSelectedTabOnTax = function (tab) {
      if ($scope.taxForm.tabs.length === 0) {
        return false;
      } else {
        return Utils.arrayObjectIndexOf($scope.taxForm.tabs, '_id', tab._id) >= 0;
      }

    };

    $scope.updateSelectedTabOnTax = function (action, tab) {

      if (action == 'add' && Utils.arrayObjectIndexOf($scope.taxForm.tabs, '_id', tab._id) == -1) {
        $scope.taxForm.tabs.push({_id: tab._id, tabName: tab.tabName});
      }
      if (action == 'remove' && Utils.arrayObjectIndexOf($scope.taxForm.tabs, '_id', tab._id) != -1) {
        $scope.taxForm.tabs.splice(Utils.arrayObjectIndexOf($scope.taxForm.tabs, '_id', tab._id), 1);
      }
    };

    $scope.tabForTax = [];
    $scope.ManagerTabTax = function () {
      $scope.tabForTax = [];
      _.forEach($scope.tabs, function (tab){
        var ttab = {_id: tab._id, tabName: tab.tabName, tabType: tab.tabType, selected: false};
        $scope.tabForTax.push(ttab);
      });
    }

    //$scope.initiateTabs = function () {
    // $scope.taxForm.tabs=[];
    // var s= _.filter($scope.tabForTax,{"selected":true});
    //if(s.length>0){ $scope.taxForm.tabs=[];}
    //}

    //Clear tax Form
    $scope.clearTaxForm = function () {
      _.forEach($scope.tabForTax, function (tab) {
        tab.selected = false;
      });
      $scope.taxForm = {
        taxInEdit: false,
        tabs: []
      };
    };

    //Edit Taxes
    $scope.editTaxes = function (tax) {
      _.forEach($scope.tabForTax, function (tab) {
        tab.selected = false;
      });
      $scope.taxForm = angular.copy(tax);
      $scope.taxForm.taxInEdit = true;
      _.forEach(tax.tabs, function (tt) {
        _.forEach($scope.tabForTax, function (tab) {
          if (tt._id == tab._id) {
            tab.selected = true;
          }
          //if(tab.selected){var ttab={_id:tab._id,tabName:tab.tabName};$scope.taxForm.tabs.push(ttab); }
        });
      });
    };

    //Create and Update Taxes
    $scope.createTax = function (form) {
      // console.log(form);
      $scope.SubmitCreateTaxs = true;

      var hasTabs = false;
      _.forEach($scope.tabForTax, function (tab){
        if(tab.selected)
          hasTabs = true;
      });

      if(hasTabs == false)
        growl.error('Cannot create a tax without tab.', {ttl: 3000});
      else {
        if (form.$valid && !$scope.taxForm.taxInEdit) {
          $scope.taxForm.tenant_id = currentUser.tenant_id;
          $scope.taxForm.deployment_id = $scope.deployment._id;
          $scope.taxForm.cascadingTaxes = [];
          $scope.taxForm.tabs = [];
          _.forEach($scope.tabForTax, function (tab) {
            if (tab.selected) {
              var ttab = {_id: tab._id, tabName: tab.tabName, tabType: tab.tabType};
              $scope.taxForm.tabs.push(ttab);
            }
          });
          Tax.save({}, $scope.taxForm, function (tax) {
            $scope.taxes.push(tax);
            $scope.taxForm = {
              taxInEdit: false,
              tabs: []
            };
            form.$setPristine();
            _.forEach($scope.tabForTax, function (tab) {
              tab.selected = false;
            });
            growl.success(tax.name + ' successfully saved!', {ttl: 3000});

          }, function (err) {
            growl.error('Error saving Tax!', {ttl: 3000});
          });
        }
        else {
          var modalInstance = $modal.open({
            template: '<h3 class="text-center"><i class="fa fa-spinner fa-spin"></i>Updating Tax</h3>',
            backdrop: 'static',
            keyboard: false
          });

          $scope.taxForm.tabs = [];
          $scope.taxForm.items = [];
          var itemLength = $scope.items.length;

          delete $scope.taxForm.taxInEdit;

          _.forEach($scope.tabForTax, function (tab) {
            if (tab.selected) {
              var ttab = {_id: tab._id, tabName: tab.tabName, tabType: tab.tabType};
              $scope.taxForm.tabs.push(ttab);
            }
          });

          var cleanTaxObject = cleanTax($scope.taxForm,false);

          for(var i = 0; i < itemLength; i++){

            var tabLength = $scope.items[i].tabs ? $scope.items[i].tabs.length : 0;
            if(tabLength>0){
              var taxAffectedItem = false;
              var tempTabs = [];
              angular.copy($scope.items[i].tabs, tempTabs);

              for(var j = 0; j < tabLength; j++){
                var taxSelected = Utils.arrayObjectIndexOf($scope.taxForm.tabs, '_id', tempTabs[j]._id);
                var taxLength = tempTabs[j].taxes ? tempTabs[j].taxes.length : 0;

                for(var k = 0; k < taxLength; k++){

                  if(tempTabs[j].taxes[k].cascadingTaxes && tempTabs[j].taxes[k].cascadingTaxes.length > 0){
                    var cctIndex = Utils.arrayObjectIndexOf(tempTabs[j].taxes[k].cascadingTaxes, "_id", $scope.taxForm._id);
                    if(cctIndex != -1){
                      taxAffectedItem = true;
                      tempTabs[j].taxes[k].cascadingTaxes[cctIndex].name = $scope.taxForm.name;
                      tempTabs[j].taxes[k].cascadingTaxes[cctIndex].type = $scope.taxForm.type;
                      tempTabs[j].taxes[k].cascadingTaxes[cctIndex].value = $scope.taxForm.value;
                    }
                  }

                  if(tempTabs[j].taxes[k]._id == $scope.taxForm._id){
                    taxAffectedItem = true;
                    if(taxSelected != -1)
                      tempTabs[j].taxes[k] = cleanTaxObject;
                    if(taxSelected == -1)
                      tempTabs[j].taxes.splice(k,1);
                  }
                }
              }

              if(taxAffectedItem){
                var item = {
                  _id : $scope.items[i]._id,
                  tabs : tempTabs
                };
                $scope.taxForm.items.push(item);
              }
            }
          }

          //Update Taxes
          Tax.update({}, $scope.taxForm, function (tax) {

            //Updates the value of given tax in cascadingTaxes for other taxes
            _.forEach($scope.taxes, function(t,i){
              if(t._id == tax._id)
                $scope.taxes[i] = tax;
              else{
                var cctIndex = Utils.arrayObjectIndexOf($scope.taxes[i].cascadingTaxes, "_id", tax._id);
                if(cctIndex != -1){
                  $scope.taxes[i].cascadingTaxes[cctIndex].name = tax.name;
                  $scope.taxes[i].cascadingTaxes[cctIndex].value = tax.value;
                  $scope.taxes[i].cascadingTaxes[cctIndex].type = tax.type;
                }
              }
            });

            //Updates the value of item.tabs
            _.forEach($scope.items, function(item){
              var itemIndex = Utils.arrayObjectIndexOf($scope.taxForm.items, "_id", item._id)
              if(itemIndex != -1){
                item.tabs = $scope.taxForm.items[itemIndex].tabs;
              }
            });

            $scope.taxForm = {
              taxInEdit: false,
              tabs: []
            };

            form.$setPristine();

            _.forEach($scope.tabForTax, function (tab) {
              tab.selected = false;
            });

            growl.success("Tax Updated!", {ttl: 3000});
            modalInstance.close();
          }, function (err) {
            growl.error("Tax Update Error!", {ttl: 3000});
            modalInstance.close();
          });
        }
      }
    };

    //Set Cascading Tax t is selected tax and tax is selected groupTax
    $scope.setCascadingTax = function (t, tax) {
      var copiedTax = angular.copy(t);
      copiedTax.isTaxableOnBaseRate = false;
      //Latest fix ranjeet
      tax.cascadingTaxes.push(copiedTax);
    };

    //Update all of the cascading taxes
    $scope.updateAllTaxes = function () {
      var modalInstance = $modal.open({
        template: '<h3 class="text-center"><i class="fa fa-spinner fa-spin"></i>Updating Cascaded Taxes</h3>',
        backdrop: 'static',
        keyboard: false
      });

      var itemLength = $scope.items.length;
      var taxAffectedItem = false;
      var payload = {
        items : [],
        taxes : []
      };

      _.forEach($scope.taxes, function (tx, i) {
        var _t = _.filter($scope.cascadingTaxes, {"_id": tx._id});
        if (_.has(_t[0], 'cascadingTaxes')) {
          tx.cascadingTaxes = [];
          _.forEach(_t[0].cascadingTaxes, function(ct){
            console.log(ct);
            if(ct.selected == true)
              tx.cascadingTaxes.push(ct);
          });
        }
        payload.taxes.push(tx);
      });

      for(var i = 0; i < itemLength; i++){
        var tabLength = $scope.items[i].tabs ? $scope.items[i].tabs.length : 0;
        if(tabLength>0){
          var taxAffectedItem = false;
          var tempTabs = [];
          angular.copy($scope.items[i].tabs, tempTabs);
          for(var j = 0; j < tabLength; j++){
            var nOfTaxes = tempTabs[j].taxes ? tempTabs[j].taxes.length : 0;
            for(var k=0; k < nOfTaxes; k++){
              var taxIndex = Utils.arrayObjectIndexOf($scope.taxes, '_id', tempTabs[j].taxes[k]._id)
              if(taxIndex !== -1){
                tempTabs[j].taxes[k] = cleanTax($scope.taxes[taxIndex], false);
                taxAffectedItem = true;
              }
            }
          }

          if(taxAffectedItem){
            var item = {
              _id : $scope.items[i]._id,
              tabs : tempTabs
            };
            payload.items.push(item);
          }
        }
      }

      Tax.cascade({id: 'cascade'}, payload, function (tax) {

        //Updates the value of item.tabs
        _.forEach($scope.items, function(item){
          var itemIndex = Utils.arrayObjectIndexOf(payload.items, "_id", item._id)
          if(itemIndex != -1){
            item.tabs = payload.items[itemIndex].tabs;
          }
        });
        modalInstance.close();
        growl.success('Taxes mapped successfully!', {ttl: 3000});
      }, function (err) {
        modalInstance.close();
        growl.error('Error Updating Taxes: Something went wrong!', {ttl: 3000});
      });
    };

    $scope.cascadingTaxes = [];
    $scope.MapCascadingTax = function () {
      $scope.cascadingTaxes = [];
      var tcascading = [];
      _.forEach($scope.taxes, function (tax, i) {
        var tctaxob = angular.copy(tax);

        _.forEach($scope.taxes, function (ctax) {
          if (tax._id != ctax._id) {
            var isfound = false;

            if (_.has(tax, 'cascadingTaxes')) {
              var _prectx = _.filter(tax.cascadingTaxes, {"_id": ctax._id});
              if (_prectx.length > 0) {
                isfound = true;
              }
            }
            var tctax = cleanTax(ctax,true);
            tctax.selected = false;
            if (!isfound) {
              if (!checkCRCascadingTax(tax, ctax))
                tctaxob.cascadingTaxes.push((tctax));
            }
          }
        });
        $scope.cascadingTaxes.push(tctaxob);
      });
    };

    function checkCRCascadingTax(masterTax, casCadingTax) {
      var isfound = false;
      _.forEach($scope.taxes, function (mtax) {
        if (mtax._id == casCadingTax._id) {
          if (_.has(mtax, 'cascadingTaxes')) {
            var _tf = _.filter(mtax.cascadingTaxes, {"_id": masterTax._id});
            if (_tf.length > 0) {
              isfound = true;
              return isfound;
            }
          }
        }
      });
      return isfound;
    }

    $scope.removeCascadingTax = function (masterTax, casCadingTax, isadd) {
      casCadingTax.selected = !isadd;
      _.forEach($scope.cascadingTaxes, function (mtax) {
        if (mtax._id == casCadingTax._id) {
          if (_.has(mtax, 'cascadingTaxes')) {
            if (isadd) {
              var tctax = cleanTax(masterTax,true);
              tctax.selected = false;
              mtax.cascadingTaxes.push(tctax);
            }
            else
              mtax.cascadingTaxes.splice(Utils.arrayObjectIndexOf(mtax.cascadingTaxes, "_id", masterTax._id), 1);
          }
        }
      });
    }

    $scope.editItem={tabItemSelected:false,itemSelected:false};

    /*  $scope.selectAllItem=function(tab){
     console.log($scope.editItem.tabItemSelected);
     for(var i=0;i<$scope.tabs.length;i++) {
     if($scope.tabs[i]._id==tab._id) {
     _.forEach($scope.tabs[i].items, function (_item) {
     _item.editSelected = $scope.editItem.tabItemSelected;
     })
     break;
     }
     }
     }*/
    $scope.selectAllItem=function() {
      console.log($scope.editItem.tabItemSelected);
      _.forEach($scope.items, function (_item) {
        _item.editSelected = $scope.editItem.itemSelected;
      });
    };

    $scope.itemLoading=true;

    $scope.deleteSelectedItem=function(){
      $scope.itemLoading=false;
      if($scope.shopKeys.length > 0){
        $scope.selectedDeleteItems = []
        for(var i=0;i<$scope.items.length;i++){
          if($scope.items[i].editSelected == true && $scope.items[i].shopifyId){
            $scope.selectedDeleteItems.push({items:$scope.items[i],shopifyId:$scope.items[i].shopifyId,shopDetails : $scope.shopKeys[0]});
          }
        }
      }
      var deleteItem=[];
      var assignedCheck = false;
      var deleteItemWithIds = [];

      _.forEach($scope.items, function (_item) {
        if(  _item.editSelected){
          if(!$scope.checkIfItemAssigned(_item)) {
            deleteItemWithIds.push(_item._id);
          } else{
            assignedCheck = true;
          }
        }
      });

      if(deleteItemWithIds.length > 0){
        Item.deleteMultipleItems({id:'items'},{items:deleteItemWithIds}).$promise.then(function(){
          _.forEach(deleteItemWithIds, function(itemId){
            $scope.items.splice(Utils.arrayObjectIndexOf($scope.items, "_id", itemId), 1);
            var tab = _.filter($scope.tabs, function (tab, index) {
              var _t = _.filter(tab.items, function (item, i) {
                if (item && item._id == itemId) {
                  $scope.tabs[index].items.splice(i,1);
                }
              });
            });
          })

          $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
          $scope.itemPagination.totalItems = $scope.items.length;
          $scope.itemLoading=true;
          if(assignedCheck)
            growl.warning('Unassigned items deleted.', {ttl: 3000});
          else
            growl.success('All selected items deleted', {ttl: 3000});
        }, function(){
          growl.error('Error while deleting items', {ttl: 3000});
        });
      } else{
        growl.error("All the items are assigned, couldn't delete any.", {ttl: 3000});
        $scope.itemLoading=true;
      }

      // *** old mutli delete logic
      // $q.all(deleteItem).then(function(re){
      //   _.forEach(re,function(data){
      //     $scope.items.splice(Utils.arrayObjectIndexOf($scope.items, "_id", data._id), 1);
      //     var tab = _.filter($scope.tabs, function (tab, index) {
      //       var _t = _.filter(tab.items, function (item, i) {
      //         if (item._id == data._id) {
      //           $scope.tabs[index].items.splice(i,1);
      //         }
      //       });
      //     });
      //   })

      //   $scope.itemPagination.numPages = Math.ceil($scope.items.length / $scope.itemPagination.perPage);
      //   $scope.itemPagination.totalItems = $scope.items.length;
      //   $scope.itemLoading=true;
      // });

      if($scope.selectedDeleteItems && $scope.selectedDeleteItems.length>0){
        var promise = $q.all(null);
        angular.forEach($scope.selectedDeleteItems, function(url){
          $scope.inProcess = true;
          promise = promise.then(function(){
            return $http.post('/api/shopify/deleteProduct',url).then(function(success,err){
              if(success){
                console.log(url)
                growl.success(url.items.name + ' successfully delete from shopify!', {ttl: 3000});
              }
              else{

                console.log(err)
                growl.success('Error in Deleting', {ttl: 3000});
              }
            })
          });
        });

        promise.then(function(){
          _.forEach($scope.items, function (_item) {
            if(  _item.editSelected){
              _item.editSelected = false
            }
          })
          $scope.editItem={tabItemSelected:false,itemSelected:false};
          growl.success('All Items already deleted', {ttl: 3000});
        })
      }
      if($scope.urbanPiperKeys.length > 0 && $scope.urbanPiperKeys[0].active)
        $scope.deleteSelectionFromUrbanPiper();
      if($scope.nomnomKeys.length > 0 && $scope.nomnomKeys[0].active)
        $scope.deleteSelectionFromNomnom();

      //growl.error("Assigned items can't be deleted!!",{ttl:2000});
      notifyOnlineOrderPartners();
    }

    var tSettings = [
      {type: 'Kot', templateNumber: '', isApplied: false, time: {}, mobiles: '', dayBack: false}, {
        type: 'Dispatch',
        templateNumber: '',
        isApplied: false,
        time: {},
        mobiles: '',
        dayBack: false
      },
      {
        type: 'Delivery',
        templateNumber: '',
        isApplied: false,
        time: {},
        mobiles: '',
        dayBack: false
      }, {type: 'DeliveryBoy', templateNumber: '', isApplied: false, time: {}, mobiles: '', dayBack: false},
      {type: 'Sale', templateNumber: '', isApplied: false, time: {hour: '14', min: '0'}, mobiles: '', dayBack: false}
    ]
    $scope.templateSettings = [];
    $scope.templates = [];
    $scope.schedule = {Time: null, mobiles: '', dayBack: false};//=Utils.getResetDate($scope.settings, new Date());
    $scope.getSMSTemplates = function () {
      if ($scope.deployment.smsSettings.length == 0) {
        $scope.templateSettings = tSettings;
      } else {
        $scope.templateSettings = $scope.deployment.smsSettings;
      }
      var _fSaleTime = _.filter($scope.templateSettings, {type: 'Sale'});
      if (_fSaleTime.length > 0) {
        if (_.has(_fSaleTime[0], 'time')) {
          var date = new Date();
          $scope.schedule.Time = date.setHours((_fSaleTime[0].time.hour == null || _fSaleTime[0].time.hour == '' ? 14 : _fSaleTime[0].time.hour), (_fSaleTime[0].time.min == null || _fSaleTime[0].time.min == '' ? 0 : _fSaleTime[0].time.min), 0, 0);
        }
      } else {

      }
      if (_.has(_fSaleTime[0], 'mobiles')) {
        $scope.schedule.mobiles = _fSaleTime[0].mobiles;
      }
      $scope.checkSaleApplied();
      //console.log($scope.deployment.smsSettings);
      Smstemplates.get().$promise.then(function (templates) {
        // console.log(templates);

        $scope.templates = _.sortBy(templates, ['templateNumber']);
        $scope.tPagination = Pagination.getNew(5);
        $scope.tPagination.numPages = Math.ceil($scope.templates.length / $scope.tPagination.perPage);
        $scope.tPagination.totalItems = $scope.templates.length;
      })
      //$scope.updateDeploymentSettings();
    }

    $scope.dropped = function (event) {
      //  console.log(event);
      var dragEl = angular.element(event.dragEl);
      var dragTemplate = dragEl.attr('id');
      var dropEl = angular.element(event.dropEl);
      var dropTemplate = dropEl.attr('id');
      var dragIndex = Utils.arrayObjectIndexOf($scope.templates, 'templateNumber', dragTemplate);
      var dropIndex = Utils.arrayObjectIndexOf($scope.templateSettings, 'type', dropTemplate);
      //console.log(dragEl);
      if ($scope.templateSettings[dropIndex].type == $scope.templates[dragIndex].type) {
        $timeout(function () {
          $scope.templateSettings[dropIndex].templateNumber = $scope.templates[dragIndex].templateNumber.toString();
          $scope.templateSettings[dropIndex].isApplied = true;
          var _fSaleTime = _.filter($scope.templateSettings, {type: 'Sale'});
          if (_fSaleTime.length > 0) {
            if (_.has(_fSaleTime[0], 'time')) {
              _fSaleTime[0].time.hour = new Date($scope.schedule.Time).getHours();
              _fSaleTime[0].time.min = new Date($scope.schedule.Time).getMinutes();
            } else {
              _fSaleTime[0].time = {
                hour: new Date($scope.schedule.Time).getHours(),
                min: new Date($scope.schedule.Time).getMinutes()
              }
            }
            if (_.has(_fSaleTime[0], 'mobiles')) {
              _fSaleTime[0].mobiles = $scope.schedule.mobiles;
            }
          }
          $scope.deployment.smsSettings = $scope.templateSettings;

          $scope.updateDeploymentSettings().then(function () {
          });
          $scope.checkSaleApplied();
        }, 10)
      } else {
        growl.error('Template type mismatch.', {ttl: 3000});
      }

    }
    $scope.showUpdateMobile = false;
    $scope.updateMobile = function () {
      $scope.showUpdateMobile = true;
    }
    $scope.removeTemplate = function (index) {
      $timeout(function () {
        $scope.templateSettings[index].templateNumber = '';
        $scope.templateSettings[index].isApplied = false;
        $scope.updateDeploymentSettings().then(function () {
        });
      }, 10)
      $scope.checkSaleApplied();
    }
    $scope.updateSettings = function () {
      $scope.updateDeploymentSettings().then(function () {
      });
    }
    $scope.saveScheduleTime = function () {
      $timeout(function () {
        var _fSaleTime = _.filter($scope.templateSettings, {type: 'Sale'});
        // console.log(_fSaleTime);
        if (_fSaleTime.length > 0) {
          if (_.has(_fSaleTime[0], 'time')) {
            console.log(new Date($scope.schedule.Time));
            _fSaleTime[0].time.hour = new Date($scope.schedule.Time).getHours();
            _fSaleTime[0].time.min = new Date($scope.schedule.Time).getMinutes();
          } else {
            _fSaleTime[0].time = {
              hour: new Date($scope.schedule.Time).getHours(),
              min: new Date($scope.schedule.Time).getMinutes()
            }
          }
        }
        $scope.updateDeploymentSettings().then(function (dep) {
          //console.log(dep);
        });

      }, 20)
    }
    $scope.checkSaleApplied = function () {
      var flag = false;
      var _fSaleTime = _.filter($scope.templateSettings, {type: 'Sale'});
      // console.log(_fSaleTime);
      if (_fSaleTime.length > 0) {
        flag = _fSaleTime[0].isApplied;
      }
      return flag;
    }
    $scope.updateMobileToSMS = function () {
      var _fSaleTime = _.filter($scope.templateSettings, {type: 'Sale'});
      if (_fSaleTime.length > 0) {
        if (_.has(_fSaleTime[0], 'mobiles')) {
          _fSaleTime[0].mobiles = $scope.schedule.mobiles;
        } else {
          var temp = angular.copy(_fSaleTime[0]);
          temp.mobiles = '';
          temp.mobiles = $scope.schedule.mobiles;
          console.log(temp);
          $scope.templateSettings.splice(Utils.arrayObjectIndexOf($scope.templateSettings, 'type', 'Sale'), 1)
          $scope.templateSettings.push(temp);
          // _fSaleTime[0]= angular.copy(temp);
        }
        if (_.has(_fSaleTime[0], 'dayBack')) {
          _fSaleTime[0].dayBack = $scope.schedule.dayBack;
        } else {
          var temp = angular.copy(_fSaleTime[0]);
          temp.dayBack = '';
          temp.dayBack = $scope.schedule.dayBack;
          console.log(temp);
          $scope.templateSettings.splice(Utils.arrayObjectIndexOf($scope.templateSettings, 'type', 'Sale'), 1)
          $scope.templateSettings.push(temp);
          // _fSaleTime[0]= angular.copy(temp);
        }
      }
      $scope.updateDeploymentSettings().then(function (dep) {
        $scope.showUpdateMobile = false;
      });
    }

    $scope.createShopify = function (form) {
      console.log($scope.shopify);
      $scope.shopify.tenantId = currentUser.tenant_id;
      $scope.shopify.deploymentId = $scope.deployment._id;
      $scope.submittedStation = true;
      if (form.$valid) {
        Shopify.create({}, $scope.shopify, function (result) {
          // $cookies['shopName'] = $scope.shopify.shopName
          // $cookies['apiKey'] = $scope.shopify.apiKey
          // $cookies['sharedSecret'] = $scope.shopify.sharedSecret
          // $cookies['deploymentId'] = $scope.deployment._id;

          console.log(result);
          $window.location.href = result.url;
        }, function (err) {
          console.log(err)
          $scope.shopify = {};
          // $scope.printStation.psInEdit=false;
          form.$setPristine();
          growl.error('Error Creating Shop!', {ttl: 3000});
        });

      }

      $scope.printStation = {};
      // $scope.printStation.psInEdit=false;
      form.$setPristine();

    }

    $scope.addSelectionToShopify = function () {
      console.log($scope.items)
      //$scope.inProcess = true
      $scope.selectedItems = []
      $scope.itemToAdd = []
      for (var i = 0; i < $scope.items.length; i++) {
        if ($scope.items[i].editSelected == true && !$scope.items[i].shopifyId) {
          $scope.selectedItems.push($scope.items[i]);
        }
      }
      console.log($scope.selectedItems.length)
      if ($scope.selectedItems.length > 0) {
        console.log($scope.selectedItems)
        for (var i = 0; i < $scope.selectedItems.length; i++) {
          $scope.itemToAdd[i] = {};
          $scope.itemToAdd[i].item = $scope.selectedItems[i];
          $scope.itemToAdd[i].shopDetails = $scope.shopKeys[0];
        }
        var promise = $q.all(null);
        angular.forEach($scope.itemToAdd, function (url) {
          promise = promise.then(function () {
            return $http.post('api/shopify/addProduct', url).then(function (success, err) {
              if (success) {
                console.log(url)
                var tempItem = url.item;
                tempItem.shopifyId = success.data.product.id
                //$scope.itemToAdd.item.shopifyId = success.data.product.id
                var tabs = []
                _.forEach(tempItem.tabs, function (tab) {
                  tabs.push({id:tab._id,rate:tab.item.rate})
                });
                var updateHistory = {
                  timeStamp:new Date,
                  name:item.name,
                  tabs:tabs,
                  rate:item.rate,
                  username:currentUser.username
                }
                tempItem.lastModified = new Date;
                tempItem.updateHistory.push(updateHistory)
                if(tempItem.updateHistory.length>5){
                  tempItem.updateHistory.splice(0,tempItem.updateHistory.length-5);
                }
                Item.update({}, tempItem, function (item) {
                  console.log(item)
                  $scope.inProcess = false
                  growl.success(item.name + ' successfully added!', {ttl: 3000});
                }, function (err) {
                  console.log(err)
                  $scope.inProcess = false
                  growl.error('Error Creating Item!', {ttl: 3000});
                });
              }
            }).error(function (err) {
              console.log(err)
              growl.error('Error in adding' + $scope.itemToAdd.item.name, {ttl: 3000});
            })
          });
        });
        promise.then(function () {
          //This is run after all of your HTTP requests are done
          growl.success('All Items already added', {ttl: 3000});
        })
        // for(var i=0;i<$scope.selectedItems.length;i++){
        //   if(!$scope.selectedItems[i].shopifyId){
        //     console.log($scope.selectedItems[i])
        //     $scope.itemToAdd.item = $scope.selectedItems[i];
        //     $scope.itemToAdd.shopDetails = $scope.shopKeys[0];
        //     console.log($scope.itemToAdd)
        //     $scope.inProcess = true
        //     //$timeout( function(){
        //       $http.post('api/shopify/addProduct',$scope.itemToAdd).then(function(success,err){
        //         if(success){
        //           console.log(success.data.product.id)
        //           $scope.itemToAdd.item.shopifyId = success.data.product.id
        //           Item.update({}, $scope.itemToAdd.item, function (item) {
        //             console.log(item)
        //             $scope.inProcess = false
        //             growl.success(item.name + ' successfully added!', {ttl: 3000});
        //           }, function (err) {
        //             console.log(err)
        //             $scope.inProcess = false
        //             growl.error('Error Creating Item!', {ttl: 3000});
        //           });
        //         }
        //         else{
        //           console.log(err)
        //           growl.success('Error in adding' + $scope.itemToAdd.item.name, {ttl: 3000});
        //         }
        //       })
        //     //}, 5000);

        //   }
        // }
      }
      else {
        growl.success('All Items already added', {ttl: 3000});
      }
      _.forEach($scope.items, function (_item) {
        if (_item.editSelected) {
          _item.editSelected = false
        }
      })
      $scope.editItem = {tabItemSelected: false, itemSelected: false};

    }

    $scope.activateShopify = function (shop) {
      var tempShopKey = shop;
      tempShopKey.active = true;
      Shopify.update({}, tempShopKey, function (result) {
        console.log(result)
        Shopify.get({'tenantId': currentUser.tenant_id, 'deploymentId': deployment._id}, function (tabs) {
          console.log(tabs[0]);
          $scope.shopKeys = tabs
        });
      })
    }

    $scope.deactivateShopify = function (shop) {
      var tempShopKey = shop;
      tempShopKey.active = false;
      Shopify.update({}, tempShopKey, function (result) {
        console.log(result)
        Shopify.get({'tenantId': currentUser.tenant_id, 'deploymentId': deployment._id}, function (tabs) {
          console.log(tabs[0]);
          $scope.shopKeys = tabs
        });
      })
    }

    $scope.deleteSelectionFromShopify = function () {
      //$scope.inProcess = true;
      if ($scope.shopKeys.length > 0) {
        $scope.selectedDeleteItems = []
        for (var i = 0; i < $scope.items.length; i++) {
          if ($scope.items[i].editSelected == true && $scope.items[i].shopifyId) {
            $scope.selectedDeleteItems.push({
              items: $scope.items[i],
              shopifyId: $scope.items[i].shopifyId,
              shopDetails: $scope.shopKeys[0]
            });
          }
        }
      }
      var promise = $q.all(null);

      angular.forEach($scope.selectedDeleteItems, function (url) {
        //$scope.inProcess = true;
        promise = promise.then(function () {
          return $http.post('/api/shopify/deleteProduct', url).success(function (success) {
            if (success) {
              console.log(url)
              var tempItem = url.items;
              delete tempItem.shopifyId
              //$scope.itemToAdd.item.shopifyId = success.data.product.id
              var tabs = []
              _.forEach(tempItem.tabs, function (tab) {
                tabs.push({id:tab._id,rate:tab.item.rate})
              });
              var updateHistory = {
                timeStamp:new Date,
                name:tempItem.name,
                tabs:tabs,
                rate:tempItem.rate,
                username:currentUser.username
              }
              tempItem.lastModified = new Date;
              tempItem.updateHistory.push(updateHistory)
              if(tempItem.updateHistory.length>5){
                tempItem.updateHistory.splice(0,tempItem.updateHistory.length-5);
              }
              Item.update({}, tempItem, function (item) {
                console.log(item)
                //$scope.inProcess = false
                growl.success(item.name + ' successfully delete from shopify!', {ttl: 3000});
              }, function (err) {
                console.log(err)
                //$scope.inProcess = false
                growl.error('Error Deleting Item!', {ttl: 3000});
              });
            }
          }).error(function (err) {
            console.log(err)
            growl.error('Error in Deleting', {ttl: 3000});
          })
        });
      });
      promise.then(function () {
        _.forEach($scope.items, function (_item) {
          if (_item.editSelected) {
            _item.editSelected = false
          }
        })
        $scope.editItem = {tabItemSelected: false, itemSelected: false};
        growl.success('All Items already deleted', {ttl: 3000});
      })
    }

    $scope.isPermission = function (permissions) {
      var flag = false;
      if (currentUser.role === 'user') {
        // flag= true;
        //   } else {
        var permissionArr = permissions.split(',');
        // console.log(permissionArr);
        _.forEach(permissionArr, function (permname) {
          if (Utils.hasPermission(currentUser.selectedPermissions, permname)) {
            flag = true;
          }
        })
      } else {
        flag = true;
      }
      return flag;
    }
    $scope.getGroupPermission = function (permissions) {
      var flag = false;
      if (currentUser.role === 'user') {
        // flag= true;
        //   } else {
        var permissionArr = permissions.split(',');
        // console.log(permissionArr);
        _.forEach(permissionArr, function (permname) {
          // alert(permname);
          if (Utils.hasUserPermission(currentUser.selectedPermissions, permname)) {
            flag = true;
          }
        })
      } else {
        flag = true;
      }
      return flag;
    }

   /*Chnanges done by Abhishek*/
     $scope.addOnItems = [];
    $scope.comboItems = [];
    var modalAddOnMap;

    $scope.renderAddOns = function () {
      $scope.addOnItems = [];
      $scope.forAddOnItems = [];
      _.forEach($scope.items, function (item) {
          if (item.category.isAddons == true){
            $scope.addOnItems.push({
              name: item.name,
              _id: item._id,
              mapItems: item.mapItems || []
            });
          }
          else{
            $scope.forAddOnItems.push({
              _id : item._id,
              name : item.name,
              category : {
                categoryName : item.category.categoryName,
                _id: item.category._id,
                superCategory: {
                  superCategoryName: item.category.superCategory.superCategoryName,
                  _id: item.category.superCategory._id
                }
              },
              rate: item.rate,
              stockQuantity : item.stockQuantity,
              unit: item.unit || null
            });
          }
      });
    };

    $scope.itemSelectCallback = function(item, index){
      item.modified = true;
    };

    $scope.itemRemoveCallback = function(item, index){
      item.modified = true;
    };

    $scope.mapItemsToAddons = function () {
      modalAddOnMap = $modal.open({
        template: '<br><h2 class="text-center"><i class="fa fa-spinner fa-spin"></i>&nbsp;Mapping AddOn Items</h2>',
        backdrop: 'static',
        keyboard: false,
        scope: $scope
      });
      $scope.autoCallUpdate($scope.addOnItems, 0);
    }

    $scope.autoCallUpdate = function (aitems, count) {
      if(aitems[count].modified){
        Item.updateItemsToAddons({item_id: aitems[count]._id, items: aitems[count].mapItems}).$promise.then(function (_item) {
          var index = Utils.arrayObjectIndexOf($scope.items, '_id', _item._id);
          $scope.items[index] = _item;
          delete aitems[count].modified;
          if (aitems.length - 1 > count) {
            count = count + 1;
            $scope.autoCallUpdate(aitems, count);
          } else {
            modalAddOnMap.close();
            notifyOnlineOrderPartners();
            growl.success('AddOns mapped successfully', {ttl: 2000});
          }
        }, function(){
          modalAddOnMap.close();
          growl.error('Error while mapping items for '+aitems[count].name, {ttl: 2000});
        });
      }
      else{
        if (aitems.length - 1 > count) {
          count = count + 1;
          $scope.autoCallUpdate(aitems, count);
        } else {
          setTimeout(function() {
            modalAddOnMap.close();
          }, 1000);
          notifyOnlineOrderPartners();
          growl.success('AddOns mapped successfully', {ttl: 2000});
        }
      }
    }

    $scope.renderCombos = function () {
      $scope.comboItems = [];
      $scope.forComboItems = [];
      _.forEach($scope.items, function (item) {
        if (item.category.isCombos == true) {
          $scope.comboItems.push({
            _id: item._id,
            name: item.name,
            mapComboItems : item.mapComboItems || []
          })
        }
        else{
          $scope.forComboItems.push({
            _id : item._id,
            name : item.name,
            category : {
              categoryName : item.category.categoryName,
              _id : item.category._id,
              superCategory: {
                superCategoryName: item.category.superCategory.superCategoryName,
                _id: item.category.superCategory._id
              }
            },
            rate: item.rate,
            stockQuantity : item.stockQuantity,
            unit: item.unit || null
          });
        }
      })
    }

    $scope.mapItemsToCombos = function () {
      modalAddOnMap = $modal.open({
        template: '<br><h2 class="text-center"><i class="fa fa-spinner fa-spin"></i>&nbsp;Mapping Combo Items</h2>',
        backdrop: 'static',
        keyboard: false,
        scope: $scope
      });
      $scope.autoCallUpdateCombo($scope.comboItems, 0);
    }

    $scope.autoCallUpdateCombo = function (aitems, count) {
      if(aitems[count].modified == true){
        Item.updateItemsToAddons({
          item_id: aitems[count]._id,
          comboItems: aitems[count].mapComboItems
        }).$promise.then(function (_item) {
          var index = Utils.arrayObjectIndexOf($scope.items, '_id', _item._id);
          $scope.items[index] = _item;
          delete aitems[count].modified;
          if (aitems.length - 1 > count) {
            count = count + 1;
            $scope.autoCallUpdateCombo(aitems, count);
          } else {
            modalAddOnMap.close();
            growl.success('Combos mapped successfully.', {ttl: 2000});
          }
        }, function(err){
          modalAddOnMap.close();
          growl.error('Error while mapping combo item '+aitems[count].name, {ttl: 2000});
        });
      }
      else{
        if (aitems.length - 1 > count) {
          count = count + 1;
          $scope.autoCallUpdateCombo(aitems, count);
        } else {
          setTimeout(function() {
            modalAddOnMap.close();
          }, 1000);
          growl.success('Combos mapped successfully.', {ttl: 2000});
        }
      }
    };
    var food = [];
    var liquor = [];
    $scope.renderLiquor = function () {
      food = [];
      liquor = [];
      $scope.fCategories = [];
      $scope.lCategories = [];
      _.forEach($scope.categories, function (category) {
        if (_.has(category, 'isliquor')) {
          if (category.isliquor) {
            $scope.lCategories.push(angular.copy(category));
          } else {
            $scope.fCategories.push(angular.copy(category));
          }
        } else {
          $scope.fCategories.push(angular.copy(category));
        }
      });
    }

    /*$scope.addSelected=function(key ,value){
     console.log(key);
     console.log(value);
     }*/
    $scope.shiftRight = function (item, value) {
      console.log('shift right', item);
      var key = item[0];
      console.log('shift right key', key);
      if(food.indexOf(key)==-1){
        console.log(food.indexOf(key));
        liquor.push(key);
      }
      else{
        food.splice(food.indexOf(key), 1);
      }
      console.log(liquor)
      var _f = _.filter($scope.categories, {_id: key});
      if (_f.length > 0) {
        _f[0].isliquor = true;
        $scope.fCategories.splice(Utils.arrayObjectIndexOf($scope.fCategories, '_id', key), 1);
        $scope.lCategories.push(angular.copy(_f[0]));
      }
    }
    $scope.shiftLeft = function (item, value) {
      //console.log(item);
      var key = item[0];
      if(liquor.indexOf(key)==-1){
        //console.log(_.find(liquor,{_id:key}));
        food.push(key);
      }
      else{
        liquor.splice(liquor.indexOf(key), 1);
      }
      //console.log(food);
      var _f = _.filter($scope.categories, {_id: key});
      if (_f.length > 0) {
        _f[0].isliquor = false;
        $scope.lCategories.splice(Utils.arrayObjectIndexOf($scope.lCategories, '_id', key), 1);
        $scope.fCategories.push(angular.copy(_f[0]));
      }
    }

    $scope.categoryWiseSplit = function(){
      console.log(food,liquor);
      var pushData = {
        food : food,
        liquor : liquor
      }
      Category.updateMultiple(pushData).$promise.then(function(success){
        _.forEach(food, function (value) {
          $scope.categories[Utils.arrayObjectIndexOf($scope.categories, '_id', value), 1].isliquor = false;
        })
        _.forEach(liquor, function (value) {
          $scope.categories[Utils.arrayObjectIndexOf($scope.categories, '_id', value), 1].isliquor = true;
        })
        growl.success('updated categories successfully', {ttl: 2000});
      })

    }

    $scope.instancePrinters = [];
    $scope.instancePrintersStation = [];
    /*Instance Wise Printer Setup*/
    $scope.getInstanceSettings = function () {
      $scope.instancePrinters = [];
      $scope.instancePrintersStation = [];
      //console.log(deployment);
      Deployment.getInstancePrint({deployment_id: deployment._id}, function (instance) {
        console.log(instance);
        var printInstances = instance.instances;
        if (_.has(deployment, 'instances')) {
          _.forEach(deployment.instances, function (instance) {
            var tempprinter = '';
            for (var i = 0; i < printInstances.length; i++) {
              if (printInstances[i].id == instance.id) {
                tempprinter = printInstances[i].printer;
              }
            }
            var instanceOb = {
              id: instance.id,
              name: instance.name,
              printer: tempprinter
            };
            // if(tempprinter!=null)
            $scope.instancePrinters.push(instanceOb);
          })
        }
      })

      Deployment.getInstanceStationsPrint({deployment_id: deployment._id}, function (instanceStation) {
        console.log(instanceStation);
        var printInstances = instanceStation.instancesStations;
        _.forEach($scope.stations, function (station) {
          if (_.has(deployment, 'instances')) {
            _.forEach(deployment.instances, function (instance) {
              var tempprinter = '';
              for (var i = 0; i < printInstances.length; i++) {
                if (printInstances[i].id == instance.id && printInstances[i].station_id == station._id) {
                  tempprinter = printInstances[i].printer;
                }
              }
              var instanceOb = {
                id: instance.id,
                name: instance.name,
                station_id: station._id,
                printer: tempprinter
              };
              // if(tempprinter!=null)
              $scope.instancePrintersStation.push(instanceOb);
            })
          }
        })
      }, function () {
        _.forEach($scope.stations, function (station) {
          if (_.has(deployment, 'instances')) {
            _.forEach(deployment.instances, function (instance) {
              var tempprinter = '';
              var instanceOb = {
                id: instance.id,
                name: instance.name,
                station_id: station._id,
                printer: tempprinter
              };
              // if(tempprinter!=null)
              $scope.instancePrintersStation.push(instanceOb);
            })
          }
        })
      })

    }

    $scope.assignPrinter = function () {
      // console.log($scope.instancePrinters);
      var instancePrinterOb = {deployment_id: deployment._id, instances: $scope.instancePrinters};
      Deployment.createInstancePrint(instancePrinterOb).$promise.then(function (result) {
        console.log(result);
        growl.success('Selected Printer Assigned To Instance. ', {ttl: 3000});
      })
    }

    $scope.assignPrinterStation = function () {
      console.log($scope.instancePrintersStation);
      var instancePrinterOb = {deployment_id: deployment._id, instancesStations: $scope.instancePrintersStation};
      Deployment.createInstanceStationsPrint(instancePrinterOb).$promise.then(function (result) {
        console.log(result);
        growl.success('Selected Printer Assigned To Instance. ', {ttl: 3000});
      })
    }


    $scope.mobikwik = {};
    $scope.isRegisteredWithMobikwik = false;

    $http({
      method: 'GET',
      url: '/api/mobikwikSettings/',
      params: {deployment_id: $scope.deployment._id, tenant_id: $scope.deployment.tenant_id}
    }).success(function (settings) {
      if (settings.length > 0) {
        $scope.mobikwik = settings[0];
        $scope.isRegisteredWithMobikwik = true;
      }
    }).error(function (err) {
      console.log("error", err);
    });

    console.log("deployment", $scope.deployment);

    $scope.registerWithMobikwik = function () {
      $scope.mobikwik.deployment_id = $scope.deployment._id;
      $scope.mobikwik.tenant_id = $scope.deployment.tenant_id;
      console.log($scope.mobikwik);
      $scope.registeringWithMobikwik = true;
      $http({
        method: 'POST',
        url: '/api/mobikwikSettings/',
        data: $scope.mobikwik
      }).success(function (data) {
        growl.success("Registered with mobikwik", {ttl: 3000});
        $scope.isRegisteredWithMobikwik = true;
        $scope.registeringWithMobikwik = false;
      }).error(function (err) {
        growl.error('Error in registering', {ttl: 3000});
        $scope.registeringWithMobikwik = false;
      })
    }

    $scope.updateMobikwikSettings = function () {
      $scope.mobikwik.deployment_id = $scope.deployment._id;
      $scope.mobikwik.tenant_id = $scope.deployment.tenant_id;
      console.log($scope.mobikwik);
      $scope.registeringWithMobikwik = true;
      $http({
        method: 'POST',
        url: '/api/mobikwikSettings/update',
        data: $scope.mobikwik
      }).success(function (data) {
        if (data.updated == true)
          growl.success("Updated mobikwik settings", {ttl: 3000});
        else
          growl.error("Error in updating", {ttl: 3000});
        $scope.registeringWithMobikwik = false;
      }).error(function (err) {
        growl.error('Error in updating', {ttl: 3000});
        $scope.registeringWithMobikwik = false;
      })
    }

    //added by tanvee
    UrbanPiper.getUrbanPiperKeys({tenantId: currentUser.tenant_id, deploymentId: deployment._id}).then(function (data) {
      if (data.length > 0)
        $scope.urbanPiperKeys.push(data[0]);
    })


    $scope.createUrbanPiper = function (form) {
      console.log("in creatye urban piper");
      $scope.urbanPiper.tenantId = currentUser.tenant_id;
      $scope.urbanPiper.deploymentId = $scope.deployment._id;
      if ($scope.urbanPiper.apiKey && $scope.urbanPiper.username) {
        console.log("hello")
        UrbanPiper.createUrbanPiper($scope.urbanPiper, function (data) {
          $scope.urbanPiperKeys.push(data);
          growl.success('UrbanPiper successfully created!', {ttl: 3000});
        })

      }
      form.$setPristine();
    }

    $scope.deleteUrbanPiper = function (shop) {
      // UrbanPiper.deleteUrbanPiper({id: shop._id}, function(success){
      //       $scope.urbanPiperKeys.splice(Utils.arrayObjectIndexOf($scope.urbanPiperKeys, "_id", shop._id), 1);
      //       growl.success('UrbanPiper successfully deleted!', {ttl: 3000});
      //     })
      // Item.get({
      //   tenant_id: localStorageService.get('tenant_id'),
      //   deployment_id: $scope.deployment._id
      // }, function (items) {
      //   var flag = 0;
      //   for (var i = 0; i < items.length; i++) {
      //     if (items[i].urbanPiperId) {
      //       flag = 1;
      //       break;
      //     }
      //   }
      //   if (flag == 0) {
      //     UrbanPiper.deleteUrbanPiper({id: shop._id}, function (success) {
      //       $scope.urbanPiperKeys.splice(Utils.arrayObjectIndexOf($scope.urbanPiperKeys, "_id", shop._id), 1);
      //       growl.success('UrbanPiper successfully deleted!', {ttl: 3000});
      //     })
      //   }
      //   else {
      //     growl.error('Items Assigned to UrbanPiper!', {ttl: 3000});
      //   }
      // });
      var flag = 0;
      _.forEach($scope.items, function(item){
        if (item.urbanPiperId) {
          flag = 1;
          return false;
        }
      });

      if (flag == 0) {
        UrbanPiper.deleteUrbanPiper({id: shop._id}, function (success) {
          $scope.urbanPiperKeys.splice(Utils.arrayObjectIndexOf($scope.urbanPiperKeys, "_id", shop._id), 1);
          growl.success('UrbanPiper successfully deleted!', {ttl: 3000});
        });
      }
      else {
        growl.error('Items Assigned to UrbanPiper!', {ttl: 3000});
      }
    }

    $scope.activateUrbanPiper = function (shop) {
      var tempShopKey = shop;
      tempShopKey.active = true;
      UrbanPiper.activateUrbanPiper(tempShopKey, function (result) {
        console.log($scope.urbanPiperKeys)
      })
    }
    $scope.deactivateUrbanPiper = function (shop) {
      var tempShopKey = shop;
      tempShopKey.active = false;
      UrbanPiper.activateUrbanPiper(tempShopKey, function (result) {
        console.log($scope.urbanPiperKeys)
      })
    }

    $scope.addSelectionToUrbanPiper = function () {
      $scope.inProcess = true
      var postData = {items: $scope.items, urbanPiperKeys: $scope.urbanPiperKeys[0]};
      UrbanPiper.addSelectionToUrbanPiper(postData, function (result) {
        if (result.status == 200) {
          $scope.inProcess = false;
          _.forEach($scope.items, function (_item) {
            if (_item.editSelected) {
              _item.editSelected = false;
              _item.urbanPiperId = 1;
            }
          });
          growl.success('Items successfully added!', {ttl: 3000});
        }
        else {
          $scope.inProcess = false;
          growl.error('Error in adding', {ttl: 3000});
        }

      })
      $scope.editItem = {tabItemSelected: false, itemSelected: false};
    }

    $scope.updateItemsInUrbanPiper = function(temp){
      var postData = {items:{associations:[]},urbanPiperKeys:temp.urbanPiperKeys};
      var query={};
      query.tenant_id = temp.items[0].tenant_id;
      query.deployment_id = temp.items[0].deployment_id;
      query.tabType = 'delivery';
      Tab.getTabsByType(query).$promise.then(function(res){
        console.log(res);
        _.forEach(temp.items, function (itm) {

          var deliveryTab = _.filter(itm.tabs, {_id:res[0]._id});
          //console.log(deliveryTab)
          if(deliveryTab.length>0){
            if(itm.urbanPiperId && parseInt(itm.urbanPiperId)>0){
              console.log(itm)
              var taxes = [{
                "name": "service tax",
                "type": "percent",
                value: 0
              },{
                "name": "vat",
                "type": "percent",
                value: 0
              }]
              if (_.has(deliveryTab[0], 'taxes')){
                _.forEach(deliveryTab[0].taxes, function (tax) {
                  if(tax.name.match(/service/i) && tax.type == 'percentage'){
                    taxes[0].value = tax.value;
                  }
                  else if(tax.name.match(/vat/i) && tax.type == 'percentage'){
                    taxes[1].value = tax.value;
                  }
                })
              }
              var tempItem = {}
              tempItem = {  "item_ref_id": itm._id,
                "location_ref_id": itm.deployment_id,
                "title": deliveryTab[0].item?deliveryTab[0].item.name:itm.name,
                "price": deliveryTab[0].item?parseInt(deliveryTab[0].item.rate):itm.rate,
                "description": deliveryTab[0].item?deliveryTab[0].item.description:itm.description,
                "active": true,
                "taxes" : taxes
              }
              console.log(tempItem,deliveryTab[0].item.name)

              postData.items.associations.push(tempItem);

            }
          }
        });
        console.log(postData)
      });
    }

    $scope.deleteSelectionFromUrbanPiper = function () {
      $scope.inProcess = true
      var postData = {items: {associations: []}, urbanPiperKeys: $scope.urbanPiperKeys[0]};
      _.forEach($scope.items, function (_item) {
        if (_item.editSelected) {
          var itemToPush = _item
          itemToPush.urbanPiperId = -1;
          console.log(itemToPush)
          var tabs = []
          _.forEach(itemToPush.tabs, function (tab) {
            tabs.push({id: tab._id, rate: tab.item.rate})
          });
          var updateHistory = {
            timeStamp: new Date,
            name: itemToPush.name,
            tabs: tabs,
            rate: itemToPush.rate,
            username: currentUser.username
          }
          itemToPush.lastModified = new Date;
          itemToPush.updateHistory.push(updateHistory)
          if(itemToPush.updateHistory.length>5){
            itemToPush.updateHistory.splice(0,itemToPush.updateHistory.length-5);
          }
          Item.update({}, itemToPush);
          var itemToPush = {
            "item_ref_id": _item._id,
            "location_ref_id": _item.deployment_id,
            "title": _item.name,
            "price": _item.rate,
            "description": _item.description,
            "active": false,
            "taxes": []
          }
          postData.items.associations.push(itemToPush);
        }
      })

      $http.post('api/urbanPiper/deleteItems', postData).success(function (success) {
        if (success) {
          $scope.inProcess = false;
          _.forEach($scope.items, function (_item) {
            if (_item.editSelected) {
              _item.editSelected = false;
              _item.urbanPiperId = -1;
            }
          });
          growl.success('Items successfully deleted!', {ttl: 3000});
        }
      }).error(function (err) {
        $scope.inProcess = false;
        growl.error('Error in deleting', {ttl: 3000});
      })
      $scope.editItem = {tabItemSelected: false, itemSelected: false};
    }

    //added by tanvee
    Nomnom.getNomnomKeys({tenantId: currentUser.tenant_id, deploymentId: deployment._id}).then(function (data) {
      if (data.length > 0)
        $scope.nomnomKeys.push(data[0]);


    })

    $scope.createNomnom = function (form) {
      $scope.nomnom.tenantId = currentUser.tenant_id;
      $scope.nomnom.deploymentId = $scope.deployment._id;
      if (form.$valid) {
        Nomnom.createNomnom($scope.nomnom, function (data) {
          console.log(data)
          $scope.nomnomKeys.push(data);
          growl.success('Nomnom successfully created!', {ttl: 3000});
        })

      }
      form.$setPristine();
    }

    $scope.deleteNomnom = function (shop) {
      var flag = 0;
      _.forEach($scope.items, function(item){
        if (item.NomnomId) {
          flag = 1;
          return false;
        }
      });
      if (flag == 0) {
        Nomnom.deleteNomnom({id: shop._id}, function (success) {
          $scope.nomnomKeys.splice(Utils.arrayObjectIndexOf($scope.nomnomKeys, "_id", shop._id), 1);
          growl.success('Nomnom successfully deleted!', {ttl: 3000});
        });
      }
      else {
        growl.error('Items Assigned to Nomnom!', {ttl: 3000});
      }
    }

    $scope.activateNomnom = function (shop) {
      var tempShopKey = shop;
      tempShopKey.active = true;
      Nomnom.activateNomnom(tempShopKey, function (result) {
        console.log($scope.nomnomKeys)
      })
    }
    $scope.deactivateNomnom = function (shop) {
      var tempShopKey = shop;
      tempShopKey.active = false;
      Nomnom.activateNomnom(tempShopKey, function (result) {
        //console.log($scope.urbanPiperKeys)
      })
    }

    $scope.addSelectionToNomnom = function () {
      $scope.inProcess = true
      var postData = {items: $scope.items, nomnomKeys: $scope.nomnomKeys[0]};
      Nomnom.addSelectionToNomnom(postData, function (result) {
        if (result.status == 200) {
          $scope.inProcess = false;
          _.forEach($scope.items, function (_item) {
            if (_item.editSelected) {
              _item.editSelected = false;
              _item.NomnomId = 1;
            }
          })
        }
        else {
          $scope.inProcess = false;
          growl.error('Error in adding', {ttl: 3000});
        }
      });
      $scope.editItem = {tabItemSelected: false, itemSelected: false};
    }

    $scope.deleteSelectionFromNomnom = function () {
      $scope.inProcess = true
      var postData = {items: $scope.items, nomnomKeys: $scope.nomnomKeys[0]};
      Nomnom.deleteSelectionFromNomnom(postData, function (result) {
        if (result.status == 200) {
          $scope.inProcess = false;
          _.forEach($scope.items, function (_item) {
            if(  _item.editSelected){
              _item.editSelected = false;
              _item.NomnomId = -1;
            }
          });
        }
        else {
          $scope.inProcess = false;
          growl.error('Error in deleting', {ttl: 3000});
        }

      })
      $scope.editItem={tabItemSelected:false,itemSelected:false};
    }
    //added by tanvee

    function updateItemInIntegrations(item, flag){
      //flag = 0 if added, flag = 1 if updated
      var qAll = {};

      if(flag == 0){
        /*****ADD ITEM IN URBANPIPER*****/
        if($scope.urbanPiperKeys.length > 0 && $scope.urbanPiperKeys[0].active){
          var postData = {item:item,urbanPiperKeys:$scope.urbanPiperKeys[0]};
          var def = $q.defer();
          UrbanPiper.addItemToUrbanPiper(postData,function(result){
            def.resolve(result);
            // if(result.status==200){
            //   item.urbanPiperId = 1;
            //   growl.success('Added to UrbanPiper!', {ttl: 3000});
            // }
            // else{
            //   growl.error('Error In Adding To UrbanPiper!', {ttl: 3000});
            // }
          });
          qAll.urbanPiper = def.promise;
        }
        /*****ADD ITEM IN URBANPIPER*****/
        /*****ADD ITEM IN NOMNOM*****/
        if($scope.nomnomKeys.length > 0 && $scope.nomnomKeys[0].active){
          var postData = {item:item,nomnomKeys:$scope.nomnomKeys[0]};
          var def = $q.defer();
          Nomnom.addItemToNomnom(postData,function(result){
            def.resolve(result);
            // if(result.status==200){
            //   item.nomnomId = 1;
            //   growl.success('Added to Nomnom!', {ttl: 3000});
            // }
            // else{
            //   growl.error('Error In Adding To Nomnom!', {ttl: 3000});
            // }
          });
          qAll.nomnom = def.promise;
        }
        /*****ADD ITEM IN NOMNOM*****/
        /*****ADD ITEM IN SHOPIFY*****/
        if($scope.shopKeys.length > 0 && $scope.shopKeys[0].active){
          var details = {
            item : item,
            shopDetails : $scope.shopKeys[0]
          };
          qAll.shopify = $http.post('api/shopify/addProduct',details);
          // $http.post('api/shopify/addProduct',details).then(function(success,err){
          //   if(success){
          //     growl.success('Added to Shop!', {ttl: 3000});
          //   }
          //   else if(err){
          //     growl.error('Error In Adding To Shop!', {ttl: 3000});
          //   }
          // });
        }
        /*****ADD ITEM IN SHOPIFY*****/

        if(qAll.urbanPiper || qAll.nomnom || qAll.shopify){
          $q.all(qAll).then(function(resolutions){
            _.forEach($scope.items, function(_item){
              if(_item._id == item._id){
                if(resolutions.urbanPiper){
                  if(resolutions.urbanPiper.status == 200){
                    _item.urbanPiperId = 1;
                    growl.success('Added to UrbanPiper!', {ttl: 3000});
                  }
                }
                if(resolutions.nomnom){
                  if(resolutions.nomnom.status == 200){
                    _item.nomnomId = 1;
                    growl.success('Added to NomNom!', {ttl: 3000});
                  }
                }
                if(resolutions.shopify == 200){
                  if(resolutions.shopify.status == 200)
                    growl.success('Added to Shopify!', {ttl: 3000});
                }
              }
            });
          });
        }
      }

      if(flag == 1){
        console.log(item, flag);
        /*****UPDATE ITEM IN SHOPIFY*****/
        if(item.shopifyId && $scope.shopKeys.length > 0 && $scope.shopKeys[0].active){
          var details = {
            item : item,
            shopDetails : $scope.shopKeys[0]
          };
          $http.post('/api/shopify/updateProduct',details).then(function(success,err){
            if(success){
              console.log(success)
              growl.success(item.name + ' successfully updated in shopify!', {ttl: 3000});
            }
          })
        }
        /*****UPDATE ITEM IN SHOPIFY*****/

        /*****UPDATE ITEM IN URBANPIPER*****/
        if ($scope.urbanPiperKeys.length > 0 && $scope.urbanPiperKeys[0].active && item.urbanPiperId && item.urbanPiperId == "1") {

          var postData = {item: item, urbanPiperKeys: $scope.urbanPiperKeys[0],tabs:$scope.tabs};
          UrbanPiper.updateItemInUrbanPiper(postData, function (result) {
            console.log(result)
            if (result.status == 200) {
              growl.success('Updated In UrbanPiper!', {ttl: 3000});
            }
            else {
              growl.error('Error Updating In UrbanPiper!', {ttl: 3000});
            }
          })
        }
        /*****UPDATE ITEM IN URBANPIPER*****/
        /*****UPDATE ITEM IN NOMNOM*****/
        if ($scope.nomnomKeys.length > 0 && $scope.nomnomKeys[0].active && item.nomnomId && item.nomnomId == "1") {
          var postData = {item: item, nomnomKeys: $scope.nomnomKeys[0]};
          Nomnom.updateItemToNomnom(postData, function (result) {
            console.log(result)
            if (result.status == 200) {
              growl.success('Updated In Nomnom!', {ttl: 3000});
            }
            else {
              growl.error('Error Updating In Nomnom!', {ttl: 3000});
            }
          });
        }
        /*****UPDATE ITEM IN NOMNOM*****/
      }
    }

    function updateItemsInIntegrations(items){
      /*****Update ITEMS IN URBANPIPER*****/
      if($scope.urbanPiperKeys.length > 0 && $scope.urbanPiperKeys[0].active){
        var postData = {items: items, urbanPiperKeys:$scope.urbanPiperKeys[0], tabs: $scope.tabs};
        UrbanPiper.updateItemsInUrbanPiper(postData,function(result){
          console.log(result)
          if(result.status==200){
            growl.success('Updated In UrbanPiper!', {ttl: 3000});
          }
          else{
            growl.error('Error Updating In UrbanPiper!', {ttl: 3000});
          }
        })
      }
      /*****UPDATE ITEMS IN URBANPIPER*****/
      /****Update ITEMS IN NOMNOM****/
      if($scope.nomnomKeys.length > 0 && $scope.nomnomKeys[0].active){
        var postData = {items:items,nomnomKeys:$scope.nomnomKeys[0]};
        Nomnom.updateItemsInNomnom(postData,function(result){
          console.log(result)
          if(result.status==200){
            growl.success('Updated In Nomnom!', {ttl: 3000});
          }
          else{
            growl.error('Error Updating In Nomnom!', {ttl: 3000});
          }
        })
      }
      /*****UPDATE ITEMS IN NOMNOM*****/
    };

    /**
     ****************THIRD PARTY INTEGRATIONS*****************
     @service - ThirdPartyService
     @scope_functions - togglePartnerActivation, copy
     @author - Shubhank
     **/

    $scope.togglePartnerActivation = function(key_status, partner, index){
      if(key_status){
        partner.deployment_id = deployment._id;
        partner.tenant_id = currentUser.tenant_id;
        ThirdPartyService.activatePartner(partner).success(function(data){
          $scope.partners[index] = data;
        }).error(function(err){
          growl.error('Unable to activate.', {ttl: 3000});
        });
      }else{
        ThirdPartyService.deactivatePartner(deployment._id, partner.customer_key).success(function(data){
          delete $scope.partners[index].customer_key;
        }).error(function(err){
          growl.error('Unable to deactivate.', {ttl: 3000});
        });
      }
    };

    $scope.copy = function(id){
      var copyTextarea = document.getElementById(id);
      copyTextarea.select();
      document.execCommand('copy');
    };

    $scope.updateDeploymentPartner = function(partner, index){
      ThirdPartyService.updateDeploymentPartner(deployment._id, partner.customer_key, {partner_merchant_id: partner.partner_merchant_id})
        .success(function(data){
          $scope.partners[index] = data;
          growl.success('Successfully updated partner details.', {ttl: 3000});
        }).error(function(err){
          growl.error('Unable to update partner.', {ttl: 3000});
        });
      }


    //Section Mapping (Added by Kanishk | 20/9/2016)
    $scope.renderSectionMapping = function () {
      $rootScope.showLoaderOverlay = true;
      $scope.allSections = [];
      sectionResource.get({deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id'), requiredFields: {name: 1}}, function (sections) {
        $scope.allSections = sections;
        $rootScope.showLoaderOverlay = false;

        $scope.sectionsWithCategories = {
          all: []
        };
        //console.log('categories', $scope.categories);
        _.forEach($scope.categories, function (category) {
          if(_.has(category, 'section') && category.section) {
            console.log(category.section)
            _.forEach($scope.allSections, function (section) {
              if (!$scope.sectionsWithCategories[section._id])
                $scope.sectionsWithCategories[section._id] = [];
              if (category.section._id == section._id)
                $scope.sectionsWithCategories[section._id].push(category);
            });
          }
          else {
            $scope.sectionsWithCategories.all.push(category);
          }
        });

      }, function (err) {
        growl.error("Error fetching sections!", {ttl: 3000});
        $rootScope.showLoaderOverlay = false;
      });
      $scope.currentSectionsPage = 1;
      $scope.sectionMapForm = {
        isUpdate: false,
        updateIndex: -1,
        deployment_id: localStorageService.get('deployment_id'),
        tenant_id: localStorageService.get('tenant_id'),
      };

      $scope.getContextMenuForSection = function(sect) {

        var menuOptions = [];
        if(sect) {
          menuOptions.push(
            ['Unassign', function ($itemScope) {
              var section = $itemScope.category.section;
              $itemScope.category.section = null;
              $rootScope.showLoaderOverlay = true;
              $itemScope.category.$updateSectionInCategoryAndItems(function (cat) {
                if (section) {
                  _.remove($scope.sectionsWithCategories[section._id], function (cat) {
                    return cat._id == $itemScope.category._id;
                  });
                }
                $scope.sectionsWithCategories.all.push(cat);
                $rootScope.showLoaderOverlay = false;
              }, function (err) {
                $rootScope.showLoaderOverlay = false;
                growl.error(err, {ttl: 3000});
              });
            }]
          );
        }
        _.forEach($scope.allSections, function (section){
          var push = true;
          if(sect){
            if(sect._id == section._id) {
              push = false;
            }
          }
          if(push){
            menuOptions.push([
              section.name, function ($itemScope) {
                var sect = $itemScope.category.section;
                $itemScope.category.section = section;
                $rootScope.showLoaderOverlay = true;
                $itemScope.category.$updateSectionInCategoryAndItems(function (cat) {
                  if (!$scope.sectionsWithCategories[section._id])
                    $scope.sectionsWithCategories[section._id] = [];
                  $scope.sectionsWithCategories[section._id].push(cat);
                  if (sect) {
                    _.remove($scope.sectionsWithCategories[sect._id], function (cat) {
                      return cat._id == $itemScope.category._id;
                    });
                  } else {
                    _.remove($scope.sectionsWithCategories.all, function (cat) {
                      return cat._id == $itemScope.category._id;
                    });
                  }
                  $rootScope.showLoaderOverlay = false;
                }, function (err) {
                  $rootScope.showLoaderOverlay = false;
                  growl.error(err, {ttl: 3000});
                });
              }
            ]);
          }
        });
        return menuOptions;
      }

      $scope.createOrUpdateSection = function (form){

        if(form.$valid){
          if(!$scope.sectionMapForm.isUpdate){
            var section = new sectionResource($scope.sectionMapForm);
            section.$saveData(function (sec) {
              growl.success("Section created!",{ttl: 3000});
              $scope.allSections.push(sec);
              $scope.sectionMapForm = {
                isUpdate: false,
                updateIndex: -1,
                deployment_id: localStorageService.get('deployment_id'),
                tenant_id: localStorageService.get('tenant_id'),
              };
            }, function (err) {
              growl.error("Some error occured. Please try again!", {ttl: 3000});
            });
          } else {
            var section = new sectionResource($scope.sectionMapForm);
            section.$update(function (upSec) {
              $scope.allSections[$scope.sectionMapForm.updateIndex] = upSec;
              $scope.sectionMapForm = {
                isUpdate: false,
                updateIndex: -1,
                deployment_id: localStorageService.get('deployment_id'),
                tenant_id: localStorageService.get('tenant_id'),
              };
              growl.success("Section updated successfully.", {ttl: 3000});
            }, function (err){
              console.log(err);
              growl.error("Some error occured while updating, Please try again!.", {ttl: 3000});
            });
          }
        }
      }

      $scope.prepareSectionForUpdate = function (section, index) {
        console.log('prepping section update');
        $scope.sectionMapForm._id = section._id;
        $scope.sectionMapForm.name = section.name;
        $scope.sectionMapForm.isUpdate = true;
        $scope.sectionMapForm.updateIndex = index;
      };

      $scope.cancelUpdate = function () {
        $scope.sectionMapForm = {
          isUpdate: false,
          updateIndex: -1,
          deployment_id: localStorageService.get('deployment_id'),
          tenant_id: localStorageService.get('tenant_id'),
        };
      }

      $scope.deleteSection = function (section, index) {

        if(confirm("Are you sure you want to delete this section?")){
          section.$delete(function (res) {
            console.log(res);
            growl.success("Section deleted successfully", {ttl: 3000});
            $scope.allSections.splice(index, 1);
          }, function (err){
            console.log(err);
            growl.error(err.data.message, {ttl: 3000});
          });
        }
      };
    }
//end

//Day Session Breakup (Added by Kanishk | 21/9/2016)
    $scope.renderDaySessionBreakup = function (){

      $scope.daySessionBreakup = {
        isUpdate: false,
        updateIndex: -1,
        allSessions: [],
        daySessionForm: {
          sessionName: "",
          fromTime: new Date('1/1/1999'),
          toTime: new Date('1/1/1999'),
          deployment_id: localStorageService.get('deployment_id'),
          tenant_id: localStorageService.get('tenant_id')
        }
      };

      dayBreakup.get({deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id'), projection: {sessionName: 1, fromTime: 1, toTime: 1}}, function (sessions){
        $scope.daySessionBreakup.allSessions = sessions;
      });

      $scope.daySessionBreakup.createOrUpdateSession = function (sessionForm) {

        if(sessionForm.$valid){
          var isValid = true;
          _.forEach($scope.daySessionBreakup.allSessions, function (session) {

            if($scope.daySessionBreakup.isUpdate){
              console.log(session._id, $scope.daySessionBreakup.daySessionForm._id)
              if(session._id != $scope.daySessionBreakup.daySessionForm._id){
                console.log('in');
                var newFromTimeHour = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getHours();
                var newFromTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getMinutes();
                var newToTimeHours = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getHours();
                var newToTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getMinutes();
                var sessionFromTimeHours = (new Date(session.fromTime)).getHours();
                var sessionFromTimeMinutes = (new Date(session.fromTime)).getMinutes();
                var sessionToTimeHours = (new Date(session.toTime)).getHours();
                var sessionToTimeMinutes = (new Date(session.toTime)).getMinutes();

                //console.log(newFromTimeHour)
                //console.log(newFromTimeMinutes)
                //console.log(newToTimeHours)
                //console.log(newToTimeMinutes)
                //console.log(sessionFromTimeHours)
                //console.log(sessionFromTimeMinutes)
                //console.log(sessionToTimeHours)
                //console.log(sessionToTimeMinutes)
                //console.log((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes));

                if((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes)){
                  isValid = false;
                }
              }
            } else {
              var newFromTimeHour = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getHours();
              var newFromTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.fromTime)).getMinutes();
              var newToTimeHours = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getHours();
              var newToTimeMinutes = (new Date($scope.daySessionBreakup.daySessionForm.toTime)).getMinutes();
              var sessionFromTimeHours = (new Date(session.fromTime)).getHours();
              var sessionFromTimeMinutes = (new Date(session.fromTime)).getMinutes();
              var sessionToTimeHours = (new Date(session.toTime)).getHours();
              var sessionToTimeMinutes = (new Date(session.toTime)).getMinutes();

              //console.log(newFromTimeHour)
              //console.log(newFromTimeMinutes)
              //console.log(newToTimeHours)
              //console.log(newToTimeMinutes)
              //console.log(sessionFromTimeHours)
              //console.log(sessionFromTimeMinutes)
              //console.log(sessionToTimeHours)
              //console.log(sessionToTimeMinutes)
              //console.log((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes));

              if((newFromTimeHour == sessionFromTimeHours && newFromTimeMinutes == sessionFromTimeMinutes) || (newToTimeHours == sessionToTimeHours && newToTimeMinutes == sessionToTimeMinutes)){
                isValid = false;
              }
            }
          });
          if(isValid) {
            if (!$scope.daySessionBreakup.isUpdate) {
              var session = new dayBreakup($scope.daySessionBreakup.daySessionForm);
              session.$saveData(function (sess) {
                $scope.daySessionBreakup.allSessions.push(sess);
                $scope.daySessionBreakup.daySessionForm = {

                  sessionName: "",
                  fromTime: new Date('1/1/1999'),
                  toTime: new Date('1/1/1999'),
                  deployment_id: localStorageService.get('deployment_id'),
                  tenant_id: localStorageService.get('tenant_id')
                };
                growl.success("Successfully created session", {ttl: 3000});
              }, function (err) {
                console.log(err);
                growl.error("Some error occured. Please try again!", {ttl: 3000});
              });
            }
            else {
              var session = new dayBreakup($scope.daySessionBreakup.daySessionForm);
              session.$update(function (sess) {
                $scope.daySessionBreakup.allSessions[$scope.daySessionBreakup.updateIndex] = sess;
                $scope.daySessionBreakup.isUpdate = false;
                $scope.daySessionBreakup.updateIndex = -1;
                $scope.daySessionBreakup.daySessionForm = {
                  sessionName: "",
                  fromTime: new Date('1/1/1999'),
                  toTime: new Date('1/1/1999'),
                  deployment_id: localStorageService.get('deployment_id'),
                  tenant_id: localStorageService.get('tenant_id')
                };
                growl.success("Successfully updated session", {ttl: 3000});
              }, function (err) {
                console.log(err);
                growl.error("Some error occured, Please try again!", {ttl: 3000});
              });
            }
          } else {
            growl.error("Session time conincides with existing sessions", {ttl: 3000});
          }
        }
      };

      $scope.daySessionBreakup.prepareSessionForUpdate = function (session, index) {
        $scope.daySessionBreakup.daySessionForm = session;
        $scope.daySessionBreakup.isUpdate = true;
        $scope.daySessionBreakup.updateIndex = -1;
      };

      $scope.daySessionBreakup.deleteSession = function (session, index) {
        if(confirm("Are you sure you want to delete this session")) {
          session.$delete(function () {
            $scope.daySessionBreakup.allSessions.splice(index, 1);
            growl.success("Session deleted successfully");
          }, function (err) {
            console.log(err);
          });
        }
      };

      $scope.daySessionBreakup.cancelUpdate = function() {
        $scope.daySessionBreakup.isUpdate = false;
        $scope.daySessionBreakup.updateIndex = -1;
        $scope.daySessionBreakup.daySessionForm = {
          sessionName: "",
          fromTime: new Date('1/1/1999'),
          toTime: new Date('1/1/1999'),
          deployment_id: localStorageService.get('deployment_id'),
          tenant_id: localStorageService.get('tenant_id')
        }
      };
    }
//end
$scope.headForm = {
     headInEdit: false
   };
 $scope.submitHeadForm = function (form) {

      $scope.submitted = true;

      if (form.$valid && !$scope.headForm.headInEdit) {
        $scope.headForm.tenant_id = currentUser.tenant_id;
        $scope.headForm.deployment_id = $scope.deployment._id;

        ComplementaryHead.save({}, $scope.headForm, function (head) {
          $scope.heads.push(head);
          /*$scope.tabCategories[$scope.tabs.length] = angular.copy($scope.categories);*/
          growl.success(head.name + ' successfully created!', {ttl: 3000});
        }, function (err) {
          growl.error('Error Saving Head: Something went wrong!', {ttl: 3000});
        });
        $scope.headForm = {};
        form.$setPristine();
      } else {
        var headPayload = cleanHeadForm($scope.headForm);
        console.log($scope.headForm)
        ComplementaryHead.update({_id:headPayload._id}, headPayload, function (head) {
          $scope.heads[Utils.arrayObjectIndexOf($scope.heads, "_id", head._id)] = head;
        });
        $scope.headForm = {};
        form.$setPristine();
        //alert("Update");
      }
    };

    // Remove Tabs
    $scope.removeHead = function (head) {
      ComplementaryHead.remove({id: head._id}, function () {
        $scope.heads.splice(Utils.arrayObjectIndexOf($scope.heads, "_id", head._id), 1);
      });
    };
    //Clear Tabs Form
    $scope.clearHeadForm = function () {
      $scope.headForm = {};
      $scope.headForm.headInEdit = false;
    };
    //Edit Tabs
    $scope.editHead = function (head) {
      $scope.headForm = angular.copy(head);
      $scope.headForm.headInEdit = true;
    };


    $scope.extraCharge={};
    $scope.charges=[];
    //$scope.tabs=angular.copy($scope.tabs);
    $scope.extraCharge.type="fixed";
    $scope.renderCharges=function(){
    $rootScope.showLoaderOverlay=true;
    $http.get('/api/charges?deployment_id='+$scope.deployment._id).success(function(data){
      console.log(data);
      $scope.charges=data;
      $rootScope.showLoaderOverlay=false;
    }).error(function(data){
      $scope.chargesNotFound=true;
      $rootScope.showLoaderOverlay=false;
    });
  }

    $scope.createExtraCharge=function(charge){
      console.log($scope.tabs,$scope.extraCharge);
      if(_.filter($scope.charges,{name:$scope.extraCharge.name}).length>0) {
        growl.error("This charge already exists", {ttl: 3000});
        return false;
      }
      var charge=angular.copy($scope.extraCharge);
      charge.value=(parseFloat($scope.extraCharge.value)).toFixed(2);
      console.log(charge.value);
      charge.deployment_id=$scope.deployment._id;
      charge.tenant_id=localStorageService.get('tenant_id');
      charge.tabs=[];
      $http.post('/api/charges',charge).success(function(charge){
        $scope.charges.push(charge);
        growl.success("Charge Successfully created",{ttl:3000});
        $scope.extraCharge={};
        $scope.extraCharge.type='fixed';
      }).error(function(err){
        growl.error("Could not create charge",{ttl:3000});
      })
    }

    $scope.addChargeToTab=function(event,chargeIndex,tabIndex){
      console.log(chargeIndex,tabIndex);
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      var tab={_id:$scope.tabs[tabIndex]._id,tabName:$scope.tabs[tabIndex].tabName,tabType:$scope.tabs[tabIndex].tabType}
      //var charge={name:$scope.charges[chargeIndex].name,amount:$scope.charges[chargeIndex].amount,_id:$scope.charges[chargeIndex]._id}
      if (action == 'add'){
        if(!$scope.charges[chargeIndex].tabs)
          $scope.charges[chargeIndex].tabs=[];
         if(_.filter($scope.charges[chargeIndex].tabs,{_id:tab._id}).length==0)
          $scope.charges[chargeIndex].tabs.push(tab);
      }
      if (action == 'remove'){
        var index=-1;
        if($scope.charges[chargeIndex].tabs) {
          for (var i = 0; i < $scope.charges[chargeIndex].tabs.length; i++) {
            if ($scope.charges[chargeIndex].tabs[i]._id == tab._id) {
              console.log(tab._id,tab.tabName,$scope.charges[chargeIndex].tabs[i]._id);
              index = i;
              break;
            }
          }
          if (index != -1) $scope.charges[chargeIndex].tabs.splice(index, 1);
          else growl.error("Charge not found in tab", {ttl: 3000});
        }
      }
      console.log($scope.charges);
    }

    $scope.assignChargesToTabNew=function(index){
      var body={deployment_id:$scope.deployment._id,tabs:$scope.charges[index].tabs,_id:$scope.charges[index]._id};
      $http.post('/api/charges/applyCharges',body).success(function(data){
       growl.success("Charges applied on Tab",{ttl:3000});
     }).error(function(err){
       growl.error("Could not apply charges on table",{ttl:3000});
     });
    }

   $scope.assignChargesToTab=function(index){
     //var charges= _.uniq($scope.tabs[index].charges,_id);
     var tab={_id:$scope.tabs[index]._id,tabName:$scope.tabs[index].tabName,tabType:$scope.tabs[index].tabType};
     var list=[];
     _.forEach($scope.charges,function(charge){
      if(_.filter(charge.tabs,{_id:tab._id}).length>0)
        list.push(charge._id);
     });
     var body={list:list,tab:tab,deployment_id:$scope.deployment._id};
     console.log(body);

     $http.post('/api/charges/assignCharges',body).success(function(data){
       growl.success("Charges applied on Tab",{ttl:3000});
     }).error(function(err){
       growl.error("Could not apply charges on table",{ttl:3000});
     })
   }

   $scope.isChargeAppliedOnTab=function(chargeIndex,tabIndex) {
     var tabId = $scope.tabs[tabIndex]._id;
     //console.log($scope.charges[chargeIndex].name,$scope.tabs[tabIndex].tabName,$scope.tabs[tabIndex].charges && $scope.tabs[tabIndex].charges.length>0  && _.filter($scope.tabs[tabIndex].charges,{_id:chargeId}).length>0);
     return ($scope.charges[chargeIndex] && _.filter($scope.charges[chargeIndex].tabs,{_id:tabId}).length>0);
   }

   $scope.openEditChargeModal=function(index){
     $scope.extraCharge=angular.copy($scope.charges[index]);
     $scope.extraCharge.value=Number($scope.charges[index].value);
      $scope.editChargeIndex=index;
     $scope.isEditCharge=true;
    }

    $scope.cancelEditCharge=function(){
      $scope.extraCharge={type:'fixed'};
      $scope.isEditCharge=false;
    }


     $scope.editCharge=function(index){
      var val=parseFloat($scope.extraCharge.value).toFixed(2);
      var charge=angular.copy($scope.extraCharge);
      charge.value=val;

      if(!$scope.extraCharge.type || $scope.extraCharge.type==''){
        growl.error("Please choose charge type",{ttl:3000});
        return false;
      }

        $http.post('/api/charges/update',$scope.extraCharge).success(function(data){
        $scope.charges[index]=charge;
        $scope.extraCharge={};
        $scope.extraCharge.type='fixed';
        $scope.isEditCharge=false;
         growl.success("successfully updated charge",{ttl:3000});
      }).error(function(err){
        growl.error("Error while updating charge.Please try again",{ttl:3000});
      });
    }



   $scope.deleteCharge=function(index){
    var charge=$scope.charges[index];
     charge.isDeleted=true;
     $http.post('/api/charges/delete',charge).success(function(data){
        $scope.charges.splice(index,1);
     }).error(function(err){
       growl.error("Could not delete charge.Please try again",{ttl:3000});
     })
   }



  //----for extra charges---//

  //--for item comments----//
    $scope.comment={};
    $scope.comments=[];
    var changedItems=[];
    var copyItems=[];
    var itemPerPage=20;
    $scope.commentItemPageNo=1;

   $scope.renderComments=function(){
    $rootScope.showLoaderOverlay=true;
    $http.get('/api/comment?deployment_id='+$scope.deployment._id).success(function(data){
    _.forEach(data,function(d){
      delete d.deployment_id; delete d.tenant_id;
    })
     $scope.comments=data;
       copyItems=[];
       var list=[];
     _.forEach($scope.items,function(i){
      list.push({_id:i._id,name:i.name,comments:i.comments});
     });
      copyItems= list.sort(function(a,b){
       return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
       });
      console.log(copyItems);
     $scope.copyItems=[];
     $rootScope.showLoaderOverlay=false;
     $scope.renderSelectedItems(1);
     console.log($scope.copyItems);
  }).error(function(err){
    $rootScope.showLoaderOverlay=false;
    growl.error("Could not fetch comments",{ttl:3000});
    $scope.commentsNotFound=true;
  })
}

  $scope.createComment=function(){
    if(_.filter($scope.comments,{name:$scope.comment.name}).length>0){
      growl.error("This comment already exists",{ttl:3000});
      return false;
    }
    if($scope.comment.name.length>50){
      growl.error("Please shorten your comment",{ttl:3000});
      return false;
    }
    var comment=angular.copy($scope.comment);
    comment.deployment_id=$scope.deployment._id;
    comment.tenant_id=localStorageService.get('tenant_id');
    $http.post('/api/comment',comment).success(function(comment){
      growl.success("Comment created",{ttl:3000});
      delete comment.deployment_id; delete comment.tenant_id;
      $scope.comments.push(comment);
      $scope.comment={};
    }).error(function(err){
      growl.error("Comment not created",{ttl:3000});
    })
  }

  $scope.assignCommentsToItem=function(){

    var data={deployment_id:$scope.deployment._id,list:changedItems};
    $http.post('/api/comment/assignComment',data).success(function(data){
      growl.success("Comments successully mapped to items",{ttl:3000});
      changedItems=[];
      _.forEach(copyItems,function(c){c.class=''});
      _.forEach($scope.copyItems,function(c){c.class=''});
    }).error(function(err){
      growl.error("Something went wrong",{ttl:3000});
    })
  }

  $scope.openEditCommentModal=function(index){
     $scope.comment=angular.copy($scope.comments[index]);
     $scope.editCommentIndex=index;
      $scope.isEditComment=true;
    }

      $scope.editComment=function(index){
        var data=angular.copy($scope.comment);
        data.deployment_id=$scope.deployment._id;
        data.tenant_id=localStorageService.get('tenant_id');
        $http.post('/api/comment/editComment',data).success(function(data){
          editCommentInItems(index,$scope.comment);
          $scope.comment={};
          $scope.isEditComment=false;
        }).error(function(err){
          growl.error("Comment not updated.Please try again",{ttl:3000});
        })
      }
       $scope.cancelEditComment=function(){
              $scope.comment={};
              $scope.isEditComment=false;
            }

      function editCommentInItems(index,comment){
           _.forEach(copyItems,function(it){
        _.forEach(it.comments,function(com){
          if(com && com._id==comment._id)
            com.name=comment.name;
        });
      });
      console.log(copyItems);
      $scope.comments[index]=comment;
      growl.success("Comment successfully updated",{ttl:3000});
     }

     $scope.deleteComment=function(index){
      var comment=angular.copy($scope.comments[index]);
      comment.deployment_id=$scope.deployment._id;
      comment.tenant_id=localStorageService.get('tenant_id');
      comment.isDeleted=true;
      $http.post('/api/comment/deleteComment',comment).success(function(data){
       deleteCommentInItems(comment,index);
      }).error(function(err){
        growl.error("Could not delete charge.Please try again",{ttl:3000});
      })
    }

      function deleteCommentInItems(comment,index){
      _.forEach(copyItems,function(item){
        var list=[];
        _.forEach(item.comments,function(ch){
          if(ch && ch._id!=comment._id)
            list.push(ch);
        });
        item.comments=list;
       });
         console.log(copyItems);
       $scope.comments.splice(index,1);
       growl.success("Comment successfully deleted",{ttl:3000});
     }

     $scope.addCommentToItem=function(item,itemIndex){
      console.log(item,itemIndex);
      var index=-1;
      for(var i=0;i<changedItems.length;i++){
        if(changedItems[i]._id==item._id){
          index=i; break;
        }
      }
      if(index!=-1) changedItems[index]={_id:item._id,comments:item.comments};
      else changedItems.push({_id:item._id,comments:item.comments});
      $scope.copyItems[itemIndex].class='active';

      console.log(changedItems);

      var itemIndex=-1;
      for(var i=0;i<copyItems;i++){
        if(copyItems[i]._id==item._id){
          itemIndex=i;
          break;
        }
      }
      if(itemIndex!=-1) {copyItems[itemIndex].comments=item.comments; copyItems[itemIndex].class='active';}

     }

     $scope.getCommentItemClass=function(item){
      if(item.class)
        return 'info';
      else
        return '';
     }

     $scope.renderSelectedItems=function(pageNo){
        var start=(pageNo-1)*itemPerPage;
        var end=start+itemPerPage;
        var copyStart=0;
        $scope.copyItems=[];
        console.log(start,end,pageNo);
         for(var i=start;i<end;i++){

          if(copyItems[i]){
            $scope.copyItems[copyStart]=copyItems[i];
            copyStart++;
          }
         }
     }

     $scope.filterComments=function(filter){
        var result=[];
        _.forEach(copyItems,function(i){
            if(i.name.toLowerCase().indexOf(filter.toLowerCase())!==-1)
              result.push(i);
        })
        console.log($scope.commentItemPageNo);
        console.log(result);
        if(filter!='')
        $scope.copyItems=result.splice(0,itemPerPage);
        else
        $scope.renderSelectedItems($scope.commentItemPageNo);
     }

     // map online-order sources to tabs

      $scope.renderOnlineOrderPartners=function(){


        $rootScope.showLoaderOverlay=true;
        $scope.copyTabs=angular.copy($scope.tabs);
        _.forEach($scope.copyTabs,function(ct){
          ct.partners=[];
        })
        $scope.onlineOrderPartners=[
            {partnerName:'POS',sourceId:'1'},
            {partnerName:'NomNom',sourceId:'14'},
            {partnerName:'Swiggy',sourceId:'10'},
            {partnerName:'Foodpanda',sourceId:'12'},
            {partnerName:'Justdial',sourceId:'13'},
            {partnerName:'UrbanPiper',sourceId:'2'}
        ]
        //for(var i=0;i<2;i++)
         // $scope.onlineOrderPartners.push({partnerName:'abc',sourceId:15});
        ThirdPartyService.getPartnersByDeployment($scope.deployment._id).success(function(data){
          _.forEach(data,function(d){
               if(d.class.indexOf('online_order')!=-1)
               $scope.onlineOrderPartners.push({partnerName:d.partner_name,sourceId:d.client_id});
              })
          console.log($scope.onlineOrderPartners);
           getTabPartners();
         }).error(function(err){
           $rootScope.showLoaderOverlay=false;
           $scope.onlineOrderPartners=[];
          growl.error("Could not get Partners",{ttl:3000});

        })
      }

      function getTabPartners(){
        $http.get('/api/tabs/tabPartners?deployment_id='+$scope.deployment._id).success(function(data){
          _.forEach(data,function(d){
            _.forEach($scope.copyTabs,function(ct){
              if(ct._id==d.tab_id)
                ct.partners=d.partners;
            })
          })
          console.log($scope.copyTabs);
          $rootScope.showLoaderOverlay=false;
        }).error(function(err){
          growl.error("Could not get tab partners",{ttl:3000});
          $scope.onlineOrderPartners=[];
          $rootScope.showLoaderOverlay=false;
        })
      }



     $scope.isPartnerAssignmentDisabled=function(tab,partner){
        var index=-1;
        var partnerFound=false; var tabId;
        for(var i=0;i<$scope.copyTabs.length;i++){
          if(_.filter($scope.copyTabs[i].partners,{sourceId:partner.sourceId}).length>0){
            partnerFound=true;
            tabId=$scope.copyTabs[i]._id;
            break;
          }
        }
        if(partnerFound && tabId!=tab._id)
          return true;
        else
          return false;
      }

      $scope.isPartnerAssignedToTab=function(tabIndex,partner){
       return ($scope.copyTabs[tabIndex].partners && _.filter($scope.copyTabs[tabIndex].partners,{sourceId:partner.sourceId}).length>0);
      }

      $scope.assignPartnerToTab=function(event,tabIndex,partner){
      var checkbox = event.target;
      var action = (checkbox.checked ? 'add' : 'remove');

      if (action == 'add'){
       if(!$scope.copyTabs[tabIndex].partners)
        $scope.copyTabs[tabIndex].partners=[];
        $scope.copyTabs[tabIndex].partners.push({partnerName:partner.partnerName,sourceId:partner.sourceId});
      }

      if (action == 'remove'){
        var index=-1;
        for (var i=0;i<$scope.copyTabs[tabIndex].partners.length;i++){
           if($scope.copyTabs[tabIndex].partners[i].sourceId==partner.sourceId ){
            index=i;
            break;
           }
        }
        if(index==-1) growl.error("Partner not found",{ttl:3000});
        else $scope.copyTabs[tabIndex].partners.splice(index,1);
      }
       console.log($scope.copyTabs);
      }

      $scope.mapPartnerToTab=function(tabIndex){
        var data={deployment_id:$scope.deployment._id,tenant_id:localStorageService.get('tenant_id'),
                  tab_id:$scope.copyTabs[tabIndex]._id,partners:$scope.copyTabs[tabIndex].partners,
                  tabName:$scope.copyTabs[tabIndex].tabName,tabType:$scope.copyTabs[tabIndex].tabType};

        $http.post('/api/tabs/deliveryPartnerMap/',data).success(function(doc){
          growl.success("Mapping done successfully",{ttl:3000});
        }).error(function(){
          growl.error("Error in mapping",{ttl:3000});
        })
      }


      $scope.colorCodes=[
         {colorName:'Red',colorCode:'ff0000'},
         {colorName:'Orange',colorCode:'ffa500'},
         {colorName:'Yellow',colorCode:'ffff00'},
         {colorName:'Green',colorCode:'00ff00'},
         {colorName:'Turquoise',colorCode:'40e0d0'},
         {colorName:'Blue',colorCode:'0000ff'},
         {colorName:'Purple',colorCode:'800080'},
         {colorName:'Pink',colorCode:'ffc0cb'},
         {colorName:'Magenta',colorCode:'ff00ff'},
         {colorName:'Sea green',colorCode:'2e8b57'},
         {colorName:'Royal Blue',colorCode:'4169e1'},
         {colorName:'Dark Orange',colorCode:'ff8c00'},
         {colorName:'Sky blue',colorCode:'87ceeb'},
         {colorName:'blue violet',colorCode:'8a2be2'}
       ];

       $scope.kdsColorCodes=[];

       function getColorCodeByName(colorName){
        var index=-1;
        for(var i=0;i<$scope.colorCodes.length;i++){
          if($scope.colorCodes[i].colorName==colorName){
            index=i;
            break;
          }
        }
        if(index!=-1) return $scope.colorCodes[i].colorCode;
        else return null;
       }

       $scope.kdsCode={colorName:$scope.colorCodes[0].colorName};

       $scope.createColorCode=function(){
        //console.log($scope.kdsCode);
        if(_.filter($scope.kdsColorCodes,{lapseTime:$scope.kdsCode.lapseTime}).length>0){
          growl.error("Color code already saved for this lapse time",{ttl:3000});
          return false;
        }

        $scope.kdsCode.colorCode=getColorCodeByName($scope.kdsCode.colorName);
        if($scope.kdsCode.colorCode==null){
          growl.error("Please select a valid color",{ttl:3000});
          return false;
        }

        $scope.kdsCode.deployment_id=$scope.deployment._id;
        $scope.kdsCode.tenant_id=localStorageService.get('tenant_id');

        $http.post('/api/kdsSettings/colorCode',$scope.kdsCode).success(function(data){
           $scope.kdsColorCodes.push(data);
           console.log(data);
           growl.success("Color Code saved successfully",{ttl:3000});
            $scope.kdsCode={colorName:$scope.colorCodes[0].colorName};
        }).error(function(err){
          console.log("Could not create color code",{ttl:3000});
        })
      }


      $scope.getColorCodes=function(){
        $rootScope.showLoaderOverlay=true;
        $http.get('/api/kdsSettings/colorCode?deployment_id='+$scope.deployment._id).success(function(data){
          $scope.kdsColorCodes=data;
          console.log($scope.kdsColorCodes);
          $rootScope.showLoaderOverlay=false;
        }).error(function(err){
          growl.error("Could not fetch color codes",{ttl:3000});
          $rootScope.showLoaderOverlay=false;
        })
      }

      $scope.editColorCode=function(index){
          $scope.isEditColorCode=true;
          $scope.kdsCode=angular.copy($scope.kdsColorCodes[index]);
      }

      $scope.saveEditedColorCode=function(){
         $scope.kdsCode.colorCode=getColorCodeByName($scope.kdsCode.colorName);

        $http.put('/api/kdsSettings/colorCode',$scope.kdsCode).success(function(data){
          growl.success("Code successfully updated",{ttl:3000});
          _.forEach($scope.kdsColorCodes,function(d){
            if(d._id==$scope.kdsCode._id){
              d.lapseTime=$scope.kdsCode.lapseTime;
              d.colorName=$scope.kdsCode.colorName;
              d.colorCode=$scope.kdsCode.colorCode;
              console.log("code matched",d);
            }
          });

          $scope.kdsCode={colorName:$scope.colorCodes[0].colorName};
          $scope.isEditColorCode=false;
        }).error(function(err){
          console.log("error in updating code",{tl:3000});
        })
      }

      $scope.cancelEditingColorCode=function(){
         $scope.kdsCode={colorName:$scope.colorCodes[0].colorName};
        $scope.isEditColorCode=false;
      }

      $scope.deleteColorCode=function(index){
        var code=$scope.kdsColorCodes[index]
        $http.delete('/api/kdsSettings/colorCode?deployment_id='+$scope.deployment._id+'&_id='+code._id).success(function(data){
            growl.success("Code successfully deleted",{ttl:3000});
            $scope.kdsColorCodes.splice(index,1);
        }).error(function(err){
          growl.error("Could not delete code",{ttl:3000});
        })
      }


   // --Item->Short Names---//

   var shortNameList=[];

   $scope.initShortItemTab=function(){
    $rootScope.showLoaderOverlay=true;
    $scope.filterComment='';
     copyItems=[];
     var list=[];
     _.forEach($scope.items,function(i){
      list.push({_id:i._id,name:i.name,shortName:i.shortName});
     });

      copyItems= list.sort(function(a,b){
       return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
       });
    $scope.renderSelectedItems(1);
    //console.log($scope.copyItems);
    $rootScope.showLoaderOverlay=false;
   }

   $scope.addSelectionToShortItemList=function(item){
    item.class='info';
    shortNameList.push({_id:item._id,shortName:item.shortName});
   }

   $scope.assignShortNamesToItems=function(){
     var data={deployment_id:$scope.deployment._id,list:shortNameList};
       $http.post('/api/items/assignShortName',data).success(function(data){
         growl.success("Names successfully assigned",{ttl:3000});
         _.forEach($scope.items,function(i){
          _.forEach($scope.copyItems,function(c){
             delete c.class;
            if(c._id==i._id)
              i.shortName=c.shortName;
          })
         })
       }).error(function(err){
        growl.error("Names not assigned",{ttl:3000});
       })
     }

       //Nitin Code goes here
  $http.get('/api/onlineOrderSettings/'+ deployment._id).success(function(onlineOrderSettings)
  {
    console.log('onlineOrderSettings',onlineOrderSettings);
     $scope.onlineOrderSettings=onlineOrderSettings;
     $scope.onlineOrderSettings.delivery_from_time=new Date(onlineOrderSettings.delivery_from_time);
     $scope.onlineOrderSettings.delivery_to_time=new Date(onlineOrderSettings.delivery_to_time);
  }).error(function(err)
  {
    $scope.onlineOrderSettings={};
  $scope.onlineOrderSettings.delivery_from_time=new Date();
  $scope.onlineOrderSettings.delivery_to_time=new Date();
  })

    $scope.updateonlineOrderSettings=function(form)
    {
      if(form.$valid)
      {
        if( $scope.onlineOrderSettings.delivery_from_time >= $scope.onlineOrderSettings.delivery_to_time)
        {
          console.log('warning')
         growl.error("From time cannot be smaller or equal than To time", {ttl: 3000});
        }
        else
        {
          $scope.onlineOrderSettings.tenant_id = localStorageService.get('tenant_id');
          $scope.onlineOrderSettings.deploymentId = deployment._id;
          $http.post('/api/onlineOrderSettings',$scope.onlineOrderSettings).success(function(s)
          {
            growl.success("Updated Successfully",{ttl: 3000});
          })
        }
      }
    }
  //Nitin Code Ends here
 //--btc --//
 $scope.btc={}
 $scope.btcList=[];

 $scope.renderBtc=function(){

  $rootScope.showLoaderOverlay=true;
  $http.get('/api/btc?deployment_id='+$scope.deployment._id).success(function(data){
    $scope.btcList=data;
    $rootScope.showLoaderOverlay=false;
  }).error(function(err){
    growl.error("Could not get comapnies",{ttl:3000});
    $rootScope.showLoaderOverlay=false;
    $scope.btcNotFound=true;
  })
 }

 $scope.createBtc=function(){
  var ex = new RegExp('^[0-9]+$');
  if ($scope.btc.contactPerson && $scope.btc.contactPerson.mobile && ((!((ex.test($scope.btc.contactPerson.mobile))) || $scope.btc.contactPerson.mobile.length != 10))) {
      growl.error("Please enter a valid Mobile Number", {ttl: 3000});
      return false;
    }

   if($scope.btc.contactPerson && $scope.btc.contactPerson.email && $scope.btc.contactPerson.email.indexOf('.')==-1){
    growl.error("Please enter a valid email id",{ttl:3000});
    return false;
   }

  if(_.filter($scope.btcList,{companyName:$scope.btc.companyName}).length>0){
    growl.error("A company with this name already exists",{ttl:3000});
    return false;
  }

  $scope.btc.deployment_id=$scope.deployment._id;
  $scope.btc.tenant_id=localStorageService.get('tenant_id');
  $scope.btc.createdBy=Auth.getCurrentUser().username;

  console.log($scope.btc);

  $http.post('/api/btc',$scope.btc).success(function(doc){
    $scope.btcList.push(doc);
    growl.success("Company Successfully created",{ttl:3000});
    $scope.btc={};
  }).error(function(err){
    growl.error("Could not create company",{ttl:3000});
  })
 }

 $scope.deleteBtc=function(index){
   var obj=$scope.btcList[index];
   $http.delete('/api/btc?deployment_id='+$scope.deployment._id+'&_id='+obj._id).success(function(data){
    $scope.btcList.splice(index,1);
    growl.success("Company Successfully deleted",{ttl:3000});
   }).error(function(err){"Could not delete company",{ttl:3000}});
 }

 $scope.editBtc=function(index){
   $scope.isBtcEdit=true;
   $scope.btc=angular.copy($scope.btcList[index]);
   $scope.btcCurrentIndex=index;
 }

 $scope.cancelEditBtc=function(){
  $scope.isBtcEdit=false;
  $scope.btc={};
  $scope.btcCurrentIndex=-1;
 }

 $scope.saveEditedBtc=function(){

  var index=-1;
  for(var i=0;i<$scope.btcList.length;i++){
    if($scope.btcList[i].companyName==$scope.btc.companyName && $scope.btcList[i]._id!=$scope.btc._id){
      index=i; break;
    }
  }

  if(index!=-1){
    growl.error("Another  Company with this name already exists",{ttl:3000});
    return false;
  }

   var ex = new RegExp('^[0-9]+$');
  if ($scope.btc.contactPerson && $scope.btc.contactPerson.mobile && ((!((ex.test($scope.btc.contactPerson.mobile))) || $scope.btc.contactPerson.mobile.length != 10))) {
      growl.error("Please enter a valid Mobile Number", {ttl: 3000});
      return false;
    }

   if($scope.btc.contactPerson && $scope.btc.contactPerson.email && $scope.btc.contactPerson.email.indexOf('.')==-1){
    growl.error("Please enter a valid email id",{ttl:3000});
    return false;
   }

  $scope.btc.lastModified={timestamp:new Date(),username:Auth.getCurrentUser().username};
  $http.post('/api/btc/update',$scope.btc).success(function(data){
    $scope.btcList[$scope.btcCurrentIndex]=angular.copy($scope.btc);
    $scope.btc={};
    $scope.isBtcEdit=false;
    growl.success("Company Successfully updated",{ttl:3000});
  }).error(function(err){
    growl.error("Could not update company details",{ttl:3000});
  })
 }
   //Nitin Code goes here
 $http.get('/api/onlineOrderSettings/'+deployment._id).success(function(onlineOrderSettings)
 {
   console.log('onlineOrderSettings',onlineOrderSettings);
   $scope.onlineOrderSettings=onlineOrderSettings;
   $scope.deliveryLocation.selected = onlineOrderSettings.deliveryLocations;
 }).error(function(err)
 {
   $scope.onlineOrderSettings={};
 $scope.onlineOrderSettings.delivery_from_time=new Date();
 $scope.onlineOrderSettings.delivery_to_time=new Date();
 })
   $scope.deliveryLocation = {selected:[]};
   $scope.updateonlineOrderSettings=function(form)
   {
     if(form.$valid)
     {
       if( $scope.onlineOrderSettings.delivery_from_time >= $scope.onlineOrderSettings.delivery_to_time)
       {
         console.log('warning')
        growl.error("From time cannot be smaller or equal than To time", {ttl: 3000});
       }
       else
       {
         $scope.onlineOrderSettings.tenant_id=currentUser.tenant_id;
         $scope.onlineOrderSettings.deploymentId=deployment._id;
         $scope.onlineOrderSettings.deliveryLocations = $scope.deliveryLocation.selected;
         $http.post('/api/onlineOrderSettings',$scope.onlineOrderSettings).success(function(s)
         {
           growl.success("Updated Successfully",{ttl: 3000});
         })
       }
     }
   }
 //Nitin Code Ends here

  //biometric


    $scope.assignShift = function(shift,shiftSelected){
      //console.log(shift,shiftSelected)
      if(shiftSelected){
        $scope.details.assignedShift.push(shift);
      }
      else{
        $scope.details.assignedShift.splice(Utils.arrayObjectIndexOf($scope.details.assignedShift, "_id", shift._id), 1);
      }
    }
    //biometric
    $scope.openTo = function($event) {
      console.log("$scope.openedTo");
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedToo = !$scope.openedToo;
    };
    $scope.openShiftPopup = function(shifts){
      var deployment_id = $scope.deployment_id;
      $modal.open({
        templateUrl: 'app/admin/deployments/_showShifts.html',
        controller: ['$scope', '$modalInstance','user', function ($scope, $modalInstance, user) {

          $scope.shiftForm = {
            shiftInEdit: false,
            shiftFrom:new Date(),
            shiftTo:new Date()
          };

          $scope.shifts=shifts;

          $scope.submitShiftForm = function (form) {

            $scope.submitted = true;
            console.log($scope.shiftForm)
            if (form.$valid && !$scope.shiftForm.shiftInEdit) {
              $scope.shiftForm.tenant_id = currentUser.tenant_id;
              $scope.shiftForm.deployment_id = deployment._id;

              ShiftTime.save({}, $scope.shiftForm, function (shift) {
                $scope.shifts.push(shift);
                /*$scope.tabCategories[$scope.tabs.length] = angular.copy($scope.categories);*/
                growl.success(shift.name + ' successfully created!', {ttl: 3000});
              }, function (err) {
                growl.error('Error Saving Head: Something went wrong!', {ttl: 3000});
              });
              $scope.shiftForm = {};
              form.$setPristine();
            } else {
              var shiftPayload = cleanShiftForm($scope.shiftForm);
              console.log(shiftPayload)
              ShiftTime.update({_id:shiftPayload._id}, shiftPayload, function (shift) {
                console.log(shift)
                $scope.shifts[Utils.arrayObjectIndexOf($scope.shifts, "_id", shift._id)] = shift;
              });
              $scope.shiftForm = {};
              form.$setPristine();
              //alert("Update");
            }
          };

          $scope.removeShift = function (shift) {
            ShiftTime.remove({id: shift._id}, function () {
              $scope.shifts.splice(Utils.arrayObjectIndexOf($scope.shifts, "_id", shift._id), 1);
            });
          };

          $scope.clearShiftForm = function () {
            $scope.shiftForm = {};
            $scope.shiftForm.shiftInEdit = false;
          };

          $scope.editShift = function (shift) {
            $scope.shiftForm = angular.copy(shift);
            $scope.shiftForm.shiftInEdit = true;
          };



          $scope.assignShift = function(shift,shiftSelected){
            //console.log(shift,shiftSelected)
            if(shiftSelected){
              $scope.details.assignedShift.push(shift);
            }
            else{
              $scope.details.assignedShift.splice(Utils.arrayObjectIndexOf($scope.details.assignedShift, "_id", shift._id), 1);
            }
          }
          $scope.close = function () {
            $modalInstance.dismiss('cancel');
          };
      }],
        size: '',
        resolve: {
          user: function () {
            return $scope.shifts;
          }
        }
      }).result.then(function (shifts) {
        $scope.shifts=shifts;
      });
    }

    function notifyOnlineOrderPartners(){
      var partners = $scope.partners.filter(function(partner){
        if(partner.class==undefined)
          return false;
        else
          return partner.class.indexOf('online_order')!=-1?true:false;
      }).map(function(partner){
        return {client_id: partner.client_id, customer_key: partner.customer_key};
      });
      if(partners.length>0){
        var Zomato_index = null;
        var Zomato = null;
        partners.forEach(function(partner, index){
          if(partner.client_id=='r1AROcuug') Zomato_index=index;
        });
        if(Zomato_index!=null){
          Zomato = angular.copy(partners[Zomato_index]);
          partners.splice(Zomato_index,1);
          ThirdPartyService.syncZomato({partners: [Zomato], event_name: "menu_sync", deployment_id: $scope.deployment._id})
          .success(function(data){
            console.log("Zomato updated.");
            growl.success(data.message,{ttl:3000});
          }).error(function(err){
            growl.error(data.message,{ttl:3000});
            console.log(err);
          });
        }
        ThirdPartyService.execute_event({partners: partners, event_name: "menu_sync", deployment_id: $scope.deployment._id})
        .success(function(data){
          console.log("Third party service updated.");
          growl.success("Online Order Partners are notified.",{ttl:3000});
        }).error(function(err){
          growl.error("Online Order Partners could not be synced.",{ttl:3000});
          console.log(err);
        });
      }
    }

    function hasDuplicatePasscode(user,allUsers,mode) {
      var flag=false;
      for(var i=0;i<allUsers.length;i++){
        if(allUsers[i].passcode==user.passcode && allUsers[i].username!=user.username)
            return true;
      }
      return false;
    }


    var ledgerMap=[];
    $scope.getLedgers=function(){
      $http.get('/api/ledgers?deployment_id='+$scope.deployment._id).then(function(result){
        var data=result.data;
        console.log(data);
        $scope.ledgerMap=[];
        mapLedgers(data.sections,'section',data.ledgerMap);
        mapLedgers(data.charges,'charges',data.ledgerMap);
        mapLedgers($scope.taxes,'taxes',data.ledgerMap);
        var payments=[{name:'cash',type:'payments'},
          {name:'creditcard',type:'payments'},
          {name:'debitcard',type:'payments'},
          {name:'coupon',type:'payments'},
          {name:'online',type:'payments'},
          {name:'other',type:'payments'}
        ];
        mapLedgers(payments,'payments',data.ledgerMap);
        var _ledger={};
         _ledger=_.find($scope.ledgerMap,{name:'sales'});
        $scope.ledgerMap.push({name:'sales',type:'sales',ledgerName:_ledger?_ledger.ledgerName:null});
        _ledger=_.find($scope.ledgerMap,{name:'roundoff'});
        $scope.ledgerMap.push({name:'roundoff',type:'roundoff',ledgerName:_ledger?_ledger.ledgerName:null});
        _ledger=_.find($scope.ledgerMap,{name:'disount'});
        $scope.ledgerMap.push({name:'discount',type:'discount',ledgerName:_ledger?_ledger.ledgerName:null});

        ledgerMap=angular.copy($scope.ledgerMap);
      },function(err){
         growl.error("Error in getting ledger mapping",{ttl:3000});
      })
    }

    function mapLedgers(array,type,ledgerMap){
      if(type=='payments') {
        _.forEach(array, function (object) {
          var _ledger = _.find(ledgerMap, {type: type, name: object.name});
          $scope.ledgerMap.push({
            name: object.name,
            type: type,
            ledgerName: _ledger? _ledger.ledgerName : null
          })
        })
      }
      else{
        _.forEach(array, function (object) {
          var _ledger = _.find(ledgerMap, {type: type, _id: object._id});
          $scope.ledgerMap.push({
            name: object.name,
            type: type,
            _id: object._id,
            ledgerName: _ledger? _ledger.ledgerName : null
          })
        })
      }
   }

    $scope.updateLedgerMapping=function(){
      var data={};
      data.deployment_id=$scope.deployment._id;
      data.tenant_id=localStorageService.get('tenant_id');
      data.ledgerMap=$scope.ledgerMap;
      $http.post('/api/ledgers?deployment_id='+$scope.deployment._id,data).then(function(success){
        ledgerMap=angular.copy($scope.ledgerMap);
        growl.success("Updated ledger mapping",{ttl:3000});
      },function(err){
        growl.error("error in mapping",{ttl:3000});
        $scope.ledgerMap=angular.copy(ledgerMap);
      })
    }

  }]).directive('checkConfirmEmail', function(){
    return {
        scope: {},
        link:function(scope,element){
            element.on('cut copy paste', function (event) {
              event.preventDefault();
            });
        }
    };
}).controller('DeleteItemModalCtrl', ['$scope', '$modalInstance', 'growl', function($scope, $modalInstance, growl) {
  console.log('DeleteItemModal opened');
    $scope.deleteItemData = {};
    $scope.deleteItemData.ok = function () {
      console.log('ok');
      //$modalInstance.close($scope.deleteItemData.email);
      if($scope.deleteItemData.email.toLowerCase() == $scope.deleteItemData.confirmEmail.toLowerCase()){
        $modalInstance.close($scope.deleteItemData.email);
      } else {
        growl.error("Email doesn't match!", {ttl: 3000});
      }
    }
    $scope.deleteItemData.cancel = function () {
      $modalInstance.dismiss('cancel');
    }
}]);

//Helper Functions for cleaning unecessary fields
function cleanStation(station){
  var _station = angular.copy(station);
  delete _station.created;
  delete _station.updated;
  delete _station.deployment_id;
  delete _station.tenant_id;
  delete _station.__v;
  delete _station.categories;
  return _station;
};

function cleanSuperCategory(sc){
  var _sc = angular.copy(sc);
  delete _sc.created;
  delete _sc.updated;
  delete _sc.deployment_id;
  delete _sc.tenant_id;
  delete _sc.selected;
  delete _sc.__v;
  return _sc;
}

function cleanTabForm(tabForm){
  var tab = {
    tabName : tabForm.tabName,
    _id : tabForm._id,
    tabType : tabForm.tabType,
    tabDetails : tabForm.tabDetails,
  }
  if(tabForm.categories)
    tab.categories = tabForm.categories;
  return tab;
};

function cleanCategory(cat,extras){
  var _cat = angular.copy(cat);
  delete _cat.created;
  delete _cat.updated;
  delete _cat.tenant_id;
  delete _cat.deployment_id;
  delete _cat.__v;
  delete _cat.selected;
  delete _cat.modified;
  delete _cat.selected;
  delete _cat.oldSelected;
  return _cat;
};

function cleanTax(tax,pure){
  var _tax = angular.copy(tax);
  delete _tax.updated;
  delete _tax.created;
  delete _tax.tenant_id;
  delete _tax.deployment_id;
  delete _tax.items;
  delete _tax.__v;
  if(pure){
    delete _tax.cascadingTaxes;
    delete _tax.tabs;
  }
  return _tax;
};

function cleanUnit(unit){
  var _unit = angular.copy(unit);
  delete _unit.created;
  delete _unit.updated;
  delete _unit.deployment_id;
  delete _unit.tenant_id;
  return _unit;
};

function cleanHeadForm(headForm){
  var head = {
    _id:headForm._id,
    name : headForm.name
  }
  return head;
};

//biometric
function cleanShiftForm(shiftForm){
  var shift = {
    _id:shiftForm._id,
    name : shiftForm.name,
    shiftFrom : shiftForm.shiftFrom,
    shiftTo : shiftForm.shiftTo
  }
  return shift;
};
//biometric



