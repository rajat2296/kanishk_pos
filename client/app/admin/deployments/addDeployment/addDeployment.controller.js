'use strict';

angular.module('posistApp')
  .controller('AdddeploymentCtrl', function ($rootScope, $scope, $modalInstance, Deployment, Auth, localStorageService) {

        $scope.deploymentForm = {
            tenant_id: localStorageService.get('tenant_id')
        };

        $scope.ok = function (form) {
            $scope.submittedDepForm = true;
            console.log(form);
            if (form.$valid) {
                $scope.deploymentForm.settings = $rootScope.settings;
                /*console.log($scope.deploymentForm);*/
                Deployment.save({}, $scope.deploymentForm, function (d){
                    form.$setPristine();
                    $modalInstance.close(d);
                    $rootScope.$broadcast('');
                });
            }


        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
  });
