'use strict';

describe('Controller: DeploymentsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var DeploymentsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DeploymentsCtrl = $controller('DeploymentsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
