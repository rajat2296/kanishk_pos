'use strict';

angular.module('posistApp')
    .controller('CustomerCtrl', function ($scope, $modalInstance, number, growl) {

        /* $scope.selected = {
         item: $scope.items[0]
         };*/
        $scope.addressTypes = ["Residence", "Office"];
        $scope.address = {
            type: "",
            line1:"",
            line2:"",
            city:"",
            state:"",
            postcode:"",
            country:"India"
        };
        $scope.newCustomer = {
            mobile: number,
            addresses: []
        };
        /*$scope.newCustomer.addresses.push(angular.copy($scope.address));*/

        $scope.addAddress = function () {
            $scope.newCustomer.addresses.push(angular.copy($scope.address));
        };

        $scope.createNewCustomer = function (form) {

            if (form.$valid) {
                if ($scope.newCustomer.addresses.length > 0) {
                    if ($scope.newCustomer)
                        $modalInstance.close($scope.newCustomer);
                } else {
                    growl.success("Please add Customer Address", {ttl: 3000});
                }
            } else {
                growl.error("Something went wrong while filling the form, please check and try again.", {ttl: 3000});
            }

        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });
