'use strict';

angular.module('posistApp')
  .controller('LoginCtrl', function ($rootScope, $scope, Auth, $location, $window, localStorageService, growl, Deployment,intercom,$modal,Utils) {
    $scope.user = {};
    $scope.error = null;

    $scope.login = function (form) {
      // console.log($stateParams);
      console.log($location.$$host.split('.')[0]);
      //return false;
      $scope.submitted = true;
      if (form.$valid) {

        Auth.login({
            email: $scope.user.email,
            // username: $location.$$host.split('.')[0]+'_'+$scope.user.username,
            //username: 'posist_' + $scope.user.username,
            username: $scope.user.username,
            password: $scope.user.password
          })
          .then(function (data) {
            // Logged in  redirect to home
            data.$promise.then(function (user) {
              //  if(user.role='superadmin')
              $rootScope.currentUser = user;
              localStorageService.set('currentUser', user);

              /* Validate: User of a different deployment can't login into this client */
              /*localStorageService.set('tenant_id', '5503dc34c8a81ac81a2934a6');
               localStorageService.set('deployment_id', '551d4f98396efe5829843d77');*/

              if (!localStorageService.get('tenant_id')) {
                localStorageService.set('tenant_id', user.tenant_id);
                localStorageService.set('lastSynced');
              }

              if (localStorageService.get('deployment_id')) {
                var _dId = localStorageService.get('deployment_id');
                if (user.role === 'user') {
                  Deployment.findOne({id : _dId}, function (dep) {

                    if (_dId != user.deployment_id) {
                      Auth.logout();
                      growl.error("You don't belong to this Deployment!", {ttl: 3000});
                      $rootScope.$state.go('login');
                    } else {
                      if(user.selectedRoles.length==1 && user.selectedRoles[0].name=="Vendor"){
                        $rootScope.$state.go('stock.indentGrn'); //('indentGRN');
                      }
                      else if (dep.isBaseKitchen){
                        $rootScope.$state.go('stock');
                      }
                      else
                      {
                        $rootScope.$state.go('billing');
                      }
                    }
                  });
                } else if (user.role === 'superadmin') {
                  /*localStorageService.set('tenant_id', '5503dc34c8a81ac81a2934a6');
                   localStorageService.set('deployment_id', '551d4f98396efe5829843d77');*/
                  /*$location.path('/gettingstarted')*/
                  $rootScope.$state.go('gettingstarted');
                  /*Deployment.get({tenant_id:localStorageService.get('tenant_id')}, function (deployments) {

                   var _f = _.filter(deployments, {isMaster:true});
                   if (_f[0].length > 0 && _f[0].isCompleted === true) {
                   $location.path('/admin/deployments');
                   } else {
                   $location.path('/gettingstarted');
                   }

                   }, function (err) {
                   alert("Error Getting Deployments");
                   });*/

                }
              } else {
                console.log("Deployment ID not found");
                /*console.log("Setting stuff in Localstorage");*/
                if (user.deployment_id === null && user.role === 'superadmin') {

                  /*Deployment.get({tenant_id:localStorageService.get('tenant_id')}, function (deployments) {

                   var _f = _.filter(deployments, {isMaster:true});
                   if (_f[0].length > 0 && _f[0].isCompleted === true) {
                   $location.path('/admin/deployments');
                   } else {
                   $location.path('/gettingstarted');
                   }

                   }, function (err) {

                   });*/
                  $rootScope.$state.go('gettingstarted');

                } else if (user.deployment_id != null && user.role === 'user') {

                  //localStorageService.set('lastSynced');
                  Deployment.findOne({id: user.deployment_id}, function (deployment) {
                    console.log(deployment);
                    if(_.has(deployment,'instances')){
                      if(deployment.instances.length>0){
                        $modal.open({
                          templateUrl: 'app/admin/instance.html',
                          controller: ['$scope', '$modalInstance','instances', function ($scope, $modalInstance,instances) {
                            $scope.instances=instances;
                            $scope.cancel = function () {
                              $modalInstance.dismiss('')
                            };
                            $scope.selectedInstance = function (instance) {
                              $modalInstance.close(instance);
                            }
                          }],
                          size: 'md',
                          resolve:{
                            instances:function(){
                              return  deployment.instances;
                            }
                          }
                        }).result.then(function (instance) {
                          var instanceSetting=Utils.getSettingValue('instance_server',deployment.settings);
                          $rootScope.url= instanceSetting==null||instanceSetting==undefined?'http://jdd.posist.org':instanceSetting;
                          $rootScope.isInstance=true;
                          localStorageService.set('deployment_id', deployment._id);
                          localStorageService.set('tenant_id', user.tenant_id);
                          localStorageService.set('instanceId', instance);
                          $rootScope.$state.go('billing');
                        });
                      }else{
                        localStorageService.set('deployment_id', user.deployment_id);
                        localStorageService.set('tenant_id', user.tenant_id);
                        localStorageService.set('instance_id', {});
                        $rootScope.$state.go('billing');
                      }
                    }else {
                      localStorageService.set('deployment_id', user.deployment_id);
                      localStorageService.set('tenant_id', user.tenant_id);
                      localStorageService.set('instance_id', {});
                      $rootScope.$state.go('billing');
                    }
                  });
                  //need to add instance selection
                  /*
                   if (_.has(d, 'instances')) {
                   if (d.instances.length > 0) {
                   // alert('instance');

                   $modal.open({
                   templateUrl: 'app/admin/instance.html',
                   controller: ['$scope', '$modalInstance', 'instances', function ($scope, $modalInstance, instances) {
                   $scope.instances = instances;
                   $scope.cancel = function () {
                   $modalInstance.dismiss('')
                   };
                   $scope.selectedInstance = function (instance) {
                   $modalInstance.close(instance);
                   }

                   }],
                   size: 'md',
                   resolve: {
                   instances: function () {
                   return d.instances;
                   }
                   }
                   }).result.then(function (instance) {
                   localStorageService.set('deployment_id', d._id);
                   localStorageService.set('instanceId', instance);
                   $rootScope.$state.go('billing');
                   });
                   } else {
                   localStorageService.set('deployment_id', d._id);
                   localStorageService.set('instanceId', {});
                   $rootScope.$state.go('billing');
                   }
                   }
                   */

                }

              }


              /* if (data.role === 'superadmin') {
               $location.path('/admin');
               }*/

              //---ADDED BY RAJAT for INTERCOM---//
              /*Deployment.findOne({id: localStorageService.get('deployment_id')}, function (deployment) {
                console.log(deployment);
                var user={
                  user_id:deployment._id,
                  name:deployment.name,
                  update_last_request_at:true,
                  new_session:true
                };*/
                intercom.createUser();
              //---ADDED BY RAJAT END---//
            });

          })
          .catch(function (err) {
            console.log(err);
            $scope.error = err.message;
            /*$scope.errors.other = err.message;*/
          });
      }
    };

    $scope.loginOauth = function (provider) {
      $window.location.href = '/auth/' + provider;
    };
  });
