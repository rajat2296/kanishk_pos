'use strict';

angular.module('posistApp')
    .controller('SignupCtrl',function ($scope, $rootScope, Auth, $location, $window, RegForm, Tenant, User, Email, $templateCache, $compile, $timeout) {


        /*$scope.regForm = {
            address1: "202 Roll Lane",
             address2: "Moti Bagh",
             city: "Delhi",
             country: "India",
             email: "34cl@34cl.com",
             mobile: "9999817598",
             name: "34 Chowringhee Lane",
             outlet: "Rohini East",
             pan: "ASC655498",
             password: "rishi",
             state: "Delhi",
             subdomain: "34cl",
             telephone: "011 22568656",
             username: "rishibaba"
        };*/
        $scope.regForm = {};
        $scope.user = {};
        $scope.errors = {};
        $scope.submitted = false;
        $scope.registeredSubdomains = ['btw', 'gianni', 'chowman', 'ohcalcutta'];

        $scope.register = function (form) {
            $scope.submitted = true;
            var _port = $location.port();
            var _template = $templateCache.get('newTenantEmailTemplate');
            var _emailScope = $rootScope.$new();

            if (form.$valid) {

                Tenant.save({}, $scope.regForm, function (tenant) {
                    User.save({}, {
                        name: $scope.regForm.name,
                        email: $scope.regForm.email,
                        username: $scope.regForm.subdomain + '_' + $scope.regForm.username, // append subdomain to superAdmin
                        password: $scope.regForm.password,
                        subdomain: $scope.regForm.subdomain,
                        tenant_id: tenant._id,
                        role: 'superadmin',
                        deployment_id: null
                    }, function (user) {

                        console.log(user);
                        console.log(tenant);
                        /* Prepare Email Template */
                        angular.extend(_emailScope,
                            {
                                tenantName: tenant.name,
                                activationLink : $location.protocol() + '://'  + tenant.subdomain + '.' + $location.host() + ':' + _port + '/activation/' + user.user._id
                            }
                        );
                        var _message = $compile($('<div>' + _template + '</div>'))(_emailScope);
                        var waitForRenderAndPrint = function () {
                            if (_emailScope.$$phase) {
                                $timeout(waitForRenderAndPrint);
                            } else {
                                var _to = tenant.name + ' <' + user.user.email + '>';
                                console.log(_to.toString());
                                 Email.send({}, {
                                     messageText: _message.text(),
                                     messageHtml: _message.html(),
                                     to: _to.toString()
                                 }, function (data) {

                                     $location.path('/');

                                 }, function (err) {
                                     console.log(err);
                                 });

                                _emailScope.$destroy();
                            }
                        };
                        waitForRenderAndPrint();


                    }, function (err) {
                        err = err.data;
                        $scope.errors = {};

                        // Update validity of form fields that match the mongoose errors
                        angular.forEach(err.errors, function (error, field) {
                            form[field].$setValidity('mongoose', false);
                            $scope.errors[field] = error.message;
                        });
                    });
                }, function (err) {
                    console.log(err);
                    err = err.data;
                    $scope.errors = {};

                    // Update validity of form fields that match the mongoose errors
                    angular.forEach(err.errors, function (error, field) {
                        form[field].$setValidity('mongoose', false);
                        $scope.errors[field] = error.message;
                    });
                });

               /* RegForm.createTenant({
                    name: $scope.regForm.name,
                    subdomain: $scope.regForm.subdomain,
                    active: true
                }).then(function(data){
                    Auth.createUser({
                        name: $scope.regForm.name,
                        email: $scope.regForm.email,
                        username: $scope.regForm.username,
                        password: $scope.regForm.password,
                        subdomain: $scope.regForm.subdomain,
                        tenant_id: data._id,
                        role: 'superadmin',
                        deployment_id: null
                    }, function (user) {
                        console.log(user);
                       *//* Auth.logout();*//*
                        *//*window.location.href = $location.protocol() + '://' + user.subdomain + '.' + $location.host() + ':' + _port + '/login';*//*

                    });
                }).then(function () {
                    // Account created, redirect to home
                    $location.path('/gettingstarted');
                }).catch(function (err) {
                    err = err.data;
                    $scope.errors = {};

                    // Update validity of form fields that match the mongoose errors
                    angular.forEach(err.errors, function (error, field) {
                        form[field].$setValidity('mongoose', false);
                        $scope.errors[field] = error.message;
                    });
                });*/

            }
        };

        $scope.loginOauth = function (provider) {
            $window.location.href = '/auth/' + provider;
        };
    }).factory('dataService', ['$http', function ($http) {
        var serviceBase = '/api/users/',
            dataFactory = {};

        dataFactory.checkUniqueValue = function (id, property, value) {
            if (!id) id = 0;

            return $http.get(serviceBase).then(
                function (results) {
                    return results;
                });
        };
        return dataFactory;

    }]).directive('wcUnique', ['dataService', function (dataService) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.bind('blur', function (e) {

                    if (!ngModel || !element.val()) return;
                    var keyProperty = scope.$eval(attrs.wcUnique);
                    var currentValue = element.val();
                    //console.log(keyProperty);

                    ngModel.$setValidity('unique', true);
                    //console.log(ngModel);

                    /*  dataService.checkUniqueValue(keyProperty.key, keyProperty.property, currentValue)
                     .then(function (unique) {
                     //Ensure value that being checked hasn't changed
                     //since the Ajax call was made
                     */
                    /*if (currentValue == element.val()) {
                     ngModel.$setValidity('unique', unique);
                     }*/
                    /*
                     //ngModel.$setValidity('unique', false);
                     ngModel.$setValidity('unique', null);
                     console.log(ngModel);
                     }, function () {
                     //Probably want a more robust way to handle an error
                     //For this demo we'll set unique to true though
                     */
                    /*ngModel.$setValidity('unique', true);*/
                    /*
                     //ngModel.$setValidity('unique', false)
                     //console.log(ngModel);
                     });*/
                });
            }
        }
    }]).factory("RegForm", function ($location, $rootScope, $http, User, Tenant, Auth, $cookieStore, $q) {
        return {
            /**
             * Create a Tenant
             *
             * @param  {Object}   tenant     - tenant info
             * @param  {Function} callback - optional
             * @return {Promise}
             */
            createTenant: function (tenant, callback) {
                var cb = callback || angular.noop;
                return Tenant.save(tenant,
                 function (data) {
                    return cb(data);
                 },
                 function (err) {
                    return cb(err);
                 }.bind(this)).$promise;

            },
            address1: "",
            address2: "",
            city: "",
            country: "",
            email: "",
            mobile: "",
            name: "",
            outlet: "",
            pan: "",
            password: "",
            state: "",
            subdomain: "",
            telephone: "",
            username: ""
        };
    });

    /*address1: "202 Roll Lane",
    address2: "Moti Bagh",
    city: "Delhi",
    country: "India",
    email: "34cl@34cl.com",
    mobile: "9999817598",
    name: "34 Chowringhee Lane",
    outlet: "Rohini East",
    pan: "ASC655498",
    password: "rishi",
    state: "Delhi",
    subdomain: "34cl",
    telephone: "011 22568656",
    username: "rishibaba"*/