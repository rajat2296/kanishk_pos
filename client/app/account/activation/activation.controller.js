'use strict';

angular.module('posistApp')
    .controller('ActivationCtrl', function ($scope, $stateParams, Auth, $location, $window, User) {
        $scope.user = {};
        $scope.errors = {};


        $scope.authForm = {
            authId: $stateParams.authId
        };

        $scope.activateUser = function (form) {

            User.getActivationLink({authId: $scope.authForm.authId}, function (user) {
                user.activated = true;
                User.save({id:user._id}, user, function (user) {
                    $location.path('/login');
                }, function (err) {
                    alert("Error in Activating");
                });
            }, function (err) {
                console.log("Error Activating");
            });

        };
    });
