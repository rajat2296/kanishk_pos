'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('activation', {
        url: '/activation/:authId',
        templateUrl: 'app/account/activation/activation.html',
        controller: 'ActivationCtrl'
      });
  });