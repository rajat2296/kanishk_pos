'use strict';

describe('Controller: ActivationCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var ActivationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActivationCtrl = $controller('ActivationCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
