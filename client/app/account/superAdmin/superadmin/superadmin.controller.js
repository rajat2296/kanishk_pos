'use strict';

angular.module('posistApp')
  .controller('SuperadminCtrl', [ '$scope', 'Smstemplates', 'Utils', 'Pagination', 'growl', '$ngBootbox', 'Deployment', 'Tenant', 'Auth', 'Settles', '$modal', 'Customer', 'StockMasterUser', 'deployment', '$http', function ($scope, Smstemplates, Utils, Pagination, growl, $ngBootbox, Deployment, Tenant, Auth, Settles, $modal, Customer, StockMasterUser, deployment, $http) {
    $scope.sms={type:'Kot',content:'',inEdit:false,templateNumber:''};
        $scope.settle={name:'',isOnlineOrder:false,inEdit:false};
        $scope.settleBank={name:'',inEdit:false};
      $scope.checkType=function(){
        console.log($scope.sms);
      }
      $scope.templates=[];
        $scope.settles=[];
         $scope.settleBanks=[];
     Smstemplates.get().$promise.then(function(templates){
      // console.log(templates);
       if(templates.length>0) {
         $scope.templates = _.sortBy(templates, ['templateNumber']);
         $scope.sms.templateNumber = $scope.templates[$scope.templates.length - 1].templateNumber + 1;
         $scope.tPagination = Pagination.getNew(10);
         $scope.tPagination.numPages = Math.ceil($scope.templates.length / $scope.tPagination.perPage);
         $scope.tPagination.totalItems = $scope.templates.length;
       }else{
         $scope.sms.templateNumber=1
       }

     })
        Settles.get().$promise.then(function(settleAll){
            // console.log(templates);
            if(settleAll.length>0) {
                $scope.settles=settleAll;
            }
        });
        
        Settles.getBank().$promise.then(function(settleBankAll){
            // console.log(templates);
            if(settleBankAll.length>0) {
                $scope.settleBanks=settleBankAll;
            }
        });
      
      $scope.psErrors=[];
      $scope.sErrors=[];
      
      $scope.createTemplate=function(smsForm){
        if(!$scope.sms.inEdit) {
           Smstemplates.save($scope.sms).$promise.then(function (smsTemplate) {
            $scope.templates.push(smsTemplate);
            growl.success('Template created successfully', {ttl: 2000});
             $scope.resetForm(smsForm);
          }, function (err) {
             angular.forEach(err.data.errors, function (error, field) {
               smsForm[field].$setValidity('mongoose', false);
               $scope.psErrors[field] = error.message;
             });
            console.log(err);
          })
        }else{
          Smstemplates.update($scope.sms).$promise.then(function (smsTemplate) {
            $scope.templates.splice(Utils.arrayObjectIndexOf($scope.templates,'_id',smsTemplate._id),1);
            $scope.templates.push(smsTemplate);
            $scope.resetForm(smsForm);
            console.log(smsTemplate);
          });
          }
      }

        $scope.createSettle=function(settleForm){
            if(!$scope.settle.inEdit) {
                Settles.save($scope.settle).$promise.then(function (settleData) {
                    $scope.settles.push(settleData);
                    growl.success('Template created successfully', {ttl: 2000});
                    $scope.resetSettleForm(settleForm);
                }, function (err) {
                    angular.forEach(err.data.errors, function (error, field) {
                        settleForm[field].$setValidity('mongoose', false);
                        $scope.sErrors[field] = error.message;
                    });
                    console.log(err);
                })
            }else{
                Settles.update($scope.settle).$promise.then(function (settleData) {
                    $scope.settles.splice(Utils.arrayObjectIndexOf($scope.settles,'_id',settleData._id),1);
                    $scope.settles.push(settleData);
                    $scope.resetSettleForm(settleForm);
                    console.log(settleData);
                });
            }
        }
    
         $scope.createSettleBank=function(settleBankForm){
            if(!$scope.settleBank.inEdit) {
                    Settles.createBank($scope.settleBank).$promise.then(function (settleData) {
                    $scope.settleBanks.push(settleData);
                    growl.success('Template created successfully', {ttl: 2000});
                    $scope.resetSettleBankForm(settleBankForm);
                }, function (err) {
                    angular.forEach(err.data.errors, function (error, field) {
                        settleBankForm[field].$setValidity('mongoose', false);
                        $scope.sErrors[field] = error.message;
                    });
                    console.log(err);
                })
            }else{
                var tempSettleData= angular.copy($scope.settleBank);
                tempSettleData.bankId=angular.copy( tempSettleData._id);
                delete tempSettleData._id;
                console.log(tempSettleData);
                Settles.updateBank(tempSettleData).$promise.then(function (settleBankData) {
                    $scope.settleBanks.splice(Utils.arrayObjectIndexOf($scope.settleBanks,'_id',settleBankData._id),1);
                    $scope.settleBanks.push(settleBankData);
                    $scope.resetSettleBankForm(settleBankForm);
                    console.log(settleBankData);
                });
            }
        }

      $scope.resetForm=function(smsForm){
        $scope.templates = _.sortBy($scope.templates, ['templateNumber']);
        $scope.sms.templateNumber = $scope.templates[$scope.templates.length - 1].templateNumber + 1;
        $scope.tPagination = Pagination.getNew(10);
        $scope.tPagination.numPages = Math.ceil($scope.templates.length / $scope.tPagination.perPage);
        $scope.tPagination.totalItems = $scope.templates.length;
        //console.log(smsTemplate);
        $scope.sms = {type: 'Kot', content: '', inEdit: false, templateNumber: ''};
        smsForm.$pristine = true;
        $scope.sms.templateNumber = $scope.templates[$scope.templates.length - 1].templateNumber + 1;

      }
        $scope.resetSettleForm=function(settleForm){
            $scope.settle.name='';
            $scope.settle = {name:'',isOnlineOrder:false, inEdit: false};
            settleForm.$pristine = true;

        }
         $scope.resetSettleBankForm=function(settleBankForm){
            $scope.settleBank.name='';
            $scope.settleBank = {name:'', inEdit: false};
            settleBankForm.$pristine = true;

        }
      $scope.editTemplate=function(template){
      /*  console.log($scope.psErrors);
        if($scope.psErrors['templateNumber'].length>0){
          growl.error('Remove the error first',{ttl:2000});
          return false;
        }*/
        $scope.sms=angular.copy( template);
        $scope.sms.inEdit=true;
      }
        $scope.editSettle=function(settleData){
            /*  console.log($scope.psErrors);
             if($scope.psErrors['templateNumber'].length>0){
             growl.error('Remove the error first',{ttl:2000});
             return false;
             }*/
            $scope.settle=angular.copy( settleData);
            $scope.settle.inEdit=true;
        }

      $scope.editSettleBank=function(settleBankData){
            /*  console.log($scope.psErrors);
             if($scope.psErrors['templateNumber'].length>0){
             growl.error('Remove the error first',{ttl:2000});
             return false;
             }*/
            $scope.settleBank=angular.copy( settleBankData);
            $scope.settleBank.inEdit=true;
        }

        $scope.deleteTemplate=function(template){
        $ngBootbox.confirm('Do you really want to delete template Number :'+template.templateNumber+' ?')
            .then(function() {
              Smstemplates.destroy({id:template._id}).$promise.then(function(temp){
                $scope.templates.splice(Utils.arrayObjectIndexOf($scope.templates,'_id',template._id),1);
                $scope.tPagination = Pagination.getNew(10);
                $scope.tPagination.numPages = Math.ceil($scope.templates.length / $scope. tPagination.perPage);
                $scope.tPagination.totalItems = $scope.templates.length;
                if($scope.templates.length>0)
                  $scope.sms.templateNumber=$scope.templates[$scope.templates.length-1].templateNumber+1;
                else
                  $scope.sms.templateNumber=1;
                growl.success('Template deleted successfully',{ttl:2000});
              })
            }, function() {
              console.log('Confirm dismissed!');
            });

      }
        $scope.deleteSettle=function(settle){
            $ngBootbox.confirm('Do you really want to delete settlement Name :'+settle.name+' ?')
                .then(function() {
                    Settles.destroy({id:settle._id}).$promise.then(function(temp){
                        $scope.settles.splice(Utils.arrayObjectIndexOf($scope.settles,'_id',settle._id),1);
                        growl.success('Settle deleted successfully',{ttl:2000});
                    })
                }, function() {
                    console.log('Confirm dismissed!');
                });

        }
         $scope.deleteSettleBank=function(settleBank){
            $ngBootbox.confirm('Do you really want to delete bank Name :'+settleBank.name+' ?')
                .then(function() {
                    Settles.destroyBank({id:settleBank._id}).$promise.then(function(temp){
                        $scope.settleBanks.splice(Utils.arrayObjectIndexOf($scope.settleBanks,'_id',settleBank._id),1);
                        growl.success('Settle bank deleted successfully',{ttl:2000});
                    })
                }, function() {
                    console.log('Confirm dismissed!');
                });
        }
        
      $scope.clearSmsForm=function(){
        $scope.sms={type:'Kot',content:'',inEdit:false,templateNumber:''};
        $scope.sms.inEdit=false;
      }
        $scope.clearSettleForm=function(){
            $scope.settle={name:'',inEdit:false};
            $scope.settle.inEdit=false;
        }
        $scope.DeploymentsTenant=[];
        $scope.DeploymentsTenantEmailers=[];

        $scope.getDeployments=function(){
            Deployment.getAllDeployments().$promise.then(function(deps){
            var sortedDeps=_.sortBy(deps, ['name']);
                $scope.DeploymentsTenant = _.sortBy(deps, ['name']);
                $scope.DeploymentsTenantEmailers=angular.copy(sortedDeps);
                console.log(deps);
            })
        }
        $scope.addSMS=function(index){
            console.log($scope.DeploymentsTenant[index]);
            var selectedDeployment=$scope.DeploymentsTenant[index];
            var smsTrans={type:'Recharge',quantity:selectedDeployment.smsCount,rate:0,user:Auth.getCurrentUser(),deployment_id:$scope.DeploymentsTenant[index]._id};
            Deployment.updateField({dep_id:selectedDeployment._id,balanceSMS:selectedDeployment.smsCount,smsTransaction:smsTrans}).$promise.then(function(result){
                selectedDeployment.balanceSMS+=parseInt( selectedDeployment.smsCount);
                selectedDeployment.smsCount='';
                growl.success('Updated sms successfully.',{ttl:2000});
                console.log(result);
            });
        }
        $scope.updateEmails=function(index){
            var selectedDeployment=$scope.DeploymentsTenantEmailers[index];
            Deployment.updateFieldEmailer({dep_id:selectedDeployment._id,emails:selectedDeployment.emails}).$promise.then(function(result){
                growl.success('Updated email successfully.',{ttl:2000});
                //console.log(result);
                $scope.DeploymentsTenantEmailers[index].isChanged=false;
            });
        }
        $scope.dep={isChanged:false};
        $scope.changedRow=function(index){
            var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            var emails=$scope.DeploymentsTenantEmailers[index].emails;
            var emailsArr=emails.split(',');
            var flag=true;
            for(var i=0;i<emailsArr.length;i++) {
                if (!EMAIL_REGEXP.test(emailsArr[i])){
                    flag=false;
                }
            }
            if(flag){
                $scope.DeploymentsTenantEmailers[index].isChanged=true;
                $scope.dep.isChanged=true;
                //growl.success("Email verified",{ttl:2000});
            }else{
                $scope.DeploymentsTenantEmailers[index].isChanged=false;
                $scope.dep.isChanged=false;
            }
            if(emails==''){
                $scope.DeploymentsTenantEmailers[index].isChanged=true;
                $scope.dep.isChanged=true;
            }
        }
        $scope.createInstance=function(deployment) {
            $modal.open({
                templateUrl: 'app/account/superAdmin/superadmin/instance.html',
                controller: ['$scope', '$modalInstance', 'deployment', 'Utils', 'Deployment', function ($scope, $modalInstance, deployment, Utils, Deployment) {
                    //alert(Utils.getDateFormattedDate(new Date()));
                    $scope.instance = {
                        id: Utils.guid(),
                        name: '',
                        preFix: '',
                        expiry: Utils.getDateFormattedDate(new Date()),
                        inEdit: false
                    };

                    $scope.instances = [];
                    if (_.has(deployment, 'instances')) {
                        $scope.instances = deployment.instances;
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('')
                    };
                    $scope.createInstance = function (instanceForm) {
                        var instanceTemp = angular.copy($scope.instance);
                        delete instanceTemp.inEdit;

                        if ($scope.instance.inEdit) {
                            for (var i = 0; i < $scope.instances.length; i++) {
                                if ($scope.instances[i].id == $scope.instance.id) {
                                    $scope.instances[i] = angular.copy(instanceTemp);
                                    $scope.refreshForm(instanceForm);
                                    break;
                                }
                            }
                        } else {
                            $scope.instances.push(instanceTemp);
                            $scope.refreshForm(instanceForm);
                        }

                    }
                    $scope.refreshForm = function (instanceForm) {
                        $scope.instance = {
                            id: Utils.guid(),
                            name: '',
                            preFix: '',
                            expiry: Utils.getDateFormattedDate(new Date()),
                            inEdit: false
                        }
                        instanceForm.$pristine = true;
                    }
                    $scope.editInstance = function (instance) {
                        $scope.instance = angular.copy(instance);
                        $scope.instance.inEdit = true;
                    }
                    $scope.deleteInstance=function(index){
                        $scope.instances.splice(index,1)
                    }
                    $scope.submit = function () {

                        $modalInstance.close($scope.instances);
                    }
                    // if(instance.expiry!=undefined){$scope.advance.datetime=advance.datetime};
                    // console.log(advance);
                    $scope.openDatePicker = function ($event, opened) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope[opened] = true;
                    };

                }],
                size: 'lg',
                resolve: {
                    deployment: function () {
                        return deployment;
                    }
                }
            }).result.then(function (instances) {
                    Deployment.updateFieldInstances({
                        dep_id: deployment._id,
                        instances: instances
                    }).$promise.then(function (status) {
                        deployment.instances=instances;
                            growl.success('Updated successfully',{ttl:3000});
                        },function(err){
                            growl.success('Error updating instances',{ttl:3000});
                        })
                })
        }
        $scope.createEmailOrderMap=function(deployment){
            $modal.open({
                templateUrl: 'app/account/superAdmin/superadmin/emailOrderMap.html',
                controller: ['$scope', '$modalInstance', 'deployment', 'Utils', 'Deployment', function ($scope, $modalInstance, deployment, Utils, Deployment) {
                    //alert(Utils.getDateFormattedDate(new Date()));
                    $scope.mapOrder={swiggy:'',foodpanda:'',zomato:'',limetray:''};
                    if(!(deployment.mapDeployment==undefined ||deployment.mapDeployment==null)){
                        $scope.mapOrder=angular.copy( deployment.mapDeployment);
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('')
                    };

                    $scope.submit = function () {

                        $modalInstance.close($scope.mapOrder);
                    }


                }],
                size: 'md',
                resolve: {
                    deployment: function () {
                        return deployment;
                    }
                }
            }).result.then(function (mappedName) {
                    console.log(mappedName);
                    Deployment.updateFieldMapOrder({
                        dep_id: deployment._id,
                        mapDeployment: mappedName
                    }).$promise.then(function (status) {
                            deployment.mapDeployment=mappedName;
                            growl.success('Updated successfully',{ttl:3000});
                        },function(err){
                            growl.success('Error updating instances',{ttl:3000});
                        })
                })
        }
        $scope.cgs=[];
        $scope.CRMSettings=function(){
            Customer.getCustomerGroups().$promise.then(function(cgs){
                $scope.cgs=angular.copy(cgs);
                $scope.cgs= _.sortBy($scope.cgs, ['tenant.subdomain']);
                console.log(cgs);
            })
        }
        $scope.getTenants = function(val) {
      return  Tenant.search({q:val
        }).$promise.then(function(tenants){
             var _c = [];
        _.forEach(tenants, function(c) {
          _c.push(c);
        });
      // console.log(_c);
            return  _c; 
        });
      }
      $scope.deploymentsCustomer=[];
      $scope.deploymentsCustomerSelected=[];
      $scope.selectedTenant=null;
      $scope.isDeploymentRendered=false;
      $scope.tenantSearchEnter=function(seletedTenant){
        $scope.isDeploymentRendered=true;
        //console.log(seletedTenant);
        Deployment.getDeploymentsByTenantName({name:seletedTenant.subdomain}).$promise.then(function(deployments){
            //console.log(deployments);
            $scope.deploymentsCustomer=deployments;
            $scope.selectedTenant=angular.copy( seletedTenant);
            
        },function(err){
            $scope.isDeploymentRendered=false;
            growl.error('There is problem getting relevent data',{ttl:2000});
        })
      }
      $scope.save_deployments=function(){

       // console.log( angular.toJson( $scope.deploymentsCustomerSelected));
       if($scope.deploymentsCustomerSelected.length==0){
          growl.error('Select atleast 1 deployment',{ttl:2000}); 
          return false; 
       }
        Customer.createCustomerGroupWithCheck({tenant:{_id:$scope.selectedTenant._id,subdomain:$scope.selectedTenant.subdomain},deployments:JSON.parse( angular.toJson( $scope.deploymentsCustomerSelected))}).$promise.then(function(response){
           for(var i=0;i<$scope.cgs.length;i++){
             if($scope.cgs[i].tenant._id==response.tenant._id){
                $scope.cgs.splice(i,1);
             }
           }
           $scope.cgs.push(response);
           $scope.cgs= _.sortBy($scope.cgs, ['tenant.subdomain']);
            growl.success('Saved/updated successfully. ',{ttl:2000});
              $scope.deploymentsCustomer=[];
              $scope.deploymentsCustomerSelected=[];
              $scope.selectedTenant=null;
               $scope.isDeploymentRendered=false;
        document.getElementById('txtSearchTenant').value='';    
        },function(err){
            growl.error('There are problem saving the data.',{ttl:2000});    
        })
      }
      $scope.cancel_deployments=function(){
              $scope.deploymentsCustomer=[];
              $scope.deploymentsCustomerSelected=[];
              $scope.selectedTenant=null;
               $scope.isDeploymentRendered=false;
        document.getElementById('txtSearchTenant').value='';    
        
      }
  $scope.transactionsTab = {
        stockTransactionForm: {},
        transactions : [],
        tenantNotSelected : true,
        disableTenantAndDeployment : false,
        showResetButton : false,
        deployments: [],
        transactionsToDelete : []   
    }

    $scope.resetTransactionsTab = function () {
        $scope.transactionsTab = {
        stockTransactionForm: {},
        transactions : [],
        tenantNotSelected : true,
        showResetButton : false,
        deployments : [],
        transactionsToDelete : []
        }   
    }

    $scope.disableTenant = function() {
        $scope.transactionsTab.disableTenantAndDeployment = true;
        $scope.transactionsTab.showResetButton = true;
        $scope.transactionsTab.tenantNotSelected = false;
    }

    $scope.dateCal={modFrom:false,modTo:false,delDateFrom:false,delDateTo:false,delItemFrom:false,delItemTo:false,delBillFrom:false,delBillTo:false,historyFromDate:false,historyToDate:false};

    $scope.openCalender = function($event,type) {
      //  console.log($scope.dateCal);
        $event.preventDefault();
        $event.stopPropagation();
        if(type=='modFrom'){$scope.dateCal.modFrom= true;$scope.dateCal.modTo= false;}
        if(type=='modTo'){$scope.dateCal.modTo= true;$scope.dateCal.modFrom= false;}
        if(type=='delDateFrom')$scope.dateCal.delDateFrom= true;
        if(type=='delDateTo')$scope.dateCal.delDateTo= true;
        if(type=='delItemFrom')$scope.dateCal.delItemFrom= true;
        if(type=='delItemTo')$scope.dateCal.delItemTo= true;
        if(type=='delBillFrom')$scope.dateCal.delBillFrom= true;
        if(type=='historyFrom')$scope.dateCal.historyFromDate= true;
        if(type=='historyTo')$scope.dateCal.historyToDate= true;
      };

      $scope.stockTransactionType = [
          {
            type : '1',
            transactionName : 'Stock Entry'
          },
          {
            type : '2',
            transactionName : 'Stock Sale'
          },
          {
            type : '3',
            transactionName : 'Physical Stock'
          },
          {
            type : '4',
            transactionName : 'Wastage Entry'
          },
          {
            type : '5',
            transactionName : 'Stock Transfer'
          },
          {
            type : '6',
            transactionName : 'Opening Stock'
          },
          {
            type : '7',
            transactionName : 'Intermediate Entry'
          },
          {
            type : '9',
            transactionName : 'Stock Return'
          },
          {
            type : '10',
            transactionName : 'Intermediate Opening'
          },
          {
            type : '11',
            transactionName : 'Finished Food Entry'
          },
          {
            type : '12',
            transactionName : 'Stock Receive'
          },
          {
            type : '13',
            transactionName : 'Finished Food Opening'
          },
          {
            type : '14',
            transactionName : 'Indent Delivery Challan'
          },
          {
            type : '15',
            transactionName : 'Indent Delivery Challan'
          },
          {
            type : '16',
            transactionName : 'Outlet Indent'
          }
      ];

      function formatTransaction(transaction) {
        var tr = transaction;
        var count = 0;
        transaction.transactionTime = moment(new Date(transaction.created)).format('DD-MM-YYYY hh:mm:ss a');
        
        _.forEach($scope.stockTransactionType, function(type) {
            if(type.type == tr.transactionType)
                tr.transactionName = type.transactionName;
        });

        if(transaction.transactionType == '1')
        {
            _.forEach(transaction._store.vendor.category, function(category) {
                _.forEach(category.items, function (item) {
                    count++;
                });
            });
            tr.numberOfItems = count;
        }
        else if(transaction.transactionType == '2')
        {
            _.forEach(transaction._store.receiver.category, function(category) {
                _.forEach(category.items, function (item) {
                    count++;
                });
            });
            tr.numberOfItems = count;
        }
        else
        {
            _.forEach(transaction._store.category, function(category) {
                _.forEach(category.items, function (item) {
                    count++;
                });
            });
            tr.numberOfItems = count;
        }
        
        return tr;
      }

      $scope.getTenantName = function(value) {
            var query = {
                search: value
            }
            return $http({
                method: 'POST',
                url: '/api/stockMasterUsers/getTenantName',
                data: query
            }).then(function(result) {
                console.log('data', result);
                return result.data;
            });
        };

        $scope.getDeploymentName = function(tenantId) {
            console.log('tenantId', $scope.transactionsTab.tenant);
            StockMasterUser.getDeploymentName({tenant_id: tenantId}, function(deployment) {
                console.log('deployment', deployment);
                $scope.transactionsTab.deployments = deployment;    
            }, function(err) {
                if (err.message)
                    growl.error(err.message, { ttl: 3000 });
                else
                    growl.error("Some error occured. Please try again", { ttl: 3000 });
            });
        }

      $scope.getStockTransactions = function(form) {
        $scope.transactionsTab.transactions = [];
        var req = {
            fromDate : new Date($scope.transactionsTab.stockTransactionForm.fromDate),
            toDate : new Date($scope.transactionsTab.stockTransactionForm.toDate),
            transactionType : $scope.transactionsTab.stockTransactionForm.transactionType,
            deployment_id: $scope.transactionsTab.stockTransactionForm.deployment_id
        }
        console.log('req', req);
        StockMasterUser.getTransactions(req, function(transactions) {
            _.forEach(transactions, function(transaction) {
            var tr = formatTransaction(transaction);    
            $scope.transactionsTab.transactions.push(tr);
            })
            
            console.log('transactions', angular.copy(transactions));
            //$scope.transactionsTab.transactions = tr;
        });
      }


    $scope.deleteTransactionModal = function(transaction , deletionType , index) {
        console.log(transaction, index);
        var deployment_id = $scope.transactionsTab.stockTransactionForm.deployment_id;
        if(deletionType == 'Single') {
        $scope.modalInstance = $modal.open({
                templateUrl: 'deleteTransaction.html',
                controller: 'DeleteTransactionCtrl',
                size: 'sm',
                backdrop: 'static',
                resolve: {
                    transaction: function() {
                        return transaction;
                    },
                    deletionType : function() {
                        return deletionType;
                    },
                    deployment_id : function() {
                        return deployment_id;
                    }
                }
                
            }).result.then(function (res) {
                if(res.result == 'success')
                    $scope.transactionsTab.transactions.splice(index, 1);
                else
                    growl.success('Some error occured. Please try again.', {ttl:3000});
            });
        }
        else if(deletionType == 'Complete') {
            if(confirm('Think Twice!')) 
                if(confirm('Galti kari paap chadega!')) {
                    $scope.modalInstance = $modal.open({
                    templateUrl: 'deleteTransaction.html',
                    controller: 'DeleteTransactionCtrl',
                    size: 'sm',
                    backdrop: 'static',
                    resolve: {
                        transaction: function() {
                            return transaction;
                        },
                        deletionType : function() {
                            return deletionType;
                        },
                        deployment_id : function() {
                            return deployment_id;
                        }
                    }       
                    }).result.then(function (res) {
                        if(res.result == 'success')
                            $scope.transactionsTab.transactions = [];
                        else
                            growl.success('Some error occured. Please try again.', {ttl:3000});
                    }); 
                }
            }
        else
        {
            growl.error('Some error occured', {ttl:3000});
        }    
    }

  }])

.controller('DeleteTransactionCtrl', function($scope, $modalInstance, transaction, deletionType, deployment_id, growl, StockMasterUser) {
    $scope.deletionForm = {};
    console.log(transaction, deletionType, deployment_id);
    $scope.deletionType = deletionType;
    $scope.removeTransaction = function() {
        console.log('inside remove transaction', $scope.deletionForm);
        if($scope.deletionForm.comment)
        {
            if($scope.deletionForm.password && $scope.deletionForm.password == 'stock@123')
            {
                if(deletionType == 'Single')
                {
                    var req = {
                        transaction: transaction,
                        comment: $scope.deletionForm.comment,
                        deployment_id : deployment_id,
                        deletionType : deletionType
                    }
                    StockMasterUser.deleteAllTransactions(req ,function(result) {
                        if(result.message)
                        {
                            growl.success(result.message, {ttl:3000});
                            $modalInstance.close({result: 'success'});
                        }
                        else
                        {
                            console.log('error while deleting', result);
                            $modalInstance.close({result: 'failure'});
                        }
                    });
                }
                else if(deletionType == 'Complete')
                {
                    var req = {
                        comment : $scope.deletionForm.comment,
                        deployment_id : deployment_id,
                        deletionType : deletionType
                    }
                    StockMasterUser.deleteAllTransactions(req ,function(result) {
                        if(result.message)
                        {
                            growl.success(result.message, {ttl:3000});
                            $modalInstance.close({result: 'success'});
                        }
                        else
                        {
                            console.log('error while deleting', result);
                            $modalInstance.close({result: 'failure'});
                        }
                    });
                }
                else
                {
                    growl.error('Some error occured', {ttl :3000})
                }
            }
            else
            {
                growl.error('Please provide correct password.', {ttl:3000})
            }    
        }
        else
        {
            growl.error('Please provide correct comment.', {ttl:3000})
        }
    }
    
    $scope.cancel = function() {
        $modalInstance.close();
    };

});
