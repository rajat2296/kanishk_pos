'use strict';

describe('Controller: SuperadminCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var SuperadminCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SuperadminCtrl = $controller('SuperadminCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
