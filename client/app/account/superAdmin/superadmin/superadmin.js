'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('superadmin', {
        url: '/superadmin',
        templateUrl: 'app/account/superAdmin/superadmin/superadmin.html',
        controller: 'SuperadminCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              var flag = true;
              return Auth.getCurrentUser().$promise.then(function (user) {
                if (!(user.role == 'owner')) {
                  growl.error('00ps !! have permission ?', {ttl: 3000});
                  flag = true;
                  // $location.path('/login');
                }
                else{
                  //if(!(user.username=="test_ranjeet" || user.username=="test_tanvee" || user.username=="posist_outlet_kanishk")){
                if(!(user.username=="test_ranjeet" )){
                    growl.error('00ps !! have permission ?', {ttl: 3000});
                    flag = true;
                    // $location.path('/login');
                  }
                //  console.log(user);
                }
                if(flag)
                  return user;
              });

            }],
          deployment: ['Deployment', 'localStorageService', function (Deployment, localStorageService){
            return Deployment.findOne({id: localStorageService.get('deployment_id')}, function (result){
              return result;
            })
          }]
        }
    })
  });
