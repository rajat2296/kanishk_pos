'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/account/login/login.html',
        controller: 'LoginCtrl',
        authenticate: false,
        resolve: {
          subdomain: ['$location', 'subdomain', function ($location, subdomain) {
            if (subdomain === null) {
              $location.path('/');
            }
          }]
        }
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/account/signup/signup.html',
        controller: 'SignupCtrl'
      })
      .state('authentication', {
        url: '/authentication/:authId',
        templateUrl: 'app/account/authentication/authenticate.html',
        controller: 'AuthenticationCtrl',
        resolve: {
          authUser: ['$state', '$stateParams', 'Auth',
            function ($state, $stateParams, Auth) {
              /*return Auth.getCurrentUser().$promise.then(function (user) {
               console.log(user);
               return user;
               });*/
            }]
        }

      })
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsCtrl',
        authenticate: true
      });
  });
