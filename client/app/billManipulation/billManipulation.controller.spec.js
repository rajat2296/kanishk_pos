'use strict';

describe('Controller: BillManipulationCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var BillManipulationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BillManipulationCtrl = $controller('BillManipulationCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
