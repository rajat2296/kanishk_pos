'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('billManipulation', {
        url: '/billmanipulation',
        templateUrl: 'app/billManipulation/billManipulation.html',
        controller: 'BillManipulationCtrl',
        /*'resolve:{
          Items:['$scope','Item','localStorageService',function($scope,Item,localStorageService){
          return   Item.get({tenant_id: localStorageService.get('tenant_id'), deployment_id: $scope.deployment._id}, function (items) {

            });
            }]
        }*/
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              var flag = true;
              return Auth.getCurrentUser().$promise.then(function (user) {
               //  if (user.role != 'superadmin') {
               // //   if (user.deployment_id == $stateParams.deploymentId) {
               //      if (!Utils.hasUserPermission(user.selectedPermissions, 'Menu')) {
               //        growl.error('00ps !! have permission ?', {ttl: 3000});
               //        flag = false;
               //        $location.path('/login');
               //      }
               //   /* } else {
               //      growl.error('00ps !! have permission ?', {ttl: 3000});
               //      flag = false;
               //      $location.path('/login');
               //    }*/
               //  }
                if (flag)
                  return user;
              });
            }]
        }
      });
  });