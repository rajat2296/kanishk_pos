'use strict';

angular.module('posistApp')
  .controller('BillManipulationCtrl', function ($scope,$http,$rootScope,growl,billResource,Item,localStorageService,Utils,Bill,$timeout,currentUser) {
      $scope.items=[];
        $scope.isDeployment=false;
        $scope.disableModification=false;
       if( localStorageService.get('deployment_id')!=null){
           Item.getItemsName({tenant_id: localStorageService.get('tenant_id'), deployment_id: localStorageService.get('deployment_id')}, function (items) {
                $scope.items=items;
               $scope.isDeployment=true;
           });
       }else{
         growl.error('Deployment is not set',{ttl:2000});
       }
    $scope.cards=['SmartCard','CreditCard','FullCoupon','DebitCard'];
    $scope.selectedCards=[];
    $scope.selectedHead=null;
      $scope.SelectedType=function(type){
        $scope.selectedHead=type;
      };
    $scope.deletionType=null;
      $scope.billModify={
        modification:{fromDate:'',toDate:'',billAmount:'',type:'percent'},
        deletion:{dateWise:{fromDate:'',toDate:''},itemWise:{fromDate:'',toDate:'',items:[]},billWise:{fromDate:'',seriesFrom:'',seriesTo:''}},
        cards:{smartCard:false,creditCard:false,fullCoupon:false,debitCard:false},
        history:{fromDate:'',toDate:''}
      };
      $scope.dateCal={modFrom:false,modTo:false,delDateFrom:false,delDateTo:false,delItemFrom:false,delItemTo:false,delBillFrom:false,delBillTo:false,historyFromDate:false,historyToDate:false};
      //$scope.openedValidTo=false;
      $scope.openCalender = function($event,type) {
      //  console.log($scope.dateCal);
        $event.preventDefault();
        $event.stopPropagation();
        if(type=='modFrom'){$scope.dateCal.modFrom= true;$scope.dateCal.modTo= false;}
        if(type=='modTo'){$scope.dateCal.modTo= true;$scope.dateCal.modFrom= false;}
        if(type=='delDateFrom')$scope.dateCal.delDateFrom= true;
        if(type=='delDateTo')$scope.dateCal.delDateTo= true;
        if(type=='delItemFrom')$scope.dateCal.delItemFrom= true;
        if(type=='delItemTo')$scope.dateCal.delItemTo= true;
        if(type=='delBillFrom')$scope.dateCal.delBillFrom= true;
          if(type=='historyFrom')$scope.dateCal.historyFromDate= true;
          if(type=='historyTo')$scope.dateCal.historyToDate= true;

      };
      $scope.dateOptions ={
        formatYear: 'yy',
        startingDay: 1
      };

        $scope.multipleItem = {};
        $scope.multipleItem.items=[];// ['Blue','Red'];

        $scope.progress=0;
        $scope.status='';
        $scope.totalAmount=0;
        $scope.amountAdjusted=0;
        $scope.doWork=function(){
           /* console.log($scope.selectedCards);
            $scope.checkCards($scope.selectedCards);
            return false;*/

            if($scope.selectedHead=='modification'){
                if($scope.billModify.modification.fromDate==null||$scope.billModify.modification.fromDate==''){
                    growl.error('Feed in start date must.',{ttl:2000});
                    return false;
                }
                if($scope.billModify.modification.toDate==null||$scope.billModify.modification.toDate==''){
                    growl.error('Feed in end date must.',{ttl:2000});
                    return false;
                }
                if($scope.billModify.modification.billAmount==null||$scope.billModify.modification.billAmount==''||$scope.billModify.modification.billAmount<0){
                    growl.error('Feed in amount.',{ttl:2000});
                    return false;
                }
                if($scope.billModify.modification.type=='percent'&& $scope.billModify.modification.billAmount>100){
                    growl.error('Percent should not exceed 100.',{ttl:2000});
                    return false;
                }
                $scope.disableModification=true;
                $scope.progress=1;
                $scope.totalAmount=0;
                $scope.amountAdjusted=0;
                var startDate=Utils.getResetDate(localStorageService.get('settings'),$scope.billModify.modification.fromDate);
                var toDate= Utils.getResetDate(localStorageService.get('settings'),$scope.billModify.modification.toDate);
                var dep_id=localStorageService.get('deployment_id');
                $scope.progress=4;
                toDate = new Date( toDate.setDate(toDate.getDate() + 1));
                billResource.billsTotalAmount({startDate:startDate,endDate:toDate,deployment_id:dep_id}).$promise.then(function(gTotal){
                  console.log(gTotal);
                   if(gTotal.length>0) {
                       $scope.totalAmount =Math.round( gTotal[0].gTotal);
                       $scope.status = status;
                       $scope.progress = 7;
                       $scope.manipulateBillsRecursive(startDate,toDate,dep_id);
                   }else{
                      // $scope.totalAmount =Math.round( gTotal[0].gTotal);
                       $scope.status = status;
                       $scope.progress = 100;
                       $scope.disableModification=false;
                       growl.error('Nothing to manipulate.',{ttl:2000});
                   }
                });
            }
            if($scope.selectedHead=='deletion') {
                console.log($scope.deletionType);
                if ($scope.deletionType == 'datewise') {
                    if( $scope.billModify.deletion.dateWise.fromDate==''|| $scope.billModify.deletion.dateWise.fromDate==null||$scope.billModify.deletion.dateWise.toDate==''|| $scope.billModify.deletion.dateWise.toDate==null){
                        growl.error('Some of the fields are blank.',{ttl:2000});
                        return false;
                    }
                    $scope.disableModification=true;
                    var unselected = [];
                    for (var i = 0; i < $scope.cards.length; i++) {
                        var isFound = false;
                        for (var j = 0; j < $scope.selectedCards.length; j++) {
                            if ($scope.cards[i] == $scope.selectedCards[j]) {
                                isFound = true;
                                break;
                            }
                        }
                        if (!isFound) {
                            unselected.push(angular.copy($scope.cards[i]));
                        }
                    }

                    var startDate = Utils.getResetDate(localStorageService.get('settings'), $scope.billModify.deletion.dateWise.fromDate);
                    var toDate =new Date( Utils.getResetDate(localStorageService.get('settings'), $scope.billModify.deletion.dateWise.toDate));
                    var dep_id = localStorageService.get('deployment_id');
                    billResource.markDeleteBill({
                        startDate: startDate,
                        endDate: new Date( angular.copy( toDate.setDate(toDate.getDate()+1)) ),
                        deployment_id: dep_id,
                        cards: unselected.length == 0 ? null : unselected,
                        type:'dateWise'
                    }).$promise.then(function (result) {
                        console.log(result);
                        growl.success('Deleted successfully.', {ttl: 3000});
                            $scope.disableModification=false;
                    })
                }
                if($scope.deletionType=='billwise'){
                   if( $scope.billModify.deletion.billWise.serialFrom==''||$scope.billModify.deletion.billWise.serialFrom==null||$scope.billModify.deletion.billWise.serialTo==''||$scope.billModify.deletion.billWise.serialTo==null|| $scope.billModify.deletion.billWise.fromDate==''|| $scope.billModify.deletion.billWise.fromDate==null){
                        growl.error('Some of the fields are blank.',{ttl:2000});
                       return false;
                   }
                    $scope.disableModification=true;
                    var unselected = [];
                    for (var i = 0; i < $scope.cards.length; i++) {
                        var isFound = false;
                        for (var j = 0; j < $scope.selectedCards.length; j++) {
                            if ($scope.cards[i] == $scope.selectedCards[j]) {
                                isFound = true;
                                break;
                            }
                        }
                        if (!isFound) {
                            unselected.push(angular.copy($scope.cards[i]));
                        }
                    }

                    var stDate =  Utils.getResetDate(localStorageService.get('settings'), $scope.billModify.deletion.billWise.fromDate);
                   var tstDate= angular.copy(stDate);
                    var tDate=angular.copy( tstDate.setDate(tstDate.getDate()+1));
                    console.log(stDate);
                    var dep_id = localStorageService.get('deployment_id');
                    billResource.markDeleteBill({
                        startDate: angular.copy(stDate),
                        endDate:new Date( tDate),
                        deployment_id: dep_id,
                        cards: unselected.length == 0 ? null : unselected,
                        serialFrom:$scope.billModify.deletion.billWise.serialFrom,
                        serialTo:$scope.billModify.deletion.billWise.serialTo,
                        type:'billWise'
                    }).$promise.then(function (result) {
                            growl.success('Deleted successfully.', {ttl: 3000});
                            $scope.disableModification=false;
                            $scope.amountCalculation(stDate,tDate);
                        })
                }
            }
        }
        $scope.manipulateBillsRecursive=function(startDate,toDate,dep_id){

            billResource.billsForManipulation({startDate:angular.copy( startDate),endDate:angular.copy(toDate),deployment_id:angular.copy(dep_id)}).$promise.then(function(bills){
                //$scope.progress=18;
                //console.log(bills);
                var billAmount=angular.copy($scope.billModify.modification.billAmount) ;

                if($scope.billModify.modification.type=='percent')
                billAmount=$scope.billModify.modification.billAmount*$scope.totalAmount*.01;
                if($scope.getKotFinished(bills)){
                    growl.error('There is no scope to manipulate more.',{ttl:4000});
                    $scope.progress=100;
                    $scope.amountCalculation(startDate,toDate);
                    $scope.disableModification=false;
                    return false;
                }
                if(billAmount>($scope.totalAmount-$scope.amountAdjusted)){
                    growl.error('There is no scope to manipulate more.',{ttl:4000});
                    $scope.progress=100;
                    $scope.amountCalculation(startDate,toDate);
                    $scope.disableModification=false;
                    return false;
                }else{
                 $scope.progress=12+ Math.round(  billAmount/($scope.totalAmount-$scope.amountAdjusted)*80);
                }
                var mBills=[];
                _.forEach(bills,function(bill){
                    var mBill;
                    if(!$scope.checkCards(bill.payments)) {
                        if (bill._kots.length > 1) {
                            var tKot = bill._kots[0];
                            mBill = angular.copy(bill);
                            mBill._kots = [];
                            mBill._kots.push(tKot);
                        } else {
                            if (bill._kots.length == 1) {
                                if (bill._kots[0].items.length > 2) {
                                    var tItems = [];
                                    var sortedItems = _.sortBy(bill._kots[0].items, ['subtotal']);
                                    //console.log(sortedItems);
                                    bill._kots[0].items = sortedItems;
                                    _.forEach(bill._kots[0].items, function (item, itemIndex) {
                                        if (itemIndex > 1) {
                                        } else {
                                            tItems.push(item);
                                        }
                                    })
                                    mBill = angular.copy(bill);
                                    mBill._kots[0].items = tItems;
                                } else {
                                    if (bill._kots[0].items.length > 1) {
                                        var t1Items = [];
                                        var tQty = 0;
                                        _.forEach(bill._kots[0].items, function (itemq, itemqIndex) {
                                            tQty += itemq.quantity;
                                        });
                                        if (tQty > 2) {
                                            _.forEach(bill._kots[0].items, function (item1, item1Index) {
                                                var tItem1 = item1;
                                                tItem1.quantity = 1;
                                                if (_.has(tItem1, 'discounts')) {
                                                    delete tItem1.discounts;
                                                }
                                                t1Items.push(tItem1);
                                            })
                                        } else {
                                            if (bill._kots[0].items[0].subtotal > bill._kots[0].items[1].subtotal) {
                                                t1Items.push(bill._kots[0].items[1]);
                                                // $scope.amountAdjusted += (bill._kots[0].items[0].subtotal);
                                            } else {
                                                t1Items.push(bill._kots[0].items[0]);
                                                //$scope.amountAdjusted += (bill._kots[0].items[1].subtotal);
                                            }
                                        }
                                        mBill = angular.copy(bill);
                                        mBill._kots[0].items = t1Items;
                                    }
                                }
                            }
                        }
                    }
                    var billService=new Bill();

                   // mBill._id=mBill.ng_id;
                   if(!(mBill==undefined||mBill==null)) {
                       if(_.has(mBill,'_offers')){
                           mBill._offers=[];}
                       if(_.has(mBill,'billDiscountAmount')){
                           mBill.billDiscountAmount=0;
                       }
                       /*discard all item wise offer*/
                       _.forEach(mBill._kots,function(kot){
                           _.forEach(kot.items,function(item){
                               if(_.has(item.discounts)){
                                   delete item.discounts;
                               }
                           })
                       })

                       ///////////////////////////////////////////////////
                       /*Back up of main data*/
                       if(!_.has(bill.copy_id,'_kots')) {
                           mBill.copy_id = {
                               date: Utils.getDateFormatted(new Date()),
                               _kots: angular.copy(bill._kots),
                               payments: angular.copy(bill.payments),
                               billDiscountAmount: angular.copy(bill.billDiscountAmount),
                               _offers: angular.copy(bill._offers),
                               isModified: true
                           };
                       }
                       /////////////////////////////////////////////////
                       _.extend(billService, mBill);
                       billService.processBill();
                      // console.log("Billamount:"+$scope.calculateAmount(bill)+"  amnipulatedAmount:"+$scope.calculateAmount(billService));
                       if(billAmount<=($scope.totalAmount-$scope.amountAdjusted)) {
                           $scope.amountAdjusted += Utils.roundNumber( ( $scope.calculateAmount(bill) - $scope.calculateAmount(billService)),0);
                           billService.payments = {
                               cash: [Utils.roundNumber($scope.calculateAmount(billService), 0)],
                               cards: []
                           };
                           mBills.push(billService);
                       }
                   }
                });
                if(mBills.length>0) {
                  //  console.log(mBills);
                    $scope.updateBillOnServer(mBills, 0, startDate, toDate, dep_id);
                }
                else{
                    var flag=false;
                   /* for(var i=0;i<bills.length;i++){
                        if(bills[i]._kots.length>1){flag=true;;break;}
                        for(var j=0;j<bills[i]._kots.length;j++){
                            if(bills[i]._kots[j].items.length>1){flag=true;;break;}
                            else{ if(bills[i]._kots[j].items[0].quantity>1){flag=true;break;} }
                        }
                    }
                    if(flag){$scope.manipulateBillsRecursive(startDate,toDate,dep_id);}else {*/
                        growl.success('can not be manipulated more', {ttl: 300});
                        $scope.progress = 100;
                        $scope.disableModification=false;
                    /*}*/
                }
               /// console.log(mBills);
                /*var bill=new Bill();
                //  bill=bills[10];
                _.extend(bill,bills[10]);
                console.log(bill);
                bill._kots[0].items.splice(2,1);
                bill.processBill();
                console.log(bill);
                //   bill.processBill();*/
            })
        }

        $scope.updateBillOnServer=function(allBills,count,startDate,toDate,dep_id){
            billResource.updateBill({bill: allBills}).$promise.then(function(id){
             //   console.log(id.billId);
                //        $rootScope.db.update('bills',{"isSynced" : "true", "syncedOn" : new Date().toISOString()},{"billId": id.billId}).then(function(success){
          /*      if(count<allBills.length-1){
                    count=count+1;
                    $scope.updateBillOnServer(allBills,count,startDate,toDate,dep_id);
                }else
                {*/
               // $timeout(function(){
                    $scope.manipulateBillsRecursive(startDate,toDate,dep_id);
//                },1000);

                /*}*/
            })
        }

        $scope.getKotFinished=function(bills){
          var  isFinished=true;
            for(var i=0;i<bills.length;i++){
                if(bills[i]._kots.length>0){
                    isFinished=false;return false;
                }
                for(var j=0;j<bills[i]._kots.length;j++){
                    if(bills[i]._kots[j].items.length>0){
                        isFinished=false;return false;
                    }
                    for(var k=0;k<bills[i]._kots[j].items.length;k++){
                        if(bills[i]._kots[j].items[k].quantity>1){
                            isFinished=false;return false;
                        }
                    }
                }
            }
            return isFinished;
        }
        $scope.reverseTransaction=function() {
            if($scope.billModify.history.fromDate==''||$scope.billModify.history.fromDate==null||$scope.billModify.history.toDate==''||$scope.billModify.history.toDate==null){
                growl.error('Some of the dates not selected.',{ttl:2000});
                return false;
            }
            var startDate = Utils.getResetDate(localStorageService.get('settings'), $scope.billModify.history.fromDate);
            var toDate =new Date( Utils.getResetDate(localStorageService.get('settings'), $scope.billModify.history.toDate));
            $scope.amountAdjusted=0;
            $scope.disableModification=true;

            billResource.billsRestore({
                dep_id: localStorageService.get('deployment_id'),
                startDate: angular.copy(startDate),
                endDate: angular.copy(toDate.setDate(toDate.getDate()+1))
            }, function (result) {
                console.log(result);
                growl.success('Transaction reversed to the original one.', {ttl: 3000});
                $scope.disableModification=false;
                $scope.amountCalculation(startDate,toDate);
            })
        }
        $scope.amountCalculation=function(startDate,toDate){
            billResource.billsTotalAmount({
                startDate: startDate,
                endDate: toDate,
                deployment_id:localStorageService.get('deployment_id')
            }).$promise.then(function (gTotal) {
                    if (gTotal.length > 0) {
                        $scope.totalAmount = Math.round(gTotal[0].gTotal);
                    }
                })
        }
        $scope.calculateAmount=function(bill){
            var oBillamount=0;
            var EBillamount=0;
            for(var i=0;i<bill._kots.length;i++){
                oBillamount+=(bill._kots[i].totalAmount+bill._kots[i].taxAmount-bill._kots[i].totalDiscount);
            }
            oBillamount-=bill.billDiscountAmount;
            /*for(var i=0;i<eBill._kots.length;i++){
                EBillamount+=(eBill._kots[i].totalAmount+eBill._kots[i].taxAmount-eBill._kots[i].totalDiscount);
            }
            EBillamount-=eBill.billDiscountAmount;
            return oBillamount-EBillamount;*/
            return oBillamount;
        }
        $scope.checkCards=function(payment){
           var billCards=[];
            if(_.has(payment,'cards')){
                billCards=payment.cards;
            }
            var unselected=[];
            for(var i=0;i<$scope.cards.length;i++){
                //var filter= _.filter($scope.selectedCards,$scope.cards[i]);
                var isFound=false;
                for(var j=0;j<$scope.selectedCards.length;j++){
                    if($scope.cards[i]==$scope.selectedCards[j]){
                        isFound=true;
                        break;
                    }
                }
                if(!isFound){
                    unselected.push(angular.copy( $scope.cards[i]));
                }
            }
            console.log(unselected);
            var isTransactionFound=false;
            for(var i=0;i<unselected.length;i++){
                for(var j=0;j<billCards.length;j++){
                    if(billCards[j].cardType==unselected[i]){
                        if(billCards[j].totalAmount>0){
                            isTransactionFound=true;
                            break;
                        }
                    }
                }
            }
            return isTransactionFound;
        }
        $scope.DeletionType=function(type){
            $scope.deletionType=type;
        }
    });
