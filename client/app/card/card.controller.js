'use strict';

angular.module('posistApp')
    .controller('CardCtrl', ['$scope', 'currentUser', '$timeout', '$http', 'growl', '$rootScope', '$ngBootbox','$modal','Utils','printer','$q', function ($scope, currentUser, $timeout, $http, growl, $rootScope, $ngBootbox,$modal,Utils,printer,$q) {
        var deployment_id = currentUser.deployment_id;
        $scope.sAmount=0;
        if(_.has(currentUser,'cardEntity')){
        if(_.has(currentUser.cardEntity,'security')){
                if(parseInt( currentUser.cardEntity.security)>0){
                    $scope.sAmount=parseInt( currentUser.cardEntity.security);
                }
           }
        }
        
        $scope.registration = {
            cardId: '',
            formNumber: '',
            deployment_id: currentUser.deployment_id,
            name: '',
            mobile: '',
            securityAmount: 0,
            entryFee: 0,
            amount: '',
            user: {name: currentUser.username, userId: currentUser._id},
            transactions: [],
            paymentType:'Cash'
        }
        $scope.paymentTypes=['Cash','CreditCard','DebitCard','OtherCard'];
        $scope.addCard = {balance: '', addAmount: '',detail:{},paymentType:'Cash'};
        $scope.rCard = {balance: '',detail:{}};
        $scope.setFocus = {focusInput: false, form: false, add: false, focusInputM: false, tran: false};
        $scope.$on('$viewContentLoaded', function (event) {
            $timeout(function () {
                $scope.setFocus.focusInput = true;
            }, 300);
        });
        $scope.inProcess = false;
        $scope.selectedCardId = '';
        $scope.selectedMobile = '';
        $scope.mobile = '';
        $scope.tCard = {};
        $scope.newCardId = '';
        $scope.selectedButton = 'addAmount';
        $scope.doFalseAll = function () {
            $scope.setFocus.add = false;
            $scope.setFocus.form = false;
            $scope.setFocus.trans = false;
            $scope.setFocus.formInputM = false;
            $scope.setFocus.focusInput = false;
        }
        $scope.doClick = function (selectedButton) {
            if(selectedButton=='settleCards'){
                $scope.doSettleAll();
                return false;
            }
            $scope.selectedButton = angular.copy(selectedButton);
            $scope.selectedCardId = '';
            $scope.selectedMobile = '';
            $scope.cardId = '';
            $scope.mobile = '';
            $scope.newCardId = '';
            if ($scope.selectedButton == 'transfer') {
                $scope.doFalseAll();
                $timeout(function () {
                    $scope.setFocus.focusInputM = false;
                }, 100);
                //$scope.setFocus.formInputM = false;
                $timeout(function () {
                    $scope.setFocus.focusInputM = true;
                }, 200);
            } else {
                $scope.doFalseAll();
                $timeout(function () {
                    $scope.setFocus.focusInput = true;
                }, 200);
            }
        }
        $scope.doSettleAll=function(){
            $ngBootbox.confirm('You are going to close all the opened card.Are you sure?').then(function(){
               $scope.passCodeModal().then(function(pass){
                    if(pass==currentUser.passcode){
                        $http.post($rootScope.url + '/api/synckots/closeOpenedCard', {
                  }).then(function (result) {
                    //console.log(result);
                    growl.error(result.data.rowsModified.toString() + ' cards has been closed.', {ttl: 3000});
                    
                });
                    }else{
                        growl.error('Wrong passcode!!',{ttl:2000});
                    }
                })
            })
        }
        $scope.cardEntered = function () {
            console.log($scope.cardId);
            if ($scope.inProcess == true) {
                growl.error('Let us finish the last work', {ttl: 1000});
                return false
            }
            $scope.inProcess = true;
            if ($scope.selectedButton == 'registration') {
                $http.post($rootScope.url + '/api/synckots/checkCardIfExist', {
                    cardId: $scope.cardId,
                    deployment_id: deployment_id
                }).then(function (result) {
                    console.log(result);
                    growl.error('CardId already exist !!', {ttl: 3000});
                    $scope.cardId='';
                    $scope.inProcess = false;
                }, function (err) {
                    console.log(err);
                    if (err.data.err == 'na') {
                        $scope.selectedCardId = angular.copy($scope.cardId);
                        $scope.doFalseAll();
                        $timeout(function () {
                            $scope.setFocus.form = true;
                        }, 200);

                    } else {
                        growl.error('There is problem accessing server', {ttl: 3000});
                    }
                    $scope.inProcess = false;
                });
            } else {
                $http.post($rootScope.url + '/api/synckots/checkCardIfExist', {
                    cardId: $scope.cardId,
                    deployment_id: deployment_id
                }).then(function (result) {
                    console.log(result);

                    $scope.selectedCardId = angular.copy($scope.cardId);
                    if ($scope.selectedButton == 'addAmount') {
                        $scope.addCard.balance = result.data.balance;
                        $scope.addCard.detail = result.data.detail;
                        //console.log( $scope.addCard.detail);
                        $scope.doFalseAll();
                        $timeout(function () {
                            $scope.setFocus.add = true;
                        }, 200);

                    }
                    if ($scope.selectedButton == 'refund') {
                        $scope.rCard.balance = result.data.balance;
                        $scope.rCard.detail = result.data.detail;
                    }
                    $scope.inProcess = false;
                }, function (err) {
                    if (err.data.err == 'na') {
                        growl.error('CardId does not exist !!', {ttl: 3000});
                    } else {
                        growl.error('There is problem accessing the server !!', {ttl: 3000});
                    }
                    $scope.inProcess = false;
                })
            }
        }
        $scope.mobileEntered = function () {
            if ($scope.inProcess == true) {
                growl.error('Let us finish the last work', {ttl: 1000});
                return false
            }
            $scope.inProcess = true;

            $http.post($rootScope.url + '/api/synckots/checkMobileIfExist', {
                mobile: $scope.mobile,
                deployment_id: deployment_id
            }).then(function (result) {
                console.log(result);
                $scope.selectedMobile = $scope.mobile;
                $scope.tCard = result.data;
                $scope.doFalseAll();
                $timeout(function () {
                    $scope.setFocus.trans = true;
                }, 200);
                $scope.inProcess = false;
            }, function (err) {
                if (err.data.err == 'cardnotfound') {
                    growl.error('mobile not found.', {ttl: 2000});
                } else {
                    growl.error('There are some problem fetching data.', {ttl: 2000});
                }
                $scope.inProcess = false;
            });
        }
        $scope.addCardD = function () {

            if ($scope.addCard.addAmount == '') {
                growl.error('Amount can not be null !!', {ttl: 2000});
                return false;
            }
            if (!parseFloat(angular.copy($scope.addCard.addAmount))) {
                growl.error('Error in parsing amount !!', {ttl: 2000});
                return false;
            }
            if ($scope.inProcess == true) {
                growl.error('Let us finish the last work', {ttl: 1000});
                return false
            }
            $scope.inProcess = true;

            var transactionOb = $scope.newTransactionOb();
            transactionOb.amount = parseFloat(angular.copy($scope.addCard.addAmount));
            transactionOb.type = 'Recharge';
            transactionOb.paymentType = $scope.addCard.paymentType;

            transactionOb.cardId = angular.copy($scope.selectedCardId);
            $http.post($rootScope.url + '/api/synckots/updateCardTransactions', transactionOb).then(function (result) {
                console.log(result);
                if (result.data.updated == 1) {
                    growl.success('Card Recharged successfully..', {ttl: 2000});
                    $scope.printSlip('Recharge',result.data.time,$scope.addCard);
                    $scope.clearAddCard();
                } else {
                    growl.error('server error!!', {ttl: 2000});
                }
                $scope.inProcess = false;
            },function(err){
                console.log(err);
                growl.error(err.data.err,{ttl:3000});
                $scope.inProcess = false;
                $scope.clearAddCard();
            });
        }
        $scope.refundCardPayment=function() {
            if ($scope.inProcess == true) {
                growl.error('Let us finish the last work', {ttl: 1000});
                return false
            }
            $scope.inProcess = true;
            $ngBootbox.confirm('Are you sure to refund this card').then(function (result) {


                var transactionOb = $scope.newTransactionOb();
                transactionOb.amount =( parseFloat(angular.copy($scope.addCard.addAmount))+parseFloat($scope.sAmount));
                transactionOb.paymentType=$scope.addCard.paymentType;
                transactionOb.type = 'Refund';
                transactionOb.cardId = angular.copy($scope.selectedCardId);
                $http.post($rootScope.url + '/api/synckots/refundCardTransactions', transactionOb).then(function (result) {
                    console.log(result);
                    if (result.data.updated == 1) {
                        // $scope.clearRefundCard();
                        growl.success('Card Refunded successfully..', {ttl: 2000});
                        $scope.printSlip('Refund',result.data.time,angular.copy( $scope.addCard ));
                        $scope.clearAddCard();
                    } else {
                        growl.error('server error!!', {ttl: 2000});
                    }
                    $scope.inProcess = false;
                },function(err){
                     growl.error('server error!!', {ttl: 2000});
                     $scope.clearAddCard();
                     $scope.inProcess = false;   
                });
            },function(err){
                growl.error('Cancelled refund.', {ttl: 2000});
                $scope.inProcess = false;
            })
        }
        $scope.refundCard = function () {
            if ($scope.inProcess == true) {
                growl.error('Let us finish the last work', {ttl: 1000});
                return false
            }
            $scope.inProcess = true;

            var transactionOb = $scope.newTransactionOb();
            transactionOb.amount = parseFloat(angular.copy($scope.rCard.balance));
            transactionOb.type = 'Refund';
            transactionOb.cardId = angular.copy($scope.selectedCardId);
            $http.post($rootScope.url + '/api/synckots/refundCardTransactions', transactionOb).then(function (result) {
                console.log(result);
                if (result.data.updated == 1) {
                    $scope.clearRefundCard();
                } else {
                    growl.error('server error!!', {ttl: 2000});
                }
                $scope.inProcess = false;
            });
        }

        $scope.clearRefundCard = function () {
            $scope.selectedCardId = '';
            $scope.cardId = '';

        }
        $scope.showCardPayment=function() {
            $scope.inProcess = true;
            $http.post($rootScope.url + '/api/synckots/getCardByCardId', {deployment_id:currentUser.deployment_id,cardId:$scope.selectedCardId}).then(function (result) {
                $scope.inProcess = false;
                console.log(result.data);
                var d=result.data;
                var trans= d.transactions;
                var tTotal=0;
               var html='<table class="table table-responsive table-condensed table-striped ">';
                html+='<tr><td>Card Id:</td><td>'+ d.cardId+'</td><td>Name:</td><td>'+ d.name+'</td><td>Mobile</td><td>'+ d.mobile+'</td></tr></table>';
                html+='<table class="table table-responsive table-condensed table-striped "><tr><th>Type</th><th>Time</th><th>User Name</th><th>Bill</th><th>Kot</th><th>Amount</th></tr>';
                 // html+='<tr><td>Security</td><td>'+Utils.getDateFormatted(new Date(d.created))+'</td><td>'+d.user.name+'</td><td></td><td></td><td>'+d.securityAmount+'</td></tr>';
                    
                _.forEach(trans,function(t){
                    tTotal+= t.amount;
                    var billNo='';
                    if(_.has(t,'billDetail')){
                        if(_.has(t.billDetail,'instance')){
                            if(_.has(t.billDetail.instance,'preFix')){
                                billNo+=t.billDetail.instance.preFix;
                            }
                            if(_.has(t.billDetail,'serialNumber')){
                                billNo+=t.billDetail.serialNumber;
                            }
                        }
                    }

                    if(t.type=='Registration'){
                        html+='<tr><td>Registration</td><td>'+ Utils.getDateFormatted(new Date(t.serverTime)) +'</td><td>'+t.user.name+'</td><td></td><td></td><td>'+t.amount+'</td></tr>';
                    }
                      if(t.type=='EntryFee'){
                        html+='<tr><td>EntryFee</td><td>'+ Utils.getDateFormatted(new Date(t.serverTime)) +'</td><td>'+t.user.name+'</td><td></td><td></td><td>'+t.amount+'</td></tr>';
                    }
                    if(t.type=='Recharge'){
                        html+='<tr><td>Recharge</td><td>'+Utils.getDateFormatted(new Date(t.serverTime))+'</td><td>'+t.user.name+'</td><td></td><td></td><td>'+t.amount+'</td></tr>';
                    }
                    if(t.type=='billTransaction'){
                        html+='<tr><td>Bill Transaction</td><td>'+Utils.getDateFormatted(new Date(t.serverTime))+'</td><td>'+t.user.name+'</td><td>'+(billNo)+'</td><td>'+ t.billDetail.kotNumber+'</td><td>'+t.amount+'</td></tr>';
                    }
                    if(t.type=='itemDeleteTransaction'){
                        html+='<tr><td>Item Deletion</td><td>'+Utils.getDateFormatted(new Date(t.serverTime))+'</td><td>'+t.user.name+'</td><td>'+(billNo)+'</td><td>'+ t.billDetail.kotNumber+'</td><td>'+t.amount+'</td></tr>';
                    }
                    if(t.type=='voidKotTransaction'){
                        html+='<tr><td>Kot Void</td><td>'+Utils.getDateFormatted(new Date(t.serverTime))+'</td><td>'+t.user.name+'</td><td>'+(billNo)+'</td><td>'+ t.billDetail.kotNumber+'</td><td>'+t.amount+'</td></tr>';
                    }
                })
                
                html+='<tr><td colspan="5" style="font-weight: bold">Refund Amount:</td><td  style="font-weight: bold">'+( parseFloat(tTotal)).toFixed(2)+'</td></tr>';
                html+='<tr><td colspan="5" style="font-weight: bold">Current Balance(W/O Security:-'+d.securityAmount+')</td><td  style="font-weight: bold">'+( parseFloat(tTotal)-parseFloat( d.securityAmount)).toFixed(2)+'</td></tr>';
               
                html+='</table>';

                $modal.open({
                    templateUrl: 'app/billing/item/_showCardTransactions.html',
                    controller:['$scope','$modalInstance','html','$timeout', function ($scope, $modalInstance,html,$timeout) {

                        $timeout( function() {
                            document.getElementById('showtrans1').innerHTML=html;
                        },0);
                        $modalInstance.opened.then($scope.init);

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }],
                    size: 'md',
                    resolve:{
                        html:function() {
                            return html;
                        }
                    }
                })
              /*  modal.rendered.then(function () {
                    //process your logic for DOM element
                    document.getElementById('showtrans1').innerHTML=html;
                });*/
            })
        }
        $scope.newTransactionOb = function () {
            var transactionOb = {
                amount: '',
                type: '',
                user: {name: currentUser.username, userId: currentUser._id},
                deployment_id: currentUser.deployment_id,
                timeStamp: new Date(),
                cardId: '',
                itemDetail:{},
                billDetail:{},
                paymentType:''
            }
            return transactionOb;
        }
        $scope.clearAddCard = function () {
            $scope.addCard.addAmount = '';
            $scope.addCard.balance = '';
            $scope.addCard.paymentType = 'Cash';
            $scope.rCard.balance='';
            $scope.selectedCardId = '';
            $scope.cardId = '';
            $scope.doFalseAll();
            $timeout(function () {
                $scope.setFocus.focusInput = true;
            }, 200);
        }
        $scope.createRegistration = function (regForm) {
            if ($scope.registration.amount == '') {
                growl.error('Feed in amount please.', {ttl: 2000});
                return false;
            }
            if (!parseFloat($scope.registration.amount)) {
                growl.error('There is problem parsing amount..', {ttl: 2000});
                return false;
            }
            if ($scope.inProcess == true) {
                growl.error('Let us finish the last work', {ttl: 1000});
                return false
            }
            $scope.inProcess = true;

            $scope.cardId = '';
            $scope.registration.cardId = $scope.selectedCardId;
            //console.log(regForm);
            //console.log($scope.registration);
            var transactionOb = $scope.newTransactionOb();
            var entryFeeAmount=0;
            var securityAmt=0;
            transactionOb.amount = parseFloat($scope.registration.amount);
            if(!(currentUser.cardEntity==null ||currentUser.cardEntity==undefined)){
                if(_.has(currentUser.cardEntity,'security')){
                        $scope.registration.securityAmount= parseInt( currentUser.cardEntity.security);
                        if(parseInt( currentUser.cardEntity.security)>0){
                           
                           // transactionOb.amount=(transactionOb.amount-parseInt( currentUser.cardEntity.security));
                        }
                }
            }
            if(_.has(currentUser,'cardEntity')){
                if(_.has(currentUser.cardEntity,'entryFee')){
                        $scope.registration.entryFee=currentUser.cardEntity.entryFee;
                        if(parseInt( currentUser.cardEntity.entryFee)>0){
                            entryFeeAmount=parseInt( currentUser.cardEntity.entryFee);
                           // transactionOb.amount=(transactionOb.amount-entryFeeAmount);
                        }
                }
            }

            if((transactionOb.amount-($scope.registration.securityAmount+entryFeeAmount))<0){
                growl.error('Registration amount should be greater than security amount and entry fee.!!',{ttl:3000});
               $scope.inProcess = false;
                return false;
            }
        
            transactionOb.paymentType=$scope.registration.paymentType;
            transactionOb.type = 'Registration';
            transactionOb.timeStamp = new Date();
            transactionOb.cardId = angular.copy($scope.selectedCardId);
            $scope.registration.transactions.push(transactionOb);
           ///////////////////////////////////////Entry Fee//////////////////////
            var transactionOb = $scope.newTransactionOb();
            if(entryFeeAmount>0){
                transactionOb.amount=-(entryFeeAmount);
                transactionOb.paymentType=$scope.registration.paymentType;
                transactionOb.type = 'EntryFee';
                transactionOb.timeStamp = new Date();
                transactionOb.cardId = angular.copy($scope.selectedCardId);
                $scope.registration.transactions.push(transactionOb);
            }
           ///////////////////////////////////////Entry Fee End//////////////////

            $http.post($rootScope.url + '/api/synckots/createCardTransactions', $scope.registration).then(function (result) {
                growl.success('Registration done successfully.', {ttl: 2000});
                $scope.printSlip('Registration',result.data.transactions[0].serverTime,result.data);
                console.log(result);
                $scope.refreshForm(regForm);
                $scope.inProcess = false;
            }, function (err) {
                if (err.data.err == "mobileerror") {
                    growl.error('mobile number already registered.', {ttl: 2000});
                } else {
                    growl.error('There are some problem with the registration.', {ttl: 2000});
                }
                $scope.registration.transactions = [];
                $scope.inProcess = false;
            });
        }
        $scope.transferCard = function () {
            var code = (Math.random().toString(36) + '00000000000000000').slice(2, 5 + 2);
            var smsObj = {
                mobile: $scope.selectedMobile,
                message: 'One time code for Transfer card is ' + code + '. Please share the code to complete transfer.'
            }
            $http.post('http://test.posist.net/api/emails/sendSms', smsObj).success(function (data) {
                console.log(data);
            });
            $ngBootbox.prompt('Enter the code(SMS has been send to mobile entered.)').then(function (result) {
                //console.log(result);
                if (result == code) {
                    if ($scope.inProcess == true) {
                        growl.error('Let us finish the last work', {ttl: 1000});
                        return false
                    }
                    $scope.inProcess = true;

                    $http.post($rootScope.url + '/api/synckots/transferCardTransaction', {
                        mobile: $scope.selectedMobile,
                        cardId: $scope.newCardId,
                        deployment_id: deployment_id
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.responce == 1) {
                            growl.success('Card transfer done successfully.', {ttl: 2000});
                            $scope.printSlip('Transfer',result.data.time,$scope.tCard)
                            $scope.selectedMobile = '';
                            $scope.mobile = '';
                            $scope.newCardId = '';
                            $scope.doFalseAll();
                            $scope.selectedButton="addAmount";
                            $scope.setFocus.focusInput = false;
                            $timeout(function () {
                                $scope.setFocus.focusInput = true;
                            }, 200);
                        }
                        $scope.inProcess = false;
                    }, function (err) {
                        if (err.data.err == 'cardexist') {
                            growl.error('Card exist !!', {ttl: 3000});
                            $scope.newCardId = '';
                        } else {
                            growl.error('There is problem with the card transfer.!', {ttl: 3000});
                        }
                        $scope.inProcess = false;
                    });
                } else {
                    growl.error('Code not matched !!', {ttl: 3000});
                }
            })
            console.log(code);
            console.log($scope.newCardId);
        }
        $scope.clearTransferCard = function () {
            $scope.selectedMobile = '';
            $scope.mobile = '';
            $scope.doFalseAll();
           $scope.selectedButton="addAmount";
            $scope.setFocus.focusInput = false;
            $timeout(function () {
                $scope.setFocus.focusInput = true;
            }, 200);
        }
        $scope.refreshForm = function (regForm) {
            $scope.registration = {
                cardId: '',
                formNumber: '',
                deployment_id: currentUser.deployment_id,
                name: '',
                mobile: '',
                securityAmount: 0,
                entryFee: 0,
                amount: '',
                user: {name: currentUser.username, userId: currentUser._id},
                transactions: [],
                closeTime:null,
                paymentType:'Cash'
            }
            regForm.$pristine = true;
            $scope.selectedCardId = '';
            $scope.cardId='';
            $scope.doFalseAll();
            /*$timeout(function () {
                $scope.setFocus.focusInput = true;
            }, 100);*/
            $scope.selectedButton="addAmount";
            $scope.setFocus.focusInput = false;
            $timeout(function () {
                $scope.setFocus.focusInput = true;
            }, 200);
        }
        $scope.printSlip=function(type,time,reg){
            var data='';
            data+="Card Transaction Slip\r\n";
            data+='=====================\r\n';
            if(type!="Transfer") {
                data += $scope.setString('CardId:', 13) + $scope.selectedCardId + '\r\n';
            }else{
                data += $scope.setString('CardId:', 13) + reg.detail.cardId + '\r\n';
            }
            data+=$scope.setString('DateTime:',13)+ Utils.getDateFormatted(new Date( time))+'\r\n';
            if(type=="Registration") {
                data += $scope.setString('Name:', 13) + reg.name + '\r\n';
                data += $scope.setString('Mobile:', 13) + reg.mobile + '\r\n';
            }else{
                data += $scope.setString('Name:', 13) + reg.detail.name + '\r\n';
                data += $scope.setString('Mobile:', 13) + reg.detail.mobile + '\r\n';
            }
            data+=$scope.setString('Trans. Type:',13)+type+'\r\n';
            data+='------------------------\r\n';
            if(type=="Registration"){
                data+=$scope.setString("Previous Amount:",16)+'0\r\n';
                data+=$scope.setString("Recharge Amount:",16)+reg.transactions[0].amount+'\r\n';
               // data+=$scope.setString("Security Amount:",16)+reg.securityAmount+'\r\n';
                data+='------------------------\r\n';
                data+=$scope.setString("Total Amount:",16)+(reg.transactions[0].amount)+'\r\n';
            }
            if(type=="Recharge"){
                data+=$scope.setString("Previous Amount:",16)+reg.balance +'\r\n';
                data+=$scope.setString("Recharge Amount:",16)+reg.addAmount+'\r\n';
                data+='------------------------\r\n';
                data+=$scope.setString("Current Balance:",16)+(parseFloat( reg.balance)+parseFloat( reg.addAmount))+'\r\n';
            }
            if(type=="Refund"){
                data+=$scope.setString("Previous Amount:",16)+reg.balance +'\r\n';
                data+=$scope.setString("Security Amount:",16)+$scope.sAmount+'\r\n';
                data+=$scope.setString("Refund Amount:",16)+(reg.balance +$scope.sAmount)+'\r\n';
                data+='------------------------\r\n';
                data+=$scope.setString("Current Balance:",16)+'0\r\n';
            }
            if(type=="Transfer"){
                data+=$scope.setString("Previous Amount:",16)+reg.balance +'\r\n';
                data+='------------------------\r\n';
                data+=$scope.setString("Current Balance:",16)+reg.balance+'\r\n';
                data+=$scope.setString("New Card Id:",16)+$scope.newCardId+'\r\n';
            }
            console.log(data);
            printer.print('dynamicTemplate',data,'dynamicPrint');
        }
        $scope.setString=function(string,length){
            var str="";
            if(string.length>length){
                str=string.substring(0,length);
            }else{
                var blankchar="";
                for(var i=0;i<(length-string.length);i++){
                    blankchar+=" ";
                }
                str=string+blankchar;
            }
            return str;
        }

     $scope.passCodeModal = function() {
      var d = $q.defer();
      $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/_passCode.html',
        controller: ['$scope', '$modalInstance', 'growl', function($scope, $modalInstance, growl) {
          $scope.passCode = "";
          $scope.setDigit = function(digit) {
            $scope.passCode += digit;
            if ($scope.passCode.length == 6) {
              $scope.applyPassCode();
            }
          };
          $scope.applyPassCode = function() {
            d.resolve($scope.passCode);
            $modalInstance.close($scope.passCode);
          };
          $scope.setKeyboardEvent=function(){
            //alert($scope.passCode);
            if ($scope.passCode.length == 6) {
              $scope.applyPassCode();
            }
          }
          $scope.cancelPassCode = function() {
            d.resolve('cancel');
            $modalInstance.close('cancel');
          };
        }],
        size: 'sm'
      })
      return d.promise;
    };

    }]);
