'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('card', {
        url: '/card',
        templateUrl: 'app/card/card.html',
        controller: 'CardCtrl',
          resolve: {
            currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location','localStorageService',
              function ($state, $stateParams, Auth,Utils,growl,$location,localStorageService) {
               return   Auth.getCurrentUser().$promise.then(function (user) {
                //console.log(user);
                   if(user.deployment_id==null) {
                       Auth.logout();
                       $location.path('/login');
                       growl.error('Admin is not allowed for access.!',{ttl:3000});
                       return false;
                   }
                   var flag=false;
                   var settings=localStorageService.get('settings');
                    if((settings==null||settings==undefined)){
                       Auth.logout();
                       $location.path('/login');
                       growl.error('Deployments are not set.!',{ttl:3000});
                    }else{
                         var security=  Utils.getSettingValue('card_security_amount', settings);
                         var entryFee=  Utils.getSettingValue('card_entry_fee', settings);
                        var cardEntity={security:parseInt( security)?parseInt( security):0,entryFee:parseInt( entryFee)?parseInt( entryFee):0};
                        console.log(cardEntity);
                        user.cardEntity=cardEntity;  
                    }
                    console.log(user);
                   // return false;
                 /* if (user.role === 'user') {
                    if (Utils.hasPermission(user.selectedPermissions,'view_past_bills')) {
                      flag = true;
                    }
                    if(!flag){
                      growl.error('No permission to view past bill !!',{ttl:3000});
                      Auth.logout();
                      $location.path('/login');
                      return false;
                    }
                  }*/
                  return user;
                });
              }]}
      });
  });