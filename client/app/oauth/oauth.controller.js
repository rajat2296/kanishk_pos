'use strict';

angular.module('posistApp')
    .controller('OauthCtrl', function($scope, $http, $cookieStore, $location, $stateParams,localStorageService,$timeout, $window,User, $state, Deployment) {
          console.log($stateParams);
           // console.log($location.$$search);
            var route = $stateParams.route;
            console.log(route);
            
                $http.post('/auth/local/oauth', {
                    key: $stateParams.key
                }).success(function(data) {
                        console.log(data.token);
                        $cookieStore.put('token', data.token);
if (!localStorageService.get('deployment_id')) {
                        User.get().$promise.then(function(user) {
                            console.log(user);
                            localStorageService.set('currentUser', user);
                            localStorageService.set('tenant_id', user.tenant_id);
                            Deployment.findOne({ id: user.deployment_id }, function(deployment) {
                                localStorageService.set('deployment_id', deployment._id);
                                localStorageService.set('settings', deployment.settings);
                                $window.location.reload(); 
                            });

                        });
                      }
        else
        {
          console.log('deployment exist');
          console.log('going to route',route);
            $state.go(route);
        }
                });
      

    });
