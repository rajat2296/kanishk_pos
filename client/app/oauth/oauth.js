'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('oauth', {
        url: '/oauth/:key/:route',
        templateUrl: 'app/oauth/oauth.html',
        controller: 'OauthCtrl'
      });
  });