'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('client/app/stock/stockReports', {
        url: '/client/app/stock/stockReports',
        templateUrl: 'app/client/app/stock/stockReports/client/app/stock/stockReports.html',
        controller: 'ClientAppStockStockReportsCtrl'
      });
  });