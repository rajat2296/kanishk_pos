'use strict';

describe('Controller: ClientAppStockStockReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var ClientAppStockStockReportsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ClientAppStockStockReportsCtrl = $controller('ClientAppStockStockReportsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
