'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('reports', {
        url: '/reports',
        templateUrl: 'app/reports/reports.html',
        controller: 'ReportsCtrl',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasUserPermission(user.selectedPermissions, 'Reports')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          downloadDetails: ['$http', function ($http){
            return  $http({
              url:'/api/reports/',
              method:"GET"
            }).then(function success(res) {
              console.log("success");
              return res.data;
            })
          }],
          settings: ['$q', 'localStorageService', '$timeout', function ($q, localStorageService, $timeout) {
            var d = $q.defer();
            var _settings = {};
            $timeout(function () {
              _settings = localStorageService.get('settings');
              d.resolve(_settings);
            });
            return d.promise;
          }],
          superCategories: ['$q', 'Supercategory', 'localStorageService', function ($q, Supercategory, localStorageService) {
            var d = $q.defer();
            Supercategory.get({tenant_id: localStorageService.get('tenant_id')}, function (scs) {
              //console.log(scs);
              d.resolve(scs);
            }, function (err) {
              growl.error('Could not fetch Super Categories!', {ttl: 3000});
              d.reject();
            });
            return d.promise;
          }],
          categories: ['$q', 'Category', 'localStorageService', '$timeout', function ($q, Category, localStorageService, $timeout) {
            var d = $q.defer();
            Category.get({tenant_id: localStorageService.get('tenant_id')}, function (categories) {
              //console.log(categories);
              d.resolve(categories);
            }, function (err) {
              growl.error('Could not fetch Categories!', {ttl: 3000});
              d.reject();
            });
            return d.promise;
          }],
          deployments: ['$q', 'Deployment', 'localStorageService', function ($q, Deployment, localStorageService) {
            var d = $q.defer();
            Deployment.get({tenant_id: localStorageService.get('tenant_id'), isMaster: false}, function (deployments) {
              //console.log(deployments);
              d.resolve(deployments);
            }, function (err) {
              d.reject();
            });
            return d.promise;
          }],
          tabs: ['$q', 'Tab', 'localStorageService', function ($q, Tab, localStorageService) {
            var d = $q.defer();
            Tab.getTenantTabs({tenant_id: localStorageService.get('tenant_id')}, function (tabs) {
              //console.log(tabs);
              d.resolve(tabs);
            }, function (err) {
              d.reject();
            });

            return d.promise;
          }],
          //kanishk
          waiters: ['$q', 'User', 'localStorageService', function ($q, User, localStorageService) {
            var d = $q.defer();
            User.getUsersWaitersByTenant({tenant_id: localStorageService.get('tenant_id')}, function (waiters) {
              //console.log(deployments);
              d.resolve(waiters);
            }, function (err) {
              d.reject();
            });
            return d.promise;
          }],
          recipes: ['stockRecipe', 'localStorageService', function(stockRecipe, localStorageService) {
            return stockRecipe.get({deployment_id: localStorageService.get('deployment_id'), tenant_id: localStorageService.get('tenant_id')}, function (result){
              return result;
            });
          }]
          //end
        }
      })
      .state('reports.rpt_downloadDetails', {
        url: '/downloadDetails',
        templateUrl: 'app/reports/rpt/_downloadDetails.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_invoice_details')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
          downloadDetails: ['$http', function ($http){
            return  $http({
              url:'/api/reports/',
              method:"GET"

            }).then(function success(res) {
              console.log("success");
              return res.data;
            })
          }]
        }
      })
      .state('reports.rpt_paymentDetails', {
        url: '/paymentdetails',
        templateUrl: 'app/reports/rpt/_paymentDetails.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_invoice_details')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }],
        }
      })
      .state('reports.rpt_invoiceDetails', {
        url: '/invoicedetails',
        templateUrl: 'app/reports/rpt/_invoiceDetails.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_sale_items')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_categoryBreakdown', {
        url: '/categorybreakdown',
        templateUrl: 'app/reports/rpt/_categoryBreakdown.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_sale_item_category')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_taxSummary', {
        url: '/taxsummary',
        templateUrl: 'app/reports/rpt/_taxSummary.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_tax_summary')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      /* .state('reports.rpt_tabUserWaiterBreakdown', {
       url: '/tabuserwaiterbreakdown',
       templateUrl: 'app/reports/rpt/_tabUserWaiterBreakdown.html',
       controller: 'ReportsCtrl',
       parent: 'reports'
       })*/
      .state('reports.rpt_tabUserWaiterBreakdown', {
        url: '/tabuserwaiterbreakdown',
        templateUrl: 'app/reports/rpt/_tabUserWaiterBreakDownNew.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_tab_user_waiter')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_kotDetail', {
        url: '/kotdetail',
        templateUrl: 'app/reports/rpt/_kotDetail.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_kot_detail')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_kotDeleteHistory', {
        url: '/kotdeletehistory',
        templateUrl: 'app/reports/rpt/_kotDeleteHistory.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_kot_delete_history')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_averageBill', {
        url: '/averagebill',
        templateUrl: 'app/reports/rpt/_averageBill.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_average_bill')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_itemWiseEnterprise', {
        url: '/itemwiseenterprise',
        templateUrl: 'app/reports/rpt/_itemWiseEnterprise.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                if (user.role === 'user') {
                  if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                    flag = true;
                  }
                  if (!flag) {
                    growl.error('No permission to view this reports !!', {ttl: 3000});
                    Auth.logout();
                    $location.path('/login');
                    return false;
                  }
                }
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_complimentary_consolidated', {
        url: '/complimentary/consolidated',
        templateUrl: 'app/reports/rpt/_complimentaryConsolidated.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_complimentary', {
        url: '/complimentary',
        templateUrl: 'app/reports/rpt/_complimentary.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_settlement', {
        url: '/settlement',
        templateUrl: 'app/reports/rpt/_settlement.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_coupon', {
        url: '/coupon',
        templateUrl: 'app/reports/rpt/_coupon.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_otherPayments', {
        url: '/otherPayments',
        templateUrl: 'app/reports/rpt/_otherPayments.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_otherPaymentsConsolidated', {
        url: '/otherPayments/consolidated',
        templateUrl: 'app/reports/rpt/_otherPaymentsConsolidated.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_instanceWiseReport', {
        url: '/instanceWise',
        templateUrl: 'app/reports/rpt/_instanceWiseReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_discountReport', {
        url: '/discountReport',
        templateUrl: 'app/reports/rpt/_discountReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_reprintReport', {
        url: '/reprintReport',
        templateUrl: 'app/reports/rpt/_reprintReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_saleReport', {
        url: '/saleReport',
        templateUrl: 'app/reports/rpt/_saleReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_waiterReportCard', {
        url: '/waiterReportCard',
        templateUrl: 'app/reports/rpt/_waiterReportCard.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_compareWaiters', {
        url: '/compareWaiters',
        templateUrl: 'app/reports/rpt/_compareWaiters.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_hourlySales', {
        url: '/hourlySales',
        templateUrl: 'app/reports/rpt/_hourlySalesReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_billCustomer', {
        url: '/billCustomerReport',
        templateUrl: 'app/reports/rpt/_billAndCustomerConsolidatedReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_otherPaymentsDetailed', {
        url: '/otherPaymentsDetailed',
        templateUrl: 'app/reports/rpt/_otherPaymentsDetailed.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_foodCosting', {
        url: '/foodCosting',
        templateUrl: 'app/reports/rpt/_foodCostingReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_kotTracking', {
        url: '/kotTracking',
        templateUrl: 'app/reports/rpt/_kotTrackingReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_customerDataReport', {
        url: '/customerDataReport',
        templateUrl: 'app/reports/rpt/_customerDataReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_growthReport', {
        url: '/growthReport',
        templateUrl: 'app/reports/rpt/_growthReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_cardReport', {
        url: '/cardReport',
        templateUrl: 'app/reports/rpt/_cardReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_deliveryReport', {
        url: '/deliveryReport',
        templateUrl: 'app/reports/rpt/_deliveryReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_deliveryBoyReport', {
        url: '/deliveryBoyReport',
        templateUrl: 'app/reports/rpt/_deliveryBoyReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_consolidateItemWiseProfitable', {
        url: '/ItemWiseProfit',
        templateUrl: 'app/reports/rpt/_consolidateItemWiseProfitable.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_consolidateItemWise', {
        url: '/consolidateItemWise',
        templateUrl: 'app/reports/rpt/_consolidateItemWise.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_callDetails', {                                          //added by rajat
        url: '/callDetails',
        templateUrl: 'app/reports/rpt/_callDetails.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location','localStorageService',
            function ($state, $stateParams, Auth,Utils,growl,$location,localStorageService) {
              return Auth.getCurrentUser().$promise.then(function (user) {

                return user;
              });
            }]}
      })
      .state('reports.rpt_noOrder', {                                              //added by rajat
        url: '/noOrder',
        templateUrl: 'app/reports/rpt/_noOrder.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location','localStorageService',
            function ($state, $stateParams, Auth,Utils,growl,$location,localStorageService) {
              return Auth.getCurrentUser().$promise.then(function (user) {

                return user;
              });
            }]}
      })
      .state('reports.rpt_tenantWise', {                                           //added by rajat
        url: '/tenantWise',
        templateUrl: 'app/reports/rpt/_tenantwise.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location','localStorageService',
            function ($state, $stateParams, Auth,Utils,growl,$location,localStorageService) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                return user;
              });
            }]}
      })
      .state('reports.rpt_dailySales', {                                           //added by rajat
        url: '/dailySales',
        templateUrl: 'app/reports/rpt/_dailySales.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
              var flag=false;
              return user;
            })
          }]}
      })
      .state('reports.rpt_removedTaxes', {
        url: '/RemovedTaxesReport',
        templateUrl: 'app/reports/rpt/_removedTaxes.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_complimentary_headwise', {
        url: '/ComplimentaryHeadwiseReport',
        templateUrl: 'app/reports/rpt/_complimentaryHeadwise.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_userAttendance', {                                       //added by tanvee
        url: '/userAttendance',
        templateUrl: 'app/reports/rpt/_userAttendance.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
              var flag=false;
              return user;
            })

          }]}
      })
      .state('reports.rpt_singleUserAttendance', {                                 //added by tanvee
        url: '/singleUserAttendance',
        templateUrl: 'app/reports/rpt/_singleUserAttendance.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
              var flag=false;
              return user;
            })

          }]}
      })
      .state('reports.rpt_sectionCategoryWise', {
        url: '/sectionCategoryWise',
        templateUrl: 'app/reports/rpt/_sectionCategoryWise.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_billWiseSales', {
        url: '/billWiseSales',
        templateUrl: 'app/reports/rpt/_billwiseSalesReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_cashierWiseCardReport', {
        url: '/cashierWiseCardReport',
        templateUrl: 'app/reports/rpt/_cashierWiseCardReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                /*if (user.role === 'user') {
                 console.log('Permission', Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise'));
                 if (Utils.hasPermission(user.selectedPermissions, 'view_item_wise_enterprise')) {
                 flag = true;
                 }
                 if (!flag) {
                 growl.error('No permission to view this reports !!', {ttl: 3000});
                 Auth.logout();
                 $location.path('/login');
                 return false;
                 }
                 }*/
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_dailySalesSummary', {
        url: '/dailySalesSummary',
        templateUrl: 'app/reports/rpt/_dailySalesSummary.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_baggaryReport', {
        url: '/TaxationBillwise',
        templateUrl: 'app/reports/rpt/_baggaryReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_reportDashboard', {
        url: '/Downloads',
        templateUrl: 'app/reports/rpt/reportDashboard.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_dailySalesReportDetailed', {
        url: '/dailySalesSummaryDetailed',
        templateUrl: 'app/reports/rpt/_dailySalesSummaryJaime.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_bohReport', {
        url: '/bohReport',
        templateUrl: 'app/reports/rpt/_bohReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_advanceBooking', {
        url: '/advanceBookingReport',
        templateUrl: 'app/reports/rpt/_advanceBooking.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_menuMix', {
        url: '/menuMixReport',
        templateUrl: 'app/reports/rpt/_menuMixReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_offers', {
        url: '/offersReport',
        templateUrl: 'app/reports/rpt/_offersReport.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_dayCheckClose', {
        url: '/dayCheckCloseReport',
        templateUrl: 'app/reports/rpt/_dayCheckClose.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_employeePerformance', {
        url: '/employeePerformance',
        templateUrl: 'app/reports/rpt/_employeePerformance.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      })
      .state('reports.rpt_enterpriseSettlement', {
        url: '/enterpriseSettlement',
        templateUrl: 'app/reports/rpt/_enterpriseSettlement.html',
        controller: 'ReportsCtrl',
        parent: 'reports',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
            function ($state, $stateParams, Auth, Utils, growl, $location) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                var flag = false;
                return user;
              });
            }]
        }
      });
})


