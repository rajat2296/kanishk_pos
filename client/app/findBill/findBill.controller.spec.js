'use strict';

describe('Controller: FindBillCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var FindBillCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FindBillCtrl = $controller('FindBillCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
