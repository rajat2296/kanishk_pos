'use strict';

angular.module('posistApp')
  .controller('FindBillCtrl',['$scope','Customer','billResource','$rootScope','Utils','currentUser','deployments','growl','DTOptionsBuilder','DTColumnDefBuilder','$location','Bill','printer','$q','$modal','User','$state','$ngBootbox', function ($scope,Customer,billResource,$rootScope,Utils,currentUser,deployments,growl,DTOptionsBuilder,DTColumnDefBuilder,$location,Bill,printer,$q,$modal,User,$state,$ngBootbox) {

    $scope.dtOptions=DTOptionsBuilder.newOptions().withPaginationType('full_numbers');
    $scope.dtColumnDefs=[
      DTColumnDefBuilder.newColumnDef(0),
      DTColumnDefBuilder.newColumnDef(1),
      DTColumnDefBuilder.newColumnDef(2),
      DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];
    $scope.hr='';
    $scope.allBills=[];
    $scope.deploymentId;
    $scope.selectedDate={
      from: Utils.getDateFormattedDate(new Date()),
      to:Utils.getDateFormattedDate(new Date())
    }
    $scope.customers=[];
    var _deployments=[];
    //  console.log($rootScope.currentUser);
    if ($scope.currentUser.role == 'user') {
      /*if (!Utils.hasUserPermission($scope.currentUser.selectedPermissions, 'Reports')) {
       growl.error('00ps !! have permission ?', {ttl: 3000});
       $location.path('/login');
       }*/
      _deployments = _.filter(deployments, {_id: $scope.currentUser.deployment_id});
      $scope.deployment_id=angular.copy( _deployments[0]._id);
    } else {
      _deployments = deployments;
    }
    $scope.deployments=_deployments;
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };
    $scope.openTo = function($event) {
      console.log($scope.openedTo);
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedTo = true;
    };
    /* if ($scope.currentUser.role == 'user') {
     if (!Utils.hasUserPermission($scope.currentUser.selectedPermissions, 'Reports')) {
     growl.error('00ps !! have permission ?', {ttl: 3000});
     $location.path('/login');
     }
     }*/
    $scope.isPermission=function(permissionname) {
      var flag=false;
      if ($scope.currentUser.role == 'user') {
        if (!Utils.hasPermission($scope.currentUser.selectedPermissions, permissionname)) {
          flag=true;
        }
      }
      return flag;
    }
    $scope.renderBills=function() {
      var d=$q.defer();
      var _deployment=  _.filter($scope.deployments,{'_id':$scope.deployment_id});
      if(_deployment.length==0){growl.error('Select Deployment Date.',{ttl:2000}); return false;}
      //var startDate=Utils.convertDate($scope.selectedDate.from);
      billResource.get({deployment_id: $scope.deployment_id,fromDate:new Date( Utils.getDateFormatted(Utils.getResetDate(_deployment[0].settings, Utils.convertDate( $scope.selectedDate.from)))),toDate:new Date(Utils.getDateFormatted(Utils.getResetDate(_deployment[0].settings,Utils.convertDate( $scope.selectedDate.to).setDate(Utils.convertDate( $scope.selectedDate.to).getDate()+1) )))}, function (bills) {
        //   console.log(bills);
        var tempBills = angular.copy(bills);
        $scope.allBills=tempBills;
        _.forEach(tempBills, function (bill) {
          var gTotal = 0;
          var gTotalItems = 0;
          if(!bill.isVoid) {
            _.forEach(bill._kots, function (kot) {
              if (!kot.isVoid) {
                gTotal += Utils.roundNumber((kot.totalAmount + kot.taxAmount - kot.totalDiscount), 2);
                gTotalItems += kot.totalItems;
              }

            });
          }
          bill.gTotalAmount = Utils.roundNumber(gTotal-bill.billDiscountAmount, 0) ;
          bill.gTotalItems = gTotalItems;
          var billNumber= Utils.hasSetting('reset_serial_daily',_deployment[0].settings)?bill.daySerialNumber:bill.serialNumber;
          bill.dBillNumber = billNumber+(parseInt( bill.splitNumber)?'-'+bill.splitNumber:'');
          bill.dBillNumber= ((_.has(bill,'instance'))?(bill.instance.preFix==undefined?'':bill.instance.preFix):'') + bill.dBillNumber ;
          if (bill.tabType == 'table') {
            bill.waiterDelivery = (_.has(bill._waiter, 'firstname')) ? bill._waiter.firstname : '';
          } else {
            bill.waiterDelivery = (_.has(bill._delivery, 'firstname')) ? bill._delivery.firstname : '';
          }

        });
        $scope.bills = tempBills;
        if($scope.bills.length==0){growl.error('There is no data.',{ttl:1000});}
        d.resolve('done');
        /*  $scope.dtOptions=DTOptionsBuilder.newOptions().withPaginationType('full_numbers');
         $scope.dtColumnDefs=[
         DTColumnDefBuilder.newColumnDef(0),
         DTColumnDefBuilder.newColumnDef(1),
         DTColumnDefBuilder.newColumnDef(2),
         DTColumnDefBuilder.newColumnDef(3).notSortable()
         ];*/
      });
      return  d.promise;
    }
    $scope.passCodeContextUser='';

    $scope.rePrintBill = function (bill) {
      if ($scope.checkDateModificationBill(bill)) {
        return false;
      }
      var billId = bill._id;
      var flag = false;
      var _deployment = _.filter($scope.deployments, {'_id': $scope.deployment_id});
      if (Utils.hasSetting('enable_passcode', _deployment[0].settings, 'reprintbill')) {
        $scope.passCodeModal().then(function (code) {
          var _f = _.filter($rootScope.users, {passcode: code});
          if (_f.length > 0) {
            $scope.passCodeContextUser = (_.has(_f[0], 'username') ? _f[0].username : '');
          }

          User.getUserByCodeDep({deployment_id: $scope.deployment_id, code: code}, function (users) {
            for (var i = 0; i < users.length; i++) {
              if (Utils.hasUserPermission(users[i].selectedPermissions, 'Reports')) {
                flag = true;
                break;
              }
            }
          }).$promise.then(function () {
            if (flag) {
              reprintBillPasscode(billId);
            } else {
              growl.error('No permission for print bill', {ttl: 1000});
            }
          });
        });
      }
      else {
        reprintBillPasscode(billId);
      }

    }

    function reprintBillPasscode(billId) {
      var _deployment = _.filter($scope.deployments, {'_id': $scope.deployment_id});
      if (Utils.hasSetting('comment_re_print', _deployment[0].settings)) {
        $scope.commentModal().then(function (comment) {
          if (comment == 'cancel' || comment.length == 0) {
            growl.error("Please enter a valid comment", {ttl: 3000});
            return false;
          }
          else
            $scope.sendToPrinter(billId, comment);
        });
      }
      else
        $scope.sendToPrinter(billId);
    }

    $scope.sendToPrinter=function(billId,reprintComment){
      var _deployment=  _.filter($scope.deployments,{'_id':$scope.deployment_id});
      var bill= _.filter($scope.allBills,{'_id':billId});
      if(_.has(bill[0],'rePrint')){
        bill[0].rePrint.push({printTime: new Date(),user:($scope.passCodeContextUser==''?currentUser.username:angular.copy( $scope.passCodeContextUser)),comment:reprintComment});
      }else{
        var reprint=[];
       var username= ($scope.passCodeContextUser==''?bill[0].username:angular.copy( $scope.passCodeContextUser))
        reprint.push({printTime: bill[0].billPrintTime,user:username});
        reprint.push({printTime: new Date(),user:($scope.passCodeContextUser==''?currentUser.username:angular.copy( $scope.passCodeContextUser)),comment:reprintComment});
        bill[0].rePrint=reprint;
      }
      // console.log(bill[0]);

      var extendedBill= _.extend(new Bill,bill[0]);
      var tempBill=angular.copy(extendedBill);
      delete tempBill._id;

      billResource.updateById(tempBill).$promise.then(function(bill){
        //  console.log(bill);

        //  console.log(extendedBill);
        extendedBill.processBill();
        extendedBill.prepareAggregation(_deployment[0].settings,extendedBill.tabType).then(function(billToPrint){
          printer.print('_template_Bill.html', billToPrint);
        });
      })
    }

    $scope.onDeploymentChange=function(){
      $scope.bills=[];
      /*
       var _deployment=  _.filter($scope.deployments,{'_id':$scope.deployment_id});
       if(_deployment.length==0){$scope.deploymentId=; return false;}*/
    }
    $scope.passCodeModal = function () {
      var d = $q.defer();
      $modal.open({
        templateUrl: 'app/billing/_passCode.html',
        controller: ['$scope', '$modalInstance', 'growl', function ($scope, $modalInstance, growl) {
          $scope.passCode = "";
          $scope.setDigit = function (digit) {
            $scope.passCode += digit;
            if ($scope.passCode.length == 6) {
              $scope.applyPassCode();
            }
          };
          $scope.applyPassCode = function () {
            d.resolve($scope.passCode);
            $modalInstance.close($scope.passCode);
          };
          $scope.cancelPassCode = function () {
            d.resolve('cancel');
            $modalInstance.close('cancel');
          };
            $scope.setKeyboardEvent=function(){

            //alert($scope.passCode);
            if ($scope.passCode.length == 6) {
              $scope.applyPassCode();
            }
          }
        }],
        size: 'sm'
      })
      return d.promise;
    };

    $scope.preVoidBill=function(bill){
    if($scope.checkDateModificationBill(bill)){
      return false;
    }
    var ng_id=bill.ng_id;
      var flag=false;
      if( currentUser.role == 'superadmin' || Utils.isUserInRole(currentUser.selectedRoles,'Manager') ) {
       $scope.voidBill(ng_id);
       return false;
      };
      $scope.passCodeModal().then(function(code){
        //  alert(code);
         var _f = _.filter($rootScope.users, {passcode:code});
              if(_f.length>0){
                  $scope.passCodeContextUser=(_.has( _f[0],'username')?_f[0].username:'');
              }

        User.getUserByCodeDep({deployment_id:$scope.deployment_id,code:code},function(users){
          for(var i=0;i< users.length;i++){
            console.log(users[i].selectedRoles);
            if(Utils.isUserInRole(users[i].selectedRoles,'Manager')){
              flag=true;
              break;
            }
          }
        }).$promise.then(function(){
          if(flag){
            $scope.voidBill(ng_id);
          }else{
            growl.error('No permission for void bill',{ttl:1000});
          }
        });

        //return false;
      });

    }
    $scope.voidBill=function(ng_id) {

      $ngBootbox.confirm('Are you sure to void this bill ?')
        .then(function () {
          if (Utils.hasSetting('enable_comment_void_bill', $scope.settings)) {
            $scope.commentModal().then(function (comment) {
              if (comment === 'cancel' || comment.length === 0) {
                growl.error('Please Enter Valid Comment', {
                  ttl: 2000
                });

                return false;
              } else {
                $rootScope.showLoaderOverlay = true;
                var up = {q: {"ng_id": ng_id,deployment_id:$scope.deployment_id}, sets:{"isVoid": true, payments: [], _cash: [],voidDetail:{userName:($scope.passCodeContextUser==''?currentUser.username:angular.copy( $scope.passCodeContextUser)),time:new Date()}},voidComment:comment};
               $scope.passCodeContextUser='';
                var tempBill = [];
                billResource.updateBillSet(up).$promise.then(function (res) {
                  $scope.renderBills().then(function () {
                    $rootScope.showLoaderOverlay = false;
                  });
                }).catch(function () {
                  $rootScope.showLoaderOverlay = false;
                });
              }
            });
          }else{
            $rootScope.showLoaderOverlay = true;
            var up = {q: {"ng_id": ng_id}, sets: {"isVoid": true, payments: [], _cash: [],voidDetail:{userName:($scope.passCodeContextUser==''?currentUser.username:angular.copy( $scope.passCodeContextUser)),time:new Date()}}};
            $scope.passCodeContextUser='';
            var tempBill = [];
            billResource.updateBillSet(up).$promise.then(function (res) {
              $scope.renderBills().then(function () {
                $rootScope.showLoaderOverlay = false;
              });
            }).catch(function () {
              $rootScope.showLoaderOverlay = false;
            });
          }
        });
    }
    $scope.commentModal = function() {
      var d = $q.defer();
      $modal.open({
        backdrop : 'static',
        keyboard :false,
        templateUrl: 'app/billing/item/_comment1.html',
        controller: function($scope, $modalInstance) {
          $scope.comment = "";
          $scope.addComment = function() {
            d.resolve($scope.comment);
            $modalInstance.close($scope.comment);
          };
          $scope.cancel = function() {
            d.resolve('cancel');
            $modalInstance.dismiss('cancel');
          };
        },
        size: 'sm'
      });
      return d.promise;
    };

    $scope.editBill=function(bill){
      // $rootScope.$broadcast('editBill', bill);
      // $state.go('billing');
    if($scope.checkDateModificationBill(bill)){
      return false;
    }
      $rootScope.editBill=bill;
      $location.path('/billing')
    }

    $scope.checkDateModificationBill=function(bill){
      var flag=false;
      //var ng_id=angular.copy(bill.ng_id);
    var _deployment=  _.filter($scope.deployments,{'_id':$scope.deployment_id});
          if (Utils.hasSetting('void_bill_max_time', _deployment[0].settings))
           {
            var hoursBeforeCutOff=0;
            hoursBeforeCutOff = parseInt(Utils.getSettingValue('void_bill_max_time',  _deployment[0].settings))
            var _deployment=  _.filter($scope.deployments,{'_id':$scope.deployment_id});
            var createdTime=new Date(Utils.getDateFormatted(bill._created));
            var currentTime=new Date(Utils.getDateFormatted(Utils.getResetDate(_deployment[0].settings,Utils.convertDate(new Date()))));
        currentTime=moment(currentTime).subtract(hoursBeforeCutOff, 'hours');
        console.log("current cutoff time",currentTime);

        if(createdTime<=currentTime)
        {
          growl.error("Modification of bill not possible after cutoff date.",{ttl:3000});
          flag= true;
        }

        }
        return flag;
    }

  }]);
