'use strict';

angular.module('posistApp')
  .controller('OfferCtrl',['$state','$scope','growl','DateList','OfferCode','$modal','deployments','currentUser','$timeout','$q','deployment','Category','Item','Utils','Offer','$ngBootbox','$location','$rootScope',function ($state,$scope,growl,DateList,OfferCode,$modal,deployments,currentUser,$timeout,$q,deployment,Category,Item,Utils,Offer,$ngBootbox,$location,$rootScope) {
    //var deployment={_id:''};
    $scope.id=null;
    $scope.loading=false;
    $scope.availableCategories=[];
    $scope.availableCategories[0]=[];
    $scope.availableCategories[1]=[];
    $scope.availableCategories[2]=[];
    $scope.availableItems=[];
    $scope.availableItems[0]=[];
    $scope.availableItems[1]=[];
    $scope.availableItems[2]=[];

    $scope.dateLists=[];
    $scope.offerCodes=[];
    $scope.offers=[];

    $scope.isPermission=function(permissions){
      // console.log(currentUser);
      var flag = false;
      if (currentUser.role === 'user') {
        // flag= true;
        //   } else {
        var permissionArr = permissions.split(',');
        // console.log(permissionArr);
        _.forEach(permissionArr, function (permname) {
          if (Utils.hasPermission(currentUser.selectedPermissions, permname)) {
            flag = true;
          }
        })
      }else{
        flag=true;
      }
      return flag;
    }
    //$rootScope.showLoaderOverlay = true;
    //  Category.get({tenant_id:currentUser.tenant_id,deployment_id:deployment._id}).$promise.then(function(categories){
    Item.getItemByDepoyment({tenant_id:currentUser.tenant_id,deployment_id:deployment._id}).$promise.then(function(itemsa){
      DateList.get({tenant_id: currentUser.tenant_id, deployment_id: deployment._id}, function (datelists) {
        console.log("Got Date Lists-" + datelists.length);
        $scope.dateLists=datelists;
        OfferCode.get({tenant_id: currentUser.tenant_id, deployment_id: deployment._id}, function (offercodes) {
          console.log("Got Offer Codes-" + offercodes.length);
          Offer.get({tenant_id: currentUser.tenant_id, deployment_id: deployment._id}, function (offers) {
            $scope.offers= _.sortBy( offers,['created']);
            $scope.offerCodes=offercodes;
            console.log($scope.offerCodes);
            var tcategories=[];
            var titems=[];
            ///////////////////////////////////////////////////////////
            _.forEach(itemsa, function(i) {
                if(_.has(i,'category')){
                delete i.category.superCategory;
                delete i.tabs;
                tcategories.push(i.category);
                titems.push(i);
              }
            });
            titems=_.sortBy(titems,['name']);
            var _uCats = _.uniq(tcategories, 'categoryName');
            _uCats= _.sortBy(_uCats,['categoryName']);
            var items=angular.copy(titems);
            $scope.categories=_uCats;
            //$scope.availableCategories[0]=[];
            $scope.availableCategories[0]=angular.copy( _uCats);
            $scope.availableCategories[1]=angular.copy( _uCats);
            $scope.availableCategories[2]= angular.copy(_uCats);
            $scope.items=items;
            // $scope.availableItems[0]=[];
            $scope.availableItems[0]=angular.copy( titems);
            $scope.availableItems[1]= angular.copy(titems);
            $scope.availableItems[2]= angular.copy(titems);
            ///////////////////////////////////////////////////////////////////////////////////
            // var categories=angular.copy(categoriesa);
            /*     var items=angular.copy(itemsa);
             $scope.categories=categories;
             //$scope.availableCategories[0]=[];
             $scope.availableCategories[0]=angular.copy( categories);
             $scope.availableCategories[1]=angular.copy( categories);
             $scope.availableCategories[2]= angular.copy(categories);
             $scope.items=items;
             // $scope.availableItems[0]=[];
             $scope.availableItems[0]=angular.copy( items);
             $scope.availableItems[1]= angular.copy(items);
             $scope.availableItems[2]= angular.copy(items);*/
            // $rootScope.showLoaderOverlay=false;
            $scope.id= $state.params.id;
            var _filterOffer= _.filter($scope.offers,{'_id':$scope.id});
            if(_filterOffer.length>0){
              $scope.offer=_filterOffer[0];
            }
            if($scope.offer.type.name=='item'||$scope.offer.type.name=='comboOffer'){
              $scope.applicableItems[1]=$scope.offer.applicable.items;
              if($scope.applicableItems[1].length>0){$scope.applicableItem[1].relation=$scope.applicableItems[1][0].relation;}
              console.log($scope.offer.applicable.items.length);
              for(var i=0;i<$scope.offer.applicable.items.length;i++){
                if(!_.has($scope.offer.applicable.items[i].item,'name')){
                  console.log(Utils.arrayObjectIndexOf($scope.availableCategories[1],'_id',$scope.offer.applicable.items[i].category._id));
                  $scope.availableCategories[1].splice(Utils.arrayObjectIndexOf($scope.availableCategories[1],'_id',$scope.offer.applicable.items[i].category._id),1);
                }else {
                  $scope.availableItems[1].splice(Utils.arrayObjectIndexOf($scope.availableItems[1],'_id',$scope.offer.applicable.items[i].item._id),1);
                }
              }
              $scope.applicableItems[2]=$scope.offer.offerApplied.items;
              if($scope.applicableItems[2].length>0){$scope.applicableItem[2].relation=$scope.applicableItems[2][0].relation;}
              for(var i=0;i<$scope.offer.offerApplied.items.length;i++){
                if(!_.has($scope.offer.offerApplied.items[i].item,'name')){
                  console.log(Utils.arrayObjectIndexOf($scope.availableCategories[2],'_id',$scope.offer.offerApplied.items[i].category._id));
                  $scope.availableCategories[2].splice(Utils.arrayObjectIndexOf($scope.availableCategories[2],'_id',$scope.offer.offerApplied.items[i].category._id),1);
                }else {
                  $scope.availableItems[2].splice(Utils.arrayObjectIndexOf($scope.availableItems[2],'_id',$scope.offer.offerApplied.items[i].item._id),1);
                }
              }
            }else{
              $scope.applicableItems[0]=$scope.offer.applicable.items;
              if($scope.applicableItems[0].length>0){$scope.applicableItem[0].relation=$scope.applicableItems[0][0].relation;}
              for(var i=0;i<$scope.offer.applicable.items.length;i++){
                if(!_.has($scope.offer.applicable.items[i].item,'name')){
                  console.log(Utils.arrayObjectIndexOf($scope.availableCategories[0],'_id',$scope.offer.applicable.items[i].category._id));
                  $scope.availableCategories[0].splice(Utils.arrayObjectIndexOf($scope.availableCategories[0],'_id',$scope.offer.applicable.items[i].category._id),1);
                }else {
                  $scope.availableItems[0].splice(Utils.arrayObjectIndexOf($scope.availableItems[0],'_id',$scope.offer.applicable.items[i].item._id),1);
                }
              }
            }
            _.forEach($scope.offer.valid.dateLists,function(datel){
              var _dateL= _.filter($scope.dateLists,{'_id':datel._id});
              if(_dateL.length>0){
                $scope.selectedDateLists.push(_dateL[0]);
              }
            })
            //$scope.selectedDateLists=$scope.offer.valid.dateLists;
            if(_.has( $scope.offer.code,'codeList')){
              if($scope.offer.code.codeList!=null) {
                var _code = _.filter($scope.offerCodes, {'_id': $scope.offer.code.codeList._id});
                if (_code.length > 0) {
                  $scope.offer.code.codeList = _code[0];
                }
              }
            }
            if(!($scope.offer.valid.date.startDate==null||$scope.offer.valid.date.startDate=='')){
              var startDate=new Date(Utils.convertDate($scope.offer.valid.date.startDate));
              //$scope.offer.valid.date.startDate= startDate.toLocaleFormat('%d-%b-%Y');
            }
            if(!($scope.offer.valid.date.endDate==null||$scope.offer.valid.date.endDate=='')){
              // $scope.offer.valid.date.endDate= Utils.convertDate($scope.offer.valid.date.endDate);
            }
            if($scope.offer.applicable.on=='item'){$scope.addLimit=true;$scope.applicableItem[0].limit='min'; }else{$scope.addLimit=false;delete $scope.applicableItem[0].limit;}
            $scope.loading=true;
            $scope.openDefineOffer();
          }, function (err) {
            $rootScope.showLoaderOverlay=false;
            growl.error("Error while fetching offers !", {ttl: 3000});
          });

        }, function (err) {
          growl.error("Error while fetching code lists !", {ttl: 3000});
        });

      }, function (err) {
        growl.error("Error while fetching date lists !", {ttl: 3000});
      });
    });
    // });

    $scope.openDataListModal=function(date,isCreate ){
      var d=$q.defer();
      $modal.open({
        templateUrl: 'app/admin/deployments/addDeployment/adddatalist.html',
        controller: ['$scope', '$modalInstance', 'datelist','isNew','growl','dep_id', function ($scope, $modalInstance, datelist,isNew,growl,dep_id) {
          $scope.dateListName=isNew?null:datelist.name;
          $scope.repeat=isNew? 'daily':datelist.repeat;
          $scope.isNew=angular.copy(isNew);
          $scope.selectedDays=isNew? []:angular.copy(datelist.dates) ; //= [new Date(2015,5,2), new Date(2015,5,10)];  //selectedDays;
          $scope.logInfos = function(event, date) {
            event.preventDefault(); // prevent the date.hover to be toggled
            var datef={isFound:false,index:0};
            for(var i=0;i<$scope.selectedDays.length;i++){
              if($scope.selectedDays[i]==Utils.getDateFormattedDate( date._d)){datef.isFound=true;datef.index=i;break;};
            }
            if(datef.isFound){
              $scope.selectedDays.splice(datef.index,1);
              //console.log($scope.selectedDays);
              growl.success('Date deselected !! ',{ttl:1000});
            }else
              $scope.selectedDays.push(Utils.getDateFormattedDate( date._d));
            if(event.type == 'mouseover') {
              //we're on it!
            }
          };
          $scope.saveUpdate=function(){
            if( $scope.dateListForm.$valid) {
              if ($scope.selectedDays.length == 0) {
                growl.error("Some dates need to be selected.", {ttl: 2000});
                return false;
              }
              if(isNew) {
                DateList.findByName({
                  deployment_id: dep_id,
                  name: $scope.dateListName
                }).$promise.then(function (datelists) {
                  if (datelists.length > 0) {
                    growl.error('Duplicate name found.', {ttl: 1000});
                    return false;
                  }

                  $scope.dateList = {selectedDays: $scope.selectedDays, name: $scope.dateListName,repeat:$scope.repeat};
                  $modalInstance.close($scope.dateList);
                  return false;
                })
              }else {
                $scope.dateList = {selectedDays: $scope.selectedDays, name: $scope.dateListName,repeat:$scope.repeat};
                $modalInstance.close($scope.dateList);
              }
            }else
            {
              growl.error("There are some required fields.",{ttl:1000})
            }
          }
          $scope.cancel=function(){
            $modalInstance.dismiss();
          }
        }],
        size: 'md',
        resolve: {
          datelist: function () {
            return date;
            //return $scope.stations[stationIndex];
          },
          isNew:function() {
            return isCreate;
          },
          dep_id:function(){
            return deployment._id;
          }
        }
      }).result.then(function (selectedDay) {
       // console.log(selectedDay);
        if (!selectedDay) {
          return false;
        }
        var dateList = {
          name: selectedDay.name,
          dates: _.sortBy( selectedDay.selectedDays),
          repeat: selectedDay.repeat,
          tenant_id: currentUser.tenant_id,
          deployment_id: deployment._id
        }
        if (isCreate) {
          DateList.save({}, dateList).$promise.then(function (result) {
            d.resolve();
            growl.success('Selected dates added to the date list.', {ttl: 2000});
            // if (isCreate) {
            $scope.dateLists.push(result);
            /*} else {
             _.forEach($scope.dateLists, function (dateList, i) {
             if (result.name == dateList.name) {
             $scope.dateLists.splice(i, 1);
             }
             });
             $scope.dateLists.push(angular.copy( dateList));
             }*/
            console.log(result);
          }).catch(function (err) {
            d.resolve();
            growl.error('There are some problem creating datelist.', {ttl: 1000});
          });

        } else {
          console.log(date);
          DateList.update({id: date._id}, dateList).$promise.then(function (result) {
            growl.success('Selected dates added to the date list.', {ttl: 2000});
            angular.forEach($scope.dateLists, function (dateList, i) {
              if (result._id == dateList._id) {
                $scope.dateLists.splice(i, 1);
              }
            });
            $scope.dateLists.push(angular.copy( result));
            console.log(result);
            d.resolve();
          }).catch(function (err) {
            d.resolve();
            growl.error('There are some problem creating datelist.', {ttl: 1000});
          })
        }
      });
      return d.promise;
    };
    $scope.deleteDataList=function(dateList){
      $ngBootbox.confirm('Do you really want to delete '+dateList.name+' ?')
        .then(function() {
          Offer.countOffer({deployment_id:deployment._id,Id:dateList._id,isCode:false}).$promise.then(function(res){
            if(res.count==0){
              DateList.destroy({id:dateList._id}).$promise.then(function(){
                angular.forEach($scope.dateLists, function (dl, i) {
                  if (dl._id == dateList._id) {
                    $scope.dateLists.splice(i, 1);
                  }
                });
              });
            }else{
              growl.error('DataList is used in the offer!!',{ttl:2000});
            }
          });

        }, function() {
          console.log('Confirm dismissed!');
        });
    }

    $scope.datePicker={openedFrom:false,openedTo:false};
    $scope.codeForm={name:'',validFrom: '',validTo: '',noOfCodes:'',codePrefix:'',deployment_id:deployment._id,tenant_id:currentUser.tenant_id};

    $scope.createCodes=function(offerCodeForm){
      console.log(offerCodeForm);
      if(!$scope.codeForm.codeInEdit) {
        OfferCode.findByPrefix({
          deployment_id: deployment._id,
          tenant_id: currentUser.tenant_id,
          codePrefix: $scope.codeForm.codePrefix
        }).$promise.then(function (pFix) {
          if (pFix.length == 0) {

            OfferCode.findByName({
              deployment_id: deployment._id,
              tenant_id: currentUser.tenant_id,
              name: $scope.codeForm.name
            }).$promise.then(function (cf) {
              if (cf.length == 0) {
                OfferCode.create({}, $scope.codeForm).$promise.then(function (offerCode) {
                  console.log(offerCode);
                  growl.success('Codes created.', {ttl: 1000});
                  $scope.clearCodeForm();
                  offerCodeForm.$setPristine = true;
                  $scope.offerCodes.push(offerCode);

                });
              } else {
                growl.error('Duplicate code name found.', {ttl: 1000});
              }
            })
          } else {
            growl.error('Duplicate prefix found.', {ttl: 1000});
          }
        });
      }else{

        OfferCode.update({},$scope.codeForm).$promise.then(function (offerCode) {
          console.log(offerCode);
          growl.success('Codes created.', {ttl: 1000});
          $scope.clearCodeForm();
          offerCodeForm.$setPristine = true;
          $scope.offerCodes.splice(Utils.arrayObjectIndexOf($scope.offerCodes,'_id',offerCode._id),1);
          $scope.offerCodes.push(offerCode);

        });
      }
    }

    $scope.open = function($event) {
     // console.log($scope.openedFrom);
      $event.preventDefault();
      $event.stopPropagation();
      $scope.datePicker.openedFrom = true;
    };
    //$scope.model=[];  //={openedFrom:false,openedTo:false};
    $scope.openTo = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.datePicker.openedTo= true;
    };

    $scope.deleteOfferCode=function(offerCode){
      $ngBootbox.confirm('Do you really want to delete '+offerCode.name+' ?')
        .then(function() {
          Offer.countOffer({deployment_id:deployment._id,Id:offerCode._id,isCode:true}).$promise.then(function(res) {
            if (res.count == 0) {
              OfferCode.destroy({id: offerCode._id}).$promise.then(function () {
                $scope.offerCodes.splice(Utils.arrayObjectIndexOf($scope.offerCodes, '_id', offerCode._id), 1);
                growl.success('Deleted successfully.', {ttl: 1000});
              })
            }else{
              growl.error('CodeList is already used in offer!!', {ttl: 2000});
            }
          });
        }, function() {
          console.log('Confirm dismissed!');
        });
    };

    $scope.editOfferCode=function(offerCode){
      $scope.isOfferEditable=true;
      $scope.codeForm=angular.copy( offerCode);
      $scope.codeForm.codeInEdit=true;

    }

    $scope.clearCodeForm=function(){
      $scope.codeForm = {
        name: '',
        validFrom: '',
        validTo: '',
        noOfCodes: '',
        codePrefix: '',
        deployment_id: deployment._id,
        tenant_id: currentUser.tenant_id,
        codeInEdit:false
      };
    }
    $scope.showOfferCodes=function(offerCode){
      $modal.open({
        templateUrl: 'app/admin/deployments/addDeployment/showOfferCodes.html',
        controller: ['$scope', '$modalInstance', 'growl','offercode', function ($scope, $modalInstance, growl,offercode) {
          $scope.offerCode=offercode;
          console.log($scope.offerCode);
          $scope.codes=[];
          for(var i=1;i<=$scope.offerCode.noOfCodes;i++){
            $scope.codes.push($scope.offerCode.codePrefix+ i.toString());
          };
          $scope.cancel=function(){
            $modalInstance.dismiss();
          };
          $scope.exportToExcel=function(){

            /*  var blob = new Blob([document.getElementById('exportable').innerHTML], {
             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
             });
             saveAs(blob, "Codes.xls");*/
            var html='';
            html+=' <table  >'
            html+='<tr ><td > <span style="font-weight: bold">'+offerCode.name+'</span></td> <td >Valid From</td> <td >Valid To</td> </tr>'
            _.forEach($scope.codes,function(code){
              html+=' <tr> <td>'+code+'</td> <td>'+Utils.getDateFormattedDate(new Date( offerCode.validFrom)).toString() +'</td> <td>'+ Utils.getDateFormattedDate(new Date( offerCode.validTo)).toString() +'</td> </tr>';
            });
            html+=' </table>';
            //    console.log(html);
            window.open('data:application/vnd.ms-excel,'+ html);
          };


        }],
        size: 'md',
        resolve: {
          offercode: function () {
            return offerCode;
          }
        }
      })
    };

    /*Offer animation starts here*/
    $scope.openedStage=0;
    $scope.disableOffer={defineOffer:false,criteria:false,offerItem:false,code:false}
    $scope.disableOfferExcept=function(type) {
      if (type == 0) {
        $scope.disableOffer.defineOffer= false;
        $scope.disableOffer.criteria= false;
        $scope.disableOffer.offerItem = false;
        $scope.disableOffer.code=false;
        return false;
      }
      $scope.disableOffer.defineOffer= true;
      $scope.disableOffer.criteria= true;
      $scope.disableOffer.offerItem = true;
      $scope.disableOffer.code=true;
      if (type == 1) {
        $scope.disableOffer.defineOffer= false;
      }
      if (type == 2) {
        $scope.disableOffer.criteria= false;
      }
      if (type == 3) {
        $scope.disableOffer.offerItem= false;
      }
      if (type == 4) {
        $scope.disableOffer.code= false;
      }

    }
    $scope.isOfferOpened=false;
    $scope.isInEdit=false;
    $scope.offerShow={defineOffer:true,criteria:true,offerItem:true,code:true};
    $scope.openDefineOffer=function(){
      $scope.openedStage=1;
      if( $scope.isOfferOpened==true){return false;}
      $timeout(function(){
        $scope.getExpended('#defineoffer',true,true).then(function(){
          $scope.isOfferOpened=true;
        });
      },300)
    };

    $scope.dynamicHandler=function(panelToHide,editAttToShow,attToHide,attToShow){
      if(panelToHide==1){
        if(!$scope.validateCreateOffer()){ return false ;};
      }
      if(panelToHide==2){
        if(!$scope.validateCriteria()){ return false ;};
      }
      if(panelToHide==3){

        if(!$scope.validateValidation()){ return false ;};
      }
      if($scope.openedStage<=panelToHide+1)
        $scope.openedStage=panelToHide+1;
      $timeout(function(){
        $scope.showFrameHeaderByNumber(panelToHide,false);
      },400);
      $timeout(function(){
        $scope.getExpended(editAttToShow,true,false);
      },500);
      $scope.getExpended(attToHide,false,false);
      if($scope.isInEdit==false){ $scope.getExpended(attToShow,true,false)}else{$scope.isInEdit=false;$timeout(function(){ $scope.disableOfferExcept(0);},500);}
      /*Special case when offer applicable on*/

      if((panelToHide==1) && ($scope.offer.type.name=='item')&& ($scope.openedStage>2) ){
        $timeout(function(){
          // $scope.getExpended('#criteria',true,false);
          $scope.openEditOffer('#criteria',2,'#editcriteria')
        },500);
      }

    }
    $scope.openEditOffer=function(attribs,frame,editAttrib){
      $scope.disableOfferExcept(frame);
      $scope.getExpended(editAttrib,false,true).then(function(){
        $scope.showFrameHeaderByNumber(frame,true);
        $scope.getExpended(attribs,true,false);
        $scope.isInEdit=true;
      });
    };
    $scope.showFrameHeaderByNumber=function(number,isShow){
      if(number==1){
        $scope.offerShow.defineOffer=isShow;
      }
      if(number==2){
        $scope.offerShow.criteria=isShow;
      }
      if(number==3){
        $scope.offerShow.offerItem=isShow;
      }
      if(number==4){
        $scope.offerShow.code=isShow;
      }
    }

    $scope.getExpended=function(attribs,flag,isTimeOut){
      var target, content;
      if (!target) target = document.querySelector(attribs);
      if (!content) content = target.querySelector('.slideable_content');
      var d=$q.defer();
      if(flag){
        content.style.border = '1px solid rgba(0,0,0,0)';
        var y = content.clientHeight;
        content.style.border = 0;
        target.style.height = y + 'px';
        //  d.resolve();
      }else{
        target.style.height = '0px';
        // d.resolve();
      }
      if(isTimeOut) {
        $timeout(function () {
          d.resolve();
        }, 500);
      }else{d.resolve();}
      return d.promise;
    }
    /*OFFERS FUNCTIONALITY STARTS HERE*/
    $scope.tabTypes=['Table','TakeOut','Delivery','Online'];
    $scope.offer={
      complimentary:false,
      name:'',
      tabs:[],
      type:{name:'',value:''},
      minBillAmount:null,
      minItemCount:null,
      comboAmount:null,
      sameOrLess:true,
      getItemOnly:false,
      getLeastValueItem:false,
      leastItemValueCount:{buyQuantity:null,getQuantity:null},
      applicable:{
        on:'complete',
        items:[],
        relation:'',
        isExclude:false
      },
      offerApplied:{
        items:[],
        relation:''
      },
      valid: {
        date: {startDate: '', endDate: ''},
        time: {startTime: new Date('1 jan 2015 00:00'), endTime: new Date('1 jan 2015 00:00')},
        days: [],
        behaviourDateList: 'Include',
        dateLists: [],
        checkBoxes:{selectedNoDates:false,validAllTime:false,selectedAllDays:false,noDateListMod:false,isUnlimited:false}
      },
      code: {
        codeList:{},
        uses: ''  /*null for unlimited use*/
      },
      deployment_id:deployment._id,
      tenant_id:currentUser.tenant_id
    }

    $scope.applicableItem=[{
      category:'',
      item:'',
      quantity:null,
      relation:'',
      limit:''
    },{
      category:'',
      item:'',
      quantity:null,
      relation:''
    },{
      category:'',
      item:'',
      quantity:null,
      relation:'',
      discountType:'percent',
      value:''
    }]
    $scope.hstep=1;
    $scope.mstep=15;
    $scope.ismeridian=true;

    $scope.applicableItems=[];
    $scope.applicableItems[0]=[];
    $scope.applicableItems[1]=[];
    $scope.applicableItems[2]=[];
    $scope.filterByCategory=function(item){
      if($scope.applicableItem[0].category==null){
        return false;
      }else {
        return item.category._id === $scope.applicableItem[0].category._id;
      }
      //return item.category._id===$scope.applicableItem[0].category._id;
    }
    $scope.filterByCategory1=function(item){
      if($scope.applicableItem[1].category==null){
        return false;
      }else {
        return item.category._id === $scope.applicableItem[1].category._id;
      }
      //return item.category._id===$scope.applicableItem[1].category._id;
    }
    $scope.filterByCategory2=function(item){
      if($scope.applicableItem[2].category==null){
        return false;
      }else {
        return item.category._id === $scope.applicableItem[2].category._id;
      }
    }
    $scope.addItem=function(relation,i){
      //console.log($scope.applicableItem[i]);
      if(i==0) {
        if($scope.offer.applicable.isExclude) {
          if ($scope.applicableItem[i].category == '' ) {
            growl.error('Select category and feed in quantity both.', {ttl: 2000});
            return false;
          }
        }else{
          if ($scope.applicableItem[i].category == '' || $scope.applicableItem[i].quantity == null) {
            growl.error('Select category and feed in quantity both.', {ttl: 2000});
            return false;
          }
        }
      }
      if(i==2){
          if($scope.offer.type.name !='comboOffer'){
            if($scope.applicableItem[i].value==''||$scope.applicableItem[i].value==null){
              growl.error('Add value for percent or amount.',{ttl:2000});
              return false;
            }
        }
      }
      /* if(i==0){
       if($scope.addLimit) {
       if ($scope.applicableItem[0].limit==''||$scope.applicableItem[0].limit==null){
       growl.error('Please select either of limit.',{ttl:2000});
       return false;
       }
       }
       }*/
      if(i==1 || i==2){
        if($scope.applicableItems[i].length>0) {
          if (($scope.applicableItem[i].category == null || $scope.applicableItem[i].category == "") && ($scope.applicableItem[i].item == null || $scope.applicableItem[i].item == "")) {
            growl.error("Cant add all category. ", {ttl: 2000});
            return false;
          }
        }
        for(var j=0;j<$scope.applicableItems[i].length;j++){
         // console.log($scope.applicableItems[i]);
          if(($scope.applicableItems[i][j].category==null||$scope.applicableItems[i][j].category=="") && ($scope.applicableItems[i][j].item==null||$scope.applicableItems[i][j].item=="")){
            growl.error("Cant add more category. ",{ttl:2000});
            return false;
          }
        }
        if(!($scope.offer.getLeastValueItem||($scope.offer.type.name=='comboOffer'))){
          if($scope.applicableItem[i].quantity==null ||$scope.applicableItem[i].quantity==''){
            growl.error("Quantity is required. ", {ttl: 2000});
            return false;
          }
        }else{
          if($scope.offer.type.name=='comboOffer' && i==1){
            if($scope.applicableItem[i].item == null || $scope.applicableItem[i].item == ""){
              growl.error('Item is not selected.',{ttl:2000});
              return false;
            }         
          }
        }
      }

      $scope.applicableItem[i].relation=relation;
      // delete $scope.applicableItem[i].category.superCategory;
      //  delete $scope.applicableItem.item.category;
      // delete $scope.applicableItem[i].item.tabs;
      $scope.applicableItems[i].push(angular.copy( $scope.applicableItem[i]));
      if($scope.applicableItem[i].item==''){
        $scope.availableCategories[i].splice(Utils.arrayObjectIndexOf($scope.availableCategories[i],'_id',$scope.applicableItem[i].category._id),1);
      }else{
        $scope.availableItems[i].splice(Utils.arrayObjectIndexOf($scope.availableItems[i],'_id',$scope.applicableItem[i].item._id),1);
      }
      $scope.applicableItem[i].category='';
      $scope.applicableItem[i].item='';
      $scope.applicableItem[i].quantity=null;
      /*if(i=0){}*/
      if(i==2){$scope.applicableItem[i].value='';$scope.applicableItem[i].discountType='percent';};
      // console.log($scope.applicableItems);
      //   $scope.addLimit=false;

      if($scope.offer.applicable.on=='item'){ $scope.applicableItem[0].limit='min';}
      if($scope.offer.applicable.on=='itemCriteria'){ delete $scope.applicableItem[0].limit;}
      $scope.applicableItem[i].category="Select Category";
    }
    $scope.changeApplicable=function(i){
      var target;
      var index=i;
      if($scope.offer.applicable.on!='itemCriteria'){$scope.offer.applicable.isExclude=false;}
      if($scope.offer.applicable.on=='item')
      {$scope.addLimit=true;$scope.applicableItem[0].limit='min';
        for(var z=0;z<$scope.applicableItems[0].length;z++) {
          //$scope.removeItem(z,0);
          if($scope.applicableItems[0][z].item==''){
            $scope.availableCategories[0].push($scope.applicableItems[0][z].category);
          }else
          {
            $scope.availableItems[0].push(angular.copy( $scope.applicableItems[0][z].item));
          }
        };
        $scope.applicableItems[0]=[];
        $scope.availableItems[0]= _.sortBy($scope.availableItems[0],['name']);
      }
      else{
        $scope.addLimit=false;delete $scope.applicableItem[0].limit;
        for(var z=0;z<$scope.applicableItems[0].length;z++){
          // $scope.removeItem(z,0);
          if($scope.applicableItems[0][z].item==''){
            $scope.availableCategories[0].push($scope.applicableItems[0][z].category);
          }else
          {
            $scope.availableItems[0].push(angular.copy( $scope.applicableItems[0][z].item));
          }
        };
        $scope.applicableItems[0]=[];
        $scope.availableItems[0]= _.sortBy($scope.availableItems[0],['name']);
      }
      if (!target) target = document.querySelector('#criteria');
      var height=parseInt( target.style.height.substring(0,target.style.height.length-2));
      /*  console.log(height);*/
      if($scope.offer.applicable.on=='item'||$scope.offer.applicable.on=='itemCriteria') {
        target.style.height = (height>200?height: (355+height)) + 'px';
      }else {

        //  var tempEvent= angular.copy( $scope.offer.applicable.on);
        $ngBootbox.confirm('Your selected items will reset . still want to change?').then(function(){
          $scope.applicableItems[0]=[];
          target.style.height = (height-355) + 'px';
        },function(){
          $timeout(function(){
            $scope.offer.applicable.on='item';
          },100);
        })
      }
    }
    $scope.removeItem=function(index,i){
      if($scope.applicableItems[i][index].item==''){
        $scope.availableCategories[i].push($scope.applicableItems[i][index].category);
      }else
      {
        $scope.availableItems[i].push(angular.copy( $scope.applicableItems[i][index].item));
      }
      $scope.applicableItems[i].splice(index,1);
      if($scope.applicableItems[i].length==0){
        $scope.applicableItem[i].relation='';
      }
      $scope.availableCategories[i]= _.sortBy($scope.availableCategories[i],['categoryName']);
      $scope.availableItems[i]= _.sortBy($scope.availableItems[i],['name']);
    }
    $scope.clearOfferComplimentary=function(){
      $scope.offer.complimentary=false;
    }
    $scope.getOfferType=function(){
      $scope.offer.complimentary=false;
      $scope.offer.getItemOnly=false;
      var target;
      if (!target) target = document.querySelector('#criteria');
      var height=parseInt( target.style.height.substring(0,target.style.height.length-2));
      if(height>0){
        if($scope.offer.type.name=='item'){
          target.style.height=515 + 'px';
        }else{
          if($scope.offer.type.name=='comboOffer')
          target.style.height=455 + 'px';
        else
          target.style.height=169 + 'px';
        }
      }
      if($scope.offer.type.name=='item'){
        $scope.offer.applicable.on='complete';
        $scope.applicableItems[0]=[];
        $scope.applicableItem[0].relation='';
      }
      if($scope.offer.applicable.on=='item'||$scope.offer.applicable.on=='itemCriteria'){
        $scope.applicableItems[1]=[];
        $scope.applicableItem[1].relation='';
        $scope.applicableItems[2]=[];
        $scope.applicableItem[2].relation='';
      }
      if($scope.offer.applicable.on=='complete'){
        $scope.applicableItems[0]=[];
        $scope.applicableItem[0].relation='';
        $scope.applicableItems[1]=[];
        $scope.applicableItem[1].relation='';
        $scope.applicableItems[2]=[];
        $scope.applicableItem[2].relation='';
      }
      /* $scope.offer.type.value='';
       if($scope.offer.type.name=='item'){
       for(var i=0;i<$scope.applicableItems.length;i++){
       if($scope.applicableItems[i].item==''){$scope.availableCategories.push($scope.applicableItems[i].category);}else
       {$scope.availableItems.push(angular.copy( $scope.applicableItems[i].item));}
       }
       $scope.applicableItems=[];
       if($scope.applicableItems.length==0){
       $scope.applicableItem.relation='';
       }
       }*/
    }
    $scope.validateCreateOffer=function(){
      if($scope.offer.name==''){growl.error('Feed in offer name.',{ttl:2000}); return false;}
      if($scope.offer.tabs.length==0){growl.error('Select at least one tab to enable offer.',{ttl:2000}); return false;}
      if($scope.offer.type.name==""){growl.error('Select offer type.',{ttl:2000}); return false;};
      if(!($scope.offer.type.name=='item' || $scope.offer.type.name=='comboOffer' )){
        if($scope.offer.type.value==""){growl.error('Feed in value to offer type.',{ttl:2000}); return false;}
        if($scope.offer.type.name=='percent'){
          if($scope.offer.type.value>100){growl.error('Percentage value shold be less than 100.',{ttl:2000}); return false;}
        }
      }
      return true;
    }
    $scope.validateCriteria=function(){
      //if($scope.offer.minBillAmount==''){growl.error('Feed in min bill amount name.',{ttl:2000}); return false;};
      //if($scope.offer.minItemCount==''){growl.error('Feed in offer name.',{ttl:2000}); return false;};
      if($scope.offer.type.name=='comboOffer'){
        if($scope.applicableItems[1].length<2){
          growl.error('Select atleast 2 combo items!!',{ttl:2000});
          return false;
        }
        if($scope.offer.comboAmount==''|| parseFloat($scope.offer.comboAmount)<=0 ||$scope.offer.comboAmount==null || $scope.offer.comboAmount==undefined){
          growl.error('Combo amount should have value.',{ttl:2000});
          return false;
        }
        if($scope.isReplaceItemSelected()){
           if($scope.applicableItems[2].length==0){
            growl.error('Select atleast 1 replaceable item!!',{ttl:2000});
            return false;
          } 
        }
      }

      if($scope.offer.type.name=='item'  ){
        if($scope.offer.getItemOnly){
          $scope.applicableItems[1]=[];
        }
        if( $scope.applicableItems[1].length==0 && !($scope.offer.getItemOnly))
        {
          growl.error('Add at least one buy item.', {ttl: 2000});
          return false;
        }
        if(!$scope.offer.getLeastValueItem){
          if($scope.applicableItems[2].length==0)
          {
            growl.error('Add at least one get item.', {ttl: 2000});
            return false;
          }
        }else{
          //console.log($scope.offer.leastItemValueCount);
          if($scope.offer.leastItemValueCount.getQuantity==null || $scope.offer.leastItemValueCount.buyQuantity==null || $scope.offer.leastItemValueCount.getQuantity=='' || $scope.offer.leastItemValueCount.buyQuantity==''){
            growl.error("Buy and get item quantity should have value.",{ttl:2000});
            return false;
          }
          if(parseInt( $scope.offer.leastItemValueCount.getQuantity) > parseInt( $scope.offer.leastItemValueCount.buyQuantity)){
            growl.error("Get quantity should less than buy quantity.",{ttl:2000});
            return false;
          }
        }
      }
      if(($scope.offer.applicable.on=='item'||$scope.offer.applicable.on=='itemCriteria')&& $scope.offer.type.name!='item' ){
        if($scope.applicableItems[0].length==0)
        {
          growl.error('Add at least one buy item.', {ttl: 2000});
          return false;
        }
      }

      return true;
    }
    $scope.validateValidation=function(){
      console.log($scope.offer.valid);
      if(!($scope.offer.valid.date.startDate==null||$scope.offer.valid.date.startDate=='')){
        $scope.offer.valid.date.startDate= Utils.convertDate($scope.offer.valid.date.startDate);
      }
      if(!($scope.offer.valid.date.endDate==null||$scope.offer.valid.date.endDate=='')){
        $scope.offer.valid.date.endDate= Utils.convertDate($scope.offer.valid.date.endDate);
      }
      if(!($scope.offer.valid.date.startDate==''||$scope.offer.valid.date.startDate==null))
      {
        if(($scope.offer.valid.date.endDate==''||$scope.offer.valid.date.endDate==null)){
          growl.error('Select end date.',{ttl:2000});
          return false;
        }
      }
      if(!($scope.offer.valid.date.endDate==''||$scope.offer.valid.date.endDate==null))
      {
        if(($scope.offer.valid.date.startDate==''||$scope.offer.valid.date.startDate==null)){
          growl.error('Select start date.',{ttl:2000});
          return false;
        }
      }
      if(!($scope.offer.valid.date.startDate==''||$scope.offer.valid.date.startDate==null)&&!($scope.offer.valid.date.endDate==''||$scope.offer.valid.date.endDate==null)){
        if(Utils.getDateFormattedDate($scope.offer.valid.date.startDate)>   Utils.getDateFormattedDate($scope.offer.valid.date.endDate)){
          growl.error('Start date should be less than end date.',{ttl:2000});
          return false;
        }
      }
      if($scope.offer.valid.days.length==0){
        growl.error('Select at least one day !',{ttl:2000});
        return false;
      }
      return true;
    }

    $scope.openedValidFrom=false;
    $scope.openedValidTo=false;
    $scope.openValidFrom = function($event) {
      console.log($scope.openedFrom);
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedValidFrom= true;
    };
    //$scope.model=[];  //={openedFrom:false,openedTo:false};
    $scope.openValidTo = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedValidTo= true;

    };
    $scope.validDate={fromDate:'',toDate:''};
    $scope.time={from:new Date(2000, 0, 1, 0, 0, 0),to:new Date(2000, 0, 1, 0, 0, 0)};
    $scope.openCreateDateList=function(){
      $scope.openDataListModal('',true).then(function(){

      });
    }

    $scope.addOfferDay=function(day){
      var isFound=false;
      for(var i=0;i<$scope.offer.valid.days.length;i++){
        if($scope.offer.valid.days[i]==day){
          isFound=true;
          $scope.offer.valid.days.splice(i,1);
          $scope.offer.valid.checkBoxes.selectedAllDays=false;
          break;
        }
      }
      if(!isFound){
        $scope.offer.valid.days.push(day);
        if($scope.offer.valid.days.length==7){$scope.offer.valid.checkBoxes.selectedAllDays=true;}
      }
      console.log($scope.offer.valid.days);
    };
    $scope.selectedDateLists=[];
    $scope.showDateLists=function(){
      // $scope.selectedDateLists=[];
      $modal.open({
        templateUrl:'app/offer/pages/showOfferModal.html',
        controller:['$scope','$modalInstance','dateLists','selectedDateLists',function($scope,$modalInstance,dateLists,selectedDateLists){
          $scope.dateLists=dateLists;
          $scope.dateListsSelected=angular.copy( selectedDateLists);

          $scope.close=function(){
            // console.log($scope.dateListsSelected);
            $modalInstance.close($scope.dateListsSelected);
          };
          $scope.cancel=function(){
            $modalInstance.dismiss();
          };
        }],
        size:'md',
        resolve:{
          dateLists:function(){
            return $scope.dateLists;
          },
          selectedDateLists:function(){
            return $scope.selectedDateLists;
          }
        }
      }).result.then(function(selectedDateList){
        console.log(selectedDateList);
        $scope.selectedDateLists=selectedDateList;
        $scope.offer.valid.dateLists=[];
        _.forEach($scope.selectedDateLists,function(_selectedDateList){
          var _dates=  _.filter($scope.dateLists,{"_id":_selectedDateList._id});
          if(_dates.length>0) {
            var tdatelist=angular.copy(_dates[0]);
            var tempDates=[];
            _.forEach(tdatelist.dates,function(date){
              var tempDate={date:'',selected:true};
              tempDate.date=angular.copy( date);
              tempDates.push(tempDate);
            })
            tdatelist.dates=tempDates;
            $scope.offer.valid.dateLists.push(tdatelist);
          }
          /*  if(_dates.length>0){
           var _f= _.filter($scope.offer.valid.dateLists,{"_id":_dates._id});
           if(_f.length>0)
           $scope.offer.valid.dateLists.splice(Utils.arrayObjectIndexOf($scope.offer.valid.dateLists,"_id",_f._id) );
           else
           { $scope.offer.valid.dateLists.push(_dates[0]);}
           }else
           {
           $scope.offer.valid.dateLists.splice(Utils.arrayObjectIndexOf($scope.offer.valid.dateLists,"_id",_dates._id) );
           }*/
        });
        // console.log($scope.offer.valid.dateList.dates);
        $scope.updateAnimationCodeList();
      });
    };
    $scope.updateAnimationCodeList=function(){
      var target;
      if (!target) target = document.querySelector('#offerItem');
      var height=parseInt( target.style.height.substring(0,target.style.height.length-2));
      console.log(height);
      if($scope.offer.valid.dateLists.length>0){
        target.style.height=828 + 'px';
      }else
      {   target.style.height=382 + 'px';}
    }
    $scope.selectedCode={};
    $scope.createCodesModal=function(){
      $modal.open({
        templateUrl: 'app/offer/pages/showOfferCodeModal.html',
        controller: ['$scope', '$modalInstance', 'offerCodes','codeForm','deployment','currentUser','growl','OfferCode', function ($scope, $modalInstance, offerCodes,codeForm,deployment,currentUser,growl,OfferCode) {
          $scope.offerCodes = offerCodes;
          $scope.codeForm=codeForm;
          $scope.datePicker={openedFrom:false,openedTo:false};
          $scope.open = function($event) {
            console.log($scope.openedFrom);
            $event.preventDefault();
            $event.stopPropagation();
            $scope.datePicker.openedFrom = true;
          };
          //$scope.model=[];  //={openedFrom:false,openedTo:false};
          $scope.openTo = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.datePicker.openedTo= true;
          };

          $scope.createCodes = function (offerCodeForm){
            // console.log($scope.dateListsSelected);
            console.log(offerCodeForm);
            OfferCode.findByPrefix({deployment_id:deployment._id,tenant_id:currentUser.tenant_id,codePrefix:$scope.codeForm.codePrefix }).$promise.then(function(pFix) {
              if (pFix.length == 0) {
                OfferCode.findByName({
                  deployment_id: deployment._id,
                  tenant_id: currentUser.tenant_id,
                  name: $scope.codeForm.name
                }).$promise.then(function (cf) {
                  if (cf.length == 0) {
                    OfferCode.create({}, $scope.codeForm).$promise.then(function (offerCode) {
                      console.log(offerCode);
                      growl.success('Codes created.', {ttl: 1000});
                      $scope.codeForm = {
                        name: '',
                        validFrom: '',
                        validTo: '',
                        noOfCodes: '',
                        codePrefix: '',
                        deployment_id: deployment._id,
                        tenant_id: currentUser.tenant_id
                      };
                      offerCodeForm.$setPristine = true;
                      //$scope.offerCodes.push(offerCode);
                      $modalInstance.close(offerCode);
                    });
                  } else {
                    growl.error('Duplicate code name found.', {ttl: 1000});
                  }
                })
              }else{
                growl.error('Duplicate prefix found.', {ttl: 1000});
              }
            });
          };
          $scope.cancel = function () {
            $modalInstance.dismiss();
          };
        }],
        size: 'md',
        resolve: {
          offerCodes: function () {
            return $scope.offerCodes;
          },
          codeForm: function () {
            return $scope.codeForm;
          },
          deployment:function(){
            return deployment;
          },
          currentUser:function(){
            return currentUser;
          }
        }
      }).result.then(function(offerCode){
        $scope.offerCodes.push(offerCode);
      })
    };
    $scope.clearCode=function(){
      $scope.offer.code.uses='';
    }
    $scope.clearDates=function(){
      $scope.offer.valid.date.startDate='';
      $scope.offer.valid.date.endDate='';
    }
    $scope.clearTime=function(){
      $scope.offer.valid.time.startTime= new Date(2000, 0, 1, 0, 0, 0);
      $scope.offer.valid.time.endTime= new Date(2000, 0, 1, 0, 0, 0);
    }
    $scope.updateAllDays=function(){
      if(!$scope.offer.valid.checkBoxes.selectedAllDays){
        $scope.offer.valid.days=[];
      }else {
        $scope.offer.valid.days = [];
        $scope.offer.valid.days.push('sunday');
        $scope.offer.valid.days.push('monday');
        $scope.offer.valid.days.push('tuesday');
        $scope.offer.valid.days.push('wednesday');
        $scope.offer.valid.days.push('thursday');
        $scope.offer.valid.days.push('friday');
        $scope.offer.valid.days.push('saturday');
      }
    }
    $scope.noDateList=function(){
      $scope.offer.valid.dateLists=[];
      $scope.selectedDateLists=[];
      $scope.updateAnimationCodeList();
    }
    $scope.offerPublished=false;

    $scope.publishOffer=function(){
      if($scope.offer.getLeastValueItem){
        $scope.offer.leastItemValueCount.buyQuantity=parseInt($scope.offer.leastItemValueCount.buyQuantity);
        $scope.offer.leastItemValueCount.getQuantity=parseInt($scope.offer.leastItemValueCount.getQuantity);
      }

      $scope.offerPublished=true;
      if($scope.offer.code.uses>0) {
        if ($scope.offer.code.codeList == null) {
          growl.success('Select code list for the required uses. ', {ttl: 2000});
          $scope.offerPublished=false;
          return false;
        }
      }

      if($scope.offer.type.name=='item' || $scope.offer.type.name=='comboOffer' ){
        if($scope.applicableItems[1].length>0){
          $scope.offer.applicable.items=$scope.applicableItems[1];
        }else{
          $scope.offer.applicable.items=[];
        }
        if($scope.applicableItems[2].length>0){
          $scope.offer.offerApplied.items=$scope.applicableItems[2];
        }else{
          $scope.offer.offerApplied.items=[];
        }
      }else {
        if($scope.offer.applicable.on=='item'||$scope.offer.applicable.on=='itemCriteria'){
          if($scope.applicableItems[0].length>0){
            $scope.offer.applicable.items=$scope.applicableItems[0];
          }else{
            $scope.offer.applicable.items=[];
          }
        }
      }
      //  console.log($scope.offer);
      Offer.findByName({deployment_id:deployment._id,name:$scope.offer.name}).$promise.then(function(offers){
        console.log(offers);
        var isDuplicate=false;
        if(offers.length>0){
          if($state.params.id!=null){
            var f_offer= _.filter(offers,{_id:$state.params.id});
            if(f_offer.length>0){
              if(offers.length>1){
                isDuplicate=true;
              }
            }else{
              isDuplicate=true;
            }
          }else{
            isDuplicate=true;
          }

        }
        if(isDuplicate){
          growl.error("Duplicate offer name found !!",{ttl:2000});
          $scope.offerPublished=false;
          return false;
        }
        if($state.params.id!=null){
          Offer.update($scope.offer).$promise.then(function (offer) {
            $scope.offers.push(offer);
            growl.success('Offer updated successfully.', {ttl: 1000});
            $location.path('/offer/show')
          })
        }
        else {
          Offer.saveData($scope.offer).$promise.then(function (offer) {
            $scope.offers.push(offer);
            growl.success('Offer created successfully.', {ttl: 1000});
            $location.path('/offer/show');
          })
        }
      })

    }
    $scope.addLimit=false;
    $scope.goToCreate=function(){
      $location.path('/offer/create')
    }
    $scope.editOffer=function(id){
      $location.path('/offer/create:id/'+id)
    }
    $scope.showOffer=function(){
      $location.path('/offer/show')
    }
    $scope.showSettings=function(){
      $location.path('/offer/settings')
    }
    $scope.validateOffer=function(offer){
      console.log(  Utils.offerValidated(offer));
    }
    $scope.clearLimit=function(){
      console.log($scope.addLimit);
      if(!$scope.addLimit){
        $scope.applicableItem[0].limit='min';
      }
    }
    $scope.deleteOffer=function(offer){
      $ngBootbox.confirm('Do you really want to delete '+ offer.name+'  offer ?')
        .then(function() {
          Offer.delete({},{_id:offer._id}).$promise.then(function(res){
            console.log(res._id);
            $scope.offers.splice(Utils.arrayObjectIndexOf($scope.offers,'_id',res._id),1);
            growl.success('Offer deleted successfully.',{ttl:2000});
          });
        }, function() {
          console.log('Confirm dismissed!');
        });
    }
    $scope.$watch('applicableItem[2].value', function(newVal, oldVal) {
      console.log($scope.applicableItem[2].discountType);
      if($scope.applicableItem[2].discountType=='percent') {
        if (newVal > 100) {
          growl.error('Value should not be greater then 100',{ttl:1000})
          $scope.applicableItem[2].value = 100;
        }
      }
    });
    $scope.changeDiscountType=function(){
      if($scope.applicableItem[2].discountType=='percent' && ($scope.applicableItem[2].value>100)){
        growl.error('Value should not be greater then 100',{ttl:1000});
        $scope.applicableItem[2].value=100;
      }
    }
    $scope.changeOfferCode=function(){
      console.log($scope.offer.code.codeList);
      if($scope.offer.code.codeList==null){
        delete $scope.offer.code.codeList;
      }
    }
    $scope.isReplaceComboSelected=function(isSelected){
      var flag=true;
      if(isSelected){
        return true;
      }else{
        for(var i=0;i<$scope.applicableItems[1].length;i++){
          if($scope.applicableItems[1][i].item.isReplaceable){
            flag=false;
            break;
          }
        }
      }
      return flag;
    }

     $scope.isReplaceItemSelected=function(){
      var flag=false;
        for(var i=0;i<$scope.applicableItems[1].length;i++){
          if($scope.applicableItems[1][i].item.isReplaceable){
            flag=true;
            break;
          }
        }
      return flag;
    }
  }]);
