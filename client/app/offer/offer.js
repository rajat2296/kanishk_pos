'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('offer', {
        url: '/offer',
        templateUrl: 'app/offer/offer.html',
        controller: 'OfferCtrl',
          resolve: {
           /* currentUser: ['$state', '$stateParams', 'Auth',
              function ($state, $stateParams, Auth) {
                return Auth.getCurrentUser().$promise.then(function (user) {
                  return user;
                });
              }]*/

              currentUser: ['$state', '$stateParams', 'Auth', 'Utils', 'growl', '$location',
                function ($state, $stateParams, Auth, Utils, growl, $location) {
                  var flag = true;
                  return Auth.getCurrentUser().$promise.then(function (user) {
                    var flag=false;
                    if (user.role === 'user') {
                      if (Utils.hasUserPermission(user.selectedPermissions,'Offers')) {
                        flag = true;
                      }
                      if(!flag){
                        growl.error('No permission to view offers. !!',{ttl:3000});
                        Auth.logout();
                        $location.path('/login');
                        return false;
                      }
                    }
                    return user;
                  });
                }]
            ,
            deployments: ['$q', 'Deployment', 'localStorageService', function ($q, Deployment, localStorageService) {
              var d = $q.defer();
              Deployment.get({tenant_id: localStorageService.get('tenant_id'), isMaster: false}, function (deployments) {
                //console.log(deployments);
                d.resolve(deployments);
              }, function (err) {
                d.reject();
              });
              return d.promise;
            }],
            deployment: ['$state', '$stateParams', 'Deployment', 'growl', '$timeout','$rootScope','localStorageService',
              function ($state, $stateParams, Deployment, growl, $timeout,$rootScope,localStorageService) {
                return Deployment.findOne({id:localStorageService.get( 'deployment_id')}).$promise.then(function (d) {
                  var tempSettings=angular.copy( d.settings);
                  d.settings=[];
                  var allSettings=[];
                  var isFound=false;
                  _.forEach(tempSettings,function(setting){
                    isFound=false;
                    for(var i=0;i<$rootScope.settings.length;i++){
                      if($rootScope.settings[i].name==setting.name){
                        isFound=true;
                        break;
                      }
                    }
                    if(isFound){allSettings.push(angular.copy( setting));}
                  });
                  _.forEach($rootScope.settings,function(setting){
                    var _fS= _.filter(allSettings,{'name':setting.name});
                    if(_fS.length==0){
                      allSettings.push(angular.copy( setting));
                    }
                  });
                  // console.log($rootScope.settings.length);
                  //console.log(allSettings);
                  d.settings=allSettings;
                  return d;
                }).catch(function (err) {
                  growl.error("Couldn't resolve deployment!", {ttl: 3000});
                  $state.transitionTo("admin");
                });

              }]
          }
      })
        .state('offer.create', {
          url: '/create',
          templateUrl: 'app/offer/pages/create.html',
          controller: 'OfferCtrl',
          parent: 'offer'
        })
        .state('offer.create.edit', {
          url: '/create/:id',
          templateUrl: 'app/offer/pages/create.html',
          controller: 'OfferCtrl',
          parent: 'offer'
        })
        .state('offer.show', {
          url: '/show',
          templateUrl: 'app/offer/pages/show.html',
          controller: 'OfferCtrl',
          parent: 'offer'
        })
        .state('offer.settings', {
          url: '/settings',
          templateUrl: 'app/offer/pages/settings.html',
          controller: 'OfferCtrl',
          parent: 'offer'
        });
    });