'use strict';

angular.module('posistApp')
	.controller('PosToDLFCtrl',['$scope','$resource','currentUser','Utils','growl','Report','localStorageService','$rootScope','$filter','deployments','POSToDLF','$location','$timeout','$http', function ($scope,$resource,currentUser,Utils,growl,Report,localStorageService,$rootScope,$filter,deployments,POSToDLF,$location,$timeout,$http) {
		console.log("currentUser : ", currentUser);
		$scope.dlfFormData=[];
    $scope.host={};
		/* Bill Resource*/
		$scope.billResource = $resource('/api/bills/:controller', {}, {
			getDataRangeBillsNew: {
				method: 'GET',
				isArray: false,
				params: {
					controller: 'getDataRangeBillsNew'
				}
			},
			getDataRangeBills: {
				method: 'GET',
				isArray: true,
				params: {
					controller: 'getDataRangeBills'
				}
			},
			getDataRangeBillsCustom: {
				method: 'POST',
				isArray: true,
				params: {
					controller: 'getDataRangeBillsCustom'
				}
			}
		});

		/* Bill Resource */
		$scope.uploadResource = $resource('/api/PosToMIs/:controller', {}, {
			UploadTextFiles: {method: 'POST',isArray: false,params: {controller: 'UploadTextFiles'}}
		});

		$scope.dlfForm={};
		POSToDLF.get({tenant_id:currentUser.tenant_id,deployment_id:currentUser.deployment_id}).$promise.then(function (result){
			if(result[0]!=undefined){
				$scope.dlfFormData=result;
				$scope.dlfForm=angular.copy($scope.dlfFormData[0]);
				console.log($scope.dlfForm);
			}
		});
		$scope.createDLF=function(formDLF){
			if(formDLF.$valid){
				$scope.dlfForm.tenant_id=currentUser.tenant_id;
				$scope.dlfForm.deployment_id=$scope.currentUser.deployment_id;
				if($scope.dlfFormData.length!=0)
				{
					POSToDLF.update({},$scope.dlfForm, function (result){
						growl.success('Successfully Updated', {ttl: 3000});
						$scope.dlfFormData[0]=angular.copy(result);
					});
				}
				else
				{
					POSToDLF.saveData({},$scope.dlfForm, function (result){
						$scope.dlfFormData.push(angular.copy(result));
						growl.success('Successfully Created', {ttl: 3000});
					});
				}
			}
		};

		$scope.clearStoreForm=function(){
			if($scope.dlfFormData && $scope.dlfFormData.length>0)
				$scope.dlfForm=angular.copy($scope.dlfFormData[0]);
			else
			  $scope.dlfForm={};	
		}



		$scope.pos2MiForm={
			fromDate:new Date(),
			toDate:new Date(),
			maxDate:new Date()
		};

		$scope.checkDateGreater= function(date){
			var d=new Date(date);
			var dd=new Date($scope.pos2MiForm.toDate);
			if(d.getTime()>dd.getTime()){
				growl.success('Greater than toDate', {ttl: 3000});
				$scope.pos2MiForm.fromDate= new Date();//getYesterDayDate();
			}
		};

		$scope.checkDateLesser= function(date){
			var d=new Date(date);
			var dd=new Date($scope.pos2MiForm.fromDate);
			if(d.getTime()<dd.getTime()){
				growl.success('Can not be Lesser than fromDate', {ttl: 3000});
				$scope.pos2MiForm.toDate= new Date();
			}
		};
		$scope.startSearch = function(whichReport, dates) {

			if($scope.dlfFormData.length==0){
				growl.error('Please Fill The Data First in POS TO Vendor Tab',{ttl: 3000});
			}
			else if(!$scope.dlfForm.ftpSettings){
				growl.error('Please Fill The Data First in FTP Settings Tab',{ttl: 3000});
			}
			else
			{
				/* Report Service - Reset */
				var pos = 1001;
				var tenantId=$scope.dlfForm.dlfTenantId; //"Mahabelly";
				$scope.report = new Report();
				$scope.report.bills = [];
				$scope.report.allTaxes = [];
				$scope.report.rows = [];
				$scope.totalSales = 0;
				$scope.totalTaxes = 0;
				$scope.totalDiscounts = 0;

				//$rootScope.showLoaderOverlay = true;
				//$scope.validateDateSelection($scope.report.filters).then(function() {
				//$scope.validateDeploymentSelection($scope.report.filters).then(function() {
				var _dep= _.filter(deployments,{_id:localStorageService.get('deployment_id')});
				var fromDateWithTime = new Date( Utils.getDateFormatted(Utils.getResetDate(_dep[0].settings , Utils.convertDate( $scope.pos2MiForm.fromDate))));
				var toDateWithTime =   new Date( Utils.getDateFormatted(Utils.getResetDate(_dep[0].settings , Utils.convertDate( $scope.pos2MiForm.toDate))));
				$scope.billResource.getDataRangeBills({
					fromDate:fromDateWithTime,
					toDate:toDateWithTime,
					tenant_id: localStorageService.get('tenant_id'),
					cutOffTimeSetting: $scope.report.filters.cutOffTimeSetting,
					deployment_id: localStorageService.get('deployment_id'),
					complimentary:false
				}).$promise.then(function(bills) {
						$scope.report.addBills(bills).then(function (bills) {
							$scope.report.prepareBillsForReport().then(function () {
								$scope.report.aggregateEachBill().then(function() {
									$scope.report.bills = $filter('orderBy')($scope.report.bills, 'serialNumber', false);
									var days=getUniqueDays(bills);
									var dataArray=[];
									_.forEach(days,function(d,i){
										var tfn = 1;
										var recNo = getBillNos(bills); //receiptNo(myobj);
										var dateForFileName=$filter('date')(d, 'yyMMddHHmm');
										var tfn_str="0000".substr(0,4-(String(tfn)).length)+String(tfn);
										var fileName = "t" + tenantId + "_" + pos + "_" + tfn_str + "_" + dateForFileName+".txt";
										var dd=$filter('date')(d, 'yyyyMMdd');
										var dd1=$filter('date')(d, 'HH:mm:ss');
										var fileData = "1|OPENED|"+tenantId+"|"+pos+"|"+recNo+"|"+tfn_str+"|"+dd+"|"+dd1+"|"+currentUser.username.substr(0, 8)+"|"+dd+"\r\n";
										tfn=parseFloat(tfn)+1;
										console.log(fileName);
										_.forEach($scope.report.bills,function(b){
											var cDate=$filter('date')(b._created, 'dd MMM yyyy');
											var dDate=$filter('date')(d, 'dd MMM yyyy');
											if(cDate==dDate){
												recNo=b.billNumber;
												dd=$filter('date')(b._created, 'yyyyMMdd');
												dd1=$filter('date')(b._created, 'HH:mm:ss');
												fileData += "101|"+b.billNumber+"|1|"+dd+"|"+dd1+"|"+b._currentUser.username.substr(0,8)+"|||||||N|SALE\r\n";
												_.forEach(b._kots,function(kot,ki){
													var itemId="";
													_.forEach(kot.items,function(itm,it){
														if (itemId != itm._id){
															var rate =parseFloat(itm.rate);
															var taxcode = "N";
															var itemTax=getTaxAmount(itm);
															if (itm.taxes != undefined)
															{
																taxcode = 0;//itm.taxes.substr(0, 4);
															}
															var itemCode = itm.number;
															fileData += "111|" + itemCode + "|" + parseFloat(itm.quantity).toFixed(3) + "|" + parseFloat(rate).toFixed(2) + "|" + parseFloat(rate).toFixed(2) + "||" + taxcode + "|||SALE||";
															fileData += "|N|" + parseFloat(itm.subtotal).toFixed(2) + "||%|" + parseFloat(itemTax).toFixed(2) + "|\r\n";
															itemId = itm._id;
														}
													});
												});
												//Code 115 Footer Deposit Transaction
												//fileData += "115|C||E|N|"+parseFloat(b.totalAmount).toFixed(2)+"|"+parseFloat(b.totalTax).toFixed(2)+"\r\n";
												//Code 121 (Footer) All Tax,Cess, Service Charge, Discount
												var grossSale=parseFloat(b.totalAmount).toFixed(2);
												var discount = parseFloat(b.totalDiscount+b.billDiscountAmount).toFixed(2);
												var charges = parseFloat(b.charges.amount).toFixed(2);
												//fileData += "121|" + decimal.Round(billAmount, 2) + "|" + decimal.Round(decimal.Parse(bill.Discount), 2) + "|0.00|" + decimal.Round(decimal.Parse(bill.ServiceCh), 2) + "|" + decimal.Round(decimal.Parse(bill.VatAmt), 2) + "|E|N||||#";
												fileData += "121|" + parseFloat(grossSale).toFixed(2) + "|" + discount + "|0.00|" + charges + "|" + parseFloat(b.totalTax).toFixed(2) + "|E|N||||\r\n";

												//code 131(Footer Payment Medium)  if payment in N currency N records are created
												var paymentName="";
												if (b.totalCash != 0){
													paymentName = "CASH";
													fileData += "131|T|" + paymentName + "|INR|1.0000000|" + parseFloat(b.totalCash).toFixed(2) + "||||" + parseFloat(b.totalCash).toFixed(2) + "\r\n";
												}
												if (b.totalCreditCardAmount != 0){
													paymentName = "CreditCard";
													fileData += "131|T|" + paymentName + "|INR|1.0000000|" + parseFloat(b.totalCreditCardAmount).toFixed(2) + "||||" + parseFloat(b.totalCreditCardAmount).toFixed(2) + "\r\n";
												}
												if (b.totalDebitCardAmount != 0){
													paymentName = "DebitCard";
													fileData += "131|T|" + paymentName + "|INR|1.0000000|" + parseFloat(b.totalDebitCardAmount).toFixed(2) + "||||" + parseFloat(b.totalDebitCardAmount).toFixed(2) + "\r\n";
												}
												if (b.totalOtherCardAmount != 0){
													paymentName = "FullCoupon";
													fileData += "131|T|" + paymentName + "|INR|1.0000000|" + parseFloat(b.totalOtherCardAmount).toFixed(2) + "||||" + parseFloat(b.totalOtherCardAmount).toFixed(2) + "\r\n";
												}
												if(b.totalCash == 0 && b.totalCreditCardAmount == 0 && b.totalDebitCardAmount == 0 && b.totalOtherCardAmount == 0){
													paymentName = "CASH";
													fileData += "131|T|" + paymentName + "|INR|1.0000000|" + parseFloat(0).toFixed(2) + "||||" + parseFloat(0).toFixed(2) + "\r\n";
												}
											}
										});
										fileData += "1|CLOSED|"+tenantId+"|"+pos+"|"+recNo+"|"+tfn_str+"|"+dd+"|"+dd1+"|"+currentUser.username.substr(0, 8)+"|"+dd+"\r\n";
										console.log(fileData);
										//if(i==0) {
										/*	$scope.uploadResource.UploadTextFiles({
										 fileName: fileName,
										 fileData: fileData
										 }).$promise.then(function (uplo) {
										 console.log(uplo);
										 });*/
										var tempob={filename:fileName,filedata:fileData};
										dataArray.push(tempob);

										//}
									});
									if(dataArray.length>0)
										$scope.pusData(dataArray,0);
								});
							});
						}).catch(function (err) {
							growl.error(err, {ttl:3000});
							$rootScope.showLoaderOverlay = false;
							$scope.isReportServiceReady = false;
						});

					}).catch(function(err) {
						growl.error("Error Fetching Bills", {
							ttl: 3000
						});
						$rootScope.showLoaderOverlay = false;
						$scope.isReportServiceReady = false;
					});

				// 	}).catch(function (err) {
				// 		growl.error(err, {ttl:3000});
				// 		$rootScope.showLoaderOverlay = false;
				// 		$scope.isReportServiceReady = false;

				// 	});

				// }).catch(function(err) {
				// 	growl.error(err, {
				// 		ttl: 3000
				// 	});
				// 	$rootScope.showLoaderOverlay = false;
				// 	$scope.isReportServiceReady = false;
				// });
			}
		};
		$scope.pusData=function(arr,count){

			$scope.uploadResource.UploadTextFiles({
				fileName: arr[count].filename,
				fileData: arr[count].filedata,
                ftpSettings:$scope.dlfForm.ftpSettings
			}).$promise.then(function (uplo) {
					console.log(uplo);
					$timeout(function(){
						if(count<arr.length-1) {
							count=count+1;
							$scope.pusData(arr, count);
						}
						else
						{
							growl.success('done',{ttl:2000});
						}
					},2000)
				});

		}
		function getBillNos(bills){
			return bills[0].billNumber;
		};
		function getUniqueDays(bills){
			var aa=[];
			_.forEach (bills,function(d){
				var compare =$filter('date')(d._created, 'dd MMM yyyy');// d.OpenTime.ToString("dd MMM yyyy");
				var Ispresent = false;
				_.forEach (aa,function(da){
					var cDate=$filter('date')(da, 'dd MMM yyyy');
					if (compare != cDate)
					{
						Ispresent = false;
					}
					else
					{
						Ispresent = true;
					}
				});
				if (Ispresent == false)
				{
					aa.push(d._created);
				}
			});
			return aa;
		};
		function getTaxAmount(item){
			var total_tax=0;
			item.taxes.forEach(function(tax){total_tax+=tax.tax_amount;});
			return total_tax;
		}
	}]);
