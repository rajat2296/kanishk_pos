'use strict';

describe('Controller: PosToDLFCtrl', function () {

  // load the controller's module
  beforeEach(module('posistApp'));

  var PosToDLFCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PosToDLFCtrl = $controller('PosToDLFCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
