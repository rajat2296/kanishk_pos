'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('PosToDLF', {
        url: '/PosToDLF',
        templateUrl: 'app/PosToDLF/PosToDLF.html',
        controller: 'PosToDLFCtrl',
        resolve: {
			currentUser: ['$rootScope', '$state', '$stateParams', 'Auth', 'Tab', 'growl',
                        function ($rootScope, $state, $stateParams, Auth, Tab,growl) {
                            return Auth.getCurrentUser().$promise.then(function (user) {
                                $rootScope.currentUser = user;
                                return user;
                            });
                        }],
            deployments: ['$q', 'Deployment', 'localStorageService', function ($q, Deployment, localStorageService) {
            var d = $q.defer();
              Deployment.get({tenant_id: localStorageService.get('tenant_id'), isMaster: false}, function (deployments) {
                d.resolve(deployments);
              }, function (err) {
                d.reject();
              });
            return d.promise;
          }]
        }
      });
  });