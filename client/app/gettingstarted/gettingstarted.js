'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('gettingstarted', {
        url: '/gettingstarted',
        templateUrl: 'app/gettingstarted/gettingstarted.html',
        resolve: {
          currentUser: ['$state', '$stateParams', 'Auth',
            function ($state, $stateParams, Auth) {
              return Auth.getCurrentUser().$promise.then(function (user) {
                return user;
              });
            }],

          deployment: ['$rootScope', '$state', '$stateParams', 'Deployment', 'growl', 'localStorageService', '$q', function ($rootScope, $state, $stateParams, Deployment, growl, localStorageService, $q) {

            /* Resolve Master Deployment, if not found create one */
            var defer = $q.defer();

            Deployment.getMasterDeployment(
              {
                tenant_id: localStorageService.get('tenant_id'),
                isMaster: true // Don't change else will create duplicate Master deployment
              }
              , function (d) {

                if (_.has(d, '_id')) {
                  if (d.isCompleted) {
                    $state.go('admin.deployments');
                  } else {
                    defer.resolve(d);
                  }


                } else {

                  // Master Deployment not found create one
                  Deployment.save({}, {
                    name: "Master Deployment",
                    settings: $rootScope.settings,
                    isMaster: true, // Important else will create duplicate Master deployment
                    tenant_id: localStorageService.get('tenant_id')
                  }, function (d) {
                    defer.resolve(d);
                  });

                }

              });

            return defer.promise;

          }],
          //StockUnits: ['$rootScope', '$state', '$stateParams', 'stockUnit','localStorageService',
          //    function ($rootScope, $state, $stateParams, stockUnit,localStorageService) {
          //        return stockUnit.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //            console.log("Got StockUnits - " +result.length);
          //            return result;
          //
          //        });
          //    }],
          //StockRecipes: ['$rootScope', '$state', '$stateParams', 'stockRecipe','localStorageService',
          //    function ($rootScope, $state, $stateParams, stockRecipe,localStorageService) {
          //        return stockRecipe.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //            console.log("Got StockRecipes - " +result.length);
          //            return result;
          //
          //        });
          //    }],
          //Receivers: ['$rootScope', '$state', '$stateParams', 'receiver','localStorageService',
          //    function ($rootScope, $state, $stateParams, receiver,localStorageService) {
          //        return receiver.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //            console.log("Got receiver - " +result.length);
          //            return result;
          //        });
          //    }],
          //StockVendor: ['$rootScope', '$state', '$stateParams', 'vendor','localStorageService',
          //    function ($rootScope, $state, $stateParams, vendor,localStorageService) {
          //        return vendor.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //            return result;
          //        });
          //    }],
          //Store: ['$rootScope', '$state', '$stateParams', 'store','localStorageService',
          //    function ($rootScope, $state, $stateParams, store,localStorageService) {
          //        return store.get({tenant_id:localStorageService.get('tenant_id'),deployment_id:$stateParams.deploymentId}).$promise.then(function (result){
          //            return result;
          //        });
          //    }]
        },
        controller: 'DeploymentsCtrl'
      });
  });
