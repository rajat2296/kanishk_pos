'use strict';

angular.module('posistApp')
    .controller('GettingstartedCtrl', ['$scope', '$rootScope', '$location', '$cacheFactory', '$http', 'RegForm', 'Auth', 'Deployment', 'Tab', 'Supercategory', 'Category', 'Item', 'Tax', 'Offer', 'currentUser', 'Utils', 'deployment', function ($scope, $rootScope, $location, $cacheFactory, $http, RegForm, Auth, Deployment, Tab, Supercategory, Category, Item, Tax, Offer, currentUser, Utils, deployment) {

        /* Resolved Master Deployment */
        $scope.deployment = deployment;

        /* Step: Settings - Update Settings */
        $scope.updateDeploymentSettings = function () {

            Deployment.update({id: deployment._id}, $scope.deployment, function (result) {
                growl.success('Deployment Settings successfully updated!', {ttl: 3000});

            }, function (err) {
                growl.error('Error Updating Deployment Settings!', {ttl: 3000});

            });

        };
        $scope.hasAttrib = function (s) {
            return _.has(s, 'value');
        };



    }]);
