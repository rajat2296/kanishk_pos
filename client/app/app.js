'use strict';

// function setCookie(cname, cvalue) {
//     var d = new Date();
//     document.cookie = cname + "=" + cvalue + ";" + ";path=/";
// }

// function getCookie(cname) {
//     var name = cname + "=";
//     var ca = document.cookie.split(';');
//     for(var i = 0; i < ca.length; i++) {
//         var c = ca[i];
//         while (c.charAt(0) == ' ') {
//             c = c.substring(1);
//         }
//         if (c.indexOf(name) == 0) {
//             return c.substring(name.length, c.length);
//         }
//     }
//     return "";
// }

// function checkCookie() {
//     var user = getCookie("username");
//     if (user != "") {
//         alert("Welcome again " + user);
//     } else {
//         user = prompt("Please enter your name:", "");
//         if (user != "" && user != null) {
//             setCookie("username", user, 365);
//         }
//     }
// }

// (function () {
//   var guid = function () {
//     function s4() {
//       return Math.floor((1 + Math.random()) * 0x10000)
//         .toString(16)
//         .substring(1);
//     }

//     return function () {
//       return (s4() + s4()
//       + '-' +
//       s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4()
//       + '-'+
//       s4() + s4() + s4());
//     };
//   }

//   if(!window.name)
//     window.name = guid()();
//   var url = window.location.href;
//   if(getCookie(url)) {
//     if(getCookie(url) != window.name){
//       window.location = 'https://google.com';
//     }
//   } else {
//     setCookie(url, window.name);
//   }
// }())

angular.module('posistApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'btford.socket-io',
    'ui.router',
    'ui.bootstrap',
    'mgo-angular-wizard',
    'ui.select',
    'multi-select',
    'angular.filter',
    'angular-growl',
    'LZW',
    'nsPopover',
    'LocalStorageModule',
    'angular-loading-bar',
    'ngAside',
    'swipe',
    'infinite-scroll',
    'daterangepicker',
    'datatables',
    'ngAside',
    'ng-appcache',
    'multipleDatePicker',
    'ngBootbox',
    'checklist-model',
    'ngDragDrop',
    'angularUtils.directives.dirPagination',
    'angular-useragent-parser',
    'angular-virtual-keyboard',
    'mn',
    'FBAngular',
    'leaflet-directive'
    //,'ngWebworker'
    //,'selectize'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, VKI_CONFIG) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');

    /* Setup Growl Notification */


    /* Loading Interceptor */
    /*$httpProvider.responseInterceptors.push('myHttpInterceptor');
     var spinnerFunction = function (data, headersGetter) {
     $('#loading').show();
     console.log("Please Wait..." + data);
     return data;
     };
     $httpProvider.defaults.transformRequest.push(spinnerFunction);*/

    /* CORS Hack which is not working */
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    VKI_CONFIG.layout.Numerico = {
      'name': "Numerico", 'keys': [
        [["1", '1'], ["2", "2"], ["3", "3"], ["Bksp", "Bksp"]],
        [["4", "4"], ["5", "5"], ["6", '6'], ["Enter", "Enter"]],
        [["7", "7"], ["8", "8"], ["9", "9"], ['0', '0']]//,
        //[["0", "0"], ["-"], ["+"], [","]]
      ], 'lang': ["pt-BR-num"]
    };
  })
  .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function (response) {
        if (response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  })
  .factory('myHttpInterceptor', function ($q, $window) {
    return function (promise) {
      return promise.then(function (response) {
        console.log("Please Wait...Done");
        $('#loading').hide();
        return response;
      }, function (response) {
        console.log("Please Wait...Error");
        $('#loading').hide();
        return $q.reject(response);
      });
    };
  })
  .factory('mySocket', function (socketFactory) {
    return socketFactory();
  })

  .factory('nSocket', function (socketFactory, Utils, localStorageService) {
    var instanceSetting = Utils.getSettingValue('instance_server', localStorageService.get('settings'));
    var url = instanceSetting == null || instanceSetting == undefined ? 'http://jdd.posist.org' : instanceSetting;

    var io_socket = io(url);
    //  var socket = socketFactory(io_socket);
    //  socket.forward('message');
    //  return socket;

    return socketFactory({ioSocket: io_socket});
    //return socketFactory();
  })
  .factory('printer', ['$rootScope', '$compile', '$http', '$timeout', '$templateCache', 'SilentPrinter', 'Utils', 'localStorageService', '$ngBootbox', function ($rootScope, $compile, $http, $timeout, $templateCache, SilentPrinter, Utils, localStorageService, $ngBootbox) {

    var printHtml = function (html) {
      console.log(html)
      var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
      hiddenFrame.contentWindow.printAndRemove = function () {
        hiddenFrame.contentWindow.print();
        $(hiddenFrame).remove();
      };
      var htmlContent = "<!doctype html>" +
        "<html>" +
        '<body onload="printAndRemove();">' +
        html +
        '</body>' +
        "</html>";
      var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
      doc.write(htmlContent);
      doc.close();
    };

    var openNewWindow = function (html) {
      var newWindow = window.open("printTest.html");
      newWindow.addEventListener('load', function () {
        $(newWindow.document.body).html(html);
      }, false);
    };

    var print = function (templateUrl, data, transferTable) {
      console.log(templateUrl)
      var template = $templateCache.get(templateUrl);
      var printScope = $rootScope.$new()

    if(!Utils.hasSetting('print_server', localStorageService.get('settings'))){

        var newData = angular.copy(data);
        newData.disableRoundoff = Utils.hasSetting('disable_roundoff', localStorageService.get('settings'))
        newData.comboItems = [];
        if (templateUrl == '_template_Bill.html'){
          _.forEach(data.bill._kots,function(kot){
            _.forEach(kot.items,function(item){
              _.forEach(item.addOns,function(addOn){
                newData.items.push({name:addOn.name,quantity:addOn.quantity,subtotal:addOn.subtotal});
              })
            })
          })
        }
        if(templateUrl == '_template_KOT.html'){
            newData.bill._kots[data.index].totalQuantityWithoutAddOn=0;
            _.forEach(data.bill._kots[data.index].items,function(item){
              newData.bill._kots[data.index].totalQuantityWithoutAddOn = newData.bill._kots[data.index].totalQuantityWithoutAddOn + item.quantity;
              if(item.mapComboItems.length>0 && !data.isVoid){
                newData.bill._kots[data.index].totalQuantityWithoutAddOn=newData.bill._kots[data.index].totalQuantityWithoutAddOn-item.quantity;
                _.forEach(item.mapComboItems,function(comboItem){
                  newData.bill._kots[data.index].totalQuantityWithoutAddOn=newData.bill._kots[data.index].totalQuantityWithoutAddOn+item.quantity;
                  newData.comboItems.push({name:item.name,comboName:comboItem.name,comment:item.comment,quantity:item.quantity});
                })
              }
            })
        }
        console.log(newData)
        angular.extend(printScope, newData);
      }
      else{
        angular.extend(printScope, data);
      }
      //console.log(printScope);
      if (Utils.hasSetting('print_server', localStorageService.get('settings'))) {
        if (templateUrl == '_template_KOT.html')
          SilentPrinter.print(data, true, false, transferTable);
        if (templateUrl == '_template_Bill.html'){
          //console.log(newData);
          SilentPrinter.print(data, false, false, transferTable);
        }
        if (templateUrl == '_template_KOT_Deleted.html')
          SilentPrinter.print(data, true, true, transferTable);
        if (templateUrl == 'dynamicTemplate')
          SilentPrinter.print(data, false, false, transferTable);
        if (templateUrl == 'dynamicTemplateMasterKot')
          SilentPrinter.print(data, true, false, transferTable);
        return false;
      }
      var element = $compile($('<div>' + template + '</div>'))(printScope);
      var waitForRenderAndPrint = function () {
        if (printScope.$$phase || $http.pendingRequests.length) {
          $timeout(waitForRenderAndPrint);
        } else {
          // Replace printHtml with openNewWindow for debugging
          // console.log(element.html());
          printHtml(element.html());
          /*openNewWindow(element.html());*/
          printScope.$destroy();
        }
      }
      waitForRenderAndPrint();
    };

    return {
      print: print
    }
  }])
  .factory('SilentPrinter', ['$rootScope', '$q', '$http', 'Deployment', 'Utils', 'localStorageService', 'growl', '$ngBootbox', function ($rootScope, $q, $http, Deployment, Utils, localStorageService, growl, $ngBootbox) {

    var getPrinters = function (currentUser) {
      /*console.log(currentUser);*/
      var _settings = [];
      _settings = (localStorageService.get('settings'));
      var printServer = 'localhost';
      var printerSetting = Utils.getSettingValue('print_server', _settings);
      if (!(printerSetting == null || printerSetting == undefined || printerSetting == '')) {
        printServer = printerSetting;
      }
      // alert(printerSetting);
      var url = "http://" + printServer + ":65505/service1.svc/getPrinters?callback=JSON_CALLBACK";
      var d = $q.defer();

      /* Deployment.get({id: currentUser.deployment_id}, function (d) {
       */
      /*console.log(d);*/
      /*
       }, function (err) {
       console.log(err);
       });*/

      $http.jsonp(url).
      success(function (data, status, headers, config) {
        d.resolve(data);
      }).
      error(function (data, status, headers, config) {
        d.reject("error fetching");
      });
      return d.promise;
    };

    var print = function (printdata, iskot, isDeletedKot, transTable) {
      //console.log(printdata);

      var settings = (localStorageService.get('settings'));
      var instance = localStorageService.get('instanceId');
      var printServer = 'localhost';
      var printerSetting = Utils.getSettingValue('print_server', settings);
      if (!(printerSetting == null || printerSetting == undefined || printerSetting == '')) {
        printServer = printerSetting;
      }
      var url = 'http://' + printServer + ':65505/service1.svc/prnfin?callback=JSON_CALLBACK';
      var urltrns = 'http://' + printServer + ':65505/service1.svc/PrintTransfer?callback=JSON_CALLBACK';
      var urlbilltrns = 'http://' + printServer + ':65505/service1.svc/DynamicPrint?callback=JSON_CALLBACK';
      // var url = "http://"+ printServer +":65505/service1.svc/getPrinters?callback=JSON_CALLBACK";
      if (!(transTable == null || transTable == undefined)) {
        if(transTable=="dynamicPrint") {
          var printername=null;
          if ($rootScope.isInstance) {
            var instancePrinter = localStorageService.get('InstancePrinter');
            if (_.has(instance, 'id')) {
              if (!(instancePrinter == null || instancePrinter == undefined)) {
                var _fip = _.filter(instancePrinter, {id: instance.id});
                if (_fip.length > 0) {
                  if(!(_fip[0].printer==null||_fip[0].printer==undefined))
                    printerName =  (_fip[0].printer);
                  //alert(printerName);
                }
              }
            }
          }

          if(localStorageService.get('masterStation') && iskot)
          {
            var printers=localStorageService.get('masterStation');
            if(printers==null || printers=="" || printers==undefined ){return false;}
            if(!(printers.constructor === Array)){ return false;}
            /*To Do*/
            ////////////////For Loop for multi master print kot////

            _.forEach(printers,function(mPrinter){
              var  printerName=mPrinter.stationPrinter;
                var durl = 'http://' + printServer + ':65505/service1.svc/PrintDynamicData?callback=JSON_CALLBACK';
                $http({
                  method: 'POST',
                  url: durl,
                  data:{data:  printdata,printers:printerName ,copies:1}
                }).success(function (data) {
                  console.log(data);
                })
              })
          /////////////////////////
          return false;
          }
          var durl = 'http://' + printServer + ':65505/service1.svc/PrintDynamicData?callback=JSON_CALLBACK';
          /* $http.jsonp(durl, {params: printdata}).success(function (data) {
           //   console.log(data);
           });*/
          $http({
            method: 'POST',
            url: durl,
            data:{data:  printdata,printers:printerName ,copies:1}
          }).success(function (data) {
            console.log(data);
          })



          return false;
        }
        var transferToPrint;
        if (transTable.type == 'table' || transTable.type == 'bill') {
          if (Utils.hasSetting('station_wise_print', settings)) {
            transferToPrint = {
              transferData: JSON.stringify(transTable),
              printers: JSON.stringify(getPrintersFromBill(printdata))
            };

          } else {
            var printers = [];
            transferToPrint = {
              transferData: JSON.stringify(transTable),
              printers: JSON.stringify(printers)
            };
          }
          var isEpos = Utils.hasSetting('enable_epos_print', settings);
          if (!isEpos) {
            $http.jsonp(urltrns, {params: transferToPrint}).success(function (data) {
              //   console.log(data);
            });
          }
          return false;
        } else {
          if (transTable.type != 'item') {
            if (Utils.hasSetting('station_wise_print', settings)) {
              transferToPrint = {
                transferData: JSON.stringify(transTable.text),
                printers: JSON.stringify(getPrintersFromBill(printdata))
              };

            } else {
              var printers = [];
              transferToPrint = {
                transferData: JSON.stringify(transTable.text),
                printers: JSON.stringify(printers)
              };
            }
            var isEpos = Utils.hasSetting('enable_epos_print', settings);
            if (!isEpos) {
              $http.jsonp(urlbilltrns, {params: transferToPrint}).success(function (data) {
                //   console.log(data);
              });
            }

            return false;
          }
        }
      }
      var prefix = '';

      /*if(_.has(instance,'preFix')){
       prefix= (billPreFix==null? instance.preFix:billPreFix);
       }*/

      var bill;
      if (iskot)
        bill = printdata.bill;
      else
        bill = printdata;
      var data = {};
      var billItems = [];
      var billTaxes = [];

      // if(_.has(bill,'prefix')) {
      if (iskot) {
        if (!((bill.prefix == null) || (bill.prefix == undefined))) {
          prefix = bill.prefix;
        }
      } else {
        // alert(bill.bill.prefix);
        if (!((bill.bill.prefix == null) || (bill.bill.prefix == undefined))) {
          prefix = bill.bill.prefix;
        }
      }
      //}
      if (iskot) {
        var index = printdata.index;
        var comboItems = [];
        for (var i = 0; i < bill._kots[index].items.length; i++) {
          // console.log(bill._kots[index].items);
          if (_.has(bill._kots[index].items[i], 'mapComboItems')) {
            if (bill._kots[index].items[i].mapComboItems.length > 0) {
              _.forEach(bill._kots[index].items[i].mapComboItems, function (citem) {
                var titem = angular.copy(citem);
                titem.name = bill._kots[index].items[i].name + "(" + citem.name + ")";
                titem.quantity = bill._kots[index].items[i].quantity;
                titem.comment = bill._kots[index].items[i].comment;
                if (_.has(bill._kots[index].items[i], 'addOns')) {
                  var addonstring = '';
                  _.forEach(bill._kots[index].items[i].addOns, function (addon) {
                    addonstring += addon.name + ':' + addon.quantity + ','
                  })
                  if (addonstring != '') {
                    titem.name += '(' + addonstring + ')';
                  }
                }
                comboItems.push(titem);
              })
            } else {
              comboItems.push(bill._kots[index].items[i]);
            }
          } else {
            comboItems.push(bill._kots[index].items[i]);
          }
        }
        /*Add On Integration*/
        _.forEach(comboItems, function (ci) {
          if (_.has(ci, 'addOns')) {
            var addonstring = '';
            _.forEach(ci.addOns, function (addon) {
              addonstring += addon.name + ':' + addon.quantity + ','
            })
            if (addonstring != '') {
              ci.name += '(' + addonstring + ')';
            }
          }
        })
        //console.log(comboItems);
        bill._kots[index].items = comboItems;

        var customer = angular.copy(bill._customer);
        var dIndex;
        if (_.has(printdata, 'isReprint')) {
          if (printdata.isReprint) {
            data.header = "Duplicate KOT \r\n==================\r\n";
          }
        }
        if (isDeletedKot) {
          dIndex = printdata.dIndex;
          data.header = "VOID KOT ITEM\r\n==================\r\n";
        }
        if (printdata.isVoid) {
          data.header = "VOID KOT \r\n==================\r\n";
        }
        // console.log(printdata);

        /*Transfer Kot integration*/
        if (transTable != null) {
          if (_.has(transTable, 'kotTText')) {
            data.header = (transTable.kotTText + ((data.header == null || data.header == undefined) ? "" : data.header) );
          }
        }
        var waiter = '--None--';
        if (bill.tabType == 'table') {
          /* var fname = _.has(bill._waiter, 'firstname') ? bill._waiter.firstname : '';
           var lname = _.has(bill._waiter, 'lastname') ? bill._waiter.lastname : '';*/
          var fname = _.has(bill._kots[index].waiter, 'firstname') ? bill._kots[index].waiter.firstname : '';
          var lname = _.has(bill._kots[index].waiter, 'lastname') ? bill._kots[index].waiter.lastname : '';
          if (fname.length > 0) {
            waiter = fname + ' ' + lname
          }
          ;
        }
        else {
          var fname = _.has(bill._delivery, 'firstname') ? bill._delivery.firstname : '';
          var lname = _.has(bill._delivery, 'lastname') ? bill._delivery.lastname : '';
          if (fname.length > 0) {
            waiter = fname + ' ' + lname
          }
          ;
        }
        //data.header = "";
        var billNumber = Utils.hasSetting('reset_serial_daily', settings) ? bill.daySerialNumber : bill.serialNumber;
        data.tab = bill.tab;
        data.tableId = bill._tableId;
        data.kotNumber = bill._kots[index].kotNumber;

        data.billNumber = prefix + billNumber + (parseInt(bill.splitNumber) ? '-' + bill.splitNumber : '');
        data.billDate = bill._kots[index].created;
        data.footer = "";
        /*   data.customerName = "";
         data.customerAddress = "";
         data.addressline1 = "";
         data.city = "";
         data.state = "";
         data.pin = "";
         data.phoneNumber = "";
         data.mobileNumber = "";*/
        data.customerName = _.has(customer, 'firstname') ? customer.firstname : "";
        data.customerAddress = _.has(customer, 'address1') ? customer.address1 : "";
        data.addressline1 = _.has(customer, 'address2') ? customer.address2 : "";
        data.city = _.has(customer, 'city') ? customer.city : "";
        data.state = _.has(customer, 'state') ? customer.state : "";
        data.pin = _.has(customer, 'postCode') ? customer.postCode : "";
        data.phoneNumber = _.has(customer, 'phone') ? customer.phone : "";
        data.mobileNumber = _.has(customer, 'mobile') ? customer.mobile : "";
        data.waiterName = waiter; //_.has(bill._waiter,'firstname')? bill._waiter.firstname:_.has(bill._delivery,'firstname')?bill._delivery.firstname:'--None--';
        data.discountAmount = 0;
        data.footer = "";
        data.advanceAmount = 0;
        data.numItemSinglePage = null;
        data.printWeightUnit = false;
        //  "jbc": null,
        data.paymentStatus = null;
        var isCategoryWise = false;
        var isStationWise = false;
        if (Utils.hasSetting('category_print_kot', settings)) {
          isCategoryWise = true;
        }
        if (Utils.hasSetting('station_wise_print', settings)) {
          isStationWise = true;
        }
        if (isDeletedKot) {
          var indexP = 0;
          if (isStationWise) {
            if (_.has(bill._kots[index].deleteHistory[dIndex], 'category')) {
              if (_.has(bill._kots[index].deleteHistory[dIndex].category, 'categoryStation')) {
                // if (_.has(bill._kots[index].deleteHistory[dIndex].category.superCategory, 'superCategoryStation')) {
                var printername = bill._kots[index].deleteHistory[dIndex].category.categoryStation.stationPrinter
                if(!(instance==null|| instance==undefined)) {
                  if(_.has(instance,'id')) {
                    var printerOverride = getPrinterStationInstance(bill._kots[index].deleteHistory[dIndex].category.categoryStation._id, instance.id);
                    if (printerOverride != null) {
                      printername = printerOverride;
                    }
                  }
                }
                // printername += bill.header;
                var flag = false;
                for (var i = 0; i < billTaxes.length; i++) {
                  if (printername === billTaxes[i].N) {
                    flag = true;
                    indexP = i;
                    break;
                  }
                }
                if (!flag) {
                  var billtax = {N: printername, A: billTaxes.length};
                  billTaxes.push(angular.copy(billtax));
                  indexP = billtax.A;
                }
                // }
              }
            }
          }
          // console.log(printdata);
          var item = {};
          item.N = angular.copy(bill._kots[index].deleteHistory[dIndex].name);
          if (_.has(bill._kots[index].deleteHistory[dIndex], 'addOns')) {
            var addonstring = '';
            _.forEach(bill._kots[index].deleteHistory[dIndex].addOns, function (addon) {
              addonstring += addon.name + ':' + addon.quantity + ','
            })
            if (addonstring != '') {
              item.N += '(' + addonstring + ')';
            }
          }
          item.Q = ( 0 - bill._kots[index].deleteHistory[dIndex].quantity);
          //if(isStationWise)
          // item.R = index;
          //item.R = 0;
          item.R = 0;//indexP;
          item.W = 0;
          item.U = "";
          if (isCategoryWise) {
            item.C = bill._kots[index].deleteHistory[dIndex].category.categoryName;
          }
          billItems.push(angular.copy(item));

        }
        else {
          //var kotitems=bill._kots[index].items;
          for (var i = 0; i < bill._kots[index].items.length; i++) {
            //   for(var i=0;i<comboItems.length;i++){
            var indexP = 0;
            if (isStationWise) {
              if (_.has(bill._kots[index].items[i], 'category')) {
                if (_.has(bill._kots[index].items[i].category, 'categoryStation')) {
                  // if (_.has(bill._kots[index].items[i].category.superCategory, 'superCategoryStation')) {
                  var printername = bill._kots[index].items[i].category.categoryStation.stationPrinter;
                  if(!(instance == null|| instance == undefined)) {
                    if (_.has(instance, 'id')) {
                      var printerOverride = getPrinterStationInstance(bill._kots[index].items[i].category.categoryStation._id, instance.id);
                      if (printerOverride != null) {
                        printername = printerOverride;
                      }
                    }
                  }
                  //console.log(printername);
                  var flag = false;
                  for (var j = 0; j < billTaxes.length; j++) {
                    if (printername === billTaxes[j].N) {
                      flag = true;

                      indexP = j;
                      break;
                    }
                  }
                  if (!flag) {
                    var billtax = {N: printername, A: billTaxes.length};
                    billTaxes.push(angular.copy(billtax));
                    indexP = billtax.A;
                  }
                  //}
                }
              }
            }
            var item = {};
            item.N = bill._kots[index].items[i].comment == null || bill._kots[index].items[i].comment == '' ? bill._kots[index].items[i].name : bill._kots[index].items[i].name + "(" + bill._kots[index].items[i].comment + ")";
            /*Void Kot Print*/
            if (printdata.isVoid) {
              item.Q = (0 - bill._kots[index].items[i].quantity);
            } else {
              item.Q = bill._kots[index].items[i].quantity;
            }
            item.R = indexP;
            /*Incorporate Category wise print logic here*/
            if (isCategoryWise) {
              item.C = bill._kots[index].items[i].category.categoryName;
            }
            item.W = 0;
            item.U = "";
            billItems.push(angular.copy(item));
          }
        }
        if (billTaxes.length == 0) {
          var tax = {N: null, A: 0};
          billTaxes.push(angular.copy(tax));
        }
      } //  Kot print finished here
      else {
        // console.log(bill);
        //var index=printdata.index;
        data.rePrintTime = null;
        var customer = angular.copy(bill.bill._customer);
        var waiter = '--None--';
        if (bill.bill.tabType == 'table') {
          var fname = _.has(bill.bill._waiter, 'firstname') ? bill.bill._waiter.firstname : '';
          var lname = _.has(bill.bill._waiter, 'lastname') ? bill.bill._waiter.lastname : '';
          if (fname.length > 0) {
            waiter = fname + ' ' + lname
          }
          ;
        }
        else {
          var fname = _.has(bill.bill._delivery, 'firstname') ? bill.bill._delivery.firstname : '';
          var lname = _.has(bill.bill._delivery, 'lastname') ? bill.bill._delivery.lastname : '';
          if (fname.length > 0) {
            waiter = fname + ' ' + lname
          }
          ;
        }
        data.header = bill.header;
        if (_.has(bill.bill, 'rePrint')) {
          if (bill.bill.rePrint.length > 1) {
            if (!Utils.hasSetting('settle_bill_on_print', settings,bill.bill.tabType)) {
              data.header = "Duplicate Print \r\n===================\r\n" + data.header;
              data.rePrintTime = Utils.getDateFormatted(new Date());
            }
          }
        }
        //console.log(bill.bill.rePrint);
        data.tab = bill.bill.tab;
        data.tableId = bill.bill._tableId;
        //data.kotNumber=bill._kots[index].kotNumber;
        var billNumber = Utils.hasSetting('reset_serial_daily', settings) ? bill.bill.daySerialNumber : bill.bill.serialNumber;
        data.billNumber = prefix + billNumber + (parseInt(bill.bill.splitNumber) ? '-' + bill.bill.splitNumber : '');
        data.billDate = bill.bill.billPrintTime; //bill._kots[index].created ;
        console.log(data.billDate);
        data.footer = "";
        data.customerName = _.has(customer, 'firstname') ? customer.firstname : "";
        data.customerAddress = _.has(customer, 'address1') ? customer.address1 : "";
        data.addressline1 = _.has(customer, 'address2') ? customer.address2 : "";
        data.city = _.has(customer, 'city') ? customer.city : "";
        data.state = _.has(customer, 'state') ? customer.state : "";
        data.pin = _.has(customer, 'postCode') ? customer.postCode : "";
        data.phoneNumber = _.has(customer, 'phone') ? customer.phone : "";
        data.mobileNumber = _.has(customer, 'mobile') ? customer.mobile : "";
        // data.waiterName = "--None--";
        //var waiter=
        data.waiterName = waiter; //_.has(bill.bill._waiter,'firstname')? bill.bill._waiter.firstname+" "+(bill.bill._waiter.lastname==undefined?'':bill.bill._waiter.lastname):_.has(bill.bill._delivery,'firstname')?bill.bill._delivery.firstname+" "+(bill.bill._waiter.lastname==undefined?'':bill.bill._waiter.lastname):'--None--';
        data.discountAmount = bill.totalDiscount;
        data.footer = bill.footer;
        data.footer += '\r\n-------------------\r\nPowered by - POSist';
        data.advanceAmount = null;
        data.numItemSinglePage = null;
        data.printWeightUnit = false;
        //  "jbc": null,
        data.paymentStatus = null;
        var isAdvance = false;
        if (_.has(bill.bill, 'advance')) {
          if (_.has(bill.bill.advance, 'amount')) {
            if (bill.bill.advance.amount > 0) {
              data.header = 'Advance Booking Slip\r\n=======================\r\n' + data.header;
              data.advanceAmount = bill.bill.advance.amount;
              isAdvance = true;
            }
          }
        }


        for (var i = 0; i < bill.items.length; i++) {
          var item = {};
          item.N = bill.items[i].name;
          item.Q = bill.items[i].quantity;
          item.R = bill.items[i].subtotal / bill.items[i].quantity;
          item.W = 0;
          item.U = "";
          billItems.push(angular.copy(item));
        }
        /* Add On Billing Print*/
        var kotsn = '';
        _.forEach(bill.bill._kots, function (kot) {
          /*if(kot.!isVoid){

           }*/
          kotsn += kot.kotNumber + ',';
          _.forEach(kot.items, function (item) {
            if (_.has(item, 'addOns')) {
              _.forEach(item.addOns, function (addon) {
                var itemAddon = {};
                itemAddon.N = addon.name;
                itemAddon.Q = addon.quantity;
                itemAddon.R = addon.subtotal / addon.quantity;
                itemAddon.W = 0;
                itemAddon.U = "";
                billItems.push(angular.copy(itemAddon));
              })
            }
          })
        })
        data.kots = kotsn;
        /*Add on End*/
        for (var j = 0; j < bill.bill._kotsTaxes.length; j++) {
          var tax = {N: bill.bill._kotsTaxes[j].name, A: bill.bill._kotsTaxes[j].tax_amount};
          billTaxes.push(angular.copy(tax));
        }
        if (bill.bill._kotsTaxes.length == 0 && (isAdvance)) {
          var tax = {N: 'Tax', A: 0};
          billTaxes.push(angular.copy(tax));
        }
      }

      // Delivery Charge Integration in print bill
      if(_.has(bill.bill,'charges')){
        if(_.has(bill.bill.charges,'detail')){
          _.forEach(bill.bill.charges.detail,function(charge){
            billTaxes.push({N:charge.name , A:charge.amount});
          })
        }
      }
      //billTaxes.push({N:'Delivery charge',A:100});

      var billObject;
      if (iskot) {
        billObject = {
          "Hd": data.header,
          "Tp": data.tab,
          "TN": data.tableId,
          "KN": data.kotNumber,
          "BN": data.billNumber,
          "BD": data.billDate,
          "Name": data.customerName,
          "Add": data.customerAddress,
          "St": data.addressline1,
          "Cy": data.city,
          "Sta": data.state,
          "Pin": data.pin,
          "PNm": data.phoneNumber,
          "MNm": data.mobileNumber,
          "Waiter": data.waiterName,
          "DAmt": data.discountAmount,
          "Ft": data.footer,
          "WU": data.printWeightUnit
        }
      } else {
        billObject = {
          "Hd": data.header,
          "Tp": data.tab,
          "TN": data.tableId,
          "KN": data.kotNumber,
          "BN": data.billNumber,
          "BD": data.billDate,
          "Name": data.customerName,
          "Add": data.customerAddress,
          "St": data.addressline1,
          "Cy": data.city,
          "Sta": data.state,
          "Pin": data.pin,
          "PNm": data.phoneNumber,
          "MNm": data.mobileNumber,
          "Waiter": data.waiterName,
          "DAmt": data.discountAmount,
          "Ft": data.footer,
          "AdvAmt": data.advanceAmount,
          "Pic": data.numItemSinglePage,
          "WU": data.printWeightUnit,
          "jbc": null,
          "PSt": data.paymentStatus,
          "RPT": data.rePrintTime,
          "KTS": data.kots

        }
      }

      /* TODO: Construct URL with GET params */
      // console.log(Utils.getSettingValue('bill_print_copies',settings));
      var kotCopies = parseInt(Utils.getSettingValue('kot_print_copies', settings)) ? parseInt(Utils.getSettingValue('kot_print_copies', settings)) : 1;
      var billsCopies = parseInt(Utils.getSettingValue('bill_print_copies', settings)) ? parseInt(Utils.getSettingValue('bill_print_copies', settings)) : 0;
      var printerName = null;
      if (!iskot) {
        if ($rootScope.isInstance) {
          var instancePrinter = localStorageService.get('InstancePrinter');
          if (_.has(instance, 'id')) {
            if (!(instancePrinter == null || instancePrinter == undefined)) {
              var _fip = _.filter(instancePrinter, {id: instance.id});
              if (_fip.length > 0) {
                var instancename = (_.has(instance, 'name')) ? instance.name : 't1';
                if(!(_fip[0].printer==null||_fip[0].printer==undefined))
                printerName = instancename + ":" + (_fip[0].printer);
                //alert(printerName);
              }
            }
          }
        }
      }
      //console.log(printerName);
      var dataToPrint = {
        billdata: JSON.stringify(billObject),
        billitemsdata: JSON.stringify(billItems),
        billtaxesdata: JSON.stringify(billTaxes),
        iskot: iskot,
        printerName: printerName,
        copies: iskot ? kotCopies : billsCopies,
        printertype: null
      };
      //  console.log(JSON.stringify(dataToPrint));
      var isEpos = Utils.hasSetting('enable_epos_print', settings);
      if (isEpos == false || isEpos == null) {
        // alert(billsCopies);
        if ((!iskot) && (billsCopies == 0)) {
          $ngBootbox.confirm('Are you sure to print bill ?').then(function () {
            $http.jsonp(url, {params: dataToPrint}).success(function (data) {
              //   console.log(data);
            });
            growl.success('Printing bill in progress.', {ttl: 2000});
          })
        } else {
          $http.jsonp(url, {params: dataToPrint}).success(function (data) {
            //   console.log(data);
          });
        }
      } else {
        /* $.getJSON( url,dataToPrint )
         .done(function( json ) {
         console.log( json );
         })
         .fail(function( jqxhr, textStatus, error ) {
         var err = textStatus + ", " + error;
         console.log( "Request Failed: " + err );
         });
         */
        var builder = new epson.ePOSBuilder();
        builder.addTextLang('en').addTextSmooth(true);
        builder.addTextAlign(builder.ALIGN_CENTER);
        if (!iskot) {

          builder.addText(data.header + '\n');
          builder.addTextAlign(builder.ALIGN_LEFT);
          builder.addText('Type:' + (data.tableId != null ? 'Table' : data.tab ) + '\n');

          if (data.tableId != null) {
            builder.addText('TableNo:' + data.tableId + '\n');
          }
          builder.addText('Bill Number:' + data.billNumber + '\n');
          if (!(data.waiterName == null || data.waiterName == '--None--')) {
            builder.addText((data.tableId != null ? 'Steward:' : 'Delivery Boy:') + data.waiterName + '\n');
          }
          builder.addText('Date:' + Utils.getDateFormatted(data.billDate) + '\n');
          if (data.customerName != "" || data.customerAddress != "" || data.addressline1 != "" || data.mobileNumber != "") {
            builder.addText('Customer Detail:\n');
            builder.addText('----------------\n');
            builder.addText('Name:' + data.customerName + '\n');
            builder.addText('Address:' + data.customerAddress + '\n');
            builder.addText(data.addressline1 + '  ' + data.city + '\n');
            builder.addText(data.state + (data.pin == null || data.pin == "" ? '' : ':' + data.pin) + '\n');
            builder.addText('Phone:' + data.phoneNumber + '\n');
            builder.addText('Mobile:' + data.mobileNumber + '\n');
          }
          builder.addText('------------------------------------------' + '\n');
          builder.addText('Item                            Qty Amount' + '\n');
          builder.addText('------------------------------------------' + '\n');

          var qty = 0, amt = 0;
          for (var i = billItems.length - 1; i >= 0; i--) {
            var amnt = billItems[i].R * billItems[i].Q;
            amt += amnt;
            qty += billItems[i].Q;
            //if (IsWU()) {
            //    builder.addText(item.ItemName).addTextPosition(314).addText(toPrice10(billitemsp[i].Quantity)).addTextPosition(404).addText(toPrice10(amnt) + '\n');
            //} else {
            if (billItems[i].N.length > 35) {
              var line = 0;
              for (var line = 0; (line * 35) <= billItems[i].N.length; line++) {
                builder.addText(billItems[i].N.substr((line * 35), 35) + '\n');
              }
              //builder.addText('-----').addTextPosition(464).addText(billItems[i].Q + '\n');
              builder.addText('').addTextPosition(314).addText(toPrice10(billItems[i].Q)).addTextPosition(404).addText(toPrice10(amnt) + '\n');
            } else

              builder.addText(billItems[i].N).addTextPosition(314).addText(toPrice10(billItems[i].Q)).addTextPosition(404).addText(toPrice10(amnt) + '\n');
            //}
          }

          builder.addText('------------------------------------------' + '\n');
          builder.addText('Total Qty:').addTextPosition(304).addText(toPrice10(qty)).addTextPosition(404).addText('' + '\n');
          builder.addText('Total Amount:').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(amt.toFixed(2)) + '\n');

          if (data.discountAmount > 0) {
            builder.addText('Discount:').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(data.discountAmount.toFixed(2)) + '\n');
            amt -= data.discountAmount;
          }


          for (var jc = 0; jc < billTaxes.length; jc++) {
            amt += parseFloat(billTaxes[jc].A);
            builder.addText(billTaxes[jc].N).addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(billTaxes[jc].A) + '\n');
          }

          if (billTaxes.length > 0 || data.discountAmount > 0) {
            builder.addText('------------------------------------------' + '\n');
            builder.addText('Rounding:').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(-(amt - Utils.roundNumber(amt, 0)).toFixed(2)) + '\n');
            builder.addText('Net Amount:').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(Utils.roundNumber(amt, 0).toFixed(2)) + '\n');
            if (Utils.hasSetting('enable_advance_booking', settings)) {
              if (_.has(bill.bill, 'advance')) {
                if (_.has(bill.bill.advance, 'amount')) {
                  if (!(data.advanceAmount == null) || (data.advanceAmount == 0) || (data.advanceAmount <= 0)) {
                    builder.addText('Advance Amount').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(Utils.roundNumber(data.advanceAmount, 2)) + '\n');
                    builder.addText('------------------------------------------' + '\n');
                    builder.addText('Payable Amount').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10((Utils.roundNumber(amt, 0) - Utils.roundNumber(data.advanceAmount, 2))) + '\n');
                  }
                }
              }
            }
          } else {
            builder.addText('------------------------------------------' + '\n');
            builder.addText('Rounding:').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(-(amt - Utils.roundNumber(amt, 0)).toFixed(2)) + '\n');
            builder.addText('Net Amount:').addTextPosition(304).addText('').addTextPosition(404).addText(toPrice10(Utils.roundNumber(amt, 0).toFixed(2)) + '\n');

          }
          builder.addText('------------------------------------------' + '\n');
          builder.addText((data.footer) + '\n');
        } else {
          //////////////////////////////////////Epson TM-i////Kot
          //   builder.addText(data.header + '\n');
          builder.addTextAlign(builder.ALIGN_LEFT);
          builder.addText('Type:' + (data.tableId != null ? 'Table' : data.tab ) + '\n');
          if (data.tableId != null) {
            builder.addText('TableNo:' + data.tableId + '\n');
          }
          builder.addText('Bill Number:' + data.billNumber + '\n');
          if (!(data.waiterName == null || data.waiterName == '--None--')) {
            builder.addText((data.tableId != null ? 'Steward:' : 'Delivery Boy:') + data.waiterName + '\n');
          }
          builder.addText('Kot Number:' + data.kotNumber + '\n');
          builder.addText('Date:' + Utils.getDateFormatted(data.billDate) + '\n');
          if (data.customerName != "" || data.customerAddress != "" || data.addressline1 != "" || data.mobileNumber != "") {
            builder.addText('Customer Detail:\n');
            builder.addText('----------------\n');
            builder.addText('Name:' + data.customerName + '\n');
            builder.addText('Address:' + data.customerAddress + '\n');
            builder.addText(data.addressline1 + '  ' + data.city + '\n');
            builder.addText(data.state + (data.pin == null || data.pin == "" ? '' : ':' + data.pin) + '\n');
            builder.addText('Phone:' + data.phoneNumber + '\n');
            builder.addText('Mobile:' + data.mobileNumber + '\n');
          }
          builder.addText('------------------------------------------' + '\n');
          builder.addText('Item                              Quantity' + '\n');
          builder.addText('------------------------------------------' + '\n');
          var qty = 0, amt = 0;
          for (var i = billItems.length - 1; i >= 0; i--) {
            qty += billItems[i].Q;
            if (billItems[i].N.length > 35) {
              var line = 0;
              for (var line = 0; (line * 35) <= billItems[i].N.length; line++) {
                builder.addText(billItems[i].N.substr((line * 35), 35) + '\n');
              }
              builder.addText('-----').addTextPosition(464).addText(billItems[i].Q + '\n');
            } else
              builder.addText(billItems[i].N).addTextPosition(464).addText(billItems[i].Q + '\n');
          }

          builder.addText('------------------------------------------' + '\n');
          builder.addText('Total:').addTextPosition(380).addText(toPrice10(qty) + '\n');
        }
///////////////////////////////////////////////EPSON TM-i///////////////////////////////
        builder.addFeedLine(3);
        builder.addCut(builder.CUT_FEED);
        var epos = new epson.ePOSPrint();
        //  var serverhost = getPropertyValue("PServer");
        var ip = Utils.getSettingValue('print_server', settings);
        if (ip == '' || ip == null) {
          return false;
        }
        var address = 'http://' + ip + '/cgi-bin/epos/service.cgi?devid=local_printer' + '&timeout=10000';
        epos.onreceive = function (res) {
          growl.success('Printed successfully', {ttl: 2000})
        }
        epos.onerror = function (err) {
          growl.error('Some problem with printer..please retry !!', {ttl: 3000});
        }
        // console.log(builder.toString());
        //epos.send(address, builder.toString());
        if (!iskot) {
          for (var p = 0; p < billsCopies; p++) {
            epos.send(address, builder.toString());
          }
        } else {
          for (var p = 0; p < kotCopies; p++) {
            epos.send(address, builder.toString());
          }
        }
      }

      /*Email and SMS bill integration*/
      //console.log( printdata);

      //if(data.customerEmail != ""){
      if (_.has(printdata.bill, '_customer')) {
        var sendEmail = Utils.hasSetting('send_email_customer', settings);
        var sendSMS = Utils.hasSetting('send_sms_customer', settings);
        var smsoutlet = Utils.getSettingValue('bill_sms_outlet', settings);
        //alert(smsoutlet);
        //console.log(settings)
        //console.log("hello", sendEmail)
        if (!iskot) {
          //console.log("hello")
          var sendObj = {billObject: billObject, items: billItems, taxes: billTaxes}
          if (sendEmail) {
            if (_.has(printdata.bill._customer, 'email')) {
              if (printdata.bill._customer.email != "") {
                sendObj.email = printdata.bill._customer.email;
                sendObj.tenant_id=localStorageService.get('tenant_id');
                $http.post('/api/emails/sendBillToCustomer', sendObj).success(function (data) {
                  // console.log(data);
                });
              }
            }
          }
          if (sendSMS) {
            var subTotal = 0;
            var discount = (billObject.DAmt == '') ? 0 : billObject.DAmt;
            var tax = 0;
            for (var i = 0; i < billItems.length; i++) {
              subTotal += (billItems[i].R * billItems[i].Q);
            }
            var grandTotal = 0.00;
            grandTotal = parseFloat(subTotal);
            grandTotal -= parseFloat(discount);
            for (var i = 0; i < billTaxes.length; i++) {
              tax += parseFloat(billTaxes[i].A);
              grandTotal += parseFloat(billTaxes[i].A);
            }
            smsoutlet = ((smsoutlet == undefined || smsoutlet == null) ? 'us' : smsoutlet);
            // var template='Dear '+ billObject.Name +',\r\n Thanks for choosing us. Your bill number is '+ billObject.BN +'.\r\n Bill Amount - '+ subTotal +'\r\nDiscount - '+ discount +'\r\nTax - '+ Utils.roundNumber( tax,2) +'\r\nAmount to pay : '+ Utils.roundNumber( grandTotal,0).toString();
            //  var messageText = "Dear " + billObject.Name + ",\r\n Thanks for choosing us. Your bill amount is Rs " + subTotal + "  Discount - " + discount + "  Tax - " + tax + "  Amount to pay : " + grandTotal;
            var template = 'Dear ' + billObject.Name + ',\r\nThank you for choosing ' + smsoutlet + '. Your bill number is ' + billObject.BN + '.\r\nBill Amount - ' + subTotal + '\r\nDiscount - ' + discount + '\r\nTax - ' + Utils.roundNumber(tax, 2) + '\r\nAmount to pay : ' + Utils.roundNumber(grandTotal, 0).toString() + '\r\nBill sent to your E-mail Id If Provided';

            var smsObj = {mobile: billObject.MNm, message: template}
            $http.post('/api/emails/sendSms', smsObj).success(function (data) {
              // console.log(data);
            });
          }
        }
      }
      /*Email and SMS bill integration ends here*/
    };
    var toPrice10 = function (val) {
      val = '' + val;
      var len = val.length;
      return ('        ' + val).slice(-8);
    }
    var getPrinterStationInstance=function(station_id,instance_id){
     // console.log(station_id+':'+instance_id);
      var printer=null;
      var stationPrinters=localStorageService.get('InstanceStationPrinter');
      //console.log(stationPrinters);
      var stationPrintersArr=[];
      if(!(stationPrinters==null && stationPrinters==undefined) ){
        stationPrintersArr=stationPrinters;
      }
      if(stationPrintersArr.length>0){
        for(var i=0;i<stationPrintersArr.length;i++){
          if(stationPrintersArr[i].station_id==station_id && stationPrintersArr[i].id==instance_id){
            if(_.has(stationPrintersArr[i],'printer')){
              if(!(stationPrintersArr[i].printer==null || stationPrintersArr[i].printer==undefined) ){
                printer=stationPrintersArr[i].printer;
              }
            }
          }
        }
      }
      return printer;
    }
    var getPrintersFromBill = function (bill) {
      var printers = [];
      for (var index = 0; index < bill._kots.length; index++) {
        for (var i = 0; i < bill._kots[index].items.length; i++) {
          if (_.has(bill._kots[index].items[i], 'category')) {
            if (_.has(bill._kots[index].items[i].category, 'categoryStation')) {
              var printername = bill._kots[index].items[i].category.categoryStation.stationPrinter;
              var _fPrinter = _.filter(printers, {'name': printername});
              if (_fPrinter.length == 0) {
                printers.push({'name': printername});
              }
            }
          }
        }
      }
      return printers;
    }

    return {
      getPrinters: getPrinters,
      print: print
    }
  }])
  .
  factory('subdomain', ['$location', function ($location) {
    var host = $location.host();
    if (host.indexOf('.') < 0)
      return null;
    else if (host.split('.').length > 2) {
      return host.split('.')[0];
    } else {
      return null;
    }

  }])

  .factory('Pagination', function () {

    var pagination = {};

    pagination.getNew = function (perPage) {

      perPage = perPage === undefined ? 5 : perPage;

      /*var paginator = {
        numPages: 1,
        perPage: perPage,
        page: 0
      };

      paginator.prevPage = function () {
        if (paginator.page > 0) {
          paginator.page -= 1;
        }
      };

      paginator.nextPage = function () {
        if (paginator.page < paginator.numPages - 1) {
          paginator.page += 1;
        }
      };*/
         var paginator = {
        numPages: 1,
        perPage: perPage,
        page: 0,
        set: 0
      };

      paginator.prevPage = function () {
        if (paginator.page > 0) {
          if(paginator.page == paginator.set)
            paginator.set -=10;
          paginator.page -= 1;
        }
      };

      paginator.nextPage = function () {
        if (paginator.page < paginator.numPages - 1) {
          paginator.page += 1;
          if(paginator.page == paginator.set+10)
            paginator.set +=10;
        }
      };


      paginator.toPageId = function (id) {
        if (id >= 0 && id <= paginator.numPages - 1) {
          paginator.page = id;
        }
      };

      return paginator;
    };

    return pagination;
  })
  .factory('PosSharedService', function ($rootScope) {
    var sharedService = {};
    sharedService.message = '';
    sharedService.prepForBroadcast = function (msg) {
      this.message = msg;
      this.broadcastItem();
    };
    sharedService.broadcastItem = function () {
      $rootScope.$broadcast('localBroadcast');
    };
    return sharedService;
  })

  .factory("$webSql", ["$q",
    function ($q) {
      return {
        openDatabase: function (dbName, version, desc, size) {
          try {
            var db = openDatabase(dbName, version, desc, size);
            if (typeof(openDatabase) == "undefined")
              throw "Browser does not support web sql";
            return {
              executeQuery: function (query, values) {
                var deferred = $q.defer();
                db.transaction(function (tx) {
                  tx.executeSql(query, values, function (tx, results) {
                    //console.log('rrsults', results);
                    deferred.resolve(results);
                  }, function (tx, e) {
                    console.log(tx, query, values);
                    console.log("There has been an error: " + e.message);
                    deferred.reject();
                  });
                });
                return deferred.promise;
              },
              insert: function (c, e, r) {
                var f = (typeof r === "boolean" && r) ? "INSERT OR REPLACE" : "INSERT";
                f += " INTO `{tableName}` ({fields}) VALUES({values});";
                var a = "",
                  b = "",
                  v = [];
                for (var d in e) {
                  a += (Object.keys(e)[Object.keys(e).length - 1] == d) ? "`" + d + "`" : "`" + d + "`, ";
                  b += (Object.keys(e)[Object.keys(e).length - 1] == d) ? "?" : "?, ";
                  v.push(e[d]);
                }
                return this.executeQuery(this.replace(f, {
                  "{tableName}": c,
                  "{fields}": a,
                  "{values}": b
                }), v);
              },
              update: function (b, g, c) {
                var f = "UPDATE `{tableName}` SET {update} WHERE {where}; ";
                var e = "";
                var v = [];
                for (var d in g) {
                  e += (Object.keys(g)[Object.keys(g).length - 1] == d) ? "`" + d + "`= ?" : "`" + d + "`= ?,";
                  v.push(g[d]);
                }
                var a = this.whereClause(c);
                return this.executeQuery(this.replace(f, {
                  "{tableName}": b,
                  "{update}": e,
                  "{where}": a.w
                }), v.concat(a.p));
              },
              del: function (b, c) {
                var d = "DELETE FROM `{tableName}` WHERE {where}; ";
                var a = this.whereClause(c);
                return this.executeQuery(this.replace(d, {
                  "{tableName}": b,
                  "{where}": a.w
                }), a.p);
              },
              select: function (b, c) {
                var d = "SELECT * FROM `{tableName}` WHERE {where}; ";
                var a = this.whereClause(c);
                return this.executeQuery(this.replace(d, {
                  "{tableName}": b,
                  "{where}": a.w
                }), a.p);
              },
              selectDynamic: function (b) {
                return this.executeQuery(b, []);
              },
              selectAll: function (a) {
                return this.executeQuery("SELECT * FROM `" + a + "`; ", []);
              },
              whereClause: function (b) {
                var a = "",
                  v = [];
                for (var c in b) {
                  if (typeof b[c] !== "undefined" && typeof b[c] !== "object" && typeof b[c] === "string" && !b[c].match(/NULL/ig)) v.push(b[c]);
                  else if (typeof b[c] !== "undefined" && typeof b[c] !== "object" && typeof b[c] === "number") v.push(b[c]);
                  else if (typeof b[c]["value"] !== "undefined" && typeof b[c] === "object" && (typeof b[c]["value"] === "number" || !b[c]["value"].match(/NULL/ig))) v.push(b[c]["value"]);
                  a += (typeof b[c] === "object") ?
                    (typeof b[c]["union"] === "undefined") ?
                      (typeof b[c]["value"] === "string" && b[c]["value"].match(/NULL/ig)) ?
                      "`" + c + "` " + b[c]["value"] :
                        (typeof b[c]["operator"] !== "undefined") ?
                        "`" + c + "` " + b[c]["operator"] + " ? " :
                        "`" + c + "` = ?" :
                      (typeof b[c]["value"] === "string" && b[c]["value"].match(/NULL/ig)) ?
                      "`" + c + "` " + b[c]["value"] + " " + b[c]["union"] + " " :
                        (typeof b[c]["operator"] !== "undefined") ?
                        "`" + c + "` " + b[c]["operator"] + " ? " + b[c]["union"] + " " :
                        "`" + c + "` = ? " + b[c]["union"] + " " :
                    (typeof b[c] === "string" && b[c].match(/NULL/ig)) ?
                    "`" + c + "` " + b[c] :
                    "`" + c + "` = ?"
                }
                return {
                  w: a,
                  p: v
                };
              },
              replace: function (a, c) {
                for (var b in c) {
                  a = a.replace(new RegExp(b, "ig"), c[b])
                }
                return a;
              },
              createTable: function (j, g) {
                var b = "CREATE TABLE IF NOT EXISTS `{tableName}` ({fields}); ";
                var c = [];
                var a = "";
                for (var e in g) {
                  var l = "{type} {null}";
                  a += "`" + e + "` ";
                  if (typeof g[e]["null"] === "undefined") g[e]["null"] = "NULL";
                  for (var k in g[e]) {
                    l = l.replace(new RegExp("{" + k + "}", "ig"), g[e][k])
                  }
                  a += l;
                  if (typeof g[e]["default"] !== "undefined") {
                    a += " DEFAULT " + g[e]["default"]
                  }
                  if (typeof g[e]["primary"] !== "undefined") {
                    a += " PRIMARY KEY"
                  }
                  if (typeof g[e]["auto_increment"] !== "undefined") {
                    a += " AUTOINCREMENT"
                  }
                  if (Object.keys(g)[Object.keys(g).length - 1] != e) {
                    a += ","
                  }
                  if (typeof g[e]["primary"] !== "undefined" && g[e]["primary"]) {
                    c.push(e)
                  }
                }
                var d = {
                  tableName: j,
                  fields: a
                };
                for (var f in d) {
                  b = b.replace(new RegExp("{" + f + "}", "ig"), d[f])
                }
                return this.executeQuery(b, []);
              },
              dropTable: function (a) {
                return this.executeQuery("DROP TABLE IF EXISTS `" + a + "`; ", []);
              }
            };
          } catch (err) {
            console.error(err);
          }
        }
      }
    }
  ])
  /*.factory("appcache", function ($rootScope, $q) {

   var appCache = window.applicationCache;
   var iface = {
   "checkForUpdate": function () {
   var def = $q.defer();
   def.reject();
   return def.promise;
   }
   };

   // If there is no appcache available, return null functions
   if (!appCache) {
   return iface;
   }

   var handleCacheEvent = function (e) {
   $rootScope.$broadcast("appcache." + e.type, e);
   };

   var bootstrapEvents = function () {
   ["cached", "checking", "downloading", "error", "noupdate",
   "obsolete", "progress", "updateready"].map(function (eventName) {

   appCache.addEventListener(eventName, handleCacheEvent, false);
   });
   };
   bootstrapEvents();

   iface.checkForUpdate = function () {
   var def = $q.defer();
   appCache.update();
   caHelpersOnce($rootScope, "appcache.updateready", function () {
   alert();
   def.resolve(function (now) {
   appCache.swapCache();
   var applyUpdateNow = function () {
   window.location.reload();
   };
   if (now) {
   applyUpdateNow();
   return function () {
   };
   }
   return applyUpdateNow;
   });
   });
   caHelpersOnce($rootScope, "appcache.noupdate", function () {
   def.reject();
   });
   return def.promise;
   };

   return iface;

   })*/

  .filter('startFrom', function () {
    return function (input, start) {
      if (input === undefined) {
        return input;
      } else {
        return input.slice(+start);
      }
    };
  })
  .filter('range', function () {
    return function (input, total) {
      total = parseInt(total);
      for (var i = 0; i < total; i++) {
        input.push(i);
      }
      return input;
    };
  })
  .filter('sumByKey', function () {
    return function (data, key) {
      if (typeof(data) === 'undefined' || typeof(key) === 'undefined') {
        return 0;
      }
      var sum = 0;
      for (var i = data.length - 1; i >= 0; i--) {
        sum += parseFloat(data[i][key]);
      }
      return sum;
    };
  })
  .filter('capitalize', function () {
    return function (input, all) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    }
  })
  .directive('vertilizeContainer', [

    function () {
      return {
        restrict: 'EA',
        link: function (scope, attrs) {
          console.log(attrs);
        },
        controller: [
          '$scope', '$window',

          function ($scope, $window) {
            // Alias this
            var _this = this;
            console.log($scope.vWidth);
            $scope.vWidth = $window.innerWidth;

            // Array of children heights
            _this.childrenHeights = [];

            // API: Allocate child, return index for tracking.
            _this.allocateMe = function () {
              _this.childrenHeights.push(0);
              return (_this.childrenHeights.length - 1);
            };

            // API: Update a child's height
            _this.updateMyHeight = function (index, height) {
              _this.childrenHeights[index] = height;
            };

            // API: Get tallest height
            _this.getTallestHeight = function () {
              var height = 0;
              for (var i = 0; i < _this.childrenHeights.length; i = i + 1) {
                height = Math.max(height, _this.childrenHeights[i]);
              }
              return height;
            };

            // Add window resize to digest cycle
            angular.element($window).bind('resize', function () {
              console.log($window.innerWidth);
              return $scope.$apply();
            });
          }]
      };
    }])
  .directive('vertilize', [

    function () {
      return {
        restrict: 'EA',
        require: '^vertilizeContainer',
        link: function (scope, element, attrs, parent) {
          // My index allocation
          var myIndex = parent.allocateMe();

          // Get my real height by cloning so my height is not affected.
          var getMyRealHeight = function () {
            var clone = element.clone()
              .removeAttr('vertilize')
              .css({
                height: '',
                width: element.width(),
                position: 'fixed',
                top: 0,
                left: 0,
                visibility: 'hidden'
              });
            element.after(clone);
            var realHeight = clone.height();
            clone['remove']();
            return realHeight;
          };

          // Watch my height
          scope.$watch(getMyRealHeight, function (myNewHeight) {
            if (myNewHeight) {
              parent.updateMyHeight(myIndex, myNewHeight);
            }
          });

          // Watch for tallest height change
          scope.$watch(parent.getTallestHeight, function (tallestHeight) {
            if (tallestHeight) {
              element.css('height', tallestHeight + 2);
            }
          });
        }
      };
    }])
  .directive('mask', function () {
    return {
      restrict: 'A',
      link: function (scope, elem, attr, ctrl) {

        if (attr.mask)
          elem.mask(attr.mask, {
            placeholder: attr.maskPlaceholder
          });
      }
    };
  })
  .directive('nospace', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, modelCtrl) {

        modelCtrl.$parsers.push(function (inputValue) {

          if (inputValue != undefined) {
            var transformedInput = inputValue.toLowerCase().replace(/ /g, '');

            if (transformedInput != inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
            }

            return transformedInput;
          } else {
            return;
          }

        });
      }
    };
  })
  .directive('resize', function ($window) {
    return {
      link: function (scope, element) {

        angular.element(document).ready(function () {
          var offset = element.offset();
          element.height(($window.outerHeight - offset.top) - 120);
        });

        angular.element($window).on('resize', function (e) {
          var offset = element.offset();
          element.height(($window.outerHeight - offset.top) - 120);
        });

      }
    }
  })
  .directive('keyboard', function () {
    return {
      require: '?ngModel',
      restrict: 'C',
      link: function (scope, element, attrs, ngModelCtrl) {
        if (!ngModelCtrl) {
          return;
        }
        $(element).keyboard({
          stickyShift: false,
          usePreview: false,
          autoAccept: true,

          change: function (e, kb, el) {
            ngModelCtrl.$setViewValue(el.value);
          }
        });
      }
    };
  })
  .directive('smartFloat', function ($filter) {
    var FLOAT_REGEXP_1 = /^\$?\d+(.\d{3})*(\,\d*)?$/; //Numbers like: 1.123,56
    var FLOAT_REGEXP_2 = /^\$?\d+(,\d{3})*(\.\d*)?$/; //Numbers like: 1,123.56
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$parsers.unshift(function (viewValue) {
          if (FLOAT_REGEXP_1.test(viewValue)) {
            ctrl.$setValidity('float', true);
            return parseFloat(viewValue);
          } else if (FLOAT_REGEXP_2.test(viewValue)) {
            ctrl.$setValidity('float', true);
            return parseFloat(viewValue);
          } else {
            ctrl.$setValidity('float', false);
            return undefined;
          }
        });
        ctrl.$formatters.unshift(
          function (modelValue) {
            return $filter('number')(parseFloat(modelValue), 2);
          }
        );
      }
    };
  })
  .directive('validNumber', function () {
    return {
      require: '?ngModel',
      link: function (scope, element, attrs, ngModelCtrl) {
        if (!ngModelCtrl) {
          return;
        }

        ngModelCtrl.$parsers.push(function (val) {

          if (val != undefined) {
            console.log(val);
            var clean = val.replace(/[^0-9]+/g, '');
            ngModelCtrl.$setViewValue(parseFloat(clean));
            ngModelCtrl.$render();

          }
          return (clean != undefined) ? parseFloat(clean) : 0;

        });

        /*element.bind('keypress', function (event) {
         if (event.keyCode === 32) {
         event.preventDefault();
         }
         });*/
      }
    };
  })
  .directive('focusMe', function ($timeout) {
    return {
      scope: {
        trigger: '@focusMe'
      },
      link: function (scope, element) {
        scope.$watch('trigger', function (value) {
          if (value === "true") {
            // console.log('trigger',value);
            //  console.log(element);
            $timeout(function () {
              element[0].focus();
            });
          }
        });
      }
    };
  })
  .directive('myFocus', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
        scope.$watch(attr.myFocus, function (n, o) {
          if (n != 0 && n) {
            element[0].focus();
          }
        });
      }
    };
  })
  .directive('ngEnter', function () {
    return function (scope, element, attrs) {
      element.bind("keypress", function (event) {
        if (event.which === 13) {
          scope.$apply(function () {
            scope.$eval(attrs.ngEnter, {
              'event': event
            });
          });

          event.preventDefault();
        }
      });
    };
  })
  .directive('ngCsvImport', function ($q, $timeout) {
    return {
      restrict: 'EA',
      transclude: true,
      replace: true,
      scope: {
        content: '=',
        header: '=',
        headerVisible: '=',
        separator: '=',
        result: '=',
        done: '&'
      },
      template: '<div><div ng-show="header && headerVisible"><label>Header</label><input type="checkbox" ng-model="header"></div>' +
      '<div ng-show="separator" class="form-group"><label>Seperator</label><input type="text" class="form-control" ng-change="changeSeparator" ng-model="separator"></div>' +
      '<div><input class="btn btn-default" type="file"/></div></div>',

      link: function (scope, element, attrs) {

        /*element.on('keyup', function (e) {
         console.log("On Keyup");
         if (scope.content != null) {
         var content = {
         csv: scope.content,
         header: scope.header,
         separator: e.target.value
         };
         scope.result = csvToJSON(content);
         scope.$apply();
         }
         });*/

        element.on('change', function (onChangeEvent) {
          console.log("On Change");
          var reader = new FileReader();
          reader.onload = function (onLoadEvent) {

            console.log("Post readAsText -> onLOAD");
            scope.$apply(function () {
              console.log("Post readAsText -> onLOAD -> Applying");
              var content = {
                csv: onLoadEvent.target.result.replace(/\r\n|\r/g, '\n'),
                header: scope.header,
                separator: scope.separator
              };

              scope.content = content.csv;
              csvToJSON(content).then(function (_result) {
                /* console.log(_result);*/
                scope.result = _result;
                scope.done()(_result);
                /*console.log(onLoadEvent);*/
              });
            });


          };
          if ((onChangeEvent.target.type === "file") && (onChangeEvent.target.files != null || onChangeEvent.srcElement.files != null)) {
            console.log("ReadAsText");
            reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);

          } else {
            if (scope.content != null) {
              console.log("ELSE ReadAsText");
              var content = {
                csv: scope.content,
                header: !scope.header,
                separator: scope.separator
              };
              scope.result = csvToJSON(content);

            }
          }
        });

        var csvToJSON = function (content) {
          var d = $q.defer();

          var lines = content.csv.split('\n');
          var result = [];
          var start = 0;
          var columnCount = lines[0].split(content.separator).length;

          var headers = [];
          if (content.header) {
            headers = lines[0].split(content.separator);
            start = 1;
          }
          var percentComplete = 0;

          try {

            for (var i = start; i < lines.length; i++) {
              var obj = {};
              var currentline = lines[i].split(new RegExp(content.separator + '(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
              percentComplete = (i + 1) / lines.length * 100;
              d.notify(percentComplete);

              if (currentline.length === columnCount) {
                if (content.header) {
                  for (var j = 0; j < headers.length; j++) {
                    obj[headers[j]] = currentline[j];
                  }
                } else {
                  for (var k = 0; k < currentline.length; k++) {
                    obj[k] = currentline[k];
                  }
                }
                result.push(obj);
              }
            }

            d.resolve(result);

          } catch (e) {

            d.reject(e);

          }

          return d.promise;
        };
      }
    };
  })
  .directive('contextMenu', function ($parse) {
    var renderContextMenu = function ($scope, event, options) {
      if (!$) {
        var $ = angular.element;
      }
      $(event.currentTarget).addClass('context');
      var $contextMenu = $('<div>');
      $contextMenu.addClass('dropdown clearfix');
      var $ul = $('<ul>');
      $ul.addClass('dropdown-menu');
      $ul.attr({
        'role': 'menu'
      });
      $ul.css({
        display: 'block',
        position: 'absolute',
        left: event.pageX + 'px',
        top: event.pageY + 'px'
      });
      angular.forEach(options, function (item, i) {
        var $li = $('<li>');
        if (item === null) {
          $li.addClass('divider');
        } else {
          var $a = $('<a>');
          $a.attr({
            tabindex: '-1',
            href: '#'
          });
          $a.text(typeof item[0] == 'string' ? item[0] : item[0].call($scope, $scope));
          $li.append($a);
          $li.on('click', function ($event) {
            $event.preventDefault();
            $scope.$apply(function () {
              $(event.currentTarget).removeClass('context');
              $contextMenu.remove();
              item[1].call($scope, $scope);
            });
          });
        }
        $ul.append($li);
      });
      $contextMenu.append($ul);
      var height = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
      );
      $contextMenu.css({
        width: '100%',
        height: height + 'px',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 9999
      });
      $(document).find('body').append($contextMenu);
      $contextMenu.on("mousedown", function (e) {
        if ($(e.target).hasClass('dropdown')) {
          $(event.currentTarget).removeClass('context');
          $contextMenu.remove();
        }
      }).on('contextmenu', function (event) {
        $(event.currentTarget).removeClass('context');
        event.preventDefault();
        $contextMenu.remove();
      });
    };
    return function ($scope, element, attrs) {
      element.on('contextmenu', function (event) {
        $scope.$apply(function () {
          event.preventDefault();
          var options = $scope.$eval(attrs.contextMenu);
          if (options instanceof Array) {
            renderContextMenu($scope, event, options);
          } else {
            throw '"' + attrs.contextMenu + '" not an array';
          }
        });
      });
    };
  })
  .directive('highChart', function () {
    return {
      restrict: 'E',
      template: '<div></div>',
      scope: {
        chartData: "=value",
        chartObj: "=?"
      },
      transclude: true,
      replace: true,
      link: function ($scope, $element, $attrs) {

        //Update when charts data changes
        $scope.$watch('chartData', function (value) {
          if (!value) return;

          // Initiate the chartData.chart if it doesn't exist yet
          $scope.chartData.chart = $scope.chartData.chart || {};

          // use default values if nothing is specified in the given settings
          $scope.chartData.chart.renderTo = $scope.chartData.chart.renderTo || $element[0];
          if ($attrs.type) $scope.chartData.chart.type = $scope.chartData.chart.type || $attrs.type;
          if ($attrs.height) $scope.chartData.chart.height = $scope.chartData.chart.height || $attrs.height;
          if ($attrs.width) $scope.chartData.chart.width = $scope.chartData.chart.type || $attrs.width;

          $scope.chartObj = new Highcharts.Chart($scope.chartData);
        });
      }
    };

  })
  .directive('hcChart', function () {
    return {
      restrict: 'C',
      replace: true,
      scope: {
        chartObject: '=',
        items: '=',
        options: '='
      },
      //template: '<div id="{{chartObject.chart.renderTo}}" style="margin: 0 auto">not working</div>',
      link: function (scope, element, attrs) {
        //console.log(angular.toJson(scope.chartObject));
        //scope.containerId = scope.chartObject.chart.renderTo;

        var chart = new Highcharts.Chart(scope.chartObject);

        scope.$watch("chartObject", function (newValue) {
          //chart.series[0].setData(newValue, true);
          //console.log(scope.chartObject);
          chart = new Highcharts.Chart(scope.chartObject);
        }, true);

      }
    }
  })
  .directive('hcPie', function () {
    return {
      restrict: 'C',
      replace: true,
      scope: {
        items: '=',
        options: '='
      },
      controller: function ($scope, $element, $attrs) {
      },
      template: '<div id="container" style="margin: 0 auto"></div>',
      link: function (scope, element, attrs) {
      //  alert();
        //console.log(scope.items.drilldown);
        var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'container',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'column'
          },
          xAxis: {
            type: 'category'
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Values'
            }
          },
          lang: {
            drillUpText: '&lt; Go Back {series.name}'
          },
          title: {
            text: attrs.title
          },
          tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage}</b>',
            percentageDecimals: 1
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                enabled: true,
                color: '#000000',
                connectorColor: '#000000',
                formatter: function () {
                  return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(2);
                }
              }
            },
            series: {
              dataLabels: {
                enabled: true,
                format: '{point.y:.2f}',
                inside: true,
                allowOverlap: false
              }
            }
          },
          series: [{
            name: 'Total Sales',
            colorByPoint: true,
            data: scope.items.tabData
          }],
          drilldown: {
            drillUpButton: {
              relativeTo: 'spacingBox',
              position: {
                y: 0,
                x: 0
              },
              theme: {
                fill: 'white',
                'stroke-width': 1,
                stroke: 'silver',
                r: 0,
                states: {
                  hover: {
                    fill: '#bada55'
                  },
                  select: {
                    stroke: '#039',
                    fill: '#bada55'
                  }
                }
              }

            },
            series: scope.items.drilldown
          }
        });

        /*scope.$watch("items", function (newValue) {
          //chart.series[0].setData(newValue, true);
          // console.log(scope.items);
        }, true);*/
      }
    }
  })
  .directive('hcPieChart', function () {
    return {
      restrict: 'C',
      replace: true,
      scope: {
        items: '='
      },
      controller: function ($scope, $element, $attrs) {
      },
      template: '<div id="container" style="margin: 0 auto"></div>',
      link: function (scope, element, attrs) {
        var chart = new Highcharts.Chart({
          chart: {
            renderTo: 'container',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
          },
          title: {
            text: ''
          },
          tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
            percentageDecimals: 1
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                enabled: true,
                color: '#000000',
                connectorColor: '#000000',
                formatter: function () {
                  return '<b>' + this.point.name + '</b>: ' + (isNaN(this.percentage) ? this.percentage : this.percentage.toFixed(2)) + ' %';
                }
              }
            }
          },
          series: [{
            type: 'pie',
            name: 'Top Categories',
            data: scope.items
          }]
        });
        scope.$watch("items", function (newValue) {
          chart.series[0].setData(newValue, true);
        }, true);

      }
    }
  })
  .directive('slideable', function () {
    return {
      restrict: 'C',
      compile: function (element, attr) {
        // wrap tag
        var contents = element.html();
        element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

        return function postLink(scope, element, attrs) {
          // default properties
          attrs.duration = (!attrs.duration) ? '.5s' : attrs.duration;
          attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
          element.css({
            'overflow': 'hidden',
            'height': '0px',
            'transitionProperty': 'height',
            'transitionDuration': attrs.duration,
            'transitionTimingFunction': attrs.easing
          });
        };
      }
    };
  })
  /* .directive('slideToggle', function() {
   return {
   scope:{
   toggableControl:'=',
   control:'='
   },
   restrict: 'A',
   link: function(scope, element, attrs) {

   var target, content;

   scope.internalControl = scope.control || {};
   scope.internalControl.abc = function () {
   alert("Hello from Directive, internal");
   }

   console.log(scope);

   attrs.expanded = false;

   element.bind('click', function() {
   console.log(attrs);
   if (!target) target = document.querySelector(attrs.slideToggle);
   if (!content) content = target.querySelector('.slideable_content');

   if(!attrs.expanded) {
   content.style.border = '1px solid rgba(0,0,0,0)';
   var y = content.clientHeight;
   content.style.border = 0;
   target.style.height = y + 'px';
   } else {
   target.style.height = '0px';
   }
   attrs.expanded = !attrs.expanded;
   });
   }
   }
   })*/
  .directive('slideToggle', function () {
    return {
      restrict: 'E',
      controller: function ($scope, $attrs, PosSharedService) {
        $scope.$on('localBroadcast', function () {
          $scope.message = 'Directive: ' + PosSharedService.message;
        });
      },
      link: function (scope, element, attrs) {
        var target, content;
        attrs.expanded = false;
        element.bind('click', function () {
          console.log(attrs);
          if (!target) target = document.querySelector(attrs.slideToggle);
          if (!content) content = target.querySelector('.slideable_content');

          if (!attrs.expanded) {
            content.style.border = '1px solid rgba(0,0,0,0)';
            var y = content.clientHeight;
            content.style.border = 0;
            target.style.height = y + 'px';
          } else {
            target.style.height = '0px';
          }
          attrs.expanded = !attrs.expanded;
        });
      }
    }
  })
  .directive('posistDraggable', ['$rootScope', 'Utils', function ($rootScope, Utils) {
    return {
      restrict: 'A',
      link: function (scope, el, attrs, controller) {
        console.log("linking draggable element");

        angular.element(el).attr("draggable", "true");
        var id = attrs.id;
        if (!attrs.id) {
          id = Utils.guid();
          angular.element(el).attr("id", id);
        }

        el.bind("dragstart", function (e) {
          e.dataTransfer.setData('text', id);

          $rootScope.$emit("DRAG-START");
        });

        el.bind("dragend", function (e) {
          $rootScope.$emit("DRAG-END");
        });
      }
    }
  }])
  .directive('posistDropTarget', ['$rootScope', 'Utils', function ($rootScope, Utils) {
    return {
      restrict: 'A',
      scope: {
        onDrop: '&'
      },
      link: function (scope, el, attrs, controller) {
        var id = attrs.id;
        if (!attrs.id) {
          id = Utils.guid();
          angular.element(el).attr("id", id);
        }

        el.bind("dragover", function (e) {
          if (e.preventDefault) {
            e.preventDefault(); // Necessary. Allows us to drop.
          }

          e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
          return false;
        });

        el.bind("dragenter", function (e) {
          // this / e.target is the current hover target.
          angular.element(e.target).addClass('lvl-over');
        });

        el.bind("dragleave", function (e) {
          angular.element(e.target).removeClass('lvl-over');  // this / e.target is previous target element.
        });

        el.bind("drop", function (e) {
          if (e.preventDefault) {
            e.preventDefault(); // Necessary. Allows us to drop.
          }

          if (e.stopPropogation) {
            e.stopPropogation(); // Necessary. Allows us to drop.
          }
          var data = e.dataTransfer.getData("text");
          var dest = document.getElementById(id);
          var src = document.getElementById(data);

          scope.onDrop({event: {dragEl: src, dropEl: dest}});
        });

        $rootScope.$on("DRAG-START", function () {
          var el = document.getElementById(id);
          angular.element(el).addClass("lvl-target");
        });

        $rootScope.$on("DRAG-END", function () {
          var el = document.getElementById(id);
          angular.element(el).removeClass("lvl-target");
          angular.element(el).removeClass("lvl-over");
        });
      }
    }
  }])
  .directive('onLongPress', function ($parse, $timeout) {
    var renderContextMenu = function ($scope, event, options, dim) {
      if (!$) {
        var $ = angular.element;
      }
      $(event.currentTarget).addClass('context');
      var $contextMenu = $('<div>');
      $contextMenu.addClass('dropdown clearfix');
      var $ul = $('<ul>');
      $ul.addClass('dropdown-menu');
      $ul.attr({
        'role': 'menu'
      });
      var pagex = dim.left + 150;
      var pagey = dim.top + 80;
      $ul.css({
        display: 'block',
        position: 'absolute',
        left: pagex + 'px',
        top: pagey + 'px'
      });
      //console.log(event.pageX,event.pageY)
      angular.forEach(options, function (item, i) {
        var $li = $('<li>');
        if (item === null) {
          $li.addClass('divider');
        } else {
          var $a = $('<a>');
          $a.attr({
            tabindex: '-1',
            href: '#'
          });
          $a.text(typeof item[0] == 'string' ? item[0] : item[0].call($scope, $scope));
          $li.append($a);
          $li.on('click', function ($event) {
            $event.preventDefault();
            $scope.$apply(function () {
              $(event.currentTarget).removeClass('context');
              $contextMenu.remove();
              item[1].call($scope, $scope);
            });
          });
        }
        $ul.append($li);
      });
      $contextMenu.append($ul);
      var height = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
      );
      $contextMenu.css({
        width: '100%',
        height: height + 'px',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 9999
      });
      console.log("contextmenu", $contextMenu)
      $(document).find('body').append($contextMenu);
      $contextMenu.on("mousedown", function (e) {
        if ($(e.target).hasClass('dropdown')) {
          $(event.currentTarget).removeClass('context');
          $contextMenu.remove();
        }
      }).on('contextmenu', function (event) {
        $(event.currentTarget).removeClass('context');
        event.preventDefault();
        $contextMenu.remove();
      });
    };
    return function ($scope, element, attrs) {
      var ua = navigator.userAgent.toLowerCase();
      var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
      if (isAndroid) {
        // element.on('contextmenu', function (event) {
        //   $scope.$apply(function () {
        //     event.preventDefault();
        //     var options = $scope.$eval(attrs.contextMenu);
        //     console.log(options,element[0].getBoundingClientRect().left)
        //     var dimension = element[0].getBoundingClientRect();
        //     if (options instanceof Array) {
        //       renderContextMenu($scope, event, options,dimension);
        //     } else {
        //       throw '"' + attrs.contextMenu + '" not an array';
        //     }
        //   });
        // });
      }
      else {
        element.bind('touchstart', function (event) {

          // console.log("hello12",event)
          $scope.longPress = true;

          // We'll set a timeout for 600 ms for a long press
          $timeout(function () {
            if ($scope.longPress) {
              //alert("hello1")
              // If the touchend event hasn't fired,
              // apply the function given in on the element's on-long-press attribute
              $scope.$apply(function () {
                //$scope.$eval($attrs.onLongPress)
                event.preventDefault();
                var options = $scope.$eval(attrs.contextMenu);
                console.log(options, element[0].getBoundingClientRect().left)
                var dimension = element[0].getBoundingClientRect();
                if (options instanceof Array) {
                  //alert("hello16")
                  renderContextMenu($scope, event, options, dimension);
                } else {
                  throw '"' + attrs.contextMenu + '" not an array';
                }
              });
            }
          }, 600);
          element.bind('touchend', function (evt) {
            // Prevent the onLongPress event from firing
            $scope.longPress = false;
            // If there is an on-touch-end function attached to this element, apply it
            if (attrs.onTouchEnd) {
              $scope.$apply(function () {
                $scope.$eval(attrs.onTouchEnd)
              });
            }
          });
          // $scope.$apply(function () {
          //   event.preventDefault();
          //   var options = $scope.$eval(attrs.contextMenu);
          //   if (options instanceof Array) {
          //     renderContextMenu($scope, event, options);
          //   } else {
          //     throw '"' + attrs.contextMenu + '" not an array';
          //   }
          // });
        });
      }
      // element.bind('touchstart', function (event) {

      //  // console.log("hello12",event)
      //   $scope.longPress = true;

      //   // We'll set a timeout for 600 ms for a long press
      //   $timeout(function() {
      //     if ($scope.longPress) {
      //       //alert("hello1")
      //       // If the touchend event hasn't fired,
      //       // apply the function given in on the element's on-long-press attribute
      //       $scope.$apply(function() {
      //         //$scope.$eval($attrs.onLongPress)
      //         event.preventDefault();
      //         var options = $scope.$eval(attrs.contextMenu);
      //         console.log(options,element[0].getBoundingClientRect().left)
      //         var dimension = element[0].getBoundingClientRect();
      //         if (options instanceof Array) {
      //           //alert("hello16")
      //           renderContextMenu($scope, event, options,dimension);
      //         } else {
      //           throw '"' + attrs.contextMenu + '" not an array';
      //         }
      //       });
      //     }
      //   }, 600);
      //   element.bind('touchend', function(evt) {
      //     // Prevent the onLongPress event from firing
      //     $scope.longPress = false;
      //     // If there is an on-touch-end function attached to this element, apply it
      //     if (attrs.onTouchEnd) {
      //       $scope.$apply(function() {
      //         $scope.$eval(attrs.onTouchEnd)
      //       });
      //     }
      //   });
      //   // $scope.$apply(function () {
      //   //   event.preventDefault();
      //   //   var options = $scope.$eval(attrs.contextMenu);
      //   //   if (options instanceof Array) {
      //   //     renderContextMenu($scope, event, options);
      //   //   } else {
      //   //     throw '"' + attrs.contextMenu + '" not an array';
      //   //   }
      //   // });
      // });
    };
  })
  // .directive('onLongPress', function($timeout) {
  //   return {
  //     restrict: 'A',
  //     link: function($scope, $elm, $attrs) {
  //       $elm.bind('touchstart', function(evt) {
  //         // Locally scoped variable that will keep track of the long press
  //         $scope.longPress = true;

  //         // We'll set a timeout for 600 ms for a long press
  //         $timeout(function() {
  //           if ($scope.longPress) {
  //             // If the touchend event hasn't fired,
  //             // apply the function given in on the element's on-long-press attribute
  //             $scope.$apply(function() {
  //               $scope.$eval($attrs.onLongPress)
  //             });
  //           }
  //         }, 600);
  //       });

  //       $elm.bind('touchend', function(evt) {
  //         // Prevent the onLongPress event from firing
  //         $scope.longPress = false;
  //         // If there is an on-touch-end function attached to this element, apply it
  //         if ($attrs.onTouchEnd) {
  //           $scope.$apply(function() {
  //             $scope.$eval($attrs.onTouchEnd)
  //           });
  //         }
  //       });
  //     }
  //   };
  // })
  .run(['$rootScope', '$location', 'Auth', '$state', '$stateParams', '$webSql', '$timeout', 'socket', '$modal', 'subdomain', 'Customer', 'Sync', 'Item', '$cookieStore', 'SilentPrinter', 'growl', 'localStorageService', 'Utils', 'billResource', function ($rootScope, $location, Auth, $state, $stateParams, $webSql, $timeout, socket, $modal, subdomain, Customer, Sync, Item, $cookieStore, SilentPrinter, growl, localStorageService, Utils, billResource) {

    /*if (subdomain.length > 0 && $location.path() === '/') {
     $location.path('/login');
     }*/


    Auth.isLoggedInAsync(function (isLoggedIn) {
      if (isLoggedIn) {
        $rootScope.currentUser = Auth.getCurrentUser();

      }
    });

    /* Event Listener: New Deployment created */
    /* socket.on('deployment:save', function (d) {
     console.log(d);
     });*/

    $rootScope.$on('deployment:save', function (event, data) {

      /*socket.emit('event:new:image',data);

       scope.$apply(function(){
       scope.messages.unshift(data);
       });*/
      alert("Deployment created");
    });


    /* Create new Application Instance ID and store to LocalStorage */
    /* $rootScope.instanceId = "";
     if (localStorageService.get('instanceId')) {
     $rootScope.instanceId = localStorageService.get('instanceId');
     } else {
     localStorageService.set('instanceId', Utils.guid());
     $rootScope.instanceId = localStorageService.get('instanceId');
     }*/

    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $rootScope.showLoaderOverlay = false;

    /* Current User */
    $rootScope.currentUser = {};

    /* Users */
    $rootScope.users = [];

    /* Customers */
    $rootScope.customers = [];

    /* Permissions */
    $rootScope.permissions = [{
      label: 'Load Card',
      name: 'load_card',
      selected: false,
      group: 'Cards'
    }, {
      label: "New Registration",
      name: 'new_registration',
      selected: false,
      group: 'Cards'
    }, {
      label: 'Refund',
      name: 'refund',
      selected: false,
      group: 'Cards'
    }, {
      label: 'Allow Billing',
      name: 'allow_billing',
      selected: false,
      group: 'Billing'
    }, {
      label: 'Allow Offer',
      name: 'allow_offer',
      selected: false,
      group: 'Billing'
    }, {
      label: 'Allow Print KOT',
      name: 'allow_print_kot',
      selected: false,
      group: 'Billing'
    }, {
      label: 'View Open Bills',
      name: 'view_open_bills',
      selected: false,
      group: 'Billing'
    }, {
      label: 'View Past Bills',
      name: 'view_past_bills',
      selected: false,
      group: 'Billing'
    }, {
      label: 'Edit Past Bills',
      name: 'edit_past_bills',
      selected: false,
      group: 'Billing'
    },
      {
        label: 'Allow Settle Bills',
        name: 'settle_bill',
        selected: false,
        group: 'Billing'
      }, {
        label: 'Accounts Management',
        name: 'accounts_management',
        selected: false,
        group: 'Billing'
      }, {
        label: 'Show Menu Item',
        name: 'show_menu_item',
        selected: false,
        group: 'Menu'
      },
      {
        label: 'Add Menu Item',
        name: 'add_menu_item',
        selected: false,
        group: 'Menu'
      }, {
        label: 'Edit Menu Item',
        name: 'edit_menu_item',
        selected: false,
        group: 'Menu'
      }, {
        label: 'Delete Menu Item',
        name: 'delete_menu_item',
        selected: false,
        group: 'Menu'
      }, {
        label: 'View Customer Details',
        name: 'view_customer_details',
        selected: false,
        group: 'CRM'
      }, {
        label: 'Edit Customer Details',
        name: 'edit_customer_details',
        selected: false,
        group: 'CRM'
      }, {
        label: 'Delete Customer Details',
        name: 'delete_customer_details',
        selected: false,
        group: 'CRM'
      }, {
        label: 'General Settings',
        name: 'genenral_settings',
        selected: false,
        group: 'Settings'
      },
      {
        label: 'SMS Settings',
        name: 'SMS_settings',
        selected: false,
        group: 'Settings'
      },
      {
        label: 'Taxation Settings',
        name: 'taxation_settings',
        selected: false,
        group: 'Settings'
      }, {
        label: 'Card Settings',
        name: 'card_settings',
        selected: false,
        group: 'Settings'
      }, {
        label: 'Offer Settings',
        name: 'offer_settings',
        selected: false,
        group: 'Settings'
      }, {
        label: 'Print Station Settings',
        name: 'print_station_settings',
        selected: false,
        group: 'Settings'
      }, {
        label: 'View Invoice Details',
        name: 'view_invoice_details',
        selected: false,
        group: 'Reports'
      }, {
        label: 'View Sale By Item Category',
        name: 'view_sale_item_category',
        selected: false,
        group: 'Reports'
      },
      {
        label: 'View Items Detail',
        name: 'view_sale_items',
        selected: false,
        group: 'Reports'
      },
      {
        label: 'View Tax Summary',
        name: 'view_tax_summary',
        selected: false,
        group: 'Reports'
      }, {
        label: 'View Tab User Waiter',
        name: 'view_tab_user_waiter',
        selected: false,
        group: 'Reports'
      },
      {
        label: 'View Kot Detail',
        name: 'view_kot_detail',
        selected: false,
        group: 'Reports'
      }, {
        label: 'View Kot Delete History',
        name: 'view_kot_delete_history',
        selected: false,
        group: 'Reports'
      }, {
        label: 'View Avarage Bill',
        name: 'view_average_bill',
        selected: false,
        group: 'Reports'
      }, {
        label: 'View Item Wise Enterprise',
        name: 'view_item_wise_enterprise',
        selected: false,
        group: 'Reports'
      }, {
        label: 'Create User',
        name: 'create_user',
        selected: false,
        group: 'Users'
      }, {
        label: 'View Users',
        name: 'view_users',
        selected: false,
        group: 'Users'
      }, {
        label: 'Edit User',
        name: 'edit_user',
        selected: false,
        group: 'Users'
      }, {
        label: 'Delete User',
        name: 'delete_user',
        selected: false,
        group: 'Users'
      }, {
        label: 'Create expense category',
        name: 'create_expense_category',
        selected: false,
        group: 'Expenses'
      }, {
        label: 'Feed in expenses ',
        name: 'create_expense_entry',
        selected: false,
        group: 'Expenses'
      }, {
        label: 'Edit expenses',
        name: 'edit_expense_category',
        selected: false,
        group: 'Expenses'
      },
      {
        label: 'Expense Report',
        name: 'show_expense_category',
        selected: false,
        group: 'Expenses'
      },
      {
        label: 'Show Offers',
        name: 'show_offer',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Create Offers',
        name: 'create_offer',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Edit Offers',
        name: 'edit_offer',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Delete Offers',
        name: 'delete_offer',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Show Datelists ',
        name: 'show_datelist',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Create Datelists ',
        name: 'create_datelist',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Edit Datelists',
        name: 'edit_datelist',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Delete Datelist',
        name: 'delete_datelist',
        selected: false,
        group: 'Offers'
      },

      {
        label: 'Show Codes',
        name: 'show_codes',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Create Codes',
        name: 'create_codes',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Edit Codes',
        name: 'edit_codes',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Delete Codes',
        name: 'delete_codes',
        selected: false,
        group: 'Offers'
      },
      {
        label: 'Show Offline Sales',
        name: 'show_offline_sales',
        selected: false,
        group: 'Offline'
      },
      {
        label: 'Store Management',
        name: 'store_management',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'Stock Entry',
        name: 'store_entry',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'Stock Sale',
        name: 'store_sale',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'Physical Stock',
        name: 'physical_stock',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'Allow Back Date Entry',
        name: 'allow_back_date_entry',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'Edit Entry',
        name: 'edit_entry',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'Supply Stock',
        name: 'supply_stock',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'Stock Settings',
        name: 'stock_settings',
        selected: false,
        group: 'Stock'
      },
      {
        label: 'View Stock Report',
        name: 'view_stock_report',
        selected: false,
        group: 'Stock Report'
      },
      {
        label: 'View Detailed Recipe Report',
        name: 'view_detailed_recipe_report',
        selected: false,
        group: 'Stock Report'
      },
      {
        label: 'View Stock Entry Report',
        name: 'view_stock_entry_report',
        selected: false,
        group: 'Stock Report'
      },
      {
        label: 'View Consumption Report',
        name: 'view_consumption_report',
        selected: false,
        group: 'Stock Report'
      },
      {
        label: 'View Purchase Detail Report',
        name: 'view_purchase_detail_report',
        selected: false,
        group: 'Stock Report'
      },
      {
        label: 'View Purchase Summary Report',
        name: 'view_purchase_summary_report',
        selected: false,
        group: 'Stock Report'
      }
    ];

    /* Settings */
    var _t = moment().toArray();
    var _defaulCutoff = new Date(_t[0], _t[1], _t[2], 2, 0, 0);

   $rootScope.settings = [
      {
        label: 'Settle Bill on Print',
        name: 'settle_bill_on_print',
        tabs: [{
          tabLabel: "Table",
          tabType: "table",
          selected: false
        }, {
          tabLabel: "Take Out",
          tabType: "takeout",
          selected: false
        }, {
          tabLabel: "Delivery",
          tabType: "delivery",
          selected: false
        }],
        group: 'Billing',
        fieldType: 'checkbox',
        selected: false
      },
   {
      label: 'Sort Item by Item Number',
      name: 'orderby_itemnumber',
      group: 'Billing',
      fieldType: 'checkbox',
      selected: true
    }, {
      label: 'Apply Automatic Discounts',
      name: 'apply_automatic_discounts',
      selected: false,
      fieldType: 'checkbox',
      group: 'Billing'
    }, {
      label: 'Cancel Item after Print Bill',
      name: 'cancel_item_after_print_bill',
      selected: false,
      fieldType: 'checkbox',
      group: 'Bill Settings'
    }, {
      label: 'Re-print Bill',
      name: 'reprint_bill',
      selected: false,
      fieldType: 'checkbox',
      group: 'Bill Settings'
    }, {
      label: 'Enable Sweet Shop',
      name: 'enable_sweet_shop',
      selected: false,
      fieldType: 'checkbox',
      group: 'Billing'
    }, {
      label: 'Category-Wise split Bill',
      name: 'category_split_bill',
      selected: false,
      fieldType: 'checkbox',
      group: 'Bill Settings'
    }, {
      label: 'Category-Wise Print Kot',
      name: 'category_print_kot',
      selected: false,
      group: 'KOT Settings'
    }, {
      label: 'Enable PassCode to Apply Discount',
      name: 'enable_passcode_apply_discount',
      selected: true,
      fieldType: 'checkbox',
      group: 'Passcode'
    }, {
      label: 'Enable PassCode to Void Bill',
      name: 'enable_passcode_void_bill',
      selected: true,
      fieldType: 'checkbox',
      group: 'Passcode'
    }, {
      label: 'Enable Comment to Void Bill',
      name: 'enable_comment_void_bill',
      selected: true,
      group: 'Billing'
    }, {
      label: 'Enable CRM Alert on Bill',
      name: 'enable_crm_alert',
      tabs: [{
        tabLabel: "Table",
        tabType: "table",
        selected: false
      }, {
        tabLabel: "Take Out",
        tabType: "takeout",
        selected: false
      }, {
        tabLabel: "Delivery",
        tabType: "delivery",
        selected: false
      }],
      selected: false,
      fieldType: 'checkbox',
      group: 'Billing'
    }, {
      label: 'Enable CRM Mandatory on Bill',
      name: 'enable_crm_mandatory',
      tabs: [{
        tabLabel: "Table",
        tabType: "table",
        selected: false
      }, {
        tabLabel: "Take Out",
        tabType: "takeout",
        selected: false
      }, {
        tabLabel: "Delivery",
        tabType: "delivery",
        selected: false
      }],
      selected: false,
      fieldType: 'checkbox',
      group: 'Billing'
    },

      {
        label: 'Manager Permission On',
        name: 'c_m_p',
        tabs: [{
          tabLabel: "Bill Transfer",
          tabType: "bTran",
          selected: false
        }, {
          tabLabel: "Kot Transfer",
          tabType: "kTran",
          selected: false
        }, {
          tabLabel: "Item Transfer",
          tabType: "iTran",
          selected: false
        },{
          tabLabel: "Void Kot",
          tabType: "vKot",
          selected: false
        },{
          tabLabel: "Void/Change Item Qty ",
          tabType: "vItem",
          selected: false
        }  ],
        selected: false,
        fieldType: 'checkbox',
        group: 'Billing'
      },
      {
        label: 'Enable Comment on KOT Print',
        name: 'enable_comment_kot',
        selected: false,
        fieldType: 'checkbox',
        group: 'KOT Settings'
      }, {
        label: 'Enable Manual KOT',
        name: 'enable_manual_kot',
        selected: true,
        fieldType: 'checkbox',
        group: 'KOT Settings'
      }, {
        label: 'Minimum Order on Delivery',
        name: 'minimum_order_delivery',
        value: 200,
        selected: true,
        fieldType: 'text',
        group: 'Billing'
      }, {
        label: 'Delivery Boy Selection Required',
        name: 'delivery_boy_selection',
        selected: false,
        fieldType: 'checkbox',
        group: 'Billing'
      }, {
        label: 'Waiter Selection Required',
        name: 'waiter_selection',
        selected: false,
        fieldType: 'checkbox',
        group: 'Billing'
      }, {
        label: 'Day Sale Cutoff Time',
        name: 'day_sale_cutoff',
        value: _defaulCutoff.toISOString(),
        fieldType: 'timepicker',
        selected: true,
        group: 'Billing'
      }, {
        label: 'Header on Bill',
        name: 'header_bill',
        value: '',
        selected: true,
        fieldType: 'textarea',
        group: 'Bill Settings'
      }, {
        label: 'Footer on Bill',
        name: 'footer_bill',
        value: 'Thank you, visit again!',
        selected: true,
        fieldType: 'textarea',
        group: 'Bill Settings'
      }, {
        label: 'Enable Settlement',
        name: 'enable_settlement',
        tabs: [{
          tabLabel: "Table",
          tabType: "table",
          selected: false
        }, {
          tabLabel: "Take Out",
          tabType: "takeout",
          selected: false
        }, {
          tabLabel: "Delivery",
          tabType: "delivery",
          selected: false
        }],
        selected: true,
        fieldType: 'checkbox',
        group: 'Billing'
      },
      {
        label: 'Settle By Manager',
        name: 'settle_by_manager',
        tabs: [{
          tabLabel: "Table",
          tabType: "table",
          selected: false
        }, {
          tabLabel: "Take Out",
          tabType: "takeout",
          selected: false
        }, {
          tabLabel: "Delivery",
          tabType: "delivery",
          selected: false
        }],
        selected: false,
        fieldType: 'checkbox',
        group: 'Billing'
      },
      {
        label: 'Settle By Cashier',
        name: 'settle_by_cashier',
        tabs: [{
          tabLabel: "Table",
          tabType: "table",
          selected: false
        }, {
          tabLabel: "Take Out",
          tabType: "takeout",
          selected: false
        }, {
          tabLabel: "Delivery",
          tabType: "delivery",
          selected: false
        }],
        selected: false,
        fieldType: 'checkbox',
        group: 'Billing'
      },

      {
        label: 'Print Server',
        name: 'print_server',
        group: 'Print Server',
        selected: true,
        fieldType: 'text',
        value: 'localhost'
      },
      {
        label: 'Instance Server',
        name: 'instance_server',
        group: 'Print Server',
        selected: true,
        fieldType: 'text',
        value: 'http://jdd.posist.org'
      },
      {
        label: 'Disable Kot',
        name: 'dis_kot',
        group: 'KOT Settings',
        fieldType: 'checkbox',
        selected: false
      }, {
        label: 'Print kot with bill',
        name: 'print_kot_bill',
        group: 'KOT Settings',
        fieldType: 'checkbox',
        selected: false
      }, {
        label: 'Kot Mandatory',
        name: 'kot_mandatory',
        group: 'Billing',
        fieldType: 'checkbox',
        selected: false
      }, {
        label: 'Enable Covers',
        name: 'enable_covers',
        tabs: [{
          tabLabel: "Table",
          tabType: "table",
          selected: false
        }, {
          tabLabel: "Take Out",
          tabType: "takeout",
          selected: false
        }, {
          tabLabel: "Delivery",
          tabType: "delivery",
          selected: false
        }],
        group: 'Billing',
        fieldType: 'checkbox',
        selected: false
      }, {
        label: 'Bill Print Copies',
        name: 'bill_print_copies',
        group: 'Bill Settings',
        selected: true,
        fieldType: 'text',
        value: '2'
      }, {
        label: 'KOT Print Copies',
        name: 'kot_print_copies',
        group: 'KOT Settings',
        selected: true,
        fieldType: 'text',
        value: '2'
      }, {
        label: 'Station wise print',
        name: 'station_wise_print',
        group: 'Print Server',
        selected: false,
        fieldType: 'checkbox'

      }, {
        label: 'Activate Inclusive Tax',
        name: 'inclusive_tax',
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Bill wise offer taxation fix',
        name: 'bill_wise_offer_tax',
        group: 'Other',
        selected: true,
        fieldType: 'checkbox'
      },
      {
        label: 'Reset SerialNumber Daily',
        name: 'reset_serial_daily',
        group: 'Other',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Enable Pass Code',
        name: 'enable_passcode',
        tabs: [{
          tabLabel: "Re-Print Bill",
          tabType: "reprintbill",
          selected: false
        }, {
          tabLabel: "Re-Print Kot",
          tabType: "reprintkot",
          selected: false
        }],
        selected: false,
        fieldType: 'checkbox',
        group: 'Passcode'
      },
      {
        label: 'Enable Advance Booking',
        name: 'enable_advance_booking',
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Enable e-POS Epson TM-i Print',
        name: 'enable_epos_print',
        group: 'Bill Settings',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Enable feedback',
        name: 'enable_feedback',
        group: 'Other',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Passcode on table',
        name: 'passcode_table',
        group: 'Passcode',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Send email of bill  ',
        name: 'send_email_customer',
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Send SMS of bill  ',
        name: 'send_sms_customer',
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Bill SMS Outlet Name',
        name: 'bill_sms_outlet',
        group: 'Billing',
        selected: true,
        fieldType: 'text'

      },
      {
        label: 'Activate virtual keyboard',
        name: 'activate_keyboard',
        group: 'Other',
        selected: false,
        fieldType: 'checkbox'
      }
      ,
      {
        label: 'Enable Notification',
        name: 'enable_notification',
        group: 'Other',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Disable Bill RoundOff',
        name: 'disable_roundoff',
        group: 'Other',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Enable Card',
        name: 'enable_card',
        group: 'Prepaid Card Settings',
        selected: false,
        fieldType: 'checkbox'
      },
      {label: 'Card Security Amount',
        name: 'card_security_amount',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Prepaid Card Settings'},
        {label: 'Card Entry Fee',
        name: 'card_entry_fee',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Prepaid Card Settings'},
      {label: 'Ruplee MerchantId',
        name: 'Ruplee MerchantId',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Ruplee Setting'},
      {label: 'Binge Username',
        name: 'binge_username',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Binge Setting'},
      {label: 'Binge Password',
        name: 'binge_password',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Binge Setting'
      },{label: 'paytm_merchant_guid',
        name: 'Paytm Merchant Guid',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Paytm Setting'},
      {label: 'paytm_secret_key',
        name: 'Paytm Secret Key',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Paytm Setting'},
      {
        label: 'paytm_industry_id',                // added paytm industry id
        name: 'Paytm Industry Id',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Paytm Setting'
      },
      {                                                   //added for technopurple
        label: 'Enable Delivery Boy App',
        name: 'enable_delivery_boy_app',
        selected: false,
        fieldType: 'checkbox',
        group: 'Delivery App'
      },
      {
        label:'My Operator Access Token',
        name:'my_operator_access_token',
        selected:false,
        value:"",
        fieldType:'text',
        group:'My Operator'
      },{
        label: 'Print Bill By Cashier Only',
        name: 'print_bill_cashier',
        group: 'Bill Settings',
        selected: false,
        fieldType: 'checkbox'
      },{
        label: 'No punch Of Item after Print',
        name: 'no_punch_item_print',
        group: 'Bill Settings',
        selected: false,
        fieldType: 'checkbox'
      },{
        label: 'Comment on re-print Bill',
        name: 'comment_re_print',
        group: 'Bill Settings',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Disable Transfer of',
        name: 'disable_transfer',
        tabs: [{
          tabLabel: "Bill",
          tabType: "tranbill",
          selected: false
        }, {
          tabLabel: "Kot",
          tabType: "trankot",
          selected: false
        }, {
          tabLabel: "Item",
          tabType: "tranitem",
          selected: false
        }],
        selected: false,
        fieldType: 'checkbox',
        group: 'Other'
      },
      {
        label: 'paytm_industry_id',                // added paytm industry id
        name: 'Paytm Industry Id',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Billing'
      },
      {
        label: 'Zomato Trace Username',
        name: 'zomato_trace_username',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Delivery App'
      },
      {
        label: 'Zomato Trace ApiKey',
        name: 'zomato_trace_apikey',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Delivery App'
      },
      {
        name: 'remove_tax_manually',
        label: 'Remove Taxes Manually',           //added for taxation
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        name: 'Inresto MerchantId',
        label: 'Inresto Merchant Id',           //added for inresto
        group: 'InResto Setting',
        selected: false,
        fieldType: 'text' ,
        value:""
      },
      {
        name: 'delhivery_auth_token',
        label: 'Delhivery auth token',           //added for delhivery
        group: 'Delivery Partner',
        selected: false,
        fieldType: 'text' ,
        value:""
      },
      {
      label: 'Hide Discount Amount Field',
      name: 'hide_discount_amount',
      fieldType: 'checkbox',
      selected: false,
      value: false,
      group: 'Other'
      },
      {
         name: 'enable_express_offer',
        label: 'Enable bill express offer',           //added for taxation
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        name: 'enable_password_express',
        label: 'Password for express offer',           //added for taxation
        group: 'Passcode',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        name: 'independent_instance',
        label: 'Independent Instance',           //added for taxation
        group: 'Other',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        name: 'n_o_tables',
        label: 'No. Of Tables',           //added for delhivery
        group: 'Other',
        selected: false,
        fieldType: 'text' ,
        value:""
      },
       {
        name: 'bluetooth_printer',
        label: 'Enable Bluetooth Printer',           //added for Bluetooth Printer
        group: 'Bill Settings',
        selected: false,
        fieldType: 'checkbox'
      }, {
        name: 'dynamic_comment',
        label: 'Dynamic Comment', //added for dynamic Comment
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Cover Mandatory',
        name: 'cover_mandatory',
        tabs: [{
          tabLabel: "Table",
          tabType: "table",
          selected: false
        }, {
          tabLabel: "Take Out",
          tabType: "takeout",
          selected: false
        }, {
          tabLabel: "Delivery",
          tabType: "delivery",
          selected: false
        }],
        selected: true,
        fieldType: 'checkbox',
        group: 'Billing'
      },
      {
        name: 'print_after_settle',
        label: 'Print After Settlement', //added for dynamic Comment
        group: 'Bill Settings',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        label: 'Max no of addons',
        name: 'max_n_addon',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Billing'
      },
      {
        label: 'Max quantity of each addons',
        name: 'max_q_addon',
        value: "",
        selected: false,
        fieldType: 'text',
        group: 'Billing'
      },
      {
        name: 'addon_compulsory',
        label: 'AddOn Compulsory',
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
        {
        label: 'Bar Exchange Ip',
        name: 'bar_exchange_ip',
        group: 'Billing',
        selected: false,
        fieldType: 'text'
      },
      {
        name: 'bill_print_template',
        label: 'Bill Print Template Number',
        group: 'Billing',
        selected: false,
        fieldType: 'text',
        value: "",
      },
      {
        name: 'void_bill_max_time',
        label: 'Stop Modify Bill Before CutOff Time (In Hrs.)',
        group: 'Billing',
        selected: false,
        fieldType: 'text',
        value: "",
      },
      {
        name: 'split_sales_in_tally',
        label: 'Split Sales In Tally',
        group: 'Billing',
        selected: false,
        fieldType: 'checkbox'
      },
      {
        name: 'roadrunnr_client_id',
        label: 'RoadRunnr Client ID',
        group: 'Delivery Partner',
        selected: false,
        fieldType: 'text',
        value: "",
      },
      {
        name: 'roadrunnr_client_secret',
        label: 'RoadRunnr Client Secret',
        group: 'Delivery Partner',
        selected: false,
        fieldType: 'text',
        value: "",
      },
      {
        name: 'latitude',
        label: 'Latitude',
        group: 'Delivery Partner',
        selected: false,
        fieldType: 'text',
        value: "",
      },
      {
        name: 'longitude',
        label: 'Longitude',
        group: 'Delivery Partner',
        selected: false,
        fieldType: 'text',
        value: "",
      },
     {
       name: 'grab_client_id',
       label: 'Grab Client Id',
       group: 'Delivery Partner',
       selected: false,
       fieldType: 'text',
       value: ""
     },
     {
       name: 'grab_public_key',
       label: 'Grab Public Key',
       group: 'Delivery Partner',
       selected: false,
       fieldType: 'text',
       value: ""
     },
     {
       name: 'grab_private_key',
       label: 'Grab Private Key',
       group: 'Delivery Partner',
       selected: false,
       fieldType: 'text',
       value: ""
     },
     {
       name: 'remove_credit_card_details',
       label: 'Remove Credit/Debit Card Details in Settlement',
       group: 'Billing',
       selected:false,
       fieldType: 'checkbox'
     }
    ];



  /* Roles & Permissions - System Defaults */
    $rootScope.roles = [{
      "name": "Waiter",
      "tenant_id": "",
      "created": "",
      "isLocked": true,
      "permissions": [{
        "group": "Billing",
        "selected": false,
        "name": "allow_billing",
        "label": "Allow Billing"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_offer",
        "label": "Allow Offer"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_print_kot",
        "label": "Allow Print KOT"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_open_bills",
        "label": "View Open Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_past_bills",
        "label": "View Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "edit_past_bills",
        "label": "Edit Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "accounts_management",
        "label": "Accounts Management"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "view_customer_details",
        "label": "View Customer Details"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "edit_customer_details",
        "label": "Edit Customer Details"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "delete_customer_details",
        "label": "Delete Customer Details"
      }]
    }, {
      "name": "Delivery",
      "tenant_id": "",
      "created": "",
      "isLocked": true,
      "permissions": [{
        "group": "Billing",
        "selected": false,
        "name": "allow_billing",
        "label": "Allow Billing"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_offer",
        "label": "Allow Offer"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_print_kot",
        "label": "Allow Print KOT"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_open_bills",
        "label": "View Open Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_past_bills",
        "label": "View Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "edit_past_bills",
        "label": "Edit Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "accounts_management",
        "label": "Accounts Management"
      }]
    }, {
      "name": "Cashier",
      "tenant_id": "",
      "created": "",
      "isLocked": true,
      "permissions": [{
        "group": "Billing",
        "selected": false,
        "name": "allow_billing",
        "label": "Allow Billing"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_offer",
        "label": "Allow Offer"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_print_kot",
        "label": "Allow Print KOT"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_open_bills",
        "label": "View Open Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_past_bills",
        "label": "View Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "edit_past_bills",
        "label": "Edit Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "accounts_management",
        "label": "Accounts Management"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "view_customer_details",
        "label": "View Customer Details"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "edit_customer_details",
        "label": "Edit Customer Details"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "delete_customer_details",
        "label": "Delete Customer Details"
      }]
    }, {
      "name": "Manager",
      "tenant_id": "",
      "created": "",
      "isLocked": true,
      "permissions": [{
        "group": "Billing",
        "selected": false,
        "name": "allow_billing",
        "label": "Allow Billing"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_offer",
        "label": "Allow Offer"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "allow_print_kot",
        "label": "Allow Print KOT"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_open_bills",
        "label": "View Open Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "view_past_bills",
        "label": "View Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "edit_past_bills",
        "label": "Edit Past Bills"
      }, {
        "group": "Billing",
        "selected": false,
        "name": "accounts_management",
        "label": "Accounts Management"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "view_customer_details",
        "label": "View Customer Details"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "edit_customer_details",
        "label": "Edit Customer Details"
      }, {
        "group": "CRM",
        "selected": false,
        "name": "delete_customer_details",
        "label": "Delete Customer Details"
      }, {
        "group": "Cards",
        "selected": false,
        "name": "load_card",
        "label": "Load Card"
      }, {
        "group": "Cards",
        "selected": false,
        "name": "new_registration",
        "label": "New Registration"
      }, {
        "group": "Cards",
        "selected": false,
        "name": "refund",
        "label": "Refund"
      }, {
        "group": "Reports",
        "selected": false,
        "name": "view_invoice_details",
        "label": "View Invoice Details"
      }, {
        "group": "Reports",
        "selected": false,
        "name": "view_invoice_item_details",
        "label": "View Invoice Item Details"
      }, {
        "group": "Menu",
        "selected": false,
        "name": "add_menu_item",
        "label": "Add Menu Item"
      }, {
        "group": "Menu",
        "selected": false,
        "name": "edit_menu_item",
        "label": "Edit Menu Item"
      }, {
        "group": "Menu",
        "selected": false,
        "name": "delete_menu_item",
        "label": "Delete Menu Item"
      }, {
        "group": "Settings",
        "selected": false,
        "name": "genenral_settings",
        "label": "General Settings"
      }, {
        "group": "Settings",
        "selected": false,
        "name": "taxation_settings",
        "label": "Taxation Settings"
      }, {
        "group": "Settings",
        "selected": false,
        "name": "card_settings",
        "label": "Card Settings"
      }, {
        "group": "Settings",
        "selected": false,
        "name": "offer_settings",
        "label": "Offer Settings"
      }, {
        "group": "Settings",
        "selected": false,
        "name": "print_station_settings",
        "label": "Print Station Settings"
      }]
    }];

// $rootScope.iosocket = io.connect("http://jdd.posist.org");
    /* $rootScope.tables=function(){
     var tablestemp=[];
     for(var i=0;i<100;i++){
     var tableOb={id:(i+1),name:('Table '+(i+1))};
     tablestemp.push(tableOb);
     }
     console.log(tablestemp);
     return tablestemp;
     };*/

//$rootScope.url="http://jdd.posist.org";
    var instanceSetting = Utils.getSettingValue('instance_server', localStorageService.get('settings'));
    $rootScope.url = instanceSetting == null || instanceSetting == undefined ? 'http://jdd.posist.org' : instanceSetting;
    if (!(localStorageService.get('instanceId') == null || localStorageService.get('instanceId') == undefined)) {
      if (localStorageService.get('instanceId').id != undefined) {
        //alert(Utils.hasSetting('independent_instance', localStorageService.get('settings')));
        if(Utils.hasSetting('independent_instance', localStorageService.get('settings'))){
          $rootScope.isInstance = false;
        }else{
          $rootScope.isInstance = true;
        }
      } else {
        $rootScope.isInstance = false;
      }
    } else {
      $rootScope.isInstance = false;
    }


    /* Connect to WebSQL */
    $rootScope.db = $webSql.openDatabase('posistApp', '1.0', 'Posist Offline DB', 50 * 1024 * 1024);

    /* Bills Table */
    /*  $rootScope.db.dropTable('bills');*/
    $rootScope.db.createTable('bills', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "serialNumber": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": "null"
      },
      "daySerialNumber": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": "null"
      },
      "billNumber": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": "null"
      },
      "splitNumber": {
        "type": "INTEGER",
        "null": "NULL",
        "default": "null"
      },
      "created": {
        "type": "REAL",
        "null": "NOT NULL"
      },
      "billId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "billData": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "isVoid": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "false"
      },
      "isSettled": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "false"
      },
      "isPrinted": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "false"
      },
      "tab": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "tabId": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "totalBill": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": 0
      },
      "totalTax": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": 0
      },
      "deployment_id": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "tenant_id": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "isSynced": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": 0 // 0 - NO ~ 1 - YES
      },
      "syncedOn": {
        "type": "TIMESTAMP",
        "null": "NOT NULL",
        "default": "null"
      },
      "customer": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "customer_address": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "delivery_time": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "closeTime": {
        "type": "REAL",
        "null": "NULL",
        "default": null
      },
      "kotNumber": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": 0
      }

    });

    /* Customers Table */
    /* $rootScope.db.dropTable('customers');*/
    $rootScope.db.createTable('customers', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "customerId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "customerData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });

    /* Items Table */
    /*   $rootScope.db.dropTable('items');*/
    $rootScope.db.createTable('items', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "itemId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "itemData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });
    /* $rootScope.db.dropTable('tabs');*/
    $rootScope.db.createTable('tabs', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "tabId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "tabData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });

    /* Users Table */
    /*   $rootScope.db.dropTable('users');*/
    $rootScope.db.createTable('users', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "userId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "userData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });
    $rootScope.db.createTable('offers', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "offerId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "offerData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });

    /*Stock stores Table */
    /*$rootScope.db.dropTable('stores');*/
    $rootScope.db.createTable('stores', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "storeId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "storeData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });

    /*Stock Receiver Table */
    /*$rootScope.db.dropTable('receivers');*/
    $rootScope.db.createTable('receivers', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "receiverId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "receiverData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });

    /*Stock Vendors Table */
    /*$rootScope.db.dropTable('vendors');*/
    $rootScope.db.createTable('vendors', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "vendorId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "vendorData": {
        "type": "TEXT",
        "null": "NOT NULL"
      }
    });

    /*Stock Tranasction Table*/
    $rootScope.db.createTable('stockTransaction', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "transactionId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "daySerialNumber": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": "null"
      },
      "transactionNumber": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": "null"
      },
      "tempTransactionNumber": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": "null"
      },
      "user": {
        "type": "TEXT",
        "null": "NOT NULL"
      },

      "store": {
        "type": "TEXT",
        "null": "NOT NULL"
      },

      "toStore": {
        "type": "TEXT",
        "null": "NOT NULL"
      },

      "category": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "vendor": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "receiver": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "transactionType": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "discountType": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "discount": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "serviceChargeType": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "serviceCharge": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "cartage": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "vatType": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "vat": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "payment": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "wastageItemType": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "wastageEntryType": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "heading": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "created": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "items": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "templateName": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "deployment_id": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "tenant_id": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "isSynced": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": 0 // 0 - NO ~ 1 - YES
      },
      "syncedOn": {
        "type": "TIMESTAMP"/*,
         "null": "NULL",
         "default":new Date()*/
      }
    });

    $rootScope.db.createTable('stocktransactions', {
      "id": {
        "type": "INTEGER",
        "null": "NOT NULL", // default is "NULL" (if not defined)
        "primary": true, // primary
        "auto_increment": true // auto increment
      },
      "transactionId": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "transactionType": {
        "type": "INTEGER",
        "null": "NOT NULL"
      },
      "daySerialNumber": {
        "type": "INTEGER",
        "default": "null"
      },
      "transactionNumber": {
        "type": "INTEGER",
        "default": "null"
      },
      "transactionData": {
        "type": "TEXT",
        "null": "NOT NULL"
      },
      "deployment_id": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "tenant_id": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "isOpen": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": 0 // 0 - NO ~ 1 - YES
      },
      "created": {
        "type": "TEXT",
        "null": "NOT NULL",
        "default": "null"
      },
      "isSynced": {
        "type": "INTEGER",
        "null": "NOT NULL",
        "default": 0 // 0 - NO ~ 1 - YES
      },
      "syncedOn": {
        "type": "TIMESTAMP",
        "null": "NULL",
        "default": "null"
      }
    });
    /* Global Loader UI Flag */
    /*$rootScope.showLoaderOverlay = false;*/

    var onlineOrderQuery = "Create Table If Not Exists OnlineBills(OnlineBillId TEXT PRIMARY KEY ON CONFLICT REPLACE,OnlineBillNumber TEXT,TimeStamp REAL,OnlineBillsData,OrderTime REAL,TabId TEXT,DownLoadStatus TEXT) ";
    $rootScope.db.selectDynamic(onlineOrderQuery, function () {
    });
    var advanceOrderQuery = "Create Table If Not Exists AdvanceBookings(AdvanceBillId TEXT PRIMARY KEY ON CONFLICT REPLACE,TimeStamp REAL,AdvanceBillsData,OrderTime REAL,TabId TEXT,DownLoadStatus TEXT) ";
    $rootScope.db.selectDynamic(advanceOrderQuery, function () {
    });

// Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

      Auth.isLoggedInAsync(function (loggedIn) {

        if (toState.authenticate && !loggedIn) {
          $location.path('/login');
        }

      });
      $rootScope.alphaKeyboard = {
        deadkeysOn: false,
        forcePosition: 'left',
        customClass: 'hideKeyboard'
      };
    $rootScope.alphaKeyboardLeft = {
        deadkeysOn: false,
        forcePosition: 'left',
        customClass: 'hideKeyboard'
      };
      $rootScope.alphaKeyboardTop = {
        deadkeysOn: false,
        forcePosition: 'top',

        customClass: 'hideKeyboard'
      };


      $rootScope.numKeyboard = {
        deadkeysOn: false,
        kt: 'Numerico',
        customClass: 'hideKeyboard'
      };

      if(Utils.hasSetting('disable_roundoff', localStorageService.get('settings'))) {
        $rootScope.disable_roundoff=true;
        //alert($rootScope.disable_roundoff);
      }else{
        $rootScope.disable_roundoff=false;
        //alert($rootScope.disable_roundoff);
      }
    });

  }])
;
