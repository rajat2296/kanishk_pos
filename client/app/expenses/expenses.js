'use strict';

angular.module('posistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('expenses', {
        url: '/expenses',
        templateUrl: 'app/expenses/expenses.html',
        controller: 'ExpensesCtrl',
        resolve: {
            currentUser: ['$state', '$stateParams', 'Auth','Utils','growl','$location','localStorageService',
                function ($state, $stateParams, Auth,Utils,growl,$location,localStorageService) {
                    return Auth.getCurrentUser().$promise.then(function (user) {
                        if(localStorageService.get('deployment_id')==undefined){
                            growl.error('default deployment not set  !!',{ttl:3000});
                            Auth.logout();
                            $location.path('/login');
                            return false;
                        }
                        var flag=false;
                        if (user.role === 'user') {
                            if (Utils.hasUserPermission(user.selectedPermissions,'Expenses')) {
                                flag = true;
                            }
                            if(!flag){
                                growl.error('No permission to view expenses. !!',{ttl:3000});
                                Auth.logout();
                                $location.path('/login');
                                return false;
                            }
                        }
                        return user;
                    });
                }]
        }
      });
  });