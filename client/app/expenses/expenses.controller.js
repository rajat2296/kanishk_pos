'use strict';

angular.module('posistApp')
  .controller('ExpensesCtrl', ['$rootScope', '$q', '$scope', '$filter', 'currentUser', 'expense', 'expenseCategories', '$resource', 'growl', 'localStorageService','Utils','intercom',function ($rootScope, $q, $scope, $filter, currentUser, expense, expenseCategories, $resource, growl, localStorageService,Utils,intercom) {

    $scope.currentDate = new Date();
    $scope.currentDate.setHours(0, 0, 0, 0);


    var d = new Date();
    d.setHours(0, 0, 0, 0);
    $scope.yesterDate = d.setDate(d.getDate() - 1);
      currentUser.deployment_id = localStorageService.get('deployment_id');

      $scope.isPermission=function(permissions){
        // console.log(currentUser);
        var flag = false;
        if (currentUser.role === 'user') {
          // flag= true;
          //   } else {
          var permissionArr = permissions.split(',');
          // console.log(permissionArr);
          _.forEach(permissionArr, function (permname) {
            if (Utils.hasPermission(currentUser.selectedPermissions, permname)) {
              flag = true;
            }
          })
        }else{
          flag=true;
        }
        return flag;
      }
    /*********** Category Master ***********/

    expenseCategories.get({
      tenant_id: currentUser.tenant_id,
      deployment_id: currentUser.deployment_id
    }).$promise.then(function (categories) {
      $scope.categories = categories;
      //      console.log(categories);
    });

    $scope.addEditCategory = function () {
      if (!$scope.category._id) {
        expenseCategories.save({}, $scope.category, function (cat) {
          $scope.categories.push(cat);
          $scope.clearCategory();
        }, function (err) {
          angular.forEach(err.data.errors, function (error, field) {
             growl.error("Error: "+error.message, {ttl: 3000});
          });

        });
      } else {
        expenseCategories.update({
          _id: $scope.category._id
        }, $scope.category, function (res) {
          var index = _.findIndex($scope.categories, {
            _id: $scope.category._id
          });
          $scope.categories.splice(index, 1, res);
          $scope.clearCategory();
        });
      }
    };

    $scope.openInEdit = function (category) {
      var obj = angular.copy(category);
      $scope.category = obj;
    };

    $scope.deleteCategory = function (category, index) {
      expenseCategories.delete({}, {
        _id: category._id
      }, function (data) {
        $scope.categories.splice(index, 1);
      });
    };

    ($scope.clearCategory = function () {
      $scope.category = {
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id,
        name: '',
        description: ''
      };
    })();


    /*********** Expense Entry ***********/

    ($scope.clearExpenseBill = function () {
      $scope.expenseBill = {
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id,
        billNo: '',
        billDate: $scope.currentDate,
        expenses: [],
        total: 0
      };
    })();

    ($scope.clearExpense = function () {
      $scope.expense = {
        category: '',
        vendor: '',
        item: '',
        rate: 0,
        qty: 0,
        amount: 0
      };
    })();

    $scope.addExpenseToBill = function () {
      $scope.expense.amount = $scope.expense.rate * $scope.expense.qty;
      $scope.expenseBill.expenses.push($scope.expense);
      $scope.clearExpense();
      $scope.submitDisable=false;
    };

    $scope.editExpense = function (index) {
      $scope.expense = $scope.expenseBill.expenses.splice(index, 1)[0];
    };

    $scope.deleteExpense = function (index) {
      $scope.expenseBill.expenses.splice(index, 1);
    };
$scope.submitDisable=false;
    $scope.submitExpensesBill = function () {
      $scope.submitDisable=true;
      angular.forEach($scope.expenseBill.expenses, function (expense, index) {
        $scope.expenseBill.total += (expense.rate * expense.qty);
      });
      expense.save({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id
      }, $scope.expenseBill, function (bill) {
        //        console.log(bill);
        $scope.clearExpenseBill();
        $scope.getCurrentBillNumber();
        $scope.clearExpense();

      });
    };

    ($scope.getCurrentBillNumber = function () {
      expense.getExpenseBillNo({
        tenant_id: currentUser.tenant_id,
        deployment_id: currentUser.deployment_id,
        billDate: $scope.expenseBill.billDate
      }).$promise.then(function (response) {
        $scope.expenseBill.billNo = response[0].billNo + 1;
        //        console.log(response[0].billNo);
      });
    })();


    /*********** List Expenses ***********/
    $scope.disableButton=false;
    $scope.buttonChange=function(){
        console.log("called");
        $scope.disableButton=false;
    };
    ($scope.clearReportSelection = function () {
        $scope.report = {
        type: '',
        totalExpenses: 0
      };
      $scope.disableButton=false;
    })();

    var reportingUtils = {
      'bill': {
          fn: expense.getBillReportBetweenDates,
          id: 'billWiseTable'
      },
      'category': {
          fn: expense.getCategoryReportBetweenDates,
          id: 'catItemWiseTable'
      },
      'item':  {
          fn: expense.getItemReportBetweenDates,
          id: 'catItemWiseTable'
      },
    };

    $scope.expenseStartDate = {
      value: $scope.yesterDate,
      open: false
    };

    $scope.expenseEndDate = {
      value: $scope.currentDate,
      open: false
    };

    $scope.$watch('report.type', function (change) {
      if (change) {
        $scope.expenseBills = [];
        $scope.report.totalExpenses = 0;
        $scope.disableButton=false;
      }
    });

$scope.buttonChange=function(){
  console.log("called");
  $scope.disableButton=false;
}

    $scope.generateReport = function () {
      $scope.disableButton=true;
      $scope.expenseBills = [];
      $scope.report.totalExpenses = 0;
      var start = new Date($scope.expenseStartDate.value),
          end = new Date($scope.expenseEndDate.value);
      if (start.getTime() < end.getTime()) {
        if ($scope.report.type) {
          reportingUtils[$scope.report.type].fn({
            startDate: start,
            endDate: end,
            tenant_id: currentUser.tenant_id,
            deployment_id: currentUser.deployment_id
          }).$promise.then(function (response) {
//           console.log(response);
            $scope.expenseBills = response;
           
          });
        } else {
          growl.error('Select Type of Report!', {
            ttl: 3000
          });
          $scope.disableButton=false;
        }
      } else {
        growl.error('Start Date should be lesser than End Date!', {
          ttl: 3000
        });
        $scope.disableButton=false;
      }
    };

    $scope.excelExportReport = function () {
      window.open('data:application/vnd.ms-excel,' + encodeURIComponent(document.getElementById(reportingUtils[$scope.report.type].id).outerHTML));
    };

    intercom.registerEvent('Expenses');

  }]);
