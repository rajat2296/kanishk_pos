'use strict';
var _ = require('lodash');
var async = require('async');
var path = require('path');
var moment = require('moment');
var Utils=require('./server/api/Utils/utils');
var mongoose=require('mongoose');
var fs = require('fs');
var reportUtils = require('./server/api/reportFunction/reportFunc.js')

var exportToCsvForCategory = function(bills){
  var _allItemsFor = [];
  _.forEach(bills,function(bill){
    var _allTaxes = [];
    var _allItems = [];
    if (bill.isVoid != true) {
      _.forEach(bill._kots, function (kot) {
        if (!kot.isVoid) {
          bill._items = []
          for(var it in kot.items){
            if(_.has(kot.items[it],'addOns')) {
              _.forEach(kot.items[it].addOns,function(addon){
                _allItems.push(addon);
              })
            }
          }
          _.forEach(kot.items, function (item) {
            _allItems.push(item);
          });
        }
      });
      _.chain(_allItems).groupBy('_id').map(function (i) {
        var _itemAmount = 0,
          _itemQ = 0,
          _itemD = 0,
          _compItems = 0,
          _nonCompItems = 0,
          _compDiscount = 0,
          _nonCompDiscount = 0;
        var _itemBillDiscount=0;
        _.forEach(i, function (it) {
          _itemAmount += Utils.roundNumber(parseFloat(it.subtotal),2);
          _itemQ += parseFloat(it.quantity);
          /* Discounts */
          var d = 0;
          if (_.has(it, 'isDiscounted') && it.isDiscounted === true) {
            _.forEach(it.discounts, function (discount) {
              if (discount.type === 'fixed') {
                _itemD += parseFloat(discount.discountAmount);
                d += discount.discountAmount;
              }
              if (discount.type === 'percentage') {
                _itemD += parseFloat(discount.discountAmount);
                d += discount.discountAmount;
              }
            });
          }
           _itemBillDiscount+=Utils.roundNumber(it.billDiscountAmount,2);
          if(Utils.roundNumber(d) === Utils.roundNumber(it.subtotal)){
            _compItems += it.quantity;
            _compDiscount += Utils.roundNumber(d,2);
          } else {
            _nonCompItems += it.quantity;
            _nonCompDiscount += d;
          }
        });
        /* Flatten Tax */
        var _t = [];
        var _itemTotalTax = 0;
        var _allItemTaxes = [];
        _.forEach(i, function (_ite) {
          _.forEach(_ite.taxes, function (tax) {
            _allItemTaxes.push({
              id: tax._id + '_amt',
              name: tax.name,
              tax_amount: tax.baseRate_preTax? parseFloat(tax.baseRate_preTax): 0,
              isConsidered: false
            });
            _allItemTaxes.push({
              id: tax._id,
              name: tax.name,
              tax_amount: tax.tax_amount? parseFloat(tax.tax_amount): 0,
              isConsidered: true
            });
            _.forEach(tax.cascadingTaxes, function (_cTax) {
              if (_cTax.selected) {
                _allItemTaxes.push({
                  id:_cTax._id + "_amt",
                  name: _cTax.name,
                  tax_amount: (_.has(_cTax, 'baseRate_preTax')) ? Utils.roundNumber(parseFloat(_cTax.baseRate_preTax),2) : 0,
                  isConsidered: false
                });
                _allItemTaxes.push({
                  id: _cTax._id,
                  name: _cTax.name,
                  tax_amount: _cTax.tax_amount? Utils.roundNumber(parseFloat(_cTax.tax_amount),2): 0,
                  isConsidered: true
                });
              }
            });
          });
        });
        var _groupedItemTaxes = [];
        _.chain(_allItemTaxes).groupBy('id').map(function (_ts) {
          var _tsTotal = 0;
          _.forEach(_ts, function (_tswa) {
            _tsTotal += Utils.roundNumber(parseFloat(_tswa.tax_amount),2);
          });
          _groupedItemTaxes.push({
            id: _ts[0].id,
            name: _ts[0].name,
            isConsidered: _ts[0].isConsidered,
            tax_amount: parseFloat(_tsTotal)
          });
        });
        /* Calc Total Tax */
        _.forEach(_groupedItemTaxes, function (_taxwa) {
          if (_taxwa.isConsidered) {
            _itemTotalTax += Utils.roundNumber(parseFloat(_taxwa.tax_amount),2);
          }
        });
        if(i[0].category){
          if((i[0].category.superCategory== undefined||i[0].category.superCategory==null)) {
            i[0].category.superCategory = {};
            i[0].category.superCategory.superCategoryName = 'NA';
            i[0].category.superCategory._id = null;
          } else if(!i[0].category.superCategory.superCategoryName)
            i[0].category.superCategory.superCategoryName = "NA";
        } else {
          i[0].category = {};
          i[0].category._id = null;
          i[0].category.categoryName = "NA";
          i[0].category.superCategory = {};
          i[0].category.superCategory.superCategoryName = 'NA';
          i[0].category.superCategory._id = null;
        }
        if(!i[0].category){
          i[0].category = {
            categoryName: "NA",
            superCategory: {
              superCategoryName: "NA"
            }
          };
        } else {
          if(!i[0].category.superCategory){
            i[0].category.superCategory = {
              superCategoryName: "NA"
            }
          } else {
            if(!i[0].category.superCategory)
              i[0].category.superCategory.superCategoryName = "NA";
          }
        }
        var _i = {
          "id": i[0]._id,
          "name": i[0].name,
          "rate": i[0].rate,
          "quantity": _itemQ,
          "subtotal": parseFloat(_itemAmount),
          "taxes": _groupedItemTaxes,
          "itemNet": (parseFloat(_itemAmount) - parseFloat(_itemD)) + parseFloat(_itemTotalTax),
          "discount": _itemD,
          "tab": bill.tab,
          "billDiscountAmount": _itemBillDiscount,
          "category": i[0].category.categoryName,
          "superCategory":i[0].category.superCategory.superCategoryName,
          "compItems": _compItems,
          "_compDiscount": _compDiscount,
          "nonCompItems": _nonCompItems,
          "_nonCompDiscount": _nonCompDiscount
        };
        console.log(_i)
        bill._items.push(_i);
      });
    }
    console.log(bill)
  })

  _.forEach(bills, function (bill) {
    var isBillComplementary = false;
    if(bill.complimentary){
      isBillComplementary = true;
    }
    _.forEach(bill._items, function (item) {
      if(isBillComplementary){
        item.complementaryItemQty = item.quantity;
        item.nonComplementaryItemQty = 0;
      }
      else {
        item.complementaryItemQty = item.compItems;
        item.nonComplementaryItemQty = item.nonCompItems;
      }
     // console.log(moment(bill.closeTime).format('YYYY-MM-DD'));
      item.billDay = moment(bill.closeTime).format('YYYY-MM-DD');
      item.billTime = moment(bill.closeTime).format('HH:mm:ss');
      item.billCloseTime = bill.closeTime;
       // console.log(item.billDiscountAmount)
      /*Adding Add Ons*/
      if(_.has(item,'addOns')) {
        _.forEach(item.addOns,function(addon){
          item.subtotal+=addon.subtotal;
          _.forEach(addon.taxes,function(at){
            item.taxes.push(at);
          })
        })
      }
      /*End Add Ons*/
      _allItemsFor.push(item);
    });
  });
  /* Pipeline: Group by Date */
  var _groupedItems = [];
  _.chain(_allItemsFor).groupBy('billDay').map(function (items) {
    _.chain(items).groupBy('id').map(function (gItemsById) {
      //console.log(gItemsById);

      var _itemTotalInEachTab = {
        _qty: 0,
        _amount: 0,
        discountedAmount:0,
        _tax: 0,
        _discount: 0,
        _taxes: [],
        _complementaryItemQty: 0,
        _nonComplementaryItemQty: 0,
        totalAmount:0
      };

      _.forEach(gItemsById, function (itemInTab) {
        delete itemInTab['itemNet']; // Remove old Item Net and Recalculate
        //console.log(itemInTab);
        if(itemInTab.billDiscountAmount == undefined || !itemInTab.billDiscountAmount)
          itemInTab.billDiscountAmount = 0;
        _itemTotalInEachTab._qty += parseFloat(itemInTab.quantity);
        _itemTotalInEachTab._complementaryItemQty += parseFloat(itemInTab.complementaryItemQty);
        _itemTotalInEachTab._nonComplementaryItemQty += parseFloat(itemInTab.nonComplementaryItemQty);
        _itemTotalInEachTab.totalAmount += parseFloat(itemInTab.subtotal) ;
       
        _itemTotalInEachTab._amount += parseFloat(itemInTab.subtotal) - (parseFloat(itemInTab.billDiscountAmount) + parseFloat(itemInTab.discount));

        _itemTotalInEachTab._discount += parseFloat(itemInTab.discount) + parseFloat(itemInTab.billDiscountAmount);
        /* if( _itemTotalInEachTab.totalAmount ==  _itemTotalInEachTab._discount)
          { console.log('insideIf')
            itemInTab.billDiscountAmount = 0;
           _itemTotalInEachTab._discount += parseFloat(itemInTab.discount) 
          }*/
        _itemTotalInEachTab.discountedAmount += (parseFloat(itemInTab.subtotal) - (parseFloat(itemInTab.discount)+ parseFloat(itemInTab.billDiscountAmount)))

        /* Aggregate Taxes */
        _.forEach(itemInTab.taxes, function (iTax) {
          //console.log(iTax.tax_amount);
          if (iTax.isConsidered) {
            _itemTotalInEachTab._tax += parseFloat(iTax.tax_amount);
          }

          /* Tax Subtotal Amount */
          _itemTotalInEachTab._taxes.push({
            id: iTax.id,
            name: iTax.name + ' Amount',
            tax_amount: (!_.isNaN(iTax.baseRate_preTax)) ? parseFloat(iTax.baseRate_preTax) : 0,
            isConsidered: false
          });
          /* Tax Amount */
          _itemTotalInEachTab._taxes.push({
            id: iTax.id,
            name: iTax.name,
            tax_amount: parseFloat(iTax.tax_amount),
            isConsidered: true
          });

          if (iTax.length > 0) {
            _.forEach(iTax.cascadingTaxes, function (cITax) {
              _itemTotalInEachTab._tax += parseFloat(cITax.tax_amount);
              /* Tax Subtotal Amount */
              _itemTotalInEachTab._taxes.push({
                id: iTax.id,
                name: iTax.name + ' Amount',
                tax_amount: parseFloat(iTax.baseRate_preTax),
                isConsidered: false
              });
              /* Tax Amount */
              _itemTotalInEachTab._taxes.push({
                id: iTax.id,
                name: iTax.name,
                tax_amount: parseFloat(iTax.tax_amount),
                isConsidered: true
              });
            });
          }

        });
      });

      var allItemTaxes = [];
      _.forEach(gItemsById, function (_it) {
        _.forEach(_it.taxes, function (_itax) {
          allItemTaxes.push(_itax);
        });
      });

      //console.log("ALL ITEMS:", gItemsById);
      var _t = [];
      _.chain(allItemTaxes).groupBy('id').map(function (_iTaxes) {
        //console.log("GTA " ,_iTaxes);
        var _tAmt = 0;
        _.forEach(_iTaxes, function (_t) {
          _tAmt += parseFloat(_t.tax_amount);
        });
        _t.push({
          id: _iTaxes[0].id,
          name: _iTaxes[0].name,
          isConsidered: _iTaxes[0].isConsidered,
          tax_amount: _tAmt
        })
      });


      var _net = 0;
      //_net = (parseFloat(_itemTotalInEachTab._amount) - parseFloat(_itemTotalInEachTab._discount)) + parseFloat(_itemTotalInEachTab._tax);
      /*Changes done by ranjeet to accomodate billwise discount*/
      _net = (parseFloat(_itemTotalInEachTab.totalAmount) - parseFloat(_itemTotalInEachTab._discount)) + parseFloat(_itemTotalInEachTab._tax);
             console.log(_net,_itemTotalInEachTab._discount,_itemTotalInEachTab._amount)
      _groupedItems.push({
        id: gItemsById[0].id,
        name: gItemsById[0].name,
        tab: gItemsById[0].tab,
        billTime: gItemsById[0].billTime,
        billDay: gItemsById[0].billDay,
        billCloseTime: gItemsById[0].billCloseTime,
        category: gItemsById[0].category,
        superCategory: gItemsById[0].superCategory,
        totalQty: _itemTotalInEachTab._qty,
        totalAmount: _itemTotalInEachTab._amount,
        totalAmountNoDiscount:  _itemTotalInEachTab.totalAmount,
        totalDiscount: _itemTotalInEachTab._discount,
        discountAmount: _itemTotalInEachTab.discountedAmount,
        totalTax: _itemTotalInEachTab._tax,
        net: _net,
        roundOff: reportUtils.getRoundOff(_net),
        afterRoundOff: reportUtils.getAfterRoundOff(_net),
        taxes: _t,
        complementaryItemQty: _itemTotalInEachTab._complementaryItemQty,
        nonComplementaryItemQty: _itemTotalInEachTab._nonComplementaryItemQty
      });
    });
  });
  var csvRows = [];
  var grandTotal = {
    net: 0,
    roundOff: 0,
    afterRoundOff: 0,
    totalQty: 0,
    comp:0,
    nonComp:0,
    amount:0,
    totalTax:0,
    discount:0
  };

  var i = 0;
  csvRows.push(reportNameG)
  i = i+1
  // console.log(csvRows);
  var grpByCloseTime = _.groupBy(_groupedItems,function(bill){
    return bill.closeTime
  })
  // console.log(Nbills)
  // console.log(grpByCloseTime)
  _.forEach(grpByCloseTime, function (closeTime, key) {
    // console.log(key);
    csvRows.push(moment(key).format('L'));
    i=i+1;
    var grpBySuperCategory = _.groupBy(grpByCloseTime[key],function(time){
      return time.superCategory;
    })
    // console.log(grpBySuperCategory)
    _.forEach(grpBySuperCategory, function (sItems , sc) {
      var superCategoryTotal = {
        net: 0,
        roundOff: 0,
        afterRoundOff: 0,
        totalQty: 0,
        comp:0,
        nonComp:0,
        amount:0,
        totalTax:0,
        discount:0
      };
      csvRows.push('')
      csvRows.push('SuperCategory:'+sc);
      i = i+2;
      var cItems = _.groupBy(sItems,function(s){
        return s.category;
      })
        // console.log(cItems)
      _.forEach(cItems,function(cWise,cat){
        // console.log(cat);
        // console.log(cWise);
        csvRows.push('Category:'+cat);
        i = i+1;
        var category = {
          net: 0,
          roundOff: 0,
          afterRoundOff: 0,
          totalQty: 0,
          comp:0,
          nonComp:0,
          amount:0,
          discount:0,
          totalTax:0
        };
        if(!discountValue)
            csvRows.push('Item Name,Qty,Comp,Non Comp,Amount,Discount');
        else
          csvRows.push('Item Name,Qty,Comp,Non Comp,Amount');
        _.forEach(allTaxesFor,function(t){
            csvRows[i] = csvRows[i] + "," + t;
        })
        csvRows[i] = csvRows[i]+','+"Total Tax , Net Amount , Round Off , G.Total";
        i=i+1
        _.forEach(cWise,function(item,index){
          if(!discountValue)
            csvRows.push(item.name+','+item.quantity+','+item.complementaryItemQty+','+item.nonComplementaryItemQty+','+item.subtotal+','+item.totalDiscount);
          else
            csvRows.push(item.name+','+item.quantity+','+item.complementaryItemQty+','+item.nonComplementaryItemQty+','+(item.subtotal-item.totalDiscount));
          _.forEach(allTaxesFor,function(t,key){
            csvRows[i] = csvRows[i] + ','  + reportUtils.getTax(key, item).toFixed(2);
          })
          csvRows[i]=csvRows[i]+','+item.totalTax + ',' + item.net +','+item.roundOff +','+item.afterRoundOff;
          i=i+1;

          if(!discountValue) {
            grandTotal.amount += item.subtotal;
            grandTotal.discount += item.totalDiscount; 
            superCategoryTotal.amount += item.subtotal;
            superCategoryTotal.discount += item.totalDiscount;
            category.amount += item.subtotal;
            category.discount += item.totalDiscount;
          } else {
            grandTotal.amount += item.subtotal - item.totalDiscount
            superCategoryTotal.amount += item.subtotal - item.totalDiscount
            category.amount += item.subtotal - item.totalDiscount
          }
          grandTotal.net += Utils.roundNumber(item.net, 2);
          grandTotal.roundOff += Utils.roundNumber(item.roundOff, 2);
          grandTotal.afterRoundOff += Utils.roundNumber(item.afterRoundOff);
          grandTotal.totalQty += item.quantity;
          grandTotal.comp += item.complementaryItemQty;
          grandTotal.nonComp += item.nonComplementaryItemQty;
          grandTotal.totalTax += Utils.roundNumber(item.totalTax, 2);
          superCategoryTotal.net += Utils.roundNumber(item.net, 2);
          superCategoryTotal.roundOff += item.roundOff, 2;
          superCategoryTotal.afterRoundOff += item.afterRoundOff;
          superCategoryTotal.totalQty += item.quantity;
          superCategoryTotal.comp += item.complementaryItemQty;
          superCategoryTotal.nonComp += item.nonComplementaryItemQty;
          superCategoryTotal.totalTax += item.totalTax;
          category.net += Utils.roundNumber(item.net, 2);
          category.roundOff += Utils.roundNumber(item.roundOff, 2);
          category.afterRoundOff += item.afterRoundOff;
          category.totalQty += item.quantity;
          category.comp += item.complementaryItemQty;
          category.nonComp += item.nonComplementaryItemQty;
          category.totalTax += item.totalTax;
        })
        if(!discountValue) {
          csvRows.push('Category Total,'+category.totalQty+','+category.comp+','+category.nonComp+','+category.amount+','+category.discount);
        } else {
          csvRows.push('Category Total,'+category.totalQty+','+category.comp+','+category.nonComp+','+category.amount);
        }
        _.forEach(allTaxesFor,function(t,key){
          csvRows[i] = csvRows[i] + ','  + reportUtils.getTotalTaxRows(key,cWise).toFixed(2);
        })
        csvRows[i]=csvRows[i]+','+category.totalTax + ',' + category.net +','+category.roundOff +','+category.afterRoundOff;
        i=i+1;
      })
      if(!discountValue) {
        csvRows.push('SuperCategory Total,'+superCategoryTotal.totalQty+','+superCategoryTotal.comp+','+superCategoryTotal.nonComp+','+superCategoryTotal.amount+','+superCategoryTotal.discount); 
      } else {
        csvRows.push('SuperCategory Total,'+superCategoryTotal.totalQty+','+superCategoryTotal.comp+','+superCategoryTotal.nonComp+','+superCategoryTotal.amount); 
      }
      _.forEach(allTaxesFor,function(t,key){
        csvRows[i] = csvRows[i] + ','  + reportUtils.getTotalTaxRows(key,sItems);
      })
      csvRows[i]=csvRows[i]+','+superCategoryTotal.totalTax + ',' + superCategoryTotal.net +','+superCategoryTotal.roundOff +','+superCategoryTotal.afterRoundOff;
      i=i+1;
    });
    csvRows.push('')
    i=i+1
  });
  csvRows.push('')
  i=i+1
  if(!discountValue){
    csvRows.push('Grand Total,'+grandTotal.totalQty+','+grandTotal.comp+','+grandTotal.nonComp+','+grandTotal.amount+','+grandTotal.discount);  
  } else {
    csvRows.push('Grand Total,'+grandTotal.totalQty+','+grandTotal.comp+','+grandTotal.nonComp+','+grandTotal.amount);
  }
  
  _.forEach(allTaxesFor,function(t,key){
    csvRows[i] = csvRows[i] + ','  + reportUtils.getTotalTaxRows(key,_groupedItems);
  })
  csvRows[i]=csvRows[i]+','+grandTotal.totalTax + ',' + grandTotal.net +','+grandTotal.roundOff +','+grandTotal.afterRoundOff;

  var csvString = csvRows.join("\r\n");
  // console.log(csvString)
  var Key = new Buffer(reportNameG+':'+deployment).toString('base64');
  // detail.reportType = reportNameG
  // var _filename = reportNameG+'_'+(type2||'')+'('+moment(detail.fromDate).format('YYYY.MM.DD')+'--'+moment(detail.toDate).format('YYYY.MM.DD')+')'+Key+'.csv'
  var _p = path.join(process.cwd(), 'uploads', '_filename.csv');
  // detail.filename = _filename
  // var exportButton = document.createElement('a');
  fs.writeFile(_p, csvString, function(err) {
    if(err) {
        return console.log(err);
    }
  })
  //   // request.post('http://test.posist.net/api/reports/',{form:detail}, function(err,httpResponse,body){console.log('SUCCESSFULL')});  
    // Report.create(detail,function(err,result){if(err) {console.log("NOT SAVED", err)} else console.log(result)})
  //   // emailSend(null,_filename,_p)
  // }); 
  // exportToCsv(csvString)
}
exportToCsvForCategory()